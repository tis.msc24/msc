Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Interface1
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim dt As DataTable = Session("DataCOA")
        Dim bVal As Boolean = False
        If dt.Rows.Count > 0 Then
            bVal = True
        End If
        
        If bVal = False Then
            sError &= "- Please select minimally 1 COA field!<BR>"
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("GetDataCOA") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetDataCOA")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acctgoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("GetDataCOA") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("GetDataCOAView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetDataCOA")
            Dim dtTbl2 As DataTable = Session("GetDataCOAView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCOA.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCOA.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acctgoid=" & cbOid
                                dtView2.RowFilter = "acctgoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("GetDataCOA") = dtTbl
                Session("GetDataCOAView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub CreateDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trntwdtl")
        dtlTable.Columns.Add("nomer", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("acctgdbcr", Type.GetType("System.String"))
        dtlTable.Columns.Add("gendesc", Type.GetType("System.String"))
        Session("DataCOA") = dtlTable
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub GetDataCOA()
        sSql = "Select 'False' AS checkvalue, ROW_NUMBER() over (order by a.acctgoid asc) nomer, a.acctgoid, a. acctgcode, a.acctgdesc, a.acctgdbcr, c.gendesc From ql_mstacctg a INNER JOIN QL_mstgen c ON a.BRANCH_CODE=c.gencode AND gengroup='CABANG'/* Where c.gencode='" & DDLBranch.SelectedValue & "'*/"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "GetDataCOA")
        Session("GetDataCOA") = dt
        Session("GetDataCOAView") = dt
    End Sub

    Private Sub BindMstData(ByVal sFilter As String)
        'ISNULL(divname, interfaceres1) AS interfaceres1,
        Dim cCabang As String = ""
        If FilterDDlBranch.SelectedValue <> "ALL" Then
            cCabang = "AND ISNULL(interfaceres1, '')='" & FilterDDlBranch.SelectedValue & "'"
        End If

        sSql = "SELECT interfaceoid, ISNULL(interfaceres1, interfaceres1) AS interfaceres1, interfacevar, c.gendesc, interfacevalue, i.activeflag, interfacenote, ISNULL(interfaceres2, '') AS interfaceres2 FROM QL_mstinterface i LEFT JOIN QL_mstgen c On c.gencode=i.interfaceres1 AND c.gengroup='CABANG' WHERE i.cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(sFilter) & "%' " & cCabang & " AND interfaceres1 <> 'MSC'"

        If cbType.Checked Then
            sSql &= " AND interfaceres2='" & FilterDDLType.SelectedValue & "'"
        End If

        If cbStatus.Checked Then
            sSql &= " AND i.activeflag='" & FilterDDLStatus.SelectedValue & "'"
        End If

        sSql &= " ORDER BY interfacevar"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstinterface")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub FillTextBox()
        Try
            sSql = "SELECT interfaceoid, interfacevar, REPLACE(interfacevalue, ' ', '') interfacevalue, interfacenote, activeflag, upduser, updtime, ISNULL(interfaceres1, 'All') AS interfaceres1, interfaceres2 FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                interfaceoid.Text = xreader("interfaceoid").ToString
                interfacevar.Text = xreader("interfacevar").ToString
                interfacevalue.Text = xreader("interfacevalue").ToString
                DDLBranch.SelectedValue = xreader("interfaceres1").ToString
                interfacenote.Text = xreader("interfacenote").ToString
                activeflag.SelectedValue = xreader("activeflag")
                createuser.Text = xreader("upduser").ToString
                createtime.Text = xreader("updtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                interfaceres2.Text = xreader("interfaceres2").ToString
            End While
            xreader.Close()
            conn.Close()
            sSql = "Select ROW_NUMBER() over (order by p.acctgoid asc) nomer, p.acctgoid, p.acctgcode, p.acctgdesc, p.acctgdbcr, (SELECT gendesc FROM QL_mstgen c WHERE p.BRANCH_CODE=c.gencode AND gengroup='CABANG') gendesc from QL_mstacctg p Inner JOIN (SELECT Name FROM splitstring('" & interfacevalue.Text & "')) d ON d.Name=p.acctgcode"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "DataCOA")
            gvDataCoa.DataSource = dt
            gvDataCoa.DataBind()
            Session("DataCOA") = dt
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub InitBranch()
        sSql = "Select gencode, gendesc from QL_mstgen Where gengroup='Cabang' And cmpcode='" & CompnyCode & "'"
        FillDDL(DDLBranch, sSql)
        FillDDL(FilterDDlBranch, sSql)
        FilterDDlBranch.Items.Add("ALL")
        FilterDDlBranch.SelectedValue = "ALL"
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("mstinterface.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If

        Page.Title = CompnyName & " - Interface"
        'Session("oid") = Request.QueryString("interfaceoid")
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            I_U.Text = "New Data"
        Else
            I_U.Text = "Update Data"
        End If
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID")
            createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            InitBranch()
            btnSaveAs.Visible = False
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox()
                TabContainer1.ActiveTabIndex = 1
            Else
                interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                btnSave.Visible = False
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        If Not Session("EmptyListCOA") Is Nothing And Session("EmptyListCOA") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCOA") Then
                Session("EmptyListCOA") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCOA") Is Nothing And Session("WarningListCOA") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCOA") Then
                Session("WarningListCOA") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindMstData(FilterText.Text)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        'cbBusUnit.Checked = False
        'FilterDDlBranch.SelectedIndex = -1 : 
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1 : cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1 : BindMstData(FilterText.Text)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(4).Text.ToUpper = "ALL" Then
                e.Row.Cells(5).Enabled = False
            Else
                e.Row.Cells(5).Enabled = True
            End If
        End If
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMst.SelectedIndexChanged
        Try
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"

            sSql = "SELECT interfacevar, interfacevalue, interfacenote, activeflag, interfaceres2 FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & gvMst.SelectedDataKey.Item("interfaceoid").ToString
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                interfacevar.Text = xreader("interfacevar").ToString
                interfacevalue.Text = xreader("interfacevalue").ToString
                interfacenote.Text = xreader("interfacenote").ToString
                activeflag.SelectedValue = xreader("activeflag")
                interfaceres2.Text = xreader("interfaceres2").ToString
            End While
            xreader.Close() : conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        Finally
            btnSave.Visible = False : btnSaveAs.Visible = True
        End Try
        TabContainer1.ActiveTabIndex = 1
    End Sub

    Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvMst.DataSource = Session("TblMst")
            gvMst.DataBind()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim sValue As String = ""
            Dim dt As DataTable = Session("DataCOA")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                sValue &= dt.Rows(C1)("acctgcode") & ", "
            Next

            If sValue <> "" Then
                sValue = Left(sValue, sValue.Length - 2)
            End If

            If sValue.Length > 4000 Then
                sValue = Left(sValue, 4000)
            End If

            Dim sSplit() As String = sValue.Split(", ")
            If interfaceres2.Text = "Single COA" Then
                If sSplit.Length > 1 Then
                    showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                    Exit Sub
                Else
                    sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sSplit(0) & "%' AND acctgflag='A'"
                    If ToDouble(GetStrData(sSql).ToString) > 1 Then
                        showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                        Exit Sub
                    End If
                End If
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_mstinterface (cmpcode, interfaceoid, interfacevar, interfacevalue, interfacegroup, interfacenote, activeflag, upduser, updtime, interfaceres1, interfaceres2) VALUES ('" & CompnyCode & "', " & interfaceoid.Text & ", '" & interfacevar.Text & "', '" & sValue & "', 'ACCTG', '" & Tchar(interfacenote.Text) & "', '" & activeflag.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DDLBranch.SelectedValue & "', '" & interfaceres2.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & interfaceoid.Text & " WHERE tablename LIKE 'QL_MSTINTERFACE' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstinterface SET interfacevalue='" & sValue & "', interfacenote='" & Tchar(interfacenote.Text) & "', activeflag='" & activeflag.SelectedValue & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, interfaceres1='" & DDLBranch.SelectedValue & "' WHERE cmpcode='" & CompnyCode & "' AND interfaceoid=" & interfaceoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : conn.Close()
            Catch ex As Exception
                objTrans.Rollback() : conn.Close()
                showMessage(ex.ToString & sSql, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstInterface.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\mstInterface.aspx?awal=true")
    End Sub 

    Protected Sub btnSaveAs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveAs.Click
        If IsInputValid() Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & interfacevar.Text & "' AND interfaceres1='" & DDLBranch.SelectedValue & "'"
            If CheckDataExists(sSql) Then
                showMessage("Variable : " & interfacevar.Text.ToUpper & " has been defined for Business Unit : " & DDLBranch.SelectedItem.Text.ToUpper & " in another data. Please choose another combination!", 2)
                Exit Sub
            End If

            Dim sValue As String = ""
            Dim dt As DataTable = Session("DataCOA")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                sValue &= dt.Rows(C1)("acctgcode") & ", "
            Next

            If sValue <> "" Then
                sValue = Left(sValue, sValue.Length - 2)
            End If

            If sValue.Length > 2000 Then
                sValue = Left(sValue, 2000)
            End If
            Dim sSplit() As String = sValue.Split(", ")
            If interfaceres2.Text = "Single COA" Then
                If sSplit.Length > 1 Then
                    showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                    Exit Sub
                Else
                    sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sSplit(0) & "%' AND activeflag='ACTIVE'"
                    If ToDouble(GetStrData(sSql).ToString) > 1 Then
                        showMessage("Selected COA for this variable must be 1 (Single COA)!", 2)
                        Exit Sub
                    End If
                End If
            End If

            interfaceoid.Text = GenerateID("QL_MSTINTERFACE", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "INSERT INTO QL_mstinterface (cmpcode, interfaceoid, interfacevar, interfacevalue, interfacegroup, interfacenote, activeflag, upduser, updtime, interfaceres1, interfaceres2) VALUES ('" & CompnyCode & "', " & interfaceoid.Text & ", '" & interfacevar.Text & "', '" & sValue & "', 'ACCTG', '" & Tchar(interfacenote.Text) & "', '" & activeflag.SelectedValue & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & DDLBranch.SelectedValue & "', '" & interfaceres2.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & interfaceoid.Text & " WHERE tablename='QL_MSTINTERFACE' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                objTrans.Commit()
                conn.Close()
            Catch ex As Exception
                objTrans.Rollback()
                conn.Close()
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            Response.Redirect("~\Master\mstInterface.aspx?awal=true")
        End If
    End Sub

#End Region

    Protected Sub BtnBindCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBindCOA.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
        Session("GetDataCOA") = Nothing : Session("GetDataCOAView") = Nothing
        gvListCOA.DataSource = Session("GetDataCOA") : gvListCOA.DataBind()
        GetDataCOA()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub BtnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnFindListCOA.Click
        If Session("GetDataCOA") Is Nothing Then
            GetDataCOA()
            If Session("GetDataCOA").Rows.Count <= 0 Then
                Session("EmptyListCOA") = "Data can't be found!"
                showMessage(Session("EmptyListCOA"), 2)
                Exit Sub
            End If
        End If

        Dim sPlus As String = FilterDDLListCust.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListCust.Text) & "%'"

        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("GetDataCOA").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("GetDataCOAView") = dv.ToTable
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind() : dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("GetDataCOAView") = Nothing
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind()
                Session("WarningListCOA") = "Data can't be found!"
                showMessage(Session("WarningListCOA"), 2)
            End If
        Else
            Session("WarningListCOA") = "Data can't be found!"
            showMessage(Session("WarningListCOA"), 2)
        End If

    End Sub

    Protected Sub BtnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAllListCOA.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
        If Session("GetDataCOA") Is Nothing Then
            GetDataCOA()
            If Session("GetDataCOA").Rows.Count <= 0 Then
                Session("EmptyListCOA") = "COA data can't be found!"
                showMessage(Session("EmptyListCOA"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("GetDataCOA")
            Session("GetDataCOAView") = dt
            gvListCOA.DataSource = Session("GetDataCOAView")
            gvListCOA.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCOA") = "COA data can't be found!"
            showMessage(Session("WarningListCOA"), 2)
        End If
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCOA.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCOA.PageIndex = e.NewPageIndex
            gvListCOA.DataSource = Session("GetDataCOAView")
            gvListCOA.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub LbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LbCloseListCOA.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub BtnSelectAllCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSelectAllCOA.Click
        If Not Session("GetDataCOAView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetDataCOAView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("GetDataCOA")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("GetDataCOA") = objTbl
                Session("GetDataCOAView") = dtTbl
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCOA") = "Please show data first!"
            showMessage(Session("WarningListCOA"), 2)
        End If
    End Sub

    Protected Sub BtnSelectNoneCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSelectNoneCOA.Click
        If Not Session("GetDataCOAView") Is Nothing Then
            Dim dtTbl As DataTable = Session("GetDataCOAView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("GetDataCOA")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("GetDataCOA") = objTbl
                Session("GetDataCOAView") = dtTbl
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCOA") = "Please show data first!"
            showMessage(Session("WarningListCOA"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCOA.Click
        If Session("GetDataCOA") Is Nothing Then
            Session("WarningListCOA") = "Selected data can't be found!"
            showMessage(Session("WarningListCOA"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("GetDataCOA")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = ""
                Session("GetDataCOAView") = dtView.ToTable
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("GetDataCOAView") = Nothing
                gvListCOA.DataSource = Session("GetDataCOAView")
                gvListCOA.DataBind()
                Session("WarningListCOA") = "Selected data can't be found!"
                showMessage(Session("WarningListCOA"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub LbAddToListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LbAddToListCOA.Click
        If Not Session("GetDataCOA") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("GetDataCOA")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If interfacevalue.Text <> "" Then
                            interfacevalue.Text &= "," & dtView(C1)("acctgcode")
                        Else
                            interfacevalue.Text = dtView(C1)("acctgcode")
                        End If
                    Next

                    If Session("DataCOA") Is Nothing Then
                        CreateDetail()
                    End If 

                    Dim objTable As DataTable = Session("DataCOA")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "acctgoid=" & dtView(C1)("acctgoid") & ""
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("acctgcode") = dtView(C1)("acctgcode")
                            dv(0)("acctgdesc") = dtView(C1)("acctgdesc")
                            dv(0)("gendesc") = dtView(C1)("gendesc")
                            dv(0)("acctgdbcr") = dtView(C1)("acctgdbcr")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("nomer") = counter
                            objRow("acctgoid") = dtView(C1)("acctgoid")
                            objRow("acctgcode") = dtView(C1)("acctgcode")
                            objRow("acctgdesc") = dtView(C1)("acctgdesc")
                            objRow("gendesc") = dtView(C1)("gendesc")
                            objRow("acctgdbcr") = dtView(C1)("acctgdbcr") 
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    gvDataCoa.DataSource = objTable
                    gvDataCoa.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCOA") = "Please select data to add to list!"
                    showMessage(Session("WarningListCOA"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCOA") = "Please show data first!"
            showMessage(Session("WarningListCOA"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub gvDataCoa_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDataCoa.RowDeleting
        Dim objTable As DataTable = Session("DataCOA")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("nomer") = C1 + 1
            dr.EndEdit()
        Next
        Session("DataCOA") = objTable
        gvDataCoa.DataSource = objTable
        gvDataCoa.DataBind()
    End Sub
End Class
