<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnDeliveryOrder.aspx.vb" Inherits="trnDeliveryOrder" Title="PT. MULTI SARANA COMPUTER" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        onclick="return tbRight_onclick()" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Delivery Order"></asp:Label></th>
        </tr>
        <tr>
            <th align="left" style="background-color: transparent" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">&nbsp;List of Delivery Order :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <contenttemplate>
<asp:Panel style="MARGIN-TOP: 4px" id="pnlFind" runat="server" Width="100%" __designer:wfdid="w26" DefaultButton="btnfind"><TABLE><TBODY><TR><TD  align=left>Cabang</TD><TD align=left><asp:DropDownList id="fDdlcabang" runat="server" CssClass="inpText" __designer:wfdid="w27">
                                                        </asp:DropDownList></TD></TR><TR><TD  align=left>Filter </TD><TD align=left><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w28">
                                                            <asp:ListItem Value="trnsjjualno">No. DO</asp:ListItem>
                                                            <asp:ListItem Value="p.orderno">Order No</asp:ListItem>
                                                            <asp:ListItem Value="custname">Customer</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="txtFindSJ" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w29"></asp:TextBox></TD></TR><TR><TD  align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w30" AutoPostBack="False" Checked="True"></asp:CheckBox></TD><TD  align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w31" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton> <asp:Label id="Label16" runat="server" Text="to" __designer:wfdid="w33"></asp:Label> &nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w34"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton> &nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w36"></asp:Label> </TD></TR><TR><TD  align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w37"></asp:CheckBox></TD><TD align=left><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w38" AutoPostBack="True">
                                                            <asp:ListItem Value="All">All</asp:ListItem>
                                                            <asp:ListItem>In Process</asp:ListItem>
                                                            <asp:ListItem>In Approval</asp:ListItem>
                                                            <asp:ListItem>Approved</asp:ListItem>
                                                            <asp:ListItem>Rejected</asp:ListItem>
                                                            <asp:ListItem>Revise</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;<asp:ImageButton id="btnfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <asp:ImageButton id="btnviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w41" Visible="False"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvTblData" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w42" DataKeyNames="branch_code,trnsjjualmstoid,trnsjjualno" PageSize="8" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvTblData_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" Font-Size="" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualno" HeaderText="No. DO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdate" HeaderText="Trans. Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size=""></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="delivdate" DataFormatString="{0:d}" HeaderText="Plan Deliv Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="Order No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Font-Size="" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="imbPrintFromList" onclick="imbPrintFromList_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" ToolTip="" CommandArgument='<%# Eval("trnsjjualmstoid") %>'></asp:ImageButton> <ajaxToolkit:ConfirmButtonExtender id="cbePrintFromList" runat="server" TargetControlID="imbPrintFromList" ConfirmText="Are You Sure Print This Data ?">
                                                                        </ajaxToolkit:ConfirmButtonExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="Label90" runat="server" Font-Size="" ForeColor="Red" 
                                                                    Text="No data found!!"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" BorderStyle="None" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w43" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy" TargetControlID="FilterPeriod1">
                                                        </ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w44" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy" TargetControlID="FilterPeriod2">
                                                        </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w45" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w46" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                        </ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvTblData"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Delivery Order</span></strong> <strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 160px" align=left><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information :" __designer:wfdid="w115"></asp:Label> </TD><TD style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:Label id="I_u2" runat="server" Font-Size="" ForeColor="Red" Text="New Detail" __designer:wfdid="w116" Visible="False"></asp:Label> <asp:Label id="I_U" runat="server" Font-Size="" ForeColor="Red" Text="New" __designer:wfdid="w117" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 160px" align=left>Cabang</TD><TD align=left><asp:DropDownList id="CabangDDL" runat="server" Width="133px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w118" AppendDataBoundItems="True" AutoPostBack="True"></asp:DropDownList> <asp:Label id="ordermstoid" runat="server" __designer:wfdid="w119" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="PromoOid" runat="server" __designer:wfdid="w120" Visible="False"></asp:Label><asp:Label id="SpgOid" runat="server" __designer:wfdid="w9" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="createtime" runat="server" __designer:wfdid="w121" Visible="False"></asp:Label><asp:Label id="typeSO" runat="server" __designer:wfdid="w122" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 160px" align=left>No. DO</TD><TD align=left><asp:TextBox id="trnsjjualno" runat="server" Width="130px" CssClass="inpTextNr" __designer:wfdid="w123" MaxLength="20" ReadOnly="True"></asp:TextBox> <asp:Label id="trnsjjualmstoid" runat="server" __designer:wfdid="w124" Visible="False"></asp:Label></TD><TD align=left>Tanggal DO</TD><TD align=left><asp:TextBox id="salesdeliveryshipdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w125" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w126" Visible="False"></asp:ImageButton> </TD></TR><TR><TD style="WIDTH: 160px" align=left>No. SO&nbsp;<asp:Label id="Label10" runat="server" CssClass="Important" Text="*" __designer:wfdid="w127"></asp:Label></TD><TD align=left><asp:TextBox id="orderno" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w128" MaxLength="50"></asp:TextBox> <asp:ImageButton id="btnsearhorder" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w129"></asp:ImageButton> <asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w130"></asp:ImageButton></TD><TD align=left>Tanggal SO</TD><TD align=left><asp:TextBox id="trnsjjualdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w131" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 160px" align=left><asp:Label id="flagCash" runat="server" __designer:wfdid="w15" Visible="False"></asp:Label><asp:Label id="trnpaytypeoid" runat="server" __designer:wfdid="w10" Visible="False"></asp:Label> <asp:Label id="amtjualnetto" runat="server" __designer:wfdid="w2" Visible="False"></asp:Label><asp:Label id="crUsage" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label><asp:Label id="periodacctg" runat="server" Width="1px" Font-Size="" ForeColor="Red" __designer:wfdid="w132" Visible="False"></asp:Label><asp:Label id="crLimit" runat="server" __designer:wfdid="w12" Visible="False">0</asp:Label><asp:Label id="termin" runat="server" __designer:wfdid="w13" Visible="False"></asp:Label><asp:Label id="custflag" runat="server" __designer:wfdid="w11" Visible="False"></asp:Label><asp:Label id="flagTax" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:GridView id="gvListSO" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w133" Visible="False" OnPageIndexChanging="gvListSO_PageIndexChanging" DataKeyNames="ordermstoid " PageSize="5" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="No. SO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="delivdate" HeaderText="Plan Deliv Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SOType" HeaderText="Type SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ExpSO" HeaderText="Expired SO" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="5px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ordermstoid" HeaderText="Draft SO">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label91" runat="server" 
                                                                        Font-Size="" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 160px" align=left>Customer</TD><TD align=left><asp:TextBox id="custname" runat="server" Width="202px" CssClass="inpTextDisabled" __designer:wfdid="w134" ReadOnly="True"></asp:TextBox> <asp:Label id="trncustoid" runat="server" Font-Size="" __designer:wfdid="w2" Visible="False"></asp:Label></TD><TD align=left>Location</TD><TD align=left><asp:DropDownList id="itemLoc" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w135" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 160px" align=left>Expedisi</TD><TD align=left><asp:DropDownList id="Ekspedisi" runat="server" Width="133px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w136" AppendDataBoundItems="True"></asp:DropDownList></TD><TD align=left>No Polisi</TD><TD align=left><asp:TextBox id="nopolisi" runat="server" Width="98px" CssClass="inpText" __designer:wfdid="w137" MaxLength="10"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 160px" align=left>Note</TD><TD align=left><asp:TextBox id="note" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w138" MaxLength="30"></asp:TextBox></TD><TD align=left>Status</TD><TD align=left><asp:TextBox id="posting" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w139" ReadOnly="True">In Process</asp:TextBox></TD></TR><TR><TD style="WIDTH: 160px" align=left></TD><TD align=left><asp:TextBox id="containerno" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w141" Visible="False" MaxLength="30"></asp:TextBox></TD><TD align=left></TD><TD align=left><asp:TextBox id="trnsjjualsenddate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w142" Visible="False" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w143" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 160px" align=left><asp:Label id="Label3" runat="server" Width="229px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Information :" __designer:wfdid="w144"></asp:Label></TD><TD align=left colSpan=3><asp:Label id="itemoid" runat="server" Font-Size="" __designer:wfdid="w145" Visible="False"></asp:Label><asp:Label id="trnsjjualdtlseq" runat="server" Font-Size="" __designer:wfdid="w146" Visible="False"></asp:Label><asp:Label id="orderdtloid" runat="server" Font-Size="" __designer:wfdid="w147" Visible="False"></asp:Label><asp:Label id="trnjualdtloid" runat="server" Font-Size="" __designer:wfdid="w149" Visible="False"></asp:Label><asp:Label id="orderflag" runat="server" Font-Size="" __designer:wfdid="w152" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 160px" align=left>Katalog</TD><TD align=left><asp:TextBox id="itemname" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w153" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbFindItem" onclick="imbFindItem_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w154"></asp:ImageButton> <asp:ImageButton id="imbClearItem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w155"></asp:ImageButton></TD><TD align=left><asp:Label id="lblQty" runat="server" Text="Sisa Qty" __designer:wfdid="w156"></asp:Label></TD><TD align=left><asp:TextBox id="SisaQty" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w157" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="sounitdesc" runat="server" Font-Bold="True" __designer:wfdid="w158">BUAH</asp:Label></TD></TR><TR><TD style="WIDTH: 160px" align=left><asp:Label id="jenisprice" runat="server" __designer:wfdid="w159" Visible="False"></asp:Label><asp:Label id="ItemCode" runat="server" Font-Bold="False" __designer:wfdid="w151" Visible="False"></asp:Label> <asp:Label id="ItemBarcode" runat="server" Font-Bold="False" __designer:wfdid="w151" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:GridView id="gvReference" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w162" DataKeyNames="itemoid" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="gvReference_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itembarcode1" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SisaQty" HeaderText="Sisa Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtySJ" HeaderText="Delivered Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="StokAkhir" HeaderText="Stock Akhir">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisprice" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label66" runat="server" ForeColor="Red" Font-Size="" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 160px" align=left>Delivered Qty</TD><TD align=left><asp:TextBox id="delivorder" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w163" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="deliveredUnit" runat="server" __designer:wfdid="w164">BUAH</asp:Label></TD><TD align=left>Stok Qty</TD><TD align=left><asp:TextBox id="StokQty" runat="server" Width="70px" CssClass="inpTextDisabled" __designer:wfdid="w165" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="LabeLStockUnit" runat="server" __designer:wfdid="w166">BUAH</asp:Label></TD></TR><TR><TD style="WIDTH: 160px" align=left>Quantity <asp:Label id="Label9" runat="server" CssClass="Important" Text="*" __designer:wfdid="w167"></asp:Label></TD><TD align=left><asp:TextBox id="trnsjjualdtlqty" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w168" AutoPostBack="True" MaxLength="8" OnTextChanged="trnsjjualdtlqty_TextChanged"></asp:TextBox>&nbsp;<asp:Label id="sounit" runat="server" Font-Bold="True" __designer:wfdid="w169">BUAH</asp:Label></TD><TD align=right><asp:ImageButton id="btnAddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w170"></asp:ImageButton></TD><TD align=left><asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w171"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 920px; HEIGHT: 155px; BACKGROUND-COLOR: beige"><asp:GridView id="tbldtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" DataKeyNames="trnsjjualdtlseq " GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="tbldtl_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualdtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itembarcode1" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtySJ" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Gudang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisprice" HeaderText="Jenis Price">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="LinkButton9" runat="server" __designer:wfdid="w3" CommandName='<%# Eval("QtySJ") %>' CommandArgument='<%# Eval("itemlongdesc") %>' ToolTip='<%# Eval("itemoid") %>'>Detail No. Roll</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Font-Size="" ForeColor="Red" Text="No Detail Data!!"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD align=left colSpan=4><asp:Label id="lbltext" runat="server" Font-Size="" Font-Bold="True" ForeColor="#585858" Text="Create" __designer:wfdid="w174"></asp:Label> By <asp:Label id="updUser" runat="server" Font-Size="" Font-Bold="True" ForeColor="#585858" Text="Upduser" __designer:wfdid="w175"></asp:Label> On <asp:Label id="updTime" runat="server" Font-Size="" Font-Bold="True" ForeColor="#585858" Text="Updtime" __designer:wfdid="w176"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w179"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbSendApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" __designer:wfdid="w180" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w618" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrintSJ" onclick="btnPrintSJ_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w181" Visible="False"></asp:ImageButton> <asp:Label id="totalqty" runat="server" __designer:wfdid="w198" Visible="False"></asp:Label> <asp:Label id="trnsjjualdtlnote" runat="server" __designer:wfdid="w196" Visible="False"></asp:Label></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w182" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div7" class="progressBackgroundFilter"></DIV><DIV id="Div8" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w183"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="cesend" runat="server" __designer:wfdid="w184" PopupButtonID="ImageButton1" Format="dd/MM/yyyy" TargetControlID="trnsjjualsenddate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w185" TargetControlID="trnsjjualdate" CultureName="en-US" MessageValidatorTip="False" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meesend" runat="server" __designer:wfdid="w186" TargetControlID="trnsjjualsenddate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w187" TargetControlID="trnsjjualdtlqty" ValidChars="0123456789."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="salesdeliveryshipdate_CE" runat="server" __designer:wfdid="w188" PopupButtonID="ImageButton2" Format="dd/MM/yyyy" TargetControlID="salesdeliveryshipdate">
                                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="salesdeliveryshipdate_MEE" runat="server" __designer:wfdid="w189" TargetControlID="salesdeliveryshipdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                            </ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrintSJ"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></th>
        </tr>
    </table>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" CssClass="Important"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPreview" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" Visible="False"><TABLE width="100%" align=center><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal"></asp:Label> </TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="100%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="desc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 15px" align=center>&nbsp;<asp:LinkButton id="lkbClosePreview" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton> </TD></TR></TABLE></asp:Panel> <asp:Button id="beHidePreview" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" TargetControlID="beHidePreview" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptPreview" PopupControlID="pnlPreview" Drag="True">
                                                            </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>