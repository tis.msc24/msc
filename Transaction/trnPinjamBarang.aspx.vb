Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class PinjamBarang
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    'Dim cClass As New ClassProc
    Dim cust As String = ""
    Dim jenis As String = ""
    Dim brand As String = ""
    Dim flag As String = ""
    Dim sColom As String
    Dim sValue As String
    Dim iVal As Integer
    Dim iCount As Integer = 0
    Dim vreport As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal message As String)
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub showMessageprint(ByVal id As Integer, ByVal no As String, ByVal other As String)
        idTemp.Text = id : NoTemp.Text = no
        OtherTemp.Text = other
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        If fCabangDDL.SelectedValue <> "ALL" Then
            sWhere &= " AND branch_code='" & fCabangDDL.SelectedValue & "'"
        End If
        sSql = "select branch_code,trnpinjammstoid,trnpinjamno,CONVERT(varchar(10), trndatepinjam, 101) trndatepinjam,trndatepinjam trndate, namapeminjam, status from QL_trnpinjammst WHERE cmpcode ='" & CompnyCode & "' " & sWhere & " order by trndate desc "
        FillGV(gvPenerimaan, sSql, "ql_trnpinjammst")
    End Sub 

    Private Sub FillTextBox(ByVal Cabang As String, ByVal foid As Integer)
        sSql = "Select branch_code,updtime,upduser, trnpinjammstoid,trnpinjamno, namapeminjam, note, convert(varchar(10),trndatepinjam, 101) trndatepinjam, status from QL_trnpinjammst p Where cmpcode= '" & CompnyCode & "' AND trnpinjammstoid=" & foid & " AND branch_code='" & Cabang & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                ddlFromcabang.SelectedValue = xreader("branch_code").ToString
                PinjamOid.Text = xreader("trnpinjammstoid").ToString.Trim
                txtNoTanda.Text = xreader("trnpinjamno").ToString.Trim
                txtNamaCust.Text = xreader("namapeminjam").ToString.Trim
                txtStatus.Text = xreader("status").ToString.Trim
                txtTglTerima.Text = xreader("trndatepinjam").ToString.Trim
                txtDetailPinjam.Text = xreader("note").ToString.Trim
                UpdTime.Text = xreader("updtime").ToString.Trim
                UpdUser.Text = xreader("upduser").ToString.Trim
                periodacctg.Text = GetPeriodAcctg(GetServerTime())
            End While
        End If
        xreader.Close() : conn.Close()

        ddlFromcabang.Enabled = False
        If txtStatus.Text = "In Process" Then
            btnPost.Visible = True
        Else
            btnPost.Visible = False : btnAddToList.Visible = False
            btnClear.Visible = False : btnSave.Visible = False
            btnDelete.Visible = False : btnSearchCust.Enabled = False
            btnErase.Enabled = False : gvPinjamBarang.Enabled = False
            printmanualpenerimaan.Visible = True
        End If

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "Select pd.flagbarang, pd.unitoid, pd.trnpinjamseq sequence,i.itemcode matcode, i.itemdesc matlongdesc, pd.itemqty qty, pd.itemoid matoid,Case pd.flagbarang When 0 then 0 Else pd.mtrlocoid End mtrlocoid,Case pd.flagbarang When 0 then ISNULL((select COUNT(itemoid) from QL_trnfixmst fm Where fm.itemoid=i.itemoid AND fm.branch_code='" & ddlFromcabang.SelectedValue & "' AND fm.fixflag='POST') - pd.itemqty,0.00) Else Isnull((Select SUM(qtyIn)-SUM(qtyOut) From QL_conmtr crd Where crd.refoid = i.itemoid and crd.periodacctg='" & periodacctg.Text & "' And crd.mtrlocoid <> -10 and crd.mtrlocoid=pd.mtrlocoid),0.00) end saldoakhir, i.stockflag From ql_trnpinjamdtl pd inner join QL_MSTITEM i on i.itemoid=pd.itemoid WHERE pd.cmpcode = '" & CompnyCode & "' AND pd.trnpinjammstoid = '" & foid & "' ORDER BY pd.trnpinjamseq Asc"
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckoneksi.ambiltabel(sSql, "tblitemdtl")
        gvPinjamBarang.DataSource = dtab
        gvPinjamBarang.DataBind()
        Session("tblitemdtl") = dtab
        i_u2.Text = "new"

    End Sub

    Private Sub FilterBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabangDDL, sSql)
            Else
                FillDDL(fCabangDDL, sSql)
                fCabangDDL.Items.Add(New ListItem("ALL", "ALL"))
                fCabangDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(fCabangDDL, sSql)
            fCabangDDL.Items.Add(New ListItem("ALL", "ALL"))
            fCabangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub DDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlFromcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlFromcabang, sSql)
            Else
                FillDDL(ddlFromcabang, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(ddlFromcabang, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Jenis Barang
        FillDDL(DDLLoc, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND a.gengroup = 'LOCATION' and a.genother6='UMUM' AND b.gengroup = 'WAREHOUSE' AND a.genother2=(select genoid from ql_mstgen where gencode = '" & ddlFromcabang.SelectedValue & "' And gengroup='cabang') AND a.cmpcode='" & CompnyCode & "' AND a.genoid <> '-10' ORDER BY a.gendesc Asc")
    End Sub

    Private Sub GenerateReqCode()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & ddlFromcabang.SelectedValue & "' AND gengroup='CABANG'")
            code = "REQ/" & BranchCode & "/" & DateTime.Now.ToString("yy") & "/" & DateTime.Now.ToString("MM") & "/" & DateTime.Now.ToString("dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnpinjamno,1) AS INT)),0) FROM ql_trnpinjammst WHERE trnpinjamno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            txtNoTanda.Text = reqcode

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub ClearDetail()
        txtBarang.Text = "" : trnpinjamdtlqty.Text = "0.00"
        itemoid.Text = "" : unitno.Text = ""
        gvPinjamBarang.SelectedIndex = -1
    End Sub

    Private Sub clearRef()
        Session("gvListCust") = Nothing : gvPinjamBarang.DataSource = Nothing
        gvPinjamBarang.DataBind()
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String)
        Try
            vreport.Load(Server.MapPath(folderReport & "printnotaptp.rpt"))
            vreport.SetParameterValue("sWhere", id)
            vreport.SetParameterValue("cmpcode", CompnyCode)
            CProc.SetDBLogonForReport(vreport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vreport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            vreport.PrintOptions.PrinterName = "EPSON SERVICE"
            vreport.PrintToPrinter(1, False, 0, 0)
            vreport.Close() : vreport.Dispose()

        Catch ex As Exception
            showMessage("Print Failed :" & ex.Message, CompnyName & " - WARNING", 2)
            vreport.Close() : vreport.Dispose() : Exit Sub
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub gvPenerimaan_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPenerimaan.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvPenerimaan.DataSource = Session("TblMst")
            gvPenerimaan.DataBind()
        End If
    End Sub

    Protected Sub printmanualpenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles printmanualpenerimaan.Click
        ID = Session("oid") : Dim sWhere As String = ""
        Try
            sWhere = "AND p.trnpinjammstoid=" & Integer.Parse(Session("oid")) & " AND p.branch_code='" & ddlFromcabang.SelectedValue & "'"
            'untuk print
            vreport.Load(Server.MapPath(folderReport & "rptPrintPinjamBarang.rpt"))
            vreport.SetParameterValue("sWhere", sWhere)
            vreport.SetParameterValue("namaPencetak", Session("UserID"))
            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vreport)
            'vreport.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            vreport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
        Catch ex As Exception
            showMessage("Print Failed :" & ex.Message, CompnyName & " - WARNING", 2)
            vreport.Close() : vreport.Dispose() : Exit Sub
        End Try
    End Sub

    Private Sub binddataItem()
        Dim matcat As String = "" : Dim sqlPlus As String = ""
        Dim period As String = Month(GetServerTime())
        If period <= 9 Then
            period = Year(GetServerTime()) & "-0" & period
        Else
            period = Year(GetServerTime()) & "-" & period
        End If
        periodacctg.Text = period : Dim sItem As String = ""

       
        If txtBarang.Text <> "" Then
            sItem &= "and (m.itemdesc like '%" & Tchar(txtBarang.Text) & "%' or itemcode like '%" & Tchar(txtBarang.Text) & "%')"
        End If
        sSql = "select * From (Select m.satuan1 satuan2,m.itemoid matoid,m.itemcode matcode,m.itemdesc matlongdesc,ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir,Case m.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End JenisNya,m.stockflag From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where m.cmpcode = '" & CompnyCode & "' AND mtrlocoid=" & DDLLoc.SelectedValue & " AND periodacctg='" & periodacctg.Text & "' AND branch_code='" & ddlFromcabang.SelectedValue & "' AND con.mtrlocoid <> -10 " & sItem & " Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.itemoid,m.satuan1,m.itemdesc,m.itemcode ) tblitem Where saldoakhir <> 0.00/*and stockflag <> 'ASSET'*/"

        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sSql &= " And stockflag='" & JenisBarangDDL.SelectedValue & "'"
        End If
        sSql &= " ORDER BY matlongdesc"
        Dim tbDtl As DataTable = ckoneksi.ambiltabel(sSql, "ql_mst_item")
        Session("TblItemDtl1") = tbDtl
        FillGV(gvMaterial, sSql, "QL_mstitem")
        gvMaterial.Visible = True
    End Sub

#End Region

#Region "Function"
    Function TambahRow()
        If lblUpdNo.Text <> "" Then
            TambahRow = lblUpdNo.Text + 1
            'ElseIf lblUpdNo.Text = Session("Row") Then
            '    TambahRow = gvPinjamBarang.Rows.Count + 1
        Else
            TambahRow = gvPinjamBarang.Rows.Count + 1
        End If

        Return TambahRow
    End Function

    Private Function generateMerk(ByVal txtCodemerk As String) As String
        Dim codeVal As String = ""
        txtCodemerk = "MB"
        Dim merkPrefix As String = txtCodemerk.Substring(0, 2)
        sSql = "select gencode from QL_mstgen where gencode like '" & merkPrefix & "%' order by gencode desc "
        Dim x As Object = ckoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode merk seperti yg diminta tidak ada sama sekali, generate kode baru
            codeVal = UCase(merkPrefix) & "00001"
        Else
            If x = "" Then
                '    ' kode merk seperti yg diminta tidak ada sama sekali, generate kode baru
                codeVal = UCase(merkPrefix) & "00001"
            Else
                ' kode merk seperti yg diminta ada, tinggal generate angka
                Dim nomer As Integer = CInt(x.ToString.Substring(2, 5))
                nomer += 1
                codeVal = UCase(merkPrefix) & tNol(nomer)
            End If
        End If
        Return codeVal
    End Function

    Private Function generateCustCode(ByVal namacustomer As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = namacustomer.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select custcode from QL_mstcust where custcode like '" & custPrefix & "%' order by custcode desc "
        Dim x As Object = ckoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tNol(angka)
            End If
        End If
        Return retVal
    End Function

    Function tNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function

    Private Function setTableDetail() As DataTable
        Dim dt As DataTable = New DataTable("trnRequest")
        Dim ds As New DataSet
        dt.Columns.Add("sequence", Type.GetType("System.Int32"))
        dt.Columns.Add("matoid", Type.GetType("System.Int32"))
        dt.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dt.Columns.Add("Qty", Type.GetType("System.Int32"))
        dt.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dt.Columns.Add("saldoakhir", Type.GetType("System.Int32"))
        dt.Columns.Add("matcode", Type.GetType("System.String"))
        dt.Columns.Add("flagbarang", Type.GetType("System.String"))
        dt.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
        dt.Columns.Add("stockflag", Type.GetType("System.String"))
        ds.Tables.Add(dt)
        Session("tblitemdtl") = dt
        Return dt
    End Function

    Function GenerateDtlID()
        iVal = GenerateID("ql_trnrequestdtl", CompnyCode)
        Return iVal
    End Function

    Function tambahNol(ByVal iAngka As Integer)
        Dim sValue = "000"
        If iAngka >= 10 Then : sValue = "00" : End If
        If iAngka >= 100 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 1000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function

    Private Function GetSortDirection(ByVal column As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column
        Return sortDirection
    End Function

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A'"
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Function Get_USDRate() As Single
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim rate As Single

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "select top 1 isnull(rate2idrvalue,0) rate2idrvalue from QL_mstrate2 where rate2month = MONTH(getdate()) and rate2year = YEAR(GETDATE()) and currencyoid = 2"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            rate = rdr("rate2idrvalue")
        End While
        conn2.Close()
        Return rate
    End Function

    Function Get_Branch() As String
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As String = String.Empty

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT gencode FROM QL_mstgen where gengroup = 'cabang' and genoid = " & Session("branch_id") & ""
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            refcode = rdr("gencode")
        End While
        conn2.Close()
        Return refcode

    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.btnPost.Attributes.Add("onclick", "return confirm('Data sudah dipost tidak dapat diupdate');")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah anda yakin akan menghapus data ini ??');")

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        'showMessage(Mid("SO1/15/03/L/01/0062", 13, 2), "", 3)
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\trnPinjamBarang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")

        Me.Title = CompnyName & " - Peminjaman Barang"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            FilterBranch() : BindData("")
            DDLBranch() : InitAllDDL()
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                Session("new") = True
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                i_u.Text = "new" : dtlseq.Text = 1 : i_u2.Text = "new"
                Session("sequence") = dtlseq.Text
                GenerateReqCode()
                'txtNoTanda.Text = GenerateID("ql_trnpinjammst", CompnyCode)
                txtTglTerima.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                txtStatus.Text = "In Process"
                btnClear.Visible = False : btnPost.Visible = False
                btnDelete.Visible = False : lblUpd.Text = "Created"
                Session("gvPinjamBarang") = Nothing
                iuAdd.Text = "new"
            Else
                Session("new") = False
                i_u.Text = "Update" : i_u2.Text = "update"
                TabContainer1.ActiveTabIndex = 1
                UpdUser.Text = Session("UserID")
                iuAdd.Text = "new" : lblUpd.Text = "Last Update"
                'FillTextBox(Session("oid"))
                FillTextBox(Session("branch_code"), Session("oid"))
            End If
            FilterPeriod1.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If

        If txtStatus.Text = "In" Then
            btnSave.Visible = False : btnAddToList.Visible = False
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        'gvPenerimaan.PageIndex = 0
        Dim sWhere As String = ""
        If checkPeriod.Checked Then
            Try
                Dim date1 As Date = toDate(FilterPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If FilterPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    showMessage("Isi Tanggal !!", CompnyName & "- Warning", 2)
                End If
                'If date1 < CDate("01/01/1900") Then
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal Awal Tidak Valid !!", CompnyName & " - Warning", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal Akhir Tidak Valid !!", CompnyName & " - Warning", 2) : Exit Sub
                End If
                If date1 > date2 Then
                    showMessage("Tanggal Awal harus lebih kecil dari Tanggal Akhir !!", CompnyName & " - Warning", 2) : Exit Sub
                    FilterPeriod1.Text = ""
                    txtPeriod2.Text = ""
                    Exit Sub
                End If
                sWhere &= " And CONVERT(CHAR(10),trndatepinjam, 101)  between '" & toDate(FilterPeriod1.Text) & "' AND '" & toDate(txtPeriod2.Text) & "'"
            Catch ex As Exception
                showMessage("Format Tanggal tidak sesuai !!", CompnyName & " - Warning", 2)
            End Try
        End If
        If checkStatus.Checked Then
            If StatusDDL.SelectedValue <> "All" Then
                sWhere = "And status='" & StatusDDL.SelectedValue & "'"
            End If
        End If
        sWhere &= " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        BindData(sWhere)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click

        i_u2.Text = "new" : itemoid.Text = "" : rbJenisBarang.SelectedValue = "0"
        trnpinjamdtlqty.Text = "0.00" : txtBarang.Text = ""
        If Session("tblitemdtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("tblitemdtl")
            dtlseq.Text = objTable.Rows.Count + 1
        Else
            dtlseq.Text = 1
        End If
        gvPinjamBarang.SelectedIndex = -1
        gvPinjamBarang.Columns(5).Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("gvPinjamBarang") = Nothing
        Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnPinjamBarang.aspx?awal=true")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        FilterText.Text = "" : FilterDDL.SelectedIndex = 0
        'FilterGVList("", "", "", "", "")
        checkPeriod.Checked = False
        checkStatus.Checked = False
        BindData("")
        StatusDDL.SelectedIndex = 0
        gvPenerimaan.PageIndex = 0
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMs As String = ""
        If Session("new") = True Then
            Session("oid") = Nothing
        End If

        Dim reqOid As Integer
        If i_u.Text = "new" Then
            reqOid = GenerateID("ql_trnpinjammst", CompnyCode)
        Else
            reqOid = PinjamOid.Text
        End If
        Dim oid As Integer = GenerateID("ql_trnpinjamdtl", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        If txtNamaCust.Text = "" Then
            sMs &= "- Maaf, Isi nama peminjam..!!<br />"
        End If

        If Session("tblitemdtl") Is Nothing Then
            sMs &= "- Maaf, Buat detail item pinjaman !!<br />"
        Else
            Dim objTableCek As DataTable
            Dim objRowCek() As DataRow
            objTableCek = Session("tblitemdtl")
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                sMs &= "- Maaf, Buat detail item pinjaman !!<br />"
            End If
        End If

        Dim objTbl As DataTable = Session("tblitemdtl")
        Dim iSeq As Integer = objTbl.Rows.Count + 1
        Dim dv As DataView = objTbl.DefaultView
        For j As Integer = 0 To dv.Count - 1
            sSql = "select saldoakhir From (Select m.stockflag,m.satuan1 satuan2,m.itemoid matoid,ISNULL(SUM(qtyIn)-SUM(qtyOut),0.0000) saldoAkhir,m.itemcode matcode,m.itemdesc matlongdesc from QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dv(j)("matoid").ToString) & " AND mtrlocoid=" & Integer.Parse(dv(j)("mtrlocoid")) & " AND periodacctg='" & periodacctg.Text & "' AND con.branch_code='" & ddlFromcabang.SelectedValue & "' AND m.cmpcode = '" & CompnyCode & "' AND con.mtrlocoid <> -10 AND m.stockflag='" & dv(j)("stockflag").ToString & "' Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.satuan1,m.itemoid,m.itemcode,m.itemdesc) tblitem Where saldoakhir <> 0.00 ORDER BY matcode"

            xCmd.CommandText = sSql : Dim qty As Integer = xCmd.ExecuteScalar
            If dv(j).Item("flagbarang") = "1" Then
                If ToDouble(dv(j)("Qty")) > qty Then
                   sMs &= "- Maaf, Qty Pinjam tidak boleh lebih besar dari Qty Stock..!!<br >"
                End If
            End If
        Next

        If sMs <> "" Then
            showMessage(sMs, CompnyName & " - Warning", 2)
            objTrans.Rollback() : conn.Close() : txtStatus.Text = "In Process"
            Exit Sub
        End If

        Try
            Dim transdate As Date = GetServerTime()
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & CompnyCode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback() : conn.Close()
                sMs &= "- Maaf, Tanggal Ini Tidak Dalam Open Stock..!!<br />"
                txtStatus.Text = "In Process"
                Exit Sub
            End If

            'Cek saldoakhir
            'If rbJenisBarang.SelectedValue <> "0" Then
            If Not Session("tblitemdtl") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("tblitemdtl")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    Dim loc As Int16 = dtab.Rows(j).Item("mtrlocoid")
                    If loc = 0 Then
                    Else
                        sSql = "select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab.Rows(j).Item("matoid")) & " AND mtrlocoid=" & Integer.Parse(dtab.Rows(j).Item("mtrlocoid")) & " AND con.mtrlocoid <> -10 AND periodacctg='" & period & "' AND branch_code='" & ddlFromcabang.SelectedValue & "' AND m.stockflag='" & dtab.Rows(j).Item("stockflag").ToString & "' Group By mtrlocoid, periodacctg, branch_code, m.stockflag, m.itemoid) tblitem"
                    End If
                    xCmd.CommandText = sSql : saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then 
                        sMs &= "- Maaf, Peminjaman tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("matlongdesc") & "' karena saldo akhir satuan std hanya tersedia 0 ..!!<br />"
                    End If
                Next
            End If


            If sMs <> "" Then
                showMessage(sMs, CompnyName & " - Warning", 2)
                objTrans.Rollback() : conn.Close() : txtStatus.Text = "In Process"
                Exit Sub
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim crdmatoid As Int32 = xCmd.ExecuteScalar + 1

            Dim tempoid As Int32 = crdmatoid : Dim rbBarang As String = ""
            If rbJenisBarang.SelectedValue = "0" Then
                rbBarang = "0"
            Else
                rbBarang = "1"
            End If

            'insert
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO ql_trnpinjammst (cmpcode,trnpinjammstoid,branch_code,trnpinjamno,trndatepinjam,namapeminjam,status,note,updtime,upduser) VALUES" & _
" ('" & CompnyCode & "'," & reqOid & ",'" & ddlFromcabang.SelectedValue & "','" & txtNoTanda.Text & "',current_timestamp,'" & Tchar(txtNamaCust.Text) & "','" & txtStatus.Text & "','" & txtDetailPinjam.Text & "',current_timestamp,'" & Session("UserID") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid=(SELECT MAX(trnpinjammstoid) FROM ql_trnpinjammst) Where tablename='ql_trnpinjammst' And cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else 'update
                sSql = "update ql_trnpinjammst set status='" & txtStatus.Text & "', namapeminjam='" & txtNamaCust.Text & "', updtime=current_timestamp, note='" & txtDetailPinjam.Text & "' Where trnpinjammstoid='" & reqOid & "' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            If Not Session("tblitemdtl") Is Nothing Then
                Dim objTable As DataTable : trnpinjamdtloid.Text = oid
                objTable = Session("tblitemdtl")
                If objTable.Rows.Count <> 0 Then
                    sSql = "delete from ql_trnpinjamdtl Where trnpinjammstoid=" & reqOid & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    Dim hpp As Double = 0
                    Dim qty_to As Integer = objTable.Rows(C1).Item("Qty")
                    'Select Last HPP 
                    sSql = "Select HPP from QL_mstitem Where itemoid=" & objTable.Rows(C1).Item("matoid") & "" : xCmd.CommandText = sSql : hpp = xCmd.ExecuteScalar

                    sSql = "insert into ql_trnpinjamdtl (cmpcode,trnpinjamdtloid,trnpinjamseq,branch_code,trnpinjammstoid,flagbarang,itemoid,itemqty,unitoid,updtime,upduser,mtrlocoid,hpp) values" & _
                    " ('" & CompnyCode & "'," & (C1 + CInt(trnpinjamdtloid.Text)) & "," & objTable.Rows(C1).Item("sequence") & ",'" & ddlFromcabang.SelectedValue & "','" & reqOid & "'," & objTable.Rows(C1).Item("flagbarang") & "," & objTable.Rows(C1).Item("matoid") & "," & ToDouble(objTable.Rows(C1).Item("Qty")) & ",'" & objTable.Rows(C1).Item("unitoid") & "',current_timestamp,'" & Session("UserID") & "'," & objTable.Rows(C1).Item("mtrlocoid") & "," & ToDouble(hpp) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    oid += 1
                Next
                sSql = "update QL_mstoid set lastoid=(select max (trnpinjamdtloid) from ql_trnpinjamdtl) where tablename='ql_trnpinjamdtl' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            Dim COA_Hutang_Usaha As Integer = 0
            Dim varadjustplus As String = GetVarInterface("VAR_HPP", ddlFromcabang.SelectedValue)
            If varadjustplus Is Nothing Or varadjustplus = "" Then
                showMessage("Interface untuk Akun VAR_HPP tidak ditemukan!", 2, "")
                objTrans.Rollback() : conn.Close()
                Exit Sub
            Else
                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & varadjustplus & "'"
                xCmd.CommandText = sSql : COA_Hutang_Usaha = xCmd.ExecuteScalar
                If COA_Hutang_Usaha = 0 Or COA_Hutang_Usaha = Nothing Then
                    showMessage("Akun COA untuk VAR_HPP tidak ditemukan!", 2, "")
                    objTrans.Rollback() : conn.Close()
                    Exit Sub
                End If
            End If

            If txtStatus.Text = "POST" Then
                Dim objTable As DataTable
                Dim hpp, AmtDebet, glValue, glUSDValue As Double
                Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
                Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
                Dim iGlSeq As Int16 = 1
                Dim periodene As String = GetDateToPeriodAcctg(GetServerTime).Trim
                Dim UsdRate As Single = Get_USDRate()
                Dim branchcode As String = Get_Branch()
                If ToDouble(UsdRate) = 0 Then
                    showMessage("Rate Currency Belum Di isi !", 2, "")
                    Exit Sub
                End If
                objTable = Session("tblitemdtl")
                Dim dvfilter As DataView = objTable.DefaultView
                dvfilter.RowFilter = "flagbarang = 1"
                If dvfilter.Count > 0 Then
                    sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,type) VALUES" & _
                            " ('" & CompnyCode & "','" & ddlFromcabang.SelectedValue & "'," & iglmst & ",Convert(date, getdate()),'" & periodene & "','PINJAM (" & txtNoTanda.Text & ")','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'REQ')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    'Update crdmtr for item out
                    Dim qty_to As Integer = objTable.Rows(C1).Item("Qty")
                    sSql = "Select HPP from QL_mstitem Where itemoid=" & objTable.Rows(C1).Item("matoid") & ""
                    xCmd.CommandText = sSql : hpp = xCmd.ExecuteScalar
                    'igldtl += 1 : iGlSeq += 1

                    If objTable.Rows(C1).Item("flagbarang") = "1" Then
                        Dim COA_gudang As Integer = 0
                        Dim vargudang As String = "" : Dim iMatAccount As String = ""
                        sSql = "Select stockflag From ql_mstitem Where itemoid= " & objTable.Rows(C1)("matoid").ToString & ""
                        xCmd.CommandText = sSql : Dim sTockFlag As String = xCmd.ExecuteScalar

                        sSql = "Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & objTable.Rows(C1)("mtrlocoid").ToString & " AND genother4='" & sTockFlag & "'"
                        xCmd.CommandText = sSql : Dim xBRanch As String = xCmd.ExecuteScalar

                        If xBRanch = "" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_INVENTORY' !!", CompnyName & " - WARNING", "")
                        End If

                        Dim mVar As String = ""
                        If sTockFlag = "I" Then
                            mVar = "VAR_INVENTORY"
                        ElseIf sTockFlag = "ASSET" Then
                            mVar = "VAR_GUDANG_ASSET"
                        Else
                            mVar = "VAR_GUDANG"
                        End If

                        iMatAccount = GetVarInterface(mVar, xBRanch)
                        If iMatAccount = "?" Or iMatAccount = "0" Or iMatAccount = "" Then
                            showMessage("Maaf,interface " & mVar & " belum di setting silahkan hubungi admin untuk seting interface !!", CompnyName & " - WARNING", "")
                            Session("click_post") = "false"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If

                        If iMatAccount Is Nothing Or iMatAccount = "" Then
                            showMessage("Interface untuk Akun " & mVar & " tidak ditemukan!", 2, "")
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        Else
                            sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAccount & "'"
                            xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                            If COA_gudang = 0 Or COA_gudang = Nothing Then
                                showMessage("Maaf, Akun COA untuk " & mVar & " tidak ditemukan!", 2, "")
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            End If
                        End If

                        'gl Value
                        glValue = ToMaskEdit(ToDouble(hpp) * ToDouble(objTable.Rows(C1).Item("qty")), 2)
                        glUSDValue = ToDouble(glValue) / ToDouble(UsdRate)

                        sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid, acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate, upduser,updtime,branch_code) VALUES " & _
                        "('" & CompnyCode & "'," & igldtl & "," & iGlSeq & "," & iglmst & "," & COA_gudang & ",'C'," & ToDouble(glValue) & "," & ToDouble(glValue) & "," & ToDouble(glUSDValue) & ",'" & txtNoTanda.Text & "','PINJAM(" & objTable.Rows(C1).Item("matlongdesc") & ")','','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlFromcabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1
                        'Insert Conmtr
                        sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) " & _
                        "VALUES ('" & CompnyCode & "','" & ddlFromcabang.SelectedValue & "'," & conmtroid + 1 & ",'PINJAM','" & transdate & "','" & period & "','" & txtNoTanda.Text & "','', 'QL_trnpinjamdtl', " & objTable.Rows(C1).Item("matoid") & ",'QL_MSTITEM'," & objTable.Rows(C1).Item("unitoid") & "," & objTable.Rows(C1).Item("mtrlocoid") & ", 0," & ToDouble(objTable.Rows(C1).Item("Qty")) & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0,'" & ToDouble(hpp) * ToDouble(objTable.Rows(C1).Item("Qty")) & "','" & txtDetailPinjam.Text & "-" & txtNoTanda.Text & "','" & ToDouble(hpp) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                    End If
                    'sum hpp
                    AmtDebet += ToMaskEdit(ToDouble(hpp) * ToDouble(objTable.Rows(C1).Item("qty")), 2)
                Next

                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                " ('" & CompnyCode & "', " & igldtl & "," & iGlSeq & "," & iglmst & "," & COA_Hutang_Usaha & ",'D'," & ToDouble(AmtDebet) & "," & ToDouble(AmtDebet) & "," & 0 & ",'" & txtNoTanda.Text & "','PINJAM (" & txtNoTanda.Text & ")','','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlFromcabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & iglmst & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid = " & conmtroid & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid = " & crdmatoid - 1 & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid = " & igldtl & " Where tablename = 'QL_trngldtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            txtStatus.Text = "In Process" : objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & "- Warning", 2)
            Exit Sub
        End Try
        Session("tblitemdtl") = Nothing : Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnPinjamBarang.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        txtStatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        gvMaterial.Visible = False
        itemoid.Text = "" : txtBarang.Text = ""
        laststok.Text = "" : trnpinjamdtlqty.Text = ""
        matcode.Text = "" : mtrlocoid.Text = ""
    End Sub

    Protected Sub gvPenerimaan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPenerimaan.PageIndexChanging
        gvPenerimaan.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Try
            If txtNamaCust.Text.Trim = "" Then : showMessage("Isi nama peminjam !!", CompnyName & "- Warning", 2)
                Exit Sub
            End If
            If txtBarang.Text = "" Then
                showMessage("Pilih Barang!!", CompnyName & "- Warning", 2)
                Exit Sub
            End If
            If trnpinjamdtlqty.Text = "" Or trnpinjamdtlqty.Text = "0" Then
                showMessage("Quantity Barang Tidak Boleh 0!!", CompnyName & "- Warning", 2)
                Exit Sub
            End If

            If txtDetailPinjam.Text.Length > 150 Then
                showMessage("Maksimal panjang karakter 150 !!", CompnyName & "- Warning", 2)
                Exit Sub
            End If

            If Session("tblitemdtl") Is Nothing Then
                setTableDetail()
            End If

            Dim dt As DataTable = Session("tblitemdtl")
            If i_u2.Text = "new" Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "matoid=" & itemoid.Text & ""
                If dv.Count > 0 Then
                    dv.RowFilter = ""
                    showMessage("Data sudah di sebelumnya, Silahkan cek ulang!", CompnyName & " - WARNING", 2)
                    dt = Session("tblitemdtl") : gvPinjamBarang.DataSource = dt
                    gvPinjamBarang.DataBind() : gvPinjamBarang.Visible = True
                    Exit Sub
                End If
                dv.RowFilter = ""
            Else
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "matoid=" & itemoid.Text & " AND sequence <> " & dtlseq.Text & " "
                If dv.Count > 0 Then
                    dv.RowFilter = ""
                    showMessage("Data sudah di tambahkan, Silahkan cek ulang!", CompnyName & " - WARNING", 2)
                    dt = Session("tblitemdtl") : gvPinjamBarang.DataSource = dt
                    gvPinjamBarang.DataBind() : gvPinjamBarang.Visible = True
                    Exit Sub
                End If
                dv.RowFilter = ""
            End If

            Dim objRow As DataRow
            If i_u2.Text = "new" Then
                If ToDouble(trnpinjamdtlqty.Text) > ToDouble(laststok.Text) Then
                    showMessage("Qty Peminjaman tidak boleh lebih dari Qty Stok", CompnyName & "- Warning", 2)
                    Exit Sub
                End If

                If Session("tblitemdtl") Is Nothing Then
                    dt = setTableDetail()
                Else
                    objRow = dt.NewRow()
                    objRow("sequence") = dt.Rows.Count + 1
                End If
            Else
                objRow = dt.Rows(dtlseq.Text - 1)
                objRow.BeginEdit()
            End If

            btnClear.Visible = True
            objRow("matoid") = Integer.Parse(itemoid.Text)
            objRow("matlongdesc") = txtBarang.Text
            objRow("Qty") = ToDouble(trnpinjamdtlqty.Text)
            objRow("unitoid") = Integer.Parse(unitno.Text)
            objRow("saldoakhir") = ToDouble(laststok.Text)
            objRow("matcode") = matcode.Text
            objRow("flagbarang") = rbJenisBarang.SelectedValue
            objRow("stockflag") = JenisBarangDDL.SelectedValue

            If rbJenisBarang.SelectedValue = "0" Then
                objRow("mtrlocoid") = 0
            Else
                objRow("mtrlocoid") = DDLLoc.SelectedValue
            End If

            If i_u2.Text = "new" Then
                dt.Rows.Add(objRow)
            Else 'update
                objRow.EndEdit()
            End If

            dt.AcceptChanges()
            Session("tblitemdtl") = dt
            gvPinjamBarang.DataSource = dt : gvPinjamBarang.DataBind()
            Session("tblitemdtl") = dt : gvPinjamBarang.Visible = True
            dtlseq.Text = (gvMaterial.Rows.Count + 1).ToString
            trnpinjamdtlqty.Text = "0.00" : txtBarang.Text = "" : itemoid.Text = ""
            i_u2.Text = "new" : unitno.Text = "" : laststok.Text = ""
            ClearDetail()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False
        panelMsg.Visible = False
    End Sub

    Protected Sub gvPinjamBarang_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPinjamBarang.RowDeleting
        Try
            Dim iIndex As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("gvPinjamBarang")
            objTable.Rows.RemoveAt(iIndex)
            'resequence
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("sequence") = C1 + 1
                dr.EndEdit()
            Next

            Session("gvPinjamBarang") = objTable
            gvPinjamBarang.DataSource = objTable
            gvPinjamBarang.DataBind()

            Session("sequence") = objTable.Rows.Count + 1
            If gvPinjamBarang.Rows.Count = 0 Then
                lblUpdNo.Text = "0"
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        txtDetailPinjam.Text = ""
        ClearDetail()
    End Sub

    Protected Sub gvPinjamBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPinjamBarang.SelectedIndexChanged
        Session("index") = gvPinjamBarang.SelectedIndex
        If Session("tblitemdtl") Is Nothing = False Then
            dtlseq.Text = gvPinjamBarang.SelectedDataKey("sequence").ToString().Trim
            i_u2.Text = "update"
            lblUpdNo.Text = gvPinjamBarang.SelectedIndex.ToString
            Dim objTable As DataTable
            objTable = Session("tblitemdtl")
            Dim edRow() As DataRow = objTable.Select("sequence=" & dtlseq.Text)
            If edRow.Length > 0 Then
                trnpinjamdtlqty.Text = edRow(0).Item("Qty").ToString
                txtBarang.Text = edRow(0).Item("matlongdesc").ToString
                itemoid.Text = edRow(0).Item("matoid").ToString
                unitno.Text = edRow(0).Item("unitoid").ToString
                laststok.Text = edRow(0).Item("saldoakhir").ToString
                rbJenisBarang.SelectedValue = edRow(0).Item("flagbarang").ToString
                JenisBarangDDL.SelectedValue = edRow(0).Item("stockflag").ToString
                If rbJenisBarang.SelectedValue = "0" Then
                    Location.Visible = False : LblTitik.Visible = False
                    DDLLoc.SelectedIndex.ToString(-1) : DDLLoc.Visible = False
                Else
                    Location.Visible = True
                    LblTitik.Visible = True : DDLLoc.Visible = True
                End If
            Else
                showMessage("tidak dapat load detail !!", CompnyName & "- Warning", 2)
            End If
            i_u2.Text = "update"
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        Dim sColom() As String = {"MSTREQOID"}
        Dim sTable() As String = {"QL_TRNMECHCHECK"}

        If CheckDataExists(Session("oid"), sColom, sTable) = True Then
            showMessage("tidak dapat hapus data, karena digunakan di tabel lain !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        'hapus tabel detail
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try

            strSQL = "delete from ql_trnpinjammst where trnpinjammstoid='" & Session("oid") & "'"
            objCmd.CommandText = strSQL
            objCmd.ExecuteNonQuery()

            'hapus tabel master
            strSQL = "delete from ql_trnpinjamdtl where trnpinjammstoid='" & Session("oid") & "'"
            objCmd.CommandText = strSQL
            objCmd.ExecuteNonQuery()
            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            objCmd.Connection.Close()
            showMessage(ex.ToString) : Exit Sub
        End Try

        Response.Redirect("~\Transaction\trnPinjamBarang.aspx?awal=true")
    End Sub

    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim status As String = gvr.Cells(3).Text.ToString
        If status = "In Process" Then
            showMessage("Nota penerimaan dengan status In Process belum bisa dicetak !", CompnyName & " - WARNING", 2)
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        PrintReport(sender.ToolTip, lbloid.Text)
    End Sub

    Protected Sub sBarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sBarang.Click
        If txtNamaCust.Text = "" Then
            showMessage("Silahkan input Peminjam terlebih dahulu!!", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        binddataItem()
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvMaterial_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMaterial.PageIndex = e.NewPageIndex
        binddataItem()
    End Sub

    Protected Sub gvMaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemoid.Text = gvMaterial.SelectedDataKey.Item("matoid")
        txtBarang.Text = gvMaterial.SelectedDataKey.Item("matlongdesc")
        unitno.Text = gvMaterial.SelectedDataKey.Item("satuan2")
        laststok.Text = gvMaterial.SelectedDataKey.Item("saldoakhir")
        matcode.Text = gvMaterial.SelectedDataKey.Item("matcode")
        JenisBarangDDL.SelectedValue = gvMaterial.SelectedDataKey.Item("stockflag")
        gvMaterial.Visible = False
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("tblitemdtl") Is Nothing Then
            dtab = Session("tblitemdtl")
        Else
            showMessage("Missing detail list session !", CompnyName & "WARNING", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("sequence = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("sequence") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvPinjamBarang.DataSource = dtab
        gvPinjamBarang.DataBind()
        Session("tblitemdtl") = dtab

        ClearDetail()
    End Sub

    Protected Sub gvPenerimaan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'showMessageprint(gvPenerimaan.SelectedDataKey.Item(0), gvPenerimaan.SelectedDataKey.Item(1), "")
        Response.Redirect("trnPinjamBarang.aspx?branch_code=" & gvPenerimaan.SelectedDataKey("branch_code").ToString & "&oid=" & gvPenerimaan.SelectedDataKey("trnpinjammstoid") & "")
    End Sub

    Protected Sub btncloseprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub btpPrintAction_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'If ddlprinter.SelectedValue = "PDF" Then
        '    Dim report2 As String = "pdf"
        '    PrintReport(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        'ElseIf ddlprinter.SelectedValue = "EXCEL" Then
        '    Session("showReport") = True
        '    showPrintExcel(NoTemp.Text, CInt(idTemp.Text))
        'Else
        '    PrintReportPTP(idTemp.Text, NoTemp.Text, OtherTemp.Text)
        'End If

        'PanelValidasi.Visible = False
        'ButtonExtendervalidasi.Visible = False
        'ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub rbJenisBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbJenisBarang.SelectedValue = "0" Then
            Location.Visible = False : LblTitik.Visible = False
            DDLLoc.SelectedIndex.ToString(-1) : DDLLoc.Visible = False
        Else
            Location.Visible = True
            LblTitik.Visible = True : DDLLoc.Visible = True
        End If
        txtBarang.Text = ""
        'If Session("oid") <> Nothing And Session("oid") <> "" Then
        'Else
        '    Session("tblitemdtl") = Nothing
        '    Session("tblitemdtl").DataSource = Nothing
        '    Session("tblitemdtl").DataBind()
        'End If      
    End Sub

#End Region

    Protected Sub ddlFromcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromcabang.SelectedIndexChanged
        GenerateReqCode() : InitAllDDL()
    End Sub
End Class
