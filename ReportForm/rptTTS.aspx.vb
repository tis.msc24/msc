Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSalesRetur
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = ""

            vReport.Load(Server.MapPath("~\Report\rptTTS.rpt"))

            sWhere = " where a.ttsdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
            If ttsno.Text.Trim <> "" Then
                sWhere &= " and a.ttsno like '%" & Tchar(ttsno.Text) & "%'"
            End If
            If orderno.Text.Trim <> "" Then
                sWhere &= " and a.trnjualno like '%" & Tchar(orderno.Text) & "%'"
            End If
            If dono.Text.Trim <> "" Then
                sWhere &= " and a.trnsjjualno like '%" & Tchar(dono.Text) & "%'"
            End If
            If barang.Text.Trim <> "" Then
                sWhere &= " and d.itemdesc like '%" & Tchar(barang.Text) & "%'"
            End If
            If merk.Text.Trim <> "" Then
                sWhere &= " and d.merk like '%" & Tchar(merk.Text) & "%'"
            End If
            If custname.Text.Trim <> "" Then
                sWhere &= " and c.custname like '%" & Tchar(custname.Text) & "%'"
            End If

            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & "-" & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TTS_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TTS_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
            Label2.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("rptTTS.aspx")
        End If

        Page.Title = CompnyCode & " - Laporan Tanda Terima Sementara"
        Label2.Text = ""

        If IsPostBack Then
            If Session("showprint") = "True" Then
                showPrint("")
            End If
        Else
            range1.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            range2.Text = Format(Now, "dd/MM/yyyy")
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        PanelErrMsg.Visible = False
        btnExtender.Visible = False
        MPEErrMsg.Hide()
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "True"
        showPrint("")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptTTS.aspx?awal=true")
    End Sub

    Protected Sub ttssearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ttssearch.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If

        sSql = "select a.TTSNo,a.TTSDate,a.trnjualno,a.trnsjjualno,b.custname,a.custoid from QL_trnttsmst a inner join QL_mstcust b on a.custoid=b.custoid where a.cmpcode='" & CompnyCode & "' and a.TTSDate between '" & date1 & "' and '" & date2 & "' and a.TTSNo like '%" & Tchar(ttsno.Text) & "%'"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVTts")
        GVTts.DataSource = dtab
        GVTts.DataBind()
        GVTts.Visible = True
        Session("GVTts") = dtab

        GVOrder.Visible = False
        GVDo.Visible = False
        GVBarang.Visible = False
        GVCustomer.Visible = False
    End Sub

    Protected Sub ttserase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ttserase.Click
        ttsno.Text = ""

        GVTts.DataSource = Nothing
        GVTts.DataBind()
        Session("GVTts") = Nothing
    End Sub

    Protected Sub GVTts_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTts.PageIndexChanging
        If Not Session("GVTts") Is Nothing Then
            GVTts.DataSource = Session("GVTts")
            GVTts.PageIndex = e.NewPageIndex
            GVTts.DataBind()
        End If
    End Sub

    Protected Sub GVTts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTts.SelectedIndexChanged
        ttsno.Text = GVTts.SelectedDataKey("TTSNo")
        orderno.Text = GVTts.SelectedDataKey("trnjualno")
        dono.Text = GVTts.SelectedDataKey("trnsjjualno")
        custname.Text = GVTts.SelectedDataKey("custname")

        GVTts.DataSource = Nothing
        GVTts.DataBind()
        GVTts.Visible = False
        Session("GVTts") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showprint") = "False"
    End Sub

    Protected Sub ordersearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ordersearch.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If

        sSql = "select orderno,trnorderdate,trncustname custname from QL_trnordermst where cmpcode='" & CompnyCode & "' and trnorderdate between '" & date1 & "' and '" & date2 & "' and orderno like '%" & Tchar(orderno.Text) & "%'"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVOrder")
        GVOrder.DataSource = dtab
        GVOrder.DataBind()
        GVOrder.Visible = True
        Session("GVOrder") = dtab

        GVDo.Visible = False
        GVTts.Visible = False
        GVBarang.Visible = False
        GVCustomer.Visible = False
    End Sub

    Protected Sub ordererase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ordererase.Click
        orderno.Text = ""

        GVOrder.DataSource = Nothing
        GVOrder.DataBind()
        Session("GVOrder") = Nothing
    End Sub

    Protected Sub GVOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVOrder.PageIndexChanging
        If Not Session("GVOrder") Is Nothing Then
            GVOrder.DataSource = Session("GVOrder")
            GVOrder.PageIndex = e.NewPageIndex
            GVOrder.DataBind()
        End If
    End Sub

    Protected Sub GVOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVOrder.SelectedIndexChanged
        orderno.Text = GVOrder.SelectedDataKey("orderno")

        GVOrder.DataSource = Nothing
        GVOrder.DataBind()
        GVOrder.Visible = False
        Session("GVOrder") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showprint") = "False"
    End Sub

    Protected Sub dosearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles dosearch.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If

        sSql = "select trnsjjualno,trnsjjualdate,trncustname custname from QL_trnsjjualmst a inner join QL_trnordermst b on a.orderno=b.orderno where a.cmpcode='" & CompnyCode & "' and trnsjjualdate between '" & date1 & "' and '" & date2 & "' and trnsjjualno like '%" & Tchar(dono.Text) & "%'"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVDo")
        GVDo.DataSource = dtab
        GVDo.DataBind()
        GVDo.Visible = True
        Session("GVDo") = dtab

        GVTts.Visible = False
        GVOrder.Visible = False
        GVBarang.Visible = False
        GVCustomer.Visible = False
    End Sub

    Protected Sub doerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles doerase.Click
        dono.Text = ""

        GVDo.DataSource = Nothing
        GVDo.DataBind()
        Session("GVDo") = Nothing
    End Sub

    Protected Sub GVDo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVDo.PageIndexChanging
        If Not Session("GVDo") Is Nothing Then
            GVDo.DataSource = Session("GVDo")
            GVDo.PageIndex = e.NewPageIndex
            GVDo.DataBind()
        End If
    End Sub

    Protected Sub GVDo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDo.SelectedIndexChanged
        dono.Text = GVDo.SelectedDataKey("trnsjjualno")

        GVDo.DataSource = Nothing
        GVDo.DataBind()
        GVDo.Visible = False
        Session("GVDo") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showprint") = "False"
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        sSql = "select itemoid,itemcode,itemdesc,Merk from QL_mstitem where itemflag='Aktif' and cmpcode='" & CompnyCode & "' and (itemcode like '%" & Tchar(barang.Text) & "%' or itemdesc like '%" & Tchar(barang.Text) & "%' or Merk like '%" & Tchar(barang.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVBarang")
        GVBarang.DataSource = dtab
        GVBarang.DataBind()
        GVBarang.Visible = True
        Session("GVBarang") = dtab

        GVOrder.Visible = False
        GVDo.Visible = False
        GVTts.Visible = False
        GVCustomer.Visible = False
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = ""
        barangoid.Text = ""
        merk.Text = ""

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
    End Sub

    Protected Sub GVBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVBarang.PageIndexChanging
        If Not Session("GVBarang") Is Nothing Then
            GVBarang.DataSource = Session("GVBarang")
            GVBarang.PageIndex = e.NewPageIndex
            GVBarang.DataBind()
        End If
    End Sub

    Protected Sub GVBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVBarang.SelectedIndexChanged
        barang.Text = GVBarang.SelectedDataKey("itemdesc")
        barangoid.Text = GVBarang.SelectedDataKey("itemoid")
        merk.Text = GVBarang.SelectedDataKey("Merk")

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showprint") = "False"
    End Sub

    Protected Sub custsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles custsearch.Click
        sSql = "select custoid,custcode,custname from QL_mstcust where custflag='Active' and cmpcode='" & CompnyCode & "' and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVCustomer")
        GVCustomer.DataSource = dtab
        GVCustomer.DataBind()
        GVCustomer.Visible = True
        Session("GVCustomer") = dtab

        GVOrder.Visible = False
        GVDo.Visible = False
        GVTts.Visible = False
        GVBarang.Visible = False
    End Sub

    Protected Sub custerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles custerase.Click
        custname.Text = ""
        custoid.Text = ""

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        Session("GVCustomer") = Nothing
    End Sub

    Protected Sub GVCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVCustomer.PageIndexChanging
        If Not Session("GVCustomer") Is Nothing Then
            GVCustomer.DataSource = Session("GVCustomer")
            GVCustomer.PageIndex = e.NewPageIndex
            GVCustomer.DataBind()
        End If
    End Sub

    Protected Sub GVCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCustomer.SelectedIndexChanged
        custname.Text = GVCustomer.SelectedDataKey("custname")
        custoid.Text = GVCustomer.SelectedDataKey("custoid")

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        GVCustomer.Visible = False
        Session("GVCustomer") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showprint") = "False"
    End Sub
End Class
