<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptGeneralLedger.aspx.vb" Inherits="reportForm_rptGeneralLedger" Title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" style="width: 975px" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Text=".: General Ledger Report" Font-Size="X-Large" ForeColor="Navy"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td align="center" colspan="3">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<DIV align=center><TABLE style="WIDTH: 533px" width="100%" align=center><TBODY><TR><TD vAlign=top align=left colSpan=1>Branch</TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top align=center colSpan=1><asp:Label id="Label5" runat="server" Text=":" __designer:wfdid="w17"></asp:Label></TD><TD vAlign=top align=left width=843 colSpan=2><asp:DropDownList id="ddlBranch" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD vAlign=top align=left colSpan=1><asp:Label id="Label20" runat="server" Width="70px" Text="Report Type" __designer:wfdid="w14"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top align=center colSpan=1><asp:Label id="Label11" runat="server" Text=":" __designer:wfdid="w15"></asp:Label></TD><TD vAlign=top align=left width=843 colSpan=2><asp:DropDownList id="DDLType" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w16" AutoPostBack="True" OnSelectedIndexChanged="DDLType_SelectedIndexChanged"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD vAlign=top align=left colSpan=1><asp:Label id="Label4" runat="server" Text="Periode" __designer:wfdid="w19"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top align=center colSpan=1><asp:Label id="Label21" runat="server" Text=":" __designer:wfdid="w20"></asp:Label></TD><TD style="WIDTH: 602px; TEXT-ALIGN: left" vAlign=top align=center width=843 colSpan=2><asp:TextBox id="gldate1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w21" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ibcal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w22"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w23"></asp:Label> <asp:TextBox id="Gldate2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w24" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="ibcal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w26"></asp:Label></TD></TR><TR><TD id="TD3" vAlign=top align=left colSpan=1 runat="server" Visible="false"><asp:Label id="Label14" runat="server" Text="Account" __designer:wfdid="w27"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" id="TD4" vAlign=top colSpan=1 runat="server" Visible="false"><asp:Label id="Label25" runat="server" Text=":" __designer:wfdid="w28"></asp:Label></TD><TD id="TD2" vAlign=top align=left colSpan=2 runat="server" Visible="false"><asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w29" AutoPostBack="True" RepeatDirection="Horizontal"><asp:ListItem Selected="True">ALL</asp:ListItem>
<asp:ListItem>SELECT</asp:ListItem>
</asp:RadioButtonList><asp:TextBox id="acctgdesc" runat="server" Width="200px" CssClass="inpText " __designer:wfdid="w30" OnTextChanged="acctgdesc_TextChanged" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w31" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbClearAcctg" onclick="imbClearAcctg_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w32" Visible="False"></asp:ImageButton>&nbsp;&nbsp; </TD></TR><TR><TD id="TD6" vAlign=top align=left colSpan=1 runat="server" Visible="false"></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" id="TD1" vAlign=top colSpan=1 runat="server" Visible="false"></TD><TD id="TD5" vAlign=top align=left colSpan=2 runat="server" Visible="false"><asp:GridView id="gvAccounting" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w54" OnSelectedIndexChanged="gvAccounting_SelectedIndexChanged" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgcode,acctgoid,acctgdesc" UseAccessibleHeader="False" EnableModelValidation="True" PageSize="5">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cblist" runat="server" __designer:wfdid="w21" Checked='<%# Eval("selected") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="AcctgId" Visible="False"><ItemTemplate>
                                                <asp:Label ID="acctgoid" runat="server" Text='<%# Eval("acctgoid") %>'></asp:Label>
                                            
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="acctgcode" HeaderText="Acctg Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Acctg Desc">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>

                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD vAlign=top align=left colSpan=1><asp:Label id="Label2" runat="server" Text="Account" __designer:wfdid="w3"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top colSpan=1><asp:Label id="Label6" runat="server" Text=":" __designer:wfdid="w4"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="AcctgName" runat="server" Width="255px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w5" Enabled="False" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w7"></asp:ImageButton> <asp:Label id="acctgoid" runat="server" __designer:wfdid="w33" Visible="False"></asp:Label></TD></TR><TR><TD vAlign=top align=left colSpan=1></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top colSpan=1></TD><TD vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=bottom align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 100px" vAlign=top align=center colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w39"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w40"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w41" TargetControlID="gldate1" Format="dd/MM/yyyy" PopupButtonID="ibcal1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w42" TargetControlID="Gldate2" Format="dd/MM/yyyy" PopupButtonID="ibcal2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w43" TargetControlID="gldate1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w44" TargetControlID="Gldate2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></DIV><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w45" DisplayGroupTree="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="CrystalReportViewer1"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w47"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w48"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w49"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w50"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w52" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w53"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Account</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="acctgcode">Account Code</asp:ListItem>
<asp:ListItem Value="acctgdesc">Account Desc</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" OnClick="btnFindListMat_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" UseAccessibleHeader="False" PageSize="100">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("acctgoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="acctgcode" HeaderText="Acctg Code">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Acctg Desc">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>
