'prgrmr : zipi ; Last Update By zipi On 29.07.2013
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

'=== IMPORTANT NOTICE ===

'1. Invoice TUNAI
'   a. Biaya Exclude : Menambah Invoice, Pengeluaran Lewat "Form Cash/Bank Expense"
'      - INSERT Invoice : Auto Insert Into Cash/Bank Module (Full Invoice)
'      - POSTING GL : Kas/Bank terhadap Penjualan 
'   b. Biaya Include : Mengurangi Invoice
'      - INSERT Invoice : Auto Insert Into Cash/Bank Module (Full Invoice)
'      - INSERT Pembayaran Biaya : Auto Insert Into Cash/Bank Module (Pengeluaran untuk pembayaran biaya)
'      - POSTING GL : Kas/Bank terhadap Penjualan 
'2. Invoice KREDIT
'   a. INSERT Piutang : Auto Insert Into A/R Module (Full Invoice)
'   b. POSTING GL : Piutang Terhadap Penjualan
'   c. Biaya Exclude : Menambah Invoice, Pengeluaran Lewat "Form Cash/Bank Expense"
'   d. Biaya Include : Tidak mempengaruhi Invoice, baru berpengaruh saat Payment A/R
'========================

Partial Class Transaction_trnnotajual
    Inherits System.Web.UI.Page

#Region "Variable"
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    Dim report As New ReportDocument
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim dv As DataView
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
#End Region

#Region "Function"
    Public Function getOID() As String
        Return Eval("trnjualmstoid")
    End Function

    Public Function getCompany() As String
        Return CompnyCode
    End Function

    Public Function getStr() As String
        Return Server.MapPath("~/reportform/PrintReport.aspx") & "?type=SI&cmpcode=" & CompnyCode & "&oid=" & Request.QueryString("oid")
    End Function

    Private Sub SumPriceInInvoiceDetail(ByVal oTblDtl2 As DataTable)
        Dim dAmountDetail As Double = 0
        'Dim dDiscDetail As Double = 0
        Try
            For C1 As Int16 = 0 To oTblDtl2.Rows.Count - 1
                dAmountDetail += ToDouble(oTblDtl2.Rows(C1).Item("amtjualnetto"))
                'dDiscDetail += oTblDtl2.Rows(C1).Item("trnjualdtldisc")
            Next
            'trna.Text = ToMaskEdit(dAmountDetail, 3)
            'trnamtjualnetto.Text = ToMaskEdit(dAmountDetail - dDiscDetail, 3)
            trnamtjual.Text = ToMaskEdit(dAmountDetail, 3)
            'amtdiscdtl.Text = ToMaskEdit(dDiscDetail, 3)
        Catch ex As Exception
            trnamtjual.Text = ToMaskEdit(dAmountDetail, 3)
        End Try
    End Sub

    Private Sub SumBiayaDetail(ByVal oTblDtl2 As DataTable)
        Dim dAmountDetail As Double = 0
        Try
            For C1 As Int16 = 0 To oTblDtl2.Rows.Count - 1
                dAmountDetail += oTblDtl2.Rows(C1).Item("itembiayaamt")
            Next
            trnjualamtcost.Text = ToMaskEdit(dAmountDetail, 3)
        Catch ex As Exception
            trnjualamtcost.Text = ToMaskEdit(dAmountDetail, 3)
        End Try
    End Sub

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnjualmstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnjualdtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("sjrefoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemgroupcode", Type.GetType("System.String"))
        dt.Columns.Add("salesdeliveryqty", Type.GetType("System.Decimal"))
        dt.Columns.Add("trnjualdtlunitoid", Type.GetType("System.Int32"))
        dt.Columns.Add("orderprice", Type.GetType("System.Decimal"))
        dt.Columns.Add("trnjualdtlnote", Type.GetType("System.String"))
        dt.Columns.Add("trnjualstatus", Type.GetType("System.String"))
        dt.Columns.Add("amtjualnetto", Type.GetType("System.Decimal"))
        dt.Columns.Add("upduser", Type.GetType("System.String"))
        dt.Columns.Add("updtime", Type.GetType("System.DateTime"))
        dt.Columns.Add("salesdeliveryno", Type.GetType("System.String"))
        dt.Columns.Add("itemlongdesc", Type.GetType("System.String"))
        dt.Columns.Add("gendesc", Type.GetType("System.String"))
        dt.Columns.Add("salesdeliveryoid", Type.GetType("System.Int32"))
        dt.Columns.Add("salesdeliverydtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("amtjualdisc", Type.GetType("System.Decimal"))
        dt.Columns.Add("unitseq", Type.GetType("System.Int32"))
        dt.Columns.Add("trnorderdtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
        dt.Columns.Add("width", Type.GetType("System.Decimal"))
        dt.Columns.Add("lenght", Type.GetType("System.Decimal"))
        dt.Columns.Add("weight", Type.GetType("System.Decimal"))
        'dt.Columns.Add("trnjualpackingqty", Type.GetType("System.Decimal"))
        ',sod.unitseq,sjd.trnorderdtloid,sjd.mtrlocoid
        Return dt
    End Function

    Private Function setTabelDetailBiaya() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("jualbiayaseq", Type.GetType("System.Int32"))
        dt.Columns.Add("itembiayaoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itembiayanote", Type.GetType("System.String"))
        dt.Columns.Add("itembiayaamt", Type.GetType("System.Decimal"))
        dt.Columns.Add("upduser", Type.GetType("System.String"))
        dt.Columns.Add("updtime", Type.GetType("System.DateTime"))
        dt.Columns.Add("acctgdesc", Type.GetType("System.String"))
        Return dt
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Function InitTaxPct(ByVal taxoid As String) As Double
        Return cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND genoid=" & taxoid)
    End Function
#End Region

#Region "Procedure"
    Private Function InitAcctgBasedOnMasterData(ByVal sMasterTable As String, ByVal sMasterColumn As String, ByVal sAcctgColumn As String, _
    ByVal iMasterOid As Integer) As Integer
        sSql = "SELECT ISNULL(" & sAcctgColumn & ",0) FROM " & sMasterTable & " WHERE cmpcode='" & CompnyCode & "' AND " & sMasterColumn & "=" & iMasterOid
        Return cKoneksi.ambilscalar(sSql)
    End Function

    Public Sub initDDLPosting(ByVal sTypePayment As String, ByVal sInvoiceType As String)
        Dim varCash As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH'")
        Dim varBank As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK'")

        If sInvoiceType = "CSH00" Or sInvoiceType.ToUpper = "CASH" Then
            If Trim(sTypePayment) = "CASH" Then
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%' AND acctgoid not in " & _
                    "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
                FillDDL(cashbankacctg, sSql)
                Label22.Visible = False
                referensino.Visible = False
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Cash Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                    lkbPosting.Visible = False
                Else
                    lkbPosting.Visible = True
                End If
                lblcashbankacctg.Text = "Cash Account"
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varBank & "%' AND acctgoid not in " & _
                    "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
                FillDDL(cashbankacctg, sSql)
                Label22.Visible = True
                referensino.Visible = True
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Bank Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                    lkbPosting.Visible = False
                Else
                    lkbPosting.Visible = True
                End If
                lblcashbankacctg.Text = "Bank Account"
            End If
            lblpaymentype.Visible = True : paymentype.Visible = True
            lblcashbankacctg.Visible = True : cashbankacctg.Visible = True
            lblapaccount.Visible = False : apaccount.Visible = False
        Else

            sSql = "SELECT distinct  acctgoid,CAST(acctgcode AS VARCHAR) + ' - ' + acctgdesc FROM QL_mstacctg W inner join QL_mstinterface i on W.acctgcode like i.interfacevalue + '%'   and i.interfacevar like 'VAR_AR%' AND  acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=W.cmpcode) AND W.cmpcode='" & CompnyCode & "'    ORDER BY 2  "

            FillDDL(apaccount, sSql)
            Label22.Visible = False
            referensino.Visible = False
            If apaccount.Items.Count < 1 Then
                showMessage("Please create/fill AP Account in Chart of Accounts!!", _
                    CompnyName & " - WARNING", 2)
                lkbPosting.Visible = False
            Else
                lkbPosting.Visible = True
            End If
            lblpaymentype.Visible = False : paymentype.Visible = False
            lblcashbankacctg.Visible = False : cashbankacctg.Visible = False
            lblapaccount.Visible = True : apaccount.Visible = True
        End If
        ' Set Cost Cash/Bank Account Visibility Based on Cost Amount
        If ToDouble(trnjualamtcost.Text) > 0 Then
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varCash & "%' OR acctgcode LIKE '" & varBank & "%') AND acctgoid not in " & _
                "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
            FillDDL(CashForCost, sSql)
            ' Set Cost Cash/Bank Account Visibility Based on Cost Type
            If trnjualcosttype.SelectedValue.ToUpper = "EXCLUDE" Then
                lblCostCashBank.Visible = False : CashForCost.Visible = False
                Exit Sub
            Else
                lblCostCashBank.Visible = True : CashForCost.Visible = True
            End If
            If CashForCost.Items.Count < 1 Then
                showMessage("Please create/fill Cash/Bank Account in Chart of Accounts!!", _
                    CompnyName & " - WARNING", 2)
                lkbPosting.Visible = False
            Else
                lkbPosting.Visible = True
            End If
        Else
            lblCostCashBank.Visible = False : CashForCost.Visible = False
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub ClearDetail()
        Sjno.Text = "" : sjoid.Text = ""
        itemname.Text = "" : itemoid.Text = ""
        trnjualdtlqty.Text = "0.00" : trnjualdtlprice.Text = "0.00"
        gendesc.Text = "" : trnjualdtluom.Text = ""
        totalqty.Text = "0" : trnjualpackingqty.Text = "0"
        amtjualnetto.Text = "0.00" : trnjualdtlnote.Text = ""
        TblDtl.SelectedIndex = -1 : I_u2.Text = "New Detail"
        TblDtl.Columns(10).Visible = True
    End Sub

    Private Sub ClearDetailBiaya()
        itembiayaoid.SelectedIndex = 0
        itembiayaamt.Text = "0.00" : itembiayanote.Text = ""
        gvBiaya.SelectedIndex = -1 : i_u3.Text = "New Detail"
    End Sub

    Private Sub generateNo()
        Try
            trnjualno.Text = GenerateID("QL_trnjualmst", CompnyCode) 'GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        Catch ex As Exception
            'trnjualno.Text = "SIJ2/" & Format(CDate(toDate(trnjualdate.Text)), "yy") & "/" & Format(CDate(toDate(trnjualdate.Text)), "MM") & "/J/0001"
        End Try

    End Sub
    Private Sub generateNoPosting()
        Try


            Dim type As String = ""
            'If referenceno.Text.Substring(1, 1) = "C" Then
            type = "SIJ"
            '    Label19.Visible = False : Label20.Visible = False : Label21.Visible = False
            '    txtwidth.Visible = False : txtlenght.Visible = False : Txtweight.Visible = False
            'Else
            '    type = "IT"
            '    Label19.Visible = True : Label20.Visible = True : Label21.Visible = True
            '    txtwidth.Visible = True : txtlenght.Visible = True : Txtweight.Visible = True
            'End If
            If referenceno.Text.Substring(3, 1) = "1" Then
                taxtype.SelectedIndex = 0
            Else
                taxtype.SelectedIndex = 1
            End If
            Dim sNo As String = type & referenceno.Text.Substring(3, 1) & "/" & Format(CDate(toDate(trnjualdate.Text)), "yy") & "/" & Format(CDate(toDate(trnjualdate.Text)), "MM") & "/" & referenceno.Text.Substring(11, 1) & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno,4) AS INTEGER))+1,1) AS IDNEW FROM QL_trnjualmst " & _
                "WHERE cmpcode='" & CompnyCode & "' AND trnjualno LIKE '" & sNo & "%'"
            trnjualno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        Catch ex As Exception
            trnjualno.Text = "SIJ2/" & Format(CDate(toDate(trnjualdate.Text)), "yy") & "/" & Format(CDate(toDate(trnjualdate.Text)), "MM") & "/J/0001"
        End Try

        'oid.Text = GenerateID("QL_trnjualmst", CompnyCode)
        'trnjualno.Text = GenNumberString("SI", "", oid.Text, DefaultFormatCounter)
    End Sub

    Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & CompnyCode & "' and curroid=" & iOid & " order by currdate desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(xCmd.ExecuteScalar, 3)
        conn.Close()
    End Sub

    Sub ReAmount()
        If currencyrate.Text.Trim = "" Then currencyrate.Text = "1.00"
        If trndiscamt.Text.Trim = "" Then trndiscamt.Text = "0.00"
        If trntaxpct.Text.Trim = "" Then trntaxpct.Text = "0.00"
        If trnamtjual.Text.Trim = "" Then trnamtjual.Text = "0.00"
        If amtdischdr.Text.Trim = "" Then amtdischdr.Text = "0.00"
        If amtdiscdtl.Text.Trim = "" Then amtdiscdtl.Text = "0.00"

        'If trndisctype.SelectedValue = "AMT" Then
        '    If ToDouble(orderamt.Text) <> 0 Then
        '        Dim pengali As Double = ToDouble(trnamtjual.Text) / ToDouble(orderamt.Text)
        '        If pengali < 0 Then
        '            pengali = 1
        '        End If
        '        trndiscamt.Text = ToMaskEdit(ToDouble(disc.Text) * pengali, 3)
        '        amtdischdr.Text = ToMaskEdit(ToDouble(disc.Text) * pengali, 3)
        '    Else
        '        trndiscamt.Text = ToMaskEdit(0, 3)
        '        amtdischdr.Text = ToMaskEdit(0, 3)
        '    End If
        'Else
        '    trndiscamt.Text = ToMaskEdit(ToDouble(disc.Text), 3)
        '    amtdischdr.Text = ToMaskEdit((ToDouble(trnamtjual.Text) * ToDouble(disc.Text)) / 100, 3)
        'End If
        'If taxtype.SelectedIndex = 2 Then ' INCLUDE TAX
        '    trnamttax.Text = ToMaskEdit(Math.Ceiling((ToDouble(trnamtjual.Text) - ToDouble(amtdischdr.Text) * ToDouble(trntaxpct.Text)) / (100 + ToDouble(trntaxpct.Text))), 3)
        'ElseIf taxtype.SelectedIndex = 1 Then
        '    trnamttax.Text = ToMaskEdit((ToDouble(trntaxpct.Text) / 100) * (ToDouble(trnamtjual.Text) - ToDouble(amtdischdr.Text)), 3)
        'Else
        '    trnamttax.Text = ToMaskEdit(0, 3)
        'End If
        'If trnamttax.Text.Trim = "" Then trnamttax.Text = "0.00"

        'Dim tempBiaya As Double = 0
        'If trnjualcosttype.SelectedValue = "Exclude" Then
        '    tempBiaya = ToDouble(trnjualamtcost.Text)
        'End If

        'If taxtype.SelectedIndex = 2 Then ' INCLUDE TAX
        '    totalinvoice.Text = ToMaskEdit(ToDouble(trnamtjual.Text), 3)
        'ElseIf taxtype.SelectedIndex = 1 Then
        '    totalinvoice.Text = ToMaskEdit((ToDouble(trnamtjual.Text) - ToDouble(amtdischdr.Text)) + ToDouble(trnamttax.Text) + tempBiaya, 3)
        'Else
        '    totalinvoice.Text = ToMaskEdit((ToDouble(trnamtjual.Text) - ToDouble(amtdischdr.Text)) + tempBiaya, 3)
        'End If
        'If totalinvoice.Text.Trim = "" Then totalinvoice.Text = "0.00"

        'totalinvoicerp.Text = ToMaskEdit(ToDouble(totalinvoice.Text) * ToDouble(currencyrate.Text), 3)
        'If totalinvoicerp.Text.Trim = "" Then totalinvoicerp.Text = "0.00"

        If trndisctype.SelectedValue = "AMT" Then 'amount
            amtdischdr.Text = ToMaskEdit(CDbl(ToDouble(trndiscamt.Text)), 3)
        Else
            If ToDouble(trndiscamt.Text) > 100 Then
                showMessage("Discount Persent can't be more than 100 !!!", CompnyName & " - WARNING", 2)
                trndiscamt.Text = "0.00"
            End If
            amtdischdr.Text = ToMaskEdit(CDbl(((ToDouble(trndiscamt.Text) / 100) * (ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text)))), 3)
        End If
        If amtdischdr.Text.Trim = "" Then
            amtdischdr.Text = "0.00"
        End If

        Dim tempnilai As Double
        tempnilai = ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text) - ToDouble(amtdischdr.Text)

        trnamttax.Text = ToMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai))), 3)
        If trnamttax.Text.Trim = "" Then
            trnamttax.Text = "0.00"
        End If

        totalinvoice.Text = ToMaskEdit(CDbl(ToDouble(tempnilai) + ToDouble(trnamttax.Text)), 3)
        If totalinvoice.Text.Trim = "" Then
            totalinvoice.Text = "0.00"
        End If
        totalinvoicerp.Text = ToMaskEdit(ToDouble(totalinvoice.Text) * ToDouble(currencyrate.Text), 3)
        If totalinvoicerp.Text.Trim = "" Then totalinvoicerp.Text = "0.00"

    End Sub

    Sub InitAllDDL()
        sSql = "select currencyoid,currencycode+'-'+currencydesc from QL_mstcurr where cmpcode like '%" & CompnyCode & "%'"
        FillDDL(currate, sSql) : FillCurrencyRate(currate.SelectedValue)
        FillDDLGen("PAYTYPE", trnpaytype)
        ' Init Hutang Biaya Account
        Dim varExpAP As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_EXP_AP'")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varExpAP & "%' AND acctgoid not in " & _
            "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(itembiayaoid, sSql)
        If itembiayaoid.Items.Count <= 0 Then
            showMessage("Please create Expense Account Payable in Chart of Account.", CompnyName & " - Information", 3)
            Exit Sub
        End If
        ' Sales Account
        Dim varSales As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_SALES'")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varSales & "%' AND acctgoid not in " & _
            "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(purchasingaccount, sSql)
        If purchasingaccount.Items.Count < 1 Then
            showMessage("Please create/fill Purchasing Account in Chart of Accounts!!", _
                CompnyName & " - WARNING", 2)
        End If
    End Sub

    Sub Filltextbox(ByVal iOid As Int64)
        'sSql = "SELECT a.trnjualmstoid,a.trnjualno,a.trnjualdate,a.trnjualres1,a.trnjualref," & _
        '    "b.orderno,b.orderdate,b.orderflag,a.trncustoid,c.custname,a.trnpaytype,a.curroid,a.currate," & _
        '    "a.trnamtjual,a.trntaxpct,a.trnamttax,bm.jualbiayamstoid,ISNULL(bm.costtype,'Include') " & _
        '    "AS costtype,ISNULL(bm.totalamtbiaya,0) AS totalamtbiaya," & _
        '    "a.trnjualnote,a.trnjualstatus,a.trnjualres1,a.upduser,a.updtime,b.identifierno,ISNULL(a.trntaxtype,'') AS trntaxtype " & _
        '    ",ISNULL(b.digit,2) AS digit, a.custref, a.trndisctype, a.trndiscamt, a.amtdischdr FROM QL_trnjualmst a INNER JOIN ql_trnordermst b ON a.cmpcode=b.cmpcode AND " & _
        '    "a.trnjualref=b.orderoid INNER JOIN QL_mstcust c ON a.cmpcode=c.cmpcode AND " & _
        '    "a.trncustoid=c.custoid FULL OUTER JOIN ql_trnjualbiayamst bm ON a.cmpcode=bm.cmpcode AND " & _
        '    "a.trnjualmstoid=bm.trnjualmstoid WHERE a.cmpcode ='" & CompnyCode & "' AND a.trnjualmstoid=" & iOid

        sSql = "SELECT a.trnjualmstoid,a.trnjualno,a.trnjualdate,b.ordermstoid ,a.trnjualres1,a.trnjualref,b.orderno,b.trnorderdate  orderdate,a.trncustoid,c.custname,a.trnpaytype,a.currencyoid,a.currencyrate,a.trnamtjual,a.trntaxpct,a.trnamttax,a.trnjualnote,a.trnjualstatus,a.trnjualres1,a.upduser,a.updtime,case a.trntaxpct when 0 then 'NON TAX' else 'TAX' end AS trntaxtype ,2 as digit, a.trncustoid   custref, a.trndisctype, a.trndiscamt, a.amtdischdr FROM QL_trnjualmst a INNER JOIN ql_trnordermst b ON a.cmpcode=b.cmpcode AND a.orderno=b.orderno INNER JOIN QL_mstcust c ON a.cmpcode=c.cmpcode AND a.trncustoid=c.custoid  WHERE a.cmpcode ='" & CompnyCode & "' AND a.trnjualmstoid=" & iOid

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                oid.Text = Trim(xreader.Item("trnjualmstoid"))
                trnjualdate.Text = Format(CDate(Trim(xreader.Item("trnjualdate").ToString)), "dd/MM/yyyy")
                trnjualno.Text = Trim(xreader.Item("trnjualno"))
                dbsino.Text = Trim(xreader.Item("trnjualno"))
                trnjualres1.Text = Trim(xreader.Item("trnjualres1"))
                refoid.Text = Trim(xreader.Item("ordermstoid"))
                orderamt.Text = GetStrData("select isnull(sum(trnorderdtlQTY * trnorderprice),0) from QL_trnorderdtl where trnordermstoid = " & refoid.Text)

                referenceno.Text = Trim(xreader.Item("orderno"))
                trncustoid.Text = Trim(xreader.Item("trncustoid"))
                trnjualcust.Text = Trim(xreader.Item("custname"))
                trnorderdate.Text = Format(CDate(Trim(xreader.Item("orderdate").ToString)), "dd/MM/yyyy")
                'identifierno.Text = xreader("identifierno").ToString
                trnpaytype.SelectedValue = Trim(xreader.Item("trnpaytype"))
                currate.SelectedValue = Trim(xreader.Item("currencyoid"))
                currencyrate.Text = ToMaskEdit(Trim(xreader.Item("currencyrate")), 3)
                trnamtjual.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("trnamtjual"))), 3)
                trndisctype.SelectedValue = Trim(xreader.Item("trndisctype"))
                trndiscamt.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("trndiscamt"))), 3)
                amtdischdr.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("amtdischdr"))), 3)
                If trndisctype.SelectedValue = "AMT" Then
                    disc.Text = (ToDouble(trnamtjual.Text) / ToDouble(orderamt.Text)) * ToDouble(trndiscamt.Text)
                Else
                    disc.Text = ToDouble(trndiscamt.Text)
                End If


                taxtype.SelectedValue = Trim(xreader("trntaxtype").ToString)
                trntaxpct.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("trntaxpct"))), 3)
                If ToDouble(trntaxpct.Text) > 0 Then
                    SetControlTax(True)
                Else
                    SetControlTax(False)
                End If
                trnamttax.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("trnamttax"))), 3)
                'trnjualcosttype.SelectedValue = Trim(xreader.Item("costtype"))
                'trnjualamtcost.Text = ToMaskEdit(ToDouble(Trim(xreader.Item("totalamtbiaya"))), 3)
                trnjualnote.Text = Trim(xreader.Item("trnjualnote"))
                posting.Text = Trim(xreader.Item("trnjualstatus"))
                updUser.Text = Trim(xreader.Item("upduser"))
                updTime.Text = xreader.Item("updtime")
                'orderflag.Text = xreader.Item("orderflag")
                custoid.Text = xreader.Item("custref")

                If posting.Text = "POST" Then
                    btnSave.Visible = False
                    btnPosting.Visible = False
                    btnDelete.Visible = False
                    imbPrint.Visible = True
                    Printout.Visible = False
                    Export.Visible = False
                    btnPrint.Visible = False
                    btnExcel.Visible = False
                Else
                    btnSave.Visible = True
                    btnPosting.Visible = True
                    btnDelete.Visible = True
                    imbPrint.Visible = False
                    Printout.Visible = False
                    Export.Visible = False
                End If
                'If xreader("digit").ToString = 6 Then
                '    digit.Checked = True
                'End If
                digit.SelectedValue = xreader.Item("digit")
                'If xreader("trntaxtype") = "TAX" Then
                '    btnPrint.Visible = True
                'End If
            End While
        End If
        xreader.Close()
        conn.Close()

        help.Visible = False
        help.Text = "../reportform/PrintReport.aspx?type=SI&cmpcode=" & CompnyCode & "&oid=" & iOid

        ' Invoice Detail
      
        'sSql = "SELECT d.trnjualdtloid,d.trnjualmstoid,d.trnjualdtlseq,d.sjrefoid,d.itemoid,d.itemgroupcode," & _
        '   "d.trnjualdtlqty salesdeliveryqty,d.trnjualdtluom,d.trnjualdtlprice orderprice,d.trnjualnote AS trnjualdtlnote,d.trnjualstatus,d.ordermstoid,d.sjrefdtloid," & _
        '   "d.amtjualnetto,d.upduser,d.updtime,sj.salesdeliveryno,i.itemlongdesc,g.gendesc," & _
        '   "sj.salesdeliveryoid FROM QL_trnjualdtl d  INNER JOIN ql_trnsalesdelivmst sj " & _
        '   "ON d.cmpcode=sj.cmpcode AND d.sjrefoid=sj.salesdeliveryoid INNER JOIN " & _
        '   "QL_mstitem i ON d.cmpcode=i.cmpcode AND i.itemoid=d.itemoid INNER JOIN ql_mstgen g " & _
        '   "ON g.cmpcode=d.cmpcode AND d.trnjualdtluom=g.genoid WHERE d.trnjualmstoid=" & iOid & " AND " & _
        '   "d.cmpcode='" & CompnyCode & "' ORDER BY d.trnjualdtlseq"

        'dt.Columns.Add("unitseq", Type.GetType("System.Int32"))
        'dt.Columns.Add("trnorderdtloid", Type.GetType("System.Int32"))
        'dt.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))


        sSql = "SELECT distinct d.trnjualdtloid,d.trnjualmstoid,d.trnjualdtlseq,jm.trnsjjualmstoid  sjrefoid,d.itemoid,(select gendesc from ql_mstgen where genoid = i.itemgroupoid) itemgroupcode,d.trnjualdtlqty salesdeliveryqty,d.trnjualdtlunitoid ,d.trnjualdtlprice orderprice,d.trnjualnote AS trnjualdtlnote,d.trnjualstatus,(select ordermstoid from ql_trnordermst o where o.orderno = m.orderno) ordermstoid,sj.trnsjjualdtloid  sjrefdtloid,d.amtjualnetto,d.amtjualdisc,d.upduser,d.updtime,d.trnsjjualno  salesdeliveryno,i.itemdesc  itemlongdesc,g.gendesc,sj.trnsjjualmstoid  salesdeliveryoid,d.unitseq,d.trnorderdtloid,d.itemloc mtrlocoid, sj.trnsjjualdtloid salesdeliverydtloid FROM QL_trnjualdtl d inner join ql_trnjualmst m on d.trnjualmstoid = m.trnjualmstoid inner join ql_trnsjjualmst jm on jm.trnsjjualno = d.trnsjjualno  INNER JOIN ql_trnsjjualdtl sj ON d.cmpcode=sj.cmpcode AND jm.trnsjjualmstoid =sj.trnsjjualmstoid and d.itemoid = sj.refoid  INNER JOIN QL_mstitem i ON d.cmpcode=i.cmpcode AND i.itemoid=d.itemoid INNER JOIN ql_mstgen g ON g.cmpcode=d.cmpcode AND d.trnjualdtlunitoid=g.genoid WHERE d.trnjualmstoid=" & iOid & " AND d.cmpcode='" & CompnyCode & "' ORDER BY d.trnjualdtlseq"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data1")
        TblDtl.DataSource = objDs.Tables("data1")
        TblDtl.DataBind()
        Session("ItemLine") = objDs.Tables("data1").Rows.Count + 1
        Session("TblDtl") = objDs.Tables("data1")
        trnjualdtlseq.Text = Session("ItemLine")

        '' Biaya Detail
        'sSql = "SELECT bd.jualbiayaseq,bd.itembiayaoid,bd.itembiayanote,bd.itembiayaamt,bd.upduser," & _
        '    "bd.updtime,a.acctgcode+'-'+a.acctgdesc AS acctgdesc FROM ql_trnjualbiayadtl bd " & _
        '    "INNER JOIN ql_mstacctg a ON bd.cmpcode=a.cmpcode AND bd.itembiayaoid=a.acctgoid " & _
        '    "INNER JOIN ql_trnjualbiayamst b ON bd.cmpcode=b.cmpcode AND bd.jualbiayamstoid=b.jualbiayamstoid " & _
        '    "WHERE bd.cmpcode='" & CompnyCode & "' AND b.trnjualmstoid=" & iOid & " ORDER BY bd.jualbiayaseq"
        'Dim mySqlDA1 As New SqlDataAdapter(sSql, conn)
        'Dim objDs1 As New DataSet
        'mySqlDA1.Fill(objDs1, "data1")
        'gvBiaya.DataSource = objDs1.Tables("data1")
        'gvBiaya.DataBind()
        'Session("ItemLineBiaya") = objDs1.Tables("data1").Rows.Count + 1
        'Session("DtlBiaya") = objDs1.Tables("data1")
        'jualbiayaseq.Text = Session("ItemLineBiaya")

        Dim userlogin As String = Session("UserID")

        If posting.Text = "POST" Then
            'If userlogin.ToUpper = "ACC1" Or userlogin.ToUpper = "ACC2" Or userlogin.ToUpper = "ACC3" Or userlogin.ToUpper = "ACC4" Or userlogin.ToUpper = "ACC5" Or userlogin.ToUpper = "ADMIN" Or userlogin.ToUpper = "ADMIN2" Then
            '    Dim statuspajak As String = cKoneksi.ambilscalar("select isnull(statusfakturpajak, '') from QL_trnjualmst where trnjualmstoid =" & oid.Text)
            '    Dim nopajak As String = cKoneksi.ambilscalar("select isnull(nofakturpajak, '') from QL_trnjualmst where trnjualmstoid =" & oid.Text)

            '    txtnofakturpajak.Text = nopajak
            '    If statuspajak = "POST" Then
            '        txtnofakturpajak.Enabled = False
            '    Else
            '        txtnofakturpajak.Enabled = True
            '    End If

            '    txtnofakturpajak.Visible = True
            '    lblacctg.Visible = True
            '    lblnofakturpajak.Visible = True
            '    postingfaktur.Visible = True

            'End If
        End If

        'SumPriceInInvoiceDetail(Session("TblDtl"))
        'ReAmount()




        sSql = "SELECT  updtime_status [updtime], createuser [user] FROM ql_collectiondtl WHERE invoiceoid = '" & oid.Text & "'"
        Dim collection As DataTable = CreateDataTableFromSQL(sSql)

        sSql = "SELECT useroid_status[user], updtime_status [updtime] FROM ql_collectionstatus where invoiceoid = '" & oid.Text & "'"
        Dim collectionStatus As DataTable = CreateDataTableFromSQL(sSql)

        For i As Integer = 0 To collection.Rows.Count - 1
            lblCollection.Text = "Collection <b>Out</b> date (" & collection.Rows(i).Item("updtime") & ") and user update '" & collection.Rows(i).Item("user") & "' <br />"
            If (i <= collectionStatus.Rows.Count - 1) Then
                lblCollection.Text = "Collection <b>Status</b> date (" & collection.Rows(i).Item("updtime") & ") and user update '" & collection.Rows(i).Item("user") & "' <br />"
            End If
        Next


    End Sub

    Public Sub SetControlForecast(ByVal status As Boolean)
        
    End Sub

    Public Sub SetControlSalesOrder(ByVal status As Boolean)
        tblDetilDso.Visible = status
        ' btnAddAllList.Visible = status

    End Sub

    Public Sub BindData(ByVal sWhere As String)
        Try
            sSql = "SELECT a.trnjualmstoid,a.trnjualno,a.trnjualdate,a.trnjualstatus,s.custname,trnjualnote,trnjualres1,a.orderno,a.trnsjjualno " & _
                "FROM QL_trnjualmst a INNER JOIN QL_mstcust s ON a.cmpcode=s.cmpcode AND a.trncustoid=s.custoid " & _
                "INNER JOIN ql_trnordermst so ON so.cmpcode=a.cmpcode AND so.orderno=a.orderno inner join QL_MSTPERSON p on so.salesoid = p.PERSONOID WHERE a.cmpcode='" & CompnyCode & "' and trnjualtype = 'PROJECK'  "

            Dim sOther As String = ""
            If chkPeriod.Checked Then
                sOther &= " AND a.trnjualdate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy") & "' AND '" & _
                    Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy") & "' "
            Else
                sOther &= " AND a.trnjualdate BETWEEN '" & Format(Today, "01/01/1990") & _
                    "' AND '" & Format(Today, "MM/dd/yyyy") & "' "
            End If
            If chkStatus.Checked Then
                If DDLStatus.SelectedIndex <> 0 Then
                    sOther &= " AND a.trnjualstatus LIKE '%" & DDLStatus.SelectedValue & "%'"
                Else
                    sOther &= " AND a.trnjualstatus LIKE '%%' "
                End If
            Else
                sOther &= " AND a.trnjualstatus LIKE '%%' "
            End If

            Dim salesperson As String = Session("UserID")

            'If salesperson.ToLower = "admin" Then
            '    sOther &= " "
            'Else
            '    sOther &= " AND  PERSONNIP like '" & Session("UserID") & "' "
            'End If


            sOther &= " " & sWhere & " ORDER BY a.periodacctg,right(a.trnjualno,4) desc,a.trnjualdate desc "
            sSql &= sOther
            sqlTempSearch = sSql ' Untuk simpan history search

            ClassFunction.FillGV(Tbldata, sSql, "ql_trnjualmst")
        Catch ex As Exception
            showMessage(ex.ToString, "", 1)
        End Try

    End Sub

    Public Sub BindDataSJjual()

        'sSql = "select sd.salesdeliveryoid as trnsjjualmstoid,sd.salesdeliveryno ,so.orderno,so.orderoid trnrefoid,sd.salesdeliverynote trnsjjualnote,so.orderdate," & _
        '"sd.salesdeliverydate trnsjjualdate,sd.salesdeliveryshipdate trnsjjualsenddate,sd.custoid ," & _
        '"c.custname,so.ordercurroid,so.ordercurrate " & _
        '"from ql_trnordermst so inner join ql_trnsalesdelivmst sd on so.orderoid=sd.ordermstoid  " & _
        '"and sd.salesdeliverystatus='POST' or sd.salesdeliverystatus='Approved' AND sd.cmpcode=so.cmpcode " & _
        '"inner join ql_trnsalesdelivdtl sdd on sdd.salesdeliverymstoid =sd.salesdeliveryoid " & _
        '"inner join ql_trnorderdtl sod on  so.ioref=sod.ordermstoid and sod.orderdtloid=sdd.orderdtloid " & _
        '"and sod.free='N' inner join ql_mstcust c ON sd.custoid=c.custoid  AND sd.cmpcode=c.cmpcode " & _
        '"WHERE so.cmpcode='" & CompnyCode & "' and so.orderoid='" & refoid.Text & "' group BY sd.salesdeliveryoid ," & _
        '"sd.salesdeliveryno ,so.orderno,so.orderoid ,sd.salesdeliverynote ,so.orderdate,sd.salesdeliverydate ," & _
        '"sd.salesdeliveryshipdate ,sd.custoid ,c.custname,so.ordercurroid,so.ordercurrate "

        sSql = "select sd.trnsjjualmstoid  as trnsjjualmstoid,sd.trnsjjualno  salesdeliveryno ,so.orderno,so.ordermstoid  trnrefoid,sd.note trnsjjualnote,so.trnorderdate  orderdate,sd.trnsjjualdate  trnsjjualdate,sd.trnsjjualdate trnsjjualsenddate,so.ordermstoid  custoid ,c.custname,1 ordercurroid,1 ordercurrate from ql_trnordermst so inner join ql_trnsjjualmst  sd on so.orderno =sd.orderno   and  sd.trnsjjualstatus ='Approved' AND sd.cmpcode=so.cmpcode inner join ql_trnsjjualdtl  sdd on sdd.trnsjjualmstoid  =sd.trnsjjualmstoid  inner join ql_trnorderdtl sod on  so.ordermstoid =sod.trnordermstoid and sod.trnorderdtloid=sdd.trnorderdtloid  inner join ql_mstcust c ON so.trncustoid=c.custoid  AND sd.cmpcode=c.cmpcode WHERE so.cmpcode='" & CompnyCode & "' and so.ordermstoid='" & refoid.Text & "' group BY sd.trnsjjualmstoid  ,sd.trnsjjualno  ,so.orderno,so.ordermstoid ,sd.note ,so.trnorderdate,sd.trnsjjualdate  ,so.trncustoid ,c.custname"

        ClassFunction.FillGV(gvSJ, sSql, "ql_trnsjjualmst")
    End Sub

    Public Sub BindDataListOrder(ByVal sWhere As String)
        sSql = "select so.orderno,so.ordermstoid orderoid,convert(char(10),so.trnorderdate,103)orderdate,so.trncustoid custoid ,c.custname,1 ordercurroid,1 ordercurrate,so.trntaxpct  ordertaxpct,0 hdiscamtpct,2 as digit, so.trnpaytype paytermoid, 0 hdisctype, case so.trntaxpct when 0 then 'NO' else 'YES' end taxable from ql_trnordermst so inner join ql_trnsjjualmst  sd on so.orderno =sd.orderno   and sd.trnsjjualstatus ='Approved'  AND sd.cmpcode=so.cmpcode inner join ql_trnsjjualdtl  sdd on sdd.trnsjjualmstoid  =sd.trnsjjualmstoid inner join ql_trnorderdtl sod on  so.ordermstoid =sod.trnordermstoid and sod.trnorderdtloid=sdd.trnorderdtloid inner join ql_mstcust c ON so.trncustoid=c.custoid  AND sd.cmpcode=c.cmpcode WHERE so.cmpcode='" & CompnyCode & "' and so.trnordertype = 'projeck' " & sWhere & " group by so.orderno,so.ordermstoid,so.trnorderdate,so.trncustoid ,c.custname,so.trntaxpct , so.trnpaytype "

        'sSql = "select so.orderno,so.orderoid,convert(char(10),so.orderdate,103)orderdate,sd.custoid ,c.custname,so.ordercurroid,convert(char(10),so.ordercurrate,103)ordercurrate,so.ordertaxpct,so.hdiscamtpct,ISNULL(so.digit,2) as digit, so.paytermoid, so.hdisctype, so.taxable from ql_trnordermst so inner join ql_trnsalesdelivmst sd on so.orderoid=sd.ordermstoid  and sd.salesdeliverystatus='POST' or sd.salesdeliverystatus='Approved' AND sd.cmpcode=so.cmpcode inner join ql_trnsalesdelivdtl sdd on sdd.salesdeliverymstoid =sd.salesdeliveryoid inner join ql_trnorderdtl sod on  so.ioref=sod.ordermstoid and sod.orderdtloid=sdd.orderdtloid and sod.free='N' inner join ql_mstcust c ON sd.custoid=c.custoid  AND sd.cmpcode=c.cmpcode WHERE so.cmpcode='" & CompnyCode & "' and so.orderno not like '%E' " & sWhere & " AND so.market='Local' group by so.orderno,so.orderoid,so.orderdate,sd.custoid ,c.custname,so.ordercurroid,so.ordercurrate,so. ordertaxpct,so.hdiscamtpct,digit, so.paytermoid,so.hdisctype, so.taxable"
        ClassFunction.FillGV(gvListSO, sSql, "ql_trnordermst")
    End Sub

    Sub binddataSJDetil(ByVal sjMstoid As Integer)

        'sSql = "SELECT i.itemlongdesc,sjd.salesdeliveryqty trnsjjualdtlqty,od.itemgroupcode,od.free,g.gendesc FROM ql_trnsalesdelivdtl sjd inner join ql_trnorderdtl od on od.orderdtloid=sjd.orderdtloid inner join ql_trnsalesdelivdtlbatch b on sjd.salesdeliverydtloid=b.salesdeliverydtloid INNER JOIN ql_mstitem i ON b.cmpcode=i.cmpcode AND b.itemoid=i.itemoid INNER JOIN ql_mstgen g ON b.delivdtlunitoid=g.genoid AND sjd.cmpcode=g.cmpcode AND sjd.salesdeliverymstoid=" & sjMstoid & " AND sjd.cmpcode='" & CompnyCode & "'"

        sSql = "SELECT i.itemdesc itemlongdesc,sjd.qty trnsjjualdtlqty,gr.gendesc  itemgroupcode,g.gendesc FROM ql_trnsjjualdtl  sjd inner join ql_trnorderdtl od on od.trnorderdtloid=sjd.trnorderdtloid INNER JOIN ql_mstitem i ON sjd.refoid =i.itemoid and sjd.refname = 'QL_MSTITEM' inner join QL_mstgen gr on gr.genoid = i.itemgroupoid  INNER JOIN ql_mstgen g ON sjd.unitoid =g.genoid AND sjd.cmpcode=g.cmpcode AND sjd.trnsjjualmstoid =" & sjMstoid & " AND sjd.cmpcode='" & CompnyCode & "'"

        ClassFunction.FillGV(tblDetilDso, sSql, "ql_trnsjjualdtl")
        tblDetilDso.Visible = True
        'PanelDetilDSO.Visible = True : btnHideDetilDso.Visible = True
        'ModalPopupExtenderDetilDSO.Show()
    End Sub

    Public Sub BindDataListForecast()
        sSql = "select ql_trnforcmst.forcoid, ql_trnforcmst.forcno,ql_trnforcmst.forcdate, ql_trnforcmst.custoid, ql_trnforcmst.forcnote, ql_mstcust.custname from ql_trnforcmst, ql_mstcust where ql_trnforcmst.custoid=ql_mstcust.custoid and  ql_trnforcmst.cmpcode=ql_mstcust.cmpcode and  ql_trnforcmst.forcstatus='Approved' "
        FillGV(gvListForecast, sSql, "ql_trnforcmst")
    End Sub

    Sub BinddataSalesForecast()
        sSql = "select ql_trnforcdtl.itemoid,ql_mstitem.itemlongdesc,ql_trnforcdtl.forcqty,ql_trnforcdtl.forcacumqty,ql_trnforcdtl.itemoid from ql_mstitem, ql_trnforcdtl, ql_trnforcmst where ql_mstitem.itemoid = ql_trnforcdtl.itemoid And ql_trnforcdtl.forcmstoid = ql_trnforcmst.forcoid And  ql_trnforcdtl.forcdtlstatus <> 'Completed' and ql_trnforcmst.forcoid = " & refoid.Text & ""
        FillGV(tblDetilForecast, sSql, "Forecast")
    End Sub
#End Region


    Protected Sub btnsearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If trnjualref.SelectedValue = "ql_trnordermst" Then
            BindDataListOrder(" and (so.orderno like '%" & Tchar(referenceno.Text) & "%' or c.custname like '%" & Tchar(referenceno.Text) & "%' ) ")
            gvListSO.Visible = True
            'PanelSO.Visible = True
            'btnHideso.Visible = True
            'mpeso.Show()
        End If
        If trnjualref.SelectedValue = "ql_trnforcmst" Then
            BindDataListForecast()
            PanelForecast.Visible = True
            btnHideForecast.Visible = True
            ModalPopupExtenderForecast.Show()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSI")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSI") = sqlSearch
            Session("click_post") = False
            Response.Redirect("~\Transaction\trnnotajualProjeck.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Sales Invoice Projeck"
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        lkbPostPreview.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data ?');")
        
        If Not IsPostBack Then
            FilterPeriod1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
            BindData("")
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update"
                Filltextbox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                btnsearchSO.Visible = True
                ImageButton2.Visible = True
                lblUpdate.Text = "Last Update By"
                lblOn.Text = "On"
            Else
                i_u.Text = "New"
                updUser.Text = Session("UserID")
                updTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
                trnjualdtlseq.Text = 1
                Session("ItemLine") = trnjualdtlseq.Text
                jualbiayaseq.Text = 1
                Session("ItemLineBiaya") = jualbiayaseq.Text
                trnjualdate.Text = Format(Now, "dd/MM/yyyy")
                'generateNo()
                periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text)))
                btnDelete.Visible = False
                imbPrint.Visible = False
                btnsearchSO.Visible = True
                ImageButton2.Visible = True
                btnSave.Visible = True
                TabContainer1.ActiveTabIndex = 0
                lblUpdate.Text = "Create By"
                lblOn.Text = "On"
            End If

            Dim objTable As New DataTable : objTable = Session("TblDtl")
            TblDtl.DataSource = objTable : TblDtl.DataBind()
            SumPriceInInvoiceDetail(objTable)

            Dim objTable2 As New DataTable : objTable2 = Session("DtlBiaya")
            gvBiaya.DataSource = objTable2 : gvBiaya.DataBind()
            SumBiayaDetail(objTable2) : ReAmount()
        End If
    End Sub

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        refoid.Text = gvListSO.SelectedDataKey.Item(0)
        referenceno.Text = gvListSO.SelectedDataKey.Item(1)
        trnorderdate.Text = Format(CDate(toDate(gvListSO.SelectedDataKey.Item(2))), "dd/MM/yyyy")
        orderamt.Text = GetStrData("select isnull(sum(trnorderdtlQTY * trnorderprice),0) from QL_trnorderdtl where trnordermstoid = " & refoid.Text)
        trnjualcust.Text = gvListSO.SelectedDataKey.Item(3)
        trncustoid.Text = gvListSO.SelectedDataKey.Item(4)
        'posting.Text = gvListSO.SelectedDataKey.Item(4)
        disc.Text = gvListSO.SelectedDataKey.Item("hdiscamtpct")
        tax.Text = gvListSO.SelectedDataKey.Item("ordertaxpct")
        trntaxpct.Text = gvListSO.SelectedDataKey.Item("ordertaxpct")
        posting.Text = "In Process"
        currate.SelectedValue = gvListSO.SelectedDataKey.Item(5)
        currencyrate.Text = ToMaskEdit(ToDouble(gvListSO.SelectedDataKey.Item(6)), 3)
        digit.SelectedValue = gvListSO.SelectedDataKey.Item("digit")
        trnpaytype.SelectedValue = gvListSO.SelectedDataKey.Item("paytermoid")
        'identifierno.Text = gvListSO.SelectedDataKey("identifierno").ToString
        'trndisctype.SelectedValue = gvListSO.SelectedDataKey.Item("hdisctype")
        If gvListSO.SelectedDataKey.Item("taxable") = "YES" Then
            taxtype.SelectedIndex = 1
        Else
            taxtype.SelectedIndex = 0
        End If

        Session("TblDtl") = Nothing : cProc.DisposeGridView(TblDtl)
        SumPriceInInvoiceDetail(Session("TblDtl")) : ReAmount()
        Session("ItemLine") = 1 : trnjualdtlseq.Text = Session("ItemLine")
        generateNo()
        'PanelSO.Visible = False : btnHideso.Visible = False : mpeso.Hide()
        gvListSO.Visible = False
        cProc.DisposeGridView(gvListSO)
    End Sub

    Protected Sub trnjualref_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TblDtl") = Nothing

        referenceno.Text = ""
        refoid.Text = ""
        trnjualcust.Text = ""
        trncustoid.Text = ""
        If trnjualref.SelectedValue = "ql_trnforcmst" Then
            SetControlForecast(True)
            SetControlSalesOrder(False)
        End If
        If trnjualref.SelectedValue = "ql_trnordermst" Then
            SetControlForecast(False)
            SetControlSalesOrder(True)
        End If
    End Sub

    Protected Sub btnSearhDSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If refoid.Text = "" Then
            showMessage("Please select Sales Order first!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        'panelDso.Visible = True
        'btnHideDSO.Visible = True
        'ModalPopupExtenderdso.Show()
        gvSJ.Visible = True
        BindDataSJjual()
    End Sub

    Protected Sub gvSJ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Cek SJ
        Dim dtlTable As DataTable
        If Session("TblDtl") Is Nothing Then
            dtlTable = setTabelDetail() : Session("TblDtl") = dtlTable
        End If
        dtlTable = Session("TblDtl")
        Dim dv As DataView = dtlTable.DefaultView
        dv.RowFilter = "salesdeliveryoid='" & gvSJ.SelectedDataKey.Item(0) & "'"
        If dv.Count > 0 Then
            lblState.Text = "SDO"
            showMessage("This data has been added before. Please check.", CompnyName & " - WARNING", 2)
            Exit Sub
        End If : dv.RowFilter = ""


        'sSql = "SELECT sj.salesdeliveryno,sum(sjd.salesdeliveryqty) salesdeliveryqty, sod.orderprice, sod.itemgroupcode, g.gendesc, sjb.delivdtlunitoid,sj.salesdeliveryoid,sjd.salesdeliverydtloid,sjb.itemoid FROM ql_trnsalesdelivdtl sjd INNER JOIN ql_trnsalesdelivmst sj ON sjd.cmpcode=sj.cmpcode AND sjd.salesdeliverymstoid=sj.salesdeliveryoid inner join ql_trnsalesdelivdtlbatch sjb on sjd.cmpcode=sjb.cmpcode and sjd.salesdeliverydtloid=sjb.salesdeliverydtloid inner join ql_trnorderdtl sod on sjd.orderdtloid=sod.orderdtloid  INNER JOIN ql_mstgen g ON sjd.cmpcode=g.cmpcode AND sjb.delivdtlunitoid=g.genoid WHERE sod.free='N' and sjd.cmpcode='" & CompnyCode & "' AND sj.salesdeliveryoid=" & gvSJ.SelectedDataKey.Item(0) & " group by sj.salesdeliveryno,sod.orderprice, sod.itemgroupcode, g.gendesc, sjb.delivdtlunitoid,sj.salesdeliveryoid,sjd.salesdeliverydtloid,sjb.itemoid"

        sSql = "SELECT sj.trnsjjualmstoid  sjrefoid,sj.trnsjjualmstoid  salesdeliveryoid,sj.trnsjjualno salesdeliveryno,i.itemdesc itemlongdesc,sjd.refoid itemoid ,sjd.qty salesdeliveryqty ,sjd.unitoid delivdtlunitoid,sod.trnorderprice orderprice,  gr.gendesc Itemgroupcode , g.gendesc , sj.trnsjjualmstoid salesdeliveryoid , sjd.trnsjjualdtloid salesdeliverydtloid ,sod.trnorderprice - (sod.trnamountdtl/sod.trnorderdtlqty) discperqty,sod.unitseq,sjd.trnorderdtloid,sjd.mtrlocoid FROM ql_trnsjjualdtl sjd INNER JOIN ql_trnsjjualmst  sj ON sjd.cmpcode=sj.cmpcode AND sjd.trnsjjualmstoid =sj.trnsjjualmstoid  inner join ql_trnorderdtl sod on sjd.trnorderdtloid=sod.trnorderdtloid  INNER JOIN ql_mstitem i ON sjd.refoid =i.itemoid and sjd.refname = 'QL_MSTITEM' inner join QL_mstgen gr on gr.genoid = i.itemgroupoid inner join QL_mstgen g on g.genoid = sjd.unitoid  WHERE sjd.cmpcode='" & CompnyCode & "' AND sj.trnsjjualmstoid =" & gvSJ.SelectedDataKey.Item(0) & " "

        Dim objSjdetil As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
        Dim objTbldetil As DataTable = Session("TblDtl")
        For c1 As Int16 = 0 To objSjdetil.Rows.Count - 1
            Dim objrow As DataRow = objTbldetil.NewRow
            objrow("trnjualdtlseq") = Session("ItemLine") + c1
            objrow("trnjualdtloid") = 0
            objrow("trnjualmstoid") = 0
            'objrow("sjrefoid") = objSjdetil.Rows(c1).Item("salesdeliverydtloid")
            objrow("sjrefoid") = objSjdetil.Rows(c1).Item("salesdeliveryoid")
            objrow("itemoid") = objSjdetil.Rows(c1).Item("itemoid")
            objrow("salesdeliveryqty") = objSjdetil.Rows(c1).Item("salesdeliveryqty")
            objrow("trnjualdtlunitoid") = objSjdetil.Rows(c1).Item("delivdtlunitoid")
            objrow("orderprice") = objSjdetil.Rows(c1).Item("orderprice")
            objrow("itemlongdesc") = objSjdetil.Rows(c1).Item("itemlongdesc")
            objrow("trnjualdtlnote") = ""
            objrow("trnjualstatus") = ""
            objrow("amtjualnetto") = (objSjdetil.Rows(c1).Item("salesdeliveryqty") * objSjdetil.Rows(c1).Item("orderprice")) - (objSjdetil.Rows(c1).Item("salesdeliveryqty") * objSjdetil.Rows(c1).Item("discperqty"))
            objrow("amtjualdisc") = objSjdetil.Rows(c1).Item("salesdeliveryqty") * objSjdetil.Rows(c1).Item("discperqty")
            objrow("upduser") = Session("UserID")
            objrow("updtime") = GetServerTime()
            objrow("salesdeliveryno") = gvSJ.SelectedDataKey.Item(1)
            objrow("itemgroupcode") = objSjdetil.Rows(c1).Item("Itemgroupcode")
            objrow("gendesc") = objSjdetil.Rows(c1).Item("gendesc")
            objrow("salesdeliveryoid") = objSjdetil.Rows(c1).Item("salesdeliveryoid")
            objrow("salesdeliverydtloid") = objSjdetil.Rows(c1).Item("salesdeliverydtloid")
            objrow("unitseq") = objSjdetil.Rows(c1).Item("unitseq")
            objrow("trnorderdtloid") = objSjdetil.Rows(c1).Item("trnorderdtloid")
            objrow("mtrlocoid") = objSjdetil.Rows(c1).Item("mtrlocoid")

            'objrow("trnjualpackingqty") = objSjdetil.Rows(c1).Item("packingqty")
            objTbldetil.Rows.Add(objrow)
        Next
        Session("ItemLine") += objSjdetil.Rows.Count
        trnjualdtlseq.Text = Session("ItemLine")
        Session("TblDtl") = objTbldetil
        SumPriceInInvoiceDetail(Session("TblDtl")) : ReAmount()

        TblDtl.DataSource = objTbldetil : TblDtl.DataBind()
        'ReAmount()
        'panelDso.Visible = False : btnHideDSO.Visible = False : ModalPopupExtenderdso.Hide()
        gvSJ.Visible = False
        tblDetilDso.Visible = False
        TblDtl.SelectedIndex = -1
        cProc.DisposeGridView(gvSJ)
    End Sub

    Protected Sub btnCloseForecast_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelForecast.Visible = False
        btnHideForecast.Visible = False
    End Sub

    Protected Sub gvListForecast_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        refoid.Text = gvListForecast.SelectedDataKey(0)
        referenceno.Text = gvListForecast.SelectedDataKey(1)
        trnjualcust.Text = gvListForecast.SelectedDataKey(3)
        trncustoid.Text = gvListForecast.SelectedDataKey(4)

        PanelForecast.Visible = False
        btnHideForecast.Visible = False
        ModalPopupExtenderForecast.Hide()
    End Sub

    Protected Sub trndisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trndisctype.SelectedValue = "AMT" Then 'amount
            lbl1.Text = "Discount (Amt)"
        Else
            lbl1.Text = "Discount (%)"
        End If
        trndiscamt.Text = "0.00"
        ReAmount()
    End Sub

    Protected Sub lkbCloseSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelSO.Visible = False : btnHideso.Visible = False
        mpeso.Hide() : cProc.DisposeGridView(gvListSO)
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblState.Text = "SDO" Then
            gvSJ.Visible = True
        End If
        lblState.Text = ""
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        referenceno.Text = "" : refoid.Text = ""
        trnjualcust.Text = "" : trncustoid.Text = ""
        trnorderdate.Text = "" : currate.SelectedIndex = 0
        currate_SelectedIndexChanged(Nothing, Nothing)
        identifierno.Text = ""
        Session("TblDtl") = Nothing : cProc.DisposeGridView(TblDtl)
        Session("ItemLine") = 1 : trnjualdtlseq.Text = Session("ItemLine")
        SumPriceInInvoiceDetail(Session("TblDtl")) : ReAmount()
        gvListSO.Visible = False
    End Sub

    Protected Sub btnAddAllList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnClearDSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sjoid.Text = "" : Sjno.Text = ""
        gvSJ.Visible = False
        tblDetilDso.Visible = False
    End Sub

    Protected Sub btnAddToList_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If itemoid.Text = "" Then : sMsg &= "- Please choose Item !!<BR>" : End If
        If ToDouble(trnjualdtlqty.Text) <= 0 Then : sMsg &= "- Quantity must be greater than 0 !!<BR>" : End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
        End If

        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail() : Session("TblDtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("TblDtl")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If I_u2.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = " and itemoid=" & itemoid.Text
            If dv.Count > 0 Then
                showMessage("This data has been added before, please check!", CompnyName & " - WARNING", 2)
                dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("trnjualdtlseq") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(trnjualdtlseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("orderprice") = ToDouble(trnjualdtlprice.Text)
        objRow("amtjualnetto") = ToDouble(amtjualnetto.Text)
        objRow("trnjualdtlnote") = trnjualdtlnote.Text

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If

        ClearDetail()
        Session("TblDtl") = objTable
        TblDtl.DataSource = objTable
        TblDtl.DataBind()
        SumPriceInInvoiceDetail(Session("TblDtl"))
        ReAmount()

        Session("ItemLine") = objTable.Rows.Count + 1
        trnjualdtlseq.Text = Session("ItemLine")
        TblDtl.SelectedIndex = -1
        btnSearhDSO.Visible = True : btnClearDSO.Visible = True
        btnAddToList.Visible = False : btnClear.Visible = False
    End Sub

    Protected Sub closeDSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        panelDso.Visible = False : btnHideDSO.Visible = False
        ModalPopupExtenderdso.Hide() : cProc.DisposeGridView(gvSJ)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        Dim pay As Double

        pay = GetStrData("select genother1 from QL_mstgen where gengroup = 'paytype' and genoid =" & trnpaytype.SelectedValue & " ")
        'If trnjualno.Text.Trim = "" Then
        '    sMsg &= "- Please fill Invoice No!!<BR>" 'Please select a PO Number 
        'Else
        '    ' Check same TrnJualNo
        '    Dim sCheck As String = "SELECT count(-1) FROM ql_trnjualmst WHERE trnjualno='" & trnjualno.Text & _
        '        "' AND cmpcode='" & CompnyCode & "'"
        '    If (Session("oid") = Nothing Or Session("oid") = "") Then
        '        If cKoneksi.ambilscalar(sCheck) > 0 Then
        '            generateNo()
        '        End If
        '    Else
        '        If cKoneksi.ambilscalar(sCheck & " AND trnjualmstoid<>" & oid.Text) > 0 Then
        '            generateNo()
        '        End If
        '    End If
        'End If
        If refoid.Text.Trim = "" Then
            sMsg &= "- Please select Sales Order!!<BR>" 'Please select a Supplier
        End If
        If IsDate(toDate(trnjualdate.Text)) = False Then
            sMsg &= "- Invalid Invoice Date!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            posting.Text = "" : Exit Sub
        End If

        'cek apa sudah ada ITEM dari SO
        If Session("TblDtl") Is Nothing Then
            showMessage("No Invoice detail!!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            posting.Text = "" : Exit Sub
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                showMessage("No Invoice detail!!", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                posting.Text = "" : Exit Sub
            End If
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnjualno.Text, "trnjualno", "ql_trnjualmst") Then
                generateNo()
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try 'INSERT TO QL_trnjualmst
                'Dim iDigit As Integer = 2
                'If digit.Checked Then
                '    iDigit = 6
                'End If
                If custoid.Text = "" Then
                    custoid.Text = trncustoid.Text
                End If
                oid.Text = GenerateID("QL_trnjualmst", CompnyCode)
                sSql = "INSERT INTO QL_trnjualmst(cmpcode,trnjualmstoid,periodacctg,trnjualtype,trnjualno,trnjualdate," & _
                    "trnjualref,trncustoid,trncustname,trnpaytype,trnamtjual,trntaxpct,trnamttax,trndisctype,trndiscamt,trnamtjualnetto," & _
                    "trnjualnote,trnjualstatus,trnjualres1,accumpayment,lastpaymentdate,amtdischdr,amtdiscdtl,currencyoid,currencyrate,upduser,updtime,trnamtjualnettoacctg,orderno,ekspedisioid,trndisctype2,createuser) VALUES ('" & _
                    CompnyCode & "'," & oid.Text & ",'" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & "','PROJECK','" & Tchar(trnjualno.Text) & _
                    "','" & CDate(toDate(trnjualdate.Text)) & "','" & refoid.Text & "'," & trncustoid.Text & ",'" & Tchar(trnjualcust.Text) & "'," & _
                    trnpaytype.SelectedValue & "," & ToDouble(trnamtjual.Text) & "," & ToDouble(trntaxpct.Text) & _
                    "," & ToDouble(trnamttax.Text) & ",'" & trndisctype.SelectedValue & "'," & ToDouble(trndiscamt.Text) & "," & ToDouble(totalinvoice.Text) & ",'" & _
                    Tchar(trnjualnote.Text) & "','" & posting.Text & "','" & Tchar(trnjualres1.Text) & "',0,'1/1/1900'," & ToDouble(amtdischdr.Text) & ",0," & currate.SelectedValue & _
                    "," & ToDouble(currencyrate.Text) & ",'" & Session("UserID") & "',CURRENT_TIMESTAMP," & ToDouble(totalinvoice.Text) & ",'" & referenceno.Text & "',0,'AMT','" & Session("UserID") & "' )"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename ='QL_trnjualmst' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim trnjualdtloid As Int64 = GenerateID("QL_trnjualdtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1

                        'setting satuan 3
                        Dim setupsatuan3 As Decimal
                        'update to last price 
                        Dim konv1_2 As Integer : Dim konv2_3 As Integer

                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        konv1_2 = xCmd.ExecuteScalar

                        sSql = "select konversi2_3 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        konv2_3 = xCmd.ExecuteScalar

                        If objTable.Rows(c1).Item("unitseq") = 1 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv1_2 * konv2_3
                        ElseIf objTable.Rows(c1).Item("unitseq") = 2 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv2_3
                        Else
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty"))
                        End If


                        sSql = "INSERT INTO QL_trnjualdtl (cmpcode,trnjualdtloid,trnjualmstoid,trnjualdtlseq,trnsjjualno,itemoid,trnjualdtlqty, trnjualdtlunitoid,unitseq,trnjualdtlprice,trnjualdtlpriceunitoid,trnjualdtldisctype,trnjualdtldiscqty,trnjualflag, trnjualnote,trnjualstatus,amtjualdisc,updtime,upduser,createuser,amtjualnetto,trnjualdtldisctype2,trnorderdtloid,itemloc,trnjualdtlqty_unit3,trnsjjualdtloid) VALUES ('" & CompnyCode & "'," & trnjualdtloid + c1 & "," & oid.Text & "," & objTable.Rows(c1).Item("trnjualdtlseq") & ",'" & objTable.Rows(c1).Item("salesdeliveryno") & "'," & objTable.Rows(c1).Item("itemoid") & "," & objTable.Rows(c1).Item("salesdeliveryqty") & "," & objTable.Rows(c1).Item("trnjualdtlunitoid") & "," & objTable.Rows(c1).Item("unitseq") & ", " & objTable.Rows(c1).Item("orderprice") & "," & objTable.Rows(c1).Item("trnjualdtlunitoid") & ",'AMT'," & objTable.Rows(c1).Item("amtjualdisc") / objTable.Rows(c1).Item("salesdeliveryqty") & ",'','','In Process'," & Tchar(objTable.Rows(c1).Item("amtjualdisc")) & ",CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & Session("UserID") & "'," & ToDouble(objTable.Rows(c1).Item("amtjualnetto")) & ",'AMT', " & objTable.Rows(c1).Item("trnorderdtloid") & ", " & objTable.Rows(c1).Item("mtrlocoid") & "," & setupsatuan3 & "," & objTable.Rows(c1).Item("salesdeliverydtloid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()


                        ' Update SJDetail status to Invoiced
                        sSql = "UPDATE ql_trnsjjualmst SET trnsjjualstatus='INVOICED' WHERE trnsjjualmstoid='" & objTable.Rows(c1).Item("salesdeliveryoid") & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + trnjualdtloid) & " where tablename = 'QL_trnjualdtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close() : posting.Text = ""
                pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Session("click_post") = "false"
                Exit Sub
            End Try
        Else
            'update tabel master    
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                'Dim iDigit As Integer = 2
                'If digit.Checked Then
                '    iDigit = 6
                'End If

                sSql = "UPDATE QL_trnjualmst SET trnjualno = '" & Tchar(trnjualno.Text) & _
                    "' , periodacctg= '" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & "', trnjualdate ='" & CDate(toDate(trnjualdate.Text)) & "',trnjualref = '" & Tchar(trnjualres1.Text) & "', trncustoid= " & trncustoid.Text & ", trnpaytype = " & trnpaytype.SelectedValue & ", trnamtjual = " & ToDouble(trnamtjual.Text) & ", trntaxpct = " & ToDouble(trntaxpct.Text) & ", trnamttax = " & ToDouble(trnamttax.Text) & ",trndisctype = '" & trndisctype.SelectedValue & "',trndiscamt = " & ToDouble(trndiscamt.Text) & ", trnamtjualnetto= " & ToDouble(totalinvoice.Text) & ", trnjualnote = '" & Tchar(trnjualnote.Text) & "',trnjualstatus = '" & posting.Text & "', trnjualres1 = '" & Tchar(trnjualres1.Text) & "',amtdischdr = " & ToDouble(amtdischdr.Text) & ",currencyoid = " & currate.SelectedValue & ",currencyrate = " & ToDouble(currencyrate.Text) & ",upduser = '" & Session("UserID") & "',updtime= CURRENT_TIMESTAMP,trncustname = '" & Tchar(trnjualcust.Text) & "'  WHERE cmpcode='" & CompnyCode & "' and trnjualmstoid=" & oid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                ' Reverse SJDetail Status (Invoiced) data lama
                sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus='Approved' WHERE trnsjjualno IN " & _
                    "(SELECT sd.trnsjjualno FROM ql_trnjualmst si inner join ql_trnjualdtl sd on si.trnjualmstoid = sd.trnjualmstoid WHERE si.cmpcode='" & CompnyCode & _
                    "' AND si.trnjualmstoid=" & oid.Text & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                ' Delete detail lama
                sSql = "Delete from QL_trnjualdtl where trnjualmstoid = " & oid.Text
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    'insert tabel detail
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim trnjualdtloid As Int64 = GenerateID("QL_trnjualdtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1

                        'setting satuan 3
                        Dim setupsatuan3 As Decimal
                        'update to last price 
                        Dim konv1_2 As Integer : Dim konv2_3 As Integer

                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        konv1_2 = xCmd.ExecuteScalar

                        sSql = "select konversi2_3 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        konv2_3 = xCmd.ExecuteScalar

                        If objTable.Rows(c1).Item("unitseq") = 1 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv1_2 * konv2_3
                        ElseIf objTable.Rows(c1).Item("unitseq") = 2 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv2_3
                        Else
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty"))
                        End If

                        sSql = "INSERT INTO QL_trnjualdtl (cmpcode,trnjualdtloid,trnjualmstoid,trnjualdtlseq,trnsjjualno,itemoid,trnjualdtlqty, trnjualdtlunitoid,unitseq,trnjualdtlprice,trnjualdtlpriceunitoid,trnjualdtldisctype,trnjualdtldiscqty,trnjualflag, trnjualnote,trnjualstatus,amtjualdisc,updtime,upduser,createuser,amtjualnetto,trnjualdtldisctype2,trnorderdtloid,itemloc,trnjualdtlqty_unit3,trnsjjualdtloid) VALUES ('" & CompnyCode & "'," & trnjualdtloid + c1 & "," & oid.Text & "," & objTable.Rows(c1).Item("trnjualdtlseq") & ",'" & objTable.Rows(c1).Item("salesdeliveryno") & "'," & objTable.Rows(c1).Item("itemoid") & "," & objTable.Rows(c1).Item("salesdeliveryqty") & "," & objTable.Rows(c1).Item("trnjualdtlunitoid") & "," & objTable.Rows(c1).Item("unitseq") & ", " & objTable.Rows(c1).Item("orderprice") & "," & objTable.Rows(c1).Item("trnjualdtlunitoid") & ",'AMT'," & objTable.Rows(c1).Item("amtjualdisc") / objTable.Rows(c1).Item("salesdeliveryqty") & ",'','','In Process'," & Tchar(objTable.Rows(c1).Item("amtjualdisc")) & ",CURRENT_TIMESTAMP,'" & Session("UserID") & "','" & Session("UserID") & "'," & ToDouble(objTable.Rows(c1).Item("amtjualnetto")) & ",'AMT', " & objTable.Rows(c1).Item("trnorderdtloid") & ", " & objTable.Rows(c1).Item("mtrlocoid") & "," & setupsatuan3 & "," & objTable.Rows(c1).Item("salesdeliverydtloid") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()


                        ' Update SJDetail status to Invoiced
                        sSql = "UPDATE ql_trnsjjualmst SET trnsjjualstatus='INVOICED' WHERE trnsjjualmstoid='" & objTable.Rows(c1).Item("salesdeliveryoid") & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + trnjualdtloid) & " where tablename='QL_trnjualdtl' and cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                'if posting
                If posting.Text = "POST" Then
                    Dim totMat As Double = 0
                    'update amount to ql_conmtr->trnsjjualdtl
                    Dim objTable1 As DataTable = Session("TblDtl")

                    For c3 As Int16 = 0 To objTable1.Rows.Count - 1
                        sSql = "update Ql_conmtr set amount=" & ToDouble(objTable1.Rows(c3).Item("salesdeliveryqty")) * (ToDouble(objTable1.Rows(c3).Item("orderprice")) _
                          * ToDouble(currencyrate.Text)) & ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP where formoid=" & objTable1.Rows(c3).Item("sjrefoid") & _
                          " and formname='QL_trnsjjualdtl' and type='PROJECK' and refoid=" & objTable1.Rows(c3).Item("itemoid") & ""
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next

                    Dim cekday As String = GetStrData("select gencode from QL_mstgen where gengroup = 'paytype' and genoid = " & trnpaytype.SelectedValue)
                    Dim days As Double = CInt(cekday.Trim.Replace("CSH", "").Replace("CRD", ""))
                    Dim vConARId As Integer = GenerateID("QL_conar", CompnyCode)
                    Dim dPayDueDate As Date = CDate(toDate(trnjualdate.Text)).AddDays(days)
                    Dim sVarAR As String = GetInterfaceValue("VAR_AR")
                    If sVarAR = "?" Then
                        showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim iAROid As Integer = GetAccountOid(sVarAR, False)

                    ' =============== QL_conar
                    sSql = "INSERT INTO QL_conar(cmpcode,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnarflag,trnartype," & _
                        "trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnarnote," & _
                        "trnarres1,upduser,updtime) VALUES ('" & CompnyCode & "'," & vConARId & ",'QL_trnjualmst'," & oid.Text & ",0," & _
                        trncustoid.Text & ",'" & iAROid & "','" & posting.Text & "','','PIUTANG','" & CDate(toDate(trnjualdate.Text)) & _
                        "','" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & "',0,'1/1/1900','',0,'" & dPayDueDate & "'," & _
                        ToDouble(totalinvoice.Text) & ",0,'SI PROJECT (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "update QL_mstoid set lastoid=" & vConARId & " where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' ===================== POSTING GL
                    Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
                    Dim dvJurnal As DataView = dtJurnal.DefaultView
                    Dim vIDMst As Integer = ClassFunction.GenerateID("QL_trnglmst", CompnyCode)
                    Dim vIDDtl As Integer = ClassFunction.GenerateID("QL_trngldtl", CompnyCode)
                    Dim vSeqDtl As Integer = 1

                    dvJurnal.RowFilter = "seq=1" ' JURNAL SALES INVOICE
                    If dvJurnal.Count > 0 Then
                        ' ============== QL_trnglmst
                        sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime)" & _
                            "VALUES ('" & CompnyCode & "'," & vIDMst & ",'" & CDate(toDate(trnjualdate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & _
                            "','SI PROJECT (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")','POST','" & Now().Date & "'," & _
                            "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        For C2 As Integer = 0 To dvJurnal.Count - 1
                            Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                            If ToDouble(dvJurnal(C2)("debet").ToString) > 0 Then
                                dAmt = ToDouble(dvJurnal(C2)("debet").ToString) : sDBCR = "D"
                            Else
                                dAmt = ToDouble(dvJurnal(C2)("credit").ToString) : sDBCR = "C"
                            End If
                            ' ============== QL_trngldtl
                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glother2,glflag,upduser,updtime,glpostdate) " & _
                                "VALUES ('" & CompnyCode & "'," & vIDDtl & "," & vSeqDtl & "," & vIDMst & "," & dvJurnal(C2)("id").ToString & ",'" & sDBCR & "'," & _
                                "" & dAmt & ",'" & trnjualno.Text & "','SI PROJECT (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")',''," & _
                                "'" & Session("oid") & "','" & posting.Text & "','" & Session("UserID") & "',current_timestamp,current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            vSeqDtl += 1 : vIDDtl += 1
                        Next
                    End If

                    dvJurnal.RowFilter = ""
                    vIDMst += 1 : vSeqDtl = 1

                    'dvJurnal.RowFilter = "seq=2" ' JURNAL BEBAN POKOK PENJUALAN
                    'If dvJurnal.Count > 0 Then
                    '    ' ============== QL_trnglmst
                    '    sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime)" & _
                    '        "VALUES ('" & CompnyCode & "'," & vIDMst & ",'" & CDate(toDate(trnjualdate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & _
                    '        "','SI - BEBAN POKOK PENJUALAN (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")','POST','" & Now().Date & "'," & _
                    '        "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    For C3 As Integer = 0 To dvJurnal.Count - 1
                    '        Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                    '        If ToDouble(dvJurnal(C3)("debet").ToString) > 0 Then
                    '            dAmt = ToDouble(dvJurnal(C3)("debet").ToString) : sDBCR = "D"
                    '        Else
                    '            dAmt = ToDouble(dvJurnal(C3)("credit").ToString) : sDBCR = "C"
                    '        End If
                    '        ' ============== QL_trngldtl
                    '        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glother2,glflag,upduser,updtime) " & _
                    '            "VALUES ('" & CompnyCode & "'," & vIDDtl & "," & vSeqDtl & "," & vIDMst & "," & dvJurnal(C3)("id").ToString & ",'" & sDBCR & "'," & _
                    '            "" & dAmt & ",'" & trnjualno.Text & "','SI - BEBAN POKOK PENJUALAN (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")',''," & _
                    '            "'" & Session("oid") & "','" & posting.Text & "','" & Session("UserID") & "',current_timestamp)"
                    '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '        vSeqDtl += 1 : vIDDtl += 1
                    '    Next
                    'End If

                    'update lastoid ql_trnglmst
                    sSql = "update QL_mstoid set lastoid=" & vIDMst & " where tablename ='QL_trnglmst' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    'update lastoid ql_trngldtl
                    sSql = "update QL_mstoid set lastoid=" & vIDDtl - 1 & " where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close() : posting.Text = ""
                pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Session("click_post") = "false"
                Exit Sub
            End Try
        End If
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        Response.Redirect("~\Transaction\trnnotajualProjeck.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnnotajualProjeck.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If oid.Text.Trim = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data No Faktur Tidak Boleh Kosong!")) 'Please Choose Invoice No
            Exit Sub
        End If

        'without check data in other table
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            '' Delete data biaya lama
            'Dim jualbiayaoid As Integer = cKoneksi.ambilscalar("SELECT jualbiayamstoid FROM ql_trnjualbiayamst WHERE cmpcode='" & _
            '    CompnyCode & "' AND trnjualmstoid=" & oid.Text)
            'sSql = "DELETE ql_trnjualbiayadtl where cmpcode='" & CompnyCode & "' and jualbiayamstoid=" & jualbiayaoid
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()

            'sSql = "DELETE ql_trnjualbiayamst where cmpcode='" & CompnyCode & "' and jualbiayamstoid=" & jualbiayaoid
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()

            ' Reverse SJDetail Status (Invoiced) data lama

            'sSql = "UPDATE ql_trnsalesdelivmst SET salesdeliverystatus='POST' WHERE salesdeliveryoid IN " & _
            '       "(SELECT si.sjrefoid FROM ql_trnjualdtl si WHERE si.cmpcode='" & CompnyCode & _
            '       "' AND si.trnjualmstoid=" & oid.Text & ")"
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus='Approved' WHERE trnsjjualno IN " & _
                 "(SELECT sd.trnsjjualno FROM ql_trnjualmst si inner join ql_trnjualdtl sd on si.trnjualmstoid = sd.trnjualmstoid WHERE si.cmpcode='" & CompnyCode & _
                 "' AND si.trnjualmstoid=" & oid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualdtl where trnjualmstoid=" & oid.Text & " AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from  QL_trnjualmst  where trnjualmstoid=" & oid.Text & " AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, "Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnnotajualProjeck.aspx?awal=true")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If chkPeriod.Checked Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try
            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere)
        Session("SearchSI") = sqlTempSearch
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterPeriod1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        BindData("")
        ddlFilter.SelectedIndex = 0 : txtFilter.Text = ""
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles btnPosting.Click
        If Session("click_post") = "true" Then
            showMessage("Data ini Sudah di click posting !, Tekan Cancel kemudian cek di List Information", CompnyName & " - Warning", 2) 'Please select a PO Number
            btnPosting.Visible = False : btnSave.Visible = False
            btnDelete.Visible = False
            Exit Sub
        Else
            Session("click_post") = "true"
        End If

        Session("click_post") = True
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        dtlTable.Columns.Add("code", Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32")) ' utk seq bila 1 transaksi ada lebih 1 jurnal
        dtlTable.Columns.Add("seqdtl", Type.GetType("System.Int32")) ' utk seq bila 1 account bisa muncul dipakai berkali2
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
        Dim nuRow As DataRow

        ' ============ JURNAL 1
        ' PIUTANG
        ' POTONGAN PENJUALAN
        '       PENJUALAN (salesdeliveryqty*orderprice)
        '       PPN KELUARAN

        ' ============ PIUTANG
        Dim sVarAR As String = GetInterfaceValue("VAR_AR")
        If sVarAR = "?" Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAR, False)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (PIUTANG USAHA)"
            nuRow("debet") = (ToDouble(totalinvoice.Text) * ToDouble(currencyrate.Text))
            nuRow("credit") = 0
            nuRow("id") = iAPOid
            nuRow("seq") = iSeq
            nuRow("seqdtl") = iSeqDtl
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        ' ============ POTONGAN PENJUALAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and jumlah diskon
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString

                'sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                'Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)

                Dim iItemAccount As String = GetInterfaceValue("VAR_DISC_JUAL")
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_DISC_JUAL' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub

                End If

                ' === Calculate Diskon
                'Dim dItemDiscHdr As Double = 0
                'If trndisctype.SelectedValue = "AMT" Then ' AMT
                '    ' (Total Amount Kotor Per Detail / Total Amount Kotor Detail) * Diskon Header
                '    dItemDiscHdr = ((ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("orderprice").ToString)) / ToDouble(trnamtjual.Text)) * ToDouble(trndiscamt.Text)
                'ElseIf trndisctype.SelectedValue = "PCT" Then ' PCT
                '    ' (Diskon Header/100) * (Total Amount Kotor Per Detail)
                '    dItemDiscHdr = (ToDouble(trndiscamt.Text) / 100) * ((ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("orderprice").ToString)))
                'End If

                Dim dItemDiscdtl As Double = ToDouble(dtDtl.Rows(C1)("amtjualdisc").ToString)
                Dim iAPdiscOid As Integer = GetAccountOid(iItemAccount, False)

                If dItemDiscdtl > 0 Then
                    If dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                        Dim oRow As DataRow = dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl)(0)
                        oRow.BeginEdit()
                        oRow("debet") += (dItemDiscdtl * ToDouble(currencyrate.Text))
                        oRow.EndEdit()
                        dtJurnal.AcceptChanges()
                    Else

                        nuRow = dtJurnal.NewRow
                        nuRow("code") = GetCodeAcctg(iAPdiscOid)
                        nuRow("desc") = GetDescAcctg(iAPdiscOid) & " (POTONGAN PENJUALAN)"
                        nuRow("debet") = (dItemDiscdtl * ToDouble(currencyrate.Text))
                        nuRow("credit") = 0
                        nuRow("id") = iAPdiscOid
                        nuRow("seq") = iSeq
                        nuRow("seqdtl") = iSeqDtl
                        dtJurnal.Rows.Add(nuRow)
                    End If
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PENJUALAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and HPP
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString

                'sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                'Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)

                Dim iItemAccount As String = GetInterfaceValue("VAR_SALES")
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_SALES' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If
                Dim iAPGudOid As Integer = GetAccountOid(iItemAccount, False)

                If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("orderprice").ToString) * ToDouble(currencyrate.Text))
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else

                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iAPGudOid)
                    nuRow("desc") = GetDescAcctg(iAPGudOid) & " (PENJUALAN)"
                    nuRow("debet") = 0
                    nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("orderprice").ToString) * ToDouble(currencyrate.Text))
                    nuRow("id") = iAPGudOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PPN KELUARAN
        If ToDouble(trnamttax.Text) > 0 Then
            Dim sPPNK As String = GetInterfaceValue("VAR_PPN_JUAL")
            If sPPNK = "?" Then
                showMessage("Please Setup Interface for PPN Keluaran on Variable 'VAR_PPN_JUAL'", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                Exit Sub
            Else
                Dim iPPNKOid As Integer = GetAccountOid(sPPNK, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(iPPNKOid)
                nuRow("desc") = GetDescAcctg(iPPNKOid) & " (PPN KELUARAN)"
                nuRow("debet") = 0
                nuRow("credit") = (ToDouble(trnamttax.Text) * ToDouble(currencyrate.Text))
                nuRow("id") = iPPNKOid
                nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If

        'iSeq += 1
        ' ============ 1 JURNAL
        ' BEBAN POKOK PENJUALAN
        '       PERSEDIAAN

        ' ============ BEBAN POKOK PENJUALAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET HPPValue and HPPAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString

                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarHPP As String = ""

                sVarHPP = GetInterfaceValue("VAR_HPP")
                If sVarHPP = "?" Then
                    showMessage("Please Setup Interface for HPP on Variable 'VAR_HPP'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarHPP, True)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("debet") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (BEBAN POKOK PENJUALAN(HPP))"
                    nuRow("debet") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    nuRow("credit") = 0
                    nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PERSEDIAAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET PersediaanValue and PersediaanAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString

                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarStock As String = ""

                sVarStock = GetInterfaceValue("VAR_GUDANG")

                If sVarStock = "?" Then
                    showMessage("Please Setup Interface for Stock on Variable 'VAR_GUDANG'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarStock, False)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (PERSEDIAAN)"
                    nuRow("debet") = 0
                    nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal

        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
        'posting.Text = "POST" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub Tbldata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Tbldata.PageIndexChanging
        If chkPeriod.Checked Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try
            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub

            End If
        End If
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"

        Tbldata.PageIndex = e.NewPageIndex
        BindData(sWhere)
        Session("SearchSI") = sqlTempSearch

    End Sub

    Protected Sub Tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate((e.Row.Cells(1).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub currate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(currate.SelectedValue)
    End Sub

    Protected Sub taxtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If taxtype.SelectedIndex = 0 Then
            SetControlTax(False)
            trntaxpct.Text = "0.00"
        ElseIf taxtype.SelectedIndex = 1 Then
            SetControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 3)
        Else
            SetControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 3)
        End If
        ReAmount()
    End Sub

    Private Sub SetControlTax(ByVal state As Boolean)
        lblTaxPct.Visible = state : trntaxpct.Visible = state
        lblTaxAmt.Visible = state : trnamttax.Visible = state
    End Sub

    Protected Sub imbAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLFindSO.SelectedIndex = 0 : txtFindSO.Text = ""
        BindDataListOrder("") : mpeso.Show()
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = " AND " & DDLFindSO.SelectedValue & " LIKE '%" & Tchar(txtFindSO.Text) & "%' "
        BindDataListOrder(sWhere) : mpeso.Show()
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(toDate(e.Row.Cells(2).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub lkbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        binddataSJDetil(sender.ToolTip)
    End Sub

    Protected Sub btnCloseDSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHideDetilDso.Visible = False : PanelDetilDSO.Visible = False : ModalPopupExtenderDetilDSO.Hide()
        ModalPopupExtenderdso.Show()
        cProc.DisposeGridView(tblDetilDso)
    End Sub

    Protected Sub gvSJ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSJ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate((e.Row.Cells(2).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub TblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub TblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dt As DataTable : dt = Session("TblDtl")
        trnjualdtlseq.Text = TblDtl.SelectedDataKey(0).ToString
        Dim objRow() As DataRow = dt.Select("trnjualdtlseq='" & trnjualdtlseq.Text & "'", Nothing, DataViewRowState.CurrentRows)
        'noteSJ.ReadOnly = True : noteSJ.CssClass = "inpTextDisabled"
        sjoid.Text = objRow(0).Item("salesdeliveryoid").ToString
        Sjno.Text = objRow(0).Item("salesdeliveryno").ToString
        'itemoid.Text = objRow(0).Item("itemoid").ToString
        itemoid.Text = objRow(0).Item("trnjualdtlseq").ToString
        itemname.Text = objRow(0).Item("itemlongdesc").ToString
        trnjualdtluom.Text = objRow(0).Item("trnjualdtlunitoid").ToString
        gendesc.Text = objRow(0).Item("gendesc").ToString
        trnjualdtlqty.Text = ToMaskEdit(ToDouble(objRow(0).Item("salesdeliveryqty")), 3)
        trnjualdtlprice.Text = ToMaskEdit(ToDouble(objRow(0).Item("orderprice")), 3)
        amtjualnetto.Text = ToMaskEdit(ToDouble(objRow(0).Item("amtjualnetto")), 3)
        totalqty.Text = ToMaskEdit(ToDouble(trnjualdtlqty.Text), 1)
        trnjualdtlnote.Text = objRow(0).Item("trnjualdtlnote").ToString
        btnSearhDSO.Visible = False : btnClearDSO.Visible = False
        btnAddToList.Visible = True : btnClear.Visible = True
        I_u2.Text = "Update" : TblDtl.Columns(10).Visible = False
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail() : trnjualdtlseq.Text = Session("ItemLine")
        btnSearhDSO.Visible = True : btnClearDSO.Visible = True
        btnAddToList.Visible = False : btnClear.Visible = False
    End Sub

    Protected Sub trnjualdtlprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        amtjualnetto.Text = ToMaskEdit(ToDouble(trnjualdtlqty.Text) * ToDouble(trnjualdtlprice.Text), 3)
    End Sub

    Protected Sub TblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles TblDtl.RowDeleting
        Dim objTable As DataTable
        Dim objRow(), objRow2() As DataRow
        objTable = Session("TblDtl")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objRow2 = objTable.Select("salesdeliveryoid='" & objRow(e.RowIndex).Item("salesdeliveryoid") & "'")
        For C1 As Integer = 0 To objRow2.Length - 1
            objTable.Rows.Remove(objRow2(C1))
        Next
        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C2)
            dr.BeginEdit() : dr(2) = C2 + 1 : dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        TblDtl.DataSource = Session("TblDtl")
        TblDtl.DataBind()
        SumPriceInInvoiceDetail(Session("TblDtl"))
        ReAmount()
        Session("ItemLine") = objTable.Rows.Count + 1
        trnjualdtlseq.Text = Session("ItemLine")
    End Sub

    Protected Sub lbkCost0_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View3)
    End Sub

    Protected Sub lkbCost1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View3)
    End Sub

    Protected Sub lkbInfo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub lkbDetil2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub imbAddBiaya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If ToDouble(itembiayaamt.Text) <= 0 Then
            sMsg &= "Cost Amount must be greater than 0!!<BR>"
        End If
        If itembiayaoid.Items.Count <= 0 Then
            sMsg &= "Please create Expense Account Payable in<BR>Chart of Account!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If Session("DtlBiaya") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetailBiaya() : Session("DtlBiaya") = dtlTable
        End If
        Dim objTable As DataTable : objTable = Session("DtlBiaya")
        'Cek apa sudah ada item biaya yang sama dalam Tabel Detail Biaya
        If i_u3.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "itembiayaoid=" & itembiayaoid.SelectedValue
            If dv.Count > 0 Then
                showMessage("This data has been added before, please check!", CompnyName & " - WARNING", 2)
                dv.RowFilter = "" : Exit Sub
            End If
            dv.RowFilter = ""
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If i_u3.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("jualbiayaseq") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(jualbiayaseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("itembiayaoid") = itembiayaoid.SelectedValue
        objRow("itembiayaamt") = ToDouble(itembiayaamt.Text)
        objRow("itembiayanote") = itembiayanote.Text
        objRow("acctgdesc") = Trim(itembiayaoid.SelectedItem.Text)
        objRow("upduser") = Session("UserID")
        objRow("updtime") = GetServerTime()

        If i_u3.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If

        ClearDetailBiaya()
        Session("DtlBiaya") = objTable
        gvBiaya.DataSource = objTable
        gvBiaya.DataBind()
        SumBiayaDetail(Session("DtlBiaya"))
        ReAmount()

        Session("ItemLineBiaya") = objTable.Rows.Count + 1
        jualbiayaseq.Text = Session("ItemLineBiaya")
        gvBiaya.SelectedIndex = -1
    End Sub

    Protected Sub imbClearBiaya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetailBiaya()
        jualbiayaseq.Text = Session("ItemLineBiaya")
    End Sub

    Protected Sub gvBiaya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dt As DataTable : dt = Session("DtlBiaya")
        jualbiayaseq.Text = gvBiaya.SelectedDataKey(0).ToString
        Dim objRow() As DataRow = dt.Select("jualbiayaseq='" & jualbiayaseq.Text & "'", Nothing, DataViewRowState.CurrentRows)
        itembiayaoid.SelectedValue = objRow(0).Item("itembiayaoid").ToString
        itembiayaamt.Text = ToMaskEdit(ToDouble(objRow(0).Item("itembiayaamt").ToString), 3)
        itembiayanote.Text = objRow(0).Item("itembiayanote").ToString
        i_u3.Text = "Update"
    End Sub

    Protected Sub gvBiaya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBiaya.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvBiaya_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvBiaya.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable : objTable = Session("DtlBiaya")
        objTable.Rows.RemoveAt(iIndex)
        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C2)
            dr.BeginEdit() : dr(0) = C2 + 1 : dr.EndEdit()
        Next
        Session("DtlBiaya") = objTable
        gvBiaya.DataSource = Session("DtlBiaya")
        gvBiaya.DataBind()
        Session("ItemLineBiaya") = objTable.Rows.Count + 1
        jualbiayaseq.Text = Session("ItemLineBiaya")
    End Sub

    Protected Sub trnjualcosttype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub currencyrate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub imbPrintInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print

            'Dim s As Integer = cKoneksi.ambilscalar("SELECT substring(trnjualno,3,1) FROM ql_trnjualmst WHERE cmpcode='" & CompnyCode & "' AND trnjualmstoid=" & sender.ToolTip)
            'If s = 0 Then
            '    report.Load(Server.MapPath("~/report/SalesInvoice.rpt"))
            'Else
            '    report.Load(Server.MapPath("~/report/SalesInvoicenontax.rpt"))
            'End If

            report.Load(Server.MapPath("~/report/rptNotaJual.rpt"))
            report.SetParameterValue("sWhere", "  where m.trnjualmstoid='" & sender.ToolTip & "' ")
            report.SetParameterValue("namaPencetak", Session("UserID"))

            'report.SetParameterValue("cmpcode", CompnyCode)
            'report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PrinterName = printerPOS
            'report.PrintOptions.PaperSize = PaperSize.PaperA5
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))

            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        'Response.Redirect("~\Transaction\trnnotajualProjeck.aspx?awal=true")
    End Sub

    Protected Sub lkbPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        posting.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
    End Sub

    Protected Sub paymentype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLPosting(paymentype.SelectedValue, trnpaytype.SelectedItem.Text.Trim)
        mpePosting.Show()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchSI") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Private Sub BindLastSearch()
        FillGV(Tbldata, Session("SearchSI"), "ql_trnjualmst")
    End Sub

    Protected Sub trnjualdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
      
        If Not IsDate(toDate(trnjualdate.Text)) Then
            showMessage("Invalid Invoice date !!", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text)))
        If Format(CDate(toDate(trnjualdate.Text)), "yy") <> Mid(dbsino.Text, 4, 2) Then
            'generateNo()
        Else
            If dbsino.Text <> "" Then
                trnjualno.Text = dbsino.Text
            End If
        End If
    End Sub

    Protected Sub tblDetilDso_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 1)
           
        End If
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        imbPrintInvoice_Click(sender, e)
    End Sub

    Protected Sub printout_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = oid.Text
        imbPrintInvoice_Click(sender, e)
    End Sub

    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Export.Click
        'Session("showReport") = True
        'showPrintExcel(oid.Text)
        Session("InvoiceOID") = oid.Text
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print

            Dim s As Integer = cKoneksi.ambilscalar("SELECT substring(trnjualno,3,1) FROM ql_trnjualmst WHERE cmpcode='" & CompnyCode & "' AND trnjualmstoid=" & sender.ToolTip)
            If s = 1 Then
                report.Load(Server.MapPath("~/report/SalesInvoice.rpt"))
            Else
                report.Load(Server.MapPath("~/report/SalesInvoicenontax.rpt"))
            End If

            'report.Load(Server.MapPath("~/report/rptSalesInvoice.rpt"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PrinterName = printerPOS
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))

            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try


    End Sub

    Public Sub showPrintExcel(ByVal oid As Integer)

        ' lblkonfirmasi.Text = ""
        Session("diprint") = "False"


        Response.Clear()


       Dim swhere As String = ""
        Dim shaving As String = ""
        Dim swhere2 As String = ""


        ' If dView.SelectedValue = "Master" Then
        sSql = "SELECT d.trnjualdtloid,d.trnjualmstoid,d.trnjualdtlseq,d.sjrefoid,d.itemoid,d.itemgroupcode,d.trnjualdtlqty salesdeliveryqty,d.trnjualdtluom,d.trnjualdtlprice orderprice,d.trnjualnote AS trnjualdtlnote,d.trnjualstatus,d.amtjualnetto,d.upduser,d.updtime,sj.salesdeliveryno,i.itemlongdesc,g.gendesc,sj.salesdeliveryoid FROM QL_trnjualdtl d  INNER JOIN ql_trnsalesdelivmst sj ON d.cmpcode=sj.cmpcode AND d.sjrefoid=sj.salesdeliveryoid INNER JOIN QL_mstitem i ON d.cmpcode=i.cmpcode AND i.itemoid=d.itemoid INNER JOIN ql_mstgen g ON g.cmpcode=d.cmpcode AND d.trnjualdtluom=g.genoid WHERE d.trnjualmstoid=" & oid & " AND d.cmpcode='SIP' ORDER BY d.trnjualdtlseq"
      
        Response.AddHeader("content-disposition", "inline;filename= Status Order Pembelian.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"


        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()


    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print

            If orderflag.Text = "MANUF" Then
                report.Load(Server.MapPath("~/report/FakturPajak.rpt"))
            ElseIf orderflag.Text = "TRADE" Then
                report.Load(Server.MapPath("~/report/FakturPajakTrading.rpt"))
            End If


            'report.Load(Server.MapPath("~/report/rptSalesInvoice.rpt"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PrinterName = printerPOS
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btncust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Panelcust.Visible = True
        Buttoncust.Visible = True
        BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Sub BindDataCust(ByVal sQueryplus As String)
        mysql = "SELECT [CMPCODE], [CUSTOID], [CUSTCODE], [CUSTNAME], [CUSTFLAG] FROM [QL_MSTCUST] WHERE (([CMPCODE] LIKE '%" & CompnyCode & "%') and [CUSTFLAG] = 'Active'  " & sQueryplus & " ) ORDER BY custcode"
        FillGV(gvCustomer, mysql, "QL_MSTCUST")
    End Sub

    Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Panelcust.Visible = False : Buttoncust.Visible = False
        ModalPopupExtender1.Hide() : cProc.DisposeGridView(gvCustomer)
    End Sub

    Protected Sub imbFindcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sTempSql As String = " AND " & DDLCustID.SelectedValue & " LIKE '%" & Tchar(txtFindCustID.Text) & "%' "
        BindDataCust(sTempSql)
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub imbAllcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Panelcust.Visible = True
        Buttoncust.Visible = True
        BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.Visible = True
        Dim sTempSql As String = " AND " & DDLcustid.SelectedValue & " LIKE '%" & Tchar(txtFindCustID.Text) & "%' "
        BindDataCust(sTempSql)
        'BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCustomer.SelectedDataKey(0).ToString
        trnjualcust.Text = gvCustomer.SelectedDataKey(1).ToString
        Panelcust.Visible = False
        Buttoncust.Visible = False
        ModalPopupExtender1.Hide()
        txtFindCustID.Text = ""
        cProc.DisposeGridView(sender)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub lkbPostPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        generateNoPosting()
        posting.Text = "POST" : btnSave_Click(Nothing, Nothing)
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Protected Sub lkbClosePreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Sub FillDDLAccounting(ByVal oDDLAccount As DropDownList, ByVal sFilterCode As String)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(oDDLAccount, sSql)
    End Sub

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' "
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Protected Sub gvPreview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPreview.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub postingfaktur_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        sSql = "UPDATE ql_trnjualmst SET nofakturpajak='" & Tchar(txtnofakturpajak.Text) & "', statusfakturpajak='POST', userfakturpajak='" & Session("UserID") & "' WHERE trnjualmstoid=" & oid.Text
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        objTrans.Commit() : xCmd.Connection.Close()
        Response.Redirect("~\Transaction\trnnotajualProjeck.aspx?awal=true")
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print

            If orderflag.Text = "MANUF" Then
                report.Load(Server.MapPath("~/report/FakturPajak.rpt"))
            ElseIf orderflag.Text = "TRADE" Then
                report.Load(Server.MapPath("~/report/FakturPajakTrading.rpt"))
            End If


            'report.Load(Server.MapPath("~/report/rptSalesInvoice.rpt"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PrinterName = printerPOS
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub trndiscamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndiscamt.TextChanged
        ReAmount()
    End Sub
End Class
