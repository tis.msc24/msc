Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Globalization

Partial Class ReportForm_rptsuratjalan
    Inherits System.Web.UI.Page


#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As New ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub clearform()
        ddlfilter.SelectedIndex = 0
        tbperiodstart.Text = Date.Now.ToString("01" & "/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        ddlfilterstatus.SelectedIndex = 0
        crv.ReportSource = Nothing
    End Sub

    Private Sub showreport()
        Dim sWhere As String = ""
        sWhere &= "and a.branch_code = '" & DDLcabang.SelectedValue & "'"
        If tbfilter.Text <> "" Then
            sWhere &= " AND " & ddlfilter.SelectedValue.ToString & " LIKE '%" & Tchar(tbfilter.Text.Trim) & "%'"
        End If
        'If cbperiod.Checked = True Then
        If tbperiodstart.Text.Replace("/", "").Trim = "" Or tbperiodend.Text.Replace("/", "").Trim = "" Then
            showMessage("Periode awal atau akhir tidak boleh kosong!", 2)
            Exit Sub
        End If

        Dim datestart As String = tbperiodstart.Text.Trim
        Dim dateend As String = tbperiodend.Text.Trim
        Dim sdate, edate As New Date
        If Date.TryParseExact(datestart, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, sdate) And Date.TryParseExact(dateend, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, edate) Then
            Dim syear As Integer = Integer.Parse(sdate.Date.Year)
            Dim eyear As Integer = Integer.Parse(edate.Date.Year)
            If (syear < 1900 Or syear > 9999) Or (eyear < 1900 Or eyear > 9999) Then
                showMessage("Tahun periode harus lebih besar dari 1900 dan lebih kecil dari 9999!", 2)
                Exit Sub
            Else
                If edate >= sdate Then
                    sWhere &= " AND CONVERT(date, a.sjldate, 103) BETWEEN '" & sdate & "' AND '" & edate & "'"
                Else
                    showMessage("Periode akhir harus lebih besar dari periode awal!", 2)
                    Exit Sub
                End If
            End If
        Else
            showMessage("Periode tidak valid!", 2)
            Exit Sub
        End If
        'End If

        If cbstatus.Checked = True And ddlfilterstatus.SelectedValue <> "All" Then
            'If ddlfilterstatus.SelectedValue = "inprocess" Then
            '    sWhere &= " AND LOWER(a.sjlstatus) = 'in process'"
            'ElseIf ddlfilterstatus.SelectedValue = "send" Then
            '    sWhere &= " AND LOWER(a.sjlstatus) = 'send'"
            'End If
            sWhere &= " and lower(a.sjlstatus)='" & ddlfilterstatus.SelectedValue & "'"
        End If

        Dim sOrder As String = " ORDER BY a.sjlno"

        Try

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\rptsuratjalanreport.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", sWhere & sOrder)
            rpt.SetParameterValue("reportName", rpt.FileName.Substring(rpt.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            'If cbperiod.Checked = True Then
            rpt.SetParameterValue("dPeriode", tbperiodstart.Text & " - " & tbperiodend.Text)
            'Else
            'rpt.SetParameterValue("sPeriod", "N/A")
            'rpt.SetParameterValue("ePeriod", "N/A")
            'End If
            crv.DisplayGroupTree = False
            crv.ReportSource = rpt
            Session("report") = rpt
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub printouttopdf()
        Dim sWhere As String = ""

        sWhere &= " AND " & ddlfilter.SelectedValue.ToString & " LIKE '%" & Tchar(tbfilter.Text.Trim) & "%'"

        'If cbperiod.Checked = True Then
        If tbperiodstart.Text.Replace("/", "").Trim = "" Or tbperiodend.Text.Replace("/", "").Trim = "" Then
            showMessage("Periode awal atau akhir tidak boleh kosong!", 2)
            Exit Sub
        End If

        Dim datestart As String = tbperiodstart.Text.Trim
        Dim dateend As String = tbperiodend.Text.Trim
        Dim sdate, edate As New Date
        If Date.TryParseExact(datestart, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, sdate) And Date.TryParseExact(dateend, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, edate) Then
            Dim syear As Integer = Integer.Parse(sdate.Date.Year)
            Dim eyear As Integer = Integer.Parse(edate.Date.Year)
            If (syear < 1900 Or syear > 9999) Or (eyear < 1900 Or eyear > 9999) Then
                showMessage("Tahun periode harus lebih besar dari 1900 dan lebih kecil dari 9999!", 2)
                Exit Sub
            Else
                If edate >= sdate Then
                    sWhere &= " AND CONVERT(date, a.sjldate, 103) BETWEEN '" & sdate & "' AND '" & edate & "'"
                Else
                    showMessage("Periode akhir harus lebih besar dari periode awal!", 2)
                    Exit Sub
                End If
            End If
        Else
            showMessage("Periode tidak valid!", 2)
            Exit Sub
        End If
        'End If

        If cbstatus.Checked = True Then
            If ddlfilterstatus.SelectedValue = "inprocess" Then
                sWhere &= " AND LOWER(a.sjlstatus) = 'in process'"
            ElseIf ddlfilterstatus.SelectedValue = "send" Then
                sWhere &= " AND LOWER(a.sjlstatus) = 'send'"
            End If
        End If

        Dim sOrder As String = " ORDER BY a.sjlno"

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT COUNT(a.sjlno) FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid INNER JOIN ql_trnsjalandtl c ON a.sjloid = c.sjloid INNER JOIN ql_mstgen d ON c.reqitemtype = d.genoid INNER JOIN ql_mstgen e ON c.reqitembrand = e.genoid"

            xCmd.CommandText = sSql & sWhere
            Dim res As Integer = xCmd.ExecuteScalar
            If res <= 0 Then
                showMessage("Data tidak ditemukan!", 3)
                Exit Sub
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\rptsuratjalanreport.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", sWhere & sOrder)
            'If cbperiod.Checked = True Then
            rpt.SetParameterValue("dPeriode", tbperiodstart.Text & " - " & tbperiodend.Text)
            'Else
            'rpt.SetParameterValue("sPeriod", "N/A")
            'rpt.SetParameterValue("ePeriod", "N/A")
            'End If
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Report_SJ_" & DateTime.Now.ToString("ddMMyyHHmmss"))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Private Sub printouttoexcel()
        Dim sWhere As String = ""

        sWhere &= " AND " & ddlfilter.SelectedValue.ToString & " LIKE '%" & Tchar(tbfilter.Text.Trim) & "%'"

        'If cbperiod.Checked = True Then
        If tbperiodstart.Text.Replace("/", "").Trim = "" Or tbperiodend.Text.Replace("/", "").Trim = "" Then
            showMessage("Periode awal atau akhir tidak boleh kosong!", 2)
            Exit Sub
        End If

        Dim datestart As String = tbperiodstart.Text.Trim
        Dim dateend As String = tbperiodend.Text.Trim
        Dim sdate, edate As New Date
        If Date.TryParseExact(datestart, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, sdate) And Date.TryParseExact(dateend, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, edate) Then
            Dim syear As Integer = Integer.Parse(sdate.Date.Year)
            Dim eyear As Integer = Integer.Parse(edate.Date.Year)
            If (syear < 1900 Or syear > 9999) Or (eyear < 1900 Or eyear > 9999) Then
                showMessage("Tahun periode harus lebih besar dari 1900 dan lebih kecil dari 9999!", 2)
                Exit Sub
            Else
                If edate >= sdate Then
                    sWhere &= " AND CONVERT(date, a.sjldate, 103) BETWEEN '" & sdate & "' AND '" & edate & "'"
                Else
                    showMessage("Periode akhir harus lebih besar dari periode awal!", 2)
                    Exit Sub
                End If
            End If
        Else
            showMessage("Periode tidak valid!", 2)
            Exit Sub
        End If
        'End If

        If cbstatus.Checked = True Then
            If ddlfilterstatus.SelectedValue = "inprocess" Then
                sWhere &= " AND LOWER(a.sjlstatus) = 'in process'"
            ElseIf ddlfilterstatus.SelectedValue = "send" Then
                sWhere &= " AND LOWER(a.sjlstatus) = 'send'"
            End If
        End If

        Dim sOrder As String = " ORDER BY a.sjlno"

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT COUNT(a.sjlno) FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid INNER JOIN ql_trnsjalandtl c ON a.sjloid = c.sjloid INNER JOIN ql_mstgen d ON c.reqitemtype = d.genoid INNER JOIN ql_mstgen e ON c.reqitembrand = e.genoid"

            xCmd.CommandText = sSql & sWhere
            Dim res As Integer = xCmd.ExecuteScalar
            If res <= 0 Then
                showMessage("Data tidak ditemukan!", 3)
                Exit Sub
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\rptsuratjalanreport.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", sWhere & sOrder)
            'If cbperiod.Checked = True Then
            rpt.SetParameterValue("dPeriode", tbperiodstart.Text & " - " & tbperiodend.Text)
            'Else
            'rpt.SetParameterValue("sPeriod", "N/A")
            'rpt.SetParameterValue("ePeriod", "N/A")
            'End If
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Report_SJ_" & DateTime.Now.ToString("ddMMyyHHmmss"))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/rptservicefinal.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        FillDDL(DDLcabang, sSql)
    End Sub
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        Session.Timeout = 60

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Reportform\rptsuratjalan.aspx")
        End If

        'If checkPagePermission("~\Transaction\trnsuratjalan.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        Page.Title = CompnyName & " - Laporan Surat Jalan"

        If Not Page.IsPostBack Then
            clearform()
            Session("report") = Nothing
            Cabangddl()
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Reportform\rptsuratjalan.aspx?awal=true")
    End Sub

    Protected Sub ibreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibreport.Click
        showreport()
    End Sub

    Protected Sub ibtopdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtopdf.Click
        printouttopdf()
    End Sub

    Protected Sub ibtoexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoexcel.Click
        printouttoexcel()
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        'crv.ReportSource = Session("report")
        showreport()
    End Sub
		
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not rpt Is Nothing Then
                If rpt.IsLoaded Then
                    rpt.Dispose()
                    rpt.Close()
                End If
            End If
        Catch ex As Exception
            rpt.Dispose()
            rpt.Close()
        End Try
    End Sub
#End Region


End Class
