<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWhReject.aspx.vb" Inherits="trnWhSupplier" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Transfer Retur" CssClass="Title" ForeColor="Maroon" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Retur :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 533px"><TBODY><TR><TD class="Label" align=left>Transfer No </TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" MaxLength="30"></asp:TextBox></TD></TR><TR><TD id="TD7" class="Label" align=left runat="server" Visible="false">Group </TD><TD id="TD9" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD5" align=left colSpan=4 runat="server" Visible="false"><asp:DropDownList id="group" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w35"></asp:DropDownList></TD></TR><TR><TD id="TD11" class="Label" align=left runat="server" Visible="false">Sub Group </TD><TD id="TD12" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD8" align=left colSpan=4 runat="server" Visible="false"><asp:DropDownList id="subgroup" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w17"></asp:DropDownList></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false">Item/Barang</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD1" align=left colSpan=4 runat="server" Visible="false"><asp:TextBox id="Filteritem" runat="server" Width="219px" CssClass="inpText" MaxLength="30" __designer:wfdid="w13"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="17px" __designer:wfdid="w14"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w15"></asp:ImageButton>&nbsp;<asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD2" class="Label" align=left runat="server" Visible="false"></TD><TD align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVItemSearch" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="itemoid" __designer:wfdid="w12">
<RowStyle BackColor="#F7F7DE"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Description">
<HeaderStyle HorizontalAlign="Left" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle Wrap="True" BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Tanggal</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w22"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w23"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w24"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton>&nbsp;&nbsp; <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w26"></asp:Label>&nbsp;&nbsp;</TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" align=left></TD><TD align=left colSpan=4><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w20">
                                                            </asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w21">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=4><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="tgl1" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w31"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="tgl2" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w32"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="tgl1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy" __designer:wfdid="w33"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" TargetControlID="tgl2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy" __designer:wfdid="w34"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE> <asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" GridLines="None" __designer:wfdid="w37">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnTrfFromReturoid" DataNavigateUrlFormatString="trnWhReject.aspx?oid={0}" DataTextField="trnTrfFromReturNo" HeaderText="Transfer No">
<ControlStyle ForeColor="Red"></ControlStyle>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle ForeColor="Red"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Username">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" ToolTip='<%# eval("trnTrfFromReturoid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("trnTrfFromReturoid") %>'></asp:Label>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Transfer No.</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" MaxLength="20" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpText" MaxLength="20" Visible="False" __designer:wfdid="w1">IN PROCESS</asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" Enabled="False" ValidationGroup="MKE"></asp:TextBox>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp;<asp:DropDownList id="FromBranch" runat="server" Width="250px" CssClass="inpText" Visible="False" __designer:wfdid="w3" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>To Branch</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="ToBranch" runat="server" Width="250px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w2" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left><asp:Label id="labelfromloc" runat="server" Width="75px" Text="From Location" Visible="False"></asp:Label><asp:Label id="labelnotr" runat="server" Width="75px" Text="No. Ref (TW)"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="fromlocation" runat="server" Width="250px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" MaxLength="200"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="ibposearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoerase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px"></asp:ImageButton> <asp:Label id="pooid" runat="server" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>To Location</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="tolocation" runat="server" Width="250px" CssClass="inpTextDisabled" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVpo" runat="server" Width="100%" ForeColor="#333333" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="FromMtrlocOid,fromMtrBranch" EmptyDataRowStyle-ForeColor="Red" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="False" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnTrfToReturNo" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="note" runat="server" Width="505px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:RadioButtonList id="rblflag" runat="server" Visible="False" AutoPostBack="True" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="Purchase">Purchase Retur</asp:ListItem>
<asp:ListItem Value="Supplier">Ambil Barang di WH Supplier</asp:ListItem>
</asp:RadioButtonList></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=3><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Usage Detail :" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" Visible="False" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3,sisaretur,unitoid,unit,unit3" EmptyDataRowStyle-ForeColor="Red" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="False" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock Gudang" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit Gudang" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sisaretur" DataFormatString="{0:#,##0.00}" HeaderText="Sisa Retur">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit Retur">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Merk</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" MaxLength="200" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="150px" CssClass="inpText" MaxLength="10" ValidationGroup="MKE" AutoPostBack="True"></asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="Max :"></asp:Label>&nbsp;&nbsp;<asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red">0.00</asp:Label>&nbsp; <asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="labelkonversi1_2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelkonversi2_3" runat="server" Visible="False"></asp:Label> <asp:Label id="labelunit1" runat="server" Visible="False"></asp:Label> <asp:Label id="labelunit2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelunit3" runat="server" Visible="False"></asp:Label> <asp:Label id="labeltempsisaretur" runat="server" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:Label id="labelsatuan1" runat="server" Visible="False"></asp:Label> <asp:Label id="labelsatuan2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelsatuan3" runat="server" Visible="False"></asp:Label> <asp:Label id="labelsatuan" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left><asp:Label id="labelseq" runat="server" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,sisaretur" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" runat="server" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Retur :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg"  runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender>
<span style="display:none"><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button></span>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

