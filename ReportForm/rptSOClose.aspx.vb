Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSOClose
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim CKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

    Private Sub fCabangInit()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = ""
            Dim namapdf As String = ""

            If ddltype.SelectedValue = "Summary" Then
                vReport.Load(Server.MapPath("~\Report\rptSOCloseSum.rpt"))

                sWhere = " Where pom.updtime between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
                If sono.Text.Trim <> "" Then
                    sWhere &= " and pom.orderno like '%" & Tchar(sono.Text) & "%'"
                End If

                If custname.Text.Trim <> "" Then
                    sWhere &= " and supp.custname like '%" & Tchar(custname.Text) & "%'"
                End If

                If fCabang.SelectedValue <> "ALL" Then
                    sSql &= " AND pom.branch_code='" & fCabang.SelectedValue & "'"
                End If

                If barang.Text.Trim <> "" Then
                    sWhere &= "and exists (select x.itemoid from QL_trnorderdtl x inner join ql_mstitem y on x.itemoid=y.itemoid where y.itemdesc like '%" & Tchar(barang.Text) & "%' and y.merk like '%" & Tchar(barang.Text) & "%' and x.trnordermstoid=pom.ordermstoid)"
                End If

                namapdf = "SO_Close_Sum_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptSOCloseDtl.rpt"))

                sWhere = " Where pom.updtime between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
                If sono.Text.Trim <> "" Then
                    sWhere &= " and pom.orderno like '%" & Tchar(sono.Text) & "%'"
                End If
                If custname.Text.Trim <> "" Then
                    sWhere &= " and supp.custname like '%" & Tchar(custname.Text) & "%'"
                End If
                If barang.Text.Trim <> "" Then
                    sWhere &= "and exists (select x.itemoid from QL_trnorderdtl x inner join ql_mstitem y on x.itemoid=y.itemoid where y.itemdesc like '%" & Tchar(barang.Text) & "%' and y.merk like '%" & Tchar(barang.Text) & "%' and x.trnordermstoid=pom.ordermstoid)"
                End If

                namapdf = "SO_Close_Dtl_"
            End If

            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & "-" & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namapdf & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
            showMessage(ex.ToString, CompnyName, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        ' '' jangan lupa cek hak akses
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            Response.Redirect("~/ReportForm/rptSOClose.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyCode & " - Laporan SO Close"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        fCabangInit()

        If Not IsPostBack Then
            range1.Text = Format(GetServerTime(), "01/MM/yyyy")
            range2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        PanelErrMsg.Visible = False
        btnExtender.Visible = False
        MPEErrMsg.Hide()
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If

        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If

        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        'Session("showprint") = "True"
        showPrint("")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "Please fill period 1 value!"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Please fill period 2 value!"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Period 1 is invalid."
            Exit Sub
        End If
        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Period 2 is invalid."
            Exit Sub
        End If
        If date2 < date1 Then
            Label2.Text = "Period 2 must be more than Period 1 !"
            Exit Sub
        End If
        Session("showprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptSOClose.aspx?awal=true")
    End Sub

    Protected Sub searchso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchso.Click
        Try
            If range1.Text.Trim = "" Then
                Label2.Text = "Please fill period 1 value!"
                Exit Sub
            End If

            If range2.Text.Trim = "" Then
                Label2.Text = "Please fill period 2 value!"
                Exit Sub
            End If

            Dim date1, date2 As New Date
            If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
                Label2.Text = "Period 1 is invalid."
                Exit Sub
            End If

            If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
                Label2.Text = "Period 2 is invalid."
                Exit Sub
            End If

            If date2 < date1 Then
                Label2.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If

            sSql = "Select a.ordermstoid,a.orderno,a.trnorderdate,b.custoid,b.custname from ql_trnordermst a inner join ql_mstcust b on a.trncustoid=b.custoid where a.cmpcode='" & CompnyCode & "' and a.updtime between '" & date1 & "' and '" & date2 & "' and a.orderno like '%" & TcharNoTrim(sono.Text) & "%' and trnorderstatus = 'Closed'"
            If fCabang.SelectedValue <> "ALL" Then
                sSql &= " AND a.branch_code='" & fCabang.SelectedValue & "'"
            End If

            sSql &= " Order By a.orderno"
            Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVSo")
            GVSo.DataSource = dtab
            GVSo.DataBind()
            GVSo.Visible = True
            Session("GVSo") = dtab

            GVCustomer.Visible = False
            GVBarang.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub eraseso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eraseso.Click
        sono.Text = "" : sooid.Text = ""
        GVSo.DataSource = Nothing
        GVSo.DataBind()
        GVSo.Visible = False
        Session("GVSo") = Nothing
    End Sub

    Protected Sub GVSo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSo.PageIndexChanging
        If Not Session("GVSo") Is Nothing Then
            GVSo.DataSource = Session("GVSo")
            GVSo.PageIndex = e.NewPageIndex
            GVSo.DataBind()
        End If
    End Sub

    Protected Sub GVSo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSo.SelectedIndexChanged
        sono.Text = GVSo.SelectedDataKey("orderno")
        sooid.Text = GVSo.SelectedDataKey("ordermstoid")
        custname.Text = GVSo.SelectedDataKey("custname")
        custoid.Text = GVSo.SelectedDataKey("custoid")

        GVSo.DataSource = Nothing
        GVSo.DataBind()
        GVSo.Visible = False
        Session("GVSo") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

    Protected Sub searchcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles searchcust.Click
        sSql = "select custoid,custcode,custname from QL_mstcust Where custflag='Active' and cmpcode='" & CompnyCode & "' and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%') And custoid IN (Select Distinct custoid from QL_trnordermst Where orderno LIKE '%" & Tchar(sono.Text.Trim) & "%' And trnorderstatus='Closed')"

        If fCabang.SelectedValue <> "ALL" Then
            sSql &= " AND branch_code='" & fCabang.SelectedValue & "'"
        End If
        sSql &= " Order By custcode"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVCustomer")
        GVCustomer.DataSource = dtab
        GVCustomer.DataBind()
        GVCustomer.Visible = True : Session("GVCustomer") = dtab
        GVSo.Visible = False : GVBarang.Visible = False
    End Sub

    Protected Sub erasecust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles erasecust.Click
        custname.Text = "" : custoid.Text = ""

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        GVCustomer.Visible = False
        Session("GVCustomer") = Nothing
    End Sub

    Protected Sub GVCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVCustomer.PageIndexChanging
        If Not Session("GVCustomer") Is Nothing Then
            GVCustomer.DataSource = Session("GVCustomer")
            GVCustomer.PageIndex = e.NewPageIndex
            GVCustomer.DataBind()
        End If
    End Sub

    Protected Sub GVCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCustomer.SelectedIndexChanged
        custname.Text = GVCustomer.SelectedDataKey("custname")
        custoid.Text = GVCustomer.SelectedDataKey("custoid")

        GVCustomer.DataSource = Nothing
        GVCustomer.DataBind()
        GVCustomer.Visible = False
        Session("GVCustomer") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        sSql = "select itemoid,itemcode,itemdesc,Merk from QL_mstitem where itemflag='Aktif' and cmpcode='" & CompnyCode & "' and (itemcode like '%" & Tchar(barang.Text) & "%' or itemdesc like '%" & Tchar(barang.Text) & "%' or Merk like '%" & Tchar(barang.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVBarang")
        GVBarang.DataSource = dtab
        GVBarang.DataBind()
        GVBarang.Visible = True
        Session("GVBarang") = dtab

        GVSo.Visible = False
        GVCustomer.Visible = False
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = "" : barangoid.Text = "" : merk.Text = ""

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
    End Sub

    Protected Sub GVBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVBarang.PageIndexChanging
        If Not Session("GVBarang") Is Nothing Then
            GVBarang.DataSource = Session("GVBarang")
            GVBarang.PageIndex = e.NewPageIndex
            GVBarang.DataBind()
        End If
    End Sub

    Protected Sub GVBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVBarang.SelectedIndexChanged
        barang.Text = GVBarang.SelectedDataKey("itemdesc")
        barangoid.Text = GVBarang.SelectedDataKey("itemoid")
        merk.Text = GVBarang.SelectedDataKey("Merk")

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub
End Class
