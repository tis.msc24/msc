<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstprofile.aspx.vb" Inherits="Master_mstprofile" title="PT. SUMBER PLASTIK - Master Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
  <%--<script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=GVmstprofile.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    
    }
    window.onload = s
  </script>--%>
                <table id="Table2" bgcolor="white" border="0" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
                    <tr>
                        <td align="left" class="header" valign="center" style="background-color: gainsboro">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Data Profile"></asp:Label>
                        </td>
                    </tr>
                </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
<ContentTemplate>
    <asp:UpdatePanel id="UpdatePanelSearch" runat="server">
        <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w12" DefaultButton="btnSearch"><TABLE style="HEIGHT: 276px" width="100%"><TBODY><TR><TD class="Label" align=left>Filter :</TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w13"><asp:ListItem Value="userid">User ID</asp:ListItem>
<asp:ListItem Value="username">User Name</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="122px" CssClass="inpText" __designer:wfdid="w14" MaxLength="30"></asp:TextBox> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w15"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w16"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w17"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbLevelUser" runat="server" Width="84px" CssClass="Label" Text="Level User" __designer:wfdid="w22"></asp:CheckBox></TD><TD align=left colSpan=5><asp:DropDownList id="fLevelUser" runat="server" CssClass="inpText" __designer:wfdid="w21" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="4">OPERATOR</asp:ListItem>
<asp:ListItem Value="3">SUPERVISOR</asp:ListItem>
<asp:ListItem Value="2">MANAGER</asp:ListItem>
<asp:ListItem Value="1">ADMIN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:CheckBox id="chkGroup" runat="server" CssClass="Label" Text="Status" __designer:wfdid="w18"></asp:CheckBox> </TD><TD align=left colSpan=5><asp:DropDownList id="FilterGroup" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w19"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Inactive</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 243px"><asp:GridView id="GVmstprofile" runat="server" Width="99%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w20" GridLines="None" CellPadding="4" EmptyDataText="No data in database." AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="userid" DataNavigateUrlFormatString="~\master\mstprofile.aspx?oid={0}" DataTextField="userid" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="username" HeaderText="User Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="approvallimit" HeaderText="Approval Limit" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="35%"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="35%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="userlevel" HeaderText="Level User">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statusprof" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !" __designer:wfdid="w71"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    &nbsp;
                                 
</ContentTemplate>
<HeaderTemplate>
<IMG alt="" src="../Images/corner.gif" align=absMiddle />
    <SPAN style="FONT-SIZE: 9pt"><STRONG> List of&nbsp;Profile</STRONG></SPAN> <strong><span
        style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
<ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 178px" class="Label" align=left></TD><TD align=left>&nbsp;&nbsp;</TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>User ID&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w94"></asp:Label></TD><TD align=left><asp:TextBox id="userid" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w95" MaxLength="10"></asp:TextBox> <asp:Label id="personoid" runat="server" Text="0" __designer:wfdid="w96"></asp:Label> <asp:Label id="useridOld" runat="server" __designer:wfdid="w97" Visible="False"></asp:Label> <asp:Label id="usertemp" runat="server" __designer:wfdid="w98" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>User Name</TD><TD align=left><asp:TextBox id="username" runat="server" Width="224px" CssClass="inpTextDisabled" __designer:wfdid="w99" MaxLength="50" Enabled="False"></asp:TextBox> <asp:Button id="btnSales" onclick="btnSales_Click" runat="server" Text="Get Data" __designer:wfdid="w100" BorderColor="Maroon" BorderWidth="1px"></asp:Button>&nbsp;<asp:ImageButton id="btnClearPerson" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w101" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Password <asp:Label id="Label1" runat="server" CssClass="Important" Text="*" __designer:wfdid="w102"></asp:Label>&nbsp;</TD><TD align=left><asp:TextBox id="profpass" runat="server" Width="150px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w103" MaxLength="20" TextMode="MultiLine"></asp:TextBox> <ajaxToolkit:PasswordStrength id="psPwd" runat="server" __designer:wfdid="w104" HelpHandlePosition="RightSide" PreferredPasswordLength="8" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent" TargetControlID="profpass"></ajaxToolkit:PasswordStrength> </TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left></TD><TD align=left><asp:Label id="lblHelp" runat="server" CssClass="Important" Font-Size="X-Small" __designer:wfdid="w105"></asp:Label> <asp:Label id="LblPaswd" runat="server" __designer:wfdid="w106" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Status</TD><TD align=left><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w107"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Inactive</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Cabang</TD><TD align=left><asp:DropDownList id="DDLCbg" runat="server" CssClass="inpText" __designer:wfdid="w108"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>User Approval<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w109" Visible="False"></asp:Label></TD><TD align=left><asp:CheckBox id="DevApproval" runat="server" Text="Approval" __designer:wfdid="w110"></asp:CheckBox>&nbsp;<asp:CheckBox id="CbTypePO" runat="server" Width="142px" Text="Approval PO Selisih" __designer:wfdid="w139"></asp:CheckBox> <asp:Label id="devisitemp" runat="server" __designer:wfdid="w111" Visible="False"></asp:Label> <asp:Label id="jobpos" runat="server" __designer:wfdid="w112" Visible="False"></asp:Label><BR /><asp:TextBox id="approvallimit" runat="server" Width="104px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w113" MaxLength="20" Visible="False"></asp:TextBox> <ajaxToolkit:MaskedEditExtender id="meeApproval" runat="server" __designer:wfdid="w114" TargetControlID="approvallimit" InputDirection="RightToLeft" Mask="9999999999" MaskType="Number"></ajaxToolkit:MaskedEditExtender> <asp:Label id="PoType" runat="server" __designer:wfdid="w111" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Credit Limit Approval</TD><TD align=left><asp:CheckBox id="crApp" runat="server" Text="Credit Limit Approval" __designer:wfdid="w115"></asp:CheckBox> <asp:Label id="divisiCR" runat="server" __designer:wfdid="w116" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Level User</TD><TD align=left><asp:DropDownList id="LevelUserDDL" runat="server" CssClass="inpText" __designer:wfdid="w117"><asp:ListItem Value="4">OPERATOR</asp:ListItem>
<asp:ListItem Value="3">SUPERVISOR</asp:ListItem>
<asp:ListItem Value="2">MANAGER</asp:ListItem>
<asp:ListItem Value="1">ADMIN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left>Spesial Akses</TD><TD align=left><asp:CheckBox id="sUser" runat="server" Text="Spesial Sales" __designer:wfdid="w118"></asp:CheckBox></TD></TR><TR><TD style="WIDTH: 178px" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New" __designer:wfdid="w119" Visible="False"></asp:Label></TD><TD align=left></TD></TR><TR><TD align=left colSpan=2><asp:Panel id="PanelUpdate" runat="server" Width="100%" __designer:wfdid="w120">Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[Updtime]" __designer:wfdid="w121"></asp:Label>&nbsp;by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[upduser]" __designer:wfdid="w122"></asp:Label></asp:Panel></TD></TR><TR><TD align=left colSpan=2><asp:ImageButton id="btnSave" tabIndex=39 runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w123"></asp:ImageButton> <asp:ImageButton id="BtnCancel" tabIndex=40 runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w124"></asp:ImageButton> <asp:ImageButton id="BtnDelete" tabIndex=41 runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w125" CommandName="Delete"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w126" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w127"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel> <BR /><asp:UpdatePanel id="UpdatePanelPerson" runat="server"><ContentTemplate>
<asp:Panel id="PanelPerson" runat="server" Width="700px" CssClass="modalBox" DefaultButton="btnFindPerson" __designer:wfdid="w129" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabePerson" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Person" __designer:wfdid="w130"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left>Filter : <asp:DropDownList id="DDLFilterPerson" runat="server" CssClass="inpText" __designer:wfdid="w131"><asp:ListItem Value="personname">Person Name</asp:ListItem>
<asp:ListItem Value="personnip">N.I.P</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterPerson" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w132"></asp:TextBox> <asp:ImageButton id="btnFindPerson" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w133"></asp:ImageButton> <asp:ImageButton id="btnAllPerson" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w134"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="GVPerson" runat="server" Width="97%" Height="31px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w135" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4" GridLines="None" DataKeyNames="nip,personname,personoid,divisi">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nip" HeaderText="NIP / User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Person Name">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divisi" HeaderText="Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Person on List"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="LinkButton4" runat="server" __designer:wfdid="w136">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="ButtonPerson" runat="server" __designer:wfdid="w137" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="MPEPerson" runat="server" __designer:wfdid="w138" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelPerson" PopupDragHandleControlID="LabelPerson" TargetControlID="ButtonPerson">
                </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
<HeaderTemplate>
<IMG alt="" src="../Images/corner.gif" align=absMiddle />
    <SPAN style="FONT-SIZE: 9pt"><STRONG> Form Profile</STRONG></SPAN>&nbsp; <strong><span
        style="font-size: 9pt">:.</span></strong>
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" PopupDragHandleControlID="Validasi" PopupControlID="Panelvalidasi" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

