<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnConAR.aspx.vb" Inherits="Accounting_trnConAR" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <script type="text/javascript">
    function s()
    {
     
    try {
        var t = document.getElementById("<%=gvSupplier.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        div1.appendChild(t2)
        }
    catch(errorInfo) {}
    try {
        var t = document.getElementById("<%=gvAcc.ClientID%>");
        var t3 = t.cloneNode(true)
        for(i = t3.rows.length -1;i > 0;i--)
        t3.deleteRow(i)
        t.deleteRow(0)
        div2.appendChild(t3)
        }
    catch(errorInfo) {}
    }
    window.onload = s
  </script>

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Saldo Awal Piutang"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Height="350px"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                            <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="800px" __designer:wfdid="w58" DefaultButton="btnFindConap"><TABLE><TBODY><TR><TD style="WIDTH: 50px" align=left><asp:Label id="Label1" runat="server" Width="50px" Text="Filter :" __designer:wfdid="w59"></asp:Label></TD><TD align=left colSpan=5><asp:DropDownList id="ddlFilter" runat="server" Width="90px" CssClass="inpText" __designer:wfdid="w60" AutoPostBack="True" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged"><asp:ListItem Value="name">Customer</asp:ListItem>
<asp:ListItem Value="no">Invoice No</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterConap" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w61"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindConap" onclick="btnFindConap_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w62"></asp:ImageButton> <asp:ImageButton id="btnViewAllConap" onclick="btnViewAllConap_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w63"></asp:ImageButton>&nbsp;</TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><asp:GridView id="gvTrnConap" runat="server" Width="950px" ForeColor="White" __designer:wfdid="w64" OnSelectedIndexChanged="gvTrnConap_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid" AllowPaging="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No.Nota">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Font-Strikeout="False" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppName" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal Nota">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField HeaderText="Total Bayar" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                        </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong><span style="font-size: 9pt">
                                :: Daftar Saldo Awal Piutang</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<DIV style="WIDTH: 100%"><TABLE width=800><TBODY><TR><TD id="TD36" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="conapoid" runat="server" Width="10px" CssClass="inpText" Visible="False" Enabled="False" MaxLength="10" __designer:wfdid="w132"></asp:TextBox> <asp:TextBox id="trnbelimstoid" runat="server" Width="10px" CssClass="inpText" Visible="False" Enabled="False" MaxLength="10" __designer:wfdid="w133"></asp:TextBox> <asp:TextBox id="paymentoid" runat="server" Width="10px" CssClass="inpText" Visible="False" Enabled="False" MaxLength="10" __designer:wfdid="w134"></asp:TextBox> <asp:Label id="cashbankoid" runat="server" Text="cashbankoid" Visible="False" __designer:wfdid="w135"></asp:Label></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee6" runat="server" __designer:wfdid="w136" ClearMaskOnLostFocus="true" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="paymentdate">
                                                                </ajaxToolkit:MaskedEditExtender> <asp:Label id="I_U" runat="server" Text="NEW" __designer:wfdid="w137"></asp:Label></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w138" TargetControlID="paymentdate" Format="dd/MM/yyyy" PopupButtonID="imbPaymentDate">
                                                                </ajaxToolkit:CalendarExtender> <asp:Label id="Label17" runat="server" Width="94px" Font-Size="X-Small" Text="No.Pembayaran" Visible="False" __designer:wfdid="w139"></asp:Label></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w140" TargetControlID="invoicedate" Format="dd/MM/yyyy" PopupButtonID="imbInvoiceDate">
                                                                </ajaxToolkit:CalendarExtender> <asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" Visible="False" Enabled="true" MaxLength="15" __designer:wfdid="w141"></asp:TextBox></TD></TR><TR><TD align=left colSpan=3 Visible="false">Cabang</TD><TD style="FONT-SIZE: 8pt" align=left><asp:DropDownList id="ddlCabang" runat="server" CssClass="inpText" __designer:wfdid="w230"></asp:DropDownList></TD><TD style="FONT-SIZE: 8pt" align=left Visible="false"></TD><TD style="FONT-SIZE: 8pt" align=left Visible="false"></TD></TR><TR><TD id="TD30" align=left colSpan=3 Visible="false"><asp:Label id="lblSuppoid" runat="server" Font-Size="X-Small" Text="Customer" __designer:wfdid="w142"></asp:Label> <asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w143"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="suppcode" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w144" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnCariSupp" onclick="btnCariSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w145">
                                                                </asp:ImageButton> <asp:Label id="suppoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w146"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left Visible="false"><asp:Label id="Label21" runat="server" Font-Size="X-Small" Text="No.Nota" __designer:wfdid="w147"></asp:Label> <asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w148"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left Visible="false"><asp:TextBox id="trnbelino" runat="server" Width="150px" CssClass="inpText" Enabled="true" MaxLength="20" __designer:wfdid="w149" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD id="TD25" align=left colSpan=3 Visible="false"><asp:Label id="Label11" runat="server" Width="77px" Font-Size="X-Small" Text="Akun Piutang" __designer:wfdid="w150"></asp:Label> <asp:ImageButton id="btnSearcAcctg" onclick="btnSearcAcctg_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w151">
                                                                </asp:ImageButton></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="acctgCode" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w152" ReadOnly="True"></asp:TextBox> </TD><TD style="FONT-SIZE: 8pt" id="TD19" align=left Visible="false"><asp:Label id="Label3" runat="server" Width="96px" Font-Size="X-Small" Text="Total Nota (DPP)" __designer:wfdid="w153"></asp:Label></TD><TD style="FONT-SIZE: 8pt" id="TD20" align=left Visible="false"><asp:TextBox id="dpp" runat="server" Width="110px" CssClass="inpText" Enabled="true" MaxLength="14" __designer:wfdid="w154" AutoPostBack="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD id="TD22" align=left colSpan=3 Visible="false"><asp:Label id="Label4" runat="server" Width="77px" Font-Size="X-Small" Text="Tanggal Nota" __designer:wfdid="w155"></asp:Label> <asp:Label id="Label9" runat="server" Width="1px" ForeColor="Red" Text="*" __designer:wfdid="w156"></asp:Label> </TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="invoicedate" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" MaxLength="10" __designer:wfdid="w157" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbInvoiceDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w158"></asp:ImageButton> <asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w159"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label24" runat="server" Font-Size="X-Small" Text="Tax (%)" __designer:wfdid="w160"></asp:Label> <asp:TextBox id="trntaxpct" runat="server" Width="50px" CssClass="inpText" Enabled="true" MaxLength="4" __designer:wfdid="w161" AutoPostBack="True" OnTextChanged="trntaxpct_TextChanged">0</asp:TextBox></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="amttax" runat="server" Width="110px" CssClass="inpTextDisabled" Enabled="true" MaxLength="14" __designer:wfdid="w162" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD id="TD31" class="Label" align=left colSpan=3 Visible="false">Currency</TD><TD style="FONT-SIZE: 8pt" align=left><asp:DropDownList id="curroid" runat="server" Width="155px" CssClass="inpTextDisabled" Enabled="False" __designer:wfdid="w163" AutoPostBack="True" OnSelectedIndexChanged="curroid_SelectedIndexChanged"></asp:DropDownList></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="Label23" runat="server" Width="96px" Font-Size="X-Small" Text="Total Nota(Nett)" __designer:wfdid="w164"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="amtbeli" runat="server" Width="110px" CssClass="inpTextDisabled" Enabled="true" MaxLength="14" __designer:wfdid="w165" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD></TR><TR><TD id="TD34" class="Label" align=left colSpan=3 Visible="false">Total A/R</TD><TD style="FONT-SIZE: 8pt" align=left><asp:TextBox id="totalAP" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="true" MaxLength="14" __designer:wfdid="w166" ReadOnly="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="lblstatus" runat="server" Text="Status" __designer:wfdid="w167"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left><asp:Label id="lblPosting" runat="server" Font-Size="X-Small" Font-Names="red" Font-Bold="True" ForeColor="Red" __designer:wfdid="w168"></asp:Label></TD></TR><TR><TD id="TD28" class="Label" align=left colSpan=3 Visible="false"><asp:Label id="Label7" runat="server" Width="26px" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w169"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left colSpan=3><asp:TextBox id="trnapnote" runat="server" Width="400px" Height="20px" CssClass="inpText" Enabled="true" MaxLength="220" __designer:wfdid="w229" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD2" align=left colSpan=5 runat="server" Visible="false">&nbsp;<asp:Label id="Label5" runat="server" Font-Size="X-Small" Text="Jatuh Tempo" Visible="False" __designer:wfdid="w171"></asp:Label> <asp:Label id="Label10" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" Visible="False" __designer:wfdid="w172"></asp:Label>&nbsp;<asp:TextBox id="payduedate" runat="server" Width="0px" CssClass="inpTextDisabled" Visible="False" Enabled="False" MaxLength="10" __designer:wfdid="w173"></asp:TextBox>&nbsp;&nbsp; <asp:Label id="Label27" runat="server" Width="109px" Font-Size="X-Small" Text="Total Pembayaran" Visible="False" __designer:wfdid="w174"></asp:Label> <asp:TextBox id="amtbayar" runat="server" Width="150px" CssClass="inpText" Visible="False" Enabled="true" MaxLength="14" __designer:wfdid="w175" AutoPostBack="True" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox></TD><TD align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD11" align=left colSpan=3 runat="server" Visible="false"></TD><TD id="TD13" align=left runat="server" Visible="false"><asp:TextBox id="payacctg" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w176" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPayAcctg" onclick="imbPayAcctg_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w177">
                                                                </asp:ImageButton> <asp:Label id="Label26" runat="server" Width="82px" Font-Size="X-Small" Text="Akun Payment" Visible="False" __designer:wfdid="w178"></asp:Label></TD><TD id="TD8" align=left runat="server"><asp:Label id="Label22" runat="server" Font-Size="X-Small" Text="Termin" Visible="False" __designer:wfdid="w179"></asp:Label></TD><TD id="TD18" align=left runat="server"><asp:DropDownList id="trnpaytype" runat="server" Width="105px" CssClass="inpText" Visible="False" __designer:wfdid="w180" AutoPostBack="True" OnSelectedIndexChanged="trnpaytype_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD1" align=left colSpan=3 runat="server" Visible="false"></TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="currate" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" MaxLength="30" __designer:wfdid="w181">1</asp:TextBox></TD><TD id="TD9" align=left runat="server" Visible="false"><asp:Label id="Label16" runat="server" Width="104px" Font-Size="X-Small" Text="Tanggal Payment" Visible="False" __designer:wfdid="w182"></asp:Label></TD><TD id="TD4" align=left runat="server" Visible="false"><asp:TextBox id="paymentdate" runat="server" Width="125px" CssClass="inpText" Visible="False" Enabled="true" MaxLength="10" __designer:wfdid="w183"></asp:TextBox> <asp:ImageButton id="imbPaymentDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w184"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD6" align=left colSpan=3 runat="server" Visible="false"><asp:Label id="postdate" runat="server" Font-Size="X-Small" Text="1/1/1900" Visible="False" __designer:wfdid="w185"></asp:Label> </TD><TD id="TD14" align=left runat="server" Visible="false"><asp:TextBox id="amtbelinetto" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" Enabled="true" MaxLength="30" __designer:wfdid="w186" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="paymentacctgoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w187"></asp:Label> <asp:Label id="acctgoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w188"></asp:Label></TD><TD id="TD12" align=left runat="server" Visible="false"><asp:Label id="purchAcctgoid" runat="server" Width="99px" Font-Size="X-Small" Visible="False" __designer:wfdid="w189"></asp:Label> </TD><TD id="TD17" align=left runat="server" Visible="false"><asp:TextBox id="purchAcctg" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w190" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbPurch" onclick="imbPurch_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w191">
                                                                </asp:ImageButton> </TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6>Last Update On <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w192"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w193"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="HEIGHT: 15px" align=left colSpan=6><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w194"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w195">
                                                                </asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w196">
                                                                </asp:ImageButton> <asp:ImageButton id="imbposting" onclick="imbposting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w197">
                                                                </asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" Visible="False" __designer:wfdid="w198">
                                                                </asp:ImageButton></TD></TR></TBODY></TABLE><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w199" ClearMaskOnLostFocus="true" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="invoicedate">
                                                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w200" CultureName="id-ID" Mask="99/99/9999" MaskType="Date" UserDateFormat="DayMonthYear" TargetControlID="payduedate">
                                                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w201" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w202" TargetControlID="trntaxpct" ValidChars="1234567890,.">
                                                </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w203" TargetControlID="dpp" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> &nbsp;&nbsp;&nbsp; <TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:UpdatePanel id="UpdatePanel3" runat="server" __designer:wfdid="w204"><ContentTemplate>
<asp:Panel id="PanelSupp" runat="server" Width="630px" CssClass="modalBox" __designer:wfdid="w205" Visible="False"><TABLE><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSupp" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Customer" __designer:wfdid="w206"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="Label2" runat="server" Text="Filter :" __designer:wfdid="w207"></asp:Label> <asp:DropDownList id="ddlSup" runat="server" CssClass="inpText" __designer:wfdid="w208">
                                                                                                <asp:ListItem>Name</asp:ListItem>
                                                                                                <asp:ListItem>Code</asp:ListItem>
                                                                                            </asp:DropDownList> <asp:TextBox id="filterSupp" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w209"></asp:TextBox> <asp:ImageButton id="btnFindSupp" onclick="btnFindSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w210">
                                                                                            </asp:ImageButton> <asp:ImageButton id="btnListSupp" onclick="btnListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w211">
                                                                                            </asp:ImageButton> </TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvSupplier" runat="server" Width="600px" ForeColor="#333333" __designer:wfdid="w212" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" DataKeyNames="suppoid,suppcode,suppname,coa_hutang,coa" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppoid" HeaderText="SuppOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="70px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppName" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                            <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="ClosePopUP" onclick="ClosePopUP_Click" runat="server" __designer:wfdid="w213">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenSupp" runat="server" __designer:wfdid="w214" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w215" PopupControlID="panelSupp" BackgroundCssClass="modalBackground" TargetControlID="btnHiddenSupp">
                                                                        </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD align=left><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w216"><ContentTemplate>
<asp:Panel id="PanelAcctg" runat="server" Width="400px" CssClass="modalBox" Visible="False" __designer:wfdid="w217"><TABLE><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblCOA" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar COA" __designer:wfdid="w218"></asp:Label> <asp:Label id="lblTypeCOA" runat="server" Text="APAccount" Visible="False" __designer:wfdid="w219"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="Label6" runat="server" Text="Filter :" __designer:wfdid="w220"></asp:Label> <asp:DropDownList id="ddlAcctg" runat="server" CssClass="inpText" __designer:wfdid="w221">
                                                                                                <asp:ListItem Value="Name">Description</asp:ListItem>
                                                                                                <asp:ListItem Value="Code">Number</asp:ListItem>
                                                                                            </asp:DropDownList> &nbsp;<asp:TextBox id="filterAcctg" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w222"></asp:TextBox> <asp:ImageButton id="btnFindAcctg" onclick="btnFindAcctg_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w223">
                                                                                            </asp:ImageButton> <asp:ImageButton id="btnListAcctg" onclick="btnListAcctg_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w224">
                                                                                            </asp:ImageButton> </TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvAcc" runat="server" Width="97%" ForeColor="#333333" __designer:wfdid="w225" OnSelectedIndexChanged="gvAcc_SelectedIndexChanged" GridLines="None" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="AcctgOid" HeaderText="AcctgOid" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="No.">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                                            <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> &nbsp; </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseAcctg" onclick="CloseAcctg_Click" runat="server" __designer:wfdid="w226">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenAcctg" runat="server" Visible="False" __designer:wfdid="w227"></asp:Button> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" __designer:wfdid="w228" TargetControlID="btnHiddenAcctg" BackgroundCssClass="modalBackground" PopupControlID="panelAcctg">
                                                                        </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> </TD></TR><TR><TD align=left colSpan=2></TD></TR></TBODY></TABLE></DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong><span style="font-size: 9pt">
                                :: Form Saldo Awal Piutang</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" Drag="True" PopupDragHandleControlID="lblCaption" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" Visible="False" CssClass="modalBox" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

