Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing

Partial Class Master_msttechnician
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub clearform()
        tbnip.Enabled = True
        tbnip.ControlStyle.CssClass = "inpText"
        lblcurrentuser.Text = Session("UserID")
        If Not String.IsNullOrEmpty(tbname.Text.Trim) Then
            tbname.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbidno.Text.Trim) Then
            tbidno.Text = ""
        End If
        ddlgender.SelectedIndex = 0
        ddlmarital.SelectedIndex = 0
        ddlreligion.SelectedIndex = 0
        If Not String.IsNullOrEmpty(tbbirthplace.Text.Trim) Then
            tbbirthplace.Text = ""
        End If
        tbbirthdate.Text = Date.Now.ToString("dd/MM/yyyy")
        If Not String.IsNullOrEmpty(tbcurstreet.Text.Trim) Then
            tbcurstreet.Text = ""
        End If
        ddlcurcity.SelectedIndex = 0
        If Not String.IsNullOrEmpty(tbcurprovince.Text.Trim) Then
            tbcurprovince.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbcurpostcode.Text.Trim) Then
            tbcurpostcode.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbcurpostcode.Text.Trim) Then
            tbcurpostcode.Text = ""
        End If
        If Not String.IsNullOrEmpty(tboristreet.Text.Trim) Then
            tboristreet.Text = ""
        End If
        ddloricity.SelectedIndex = 0
        If Not String.IsNullOrEmpty(tboriprovince.Text.Trim) Then
            tboriprovince.Text = ""
        End If
        If Not String.IsNullOrEmpty(tboripostcode.Text.Trim) Then
            tboripostcode.Text = ""
        End If
        If Not String.IsNullOrEmpty(tboripostcode.Text.Trim) Then
            tboripostcode.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbphone1.Text.Trim) Then
            tbphone1.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbphone2.Text.Trim) Then
            tbphone2.Text = ""
        End If
        If Not String.IsNullOrEmpty(tbemail.Text.Trim) Then
            tbemail.Text = ""
        End If
        ddllastedu.SelectedIndex = 0
        If Not String.IsNullOrEmpty(tblasteduschool.Text.Trim) Then
            tblasteduschool.Text = ""
        End If
        If Not String.IsNullOrEmpty(tblasteducity.Text.Trim) Then
            tblasteducity.Text = ""
        End If
        ddlstatus.SelectedIndex = 0
        tbjoindate.Text = Date.Now.ToString("dd/MM/yyyy")
        If Not String.IsNullOrEmpty(tbnote.Text.Trim) Then
            tbnote.Text = ""
        End If
        imgphoto.ImageUrl = "~/Images/no-image.png"
        If String.IsNullOrEmpty(lblphoto.Text.Trim) Then
            lblphoto.Text = ""
        End If
        If String.IsNullOrEmpty(lblnip.Text.Trim) Then
            lblnip.Text = ""
        End If
        lblcreate.Text = "Create On <span style='font-weight: bold;'>" & Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & Session("UserID") & "</span>"
    End Sub

    Private Sub bindalldata()
        sSql = "SELECT a.personoid, a.personnip, a.personname, a.personcrtaddress FROM ql_mstperson a inner join QL_mstgen b on genoid = PERSONPOST  WHERE a.cmpcode = '" & CompnyCode & "' and b.gendesc = 'teknisi' and b.gengroup = 'jobposition'  ORDER BY a.personname"
        Session("DataTeknisi") = Nothing
        Session("DataTeknisi") = cKon.ambiltabel(sSql, "teknisi")
        gvdata.DataSource = Session("DataTeknisi")
        gvdata.DataBind()
    End Sub

    Private Sub finddata(ByVal filter As String, ByVal val As String)
        sSql = "SELECT a.personoid, a.personnip, a.personname, a.personcrtaddress FROM ql_mstperson a inner join QL_mstgen b on genoid = PERSONPOST WHERE a.cmpcode = '" & CompnyCode & "' and b.gendesc = 'teknisi' and b.gengroup = 'jobposition' "
        If filter = "nip" Then
            sSql &= " AND a.personnip LIKE '%" & val & "%'"
        ElseIf filter = "name" Then
            sSql &= " AND a.personname LIKE '%" & val & "%'"
        End If
        sSql &= " ORDER BY a.personname"
        Session("DataTeknisi") = Nothing
        Session("DataTeknisi") = cKon.ambiltabel(sSql, "teknisi")
        gvdata.DataSource = Session("DataTeknisi")
        gvdata.DataBind()
    End Sub

    Private Sub fillform()
        tbnip.Enabled = False
        tbnip.ControlStyle.CssClass = "inpTextDisabled"
        lblcurrentuser.Text = Session("UserID")
        Try
            Dim oid As Integer = 0
            If Integer.TryParse(Session("oid"), oid) Then
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                sSql = "SELECT personoid, personnip, personidno, personname, persongender, personmrtstatus, personreligion, personbirthplace, personbirthdate, personcrtaddress, personcrtcity, personcrtprovince, personcrtpostcode, personoriaddress, personoricity, personoriprovince, personoripostcode, personphone1, personphone2, personjoindate, personpost, persondepartment, personlastedu, personlastschool, personlasteducity, note, personphoto, personemail, personstatus, createuser, createtime, upduser, updtime, status FROM ql_mstperson WHERE personoid = " & oid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xreader = xCmd.ExecuteReader
                If xreader.HasRows Then
                    While xreader.Read
                        tbnip.Text = xreader("personnip").ToString
                        lblnip.Text = xreader("personnip").ToString
                        tbname.Text = xreader("personname").ToString
                        tbidno.Text = xreader("personidno").ToString
                        ddlgender.SelectedValue = xreader("persongender")
                        ddlmarital.SelectedValue = xreader("personmrtstatus")
                        ddlreligion.SelectedValue = xreader("personreligion")
                        tbbirthplace.Text = xreader("personbirthplace").ToString
                        tbbirthdate.Text = Format(xreader("personbirthdate"), "dd/MM/yyyy")
                        tbcurstreet.Text = xreader("personcrtaddress").ToString
                        ddlcurcity.SelectedValue = xreader("personcrtcity").ToString
                        tbcurprovince.Text = xreader("personcrtprovince").ToString
                        tbcurpostcode.Text = xreader("personcrtpostcode").ToString
                        tboristreet.Text = xreader("personoriaddress").ToString
                        ddloricity.SelectedValue = xreader("personoricity").ToString
                        tboriprovince.Text = xreader("personoriprovince").ToString
                        tboripostcode.Text = xreader("personoripostcode").ToString
                        tbphone1.Text = xreader("personphone1").ToString
                        tbphone2.Text = xreader("personphone2").ToString
                        tbemail.Text = xreader("personemail").ToString
                        ddllastedu.SelectedValue = xreader("personlastedu")
                        tblasteduschool.Text = xreader("personlastschool").ToString
                        tblasteducity.Text = xreader("personlasteducity").ToString
                        ddlstatus.SelectedValue = xreader("personstatus")
                        ddlempstatus.SelectedValue = xreader("status")
                        tbjoindate.Text = Format(xreader("personjoindate"), "dd/MM/yyyy")
                        tbnote.Text = xreader("note").ToString
                        Dim sphoto As String = xreader("personphoto").ToString
                        If sphoto.Trim <> "" Then
                            imgphoto.ImageUrl = "~/Images/Photo/" & sphoto
                        Else
                            imgphoto.ImageUrl = "~/Images/no-image.png"
                        End If
                        lblphoto.Text = xreader("personphoto").ToString
                        If Not IsDBNull(xreader("upduser")) Or xreader("upduser").ToString <> "" Then
                            lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("upduser").ToString & "</span>"
                        Else
                            lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("createtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("createuser").ToString & "</span>"
                        End If
                    End While
                Else
                    If Not xreader Is Nothing Then
                        If Not xreader.IsClosed Then
                            xreader.Close()
                        End If
                    End If
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            Else
                Response.Redirect("~\Master\msttechnician.aspx?awal=true")
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        Finally
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub uploadfilegambar(ByVal fname As String, ByVal pid As Integer)
        Try
            Dim objTrans As SqlClient.SqlTransaction
            Dim savePath As String = Server.MapPath("~/Images/Photo/")
            Const sbmpW = 800
            Const sbmpH = 600
            If tbfileupload.HasFile Then
                If checktypefile(tbfileupload.FileName) Then
                    Dim divideBy, divideByH, divideByW As Double
                    Dim newWidth, newHeight As Integer
                    savePath &= fname
                    Dim uploadBmp As Bitmap = Bitmap.FromStream(tbfileupload.PostedFile.InputStream)
                    divideByW = uploadBmp.Width / sbmpW
                    divideByH = uploadBmp.Height / sbmpH
                    If divideByW > 1 Or divideByH > 1 Then
                        If divideByW > divideByH Then
                            divideBy = divideByW
                        Else
                            divideBy = divideByH
                        End If
                        newWidth = CInt(CDbl(uploadBmp.Width) / divideBy)
                        newHeight = CInt(CDbl(uploadBmp.Height) / divideBy)
                    Else
                        newWidth = uploadBmp.Width
                        newHeight = uploadBmp.Height
                    End If
                    Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight)
                    newBmp.SetResolution(uploadBmp.HorizontalResolution, uploadBmp.VerticalResolution)
                    Dim newGraphic As Graphics = Graphics.FromImage(newBmp)
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    objTrans = conn.BeginTransaction()
                    xCmd.Transaction = objTrans

                    Try
                        newGraphic.Clear(Color.White)
                        newGraphic.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                        newGraphic.DrawImage(uploadBmp, New Rectangle(0, 0, newWidth, newHeight), 0, 0, uploadBmp.Width, uploadBmp.Height, GraphicsUnit.Pixel)
                        newGraphic.Dispose()
                        newBmp.Save(savePath, Imaging.ImageFormat.Jpeg)

                        If Session("oid") Is Nothing Or Session("oid") = "" Then
                            sSql = "UPDATE QL_mstoid SET lastoid=" & pid & " WHERE tablename LIKE 'Photo' AND cmpcode LIKE '%" & CompnyCode & "%'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Else
                            If lblphoto.Text.Trim = "" Then
                                sSql = "UPDATE QL_mstoid SET lastoid=" & pid & " WHERE tablename LIKE 'Photo' AND cmpcode LIKE '%" & CompnyCode & "%'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If

                        objTrans.Commit()
                        conn.Close()

                        imgphoto.ImageUrl = Nothing
                        imgphoto.ImageUrl = "~/Images/Photo/" & fname
                    Catch ex As Exception
                        objTrans.Rollback()
                    End Try
                Else
                    showMessage("File foto tidak bisa diupload. Foto harus bertipe .jpg/.jpeg/.gif/.png/.bmp!", 2)
                    imgphoto.ImageUrl = "~/Images/no-image.png"
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    Exit Sub
                End If
            Else
                showMessage("Please choose uploaded file first !", 2)
                imgphoto.ImageUrl = "~/Images/no-image.png"
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Exit Sub
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Exit Sub
        End Try
    End Sub

    Private Sub initcity()
        sSql = "SELECT genoid, gendesc FROM QL_MSTGEN WHERE gengroup = 'CITY' AND cmpcode = '" & CompnyCode & "'"
        FillDDL(ddlcurcity, sSql)
        FillDDL(ddloricity, sSql)


        sSql = "select count(-1) from ql_mstgen where gengroup = 'jobposition' and cmpcode = '" & CompnyCode & "' and gendesc = 'teknisi'"
        If cKon.ambilscalar(sSql) = 0 Then
            showMessage("Tolong isi description 'Teknisi' pada group 'JobPosition' di Data General !!", 2)
            Exit Sub
        End If
        sSql = "select genoid, gendesc from ql_mstgen where gengroup = 'jobposition' and cmpcode = '" & CompnyCode & "' and gendesc = 'teknisi'"
        FillDDL(jobposition, sSql)
    End Sub
#End Region

#Region "Function"
    Private Function saverecord() As String
        Dim result As String = ""

        If String.IsNullOrEmpty(tbnip.Text.Trim) Then
            result = "NIP teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbname.Text.Trim) Then
            result = "Nama teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbbirthplace.Text.Trim) Then
            result = "Tempat lahir teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbcurstreet.Text.Trim) Then
            result = "Alamat teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbphone1.Text.Trim) Then
            result = "Nomor telepon teknisi tidak boleh kosong!"
        Else
            Dim nip As String = Tchar(tbnip.Text.Trim)
            Dim name As String = Tchar(tbname.Text.Trim)
            Dim idno As String = Tchar(tbidno.Text.Trim)
            Dim gender As String = ddlgender.SelectedValue
            Dim marital As String = ddlmarital.SelectedValue
            Dim religion As String = ddlreligion.SelectedValue
            Dim birthplace As String = Tchar(tbbirthplace.Text.Trim)
            Dim tbirthdate, birthdate As New Date
            If Date.TryParseExact(tbbirthdate.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, tbirthdate) Then
                birthdate = tbirthdate
            Else
                result = "Tanggal lahir tidak valid!"
                Return result
            End If
            Dim cstreet As String = Tchar(tbcurstreet.Text.Trim)
            Dim ccity As String = ddlcurcity.SelectedValue
            Dim cprovince As String = Tchar(tbcurprovince.Text.Trim)
            Dim cpostcode As String = Tchar(tbcurpostcode.Text.Trim)
            Dim ostreet As String = Tchar(tboristreet.Text.Trim)
            Dim ocity As String = ddloricity.SelectedValue
            Dim oprovince As String = Tchar(tboriprovince.Text.Trim)
            Dim opostcode As String = Tchar(tboripostcode.Text.Trim)
            Dim phone1 As String = Tchar(tbphone1.Text.Trim)
            Dim phone2 As String = Tchar(tbphone2.Text.Trim)
            Dim email As String = Tchar(tbemail.Text.Trim)
            Dim lastedu As String = ddllastedu.SelectedValue
            Dim lasteduschool As String = Tchar(tblasteduschool.Text.Trim)
            Dim lasteducity As String = Tchar(tblasteducity.Text.Trim)
            Dim status As String = ddlstatus.SelectedValue
            Dim tjoindate, joindate As New Date
            If Date.TryParseExact(tbjoindate.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, tjoindate) Then
                joindate = tjoindate
            Else
                result = "Tanggal bergabung tidak valid!"
                Return result
            End If
            Dim note As String = Tchar(tbnote.Text.Trim)
            Dim photo As String = Tchar(lblphoto.Text.Trim)
            Dim empstatus As String = ddlempstatus.SelectedValue

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                sSql = "SELECT COUNT(personnip) FROM ql_mstperson WHERE personnip = '" & nip & "'"
                xCmd.CommandText = sSql
                Dim selectres As Integer = xCmd.ExecuteScalar
                If selectres > 0 Then
                    result = "NIP sudah terdaftar sebelumnya, gunakan NIP lain!"
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    Return result
                End If

                Dim oid As Integer = GenerateID("QL_MSTPERSON", "JPT")

                sSql = "INSERT INTO QL_MSTPERSON (CMPCODE, PERSONOID, PERSONNIP, PERSONIDNO, PERSONNAME, PERSONGENDER, PERSONMRTSTATUS, PERSONRELIGION, PERSONBIRTHPLACE, PERSONBIRTHDATE, PERSONCRTADDRESS, PERSONCRTCITY, PERSONCRTPROVINCE, PERSONCRTPOSTCODE, PERSONORIADDRESS, PERSONORICITY, PERSONORIPROVINCE, PERSONORIPOSTCODE, PERSONPHONE1, PERSONPHONE2, PERSONJOINDATE, PERSONLASTEDU, PERSONLASTSCHOOL, PERSONLASTEDUCITY, NOTE, PERSONPHOTO, PERSONEMAIL, PERSONSTATUS, CREATEUSER, CREATETIME, UPDUSER, UPDTIME, STATUS,PERSONPOST) VALUES ('" & CompnyCode & "', " & oid & ", '" & nip & "', '" & idno & "', '" & name & "', '" & gender & "', '" & marital & "', '" & religion & "', '" & birthplace & "', '" & birthdate & "', '" & cstreet & "', " & ccity & ", '" & cprovince & "', '" & cpostcode & "', '" & ostreet & "', " & ocity & ", '" & oprovince & "', '" & opostcode & "', '" & phone1 & "', '" & phone2 & "', '" & joindate & "', '" & lastedu & "', '" & lasteduschool & "', '" & lasteducity & "', '" & note & "', '" & photo & "', '" & email & "', '" & status & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "', '" & empstatus & "'," & jobposition.SelectedValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_MSTOID SET lastoid=" & oid & " WHERE tablename LIKE 'QL_MSTPERSON' AND cmpcode LIKE '%" & CompnyCode & "%'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If

        Return result
    End Function

    Private Function updaterecord(ByVal oid As Integer) As String
        Dim result As String = ""

        If String.IsNullOrEmpty(tbnip.Text.Trim) Then
            result = "NIP teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbname.Text.Trim) Then
            result = "Nama teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbbirthplace.Text.Trim) Then
            result = "Tempat lahir teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbcurstreet.Text.Trim) Then
            result = "Alamat teknisi tidak boleh kosong!"
        ElseIf String.IsNullOrEmpty(tbphone1.Text.Trim) Then
            result = "Nomor telepon teknisi tidak boleh kosong!"
        Else
            Dim nip As String = Tchar(tbnip.Text.Trim)
            Dim name As String = Tchar(tbname.Text.Trim)
            Dim idno As String = Tchar(tbidno.Text.Trim)
            Dim gender As String = ddlgender.SelectedValue
            Dim marital As String = ddlmarital.SelectedValue
            Dim religion As String = ddlreligion.SelectedValue
            Dim birthplace As String = Tchar(tbbirthplace.Text.Trim)
            Dim tbirthdate, birthdate As New Date
            If Date.TryParseExact(tbbirthdate.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, tbirthdate) Then
                birthdate = tbirthdate
            Else
                result = "Tanggal lahir tidak valid!"
                Return result
            End If
            Dim cstreet As String = Tchar(tbcurstreet.Text.Trim)
            Dim ccity As String = ddlcurcity.SelectedValue
            Dim cprovince As String = Tchar(tbcurprovince.Text.Trim)
            Dim cpostcode As String = Tchar(tbcurpostcode.Text.Trim)
            Dim ostreet As String = Tchar(tboristreet.Text.Trim)
            Dim ocity As String = ddloricity.SelectedValue
            Dim oprovince As String = Tchar(tboriprovince.Text.Trim)
            Dim opostcode As String = Tchar(tboripostcode.Text.Trim)
            Dim phone1 As String = Tchar(tbphone1.Text.Trim)
            Dim phone2 As String = Tchar(tbphone2.Text.Trim)
            Dim email As String = Tchar(tbemail.Text.Trim)
            Dim lastedu As String = ddllastedu.SelectedValue
            Dim lasteduschool As String = Tchar(tblasteduschool.Text.Trim)
            Dim lasteducity As String = Tchar(tblasteducity.Text.Trim)
            Dim status As String = ddlstatus.SelectedValue
            Dim tjoindate, joindate As New Date
            If Date.TryParseExact(tbjoindate.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, tjoindate) Then
                joindate = tjoindate
            Else
                result = "Tanggal bergabung tidak valid!"
                Return result
            End If
            Dim note As String = Tchar(tbnote.Text.Trim)
            Dim photo As String = Tchar(lblphoto.Text.Trim)
            Dim empstatus As String = ddlempstatus.SelectedValue

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                If lblnip.Text.Trim <> tbnip.Text.Trim Then
                    sSql = "SELECT COUNT(personnip) FROM ql_mstperson WHERE personnip = '" & nip & "'"
                    xCmd.CommandText = sSql
                    Dim selectres As Integer = xCmd.ExecuteScalar
                    If selectres > 0 Then
                        result = "NIP sudah terdaftar sebelumnya, gunakan NIP lain!"
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        Return result
                    End If
                End If

                sSql = "UPDATE QL_MSTPERSON SET PERSONIDNO = '" & idno & "', PERSONNIP = '" & nip & "', PERSONNAME = '" & name & "', PERSONGENDER = '" & gender & "', PERSONMRTSTATUS = '" & marital & "', PERSONRELIGION = '" & religion & "', PERSONBIRTHPLACE = '" & birthplace & "', PERSONBIRTHDATE = '" & birthdate & "', PERSONCRTADDRESS = '" & cstreet & "', PERSONCRTCITY = " & ccity & ", PERSONCRTPROVINCE = '" & cprovince & "', PERSONCRTPOSTCODE = '" & cpostcode & "', PERSONORIADDRESS = '" & ostreet & "', PERSONORICITY = " & ocity & ", PERSONORIPROVINCE = '" & oprovince & "', PERSONORIPOSTCODE = '" & opostcode & "', PERSONPHONE1 = '" & phone1 & "', PERSONPHONE2 = '" & phone2 & "', PERSONJOINDATE = '" & joindate & "', PERSONLASTEDU = '" & lastedu & "', PERSONLASTSCHOOL = '" & lasteduschool & "', PERSONLASTEDUCITY = '" & lasteducity & "', NOTE = '" & note & "', PERSONPHOTO = '" & photo & "', PERSONEMAIL = '" & email & "', PERSONSTATUS = '" & status & "', UPDUSER = '" & lblcurrentuser.Text & "', UPDTIME = '" & DateTime.Now & "', STATUS = '" & empstatus & "',PERSONPOST =" & jobposition.SelectedValue & " WHERE PERSONOID = " & oid & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                conn.Close()
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If

        Return result
    End Function

    Private Function setnip() As String
        Dim code As String = ""

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Try
            Dim oid As Integer = GenerateID("QL_MSTPERSON", "JPT")
            Dim soid As String = oid.ToString("000")
            sSql = "SELECT gencode FROM QL_MSTGEN WHERE gendesc = 'NIP Code' AND cmpcode LIKE '%" & CompnyCode & "%'"
            xCmd.CommandText = sSql
            Dim scode As String = xCmd.ExecuteScalar
            conn.Close()
            code &= scode
            code &= Date.Now.ToString("ddMMyyyy")
            code &= soid
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return code
    End Function

    Private Function checkpersonexist(ByVal oid As Integer) As Boolean
        Dim result As Boolean = False

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "SELECT COUNT(reqperson) FROM ql_trnrequest WHERE reqperson = " & oid & ""
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteScalar
            If res > 0 Then
                result = True
            Else
                'sSql = "SELECT COUNT(spkperson) FROM ql_trnspkmainprod WHERE spkperson = " & oid & ""
                'xCmd.CommandText = sSql
                'res = xCmd.ExecuteScalar
                'If res > 0 Then
                '    result = True
                'Else
                sSql = "SELECT personnip FROM ql_mstperson WHERE personoid = " & oid & ""
                xCmd.CommandText = sSql
                Dim nip As String = xCmd.ExecuteScalar
                sSql = "SELECT COUNT(userid) FROM ql_mstprof WHERE LTRIM(RTRIM(userid)) = '" & nip.Trim & "'"
                xCmd.CommandText = sSql
                res = xCmd.ExecuteScalar
                If res > 0 Then
                    result = True
                End If
                'End If
            End If
            conn.Close()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return result
    End Function

    Private Function checktypefile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        Session.Timeout = 60
		If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
		
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Master\msttechnician.aspx")
            TabContainer1.ActiveTabIndex = 0
        End If

        Page.Title = CompnyName & " - Teknisi"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            lbltitle.Text = "Tambah Data Teknisi"
        Else
            lbltitle.Text = "Ubah Data Teknisi"
        End If
        ibdelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")

        If Not Page.IsPostBack Then
            bindalldata()
            initcity()
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                TabContainer1.ActiveTabIndex = 0
                'tbnip.Text = setnip()
                clearform()
            Else
                fillform()
                TabContainer1.ActiveTabIndex = 1
                ibdelete.Visible = True
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Master\msttechnician.aspx?awal=true")
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        Dim result As String = ""
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            result = saverecord()
            If result = "" Then
                Response.Redirect("~\Master\msttechnician.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        Else
            result = updaterecord(Session("oid"))
            If result = "" Then
                Response.Redirect("~\Master\msttechnician.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        End If
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim oid As Integer = 0
        If Integer.TryParse(Session("oid"), oid) Then
            If checkpersonexist(oid) = False Then
                If DeleteData("ql_mstperson", "personoid", oid, CompnyCode) Then
                    Response.Redirect("~\Master\msttechnician.aspx?awal=true")
                Else
                    showMessage("Error deleting data!", 2)
                End If
            Else
                showMessage("Data telah digunakan dalam transaksi/profil, data tidak bisa dihapus!", 2)
            End If
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        finddata(ddlfilter.SelectedValue, Tchar(tbfilter.Text.Trim))
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        bindalldata()
        ddlfilter.SelectedIndex = 0
        tbfilter.Text = ""
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub gvdata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdata.PageIndexChanging
        gvdata.PageIndex = e.NewPageIndex
        If tbfilter.Text.Trim <> "" Then
            finddata(ddlfilter.SelectedValue, Tchar(tbfilter.Text.Trim))
        Else
            bindalldata()
        End If
        'gvdata.DataSource = Session("DataTeknisi")
        'gvdata.DataBind()
    End Sub

    Protected Sub ibupload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibupload.Click
        Dim fname As String = ""
        Dim pid As Integer = 0
        If Not Session("oid") Is Nothing Or Session("oid") <> "" Then
            If lblphoto.Text.Trim = "" Then
                pid = GenerateID("Photo", "JPT")
                fname = "img_" & pid & Path.GetExtension(tbfileupload.FileName).ToLower()
            Else
                fname = lblphoto.Text.Trim
            End If
        Else
            pid = GenerateID("Photo", "JPT")
            fname = "img_" & pid & Path.GetExtension(tbfileupload.FileName).ToLower()
        End If
        uploadfilegambar(fname, pid)
        lblphoto.Text = fname
    End Sub
#End Region

    Protected Sub jobposition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
