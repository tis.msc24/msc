'Prgmr:ifan | LastUpdt:10.10.10
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Partial Class trndpap_payother
    Inherits System.Web.UI.Page

#Region "Variables"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("ql_Conn"))
    Dim objCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function MonthToAlphabet(ByVal iMonth As Integer) As String
        Select Case iMonth
            Case 1 : Return "A"
            Case 2 : Return "B"
            Case 3 : Return "C"
            Case 4 : Return "D"
            Case 5 : Return "E"
            Case 6 : Return "F"
            Case 7 : Return "G"
            Case 8 : Return "H"
            Case 9 : Return "I"
            Case 10 : Return "J"
            Case 11 : Return "K"
            Case 12 : Return "L"
            Case Else : Return "?"
        End Select
    End Function
    Private Function GetVarInterface(ByVal sVar As String, ByVal sCmpcode As String) As String
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim xCmd As New SqlCommand("SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' And cmpcode='MSC' AND interfaceres1='" & sCmpcode & "'", conn)
        Dim cSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVar & "' and cmpcode='MSC' AND interfaceres1='" & sCmpcode & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Try
            GetVarInterface = xCmd.ExecuteScalar
        Catch ex As Exception
            GetVarInterface = "Kode COA tidak ditemukan !!"
        End Try
        conn.Close()
        Return GetVarInterface
    End Function
    Private Function seperate(ByVal cabangin As String) As String
        Dim init As String = "'" & cabangin & "'"
        init = init.Replace(",", "','").Replace(" ", "")
        Return init
    End Function
#End Region

#Region "Procedures"
    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                CabangNya.SelectedValue = Session("branch_id")
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            CabangNya.SelectedValue = Session("branch_id")
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'untuk print
        If type <> "" Then
            If type = "BBM" Then
                report.Load(Server.MapPath(folderReport & "printDPAP_ReturBBM.rpt"))
            ElseIf type = "BKM" Then
                report.Load(Server.MapPath(folderReport & "printDPAP_ReturBKM.rpt"))
            ElseIf type = "BGM" Then
                report.Load(Server.MapPath(folderReport & "printDPAP_ReturBGM.rpt"))
            End If

            report.SetParameterValue("cmpcode", cmpcode)
            report.SetParameterValue("no", no)
            report.SetParameterValue("companyname", CompnyName)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
            report.Close() : report.Dispose() : Session("no") = Nothing
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer)
        sSql = "SELECT ap.trndpapoid,ap.cashbankoid,ap.trndpapno,ap.trndpapdate,ap.suppoid,s.suppname," & _
            "ap.payreftype,ap.cashbankacctgoid,ap.payduedate,ap.payrefno,ap.trndpapacctgoid," & _
            "ap.currencyoid,ap.currencyrate,ap.trndpapamt,ap.trndpapnote,ap.trndpapstatus,ap.upduser,ap.updtime, ap.trndpapoid_ref , (select d.trndpapno  from ql_trndpap d where d.cmpcode='" & cmpcode & "' and d.trndpapoid=ap.trndpapoid_ref  )dpno, trndpapacumamt, ap.createtime  " & _
            "FROM QL_trndpap_back ap INNER JOIN QL_mstsupp s ON s.cmpcode=ap.cmpcode AND s.suppoid=ap.suppoid " & _
            "WHERE ap.cmpcode='" & cmpcode & "' AND ap.trndpapoid=" & iOid

        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_TRNDPAP")
        If dtMst.Rows.Count < 1 Then
            showMessage("Gagal membuka data Retur-Uang Muka Pembelian !!<BR>Info :<BR>" & sSql, 1)
            Exit Sub
        Else
            FillDDL(CabangNya, "select gencode,gendesc from QL_mstgen where gengroup='CABANG'")
            CabangNya.SelectedValue = GetStrData(" select gencode from QL_trndpap INNER JOIN QL_mstgen on gencode=branch_code where trndpapoid = '" & dtMst.Rows(0)("trndpapoid_ref").ToString & "'")
            trndparoid.Text = dtMst.Rows(0)("trndpapoid").ToString
            cashbankoid.Text = dtMst.Rows(0)("cashbankoid").ToString
            trndparno.Text = dtMst.Rows(0)("trndpapno").ToString
            trndpardate.Text = Format(CDate(dtMst.Rows(0)("trndpapdate").ToString), "dd/MM/yyyy")
            suppoid.Text = dtMst.Rows(0)("suppoid").ToString
            suppname.Text = dtMst.Rows(0)("suppname").ToString
            payreftype.SelectedValue = dtMst.Rows(0)("payreftype").ToString
            currentpayreftype.Text = dtMst.Rows(0)("payreftype").ToString
            InitDDLCashBank(payreftype.SelectedValue)
            cashbankacctgoid.SelectedValue = dtMst.Rows(0)("cashbankacctgoid").ToString
            currentcbacctgoid.Text = dtMst.Rows(0)("cashbankacctgoid").ToString
            createtime.Text = Format(dtMst.Rows(0)("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
            If payreftype.SelectedValue <> "CASH" Then
                payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "dd/MM/yyyy")
                payrefno.Text = dtMst.Rows(0)("payrefno").ToString
            End If
            trndparacctgoid.SelectedValue = dtMst.Rows(0)("trndpapacctgoid").ToString
            currencyoid.SelectedValue = dtMst.Rows(0)("currencyoid").ToString
            currencyrate.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("currencyrate").ToString), 4)
            PAYAMT.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndpapamt").ToString), 4)
            trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndpapacumamt").ToString), 4)
            trndparnote.Text = dtMst.Rows(0)("trndpapnote").ToString
            trndparstatus.Text = dtMst.Rows(0)("trndpapstatus").ToString
            upduser.Text = dtMst.Rows(0)("upduser").ToString
            updtime.Text = dtMst.Rows(0)("updtime").ToString
            dpno.Items.Clear()
            dpno.Items.Add(dtMst.Rows(0)("dpno"))
            dpno.Items(0).Value = dtMst.Rows(0)("trndpapoid_ref")
            If trndparstatus.Text <> "POST" Then
                btnSave.Visible = True : btnDelete.Visible = True : btnPosting.Visible = True
            Else
                CabangNya.Enabled = False : btnSave.Visible = False : btnDelete.Visible = False : btnPosting.Visible = False
            End If
            trndpardate.Enabled = False
            imbDPARDate.Visible = False
            trndpardate.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub showTableCOA(ByVal noRef As String, ByVal cmpcode As String, ByVal Obj As GridView)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn"))
        Dim ssql As String = "select a.acctgcode acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where noref ='" & noRef & "' order by d.gldbcr desc, a.acctgcode"
        'and d.branch_code='" & cmpcode & "'
        Dim xadap As New SqlDataAdapter(ssql, conn)
        Dim otable As New DataTable
        xadap.Fill(otable)
        If otable.Rows.Count = 0 Then
            ssql = "select a.acctgcode acctgcode , a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote from ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid where glnote like '%" & noRef & "' order by d.gldbcr desc, a.acctgcode"
            ' and d.branch_code='" & cmpcode & "'
            otable.Rows.Clear()
            Dim xadap2 As New SqlDataAdapter(ssql, conn)
            xadap2.Fill(otable)
        End If
        Obj.DataSource = otable
        Obj.DataBind()

    End Sub

    Private Sub BindData(ByVal sPLus As String)
        If FilterDDL.SelectedValue = "branch_code" Then
            sPLus = "AND trndpapoid_ref in( select trndpapoid from QL_trndpap where branch_code ='"
            Dim tambahan As String = GetStrData("select gencode from QL_mstgen where gendesc ='" & FilterTextsupp.Text & "' AND gengroup='CABANG'")
            sPLus &= tambahan & "')"
        End If

        sSql = "SELECT AP.cashbankoid, AP.trndpapoid,AP.trndpapno,AP.trndpaPdate,s.suppname,AP.trndpapamt,AP.trndpapstatus, AP.trndpapnote, trndpapflag, (select d.trndpapno  from ql_trndpap d where d.cmpcode='MSC' and d.trndpapoid=AP.trndpapoid_ref  )dpno , (select gendesc  from ql_trndpap d INNER JOIN QL_mstgen on branch_code=gencode where d.trndpapoid=AP.trndpapoid_ref and gengroup='CABANG' )branch_code    FROM QL_trndpap_back AP INNER JOIN QL_mstsupp S ON S.cmpcode=AP.cmpcode AND S.suppoid=AP.suppoid WHERE AP.cmpcode='" & cmpcode & "' " & sPLus & "       and  ap.trndpapdate  between '" & toDate(dateAwal.Text) & "'  and '" & toDate(dateAkhir.Text) & "'   " & IIf(trndpapflag.SelectedValue = "ALL", "", " and  ap.trndpapflag='" & trndpapflag.SelectedValue & "'  ") & IIf(statuse.SelectedValue = "ALL", "", " and  ap.trndpapstatus='" & statuse.SelectedValue & "'   ") & "  ORDER BY " & orderby.SelectedValue

        FillGV(gvMst, sSql, "QL_TRNDPAP")
    End Sub

    Private Sub BindSupplier(ByVal sPlus As String)
        sSql = "SELECT DISTINCT s.* FROM QL_mstsupp s INNER JOIN QL_TRNdpap p on p.suppoid=s.suppoid and p.cmpcode=s.cmpcode  " & _
            "WHERE s.suppoid in (SElect suppoid from QL_trndpap where branch_code='" & CabangNya.SelectedValue & "' and trndpapamt-trndpapacumamt > 0 AND trndpapstatus='POST') AND p.cmpcode='" & cmpcode & "' " & sPlus & " ORDER BY s.suppcode"
        FillGV(gvCust, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub SetControlCashBank(ByVal bState As String)
        lblDueDate.Visible = bState : lblNeedDue.Visible = bState
        payduedate.Visible = bState : imbDueDate.Visible = bState : lblDate.Visible = bState
        lblRefNo.Visible = bState 'lblNeedRef.Visible = bState : 
        payrefno.Visible = bState
    End Sub

    Private Sub InitDDLCashBank(ByVal sCashbank As String)
        cashbankacctgoid.Items.Clear()
        If Trim(sCashbank) = "CASH" Then
            Dim varCash As String = GetVarInterface("VAR_CASH", CabangNya.SelectedValue)
            If varCash = "" Then
                showMessage("Silakan buat Perkiraan Kas di Data Perkiraan (VAR_CASH) !!", 2)
            Else
                sSql = "SELECT a1.acctgoid,a1.acctgcode+'-'+a1.acctgdesc FROM QL_mstacctg a1 WHERE a1.acctgcode in (" & seperate(varCash) & ")   "
                sSql &= "  AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode ) ORDER BY a1.acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        ElseIf Trim(sCashbank) = "CREDIT CARD" Then
            Dim varCREDITCARD As String = GetVarInterface("VAR_CREDIT_CARD", CabangNya.SelectedValue)
            If varCREDITCARD = "" Then
                showMessage("Silakan buat Perkiraan CREDIT CARD di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode in (" & seperate(varCREDITCARD) & ")  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        ElseIf Trim(sCashbank) = "GIRO" Then
            Dim varCREDITCARD As String = GetVarInterface("VAR_GIRO_HUTANG", CabangNya.SelectedValue)
            If varCREDITCARD = "" Then
                showMessage("Silakan buat Perkiraan Giro Piutang di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode IN (" & seperate(varCREDITCARD) & ") AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        ElseIf Trim(sCashbank) = "NON CASH" Then
            Dim varBank As String = GetVarInterface("VAR_BANK", CabangNya.SelectedValue)
            If varBank = "" Then
                showMessage("Silakan buat Perkiraan Bank di Data Perkiraan !!", 2)
            Else

                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode in(" & seperate(varBank) & ")  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If

        ElseIf Trim(sCashbank) = "OTHER" Then
            Dim varBank As String = GetVarInterface("VAR_BANK", CabangNya.SelectedValue)
            Dim varCash As String = GetVarInterface("VAR_CASH", CabangNya.SelectedValue)
            If varBank = "" Then
                showMessage("Silakan buat Perkiraan Bank di Data Perkiraan !!", 2)
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE   (acctgcode NOT in (" & seperate(varBank) & ")  AND  acctgcode NOT IN (" & seperate(varCash) & "))   AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode ) ORDER BY acctgcode"
                FillDDL(cashbankacctgoid, sSql)
            End If
        End If
        If sCashbank = "NON CASH" Then
            SetControlCashBank(True)
        Else
            SetControlCashBank(False)
        End If


        If payreftype.SelectedValue = "CREDIT CARD" Then
            creditsearch.Visible = True
            CREDITCLEAR.Visible = True
            code.Visible = True
            code.Text = ""
            payrefno.Text = ""
            payrefno.Enabled = False
            payrefno.CssClass = "inpTextDisabled"
        Else
            creditsearch.Visible = False
            CREDITCLEAR.Visible = False
            code.Visible = False
            code.Text = ""
            payrefno.Text = ""
            payrefno.Enabled = True
            payrefno.CssClass = "inpText"
        End If
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & cmpcode & "' and curroid=" & iOid & " order by currdate desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(objCmd.ExecuteScalar, 4)
        conn.Close()
    End Sub

    Private Sub InitAllDDL()
        FillDDL(currencyoid, "select 1,0 ")
        If currencyoid.Items.Count > 0 Then
            FillCurrencyRate(currencyoid.SelectedItem.Value)
        Else
            currencyrate.Text = ToMaskEdit(1, 4)
        End If

        InitDDLCashBank(payreftype.SelectedValue)
        'Dim mysql As String = " select trndpapoid as value,trndpapno as item from QL_trndpap where trndpapstatus='POST'"
        'FillDDL(dpno, mysql)
        ' dpap
        Dim vardpap As String = GetVarInterface("VAR_DPAP", CabangNya.SelectedValue)
        If vardpap = "" Then
            showMessage("Buat Perkiraan untuk Uang Muka Pembelian (VAR_DPAP) !!", 2)
        Else
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & vardpap & "%'  AND acctgoid not in " & _
                "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
            FillDDL(trndparacctgoid, sSql)
        End If

    End Sub

    Private Sub GenerateNo()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
            code = "DPR/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndpapno,1) AS INT)),0) FROM ql_trndpap_back WHERE trndpapno LIKE '" & code & "%'"
            objCmd.CommandText = sSql
            Dim sequence As String = Format((objCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            trndparno.Text = reqcode

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        'Dim typedpap As String = ""
        'If payreftype.SelectedValue = "CASH" Then
        '    typedpap = "T"
        'ElseIf payreftype.SelectedValue = "NON CASH" Then
        '    typedpap = "B"
        'ElseIf payreftype.SelectedValue = "GIRO" Then
        '    typedpap = "G"
        'Else 'OTHER
        '    typedpap = "O"
        'End If
        'Dim sNo As String = "R.DP.AP." & typedpap & "-" & Format(CDate(toDate(trndpardate.Text.Trim)), "yy/MM/")
        'sSql = "SELECT  isnull(max(abs(replace(trndpapno,'" & sNo & "',''))),0)+1   AS IDNEW FROM ql_trndpap_back " & _
        '    "WHERE cmpcode='" & cmpcode & "' AND trndpapno LIKE '" & sNo & "%'"
        'trndparno.Text = GenNumberString(sNo, "", cKon.ambilscalar(sSql), 4)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        '' THROW TO NOT AUTHORIZED PAGE IF NO ROLE EXIST
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("~\accounting\trndpap_payother.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & "- Down Payment AP - (Return/Pay Other)" ' CHANGE FORM NAME HERE
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk HAPUS data ini?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POSTING data ini?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not Page.IsPostBack Then
            InitDDLBranch()
            InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) ' FOR UPDATE DATE
                TabContainer1.ActiveTabIndex = 1
                payreftype.Enabled = False
                payreftype.CssClass = "inpTextDisabled"

                imbSearchCust.Visible = False
                imbClearCust.Visible = False
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                trndpardate.Text = Format(GetServerTime, "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                InitDDLCashBank(payreftype.SelectedValue)
                GenerateNo()
                btnDelete.Visible = False
                trndparstatus.Text = "In Process"
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0

            End If
            dateAwal.Text = Format(Now, "01/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")

            BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(filtertext.Text) & "%'  ")
        End If

        If trndpaRstatus.Text = "POST" Then
            btnshowCOA.Visible = True
        Else
            btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub imbOKValidasi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLCashBank(payreftype.SelectedValue)
        GenerateNo()


    End Sub

    Protected Sub currencyoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(currencyoid.SelectedValue)
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSupplier("  and  " & suppFilter1.SelectedValue & " like '%" & Tchar(filtertext.Text) & "%'")
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, True)
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid.Text = "" : suppname.Text = ""
        dpno.Items.Clear()
        trndpaRamt.Text = ""
        PAYAMT.Text = ""
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            suppoid.Text = gvCust.SelectedDataKey("suppoid").ToString
            suppname.Text = gvCust.SelectedDataKey("suppname").ToString
            cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, False)
            cProc.DisposeGridView(gvCust)
            sSql = "select d.trndpapoid, d.trndpapno  from QL_trndpap d where d.suppoid=" & suppoid.Text & "  and d.trndpapamt-d.trndpapacumamt >0 and d.cmpcode='" & cmpcode & "' and  trndpapstatus='POST' and branch_code='" & CabangNya.SelectedValue & "' AND cashbankacctgoid > 0"
            FillDDL(dpno, sSql)
            FillAmountDP()
            sSql = "Select branch_code from QL_trndpap where trndpapoid=" & dpno.SelectedValue
            branch_code.Text = GetStrData(sSql)
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, 1) : Exit Sub
        End Try
        
    End Sub

    Protected Sub lkbCloseCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, False)
        cProc.DisposeGridView(gvCust)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\accounting\trndpap_payother.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trndpaRstatus.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            sSql = "DELETE FROM QL_trndpap_back WHERE cmpcode='" & cmpcode & "' AND trndpapoid=" & trndpaRoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_cashbankgl WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Response.Redirect("~\accounting\trndpap_payother.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If Not IsDate(toDate(trndpardate.Text)) Then
            sMsg &= "- Format Tanggal Salah !!<BR>"
        End If
        If suppoid.Text = "" Then
            sMsg &= "- Silahkan Pilih Supplier !!<BR>"
        End If
        If cashbankacctgoid.Items.Count < 1 Then
            sMsg &= "- Silahkan Pilih cash, bank atau other COA  !!<BR>"
        End If
        If trndparacctgoid.Items.Count < 1 Then
            sMsg &= "- Silahkan Pilih DP account !!<BR>"
        End If
        If payduedate.Visible Then
            If Not IsDate(toDate(payduedate.Text)) Then
                sMsg &= "- Format Due Date Salah !!<BR>"
            End If

        End If
        If payduedate.Visible And (IsDate(toDate(payduedate.Text))) And (IsDate(toDate(trndpardate.Text))) Then
            If CDate(toDate(payduedate.Text)) < CDate(toDate(trndpardate.Text)) Then
                sMsg &= "- Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !!<BR>"
            End If
        End If
        If currencyoid.Items.Count < 1 Then
            sMsg &= "- Buat data Mata Uang di data Currency !!<BR>"
        End If
        If ToDouble(currencyrate.Text) <= 0 Then
            currencyrate.Text = ToMaskEdit(1, 4)
        End If
        If ToDouble(trndparamt.Text) <= 0 Then
            sMsg &= "- DP Amount Harus > 0 !!<BR>"
        End If
        If ToDouble(PAYAMT.Text) <= 0 Then
            sMsg &= "- Pay Amount HArus > 0 !!<BR>"
        End If
        If ToDouble(PAYAMT.Text) <> ToDouble(trndparamt.Text) Then
            sMsg &= "- Pay Amt Harus Sama Dengan DP Amt !!<BR>"
        End If
        If trndparnote.Text.Trim.Length > 200 Then
            sMsg &= "- Note maksimal 200 karakter !!<BR>"
        End If

        'CEK APAKAH GIRO SUDAH DI CLEARING BLOM?
        If dpno.Items.Count > 0 Then
            If dpno.SelectedItem.Text.Trim.IndexOf("DP.AP.G") >= 0 Then
                sSql = "select COUNT(-1) from QL_TRNDPAP A INNER JOIN QL_GiroPaymentDtl d ON A.CMPCODE=d.cmpcode  and d.NoGiro=a.payrefno  and a.trndpapoid=" & dpno.SelectedValue & "   inner  join QL_GiroPaymentMst m on m.GiroPaymentMstOid=d.GiroPaymentMstOid and m.cmpcode=d.cmpcode and m.GiroPaymentStatus='POST'  AND paymentref_table IN ('QL_CASHBANKMST-PAYAP','QL_TRNDPAP')"
                If GetStrData(sSql) = 0 Then
                    sMsg &= "- That's Giro must be clearing !!<BR>"
                End If

                'cek validasi data
                sSql = "select d.trndpapamt-d.trndpapacumamt  from QL_TRNDPAp d where d.trndpapoid=" & dpno.SelectedValue & "  and d.cmpcode='" & cmpcode & "'"
                If ToDouble(trndparamt.Text) <> ToDouble(GetStrData(sSql)) Then
                    sMsg &= "- DP amount isn't valid !!<BR>"
                End If
            End If
        Else
            sMsg &= "- Silahkan Pilih DP No !!<BR>"
        End If

        If checkApproval("QL_TRNDPAP_BACK", "QL_TRNDPAP_BACK", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
            sMsg &= "- Maaf, data ini sudah send Approval..!!<br>"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trndpap_back WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT trndpapstatus FROM QL_trndpap_back WHERE trndpapoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = 'MSC'"

            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                sMsg &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            trndparstatus.Text = "In Process" : Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            GenerateNo()
        End If
        Session("CBMst") = ClassFunction.GenerateID("QL_trncashbankmst", cmpcode)
        Session("DPAP") = ClassFunction.GenerateID("QL_TRNDPAP_back", cmpcode)
        Session("GLMst") = ClassFunction.GenerateID("QL_trnglmst", cmpcode)
        Session("GLDtl") = ClassFunction.GenerateID("QL_trngldtl", cmpcode)
        Dim iCbdtloid As Int64 = ClassFunction.GenerateID("QL_CASHBANKGL", cmpcode)
        Session("GLSeq") = 1 : currencyrate.Text = 1
        ' temp var utk gl-trans connection
        Dim iCBMst, iTransOid As Integer

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' QL_TRNDPAP
                Dim due As String = "" : Dim refno As String = ""
                If payreftype.SelectedValue = "CASH" Then
                    due = "'1/1/1900'" : refno = ""
                Else
                    due = "'" & CDate(toDate(payduedate.Text)) & "'" : refno = Tchar(payrefno.Text)
                End If

                sSql = "INSERT INTO QL_TRNDPAP_back (cmpcode,trndpapoid,trnDPAPno,trndpapdate,suppoid,cashbankoid," & _
                    "trndpapacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate," & _
                    "trndpapamt,taxtype,taxoid,taxpct,taxamt,trnDPAPnote,trndpapflag,trndpapacumamt,trndpapstatus, " & _
                    "createuser,createtime,upduser,updtime, trndpapoid_ref) VALUES " & _
                    "('" & cmpcode & "'," & Session("DPAP") & ",'" & trndparno.Text & "','" & CDate(toDate(trndpardate.Text)) & _
                    "'," & suppoid.Text & "," & Session("CBMst") & "," & trndparacctgoid.SelectedValue & ",'" & _
                    payreftype.SelectedValue & "'," & cashbankacctgoid.SelectedValue & "," & due & ",'" & refno & _
                    "'," & currencyoid.SelectedValue & "," & ToDouble(currencyrate.Text) & "," & ToDouble(PAYAMT.Text) & _
                    ",'',0,0,0,'" & Tchar(trndparnote.Text) & "','CLOSE'," & ToDouble(trndparamt.Text) & ",'" & trndparstatus.Text & _
                    "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP, " & dpno.SelectedValue & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                iTransOid = Session("DPAP")

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("DPAP") & " WHERE tablename='QL_TRNDPAP_back' AND cmpcode='" & cmpcode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'udp[ate ke dp asline untuk cashbacknya
                sSql = "UPDATE QL_TRNDPAP SET trndpapacumamt=trndpapacumamt+" & ToDouble(PAYAMT.Text) & ", trndpapacumamt_BACK=" & ToDouble(PAYAMT.Text) & ", TRNdpapFLAG= (CASE WHEN trndpapacumamt+" & ToDouble(PAYAMT.Text) & ">=TRNdpapAMT THEN 'CLOSE' ELSE  'OPEN' end  ) WHERE trndpapoid=" & dpno.SelectedValue & " AND cmpcode='" & cmpcode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' QL_trncashbankmst
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                Dim sNo As String = "" : Dim iCurID As Integer = 0
                Dim sAcctgCode As String = ""

                If payreftype.SelectedValue = "CASH" Then
                    sCBType = "BKM"
                ElseIf payreftype.SelectedValue = "NON CASH" Then
                    sCBType = "BBM"
                ElseIf payreftype.SelectedValue = "GIRO" Then
                    sCBType = "BGM"
                ElseIf payreftype.SelectedValue = "CREDIT CARD" Then
                    sCBType = "BCM"
                Else
                    sCBType = "O"
                End If
                If sCBType <> "O" Then
                    ' Cashbank No
                    Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
                    sNo = sCBType & "/" & Cabang & "/" & Format(CDate(toDate(trndpardate.Text)), "yy/MM/dd") & "/"
                    sSql = "SELECT MAX(ABS(replace(cashbankno,'" & sNo & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                    objCmd.CommandText = sSql
                    If Not IsDBNull(objCmd.ExecuteScalar) Then
                        iCurID = objCmd.ExecuteScalar + 1
                    Else
                        iCurID = 1
                    End If
                    Session("sCBNo") = GenNumberString(sNo, "", iCurID, 4)
                    cashbankoid.Text = Session("CBMst")
                    sSql = "INSERT INTO QL_trncashbankmst (branch_code,cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype," & _
                        "cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,createuser,createtime,upduser,updtime," & _
                        "cashbankcurroid,cashbankcurrate, pic, pic_refname) VALUES " & _
                        "('" & CabangNya.SelectedValue & "','" & cmpcode & "','" & Session("CBMst") & "','" & Session("sCBNo") & "','" & trndparstatus.Text & _
                        "','" & sCBType & "','R_DPAP'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(trndpardate.Text)) & _
                        "','RETUR UM. PEMBELIAN|NO=" & trndparno.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & _
                        Session("UserID") & "',CURRENT_TIMESTAMP," & currencyoid.SelectedValue & "," & ToDouble(currencyrate.Text) & ", " & suppoid.Text & ", 'QL_MSTSUPP')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCBMst = Session("CBMst")

                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("CBMst") & " WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If
            Else

                Dim due As String = "" : Dim refno As String = ""
                If payreftype.SelectedValue = "CASH" Then
                    due = "'1/1/1900'" : refno = ""
                Else
                    due = "'" & CDate(toDate(payduedate.Text)) & "'" : refno = Tchar(payrefno.Text)
                End If

                sSql = "UPDATE QL_TRNDPAP_BACK SET trnDPAPno='" & trndparno.Text & "',trndpapdate='" & CDate(toDate(trndpardate.Text)) & _
                    "',suppoid=" & suppoid.Text & ",cashbankoid=" & cashbankoid.Text & "," & "trndpapacctgoid=" & _
                    trndparacctgoid.SelectedValue & ",payreftype='" & payreftype.SelectedValue & "',cashbankacctgoid=" & _
                    cashbankacctgoid.SelectedValue & ",payduedate=" & due & ",payrefno='" & refno & "',currencyoid=" & _
                    currencyoid.SelectedValue & ",currencyrate=" & ToDouble(currencyrate.Text) & "," & "trndpapamt=" & _
                    ToDouble(PAYAMT.Text) & ",trnDPAPnote='" & Tchar(trndparnote.Text) & "',trndpapflag='CLOSE'," & _
                    "trndpapacumamt=" & ToDouble(trndparamt.Text) & ",trndpapstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & _
                    "',updtime=CURRENT_TIMESTAMP " & _
                    "WHERE cmpcode='" & cmpcode & "' AND trndpapoid=" & trndparoid.Text
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                iTransOid = trndparoid.Text

                Dim bUpdPayType As Boolean = False
                Dim sCBType As String = "" : Dim sCBCode As String = ""
                Dim sNo As String = "" : Dim iCurID As Integer = 0

                If payreftype.SelectedValue <> currentpayreftype.Text Or cashbankacctgoid.SelectedValue <> currentcbacctgoid.Text Then
                    ' jika ada perubahan pada payment type
                    If payreftype.SelectedValue = "CASH" Then
                        sCBType = "BKM"
                    ElseIf payreftype.SelectedValue = "NON CASH" Then
                        sCBType = "BBM"
                    ElseIf payreftype.SelectedValue = "GIRO" Then
                        sCBType = "BGM"
                    ElseIf payreftype.SelectedValue = "CREDIT CARD" Then
                        sCBType = "BCM"
                    Else
                        sCBType = "O"
                    End If
                    ' Cashbank No
                    Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
                    sNo = sCBType & "/" & Cabang & "/" & Format(CDate(toDate(trndpardate.Text)), "yy/MM") & "/"
                    sSql = "SELECT MAX(ABS(replace(cashbankno,'" & sNo & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%'"
                    objCmd.CommandText = sSql
                    If Not IsDBNull(objCmd.ExecuteScalar) Then
                        iCurID = objCmd.ExecuteScalar + 1
                    Else
                        iCurID = 1
                    End If
                    Session("sCBNo") = GenNumberString(sNo, "", iCurID, 3)
                    bUpdPayType = True
                End If

                If sCBType <> "O" Then
                    sSql = "UPDATE QL_trncashbankmst SET branch_code='" & CabangNya.SelectedValue & "',cashbankacctgoid=" & cashbankacctgoid.SelectedValue & _
                        ",cashbankdate='" & CDate(toDate(trndpardate.Text)) & "',cashbankstatus='" & trndparstatus.Text & _
                        "',upduser='" & Session("UserID") & "',updtime=current_timestamp,cashbanknote=" & _
                        "'RETUR UM. PENJUALAN|NO=" & trndparno.Text & "', pic=" & suppoid.Text & ", pic_refname='QL_MSTSUPP',cashbankcurroid=" & currencyoid.SelectedValue & _
                        ",cashbankcurrate=" & ToDouble(currencyrate.Text)
                    If bUpdPayType Then
                        sSql &= ",cashbankno='" & Session("sCBNo") & "',cashbanktype='" & sCBType & "'"
                    End If
                    sSql &= " WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & cashbankoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCBMst = cashbankoid.Text
                End If
            End If

            ' IF POST
            If trndparstatus.Text = "POST" Then
                If payreftype.SelectedValue <> "OTHER" Then
                    'insert to cashbank gl
                    sSql = "INSERT INTO QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglnote," & _
                                      "cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                                      "('" & cmpcode & "'," & iCbdtloid & "," & cashbankoid.Text & "," & trndparacctgoid.SelectedValue & _
                                      "," & ToDouble(PAYAMT.Text) & ",'R_dpap|No=" & trndparno.Text & " ~ " & Tchar(trndparnote.Text) & "','" & trndparstatus.Text & "','" & trndparno.Text & "','" & CDate(toDate(trndpardate.Text)) & "','" & Tchar(payrefno.Text.Trim) & _
                                      "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.SelectedValue & "' )"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCbdtloid & " WHERE tablename='QL_CASHBANKGL' AND cmpcode='" & cmpcode & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                ' RETUR QL_trnglmst
                sSql = "INSERT INTO QL_trnglmst (branch_code,cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type) VALUES " & _
                    "('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLMst") & ",'" & CDate(toDate(trndpardate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(trndpardate.Text))) & "','RETUR UM. PEMBELIAN|NO=" & trndparno.Text & "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'DPAPR')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' QL_trngldtl - Debet (KAS/BANK)
                sSql = "INSERT INTO QL_trngldtl (branch_code,cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt, glamtidr, glamtusd,noref,glnote," & _
                    "glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES " & _
                    "('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLDtl") & "," & Session("GLSeq") & "," & Session("GLMst") & _
                    "," & cashbankacctgoid.SelectedValue & ",'D'," & ToDouble(PAYAMT.Text) * ToDouble(currencyrate.Text) & ", " & ToDouble(PAYAMT.Text) * ToDouble(currencyrate.Text) & ", 0,'" & _
                    trndparno.Text & "','DP A/P Retur | Supp. " & Tchar(suppname.Text) & " | No. DP. " & dpno.SelectedItem.Text & " | Note." & Tchar(trndparnote.Text) & "','QL_trndpap_back " & iCBMst & "','" & iTransOid & _
                    "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & _
                    "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Session("GLDtl") += 1 : Session("GLSeq") += 1

                ' QL_trngldtl - Credit (UM PENJUALAN)
                sSql = "INSERT INTO QL_trngldtl (branch_code,cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt, glamtidr, glamtusd,noref,glnote," & _
                    "glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES " & _
                    "('" & CabangNya.SelectedValue & "','" & cmpcode & "'," & Session("GLDtl") & "," & Session("GLSeq") & "," & Session("GLMst") & _
                    "," & trndparacctgoid.SelectedValue & ",'C'," & ToDouble(PAYAMT.Text) * ToDouble(currencyrate.Text) & "," & ToDouble(PAYAMT.Text) * ToDouble(currencyrate.Text) & ", 0, '" & _
                    trndparno.Text & "','DP A/P Retur | Supp. " & Tchar(suppname.Text) & " | No. DP. " & dpno.SelectedItem.Text & " | Note." & Tchar(trndparnote.Text) & "','QL_trndpap_back " & iCBMst & "','" & iTransOid & _
                    "','" & trndparstatus.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & _
                    "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Session("GLDtl") += 1 : Session("GLSeq") += 1

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("GLMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_trnglmst'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("GLDtl") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='QL_trngldtl'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : trndparstatus.Text = "In Process"
            showMessage(ex.ToString, 1) : Exit Sub
        End Try
        Response.Redirect("~\accounting\trndpap_payother.aspx?awal=true")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvMst.PageIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterTextsupp.Text) & "%' ")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDL.SelectedIndex = 0 : FilterTextsupp.Text = ""
        BindData(" ")
    End Sub

    Private Sub FillAmountDP()
        trndparamt.Text = 0
        If dpno.Items.Count > 0 Then
            sSql = "select d.trndpapamt-d.trndpapacumamt from QL_TRNDPAP d where d.trndpapoid=" & dpno.SelectedValue & " and d.cmpcode='" & cmpcode & "'"
            trndparamt.Text = ToMaskEdit(ToDouble(GetStrData(sSql)), 3)
            sSql = "select CASE WHEN d.payreftype = 'BBK' THEN 'NON CASH' ELSE 'CASH' END from QL_TRNDPAP d where d.trndpapoid=" & dpno.SelectedValue & " and d.cmpcode='" & cmpcode & "' AND d.payreftype NOT IN ('RETUR')"
            payreftype.SelectedValue = GetStrData(sSql)
            payreftype_SelectedIndexChanged(Nothing, Nothing)
            sSql = "Select d.cashbankacctgoid from QL_TRNDPAP d where d.trndpapoid=" & dpno.SelectedValue & " and d.cmpcode='" & cmpcode & "' AND d.payreftype NOT IN ('RETUR') and cashbankacctgoid > 0"
            cashbankacctgoid.SelectedValue = GetStrData(sSql)
        End If
    End Sub

    Protected Sub trndpapamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndparamt.TextChanged
        trndparamt.Text = NewMaskEdit(ToDouble(trndparamt.Text.Trim))

    End Sub

    Protected Sub trndpapdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpardate.TextChanged

        GenerateNo()

    End Sub

    Protected Sub imbsearchFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbsearchFilter.Click
        BindSupplier("")
        cProc.SetModalPopUpExtender(btnHideCust, pnlCust, mpeCust, True)
    End Sub

    Protected Sub CREDITCLEAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CREDITCLEAR.Click
        payrefno.Text = ""
        code.Text = ""
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(trndparno.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(4).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(3).Text), 4)

        End If
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterTextsupp.Text) & "%' ")
    End Sub

    Protected Sub dpno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpno.SelectedIndexChanged
        branch_code.Text = GetStrData("Select branch_code from QL_trndpap where trndpapoid=" & dpno.SelectedValue)
        FillAmountDP()
    End Sub

    Protected Sub PAYAMT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ToDouble(PAYAMT.Text) = 0 Then
            PAYAMT.Text = 0
        Else
            PAYAMT.Text = ToMaskEdit(ToDouble(PAYAMT.Text), 4)
        End If
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitAllDDL()
        GenerateNo()
        imbClearCust_Click(Nothing, Nothing)
    End Sub

    Protected Sub gvMst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & gvMst.SelectedDataKey.Item("cashbankoid").ToString & ""
        Dim sqlku As String = "SELECT cashbankno FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & gvMst.SelectedDataKey.Item("cashbankoid").ToString & ""
        PrintReport(cKon.ambilscalar(sqlku), cKon.ambilscalar(sSql))
    End Sub
#End Region

End Class
