<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="fm_mstgen.aspx.vb" Inherits="fm_mstgen" Title="PT. MULTI SARANA COMPUTER - Master General" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Data General"></asp:Label><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                        SelectCommand="SELECT [genoid], [gencode], [gendesc], [gengroup], [genother1], [genother2] FROM [QL_mstgen] WHERE ([gengroup] = @gengroup)" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="PROVINCE" Name="gengroup" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label">Filter :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="gendesc">Description</asp:ListItem>
                                                    <asp:ListItem Value="genCode">Code</asp:ListItem>
                                                    <asp:ListItem Value="gengroup" Enabled="False">General Group</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton
                                                    ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="Label">
                                                <asp:CheckBox ID="chkGroup" runat="server" Text="Group" Width="109px" /></td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterGroup" runat="server" CssClass="inpText" Width="155px" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 10px" colspan="5">
                                                &nbsp;<fieldset id="field1" style="height: 290px; width: 98%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                    <div id='divTblData'>
                                        <asp:GridView ID="GVmstgen" runat="server" EmptyDataText="No data in database." AutoGenerateColumns="False" Width="100%" CellPadding="4" AllowPaging="True" EnableModelValidation="True" ForeColor="#333333" GridLines="None" PageSize="8">
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="fm_mstgen.aspx?oid={0}"
                                                    DataTextField="genCode" HeaderText="Code">
                                                    <ItemStyle Width="200px" Font-Size="X-Small" />
                                                    <HeaderStyle Width="200px" Font-Size="X-Small" CssClass="gvhdr" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gendesc" HeaderText="Description" SortExpression="gendesc">
                                                    <ItemStyle Width="550px" Font-Size="X-Small" />
                                                    <HeaderStyle Width="550px" Font-Size="X-Small" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="gengroup" HeaderText="Group" SortExpression="gengroup">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" CssClass="gvhdr" Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EmptyDataTemplate>



                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        </div>
                                </fieldset>
                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List Of General</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" Visible="False" __designer:wfdid="w54"></asp:Label> <asp:TextBox id="genoid" runat="server" Width="25px" CssClass="inpText" Visible="False" __designer:wfdid="w55" MaxLength="10" size="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left>Code <asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w56"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="gencode" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w57" MaxLength="30" size="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" vAlign=top align=left>Description <asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w58"></asp:Label><BR /></TD><TD class="Label" vAlign=top align=left><asp:TextBox id="gendesc" runat="server" Width="200px" Height="50px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w59" MaxLength="70" size="20" TextMode="MultiLine" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label4" runat="server" CssClass="Important" Text="* 70 characters" __designer:wfdid="w60"></asp:Label> </TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left>Group</TD><TD class="Label" align=left><asp:DropDownList id="gengroup" runat="server" Width="205px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w61" AutoPostBack="True"></asp:DropDownList> <asp:Label id="CutOfDate" runat="server" Visible="False" __designer:wfdid="w62"></asp:Label></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="CbAssetType" runat="server" Text="Cabang" __designer:wfdid="w63"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="sCabang" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w64" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="Label7" runat="server" Text="COA Asset" Visible="False" __designer:wfdid="w65"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="ddlCountry" runat="server" Width="205px" CssClass="inpText" Visible="False" __designer:wfdid="w67" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList><asp:TextBox id="TglCOA" runat="server" Width="114px" CssClass="inpText" __designer:wfdid="w68"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w69"></asp:ImageButton><asp:TextBox id="genother1" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w66" MaxLength="240" size="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><SPAN style="FONT-SIZE: 7pt"></SPAN><asp:Label id="Label6" runat="server" Width="90px" Text="COA Accum" Visible="False" __designer:wfdid="w70"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="genother2" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w71" MaxLength="50" size="20"></asp:TextBox><asp:DropDownList id="ddlProvince" runat="server" Width="205px" CssClass="inpText" Visible="False" __designer:wfdid="w72" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><SPAN style="FONT-SIZE: 7pt"><asp:Label id="Label5" runat="server" Width="104px" Text="COA Dep_Expense" Visible="False" __designer:wfdid="w73"></asp:Label></SPAN></TD><TD class="Label" align=left><asp:TextBox id="genother3" runat="server" Width="200px" CssClass="inpText" Visible="False" __designer:wfdid="w74" MaxLength="50" size="20" Enabled="False"></asp:TextBox><asp:DropDownList id="ddlItemtype" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w75">
                                                    </asp:DropDownList> <asp:DropDownList id="DdlType" runat="server" Width="205px" CssClass="inpText" Visible="False" __designer:wfdid="w76"><asp:ListItem Value="UMUM">GUDANG UMUM</asp:ListItem>
<asp:ListItem Value="TITIPAN">GUDANG TITIPAN</asp:ListItem>
<asp:ListItem Value="RUSAK">GUDANG BARANG RUSAK</asp:ListItem>
<asp:ListItem Value="KONSINYASI">GUDANG KONSINYASI</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="ValueDep" runat="server" Text="Dep. Value" Visible="False" __designer:wfdid="w77"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="genother7" runat="server" Width="100px" CssClass="inpText" Visible="False" __designer:wfdid="w78" MaxLength="10"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=2>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w79"></asp:Label> By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w80"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w81"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w82"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w83"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w84" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w85"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="Dep" runat="server" __designer:wfdid="w86" TargetControlID="genother7" ValidChars="1234567890-/"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w87" TargetControlID="TglCOA" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear">
                                                    </ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w88" TargetControlID="TglCOA" Format="dd/MM/yyyy" PopupButtonID="imbDate"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: Form General</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w1" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w2"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w3"></asp:Image></td>
                            <td align="left">
                                <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important" __designer:wfdid="w4"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPopUpOK" OnClick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" __designer:wfdid="w6" DropShadow="True" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w7" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
