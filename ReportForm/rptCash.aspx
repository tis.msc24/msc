<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptCash.aspx.vb" Inherits="ReportForm_DailyCashReport" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Daily Cash Report" CssClass="Title" ForeColor="Navy" Width="180px" Font-Size="Medium"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center">
             <asp:UpdatePanel ID="upReportForm" runat="server">
             <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport" __designer:wfdid="w1"><TABLE><TBODY><TR><TD id="TD3" class="Label" align=left Visible="false"><asp:Label id="Cabang" runat="server" Text="Cabang" __designer:wfdid="w2"></asp:Label></TD><TD id="TD2" class="Label" align=center Visible="false">:</TD><TD id="TD1" class="Label" align=left Visible="false"><asp:DropDownList id="FilterDDLDiv" runat="server" CssClass="inpText" __designer:wfdid="w3" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="TD8" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label7" runat="server" Text="Type" __designer:wfdid="w4"></asp:Label></TD><TD id="TD7" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD9" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" __designer:wfdid="w5" AutoPostBack="True"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w6"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w7" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w9"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w10" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="(MM/dd/yyyy)" __designer:wfdid="w12"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="Account" __designer:wfdid="w19"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="AcctgName" runat="server" Width="255px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w15" TextMode="MultiLine" Rows="2" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchAcctg" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseAcctg" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w17"></asp:ImageButton> <asp:Label id="acctgoid" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label></TD></TR><TR><TD id="TD4" class="Label" align=left runat="server" Visible="false"><asp:Label id="lbCurr" runat="server" Text="Currency Value" __designer:wfdid="w15"></asp:Label></TD><TD id="TD5" class="Label" align=center runat="server" Visible="false"><asp:Label id="lbSeptCurr" runat="server" Text=":" __designer:wfdid="w16"></asp:Label></TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w17" Enabled="False"><asp:ListItem Value="IDR">INDONESIAN RUPIAH</asp:ListItem>
<asp:ListItem Value="USD">US DOLLAR</asp:ListItem>
<asp:ListItem>VALAS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w18" Format="dd/MM/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w19" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w20" Format="dd/MM/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w21" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w22"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w23"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w24"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 125px" class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w20" DisplayAfter="250" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div5" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w21"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" __designer:dtid="281474976710682" __designer:wfdid="w28" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</ContentTemplate>
                                                        <Triggers>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</Triggers>
                                                    </asp:UpdatePanel>
                            &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 226px">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" DefaultButton="btnFindListAcctg" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Account</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListAcctg" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="acctgcode">Account Code</asp:ListItem>
<asp:ListItem Value="acctgdesc">Account Desc</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListAcctg" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:ImageButton id="btnFindListAcctg" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListAcctg" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvAcctg" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="acctgoid,acctgcode,acctgdesc" UseAccessibleHeader="False" PageSize="100">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><HeaderTemplate>
&nbsp;
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" ToolTip='<%# Eval("acctgoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="acctgcode" HeaderText="Acctg Code">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Acctg Desc">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListAcctg" runat="server" OnClick="lbAddToListAcctg_Click">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListAcctg" runat="server" OnClick="lbCloseListAcctg_Click">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblListMat" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

