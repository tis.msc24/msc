﻿Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunctionAccounting
Imports ClassProcedure
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class ReportForm_ReportPayAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        If IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then
            If DateDiff(DateInterval.Day, CDate(toDate(txtStart.Text)), CDate(toDate(txtFinish.Text))) < 0 Then
                sReturn &= "- End Date must be greater than Start Date.<BR>"
            End If
        End If
        If rbSupplier.SelectedValue = "SELECT" Then
            If custoid.Text = "" Then
                sReturn &= "- End supplier not null.<BR>"
            End If
        End If


        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT DISTINCT 0 selected, s.suppoid,s.suppcode,s.suppname,s.suppaddr FROM QL_mstsupp s LEFT JOIN QL_GiroPaymentDtl gd ON s.suppoid = gd.CustOid LEFT JOIN QL_GiroPaymentMst gm ON gm.GiroPaymentMstOid=gd.GiroPaymentMstOid WHERE s.suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR s.suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' AND gm.Tipe='OUT' ORDER BY s.suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("QL_mstsupp") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        'If Not Session("QL_mstsupp") Is Nothing Then
        '    Dim dtab As DataTable = Session("QL_mstsupp")

        '    If dtab.Rows.Count > 0 Then
        '        Dim cb As System.Web.UI.WebControls.CheckBox
        '        Dim dView As DataView = dtab.DefaultView
        '        Dim drView As DataRowView

        '        For i As Integer = 0 To gvSupplier.Rows.Count - 1
        '            cb = gvSupplier.Rows(i).FindControl("chkSelect")
        '            dView.RowFilter = "suppoid = " & cb.ToolTip
        '            drView = dView.Item(0)
        '            drView.BeginEdit()
        '            If cb.Checked = True Then
        '                drView("selected") = 1
        '            Else
        '                drView("selected") = 0
        '            End If
        '            drView.EndEdit()
        '            dView.RowFilter = ""
        '        Next
        '        dtab.AcceptChanges()
        '    End If
        '    Session("QL_mstsupp") = dtab
        'End If
        If Not Session("account") Is Nothing Then
            Dim dtab As DataTable = Session("account")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim lbl As System.Web.UI.WebControls.Label
                Dim acctgoid As Integer
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvAccounting.Rows.Count - 1
                    cb = gvAccounting.Rows(i).FindControl("cbAccount")
                    If cb.Checked = True Then
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 1
                        drView.EndEdit()
                        dView.RowFilter = ""
                    Else
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 0
                        drView.EndEdit()
                        dView.RowFilter = ""
                    End If
                Next
                dtab.AcceptChanges()
            End If

            Session("account") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = ""
        Dim rptName As String = ""
        Dim Dtl As String = ""
        Dim Join As String = ""
        Dim nFile As String = ""
        Dim sSuppOid As String = ""
        Dim msg As String = String.Empty

        Try

            If sType = "Print Excel" Then
                nFile = Server.MapPath(folderReport & "rptPayAPStatus.rpt")
            Else
                nFile = Server.MapPath(folderReport & "rptPayAPStatus.rpt")
            End If

            Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
            report.Load(nFile)
            rptName = "PayAPStatus"

            Dim sAcctgOid As String = ""
            If rblAccount.SelectedValue = "SELECT" Then
                Dim dtAcctg As DataTable : dtAcctg = Session("account")
                Dim dvAcctg As DataView = dtAcctg.DefaultView
                dvAcctg.RowFilter = "selected=1"

                For R1 As Integer = 0 To dvAcctg.Count - 1
                    sAcctgOid &= dvAcctg(R1)("id").ToString & ","
                Next
            End If

            sSql = "SELECT DISTINCT cbm.cashbankoid, ( CASE cbm.cashbankno WHEN '' THEN CONVERT ( VARCHAR (10), cbm.cashbankoid ) ELSE cbm.cashbankno END ) AS draftno, cbm.cashbankno, cbm.cashbankdate AS cashbankdate, cbm.cashbankstatus, cbm.cmpcode, cbm.pic personoid, s.suppname, cgl.cashbankglamtidr cashbankamt, cbm.cashbanknote, cbm.createuser, cgl.duedate cashbankduedate FROM QL_trncashbankmst cbm INNER JOIN QL_mstsupp s ON s.suppoid = cbm.pic INNER JOIN QL_cashbankgl cgl ON cgl.cashbankoid = cbm.cashbankoid"

            sSql &= " WHERE cashbankgroup='AP'"

            If custoid.Text <> "" Then
                sSql &= " AND pic = '" & custoid.Text & "' "
            End If

            If txtStart.Text <> "" And txtFinish.Text <> "" Then

                sSql &= " AND cbm.cashbankdate>='" & CDate(toDate(txtStart.Text)) & " 00:00:00' AND cbm.cashbankdate<='" & CDate(toDate(txtFinish.Text)) & " 23:59:59'"

            End If

            If rblAccount.SelectedValue = "SELECT" Then
                If sAcctgOid <> "" Then
                    sSql &= " AND cbm.cashbankacctgoid IN (" & Left(sAcctgOid, sAcctgOid.Length - 1) & ") "
                End If
            End If

            If Txtnobukti.Text <> "" Then
                sSql &= "  AND cbm.cashbankno like '%" & Txtnobukti.Text & "%'"
            End If
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")

            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("periodawal", CDate(toDate(txtStart.Text)))
            report.SetParameterValue("periodakhir", CDate(toDate(txtFinish.Text)))
            report.SetParameterValue("Title", "AP Payment Status Report")
            report.SetParameterValue("PrintLastModified", strLastModified)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.PrintOptions.PaperSize = PaperSize.PaperFolio
            cProc.SetDBLogonForReport2(report)


            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try

    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\ReportPayAP.aspx")
        End If
        If checkPagePermission("~\ReportForm\ReportPayAP.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Account Receiveable Report"
        If Not Page.IsPostBack Then

            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            'FillDDL(FilterDDLDiv, sSql)

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "01/MM/yyyy")
            txtFinish.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        Session("QL_mstsupp") = Nothing
        gvSupplier.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
            custoid.Text = ""
            FilterTextSupplier.Text = ""
            tr_supp.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
            custoid.Text = ""
            FilterTextSupplier.Text = ""
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        tr_supp.Visible = True
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        custoid.Text = ""
        tr_supp.Visible = False
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()

        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstsupp")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        ShowReport("View")
       
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\ReportPayAP.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvSupplier.SelectedIndexChanged
        custoid.Text = gvSupplier.SelectedDataKey("suppoid")
        FilterTextSupplier.Text = gvSupplier.SelectedDataKey("suppname")
        tr_supp.Visible = False
    End Sub

    Protected Sub rblAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblAccount.SelectedIndexChanged
        If rblAccount.SelectedValue = "ALL" Then
            td_caricoa.Visible = False
            acctgdesc.Visible = False
            btnsearchacctg.Visible = False
            btnDelete.Visible = False
            acctgdesc.Text = ""

            gvAccounting.Visible = False
            gvAccounting.DataSource = Nothing
            gvAccounting.DataBind()
            tr_coa.Visible = False
        Else
            td_caricoa.Visible = True
            acctgdesc.Visible = True
            btnsearchacctg.Visible = True
            btnDelete.Visible = True
            acctgdesc.Text = ""
        End If
    End Sub

    Protected Sub btnsearchacctg_Click(sender As Object, e As ImageClickEventArgs) Handles btnsearchacctg.Click
        tr_coa.Visible = True
        Dim sDate As String = ""
        BindAccounting(acctgdesc.Text.Trim)
        gvAccounting.Visible = True
    End Sub

    Private Sub BindAccounting(ByVal filter As String)
        Try
            Dim dtab As New DataTable
            sSql = "SELECT DISTINCT 0 as selected, ac.acctgoid as id, ac.acctgcode as code,ac.acctgdesc as name  FROM QL_mstacctg ac INNER JOIN QL_trncashbankmst cbm ON ac.acctgoid = cbm.cashbankacctgoid WHERE ac.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null ) AND (ac.acctgcode LIKE '%" & Tchar(filter.Trim) & "%' OR ac.acctgdesc LIKE '%" & Tchar(filter.Trim) & "%') AND cbm.cashbankgroup= 'AP' ORDER BY acctgcode"

            dtab = cKon.ambiltabel(sSql, "QL_mstacctg")
            gvAccounting.DataSource = dtab
            gvAccounting.DataBind()
            Session("account") = dtab
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            conn.Close() : Exit Sub
        End Try
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As ImageClickEventArgs) Handles btnDelete.Click
        tr_coa.Visible = False
    End Sub

    Protected Sub gvAccounting_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvAccounting.PageIndexChanging
        UpdateCheckedGV()
        Dim dtab As DataTable = Session("account")
        gvAccounting.PageIndex = e.NewPageIndex
        gvAccounting.DataSource = dtab
        gvAccounting.DataBind()
        Session("account") = dtab
    End Sub
#End Region
End Class
