<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="debitnote.aspx.vb" Inherits="debitnote" Title="Debit Note" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">

    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="height: 34px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Debet Note" CssClass="Title" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1X">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<DIV><asp:Panel id="Panel1s" runat="server" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD align=left>Filter </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" Font-Size="Small">
                                                                <asp:ListItem Value="no">DN No.</asp:ListItem>
                                                                <asp:ListItem Value="name">Customer/Supplier Name</asp:ListItem>
                                                                <asp:ListItem Value="note">Note</asp:ListItem>
                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="235px" CssClass="inpText" Font-Size="Small" MaxLength="30"></asp:TextBox></TD></TR><TR><TD align=left>Period </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="75px" CssClass="inpText" Font-Size="Small"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="75px" CssClass="inpText" Font-Size="Small"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD id="TD2" align=left Visible="false">Cabang</TD><TD id="TD1" align=left Visible="false">:</TD><TD id="TD3" align=left colSpan=4 Visible="false"><asp:DropDownList id="DDLfiltercabang" runat="server" Width="100px" CssClass="inpText" Font-Size="Small">
                                                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                                <asp:ListItem Value="AP">A/P</asp:ListItem>
                                                                <asp:ListItem Value="AR">A/R</asp:ListItem>
                                                            </asp:DropDownList></TD></TR><TR><TD align=left>Type </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="tipene" runat="server" Width="100px" CssClass="inpText" Font-Size="Small"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="AP">A/P</asp:ListItem>
<asp:ListItem Value="AR">A/R</asp:ListItem>
<asp:ListItem>NT</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="100px" CssClass="inpText" Font-Size="Small"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>IN APPROVAL</asp:ListItem>
<asp:ListItem Value="APPROVED">APPROVED</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=4><asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png"></asp:ImageButton><asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=4><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" Enabled="True" TargetControlID="tgl1" PopupButtonID="sTgl1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce2" runat="server" Enabled="True" TargetControlID="tgl2" PopupButtonID="sTgl2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVmstgen" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="GVmstgen_SelectedIndexChanged" PageSize="8" GridLines="None" DataKeyNames="cmpcode,oid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="no" HeaderText="DN No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tgl" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reftype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Customer/Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invoice" HeaderText="Invoice">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="DN Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dnstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data not found !!"></asp:Label>

                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" Font-Bold="False" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 13px" align=left>&nbsp;</TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" />
                            <strong><span>List of Debet Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE><TBODY><TR><TD align=left><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" Visible="False"></asp:Label> <asp:DropDownList id="DDLOutlet" runat="server" Width="200px" CssClass="inpText" Visible="False" OnSelectedIndexChanged="DDLOutlet_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left></TD><TD style="WIDTH: 318px" align=left></TD><TD align=left><asp:Label id="CutofDate" runat="server" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="invoicedate" runat="server" Visible="False"></asp:Label></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="lblDraftno" runat="server" Text="Draft No."></asp:Label></TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:Label id="draft" runat="server"></asp:Label><asp:TextBox id="cnno" runat="server" Width="120px" CssClass="inpTextDisabled" MaxLength="15" Visible="False" Enabled="False" size="20"></asp:TextBox> <asp:Label id="refoid" runat="server" Visible="False"></asp:Label> <asp:Label id="cnoid" runat="server" Visible="False"></asp:Label></TD><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="DDLcabang" runat="server" Width="184px" CssClass="inpText" OnSelectedIndexChanged="DDLcabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>&nbsp;<asp:Label id="cabang" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="lblAPARNo" runat="server" Width="85px" Text="Type"></asp:Label></TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:DropDownList id="reftype" runat="server" Width="50px" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="AR">A/R</asp:ListItem>
<asp:ListItem Value="AP">A/P</asp:ListItem>
<asp:ListItem>NT</asp:ListItem>
</asp:DropDownList> <asp:Label id="cust_supp_oid" runat="server" Visible="False"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" align=left>Date <asp:Label id="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD style="WHITE-SPACE: nowrap" align=left>:</TD><TD align=left><asp:TextBox id="tgl" runat="server" Width="75px" CssClass="inpTextDisabled" MaxLength="10" Enabled="False" AutoPostBack="True" OnTextChanged="tgl_TextChanged"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="lblsupp_cust" runat="server" Width="59px" Text="Supplier"></asp:Label><asp:Label id="Label3" runat="server" Width="10px" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="nama" runat="server" Width="231px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch_SC" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnclearSC" onclick="btnclearSC_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="WHITE-SPACE: nowrap" align=left>Currency</TD><TD style="WHITE-SPACE: nowrap" align=left>:</TD><TD align=left><asp:DropDownList id="curroid" runat="server" Width="80px" CssClass="inpTextDisabled" Enabled="False">
                                                    </asp:DropDownList>&nbsp;<asp:Label id="aparrateoid" runat="server" Visible="False"></asp:Label> <asp:Label id="aparrate2oid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left><asp:GridView id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="oid,name" GridLines="None" PageSize="8" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name">
<HeaderStyle CssClass="gvhdr" Width="300px"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label13" 
        runat="server" ForeColor="Red" Text="No data found!!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left>Invoice &nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>&nbsp;</TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="invno" runat="server" Width="197px" CssClass="inpText" MaxLength="30"></asp:TextBox> <asp:ImageButton id="btnSearchinv" onclick="btnSearchPurchasing_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearinv" onclick="btnClearinv_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left>Amount <asp:Label id="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="amount" runat="server" Width="116px" CssClass="inpText" Font-Size="Small" MaxLength="10" AutoPostBack="True" size="20" OnTextChanged="amount_TextChanged"></asp:TextBox><asp:Label id="lblmaxpayment" runat="server">&lt;=</asp:Label> <asp:Label id="maxpayment" runat="server"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=4><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="refoid,invno,trndate,amttrans,amtpaid,acctgoid,payduedate,cmpcode,amtbalance,apaccount,currencyoid" GridLines="None" PageSize="8" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged" OnPageIndexChanging="gvPurchasing_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="invno" HeaderText="No. Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Jth Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Nota Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total Pembayaran">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbalance" HeaderText="Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="nodata" 
        runat="server" ForeColor="Red">Data Not Found!!</asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left>Debet COA</TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:DropDownList id="coadebet" runat="server" Width="349px" CssClass="inpTextDisabled" Font-Size="Small" Enabled="False"></asp:DropDownList></TD><TD align=left>Credit COA</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="coacredit" runat="server" Width="349px" CssClass="inpText" Font-Size="Small"></asp:DropDownList></TD></TR><TR><TD align=left>Note</TD><TD align=left>:</TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="note" runat="server" Width="345px" CssClass="inpText" Font-Size="Small" MaxLength="200" size="20" TextMode="MultiLine"></asp:TextBox></TD><TD vAlign=top align=left><asp:Label id="Label8" runat="server" Text="Status"></asp:Label></TD><TD vAlign=top align=left>:</TD><TD vAlign=top align=left><asp:Label id="LblPosting" runat="server" CssClass="Important" Text="IN PROCESS"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left></TD><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left></TD><TD style="WIDTH: 318px" align=left><ajaxToolkit:FilteredTextBoxExtender id="fteamt" runat="server" TargetControlID="amount" ValidChars="123456789.,0"></ajaxToolkit:FilteredTextBoxExtender> </TD><TD align=left></TD><TD align=left></TD><TD align=left><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="tgl" Format="dd/MM/yyyy" PopupButtonID="ImageButton1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="tgl"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left colSpan=3><asp:Label id="create" runat="server" Font-Bold="True"></asp:Label> <asp:Label id="update" runat="server" Font-Bold="True"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnsave" runat="server" ImageUrl="~/Images/Save.png"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton> <asp:ImageButton id="btnPost" onclick="btnPost_Click" runat="server" ImageUrl="~/Images/sendapproval.png"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png"></asp:ImageButton> <asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image> Please Wait ..... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" />
                            <strong><span>Form of Debet Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" GridLines="None" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333">
                                    <RowStyle CssClass="gvrow" BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

                                    <EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
                                    <Columns>
                                        <asp:BoundField DataField="acctgcode" HeaderText="Code">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="acctgdesc" HeaderText="Perkiraan">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="glnote" HeaderText="Note">
                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="debet" HeaderText="Debet">
                                            <HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="kredit" HeaderText="Credit">
                                            <HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>

                                    <AlternatingRowStyle CssClass="gvrowalternate" BackColor="White"></AlternatingRowStyle>
<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" Drag="True" PopupDragHandleControlID="lblPosting2" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> <BR /><asp:UpdatePanel id="upPopUpMsg" runat="server"><ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True">
                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
    </asp:UpdatePanel>
</td>
        </tr>
    </table>
</asp:Content>
