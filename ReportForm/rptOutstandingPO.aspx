<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptOutstandingPO.aspx.vb" Inherits="ReportForm_rptOutstandingPO" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="center" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan Outstanding PO :." Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 560px; HEIGHT: 72px"><TBODY><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right runat="server" visible="true"></TD><TD style="WIDTH: 454px; HEIGHT: 1px" colSpan=3 runat="server" visible="true"></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="tdPeriod1" align=right runat="server" visible="true"><asp:Label id="Label3" runat="server" Text="Periode : "></asp:Label></TD><TD style="WIDTH: 454px; HEIGHT: 1px" id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label1" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="imageButton1" TargetControlID="dateAwal">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="imageButton2" TargetControlID="dateAkhir">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date">
            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date">
            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right runat="server" visible="true"><asp:Label id="Label2" runat="server" Text="Type Laporan :"></asp:Label></TD><TD style="WIDTH: 454px; HEIGHT: 1px" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="type" runat="server" Width="154px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" id="Td2" align=right runat="server" visible="true">Nomor PO :</TD><TD style="WIDTH: 454px; HEIGHT: 1px" id="Td3" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="nota" runat="server" Width="151px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="oid" runat="server" Visible="False"></asp:Label><BR /><asp:GridView id="GVNota" runat="server" Width="450px" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelipono,suppname,suppoid" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1" UseAccessibleHeader="False">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:CommandField ShowSelectButton="True">
                    <HeaderStyle Font-Size="X-Small" Width="50px" />
                    <ItemStyle Font-Size="X-Small" ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                </asp:CommandField>
                <asp:BoundField DataField="trnbelipono" HeaderText="No Nota" />
                <asp:BoundField DataField="suppname" HeaderText="Supplier">
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="Silver" Font-Bold="True" />
            <EmptyDataTemplate>
                <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
            </EmptyDataTemplate>
            <SelectedRowStyle BackColor="Yellow" />
            <AlternatingRowStyle BackColor="White" />
    <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
        </asp:GridView> </TD></TR>
    <tr>
        <td runat="server" align="right" style="font-size: small; vertical-align: top; width: 35%;
            white-space: nowrap; text-align: right" visible="true">
            Group :</td>
        <td runat="server" align="left" colspan="3" style="width: 454px; height: 1px" visible="true">
            <asp:DropDownList ID="FilterDDLGrup" runat="server" CssClass="inpText" Width="405px">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td runat="server" align="right" style="font-size: small; vertical-align: top; width: 35%;
            white-space: nowrap; text-align: right" visible="true">
            Sub Group :</td>
        <td runat="server" align="left" colspan="3" style="width: 454px; height: 1px" visible="true">
            <asp:DropDownList ID="FilterDDLSubGrup" runat="server" CssClass="inpText" Width="405px">
            </asp:DropDownList></td>
    </tr>
    <TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right><asp:Label id="Label4" runat="server" Text="Barang :"></asp:Label></TD><TD style="WIDTH: 454px; HEIGHT: 1px" align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label> &nbsp;<BR /><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" UseAccessibleHeader="False" PageSize="12">
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:CommandField ShowSelectButton="True">
                    <HeaderStyle Font-Size="X-Small" Width="50px" />
                    <ItemStyle Font-Size="X-Small" ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                </asp:CommandField>
                <asp:BoundField DataField="itemcode" HeaderText="Kode Barang" />
                <asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang" />
                <asp:BoundField DataField="merk" HeaderText="Merk">
                    <HeaderStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False" />
                <asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False" />
                <asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False" />
                <asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False" />
                <asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False" />
                <asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False" />
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="Silver" Font-Bold="True" />
            <EmptyDataTemplate>
                <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
            </EmptyDataTemplate>
            <SelectedRowStyle BackColor="Yellow" />
            <AlternatingRowStyle BackColor="White" />
            <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
        </asp:GridView> </TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Merk :</TD><TD style="WIDTH: 454px; HEIGHT: 1px" align=left colSpan=3><asp:TextBox id="merk" runat="server" Width="250px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: small; VERTICAL-ALIGN: top; WIDTH: 35%; WHITE-SPACE: nowrap; TEXT-ALIGN: right" align=right>Supplier :</TD><TD style="WIDTH: 454px; HEIGHT: 1px" align=left colSpan=3><asp:TextBox id="suppname" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupplier" onclick="btnSearchSupplier_click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseSupplier" onclick="btnEraseSupplier_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="suppoid" runat="server" Visible="False">suppoid</asp:Label><asp:GridView id="gvSupp" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="suppcode,suppoid,suppname,supptype" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged1" UseAccessibleHeader="False" PageSize="12">
                <Columns>
                    <asp:CommandField ShowSelectButton="True">
                        <HeaderStyle Font-Size="X-Small" Width="50px" ForeColor="Red" />
                        <ItemStyle Font-Size="X-Small" ForeColor="Red" HorizontalAlign="Center" Width="50px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="suppcode" HeaderText="Kode Supplier" />
                    <asp:BoundField DataField="suppname" HeaderText="Nama Supplier" />
                    <asp:BoundField DataField="supptype" HeaderText="Tipe" />
                </Columns>
                <RowStyle BackColor="#F7F7DE" />
                <EmptyDataTemplate>
                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="Yellow" />
                <PagerStyle HorizontalAlign="Right" BackColor="Silver" Font-Bold="True" />
                <AlternatingRowStyle BackColor="White" />
            <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" />
            </asp:GridView> </TD></TR>
    <tr>
        <td align="right" style="font-size: small; vertical-align: top; width: 35%; white-space: nowrap;
            text-align: right">
            Taxable :</td>
        <td align="left" colspan="3" style="width: 454px; height: 1px">
            <asp:DropDownList ID="taxable" runat="server" CssClass="inpText" Width="64px">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>YES</asp:ListItem>
                <asp:ListItem>NO</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD align=center colSpan=4>&nbsp; &nbsp; <asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton><BR /><asp:UpdateProgress id="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <strong><span style="font-size: 14pt; color: #800080">Please Wait....</span></strong><br />
                    <asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress> </TD></TR><TR><TD colSpan=4 align="center"><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 91px" align=center colSpan=4>&nbsp; &nbsp; <TABLE><TBODY><TR><TD style="WIDTH: 100px" vAlign=top align=left><CR:CrystalReportViewer id="crvMutasiStock" runat="server" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="True"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

