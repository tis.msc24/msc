<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trninitdpap.aspx.vb" Inherits="Accounting_trninitdpap" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" Text=".: DP AP Balance" CssClass="Title"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: List of DP AP Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w29" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR id="trcaribranch" runat="server" visible="false"><TD>Branch</TD><TD><asp:DropDownList id="dd_branchCari" runat="server" Width="214px" CssClass="inpText" __designer:wfdid="w30" AutoPostBack="True">
                                                        </asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD><asp:DropDownList id="FilterDDL" runat="server" Width="139px" CssClass="inpText" __designer:wfdid="w31">
                                                            <asp:ListItem Value="trndpapno">DP AP Balance No.</asp:ListItem>
                                                            <asp:ListItem Value="suppname">Supplier</asp:ListItem>
                                                            <asp:ListItem Value="payreftype">Tipe Pembayaran</asp:ListItem>
                                                        </asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w32"></asp:TextBox> </TD></TR><TR><TD id="TD1" runat="server" Visible="false">Periode </TD><TD id="TD2" runat="server" Visible="false"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w33"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton> -<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w35"></asp:TextBox> <asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w37" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date">
                                                        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w38" Format="dd/MM/yyyy" PopupButtonID="imageButton1" TargetControlID="dateAwal">
                                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w39" Format="dd/MM/yyyy" PopupButtonID="imageButton2" TargetControlID="dateAkhir">
                                                        </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w40" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date">
                                                        </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD>Status</TD><TD><asp:DropDownList id="statuse" runat="server" Width="119px" CssClass="inpText" __designer:wfdid="w41">
                                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                            <asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
                                                            <asp:ListItem>POST</asp:ListItem>
                                                        </asp:DropDownList> &nbsp;<asp:ImageButton id="imbFindBoM" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w42" AlternateText="Find BoM"></asp:ImageButton> <asp:ImageButton id="imbAllBoM" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w43" AlternateText="View All BoM"></asp:ImageButton> </TD></TR><TR><TD></TD><TD><asp:ImageButton id="btnCheckAll0" runat="server" ImageUrl="~/Images/selectall.png" Visible="False" __designer:wfdid="w44" AlternateText="SELECT ALL"></asp:ImageButton> <asp:ImageButton id="btnUncheckAll0" runat="server" ImageUrl="~/Images/selectnone.png" Visible="False" __designer:wfdid="w45"></asp:ImageButton> <asp:ImageButton id="btnPosting3" runat="server" ImageUrl="~/Images/postselect.png" Visible="False" __designer:wfdid="w46"></asp:ImageButton> </TD></TR><TR><TD colSpan=2><asp:GridView id="gvMst" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w47" EmptyDataText="No data in database." GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="trndpapoid,cmpcode" EnableModelValidation="True" AllowSorting="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
                                                        <asp:CheckBox ID="cbPosting" runat="server" __designer:wfdid="w9" ToolTip="<%# GetIDCB() %>"></asp:CheckBox>
                                                    
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:HyperLinkField DataNavigateUrlFields="trndpapoid,cmpcode" DataNavigateUrlFormatString="trninitdpap.aspx?oid={0}&amp;cmpcode={1}" DataTextField="trndpapno" HeaderText="DP AP No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="outlet" HeaderText="Branch">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payreftype" HeaderText="Tipe Pembayaran">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data not found !!" __designer:wfdid="w2"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> <TABLE style="WIDTH: 741px" id="TB" runat="server" visible="false"><TBODY><TR><TD id="TD3" align=left colSpan=2 runat="server" Visible="false"><asp:Button id="btnAll" runat="server" Width="75px" CssClass="gray" Font-Bold="True" Text="VIEW ALL" __designer:wfdid="w48"></asp:Button> <asp:Button id="btnSearch" runat="server" Width="75px" CssClass="orange" Font-Bold="True" Text="FIND" __designer:wfdid="w49"></asp:Button> <asp:Button id="btnCheckAll" runat="server" CssClass="green" Font-Bold="True" Text="SELECT ALL" __designer:wfdid="w50"></asp:Button> <asp:Button id="btnUncheckAll" runat="server" CssClass="red" Font-Bold="True" Text="SELECT NONE" __designer:wfdid="w51"></asp:Button> <asp:Button id="btnPosting" onclick="curroid_SelectedIndexChanged" runat="server" CssClass="orange" Font-Bold="True" Text="POST SELECTED" __designer:wfdid="w52"></asp:Button></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="trndpapoid" runat="server" Visible="False" __designer:wfdid="w263"></asp:Label></TD><TD align=left></TD><TD align=left colSpan=3><ajaxToolkit:CalendarExtender id="cepayduedate" runat="server" __designer:wfdid="w264" Format="dd/MM/yyyy" PopupButtonID="imbDueDate" TargetControlID="payduedate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cDpapDate" runat="server" __designer:wfdid="w265" Format="dd/MM/yyyy" PopupButtonID="imbDPAPDate" TargetControlID="trndpapdate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskDpapDate" runat="server" __designer:wfdid="w266" TargetControlID="trndpapdate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meepayduedate" runat="server" __designer:wfdid="w267" TargetControlID="payduedate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD id="TD7" align=left runat="server" Visible="false">Branch</TD><TD id="TD6" align=left runat="server" Visible="false">:</TD><TD id="TD5" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="dd_branch" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w274"></asp:DropDownList></TD></TR><TR><TD align=left>Date</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapdate" runat="server" Width="72px" CssClass="inpTextDisabled" __designer:wfdid="w268" ReadOnly="True" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDPAPDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w269"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w270"></asp:Label> <asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w271"></asp:Label></TD></TR><TR><TD align=left>DP No<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w272"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w273" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left>Supplier</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="suppname" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w275" MaxLength="50"></asp:TextBox> <asp:Label id="suppoid" runat="server" Visible="False" __designer:wfdid="w276"></asp:Label> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w277"></asp:ImageButton> <asp:ImageButton style="WIDTH: 14px" id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w278"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="gvSupp" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w279" OnSelectedIndexChanged="gvSupp_SelectedIndexChanged" EmptyDataText="No data found" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="suppoid,suppname" EnableModelValidation="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Supplier Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="True" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Account DP</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="trndpapacctgoid" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w280"></asp:DropDownList></TD></TR><TR><TD align=left>Payment Type</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="payreftype" runat="server" Width="132px" CssClass="inpText" __designer:wfdid="w281" AutoPostBack="True">
                                                        <asp:ListItem>CASH</asp:ListItem>
                                                        <asp:ListItem>NONCASH</asp:ListItem>
                                                        <asp:ListItem>GIRO</asp:ListItem>
                                                    </asp:DropDownList></TD></TR><TR><TD align=left>Currency</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="curroid" runat="server" CssClass="inpText" __designer:wfdid="w282" AutoPostBack="True"></asp:DropDownList> &nbsp;<asp:TextBox id="currate" runat="server" Width="70px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w283" Enabled="False"></asp:TextBox> </TD></TR><TR id="tr1" runat="server"><TD id="TD9" align=left runat="server" Visible="false">Jatuh Tempo&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="*" __designer:wfdid="w284"></asp:Label></TD><TD id="TD8" align=left runat="server" Visible="false">:</TD><TD id="TD10" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w285"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w286"></asp:ImageButton> <asp:Label id="lblDate" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w287"></asp:Label></TD></TR><TR id="tr2" runat="server"><TD align=left>Ref No.&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w288"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="payrefno" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w289" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left>Amount</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapamt" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w290" MaxLength="15" AutoPostBack="True"></asp:TextBox>&nbsp;&nbsp; </TD></TR><TR><TD vAlign=top align=left>Note</TD><TD vAlign=top align=left>:</TD><TD vAlign=top align=left colSpan=3><asp:TextBox id="trndpapnote" runat="server" Width="375px" Height="33px" CssClass="inpText" __designer:wfdid="w291" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpapstatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w292" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left colSpan=5><asp:Label id="create" runat="server" Font-Bold="False" __designer:wfdid="w293"></asp:Label>&nbsp;<asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w294"></asp:Label></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton style="MARGIN-RIGHT: 10px; HEIGHT: 23px" id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w295"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w296"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w297"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w298"></asp:ImageButton></TD></TR><TR><TD id="TD4" align=left colSpan=5 runat="server" Visible="false"><asp:Button id="btnSave" runat="server" Width="75px" CssClass="green" Font-Bold="True" Text="SAVE" __designer:wfdid="w299"></asp:Button> <asp:Button id="btnCancel" runat="server" Width="75px" CssClass="gray" Font-Bold="True" Text="CANCEL" __designer:wfdid="w300"></asp:Button> <asp:Button id="btnPosting2" runat="server" Width="75px" CssClass="orange" Font-Bold="True" Text="POSTING" __designer:wfdid="w301"></asp:Button> <asp:Button id="btnDelete" runat="server" Width="75px" CssClass="red" Font-Bold="True" Text="DELETE" __designer:wfdid="w302"></asp:Button></TD></TR><TR><TD align=left colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w303"><ProgressTemplate>
<asp:Image id="ImageLoading" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w304"></asp:Image> &nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="fteamt" runat="server" __designer:wfdid="w305" TargetControlID="trndpapamt" ValidChars="123456789.,0"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                            
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: Form DP AP Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>

    </table>

    
                               <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBox" Visible="False" DefaultButton="btnMsgBoxOK">
                <table cellspacing="1" cellpadding="1" border="0">
                    <tbody>
                        <tr>
                            <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                            </td>
                            <td class="Label" valign="top" align="left">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                                <asp:ImageButton ID="btnMsgBoxOK" OnClick ="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
