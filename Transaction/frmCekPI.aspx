<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmCekPI.aspx.vb" Inherits="frmCekPI"
    Title="PT. MULTI SARANA COMPUTER - Cek Nota Hutang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table class="tabelhias" width="100%">
        <tr>
            <th class="header">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Cek Hutang Per Nota"></asp:Label></th>
        </tr>
    </table>

    <div style="text-align: left;">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
            Font-Size="9pt">
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 100px">Cabang</TD><TD colSpan=5><asp:DropDownList id="CabangDDL" runat="server" CssClass="inpText">
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 100px">No. Nota</TD><TD colSpan=5><asp:TextBox id="txtorderNo" runat="server" Width="120px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchBPM" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseBPM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="ordermstoid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 100px"></TD><TD colSpan=5><asp:GridView id="gvBPM2" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." DataKeyNames="branch_code,orderoid,orderno,custoid,custname,deliverydate,orderstatus,periodacctg,AmtPot,AmtDP,amtBayar,amtRetur,AmtNetto,DiscUmum,DiscIn,AmtSisa,NotaManual,AmtJualNya" Font-Underline="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="No. Nota PI">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="deliverydate" HeaderText="Tanggal">
<HeaderStyle Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="NotaManual" HeaderText="Nota Manual" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AmtSisa" HeaderText="Amt Sisa" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 100px"><asp:Label id="Label4" runat="server" Width="87px" Text="Tanggal Nota"></asp:Label></TD><TD colSpan=5><asp:TextBox id="tglAwal2" runat="server" Width="78px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 100px">Customer</TD><TD colSpan=5><asp:TextBox id="custname" runat="server" Width="220px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;</TD></TR><TR><TD colSpan=6><asp:Button id="btnCloseBPM" runat="server" CssClass="btn green" Text="Info Nota" Visible="False"></asp:Button>&nbsp;<asp:Button id="btnInfoTrn" runat="server" CssClass="btn red" Text="Info Transaksi" Visible="False"></asp:Button></TD></TR><TR><TD colSpan=6>
<HR />
</TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 500px" colSpan=4><asp:Label id="Label1" runat="server" Width="232px" Height="9px" Font-Bold="True" Text="Transaksi Rp."></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD colSpan=6></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label12" runat="server" Width="97px" Text="Amount Bruto :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="AmtBruto" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label8" runat="server" Text="Discount :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="DiscUmum" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD style="WIDTH: 100px" id="TD1" align=right runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Disc Intern :"></asp:Label></TD><TD style="WIDTH: 467px" id="TD3" colSpan=4 runat="server" Visible="false"><asp:Label id="DiscInt" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" id="TD2" colSpan=1 runat="server" Visible="false"></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label3" runat="server" Text="Amount Netto :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="TotNota" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1><asp:Label id="AmtDisc" runat="server" Height="3px" Text="0.0" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label7" runat="server" Text="Bayar Nota :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="AmtBayar" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label9" runat="server" Text="Titipan :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="AmtDP" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label11" runat="server" Text="Retur :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="AmtRet" runat="server" Text="0.00"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD align=right colSpan=6>
<HR />
</TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="Label18" runat="server" Text="Sisa :"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=4><asp:Label id="AmtSisaNett" runat="server" Text="0.0"></asp:Label></TD><TD style="WIDTH: 467px" colSpan=1></TD></TR><TR><TD colSpan=6>
<HR />
</TD></TR><TR><TD colSpan=6><asp:GridView id="gv_item" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." DataKeyNames="itemcode,qty,unit" Font-Underline="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="PriceItem" HeaderText="Price/unit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtjualnetto" HeaderText="Netto/Unit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="562949953421381" __designer:wfdid="w1" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="562949953421382">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="562949953421383"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="562949953421384"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="562949953421385"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:dtid="562949953421386" __designer:wfdid="w1"></asp:Image><BR __designer:dtid="562949953421387" />Please Wait .....</SPAN><BR __designer:dtid="562949953421388" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
<asp:UpdatePanel ID="upListMat" runat="server">
<ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">Catatan Transaksi Nota</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListNya" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="branch_code,gendesc,trnOid,notrans,trnDate,trnAmtnya,amtBayarNya" GridLines="None" PageSize="5" OnRowDataBound="gvListNya_RowDataBound" EnableModelValidation="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="notrans" HeaderText="No. Transasksi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnDate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnAmtnya" HeaderText="Amt Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtBayarNya" HeaderText="Amt Bayar/Amt Retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3>&nbsp;&nbsp; <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="lbCloseListMat" />
</Triggers>
</asp:UpdatePanel>
        <asp:UpdatePanel ID="updPanel2" runat="server">
            <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w113"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header" __designer:wfdid="w114"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w115"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="isiPesan" runat="server" ForeColor="Red" __designer:wfdid="w116"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:Button id="btnErrOK" onclick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " __designer:wfdid="w117" UseSubmitBehavior="False"></asp:Button> </TD></TR></TBODY></TABLE></asp:Panel> &nbsp; <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" __designer:wfdid="w118" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi" TargetControlID="PanelValidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtenderValidasi" runat="server" Visible="False" __designer:wfdid="w119"></asp:Button> 
</ContentTemplate>
        </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <strong><span style="font-size: 9pt">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
Form Cek Hutang Per Nota :.</span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer><br />
        <br />
        &nbsp;</div>
</asp:Content>

