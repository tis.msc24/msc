Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_DailyBankReport
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvAcctg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvAcctg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "acctgoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvAcctg.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    'Public Function GetSessionCheckAll() As Boolean
    '    If Session("CheckAll") = True Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyCode
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        'If FilterDDLAccount.SelectedValue = "" Then
        '    showMessage("Please select Account first!", 2)
        '    Exit Sub
        'End If
        crvReportForm.ReportSource = Nothing
        Try
            If DDLType.SelectedValue = "Summary" Then
				If FilterCurrency.SelectedValue.ToUpper <> "VALAS" Then
					If sType = "Print Excel" Then
                        report.Load(Server.MapPath(folderReport & "rptDailyBank_xls.rpt"))
					Else
						report.Load(Server.MapPath(folderReport & "rptDailyBank.rpt"))
					End If
				Else
					report.Load(Server.MapPath(folderReport & "rptDailyBankValas.rpt"))
				End If

                Dim sPeriod As String = ""
                Dim acctgoid As String = ""
                Dim sActgoid As String = ""
                Dim arCode() As String = AcctgName.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    acctgoid &= " AND oid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                    sActgoid &= " Where a.acctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                Else
                    showMessage("Silahkan pilih account terlebih dahulu!", 2)
                    Exit Sub
                End If
                Dim sWhere As String = "WHERE CompCode='" & CompnyCode & "' AND branch_code LIKE '" & FilterDDLDiv.SelectedValue & "'"
                
                'Dim iAcctgOid As Integer = CInt(FilterDDLAccount.SelectedValue)
                Dim sAnd As String = ""

                If IsValidPeriod() Then
                    sPeriod = Format(CDate(toDate(FilterPeriod1.Text)), "dd MMMM yyyy") & " s/d " & Format(CDate(toDate(FilterPeriod2.Text)), "dd MMMM yyyy")
                    sWhere &= " AND Tanggal BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"
                    report.SetParameterValue("sBranch", FilterDDLDiv.SelectedValue)
                    sAnd = " where d.branch_code LIKE '" & FilterDDLDiv.SelectedValue & "' " & acctgoid & " and d.Tanggal <=DATEADD(DAY,-1,'" & CDate(toDate(FilterPeriod1.Text)) & "') and Oid = a.acctgoid"
                Else
                    Exit Sub
                End If
                Dim sWhereBranch As String = sActgoid

                report.SetParameterValue("sCompnyName", "MULTI SARANA COMPUTER")
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sBranch", FilterDDLDiv.SelectedValue)
                report.SetParameterValue("sAnd", sAnd)
                report.SetParameterValue("sAcctgoid", sActgoid)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("sWhereBranch", sWhereBranch)
                'report.SetParameterValue("iAcctgOid", iAcctgOid)
                report.SetParameterValue("sTgl", CDate(toDate(FilterPeriod1.Text)))
                report.SetParameterValue("sCurrency", FilterCurrency.SelectedValue)
                report.SetParameterValue("cmpcode", "MSC")
                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                'report.PrintOptions.PaperSize = PaperSize.PaperA4
                If FilterCurrency.SelectedValue.ToUpper = "VALAS" Then
                    report.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    report.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
            Else
                Dim sTipe As String = ""
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptDailyBankDtl_xls.rpt"))
                    sTipe = "XLS"
                Else
                    report.Load(Server.MapPath(folderReport & "rptDailyBankDtl.rpt"))
                    sTipe = "PDF"
                End If

                Dim sPeriod As String = ""
                Dim acctgoid As String = ""
                Dim glacctgoid As String = ""
                Dim arCode() As String = AcctgName.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next

                If sCodeIn <> "" Then
                    acctgoid &= " AND cb.acctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                    glacctgoid &= " a.acctgoid IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                Else
                    showMessage("Silahkan pilih account terlebih dahulu!", 2)
                    Exit Sub
                End If
                Dim sWhere As String = " WHERE cb.cmpcode='" & CompnyCode & "' AND cb.branch_code LIKE '" & FilterDDLDiv.SelectedValue & "' " & acctgoid & ""

                Dim sAnd As String = ""
                Dim sAnd2 As String = ""

                If IsValidPeriod() Then
                    sPeriod = Format(CDate(toDate(FilterPeriod1.Text)), "dd/MM/yyyy") & " s/d " & Format(CDate(toDate(FilterPeriod2.Text)), "dd/MM/yyyy")
                    sWhere &= " AND cb.Tanggal BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "'"

                    sAnd &= "AND gm.gldate <=dateadd(day,-1,'" & CDate(toDate(FilterPeriod1.Text)) & " 0:0:0') "
                    sAnd2 = " where cb.branch_code LIKE '" & FilterDDLDiv.SelectedValue & "' " & acctgoid & " and cb.Tanggal <=DATEADD(DAY,-1,'" & CDate(toDate(FilterPeriod1.Text)) & "') and acctgoid = a.acctgoid"
                Else
                    Exit Sub
                End If

                sSql = "DECLARE @cmpcode VARCHAR(10);SET @cmpcode='" & CompnyCode & "';" & _
"select *,'IDR' AS currency from (SELECT branch_code, cabang,cb.cmpcode,cb.acctgoid,Tanggal,[Nomor BuKti],Keterangan,ISNULL([Kode Supp/Cust],'-') [Kode Supp/Cust],[Nama Supp/Cust],[Kode PerK. TransaKsi],[Ket. PerK. TransaKsi],[Kode PerK. Lawan],[Ket. PerK. Lawan],[Mata Uang],[Saldo Awal],[Amt. Debet IDR],[Amt. Credit IDR],[Amt. Debet Valas],[Amt. Credit Valas],Urutan, 'MULTI SARANA COMPUTER' [Business Unit]" & _
"FROM (SELECT '" & FilterDDLDiv.SelectedValue & "' branch_code, '" & IIf(FilterDDLDiv.SelectedValue = "ALL", "ALL", FilterDDLDiv.SelectedValue) & "' AS cabang ,'" & CompnyCode & "' cmpcode, a.acctgoid AS acctgoid, '" & CDate(toDate(FilterPeriod1.Text)) & "' [Tanggal], 'Saldo Awal' [Nomor BuKti], '' [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], a.acctgcode [Kode PerK. TransaKsi], a.acctgdesc [Ket. PerK. TransaKsi], '' [Kode PerK. Lawan], '' [Ket. PerK. Lawan], 'IDR' [Mata Uang], ISNULL(gl.amtopenidr,0.0) + isnull((select SUM(cb.[Amt. Credit IDR]) from( " & _
"/*MUTATION IN*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], cashbanKglamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST'  AND cashbankgroup in ('MUTATION','MUTATIONTO') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*RECEIPT,APK*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], cashbanKglamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST'  AND cashbankgroup not in ('MUTATION','MUTATIONTO','AR','R_DPAR','R_DPAP') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*AR PAYMENT*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], payamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' and cashbankgroup = 'AR' /*AND ap.payflag <> 'OTHER'*/ AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AP*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], trndpapamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.cashbankacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.trndpapacctgoid " & _
"INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AP RETUR*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], trndpapamt [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap_back ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid INNER JOIN QL_mstacctg acc ON ap.cashbankacctgoid=acc.acctgoid " & _
"INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.trndpapacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*REALISASI GIRO IN*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], 0.0 [Amt. Debet IDR], gm.giroamt [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_GiroPaymentMst gm ON gm.cashbankoid=cb.cashbankoid /*INNER JOIN QL_GiroPaymentDtl gd ON gd.GiroPaymentMstOid = gm.GiroPaymentMstOid */" & _
"INNER JOIN QL_mstacctg acc ON acc.acctgoid= gm.InAcctgOid /*INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=gd.giroacctgoid*/ " & _
"INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid  WHERE cb.cashbanKtype='RGM' AND cb.cashbanKstatus='POST'  AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AR*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], trndparamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.trndparacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid " & _
"INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ and ap.flagdp NOT IN ('PENGAKUAN') UNION ALL " & _
"/*HR PAYMENT*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],0.0 [Amt. Debet IDR], payamtidr [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayhrdtl ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_trnpayhrmst hm ON hm.cmpcode=ap.cmpcode AND hm.payhrmstoid=ap.payhrmstoid INNER JOIN QL_mstsupp s ON s.suppoid=hm.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' and cashbankgroup = 'PAYHR' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ " & _
") AS cb " & sAnd2 & "),0) - isnull((select sum(cb.[Amt. Debet IDR]) from (/*???*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstsupp s ON s.suppoid=(CASE WHEN cashbanKgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN pic ELSE 0 END) " & _
"INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*EXPENSE*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cashbankgroup NOT IN ('APK','ARK','AP','AR','MUTATION','R_DPAR','R_DPAP') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*MUTATION*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cashbankgroup IN ('MUTATION') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*AP PAYMENT*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],payamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid /*AND s.branch_code=ap.branch_code*/ " & _
"INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' /*AND ap.payflag <> 'OTHER' */AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ AND cashbankgroup='AP' UNION ALL " & _
"/*DP AP*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],trndpapamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap ap ON cb.cmpcode=ap.cmpcode  AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.cashbankacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.trndpapacctgoid " & _
"INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AR RETUR*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],ap.trndparamt [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar_back ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid INNER JOIN QL_trndpar dp ON dp.trndparoid = ap.trndparoid_ref AND dp.branch_code = cb.branch_code INNER JOIN QL_mstacctg acc ON ap.cashbankacctgoid=acc.acctgoid " & _
"INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.trndparacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*REALISASI GIRO OUT*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],gm.giroamt [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_GiroPaymentMst gm ON gm.cashbankoid=cb.cashbanKoid AND gm.cmpcode=cb.cmpcode AND cb.branch_code=gm.branch_code INNER JOIN QL_GiroPaymentDtl gd ON gd.GiroPaymentMstOid = gm.GiroPaymentMstOid INNER JOIN QL_mstacctg acc ON acc.acctgoid= gm.outAcctgOid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=gd.giroacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid  WHERE cb.cashbanKtype='RGK' AND cb.cashbanKstatus='POST'  AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AR*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal],trndparamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.cashbankacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid " & _
"INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ and ap.flagdp NOT IN ('PENGAKUAN') UNION ALL " & _
"/*AR KOREKSI*/ " & _
"SELECT cb.branch_code ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], payamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' and cashbankgroup = 'ARK'/*AND ap.payflag <> 'OTHER'*/ AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ ) AS cb " & sAnd2 & "),0) [Saldo Awal] " & _
", 0.0 [Amt. Debet IDR], 0.0 [Amt. Credit IDR], 0.0 [Amt. Debet Valas], 0.0 [Amt. Credit Valas], gl.crdgloid [Urutan] FROM QL_mstacctg a LEFT JOIN QL_crdgl gl ON gl.acctgoid=a.acctgoid AND gl.crdgloid<0 " & _
"AND gl.cmpcode=@cmpcode where /*gl.branch_code like '" & FilterDDLDiv.SelectedValue & "'*/ " & glacctgoid & " /*WHERE a.acctgoid=@acctgoid*/ UNION ALL " & _
"/*???*/" & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], gl.cashbankglnote [Keterangan], ISNULL(suppcode, '') [Kode Supp/Cust], ISNULL(suppname, '') [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], cashbanKglamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstsupp s ON s.suppoid=(CASE WHEN cashbanKgroup IN ('EXPENSE', 'EXPENSE GIRO') THEN pic ELSE 0 END) INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*EXPENSE*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], gl.cashbankglnote [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], cashbanKglamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid " & _
"INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cashbankgroup NOT IN ('APK','ARK','AP','AR','MUTATION','R_DPAR','R_DPAP') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*MUTATION*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], gl.cashbankglnote [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], cashbanKglamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], cashbanKglamt  [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cashbankgroup IN ('MUTATION') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*MUTATION IN/BBM*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], cb.cashbanknote [Keterangan], (Select cbm.cashbankno FROM QL_trncashbankmst cbm Where cbm.cashbankoid=cb.bankoid AND cbm.branch_code=cb.branch_code) [Kode Supp/Cust],'' [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], cashbanKglamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], cashbanKglamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.trnid=gl.trnid " & _
"AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST'  AND cashbankgroup in ('MUTATION','MUTATIONTO') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*RECEIPT,APK*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], gl.cashbankglnote [Keterangan], (case when cb.cashbankgroup = 'RECEIPT' then '' else (Select cbm.cashbankno FROM QL_trncashbankmst cbm Where cbm.cashbankoid=cb.bankoid AND cbm.branch_code=cb.branch_code) end) AS [Kode Supp/Cust],'' [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], cashbanKglamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], cashbanKglamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb " & _
"INNER JOIN QL_cashbanKgl gl ON cb.cmpcode=gl.cmpcode AND cb.cashbanKoid=gl.cashbanKoid AND cb.branch_code=gl.branch_code AND cb.trnid=gl.trnid INNER JOIN QL_mstacctg acc ON gl.acctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST'  AND cashbankgroup not in ('MUTATION','MUTATIONTO','AR','R_DPAR','R_DPAP') AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*AP PAYMENT*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], (select bm.trnbelino from ql_trnbelimst bm where bm.trnbelimstoid = ap.payrefoid and bm.branch_code = ap.branch_code ) +' - '+ ap.paynote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], payamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], payamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid " & _
"INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid " & _
"/*AND s.branch_code=ap.branch_code*/ " & _
"INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' /*AND ap.payflag <> 'OTHER'*/ AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ AND cashbankgroup='AP' UNION ALL " & _
"/*AR PAYMENT*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],(CASE WHEN ap.paynote like '%SI%' THEN (SELECT jm.trnjualno FROM QL_trnjualmst jm where jm.trnjualmstoid = ap.payrefoid and jm.branch_code = ap.branch_code) ELSE (SELECT jm.trnbiayaeksno FROM QL_trnbiayaeksmst jm where jm.trnbiayaeksoid = ap.payrefoid and jm.branch_code = ap.branch_code) END) [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], payamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], payamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid " & _
"INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' and cashbankgroup = 'AR'/*AND ap.payflag <> 'OTHER'*/ AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*AR KOREKSI*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],(SELECT jm.trnjualno FROM QL_trnjualmst jm where jm.trnjualmstoid = ap.payrefoid and jm.branch_code = ap.branch_code) [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], payamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], payamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid " & _
"INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' and cashbankgroup = 'ARK' /*AND ap.payflag <> 'OTHER'*/ AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AP*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.trndpapno + ' - ' + ap.trndpapnote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], trndpapamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], trndpapamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.trndpapacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.cashbankacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AP*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.trndpapno + ' - ' + ap.trndpapnote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], trndpapamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], trndpapamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.trndpapacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.cashbankacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AP RETUR*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti], ap.trndpapno + ' - ' + ap.trndpapnote [Keterangan], suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], trndpapamt [Amt. Credit IDR], 0.0 [Amt. Debet Valas], trndpapamt [Amt. Credit Valas], '01' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_trndpap_back ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid INNER JOIN QL_mstacctg acc ON ap.trndpapacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.cashbankacctgoid INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AR RETUR*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.trndparno + ' - ' + ap.trndparnote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], ap.trndparamt [Amt. Debet IDR], 0.0 [Amt. Credit IDR], ap.trndparamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar_back ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid INNER JOIN QL_trndpar dp ON dp.trndparoid = ap.trndparoid_ref AND cb.branch_code = dp.branch_code INNER JOIN QL_mstacctg acc ON ap.trndparacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=ap.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*RGK*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cb.cashbankdate [Tanggal], cb.cashbanKno [Nomor BuKti], gm.GiroNote [Keterangan], '' [Kode Supp/Cust], '' [Nama Supp/Cust], acc.acctgcode [Kode PerK. TransaKsi], acc.acctgdesc [Ket. PerK. TransaKsi], acc2.acctgcode [Kode PerK. Lawan], acc2.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], gm.giroamt [Amt. Debet IDR], 0.0 [Amt. Credit IDR],gm.GiroAmt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_GiroPaymentMst gm ON gm.cashbankoid=cb.cashbanKoid AND gm.cmpcode=cb.cmpcode AND cb.branch_code=gm.branch_code INNER JOIN QL_GiroPaymentDtl gd ON gd.GiroPaymentMstOid = gm.GiroPaymentMstOid INNER JOIN QL_mstacctg acc ON acc.acctgoid= gm.outAcctgOid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=gd.giroacctgoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid  WHERE cb.cashbanKtype='RGK' AND cb.cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*RGM*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cb.cashbankdate [Tanggal], cb.cashbanKno [Nomor BuKti], cb.cashbankno + ' ' + gid.GiroNote [Keterangan], (SELECT custcode FROM QL_mstcust where custoid = gid.CustOid) [Kode Supp/Cust], (SELECT custname FROM QL_mstcust where custoid = gid.CustOid) [Nama Supp/Cust], acc.acctgcode [Kode PerK. TransaKsi], acc.acctgdesc [Ket. PerK. TransaKsi], (select distinct acc2.acctgcode from QL_GiroPaymentDtl gd INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid = gd.GiroAcctgOid where gd.GiroPaymentMstOid = gid.GiroPaymentMstOid) [Kode PerK. Lawan], (select distinct acc2.acctgdesc from QL_GiroPaymentDtl gd INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid = gd.GiroAcctgOid where gd.GiroPaymentMstOid = gid.GiroPaymentMstOid) [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], gid.giroamt [Amt. Credit IDR], 0.0 [Amt. Debet Valas], gid.giroamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid INNER JOIN (select gm.CashBankOid, gm.InAcctgOid, gd.GiroPaymentMstOid, gd.GiroAmt, gm.GiroNote, gd.CustOid from QL_GiroPaymentDtl gd /*INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid = gd.paymentoid*/INNER JOIN QL_GiroPaymentMst gm ON gm.GiroPaymentMstOid = gd.GiroPaymentMstOid) gid ON gid.CashBankOid = cb.cashbankoid INNER JOIN QL_mstacctg acc ON acc.acctgoid= gid.InAcctgOid WHERE cb.cashbanKtype='RGM' AND cb.cashbanKstatus='POST' /*AND cb.cashbankacctgoid=@acctgoid*/ UNION ALL " & _
"/*DP AR*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.trndparno + ' - ' + ap.trndparnote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], trndparamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], trndparamt [Amt. Credit Valas], '01' [Urutan] FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.trndparacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ and ap.flagdp NOT IN ('PENGAKUAN') UNION ALL " & _
"/*DP AR*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.trndparno + ' - ' + ap.trndparnote [Keterangan], custcode [Kode Supp/Cust], custname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], trndparamtidr [Amt. Debet IDR], 0.0 [Amt. Credit IDR], trndparamt [Amt. Debet Valas], 0.0 [Amt. Credit Valas], '02' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_trndpar ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.trndparacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_mstcust s ON s.custoid=ap.custoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBK' AND cashbanKstatus='POST' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ and ap.flagdp NOT IN ('PENGAKUAN') UNION ALL " & _
"/*HR PAYMENT*/ " & _
"SELECT cb.branch_code, (select g.gendesc from ql_mstgen g where g.gencode = cb.branch_code and g.gengroup = 'CABANG') AS cabang ,cb.cmpcode, cb.cashbankacctgoid AS acctgoid, cashbankdate [Tanggal], cashbanKno [Nomor BuKti],ap.paynote [Keterangan], s.suppcode [Kode Supp/Cust], suppname [Nama Supp/Cust], acc2.acctgcode [Kode PerK. TransaKsi], acc2.acctgdesc [Ket. PerK. TransaKsi], acc.acctgcode [Kode PerK. Lawan], acc.acctgdesc [Ket. PerK. Lawan], currencycode [Mata Uang], 0.0 [Saldo Awal], 0.0 [Amt. Debet IDR], payamtidr [Amt. Credit IDR], 0.0 [Amt. Debet Valas], payamt [Amt. Credit Valas], '01' [Urutan] " & _
"FROM QL_trncashbanKmst cb INNER JOIN QL_trnpayhrdtl ap ON cb.cmpcode=ap.cmpcode AND cb.cashbanKoid=ap.cashbanKoid AND cb.branch_code=ap.branch_code INNER JOIN QL_mstacctg acc ON ap.payacctgoid=acc.acctgoid INNER JOIN QL_mstacctg acc2 ON acc2.acctgoid=cb.cashbankacctgoid INNER JOIN QL_trnpayhrmst hrm ON hrm.cmpcode=ap.cmpcode AND hrm.payhrmstoid=ap.payhrmstoid INNER JOIN QL_mstsupp s ON s.suppoid=hrm.suppoid INNER JOIN QL_mstcurr c ON c.currencyoid=cb.cashbankcurroid WHERE cashbanKtype='BBM' AND cashbanKstatus='POST' and cashbankgroup = 'PAYHR' AND cb.cmpcode=@cmpcode /*AND cb.cashbankacctgoid=@acctgoid*/ " & _
") AS cb " & sWhere & ") as tbl ORDER BY [Tanggal],[Urutan],[Nomor BuKti]"

                Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankmst")
                Dim dvTbl As DataView = dtTbl.DefaultView
                report.SetDataSource(dvTbl.ToTable)

                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("PrintUserName", Session("UserID"))
                report.SetParameterValue("sPeriod", sPeriod)
                report.SetParameterValue("sTipe", sTipe)

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                'report.PrintOptions.PaperSize = PaperSize.PaperA4
            End If

            If sType = "View" Then
                Session("ViewReport") = "True"
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DAILYBANKREPORT_" & Format(CDate(toDate(FilterPeriod1.Text)), "ddMMMyy").ToUpper & "_" & Format(CDate(toDate(FilterPeriod2.Text)), "ddMMMyy").ToUpper)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DAILYBANKREPORT_" & Format(CDate(toDate(FilterPeriod1.Text)), "ddMMMyy").ToUpper & "_" & Format(CDate(toDate(FilterPeriod2.Text)), "ddMMMyy").ToUpper)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
        End Try
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterDDLDiv, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterDDLDiv, sSql)
            Else
                FillDDL(FilterDDLDiv, sSql)
                FilterDDLDiv.Items.Add(New ListItem("ALL", "%"))
                FilterDDLDiv.SelectedValue = "%"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterDDLDiv, sSql)
            FilterDDLDiv.Items.Add(New ListItem("ALL", "%"))
            FilterDDLDiv.SelectedValue = "%"
        End If
    End Sub

    Private Function FillDDLAcctgWithAll(ByRef oDDLObject As DropDownList, ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "", Optional ByVal sNoneText As String = "") As Boolean
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        FillDDLAcctgWithAll = True
        oDDLObject.Items.Clear()

        Dim sCode As String = ""
        Dim sSql As String = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1<>'MSC' " & IIf(sCmpCode <> "%", " AND interfaceres1='" & sCmpCode & "'", "")
        Dim dtCode As DataTable = GetDataTable(sSql, "QL_mstinterface")
        If dtCode.Rows.Count < 1 Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
            sCode = GetStrData(sSql)
        Else
            For C0 As Integer = 0 To dtCode.Rows.Count - 1
                sCode += (dtCode.Rows(C0)("interfacevalue").ToString & ",")
            Next
            sCode = Left(sCode, sCode.Length - 1)
        End If

        If sCode <> "?" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            If sNoneText <> "" And sNoneValue <> "" Then
                FillDDLWithAdditionalText(oDDLObject, sSql, sNoneText, sNoneValue)
            Else
                FillDDL(oDDLObject, sSql)
            End If
        End If
        If oDDLObject.Items.Count = 0 Then
            FillDDLAcctgWithAll = False
        End If
        Return FillDDLAcctgWithAll
    End Function

    Public Sub BindDataListAcctg(ByVal sVar As String, ByVal sCmpCode As String, Optional ByVal sNoneValue As String = "")
        Try
            Dim cBrn As String = ""
            If FilterDDLDiv.SelectedValue.ToUpper <> "ALL" Then
                cBrn = "AND gl.branch_code='" & FilterDDLDiv.SelectedValue & "'"
            End If
            sSql = "SELECT DISTINCT 'False' AS Checkvalue, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.acctgflag='A' AND a.acctgoid IN (SELECT gl.acctgoid FROM QL_trngldtl gl WHERE a.acctgoid=gl.acctgoid AND a.cmpcode=gl.cmpcode " & cBrn & ")"
            Dim sCode As String = ""
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sVar & "' AND interfaceres1<>'MSC' " & IIf(sCmpCode <> "%", " AND interfaceres1='" & sCmpCode & "'", "")
            Dim dtCode As DataTable = GetDataTable(sSql, "QL_mstinterface")
            If dtCode.Rows.Count < 1 Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfaceres1='MSC' AND interfacevar='" & sVar & "'"
                sCode = GetStrData(sSql)
            Else
                For C0 As Integer = 0 To dtCode.Rows.Count - 1
                    sCode += (dtCode.Rows(C0)("interfacevalue").ToString & ",")
                Next
                sCode = Left(sCode, sCode.Length - 1)
            End If

            If sCode <> "?" Then
                sSql = "SELECT DISTINCT 'False' AS Checkvalue, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C1 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                    If C1 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
            End If
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvAcctg.DataSource = Session("TblMatView")
            gvAcctg.DataBind() : gvAcctg.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If
        If Session("branch_id") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptBank.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Daily Bank Report"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not Page.IsPostBack Then
            InitDDLBranch()
            FilterDDLDiv_SelectedIndexChanged(Nothing, Nothing)

            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        Else
            If Session("ViewReport") = "True" Then
                ShowReport("View")
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterDDLDiv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLDiv.SelectedIndexChanged
        acctgoid.Text = "" : AcctgName.Text = ""
        'FillDDLAcctgWithAll(FilterDDLAccount, "VAR_BANK", FilterDDLDiv.SelectedValue)
        Session("ViewReport") = "False" : crvReportForm.ReportSource = Nothing : crvReportForm.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Session("ViewReport") = "False"
        Response.Redirect("~\ReportForm\rptBank.aspx?awal=true")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLType.SelectedIndexChanged
        If DDLType.SelectedValue = "Summary" Then
            lbCurr.Visible = True : lbSeptCurr.Visible = True : FilterCurrency.Visible = True
        Else
            lbCurr.Visible = False : lbSeptCurr.Visible = False : FilterCurrency.Visible = False
        End If
    End Sub
#End Region

    Protected Sub btnSearchAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAcctg.Click
        FilterDDLListAcctg.SelectedIndex = 0
        FilterTextListAcctg.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvAcctg.DataSource = Nothing : gvAcctg.DataBind()
        BindDataListAcctg("VAR_BANK", IIf(FilterDDLDiv.SelectedValue = "ALL", "MSC", FilterDDLDiv.SelectedValue))
    End Sub

    Protected Sub btnEraseAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseAcctg.Click
        acctgoid.Text = ""
        AcctgName.Text = ""
    End Sub

    Protected Sub btnFindListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAcctg.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListAcctg.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListAcctg.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next

            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Data Account tidak ditemukan!"
                showMessage("Maaf, Data Account yang anda cari tidak ditemukan!", 2)
                mpeListMat.Show()
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAcctg.Click
        FilterDDLListAcctg.SelectedIndex = 0
        FilterTextListAcctg.Text = ""
        If UpdateCheckedMat() Then
            BindDataListAcctg("VAR_BANK", IIf(FilterDDLDiv.SelectedValue = "ALL", "MSC", FilterDDLDiv.SelectedValue))
            Session("TblMatView") = Session("TblMat")
            gvAcctg.DataSource = Session("TblMatView")
            gvAcctg.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddToListAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListAcctg.Click
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then

            If AcctgName.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= ";" & dv(C1)("acctgoid").ToString & ";"
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    AcctgName.Text &= dv(C1)("acctgoid").ToString & ";"
                Next
            End If


            If AcctgName.Text <> "" Then
                AcctgName.Text = Left(AcctgName.Text, AcctgName.Text.Length - 1)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            showMessage("- Maaf, Silahkan pilih Account, dengan beri centang pada kolom pilih!<BR>", 2)
        End If
    End Sub

    Protected Sub lbCloseListAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListAcctg.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvAcctg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAcctg.PageIndexChanging
        gvAcctg.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListAcctg.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListAcctg.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                'If ddlPIC.SelectedValue <> "NONE" Then
                '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
                'End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                showMessage("Data Account tidak ditemukan!", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Silahkan tampilkan data account terlebih dahulu!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "acctgoid=" & dtTbl.Rows(C1)("acctgoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvAcctg.DataSource = Session("TblMatView")
                gvAcctg.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Silahkan tampilkan data account terlebih dahulu!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub
End Class
