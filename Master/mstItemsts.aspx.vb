Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class mstItemsts
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private oRegex As Regex
    Dim oMatches As MatchCollection
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Private rptSupp As New ReportDocument
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Dim salesitem As String = ""
#End Region

#Region "Function"
    Function HargaExpedisi(ByVal p As Decimal, ByVal l As Decimal, ByVal t As Decimal, ByVal jalur As String) As Double
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As Double

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT price FROM mstexpedisibranch where panjang = " & p & " and lebar =" & l & " and tinggi=" & t & " and branchcode = '" & Session("branch_code") & "' and expedisi_type='" & jalur & "'"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)
        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read
                refcode = rdr("price") / (p * l * t)
            End While
        Else
            refcode = 0
        End If
        conn2.Close()
        Return refcode
    End Function

    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Function getHargaBeli(ByVal itemOid As String) As String()
        Dim retVal(3) As String
        sSql = _
        "select m.trnbelidate 'tgl',m.trnbelimstoid, d.trnbelidtloid, m.trnbelino 'nonota',g.gendesc 'satasli', " & _
        "((d.amtbelinetto-( " & _
        "( " & _
        "(d.amtbelinetto/(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid)*100) " & _
        "/100*(m.amtdischdr+m.amtdischdr2+m.amtdischdr3)) " & _
        "))+(m.trnamttax*( " & _
        "d.amtbelinetto/ " & _
        "	(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid) " & _
        "*100)/100))/d.trnbelidtlqty 'harga', " & _
        "i.satuan1,i.satuan2,i.satuan3,i.konversi1_2,i.konversi2_3,d.trnbelidtlunitoid, " & _
        "g2.gendesc 'SatBesar',g3.gendesc 'SatSedang',g4.gendesc 'SatKecil',s.suppname,i.itemdesc   " & _
        "from ql_trnbelimst m  " & _
        "inner join ql_trnbelidtl d on d.trnbelimstoid=m.trnbelimstoid  " & _
        "inner join ql_mstgen g on d.trnbelidtlunitoid=g.genoid  " & _
        "inner join ql_mstitem i on d.itemoid=i.itemoid  " & _
        "inner join ql_mstgen g2 on i.satuan1=g2.genoid  " & _
        "inner join ql_mstgen g3 on i.satuan2=g3.genoid  " & _
        "inner join ql_mstgen g4 on i.satuan3=g4.genoid  " & _
        "inner join ql_mstsupp s on m.trnsuppoid=s.suppoid  " & _
        "where i.itemoid='" & itemOid & "' and m.trnbelidate between '" & DateTime.Now.AddYears(-1).ToShortDateString & "' and '" & DateTime.Now.ToShortDateString & "'" & _
        "order by m.trnbelidate, m.trnbelimstoid, d.trnbelidtloid "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        If dtTmp.Rows.Count <= 0 Then
            ' belum ada pmebelian
            retVal(0) = "0"
            retVal(1) = "0"
            retVal(2) = "0"
        Else
            Dim targetRow As Integer = dtTmp.Rows.Count - 1

            ' ambil harga sat besar
            Dim hrgSatBesar As Double = 0
            If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi1_2")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3") * dtTmp.Rows(targetRow)("konversi1_2")
            End If

            Dim hrgSatSedang As Double = 0
            ' cek dulu apakah sat 1,2,3 sama semua
            If dtTmp.Rows(targetRow)("satuan1") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatSedang = 0
            Else
                ' ambil harga sat sedang
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3")
                End If
            End If

            Dim hrgSatKecil As Double = 0
            If dtTmp.Rows(targetRow)("satuan2") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatKecil = 0
            Else
                ' ambil harga sat kecil
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga")
                End If
            End If

            retVal(0) = hrgSatBesar
            retVal(1) = hrgSatSedang
            retVal(2) = hrgSatKecil
        End If

        Return retVal
    End Function

    Function isHargaUpdated(ByVal itemOid As String) As String
        Dim retVal As String = "PNA"
        Dim hargaHitung(3) As String
        hargaHitung = getHargaBeli(itemOid)

        ' cek apakah sebelumnya ada pembelian
        If hargaHitung(0) = "0" And hargaHitung(1) = "0" And hargaHitung(2) = "0" Then
            retVal = "PNA"
            GoTo selesai
        End If

        Dim hargaSekarang(3) As String
        sSql = "select itemoid, itemdesc, itempriceunit1, itempriceunit2, itempriceunit3, " & _
        "satuan1, satuan2, satuan3 " & _
        "from ql_mstitem where itemoid='" & itemOid & "' "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        Dim satuan1 As String = dtTmp.Rows(0)("satuan1").ToString()
        Dim satuan2 As String = dtTmp.Rows(0)("satuan2").ToString()
        Dim satuan3 As String = dtTmp.Rows(0)("satuan3").ToString()

        'If dtTmp.Rows.Count > 0 Then
        hargaSekarang(0) = dtTmp.Rows(0)("itempriceunit1")
        hargaSekarang(1) = dtTmp.Rows(0)("itempriceunit2")
        hargaSekarang(2) = dtTmp.Rows(0)("itempriceunit3")


        'For i As Integer = 0 To 2
        Dim dHargaSkg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaSekarang(0), "0"))
        Dim dHargaHtg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaHitung(0), "0"))
        If dHargaSkg1 <= dHargaHtg1 Then
            retVal = "FALSE"
            GoTo selesai
        Else
            retVal = "TRUE"
        End If
        'Next i

        If satuan1 = satuan2 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaSekarang(1), "0"))
            Dim dHargaHtg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaHitung(1), "0"))
            If dHargaSkg2 <= dHargaHtg2 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If


        If satuan2 = satuan3 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaSekarang(2), "0"))
            Dim dHargaHtg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaHitung(2), "0"))
            If dHargaSkg3 <= dHargaHtg3 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If
selesai:
        Return retVal
    End Function

    Function getTanda(ByVal itemOid As String) As String
        Dim retVal As String = ""
        'If itemOid = "1043" Then
        '    Dim aaa As String = "lalalala"
        'End If
        Dim hasil As String = isHargaUpdated(itemOid)
        If hasil = "TRUE" Then
            retVal = "V"
        ElseIf hasil = "FALSE" Then
            retVal = "X"
        ElseIf hasil = "PNA" Then
            retVal = "O"
        End If

        Return retVal
    End Function

    Function tandaHarga(ByVal tanda As String) As String
        Dim retVal As String = ""
        If tanda = "X" Then
            retVal = "<span style='color:#990000;font-weight:bolder;'>X</span>"
        ElseIf tanda = "V" Then
            retVal = "<span style='color:#009966;font-weight:bolder;'>V</span>"
        Else
            retVal = "<span style='color:#ff6600;font-weight:bolder;'>O</span>"
        End If
        Return retVal
    End Function

    Sub copyGridView()
        '' copy column from gridview to datatable
        'Dim dtOlah As DataTable = Session("dtPublic")
        'Dim i As Integer = 0

        'For i = 0 To GVmstgen.Rows.Count - 1
        '    Dim row As GridViewRow = GVmstgen.Rows(i)
        '    If row.RowType = DataControlRowType.DataRow Then
        '        Dim lbl As System.Web.UI.WebControls.Label = row.FindControl("lblStatusHarga")
        '        dtOlah.Rows(i)("infoharga") = lbl.Text
        '    End If
        'Next i

        '' update dtPublic
        'Session("dtPublic") = dtOlah

        'Dim zz As String = "123"
    End Sub

    Function formatToIDR(ByVal money As String) As String
        'Dim ci As New System.Globalization.CultureInfo("id-ID")
        'Return money.ToString("c", ci)
        If money.IndexOf(".") > 0 Then
            Dim depan As String = money.Substring(0, money.IndexOf("."))
            depan = depan.Replace(",", ".")
            Dim belakang As String = money.Substring(money.IndexOf(".") + 1, money.Length - (depan.Length + 1))
            Return depan & "," & belakang
        Else
            Return money.Replace(",", ".")
        End If
    End Function
#End Region

#Region "Procedure"

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "item_" & ID & ".jpg"
        Return fname
    End Function

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String)

        sSql = "SELECT top " & SelectTop.Text & " 1 as nomor, QL_mstitem.cmpcode, itemoid, itemcode, itemdesc,merk,QL_mstgen.gendesc as itemgroupoid, itemsafetystockunit1, ql_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as personoid,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid = QL_mstgen.genoid inner join QL_mstgen gsub on QL_mstitem.itemsubgroupoid = gsub.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid LEFT join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1 " & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " QL_mstitem.cmpcode='MSC' And itemflag = '" & ddlStatusView.SelectedValue & "' ORDER BY itemdesc"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
        Session("dtPublic") = GVmstgen.DataSource
    End Sub

    Public Sub BindDataCabang()
        ' this function will load ALL DATA at first time
        ' so call this function wisely.
        sSql = "select itm.itemoid,itm.itemdesc,itm.pricelist,cbg.branch_code,gen.gendesc branch_name, cbg.biayaexpedisi,(itm.pricelist+cbg.biayaexpedisi) pricelist_cbg,cbg.expedisi_type from ql_mstitem itm inner join ql_mstitem_branch cbg on itm.itemoid = cbg.itemoid inner join ql_mstgen gen on gen.gengroup='cabang' and gen.gencode = cbg.branch_code Where itm.itemoid = '" & Session("oid") & "'"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        GV_Branch.DataSource = objDs.Tables("data")
        GV_Branch.DataBind()
        Session("dtPublic") = GV_Branch.DataSource
    End Sub

    Public Sub findAData()
        ' search data
        Dim dtTmp As DataTable = Session("dtPublic")
        Dim dv As DataView = dtTmp.DefaultView

        If FilterDDL.SelectedValue <> "infoharga" Then
            dv.RowFilter = FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        End If

        Dim aa As Integer = 2
        GVmstgen.DataSource = dv.ToTable()
        GVmstgen.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As String)
        Try
            sSql = "SELECT mi.[cmpcode],mi.personoid,mi.[itemoid],mi.[itemcode],mi.[itemdesc],mi.[itemgroupoid],mi.[itemsubgroupoid],mi.itemtype,mi.merk,bottompricegrosir minimprice,mi.pricelist priceNormal,mi.[createuser],mi.[upduser],mi.[updtime], mi.[itemflag], mi.[acctgoid],mi.personoid, keterangan,statusitem,stockflag,mi.satuan1,has_SN,Isnull(itemref,'') itemref FROM [QL_mstitem] mi Where mi.cmpcode='" & CompnyCode & "' and mi.itemoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False

            While xreader.Read
                itemoid.Text = xreader("itemoid")
                itemcode.Text = xreader("itemcode")
                ItemRef.Text = xreader("itemref")
                itemdesc.Text = xreader("itemref")
                itemgroupoid.SelectedValue = xreader("itemgroupoid").ToString
                dd_type.SelectedValue = xreader("itemtype").ToString
                dd_merk.SelectedValue = xreader("merk").ToString.Trim
                satuan2.SelectedValue = xreader("satuan1").ToString
                spgOid.SelectedValue = xreader("personoid").ToString
                DDLStatusItem.SelectedValue = xreader("statusitem").ToString
                dd_stock.SelectedValue = xreader("stockflag")
                acctgoid.SelectedValue = xreader("acctgoid").ToString.Trim
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss")
                ddlStatus.SelectedValue = xreader("itemflag").ToString.Trim
                keterangan.Text = xreader("keterangan").trim
                btnDelete.Enabled = True
                If xreader("has_SN") = "1" Then
                    cb_sn.Checked = True
                End If
            End While
            xreader.Close()
            conn.Close()
            If Session("UserId") <> "admin" Then
                'spgOid.Enabled = False
            End If

        Catch ex As Exception
            showMessage(ex.Message & ex.ToString & sSql, "ERROR") : Exit Sub
        End Try

    End Sub

    Public Sub GenerateGenID()
        itemoid.Text = GenerateID("QL_mstitem", CompnyCode)
    End Sub

    Private Sub clearItem()
        itemoid.Text = "" : itemcode.Text = ""
        itemdesc.Text = "" : itemgroupoid.SelectedIndex = 0
        satuan2.SelectedIndex = 0 : ddlStatus.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
    End Sub

    Private Sub initDDL()
        Dim msg As String = ""

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemgroupoid, sSql)
        Else
            msg &= " - Isi Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"

        'itemtype
        sSql = "select genoid ,gendesc from ql_mstgen where cmpcode = '" & CompnyCode & "' and gengroup = 'ITEMTYPE'order by GENDESC"
        FillDDL(dd_type, sSql)

        'itemmerk
        sSql = "select gendesc ,g2.gendesc from ql_mstgen g2 where cmpcode = '" & CompnyCode & "' and gengroup = 'ITEMMERK' ORDER BY g2.GENDESC"
        FillDDL(dd_merk, sSql)

        sSql = "select acctgoid, acctgcode  + '-' + acctgdesc from QL_mstacctg where cmpcode ='" & CompnyCode & "' "
        FillDDL(acctgoid, sSql)

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMUNIT' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(satuan2, sSql)

        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' and PERSONSTATUS in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%AMP%' AND STATUS='AKTIF') AND PERSONNAME IN (Select USERNAME from QL_MSTPROF pf Where pf.USERNAME=PERSONNAME AND STATUSPROF='Active')"
        FillDDL(spgOid, sSql)

        If msg <> "" Then
            showMessage(msg, CompnyName & " - INFORMASI")
            btnSave.Visible = False
            Exit Sub
        End If

    End Sub

    Private Sub inituserID()
        salesitem = GetStrData("select personoid from QL_MSTPERSON where PERSONNAME =(select username from QL_MSTPROF where USERID='" & Session("UserID") & "') ")
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rptMstItem.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim sWhere As String = "" : Dim sFilter As String = ""

        sWhere = " Where QL_mstitem.cmpcode='MSC' AND upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' AND QL_mstitem.itemflag = '" & ddlStatusView.SelectedValue & "'"

        'If Session("UserId") <> "Admin" Then
        '    If salesitem = "" Then salesitem = 0
        '    sWhere &= " And QL_mstitem.personoid=" & salesitem & ""
        'End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If

        If JenisB.SelectedValue <> "ALL" Then
            sWhere &= " And QL_mstitem.stockflag='" & JenisB.SelectedValue & "'"
        End If

        If sFilter = "" Then
            sFilter = " ALL "
        End If

        rptReport.SetParameterValue("sWhere", sWhere)
        rptReport.SetParameterValue("sFilter", " Filter By : " & sFilter)
        rptReport.SetParameterValue("SelectTop", SelectTop.Text)
        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
        'Response.Redirect(Page.AppRelativeVirtualPath.ToString)
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = ""
        sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "

        'If Session("UserId") <> "Admin" Then
        '    If salesitem = "" Then salesitem = 0
        '    sWhere &= " And QL_mstitem.personoid=" & salesitem & ""
        'End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If

        If JenisB.SelectedValue <> "ALL" Then
            sWhere &= " And QL_mstitem.stockflag='" & JenisB.SelectedValue & "'"
        End If
        Response.Clear()

        If printType = "SUMMARY" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT top " & SelectTop.Text & " itemcode [Kode Katalog], itemdesc [Nama Ktalog],merk Merk,ql_mstitem.createuser [User Input], QL_mstitem.Upduser [User Edit], itemflag [Status Barang], personname as PIC,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End [Jenis Barang],QL_mstitem.hpp FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid = QL_mstgen.genoid inner join QL_mstgen gsub on QL_mstitem.itemsubgroupoid = gsub.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid LEFT join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1 " & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " QL_mstitem.cmpcode='" & CompnyCode & "' And itemflag = '" & ddlStatusView.SelectedValue & "' ORDER BY itemdesc"

        ElseIf printType = "DETAIL" Then

            Response.AddHeader("content-disposition", "inline;filename=mstitemdtl.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT itemoid, itemcode, itemdesc,merk, QL_mstgen.gendesc as itemgroup , g1.gendesc as Unit,bottompricegrosir Minim,QL_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as PIC,hpp FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid = QL_mstgen.genoid inner join QL_mstgen gsub on QL_mstitem.itemsubgroupoid =gsub.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1 inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 inner join ql_mstgen ggdtl on ggdtl.genoid = ql_mstitem.itemsubgroupoid inner join QL_mstgen gpay on gpay.genoid = QL_MSTITEM.payoid " & sWhere & " and itemoid = " & oid
        End If

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()

    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("branch_id") = branch_id
            Session("branch") = branch
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_code") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If

            Response.Redirect("mstItemsts.aspx?page=" & GVmstgen.PageIndex)
        End If

        Page.Title = CompnyName & " - Data Item"
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "javascript:return confirm('Anda yakin akan HAPUS data ini ?');")
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update"
        Else
            i_u.Text = "New"
            TabPanel3.Enabled = False
        End If

        If Not IsPostBack Then
            GVmstgen.PageIndex = Session("page")
            inituserID() : BindData("")
            BindDataCabang() : initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) : getUsername()
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update"
                cb_sn.Enabled = True
                'cb_sn.Text = "Type SN tidak dapat diupdate 2 kali"
            Else
                GenerateGenID()
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                GVmstgen.PageIndex = ToDouble(Request.QueryString("page"))
                GVmstgen.DataBind()
                lblupd.Text = "Create"
            End If

        End If
    End Sub
    'BUat paging
    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "

        'If Session("UserId") <> "Admin" Then
        '    If salesitem = "" Then salesitem = 0
        '    sWhere &= " And QL_mstitem.personoid=" & salesitem & ""
        'End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If

        If JenisB.SelectedValue <> "ALL" Then
            sWhere &= " And QL_mstitem.stockflag='" & JenisB.SelectedValue & "'"
        End If
        BindData(sWhere)
        Session("page") = GVmstgen.PageIndex
        'findAData()
    End Sub

    Protected Sub GV_Branch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Branch.PageIndexChanging
        GV_Branch.PageIndex = e.NewPageIndex
        BindDataCabang()
        Session("page") = GV_Branch.PageIndex
        TabContainer1.ActiveTabIndex = 2
        'findAData()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sWhere As String = ""
        sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "

        'If Session("UserId") <> "Admin" Then
        '    If salesitem = "" Then salesitem = 0
        '    sWhere &= " And QL_mstitem.personoid=" & salesitem & ""
        'End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If

        If JenisB.SelectedValue <> "ALL" Then
            sWhere &= " And QL_mstitem.stockflag='" & JenisB.SelectedValue & "'"
        End If
        BindData(sWhere)
        'findAData()
    End Sub

    Protected Sub getBranch()
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As Double
        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT gencode FROM ql_mstgen where gengroup = 'cabang' and cmpcode='" & CompnyName & "'"
        cmd.CommandType = CommandType.Text

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read

            End While
        Else
            refcode = 0
        End If
        conn2.Close()
    End Sub

    Protected Sub getUsername()
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT ISNULL(PERSONOID,0) PERSONOID FROM QL_MSTPROF prof INNER JOIN QL_MSTPERSON per ON prof.USERNAME = per.PERSONNAME WHERE PERSONNAME = '" & Session("UserId") & "'"
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read
                UserName.Text = rdr("PERSONOID").ToString
            End While
        Else
            UserName.Text = 0
        End If
        conn2.Close()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If itemoid.Text = "" Then
            showMessage("Silahkan Pilih Data Barang !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim sColomnName() As String = {"refoid", "itemoid", "itemoid"}
        Dim sTable() As String = {"ql_crdmtr", "QL_podtl", "QL_trnorderdtl"}
        If CheckDataExists(Session("oid"), sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from dbo.ql_crdmtr where refoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from QL_podtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from QL_trnorderdtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "delete from QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_MSTITEM_branch WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR") : Exit Sub
        End Try

        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah terhapus !", "INFORMASI")
        Response.Redirect("~\Master\mstItemsts.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "SUMMARY"
        imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoItem") = ""
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub itemdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemdesc.TextChanged
        ItemRef.Text = itemdesc.Text
        'itemdesc.Text = itemdesc.Text.Replace("'", "")
        'If (Session("oid") <> Nothing And Session("oid") <> "") = False Then
        '    generateItemId(itemdesc.Text)
        'End If
    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim namaItem, jumlahNol, c, c2, kodeItem As String
        jumlahNol = "" : Dim d As Integer = 0
        namaItem = Left(itemgroupoid.SelectedItem.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT isnull(max((replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "'"))
        c2 = ToDouble(GetStrData("SELECT isnull(max((replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If

        kodeItem = GenNumberString(namaItem, "", d, 5)
        itemcode.Text = kodeItem
    End Sub

    Protected Sub itempriceunit3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'itempriceunit3.Text = NewMaskEdit(ToDouble(itempriceunit3.Text))
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(2).Text = NewMaskEdit(ToDouble(e.Row.Cells(2).Text))
            'e.Row.Cells(3).Text = NewMaskEdit(ToDouble(e.Row.Cells(3).Text))
            'e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            'e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
        End If
    End Sub

    Protected Sub btnprintlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "DETAIL"
        imbPrintPDF.Visible = False
        orderNoForReport.Text = sender.ToolTip : orderIDForReport.Text = sender.CommandArgument()
        Session("NoItem") = sender.ToolTip
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "Barang_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstItemsts.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()

        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstItemsts.aspx?awal=true")
        End If
    End Sub

    Public Function viewFullImage(ByVal objPath As System.Web.UI.WebControls.Label) As String
        Dim sURL1() As String = Request.Url.ToString.Split("/")
        Dim sURL2 As String = ""
        For i As Integer = 0 To sURL1.Length - 3
            sURL2 &= (sURL1(i) & "/")
        Next
        Return ("<a href='" & sURL2 & "Images/" & objPath.Text & "' target='_blank'>[ Click Here to see Full Sized Image ]</a>")
    End Function

    Protected Sub FilterDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub itemgroupoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemgroupoid.SelectedIndexChanged
        'sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and (genother1='' or genother1='" & itemgroupoid.SelectedValue & "') and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        itemgroupoid.SelectedItem.Text = itemgroupoid.SelectedItem.Text.Replace("'", "")
        If (Session("oid") <> Nothing And Session("oid") <> "") = False Then
            generateItemId(itemgroupoid.SelectedItem.Text)
        End If
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImageButton2.Click
        Response.Redirect("~\Master\mstItemsts.aspx?awal=true")
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptSupp.Close() : rptSupp.Dispose()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If itemcode.Text.Trim = "" Then errMessage &= "- Isi Kode Barang Dahulu ! <BR>"
        If itemdesc.Text.Trim = "" Then errMessage &= "- Isi Nama Barang Dahulu ! <BR>"
        If dd_type.SelectedValue.Trim = "" Then errMessage &= "- Isi Merk Dahulu ! <BR>"
        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If
        Dim DescItem As String = ""
        If dd_merk.SelectedItem.Text = "NONE" Then
            DescItem = itemgroupoid.SelectedItem.Text & " " & itemdesc.Text
        ElseIf dd_merk.SelectedItem.Text = "NON" Then
            DescItem = itemgroupoid.SelectedItem.Text & " " & itemdesc.Text
        Else
            DescItem = itemgroupoid.SelectedItem.Text & " " & dd_merk.SelectedItem.Text & " " & itemdesc.Text
        End If

        'If Session("oid") <> Nothing Or Session("oid") <> "" Then 
        '    If ddlStatus.SelectedValue = "Tidak Aktif" Then
        '        sSql = "SELECT isnull(SUM(qtyIn),0)-isnull(sum(qtyOut),0) from QL_conmtr WHERE refoid = " & itemoid.Text & " and periodacctg='" & GetDateToPeriodAcctg(GetServerTime()).Trim & "'"
        '        Dim cekStok As Double = GetStrData(sSql)
        '        If cekStok <> 0 Then
        '            showMessage("Item tidak dapat non aktifkan ketika stok tidak sama dengan 0 (Nol)!", CompnyName & " - Warning")
        '            Exit Sub
        '        End If
        '    End If
        'End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_mstitem WHERE cmpcode='" & CompnyCode & "' And itemdesc = '" & Tchar(itemdesc.Text.Trim) & "' and merk = '" & Tchar(dd_type.SelectedValue.Trim) & "'"
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND itemoid <> " & itemoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Nama Barang dan Merk sudah dipakai ", CompnyName & " - WARNING")
            Exit Sub
        End If
        Dim sn As Integer = 0
        If cb_sn.Checked = True Then
            sn = 1
        End If
        Session("ItemOid") = GenerateID("QL_mstitem", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")

            If Session("oid") = Nothing Or Session("oid") = "" Then
                generateItemId(itemdesc.Text)
                Dim branch_id As String = String.Empty
                If cb_sn.Checked = True Then
                    sn = 1
                End If
                sSql = "INSERT INTO QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc],[itemgroupoid],[itemsubgroupoid],itemtype,merk,[itempriceunit1],[itempriceunit2],[satuan1],[itemsafetystockunit1],[acctgoid],[createuser],[upduser],[updtime],[itemflag], konversi1_2,personoid, keterangan, bottompricegrosir,itemsafetystockunitgrosir,pricelist,discountunit1,discountunit2,discountunit3,qty3_to2,qty3_to1,statusitem,stockflag,payoid,has_SN,konversi2_3,satuan2,satuan3,itemname,itemref) " & _
                " Values ('" & CompnyCode & "'," & Session("ItemOid") & ",'" & Tchar(itemcode.Text) & "','" & Tchar(DescItem.ToUpper) & "'," & itemgroupoid.SelectedValue & "," & 0 & "," & dd_type.SelectedValue & ",'" & Tchar(dd_merk.SelectedValue).ToUpper & "',0.0000,0.0000," & satuan2.SelectedValue & ",0.0000," & Tchar(acctgoid.SelectedValue) & ",'" & Session("UserID") & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(ddlStatus.SelectedValue) & "'," & 1 & "," & spgOid.SelectedValue & ",'" & Tchar(keterangan.Text.ToUpper) & "',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000," & 1 & "," & 1 & ",'" & Tchar(DDLStatusItem.SelectedValue.ToUpper) & "','" & dd_stock.SelectedValue.ToUpper & "',34,'" & sn & "',1," & satuan2.SelectedValue & "," & satuan2.SelectedValue & ",'" & Tchar(DescItem.ToUpper) & "','" & Tchar(ItemRef.Text.ToUpper) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim vhargExpedisi_Darat, vhargExpedisi_Laut, vhargExpedisi_Udara As Double
                vhargExpedisi_Darat = HargaExpedisi(1, 1, 1, "Darat")
                vhargExpedisi_Laut = HargaExpedisi(1, 1, 1, "Laut")
                vhargExpedisi_Udara = HargaExpedisi(1, 1, 1, "Udara")

                sSql = "Update QL_mstoid set lastoid=" & Session("ItemOid") + 1 & " Where tablename='QL_mstitem' and cmpcode ='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Insert Ke Branch
                Dim sqlstr As String = String.Empty
                Dim rdr As SqlDataReader : Dim cmd As New SqlCommand
                conn2.Open() : cmd.Connection = conn2
                sqlstr = "SELECT gencode FROM ql_mstgen Where gengroup = 'cabang' and cmpcode='" & CompnyName & "'"
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sqlstr : rdr = cmd.ExecuteReader
                If rdr.HasRows Then
                    While rdr.Read
                        Try
                            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type) values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Darat & ",'" & rdr("gencode").ToString & "','Darat')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Catch
                            Exit Try
                        End Try
                        Try
                            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Laut & ",'" & rdr("gencode") & "','Laut')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Catch
                            Exit Try
                        End Try
                        Try
                            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type) values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Udara & ",'" & rdr("gencode") & "','Udara')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Catch
                            Exit Try
                        End Try
                    End While
                End If

            Else
                sSql = "UPDATE QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                       "[itemname]='" & Tchar(DescItem.ToUpper) & "', [itemdesc]='" & Tchar(DescItem.ToUpper) & "', [itemgroupoid]=" & itemgroupoid.SelectedValue & ", [itemsubgroupoid]=" & 0 & ",[itemtype]=" & dd_type.SelectedValue & ",merk='" & Tchar(dd_merk.SelectedValue) & "'," & _
                       "[satuan1]=" & satuan2.SelectedValue & "," & _
                       "[satuan2]=" & satuan2.SelectedValue & "," & _
                       "[satuan3]=" & satuan2.SelectedValue & "," & _
                       "[qty3_to2]=" & ToDouble(1) & "," & _
                       "[qty3_to1]=" & ToDouble(1) & "," & _
                       "[acctgoid]='" & acctgoid.SelectedValue & "', " & _
                       "[statusitem]='" & DDLStatusItem.SelectedValue & "', " & _
                       " stockflag = '" & dd_stock.SelectedValue & "', " & _
                       "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp, [itemflag]='" & ddlStatus.SelectedValue & "' ,personoid = " & spgOid.SelectedValue & ", keterangan='" & Tchar(keterangan.Text) & "',has_SN='" & sn & "',itemref='" & Tchar(ItemRef.Text.ToUpper) & "' WHERE cmpcode like '%" & CompnyCode & "%' and itemoid=" & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try
        Session("oid") = Nothing
        Image1.ImageUrl = "~/images/information.png"
        Response.Redirect("~\Master\mstItemsts.aspx")
    End Sub
#End Region
End Class