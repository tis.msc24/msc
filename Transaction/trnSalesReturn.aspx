<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnSalesReturn.aspx.vb" Inherits="Transaction_trnSalesReturn" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Sales Return" Width="182px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp;List Of Sales Return</span><strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Cabang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="FCabangNya" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Filter</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="120px" CssClass="inpText"><asp:ListItem Value="a.trnjualreturno">No. Retur(SR)</asp:ListItem>
<asp:ListItem Value="a.refnotaretur">No. SI</asp:ListItem>
<asp:ListItem Value="a.orderno">No. SO</asp:ListItem>
<asp:ListItem Value="b.custname">Customer</asp:ListItem>
</asp:DropDownList><SPAN style="COLOR: #ff0000">&nbsp;<asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp; </SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px; HEIGHT: 22px"><asp:CheckBox id="CbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px; HEIGHT: 22px">:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 22px"><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="60px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;to <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="60px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></SPAN></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 94px">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 14px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilterstatus" runat="server" Width="123px" CssClass="inpText"><asp:ListItem Selected="True" Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD colSpan=3><asp:GridView id="GVTrn" runat="server" Width="100%" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" DataKeyNames="branch_code,trnjualreturmstoid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="gendesc" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualreturno" HeaderText="No. Retur (SR)">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refnotaretur" HeaderText="No. SI">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="No. SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" Text="Print Nota" CommandName='<%# Eval("branch_code") %>' Font-Underline="True" ToolTip='<%# String.Format("{0},{1}",Eval("trnjualreturmstoid"),Eval("trnjualreturno")) %>'></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="tbperiodstart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="tbperiodend" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender10" runat="server" TargetControlID="tbperiodstart" Format="dd/MM/yyyy" PopupButtonID="ibperiodstart"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="tbperiodend" Format="dd/MM/yyyy" PopupButtonID="ibperiodend"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVTrn"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp;Form of Sales Return</span>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=6><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="masterstate" runat="server" Visible="False">new</asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="ddlFromcabang" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD><asp:Label id="paytype" runat="server" Visible="False"></asp:Label> </TD><TD></TD><TD><asp:Label id="lblcurr" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD style="WIDTH: 96px">Type</TD><TD>:</TD><TD><asp:DropDownList id="DDLtype" runat="server" CssClass="inpText"><asp:ListItem Value="1">Potong Invoice</asp:ListItem>
<asp:ListItem Value="4">Jadikan DP</asp:ListItem>
</asp:DropDownList> </TD><TD>No.&nbsp;Retur</TD><TD>:</TD><TD><asp:TextBox id="noreturn" runat="server" Width="181px" CssClass="inpTextDisabled" MaxLength="50" Enabled="False"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">Customer</TD><TD>:</TD><TD><asp:TextBox id="custname" runat="server" Width="200px" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:ImageButton id="btncustomer" onclick="btncustomer_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnerasercustomer" onclick="btnerasercustomer_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label></TD><TD>Tanggal Retur</TD><TD>:</TD><TD colSpan=1><asp:TextBox id="returndate" runat="server" Width="70px" CssClass="inpTextDisabled" MaxLength="10" Enabled="False"></asp:TextBox><asp:ImageButton id="btnreturndate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:Label id="Label8" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px"></TD><TD></TD><TD><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="trncustoid,custcode,trncustname" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8" OnPageIndexChanging="gvCustomer_PageIndexChanging" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="CUSTCODE" HeaderText="Cust. Code" ReadOnly="True" SortExpression="CUSTCODE">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Cust. Name" SortExpression="trncustname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label 
        ID="lblstatusdataCust" runat="server" CssClass="Important" 
        Text="No Customer Data !"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD></TD><TD></TD><TD colSpan=1></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">No Invoice</TD><TD>:</TD><TD><asp:TextBox id="invoiceno" runat="server" Width="150px" CssClass="inpText" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchinvoice" onclick="btnsearchinvoice_Click" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseinvoice" onclick="btneraseinvoice_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="sjno" runat="server" Visible="False"></asp:Label><asp:Label id="sono" runat="server" Visible="False"></asp:Label><asp:Label id="JualMstOid" runat="server" Visible="False"></asp:Label><asp:Label id="typeSO" runat="server" Text="typeSO" Visible="False"></asp:Label><asp:Label id="branch_code" runat="server" Visible="False"></asp:Label></TD><TD>Invoice&nbsp;Date</TD><TD>:</TD><TD><asp:TextBox id="trdate" runat="server" Width="70px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px"><asp:Label id="spgoid" runat="server" Visible="False"></asp:Label> <asp:Label id="promooid" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD colSpan=4><asp:GridView id="GVTr" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="trnjualmstoid,trnsjjualno,orderno,trnpaytype,trncustoid,currencyoid,trnamttax,typeSO,branch_code,spgoid,trnamtjualidr,trnjualno,trnjualdate,custname" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderno" HeaderText="Sales Order No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualno" HeaderText="Sales Delivery No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeSO" HeaderText="typeSO" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
<asp:BoundField DataField="spgoid" HeaderText="spgoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label 
        ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">Total Return</TD><TD>:</TD><TD><asp:TextBox id="grossreturn" runat="server" Width="130px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> <asp:Label id="amtdiscdtl" runat="server" Visible="False"></asp:Label></TD><TD>No. Ref</TD><TD>:</TD><TD><asp:TextBox id="noref" runat="server" Width="125px" CssClass="inpText" MaxLength="20"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">Total Return Nett</TD><TD>:</TD><TD><asp:TextBox id="totalamount" runat="server" Width="130px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> <asp:Label id="returpiutangamount" runat="server" Visible="False">0.00</asp:Label></TD><TD id="TD1" Visible="true">Status</TD><TD id="TD3" Visible="true">:</TD><TD id="TD2" Visible="true"><asp:TextBox id="status" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">In Process</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px">Keterangan</TD><TD>:</TD><TD><asp:TextBox id="note" runat="server" Width="433px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD id="TD5" runat="server" Visible="false">Tax Amount</TD><TD id="TD4" runat="server" Visible="false">:</TD><TD id="TD6" runat="server" Visible="false"><asp:TextBox id="taxamount" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox><asp:Label id="taxpct" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px" id="TD9" runat="server" Visible="false">Curr</TD><TD id="TD7" runat="server" Visible="false">:</TD><TD id="TD8" runat="server" Visible="false"><asp:TextBox id="txtcurr" runat="server" Width="130px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD><asp:Label id="LblNettSO" runat="server" Width="87px" Text="Amt. Netto SO" Visible="False"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":" Visible="False"></asp:Label></TD><TD><asp:TextBox id="NettSO" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 96px"><SPAN style="FONT-SIZE: 10pt; COLOR: #ff0000"><STRONG><SPAN><asp:Label id="Label1" runat="server" Width="109px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Data :"></asp:Label></SPAN></STRONG></SPAN></TD><TD><SPAN style="FONT-SIZE: 10pt; COLOR: #ff0033"></SPAN></TD><TD><asp:DropDownList id="discheadertype" runat="server" Width="50px" CssClass="inpText" Visible="False" AutoPostBack="True">
                                                                                <asp:ListItem 
                        Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem 
                        Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList><asp:TextBox id="totaldischeader" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox><asp:TextBox id="discamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD></TD><TD></TD><TD><asp:TextBox id="discheader" runat="server" Width="125px" CssClass="inpText" Visible="False" AutoPostBack="True" MaxLength="15">0.00</asp:TextBox> </TD></TR><TR><TD style="WIDTH: 96px" align=left>
                                                                                Katalog</TD><TD>:</TD><TD><asp:TextBox id="itemname" runat="server" Width="313px" CssClass="inpText" MaxLength="500"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchitem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseitem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD>Location</TD><TD>:</TD><TD><asp:DropDownList id="fromlocation" runat="server" CssClass="inpText"></asp:DropDownList><asp:Label id="itemloc" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 96px"><asp:Label id="itemcode" runat="server" Visible="False"></asp:Label> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD><asp:GridView id="GVItem" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="itemoid,satuan1,unitseq,trnjualdtloid,trnjualdtlprice,promodisc,promodiscamt,itemloc,trnjualdtlqty,itemdesc,itemcode,unit,Sisaqty,QtyRet,promooid" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdtlqty" DataFormatString="{0:#,##0.00}" HeaderText="Qty SI">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyRet" DataFormatString="{0:#,##0.00}" HeaderText="Qty Retur">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Sisaqty" DataFormatString="{0:#,##0.00}" HeaderText="Sisa Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD><asp:Label id="trnjualdtloid" runat="server" Visible="False"></asp:Label><asp:Label id="unitseq" runat="server" Visible="False"></asp:Label><asp:Label id="lbldate" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD><asp:TextBox id="netamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD style="WIDTH: 96px">Qty Retur</TD><TD>:</TD><TD><asp:TextBox id="qty" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True" MaxLength="15">0.00</asp:TextBox>&nbsp;Max: <asp:Label id="maxqty" runat="server">0.00</asp:Label> <asp:Label id="unit" runat="server"></asp:Label><asp:Label id="satuan1" runat="server" Visible="False"></asp:Label> </TD><TD>Price</TD><TD>:</TD><TD><asp:TextBox id="priceperunit" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="15" Enabled="False">0.00</asp:TextBox>&nbsp;</TD></TR><TR><TD style="WIDTH: 96px">Disc/unit</TD><TD>:</TD><TD><asp:TextBox id="promodisc" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD>Total Disc.</TD><TD>:</TD><TD><asp:TextBox id="promodiscamt" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> </TD></TR><TR><TD style="WIDTH: 96px">Amount</TD><TD>:</TD><TD><asp:TextBox id="totalamountdtl" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> </TD><TD><asp:Label id="LblQtySO" runat="server" Text="Qty SO" Visible="False"></asp:Label></TD><TD><asp:Label id="Label3" runat="server" Text=":" Visible="False"></asp:Label></TD><TD><asp:TextBox id="QtySO" runat="server" Width="125px" CssClass="inpText" Visible="False" MaxLength="20">0.00</asp:TextBox></TD></TR><TR><TD style="WIDTH: 96px">Note</TD><TD>:</TD><TD colSpan=2><asp:TextBox id="notedtl" runat="server" Width="400px" CssClass="inpText" MaxLength="200"></asp:TextBox> <asp:Label id="labelseq" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD align=left><asp:ImageButton id="btnaddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsBottom"></asp:ImageButton> <asp:ImageButton id="btnclear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="detailstate" runat="server" Visible="False">new</asp:Label> </TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 950px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtl" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="itemoid,netamt,unitoid,maxqty,itemcode,unitseq,trnjualdtloid,locationoid,promodisc,promodiscamt,branch_code" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No ">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemname" HeaderText="Item Desc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty Retur">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyso" DataFormatString="{0:#,##0.00}" HeaderText="Qty SO">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promodiscamt" DataFormatString="{0:#,##0.00}" HeaderText="Discount">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="netamt" DataFormatString="{0:#,##0.00}" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="locationoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
                                                                                        Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=6><asp:Label id="costamount" runat="server" Visible="False">0</asp:Label><asp:Label id="cashamount" runat="server" Visible="False">0</asp:Label><asp:Label id="returamount" runat="server" Visible="False"></asp:Label><asp:Label id="amtppn" runat="server" Visible="False"></asp:Label><asp:Label id="amtpiutang" runat="server" Visible="False"></asp:Label><asp:Label id="amtpotpenjualan" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:Label id="lblcreate" runat="server"></asp:Label> <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label> <asp:Label id="createtime" runat="server" Font-Bold="True" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" onclick="ibdelete_Click" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapproval" runat="server" ImageUrl="~/Images/sendapproval.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibposting" runat="server" ImageUrl="~/Images/posting.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:FilteredTextBoxExtender id="MEQtySO" runat="server" TargetControlID="QtySO" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEQty" runat="server" TargetControlID="qty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="MEE30" runat="server" TargetControlID="returndate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEE1" runat="server" TargetControlID="discheader" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CE1" runat="server" TargetControlID="returndate" PopupButtonID="btnreturndate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="uppopupmsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tr>
                        <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                            <asp:Label ID="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" colspan="2"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                        <td style="TEXT-ALIGN: left" class="Label">
                            <asp:Label ID="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bepopupmsg" runat="server" BorderColor="Transparent" BorderStyle="None" BackColor="Transparent" Visible="False" CausesValidation="False"></asp:Button>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanelSN" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlInvnSN" runat="server" Width="600px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabelDescSn" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Enter SN of Item"></asp:Label> </TD></TR><TR><TD><asp:Label id="NewSN" runat="server" ForeColor="Red" Text="New SN"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextItem" runat="server" Width="195px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp;<asp:Label id="KodeItem" runat="server" Visible="False"></asp:Label> &nbsp;<asp:Label id="SNseq" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Qty Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextQty" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp; <asp:TextBox id="TextSatuan" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Scan Serial Number &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextSN" onkeypress="return EnterEvent(event)" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> &nbsp; <asp:ImageButton id="EnterSN" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY><CAPTION><DT style="TEXT-ALIGN: center">&nbsp;</DT></CAPTION><TBODY><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 100px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV style="TEXT-ALIGN: center" id="Div2"><STRONG><asp:Label id="LabelPesanSn" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label> </STRONG></DIV><DIV></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 67%"><asp:GridView id="GVSN" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" OnRowDeleting="GVSN_RowDeleting" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="SN" HeaderText="Serial Number">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="450px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSN" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lkbPilihItem" runat="server" Font-Size="X-Small">[ OK ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupSN" runat="server" TargetControlID="btnHideSN" PopupControlID="UpdatePanelSN" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptInvtry" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSN" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="TextSN"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
        
</asp:Content>

