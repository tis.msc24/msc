<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trndpap_payother.aspx.vb" Inherits="trndpap_payother" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Down Payment AP - (Return/Pay Other)"></asp:Label><asp:SqlDataSource ID="SDSList"
                        runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>">
                    </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>:: List Down Payment AP - Pay Other</strong></span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w2" DefaultButton="btnSearch"><TABLE width=750><TBODY><TR><TD style="FONT-SIZE: small; WIDTH: 64px; HEIGHT: 22px" class="Label" align=left>Filter&nbsp; </TD><TD style="HEIGHT: 22px" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="111px" CssClass="inpText" __designer:wfdid="w3"><asp:ListItem Value="trndpapno">No</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="branch_code">Branch</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="FilterTextsupp" runat="server" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox><asp:DropDownList id="jenis" runat="server" Width="50px" CssClass="inpText" Visible="False" __designer:wfdid="w5" Enabled="False"><asp:ListItem Value="B">NON CASH</asp:ListItem>
<asp:ListItem Value="T">CASH</asp:ListItem>
<asp:ListItem Value="G">GIRO</asp:ListItem>
<asp:ListItem Value="C">CREDIT CARD</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small; WIDTH: 64px" class="Label" align=left>Periode </TD><TD align=left colSpan=4><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w6"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="imageButton1" TargetControlID="dateAwal" __designer:wfdid="w10"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="imageButton2" TargetControlID="dateAkhir" __designer:wfdid="w11"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" __designer:wfdid="w12" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" __designer:wfdid="w13" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="FONT-SIZE: small; WIDTH: 64px" align=left>Order By</TD><TD style="FONT-SIZE: small; HEIGHT: 10px" align=left colSpan=4><asp:DropDownList id="orderby" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w14"><asp:ListItem Value="trndpapdate">Tgl (A-Z)</asp:ListItem>
<asp:ListItem Value="trndpapdate desc">Tgl (Z-A)</asp:ListItem>
<asp:ListItem Value="suppname">Supplier(A-Z)</asp:ListItem>
<asp:ListItem Value="suppname desc">Supplier(Z-A)</asp:ListItem>
<asp:ListItem Value="trnDPApno ">No DP.P(A-Z)</asp:ListItem>
<asp:ListItem Value="trnDPApno desc">No DP.P(Z-A)</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w15"></asp:ImageButton> <asp:ImageButton id="btnAll" onclick="btnAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton> <asp:DropDownList id="trndpapflag" runat="server" Width="72px" CssClass="inpText" Visible="False" __designer:wfdid="w17"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="OPEN">OPEN</asp:ListItem>
<asp:ListItem Value="CLOSE">CLOSE</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="statuse" runat="server" Width="119px" CssClass="inpText" Visible="False" __designer:wfdid="w18"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: small" align=left colSpan=5><asp:GridView id="gvMst" runat="server" Width="950px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w19" OnSelectedIndexChanged="gvMst_SelectedIndexChanged" DataKeyNames="cashbankoid" GridLines="None" AutoGenerateColumns="False" CellPadding="4" OnPageIndexChanging="gvMst_PageIndexChanging" AllowPaging="True" AllowSorting="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trndpapoid" DataNavigateUrlFormatString="trndpap_payother.aspx?oid={0}" DataTextField="trndpapno" HeaderText="No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trndpapdate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapamt" HeaderText="Pay Amt/Return">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpno" HeaderText="DP Ref.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Branch">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trndpapnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" Text="No data found !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMst"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="branch_code" runat="server" Visible="False" __designer:wfdid="w309"></asp:Label><BR />Cabang</TD><TD class="Label" align=left><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="CabangNya_SelectedIndexChanged" __designer:wfdid="w3"></asp:DropDownList></TD><TD style="WIDTH: 156px" class="Label" align=left><asp:Label id="cashbankoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 229px" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeDPARDate" runat="server" TargetControlID="trndpardate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDPARDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="imbDPARDate" TargetControlID="trndpardate"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 377px" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" TargetControlID="payduedate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 21px" class="Label" align=left>RDP AP No</TD><TD style="HEIGHT: 21px" class="Label" align=left><asp:TextBox id="trndparno" runat="server" Width="126px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 156px; HEIGHT: 21px" class="Label" align=left>Date*</TD><TD style="WIDTH: 229px; HEIGHT: 21px" class="Label" align=left><asp:TextBox id="trndpardate" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbDPARDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="dd/MM/yyyy"></asp:Label></TD><TD style="HEIGHT: 21px" class="Label" align=left>Supplier*&nbsp;</TD><TD style="WIDTH: 377px; HEIGHT: 21px" class="Label" align=left><asp:TextBox id="suppname" runat="server" Width="250px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left>No DP</TD><TD class="Label" align=left><asp:DropDownList id="dpno" runat="server" Width="219px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList> </TD><TD style="WIDTH: 156px" class="Label" align=left>DP Amt</TD><TD style="WIDTH: 229px" class="Label" align=left><asp:TextBox id="trndparamt" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> </TD><TD class="Label" align=left>COA DP</TD><TD style="WIDTH: 377px" class="Label" align=left><asp:DropDownList id="trndparacctgoid" runat="server" Width="247px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Payment Type</TD><TD class="Label" align=left><asp:DropDownList id="payreftype" runat="server" Width="102px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="payreftype_SelectedIndexChanged"><asp:ListItem>NON CASH</asp:ListItem>
<asp:ListItem Selected="True">CASH</asp:ListItem>
<asp:ListItem>OTHER</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 156px" class="Label" align=left>Pay Amt</TD><TD style="WIDTH: 229px" class="Label" align=left><asp:TextBox id="PAYAMT" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w113" MaxLength="14" OnTextChanged="PAYAMT_TextChanged"></asp:TextBox> </TD><TD class="Label" align=left>COA Payment</TD><TD style="WIDTH: 377px" class="Label" align=left><asp:DropDownList id="cashbankacctgoid" runat="server" Width="374px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Font-Size="X-Small" Text="Due Date" Visible="False"></asp:Label> <asp:Label id="lblNeedDue" runat="server" CssClass="Important" Text="*" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="lblDate" runat="server" CssClass="Important" Text="dd/MM/yyyy" Visible="False"></asp:Label></TD><TD style="WIDTH: 156px" class="Label" align=left><asp:Label id="lblRefNo" runat="server" Font-Size="X-Small" Text="Ref. No"></asp:Label></TD><TD class="Label" align=left colSpan=2><asp:TextBox id="payrefno" runat="server" Width="125px" CssClass="inpText" MaxLength="20"></asp:TextBox>&nbsp;<asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" Visible="False" Enabled="False" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;</TD><TD style="WIDTH: 377px" class="Label" align=left><asp:Label id="suppoid" runat="server" Visible="False"></asp:Label> <asp:TextBox id="currencyrate" runat="server" Width="27px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:DropDownList id="currencyoid" runat="server" Width="64px" CssClass="inpText" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="currencyoid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note <asp:Label id="Label3" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w5"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="trndparnote" runat="server" Width="449px" Height="33px" CssClass="inpText" Font-Size="Medium" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD><TD class="Label" align=left>Status Retur-UM</TD><TD style="WIDTH: 377px" class="Label" align=left><asp:TextBox id="trndparstatus" runat="server" Width="100px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="ftt" runat="server" TargetControlID="trndparamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDue" runat="server" Format="dd/MM/yyyy" PopupButtonID="imbDueDate" TargetControlID="payduedate"></ajaxToolkit:CalendarExtender> <asp:Label id="currentpayreftype" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 156px" class="Label" align=left></TD><TD style="WIDTH: 229px" class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meeRate" runat="server" TargetControlID="currencyrate" Mask="999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD><TD class="Label" align=left>&nbsp;<asp:Label id="trndparoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 377px" class="Label" align=left><asp:Label id="currentcbacctgoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=5>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label>&nbsp;<asp:Label id="createtime" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 377px; HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Posting"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" Visible="False"></asp:ImageButton></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="WIDTH: 377px; HEIGHT: 10px" class="Label" align=left><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="PAYAMT" __designer:wfdid="w114" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:UpdatePanel id="upCust" runat="server"><ContentTemplate>
<asp:Panel id="pnlCust" runat="server" Width="500px" CssClass="modalBox" Visible="False" BorderStyle="Solid"><TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=1><asp:Label id="lblCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Supplier"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=2>Filter&nbsp; : <asp:DropDownList id="suppFilter1" runat="server" Width="67px" CssClass="inpText"><asp:ListItem Value="suppname">Nama</asp:ListItem>
<asp:ListItem Value="suppcode">Kode</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="filtertext" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbsearchFilter" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD></TR><TR></TR><TR><TD style="TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=1>&nbsp;</TD></TR><TR><TD colSpan=5><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvCust" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" DataKeyNames="suppoid,suppname" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=5>&nbsp; <asp:LinkButton id="lkbCloseCust" onclick="lkbCloseCust_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideCust" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeCust" runat="server" TargetControlID="btnHideCust" PopupControlID="pnlCust" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCust" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel>&nbsp; </TD><TD vAlign=top align=left></TD></TR><TR><TD vAlign=top align=left></TD><TD vAlign=top align=left></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>:: Form Down Payment AP - Pay Other</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKValidasi_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Perkiraan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

