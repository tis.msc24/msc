<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnKembaliBarang.aspx.vb" Inherits="KembaliBarang" title="MSC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table class="header" width="100%">
        <tr>
            <td align="left" style="height: 15px; background-color: silver; width: 337px;">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Pengembalian Barang"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel3" runat="server">
                    <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnFind" __designer:wfdid="w313"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 168px">Cabang</TD><TD>:</TD><TD><asp:DropDownList id="sFillterCbng" runat="server" CssClass="inpText" __designer:wfdid="w332"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 168px">Filter&nbsp; </TD><TD>:</TD><TD><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w314"><asp:ListItem Value="trnkembalino">No. Pengembalian</asp:ListItem>
<asp:ListItem Value="trnpinjamno">No. Peminjaman</asp:ListItem>
<asp:ListItem Value="km.namapeminjam">Nama Peminjam</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w315"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkPeriod" runat="server" Text="Periode" __designer:wfdid="w316"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:TextBox id="FilterPeriod1" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w317"></asp:TextBox> &nbsp;<asp:ImageButton id="ibPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w318"></asp:ImageButton> to&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w319"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="ibPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w320"></asp:ImageButton>&nbsp; <asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w321"></asp:Label></TD></TR><TR><TD style="WIDTH: 168px"><asp:CheckBox id="checkStatus" runat="server" Text="Status" __designer:wfdid="w322"></asp:CheckBox> </TD><TD>:</TD><TD style="WIDTH: 1264px"><asp:DropDownList id="StatusDDL" runat="server" CssClass="inpText" __designer:wfdid="w323"><asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w324"></asp:ImageButton> <asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w325"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w326"></asp:ImageButton></TD></TR><TR><TD colSpan=3><asp:GridView style="max-width: 940px" id="gvPenerimaan" runat="server" Width="940px" ForeColor="#333333" AllowSorting="True" PageSize="8" OnRowDataBound="gvPenerimaan_RowDataBound" AutoGenerateColumns="False" DataKeyNames="trnkembalimstoid,branch_code" CellPadding="4" GridLines="None" AllowPaging="True" __designer:wfdid="w306">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnkembalino" HeaderText="No. Pengembalian">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnpinjamno" HeaderText="No. Peminjaman">
<FooterStyle BackColor="DodgerBlue" BorderColor="DodgerBlue"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="170px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="namapeminjam" HeaderText="Nama Peminjam">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndatepinjam" HeaderText="Tanggal Pinjam">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnkembalidate" HeaderText="Tanggal Kembali">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:LinkButton id="lbPrint" onclick="lbPrint_Click" runat="server" ToolTip='<%# Eval("trnkembalimstoid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="65px"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label10" runat="server" ForeColor="Red" Text="Data Not Found"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibPeriod1" TargetControlID="FilterPeriod1" Enabled="True" __designer:wfdid="w328"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibPeriod2" TargetControlID="txtPeriod2" Enabled="True" __designer:wfdid="w329"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" TargetControlID="FilterPeriod1" Enabled="True" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder="" __designer:wfdid="w330"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" __designer:wfdid="w331"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="gvPenerimaan"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                .<strong><span style="font-size: 9pt">:</span></strong> <strong><span style="font-size: 9pt"> List Of Pengembalian</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 122px">Cabang</TD><TD>:</TD><TD colSpan=7><asp:DropDownList id="ddlCabangNya" runat="server" CssClass="inpText" __designer:wfdid="w289" AutoPostBack="True"></asp:DropDownList> <asp:Label id="lbloid" runat="server" __designer:wfdid="w239" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" ForeColor="Red" __designer:wfdid="w185" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">No.&nbsp;Pengembalian</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtNoTanda" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w240" ReadOnly="True" MaxLength="15"></asp:TextBox>&nbsp;<asp:Label id="Kembalioid" runat="server" Text="Kembalioid" __designer:wfdid="w241" Visible="False"></asp:Label>&nbsp;</TD></TR><TR><TD style="WIDTH: 122px">Tanggal</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtTglTerima" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w242" ReadOnly="True"></asp:TextBox> <asp:Label id="Label7" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w243"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">No. Peminjaman</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="TxtPinjamNo" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w244" MaxLength="100"></asp:TextBox>&nbsp;<asp:ImageButton id="sBtnPinjamNO" onclick="sBtnPinjamNO_Click" runat="server" ImageUrl="~/Images/search2.gif" __designer:wfdid="w245"></asp:ImageButton> <asp:ImageButton id="ErasePinjamNo" onclick="ErasePinjamNo_Click" runat="server" Width="18px" ImageUrl="~/Images/erase.bmp" __designer:wfdid="w246"></asp:ImageButton>&nbsp;<asp:Label id="TrnPinjamOid" runat="server" __designer:wfdid="w247" Visible="False">0</asp:Label></TD></TR><TR><TD style="WIDTH: 122px"></TD><TD></TD><TD colSpan=7><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GvPinjam" runat="server" Width="70%" ForeColor="#333333" __designer:wfdid="w248" Visible="False" PageSize="8" AutoGenerateColumns="False" DataKeyNames="trnpinjammstoid,trnpinjamno,namapeminjam" CellPadding="4" GridLines="None" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" OnSelectedIndexChanged="GvPinjam_SelectedIndexChanged" OnPageIndexChanging="GvPinjam_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True" Font-Size="X-Small"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnpinjamno" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="175px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="namapeminjam" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 122px">Peminjam<asp:Label id="Label3" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w249"></asp:Label></TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtNamaCust" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w250" MaxLength="100" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 122px">Keterangan</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtDetailPinjam" runat="server" Width="288px" Height="20px" CssClass="inpText" Font-Size="Medium" __designer:wfdid="w251" MaxLength="150" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label8" runat="server" Width="128px" ForeColor="Red" Text="maks. 150 karakter" __designer:wfdid="w252"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">Status</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtStatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w253" ReadOnly="True" Enabled="False"></asp:TextBox></TD></TR><TR><TD colSpan=9><asp:Label id="Label18" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black" Text="Detail :" __designer:wfdid="w254" Font-Underline="True"></asp:Label>&nbsp; <asp:Label id="dtlseq" runat="server" __designer:wfdid="w255" Visible="False"></asp:Label>&nbsp;<asp:Label id="dtlrow" runat="server" __designer:wfdid="w256" Visible="False"></asp:Label>&nbsp;<asp:Label id="lblNo" runat="server" __designer:wfdid="w257" Visible="False"></asp:Label><asp:Label id="lblUpdNo" runat="server" __designer:wfdid="w258" Visible="False"></asp:Label>&nbsp;<asp:Label id="iuAdd" runat="server" __designer:wfdid="w259" Visible="False"></asp:Label>&nbsp;<asp:Label id="i_u2" runat="server" ForeColor="Red" __designer:wfdid="w260" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 122px">Barang</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="txtBarang" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w261" MaxLength="25" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="sBarang" runat="server" Width="19px" ImageUrl="~/Images/search.gif" Height="18px" __designer:wfdid="w262"></asp:ImageButton> <asp:ImageButton id="EraseItem" onclick="EraseItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" Height="18px" __designer:wfdid="w263"></asp:ImageButton> <asp:Label id="itemoid" runat="server" __designer:wfdid="w264" Visible="False"></asp:Label>&nbsp;<asp:Label id="trnpinjamdtloid" runat="server" __designer:wfdid="w266" Visible="False">0</asp:Label>&nbsp;</TD></TR><TR><TD style="WIDTH: 122px"></TD><TD></TD><TD colSpan=7><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvMaterial" runat="server" Width="70%" ForeColor="#333333" __designer:wfdid="w268" Visible="False" PageSize="8" AutoGenerateColumns="False" DataKeyNames="itemoid,itemcode,itemdesc,itemqty,trnpinjamdtloid,flagbarang,mtrlocoid" CellPadding="4" GridLines="None" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" OnSelectedIndexChanged="gvMaterial_SelectedIndexChanged" OnPageIndexChanging="gvMaterial_PageIndexChanging" OnRowDataBound="gvMaterial_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 122px"><asp:Label id="Location" runat="server" __designer:wfdid="w269">Location</asp:Label></TD><TD><asp:Label id="lblTitik" runat="server" __designer:wfdid="w270">:</asp:Label></TD><TD colSpan=7><asp:DropDownList id="LocationDDL" runat="server" CssClass="inpText" __designer:wfdid="w271"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 122px">Quantity</TD><TD>:</TD><TD colSpan=7><asp:TextBox id="trnpinjamdtlqty" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w272" MaxLength="30">0</asp:TextBox>&nbsp;<asp:Label id="QtyV" runat="server" Text="0" __designer:wfdid="w273" Visible="False"></asp:Label> <asp:Label id="flagbarang" runat="server" __designer:wfdid="w274" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=9><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w275"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w276"></asp:ImageButton> <asp:Label id="matcode" runat="server" __designer:wfdid="w4" Visible="False"></asp:Label> <asp:Label id="periodacctg" runat="server" __designer:wfdid="w3" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=9><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPinjamBarang" runat="server" Width="100%" Height="40px" ForeColor="#333333" __designer:wfdid="w278" PageSize="2" AutoGenerateColumns="False" DataKeyNames="sequence" CellPadding="4" GridLines="None" OnRowDataBound="gvPinjamBarang_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" BorderColor="Blue" Font-Bold="True" Width="10%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sequence" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="20%"></HeaderStyle>

<ItemStyle BorderColor="Blue" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="60%"></HeaderStyle>

<ItemStyle BorderColor="Blue" Width="60%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemQty" HeaderText="Qty Kembali">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="qtypinjam" HeaderText="Qty Pinjam" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnpinjamdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="matcode" Visible="False"></asp:BoundField>
<asp:BoundField DataField="flagbarang" HeaderText="flagbarang" Visible="False"></asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD colSpan=9><asp:Label id="lblUpd" runat="server" __designer:wfdid="w279"></asp:Label>&nbsp;On <asp:Label id="UpdTime" runat="server" Font-Bold="True" __designer:wfdid="w280"></asp:Label>&nbsp;By <asp:Label id="UpdUser" runat="server" Font-Bold="True" __designer:wfdid="w281"></asp:Label></TD></TR><TR><TD colSpan=9><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w282"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w284"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w285"></asp:ImageButton><asp:ImageButton id="printmanualpenerimaan" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w286" Visible="False"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=9><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w287" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w288"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ItemQtyFTE" runat="server" __designer:wfdid="w2" TargetControlID="trnpinjamdtlqty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR></TBODY></TABLE><DIV>&nbsp;</DIV>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                .<strong><span style="font-size: 9pt">: <span>
                Form Pengembalian</span></span></strong>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanel2" runat="server">
        <contenttemplate>
<asp:Panel id="panelMsg" runat="server" Width="264px" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TBODY><TR><TD style="BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

