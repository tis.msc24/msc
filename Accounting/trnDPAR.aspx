<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnDPAR.aspx.vb" Inherits="Accounting_DownPaymentAR" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="Navy" Text=".: Down Payment A/R" CssClass="Title" Font-Size="Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            &nbsp;<strong><span style="font-size: 9pt">List of Down Payment A/R :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w832"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left>Cabang</TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="fddlCabang" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w833"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w834"><asp:ListItem Value="dp.trndparoid">Draft No.</asp:ListItem>
<asp:ListItem Value="trndparno">DP No.</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">DP Account</asp:ListItem>
<asp:ListItem Value="trndparnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w835"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w836"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w837"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w838"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w839"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w840"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w841"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w842"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w843"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w844">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w849" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w850" AlternateText="View All"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w851" AlternateText="Print" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=5><asp:LinkButton id="lbInProcess" runat="server" ForeColor="Red" __designer:wfdid="w852"></asp:LinkButton></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="gvTRN" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w827" AllowSorting="True" PageSize="8" GridLines="None" DataKeyNames="trndparoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="gvTRN_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trndparoid" DataNavigateUrlFormatString="~\Accounting\trnDPAR.aspx?oid={0}" DataTextField="trndparoid" HeaderText="Draft No." SortExpression="trndparoid">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trndparno" HeaderText="DP No." SortExpression="trndparno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpardate" HeaderText="DP Date" SortExpression="dpardate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" SortExpression="custname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="DP Account" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dparpaytype" HeaderText="Payment Type" SortExpression="dparpaytype">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparstatus" HeaderText="Status" SortExpression="trndparstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparnote" HeaderText="Note" SortExpression="trndparnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
&nbsp; <asp:ImageButton id="imbprintdpar" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w64"></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w854"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=5><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w855" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w856"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w1" Format="dd/MM/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w2" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w3" Format="dd/MM/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w4" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Font-Bold="True" Text="New Data" __designer:wfdid="w573"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w574" Visible="False"></asp:Label><asp:Label id="custoid" runat="server" __designer:wfdid="w575" Visible="False">
</asp:Label> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w577" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="CutofDate" runat="server" Text=" " __designer:wfdid="w576" Visible="False"></asp:Label> </TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Cabang" __designer:wfdid="w578"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" Width="205px" CssClass="inpTextDisabled" __designer:wfdid="w579" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Draft No." __designer:wfdid="w580"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparno" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w581" Visible="False" Enabled="False"></asp:TextBox><asp:Label id="dparoid" runat="server" __designer:wfdid="w582"></asp:Label></TD></TR><TR><TD class="Label" align=left>Type DP</TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLTypeDP" runat="server" CssClass="inpText" __designer:wfdid="w583" OnSelectedIndexChanged="DDLTypeDP_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="NORMAL">DP Normal</asp:ListItem>
<asp:ListItem Value="TITIPAN">DP Titipan</asp:ListItem>
<asp:ListItem Value="PENGAKUAN">DP Pengakuan</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="DP Date" __designer:wfdid="w584"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dpardate" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w585" AutoPostBack="True" Enabled="False" EnableTheming="True" ToolTip="DD/MM/YYYY"></asp:TextBox><asp:ImageButton id="imbDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w586" Visible="False"></asp:ImageButton> <asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w587"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Customer" __designer:wfdid="w588"></asp:Label> <asp:Label id="Label17" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w589"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w590" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w591"></asp:ImageButton> <asp:ImageButton id="btnClearCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w592"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="DP Account" __designer:wfdid="w593"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" __designer:wfdid="w594"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblRefNo" runat="server" Text="Ref. No." __designer:wfdid="w595"></asp:Label><asp:Label id="lblWarnRefNo" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w596" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparpayrefno" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w597" MaxLength="20"></asp:TextBox> <asp:Label id="LblTransBank" runat="server" Width="208px" CssClass="Important" Text="* Isi Nomer transaksi bank pengirim" __designer:wfdid="w598" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Peyment Type" __designer:wfdid="w599"></asp:Label> </TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dparpaytype" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w600" AutoPostBack="True"><asp:ListItem Value="BKM">CASH</asp:ListItem>
<asp:ListItem Value="BBM">BANK</asp:ListItem>
<asp:ListItem Value="BGM">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Enabled="False" Value="DPP">DOWN PAYMENT</asp:ListItem>
<asp:ListItem Enabled="False" Value="DPS">DP SEMENTARA</asp:ListItem>
</asp:DropDownList><asp:TextBox id="TrnDpNo" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w601" Visible="False" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w602" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="ebtnDp" onclick="ebtnDp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w603" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="CbCoaOid" runat="server" Text="0" __designer:wfdid="w605" Visible="False"></asp:Label> <asp:Label id="OidAbal2" runat="server" Text="0" __designer:wfdid="w604" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:GridView id="gvTitipanDP" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w606" PageSize="5" GridLines="None" DataKeyNames="trndparoid,branch_code,trndparno,trndparamtidr,cashbankoid,cashbankno,trndparacctgoid,cashbankacctgoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="Black"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trndparno" HeaderText="No. DP">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparamtidr" HeaderText="Amount DP">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trndparoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trndparacctgoid" HeaderText="trndparacctgoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Width="102px" Text="Pay Account" __designer:wfdid="w607"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dparpayacctgoid" runat="server" CssClass="inpText" __designer:wfdid="w608" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Cash/Bank No." __designer:wfdid="w609"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" Width="160px" CssClass="inpTextDisabled" __designer:wfdid="w610" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date" __designer:wfdid="w611"></asp:Label> <asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w612" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDueDate" runat="server" Text=":" __designer:wfdid="w613"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dparduedate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w614" ToolTip="DD/MM/YYYY"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w615"></asp:ImageButton> <asp:Label id="lblInfoDueDate" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w616"></asp:Label></TD><TD id="TD26" class="Label" align=left runat="server" Visible="false"><asp:Label id="lblDTG" runat="server" Width="89px" Text="Date Take Giro" __designer:wfdid="w617"></asp:Label> <asp:Label id="lblWarnDTG" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w618" Visible="False"></asp:Label></TD><TD id="TD27" class="Label" align=center runat="server" Visible="false"><asp:Label id="lblSeptDTG" runat="server" Text=":" __designer:wfdid="w619"></asp:Label></TD><TD id="TD25" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="dpartakegiro" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w620" ToolTip="DD/MM/YYYY"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w621"></asp:ImageButton><asp:Label id="lblInfoDTG" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w622"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="DP Amount" __designer:wfdid="w623"></asp:Label> <asp:Label id="Label22" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w624"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w625" AutoPostBack="True" MaxLength="12"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label20" runat="server" Width="97px" Text="Total Cash/Bank" __designer:wfdid="w626"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparnett" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w627" Enabled="False"></asp:TextBox></TD></TR><TR><TD id="TD11" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD14" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD13" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD24" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label12" runat="server" Text="Currency" __designer:wfdid="w640"></asp:Label></TD><TD id="TD5" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD19" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="curroid" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w641">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Note" __designer:wfdid="w642"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparnote" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w643" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w644"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="dparstatus" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w645" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w656"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w657"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w658"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w659"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w660" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w661" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w662" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w663"></asp:ImageButton> <asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w664" AlternateText="Show COA"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w5" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w7"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="ceDTG" runat="server" __designer:wfdid="w13" Format="dd/MM/yyyy" PopupButtonID="imbDTG" TargetControlID="dpartakegiro"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w8" Format="dd/MM/yyyy" PopupButtonID="imbDate" TargetControlID="dpardate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" __designer:wfdid="w10" Format="dd/MM/yyyy" PopupButtonID="imbDueDate" TargetControlID="dparduedate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" __designer:wfdid="w14" TargetControlID="dpartakegiro" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w9" TargetControlID="dpardate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" __designer:wfdid="w11" TargetControlID="dparduedate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w12" TargetControlID="dparamt" ValidChars="1234567890,.">
                </ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><DIV>&nbsp;</DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Down Payment A/R :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel ID="upListCust" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" __designer:wfdid="w18" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w19"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3>&nbsp;<asp:Panel id="pnlFilterListCust" runat="server" Width="100%" Height="20px" __designer:wfdid="w2" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w3">
                                        <asp:ListItem Value="custcode">Code</asp:ListItem>
                                        <asp:ListItem Value="custname">Name</asp:ListItem>
                                        <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                    </asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox> <asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w25" PageSize="5" GridLines="None" DataKeyNames="custoid,custname,payrefno" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="Black"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="No. Reff">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCust" runat="server" __designer:wfdid="w26">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" __designer:wfdid="w27" TargetControlID="btnHideListCust" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" __designer:wfdid="w28" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w29" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w30"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w31"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w32"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w34" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w35" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlCOAPosting" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w36" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblCOAPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w37"></asp:Label> <asp:DropDownList id="DDLRateType" runat="server" Width="125px" CssClass="inpText" Font-Size="Medium" Font-Bold="True" __designer:wfdid="w38" Visible="False" AutoPostBack="True"><asp:ListItem Value="Default">Rate Default</asp:ListItem>
<asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
<asp:ListItem Value="USD">Rate To USD</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvCOAPosting" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w39" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdebet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcredit" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseCOAPosting" runat="server" __designer:wfdid="w40">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideCOAPosting" runat="server" __designer:wfdid="w41" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeCOAPosting" runat="server" __designer:wfdid="w42" TargetControlID="btnHideCOAPosting" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting">
            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

