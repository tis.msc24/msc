'Prgmr:Shutup_M | LastUpdt:28.02.2016
Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class NotaRombeng
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim cKon As New Koneksi
    'Dim cfunction As New ClassFunction
    Public sql_temp As String
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
    'Dim tempPayFlag As String = ""
#End Region

#Region "Function"
    Function getCurrency() As Double
        Dim curr As Double
        sSql = "select rate2idrvalue from ql_mstrate2 where rate2year =YEAR(getdate()) and rate2month = MONTH(getdate()) and currencyoid = 2"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curr = ToMaskEdit(xCmd.ExecuteScalar, 2)
        Return curr
        conn.Close()
    End Function
#End Region

#Region "Prosedure"
    Private Sub ShowCOA(ByVal noRef As String, ByVal cashbankoid As Int64)
        sSql = "select a.acctgcode, a.acctgdesc, case d.gldbcr when 'D' then d.glamt else 0 end 'Debet', case d.gldbcr when 'C' then d.glamt else 0 end 'Kredit', glnote From ql_trngldtl d Inner Join ql_mstacctg a on a.acctgoid=d.acctgoid Where glother2=" & cashbankoid & " AND noref='" & noRef & "' And d.cmpcode='" & cmpcode & "' Order By d.glseq Asc"
        Dim dtcoa As DataTable = cKon.ambiltabel(sSql, "ShowCOA")
        gvakun.DataSource = dtcoa
        gvakun.DataBind()
    End Sub

    Private Sub bindAccounting()
        Try
            sSql = "Select con.refoid, i.itemcode, i.itemdesc, con.mtrlocoid, 'GUDANG' +' '+lo.gendesc Gudang, SUM(qtyIn)-SUM(qtyOut) SaldoNya From QL_conmtr con Inner Join QL_mstitem i ON i.itemoid=con.refoid Inner Join QL_mstgen lo ON lo.genoid=con.mtrlocoid AND gengroup='LOCATION' AND genother6='ROMBENG' WHERE con.periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND branch_code='" & deptoid.SelectedValue & "' AND (i.itemcode LIKE '%" & TcharNoTrim(itemdesc.Text) & "%' OR i.itemdesc LIKE '%" & TcharNoTrim(itemdesc.Text) & "%') Group BY con.refoid, i.itemcode, i.itemdesc, con.mtrlocoid, con.branch_code, lo.gendesc Having (SUM(qtyIn)-SUM(qtyOut))>0"
            Dim tblitem As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("tblitem") = tblitem
            GVItem.DataSource = tblitem
            GVItem.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub Binddata()
        Try
            If IsDate(toDate(txtPeriode1.Text)) = False Or IsDate(toDate(txtPeriode1.Text)) = False Then
                showMessage("Periode awal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If IsDate(toDate(txtPeriode2.Text)) = False Or IsDate(toDate(txtPeriode2.Text)) = False Then
                showMessage("Periode akhir salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            'Dim mySqlConn As New SqlConnection(ConnStr)
            sSql = "Select * From (SELECT cb.branch_code, 0 AS selected, cashbankno, gendesc, cashbankoid, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode and cb.branch_code = p.branch_code) cashbankglamt, cashbankstatus, cashbanknote, cashbankacctgoid, g.currencycode rate, cb.cmpcode FROM QL_trncashbankmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid inner join ql_mstgen gen on gen.gengroup='CABANG' And gen.gencode = cb.branch_code WHERE cb.cashbankno like '%" & Tchar(no.Text.Trim) & "%' and cb.cashbankacctgoid IN (select ao.acctgoid from ql_mstacctg ao) and cb.cmpcode = '" & cmpcode & "' AND cashbankgroup='RAGS' UNION ALL SELECT cb.branch_code, 0 AS selected, cashbankno, gendesc, cashbankoid, cashbankdate, (SELECT SUM(cashbankglamt) FROM QL_cashbankgl p WHERE p.cashbankoid=cb.cashbankoid AND p.cmpcode=cb.cmpcode and cb.branch_code = p.branch_code) cashbankglamt, cashbankstatus, cashbanknote, cashbankacctgoid, g.currencycode rate, cb.cmpcode FROM QL_trncbclosedmst cb inner join ql_mstcurr g on g.currencyoid=cb.cashbankcurroid inner join ql_mstgen gen on gen.gengroup='cabang' And gen.gencode = cb.branch_code WHERE cb.cashbankno like '%" & Tchar(no.Text.Trim) & "%' and cb.cashbankacctgoid in (select ao.acctgoid from ql_mstacctg ao) and cb.cmpcode = '" & cmpcode & "' AND cashbankgroup='RAGS') cb Where cb.cmpcode = '" & cmpcode & "'"

            If cbPeriode.Checked Then
                If txtPeriode1.Text = "" And txtPeriode2.Text = "" Then
                    showMessage("Tolong isi Periode!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If

                If txtPeriode1.Text <> "" And txtPeriode2.Text <> "" Then
                    If CDate(toDate(txtPeriode1.Text)) <= CDate(toDate(txtPeriode2.Text)) Then
                        sSql &= " AND cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' and cashbankdate<dateadd(day,1,'" & CDate(toDate(txtPeriode2.Text)) & "')"
                    Else
                        showMessage("Tanggal akhir harus sama atau lebih dari tanggal mulai !", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                        Exit Sub
                    End If
                ElseIf txtPeriode1.Text <> "" And txtPeriode2.Text = "" Then
                    sSql &= " AND cashbankdate >='" & CDate(toDate(txtPeriode1.Text)) & "'"
                ElseIf txtPeriode1.Text = "" And txtPeriode2.Text <> "" Then
                    sSql &= " AND cashbankdate <='" & CDate(toDate(txtPeriode2.Text)) & "'"
                End If
            End If

            If statuse.SelectedValue <> "ALL" Then
                sSql &= " AND cashbankstatus='" & statuse.SelectedValue & "'"
            End If

            If statuse.SelectedValue <> "ALL" Then
                If statuse.SelectedValue.ToUpper = "IN PROCESS" Then
                    sSql &= " AND cashbankstatus=''"
                Else
                    sSql &= " AND cashbankstatus='" & statuse.SelectedValue.Trim & "'"
                End If
            End If

            sSql &= " ORDER BY cashbankdate desc"
            Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "Receipt")
            Session("tbldata") = objTable : gvmstcost.DataSource = objTable
            gvmstcost.DataBind() : calcTotalInGrid()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Public Sub bindLastSearched()
        If (Not Session("SearchExpense") Is Nothing) Or (Not Session("SearchExpense") = "") Then
            Dim mySqlConn As New SqlConnection(ConnStr)
            Dim sqlSelect As String = Session("SearchExpense")

            Dim objTable As DataTable = cKoneksi.ambiltabel(sqlSelect, "cost")
            Session("tbldata") = objTable
            gvmstcost.DataSource = objTable
            gvmstcost.DataBind()
            mySqlConn.Close()
            calcTotalInGrid()
        End If
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dd_cabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_cabang, sSql)
            Else
                FillDDL(dd_cabang, sSql)
                dd_cabang.Items.Add(New ListItem("ALL", "ALL"))
                dd_cabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dd_cabang, sSql)
            dd_cabang.Items.Add(New ListItem("ALL", "ALL"))
            dd_cabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(deptoid, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(deptoid, sSql)
            Else
                FillDDL(deptoid, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(deptoid, sSql)
        End If
    End Sub

    Public Sub initDDLAll(ByVal cashbank As String)
        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet
        If cashbank = "" Then
            ' mengisi data pada DDL cmpcode
            sSql = "SELECT gendesc,genother1 FROM QL_mstgen WHERE gengroup='CABANG' AND cmpcode='" & cmpcode & "'"

            mySqlDA = New SqlDataAdapter(sSql, ConnStr)
            objDs = New DataSet
            mySqlDA.Fill(objDs, "dataA")
        Else
            cashbankacctgoid.Items.Clear()
            ' untuk mengisi data pada DDL CashBank
            If Trim(cashbank) = "CASH" Then
                sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '111%' "
                mySqlDA = New SqlDataAdapter(sSql, ConnStr)
                objDs = New DataSet
                mySqlDA.Fill(objDs, "dataA")

                If objDs.Tables("dataA").Rows.Count > 0 Then
                    For C1 As Int16 = 0 To objDs.Tables("dataA").Rows.Count - 1
                        cashbankacctgoid.Items.Add(Trim(objDs.Tables("dataA").Rows(C1).Item(1).ToString & "-" & objDs.Tables("dataA").Rows(C1).Item(2).ToString))
                        cashbankacctgoid.Items.Item(C1).Value = Trim(objDs.Tables("dataA").Rows(C1).Item(0).ToString)
                    Next
                Else
                    showMessage("Isi/Buat account CASH di master accounting !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                End If
            ElseIf Trim(cashbank) = "NON CASH" Then
                sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '112%'"
                mySqlDA = New SqlDataAdapter(sSql, ConnStr)
                objDs = New DataSet
                mySqlDA.Fill(objDs, "dataA")

                If objDs.Tables("dataA").Rows.Count > 0 Then
                    For C1 As Int16 = 0 To objDs.Tables("dataA").Rows.Count - 1
                        cashbankacctgoid.Items.Add(Trim(objDs.Tables("dataA").Rows(C1).Item(1).ToString & "-" & objDs.Tables("dataA").Rows(C1).Item(2).ToString))
                        cashbankacctgoid.Items.Item(C1).Value = Trim(objDs.Tables("dataA").Rows(C1).Item(0).ToString)
                    Next
                Else
                    showMessage("Isi/Buat account Bank di master accounting!!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                End If
            End If
        End If
    End Sub

    Private Sub initPayactg()
        Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet

        sSql = "SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode like '1103%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"

        mySqlDA = New SqlDataAdapter(sSql, ConnStr)
        objDs = New DataSet
        mySqlDA.Fill(objDs, "dataB")

        If objDs.Tables("dataB").Rows.Count <= 0 Then
            showMessage("Isi/Buat account pembayaran di master accounting !", "WARNING", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Private Sub initddlcasbank(ByVal cashbank As String)
        Dim scbtype As String = "" : Dim sVar As String = ""
        If payflag.SelectedValue = "CASH" Then
            scbtype = "BKM" : sVar = "VAR_CASH"
        ElseIf payflag.SelectedValue = "NON CASH" Then
            scbtype = "BBM" : sVar = "VAR_BANK"
        End If

        cashbankacctgoid.Items.Clear()
        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & deptoid.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & cmpcode & "'"
            sCode = GetStrData(sSql)
        End If

        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillDDL(cashbankacctgoid, sSql)
        End If

        If sCode = "" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='All'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND (acctgdesc LIKE '%" & Tchar(txtCoa.Text.Trim) & "%' OR acctgcode LIKE '%" & Tchar(txtCoa.Text.Trim) & "%') AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(GvCoa, sSql, "QL_mstacctg")
        End If
        GvCoa.Visible = True
    End Sub

    Public Sub fillTextBox(ByVal vpayid As Integer, ByVal status As String, ByVal cabang As String)
        sSql = "SELECT cb.cashbankoid, cb.upduser, cb.updtime, case when cashbanktype='BKM' then 'CASH' else 'NON CASH' end payflag, cashbankacctgoid, cashbankstatus, cb.cmpcode, (Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid) cashbankglamt,cb.cashbanknote,convert(char(10),cashbankdate, 103) cashbankdate, convert(char(10),cashbankdate, 103) duedate, cb.cashbankrefno refno, cb.cashbankno, cb.cashbankcurroid, cb.cashbankcurrate, '' pic, cb.deptoid, cb.bankoid, 0 personoid, cb.branch_code, cb.createtime, cb.upduser, cb.updtime, (Select trnragsmstoid from QL_trnragsmst rag Where rag.cashbankmstoid=cb.cashbankoid AND cb.branch_code=rag.branch_code) trnragsmstoid FROM QL_trncashbankmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cb.cashbankstatus='" & status & "' AND cb.branch_code='" & cabang & "' UNION ALL SELECT cb.cashbankoid, cb.upduser, cb.updtime, case when cashbanktype='BKM' then 'CASH' else 'NON CASH' end payflag, cashbankacctgoid, cashbankstatus, cb.cmpcode, (Select SUM(p.cashbankglamt) From QL_cashbankgl p Where p.cashbankoid=cb.cashbankoid) cashbankglamt, cb.cashbanknote, convert(char(10),cashbankdate, 103) cashbankdate, convert(char(10),cashbankdate, 103) duedate, cb.cashbankrefno refno, cb.cashbankno, cb.cashbankcurroid, cb.cashbankcurrate, '' pic, cb.deptoid, cb.bankoid, 0 personoid, cb.branch_code, cb.createtime, cb.upduser, cb.updtime, (Select trnragsmstoid FROM QL_trnragsmst rag Where rag.cashbankmstoid=cb.cashbankoid AND cb.branch_code=rag.branch_code) trnragsmstoid FROM QL_trncbclosedmst cb Where cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid=" & vpayid & " AND cb.cashbankstatus='" & status & "' AND cb.branch_code='" & cabang & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                Session("vNoCashBank") = Trim(xreader("cashbankno"))
                cashbankoid.Text = xreader("cashbankoid")
                payflag.SelectedValue = Trim(xreader("payflag"))

                If payflag.SelectedValue = "CASH" Then
                    payrefno.Text = ""
                Else
                    If IsDBNull(xreader("refno")) Then
                        payrefno.Text = ""
                    Else
                        payrefno.Text = Trim(xreader("refno"))
                    End If
                End If

                CashBankNo.Text = xreader("cashbankno")
                cashbankdate.Text = xreader("cashbankdate")
                cashbanknote.Text = xreader("cashbanknote")
                deptoid.SelectedValue = Trim(xreader("branch_code").ToString)
                trnragsmstoid.Text = xreader("trnragsmstoid")
                initddlcasbank(payflag.SelectedValue)

                If payflag.SelectedValue = "CASH" Then
                    setcontrolCash(False)
                    payrefno.Text = ""
                Else
                    setcontrolCash(True)
                End If

                lblCoa.Text = xreader("cashbankacctgoid")
                txtCoa.Text = GetStrData("SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND acctgoid=" & xreader("cashbankacctgoid") & "")

                upduser.Text = Trim(xreader("upduser"))
                updtime.Text = Trim(xreader("updtime"))
                If Trim(xreader.Item("cashbankstatus")) = "POST" Or Trim(xreader.Item("cashbankstatus")) = "CLOSED" Then
                    btnPosting.Visible = False : btnsave.Visible = False
                    btnDelete.Visible = False : btnAddToList.Visible = False
                    lblPOST.Text = Trim(xreader.Item("cashbankstatus"))
                    btnClear.Visible = False : GvCoa.Visible = False
                Else
                    btnPosting.Visible = True : btnsave.Visible = True
                    btnDelete.Visible = True : btnAddToList.Visible = True
                    lblPOST.Text = ""
                End If

                payflag.Enabled = False
                deptoid.SelectedValue = (xreader.Item("branch_code"))
                createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss tt")
            End While
        End If

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        sSql = "SELECT gl.cashbankgloid, gl.acctgoid, trnragsdtlseq, i.itemdesc, rg.trnragsdtlprice, rg.trnragsdtlqty, rg.trnragsdtlnetto, rg.trnragsdtlnote, rg.mtrlocoid, i.itemoid FROM QL_cashbankgl gl INNER JOIN QL_trnragsdtl rg ON rg.trncashbankdtloid=gl.cashbankgloid AND rg.branch_code=gl.branch_code INNER JOIN QL_mstitem i ON i.itemoid=rg.itemoid WHERE gl.cmpcode='" & cmpcode & "' AND gl.cashbankoid=" & vpayid & " AND gl.branch_code='" & cabang & "' UNION ALL SELECT gl.cashbankgloid seq, gl.acctgoid, trnragsdtlseq, i.itemdesc, rg.trnragsdtlprice, rg.trnragsdtlqty, rg.trnragsdtlnetto, rg.trnragsdtlnote, rg.mtrlocoid, i.itemoid FROM QL_trncbcloseddtl gl INNER JOIN QL_trnragsdtl rg ON rg.trncashbankdtloid=gl.cashbankgloid AND rg.branch_code=gl.branch_code INNER JOIN QL_mstitem i ON i.itemoid=rg.itemoid WHERE gl.cmpcode='" & cmpcode & "' AND gl.cashbankoid=" & vpayid & " AND gl.branch_code='" & cabang & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "dataCost")
        GVDtlCost.Visible = True

        Dim dv As DataView = objDs.Tables("dataCost").DefaultView
        dv.Sort = "trnragsdtlseq DESC"
        GVDtlCost.DataSource = objDs.Tables("dataCost")
        GVDtlCost.DataBind()
        Session("dtlTable") = objDs.Tables("dataCost")

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If

        calcTotalInGridDtl()
        TabContainer1.ActiveTabIndex = 1
        imbCBDate.Visible = False : cashbankdate.Enabled = False
        cashbankdate.CssClass = "inpTextDisabled"
    End Sub

    Private Sub generateCashBankID()
        cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        CashBankNo.Text = GenerateID("QL_trncashbankmst", cmpcode)
    End Sub

    Private Sub generateCostID()
        cashbankgloid.Text = GenerateID("QL_cashbankgl", cmpcode)
    End Sub

    Private Sub generateCashBankNo(ByVal cashbanktype As String)
        Dim sCBType As String = ""
        Dim iCurID As Integer = 0

        If cashbanktype = "CASH" Then sCBType = "BKM" Else sCBType = "BBM"
        sSql = "SELECT MAX(ABS(Right(RTRIM(cashbankno),LEN(cashbankno)-3))) cashbankno FROM QL_trncashbankmst WHERE LEFT(cashbankno,3)='" & sCBType & "'"
        xCmd.CommandText = sSql

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        If Not IsDBNull(xCmd.ExecuteScalar) Then
            iCurID = xCmd.ExecuteScalar + 1
        Else
            iCurID = 1
        End If
        conn.Close()

        Session("vNoCashBank") = GenNumberString(sCBType, "", iCurID, DefCounter)

    End Sub

    Protected Sub calcTotalInGrid()
        Dim iCountrow As Integer = gvmstcost.Rows.Count
        Dim iGtotal As Double = 0

        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(gvmstcost.Rows(i).Cells(3).Text)
        Next
        lblgrandtotal.Text = ToMaskEdit(ToDouble(iGtotal), 2)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim iCountrow As Integer = GVDtlCost.Rows.Count
        Dim iGtotal As Double = 0
        Dim dt As New DataTable
        dt = Session("dtlTable")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVDtlCost.Rows(i).Cells(3).Text) * ToDouble(GVDtlCost.Rows(i).Cells(4).Text)
            dt.Rows(i).BeginEdit()
            dt.Rows(i).Item(0) = i + 1
            dt.Rows(i).EndEdit()
        Next
        Session("dtlTable") = dt
        GVDtlCost.DataSource = dt
        GVDtlCost.DataBind()

        lblTotExpense.Text = "Total Receipt : IDR"
        TotalNetto.Text = ToMaskEdit(ToDouble(iGtotal), 3)
    End Sub

    Protected Sub ClearDtlAP()
        itemdesc.Text = "" : Itemoid.Text = ""
        QtyNya.Text = "0.00" : MaxQty.Text = "0.00"
        trnragsdtlnote.Text = "" : ItemPrice.Text = "0"
        no.Text = "" : trnragsdtlseq.Text = "1"
        i_u2.Text = "New Detail"
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select rate2idrvalue from ql_mstrate2 where rate2year =YEAR(getdate()) and rate2month = MONTH(getdate()) and currencyoid = " & iOid & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        conn.Close()
    End Sub

    Private Sub BindAkun()
        Dim sVar As String = "" : Dim dVal As Boolean = False
        If payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH"
            dVal = True
        ElseIf payflag.SelectedValue = "NON CASH" Then
            sVar = "VAR_BANK"
            dVal = True
        End If

        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & deptoid.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "?" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & cmpcode & "'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND (acctgdesc LIKE '%" & Tchar(txtCoa.Text.Trim) & "%' OR acctgcode LIKE '%" & Tchar(txtCoa.Text.Trim) & "%') AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            FillGV(GvCoa, sSql, "QL_mstacctg")
        End If
    End Sub

    Private Sub BindDataClosed()
        sSql = "Select cmpcode,cb.cashbankoid,cb.cashbankno,cb.cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code from ql_trncashbankmst cb Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cashbankoid ASC"
        Dim cbMst As DataTable = cKoneksi.ambiltabel(sSql, "Ql_CbClosed")
        Session("ClosedMst") = cbMst

        sSql = "Select cb.cmpcode,cashbankgloid,cb.cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cb.upduser,cb.updtime,duedate,refno,cashbankglstatus,cb.branch_code from QL_cashbankgl gl Inner Join QL_trncashbankmst cb ON cb.cashbankoid=gl.cashbankoid Where cb.cashbankgroup='RECEIPT' AND cb.cashbankstatus='' AND cb.cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Order by cb.cashbankoid ASC"
        Dim cbgl As DataTable = cKoneksi.ambiltabel2(sSql, "Ql_GlClosed")
        Session("ClosedDtl") = cbgl

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'insert mst
            For c1 As Int32 = 0 To cbMst.Rows.Count - 1
                sSql = "INSERT into QL_trncbclosedmst (cmpcode,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser,createtime, bankoid,branch_code,flagexpedisi) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbMst.Rows(c1)("cashbankoid")) & ",'" & cbMst.Rows(c1)("cashbankno") & "','CLOSED','" & cbMst.Rows(c1)("cashbanktype") & "','RECEIPT'," & Integer.Parse(cbMst.Rows(c1)("cashbankacctgoid")) & ",'" & cbMst.Rows(c1)("cashbankdate") & "','" & Tchar(cbMst.Rows(c1)("cashbanknote")) & "','" & cbMst.Rows(c1)("upduser") & "',current_timestamp," & cbMst.Rows(c1)("cashbankcurroid") & "," & ToDouble(cbMst.Rows(c1)("cashbankcurrate")) & ", '" & Tchar(cbMst.Rows(c1)("pic")) & "', 'QL_mstperson', '" & Integer.Parse(cbMst.Rows(c1)("deptoid")) & "', '" & cbMst.Rows(c1)("createuser") & "','" & cbMst.Rows(c1)("createtime") & "'," & Integer.Parse(cbMst.Rows(c1)("bankoid")) & ",'" & cbMst.Rows(c1)("branch_code") & "','')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next
            'insert dtl
            For c1 As Int32 = 0 To cbgl.Rows.Count - 1
                sSql = "INSERT into QL_trncbcloseddtl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,upduser,updtime,duedate,refno,cashbankglstatus,branch_code) VALUES " & _
                "('" & cmpcode & "'," & Integer.Parse(cbgl.Rows(c1)("cashbankgloid")) & "," & Integer.Parse(cbgl.Rows(c1)("cashbankoid")) & "," & Integer.Parse(cbgl.Rows(c1)("acctgoid")) & "," & cbgl.Rows(c1)("cashbankglamt") & "," & cbgl.Rows(c1)("cashbankglamtidr") & "," & cbgl.Rows(c1)("cashbankglamtusd") & ",'" & Tchar(cbgl.Rows(c1)("trnragsdtlnote")) & "','" & Tchar(cbgl.Rows(c1)("upduser")) & "','" & cbgl.Rows(c1)("updtime") & "','" & cbgl.Rows(c1)("duedate") & "','" & Tchar(cbgl.Rows(c1)("refno")) & "','" & Tchar(cbgl.Rows(c1)("cashbankglstatus")) & "','" & Tchar(cbgl.Rows(c1)("branch_code")) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cashbankoid IN (Select cashbankoid from ql_trncashbankmst Where cashbankgroup='RECEIPT' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & " <br />" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Private Sub setcontrolCash(ByVal status As Boolean)
        Label17.Visible = status : payrefno.Visible = status : Label8.Visible = status
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal cbOid As String)
        'untuk print
        Dim type As String = payflag.SelectedValue
        Dim cbNo As String = "RECEIPT-" & GetStrData(sSql)
        Dim sWhere As String = "AND cashbankgroup='RECEIPT'"
        If type <> "" Then
            report.Load(Server.MapPath(folderReport & "printBKK.rpt"))
            'Report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("cmpCode", cmpcode)
            report.SetParameterValue("cbOid", cbOid)
            report.SetParameterValue("companyname", "MULTI SARANA COMPUTER")

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
            report.Close() : report.Dispose() : Session("no") = Nothing
        End If
    End Sub
#End Region
 
#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cbPeriode.Checked = True
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        'Proses Auto closed Jika transaksi tanggal kemarin
        sSql = "Select Count(cashbankoid) from ql_trncashbankmst Where cashbankgroup='RAGS' AND cashbankstatus='' AND cashbankdate<(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))"
        Dim cOs As Double = GetStrData(sSql)
        If ToDouble(cOs) > 0 Then
            BindDataClosed()
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim lokasioid As Int32 = Session("lokasioid")
            Dim lokasi As String = Session("lokasi")
            Dim lokasino As String = Session("lokasino")
            Dim sqlExpense As String = Session("SearchExpense")
            Dim cbType As String = Session("CashBankType")

            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole : Session("lokasioid") = lokasioid
            Session("lokasi") = lokasi : Session("lokasino") = lokasino
            Session("SearchExpense") = sqlExpense
            Response.Redirect("TrnNotaRombeng.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("cashbankoid")
        Session("status") = Request.QueryString("cashbankstatus")
        Session("cabang") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        Page.Title = CompnyName & " - Receipt"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date

        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        '=============================================================
        If Not IsPostBack Then
            initDDLAll("") : InitCabang() : InitDDLBranch()
            txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            binddata()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                fillTextBox(Session("oid"), Session("status"), Session("cabang"))
                lblIO.Text = "U P D A T E"
                gvmstcost.Visible = True
                gvmstcost.DataSource = Session("tbldata")
                gvmstcost.DataBind() 
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                setcontrolCash(False)
                cashbankdate.Text = Format(GetServerTime, "dd/MM/yyyy")
                generateCostID() : generateCashBankID()
                btnDelete.Visible = False
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TotalNetto.Text = "0.00" : lblIO.Text = "N E W"
                TabContainer1.ActiveTabIndex = 0
                initddlcasbank(payflag.SelectedValue)  
            End If

            Dim dtlTable As DataTable : dtlTable = Session("dtlTable")
            GVDtlCost.DataSource = dtlTable : GVDtlCost.DataBind()
            calcTotalInGridDtl()
        End If

        If lblPOST.Text = "POST" Or lblPOST.Text = "CLOSED" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtCoa.Text = "" : lblCoa.Text = ""
        initddlcasbank(payflag.SelectedValue)
        If Trim(payflag.SelectedValue) = "CASH" Then
            setcontrolCash(False)
        Else
            setcontrolCash(True)
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        bindAccounting()
    End Sub

    Protected Sub GVDtlCost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            trnragsdtlseq.Text = GVDtlCost.SelectedDataKey.Item("trnragsdtlseq")
            If Session("dtlTable") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("dtlTable")
                Dim dv As DataView = objTable.DefaultView

                dv.RowFilter = "trnragsdtlseq=" & trnragsdtlseq.Text
                Itemoid.Text = dv.Item(0).Item("itemoid")
                itemdesc.Text = dv.Item(0).Item("itemdesc")
                MtrLocoid.Text = dv.Item(0).Item("mtrlocoid")
                ItemPrice.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("trnragsdtlprice")), 3)
                QtyNya.Text = ToMaskEdit(dv.Item(0).Item("trnragsdtlqty"), 3)
                trnragsdtlnote.Text = dv.Item(0).Item("trnragsdtlnote")
                MaxQty.Text = ToMaskEdit(GetStrData("Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) from QL_conmtr Where periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND mtrlocoid=" & Integer.Parse(dv.Item(0).Item("mtrlocoid")) & " AND refoid=" & Integer.Parse(dv.Item(0).Item("itemoid")) & ""), 3)
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Protected Sub GVDtlCost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlCost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlCost.RowDeleting
        Dim objTable As DataTable = Session("dtlTable")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("trnragsdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        GVDtlCost.DataSource = objTable
        GVDtlCost.DataBind()
        ClearDtlAP()
        calcTotalInGridDtl()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("dtlTable") = Nothing
        gvmstcost.PageIndex = 0 : binddata()
        Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("dtlTable") = Nothing
        cbPeriode.Checked = True
        statuse.SelectedIndex = 0
        no.Text = ""
        If Session("branch_id") = "10" Then
            dd_cabang.SelectedValue = "ALL"
        End If
        txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        binddata() : Session("SearchExpense") = sql_temp
    End Sub

    Protected Sub gvmstcost_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvmstcost.PageIndexChanging
        gvmstcost.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub btnCancel_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        Response.Redirect("TrnNotaRombeng.aspx?awal=true")
    End Sub  

    Protected Sub gvmstcost_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvmstcost.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            If e.Row.Cells(6).Text = "&nbsp;" Then
                e.Row.Cells(6).Text = "In Process"
            End If
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        ShowCOA(Session("vNoCashBank"), cashbankoid.Text)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
        End If
    End Sub 

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(toDate(cashbankdate.Text)) = False Or IsDate(toDate(cashbankdate.Text)) = False Then
            showMessage("Tanggal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If CDate(toDate(cashbankdate.Text)) < GetServerTime() Then
            showMessage("Tanggal Tidak bisa kurang dari tanggal sekarang !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If 
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        initddlcasbank(payflag.SelectedValue)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(CashBankNo.Text) & "'"
        PrintReport(CashBankNo.Text, Integer.Parse(Session("oid")))
    End Sub

    Protected Sub gvmstcost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvmstcost.SelectedIndexChanged
        Response.Redirect("~\Transaction\TrnNotaRombeng.aspx?cashbankoid=" & gvmstcost.SelectedDataKey("cashbankoid").ToString & "&cashbankstatus=" & gvmstcost.SelectedDataKey("cashbankstatus").ToString & "&branch_code=" & gvmstcost.SelectedDataKey("branch_code").ToString & "")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Transaction\TrnNotaRombeng.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\TrnNotaRombeng.aspx?awal=true")
    End Sub

    Protected Sub BtnCariCoa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindAkun()
        GvCoa.Visible = True
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtCoa.Text = ""
        GvCoa.Visible = False
    End Sub

    Protected Sub GvCoa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtCoa.Text = GvCoa.SelectedDataKey(1).ToString().Trim & " - " & GvCoa.SelectedDataKey(2S).ToString().Trim
        lblCoa.Text = GvCoa.SelectedDataKey(0).ToString().Trim
        initddlcasbank(payflag.SelectedValue)
        GvCoa.Visible = False
    End Sub

    Protected Sub GvCoa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvCoa.PageIndex = e.NewPageIndex
        BindAkun()
        GvCoa.Visible = True
    End Sub

    Protected Sub btnViewLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewLast.Click
        Session("dtlTable") = Nothing
        If Session("SearchExpense") Is Nothing = False Then
            bindLastSearched()
        End If
    End Sub 

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim sMsg As String = "" : Dim pDate As Date = Format(GetServerTime(), "MM/dd/yyyy")
        Dim mDate As Date = CDate(toDate(cashbankdate.Text))
        '=========================
        'CEK PERIODE AKTIF BULANAN
        '========================= 
        sSql = "Select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl Where glflag='OPEN'"
        If GetDateToPeriodAcctg(GetServerTime()) < GetStrData(sSql) Then
            sMsg &= "Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !"
        End If

        If Not Session("dtlTable") Is Nothing Then
            Dim objTblDetail As DataTable
            Dim objRowDetail() As DataRow
            objTblDetail = Session("dtlTable")
            objRowDetail = objTblDetail.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowDetail.Length = 0 Then
                sMsg &= "Maaf, Detail data harus di isi!!<BR>"
            End If
        Else
            sMsg &= "Maaf, Detail data harus di isi!!<BR>"
        End If

        If payrefno.Text.Trim = "" And payflag.SelectedValue <> "CASH" Then
            sMsg &= "No cek(Payrefno) harus di isi !!<BR>"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trncashbankmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah tersimpan, silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<br />"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT cashbankstatus FROM QL_trncashbankmst WHERE cashbankoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            Dim srest As String = GetStrData(sSql)
            If srest.ToLower = "post" Then
                sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
            End If
        End If

        Dim Pendapatan, OidHpp, OidCOA As Integer
        Dim Var_HPP As String = GetVarInterface("VAR_HPP", deptoid.SelectedValue)
        Dim Var_Piutang As String = GetVarInterface("VAR_ROMBENG", deptoid.SelectedValue)
        Dim Var_Rombeng As String = GetVarInterface("VAR_STOK_ROMBENG", deptoid.SelectedValue)

        If Var_Piutang Is Nothing Or Var_Piutang = "" Then
            sMsg &= "Akun COA untuk VAR_NON_EXP tidak ditemukan..!!<br />"
        Else
            sSql = "SELECT acctgoid FROM ql_mstacctg WHERE cmpcode = '" & cmpcode & "' AND acctgcode = '" & Var_Piutang & "'"
            Pendapatan = GetScalar(sSql)
            If Pendapatan = 0 Or Pendapatan = Nothing Then
                sMsg &= "Akun COA untuk VAR_NON_EXP tidak ditemukan..!!<br />"
            End If
        End If

        If Var_HPP Is Nothing Or Var_HPP = "" Then
            sMsg &= "Akun COA untuk VAR_HPP tidak ditemukan..!!<br />"
        Else
            sSql = "SELECT acctgoid FROM ql_mstacctg WHERE cmpcode = '" & cmpcode & "' AND acctgcode = '" & Var_HPP & "'"
            OidHpp = GetScalar(sSql)
            If OidHpp = 0 Or OidHpp = Nothing Then
                sMsg &= "Akun COA untuk VAR_HPP tidak ditemukan..!!<br />"
            End If
        End If

        If Var_Rombeng Is Nothing Or Var_Rombeng = "" Then
            sMsg &= "Akun COA untuk VAR_STOK_ROMBENG tidak ditemukan..!!<br />"
        Else
            sSql = "SELECT acctgoid FROM ql_mstacctg WHERE cmpcode = '" & cmpcode & "' AND acctgcode = '" & Var_Rombeng & "'"
            OidCOA = GetScalar(sSql)
            If OidCOA = 0 Or OidCOA = Nothing Then
                sMsg &= "Akun COA untuk VAR_STOK_ROMBENG tidak ditemukan..!!<br />"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            lblPOST.Text = ""
            Exit Sub
        End If

        Dim scbtype As String = "", sVar As String = "", iCurID As Integer = 0, sCBCode As String = "", HppNya As Double = 0, rateusd As Double = getCurrency(), RagsNo As String = ""
        If Session("oid") = Nothing Or Session("oid") = "" Then
            cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
            trnragsmstoid.Text = GenerateID("QL_trnragsmst", cmpcode)
        Else
            cashbankoid.Text = Session("oid")
            CashBankNo.Text = Session("oid")
        End If

        Session("vIDCost") = GenerateID("QL_cashbankgl", cmpcode)
        trnragsdtloid.Text = GenerateID("QL_trnragsdtl", cmpcode)
        Dim conmtroid As Integer = GenerateID("QL_conmtr", cmpcode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", cmpcode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", cmpcode)

        If payflag.SelectedValue = "CASH" Then
            scbtype = "BKM" : sVar = "VAR_CASH"
        ElseIf payflag.SelectedValue = "NON CASH" Then
            scbtype = "BBM" : sVar = "VAR_BANK"
        End If

        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & deptoid.SelectedValue & "' AND gengroup='CABANG'")

        If lblPOST.Text = "POST" Then
            Dim sNo As String = scbtype & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1 FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & deptoid.SelectedValue & "'"
            CashBankNo.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)

            Dim NoRags As String = "RAGS/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT isnull(max(abs(replace(trnragsno,'" & sNo & "',''))),0)+1 FROM QL_trnragsmst WHERE trnragsno LIKE '" & NoRags & "%' and branch_code='" & deptoid.SelectedValue & "'"
            RagsNo = GenNumberString(NoRags, "", cKoneksi.ambilscalar(sSql), 4)
        Else
            RagsNo = trnragsmstoid.Text
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_trncashbankmst (cmpcode, cashbankoid, cashbankno, cashbankstatus, cashbanktype, cashbankgroup, cashbankacctgoid, cashbankdate, cashbanknote, upduser, updtime, cashbankcurroid, cashbankcurrate, pic, PIC_REFNAME, deptoid, createuser, createtime, bankoid, branch_code, cashbankamount, cashbankamountidr, cashbankamountusd, cashbankduedate) VALUES " & _
                "('" & cmpcode & "', " & cashbankoid.Text & ", '" & CashBankNo.Text & "', '" & lblPOST.Text & "', '" & scbtype & "', 'RAGS', " & Integer.Parse(lblCoa.Text) & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(cashbanknote.Text) & "', '" & Session("UserID") & "', Current_timestamp, 1, 1, 0, '', '" & deptoid.SelectedValue & "', '" & Session("userid") & "', '" & CDate(toDate(createtime.Text)) & "', 0, '" & deptoid.SelectedValue & "', " & ToDouble(TotalNetto.Text) & ", " & ToDouble(TotalNetto.Text) & ", '1', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)))"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO [QL_trnragsmst] ([cmpcode], [branch_code], [trnragsmstoid], [trnragsno], [trnragsdate], [cashbankmstoid], [acctgoid], [typerags], [trnragsamt], [trnragsstatus], [trnragsnote], [createtime], [createuser], [updtime], [upduser])" & _
                " VALUES ('" & cmpcode & "', '" & deptoid.SelectedValue & "', " & trnragsmstoid.Text & ", '" & RagsNo & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & cashbankoid.Text & ", " & lblCoa.Text & ", '" & scbtype & "', " & ToDouble(TotalNetto.Text) & ", '" & lblPOST.Text & "', '" & Tchar(cashbanknote.Text) & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Session("userid") & "', current_timestamp, '" & Session("userid") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

                sSql = "UPDATE QL_mstoid SET lastoid=" & Integer.Parse(cashbankoid.Text) & " WHERE tablename='QL_trncashbankmst' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Integer.Parse(trnragsmstoid.Text) & " WHERE tablename='QL_trnragsmst' AND cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else

                sSql = "UPDATE QL_trncashbankmst SET cashbankno='" & CashBankNo.Text & "', cashbankacctgoid=" & lblCoa.Text & ", cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), cashbankstatus='" & lblPOST.Text & "', upduser = '" & Session("UserID") & "', updtime=current_timestamp, cashbanknote='" & Tchar(cashbanknote.Text) & "', deptoid='" & deptoid.SelectedValue & "', cashbanktype='" & scbtype & "' WHERE cmpcode = '" & cmpcode & "' AND cashbankoid = " & Integer.Parse(cashbankoid.Text) & " AND branch_code='" & deptoid.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE [QL_trnragsmst] SET [branch_code] = '" & deptoid.SelectedValue & "', [trnragsno] = '" & RagsNo & "', [trnragsdate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [cashbankmstoid] = " & Integer.Parse(cashbankoid.Text) & ", [acctgoid] = " & Integer.Parse(lblCoa.Text) & ", [typerags] = '" & scbtype & "', [trnragsamt] =" & ToDouble(TotalNetto.Text) & ", [trnragsstatus] = '" & lblPOST.Text & "', [trnragsnote] = '" & Tchar(cashbanknote.Text) & "', [updtime] = current_timestamp, [upduser] = '" & Session("UserID") & "' WHERE trnragsmstoid=" & Integer.Parse(trnragsmstoid.Text) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Not Session("dtlTable") Is Nothing Then
                Dim objTable As DataTable : Dim objRow() As DataRow
                objTable = Session("dtlTable")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid = " & Integer.Parse(cashbankoid.Text) & " AND branch_code='" & deptoid.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "DELETE FROM QL_trnragsdtl WHERE trnragsmstoid = " & Integer.Parse(trnragsmstoid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                For C1 As Int16 = 0 To objRow.Length - 1
                    sSql = "INSERT into QL_cashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglnote, upduser, updtime, duedate, refno, cashbankglstatus, branch_code) VALUES " & _
                    "('" & cmpcode & "'," & (Session("vIDCost") + C1) & "," & Integer.Parse(cashbankoid.Text) & "," & Pendapatan & "," & ToDouble(objRow(C1)("trnragsdtlprice")) * ToDouble(objRow(C1)("trnragsdtlqty")) & "," & ToDouble(objRow(C1)("trnragsdtlprice")) * ToDouble(objRow(C1)("trnragsdtlqty")) & "," & ToDouble(objRow(C1)("trnragsdtlprice")) * ToDouble(objRow(C1)("trnragsdtlqty")) & ",'" & Tchar(objRow(C1)("trnragsdtlnote")) & "','" & Session("UserID") & "',current_timestamp,current_timestamp,'" & Tchar(payrefno.Text) & "','" & lblPOST.Text & "','" & deptoid.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO [QL_trnragsdtl] ([cmpcode], [branch_code], [trnragsdtloid], [trnragsmstoid], [trncashbankdtloid], [trnragsdtlseq], [itemoid], [mtrlocoid], [trnragsdtlqty], [trnragsdtlprice], [trnragsdtlnetto], [trnragsdtlstatus], [trnragsdtlnote], [createtime], [createuser], [updtime], [upduser])" & _
                    " VALUES ('" & cmpcode & "', '" & deptoid.SelectedValue & "', " & trnragsdtloid.Text + C1 & ", " & trnragsmstoid.Text & ", " & (Session("vIDCost") + C1) & ", " & objRow(C1)("trnragsdtlseq") & ", " & objRow(C1)("itemoid") & ", " & objRow(C1)("mtrlocoid") & ", " & objRow(C1)("trnragsdtlqty") & ", " & objRow(C1)("trnragsdtlprice") & ", " & ToDouble(objRow(C1)("trnragsdtlprice")) * ToDouble(objRow(C1)("trnragsdtlqty")) & ", '" & lblPOST.Text & "', '" & Tchar(objRow(C1)("trnragsdtlnote")) & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If lblPOST.Text = "POST" Then
                        sSql = "SELECT hpp FROM ql_mstitem WHERE itemoid=" & objRow(C1)("itemoid") & ""
                        xCmd.CommandText = sSql : HppNya = xCmd.ExecuteScalar
                        '== INSERT KE STOK KELUAR BARANG ROMBENG ==
                        sSql = " INSERT INTO QL_conmtr (cmpcode, conmtroid, formoid, Formname, type, trndate, periodacctg, mtrlocoid, refoid, qtyout, amount, note, FormAction, upduser, updtime, unitoid, refname, hpp, branch_code) VALUES" & _
                        " ('" & cmpcode & "', " & (conmtroid + C1) & ", " & CInt(trnragsdtloid.Text) + C1 & ", 'QL_trnragsdtl', 'RAGS', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & objRow(C1)("mtrlocoid") & ", " & objRow(C1)("itemoid") & ", " & ToDouble(objRow(C1)("trnragsdtlqty")) & ", " & ToDouble(objRow(C1)("trnragsdtlqty")) * ToDouble(HppNya) & ", '" & Tchar(objRow(C1)("trnragsdtlnote")) & " - " & Tchar(RagsNo) & " ', '" & Tchar(RagsNo) & "', '" & Session("UserID") & "', current_timestamp, 945, 'QL_MSTITEM', " & ToDouble(HppNya) & ", '" & deptoid.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") + objRow.Length) & " WHERE tablename='QL_cashbankgl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & (trnragsdtloid.Text + objRow.Length) & " WHERE tablename='QL_trnragsdtl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & (objRow.Length - 1 + conmtroid) & " Where tablename = 'QL_conmtr' And cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If lblPOST.Text = "POST" Then
                'update status cashbankmst
                sSql = "UPDATE ql_trncashbankmst SET cashbankno='" & CashBankNo.Text & "', posttime=current_timestamp, cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), postuser='" & Session("userid") & "' Where cashbankoid=" & Integer.Parse(cashbankoid.Text) & " And cmpcode='" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

                '//////INSERT INTO TRN GL MST
                sSql = "INSERT into QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, type, branch_code) VALUES " & _
                " ('" & cmpcode & "', " & vIDMst & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetDateToPeriodAcctg(GetServerTime()) & "', 'User." & Tchar(Session("UserID")) & " | No. " & CashBankNo.Text & " | Note." & Tchar(cashbanknote.Text) & " | No. Rags." & RagsNo & "', 'POST', '" & GetServerTime() & "', '" & Session("UserID") & "', current_timestamp, 'RAGS', '" & deptoid.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE tablename='QL_trnglmst'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim iSeq As Integer = 1
                Dim dt As DataTable : dt = Session("dtlTable") 
                If Not Session("dtlTable") Is Nothing Then
                    sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glflag) " & _
                       "VALUES ('" & cmpcode & "', " & vIDDtl & ", 1, " & vIDMst & ", " & lblCoa.Text & ", 'D', '" & ToDouble(TotalNetto.Text) & "', '" & ToDouble(TotalNetto.Text) & "', '1', '" & Tchar(CashBankNo.Text) & "', 'User." & Tchar(Session("UserID")) & " | No. " & CashBankNo.Text & " | Note." & Tchar(cashbanknote.Text) & " | No. Rags." & RagsNo & "', '" & Session("UserID") & "', current_timestamp, '" & cashbankoid.Text & "', '" & deptoid.SelectedValue & "', '" & lblPOST.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDDtl = vIDDtl + 1

                    sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glflag) VALUES " & _
                      " ('" & cmpcode & "', " & vIDDtl & ", 2, " & vIDMst & ", " & Pendapatan & ", 'C', " & ToDouble(TotalNetto.Text) & ", " & ToDouble(TotalNetto.Text) & ", " & ToDouble(TotalNetto.Text) & ", '" & CashBankNo.Text & "', 'User." & Tchar(Session("UserID")) & " | No. " & CashBankNo.Text & " | Note." & Tchar(cashbanknote.Text) & " | No. Rags." & RagsNo & "', '" & Session("UserID") & "', current_timestamp, '" & cashbankoid.Text & "', '" & deptoid.SelectedValue & "', '" & lblPOST.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDDtl = vIDDtl + 1 : Dim AmtHpp, TotalHPP As Double

                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "SELECT hpp FROM ql_mstitem WHERE itemoid=" & dt.Rows(C1)("itemoid") & ""
                        xCmd.CommandText = sSql : HppNya = xCmd.ExecuteScalar
                        AmtHpp += HppNya * ToDouble(dt.Rows.Item(C1)("trnragsdtlqty"))
                    Next
                    TotalHPP = AmtHpp
                    sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glflag) VALUES " & _
                    " ('" & cmpcode & "', " & vIDDtl & ", 3, " & vIDMst & ", " & OidHpp & ", 'D', " & TotalHPP & ", " & TotalHPP & ", " & TotalHPP & ", '" & CashBankNo.Text & "', 'User." & Tchar(Session("UserID")) & " | No. " & CashBankNo.Text & " | Note." & Tchar(cashbanknote.Text) & " | No. Rags." & RagsNo & "', '" & Session("UserID") & "', current_timestamp, '" & cashbankoid.Text & "', '" & deptoid.SelectedValue & "', '" & lblPOST.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDDtl = vIDDtl + 1

                    sSql = "INSERT into QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, upduser, updtime, GLOTHER2, branch_code, glflag) VALUES " & _
                   " ('" & cmpcode & "', " & vIDDtl & ", 4, " & vIDMst & ", " & OidCOA & ", 'C', " & TotalHPP & ", " & TotalHPP & ", " & TotalHPP & ", '" & CashBankNo.Text & "', 'User." & Tchar(Session("UserID")) & " | No. " & CashBankNo.Text & " | Note." & Tchar(cashbanknote.Text) & " | No. Rags." & RagsNo & "', '" & Session("UserID") & "', current_timestamp, '" & cashbankoid.Text & "', '" & deptoid.SelectedValue & "', '" & lblPOST.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl & " WHERE tablename='QL_trngldtl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code AND cb.cashbankoid=g.cashbankoid AND g.trnid=0"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "<br/>" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\TrnNotaRombeng.aspx?awal=true")
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Try
            Dim sMsg As String = ""
            If Itemoid.Text = "" Then
                sMsg &= "item harus diisi !!<BR>"
            End If

            If ToDouble(ItemPrice.Text) <= 0 Then
                sMsg &= "Price harus >= 0!!"
            End If

            If lblCoa.Text = "0" Then
                sMsg &= "Cash/Akun Bank belum dipilih!!"
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If Session("dtlTable") Is Nothing Then
                Dim dt As New DataTable
                dt.Columns.Add("trnragsdtlseq", Type.GetType("System.Int32"))
                dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dt.Columns.Add("itemdesc", Type.GetType("System.String"))
                dt.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
                dt.Columns.Add("trnragsdtlqty", Type.GetType("System.Decimal"))
                dt.Columns.Add("trnragsdtlprice", Type.GetType("System.Decimal"))
                dt.Columns.Add("trnragsdtlnetto", Type.GetType("System.Decimal"))
                dt.Columns.Add("trnragsdtlnote", Type.GetType("System.String"))
                Session("dtlTable") = dt
            End If

            Dim objTable As DataTable = Session("dtlTable")
            Dim dv As DataView = objTable.DefaultView

            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & Itemoid.Text & ""
            Else
                dv.RowFilter = "itemoid=" & Itemoid.Text & " AND trnragsdtlseq <> " & trnragsdtlseq.Text & ""
            End If

            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("detail item sudah ada..!!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            dv.RowFilter = ""

            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("trnragsdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(trnragsdtlseq.Text - 1)
                objRow.BeginEdit()
            End If

            objRow("itemoid") = Itemoid.Text
            objRow("itemdesc") = itemdesc.Text
            objRow("mtrlocoid") = MtrLocoid.Text
            objRow("trnragsdtlqty") = ToDouble(QtyNya.Text)
            objRow("trnragsdtlprice") = ToDouble(ItemPrice.Text)
            objRow("trnragsdtlnetto") = ToDouble(ItemPrice.Text) * ToDouble(QtyNya.Text)
            objRow("trnragsdtlnote") = trnragsdtlnote.Text

            Session("dtlTable") = objTable
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If

            GVDtlCost.DataSource = Session("dtlTable")
            GVDtlCost.DataBind()
            ClearDtlAP() : calcTotalInGridDtl()
            payflag.Enabled = True
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_cashbankgl WHERE cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code='" & deptoid.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text) & " AND branch_code='" & deptoid.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete QL_trnragsmst Where trnragsmstoid=" & Integer.Parse(trnragsmstoid.Text) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete QL_trnragsdtl Where trnragsmstoid=" & Integer.Parse(trnragsmstoid.Text) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "<br/>" & sSql, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("dtlTable") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK") 
    End Sub 

    Protected Sub BtnCari_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCari.Click
        bindAccounting()
        GVItem.Visible = True
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        lblPOST.Text = "POST" : btnsave_Click(sender, e)
    End Sub

    Protected Sub GVItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItem.PageIndexChanging
        GVItem.PageIndex = e.NewPageIndex
        bindAccounting() : GVItem.Visible = True
    End Sub

    Protected Sub GVItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItem.RowDataBound

    End Sub

    Protected Sub GVItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItem.SelectedIndexChanged
        Try
            Itemoid.Text = GVItem.SelectedDataKey.Item("refoid").ToString
            itemdesc.Text = GVItem.SelectedDataKey.Item("itemdesc").ToString
            QtyNya.Text = ToMaskEdit(GVItem.SelectedDataKey.Item("SaldoNya"), 3)
            MtrLocoid.Text = GVItem.SelectedDataKey.Item("mtrlocoid")
            MaxQty.Text = ToMaskEdit(GetStrData("Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) from QL_conmtr Where periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND mtrlocoid=" & Integer.Parse(GVItem.SelectedDataKey.Item("mtrlocoid")) & " AND refoid=" & Integer.Parse(GVItem.SelectedDataKey.Item("refoid")) & ""), 3)
            GVItem.Visible = False
            If QtyNya.Text > MaxQty.Text Then
                showMessage("Maaf, Quantity Stok tidak mencukupi..!!", CompnyName & " - ERROR !!", 1, "modalMsgBox")
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR !!", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Protected Sub ItemPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ItemPrice.TextChanged
        If ItemPrice.Text = "" Then
            ItemPrice.Text = 0
        End If
        ItemPrice.Text = ToMaskEdit(ItemPrice.Text, 3)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDtlAP() : GVDtlCost.SelectedIndex = -1
        cashbankgloid.Text = Session("vIDCost")
    End Sub

    Protected Sub BtnPrintGV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cashbankno As String = sender.tooltip
        sSql = "SELECT cashbankoid FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & cashbankno & "'"
        PrintReport(cashbankno.ToString, Integer.Parse(GetStrData(sSql)))
    End Sub
#End Region
End Class
