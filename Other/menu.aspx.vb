'Prgmr:zipi | LastUpdt:23.05.13
Imports System.Data
Imports System.Text.RegularExpressions
Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Net.Mail
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class menu
    Inherits System.Web.UI.Page
    Dim dv As DataView
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlSelect As String = "SELECT DISTINCT [QL_ROLEDTL].[FORMTYPE], [QL_ROLEDTL].[FORMNAME], [QL_ROLEDTL].[FORMADDRESS] FROM [QL_ROLEDTL] INNER JOIN [QL_ROLE] ON [QL_ROLE].[ROLEOID]=[QL_ROLEDTL].[ROLEOID] AND [QL_ROLEDTL].[CMPCODE] = [QL_ROLE].[CMPCODE] INNER JOIN [QL_USERROLE] ON [QL_ROLE].[ROLEOID]=[QL_USERROLE].[ROLEOID] AND [QL_USERROLE].[CMPCODE] = [QL_ROLE].[CMPCODE] WHERE ([QL_USERROLE].[CMPCODE] = '" & CompnyCode & "') AND ([QL_USERROLE].[USERPROF] = '" & Session("UserID") & "')"
        Session("Role") = cKoneksi.ambiltabel(sqlSelect, "Role")

        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            'dv = Session("Role").DefaultView
            'MsgBox(dv.Count)
            Response.Redirect("menu.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Home"
        Dim sql As String = ""
        Dim sSqli As String = "Select potypeapp from QL_mstprof Where USERID='" & Session("UserID") & "'"
        Dim sType As String = GetStrData(sSqli)

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If sType.ToString = "True" Then
            sql &= " AND po.typepo='Selisih'"
        Else
            sql &= " AND po.typepo='Normal'"
        End If
 
        Dim QlSO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNORDERMST' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCode As String = "" : Dim sCodeSO As String = ""
        Dim SoCode() As String = QlSO.Split(",")
        For C1 As Integer = 0 To SoCode.Length - 1
            If SoCode(C1) <> "" Then
                sCodeSO &= "'" & SoCode(C1).Trim & "',"
            End If
        Next

        Dim QlDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjjualmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeDO As String = ""
        Dim DOCode() As String = QlDO.Split(",")
        For C1 As Integer = 0 To DOCode.Length - 1
            If DOCode(C1) <> "" Then
                sCodeDO &= "'" & DOCode(C1).Trim & "',"
            End If
        Next

        Dim QlSR As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnjualreturmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeSR As String = ""
        Dim SRCode() As String = QlSR.Split(",")
        For C1 As Integer = 0 To SRCode.Length - 1
            If SRCode(C1) <> "" Then
                sCodeSR &= "'" & SRCode(C1).Trim & "',"
            End If
        Next

        Dim QlADJ As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnstockadj' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeAdj As String = ""
        Dim AdjCode() As String = QlADJ.Split(",")
        For C1 As Integer = 0 To AdjCode.Length - 1
            If AdjCode(C1) <> "" Then
                sCodeAdj &= "'" & AdjCode(C1).Trim & "',"
            End If
        Next

        Dim QlPO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_pomst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePO As String = ""
        Dim POCode() As String = QlPO.Split(",")
        For C1 As Integer = 0 To POCode.Length - 1
            If POCode(C1) <> "" Then
                sCodePO &= "'" & POCode(C1).Trim & "',"
            End If
        Next

        Dim QlPOC As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNPOCLOSE' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePOC As String = ""
        Dim POCCode() As String = QlPOC.Split(",")
        For C1 As Integer = 0 To POCCode.Length - 1
            If POCCode(C1) <> "" Then
                sCodePOC &= "'" & POCCode(C1).Trim & "',"
            End If
        Next

        Dim QlPDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjbelimst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePdO As String = ""
        Dim PdOCode() As String = QlPDO.Split(",")
        For C1 As Integer = 0 To PdOCode.Length - 1
            If PdOCode(C1) <> "" Then
                sCodePdO &= "'" & PdOCode(C1).Trim & "',"
            End If
        Next

        Dim QlPR As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbelireturmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePr As String = ""
        Dim PrCode() As String = QlPR.Split(",")
        For C1 As Integer = 0 To PrCode.Length - 1
            If PrCode(C1) <> "" Then
                sCodePr &= "'" & PrCode(C1).Trim & "',"
            End If
        Next

        Dim QlTw As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trntrfmtrmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeTw As String = ""
        Dim TwCode() As String = QlTw.Split(",")
        For C1 As Integer = 0 To TwCode.Length - 1
            If TwCode(C1) <> "" Then
                sCodeTw &= "'" & TwCode(C1).Trim & "',"
            End If
        Next

        Dim QlTws As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trfwhservicemst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeTws As String = ""
        Dim TwsCode() As String = QlTws.Split(",")
        For C1 As Integer = 0 To TwsCode.Length - 1
            If TwsCode(C1) <> "" Then
                sCodeTws &= "'" & TwsCode(C1).Trim & "',"
            End If
        Next

        Dim QlPro As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstpromosupp' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePro As String = ""
        Dim pRoCode() As String = QlPro.Split(",")
        For C1 As Integer = 0 To pRoCode.Length - 1
            If pRoCode(C1) <> "" Then
                sCodePro &= "'" & pRoCode(C1).Trim & "',"
            End If
        Next

        Dim QlExp As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbiayaeksmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeExp As String = ""
        Dim ExpRoCode() As String = QlExp.Split(",")
        For C1 As Integer = 0 To ExpRoCode.Length - 1
            If ExpRoCode(C1) <> "" Then
                sCodeExp &= "'" & ExpRoCode(C1).Trim & "',"
            End If
        Next

        Dim QlCN As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_creditnote' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeCN As String = ""
        Dim CNRoCode() As String = QlCN.Split(",")
        For C1 As Integer = 0 To CNRoCode.Length - 1
            If CNRoCode(C1) <> "" Then
                sCodeCN &= "'" & CNRoCode(C1).Trim & "',"
            End If
        Next

        Dim QlDN As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbiayaeksmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeDN As String = ""
        Dim DNRoCode() As String = QlDN.Split(",")
        For C1 As Integer = 0 To DNRoCode.Length - 1
            If DNRoCode(C1) <> "" Then
                sCodeDN &= "'" & DNRoCode(C1).Trim & "',"
            End If
        Next

        Dim QlCA As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcustapproval' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeCA As String = ""
        Dim CACode() As String = QlCA.Split(",")
        For C1 As Integer = 0 To CACode.Length - 1
            If CACode(C1) <> "" Then
                sCodeCA &= "'" & CACode(C1).Trim & "',"
            End If
        Next

        Dim QlFis As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNFINAL' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeFin As String = ""
        Dim FinCode() As String = QlFis.Split(",")
        For C1 As Integer = 0 To FinCode.Length - 1
            If FinCode(C1) <> "" Then
                sCodeFin &= "'" & FinCode(C1).Trim & "',"
            End If
        Next

        Dim QlFit As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnfinalintmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeFit As String = ""
        Dim FitCode() As String = QlFit.Split(",")
        For C1 As Integer = 0 To FitCode.Length - 1
            If FitCode(C1) <> "" Then
                sCodeFit &= "'" & FitCode(C1).Trim & "',"
            End If
        Next

        Dim Qlpb As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnrequestitem' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodepb As String = ""
        Dim PbCode() As String = Qlpb.Split(",")
        For C1 As Integer = 0 To PbCode.Length - 1
            If PbCode(C1) <> "" Then
                sCodepb &= "'" & PbCode(C1).Trim & "',"
            End If
        Next

        Dim Qlfc As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='Ql_trnforecastmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodefc As String = ""
        Dim FcCode() As String = Qlfc.Split(",")
        For C1 As Integer = 0 To FcCode.Length - 1
            If FcCode(C1) <> "" Then
                sCodefc &= "'" & FcCode(C1).Trim & "',"
            End If
        Next

        ' Add UNION ALL query bila ada approval baru
        sSql = "SELECT COUNT(-1) jml,1 seq,'PURCHASE ORDER' AS type, '' AS address FROM ql_approval a INNER JOIN ql_pomst po ON a.cmpcode=po.cmpcode AND a.oid=po.trnbelimstoid WHERE po.trnbelitype='GROSIR' and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_pomst' AND statusrequest='New' AND event='In Approval' AND po.trnbelistatus='In Approval' " & sql & " AND a.branch_code IN (" & Left(sCodePO, sCodePO.Length - 1) & ")" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) jml,2 seq,'ADJUSMENT STOCK' AS type,'' AS address FROM ql_approval a WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnstockadj' AND statusrequest='New' AND event='In Approval' And a.branch_code IN (" & Left(sCodeAdj, sCodeAdj.Length - 1) & ")" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) jml,3 seq,'PO CLOSE' AS type,'' AS address FROM ql_approval a INNER JOIN ql_pomst po ON a.cmpcode=po.cmpcode AND a.oid=po.trnbelimstoid WHERE po.trnbelitype='GROSIR' and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='QL_TRNPOCLOSE' AND statusrequest='New' AND event='In Approval' AND po.trnbelistatus='In Approval' " & sql & " AND a.branch_code IN (" & Left(sCodePOC, sCodePOC.Length - 1) & ")" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) jml,4 seq,'PURCHASE DELIVERY ORDER' AS type,'' AS address FROM ql_approval a INNER JOIN QL_TRNSJBELIMST sj ON a.cmpcode=sj.cmpcode AND a.oid=sj.trnsjbelioid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnsjbelimst' AND statusrequest='New' AND event='In Approval' AND sj.trnsjbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePdO, sCodePdO.Length - 1) & ")" & _
             "UNION ALL " & _
              "SELECT COUNT(-1) jml,5 seq,'PURCHASE RETURN' AS type,'' AS address FROM QL_approval a inner join ql_trnbelireturmst b on a.cmpcode = b.cmpcode and a.oid = b.trnbeliReturmstoid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnbelireturmst' AND statusrequest='New' AND event='In Approval' AND b.trnbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePr, sCodePr.Length - 1) & ")" & _
             " UNION ALL " & _
  " SELECT COUNT(-1) jml,7 seq,'SALES ORDER' AS type,'' AS address FROM ql_approval a INNER JOIN QL_trnordermst so ON a.cmpcode=so.cmpcode AND a.oid=so.ordermstoid and a.branch_code = so.branch_code WHERE so.branch_code IN (" & Left(sCodeSO, sCodeSO.Length - 1) & ") and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnordermst' AND statusrequest='New' AND event='In Approval' AND so.trnorderstatus='In Approval'" & _
              "UNION ALL " & _
              "SELECT COUNT(-1) jml,8 seq,'DELIVERY ORDER' AS type,'' AS address FROM ql_trnsjjualmst jj inner JOIN QL_APPROVAL ap ON ap.OID=jj.trnsjjualmstoid AND jj.branch_code=ap.branch_code inner JOIN QL_trnordermst so on so.orderno=jj.orderno AND so.branch_code=jj.branch_code WHERE(jj.cmpcode = ap.CMPCODE And jj.cmpcode = so.cmpcode) AND jj.orderno=so.orderno AND jj.trnsjjualmstoid = ap.OID AND ap.statusrequest = 'New' AND (ap.EVENT = 'In Approval') AND (jj.cmpcode ='MSC') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'QL_trnsjjualmst') AND (jj.trnsjjualstatus = 'In Approval') And jj.branch_code IN (" & Left(sCodeDO, sCodeDO.Length - 1) & ")" & _
              "UNION ALL " & _
              "SELECT COUNT(-1) jml,9 seq,'TRANSFER WAREHOUSE' AS type,'' AS address From QL_approval a inner join QL_trntrfmtrmst b on a.cmpcode = b.cmpcode and a.oid = b.trfmtrmstoid WHERE a.approvaluser='" & Session("UserID") & "' and a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trntrfmtrmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.status = 'In Approval' AND a.branch_code IN (" & Left(sCodeTw, sCodeTw.Length - 1) & ")" & _
              "UNION ALL " & _
              "SELECT COUNT(-1) jml,10 seq,'TW SERVICE CUSTOMER' AS type,'' AS address From QL_approval a inner join QL_trfwhservicemst b on a.cmpcode = b.cmpcode and a.oid = b.trfwhserviceoid WHERE a.approvaluser='" & Session("UserID") & "' and a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trfwhservicemst' AND statusrequest = 'New' AND event = 'In Approval' AND b.trfwhservicestatus = 'In Approval' AND b.trfwhservicetype = 'INTERNAL' AND a.branch_code IN (" & Left(sCodeTws, sCodeTws.Length - 1) & ")" & _
              "UNION ALL " & _
              "SELECT COUNT(-1) jml,11 seq,'SERVICE SUPPLIER INTERNAL' AS type,'' AS address From QL_approval a inner join QL_trfwhservicemst b on a.cmpcode = b.cmpcode and a.oid = b.trfwhserviceoid WHERE a.approvaluser='" & Session("UserID") & "' and a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trfwhservicemst' AND statusrequest = 'New' AND event = 'In Approval' AND b.trfwhservicestatus = 'In Approval' AND b.trfwhservicetype = 'INTERNSUPP' AND a.branch_code IN (" & Left(sCodeTws, sCodeTws.Length - 1) & ")" & _
             "UNION ALL " & _
              "SELECT COUNT(distinct oid) jml,12 seq,'SALES RETURN' AS type,'' AS address From QL_approval a inner join QL_trnjualreturmst b on a.cmpcode = b.cmpcode and a.oid = b.trnjualreturmstoid WHERE a.branch_code IN (" & Left(sCodeSR, sCodeSR.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_trnjualreturmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.trnjualstatus = 'In Approval' " & _
              " UNION ALL SELECT COUNT(distinct oid) jml,13 seq,'PROMO SUPPLIER' AS type,'' AS address From QL_approval a inner join QL_mstpromosupp b on a.cmpcode = b.cmpcode and a.oid = b.promooid WHERE a.branch_code IN (" & Left(sCodePro, sCodePro.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_mstpromosupp' AND statusrequest = 'New' AND event = 'In Approval' AND b.statuspromo = 'In Approval' " & _
              "UNION ALL SELECT COUNT(distinct oid) jml,14 seq,'NOTA NT' AS type,'' AS address From QL_approval a inner join ql_trnbiayaeksmst b on a.cmpcode = b.cmpcode and a.oid = b.trnbiayaeksoid WHERE a.branch_code IN (" & Left(sCodeExp, sCodeExp.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'ql_trnbiayaeksmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.approvalstatus = 'In Approval' " & _
              " UNION ALL SELECT COUNT(distinct a.oid) jml,15 seq,'CREDIT NOTE' AS type,'' AS address From QL_approval a inner join QL_CreditNote b on a.cmpcode = b.cmpcode and a.oid = b.oid WHERE /*a.branch_code IN (" & Left(sCodeCN, sCodeCN.Length - 1) & ") And*/ a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_creditnote' AND statusrequest = 'New' AND event = 'In Approval' AND b.cnstatus = 'In Approval' " & _
              " UNION ALL SELECT COUNT(distinct a.oid) jml,16 seq,'DEBIT NOTE' AS type,'' AS address From QL_approval a inner join QL_DebitNote b on a.cmpcode = b.cmpcode and a.oid = b.oid WHERE /*a.branch_code IN (" & Left(sCodeDN, sCodeDN.Length - 1) & ") And*/ a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_DebitNote' AND statusrequest = 'New' AND event = 'In Approval' AND b.dnstatus = 'In Approval' " & _
              "UNION ALL SELECT COUNT(distinct oid) jml,17 seq,'CUSTOMER APPROVAL' AS type,'' AS address From QL_approval a inner join QL_mstcustapproval b on a.cmpcode = b.cmpcode and a.oid = b.custapprovaloid WHERE a.branch_code IN (" & Left(sCodeCA, sCodeCA.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_mstcustapproval' AND statusrequest = 'New' AND event = 'In Approval' AND b.custapprovalstatus='In Approval' " & _
              "UNION ALL SELECT COUNT(distinct oid) jml,18 seq,'SERVICE FINAL' AS type,'' AS address From QL_approval a inner join QL_TRNFINAL b on a.cmpcode = b.cmpcode and a.oid = b.FINOID WHERE a.branch_code IN (" & Left(sCodeFin, sCodeFin.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_TRNFINAL' AND statusrequest = 'New' AND event = 'In Approval' AND b.FINSTATUS='IN APPROVAL' " & _
              "UNION ALL SELECT COUNT(distinct oid) jml,19 seq,'SERVICE FINAL INTERNAL' AS type,'' AS address From QL_approval a inner join QL_trnfinalintmst b on a.cmpcode = b.cmpcode and a.oid = b.finoid WHERE a.branch_code IN (" & Left(sCodeFit, sCodeFit.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_trnfinalintmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.finitemstatus='IN APPROVAL' " & _
              "UNION ALL SELECT COUNT(distinct oid) jml,20 seq,'PERMINTAAN BARANG' AS type,'' AS address From QL_approval a inner join QL_trnrequestitem b on a.cmpcode = b.cmpcode and a.oid = b.trnrequestitemoid WHERE a.branch_code IN (" & Left(sCodepb, sCodepb.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_trnrequestitem' AND statusrequest = 'New' AND event = 'In Approval' AND b.trnrequestitemstatus='IN APPROVAL'" & _
              " UNION ALL SELECT COUNT(distinct oid) jml, 20 seq, 'FORECAST PO' AS type,'' AS address From QL_approval a Inner JOIN Ql_trnforecastmst b on a.cmpcode = b.cmpcode and a.oid = b.trnforecastoid WHERE a.branch_code IN (" & Left(sCodefc, sCodefc.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'Ql_trnforecastmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.trnforecaststatus='IN APPROVAL'"

        'MASTER ITEM PRICE < LAST PRICE' nie taruh paling bawah trus

        Dim tbApproval As DataTable = cKon.ambiltabel(sSql, "QL_approval")
        Dim dvApproval As DataView = tbApproval.DefaultView
        dvApproval.RowFilter = "jml>0"
        dtApproval.DataSource = dvApproval
        dtApproval.DataBind()

        Dim bVisible As Boolean = False
        If Not IsPostBack Then

            If Session("branch_id") = "10" Then
                'Init Notify
                InitNotifySOInproc()
                TDNotifyDOinproc.Visible = True

                InitNotifyPricelist()
                TDNotifyPricelist.Visible = True

                InitNotifyPromo()
                TDNotifyPromo.Visible = True

                InitNotifyPiutangDueDate()
                TDNotifyPiutangDueDate.Visible = True

                InitNotifyPOoutstanding()
                TDNotifyPOoutstanding.Visible = True
            End If
            BindDataList()

            dv = Session("Role").DefaultView
            ' ======
            ' MASTER
            ' ======
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROFILE'"
            If dv.Count > 0 Then
                mnuProfile.Text = SetLinking("User.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'ROLE'"
            If dv.Count > 0 Then
                mnuRole.Text = SetLinking("Role.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'USER ROLE'"
            If dv.Count > 0 Then
                mnuUserRole.Text = SetLinking("UserRole.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'GENERAL'"
            If dv.Count > 0 Then
                mnuGeneral.Text = SetLinking("General.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CURRENCY'"
            If dv.Count > 0 Then
                mnuCurrency.Text = SetLinking("Currency.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'APPROVAL'"
            If dv.Count > 0 Then
                mnuDimensi.Text = SetLinking("User Account.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER KATALOG'"
            If dv.Count > 0 Then
                mnuKatalog.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PRICE KATALOG'"
            If dv.Count > 0 Then
                mstPrice.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPLOAD INIT STOCK'"
            If dv.Count > 0 Then
                UploadInit.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'BOTTOM PRICE'"
            If dv.Count > 0 Then
                mnuBottomPrice.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'BOTTOM PRICE'"
            If dv.Count > 0 Then
                PriceBottom.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER USER ID'"
            If dv.Count > 0 Then
                mnuMstUserID.Text = SetLinking("Small Business Resources.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROMO'"
            If dv.Count > 0 Then
                genbarcmat.Text = SetLinking("Promo.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER GROUP'"
            If dv.Count > 0 Then
                CustGroup.Text = SetLinking("Promo.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROGRAM POINT CUSTOMER'"
            If dv.Count > 0 Then
                MnuProgPoint.Text = SetLinking("Promo.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER APPROVAL'"
            If dv.Count > 0 Then
                MnuCustApp.Text = SetLinking("Promo.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPDATE PIC ITEM'"
            If dv.Count > 0 Then
                mnuItem.Text = SetLinking("UserRole.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER CABANG'"
            If dv.Count > 0 Then
                mnuCustomer.Text = SetLinking("Customer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SUPPLIER'"
            If dv.Count > 0 Then
                mnuSupplier.Text = SetLinking("Supplier.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'OPEN CABANG PDO'"
            If dv.Count > 0 Then
                MnuOpenPO.Text = SetLinking("purchaseInvoice.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPDATE TANGGAL PROMO PI'"
            If dv.Count > 0 Then
                mnuUpdatePI.Text = SetLinking("purchaseInvoice.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROMO SUPPLIER'"
            If dv.Count > 0 Then
                MnuProgSupp.Text = SetLinking("Promo.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'EKSPEDISI'"
            If dv.Count > 0 Then
                mnuRoute.Text = SetLinking("WIPRes.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER CUSTOMER PUSAT'"
            If dv.Count > 0 Then
                mnuBOM.Text = SetLinking("Customer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SUPPLIER ADMIN'"
            If dv.Count > 0 Then
                mnuPrintBarcode.Text = SetLinking("Supplier.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SETTING COA PERSEDIAAN'"
            If dv.Count > 0 Then
                MstSCOA.Text = SetLinking("Region.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER EXPEDISI'"
            If dv.Count > 0 Then
                MstExpedisi.Text = SetLinking("Region.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            'rate pajak

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'BARANG + DIMENSI'"
            If dv.Count > 0 Then
                mstDimensi.Text = SetLinking("currency_money.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'TARGET ITEM'"
            If dv.Count > 0 Then
                mstTargetItem.Text = SetLinking("currency_money.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'UPLOAD ADJUSMENT 	
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPLOAD ADJUSMENT'"
            If dv.Count > 0 Then
                mnuUploadAdj.Text = SetLinking("InitMat.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER PRICE CABANG'"
            If dv.Count > 0 Then
                MnuPriceCabang.Text = SetLinking("InitMat.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER JABATAN'"
            If dv.Count > 0 Then
                MnuJabatan.Text = SetLinking("InitMat.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER CABANG'"
            If dv.Count > 0 Then
                MnuMstCabang.Text = SetLinking("Region.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'STATUS KATALOG'"
            If dv.Count > 0 Then
                MnuStsKatalog.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'ADJUSMENT HPP KATALOG'"
            If dv.Count > 0 Then
                MnuAdjHpp.Text = SetLinking("Material.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'rate standart
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'RATE STANDARD'"
            If dv.Count > 0 Then
                RateStandard.Text = SetLinking("currency_money.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'Ekpedisi Branch
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'HARGA EXPEDISI'"
            If dv.Count > 0 Then
                expedisiBranch.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER GUDANG'"
            If dv.Count > 0 Then
                MnuGudang.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'INITIAL STOCK'"
            If dv.Count > 0 Then
                mnuInitStockAwal.Text = SetLinking("InitMat.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'SO Close
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SO CANCELED'"
            If dv.Count > 0 Then
                mnuSOCanceled.Text = SetLinking("CancelSO.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SURAT JALAN'"
            If dv.Count > 0 Then
                mnuSuratJalan.Text = SetLinking("SJDOC.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CLOSING STOCK'"
            '"
            If dv.Count > 0 Then
                mnuClosingStock.Text = SetLinking("BOM.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SERVICE FINAL'"
            If dv.Count > 0 Then
                mnuFinal.Text = SetLinking("BOM.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK HISTORY PENERIMAAN'"
            If dv.Count > 0 Then
                MnuCekTTS.Text = SetLinking("BOM.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TC SUPPLIER INTERNAL'"
            If dv.Count > 0 Then
                MnuSuppInt.Text = SetLinking("SPK.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            ' Set TabPanel Visibility & Reset it for next use
            TabContainer1.Tabs(0).Enabled = bVisible : bVisible = False
            ' ==========
            ' SERVICE
            ' ==========

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PENERIMAAN SERVICE'"
            If dv.Count > 0 Then
                mnpPR.Text = SetLinking("Purchase.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'INVOICE SERVICE'"
            If dv.Count > 0 Then
                mnuPReturn.Text = SetLinking("InOutMat.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SERVICE FINAL INTERNAL'"
            If dv.Count > 0 Then
                MnuFinInt.Text = SetLinking("ItemRecv.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN INVOICE SERVICE'"
            If dv.Count > 0 Then
                mnuSOStatus2.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'SERVICE SUPPLIER INTERNAL STATUS'"
            If dv.Count > 0 Then
                MnuSuppIntRpt.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SERVICE FINAL INTERNAL'"
            If dv.Count > 0 Then
                RptSFI.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN CUSTOMER'"
            If dv.Count > 0 Then
                mnurptcust.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PENERIMAAN BARANG'"
            If dv.Count > 0 Then
                mnuPOStatus.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TW SERVICE CUSTOMER'"
            If dv.Count > 0 Then
                mnuTrfService.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TC SERVICE CUSTOMER'"
            If dv.Count > 0 Then
                TcConfirmInt.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SERVICE SUPPLIER'"
            If dv.Count > 0 Then
                MnuSvcSupplier.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'KEMBALI SUPPLIER'"
            If dv.Count > 0 Then
                MnuKembaliSupp.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STATUS SERVICE SUPPLIER'"
            If dv.Count > 0 Then
                MnuSvcSupp.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STATUS TW SERVICE CUSTOMER'"
            If dv.Count > 0 Then
                mnuRptTwInt.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SERVICE FINAL'"
            If dv.Count > 0 Then
                mnustatusdeliv.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            ' Set TabPanel Visibility & Reset it for next use
            TabContainer1.Tabs(1).Enabled = bVisible : bVisible = False
            ' ===========
            ' MARKETING
            ' ===========
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SURAT JALAN'"
            If dv.Count > 0 Then
                mnuSuratJalan.Text = SetLinking("SJDOC.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES INVOICE'"
            If dv.Count > 0 Then
                mnuSI.Text = SetLinking("sales_invoice.jpeg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SI RECORD'"
            If dv.Count > 0 Then
                mnuSIRecord.Text = SetLinking("sales_invoice.jpeg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SO CLOSE'"
            If dv.Count > 0 Then
                mnuSOclos.Text = SetLinking("so_close.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES ORDER'"
            If dv.Count > 0 Then
                mnuSO.Text = SetLinking("Sales.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES RETURN'"
            If dv.Count > 0 Then
                mnuSReturn.Text = SetLinking("Sales_Return.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES INVOICE'"
            If dv.Count > 0 Then
                mnuOutsSO.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TANDA TERIMA SEMENTARA'"
            If dv.Count > 0 Then
                mnuWIPRes.Text = SetLinking("TransferItem.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'NOTA PENJUALAN ROMBENG'"
            If dv.Count > 0 Then
                MnuRags.Text = SetLinking("TransferItem.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES ORDER'"
            If dv.Count > 0 Then
                mnuSOStatus.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PENJUALAN BERSIH'"
            If dv.Count > 0 Then
                mnuJualBersih.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SURAT JALAN'"
            If dv.Count > 0 Then
                RptSuratJalan.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN RETUR PENJUALAN'"
            If dv.Count > 0 Then
                mnuRptSalesRetur.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN NOTA EXPEDISI'"
            If dv.Count > 0 Then
                RptNotaExp.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PROGRAM POINT CUSTOMER'"
            If dv.Count > 0 Then
                MnuLapProgPoint.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN HISTORY PRICE JUAL'"
            If dv.Count > 0 Then
                MnuHistPriceSO.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN PROMO CUSTOMER'"
            If dv.Count > 0 Then
                MnuPromCust.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN CUSTOMER APPROVAL'"
            If dv.Count > 0 Then
                MnuCustApproval.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN INFO CETAK SI'"
            If dv.Count > 0 Then
                MnuInfoPrintOutsSI.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN PENJUALAN ROMBENG'"
            If dv.Count > 0 Then
                MnuNotaRags.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN STATUS SI KONSINYASI'"
            If dv.Count > 0 Then
                MnuStatusSIKonsi.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME ='LAPORAN PENJUALAN TRI WULAN'"
            If dv.Count > 0 Then
                RptJualTriWulan.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SO CLOSE'"
            If dv.Count > 0 Then
                rptSOClose.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'MANIFEST GIRO OUT STATUS'"
            If dv.Count > 0 Then
                mnuManGiroOutStatus.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'MANIFEST GIRO IN REPORT'"
            If dv.Count > 0 Then
                mnuManGiroInStatus.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'INCOMING GIRO REALIZATION'"
            If dv.Count > 0 Then
                mnugiroin.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'OUTGOING GIRO REALIZATION'"
            If dv.Count > 0 Then
                mnugiroout.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'DELIVERY ORDER'"
            If dv.Count > 0 Then
                mnuSDO.Text = SetLinking("bpbr.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            '' Set TabPanel Visibility & Reset it for next use
            TabContainer1.Tabs(2).Enabled = bVisible : bVisible = False

            ' ======
            ' PURCHASING
            ' ======
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE ORDER'"
            If dv.Count > 0 Then
                mnuBPM.Text = SetLinking("Purchase.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE INVOICE'"
            If dv.Count > 0 Then
                mnuPI.Text = SetLinking("Invoice.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE RETURN'"
            If dv.Count > 0 Then
                mnuBPBJ.Text = SetLinking("poREV_PCH.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME LIKE 'PO CLOSE%'"
            If dv.Count > 0 Then
                mnuSPKClose.Text = SetLinking("SPK.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT BANK'"
            If dv.Count > 0 Then
                mnuBankRpt.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT CASH'"
            If dv.Count > 0 Then
                mnuCashRpt.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'GENERAL JOURNAL REPORT'"
            If dv.Count > 0 Then
                mnuGenJrnl.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT GENERAL LEDGER'"
            If dv.Count > 0 Then
                mnuGenLed.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES DELIVERY ORDER'"
            If dv.Count > 0 Then
                mnuRPTDO.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TW STATUS'"
            If dv.Count > 0 Then
                mnutwsts.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'report piutang 19 desember 14.45
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PIUTANG'"
            If dv.Count > 0 Then
                rptPiutang.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PIUTANG GANTUNG'"
            If dv.Count > 0 Then
                mnuPiutangGantung.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN MATERIAL USAGE ON PRICE'"
            If dv.Count > 0 Then
                MnuMatUsagePrice.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN DEBIT & CREDIT NOTE'"
            If dv.Count > 0 Then
                mnuDNCN.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN ANALISA BIAYA'"
            If dv.Count > 0 Then
                mnuBiaya.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN NOTA PIUTANG GANTUNG'"
            If dv.Count > 0 Then
                mnuNotaPiutangGantung.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PINJAM BARANG'"
            If dv.Count > 0 Then
                MnuPinjam.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN HUTANG'"
            If dv.Count > 0 Then
                mnukartuhutang.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN REALISASI VOUCHER'"
            If dv.Count > 0 Then
                mnuRealisasiVoucher.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN HISTORY HPP ITEM'"
            If dv.Count > 0 Then
                rptIOWH.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REALISASI PEMBELIAN'"
            If dv.Count > 0 Then
                MnuBeliBersih.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME LIKE 'REPORT NERACA'"
            'If dv.Count > 0 Then
            '    mnuNrc.Text = SetLinking("Chart.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
            '        dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            'End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE ORDER'"
            If dv.Count > 0 Then
                rptPO.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PO STATUS'"
            If dv.Count > 0 Then
                rptPOStatus.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE DELIVERY ORDER'"
            If dv.Count > 0 Then
                rptPDO.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE RETURN'"
            If dv.Count > 0 Then
                mnuRptPRetur.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE INVOICE'"
            If dv.Count > 0 Then
                mnuRptPI.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PO CLOSE'"
            If dv.Count > 0 Then
                rptPOClose.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN VOUCHER'"
            If dv.Count > 0 Then
                rptVoucher.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REKAP PROMO SUPPLIER'"
            If dv.Count > 0 Then
                MnuLapProgSupp.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAP. PEMBELIAN AMP'"
            If dv.Count > 0 Then
                MnuPOAMP.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            ' Set TabPanel Visibility & Reset it for next use
            TabContainer1.Tabs(3).Enabled = bVisible : bVisible = True

            ' ===========
            ' ACCTG
            ' ===========
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'EXPENSE ACCOUNTING'"
            If dv.Count > 0 Then
                mnuCBExpense.Text = SetLinking("Expense.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    "CASH/BANK " & dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS PURCHASE'"
            If dv.Count > 0 Then
                PurchaseFA.Text = SetLinking("FixedAssets.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PAYMENT AR SERVIS'"
            If dv.Count > 0 Then
                PayInvServis.Text = SetLinking("Payment.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'REALISASI VOUCHER'"
            If dv.Count > 0 Then
                RealVoucher.Text = SetLinking("CashBank.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS TRANSACTION'"
            If dv.Count > 0 Then
                TrnFA.Text = SetLinking("FixedAssets.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT DP AR'"
            If dv.Count > 0 Then
                rptDPAR.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT DP AP'"
            If dv.Count > 0 Then
                rptDPAP.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT TRIAL BALANCE'"
            If dv.Count > 0 Then
                rptTB.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL PIUTANG'"
            If dv.Count > 0 Then
                mnuInitAR.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AR BALANCE'"
            If dv.Count > 0 Then
                InitDPAr.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AP BALANCE'"
            If dv.Count > 0 Then
                InitDPAp.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME ='DOWN PAYMENT A/R'"
            If dv.Count > 0 Then
                mnu_DPAR.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString & "?awal=true", _
                dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DOWN PAYMENT A/P'"
            If dv.Count > 0 Then
                mnu_DPAP.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString & "?awal=true", _
                dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            ' Closing
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MONTHLY CLOSING'"
            If dv.Count > 0 Then
                MNUCLOSINGACCTG.Text = SetLinking("closingMonthly.png", dv.Item(0).Item("FORMADDRESS").ToString & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            ''ar payment
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/R PAYMENT'"
            If dv.Count > 0 Then
                mnu_AR_Payment.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PELUNASAN PIUTANG GANTUNG'"
            If dv.Count > 0 Then
                mnuPaymentHR.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'GIRO FLAG STATUS'"
            If dv.Count > 0 Then
                mnuGiroFlag.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME LIKE 'RETURN NOTA PIUTANG GANTUNG%'"
            If dv.Count > 0 Then
                mnuHRReturn.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            ''ar payment
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/R KOREKSI'"
            If dv.Count > 0 Then
                mnu_AR_Koreksi.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AP RETUR'"
            If dv.Count > 0 Then
                mnuDPAPRetur.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AR RETUR'"
            If dv.Count > 0 Then
                mnuDPARRetur.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PREPAID TRANSACTION'"
            If dv.Count > 0 Then
                MnuPrepaid.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PREPAID DISPOSE'"
            If dv.Count > 0 Then
                MnuPrepaidDispose.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            ''ar payment
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/P KOREKSI'"
            If dv.Count > 0 Then
                mnu_AP_Koreksi.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/P PAYMENT'"
            If dv.Count > 0 Then
                mnuAPPymnt.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CREDIT NOTE'"
            If dv.Count > 0 Then
                MNUCREDITNOTE.Text = SetLinking("Payment.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CASH/BANK RECEIPT'"
            If dv.Count > 0 Then
                mnureceipt.Text = SetLinking("CashBank.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MUTATION ACCOUNTING'"
            If dv.Count > 0 Then
                mnuCBMut.Text = SetLinking("Mutation.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'GIRO'"
            If dv.Count > 0 Then
                MnuGIRO.Text = SetLinking("giro.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MANIFESTED GIRO OUT (A/P)'"
            If dv.Count > 0 Then
                MnuGIRO_OUT.Text = SetLinking("giro.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MANIFESTED GIRO IN (A/R)'"
            If dv.Count > 0 Then
                MnuGIRO_IN.Text = SetLinking("giro.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            TabContainer1.Tabs(4).Enabled = bVisible : bVisible = False

            ' ===========
            ' INVENTORY
            ' ===========
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK HUTANG PER NOTA'"
            If dv.Count > 0 Then
                MnuCekHutang.Text = SetLinking("CashBank.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE DO'"
            If dv.Count > 0 Then
                mnuPDO.Text = SetLinking("supplier_exp.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'FORECAST PO'"
            If dv.Count > 0 Then
                MnuForeCastPO.Text = SetLinking("Purchase.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'NOTA EKSPEDISI'"
            If dv.Count > 0 Then
                NotaExpMnu.Text = SetLinking("CashBank.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK PIUTANG PER NOTA'"
            If dv.Count > 0 Then
                PerNotaMnu.Text = SetLinking("CashBank.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                 dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PINJAM BARANG'"
            If dv.Count > 0 Then
                mnuPinjamBarang.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PENGEMBALIAN BARANG'"
            If dv.Count > 0 Then
                MnuKembalibarang.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PERMINTAAN BARANG'"
            If dv.Count > 0 Then
                MnuPb.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TW E-COMMERCE'"
            If dv.Count > 0 Then
                MnuTWecommerce.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'STOCK ADJUSTMENT'"
            If dv.Count > 0 Then
                mnuSDOTRD.Text = SetLinking("InitStock.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFER WAREHOUSE'"
            If dv.Count > 0 Then
                mnuTrfWh.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFER CONFIRM'"
            If dv.Count > 0 Then
                mnuTrfConf.Text = SetLinking("TWin.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFORM ITEM'"
            If dv.Count > 0 Then
                mnuTransform.Text = SetLinking("icon-transfer.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            'FORECAST PO

            '========Material Usage============
            '----------------------------------
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'MATERIAL USAGE'"
            If dv.Count > 0 Then
                mnuMatusageUmum.Text = SetLinking("mutation.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If
            '==================================

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK ADMIN'"
            If dv.Count > 0 Then
                mnuPackList.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK'"
            If dv.Count > 0 Then
                mnuStockReport.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN DIMENSI ITEM'"
            If dv.Count > 0 Then
                mnuDimensiItem.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK ADJUSTMENT'"
            If dv.Count > 0 Then
                mnuRptAS.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If 

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN DEADSTOCK'"
            If dv.Count > 0 Then
                mnuRptDeadStock.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PERMINTAAN BARANG'"
            If dv.Count > 0 Then
                MnuRptPB.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TW E-COMMERCE'"
            If dv.Count > 0 Then
                MnuTWEcom.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN FAST MOVING'"
            If dv.Count > 0 Then
                mnuMatStock.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN LABA RUGI PER ITEM'"
            If dv.Count > 0 Then
                mnuWIPStock.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TRANSFER WAREHOUSE'"
            If dv.Count > 0 Then
                mnuRptTW.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TRANSFORMASI ITEM'"
            If dv.Count > 0 Then
                mnuRptTransform.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN MATERIAL USAGE'"
            If dv.Count > 0 Then
                mnuMatUsage.Text = SetLinking("Report.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If


            'Report DPAR

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS BALANCE'"
            If dv.Count > 0 Then
                InitFA.Text = SetLinking("FixedAssets.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CHART OF ACCOUNTS'"
            If dv.Count > 0 Then
                mnuCOA.Text = SetLinking("COA.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'BUKA POSTING AR PAYMENT'"
            If dv.Count > 0 Then
                Label1.Text = SetLinking("giro.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL VOUCHER'"
            If dv.Count > 0 Then
                InitVoucher.Text = SetLinking("COA.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'COA BALANCE'"
            If dv.Count > 0 Then
                mnuInitCOA.Text = SetLinking("SPK.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'INTERFACE'"
            If dv.Count > 0 Then
                mnuInterface.Text = SetLinking("Interface.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MEMORIAL JURNAL'"
            If dv.Count > 0 Then
                MnuFreeJurnal.Text = SetLinking("app_48.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'NOTA PIUTANG GANTUNG'"
            If dv.Count > 0 Then
                mnuNotaHR.Text = SetLinking("app_48.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CASH BON'"
            If dv.Count > 0 Then
                mnuCashBon.Text = SetLinking("app_48.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL HUTANG'"
            If dv.Count > 0 Then
                mnuInitAP.Text = SetLinking("icon-owner-60.gif", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DEBIT NOTE'"
            If dv.Count > 0 Then
                MNUDEBITNOTE.Text = SetLinking("Payment.png", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'NO FAKTUR PAJAK'"
            If dv.Count > 0 Then
                mnuMstFaktur.Text = SetLinking("app_48.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'ASSIGN NO FAKTUR'"
            If dv.Count > 0 Then
                mnuAssignFaktur.Text = SetLinking("app_48.jpg", dv.Item(0).Item("FORMADDRESS").ToString.TrimEnd & "?awal=true", _
                    dv.Item(0).Item("FORMNAME").ToString) : bVisible = True
            End If

            TabContainer1.Tabs(5).Enabled = bVisible : bVisible = False
            ' ========
            dv.RowFilter = ""

            For C1 As Integer = 0 To 5

                If TabContainer1.Tabs(C1).Enabled = True Then
                    TabContainer1.ActiveTabIndex = C1 : Exit For
                End If
            Next
            'BindData(CompnyCode)
            If Session("branch_id") <> "10" Then
                imbPrintPLExcel.Visible = False
            End If

        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Function SetLinking(ByVal sImgPath As String, ByVal sMenuPath As String, ByVal sMenuText As String) As String
        Dim sTag As String = ""
        sTag = "<a href='../" & sMenuPath & "'><IMG Height='48px' Width='48' " & _
            "src='../Images/MENU/" & sImgPath & "' Border='0'/><br />" & sMenuText & "</a>"
        Return sTag
    End Function

    Private Function checkRegex(ByVal input As String, ByVal pattern As String) As Boolean
        oRegex = New Regex(pattern, RegexOptions.IgnoreCase)
        omatches = oRegex.Matches(input)
        If Not (oMatches.Count > 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub BindData(ByVal sCompCode As String)
        'sSql = "select mst.orderoid,mst.orderno,mst.ioref,isnull((select top 1 orderno from ql_trnordermst where ioref=mst.ioref order by orderoid desc),0) as iorev  from ql_trnordermst mst where mst.ordertype='so' and cmpcode='" & sCompCode & "' and mst.ioref=(select orderoid from ql_trnordermst where orderoid=mst.ioref and orderstatus='revisi' and ordertype='io') order by mst.orderoid desc"
        'FillGV(tbldata, sSql, "QL_pomst")
    End Sub

    Private Sub InitNotifySOInproc()
        Dim sqlPlus As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sqlPlus &= " AND som.createuser='" & Session("UserID") & "'"
            End If
        End If

        sSql = "SELECT som.cmpcode, Count(ordermstoid) jmlapp  FROM QL_trnordermst som  WHERE trnorderstatus='In Process' " & sqlPlus & " GROUP BY cmpcode "
        Dim dtApp As DataTable = cKon.ambiltabel(sSql, "QL_so_inproc")
        Dim dvApp As DataView = dtApp.DefaultView
        If dvApp.Count > 0 Then
            Dim arColNameList() As String = {"apptype", "appsept", "appcount", "appformvalue"}
            Dim arColTypeList() As String = {"System.String", "System.String", "System.Int32", "System.String"}
            Dim dtList As DataTable = SetTableDetail("tblAppList", arColNameList, arColTypeList)
            Dim drList As DataRow

            drList = dtList.NewRow : drList("apptype") = "- Sales Order" : drList("appsept") = ":" : drList("appcount") = CInt(dvApp(0)("jmlapp").ToString) : drList("appformvalue") = "Transaction\ft_trnSlsIntOrder" : dtList.Rows.Add(drList)

            dvApp.RowFilter = ""

            GVListPost.DataSource = dtList
            GVListPost.DataBind()
        End If
    End Sub

    Protected Sub GVListPost_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListPost.SelectedIndexChanged
        Response.Redirect("~\" & GVListPost.SelectedDataKey.Item("appformvalue").ToString & ".aspx?filstat=true")
    End Sub

    Private Sub BindPricelist()
        sSql = "SELECT itemcode, itemdesc, Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End AS stockflag, Merk from ql_mstitem where itemflag = 'Aktif' and pricelist = 0"
        'If Session("UserID").ToString.ToUpper <> "ADMIN" Then
        '    sSql &= " AND mr.deptoid in (select deptoid from ql_mstperson where personoid in (select personoid from ql_mstprof where profoid = '" & Session("UserID") & "'))"
        'End If
        sSql &= " ORDER BY itemdesc"
        FillGV(gvListPricelist, sSql, "QL_mstitem")
    End Sub

    Private Sub InitNotifyPricelist()
        sSql = "SELECT COUNT(itemoid) as itemoid FROM ql_mstitem where itemflag = 'Aktif' and pricelist = 0"
        Dim dtNote As DataTable = cKon.ambiltabel(sSql, "QL_mstitemcount")
        For c1 As Integer = 0 To dtNote.Rows.Count - 1
            totalBarang.Text += dtNote.Rows(c1)("itemoid").ToString
        Next
        BindPricelist()
    End Sub

    Protected Sub gvListPricelist_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPricelist.PageIndexChanging
        gvListPricelist.PageIndex = e.NewPageIndex
        BindPricelist()
    End Sub

    Private Sub ShowPrintPL(ByVal sType As String)
        Dim sWhere As String = "" : Dim periode As String = ""
        Try
            If sType = "EXCEL" Then
                rptReport.Load(Server.MapPath(folderReport & "rptBarangPL.rpt"))
            End If
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, rptReport)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                rptReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Master_Barang_PL_NOL" & Format(GetServerTime(), "dd_MM_yy"))
                rptReport.Close() : rptReport.Dispose()
            End If
            rptReport.Close() : rptReport.Dispose()
        Catch ex As Exception
            rptReport.Close()
            rptReport.Dispose()
            'showMessage(ex.Message, 1, "")
        End Try
    End Sub

    Protected Sub imbPrintPLExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowPrintPL("EXCEL")
    End Sub

    Private Sub BindPromo()
        sSql = "select promooid,promocode, promoname, typenya AS type, crtuser from QL_mstpromosupp where promodate2 < CURRENT_TIMESTAMP and statuspromo IN ('Approved') and promores1 = 0"
        sSql &= " ORDER BY promocode"
        FillGV(gvListPromo, sSql, "QL_mstpromosupp")
    End Sub

    Private Sub InitNotifyPromo()
        Dim sqlPlus As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sqlPlus &= " AND crtuser='" & Session("UserID") & "'"
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sqlPlus &= " AND crtuser='" & Session("UserID") & "'"
            End If
        End If
        BindPromo()
    End Sub

    Protected Sub gvListPromo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPromo.PageIndexChanging
        gvListPromo.PageIndex = e.NewPageIndex
        BindPromo()
    End Sub

    Protected Sub gvListPromo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPromo.SelectedIndexChanged
        Response.Redirect("~\ReportForm\rptPromoSupp.aspx?promooid=" & gvListPromo.SelectedDataKey.Item("promooid").ToString & "&type=" & gvListPromo.SelectedDataKey.Item("type").ToString & "")
    End Sub

    Private Sub BinPiutangDueDate(ByVal sqlPlus As String)
        sSql = "SELECT trnjualno, CONVERT(varchar,trnjualdate,103) AS trnjualdate, CONVERT(varchar,payduedate,103) AS payduedate,ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) trnamtjualnetto FROM ( select j.trnjualmstoid, j.trnjualno,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trnjualstatus='POST' " & sqlPlus & " ) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr) group by trnjualno,trnjualdate, payduedate"
        FillGV(gvListPiutangDueDate, sSql, "QL_conar")
    End Sub

    Private Sub InitNotifyPiutangDueDate()
        Dim sqlPlus As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sqlPlus &= " AND j.branch_code='" & Session("branch_id") & "'"
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sqlPlus &= " AND j.branch_code='" & Session("branch_id") & "'"
            End If
        End If
        BinPiutangDueDate(sqlPlus)
    End Sub

    Protected Sub gvListPiutangDueDate_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPiutangDueDate.PageIndexChanging
        gvListPiutangDueDate.PageIndex = e.NewPageIndex
        BinPiutangDueDate("")
    End Sub

    Protected Sub gvListPiutangDueDate_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListPiutangDueDate.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Private Sub BinPOoutstanding(ByVal sqlPlus As String)
        sSql = "select trnbelimstoid, trnbelipono, CONVERT(varchar(20),trnbelipodate,103) AS trnbelipodate, CONVERT(varchar(20),TRNBELIdeliverydate,103) AS etd, (select suppname from QL_mstsupp where suppoid = po.trnsuppoid) AS suppname, amtbelinettoidr from QL_pomst po where trnbelistatus = 'Approved' and trnbelipono NOT IN (select trnbelipono from ql_trnsjbelimst where trnsjbelistatus = 'Approved') AND DATEDIFF(D,DATEADD(D,-3,TRNBELIdeliverydate),CURRENT_TIMESTAMP) >= -3 order by TRNBELIdeliverydate DESC"
        FillGV(gvListPOoutstanding, sSql, "QL_pomst")
    End Sub

    Private Sub InitNotifyPOoutstanding()
        Dim sqlPlus As String = ""
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sqlPlus &= " AND po.upduser='" & Session("UserID") & "'"
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sqlPlus &= " AND po.upduser='" & Session("UserID") & "'"
            End If
        End If
        BinPOoutstanding(sqlPlus)
    End Sub

    Protected Sub gvListPOoutstanding_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPOoutstanding.PageIndexChanging
        gvListPOoutstanding.PageIndex = e.NewPageIndex
        BinPOoutstanding("")
    End Sub

    Protected Sub gvListPOoutstanding_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListPOoutstanding.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(1).Text = Format(e.Row.Cells(1).Text, "dd/MM/yyyy")
            'e.Row.Cells(2).Text = Format((e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Private Sub BindDataList()
        Dim QlCA As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcustapproval' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeCA As String = ""
        Dim CACode() As String = QlCA.Split(",")
        For C1 As Integer = 0 To CACode.Length - 1
            If CACode(C1) <> "" Then
                sCodeCA &= "'" & CACode(C1).Trim & "',"
            End If
        Next

        sSql = "SELECT COUNT(distinct oid) jml,21 seq, 'CUSTOMER APPROVAL' AS type, '' AS address From QL_approval a inner join QL_mstcustapproval b on a.cmpcode = b.cmpcode and a.oid = b.custapprovaloid WHERE a.branch_code IN (" & Left(sCodeCA, sCodeCA.Length - 1) & ") And a.approvaluser='" & Session("userid") & "' AND a.cmpcode = '" & CompnyCode & "' And a.tablename = 'QL_mstcustapproval' AND statusrequest = 'New' AND event = 'In Approval' AND b.custapprovalstatus='In Approval'"
        DtTotal.Text = GetScalar(sSql)

        sSql = "SELECT cp.custapprovaloid, cb.gendesc Cabang, cu.custcode, cu.custname, cp.custapprovalno, Convert(char(20),cp.custapprovaldate,103) custapprovaldate, cp.custapprovaltype, tr.genoid TerMinOid, tr.gendesc TerminNew, cp.addrnew +','+cp.telprmhnew+','+cp.telpktrnew+','+cp.telpwanew RevData, CAST(cp.clawalnew AS VARCHAR(20)) clawalnew, cp.cltempnew, cp.custapprovalnote, cp.custapprovalstatus, Case cp.custapprovaltype When 'CL AWAL' then CAST(cp.clawalnew AS VARCHAR(20)) When 'CL TEMPORARY' then CAST(cp.cltempnew AS VARCHAR(20)) When 'OVERDUE' then CAST(cp.cloverdue AS VARCHAR(20)) When 'TERMIN' then tr.gendesc+' '+ 'Hari' else '-' End NewData FROM QL_mstcustapproval cp INNER JOIN QL_mstgen cb On cb.gencode=cp.branch_code AND cb.gengroup='CABANG' Left JOIN QL_mstgen tr ON tr.genoid=cp.terminnew AND tr.gengroup='PAYTYPE' Left Join QL_mstgen ct ON ct.genoid=cp.citynew AND tr.gengroup='CITY' Left Join QL_mstgen pro ON pro.genoid=cp.provnew AND tr.gengroup='CITY' Inner Join QL_mstcust cu ON cu.custoid=cp.custoid AND cp.branch_code=cu.branch_code Inner Join QL_APPROVAL ap ON ap.OID=cp.custapprovaloid AND ap.TABLENAME='QL_mstcustapproval' AND ap.event='In Approval' AND ap.APPROVALUSER='" & Session("userid") & "' Where cp.custapprovalstatus='IN APPROVAL' AND ap.statusrequest = 'New' AND ap.branch_code IN (" & Left(sCodeCA, sCodeCA.Length - 1) & ") Order By custapprovaldate"
        FillGV(gvMst, sSql, "QL_mstCustApp")
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindDataList()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(5).Text <> "TERMIN" Then
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 0)
            End If
        End If
    End Sub

    Protected Sub aLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles aLinkButton.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "update QL_mstoid set lastoid=(select MAX(conmtroid)+1 from QL_conmtr) Where tablename='QL_conmtr'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            Exit Sub
        End Try
    End Sub
End Class
