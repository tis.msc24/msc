<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ft_trnSlsIntOrder.aspx.vb" Inherits="trnSlsIntOrder" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%-- Add content controls here --%>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0" width="100%" class="tabelhias">
        <tr>
            <th class="header" colspan="3" valign="middle">
               <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Sales Order"></asp:Label>
            </th>
        </tr>
        <tr>
            <th align="left" colspan="3" style="background-color: transparent">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                  <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1" AccessKey="" BorderColor="">
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" /><strong><span style="font-size: 9pt"> List of Sales Order</span></strong>&nbsp;<strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel6" runat="server" DefaultButton="btnSearch" __designer:wfdid="w23"><TABLE width="100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFromcabang" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w24"></asp:DropDownList></TD></TR><TR><TD align=left>Filter</TD><TD align=left colSpan=4><asp:DropDownList id="DDLSearch" runat="server" Width="104px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w25"><asp:ListItem Value="OrderNo">Order No</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="trnordernote ">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w26"></asp:TextBox>&nbsp;&nbsp; </TD></TR><TR><TD align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w27" Checked="True" AutoPostBack="False"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w28" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29">
                                                    </asp:ImageButton> <asp:Label id="Label16" runat="server" Text="to" __designer:wfdid="w30"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox> <asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w32">
                                                    </asp:ImageButton> <asp:Label id="Label17" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w33"></asp:Label>&nbsp; </TD></TR><TR><TD align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w34">
                                                    </asp:CheckBox> </TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w35" AutoPostBack="True"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
<asp:ListItem>Reviced</asp:ListItem>
<asp:ListItem>Canceled</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Closed</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w36">
                                                    </asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w37">
                                                    </asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=5><asp:GridView id="GVmstOrder" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w38" GridLines="None" DataKeyNames="branch_code,ordermstoid,orderno,trntaxpct" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No data in database." PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="ordermstoid" DataNavigateUrlFormatString="ft_trnSlsIntOrder.aspx?oid={0}" DataTextField="ordermstoid" HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="No. SO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="375px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="375px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" DataFormatString="{0:d}" HeaderText="Tgl SO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JamBuat" HeaderText="Jam Buat">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnordernote" HeaderText="Note" HtmlEncode="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Cabang" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" CssClass="Important" Text="No Data Found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w39" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy">
                                                    </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w40" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy">
                                                    </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w41" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date">
</ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w42" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date">
                                                    </ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVmstOrder"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2" AccessKey=""
                        BorderColor="">
                        <HeaderTemplate>
                            <img id="Img1" align="absMiddle" alt="" onclick="return IMG1_onclick()" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt"> Form Sales Order&nbsp;</span></strong><strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR id="trcopy" runat="server"><TD id="TD11" align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Width="122px" Font-Size="12pt" Font-Bold="True" ForeColor="Black" Text="Information :" __designer:wfdid="w156" Font-Underline="True"></asp:Label></TD><TD id="TD10" align=left runat="server" Visible="true"><asp:Label id="createtime" runat="server" Font-Bold="True" ForeColor="#585858" Visible="False" __designer:wfdid="w157"></asp:Label> <asp:Label id="I_U" runat="server" Font-Size="8pt" ForeColor="Red" Visible="False" __designer:wfdid="w158"></asp:Label></TD><TD id="TD25" vAlign=middle align=left runat="server" Visible="true"><asp:Label id="refoid" runat="server" Visible="False" __designer:wfdid="w159"></asp:Label></TD><TD id="TD20" align=left runat="server" Visible="true"><asp:Label id="rateidrvalue" runat="server" Visible="False" __designer:wfdid="w160"></asp:Label><asp:Label id="oid" runat="server" Visible="False" __designer:wfdid="w161"></asp:Label><asp:Label id="rate2oid" runat="server" Visible="False" __designer:wfdid="w162"></asp:Label></TD><TD id="TD19" align=left runat="server" Visible="true"><asp:Label id="rateoid" runat="server" Visible="False" __designer:wfdid="w163">0</asp:Label> <asp:Label id="CurrOid" runat="server" Visible="False" __designer:wfdid="w164"></asp:Label></TD><TD id="TD18" vAlign=middle align=left runat="server" Visible="true"><asp:DropDownList id="ordergetsourceref" runat="server" Width="134px" CssClass="inpText" Font-Size="8pt" Visible="False" __designer:wfdid="w165" AutoPostBack="True"><asp:ListItem>None</asp:ListItem>
<asp:ListItem>Quotation</asp:ListItem>
</asp:DropDownList> </TD></TR><TR id="Tr2" runat="server"><TD align=left>Cabang</TD><TD align=left><asp:DropDownList id="CabangNya" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w166" AutoPostBack="True"></asp:DropDownList></TD><TD align=left>No. SO&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w167"></asp:Label></TD><TD align=left><asp:TextBox id="orderno" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w168" MaxLength="20" ReadOnly="True"></asp:TextBox></TD><TD align=left><asp:CheckBox id="cbCash" runat="server" Text="Cash" __designer:wfdid="w169" AutoPostBack="True"></asp:CheckBox></TD><TD align=left><asp:CheckBox id="cbSR" runat="server" Width="133px" Text="Special Request" __designer:wfdid="w170"></asp:CheckBox></TD></TR><TR id="Tr3" runat="server"><TD vAlign=middle align=left>Cust. Group&nbsp;<asp:Label id="Label10" runat="server" CssClass="Important" Text="*" __designer:wfdid="w171"></asp:Label> </TD><TD align=left><asp:TextBox id="CustNameGroup" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w172" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtnFindCustGroup" onclick="ibtnFindCustGroup_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w173" Enabled="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCustGroup" onclick="btnClearCustGroup_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w174" Enabled="False" EnableTheming="True"></asp:ImageButton> </TD><TD vAlign=middle align=left>Periode</TD><TD vAlign=middle align=left><asp:TextBox id="identifierno" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w175" ReadOnly="True"></asp:TextBox></TD><TD vAlign=middle align=left><asp:Label id="jenisSO" runat="server" Text="Jenis SO" __designer:wfdid="w176"></asp:Label> </TD><TD vAlign=middle align=left><asp:DropDownList id="orderflag" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w177" AutoPostBack="True" OnSelectedIndexChanged="orderflag_SelectedIndexChanged"><asp:ListItem>GROSIR</asp:ListItem>
</asp:DropDownList></TD></TR><TR id="Tr5" runat="server"><TD vAlign=middle align=left><asp:Label id="custgroupoid" runat="server" Visible="False" __designer:wfdid="w178"></asp:Label></TD><TD vAlign=middle align=left colSpan=5><asp:GridView id="gvCustomerGroup" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w179" OnSelectedIndexChanged="gvCustomerGroup_SelectedIndexChanged" PageSize="8" EmptyDataText="No data in database." AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custgroupoid,custgroupname,custgroupaddr,custgroupcode,crlimit" GridLines="None" OnRowDataBound="gvCustomerGroup_RowDataBound" OnPageIndexChanging="gvCustomerGroup_PageIndexChanging">
<RowStyle BackColor="#FFFBD6"  ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"  Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custgroupoid" HeaderText="Group ID" ReadOnly="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle  Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupname" HeaderText="Group Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle ></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="crlimit" HeaderText="Sisa Cr.Limit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"  Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="lblstatusdataCust" runat="server" Text="No Customer Data !" CssClass="Important"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD vAlign=middle align=left>Customer <asp:Label id="Label36" runat="server" CssClass="Important" Text="*" __designer:wfdid="w180"></asp:Label></TD><TD align=left><asp:TextBox id="CustName" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w181"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtnFindCust" onclick="ibtnFindCust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w182"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" onclick="btnClearCust_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w183"></asp:ImageButton></TD><TD vAlign=middle align=left>Sales</TD><TD align=left><asp:DropDownList id="spgOid" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w184" AutoPostBack="True"></asp:DropDownList> <asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w185"></asp:Label></TD><TD vAlign=middle align=left><asp:Label id="Label30" runat="server" Text="Currency" __designer:wfdid="w186"></asp:Label></TD><TD vAlign=middle align=left><asp:DropDownList id="currencyoid" runat="server" Width="134px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w187" Enabled="False"></asp:DropDownList></TD></TR><TR><TD vAlign=middle align=left></TD><TD vAlign=middle align=left colSpan=5><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" Visible="False" __designer:wfdid="w188" PageSize="8" EmptyDataText="No data in database." AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custoid,custcode,custname,custpaytermdefaultoid,custcurrdefaultoid,salesoid,timeofpayment,CUSTADDR" GridLines="None">
<RowStyle BackColor="#FFFBD6"  ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"  ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"  Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="CUSTCODE" HeaderText="User ID" ReadOnly="True" SortExpression="CUSTCODE">
<HeaderStyle CssClass="gvhdr"  ForeColor="Black"></HeaderStyle>

<ItemStyle  Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTNAME" HeaderText="Person Name" SortExpression="CUSTNAME">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"  ForeColor="Black"></HeaderStyle>

<ItemStyle ></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="CUSTADDR" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="crlimit" HeaderText="Sisa Credit Limit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate><asp:Label ID="lblstatusdataCust" runat="server" CssClass="Important" Text="No Customer Data !"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD vAlign=middle align=left>No. Ref <asp:Label id="Label300" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w189"></asp:Label> </TD><TD vAlign=middle align=left><asp:TextBox id="POref" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w190" MaxLength="16"></asp:TextBox></TD><TD align=left>Tanggal SO&nbsp;</TD><TD vAlign=middle align=left><asp:TextBox id="orderdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w191" MaxLength="20" Enabled="False"></asp:TextBox> <asp:ImageButton id="iborderdate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w192"></asp:ImageButton> <asp:Label id="Label1" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w193"></asp:Label> </TD><TD vAlign=middle align=left><asp:Label id="Label50" runat="server" Width="83px" Text="Rate To (IDR)" __designer:wfdid="w194"></asp:Label></TD><TD vAlign=middle align=left><asp:TextBox id="rate2idrvalue" runat="server" Width="128px" CssClass="inpTextDisabled" __designer:wfdid="w195" Enabled="False">1</asp:TextBox></TD></TR><TR><TD vAlign=middle align=left><asp:Label id="Label29" runat="server" Text="TOP" __designer:wfdid="w196"></asp:Label></TD><TD vAlign=middle align=left><asp:DropDownList id="DDLTop" runat="server" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w197" Enabled="False"></asp:DropDownList>&nbsp;<asp:Label id="lblcash" runat="server" Text="cash" Visible="False" __designer:wfdid="w198"></asp:Label> <asp:Label id="lbldays" runat="server" CssClass="inpText" Font-Size="8pt" Font-Names="Andalus" ForeColor="Red" Text="*) Days" __designer:wfdid="w199"></asp:Label></TD><TD align=left>Tanggal DO&nbsp;</TD><TD vAlign=middle align=left><asp:TextBox id="deliverydate" runat="server" Width="100px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w200" MaxLength="20" EnableTheming="True"></asp:TextBox>&nbsp;<asp:ImageButton id="iborderdate2" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w201"></asp:ImageButton>&nbsp;<asp:Label id="Label14" runat="server" CssClass="Important" Font-Size="8pt" Text="(dd/MM/yyyy)" __designer:wfdid="w202"></asp:Label></TD><TD vAlign=middle align=left>Promo</TD><TD vAlign=middle align=left><asp:DropDownList id="ddlPromo" runat="server" Width="134px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w203" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD vAlign=middle align=left>Expedisi</TD><TD vAlign=middle align=left><asp:DropDownList id="ddlexpedisi" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w204"><asp:ListItem>Darat</asp:ListItem>
<asp:ListItem>Laut</asp:ListItem>
<asp:ListItem>Udara</asp:ListItem>
</asp:DropDownList></TD><TD align=left>DO.&nbsp;Address</TD><TD vAlign=middle align=left><asp:TextBox id="deliveryaddr" runat="server" Width="172px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w205" TextMode="MultiLine"></asp:TextBox></TD><TD align=left>Order Note <asp:Label id="Label303" runat="server" CssClass="Important" Text="*" Visible="False" __designer:wfdid="w206"></asp:Label> </TD><TD align=left><asp:TextBox id="ordernote" runat="server" Width="172px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w207" MaxLength="300" TextMode="MultiLine"></asp:TextBox>&nbsp; </TD></TR><TR><TD vAlign=middle align=left>Type SO</TD><TD vAlign=middle align=left><asp:DropDownList id="typeSO" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w208" AutoPostBack="True"><asp:ListItem>Dealer</asp:ListItem>
<asp:ListItem Value="ecommers">E-Commerce</asp:ListItem>
<asp:ListItem>Kanvas</asp:ListItem>
<asp:ListItem>User</asp:ListItem>
<asp:ListItem>Konsinyasi</asp:ListItem>
<asp:ListItem>Project</asp:ListItem>
</asp:DropDownList></TD><TD vAlign=middle align=left>Taxable</TD><TD vAlign=middle align=left><asp:CheckBox id="chkTax" runat="server" Text="INCLUDE" __designer:wfdid="w209"></asp:CheckBox><asp:Label id="lbltax" runat="server" Visible="False" __designer:wfdid="w210"></asp:Label></TD><TD vAlign=middle align=left>Total Order&nbsp;</TD><TD vAlign=middle align=left><asp:TextBox id="totalorder" runat="server" Width="128px" CssClass="inpTextDisabled" __designer:wfdid="w211" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD id="TD9" vAlign=middle align=left runat="server" visible="true">Status</TD><TD id="TD12" vAlign=middle align=left runat="server" visible="true"><asp:TextBox id="orderstatus" runat="server" Width="128px" CssClass="inpTextDisabled" __designer:wfdid="w212" MaxLength="10" ReadOnly="True">In Process</asp:TextBox></TD><TD id="TD13" vAlign=middle align=left runat="server" visible="true"><asp:Label id="Label31" runat="server" Text="Bundling" Visible="False" __designer:wfdid="w213"></asp:Label></TD><TD id="TD14" vAlign=middle align=left runat="server" visible="true"><asp:CheckBox id="bundling" runat="server" Text="Yes" Visible="False" __designer:wfdid="w214" AutoPostBack="True" OnCheckedChanged="bundling_CheckedChanged"></asp:CheckBox> <asp:Label id="lblbundling" runat="server" Visible="False" __designer:wfdid="w215"></asp:Label> </TD><TD id="TD15" vAlign=middle align=left runat="server" visible="true"></TD><TD id="TD16" align=left runat="server" visible="true"><asp:Label id="Posting" runat="server" Visible="False" __designer:wfdid="w216"></asp:Label><asp:Label id="hdisc" runat="server" Text="0.00" Visible="False" __designer:wfdid="w217"></asp:Label></TD></TR><TR><TD id="Td1" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblNoOrderOnLine" runat="server" Width="82px" Text="No. SO Online" Visible="False" __designer:wfdid="w218"></asp:Label></TD><TD id="Td2" vAlign=middle align=left runat="server" visible="true"><asp:TextBox id="NoOrder" runat="server" Width="125px" CssClass="inpText" Visible="False" __designer:wfdid="w219" MaxLength="50"></asp:TextBox></TD><TD id="Td3" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblNamaEndUSer" runat="server" Width="91px" Text="Nama End User" Visible="False" __designer:wfdid="w220"></asp:Label></TD><TD id="Td4" vAlign=middle align=left runat="server" visible="true"><asp:TextBox id="NamaEndUSer" runat="server" Width="125px" CssClass="inpText" Visible="False" __designer:wfdid="w221" MaxLength="50"></asp:TextBox></TD><TD id="Td5" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblNoInvOnline" runat="server" Width="91px" Text="No. Inv. Online" Visible="False" __designer:wfdid="w222"></asp:Label></TD><TD id="Td6" align=left runat="server" visible="true"><asp:TextBox id="NoInvOnline" runat="server" Width="125px" CssClass="inpText" Visible="False" __designer:wfdid="w223" MaxLength="50"></asp:TextBox></TD></TR><TR><TD id="Td7" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblNoAwb" runat="server" Width="62px" Text="No. AWB" Visible="False" __designer:wfdid="w224"></asp:Label></TD><TD id="Td24" vAlign=middle align=left runat="server" visible="true"><asp:TextBox id="NoAwb" runat="server" Width="125px" CssClass="inpText" Visible="False" __designer:wfdid="w225" MaxLength="50"></asp:TextBox></TD><TD id="Td26" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblShippingAddr" runat="server" Width="91px" Text="Shipping Addres" Visible="False" __designer:wfdid="w226"></asp:Label></TD><TD id="Td34" vAlign=middle align=left runat="server" visible="true"><asp:TextBox id="ShipppingAddres" runat="server" Width="150px" CssClass="inpText" Visible="False" __designer:wfdid="w227" MaxLength="500" TextMode="MultiLine"></asp:TextBox></TD><TD id="Td35" vAlign=middle align=left runat="server" visible="true"><asp:Label id="LblToAddres" runat="server" Width="91px" Text="To Addres" Visible="False" __designer:wfdid="w228"></asp:Label></TD><TD id="Td36" align=left runat="server" visible="true"><asp:TextBox id="ToAddress" runat="server" Width="150px" CssClass="inpText" Visible="False" __designer:wfdid="w229" MaxLength="500" TextMode="MultiLine"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD align=left colSpan=2><asp:Label id="Label38" runat="server" Width="116px" Font-Size="12pt" Font-Bold="True" ForeColor="Black" Text="Data Detail :" __designer:wfdid="w230" Font-Underline="True"></asp:Label> <asp:Label id="I_U2" runat="server" Font-Bold="True" ForeColor="Red" Visible="False" __designer:wfdid="w231">New Detail</asp:Label> <asp:Label id="ItemGroup" runat="server" Visible="False" __designer:wfdid="w232"></asp:Label> <asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w233"></asp:Label> <asp:Label id="itemunit" runat="server" Visible="False" __designer:wfdid="w234"></asp:Label></TD><TD align=left><asp:Label id="StatusBarang" runat="server" Visible="False" __designer:wfdid="w235"></asp:Label></TD><TD align=left><asp:Label id="lbltempAmt" runat="server" __designer:wfdid="w236" visible="false"></asp:Label><asp:Label id="txtVarPrice" runat="server" Visible="False" __designer:wfdid="w237">0.00</asp:Label> <asp:Label id="satuan1" runat="server" Visible="False" __designer:wfdid="w238"></asp:Label></TD><TD align=left><asp:Label id="trnorderdtlseq" runat="server" Visible="False" __designer:wfdid="w239"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label37" runat="server" Width="72px" Font-Size="8pt" __designer:wfdid="w240">Jenis Price</asp:Label> </TD><TD align=left><asp:DropDownList id="jPrice" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w241" AutoPostBack="True"><asp:ListItem Value="NORMAL">PRICE NORMAL</asp:ListItem>
<asp:ListItem Value="NOTA">PRICE NOTA</asp:ListItem>
<asp:ListItem Value="KHUSUS">PRICE KHUSUS</asp:ListItem>
<asp:ListItem Value="SILVER">PRICE SILVER</asp:ListItem>
<asp:ListItem Value="PLATINUM">PRICE PLATINUM</asp:ListItem>
<asp:ListItem Value="GOLD">PRICE GOLD</asp:ListItem>
<asp:ListItem Enabled="False" Value="promo">PROMO</asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Label id="PembagiTax" runat="server" Visible="False" __designer:wfdid="w242"></asp:Label></TD><TD align=left><asp:TextBox id="disc" runat="server" Width="33px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w243" ReadOnly="True">0.00</asp:TextBox> <asp:CheckBox id="cbBonus" runat="server" Width="103px" Text="Item Bonus" Visible="False" __designer:wfdid="w244" AutoPostBack="True" OnCheckedChanged="cbBonus_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:TextBox id="DiscPct" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w245" ReadOnly="True">0.00</asp:TextBox> </TD></TR><TR><TD align=left>Katalog&nbsp;<asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w246"></asp:Label> </TD><TD align=left><asp:TextBox id="itemlongdesc" runat="server" Width="264px" CssClass="inpText" __designer:wfdid="w247" MaxLength="255"></asp:TextBox> <asp:ImageButton id="ibtnsearchitem" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w248"></asp:ImageButton> <asp:ImageButton id="ibtneraseitem" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w249">
                                                            </asp:ImageButton></TD><TD align=left>Merk</TD><TD align=left><asp:TextBox id="Merk" runat="server" Width="129px" CssClass="inpTextDisabled" __designer:wfdid="w250" MaxLength="255" ReadOnly="True"></asp:TextBox></TD><TD align=left><asp:TextBox id="totalqty" runat="server" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w251" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD align=left></TD><TD align=left colSpan=4><asp:GridView style="MARGIN-TOP: 0px" id="gvItemSearch" runat="server" Width="100%" Height="80px" ForeColor="#333333" __designer:wfdid="w252" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemoid,itemgroupcode,itemdesc,merk,pricelist,saldoakhir,price,discunit1,discunit2,discunit3,PriceNya,typebarang,QtyTuku,promtype,MinimPrice,StatusBarang,promoid,discnya,pricesilver,priceplatinum,pricegold,priceitem,pricenota,priceNormal,pricekhusus,priceexpedisi,hpp,itemdtloid" GridLines="None" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemgroupcode" HeaderText="Group">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PriceNya" HeaderText="FOB SBY">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceList" HeaderText="Price Cabang">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="netto" HeaderText="Netto" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Last Stock">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typebarang" HeaderText="Type barang" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="promtype" HeaderText="Type Promo" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="StatusBarang" HeaderText="Status Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtyTuku" HeaderText="QtyTuku" Visible="False"></asp:BoundField>
<asp:BoundField DataField="promoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblstatusitemdata" runat="server" CssClass="Important" 
        Text="No Item data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Quantity <asp:Label id="Label15" runat="server" CssClass="Important" Text="*" __designer:wfdid="w253"></asp:Label> </TD><TD align=left><asp:TextBox id="OrderQty" runat="server" Width="81px" CssClass="inpText" __designer:wfdid="w254" AutoPostBack="True" MaxLength="7">0.00</asp:TextBox> <asp:DropDownList id="ddlUnit" runat="server" Width="65px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w255" AutoPostBack="True" Enabled="False"></asp:DropDownList>&nbsp;</TD><TD align=left>Disc. per unit</TD><TD align=left><asp:TextBox id="Discdtl1" runat="server" CssClass="inpText" __designer:wfdid="w256" AutoPostBack="True" MaxLength="13">0.00</asp:TextBox></TD><TD id="TD41" align=left runat="server" Visible="false"><asp:TextBox id="Discdtl2" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w257" AutoPostBack="True" MaxLength="13">0</asp:TextBox></TD></TR><TR><TD align=left>Price Per Unit <asp:Label id="Label18" runat="server" Width="2px" Height="6px" CssClass="Important" Text="*" __designer:wfdid="w258"></asp:Label></TD><TD align=left><asp:TextBox id="OrderPrice" runat="server" Width="101px" CssClass="inpText" __designer:wfdid="w259" AutoPostBack="True" MaxLength="13">0.00</asp:TextBox> <asp:Label id="lblcurr" runat="server" Text="IDR" __designer:wfdid="w260"></asp:Label> </TD><TD align=left>Total</TD><TD align=left><asp:TextBox id="TxtTotKotor" runat="server" Width="132px" CssClass="inpTextDisabled" __designer:wfdid="w261" ReadOnly="True">0.00</asp:TextBox></TD><TD align=left><asp:ImageButton id="imbAdd" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w262" AlternateText="Add to List"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w263" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD id="Td8" align=left runat="server" visible="false">Disc Type </TD><TD id="Td17" align=left runat="server" visible="false"><asp:DropDownList id="disctype" runat="server" Width="134px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w264" AutoPostBack="True"><asp:ListItem Selected="True">AMOUNT</asp:ListItem>
<asp:ListItem>PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD id="Td21" align=left runat="server" visible="false">Disc Value </TD><TD id="Td22" align=left runat="server" visible="false"><asp:Label id="discount" runat="server" Visible="False" __designer:wfdid="w265"></asp:Label></TD><TD id="Td23" align=left runat="server" visible="false">Disc <asp:Label id="lblVarPrice" runat="server" Text="Var Price" __designer:wfdid="w266"></asp:Label></TD></TR><TR><TD id="Td27" align=left runat="server" visible="false">Note </TD><TD id="Td28" align=left runat="server" visible="false"><asp:TextBox id="ItemNote" runat="server" Width="129px" CssClass="inpText" __designer:wfdid="w267" MaxLength="50"></asp:TextBox></TD><TD id="Td29" align=left runat="server" visible="false">Free</TD><TD id="Td30" align=left runat="server" visible="false"><asp:DropDownList id="DDLFree" runat="server" Width="134px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w268" AutoPostBack="True" Enabled="False"><asp:ListItem Selected="True" Value="N">NO</asp:ListItem>
<asp:ListItem Value="Y">YES</asp:ListItem>
</asp:DropDownList></TD><TD id="Td31" align=left runat="server" visible="false"></TD></TR><TR><TD style="HEIGHT: 155px" id="Td32" align=left colSpan=5><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 140px; BACKGROUND-COLOR: beige"><asp:GridView id="gvItemOrdered" runat="server" Width="100%" Height="58px" ForeColor="#333333" __designer:wfdid="w269" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="orderdtlseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderdtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemLDesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemgroupcode" HeaderText="Group Item" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="22px"></HeaderStyle>

<ItemStyle Width="22px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderqty" DataFormatString="{0:#,##0.00}" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="disc" HeaderText="Disc. Promo">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtldisc1" HeaderText="Disc. Umum">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtldisc2" HeaderText="Disc. Intern" Visible="False">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="varprice" HeaderText="VarPrice" Visible="False">
<ControlStyle Width="100px"></ControlStyle>

<FooterStyle Width="50px"></FooterStyle>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="total" HeaderText="Sub Total">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totKotor" HeaderText="Bruto" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemnote" HeaderText="Notes" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="tempAmtUsd" HeaderText="tempAmtUsd" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="jenisprice" HeaderText="Jenis Price">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="discamtpct" HeaderText="Disc %" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceitem" HeaderText="pricenota" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="pricenota" HeaderText="pricenota" Visible="False"></asp:BoundField>
<asp:BoundField DataField="pricekhusus" HeaderText="pricekhusus" Visible="False"></asp:BoundField>
<asp:BoundField DataField="pricesilver" HeaderText="pricesilver" Visible="False"></asp:BoundField>
<asp:BoundField DataField="pricegold" HeaderText="pricegold" Visible="False"></asp:BoundField>
<asp:BoundField DataField="priceplatinum" HeaderText="priceplatinum" Visible="False"></asp:BoundField>
<asp:BoundField DataField="MinimPrice" HeaderText="MinimPrice" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                            <asp:Label 
        ID="lblnoordereditem" runat="server" CssClass="Important" 
        Text="No Ordered Item !!"></asp:Label>
                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=left colSpan=5><asp:Label id="lblOrderAmount" runat="server" Font-Size="10pt" Font-Bold="True" ForeColor="Navy" Text="Total Netto :" __designer:wfdid="w270" Amount></asp:Label>&nbsp;<asp:Label id="orderamount" runat="server" Font-Size="10pt" Font-Bold="True" ForeColor="Navy" Text="0.00" __designer:wfdid="w271"></asp:Label> <asp:Label id="curr" runat="server" Font-Size="10pt" Font-Bold="True" __designer:wfdid="w272">Rupiah</asp:Label></TD></TR><TR><TD align=left colSpan=5><asp:Label id="lblUpdate" runat="server" Text="Last Update" __designer:wfdid="w273"></asp:Label>&nbsp;<asp:Label id="updUser" runat="server" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w274"></asp:Label>&nbsp;<asp:Label id="lblOn" runat="server" Text="By" __designer:wfdid="w275"></asp:Label>&nbsp;<asp:Label id="updTime" runat="server" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w276"></asp:Label>&nbsp;<asp:Label id="warningcl" runat="server" CssClass="Important" Font-Size="8pt" __designer:wfdid="w277"></asp:Label> </TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w278"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w279">
                                                    </asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w280">
                                                    </asp:ImageButton> <asp:ImageButton id="BtnApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w281" AlternateText="Send for Approval"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" Visible="False" __designer:wfdid="w282"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w284" AssociatedUpdatePanelID="UpdatePanel8"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w285"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><asp:Label id="MinimPrice" runat="server" Visible="False" __designer:wfdid="w286"></asp:Label><asp:Label id="pricenormal" runat="server" Visible="False" __designer:wfdid="w287"></asp:Label><asp:Label id="pricenota" runat="server" Visible="False" __designer:wfdid="w288"></asp:Label><asp:Label id="pricekhusus" runat="server" Visible="False" __designer:wfdid="w289"></asp:Label><asp:Label id="pricesilver" runat="server" Visible="False" __designer:wfdid="w290"></asp:Label><asp:Label id="priceplatinum" runat="server" Visible="False" __designer:wfdid="w291"></asp:Label><asp:Label id="pricegold" runat="server" Visible="False" __designer:wfdid="w292"></asp:Label><asp:Label id="itemdtloid" runat="server" Visible="False" __designer:wfdid="w293"></asp:Label><asp:Label id="hpp" runat="server" Visible="False" __designer:wfdid="w294"></asp:Label><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w295" Format="dd/MM/yyyy" PopupButtonID="iborderdate2" TargetControlID="deliverydate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee5" runat="server" __designer:wfdid="w296" TargetControlID="DiscPct" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee6" runat="server" __designer:wfdid="w297" TargetControlID="disc" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft" CultureName="en-US">
                                                            </ajaxToolkit:MaskedEditExtender><asp:Label id="granddisc" runat="server" Visible="False" __designer:wfdid="w300"></asp:Label><asp:Label id="custcode" runat="server" Visible="False" __designer:wfdid="w301"></asp:Label><asp:Label id="DiscHdr2" runat="server" Font-Size="10pt" Font-Bold="True" Text="0.00" Visible="False" __designer:wfdid="w302"></asp:Label><asp:Label id="DiscHdr1" runat="server" Font-Size="10pt" Font-Bold="True" Text="0.00" Visible="False" __designer:wfdid="w303"></asp:Label><asp:Label id="ddisc" runat="server" Font-Size="10pt" Font-Bold="True" ForeColor="Navy" Text="0.00" Visible="False" __designer:wfdid="w304"></asp:Label><asp:Label id="totKotor" runat="server" Font-Size="10pt" Font-Bold="True" ForeColor="Navy" Text="0.00" Visible="False" __designer:wfdid="w305"></asp:Label><asp:Label id="priceexpedisi" runat="server" Visible="False" __designer:wfdid="w306"></asp:Label><asp:Label id="BottomPrice" runat="server" Visible="False" __designer:wfdid="w307"></asp:Label><asp:Label id="Konversike3" runat="server" Visible="False" __designer:wfdid="w308"></asp:Label></TD></TR></TBODY></TABLE> <asp:DropDownList id="DDLWH" runat="server" Width="128px" CssClass="inpText" Font-Size="8pt" Visible="False" __designer:wfdid="w298"></asp:DropDownList><asp:DropDownList id="taxable" runat="server" Width="104px" CssClass="inpTextDisabled" Font-Size="8pt" Visible="False" __designer:wfdid="w299" Enabled="False" OnSelectedIndexChanged="taxable_SelectedIndexChanged"><asp:ListItem Value="NONTAX">NONTAX</asp:ListItem>
<asp:ListItem Value="INC">INC</asp:ListItem>
</asp:DropDownList>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></th>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="100%" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE cellSpacing=1 cellPadding=1 width="100%" border=0><TBODY><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                    <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD style="WIDTH: 444px" vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> <asp:Label id="LblsMsg" runat="server" ForeColor="Black" __designer:wfdid="w1"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 25px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False">
            </asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
