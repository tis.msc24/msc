<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWhSupplier.aspx.vb" Inherits="trnWhSupplier" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Transfer Confirm" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
               <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                 <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Confirm :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>Transfer No </TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" class="Label" align=left>:</TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w40" MaxLength="30"></asp:TextBox></TD></TR><TR><TD id="TD9" class="Label" align=left Visible="true">Cabang</TD><TD id="TD11" class="Label" align=left Visible="true">:</TD><TD id="TD8" align=left colSpan=4 Visible="true"><asp:DropDownList id="drCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w41"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left><asp:CheckBox id="CbTanggal" runat="server" Text="Tanggal" __designer:wfdid="w56"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w42"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w44"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton>&nbsp;&nbsp; <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w46"></asp:Label>&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>Status</TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>: </TD><TD align=left colSpan=4><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w47"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w48">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w49">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w50" DefaultButton="btnSearch"><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w51" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="InTransferoid" DataNavigateUrlFormatString="trnWhSupplier.aspx?oid={0}" DataTextField="InTransferNo" HeaderText="Transfer No">
<ControlStyle ForeColor="Red"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Red"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Username">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FromBranch" HeaderText="From Branch">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ToBranch" HeaderText="To Branch">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("InTransferoid") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" ToolTip='<%# eval("InTransferoid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w52" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w53" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w54" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="tgl1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w55" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="tgl2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="TujuanCb" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w80"></asp:DropDownList> <asp:Label id="OidTC" runat="server" Text="OidTC" __designer:wfdid="w81" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:DropDownList id="FromBranch" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w82" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Transfer No.</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" __designer:wfdid="w83" Enabled="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w84" Visible="False"></asp:Label> </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w85" Visible="False" MaxLength="20">IN PROCESS</asp:TextBox> <asp:Label id="FromLoc" runat="server" __designer:wfdid="w95" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Date</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w86" Enabled="False" ValidationGroup="MKE"></asp:TextBox> <asp:ImageButton id="imbTwDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w88"></asp:Label>&nbsp; </TD><TD class="Label" align=left>To Branch</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="ToBranch" runat="server" Width="250px" CssClass="inpTextDisabled" AutoPostBack="True" __designer:wfdid="w89" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="labelnotr" runat="server" Width="75px" Text="No. Ref (TW)" __designer:wfdid="w90"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w91" MaxLength="200"></asp:TextBox>&nbsp;<asp:ImageButton id="ibposearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w92"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoerase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w93"></asp:ImageButton> <asp:Label id="pooid" runat="server" __designer:wfdid="w94" Visible="False"></asp:Label>&nbsp; </TD><TD class="Label" align=left><asp:Label id="ToLoc" runat="server" Width="69px" __designer:wfdid="w96">To Location</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="tolocation" runat="server" Width="250px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w97"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceTwDate" runat="server" __designer:wfdid="w98" Format="dd/MM/yyyy" PopupButtonID="imbTwDate" TargetControlID="transferdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="TwDate" runat="server" __designer:wfdid="w99" TargetControlID="transferdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVpo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w100" Visible="False" DataKeyNames="ToMtrlocOid,toMtrBranch,fromMtrBranch,FromMtrlocoid,trfmtrnote" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnTrfToReturNo" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w101" MaxLength="200"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="labelfromloc" runat="server" Width="86px" Text="From Location" __designer:wfdid="w102" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="fromlocation" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w103" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Item Detail :" __designer:wfdid="w104" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w105" Visible="False"></asp:Label> <asp:RadioButtonList id="rblflag" runat="server" AutoPostBack="True" __designer:wfdid="w106" Visible="False" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="Purchase">Purchase Retur</asp:ListItem>
<asp:ListItem Value="Supplier">Ambil Barang di WH Supplier</asp:ListItem>
</asp:RadioButtonList> <asp:Label id="StatusExp" runat="server" __designer:wfdid="w107" Visible="False"></asp:Label> <asp:Label id="typedimensi" runat="server" __designer:wfdid="w108" Visible="False"></asp:Label> <asp:Label id="beratvolume" runat="server" __designer:wfdid="w109" Visible="False"></asp:Label> <asp:Label id="beratbarang" runat="server" __designer:wfdid="w110" Visible="False"></asp:Label> <asp:Label id="jenisexp" runat="server" __designer:wfdid="w111" Visible="False"></asp:Label> <asp:Label id="amtexpedisi" runat="server" __designer:wfdid="w112" Visible="False"></asp:Label> <asp:Label id="NettoExp" runat="server" __designer:wfdid="w113" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item" __designer:wfdid="w114"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w115"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w116"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w117"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w118"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" __designer:wfdid="w119" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w120" Visible="False" DataKeyNames="itemoid,satuan1,satuan2,satuan3,unit1,unit2,konversi1_2,konversi2_3,sisaretur,unitoid,unit,unit3,itemdesc,trfmtrmstoid,statusexp,typedimensi,beratvolume,beratbarang,jenisexp,amtexpedisi,nettoexp" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock Gudang" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit Gudang" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisaretur" DataFormatString="{0:#,##0.00}" HeaderText="Sisa Confirm">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrmstoid" HeaderText="trfmtrmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="statusexp" HeaderText="statusexp" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typedimensi" HeaderText="typedimensi" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="beratvolume" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratbarang" HeaderText="beratbarang" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisexp" HeaderText="jenisexp" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtexpedisi" HeaderText="amtexpedisi" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="nettoexp" HeaderText="nettoexp" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisexp" HeaderText="jenisexp" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" CssClass="gvhdr"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="labelkonversi1_2" runat="server" __designer:wfdid="w122" Visible="False"></asp:Label> <asp:Label id="labelkonversi2_3" runat="server" __designer:wfdid="w123" Visible="False"></asp:Label> <asp:Label id="labelunit1" runat="server" __designer:wfdid="w124" Visible="False"></asp:Label> <asp:Label id="labelunit2" runat="server" __designer:wfdid="w125" Visible="False"></asp:Label> <asp:Label id="labelunit3" runat="server" __designer:wfdid="w126" Visible="False"></asp:Label> <asp:Label id="labeltempsisaretur" runat="server" __designer:wfdid="w127" Visible="False"></asp:Label><asp:Label id="labelsatuan1" runat="server" __designer:wfdid="w128" Visible="False"></asp:Label><asp:Label id="labelsatuan2" runat="server" __designer:wfdid="w129" Visible="False"></asp:Label><asp:Label id="labelsatuan3" runat="server" __designer:wfdid="w130" Visible="False"></asp:Label><asp:Label id="labelsatuan" runat="server" __designer:wfdid="w131" Visible="False"></asp:Label><asp:Label id="trfmtrmstoid" runat="server" __designer:wfdid="w132" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Merk</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" __designer:wfdid="w133" Enabled="False" MaxLength="200"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w134"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w135" MaxLength="10" ValidationGroup="MKE">0</asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="Max :" __designer:wfdid="w136"></asp:Label>&nbsp;&nbsp;<asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red" __designer:wfdid="w137">0.00</asp:Label>&nbsp;<asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w138"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" __designer:wfdid="w139" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w140" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w141"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w143" DataKeyNames="itemoid,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,sisaretur,trfmtrmstoid,statusexp,typedimensi,beratvolume,beratbarang,jenisexp,amtexpedisi,nettoexp" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" runat="server" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trfmtrmstoid" HeaderText="trfmtrmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="statusexp" HeaderText="statusexp" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typedimensi" HeaderText="typedimensi" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="beratvolume" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratbarang" HeaderText="beratbarang" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisexp" HeaderText="jenisexp" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtexpedisi" HeaderText="amtexpedisi" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="nettoexp" HeaderText="nettoexp" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w144"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w145"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w146"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w147"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w148" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w149" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w150" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w151"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w121" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Confirm :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg"  runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender>
<span style="display:none"><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button></span>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

