<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstItemPrice.aspx.vb" Inherits="mstItemPrice" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
   <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="height: 28px">
                <asp:Label ID="HDRBARANG" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Data Barang (Price List)"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">

                <contenttemplate></contenttemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<ajaxToolkit:TabContainer id="TabContainer1" runat="server" Width="100%" ActiveTabIndex="1"><cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Daftar Barang :.</span></strong>

                        
</HeaderTemplate>
<ContentTemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><asp:UpdatePanel id="UPanelListMaterial" runat="server" __designer:wfdid="w208"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" CssClass="normalFont" Text="Filter 1 :" __designer:wfdid="w209"></asp:Label></TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w210" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged"><asp:ListItem Selected="True" Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemflag">Status</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w212" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w213"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w214"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w215"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left>Status :</TD><TD align=left colSpan=5><asp:DropDownList id="ddlStatusView" runat="server" Width="99px" CssClass="inpText" __designer:wfdid="w216" AutoPostBack="True">
                                                            <asp:ListItem Value="Aktif">Aktif</asp:ListItem>
                                                            <asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;View Top <asp:TextBox id="SelectTop" runat="server" Width="30px" CssClass="inpText" __designer:wfdid="w217" MaxLength="30">100</asp:TextBox> <asp:Button id="BtnViewItem" runat="server" CssClass="btn red" Text="Edit Price" __designer:wfdid="w218"></asp:Button> <asp:DropDownList id="ddlStatusHarga" runat="server" CssClass="inpText" Font-Bold="True" __designer:wfdid="w211" Visible="False"><asp:ListItem>V</asp:ListItem>
<asp:ListItem>X</asp:ListItem>
<asp:ListItem>O</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel1" runat="server" __designer:wfdid="w219" DefaultButton="btnSearch"><asp:GridView id="GVmstgen" runat="server" Width="100%" CssClass="normalFont" ForeColor="#333333" __designer:wfdid="w220" EnableModelValidation="True" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemoid" GridLines="None" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" BorderColor="Cyan" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="itemoid" DataNavigateUrlFormatString="mstItemPrice.aspx?oid={0}" DataTextField="itemcode" HeaderText="Kode" SortExpression="itemcode">
<ControlStyle Font-Size="Small"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="65%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bottompricegrosir" HeaderText="Nota" SortExpression="bottompricegrosir">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PriceNormal" HeaderText="Normal			" SortExpression="PriceNormal">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="minimprice" HeaderText="Minim" SortExpression="minimprice">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricekhusus" HeaderText="Khusus" SortExpression="pricekhusus">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricesilver" HeaderText="Price Silver">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricegold" HeaderText="Price Gold">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceplatinum" HeaderText="Price Platinum">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="btnprintlist" onclick="btnprintlist_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w165" CommandArgument='<%#eval("itemoid") %>' ToolTip='<%#eval("itemcode") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Tidak Ditemukan !!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w221"><ProgressTemplate>
<asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w222"></asp:Image><BR /><SPAN class="normalFont">Please wait...</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="fteTop" runat="server" __designer:wfdid="w223" Enabled="True" TargetControlID="SelectTop" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><DIV style="DISPLAY: block; TEXT-ALIGN: center">&nbsp;</DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> &nbsp; &nbsp;&nbsp; </DIV>
</ContentTemplate>
</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2"><HeaderTemplate>
 <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
<strong><span style="font-size: 9pt">Form Barang :.</span></strong>
                       
</HeaderTemplate>
<ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label57" runat="server" Width="94px" CssClass="normalFont" Font-Size="Small" Text="Filter Barang" __designer:wfdid="w51"></asp:Label> <SPAN style="WIDTH: 80px; COLOR: #ff0000"></SPAN></TD><TD style="COLOR: #000099" align=left colSpan=6><asp:DropDownList id="FilterItemDDL" runat="server" CssClass="inpText" __designer:wfdid="w52"><asp:ListItem Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Barang</asp:ListItem>
<asp:ListItem Value="PERSONNAME">PIC</asp:ListItem>
<asp:ListItem Enabled="False" Value="statusitem">Jenis Barang</asp:ListItem>
<asp:ListItem>merk</asp:ListItem>
</asp:DropDownList> <asp:TextBox style="TEXT-TRANSFORM: uppercase" id="itemdesc" runat="server" Width="338px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w53" MaxLength="250" AutoPostBack="True" size="20"></asp:TextBox> <asp:ImageButton id="btnSearchmat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w54"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnClearB" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w55"></asp:ImageButton> </TD></TR><!-- Grouping Barang--><TR><TD align=left><asp:Label id="itemoid" runat="server" __designer:wfdid="w56" Visible="False"></asp:Label> </TD><TD style="COLOR: #000099" align=left colSpan=6><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="gvMaterial" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w57" Visible="False" PageSize="8" GridLines="None" DataKeyNames="itemcode,itemdesc,jenis,itemflag,HPP,PERSONNAME,Merk,itemoid,JenisBarang,minimprice,priceNormal,PriceNota,pricekhusus,itemsafetystockunit1,discunit1,discunit2,discunit3,upduser,updtime,upd" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenis" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PERSONNAME" HeaderText="PIC">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" Visible="False"></asp:BoundField>
</Columns>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<EmptyDataTemplate>
                                                                            <asp:Label ID="Label19" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Item data found!!"></asp:Label>

                                                                        
</EmptyDataTemplate>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left>Type Barang</TD><TD style="COLOR: #000099" align=left colSpan=6><asp:TextBox id="TypeBarang" runat="server" Width="200px" CssClass="inpTextDisabled" Font-Size="Small" __designer:wfdid="w58" MaxLength="50" AutoPostBack="True" Enabled="False"></asp:TextBox> </TD></TR><TR><TD style="COLOR: #000099" align=left colSpan=7><asp:Label style="FONT-WEIGHT: 700" id="Label44" runat="server" Width="155px" CssClass="normalFont" Font-Size="Small" Text=":. Price List Katalog" __designer:wfdid="w59"></asp:Label> </TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Width="76px" CssClass="normalFont" Font-Size="Small" Text="Price Nota" __designer:wfdid="w60"></asp:Label> &nbsp; </TD><TD style="WIDTH: 228px" align=left colSpan=3><asp:TextBox id="PriceNota" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w61" MaxLength="9" AutoPostBack="True">0</asp:TextBox> </TD><TD align=left colSpan=1><asp:Label id="Label1" runat="server" Width="90px" CssClass="normalFont" Font-Size="Small" Text="Diskon 1" __designer:wfdid="w62"></asp:Label> </TD><TD align=left colSpan=2><asp:TextBox id="Disc1" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w63" MaxLength="9" AutoPostBack="True">0</asp:TextBox> &nbsp;&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label32" runat="server" Width="90px" CssClass="normalFont" Font-Size="Small" Text="Price Normal" __designer:wfdid="w64"></asp:Label> </TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="PriceList" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w65" MaxLength="9" AutoPostBack="True" OnTextChanged="PriceList_TextChanged">0</asp:TextBox> </TD><TD vAlign=top align=left colSpan=1><asp:Label id="Label11" runat="server" Width="90px" CssClass="normalFont" Font-Size="Small" Text="Diskon 2" __designer:wfdid="w66"></asp:Label> </TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="Disc2" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w67" MaxLength="9" AutoPostBack="True">0</asp:TextBox> &nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label29" runat="server" Width="78px" CssClass="normalFont" Font-Size="Small" Text="Minim Price" __designer:wfdid="w68"></asp:Label> </TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="bottompriceGrosir" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w69" MaxLength="9" AutoPostBack="True" OnTextChanged="bottompriceGrosir_TextChanged">0</asp:TextBox> <asp:Label id="Label10" runat="server" Width="69px" CssClass="normalFont" Font-Size="Small" Text="Minim Qty" __designer:wfdid="w70"></asp:Label> <asp:TextBox id="MinimStock" runat="server" Width="74px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w71" MaxLength="9" AutoPostBack="True">0</asp:TextBox> </TD><TD align=left colSpan=1><asp:Label id="Label14" runat="server" Width="60px" CssClass="normalFont" Font-Size="Small" Text="Diskon 3" __designer:wfdid="w72"></asp:Label> </TD><TD align=left colSpan=2><asp:TextBox id="Disc3" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w73" MaxLength="9" AutoPostBack="True" OnTextChanged="PriceList_TextChanged">0</asp:TextBox> &nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Width="91px" CssClass="normalFont" Font-Size="Small" Text="Price Khusus" __designer:wfdid="w74"></asp:Label> </TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="PriceKhusus" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w75" MaxLength="9" AutoPostBack="True">0</asp:TextBox> </TD><TD align=left colSpan=1><asp:Label id="UserName" runat="server" __designer:wfdid="w76" Visible="False"></asp:Label> </TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Width="91px" CssClass="normalFont" Font-Size="Small" Text="Price Silver" __designer:wfdid="w77"></asp:Label> </TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="PriceSilver" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w78" MaxLength="9" AutoPostBack="True">0</asp:TextBox> </TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Label4" runat="server" Width="91px" CssClass="normalFont" Font-Size="Small" Text="Price Gold" __designer:wfdid="w81"></asp:Label></TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="PriceGold" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w82" MaxLength="9" AutoPostBack="True">0</asp:TextBox></TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="102px" CssClass="normalFont" Font-Size="Small" Text="Price Platinum" __designer:wfdid="w79"></asp:Label> </TD><TD style="WIDTH: 228px" vAlign=top align=left colSpan=3><asp:TextBox id="PricePlatinum" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w80" MaxLength="9" AutoPostBack="True">0</asp:TextBox> </TD><TD align=left colSpan=1></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left colSpan=7><asp:Label id="lblupd" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w83"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w84"></asp:Label> on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w85"></asp:Label> </TD></TR><TR><TD align=left colSpan=7><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w86"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=7><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w88" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w89"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <cc1:FilteredTextBoxExtender id="fte20" runat="server" __designer:wfdid="w90" Enabled="True" TargetControlID="PriceList" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="fte13" runat="server" __designer:wfdid="w91" Enabled="True" TargetControlID="bottompriceGrosir" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTEGold" runat="server" __designer:wfdid="w92" Enabled="True" TargetControlID="PriceGold" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTENota" runat="server" __designer:wfdid="w93" Enabled="True" TargetControlID="PriceNota" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="DiscFTE2" runat="server" __designer:wfdid="w94" Enabled="True" TargetControlID="Disc2" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="DiscFTE1" runat="server" __designer:wfdid="w95" Enabled="True" TargetControlID="Disc1" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="DiscFTE3" runat="server" __designer:wfdid="w96" Enabled="True" TargetControlID="Disc3" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTESilver" runat="server" __designer:wfdid="w97" Enabled="True" TargetControlID="PriceSilver" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTEKhusus" runat="server" __designer:wfdid="w98" Enabled="True" TargetControlID="PriceKhusus" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTEStockM" runat="server" __designer:wfdid="w99" Enabled="True" TargetControlID="MinimStock" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> <cc1:FilteredTextBoxExtender id="FTEPlatinum" runat="server" __designer:wfdid="w100" Enabled="True" TargetControlID="PricePlatinum" ValidChars=",.0123456789"></cc1:FilteredTextBoxExtender> &nbsp;&nbsp; </TD></TR><TR><TD style="HEIGHT: 14px" vAlign=top align=left colSpan=5>&nbsp;</TD><TD style="WIDTH: 88px; HEIGHT: 14px" vAlign=top align=left colSpan=1></TD><TD style="HEIGHT: 14px" vAlign=top align=left colSpan=1></TD></TR><!-- end grouping barang --><!-- Dimensi --><!-- end dimensi --></TBODY></TABLE>
</ContentTemplate>
</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3"><HeaderTemplate>
<img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
<strong><span style="font-size: 9pt">List Harga Cabang :.</span></strong>                 
</HeaderTemplate>
<ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:GridView id="GV_Branch" runat="server" Width="100%" CssClass="normalFont" ForeColor="#333333" __designer:wfdid="w180" EnableModelValidation="True" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemoid" GridLines="None">
<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
<Columns>
<asp:BoundField DataField="nomor" HeaderText="No" SortExpression="nomor" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_name" HeaderText="Nama Cabang" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="65%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="Small" Width="65%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricelist" HeaderText="Price List">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="biayaExpedisi" HeaderText="Biaya Expedisi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="pricelist_cbg" HeaderText="Price List Cabang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="expedisi_type" HeaderText="Jalur Expedisi" SortExpression="Expedisi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Tidak Ditemukan !!!"></asp:Label>
                                
</EmptyDataTemplate>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<PagerSettings PageButtonCount="15"></PagerSettings>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<RowStyle BackColor="#FFFBD6" BorderColor="Cyan" ForeColor="#333333"></RowStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w179"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
</cc1:TabPanel>
</ajaxToolkit:TabContainer>&nbsp; 
</contenttemplate>
                </asp:UpdatePanel>&nbsp;
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label><BR /></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="label15" BackgroundCssClass="modalBackground" PopupControlID="Panelvalidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdPanelPrint" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPrint" runat="server" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Barang"></asp:Label><BR /><asp:Label id="lblError" runat="server" CssClass="Important" __designer:wfdid="w224"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 12px; TEXT-ALIGN: center" align=left colSpan=2></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" TargetControlID="btnHidePrint" PopupControlID="pnlPrint" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPrint" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
<asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w81" Visible="False" DefaultButton="BtnFindItem"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" __designer:wfdid="w82" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w83"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w84"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4>Jenis Barang : &nbsp; <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w85"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Button id="BtnFindItem" runat="server" CssClass="btn orange" Text="Searching" __designer:wfdid="w86"></asp:Button> <asp:Button id="BtnViewAll" runat="server" CssClass="btn gray" Text="View All" __designer:wfdid="w87"></asp:Button> </TD></TR><TR><TD class="Label" align=center colSpan=4><asp:Button id="BtnSelectALL" runat="server" CssClass="btn red" Text="Select All" __designer:wfdid="w13"></asp:Button> <asp:Button id="BtnSelectNone" runat="server" CssClass="btn green" Text="Select None" __designer:wfdid="w32"></asp:Button></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w88" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" UseAccessibleHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" __designer:wfdid="w173" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Price Nota"><HeaderTemplate>
<asp:CheckBox id="CbPriceNota" runat="server" Text="Price Nota" __designer:wfdid="w16" AutoPostBack="true" Checked="<%# GetSessionCheckPriceNota() %>" OnCheckedChanged="CbPriceNota_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:TextBox id="Nota" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("PriceNota") %>' __designer:wfdid="w2" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTENota" runat="server" __designer:wfdid="w3" ValidChars="1234567890.," TargetControlID="Nota"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Normal"><HeaderTemplate>
<asp:CheckBox id="CbPriceNormal" runat="server" Text="Normal" __designer:wfdid="w22" AutoPostBack="true" Checked="<%# GetSessionCheckPriceNormal() %>" OnCheckedChanged="CbPriceNormal_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:TextBox id="Normal" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("PriceNormal") %>' __designer:wfdid="w4" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTENormal" runat="server" __designer:wfdid="w5" ValidChars="1234567890.," TargetControlID="Normal"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Minim"><HeaderTemplate>
<asp:CheckBox id="CbPriceMinim" runat="server" Text="Minim" __designer:wfdid="w29" AutoPostBack="true" Checked="<%# GetSessionCheckPriceMinim() %>" OnCheckedChanged="CbPriceMinim_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:TextBox id="Khusus" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("minimprice") %>' __designer:wfdid="w167" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEKhusus" runat="server" __designer:wfdid="w172" TargetControlID="Khusus" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Khusus"><HeaderTemplate>
<asp:CheckBox id="CbPriceKhusus" runat="server" Text="Khusus" __designer:wfdid="w176" AutoPostBack="true" Checked="<%# GetSessionCheckPriceKhusus() %>" OnCheckedChanged="CbPriceKhusus_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:TextBox id="Minim" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("pricekhusus") %>' __designer:wfdid="w174" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEMinim" runat="server" __designer:wfdid="w175" ValidChars="1234567890.," TargetControlID="Minim"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="laststock" HeaderText="Last Stock">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Update Price"><ItemTemplate>
<asp:CheckBox id="chkPrice" runat="server" __designer:wfdid="w10" Checked='<%# eval("checkprice") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=4><asp:Button id="BtnSaveItem" runat="server" CssClass="btn green" Text="Save" __designer:wfdid="w89"></asp:Button> <asp:Button id="BtnClose" runat="server" CssClass="btn gray" Text="Close" __designer:wfdid="w90"></asp:Button></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w91" TargetControlID="btnHideListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" __designer:wfdid="w92" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
</asp:Content>
