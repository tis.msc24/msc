<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmPenerimaanService.aspx.vb" Inherits="ReportForm_frmReportPenerimaanBarang" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Navy" Text=".: Laporan Penerimaan Service"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-color: transparent" valign="center">
    <table width="100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=left>Cabang</TD><TD>:</TD><TD align=left><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left>Type TTS</TD><TD>:</TD><TD align=left><asp:DropDownList id="saldoawal" runat="server" Width="90px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="N">Service</asp:ListItem>
<asp:ListItem Value="M" Enabled="False">Mutasi</asp:ListItem>
<asp:ListItem Value="Y" Enabled="False">Saldo Awal</asp:ListItem>
</asp:DropDownList></TD></TR>
    <tr>
        <td align="left">
            Type Service</td>
        <td>
        </td>
        <td align="left">
            <asp:DropDownList id="DDLGaransi" runat="server" Width="90px" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>Servis</asp:ListItem>
                <asp:ListItem>Garansi</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <TR><TD align=left>Filter</TD><TD>:</TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText"><asp:ListItem Value="r.reqcode">No. Tanda Masuk</asp:ListItem>
<asp:ListItem Value="c.custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="176px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD align=left>Periode</TD><TD>:</TD><TD align=left><asp:TextBox id="txtTgl1" runat="server" Width="72px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="periode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to <asp:TextBox id="txtTgl2" runat="server" Width="72px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="periode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" CssClass="inpText"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD id="TD3" runat="server" Visible="false"><asp:RadioButtonList id="cbstatus" runat="server" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD><TD id="TD1" runat="server" Visible="false">:</TD><TD id="TD2" align=left runat="server" Visible="false"><asp:DropDownList id="StatusDDL" runat="server" Width="104px" CssClass="inpText"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In</asp:ListItem>
<asp:ListItem>Out</asp:ListItem>
<asp:ListItem>Receive</asp:ListItem>
<asp:ListItem>Check</asp:ListItem>
<asp:ListItem>CheckOut</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Ready</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
<asp:ListItem>SPK</asp:ListItem>
<asp:ListItem>SPKClose</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnView" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTgl2" PopupButtonID="periode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTgl1" PopupButtonID="periode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="txtTgl1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="txtTgl2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CRVPenerimaanBarang" runat="server" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer> <asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TBODY><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" TargetControlID="btnMsg" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" DropShadow="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>&nbsp;&nbsp; <asp:Button id="btnMsg" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></td>
        </tr>
    </table>
            </th>
        </tr>
    </table>
</asp:Content>

