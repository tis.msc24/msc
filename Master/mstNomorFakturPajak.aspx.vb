Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class mstNomorFakturPajak
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"
    Public Sub FillTextBox(ByVal oidPajak As String)

        sSql = "SELECT mstfakturoid,mstfakturno, left(prefix,3) prefix1,RIGHT(prefix,2) prefix2, mstfakturdate,mstfakturnoawal noAwal,mstfakturnoakhir noAkhir,mstfakturnote note,mstfakturstatus Active, crtuser, crttime, upduser, updtime,minimaldate  " & _
                "FROM QL_mstfakturpajak where MSTFAKTUROID = '" & oidPajak & "'"

        Dim dtData As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnprodresult")
        If dtData.Rows.Count > 0 Then
            oid.Text = dtData.Rows(0)("mstfakturoid").ToString
            txtTglpjk.Text = Format(dtData.Rows(0)("mstfakturdate"), "dd/MM/yyyy")
            tbNoDoc.Text = dtData.Rows(0)("mstfakturno").ToString
            DDLstatus1.Text = dtData.Rows(0)("ACTIVE").ToString
            lblTahunPajak.Text = txtTglpjk.Text.Substring(2, 2)
            TextBox2.Text = dtData.Rows(0)("prefix1").ToString
            TextBox3.Text = dtData.Rows(0)("prefix2").ToString
            TextBox4.Text = dtData.Rows(0)("noAwal").ToString
            TextBox5.Text = dtData.Rows(0)("noAkhir").ToString
            ddlStatus.SelectedValue = dtData.Rows(0)("Active").ToString
            txtNote.Text = dtData.Rows(0)("note").ToString
            Updtime.Text = Format(dtData.Rows(0)("updtime"), "dd/MMyyyy")
            Upduser.Text = dtData.Rows(0)("upduser").ToString
            minimaldate.Text = Format(dtData.Rows(0)("minimaldate"), "dd/MM/yyyy")


            sSql = "select dtlfakturoid,mstfakturoid,dltfakturseq seq,dtlfakturnofaktur GenerateNumber,dtlfakturstatus status,dtlfakturrefno,dtlfakturreftype from QL_mstfakturpajakdtl where mstfakturoid='" & Session("oid") & "'"

            Dim mysqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDS As New DataSet
            mysqlDA.Fill(objDS, "ssql3")
            GVgenerate.DataSource = objDS.Tables("ssql3")
            GVgenerate.DataBind()
            Session("generate") = objDS.Tables("ssql3")

            lblNomorSekarang.Text = "Generated <B>" & objDS.Tables("ssql3").Rows.Count & "</B> nomor faktur pajak."
        End If
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        sSql = "select DISTINCT K.mstfakturoid,mstfakturno ,mstfakturdate date,mstfakturnoawal awal,mstfakturnoakhir akhir, mstfakturnote note,mstfakturstatus status from QL_mstfakturpajak K inner join QL_mstfakturpajakdtl l on k.mstfakturoid=l.mstfakturoid where K.cmpcode='" & CompnyCode & "' "

        sSql = sSql & IIf(ddlFilterStatus.SelectedValue.ToLower = "all", "", " and mstfakturstatus = '" & ddlFilterStatus.SelectedValue & "'")
        sSql = sSql & IIf(txtFilternoPajak.Text = "", "", " and mstfakturno like '%" & txtFilternoPajak.Text & "%'")
        sSql = sSql & " and mstfakturdate between '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy") & "' and '" & Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy") & "'"
        sSql &= " Order by K.mstfakturoid asc"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstcurrdtl.DataSource = objDs.Tables("data")
        GVmstcurrdtl.DataBind()

        For x As Int32 = 0 To GVmstcurrdtl.Rows.Count - 1
            If GVmstcurrdtl.Rows(x).Cells(4).Text = "IN ACTIVE" Then
                GVmstcurrdtl.Rows(x).Cells(4).ForeColor = Drawing.Color.Red
            ElseIf GVmstcurrdtl.Rows(x).Cells(4).Text = "ACTIVE" Then
                GVmstcurrdtl.Rows(x).Cells(4).ForeColor = Drawing.Color.Green
            End If
        Next
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption1.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xSetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xSetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xSetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xSetRole
            Response.Redirect("mstNomorFakturPajak.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Data Nomor Faktur Pajak"
        txtTglpjk.Text = Format(Now, "dd/MM/yyyy")

        If Not Page.IsPostBack Then
            FilterPeriod1.Text = Format(Now.AddDays(-30), "dd/MM/yyyy")
            FilterPeriod2.Text = Format(Now, "dd/MM/yyyy")
            BindData("")

            If Session("oid") IsNot Nothing And Session("oid") <> "" Then
                'kalau listnya di klik
                FillTextBox(Session("oid"))
                If Session("UserID") IsNot Nothing And Session("UserID") <> "" Then
                    If Session("UserID").ToString.ToLower <> "server" Then
                        'btnSave.Visible = False
                        txtTglpjk.CssClass = "inpTextDisabled" : txtTglpjk.Enabled = False
                        TextBox2.CssClass = "inpTextDisabled" : TextBox2.Enabled = False
                        TextBox3.CssClass = "inpTextDisabled" : TextBox3.Enabled = False
                        TextBox4.CssClass = "inpTextDisabled" : TextBox4.Enabled = False
                        TextBox5.CssClass = "inpTextDisabled" : TextBox5.Enabled = False
                        'txtNote.CssClass = "inpTextDisabled" : txtNote.Enabled = False
                    Else
                        TD1.Visible = True
                        TD2.Visible = True
                    End If
                End If
                PanelUpdate.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                ' kalau buat baru
                TabContainer1.ActiveTabIndex = 0
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If Session("generate") Is Nothing Then
            sMsg &= "- Silakan Generate detail Nomor Faktur Pajak"
        Else
            Dim objTable As DataTable = Session("generate")
            If objTable.Rows.Count <= 0 Then
                sMsg &= "- Silakan Generate detail Nomor Faktur Pajak"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning")
            Exit Sub
        End If

        Dim noPajakDtlOid As String = GenerateID("QL_mstfakturpajakdtl", CompnyCode)
        oid.Text = GenerateID("QL_mstfakturpajak", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            minimaldate.Text = txtTglpjk.Text

            'mode insert
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                '    oid.Text = GetStrData("select lastoid +1 from ql_mstoid where tablename ='QL_trnNoPajak'")
                Dim aa, bb, cc As String
                aa = TextBox2.Text
                bb = TextBox3.Text
                cc = aa & " - " & bb

                sSql = "INSERT into QL_mstfakturpajak ([cmpcode],[mstfakturoid], [mstfakturno], [mstfakturdate], [mstfakturnoawal], [mstfakturnoakhir], [mstfakturnote], [mstfakturstatus], [crtuser],[crttime],[upduser],[updtime],[syncflag],[synctime],[prefix],[minimaldate]) VALUES ('" & CompnyCode & "'," & oid.Text & ",'" & tbNoDoc.Text & "','" & CDate(toDate(txtTglpjk.Text)) & "','" & TextBox4.Text & "','" & TextBox5.Text & "','" & txtNote.Text & "','" & DDLstatus1.SelectedValue & "','" & Session("UserID") & "',current_timestamp, '" & Session("UserID") & "' , current_timestamp,'',current_timestamp,'" & cc & "','" & CDate(toDate(minimaldate.Text)) & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not Session("generate") Is Nothing Then
                    Dim objTable As DataTable = Session("generate")
                    For x As Integer = 0 To objTable.Rows.Count - 1

                        sSql = "INSERT into QL_mstfakturpajakdtl ([cmpcode],[dtlfakturoid],[mstfakturoid],[dltfakturseq],[dtlfakturnofaktur],[dtlfakturreftype],[dtlfakturrefno],[dtlfakturnote],[dtlfakturstatus],[crtuser],[crttime],[upduser],[updtime]) VALUES ('" & CompnyCode & "'," & noPajakDtlOid & "," & oid.Text & "," & objTable.Rows(x).Item("SEQ") & ",'" & objTable.Rows(x).Item("GenerateNumber") & "','','', '' ,'" & objTable.Rows(x).Item("STATUS") & "','" & Session("UserID") & "',current_timestamp, '" & Session("UserID") & "' , current_timestamp)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        noPajakDtlOid += 1
                    Next
                End If
            Else 'mode update

                sSql = "UPDATE QL_mstfakturpajak set mstfakturstatus='" & DDLstatus1.SelectedValue & "',mstfakturnote='" & txtNote.Text & "', minimaldate='" & CDate(toDate(minimaldate.Text)) & "' where mstfakturoid = " & Session("oid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            'jika insert data baru, update mstoid
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename = 'QL_mstfakturpajak' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & noPajakDtlOid - 1 & " where tablename = 'QL_mstfakturpajakdtl' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - Error")
            Exit Sub
        End Try
        Session.Remove("oid")
        Response.Redirect("~\Master\mstNomorFakturPajak.aspx?awal=true")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindData("")
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing
        Response.Redirect("~\Master\mstNomorFakturPajak.aspx")
    End Sub

    Protected Sub GENERATE_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If TextBox2.Text = "" Or TextBox3.Text = "" Or TextBox4.Text = "" Or TextBox5.Text = "" Then
            showMessage("Generate harus di isi", " WARNING ")
            Exit Sub
        End If
        If Not Session("oid") Is Nothing Then
            showMessage("Tidah bisa generate lagi!", " WARNING ")
            Exit Sub
        End If

        If iui.Text = "baru" Then
            Dim c, kode, namaItem As String
            Dim d As Integer
            namaItem = "FP-"
            c = ToDouble(GetStrData("SELECT isnull(max(abs(replace(mstfakturno, '" & Tchar(namaItem) & "', ''))),0) FROM QL_MSTFAKTURPAJAK WHERE left(mstfakturno,3) = '" & Tchar(namaItem) & "' "))
            d = CInt(c) + 1
            kode = GenNumberString(namaItem, "", d, 5)
            tbNoDoc.Text = kode
        End If

        'begin
        Dim dt As New DataTable
        dt.Columns.Add("SEQ", Type.GetType("System.Int32"))
        dt.Columns.Add("STATUS", Type.GetType("System.String"))
        'dt.Columns.Add("ITSERVTARIF", Type.GetType("System.String"))
        dt.Columns.Add("GenerateNumber", Type.GetType("System.String"))
        dt.Columns.Add("dtlfakturreftype", Type.GetType("System.String"))
        dt.Columns.Add("dtlfakturrefno", Type.GetType("System.String"))

        Dim objRow As DataRow
        Dim nilaiNo As String = "" 'ganti field table di datatable
        Dim tb4 As Integer = Tnumber(TextBox4.Text)
        Dim tb5 As Integer = Tnumber(TextBox5.Text)
        'Dim awaltb4 As String = Left(TextBox4.Text, 2)
        Dim awaltb5 As String = Left(TextBox5.Text, 2)

        For i As Integer = Tnumber(tb4) To Tnumber(tb5)
            objRow = dt.NewRow()
            nilaiNo = TextBox2.Text & "-" & TextBox3.Text & "." & GenNumberString("", "", i, 8)
            objRow("GenerateNumber") = nilaiNo
            objRow("SEQ") = dt.Rows.Count + 1
            objRow("STATUS") = DDLstatus2.SelectedValue
            dt.Rows.Add(objRow)
        Next

        Session("generate") = dt
        GVgenerate.DataSource = dt
        GVgenerate.Visible = True
        GVgenerate.DataSource = Nothing
        GVgenerate.DataSource = dt
        GVgenerate.DataBind()
        lblNomorSekarang.Text = "Generated <B>" & dt.Rows.Count & "</B> nomor faktur pajak."
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindData("")
    End Sub
#End Region
End Class
