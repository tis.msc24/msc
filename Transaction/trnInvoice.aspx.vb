'Prgmr :4ncIiI<a im03t | LastUpdt:11.23.2012 - MM/dd/YYYY
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.Globalization
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_Invoice
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    Dim folderReport As String = "~/Report/"
    Dim report As ReportDocument
#End Region

#Region "Procedure"
    Private Function getPotongan(ByVal soid As Integer, ByVal price As Decimal, ByVal custoid As Integer) As Decimal
        Dim res As Decimal = 0.0
        Dim modulovar As Integer = 0

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "SELECT COUNT(b.soid) FROM ql_trnrequest a INNER JOIN ql_trninvoice b ON a.reqoid = b.mstreqoid WHERE a.garansi <> 'Garansi' AND a.reqcustoid = " & custoid & " AND (LOWER(b.sstatus) = 'paid' OR LOWER(b.sstatus) = 'invoiced') AND b.soid <> " & soid & " AND b.updtime <= (SELECT updtime FROM ql_trninvoice WHERE soid = " & soid & ")"
            xCmd.CommandText = sSql : Dim result As Integer = xCmd.ExecuteScalar

            If result Mod 6 = 5 Then
                sSql = "SELECT SUM(stotalprice) FROM (SELECT TOP 5 b.stotalprice FROM ql_trnrequest a INNER JOIN ql_trninvoice b ON a.reqoid = b.mstreqoid WHERE a.garansi <> 'Garansi' AND a.reqcustoid = " & custoid & " AND (LOWER(b.sstatus) = 'paid' OR LOWER(b.sstatus) = 'invoiced') AND b.soid <> " & soid & " AND b.updtime <= (SELECT updtime FROM ql_trninvoice WHERE soid = " & soid & ") ORDER BY b.soid desc) x"

                xCmd.CommandText = sSql
                Dim total As Decimal = xCmd.ExecuteScalar
                Dim resdisc As Decimal = price - (total / 5.0)
                If resdisc >= 0 Then
                    res = total / 5.0
                Else
                    res = price
                End If
            End If
        Catch ex As Exception
        End Try
        Return res
    End Function

    Private Function CrtDtlFinal() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("FINOID", Type.GetType("System.Int32"))
        dtab.Columns.Add("trnfinalno", Type.GetType("System.String"))
        dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("ItemoidTTS", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("itemqty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("BiayaService", Type.GetType("System.Decimal")) 
        dtab.Columns.Add("TotalNett", Type.GetType("System.Decimal"))
        dtab.Columns.Add("typetts", Type.GetType("System.String"))
        dtab.Columns.Add("CbgInput", Type.GetType("System.String"))
        dtab.Columns.Add("cabangasal", Type.GetType("System.String"))
        dtab.Columns.Add("reqdtljob", Type.GetType("System.String"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        Session("InvDetail") = dtab
        Return dtab
    End Function

    Private Sub DDLfCabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub initTypeJob()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            ImageButton3.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 2 Then 'Warning
            ImageButton3.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        WARNING.Text = strCaption : lblValidasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnExtender, PanelErrMsg, MPEErrMsg, True)
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String)

    End Sub

    Public Sub GenerateGenID()
        txtPlanOid.Text = GenerateID("QL_trnInvoice", CompnyCode)
    End Sub

    Private Sub BindData(ByVal sWhere As String) 'GV List Awal
        Try
            sSql = "Select si.SOID,si.BRANCH_CODE,si.invoiceno,si.MSTREQOID,co.custname,rqm.reqcode,si.biayaservis,si.amttotalspart,si.potongan,si.amtnetto,si.CREATETIME,si.CREATEUSER,si.UPDTIME,si.UPDUSER,si.SSTATUS,cONVERT(VARCHAR(20),SSTARTTIME,103) SSTARTTIME,rqm.reqcustoid From QL_TRNINVOICE si Inner Join QL_mstcust co ON co.custoid=si.custoid Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=si.MSTREQOID AND rqm.reqcustoid=co.custoid Where si.CMPCODE='" & CompnyCode & "' " & sWhere & " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
            If fCabang.SelectedValue <> "ALL" Then
                sSql &= " AND si.BRANCH_CODE='" & fCabang.SelectedValue & "'"
            End If

            sSql &= " Order BY si.SSTARTTIME Desc"

            Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "ChkInvoice")
            GVListInvoice.DataSource = xTableItem1
            GVListInvoice.DataBind() : GVListInvoice.SelectedIndex = -1
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataInfo(ByVal sWhere As String) 'Binding Nota Beli
        Try
            sSql = "Select rqm.branch_code CodeReq, cr.gendesc DescCbInp, Convert(VarChar(20),rqm.reqdate,103) reqdate, rqm.reqoid, rqm.reqcode, co.custoid, co.custcode, co.custname, co.phone1, co.custaddr, reqstatus, con.mtrlocoid, SUM(Qtyin)-SUM(QtyOut) OsQty, SUM(Qtyin) Qtyin, SUM(QtyOut) QtyOut, (Select SUM(rq.reqqty) From QL_TRNREQUESTDTL rq Where rq.reqmstoid=rqm.reqoid) ReqQty, con.mtrlocoid MtrWhOid, gc.gendesc GudangNya From QL_TRNREQUEST rqm Inner JOin QL_mstcust co ON co.custoid=rqm.reqcustoid Inner Join QL_mstgen cr ON cr.gencode=rqm.branch_code AND cr.gengroup='CABANG' Inner Join (Select MSTREQOID,con.mtrlocoid,con.branch_code, (qtyIn) Qtyin, (qtyOut) QtyOut From QL_conmtr con Inner Join QL_TRNINVOICE inv ON con.formoid=inv.SOID Where con.Formname='QL_TRNINVOICEITEM' AND con.mtrlocoid IN (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='TITIPAN' AND c.gencode='" & DdlCabang.SelectedValue & "') UNION ALL Select rqd.reqmstoid MSTREQOID, con.mtrlocoid, con.branch_code, (qtyIn) QtyIn,(qtyOut) QtyOut From QL_conmtr con Inner Join QL_TRNREQUESTDTL rqd ON con.conrefoid=rqd.reqdtloid Where con.Formname<>'QL_TRNINVOICEITEM' AND con.mtrlocoid in (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='TITIPAN' AND c.gencode='" & DdlCabang.SelectedValue & "')) con ON rqm.reqoid=con.MSTREQOID Inner Join QL_mstgen cf ON cf.gencode=con.branch_code AND cr.gengroup='CABANG' Inner Join QL_mstgen gc ON gc.genoid=con.mtrlocoid AND gc.gengroup='LOCATION' Where con.mtrlocoid in (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.genother5='TITIPAN' AND c.gencode=con.branch_code AND con.BRANCH_CODE='" & DdlCabang.SelectedValue & "' AND (rqm.reqcode LIKE '%" & Tchar(txtNo.Text) & "%' Or co.custname LIKE '%" & Tchar(txtNo.Text) & "%') AND rqm.reqoid IN (Select MSTREQOID from QL_TRNFINAL fm Where MSTREQOID=rqm.reqoid AND FINSTATUS IN ('APPROVED','POST'))) Group BY rqm.branch_code, cr.gendesc, Convert(VarChar(20),rqm.reqdate,103), rqm.reqoid, rqm.reqcode, co.custoid, co.custcode, co.custname, co.phone1, co.custaddr, reqstatus, con.mtrlocoid, gc.gendesc Having SUM(Qtyin)-SUM(QtyOut)>0.00"
            Dim xTableItem2 As DataTable = cKoneksi.ambiltabel(sSql, "Info")
            GVListInfo.DataSource = xTableItem2
            GVListInfo.DataBind() : GVListInfo.SelectedIndex = -1
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub ClearInfo()
        txtNo.Text = "" : AmtDisc.Text = "" : txtNamaCust.Text = ""
        txtAlamatCust.Text = "" : txtHPCust.Text = "" : txtStatus.Text = ""
        txtHargaServis.Text = "" : txtHargaJob.Text = "" : txtHargaSPart.Text = ""
        ReqOid.Text = "" : DdlCabang.Enabled = True : GVListInfo.Visible = False
        Session("InvDetail") = Nothing : GvInvDtl.Visible = False
    End Sub

    Private Sub HitungUlang()
        If ToDouble(AmtDisc.Text) > ToDouble(txtHargaServis.Text) Then
            showMessage("- Maaf, Nilai potongan lebih besar dari nilai total biaya..!!!<BR>", 2)
            Exit Sub
        End If
        txtJumlah.Text = ToMaskEdit(ToDouble(txtHargaServis.Text) - ToDouble(AmtDisc.Text), 3)
    End Sub

    Private Sub Checkedfinal()
        If Session("BindFinal") IsNot Nothing Then
            Dim dt As DataTable = Session("BindFinal")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVfinal.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVfinal.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "FINOID=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("BindFinal") = dt
        End If
    End Sub

    Private Sub BindFinal()
        sSql = "SELECT rd.reqdtloid,i.itemoid,i.itemdesc,CASE typetts WHEN 'SERVICE' THEN itemqty ELSE (CASE WHEN itempartoid=0 THEN itemreqoid ELSE itempartqty END) END qtyfinal,fm.trnfinalno,fd.trnfinaloid,fd.trnifinaldtloid,rd.reqdtljob,rd.snno,rd.reqdtljob,fd.locoidtitipan FROM QL_TRNREQUESTDTL rd INNER JOIN QL_TRNFINALSPART fd ON rd.reqdtloid=fd.reqdtloid INNER JOIN QL_TRNFINAL fm ON fm.MSTREQOID=rd.reqmstoid AND fd.trnfinaloid=fm.FINOID AND fm.FINITEMSTATUS IN ('APPROVED','POST') INNER JOIN QL_mstitem i ON i.itemoid=CASE typetts WHEN 'SERVICE' THEN itemreqoid ELSE (CASE WHEN itempartoid=0 THEN itemreqoid ELSE itempartoid END) END WHERE(fd.trnfinaloid = 2229)"
        Try
            If ReqOid.Text = "" Then
                lblState.Text = "TRUE" : lblState.Visible = False
                showMessage("- Maaf, Anda belum pilih nomer penerimaan..!!<br>", 2)
                Exit Sub
            End If
            sSql = "Select ROW_NUMBER() OVER (ORDER BY finoid ASC) AS Seq,checkvalue,FINOID,trnfinalno,BiayaService,SUM(TotalNett) Totalspart,(BiayaService)+SUM(TotalNett) NettFinal,typetts,MSTREQOID,locoidtitipan From (Select 'False' AS checkvalue,FINOID,fm.trnfinalno,fm.typetts,SUM(fd.itempartprice*fd.itempartqty) TotalNett,fm.priceservis BiayaService,fm.MSTREQOID,fm.locoidtitipan From QL_TRNFINAL fm Inner Join QL_TRNFINALSPART fd ON fd.trnfinaloid=fm.FINOID Inner Join QL_mstitem i ON i.itemoid=fd.itemreqoid Inner Join QL_mstgen cbi ON cbi.gencode=fm.BRANCH_CODE And cbi.gengroup='CABANG' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal And cba.gengroup='cabang' Where fd.reqoid=" & Integer.Parse(ReqOid.Text) & " AND FINSTATUS IN ('APPROVED','POST') AND FINITEMSTATUS <>'INVOICED' Group By FINOID,fm.trnfinalno,fm.typetts,fm.priceservis,fm.MSTREQOID,fm.locoidtitipan) fi Group By FINOID,trnfinalno,BiayaService,typetts,checkvalue,MSTREQOID,locoidtitipan"
            Dim df As DataTable = cKoneksi.ambiltabel(sSql, "QL_ReqDtl")
            GVfinal.DataSource = df : GVfinal.DataBind()
            Session("BindFinal") = df : Session("BindFinals") = Session("BindFinal")
            GVfinal.SelectedIndex = -1 : GVfinal.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub generateNo()
        Try
            Dim type As String = "" : Dim kode As String = ""

            sSql = "SELECT garansi FROM ql_trnrequest WHERE reqoid = " & Integer.Parse(ReqOid.Text) & ""
            xCmd.CommandText = sSql
            Dim garansi As String = xCmd.ExecuteScalar
            If garansi = "Garansi" Then
                kode = "2"
            Else
                kode = "1"
            End If

            Dim sNo As String = "IV" & kode & "." & Format(CDate(toDate(txtTglTarget.Text)), "yy") & "." & Format(CDate(toDate(txtTglTarget.Text)), "MM") & ".I."
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(invoiceno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trninvoice WHERE cmpcode='" & CompnyCode & "' AND invoiceno LIKE '" & sNo & "%'"
            noinvo.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        Catch ex As Exception
            noinvo.Text = "IV2." & Format(CDate(toDate(txtTglTarget.Text)), "yy") & "." & Format(CDate(toDate(txtTglTarget.Text)), "MM") & ".J.0001"
        End Try
    End Sub

    Private Sub GenerateReqCode()
        sSql = " Select (lastoid+1) from QL_mstoid where tablename like '%QL_TRNINVOICE%' and cmpcode like '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim Code As String = xCmd.ExecuteScalar
        noinvo.Text = Code
        conn.Close()
    End Sub

    Private Sub FillTextinfo(ByVal idPage As Integer)
        Try
            sSql = "SELECT ROW_NUMBER() OVER (ORDER BY reqdtljob ASC) AS Seq,rqd.reqmstoid ReqOid,rqd.reqdtloid,fd.itemreqoid,ir.itemdesc ItemReqDesc,rqd.snno,rqd.kelengkapan,rqd.reqdtljob,rqd.reqqty,rqd.typegaransi,ru.genoid UniOidReq,ru.gendesc UnitReqDesc,fd.itempartoid,ip.itemdesc DescPart,fd.itempartunitoid,pu.gendesc,fd.itempartqty QtyPart,fd.itempartprice PricePart,fd.itemtotalprice TotalPricePart,(Select Distinct typetts from QL_TRNFINAL fm Where fm.FINOID=fd.trnfinaloid AND fm.BRANCH_CODE=fd.branch_code) TypeTTs From QL_TRNREQUESTDTL rqd Inner Join QL_TRNREQUEST rqm On rqm.reqoid=rqd.reqmstoid AND rqm.branch_code=rqd.branch_code Inner Join QL_mstitem ir ON ir.itemoid=rqd.itemoid Inner Join QL_mstgen ru ON ru.genoid=ir.satuan1 AND ru.gengroup='Itemunit' Inner Join QL_TRNFINALSPART fd ON fd.reqoid=rqd.reqmstoid AND rqd.itemoid=fd.itemreqoid Inner JOin ql_mstitem ip ON ip.itemoid=fd.itempartoid Inner Join QL_mstgen pu ON pu.genoid=fd.itempartunitoid AND pu.gengroup='Itemunit' Where rqm.reqoid=" & idPage & ""
            Dim sd As DataTable = cKoneksi.ambiltabel(sSql, "QL_ReqDtl")
            GvInvDtl.DataSource = sd : GvInvDtl.DataBind()
            Session("ReqInvDtl") = sd : GvInvDtl.SelectedIndex = -1
            GvInvDtl.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub showprint(ByVal sOid As Integer)
        report = New ReportDocument
        report.Load(Server.MapPath(folderReport & "printnotainvoice.rpt"))
        Dim printID As Integer
        If Session("no") = 0 Then
            Session("no") = soid
        End If
        printID = Session("oid")
        report.SetParameterValue("sWhere", sOid)
        report.SetParameterValue("SalesName", Session("UserID"))

        CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Session("no") & "_" & Format(GetServerTime(), "dd_MM_yy"))
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT invoiceno FROM ql_trninvoice WHERE soid = " & sOid & ""
            xCmd.CommandText = sSql
            Dim res As String = xCmd.ExecuteScalar

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "PrintNotaInvoice.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", " AND QLS.cmpcode = '" & CompnyCode & "' AND QLS.soid = " & sOid & "")
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sOid & "Nota_Invoice" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub HitungNet(ByVal OidFIn As String)

        Dim tr As DataTable = Session("InvDetail")
        Dim TotalSpart, bServis As Double
        If tr.Rows.Count > 0 Then
            For c1 As Int32 = 0 To tr.Rows.Count - 1
                TotalSpart += ToDouble(tr.Rows(c1)("TotalNett"))
            Next
        End If

        sSql = "Select FINOID,priceservis,SUM(TotalNett) TotalNett From (Select fm.FINOID,SUM(fd.itempartprice*fd.itempartqty) TotalNett,fm.priceservis From QL_TRNFINAL fm Inner Join QL_TRNFINALSPART fd ON fd.trnfinaloid=fm.FINOID Where fd.trnfinaloid IN (" & OidFIn.ToString.Trim & ") Group By fm.finoid ,fm.FINOID,fm.priceservis) fv Group By FINOID,priceservis"
        Dim tb As DataTable = cKoneksi.ambiltabel2(sSql, "QL_FinalSum")
        For C2 As Int32 = 0 To tb.Rows.Count - 1
            bServis += ToDouble(tb.Rows(C2)("priceservis"))
        Next

        txtHargaJob.Text = ToMaskEdit(ToDouble(bServis), 3)
        txtHargaSPart.Text = ToMaskEdit(ToDouble(TotalSpart), 3)
        txtHargaServis.Text = ToMaskEdit(bServis + TotalSpart, 3)
        txtJumlah.Text = ToMaskEdit(txtHargaServis.Text - AmtDisc.Text, 3)
    End Sub

    Private Sub FillTextBox(ByVal idPage As Integer) ' No transaksi for pilihan master fill text box
        Try
            sSql = "Select im.BRANCH_CODE,im.SOID,im.MSTREQOID,im.invoiceno,im.SSTARTTIME TglInv,rqm.reqcode,cuk.custoid,cuk.custname,cuk.custaddr,cuk.phone1,im.biayaservis,im.amttotalspart,im.potongan,im.amtnetto ,im.SSTATUS,im.UPDTIME,im.UPDUSER,im.totalbiaya,im.createtime,im.locoidtitipan From QL_TRNINVOICE im Inner Join QL_mstcust cuk ON cuk.custoid=im.custoid /*And cuk.branch_code=im.BRANCH_CODE*/ Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=im.MSTREQOID Where im.SOID=" & idPage & ""
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xReader = xCmd.ExecuteReader
            If xReader.HasRows Then
                While xReader.Read
                    DdlCabang.SelectedValue = xReader("branch_code").ToString.Trim
                    txtPlanOid.Text = Integer.Parse(idPage)
                    lblcustoid.Text = Integer.Parse(xReader("custoid"))
                    ReqOid.Text = Integer.Parse(xReader("MSTREQOID"))
                    noinvo.Text = xReader("invoiceno").ToString
                    txtNo.Text = xReader("reqcode").ToString
                    txtTglTarget.Text = Format(GetServerTime(), "dd/MM/yyyy")
                    txtNamaCust.Text = xReader("custname").ToString.Trim
                    txtAlamatCust.Text = xReader("custaddr").ToString.Trim
                    txtHPCust.Text = xReader("phone1").ToString.Trim
                    txtStatus.Text = xReader("SSTATUS").ToString.Trim
                    txtHargaJob.Text = ToMaskEdit(xReader("biayaservis"), 3)
                    txtHargaSPart.Text = ToMaskEdit(xReader("amttotalspart"), 3)
                    txtHargaServis.Text = ToMaskEdit(xReader("totalbiaya"), 3)
                    AmtDisc.Text = ToMaskEdit(xReader("potongan"), 3)
                    txtJumlah.Text = ToMaskEdit(xReader("amtnetto"), 3)
                    updUser.Text = xReader("UPDUSER").ToString.Trim
                    updTime.Text = Format(xReader("UPDTIME"), "dd/MM/yyyy HH:mm:ss")
                    createtime.Text = Format(xReader("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
                    mtrwhoid.Text = Integer.Parse(xReader("locoidtitipan"))
                End While
            End If
            xReader.Close()
            conn.Close()
            If txtStatus.Text = "POST" Then
                btnSave.Visible = False : btnReady.Visible = False
                btnBatal.Visible = False : btnprint.Visible = True
            Else
                btnSave.Visible = True : btnReady.Visible = True
                btnBatal.Visible = True : btnprint.Visible = False
            End If

            Dim dt As Double = GetScalar("Select Count(fm.FINOID) From QL_TRNINVOICEITEM id Inner Join QL_TRNFINAL fm ON id.finoid=fm.FINOID AND fm.cabangasal=id.cabangasal Inner Join QL_mstitem i ON i.itemoid=id.itemreqoid Where id.soid=" & idPage & "")

            If dt > 0 Then
                sSql = "Select ROW_NUMBER() OVER (ORDER BY fm.finoid ASC) AS Seq,fm.FINOID,fm.trnfinalno,i.itemoid,i.itemcode,i.itemdesc,id.itemqty,fm.priceservis BiayaService,id.amttotalspartdtl TotalNett,fm.typetts,fm.BRANCH_CODE CbgInput,fm.cabangasal,cbi.gendesc DeckCbInp,id.ItemoidTTS,id.finoid,'' reqdtljob,id.reqdtloid From QL_TRNINVOICEITEM id Inner Join QL_TRNFINAL fm ON id.finoid=fm.FINOID AND fm.cabangasal=id.cabangasal Inner Join QL_mstitem i ON i.itemoid=id.itemreqoid Inner Join QL_mstgen cbi ON cbi.gencode=fm.BRANCH_CODE And cbi.gengroup='CABANG' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal And cba.gengroup='cabang' Where id.soid=" & idPage & " AND MSTREQOID=" & Integer.Parse(ReqOid.Text) & " Group BY fm.FINOID,fm.trnfinalno,i.itemoid,i.itemcode,i.itemdesc,id.itemqty,fm.priceservis,id.amttotalspartdtl ,fm.typetts,fm.BRANCH_CODE,fm.cabangasal,cbi.gendesc,id.ItemoidTTS,id.finoid,fm.MSTREQOID,soiddtl,id.reqdtloid"
            Else
                sSql = "Select ROW_NUMBER() OVER (ORDER BY fm.finoid ASC) AS Seq,fm.FINOID,fm.trnfinalno,0 itemoid,'' itemcode,'' itemdesc,0.00 itemqty, fm.priceservis BiayaService,fm.priceservis+Isnull((Select SUM(fd.itemtotalprice) From QL_TRNFINALSPART fd Where fd.trnfinaloid=fm.FINOID AND fm.BRANCH_CODE=fd.branch_code),0.00) TotalNett,fm.typetts,fm.BRANCH_CODE CbgInput,fm.cabangasal,cbi.gendesc DeckCbInp,0 itemoidtts,'' reqdtljob,id.reqdtloid From QL_TRNINVOICEITEM id Inner Join QL_TRNFINAL fm ON id.finoid=fm.FINOID AND fm.cabangasal=id.cabangasal Inner Join QL_mstgen cbi ON cbi.gencode=fm.BRANCH_CODE And cbi.gengroup='CABANG' INNER Join QL_mstgen cba ON cba.gencode=fm.cabangasal And cba.gengroup='cabang' Where id.soid=" & idPage & " AND MSTREQOID=" & Integer.Parse(ReqOid.Text) & ""
            End If

            Dim dg As DataTable = cKoneksi.ambiltabel(sSql, "QL_InvDetail")
            GvInvDtl.DataSource = dg : GvInvDtl.DataBind()
            Session("InvDetail") = dg : GvInvDtl.SelectedIndex = -1
            GvInvDtl.Visible = True : Dim OidFIn As String = ""
            If dg.Rows.Count > 0 Then
                For C1 As Integer = 0 To dg.Rows.Count - 1
                    OidFIn &= "'" & dg.Rows(C1)("FINOID").ToString & "',"
                Next
                OidFIn = Left(OidFIn, OidFIn.Length - 1)
            Else
                OidFIn = "0"
            End If
            HitungNet(OidFIn)
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("trnInvoice.aspx")
        End If

        Session("idPage") = Request.QueryString("idPage")
        btnReady.Attributes.Add("OnClick", "javascript:return confirm('Data yang sudah diposting tidak bisa diupdate lagi, \n Anda yakin ingin melanjutkan ?');")
        btnBatal.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin ingin menghapus data ini ?');")

        Page.Title = CompnyName & " - Invoice Service"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            DDLfCabang() : initTypeJob() : txti_u.Text = "New"
            txtPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData("")
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                btnSearch.Visible = True : btnErase.Visible = True
                GenerateReqCode()
                txtTglTarget.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TglTgl.Text = Format(GetServerTime(), "dd/MM/yyyy")
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
            ElseIf Session("idPage") <> Nothing And Session("idPage") <> "" Then
                btnSearch.Visible = False : btnErase.Visible = False
                txti_u.Text = "Update"
                FillTextBox(Session("idPage"))
                TglTgl.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 1
                'btnBatal.Visible = True : btnReady.Visible = True

                If txtStatus.Text = "Final" Then
                    btnReady.Visible = True : btnBatal.Visible = True
                    btnSave.Visible = True :   btnprint.Visible = False
                ElseIf txtStatus.Text = "Invoiced" Then
                    btnReady.Visible = False : btnBatal.Visible = False
                    btnSave.Visible = False :  btnprint.Visible = True
                ElseIf txtStatus.Text = "Paid" Then
                    btnReady.Visible = False : btnBatal.Visible = False
                    btnSave.Visible = False : btnprint.Visible = True
                ElseIf txtStatus.Text.ToUpper = "CLOSED" Then
                    btnBatal.Visible = False : btnReady.Visible = False
                    btnReady.Visible = False : btnSave.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click

        Dim sWhere As String = ""

        If cbPeriod.Checked Then
            Try
                Dim d As DateTime
                Dim errMessage As String = ""
                If txtPeriod1.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal awal !] ! <br>"
                End If

                If txtPeriod2.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal akhir !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod1.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal awal salah !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod2.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal akhir salah !] ! <br>"
                End If

                If errMessage <> "" Then
                    showMessage(errMessage, 2)
                    Exit Sub
                End If

                txtPeriod1.Enabled = True : txtPeriod2.Enabled = True
                Dim date1 As Date = toDate(txtPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)

                If txtPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    errMessage &= "- Maaf, Tolong isi periode...!!<br>"
                End If

                If date1 < toDate("01/01/1900") Then
                    errMessage &= "- Maaf, Tanggal awal salah !"
                End If

                If date2 < toDate("01/01/1900") Then
                    errMessage &= "- Maaf, Tanggal akhir salah..!!<br>"
                End If

                If date1 > date2 Then
                    errMessage &= "- Maaf, Period 1 harus lebih kecil dari Period 2..!!<br>"
                    txtPeriod1.Text = "" : txtPeriod2.Text = ""
                End If

                sWhere &= " AND CONVERT(CHAR(10),si.SSTARTTIME,101) BETWEEN '" & toDate(txtPeriod1.Text) & "' AND '" & toDate(txtPeriod2.Text) & "'"

                If errMessage <> "" Then
                    showMessage(errMessage, 2)
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Maaf, Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If

        If cbStatus.Checked Then
            If ddlStatus.SelectedValue.ToLower <> "all" Then
                sWhere &= " AND si.SSTATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
            End If
        End If
        BindData(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = "" : ddlFilter.SelectedIndex = 0
        cbPeriod.Checked = True
        txtPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = False : ddlStatus.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub GVListInvoice_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GVListInvoice_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListInvoice.PageIndexChanging
        GVListInvoice.PageIndex = e.NewPageIndex

        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbPeriod.Checked Then

            Try
                Dim d As DateTime : Dim errMessage As String = ""
                If txtPeriod1.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal awal !] ! <br>"
                End If

                If txtPeriod2.Text = "" Then
                    errMessage &= "- [Tolong isi tanggal akhir !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod1.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal awal salah !] ! <br>"
                End If

                If Not DateTime.TryParseExact(txtPeriod2.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
                    errMessage &= "- [Format tanggal akhir salah !] ! <br>"
                End If

                If errMessage <> "" Then
                    showMessage(errMessage, 2)
                    Exit Sub
                End If

                txtPeriod1.Enabled = True : txtPeriod2.Enabled = True
                Dim date1 As Date = toDate(txtPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If txtPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    showMessage("Tolong isi periode  !", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal awal salah !", 2) : Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal akhir salah !", 2) : Exit Sub
                End If
                If date1 > date2 Then
                    showMessage("Period 1 harus lebih kecil dari Period 2 !", 2)
                    txtPeriod1.Text = ""
                    txtPeriod2.Text = ""
                    Exit Sub
                End If
                sWhere &= " AND CONVERT(CHAR(10),si.SSTARTTIME,101) BETWEEN '" & toDate(txtPeriod1.Text) & "' AND '" & toDate(txtPeriod2.Text) & "'"
            Catch ex As Exception
                showMessage("Tolong isi tanggal awal dan akhir !", 2)
            End Try
        End If
        If cbStatus.Checked Then
            If ddlStatus.SelectedValue.ToLower <> "all" Then
                sWhere &= " AND QLA.SSTATUS LIKE '%" & ddlStatus.SelectedValue & "%'"
            End If
        End If
        BindData(sWhere) 
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindDataInfo("") : GVListInfo.Visible = True
    End Sub

    Protected Sub GVListInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVListInfo.PageIndexChanging
        GVListInfo.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere = " AND QLR.REQSTATUS = 'FINAL' and qlr.reqflag = 'In'"
        BindDataInfo(sWhere) 
    End Sub

    Protected Sub GVListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVListInfo.SelectedIndexChanged
        ReqOid.Text = Integer.Parse(GVListInfo.SelectedDataKey.Item("reqoid"))
        txtNo.Text = GVListInfo.SelectedDataKey.Item("reqcode").ToString
        lblcustoid.Text = Integer.Parse(GVListInfo.SelectedDataKey.Item("custoid"))
        txtNamaCust.Text = GVListInfo.SelectedDataKey.Item("custname").ToString
        txtAlamatCust.Text = GVListInfo.SelectedDataKey.Item("custaddr").ToString
        txtHPCust.Text = GVListInfo.SelectedDataKey.Item("phone1").ToString
        mtrwhoid.Text = Integer.Parse(GVListInfo.SelectedDataKey.Item("mtrwhoid"))

        txtHargaJob.Text = ToMaskEdit(0, 3)
        txtHargaSPart.Text = ToMaskEdit(0, 3)
        txtHargaServis.Text = ToMaskEdit(0, 3)
        AmtDisc.Text = ToMaskEdit(0, 3)
        GVListInfo.Visible = False
        DdlCabang.Enabled = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\Transaction\trnInvoice.aspx?page=true")
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        ClearInfo()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim lasthpp As Double, itempartoid As Double = 0, errMessage As String = "", d As DateTime, QtyTotal As Double = 0 : Dim TotalHpp As Double = 0, TotalPart As Double = 0, FinOId As String = "", Period As String = GetDateToPeriodAcctg(GetServerTime()), dtl As DataTable = Session("InvDetail")

        If ReqOid.Text = "" Then
            errMessage &= "- Maaf, Tolong pilih No. Tanda Terima(TTS)..!!<BR>"
        End If

        If txtTglTarget.Text = "" Then
            errMessage &= "- Maaf, Tanggal Invoice tidak boleh kosong..!!<BR>"
        End If

        If Session("InvDetail") Is Nothing Then
            errMessage &= "-  Maaf, Tidak ada detail data<br>- Tolong cek datanya..!!<br>"
        Else
            If dtl.Rows.Count <= 0 Then
                errMessage &= "-  Maaf, Tidak ada detail data<br>- Tolong cek datanya..!!<br>"
            End If
        End If

        If Not DateTime.TryParseExact(txtTglTarget.Text, "dd/MM/yyyy", Nothing, System.Globalization.DateTimeStyles.None, d) Then
            errMessage &= "- Maaf, Format tanggal salah, Gunakan format dd/MM/yyyy..!!<br>"
        End If

        If Session("idPage") = Nothing Or Session("idPage") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM [QL_TRNINVOICE] WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                errMessage &= "- Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT [SSTATUS] FROM [QL_TRNINVOICE] WHERE [SOID] = " & Integer.Parse(Session("idPage")) & " AND cmpcode = '" & CompnyCode & "' AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"

            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                errMessage &= "- Maaf, Data tidak ditemukan..!<br />- Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    errMessage &= "- Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If

        If errMessage <> "" Then
            txtStatus.Text = "IN PROCESS"
            showMessage(errMessage, 2)
            Exit Sub
        End If

        Dim df As New DataTable : df = Session("InvDetail")
        For r1 As Integer = 0 To df.Rows.Count - 1
            FinOId &= Integer.Parse(df.Rows(r1)("finoid"))
            If r1 < df.Rows.Count - 1 Then
                FinOId &= ","
            End If
        Next

        If txtStatus.Text = "POST" Then
            '-- Query Ambil Item Sparepart ketika final --
            sSql = "Select itemtotalprice, itempartqty, Case hpp When 0.0 Then 1 Else Hpp End Hpp, itempartoid From (SELECT fm.trnfinalno, fd.itemreqoid, fd.itempartoid, fd.itemqty, fd.itempartqty, fd.itempartprice, fd.itemtotalprice, fd.itemlocoid, fd.locoidtitipan, fm.typetts, ISNULL((Select i.HPP from QL_mstitem i Where i.itemoid=fd.itempartoid),0.00) Hpp From QL_TRNFINALSPART fd Inner Join QL_TRNFINAL fm ON fm.FINOID=fd.trnfinaloid Where fm.MSTREQOID=" & Integer.Parse(ReqOid.Text) & " AND fm.finoid IN (" & FinOId & ")And fm.FINSTATUS IN ('POST','APPROVED')) fn"
            Dim fd As DataTable = cKoneksi.ambiltabel(sSql, "QL_FdDtl")
            Session("FdDtl") = fd

            If fd.Rows.Count > 0 Then
                For P1 As Integer = 0 To fd.Rows.Count - 1
                    itempartoid += ToDouble(fd.Rows(P1)("itempartoid"))
                    TotalHpp += ToDouble(fd.Rows(P1)("HPP")) * ToDouble(fd.Rows(P1)("itempartqty"))
                    QtyTotal += ToDouble(fd.Rows(P1)("itempartqty"))
                    TotalPart += ToDouble(fd.Rows(P1)("itemtotalprice"))
                Next
            End If
        End If

        '--- Query Ambil Sparepart ---
        sSql = "Select trnfinaloid, trnifinaldtloid, itemreqoid, i.itemdesc, fd.itempartprice, Case When fd.itempartoid=fd.itemreqoid Then fd.itemreqoid Else fd.itempartoid End itemoid, Case When fd.itempartoid=fd.itemreqoid Then fd.itemqty Else fd.itempartqty End QtyPart, itemqty From QL_TRNFINALSPART fd Inner Join QL_mstitem i ON i.itemoid=(Case When fd.itempartoid=fd.itemreqoid Then fd.itemreqoid Else fd.itempartoid End) Where trnfinaloid IN (" & FinOId & ")"
        Dim Dsp As DataTable = cKoneksi.ambiltabel2(sSql, "Ql_Spart")

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            txtPlanOid.Text = GenerateID("QL_trnInvoice", CompnyCode)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            updUser.Text = Session("UserID")
            updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            If txtStatus.Text = "POST" Then
                '---- Generated No Transaksi Invoice ----
                '========================================
                Dim Code As String = "" : Dim iCurID As Integer = 0
                sSql = "Select genother1 from QL_mstgen Where gencode='" & DdlCabang.SelectedValue & "' AND gengroup='CABANG'"
                xCmd.CommandText = sSql : Dim BranchCode As String = xCmd.ExecuteScalar
                Code = "INV/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                sSql = "SELECT ISNULL(MAX(ABS(replace(invoiceno,'" & Code & "',''))),0) invoiceno FROM QL_TRNINVOICE WHERE invoiceno LIKE '" & Code & "%'"
                xCmd.CommandText = sSql
                If Not IsDBNull(xCmd.ExecuteScalar) Then
                    iCurID = xCmd.ExecuteScalar + 1
                Else
                    iCurID = 1
                End If
                Code = GenNumberString(Code, "", iCurID, 4) : noinvo.Text = Code
            End If

            'mode insert
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                GenerateGenID()
                sSql = "INSERT INTO [QL_TRNINVOICE] ([CMPCODE], [BRANCH_CODE], [SOID], [MSTREQOID], [SSTARTTIME],[SENDTIME], [SSTATUS], [SFLAG], [CREATEUSER], [CREATETIME], [UPDUSER], [UPDTIME], [invoiceno], [potongan], [custoid],[biayaservis], [amttotalspart], [amtnetto], totalbiaya, locoidtitipan)" & _
                "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(txtPlanOid.Text) & "," & Integer.Parse(ReqOid.Text) & ",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" & txtStatus.Text & "','" & txtStatus.Text & "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Tchar(noinvo.Text) & "'," & ToDouble(AmtDisc.Text) & "," & Integer.Parse(lblcustoid.Text) & "," & ToDouble(txtHargaJob.Text) & "," & ToDouble(txtHargaSPart.Text) & "," & ToDouble(txtJumlah.Text) & "," & ToDouble(txtHargaServis.Text) & "," & Integer.Parse(mtrwhoid.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & txtPlanOid.Text & " Where tablename = 'QL_trnInvoice' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf txti_u.Text = "Update" Then
                'mode update
                sSql = "UPDATE [QL_TRNINVOICE] SET [MSTREQOID] =" & Integer.Parse(ReqOid.Text) & ", [SSTATUS] = '" & (txtStatus.Text) & "', [SFLAG] ='" & Tchar(txtStatus.Text) & "', [UPDUSER] ='" & Session("UserID") & "', [UPDTIME] = CURRENT_TIMESTAMP, [invoiceno] = '" & Tchar(noinvo.Text) & "', [potongan] = " & ToDouble(AmtDisc.Text) & ",[custoid] = " & Integer.Parse(lblcustoid.Text) & ", [biayaservis] = " & ToDouble(txtHargaJob.Text) & ", [amttotalspart] = " & ToDouble(txtHargaSPart.Text) & ", [amtnetto] = " & ToDouble(txtJumlah.Text) & ", totalbiaya=" & ToDouble(txtHargaServis.Text) & ", locoidtitipan=" & Integer.Parse(mtrwhoid.Text) & " WHERE [SOID]=" & Integer.Parse(txtPlanOid.Text) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "Delete FROM QL_trnInvoiceitem Where SOID=" & txtPlanOid.Text & " AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'insert tabel QL_trnInvoiceitem / detail
            '---------------------------------------
            Dim oidCounter As Integer = GenerateID("QL_TRNINVOICEITEM", CompnyCode)
            Dim dt As New DataTable : dt = Session("InvDetail")

            For c1 As Int16 = 0 To dt.Rows.Count - 1
                sSql = "INSERT INTO [QL_TRNINVOICEITEM] ([CMPCODE], [BRANCH_CODE], [soiddtl], [soid], [finoid], [biayaservisdtl], [amttotalspartdtl], [totalbiayaamt], [typetts], [seq], [createuser], [createtime], [upduser], [updtime], [sostatus], [cabangasal], itemreqoid, itemqty, ItemoidTTS, reqdtloid) " & _
           "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(oidCounter) & "," & Integer.Parse(txtPlanOid.Text) & "," & Integer.Parse(dt.Rows(c1)("FINOID")) & "," & ToDouble(dt.Rows(c1)("BiayaService")) & "," & ToDouble(dt.Rows(c1)("TotalNett")) & ",0,'" & Tchar(dt.Rows(c1)("typetts").ToString) & "'," & Integer.Parse(dt.Rows(c1)("seq")) & ",'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'','" & Tchar(dt.Rows(c1)("cabangasal").ToString) & "'," & Integer.Parse(dt.Rows(c1)("itemoid")) & "," & ToDouble(dt.Rows(c1)("itemqty")) & "," & Integer.Parse(dt.Rows(c1)("ItemoidTTS")) & "," & Integer.Parse(dt.Rows(c1)("reqdtloid")) & ")"

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                oidCounter += 1
                If txtStatus.Text = "POST" Then
                    sSql = "UPDATE QL_TRNFINAL SET FINITEMSTATUS='INVOICED' WHERE FINOID=" & Integer.Parse(dt.Rows(c1)("FINOID")) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            '-- Insert Sparepart --
            Dim invoicepartoid As Integer = GenerateID("QL_TRNINVOICEPART", CompnyCode)
            sSql = "Delete FROM QL_TRNINVOICEPART Where invoiceoid=" & Integer.Parse(txtPlanOid.Text) & " AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For C1 As Int32 = 0 To Dsp.Rows.Count - 1
                sSql = "INSERT INTO [QL_TRNINVOICEPART] ([CMPCODE], [invoicepartoid], [invoiceoid], [invoicedtloid], [itemoid], [itempartqty], [itempartprice], [amtnettodtl], [branch_code], [upduser], [updtime], [itemreqoid])" & _
                " VALUES ('" & CompnyCode & "', " & Integer.Parse(invoicepartoid) & ", " & Integer.Parse(txtPlanOid.Text) & ", 0, " & Integer.Parse(Dsp.Rows(C1)("itemoid")) & ", " & ToDouble(Dsp.Rows(C1)("QtyPart")) & ", " & ToDouble(Dsp.Rows(C1)("itempartprice")) & ", " & ToDouble(Dsp.Rows(C1)("itempartprice")) * ToDouble(Dsp.Rows(C1)("QtyPart")) & ", '" & DdlCabang.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Integer.Parse(Dsp.Rows(C1)("itemreqoid")) & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                invoicepartoid += 1
            Next

            sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(oidCounter) - 1 & " WHERE tablename= 'QL_trnInvoiceitem' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(invoicepartoid) - 1 & " WHERE tablename= 'QL_TRNINVOICEPART' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If txtStatus.Text = "POST" Then
                'Generate crdmtr ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

                Dim dtNya As DataTable = Session("InvDetail")
                If dtNya.Rows.Count > 0 Then
                    For Oi As Int32 = 0 To dtNya.Rows.Count - 1
                        sSql = "Select Hpp From ql_mstitem Where itemoid=" & Integer.Parse(dtNya.Rows(Oi).Item("itemoid")) & ""
                        xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                        '---- Proses Stok Keluar gudang barang titipan ----

                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note,HPP,Branch_Code,conrefoid) VALUES " & _
                        "('" & CompnyCode & "', " & Integer.Parse(conmtroid) & ", 'SERVICEINV',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Period & "', '" & Tchar(noinvo.Text) & "', " & Integer.Parse(txtPlanOid.Text) & ", 'QL_TRNINVOICEITEM', " & Integer.Parse(dtNya.Rows(Oi).Item("itemoid")) & ", 'QL_MSTITEM', 945, " & Integer.Parse(mtrwhoid.Text) & ", 0, " & ToDouble(dtNya.Rows(Oi).Item("itemqty")) & ", '', '" & Session("UserID") & "',CURRENT_TIMESTAMP, 0, 0," & ToDouble(lasthpp) * ToDouble(dtNya.Rows(Oi).Item("itemqty")) & ", '" & Tchar(noinvo.Text) & " ~ " & Tchar(dtNya.Rows(Oi).Item("trnfinalno")) & "', " & ToDouble(lasthpp) & ",'" & DdlCabang.SelectedValue & "'," & Integer.Parse(dtNya.Rows(Oi).Item("reqdtloid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        '---- End Proses stok keluar gudang barang titipan ----
                    Next

                    'Update lastoid QL_conmtr
                    sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    errMessage &= "- Maaf, barang rusak tidak terproses segera hubungi admin atau programmer..!!<br>"
                End If

                '--- Jika amount Netto > 0 maka create auto jurnal ---
                If ToDouble(txtHargaServis.Text) > 0.0 Then
                    Dim vConARId As Integer = GenerateID("QL_conar", CompnyCode)
                    Dim dPayDueDate As Date = GetServerTime().AddDays(0)
                    Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
                    Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)

                    sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime) VALUES" & _
                      " ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(iglmst) & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','TRNINV(" & Tchar(noinvo.Text) & ")','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(iglmst) & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Dim iAkunSementara As String = GetVarInterface("VAR_SERVIS", DdlCabang.SelectedValue)
                    If iAkunSementara = "?" Or iAkunSementara = "0" Or iAkunSementara = "" Then
                        errMessage &= "- Maaf,interface 'VAR_SERVIS' belum di setting silahkan hubungi admin untuk seting interface !!<br>"
                    End If

                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iAkunSementara & "'"
                    xCmd.CommandText = sSql : Dim COA_TAMPUNG As Integer = xCmd.ExecuteScalar
                    If COA_TAMPUNG = 0 Or COA_TAMPUNG = Nothing Then
                        errMessage &= "Maaf, Akun COA untuk VAR_SERVIS tidak ditemukan..!!<br>"
                    End If

                    '--- Insert Jurnal Persediaan ---
                    If ToDouble(itempartoid) > 0.0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 1 & "," & Integer.Parse(iglmst) & "," & Integer.Parse(COA_TAMPUNG) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & Tchar(noinvo.Text) & "','INV (" & noinvo.Text & ")','" & txtPlanOid.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1

                        '---- COA PERSEDIAAN BARANG DAGANGAN ----
                        Dim iMatHpp As String = GetVarInterface("VAR_HPP", DdlCabang.SelectedValue)
                        If iMatHpp = "?" Or iMatHpp = "0" Or iMatHpp = "" Then
                            errMessage &= "- Maaf, interface 'VAR_HPP' belum di setting<br>- silahkan hubungi admin untuk seting interface !!"
                        End If

                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatHpp & "'"
                        xCmd.CommandText = sSql : Dim COA_HPP As Integer = xCmd.ExecuteScalar
                        If COA_HPP = 0 Or COA_HPP = Nothing Then
                            errMessage &= "- Maaf, Akun COA untuk VAR_HPP tidak ditemukan..!!<br>"
                        End If

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        "('" & CompnyCode & "'," & Integer.Parse(igldtl) + 1 & "," & 2 & "," & Integer.Parse(iglmst) & "," & Integer.Parse(COA_HPP) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & ",'" & Tchar(noinvo.Text) & "','TRNINVOICE(" & Tchar(noinvo.Text) & ")','" & txtPlanOid.Text & "','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1
                    End If
                    '--- End Jurnal Persediaan ---

                    '=========== Jurnal PIUTANG USAHA (D) ============
                    Dim sVarAR As String = GetVarInterface("VAR_AR", DdlCabang.SelectedValue)
                    If sVarAR = "?" Then
                        errMessage &= "- Maaf, Akun COA untuk VAR_AR tidak ditemukan..!!<br>"
                    End If

                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & sVarAR & "'"
                    xCmd.CommandText = sSql : Dim iAROid As Integer = xCmd.ExecuteScalar


                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                    "('" & CompnyCode & "', " & Integer.Parse(igldtl) + 1 & ", " & 3 & ", " & Integer.Parse(iglmst) & "," & Integer.Parse(iAROid) & ",'D'," & ToDouble(txtHargaServis.Text) & "," & ToDouble(txtHargaServis.Text) & "," & ToDouble(txtHargaServis.Text) & ",'" & Tchar(noinvo.Text) & "','TRNINVOICE(" & Tchar(noinvo.Text) & ")','" & Integer.Parse(txtPlanOid.Text) & "','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    igldtl += 1
                    '===============================================

                    '=========== Jurnal PENJUALAN USAHA (C) ============
                    Dim VarJualServis As String = GetVarInterface("VAR_SALES", DdlCabang.SelectedValue)
                    If sVarAR = "?" Then
                        errMessage &= "- Maaf, Akun COA untuk VAR_SALES tidak ditemukan..!!<br>"
                    End If

                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & VarJualServis & "'"
                    xCmd.CommandText = sSql : Dim OidJualSpart As Integer = xCmd.ExecuteScalar

                    sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid, acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate, upduser,updtime,branch_code) VALUES " & _
                    "('" & CompnyCode & "'," & Integer.Parse(igldtl) + 1 & "," & 4 & "," & Integer.Parse(iglmst) & "," & Integer.Parse(OidJualSpart) & ",'C'," & ToDouble(txtHargaServis.Text) & "," & ToDouble(txtHargaServis.Text) & "," & ToDouble(txtHargaServis.Text) & ",'" & Tchar(noinvo.Text) & "','TRNINVOICE(" & Tchar(noinvo.Text) & ")','" & Integer.Parse(txtPlanOid.Text) & "','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    igldtl += 1
                    '===============================================

                    '=============== QL_conar ===============
                    sSql = "INSERT INTO QL_conar (cmpcode, branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnarflag, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amttransidr, amttransusd, amtbayar, trnarnote, trnarres1, upduser, updtime) VALUES " & _
                    "('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & vConARId & ",'QL_TRNINVOICE'," & txtPlanOid.Text & ",0," & lblcustoid.Text & ",'" & iAROid & "','POST','','PIUTANGINV','" & GetServerTime() & "','" & GetDateToPeriodAcctg(GetServerTime()) & "',0,'1/1/1900','',0,'" & dPayDueDate & "'," & ToDouble(txtJumlah.Text) & "," & ToDouble(txtJumlah.Text) & "," & ToDouble(txtJumlah.Text) & ",0.0000,'INVOICE SERVIS(" & Tchar(txtNamaCust.Text) & "|NO=" & Tchar(noinvo.Text) & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstoid set lastoid=" & vConARId & " Where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstoid set lastoid=" & Integer.Parse(igldtl) & " Where tablename ='QL_trngldtl' And cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If errMessage <> "" Then
                        txtStatus.Text = "IN PROCESS"
                        objTrans.Rollback() : conn.Close()
                        showMessage(errMessage, 2)
                        Exit Sub
                    End If
                End If
                '--- End If txtStatus.Text = "POST" ---
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            txtStatus.Text = "IN PROCESS"
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("~\transaction\trnInvoice.aspx?awal=true")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
    End Sub

    Protected Sub printGV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Dim temp As String = sender.commandargument.ToString
        '  PrintReport(temp.Remove(temp.IndexOf(";")), temp.Substring(temp.IndexOf(";") + 1))
    End Sub

    Protected Sub GVListInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVListInvoice.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble((e.Row.Cells(4).Text)), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble((e.Row.Cells(5).Text)), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble((e.Row.Cells(6).Text)), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble((e.Row.Cells(7).Text)), 3)
        End If
    End Sub

    Protected Sub txtHargaJob_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaJob.TextChanged
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
    End Sub

    Protected Sub txtHargaSPart_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHargaSPart.TextChanged
        txtHargaServis.Text = ToDouble(txtHargaJob.Text) + ToDouble(txtHargaSPart.Text)
    End Sub

    Protected Sub btnReady_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtStatus.Text = "POST" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnBatal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If txti_u.Text = "Update" Then
                'mode update
                'delete QL_trnInvoice / master
                sSql = "Delete from QL_trnInvoice where SOID=" & txtPlanOid.Text & " AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete QL_trnInvoiceitem / detail
                sSql = "Delete from QL_trnInvoiceitem where SOID=" & txtPlanOid.Text & " And BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'delete(QL_trnInvoicepart / detail)
                sSql = "Delete from QL_TRNINVOICEPART where invoiceoid=" & txtPlanOid.Text & " AND BRANCH_CODE='" & DdlCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />;" & sSql, 1)
            Exit Sub
        End Try
        Session.Remove("idPage")
        Response.Redirect("~\Transaction\trnInvoice.aspx?page=true")
    End Sub

    Protected Sub printInvoice(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim tmpstatus As String = gvr.Cells(6).Text.ToString
        If tmpstatus <> "Invoiced" And tmpstatus <> "Paid" Then
            showMessage("Print out invoice hanya untuk invoice dengan status 'Invoiced' atau 'Paid' !", 2)
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        Else
            Dim soid As Integer = GVListInvoice.DataKeys(gvr.RowIndex).Item("soid")
            Dim status As String = GVListInvoice.DataKeys(gvr.RowIndex).Item("sstatus")
            Dim custoid As Integer = Integer.Parse(GVListInvoice.DataKeys(gvr.RowIndex).Item("reqcustoid"))
            Dim price As Decimal = Decimal.Parse(GVListInvoice.DataKeys(gvr.RowIndex).Item("stotalprice"))
            showprint(soid)
        End If
    End Sub

    Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprint.Click
        Dim soid As Integer = Integer.Parse(Session("idPage"))
        Dim status As String = txtStatus.Text
        Dim custoid As Integer = Integer.Parse(lblcustoid.Text)
        Dim price As Decimal = Decimal.Parse(txtHargaServis.Text.Trim.Replace(",", ""))
        showprint(soid)
    End Sub

    Protected Sub BtnSfinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSfinal.Click
        If ReqOid.Text <> "" Then
            BindFinal()
            panelItem.Visible = True : btnHideItem.Visible = True : mpeItem.Show()
        Else
            lblState.Text = "TRUE" : lblState.Visible = False
            showMessage("- Maaf, Anda belum pilih nomer penerimaan..!!<br>", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnCloseFn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseFn.Click
        btnHideItem.Visible = False : panelItem.Visible = False
    End Sub

    Protected Sub AddToListFn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AddToListFn.Click
        Try
            Checkedfinal()
            If Session("BindFinal") IsNot Nothing Then
                Dim dt As DataTable = Session("BindFinal")

                If dt.Rows.Count > 0 Then
                    Dim df As DataView = dt.DefaultView
                    df.RowFilter = "checkvalue='True'"

                    If df.Count > 0 Then
                        Dim sErr As String = "" : Dim dtab As DataTable
                        If Session("InvDetail") Is Nothing Then
                            dtab = CrtDtlFinal() : Session("InvDetail") = dtab
                            SeqBarang.Text = 0 + 1
                        Else
                            dtab = Session("InvDetail")
                            SeqBarang.Text = dtab.Rows.Count + 1
                        End If

                        Dim OidFIn As String = ""
                        For C1 As Integer = 0 To df.Count - 1
                            OidFIn &= "'" & df(C1)("FINOID").ToString & "',"
                        Next
                        OidFIn = Left(OidFIn, OidFIn.Length - 1)

                        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                            sSql = "Select oi.invoiceno,fn.trnfinalno,SSTATUS,oi.CREATETIME,oi.CREATEUSER from QL_TRNINVOICEITEM id Inner Join QL_TRNINVOICE oi ON oi.SOID=id.soid Inner Join QL_TRNFINAL fn ON fn.FINOID=id.finoid Where SSTATUS='IN PROCESS' AND id.finoid IN (" & OidFIn & ")"
                            Dim CekFin As DataTable = cKoneksi.ambiltabel2(sSql, "CekFin")
                            If CekFin.Rows.Count > 0 Then
                                For i As Integer = 0 To CekFin.Rows.Count - 1
                                    sValidasi.Text &= "- Maaf, No Final " & CekFin.Rows(i)("trnfinalno") & " sudah pernah di invoice dengan No. Draft Invoice : " & CekFin.Rows(i)("invoiceno") & " dan status : " & CekFin.Rows(i)("SSTATUS") & " <br />"
                                Next
                            End If
                        End If

                        If sValidasi.Text <> "" Then
                            showMessage(sValidasi.Text, 2)
                            btnHideItem.Visible = False : panelItem.Visible = False
                            Exit Sub
                        End If

                        sSql = "Select ROW_NUMBER() OVER (ORDER BY finoid ASC) AS Seq,* from  (Select fm.mstreqoid,fm.FINOID,fm.trnfinalno,fm.BRANCH_CODE CbgInput,cbi.gendesc DeckCbInp,fm.cabangasal,cba.gendesc deskCbA,fm.FINSTATUS,fm.typetts,fm.amtnettservis,i.itemoid,i.itemcode,i.itemdesc,fd.itemqty,SUM(fd.itempartprice*fd.itempartqty) TotalNett,fm.locoidtitipan,fd.reqdtloid,fm.priceservis BiayaService,itemoid ItemoidTTS,(Select rd.reqdtljob from QL_TRNREQUESTDTL rd Where rd.reqdtloid=fd.reqdtloid AND rd.reqmstoid=fm.MSTREQOID) reqdtljob From QL_TRNFINAL fm Inner Join QL_TRNFINALSPART fd ON fd.trnfinaloid=fm.FINOID Inner Join QL_mstitem i ON i.itemoid=fd.itemreqoid Inner Join QL_mstgen cbi ON cbi.gencode=fm.BRANCH_CODE And cbi.gengroup='CABANG' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal And cba.gengroup='cabang' Where fm.typetts='SERVICE' AND FINITEMSTATUS <>'INVOICED' Group By fm.BRANCH_CODE,fm.finoid,fm.trnfinalno,fd.itemqty,fm.locoidtitipan,i.itemoid,i.itemcode,i.itemdesc,fm.priceservis,fm.amtnettservis,cbi.gendesc,fm.cabangasal,cba.gendesc,fm.FINSTATUS,fm.typetts,fd.reqdtloid,itemreqoid,fm.MSTREQOID UNION ALL Select fm.MSTREQOID,fm.FINOID,fm.trnfinalno,fm.BRANCH_CODE CbgInput,cbi.gendesc DeckCbInp,fm.cabangasal,cba.gendesc deskCbA,fm.FINSTATUS,fm.typetts,fm.amtnettservis,Case When fd.itempartoid=fd.itemreqoid Then fd.itemreqoid Else fd.itempartoid End itemoid,i.itemcode,i.itemdesc,fd.itemqty,SUM(fd.itempartprice*fd.itempartqty) TotalNett,fm.locoidtitipan,fd.reqdtloid,fm.priceservis BiayaService,itemreqoid ItemoidTTS,(Select rd.reqdtljob from QL_TRNREQUESTDTL rd Where rd.reqdtloid=fd.reqdtloid AND rd.reqmstoid=fm.MSTREQOID) reqdtljob From QL_TRNFINAL fm Inner Join QL_TRNFINALSPART fd ON fd.trnfinaloid=fm.FINOID Inner Join QL_mstitem i ON i.itemoid=(Case When fd.itempartoid=fd.itemreqoid Then fd.itemreqoid Else fd.itempartoid End)Inner Join QL_mstgen cbi ON cbi.gencode=fm.BRANCH_CODE And cbi.gengroup='CABANG' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal And cba.gengroup='cabang' Where fm.typetts='REPLACE' AND FINITEMSTATUS <>'INVOICED' Group By fm.BRANCH_CODE,fm.finoid,fm.trnfinalno,fd.itemqty,fm.locoidtitipan,i.itemoid,i.itemcode,i.itemdesc,fm.priceservis,fm.amtnettservis,cbi.gendesc,fm.cabangasal,cba.gendesc,fm.FINSTATUS,fm.typetts,fd.reqdtloid,fd.itempartoid,fd.itemreqoid,fm.MSTREQOID) cg Where FINOID IN (" & OidFIn & ") AND MSTREQOID=" & Integer.Parse(ReqOid.Text) & ""
                        Dim dv As DataTable = cKoneksi.ambiltabel2(sSql, "QL_FinalDtl")
                        Session("FinalDtl") = dv
                        For C1 As Integer = 0 To dv.Rows.Count - 1
                            Dim dtView As DataView = dtab.DefaultView
                            dtView.RowFilter = "FINOID=" & Integer.Parse(dv.Rows(C1)("FINOID")) & " And seq=" & Integer.Parse(dv.Rows(C1)("Seq")) & ""
                            Dim drow As DataRow : Dim drowedit() As DataRow

                            If dtView.Count <= 0 Then
                                drow = dtab.NewRow
                                drow("seq") = Integer.Parse(dv.Rows(C1)("Seq")) 'SeqBarang.Text
                                drow("FINOID") = Integer.Parse(dv.Rows(C1)("FINOID"))
                                drow("trnfinalno") = dv.Rows(C1)("trnfinalno").ToString
                                drow("itemoid") = Integer.Parse(dv.Rows(C1)("itemoid"))
                                drow("itemdesc") = dv.Rows(C1)("itemdesc").ToString
                                drow("itemqty") = ToMaskEdit(dv.Rows(C1)("itemqty"), 3)
                                drow("BiayaService") = ToMaskEdit(dv.Rows(C1)("BiayaService"), 3)
                                drow("TotalNett") = ToMaskEdit(dv.Rows(C1)("TotalNett"), 3)
                                drow("typetts") = dv.Rows(C1)("typetts").ToString
                                drow("CbgInput") = dv.Rows(C1)("CbgInput").ToString
                                drow("cabangasal") = dv.Rows(C1)("cabangasal").ToString
                                drow("ItemoidTTS") = Integer.Parse(dv.Rows(C1)("ItemoidTTS"))
                                drow("reqdtljob") = dv.Rows(C1)("reqdtljob").ToString
                                drow("reqdtloid") = Integer.Parse(dv.Rows(C1)("reqdtloid"))
                                dtab.Rows.Add(drow) : dtab.AcceptChanges()
                            Else
                                drowedit = dtab.Select("FINOID=" & Integer.Parse(dv.Rows(C1)("FINOID")) & " AND seq=" & Integer.Parse(dv.Rows(C1)("Seq")) & "")
                                drow = drowedit(0)
                                drowedit(0).BeginEdit()
                                'drow("seq") = SeqBarang.Text
                                drow("FINOID") = Integer.Parse(dv.Rows(C1)("FINOID"))
                                drow("trnfinalno") = dv.Rows(C1)("trnfinalno").ToString
                                drow("itemoid") = Integer.Parse(dv.Rows(C1)("itemoid"))
                                drow("itemdesc") = dv.Rows(C1)("itemdesc").ToString
                                drow("itemqty") = ToMaskEdit(dv.Rows(C1)("itemqty"), 3)
                                drow("BiayaService") = ToMaskEdit(dv.Rows(C1)("BiayaService"), 3)
                                drow("TotalNett") = ToMaskEdit(dv.Rows(C1)("TotalNett"), 3)
                                drow("typetts") = dv.Rows(C1)("typetts").ToString
                                drow("CbgInput") = dv.Rows(C1)("CbgInput").ToString
                                drow("cabangasal") = dv.Rows(C1)("cabangasal").ToString
                                drow("ItemoidTTS") = Integer.Parse(dv.Rows(C1)("ItemoidTTS"))
                                drow("reqdtljob") = dv.Rows(C1)("reqdtljob").ToString
                                drow("reqdtloid") = Integer.Parse(dv.Rows(C1)("reqdtloid"))
                                drowedit(0).EndEdit()
                                dtab.Select(Nothing, Nothing)
                                dtab.AcceptChanges()
                            End If
                            drow.EndEdit() : SeqBarang.Text += 1
                            dtView.RowFilter = ""
                        Next

                        btnHideItem.Visible = False : panelItem.Visible = False
                        mpeItem.Show() : GvInvDtl.DataSource = dtab
                        GvInvDtl.DataBind() : Session("InvDetail") = dtab
                        HitungNet(OidFIn) : GvInvDtl.Visible = True
                    Else
                        sValidasi.Text &= "- Maaf, Anda belum memilih no. final..!!<br />"
                    End If

                End If
            Else
                sValidasi.Text &= "- Maaf, Silahkan klik add to list dulu..!!<br>"
            End If

            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub BtnViewFinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewFinal.Click
        Checkedfinal()
        If Session("BindFinal") IsNot Nothing Then
            Dim dt As DataTable = Session("BindFinal")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = "fm.trnfinalno LIKE '%%'"

                dv.RowFilter = sFilter
                Session("BindFinal") = dv.ToTable
                GVfinal.DataSource = Session("BindFinal")
                GVfinal.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda cari tidak ada..!!", 2)
            End If
        End If
    End Sub

    Protected Sub BtnFinals_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnFinals.Click
        Checkedfinal()
        If Session("BindFinal") IsNot Nothing Then
            Dim dt As DataTable = Session("BindFinal")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = " trnfinalno LIKE '%" & Tchar(FinalNo.Text) & "%'"

                dv.RowFilter = sFilter
                Session("BindFinal") = dv.ToTable
                GVfinal.DataSource = Session("BindFinal")
                GVfinal.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda cari tidak ada..!!", 2)
            End If
        End If
    End Sub

    Protected Sub GvInvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvInvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("InvDetail") Is Nothing Then
            dtab = Session("InvDetail")
        Else
            showMessage("Error Program..!!", 1)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(0).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges() : GvInvDtl.DataSource = dtab
        GvInvDtl.DataBind() : Session("InvDetail") = dtab

        Dim OidFIn As String = "" : Dim tr As DataTable = Session("InvDetail")
        If tr.Rows.Count > 0 Then
            For c1 As Int32 = 0 To tr.Rows.Count - 1
                OidFIn &= "'" & tr.Rows(c1)("FINOID").ToString & "',"
            Next
            OidFIn = Left(OidFIn, OidFIn.Length - 1)
        Else
            OidFIn = "0"
        End If
        HitungNet(OidFIn)
    End Sub

    Protected Sub GVfinal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVfinal.PageIndexChanging
        Checkedfinal()
        GVfinal.PageIndex = e.NewPageIndex
        If Session("BindFinal") IsNot Nothing Then
            Dim dt As DataTable = Session("BindFinal")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = " trnfinalno LIKE '%" & Tchar(FinalNo.Text) & "%'"

                dv.RowFilter = sFilter
                Session("BindFinal") = dv.ToTable
                GVfinal.DataSource = Session("BindFinal")
                GVfinal.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda cari tidak ada..!!", 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub GVfinal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVfinal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub
 
    Protected Sub AmtDisc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AmtDisc.TextChanged
        AmtDisc.Text = ToMaskEdit(AmtDisc.Text, 3)
        HitungUlang()
    End Sub

#End Region
End Class
