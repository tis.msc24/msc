Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_Master_Customer
    Inherits System.Web.UI.Page

#Region "Variable"
    Private Regex As Regex
    Dim oMatches As MatchCollection
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String
    Dim sFax1 As String
    Dim sFax2 As String
    Dim sValue As String
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        validasi.Text = message
        lblCaption.Text = caption
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEError.Show()
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rpt_mstCustomer.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim sWhere As String = "" : Dim sFilter As String = ""

        sWhere = " where QL_mstcust.cmpcode LIKE '%" & CompnyCode & "%' and custflag = '" & DDLStatus.SelectedValue & "' "

        If txtFilter.Text.Trim <> "" Then
            If ddlFilter.SelectedItem.Text.ToUpper = "Code" Or ddlFilter.SelectedItem.Text.ToUpper = "Nama" Or ddlFilter.SelectedItem.Text.ToUpper = "Group" Then
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '" & Tchar(txtFilter.Text) & "' "
            Else
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '%" & Tchar(txtFilter.Text) & "%' "
            End If
            'sFilter &= " " & ddlFilter.SelectedItem.Text & " = " & txtFilter.Text & ", "
        End If

        'If sFilter = "" Then
        '    sFilter = " ALL "
        'End If

        rptReport.SetParameterValue("sWhere", sWhere)
        'rptReport.SetParameterValue("sFilter", " Filter By : " & sFilter)

        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = ""

        If txtFilter.Text.Trim <> "" Then
            If ddlFilter.SelectedItem.Text.ToUpper = "Code" Or ddlFilter.SelectedItem.Text.ToUpper = "Nama" Or ddlFilter.SelectedItem.Text.ToUpper = "Group" Then
                sWhere = " Where upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '" & Tchar(txtFilter.Text) & "' "
            Else
                sWhere = " Where upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '%" & Tchar(txtFilter.Text) & "%' "
            End If
        End If
        Response.Clear()
        If printType = "CUSTOMER" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"
            sSql = "select custoid,custcode,custname,custaddr,phone1,custgroup,custflag,notes from QL_mstcust " & sWhere & " order by custcode"
        End If
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet : Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()
    End Sub

    Private Sub fillFaxField(ByVal faxString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal faxCode As TextBox)
        Dim sTemp() As String = faxString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                faxCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillPhoneField(ByVal phoneString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal phoneCode As TextBox)
        Dim sTemp() As String = phoneString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                phoneCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub initProvince(ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%'   ORDER BY gendesc"
        FillDDL(custprovoid, sSql)

        sSql = "SELECT genoid FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%' and gendesc like 'BALI'  ORDER BY gendesc"
        custprovoid.SelectedValue = sSql
    End Sub

    Private Sub initCity(ByVal sProv As String, ByVal sCountry As String)
        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('CITY') AND (genother2='" & sProv & "' ) AND (genother1='" & sCountry & "') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
            FillDDL(custcityoid, sSql)
            custcityoid.Items.Add(New ListItem("-", "-"))
            custcityoid.SelectedValue = "-"
        Else
            sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('CITY') AND (genother2='" & sProv & "' ) AND (genother1='" & sCountry & "') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
            FillDDL(custcityoid, sSql)
        End If
    End Sub

    Private Sub initCountry()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)
    End Sub

    Private Sub initPrefix()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXCOMPANY') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(precustname, sSql)
    End Sub

    Private Sub initConTitle()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXPERSON') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(ddlconttitle, sSql)
        FillDDL(ddlconttitle2, sSql)
        FillDDL(ddlconttitle3, sSql)
    End Sub

    Private Sub BindCustGroup()
        sSql = "Select custgroupoid,custgroupname,ISNULL(custgroupcreditlimitrupiah,0.000) LimitNya,ISNULL(custgroupcreditlimitusagerupiah,0.0000) UsageNya from QL_mstcustgroup WHERE cmpcode='MSC' AND custgroupflag='ACTIVE' And (custgroupcode LIKE '%" & Tchar(CustNameGroup.Text) & "%' OR custgroupname LIKE '%" & Tchar(CustNameGroup.Text) & "%') ORDER BY custgroupname"
        FillGV(gvCustGroup, sSql, "ql_mstcustgroup")
        gvCustGroup.Visible = True
    End Sub

    Private Sub initCurr()
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custdefaultcurroid, sSql)
        FillDDL(custcreditlimitcurroid, sSql)
    End Sub

    Private Sub checkCityList()
        Try
            If custcityoid.Items.Count = 0 Then
                'cpcityphonecode.Text = "" 
                Exit Sub
            Else
                Try
                    initCityPhoneCode(custcityoid.SelectedValue)
                Catch ex As Exception
                    showMessage(ex.ToString, CompnyName & " - Error", 1)
                End Try
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Private Sub initCityPhoneCode(ByVal ctoid As Char)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, '') FROM QL_mstgen WHERE (gengroup IN ('CITY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If 
        conn.Close()
    End Sub

    Private Sub initCountryPhoneCode(ByVal ctoid As Int32)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, ''), genoid, gengroup FROM QL_mstgen WHERE (gengroup IN ('COUNTRY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        cpcountryphonecode.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Private Sub Outlate()
        sSql = "SELECT gencode from QL_mstgen Where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'"
        KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
    End Sub

    Private Sub DDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangFilter, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangFilter, sSql)
            Else
                FillDDL(CabangFilter, sSql)
                CabangFilter.Items.Add(New ListItem("ALL", "ALL"))
                CabangFilter.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangFilter, sSql)
            CabangFilter.Items.Add(New ListItem("ALL", "ALL"))
            CabangFilter.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(branchID, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(branchID, sSql)
            Else
                FillDDL(branchID, sSql) 
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(branchID, sSql) 
        End If
    End Sub

    Private Sub initDDL()
        initPrefix() : initConTitle()
        initCurr()

        Dim sWhere As String = ""
        sWhere = " and gendesc like 'INDONESIA%'  "
        sSql = "SELECT genoid, gendesc gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' " & sWhere & " ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)

        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personstatus in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%SALES%') "
        FillDDL(spgOid, sSql)

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE (gengroup IN ('PAYTYPE')) AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custpaytermdefaultoid, sSql)

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' and  gendesc='cash' AND cmpcode LIKE '%" & CompnyCode & "%'"
        Else
            sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' AND cmpcode LIKE '%" & CompnyCode & "%'"
        End If

        FillDDL(dd_timeofpayment, sSql)
        initCountryPhoneCode(custcountryoid.SelectedValue)
        checkCityList()
        sSql = "SELECT gencode from QL_mstgen where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'" : KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
    End Sub

    Private Sub InitCoa()
        FillDDLAcctg(araccount, "VAR_AR", branchID.SelectedValue)
        FillDDLAcctg(returaccount, "VAR_RETJUAL", branchID.SelectedValue )
    End Sub

    Public Sub generateCustID()
        sSql = "SELECT (lastoid+1) FROM QL_mstoid WHERE tablename LIKE '%QL_mstcust%' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        custoid.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Public Sub clearItems()
        custcode.Text = ""
        custname.Text = ""
        custaddr.Text = ""
        custpostcode.Text = "" 
        phone1.Text = "" : phone2.Text = "" : phone3.Text = ""
        custfax1.Text = "" : CustFax2.Text = ""
        custemail.Text = "" : custwebsite.Text = ""
        notes.Text = ""
        custnpwp.Text = ""
        UpdUser.Text = Session("UserId")
        UpdTime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
        precustname.SelectedIndex = 0
        custcityoid.SelectedIndex = 0
        custprovoid.SelectedIndex = 0
        custcountryoid.SelectedIndex = 0
        custflag.SelectedIndex = 0
        custpaytermdefaultoid.SelectedIndex = 0
        custdefaultcurroid.SelectedIndex = 0
        custcreditlimitcurroid.SelectedIndex = 0
        custcreditlimit.Text = ""
    End Sub

    Private Sub bindCust(ByVal sWhere As String)
        sSql = "SELECT custoid, case custcode when '' then Convert(char(10),custoid) else custcode end custcode, custname, custaddr, b.gendesc AS city,cbg.gendesc cabang, ISNULL(c.phone1, 0) AS phone, c.custflag,custgroup, notes,isnull(c.custcreditlimitrupiah,0.00) creditlimit,isnull(c.custcreditlimitusagerupiah,0.00) creditusage ,case when custcreditlimitusagerupiah>custcreditlimitrupiah then (custcreditlimitusagerupiah-custcreditlimitrupiah) * -1 else 0 end creditover,c.pin_bb, (select g.gendesc from ql_mstgen g where g.genoid = c.timeofpayment and g.gengroup = 'PAYTYPE') AS termin,(SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=c.custoid AND j.trnjualstatus='POST' AND branch_code=c.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) custcreditlimitusagaeoverdue FROM QL_MSTCUST c INNER JOIN QL_mstgen b ON b.genoid = c.custcityoid LEFT JOIN QL_mstgen cbg on cbg.gencode = c.branch_code and cbg.gengroup = 'Cabang' WHERE c.cmpcode LIKE 'MSC' and custgroup in ('GROSIR','PROJECT','','TOKO') " & sWhere & ""

        If CabangFilter.SelectedValue <> "ALL" Then
            sSql &= "AND c.branch_code='" & CabangFilter.SelectedValue & "'"
        End If

        sSql &= "ORDER BY c.custname"
        Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "customer")
        GVmstcust.Visible = True
        GVmstcust.DataSource = xTableItem1
        GVmstcust.DataBind()
        GVmstcust.SelectedIndex = -1
    End Sub

    Private Sub fillTextBox(ByVal idPage As Integer)
        sSql = "SELECT ct.createtime, branch_code, custoid, custgroupoid, custcode, timeofpayment, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3,custfax1, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, Isnull(custcreditlimitrupiah,0.000) custcreditlimitrupiah, ISNULL(custcreditlimitusagerupiah,0.00) custcreditlimitusagerupiah,notes, createuser, upduser, updtime, prefixcmp, prefixcp1, prefixcp2, prefixcp3, custgroup, custbank, custpassportname, custpassportno, custbirthdate, salesoid, custacctgoid, coa_retur,pin_bb, (Select custgroupname from QL_mstcustgroup cg Where cg.custgroupoid=ct.custgroupoid) cgName,(SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr, j.amtreturidr, j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=ct.custoid AND j.trnjualstatus='POST' AND branch_code=ct.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) custcreditlimitusagaeoverdue FROM QL_mstcust ct WHERE cmpcode='" & CompnyCode & "' AND custoid=" & idPage & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = "" : Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read
                branchID.SelectedValue = xReader("branch_code").ToString.Trim
                CustOid.Text = xReader("custoid")
                dd_timeofpayment.SelectedValue = xReader("timeofpayment").ToString
                Session("creditlimit") = xReader("custcreditlimitrupiah").ToString
                custcode.Text = xReader("custcode").ToString.Trim
                custflag.Text = xReader("custflag").ToString.Trim
                custaddr.Text = xReader("custaddr").ToString.Trim
                custcountryoid.SelectedValue = xReader("custcountryoid")
                custpostcode.Text = xReader("custpostcode").ToString.Trim
                phone1.Text = xReader("phone1").ToString
                phone2.Text = xReader("phone2").ToString
                phone3.Text = xReader("phone3").ToString
                custfax1.Text = xReader("custfax1").ToString
                lastsalesdate.Text = GetStrData("select top 1 trnjualdate from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & CustOid.Text & "' order by trnjualdate desc")
                If lastsalesdate.Text = "?" Then
                    lastsalesdate.Text = "-"
                End If

                txtage.Text = GetStrData("select top 1 DATEDIFF(d, trnjualdate , GETDATE()) [umur sales] from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & CustOid.Text & "' order by trnjualdate desc")
                If txtage.Text = "?" Then
                    txtage.Text = "-"
                End If

                custemail.Text = xReader("custemail").ToString.Trim
                custwebsite.Text = xReader("custwebsite").ToString.Trim
                custnpwp.Text = xReader("custnpwp").ToString.Trim
                contactperson1.Text = xReader("contactperson1").ToString.Trim
                contactperson2.Text = xReader("contactperson2").ToString.Trim
                contactperson3.Text = xReader("contactperson3").ToString.Trim
                phonecontactperson1.Text = xReader("phonecontactperson1").ToString.Trim
                phonecontactperson2.Text = xReader("phonecontactperson2").ToString.Trim
                phonecontactperson3.Text = xReader("phonecontactperson3").ToString.Trim
                notes.Text = Trim(xReader("notes")).ToString
                UpdUser.Text = xReader("upduser").ToString.Trim
                UpdTime.Text = Format(xReader("updtime"), "dd/MM/yyyy HH:mm:ss")
                TglGabung.Text = Format(xReader("createtime"), "dd/MM/yyyy")
                initPrefix()
                precustname.SelectedValue = xReader("PREFIXCMP").ToString
                custname.Text = xReader("custname").ToString
                ddlcustgroup.SelectedValue = xReader("custgroup").ToString.Trim

                If xReader("custgroupoid").ToString.Trim = 0 Then
                    custgroupoid.Text = ""
                Else
                    custgroupoid.Text = xReader("custgroupoid")
                End If
                CustNameGroup.Text = xReader("cgName").ToString
                custprovoid.Items.Clear()
                initProvince(custcountryoid.SelectedValue)
                custprovoid.SelectedValue = xReader("custprovoid")
                custcityoid.Items.Clear()
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
                custcityoid.SelectedValue = xReader("custcityoid")
                custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitrupiah").ToString), 4)
                custcreditlimitusagerupiah.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitusagerupiah").ToString), 4)
                ddlconttitle.SelectedValue = xReader("prefixcp1").ToString
                ddlconttitle2.SelectedValue = xReader("prefixcp2").ToString
                ddlconttitle3.SelectedValue = xReader("prefixcp3").ToString
                namapaspor.Text = xReader("custpassportname").ToString
                nopaspor.Text = xReader("custpassportno").ToString
                bankrekening.Text = xReader("custbank").ToString
                spgOid.SelectedValue = xReader("salesoid").ToString

                If xReader("custbirthdate") <> "1/1/1900" Then
                    tgllahir.Text = Format(xReader("custbirthdate"), "dd/MM/yyyy")
                End If

                InitCoa()
                araccount.SelectedValue = xReader("custacctgoid")
                returaccount.SelectedValue = xReader("coa_retur").ToString
                pin_bb.Text = xReader("pin_bb").ToString
                custcreditlimitusageoverdue.Text = ToMaskEdit(ToDouble(xReader("custcreditlimitusagaeoverdue").ToString), 4)
            End While
        End If
        ddlcustgroup.Enabled = False : ddlcustgroup.CssClass = "inpTextDisabled"
        CustNameGroup.Enabled = False : CustNameGroup.CssClass = "inpTextDisabled"
        btnFindCG.Visible = False : btnClearCG.Visible = False
        precustname.Enabled = False : precustname.CssClass = "inpTextDisabled"
        custname.Enabled = False : custname.CssClass = "inpTextDisabled"
        custprovoid.Enabled = False : custprovoid.CssClass = "inpTextDisabled"
        branchID.Enabled = False : branchID.CssClass = "inpTextDisabled"
        custcityoid.Enabled = False : custcityoid.CssClass = "inpTextDisabled"
        custaddr.Enabled = False : custaddr.CssClass = "inpTextDisabled"
        contactperson1.Enabled = False : contactperson1.CssClass = "inpTextDisabled"
        contactperson2.Enabled = False : contactperson2.CssClass = "inpTextDisabled"
        custfax1.Enabled = False : custfax1.CssClass = "inpTextDisabled"
        contactperson3.Enabled = False : contactperson3.CssClass = "inpTextDisabled"
        custwebsite.Enabled = False : custwebsite.CssClass = "inpTextDisabled"
        custemail.Enabled = False : custemail.CssClass = "inpTextDisabled"
        custnpwp.Enabled = False : custnpwp.CssClass = "inpTextDisabled"
        phone1.Enabled = False : phone1.CssClass = "inpTextDisabled"
        phone2.Enabled = False : phone2.CssClass = "inpTextDisabled"
        phone3.Enabled = False : phone3.CssClass = "inpTextDisabled"
        ddlconttitle.Enabled = False : ddlconttitle.CssClass = "inpTextDisabled"
        ddlconttitle2.Enabled = False : ddlconttitle2.CssClass = "inpTextDisabled"
        ddlconttitle3.Enabled = False : ddlconttitle3.CssClass = "inpTextDisabled"
        bankrekening.Enabled = False : bankrekening.CssClass = "inpTextDisabled"
        returaccount.Enabled = False : returaccount.CssClass = "inpTextDisabled"
        araccount.Enabled = False : araccount.CssClass = "inpTextDisabled"
        btnSave.Visible = False
        conn.Close()
    End Sub

    Private Sub CreditLimit(ByVal idPage As Integer)
        sSql = "SELECT timeofpayment FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & idPage & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = ""
        Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read

                dd_timeofpayment.SelectedValue = xReader("timeofpayment")

            End While
        End If

    End Sub

    Private Sub DataBindTrn()
        sSql = "SELECT trnjualno, CONVERT(varchar,trnjualdate,103) AS trnjualdate, CONVERT(varchar,payduedate,103) AS payduedate,ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) trnamtjualnetto FROM ( select j.trnjualmstoid, j.trnjualno,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=" & custoid.Text & " AND j.trnjualstatus='POST' ) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr) group by trnjualno,trnjualdate, payduedate"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstmat")
        Session("TblListMat") = dt
        gvListNya.DataSource = Session("TblListMat")
        gvListNya.DataBind() : gvListNya.Visible = True
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub
#End Region

#Region "Function"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function generateCustCode(ByVal custname As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = custname.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select custcode from QL_mstcust where custcode like '" & custPrefix & "%' order by custcode desc "
        Dim x As Object = cKoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tambahNol(angka)
            End If
        End If
        Return retVal
    End Function

    Function tambahNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sMsg As String = ""
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            sMsg &= "- Session login anda habis !<br>"
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Session("consignee") = Nothing
            Dim userId As String = Session("UserID")
            Dim xSetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xSetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear() ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xSetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xSetRole
            Session("tblSA") = Nothing
            Session("tblCP") = Nothing
            Session("tblBA") = Nothing
            TabContainer1.ActiveTabIndex = 0
            Response.Redirect("Master_Customer.aspx")
        End If

        Page.Title = CompnyName & " - Data Customer Cabang"
        Session("idPage") = Request.QueryString("idPage")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah Anda yakin ingin menghapus data ini ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'") 

        If Not IsPostBack Then
            DDLBranch() : fDDLBranch()
            initDDL() : InitCoa() : bindCust("")
            'Disable Credit Limit when cash
            If Session("branch_id") <> "10" Then
                imbPrint.Visible = False
            End If

            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                initProvince(custcountryoid.SelectedValue)
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue) 
                initCountryPhoneCode(custcountryoid.SelectedValue)
                initCityPhoneCode(custcityoid.SelectedValue)
                I_U.Text = "New"
                TglGabung.Text = Format(GetServerTime(), "dd/MM/yyyy")
                UpdTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                UpdUser.Text = Session("UserID")
                lblLast.Visible = False : TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False : btnSave.Visible = True
                'BindConsigneeData(0)
                btnInfoNota.Visible = False
            Else
                initCountry()
                I_U.Text = "UPDATE"
                lblCreate.Visible = False
                With SDSData
                    .SelectParameters("custoid").DefaultValue = Session.Item("idPage")
                    .SelectParameters("cmpcode").DefaultValue = CompnyCode
                End With
                fillTextBox(Session("idPage"))
                TabContainer1.ActiveTabIndex = 1
                btnInfoNota.Visible = True 
            End If
        End If
    End Sub

    Protected Sub GVmstcst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstcust.PageIndexChanging
        GVmstcust.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbStatus.Checked = True Then
            sWhere &= " AND c.custflag='" & DDLStatus.SelectedValue & "'"
        End If
        If cbOverDue.Checked = True Then
            sWhere &= " AND (SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=c.custoid AND j.trnjualstatus='POST' AND branch_code=c.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) " & ddlOverDue.SelectedValue & ""
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub custcountryoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcountryoid.SelectedIndexChanged
        initProvince(custcountryoid.SelectedValue)
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        initCountryPhoneCode(custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub custprovoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custprovoid.SelectedIndexChanged
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbStatus.Checked = True Then
            sWhere &= " AND c.custflag='" & DDLStatus.SelectedValue & "'"
        End If
        If cbOverDue.Checked = True Then
            sWhere &= " AND (SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=c.custoid AND j.trnjualstatus='POST' AND branch_code=c.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) " & ddlOverDue.SelectedValue & ""
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = ""
        cbStatus.Checked = False
        ddlFilter.SelectedIndex = 0
        DDLStatus.SelectedIndex = 0
        cbOverDue.Checked = False
        bindCust("")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If Not Session("consignee") Is Nothing Then
            Session("consignee") = Nothing
        End If
        I_U.Text = "NEW"
        btnDelete.Visible = False
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\master\Master_Customer.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If custcode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode Customer !<br>"
        End If

        If precustname.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih Prefix Customer !<br>"
        End If

        If custname.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Nama Customer !<br>"
        End If

        If custname.Text.Length > 100 Then
            sMsg &= "- Nama Customer Max. 100 karakter !<br>"
        End If

        If contactperson1.Text = "" Then
            sMsg &= "- Silahkan isi Nama Pemilik !<br>"
        End If

        If custaddr.Text = "" Then
            sMsg &= "- Silahkan isi Alamat Customer !<br>"
        End If

        If custcityoid.SelectedValue = "" Then
            sMsg &= "- Silahkan pilih Kota Customer !<br>"
        End If

        If ddlconttitle.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 1 !<br>"
        End If

        If ddlconttitle2.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 2 !<br>"
        End If

        If ddlconttitle3.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 3 !<br>"
        End If

        If custcountryoid.SelectedValue = "" Then
            sMsg &= "- Silahkan pilih nama Negara !<br>"
        End If

        If custprovoid.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Kota dan Provinsi untuk Negara " & custcountryoid.SelectedItem.Text & "!<br>"
        End If

        If custprovoid.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih nama Provinsi !<br>"
        End If

        If custcityoid.Items.Count = 0 Then
            sMsg &= "- Silahkan pilih Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " atau buat Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " jika belum ada..!<br>"
        ElseIf custcityoid.SelectedValue = "-" Then
            sMsg &= "- Silahkan pilih Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " atau buat Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " jika belum ada..!<br>"
        End If

        If phone1.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Telepon Rumah !<br>"
        End If

        If phone2.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Telepon Kantor !<br>"
        End If

        If phone3.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Telepon HP/WA !<br>"
        End If

        If custemail.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat email tidak valid, Ex : mail@sample.com !!<BR>"
            End If
        End If

        If custwebsite.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custwebsite.Text, "\w\w\w\.([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat website tidak valid, Ex : www.sample.com !!<BR>"
            End If
        End If

        If notes.Text.Trim.Length > 500 Then
            sMsg &= "- Catatan tidak boleh lebih dari 500 karakter !<br>"
        End If

        Dim datebirth As New Date
        If tgllahir.Text.Trim <> "" Then
            If Date.TryParseExact(tgllahir.Text, "dd/MM/yyyy", Nothing, Nothing, datebirth) = False Then
                sMsg &= "- Tanggal lahir customer tidak valid !<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        ' Cek Nama - Alamat dan Kota yang sudah ada
        Dim sQuery As String = "select count(-1) from ql_mstcust where cmpcode='" & CompnyCode & "' " & _
            "AND custname= '" & Tchar(custname.Text) & "' and custaddr='" & Tchar(custaddr.Text) & "' " & _
            "and custcityoid=" & custcityoid.SelectedValue & " AND branch_code='" & branchID.SelectedValue & "' "
        If I_U.Text.ToLower <> "new" Then
            sQuery &= " and custoid <> " & Session("idPage")
        End If

        Dim iCount As Integer = ToDouble(GetStrData(sQuery))
        If iCount > 0 Then
            showMessage("Data Customer ini sudah ada :<BR>Cabang =" & branchID.SelectedItem.Text & "<BR>Nama = " & custname.Text & "<BR>Alamat =" & custaddr.Text & "<BR>Kota =" & custcityoid.SelectedItem.Text, CompnyName & "- Warning", 2)
            Exit Sub
        End If

        Dim tittlename As String = "" : Dim CustGroup As String = ""
        If precustname.SelectedItem.Text.ToLower = "other" Then
            tittlename = ""
        Else
            tittlename = "," & precustname.SelectedItem.Text
        End If

        If custgroupoid.Text = "" Then
            CustGroup = 0
        Else
            CustGroup = custgroupoid.Text
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                'New Insert
                custoid.Text = GenerateID("QL_mstcust", CompnyCode)
                'custcode.Text = generateCustCode(custname.Text.Trim)
                sSql = "INSERT INTO QL_mstcust (cmpcode, custoid, custgroupoid, branch_code, timeofpayment, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, statusAR, custpaytermdefaultoid, custcurrdefaultoid, custcreditlimitcurroid, custcreditlimit, custcreditlimitusage, custcreditlimitrupiah, custcreditlimitusagerupiah, custacctgoid, notes, resfield1, resfield2, createuser, upduser, updtime, cdphonecountry, cdphonecity, prefixcmp, prefixcp1, prefixcp2, prefixcp3, CustGroup, custbank, custpassportname, custpassportno, custbirthdate, salesoid, coa_retur, pin_bb, createtime) VALUES " & _
                "('" & CompnyCode & "', " & Tchar(CustOid.Text) & "," & CustGroup & ",'" & branchID.SelectedValue & "' ," & dd_timeofpayment.SelectedValue & ",'" & Tchar(custcode.Text) & "', '" & Tchar(custname.Text) & "', '" & custflag.SelectedValue & "', '" & Tchar(custaddr.Text) & "', " & custcityoid.SelectedValue & ", " & custprovoid.SelectedValue & ", " & custcountryoid.SelectedValue & ", '" & custpostcode.Text & "', '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & custfax1.Text & "', '', '" & Tchar(custemail.Text) & "', '" & Tchar(custwebsite.Text) & "', '" & Tchar(custnpwp.Text) & "', '" & Tchar(contactperson1.Text) & "', '" & Tchar(contactperson2.Text) & "', '" & Tchar(contactperson3.Text) & "', '" & phonecontactperson1.Text & "', '" & phonecontactperson2.Text & "', '" & phonecontactperson3.Text & "', '" & statusar.SelectedValue & "', 0, 0, 0, 0, 0, " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & araccount.SelectedValue & ", '" & Tchar(notes.Text) & "', '', '', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', " & precustname.SelectedValue & ", " & ddlconttitle.SelectedValue & ", " & ddlconttitle2.SelectedValue & ", " & ddlconttitle3.SelectedValue & ", '" & ddlcustgroup.SelectedValue.ToString & "', '" & Tchar(bankrekening.Text) & "', '" & Tchar(namapaspor.Text) & "', '" & Tchar(nopaspor.Text) & "', '" & datebirth & "'," & spgOid.SelectedValue & "," & returaccount.SelectedValue & ",'" & pin_bb.Text & "','" & CDate(toDate(TglGabung.Text)) & "')"

            Else

                sSql = "UPDATE QL_mstcust set branch_code='" & branchID.SelectedValue & "', timeofpayment=" & dd_timeofpayment.SelectedValue & ", custcode = '" & Tchar(custcode.Text) & "', custname = '" & Tchar(custname.Text) & "', custflag = '" & custflag.SelectedValue & "', custaddr = '" & Tchar(custaddr.Text) & "', custcityoid = " & custcityoid.SelectedValue & ", custprovoid = " & custprovoid.SelectedValue & ", custcountryoid = " & custcountryoid.SelectedValue & ", custpostcode = '" & custpostcode.Text & "', phone1 = '" & phone1.Text & "', phone2 = '" & phone2.Text & "', phone3 = '" & phone3.Text & "', custfax1 = '" & custfax1.Text & "', custemail = '" & Tchar(custemail.Text) & "', custwebsite = '" & Tchar(custwebsite.Text) & "', custnpwp = '" & Tchar(custnpwp.Text) & "', contactperson1 = '" & Tchar(contactperson1.Text) & "', contactperson2 = '" & Tchar(contactperson2.Text) & "', contactperson3 = '" & Tchar(contactperson3.Text) & "', phonecontactperson1 = '" & phonecontactperson1.Text & "', phonecontactperson2 = '" & phonecontactperson2.Text & "', phonecontactperson3 = '" & phonecontactperson3.Text & "', custcreditlimitrupiah = " & ToDouble(custcreditlimitrupiah.Text) & ", custcreditlimitusagerupiah = " & ToDouble(custcreditlimitusagerupiah.Text) & ", notes = '" & Tchar(notes.Text) & "', upduser = '" & Session("UserID") & "', updtime = current_timestamp, prefixcmp = " & precustname.SelectedValue & ", prefixcp1 = " & ddlconttitle.SelectedValue & ", prefixcp2 = " & ddlconttitle2.SelectedValue & ", prefixcp3 = " & ddlconttitle3.SelectedValue & ", CustGroup = '" & ddlcustgroup.SelectedValue.ToString & "', custbank = '" & Tchar(bankrekening.Text) & "', custpassportname = '" & Tchar(namapaspor.Text) & "', custpassportno = '" & Tchar(nopaspor.Text) & "', custbirthdate = '" & datebirth & "',salesoid = " & spgOid.SelectedValue & ", custacctgoid=" & araccount.SelectedValue & ", coa_retur=" & returaccount.SelectedValue & ", pin_bb='" & pin_bb.Text & "', custgroupoid = " & CustGroup & ", createtime='" & CDate(toDate(TglGabung.Text)) & "' WHERE cmpcode='" & CompnyCode & "' AND custoid=" & Session("idPage") & ""

            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Session("idPage") = Nothing Or Session("idPage") = "" Then
                sSql = "update QL_mstoid set lastoid=" & custoid.Text & " where tablename like 'QL_mstcust' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close() 
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br /> " & Ssql, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Session("idPage") = Nothing
        Session("consignee") = Nothing
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data tersimpan"))
        Response.Redirect("~\master\Master_Customer.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sColomnName() As String = {"REQCUSTOID", "trncustoid", "trncustoid"}
        Dim sTable() As String = {"QL_TRNREQUEST", "QL_trnordermst", "QL_trnjualmst"}
        If CheckDataExists(custoid.Text, sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain !", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstcust WHERE cmpcode = '" & CompnyCode & "' AND custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\master\Master_Customer.aspx?awal=true")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False : PanelErrMsg.Visible = False
    End Sub

    Protected Sub custcityoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcityoid.SelectedIndexChanged
        initCityPhoneCode(custcityoid.SelectedValue)
    End Sub

    Protected Sub custname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custname.TextChanged
         
        'HANYA NEW YANG GENERATE CODE
        If custname.Text.Trim <> "" Then
            If custname.Text.Length < 1 Then
                showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                custname.Text = ""
                custcode.Text = ""
                Exit Sub
            Else
                custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
            End If
        Else
            custcode.Text = ""
        End If
        Dim text As String = custname.Text.ToUpper
        custname.Text = text
    End Sub

    Protected Sub DropBrunch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles branchID.TextChanged
        Outlate()
        If custname.Text <> "" Then
            If Session("idPage") <> "" And Session("idPage") <> Nothing Then
            Else
                'HANYA NEW YANG GENERATE CODE
                If custname.Text.Trim <> "" Then
                    If custname.Text.Length < 1 Then
                        showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                        custname.Text = ""
                        custcode.Text = ""
                        Exit Sub
                    Else
                        custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
                    End If
                Else
                    custcode.Text = ""
                End If
            End If
        End If
    End Sub    

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "Customer_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "CUSTOMER" : imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoCust") = "" : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub branchID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitCoa()
    End Sub

    Protected Sub GVmstcust_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(6).Text = NewMaskEdit(ToDouble(e.Row.Cells(6).Text))
                e.Row.Cells(7).Text = NewMaskEdit(ToDouble(e.Row.Cells(7).Text))
                e.Row.Cells(8).Text = NewMaskEdit(ToDouble(e.Row.Cells(8).Text))
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 2)
        End Try
    End Sub

    Protected Sub custaddr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim text As String = custaddr.Text.ToUpper
        custaddr.Text = text
    End Sub

    Protected Sub notes_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim text As String = notes.Text.ToUpper
        notes.Text = text
    End Sub

    Protected Sub contactperson1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim text As String = contactperson1.Text.ToUpper
        contactperson1.Text = text
    End Sub

    Protected Sub btnFindCG_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindCG.Click
        BindCustGroup()
    End Sub

    Protected Sub btnClearCG_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCG.Click
        CustNameGroup.Text = "" : gvCustGroup.Visible = False
        custgroupoid.Text = ""
    End Sub

    Protected Sub gvCustGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustGroup.PageIndexChanging
        gvCustGroup.PageIndex = e.NewPageIndex
        BindCustGroup() : gvCustGroup.Visible = True
    End Sub

    Protected Sub gvCustGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCustGroup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
        End If
    End Sub

    Protected Sub gvCustGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCustGroup.SelectedIndexChanged
        custgroupoid.Text = gvCustGroup.SelectedDataKey("custgroupoid")
        CustNameGroup.Text = gvCustGroup.SelectedDataKey("custgroupname").ToString
        custcreditlimitrupiah.Text = ToMaskEdit(gvCustGroup.SelectedDataKey("LimitNya"), 4)
        custcreditlimitusagerupiah.Text = ToMaskEdit(gvCustGroup.SelectedDataKey("UsageNya"), 4)
        gvCustGroup.Visible = False
    End Sub

    Protected Sub btnInfoNota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInfoNota.Click
        DataBindTrn()
        mpeListMat.Show()
    End Sub

    Protected Sub gvListNya_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListNya.PageIndexChanging
        gvListNya.PageIndex = e.NewPageIndex
        DataBindTrn() : mpeListMat.Show()
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListNya.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
#End Region
End Class