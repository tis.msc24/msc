<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
CodeFile="mstItemhpp.aspx.vb" Inherits="mstItemhpp" Title="" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
<%--<script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=GVmstgen.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    }
    window.onload = s
  </script>--%>
  <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center" style="height: 28px">
                            <asp:Label ID="HDRBARANG" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Maroon" Text=":: Data Barang (Price List+HPP)"></asp:Label></th>
                    </tr>
                </table>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                
                    <contenttemplate>
 
</contenttemplate>           
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left;">
                                <asp:UpdatePanel id="UPanelListMaterial" runat="server">
                                    <contenttemplate>
<TABLE><TBODY><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" CssClass="normalFont" Text="Filter 1 :" __designer:wfdid="w172"></asp:Label></TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w173" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged"><asp:ListItem Selected="True" Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem>Merk</asp:ListItem>
<asp:ListItem Value="personname">PIC</asp:ListItem>
<asp:ListItem Value="QL_mstgen.gendesc">Grup Barang</asp:ListItem>
<asp:ListItem Value="gsub.gendesc">Sub Grup Barang</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round((itempriceunit1/konversi1_2)/konversi2_3,0))">Harga Qty</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round(itempriceunit2/konversi2_3,0))">Harga Partai</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),itempriceunit3)">Harga Ecer</asp:ListItem>
<asp:ListItem Value="g1.gendesc">Sat Besar</asp:ListItem>
<asp:ListItem Value="g2.gendesc">Sat Sdg</asp:ListItem>
<asp:ListItem Value="itemflag">Status</asp:ListItem>
</asp:DropDownList> </TD><TD align=left><asp:DropDownList id="ddlStatusHarga" runat="server" Width="40px" CssClass="inpText" Font-Bold="True" __designer:wfdid="w174" Visible="False"><asp:ListItem>V</asp:ListItem>
<asp:ListItem>X</asp:ListItem>
<asp:ListItem>O</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w175" MaxLength="30"></asp:TextBox> </TD><TD align=left><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton> </TD><TD align=left><asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton> </TD><TD align=left><asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label38" runat="server" CssClass="normalFont" Text="Filter 2 :" __designer:wfdid="w172"></asp:Label></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL2" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w6" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged"><asp:ListItem Selected="True">Merk</asp:ListItem>
<asp:ListItem Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="personname">PIC</asp:ListItem>
<asp:ListItem Value="QL_mstgen.gendesc">Grup Barang</asp:ListItem>
<asp:ListItem Value="gsub.gendesc">Sub Grup Barang</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round((itempriceunit1/konversi1_2)/konversi2_3,0))">Harga Qty</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round(itempriceunit2/konversi2_3,0))">Harga Partai</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),itempriceunit3)">Harga Ecer</asp:ListItem>
<asp:ListItem Value="g1.gendesc">Sat Besar</asp:ListItem>
<asp:ListItem Value="g2.gendesc">Sat Sdg</asp:ListItem>
<asp:ListItem Value="itemflag">Status</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText2" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w8" MaxLength="30"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label39" runat="server" CssClass="normalFont" Text="Filter 3 :" __designer:wfdid="w172"></asp:Label></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL3" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w7" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged"><asp:ListItem Selected="True" Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem>Merk</asp:ListItem>
<asp:ListItem Value="personname">PIC</asp:ListItem>
<asp:ListItem Value="QL_mstgen.gendesc">Grup Barang</asp:ListItem>
<asp:ListItem Value="gsub.gendesc">Sub Grup Barang</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round((itempriceunit1/konversi1_2)/konversi2_3,0))">Harga Qty</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),round(itempriceunit2/konversi2_3,0))">Harga Partai</asp:ListItem>
<asp:ListItem Value="convert(numeric(18,0),itempriceunit3)">Harga Ecer</asp:ListItem>
<asp:ListItem Value="g1.gendesc">Sat Besar</asp:ListItem>
<asp:ListItem Value="g2.gendesc">Sat Sdg</asp:ListItem>
<asp:ListItem Value="itemflag">Status</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText3" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w9" MaxLength="30"></asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left>Urutkan :</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="orderby" runat="server" Width="99px" CssClass="inpText" __designer:wfdid="w3" AutoPostBack="True"><asp:ListItem Value="itemdesc , personname">Nama</asp:ListItem>
<asp:ListItem Value="itemoid desc">ID Terakhir</asp:ListItem>
</asp:DropDownList></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left>Status :</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="ddlStatusView" runat="server" Width="99px" CssClass="inpText" __designer:wfdid="w7" AutoPostBack="True"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList>&nbsp;View Top <asp:TextBox id="SelectTop" runat="server" Width="30px" CssClass="inpText" __designer:wfdid="w8" MaxLength="30">100</asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fteTop" runat="server" __designer:wfdid="w9" TargetControlID="SelectTop" ValidChars=",.0123456789" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE><DIV style="DISPLAY: block; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w180"><ProgressTemplate>
<asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w181"></asp:Image><BR />
<span class="normalFont">Please wait...</span><BR />
</ProgressTemplate>
</asp:UpdateProgress></DIV><asp:Panel id="Panel1" runat="server" __designer:wfdid="w182" DefaultButton="btnSearch"><asp:GridView id="GVmstgen" runat="server" Width="100%" CssClass="normalFont" ForeColor="Black" __designer:dtid="1970324836974614" __designer:wfdid="w183" DataKeyNames="itemoid" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="Lavender" BorderColor="Cyan" __designer:dtid="1970324836974632"></RowStyle>
<Columns __designer:dtid="1970324836974622">
<asp:BoundField DataField="nomor" HeaderText="No" SortExpression="nomor" Visible="False"></asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="itemoid" DataNavigateUrlFormatString="mstItemhpp.aspx?oid={0}" DataTextField="itemcode" HeaderText="Kode Barang" SortExpression="itemcode" __designer:dtid="1970324836974623">
<ControlStyle Font-Size="Small" __designer:dtid="1970324836974626"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="15%" __designer:dtid="1970324836974625"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="Small" Width="15%" __designer:dtid="1970324836974624"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang" SortExpression="itemdesc" __designer:dtid="1970324836974627">
<HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="65%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="Small" Width="65%" __designer:dtid="1970324836974628"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk"></asp:BoundField>
<asp:BoundField DataField="bottompricegrosir" HeaderText="PriceList" SortExpression="bottompricegrosir"></asp:BoundField>
<asp:BoundField DataField="itempriceunit1" HeaderText="Harga Qty" SortExpression="itempriceunit1">
<ItemStyle Font-Size="Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Unit1 (toUnit3)" SortExpression="satuan1"></asp:BoundField>
<asp:BoundField DataField="itempriceunit2" HeaderText="Harga Partai" SortExpression="itempriceunit2">
<ItemStyle Font-Size="Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Unit2 (toUnit3)" SortExpression="satuan2"></asp:BoundField>
<asp:BoundField DataField="itempriceunit3" HeaderText="Harga Ecer" SortExpression="itempriceunit3" __designer:dtid="1970324836974630">
<ItemStyle Font-Size="Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Unit3"></asp:BoundField>
<asp:BoundField DataField="Stock" HeaderText="G.Stock Grosir"></asp:BoundField>
<asp:BoundField DataField="stockGUDANG35" HeaderText="G.Stock Retail">
<HeaderStyle ForeColor="Red"></HeaderStyle>

<ItemStyle ForeColor="Red"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockSupp" HeaderText="G.Supplier"></asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="btnprintlist" onclick="btnprintlist_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3" ToolTip='<%#eval("itemcode") %>' CommandArgument='<%#eval("itemoid") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" __designer:dtid="1970324836974616"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="WhiteSmoke" Font-Bold="False" ForeColor="DarkRed" __designer:dtid="1970324836974633"></PagerStyle>
<EmptyDataTemplate __designer:dtid="1970324836974619">
<asp:Label id="Label5" runat="server" __designer:dtid="1970324836974620" CssClass="Important" Text="Data Tidak Ditemukan !!!" __designer:wfdid="w4"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" __designer:dtid="1970324836974617"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" __designer:dtid="1970324836974615"></HeaderStyle>

<EditRowStyle BackColor="#999999" __designer:dtid="1970324836974621"></EditRowStyle>

<AlternatingRowStyle BackColor="White" ForeColor="#284775" __designer:dtid="1970324836974618"></AlternatingRowStyle>
</asp:GridView></asp:Panel> 
</contenttemplate>
                                    <triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</triggers>
                                </asp:UpdatePanel>&nbsp; &nbsp;&nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Daftar Barang :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    &nbsp; &nbsp;&nbsp;
                    &nbsp; &nbsp;&nbsp;
                    &nbsp; &nbsp;&nbsp;
                    &nbsp; &nbsp;&nbsp;
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<table><tr><td align="left" class="Label" style="width: 229px; height: 18px"><asp:TextBox ID="itemoid" runat="server" CssClass="inpText" Enabled="False" MaxLength="10"
                                            size="20" Visible="False" Width="25px"></asp:TextBox> </td><td align="left" style="width: 212px; height: 18px"><asp:Label ID="i_u" runat="server" Font-Names="Verdana" Font-Size="8pt" ForeColor="Red"
                                            Text="New" Visible="False"></asp:Label> </td><td align="left" style="width: 38px; color: #000099; height: 18px"></td><td align="left" style="width: 145px; color: #000099; height: 18px">&nbsp;</td><td align="left" style="width: 165px; color: #000099; height: 18px">&nbsp;</td><td align="left" style="width: 110px; color: #000099; height: 18px"></td><td align="left" style="color: #000099; height: 18px"></td></tr><tr style="color: #000099"><td align="left" class="Label" style="font-size: 12px; font-weight: normal; width: 229px;">Kode Barang <span style="color: #ff0000">*</span></td><td align="left" style="font-weight: normal; font-size: 11px;"><asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<asp:TextBox id="itemcode" runat="server" Width="152px" CssClass="inpTextDisabled" size="20" Enabled="False" MaxLength="20" __designer:wfdid="w84"></asp:TextBox> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="font-weight: normal; font-size: 11px;"></td><td align="left" style="font-size: 12px; font-weight: normal;">Nama Barang <span style="color: #ff0000">*</span></td><td align="left" style="font-weight: normal; font-size: 11px;" colspan="3"><asp:UpdatePanel id="UpdatePanel1" runat="server"><contenttemplate>
<asp:TextBox id="itemdesc" runat="server" Width="298px" Height="16px" CssClass="inpText" Font-Size="Small" size="20" MaxLength="250" __designer:wfdid="w86" AutoPostBack="True"></asp:TextBox> 
</contenttemplate>
</asp:UpdatePanel> &nbsp;&nbsp;</td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px" valign="top"><asp:Label ID="Label22" runat="server" Text="Grup Barang" Width="87px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:UpdatePanel id="UpdatePanel17" runat="server"><contenttemplate><asp:DropDownList ID="itemgroupoid" runat="server" CssClass="inpText"
                                            Width="207px" Font-Size="Small" AutoPostBack="True" OnSelectedIndexChanged="itemgroupoid_SelectedIndexChanged"></asp:DropDownList> </contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px" valign="top"><asp:Label ID="Label21" runat="server" Text="Sub Grup Barang" Width="122px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 165px; height: 10px" valign="top"><asp:UpdatePanel id="UpdatePanel18" runat="server"><contenttemplate><asp:DropDownList ID="itemsubgroupoid" runat="server" CssClass="inpText"
                                            Width="200px" Font-Size="Small"></asp:DropDownList> </contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px; height: 10px" valign="top">Merk <span style="color: #ff0000">*</span></td><td align="left" style="height: 10px" valign="top"><asp:TextBox ID="merk" runat="server" CssClass="inpText" Font-Size="Small" MaxLength="50"
                                            size="20" Width="151px"></asp:TextBox> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px" valign="top"><asp:Label ID="Label3" runat="server" Text="Sat Bsr" Width="109px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:DropDownList ID="satuan1" runat="server" CssClass="inpText"
                                            Width="112px" Font-Size="Small"></asp:DropDownList> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px" valign="top"><asp:Label ID="Label18" runat="server" Text="Sat Sdg" Width="109px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 165px; height: 10px" valign="top"><asp:DropDownList ID="satuan2" runat="server" CssClass="inpText"
                                            Width="112px" Font-Size="Small"></asp:DropDownList> </td><td align="left" style="width: 110px; height: 10px" valign="top"><asp:Label ID="Label20" runat="server" Text="Sat Std" Width="109px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="height: 10px" valign="top"><asp:DropDownList ID="satuan3" runat="server" CssClass="inpText"
                                            Width="112px" Font-Size="Small"></asp:DropDownList> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px" valign="top"><asp:Label ID="Label2" runat="server" CssClass="normalFont" Font-Size="Small" Text="Konversi Unit 1-2"
                                            Width="122px"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:UpdatePanel id="UpdatePanel7" runat="server"><contenttemplate>
<asp:TextBox id="konversi1_2" runat="server" Width="96px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w15" AutoPostBack="True" OnTextChanged="konversi1_2_TextChanged">1</asp:TextBox> <BR /><ajaxToolkit:FilteredTextBoxExtender id="fte7" runat="server" __designer:dtid="281474976710815" Enabled="True" TargetControlID="konversi1_2" ValidChars="0123456789" __designer:wfdid="w16"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px" valign="top"><asp:Label ID="Label1" runat="server" CssClass="normalFont" Font-Size="Small" Text="Konversi Unit 2-3" Width="122px"></asp:Label> </td><td align="left" style="width: 165px; height: 10px" valign="top"><asp:UpdatePanel id="UpdatePanel6" runat="server"><contenttemplate>
<asp:TextBox id="konversi2_3" runat="server" Width="106px" CssClass="inpText" Font-Size="Small" MaxLength="4" __designer:wfdid="w21" AutoPostBack="True" OnTextChanged="konversi2_3_TextChanged">1</asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="fte8" runat="server" __designer:dtid="281474976710816" Enabled="True" TargetControlID="konversi2_3" ValidChars="0123456789" __designer:wfdid="w22"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px; height: 10px" valign="top"></td><td align="left" style="height: 10px" valign="top"></td></tr>
    <tr>
        <td align="left" class="Label" style="width: 229px; height: 10px" valign="top">
            <asp:Label ID="Label37" runat="server" CssClass="normalFont" Font-Size="Small" Text="Qty Min Sat Std-Bsr"
                Width="109px"></asp:Label></td>
        <td align="left" style="width: 212px; height: 10px">
            <asp:UpdatePanel id="UpdatePanel24" runat="server"><contenttemplate>
<asp:TextBox id="Qty3_to1" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w25" AutoPostBack="True" OnTextChanged="qty2_to1_TextChanged">1</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte25" runat="server" Enabled="True" TargetControlID="qty3_to1" ValidChars=",.0123456789" __designer:wfdid="w26"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel></td>
        <td align="left" style="width: 38px; height: 10px">
        </td>
        <td align="left" style="width: 145px; height: 10px" valign="top">
            <asp:Label ID="Label36" runat="server" CssClass="normalFont" Font-Size="Small" Text="Qty Min Sat Std-Sdg"
                Width="109px"></asp:Label></td>
        <td align="left" style="width: 165px; height: 10px" valign="top">
            <asp:UpdatePanel id="UpdatePanel23" runat="server"><contenttemplate>
<asp:TextBox id="qty3_to2" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w29" AutoPostBack="True" OnTextChanged="qty3_to2_TextChanged">1</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte24" runat="server" Enabled="True" TargetControlID="qty3_to2" ValidChars=",.0123456789" __designer:wfdid="w30"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel></td>
        <td align="left" style="width: 110px; height: 10px" valign="top">
        </td>
        <td align="left" style="height: 10px" valign="top">
        </td>
    </tr>
    <tr><td align="left" class="Label" style="width: 229px; height: 10px"><asp:Label ID="Label10" runat="server" Text="Harga Qty" Width="123px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:UpdatePanel id="UpdatePanel3" runat="server"><contenttemplate>
<asp:TextBox id="itempriceunit1" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w52" AutoPostBack="True" MaxLength="9" OnTextChanged="itempriceunit1_TextChanged"></asp:TextBox> <asp:Label id="lblhrgQtyPcs" runat="server" Text="0" __designer:wfdid="w1"></asp:Label> / Ecer<ajaxToolkit:FilteredTextBoxExtender id="fte4" runat="server" __designer:wfdid="w53" TargetControlID="itempriceunit1" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px"><asp:Label ID="Label23" runat="server" Text="Barcode Sat Bsr" Width="122px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td style="width: 165px; height: 10px"><asp:TextBox ID="itembarcode1" runat="server" CssClass="inpText"
                                            MaxLength="25" size="20" Width="151px" Font-Size="Small"></asp:TextBox> </td><td align="left" style="width: 110px; height: 10px"><asp:Label ID="Label26" runat="server" CssClass="normalFont" Font-Size="Small" Text="Disc Harga Bsr"
        Width="123px"></asp:Label> </td><td align="left" style="height: 10px"><asp:UpdatePanel id="UpdatePanel20" runat="server"><contenttemplate>
<asp:TextBox id="DiscUnit1" runat="server" Width="70px" CssClass="inpText" Font-Size="Small" MaxLength="5" __designer:wfdid="w2" AutoPostBack="True" OnTextChanged="DiscUnit1_TextChanged"></asp:TextBox> %<ajaxToolkit:FilteredTextBoxExtender id="fte21" runat="server" Enabled="True" TargetControlID="DiscUnit1" ValidChars=",.0123456789" __designer:wfdid="w3"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px"><asp:Label ID="Label14" runat="server" Text="Harga Partai" Width="123px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:UpdatePanel id="UpdatePanel4" runat="server"><contenttemplate>
<asp:TextBox id="itempriceunit2" runat="server" Width="111px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w40" AutoPostBack="True" MaxLength="9" OnTextChanged="itempriceunit2_TextChanged"></asp:TextBox>&nbsp;<asp:Label id="lblPrtPcs" runat="server" Text="0" __designer:wfdid="w2"></asp:Label>&nbsp;/ Ecer<BR /><ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" __designer:wfdid="w41" TargetControlID="itempriceunit2" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px"><asp:Label ID="Label13" runat="server" Text="Barcode Sat Sdg" Width="122px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td style="width: 165px; height: 10px"><asp:TextBox ID="itembarcode2" runat="server" CssClass="inpText"
                                            MaxLength="25" size="20" Width="151px" Font-Size="Small"></asp:TextBox> </td><td align="left" style="width: 110px; height: 10px"><asp:Label ID="Label28" runat="server" CssClass="normalFont" Font-Size="Small" Text="Disc Harga Stg"
        Width="123px"></asp:Label> </td><td align="left" style="height: 10px"><asp:UpdatePanel id="UpdatePanel21" runat="server"><contenttemplate>
<asp:TextBox id="DiscUnit2" runat="server" Width="72px" CssClass="inpText" Font-Size="Small" MaxLength="5" __designer:wfdid="w5" AutoPostBack="True" OnTextChanged="DiscUnit2_TextChanged"></asp:TextBox> %<ajaxToolkit:FilteredTextBoxExtender id="fte22" runat="server" Enabled="True" TargetControlID="DiscUnit2" ValidChars=",.0123456789" __designer:wfdid="w6"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px"><asp:Label ID="Label15" runat="server" Text="Harga Ecer" Width="123px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:UpdatePanel id="UpdatePanel5" runat="server"><contenttemplate>
<asp:TextBox id="itempriceunit3" runat="server" Width="109px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w5" AutoPostBack="True" OnTextChanged="itempriceunit3_TextChanged"></asp:TextBox>&nbsp; <BR /><ajaxToolkit:FilteredTextBoxExtender id="fte6" runat="server" Enabled="True" TargetControlID="itempriceunit3" ValidChars=",.0123456789" __designer:wfdid="w6"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px"><asp:Label ID="Label24" runat="server" Text="Barcode Sat Std" Width="122px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td style="width: 165px; height: 10px"><asp:TextBox ID="itembarcode3" runat="server" CssClass="inpText"
                                            MaxLength="25" size="20" Width="151px" Font-Size="Small"></asp:TextBox> </td><td align="left" style="width: 110px; height: 10px"><asp:Label ID="Label31" runat="server" CssClass="normalFont" Font-Size="Small" Text="Disc Harga Std" Width="123px"></asp:Label> </td><td align="left" style="height: 10px"><asp:UpdatePanel id="UpdatePanel22" runat="server"><contenttemplate>
<asp:TextBox id="DiscUnit3" runat="server" Width="72px" CssClass="inpText" Font-Size="Small" MaxLength="5" __designer:wfdid="w8" AutoPostBack="True" OnTextChanged="DiscUnit3_TextChanged"></asp:TextBox> %<ajaxToolkit:FilteredTextBoxExtender id="fte23" runat="server" Enabled="True" TargetControlID="DiscUnit3" ValidChars=",.0123456789" __designer:wfdid="w9"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px">
    <asp:Label ID="Label41" runat="server" CssClass="normalFont" Font-Size="Small" Text="HPP"
        Visible="True" Width="62px"></asp:Label>
    <asp:Label ID="Label40" runat="server" CssClass="normalFont" Font-Size="Small" Text="%x to BtmPrice"
        Visible="True" Width="115px"></asp:Label>&nbsp;
    <br />
</td><td align="left" style="width: 212px; height: 10px">
    <asp:TextBox ID="hpp" runat="server" CssClass="inpTextDisabled" OnTextChanged="hpp_TextChanged"
        ReadOnly="True" Width="111px"></asp:TextBox>
    <asp:UpdatePanel id="UpdatePanel25" runat="server">
        <contenttemplate>
<asp:TextBox id="persentobtmprice" runat="server" Width="93px" CssClass="inpText"  AutoPostBack="True"  MaxLength="4" OnTextChanged="persentobtmprice_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="persentobtmprice" ValidChars="1234567890."></ajaxToolkit:FilteredTextBoxExtender>
</contenttemplate>
    </asp:UpdatePanel></td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px">
    &nbsp;<asp:Label ID="Label29" runat="server" CssClass="normalFont" Font-Size="Small" Text="Bottom Price Grosir"
        Width="123px"></asp:Label></td><td style="width: 165px; height: 10px">
            &nbsp;<asp:UpdatePanel id="UpdatePanel12" runat="server"><contenttemplate>
<asp:TextBox id="bottompriceGrosir" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w14" MaxLength="9" AutoPostBack="True" OnTextChanged="bottompriceGrosir_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte13" runat="server" __designer:wfdid="w15" TargetControlID="bottompriceGrosir" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px; height: 10px"><span style="font-size: 10pt"><asp:Label ID="Label32" runat="server" CssClass="normalFont" Font-Size="Small" Text="PriceList"
        Width="123px"></asp:Label></span></td><td align="left" style="height: 10px">
            &nbsp;<asp:UpdatePanel id="UpdatePanel19" runat="server"><contenttemplate>
<asp:TextBox id="PriceList" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w12" MaxLength="9" AutoPostBack="True" OnTextChanged="PriceList_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte20" runat="server" __designer:wfdid="w13" TargetControlID="PriceList" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px;"><asp:Label ID="spg" runat="server" Text="PIC" Width="109px" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px;"><asp:DropDownList ID="spgOid" runat="server" CssClass="inpText"
                                            Width="112px" Font-Size="Small"></asp:DropDownList> </td><td align="left" style="width: 38px;"></td><td align="left" style="width: 145px;"><span style="font-size: 10pt">Payment</span></td><td style="width: 165px;"><asp:DropDownList ID="payment" runat="server" CssClass="inpText" Font-Size="Small"
                                                    Width="112px"></asp:DropDownList> </td><td align="left" style="width: 110px;">
                                                        <asp:Label ID="Label30" runat="server" CssClass="normalFont" Font-Size="Small" Text="Harga Bawah Retail"
                                                            Visible="False" Width="123px"></asp:Label></td><td align="left"><asp:UpdatePanel id="UpdatePanel13" runat="server" Visible="False"><contenttemplate>
<asp:TextBox id="bottompriceRetail" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w1" MaxLength="9" AutoPostBack="True" OnTextChanged="bottompriceRetail_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte14" runat="server" __designer:wfdid="w2" TargetControlID="bottompriceRetail" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px">
                                                        &nbsp;</td><td align="left" style="width: 212px; height: 10px">
                                                        &nbsp;</td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="width: 145px; height: 10px">
                                                        &nbsp;</td><td style="width: 165px; height: 10px">
                                                        &nbsp;</td><td align="left" style="width: 110px; height: 10px"></td><td align="left" style="height: 10px"></td></tr><tr><td align="left" class="Label" style="width: 229px; height: 29px"><asp:Label ID="Label33" runat="server" CssClass="normalFont" Font-Size="Small" Text="Harga Barang1 Retail"
                                                            Visible="False" Width="123px"></asp:Label> </td><td style="width: 212px; height: 29px"><asp:UpdatePanel id="UpdatePanel10" runat="server" Visible="False"><contenttemplate>
<asp:TextBox id="itempriceunit1retail" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w48" AutoPostBack="True" OnTextChanged="itempriceunit1retail_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte11" runat="server" Enabled="True" TargetControlID="itempriceunit1retail" ValidChars=",.0123456789" __designer:wfdid="w49"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 38px; height: 29px"></td><td align="left" style="font-weight: normal; font-size: 12px"><asp:Label ID="Label34" runat="server" CssClass="normalFont" Font-Size="Small" Text="Harga Barang2 Retail"
        Visible="False" Width="123px"></asp:Label> </td><td style="width: 165px; height: 29px"><asp:UpdatePanel id="UpdatePanel11" runat="server" Visible="False"><contenttemplate>
<asp:TextBox id="itempriceunit2retail" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w51" AutoPostBack="True" OnTextChanged="itempriceunit2retail_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte12" runat="server" Enabled="True" TargetControlID="itempriceunit2retail" ValidChars=",.0123456789" __designer:wfdid="w52"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px; height: 29px"><asp:Label ID="Label35" runat="server" CssClass="normalFont" Font-Size="Small" Text="Harga Barang3 Retail"
        Visible="False" Width="123px"></asp:Label> </td><td align="left" style="height: 29px"><asp:UpdatePanel id="UpdatePanel14" runat="server" Visible="False"><contenttemplate>
<asp:TextBox id="itempriceunit3retail" runat="server" Width="110px" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w54" AutoPostBack="True" OnTextChanged="itempriceunit3retail_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte15" runat="server" Enabled="True" TargetControlID="itempriceunit3retail" ValidChars=",.0123456789" __designer:wfdid="w55"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px"><asp:Label ID="Label42" runat="server" CssClass="normalFont" Font-Bold="True" Font-Size="Small"
                                            Text="� Info Last Supplier" Width="143px"></asp:Label></td><td align="left" style="width: 212px; height: 10px">
                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="inpTextDisabled"
                                                    Width="290px" ReadOnly="True"></asp:TextBox></td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="font-weight: normal; font-size: 11px"></td><td align="left" style="width: 165px; height: 10px"></td><td align="left" style="width: 110px; height: 10px"></td><td align="left" style="height: 10px"></td></tr>
    <tr>
        <td align="left" class="Label" style="width: 229px; height: 10px">
            <asp:Label ID="Label7" runat="server" CssClass="normalFont" Font-Bold="True" Font-Size="Small"
                Text="� Info Harga Beli"></asp:Label></td>
        <td align="left" style="width: 212px; height: 10px">
        </td>
        <td align="left" style="width: 38px; height: 10px">
        </td>
        <td align="left" style="font-weight: normal; font-size: 11px">
        </td>
        <td align="left" style="width: 165px; height: 10px">
        </td>
        <td align="left" style="width: 110px; height: 10px">
        </td>
        <td align="left" style="height: 10px">
        </td>
    </tr>
    <tr><td align="left" class="Label" style="width: 229px; height: 34px"><asp:Label ID="Label9" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Harga Sat Bsr :"></asp:Label> </td><td align="left" style="width: 212px; height: 34px"><asp:Label ID="Label27" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Rp. "></asp:Label> &nbsp;<asp:Label ID="lblHrgBeliBsr" runat="server" CssClass="normalFont"
                                                Font-Bold="False" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 38px; height: 34px"></td><td align="left" style="font-weight: normal; font-size: 11px; height: 34px"><asp:Label ID="Label17" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Harga Sat Sdg :"></asp:Label> </td><td align="left" style="width: 165px; height: 34px"><asp:Label ID="Label11" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Rp. "></asp:Label> <asp:Label ID="lblHrgBeliSdg" runat="server" CssClass="normalFont" Font-Bold="False"
                                            Font-Size="Small"></asp:Label> </td><td align="left" style="width: 110px; height: 34px"><asp:Label ID="Label25" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Harga Sat Kcl :"></asp:Label> </td><td align="left" style="height: 34px"><asp:Label ID="Label19" runat="server" CssClass="normalFont" Font-Bold="False" Font-Size="Small"
                                            Text="Rp. "></asp:Label> <asp:Label ID="lblHrgBeliKcl" runat="server" CssClass="normalFont" Font-Bold="False"
                                            Font-Size="Small"></asp:Label> </td></tr><tr><td align="left" class="Label" style="width: 229px; height: 10px"><asp:Label ID="Label12" runat="server" Text="Status" CssClass="normalFont" Font-Size="Small"></asp:Label> </td><td align="left" style="width: 212px; height: 10px"><asp:DropDownList ID="ddlStatus" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList> </td><td align="left" style="width: 38px; height: 10px"></td><td align="left" style="font-weight: normal; font-size: 11px;"><span style="font-size: 12px; font-weight: normal;">Safety Stock</span> Retail <span style="color: #ff0000">*</span></td><td align="left" style="width: 165px; height: 10px"><asp:UpdatePanel id="UpdatePanel8" runat="server"><contenttemplate>
<asp:TextBox id="itemsafetystock" runat="server" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w139" AutoPostBack="True" OnTextChanged="itemsafetystock_TextChanged">1</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte9" runat="server" __designer:dtid="281474976710816" Enabled="True" TargetControlID="itemsafetystock" ValidChars="0123456789" __designer:wfdid="w140"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px; height: 10px"><span style="font-size: 9pt">Safety Stock</span> Grosir</td><td align="left" style="height: 10px"><asp:UpdatePanel id="UpdatePanel15" runat="server"><contenttemplate>
<asp:TextBox id="itemsafetystockgrosir" runat="server" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w13" AutoPostBack="True" OnTextChanged="itemsafetystockgrosir_TextChanged">1</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte119" runat="server" __designer:dtid="281474976710816" Enabled="True" TargetControlID="itemsafetystockgrosir" ValidChars="0123456789" __designer:wfdid="w14"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px">Edit Harga pd Nota</td><td align="left" style="width: 212px"><asp:DropDownList ID="editprice" runat="server" CssClass="inpText"
                                            Width="73px" Font-Size="Small"><asp:ListItem Value="T">Ya</asp:ListItem>
<asp:ListItem Value="F">Tidak</asp:ListItem>
</asp:DropDownList> </td><td align="left" style="width: 38px"></td><td align="left" style="font-weight: normal; font-size: 11px">Max Stock Retail <span style="color: #ff0000">*</span></td><td align="left" style="width: 165px"><asp:UpdatePanel id="UpdatePanel9" runat="server"><contenttemplate>
<asp:TextBox id="Maxstock" runat="server" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w21" AutoPostBack="True" OnTextChanged="Maxstock_TextChanged">100</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte10" runat="server" __designer:dtid="281474976710816" Enabled="True" TargetControlID="Maxstock" ValidChars="0123456789" __designer:wfdid="w22"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td><td align="left" style="width: 110px">Max Stock Grosir</td><td align="left"><asp:UpdatePanel id="UpdatePanel16" runat="server"><contenttemplate>
<asp:TextBox id="Maxstockgrosir" runat="server" CssClass="inpText" Font-Size="Small" MaxLength="9" __designer:wfdid="w11" AutoPostBack="True" OnTextChanged="Maxstockgrosir_TextChanged">100</asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="fte1110" runat="server" __designer:dtid="281474976710816" Enabled="True" TargetControlID="Maxstockgrosir" ValidChars="0123456789" __designer:wfdid="w12"></ajaxToolkit:FilteredTextBoxExtender> 
</contenttemplate>
</asp:UpdatePanel> </td></tr><tr><td align="left" class="Label" style="width: 229px;"><asp:Label ID="Label16" runat="server" Text="Gambar" CssClass="normalFont"></asp:Label> </td><td align="left" colspan="4"><asp:FileUpload ID="FileUploadInsert" runat="server" CssClass="inpText"
                                            Width="249px" /> <asp:Button ID="btnUpload" runat="server" CssClass="tombol" Text="Upload" /> <br /><asp:Image ID="imgPerson" runat="server" Height="164px" ImageAlign="Top" Width="181px" /> <asp:Label ID="lblMenuPic" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red"
                                            Text="itempicture" Visible="False"></asp:Label> </td><td align="left" style="width: 110px;"><asp:Label ID="Label6" runat="server" Height="11px" Text="Kode Akun"
                                            Width="12px" CssClass="normalFont" Font-Size="Small" Visible="False"></asp:Label> </td><td align="left"><asp:DropDownList ID="acctgoid" runat="server" CssClass="inpText"
                                            Width="59px" Font-Size="Small" Visible="False"></asp:DropDownList> </td></tr><tr><td align="left" class="Label" style="width: 229px"><asp:Label ID="Label4" runat="server" CssClass="normalFont" Font-Size="Small" Text="Keterangan"></asp:Label> </td><td align="left" colspan="6"><asp:TextBox ID="keterangan" runat="server" CssClass="inpText" Font-Size="Small"
                                            MaxLength="100" Width="559px"></asp:TextBox> </td></tr><tr><td align="left" class="Label" style="width: 229px;"></td><td align="left" style="width: 212px;"> </td><td align="left" style="width: 38px;"></td><td align="left" style="width: 145px;">&nbsp; &nbsp; </td><td align="left" style="width: 165px;">&nbsp; &nbsp; </td><td align="left" style="width: 110px;"></td><td align="left"></td></tr></table><table><tr><td align="left" class="Label" style="color: #585858; height: 14px"><asp:Label ID="lblupd" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label> by &nbsp;<asp:Label ID="Upduser" runat="server" Font-Bold="True" Font-Size="X-Small"
                                                    ForeColor="#585858"></asp:Label> on &nbsp;<asp:Label ID="Updtime" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#585858"></asp:Label> &nbsp; </td></tr><tr><td align="left"><asp:ImageButton ID="btnSave" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Save.png" /> <asp:ImageButton ID="BtnCancel" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Cancel.png" /> <asp:ImageButton ID="btnDelete" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Delete.png" /> </td></tr></table>
</ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Barang :.</span></strong>
                        
</HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                                
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                </td>
        </tr>
    </table>
<asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
<contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label><BR /></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" DropShadow="True" PopupControlID="Panelvalidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="label15" Drag="True" TargetControlID="ButtonExtendervalidasi"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
</asp:UpdatePanel> 
    <asp:UpdatePanel id="UpdPanelPrint" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Barang"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" align=left></TD><TD style="HEIGHT: 20px" align=left></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" TargetControlID="btnHidePrint" Drag="True" PopupDragHandleControlID="lblPrint" BackgroundCssClass="modalBackground" PopupControlID="pnlPrint"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>
