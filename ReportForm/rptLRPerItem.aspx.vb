Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptPerItem
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLcbg, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcbg, sSql)
            Else
                FillDDL(DDLcbg, sSql)
                DDLcbg.Items.Add(New ListItem("ALL", "ALL"))
                DDLcbg.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLcbg, sSql)
            DDLcbg.Items.Add(New ListItem("ALL", "ALL"))
            DDLcbg.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitddlSales()
        sSql = "SELECT DISTINCT personoid, personname FROM ql_mstperson p INNER JOIN QL_MSTPROF u ON u.personnoid=p.PERSONOID Where personnoid IN (Select spgoid from QL_trnjualmst ) AND personnoid <> 0"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND u.BRANCH_CODE='" & Session("branch_id") & "'"
            FillDDL(DDLcbg, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND u.BRANCH_CODE='" & Session("branch_id") & "'"
                FillDDL(DDLcbg, sSql)
            Else
                FillDDL(ddlSales, sSql)
                ddlSales.Items.Add("ALL")
                ddlSales.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlSales, sSql)
            ddlSales.Items.Add("ALL")
            ddlSales.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub 

    Private Sub bindDataListGudang()
        'sSql = "select genoid, gencode, gendesc  from ql_mstgen " & _
        '"where gengroup='WAREHOUSE' and (gendesc like '%" & Tchar(mtrlocname.Text) & "%' or gencode like '%" & Tchar(mtrlocname.Text) & "%')"
        'FillGV(gvGudang, sSql, "gudang")
        'gvGudang.Visible = True
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Dim sWhere As String = "", vSql = ""
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                showMessage("Tanggal awal harus <= Tanggal akhir", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Format tanggal salah", 2)
            Exit Sub
        End Try
        Try
            Report = New ReportDocument
            If tipe = "excel" Then
                Report.Load(Server.MapPath("~\Report\rptLRPerItemExl.rpt"))
            Else
                Report.Load(Server.MapPath("~\Report\rptLRPerItem.rpt"))
            End If
            sWhere &= " Where convert(char(10),m.trnjualdate,121) between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "' "
            If ddlSales.SelectedValue <> "ALL" Then
                sWhere &= " and m.salesoid='" & ddlSales.SelectedValue & "'"
            End If

            If DDLcbg.SelectedValue <> "ALL" Then
                sWhere &= " And m.branch_code='" & DDLcbg.SelectedValue & "' "
            End If

            If ItemName.Text <> "" Then
                Dim sMatcode() As String = Split(ItemName.Text, ";")
                vSql = " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    vSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    If c1 < sMatcode.Length - 1 Then
                        vSql &= " OR"
                    End If
                Next
                vSql &= ")"
                sWhere &= vSql
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                vSql = " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    vSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        vSql &= " OR "
                    End If
                Next
                vSql &= ")"
                sWhere &= vSql
            End If

            cProc.SetDBLogonForReport(Report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Report.SetParameterValue("dateAwal", dateAwal.Text)
            Report.SetParameterValue("dateAkhir", dateAkhir.Text)
            Report.SetParameterValue("sWhere1", sWhere)
            Report.SetParameterValue("reportName", Report.FileName.Substring(Report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "view" Then
                Session("diprint") = "True"
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = Report
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Rpt_LRperItem_" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close() : Report.Dispose()
                Response.Redirect("~\ReportForm\rptLRPerItem.aspx?awal=true")
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Rpt_LRperItem_" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close() : Report.Dispose()
                Response.Redirect("~\ReportForm\rptLRPerItem.aspx?awal=true")
            End If
        Catch ex As Exception
            showMessage(ex.Message & "<br /><br />" & ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT 'False' AS checkvalue, custoid, custcode, custname, custaddr, (Select gendesc from ql_mstgen WHere gengroup='CABANG' AND gencode=c.branch_code) Cabang FROM QL_mstcust c WHERE cmpcode='" & CompnyCode & "' AND c.custoid IN (Select o.trncustoid From ql_trnjualmst o Where o.trncustoid=c.custoid AND c.branch_code=o.branch_code AND convert(char(10),o.trnjualdate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "') "
        If DDLcbg.SelectedValue <> "ALL" Then
            sSql &= " AND c.branch_code='" & DDLcbg.SelectedValue & "'"
        End If
        sSql &= " ORDER BY custcode"
        Session("TblCust") = cKon.ambiltabel(sSql, "QL_mstcust")
    End Sub

    Private Sub BindListMat()
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        sSql = "SELECT 'False' AS checkvalue, i.itemoid,i.itemcode, i.itemdesc, i.itemcode,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya,i.itemgroupoid,'Buah' satuan3, i.merk,i.stockflag,i.personoid,i.itemflag FROM QL_mstitem i Where itemoid IN (Select itemoid from QL_trnjualdtl d INNER JOIN QL_trnjualmst mst ON d.trnjualmstoid=mst.trnjualmstoid AND d.branch_code=mst.branch_code INNER JOIN QL_mstcust c ON c.custoid=mst.trncustoid AND c.branch_code=mst.branch_code Where convert(char(10),mst.trnjualdate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "'"

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & TcharNoTrim(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        sSql &= ") ORDER BY itemdesc"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then 
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            'Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            'Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\ReportForm\rptLRPerItem.aspx") ' di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Laporan Laba Rugi Per Item"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then
            fDDLBranch() : InitddlSales()
            Session("showReport") = Nothing
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If 
        'End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptLRPerItem.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("excel")
    End Sub

    Protected Sub imbSearchGd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListGudang()
    End Sub

    Protected Sub gvGudang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'gvGudang.PageIndex = e.NewPageIndex
        'bindDataListGudang()
    End Sub

    Protected Sub gvGudang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'mtrlocname.Text = gvGudang.SelectedDataKey.Item("gendesc")
        'mtrlocoid.Text = gvGudang.SelectedDataKey.Item("genoid")
        'gvGudang.Visible = False
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        showPrint("pdf")
    End Sub

    Protected Sub btnviewreportasli_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        showPrint("view")
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("view")
    End Sub
#End Region

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing
            gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)

        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5"
            gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"

        If stockFlag.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag='" & stockFlag.SelectedValue & "'"
        End If

        'sPlus &= " AND itemflag='" & ddlStatus1.SelectedValue & "'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnALLSelectItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Maaf, Anda belum pilih katalog..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedItem.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Mohon maaf, data tidak ditemukan..!! !"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If ItemName.Text <> "" Then
                            ItemName.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            ItemName.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        ItemName.Text = ""
    End Sub
End Class
