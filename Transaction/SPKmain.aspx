<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="SPKMAIN.aspx.vb" Inherits="SpkMain" title="MULTI SARANA COMPUTER SERVICE - SPK MAIN"%>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" char="header" colspan="3" style="background-color: silver">
                            <asp:Label ID="Label249" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: SPK MAIN"></asp:Label></td>
                    </tr>
                </table>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif"/>
                            <span style="font-size: 9pt"><strong> List SPK Main</strong></span> <strong><span
                                style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanelSearch" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w569" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 105px" align=left>Filter</TD><TD align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w570"><asp:ListItem Value="spkno">SPK NO</asp:ListItem>
<asp:ListItem Value="spkaddnote">SPK NOTE</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w571" MaxLength="30"></asp:TextBox> &nbsp; </TD><TD align=left colSpan=1><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w572" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 105px" align=left>Periode (Open)</TD><TD align=left>:</TD><TD align=left colSpan=5><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w573"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w574"></asp:ImageButton> <asp:Label id="Label16" runat="server" Text="to" __designer:wfdid="w575"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w576"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w577"></asp:ImageButton> <asp:Label id="Label_17" runat="server" CssClass="Important" Text="* (dd/MM/yyyy)" __designer:wfdid="w578"></asp:Label></TD><TD align=left colSpan=1><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w579" TargetControlID="FilterPeriod1" PopupButtonID="imbPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD style="WIDTH: 105px" align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=5><asp:DropDownList id="FilterStatus" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w580"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem Value="POST">POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w581"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w582"></asp:ImageButton> <ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w584" TargetControlID="FilterPeriod2" PopupButtonID="imbPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD><TD align=left colSpan=1>&nbsp;<ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w583" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=8><asp:Panel id="Panel2" runat="server" Width="100%" Height="325px" __designer:wfdid="w2"><asp:GridView id="GVtrnspkmain" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" GridLines="None" EmptyDataText="No data in database." AllowPaging="True" DataKeyNames="spkmainoid" CellPadding="4" AutoGenerateColumns="False" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="spkmainoid" DataNavigateUrlFormatString="~\transaction\SPKmain.aspx?oid={0}" DataTextField="spkno" HeaderText="SPK No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="spkopendate" HeaderText="Open Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkplanstart" HeaderText="PlanStart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkplanend" HeaderText="PlanEnd">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkaddnote" HeaderText="SPK Note">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="LinkButton1" onclick="LinkButton1_Click1" runat="server" CommandArgument='<%# eval("spkmainoid") %>' ToolTip='<%# eval("spkno") %>'>PRINT</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="GVtrnspkmain"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong> SPK Main (mode :
                                <asp:Label ID="i_u" runat="server" ForeColor="Red"></asp:Label>
                                )</strong></span>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE class="tabelhias" cellSpacing=2 cellPadding=2 width="100%"><TBODY><TR><TD colSpan=3><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w589" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w590"><asp:Label id="Label2x" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="SPK MAIN & Barang Service" __designer:wfdid="w591"></asp:Label> <asp:Label id="Label1" runat="server" Font-Size="Small" ForeColor="#585858" Text="|" __designer:wfdid="w592"></asp:Label> <asp:LinkButton id="LinkButton2" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w593">Process Service</asp:LinkButton> <asp:Label id="Label7" runat="server" Font-Size="Small" ForeColor="#585858" Text="|" __designer:wfdid="w594"></asp:Label> <asp:LinkButton id="LinkButton6" runat="server" CssClass="submenu" Font-Size="Small" __designer:wfdid="w595">Process Sparepart</asp:LinkButton><BR /><TABLE width="100%"><TBODY><TR><TD>SPK NO</TD><TD><asp:TextBox id="spkno" runat="server" Width="145px" CssClass="inpTextDisabled" Text="" __designer:wfdid="w596" MaxLength="50" Enabled="False"></asp:TextBox> <asp:Label id="spkjenisbarang" runat="server" __designer:wfdid="w597" Visible="False"></asp:Label></TD><TD><asp:Label id="Label11" runat="server" Text="Cabang" __designer:wfdid="w598"></asp:Label></TD><TD><asp:DropDownList id="division" runat="server" Width="167px" CssClass="inpText" __designer:wfdid="w599" OnSelectedIndexChanged="division_SelectedIndexChanged"></asp:DropDownList></TD><TD>Status</TD><TD><asp:TextBox id="spkstatus" runat="server" Width="94px" CssClass="inpTextDisabled" __designer:wfdid="w600" Enabled="False">In Process</asp:TextBox>&nbsp;</TD></TR><TR><TD>Tanggal SPK</TD><TD><asp:TextBox id="spkopendate" runat="server" Width="76px" CssClass="inpTextDisabled" __designer:wfdid="w601" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w602"></asp:ImageButton>&nbsp;<ajaxToolkit:CalendarExtender id="caloex5" runat="server" __designer:wfdid="w603" TargetControlID="spkopendate" PopupButtonID="btnCalendar1" Format="dd/MM/yyyy" Enabled="True"></ajaxToolkit:CalendarExtender></TD><TD>Rencana Mulai</TD><TD><asp:TextBox id="spkplanstarttime" runat="server" Width="38px" CssClass="inpTextDisabled" __designer:wfdid="w604" Enabled="False"></asp:TextBox> <asp:TextBox id="spkplanstart" runat="server" Width="62px" CssClass="inpTextDisabled" __designer:wfdid="w605" Enabled="False"></asp:TextBox></TD><TD>Rencana Selesai</TD><TD><asp:TextBox id="spkplanendtime" runat="server" Width="41px" CssClass="inpTextDisabled" __designer:wfdid="w606" Enabled="False"></asp:TextBox> <asp:TextBox id="spkplanend" runat="server" Width="63px" CssClass="inpTextDisabled" __designer:wfdid="w607" Enabled="False"></asp:TextBox></TD></TR><TR><TD>SPK&nbsp;Note</TD><TD><asp:TextBox id="spkaddnote" runat="server" Width="372px" Height="31px" CssClass="inpText" Font-Size="8pt" Text="" __designer:wfdid="w608" MaxLength="50" TextMode="MultiLine"></asp:TextBox></TD><TD>PIC</TD><TD><asp:TextBox id="tbpic" runat="server" Width="104px" CssClass="inpTextDisabled" __designer:wfdid="w609" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibsearchpic" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w610"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibdelpic" onclick="btnioclear_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w611"></asp:ImageButton>&nbsp;<asp:Label id="lblpic" runat="server" __designer:wfdid="w612" Visible="False"></asp:Label></TD><TD></TD><TD><asp:Label id="spkmainoid" runat="server" __designer:wfdid="w613" Visible="False"></asp:Label></TD></TR><TR><TD colSpan=6><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" __designer:wfdid="w614" Font-Overline="False" Font-Underline="True">Barang Service :</asp:Label> <asp:Label id="i_u_spkresultitem" runat="server" ForeColor="Red" Text="New Detail" __designer:wfdid="w615" Visible="False"></asp:Label> <asp:Label id="iodtloid" runat="server" __designer:wfdid="w616" Visible="False"></asp:Label> <asp:Label id="spkdtloid" runat="server" __designer:wfdid="w617" Visible="False"></asp:Label> <asp:Label id="qtyspk" runat="server" __designer:wfdid="w618" Visible="False"></asp:Label> <asp:Label id="orderqty" runat="server" __designer:wfdid="w619" Visible="False"></asp:Label></TD></TR><TR><TD>Barcode No</TD><TD><asp:TextBox id="iono" runat="server" Width="112px" CssClass="inpText" Text="" __designer:wfdid="w620" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnshowiosearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w621"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnioclear" onclick="btnioclear_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w622"></asp:ImageButton> (<asp:Label id="ioseq" runat="server" __designer:wfdid="w623">1</asp:Label>)</TD><TD>Nama Barang</TD><TD colSpan=3><asp:TextBox id="itemlongdesc" runat="server" Width="369px" CssClass="inpTextDisabled" Text="" __designer:wfdid="w624" Enabled="False"></asp:TextBox>&nbsp;</TD></TR><TR><TD></TD><TD><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvItem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w2" PageSize="8" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="iono,iorefno,custname,consaddress,itemlongdesc,ioestdeliverydate,ioqty,ioqtyunitoid,iounit1,iodtloid,bomoid,itemoid,itemgroupcode,QtySPK" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="iono" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="True" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ioestdeliverydate" HeaderText="Tanggal Terima">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ioqty" HeaderText="Qty" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="iounit1" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomoid" HeaderText="BOM" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtySpk" HeaderText="Qty SPK" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No detail Barcode !!" __designer:wfdid="w5"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD></TD><TD colSpan=3></TD></TR><TR><TD colSpan=6><TABLE id="TABLE1" runat="server" Visible="false"><TBODY><TR style="FONT-SIZE: 8pt"><TD># Customer</TD><TD><asp:TextBox id="iocustnameFilter" runat="server" Width="148px" CssClass="inpText" __designer:wfdid="w625"></asp:TextBox></TD><TD>&nbsp;# Nama Barang</TD><TD><asp:TextBox id="ioitemdescfilter" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w626"></asp:TextBox></TD><TD>&nbsp;# Tanggal Terima</TD><TD><asp:TextBox id="deliverydate1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w627"></asp:TextBox> <asp:ImageButton id="btndlv1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w628"></asp:ImageButton> - <asp:TextBox id="deliverydate2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w629"></asp:TextBox> <asp:ImageButton id="btndlv2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w630"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w631"></asp:Label></TD><TD><asp:ImageButton id="btnsearchIO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w632"></asp:ImageButton> <asp:ImageButton id="btnsearchIOAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w633" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD><ajaxToolkit:CalendarExtender id="cedlv2" runat="server" __designer:wfdid="w634" TargetControlID="deliverydate2" PopupButtonID="btndlv2" Format="dd/MM/yyyy" Enabled="True"></ajaxToolkit:CalendarExtender></TD><TD></TD><TD><ajaxToolkit:CalendarExtender id="cedlv1" runat="server" __designer:wfdid="w635" TargetControlID="deliverydate1" PopupButtonID="btndlv1" Format="dd/MM/yyyy" Enabled="True"></ajaxToolkit:CalendarExtender></TD><TD></TD><TD></TD><TD><ajaxToolkit:MaskedEditExtender id="meedlv1" runat="server" __designer:wfdid="w636" TargetControlID="deliverydate1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD><TD><ajaxToolkit:MaskedEditExtender id="meedlv2" runat="server" __designer:wfdid="w637" TargetControlID="deliverydate2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Jenis Barang</TD><TD><asp:DropDownList id="resultunitoid1" runat="server" Width="186px" CssClass="inpText" __designer:wfdid="w639" AutoPostBack="True"></asp:DropDownList>&nbsp;<asp:DropDownList id="bomoid" runat="server" Width="330px" CssClass="inpText" __designer:wfdid="w640" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD></TD><TD><asp:TextBox id="resultqty1" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w641" Visible="False" AutoPostBack="True"></asp:TextBox>&nbsp; <asp:Label id="Label8" runat="server" CssClass="Important" Text="Max :" __designer:wfdid="w642" Visible="False"></asp:Label> <asp:Label id="maxqty" runat="server" CssClass="Important" Text="0" __designer:wfdid="w643" Visible="False"></asp:Label></TD><TD align=right colSpan=2><asp:ImageButton id="btnAddToList1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w644"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl1" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD></TD><TD><asp:TextBox id="resultqty2" runat="server" Width="87px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:TextBox> <asp:Label id="refoid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomstatus" runat="server" ForeColor="Red" Visible="False"></asp:Label></TD><TD><asp:DropDownList id="resultunitoid2" runat="server" Width="74px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD><ajaxToolkit:FilteredTextBoxExtender id="ioftx1" runat="server" TargetControlID="resultqty1" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD colSpan=2><ajaxToolkit:FilteredTextBoxExtender id="ioftx2" runat="server" TargetControlID="resultqty2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 130px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 130%"><asp:GridView id="tblspkresultitem" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ioseq,iono,custname,itemoid,itemlongdesc,ioestdeliverydate,resultqty1,resultunitoid1,unit1,iodtloid,bomoid,bomdesc,spkdtloid,refname" EmptyDataText="Data Not Found" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Blue" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkdtloid" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="iono" HeaderText="No Barcode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="resultqty1" HeaderText="Qty1" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bomdesc" HeaderText="BOM Formula" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowCancelButton="False" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Text="No Barcode data !!" __designer:wfdid="w106"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=6><asp:Label id="iorefno" runat="server" Visible="False"></asp:Label> <asp:Label id="custname" runat="server" Visible="False"></asp:Label> <asp:Label id="ioestdeliverydate" runat="server" Visible="False"></asp:Label> <asp:Label id="ioqty" runat="server" Visible="False"></asp:Label> <asp:Label id="ioqtyunitoid" runat="server" Visible="False"></asp:Label><asp:Label id="iounit1" runat="server" Visible="False"></asp:Label><asp:Label id="qty2" runat="server" Visible="False"></asp:Label><asp:Label id="unit2" runat="server" Visible="False"></asp:Label><asp:Label id="iounit2" runat="server" Visible="False"></asp:Label><asp:Label id="berat" runat="server" Visible="False"></asp:Label> <asp:Label id="panjang" runat="server" Visible="False"></asp:Label> <asp:Label id="bomdesc" runat="server" Visible="False"></asp:Label> <asp:Label id="REFNAME" runat="server" Visible="False"></asp:Label> <asp:Label id="bomunitoid" runat="server" Visible="False"></asp:Label> <asp:Label id="bomqty" runat="server" Visible="False">1</asp:Label></TD></TR></TBODY></TABLE></asp:View><asp:View id="View3" runat="server"><TABLE width="100%"><TBODY><TR><TD colSpan=5>&nbsp;<asp:LinkButton id="LinkButton8" runat="server" CssClass="submenu" Font-Size="Small">SPK MAIN & Barang Service</asp:LinkButton> <asp:Label id="Label19" runat="server" Font-Size="Small" ForeColor="#585858" Text="|"></asp:Label> <asp:Label id="Label18" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Process Service"></asp:Label> <asp:Label id="Label20" runat="server" Font-Size="Small" ForeColor="#585858" Text="|"></asp:Label> <asp:LinkButton id="LinkButton10" runat="server" CssClass="submenu" Font-Size="Small">Process Sparepart</asp:LinkButton></TD><TD style="WIDTH: 286px"></TD></TR><TR><TD colSpan=5><asp:Label id="Label6" runat="server" Font-Bold="True" ForeColor="Black" Font-Overline="False" Font-Underline="True">Process Service ::..</asp:Label> <asp:Label id="i_u_spkprocessitem" runat="server" ForeColor="Red" Text="New Detail"></asp:Label></TD><TD style="WIDTH: 286px"></TD></TR><TR><TD style="WIDTH: 106px">Barang&nbsp;No</TD><TD style="WIDTH: 171px"><asp:DropDownList id="spkdtloid_2" runat="server" Width="112px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 84px"><asp:Label id="itemoid_resultno" runat="server" Width="54px"></asp:Label></TD><TD style="WIDTH: 286px"><asp:Label id="qtycylinder" runat="server" Visible="False"></asp:Label> <asp:Label id="jenisfgoid" runat="server"></asp:Label></TD><TD style="WIDTH: 101px"></TD><TD style="WIDTH: 286px"></TD></TR><TR><TD style="WIDTH: 106px">Seq Process</TD><TD style="WIDTH: 171px"><asp:DropDownList id="seqprocess1" runat="server" Width="113px" CssClass="inpText"><asp:ListItem>1</asp:ListItem>
<asp:ListItem>2</asp:ListItem>
<asp:ListItem>3</asp:ListItem>
<asp:ListItem>4</asp:ListItem>
<asp:ListItem>5</asp:ListItem>
<asp:ListItem>6</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>8</asp:ListItem>
<asp:ListItem>9</asp:ListItem>
<asp:ListItem>10</asp:ListItem>
<asp:ListItem>11</asp:ListItem>
<asp:ListItem>12</asp:ListItem>
<asp:ListItem>13</asp:ListItem>
<asp:ListItem>14</asp:ListItem>
<asp:ListItem>15</asp:ListItem>
<asp:ListItem>16</asp:ListItem>
<asp:ListItem>17</asp:ListItem>
<asp:ListItem>18</asp:ListItem>
<asp:ListItem>19</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 84px">Planning&nbsp;Mulai</TD><TD style="WIDTH: 286px"><asp:TextBox id="planstarttime" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> <asp:TextBox id="planstartdate" runat="server" Width="50px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnplanstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Labelv3_1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="planstartdate" Mask="99/99/9999" MaskType="Date" Enabled="True" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDateFormat="" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="planstartdate" PopupButtonID="btnplanstart" Format="dd/MM/yyyy" Enabled="True"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="planstarttime" Mask="99:99" MaskType="Time" Enabled="True" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDateFormat="" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" UserTimeFormat="TwentyFourHour" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender></TD><TD style="WIDTH: 101px">Planning&nbsp;Selesai</TD><TD style="WIDTH: 286px"><asp:TextBox id="planendtime" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> <asp:TextBox id="planenddate" runat="server" Width="50px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnplanend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Labelv3_2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="planendtime" Mask="99:99" MaskType="Time" Enabled="True" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDateFormat="" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" UserTimeFormat="TwentyFourHour" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender4" runat="server" TargetControlID="planenddate" PopupButtonID="btnplanend" Format="dd/MM/yyyy" Enabled="True"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="planenddate" Mask="99/99/9999" MaskType="Date" Enabled="True" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDateFormat="" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 106px">Nama Process</TD><TD colSpan=3><asp:DropDownList id="processoid" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 101px"><asp:Label id="Label9" runat="server" Text="Machine" Visible="False"></asp:Label></TD><TD style="WIDTH: 286px"><asp:DropDownList id="machinedtloid" runat="server" Width="219px" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 106px" id="TD1" runat="server" Visible="false">Tolerance</TD><TD style="WIDTH: 171px" id="TD2" runat="server" Visible="false"><asp:DropDownList id="ddltolerance" runat="server" Width="113px" CssClass="inpText" __designer:wfdid="w4" AutoPostBack="True" OnSelectedIndexChanged="ddltolerance_SelectedIndexChanged"></asp:DropDownList>&nbsp;(%)</TD><TD style="WIDTH: 84px">Status</TD><TD style="WIDTH: 286px"><asp:TextBox id="spkprodstatus" runat="server" Width="90px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w2" Enabled="False">In Process</asp:TextBox> <asp:Label id="spkprocessoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 101px"></TD><TD style="WIDTH: 286px"><asp:TextBox id="stddowntime" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text="0" MaxLength="10" Visible="False"></asp:TextBox> <asp:Label id="machineoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px">Note</TD><TD colSpan=3><asp:TextBox id="note" runat="server" Width="425px" CssClass="inpText" Font-Size="X-Small" MaxLength="100"></asp:TextBox></TD><TD colSpan=2><asp:ImageButton id="btnAddToList2" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl2" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD colSpan=6><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 180px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset6"><DIV id="Div6"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvprocessitem" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="spkdtloid,seqprocessitem,spkprocessoid,spkprocessname,planstart,planend,machineoid,machine,outputqtytolerance,stdpreptime,stddowntime,stdspeed,stdprodtime,note,spkprodstatus,machinedtloid,processoid" EmptyDataText="Data Not Found" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Firebrick" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkdtloid" HeaderText="BarangNo">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="seqprocessitem" HeaderText="SeqProcess">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessname" HeaderText="NamaProcess">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="planstart" HeaderText="Rencana Mulai">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="planend" HeaderText="Rencana Selesai">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="machine" HeaderText="Machine" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputqtytolerance" HeaderText="Tolerance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stdpreptime" HeaderText="Std Prepair" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stddowntime" HeaderText="Std Istirahat" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stdspeed" HeaderText="StdSpeed" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stdprodtime" HeaderText="Std Kerja" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowCancelButton="False" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkprocessoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Text="No Planning data !!" __designer:wfdid="w3"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><asp:TextBox id="outputqtytolerance" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text="" __designer:wfdid="w3" MaxLength="3" Visible="False"></asp:TextBox> <asp:TextBox id="stdspeed" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text="0" MaxLength="10" Visible="False"></asp:TextBox> <asp:TextBox id="stdprodtime" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text="0" MaxLength="10" Visible="False"></asp:TextBox> <asp:TextBox id="stdpreptime" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text="0" __designer:wfdid="w1" MaxLength="10" Visible="False"></asp:TextBox></FIELDSET></TD></TR><TR><TD colSpan=6><asp:Label id="Label10" runat="server" Font-Bold="True" ForeColor="Black" Font-Overline="False" Font-Underline="True">Process Service Job ::..</asp:Label> <asp:Label id="i_u_spkprocessitemresult" runat="server" ForeColor="Red" Text="New Detail"></asp:Label> <asp:TextBox id="outputoid" runat="server" Width="16px" CssClass="inpTextDisabled" Text="" MaxLength="50" Visible="False"></asp:TextBox> <asp:TextBox id="outputref" runat="server" Width="43px" CssClass="inpTextDisabled" Text="" MaxLength="50" Visible="False"></asp:TextBox> <asp:Label id="unit1unit2conv" runat="server" Visible="False"></asp:Label> <asp:Label id="unit2unit3conv" runat="server" Visible="False"></asp:Label> <asp:Label id="olditemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 106px">Barang&nbsp;No</TD><TD style="WIDTH: 171px"><asp:DropDownList id="spkdtloid_3" runat="server" Width="105px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 84px" colSpan=1>Nama Process</TD><TD colSpan=3><asp:DropDownList id="processname_1" runat="server" Width="310px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 106px">Job Type</TD><TD style="WIDTH: 171px"><asp:DropDownList id="outputtype" runat="server" Width="105px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 84px" colSpan=1>Nama Job</TD><TD colSpan=3><asp:TextBox id="outputname" runat="server" Width="305px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchOutput" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearOutput" onclick="btnClearOutput_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 106px"></TD><TD style="WIDTH: 171px"></TD><TD style="WIDTH: 84px" colSpan=1></TD><TD colSpan=3><asp:GridView id="GVMat2" runat="server" Width="97%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w1" PageSize="8" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="matoid,matcode,matlongdesc,matunit,matunitoid,qty" AllowPaging="True" GridLines="None" Visible="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Tarif">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" Width="175px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="175px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Region on List" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 106px">Note</TD><TD colSpan=5><asp:TextBox id="notedtlitemresult" runat="server" Width="500px" CssClass="inpText" MaxLength="150"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 106px"></TD><TD colSpan=5><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvItem2" runat="server" Width="97%" ForeColor="#333333" PageSize="8" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemcode,itemlongdesc,itemunitoid,unit,itemoid,itservtarif" AllowPaging="True" GridLines="None" Visible="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Nama Job">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itservtarif" HeaderText="Tarif">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="X-Small" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Job data found!!" __designer:wfdid="w46"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 106px">Tarif</TD><TD style="WIDTH: 171px"><asp:TextBox id="outputqty1" runat="server" Width="97px" CssClass="inpTextDisabled" Font-Size="X-Small" Text="" MaxLength="8" Enabled="False" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:DropDownList id="outputunit1" runat="server" Width="59px" CssClass="inpText" Font-Size="X-Small" Visible="False"></asp:DropDownList>&nbsp;</TD><TD style="WIDTH: 84px"><asp:TextBox id="outputqty2" runat="server" Width="94px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="10" Visible="False" AutoPostBack="True"></asp:TextBox></TD><TD style="WIDTH: 286px"><asp:DropDownList id="outputunit2" runat="server" Width="64px" CssClass="inpText" Font-Size="X-Small" Visible="False"></asp:DropDownList></TD><TD colSpan=2><asp:ImageButton id="btnAddToList3" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancelDtl3" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD colSpan=6><FIELDSET style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 180px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset7"><DIV id="Div7"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvprocessitemresult" runat="server" Width="98%" Height="5px" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="spkdtloid,spkprocessname,spkprocessoid,outputoid,outputname,outputref,outputtype,outputqty1,outputunitoid1,outputunit1,outputqty2,outputunitoid2,outputunit2,note,typejob" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkdtloid" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessname" HeaderText="NamaProcess">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typejob" HeaderText="Type">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="outputname" HeaderText="Nama Job">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputtype" HeaderText="Typeoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputqty1" HeaderText="Tarif">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputunit1" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputqty2" HeaderText="Qty2" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="6%"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="6%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="outputunit2" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="3%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Overline="False" Font-Size="X-Small" Font-Strikeout="False" Width="3%"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkprocessoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Route on List" __designer:wfdid="w45"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR><TD colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="Filstdpreptime" runat="server" TargetControlID="stdpreptime" Enabled="False" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="Filtoutqtytol" runat="server" TargetControlID="outputqtytolerance" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="filddowntime" runat="server" TargetControlID="stddowntime" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="Filtstdspeed" runat="server" TargetControlID="stdspeed" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="Filtstdprodtime" runat="server" TargetControlID="stdprodtime" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="outputqty1" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" TargetControlID="outputqty2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View4" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:LinkButton id="LinkButton11" runat="server" CssClass="submenu" Font-Size="Small">SPK MAIN & Barang Service</asp:LinkButton> <asp:Label id="Label22" runat="server" Font-Size="Small" ForeColor="#585858" Text="|"></asp:Label> <asp:LinkButton id="LinkButton13" runat="server" CssClass="submenu" Font-Size="Small">Process Service</asp:LinkButton> <asp:Label id="Label25" runat="server" Font-Size="Small" ForeColor="#585858" Text="|"></asp:Label> <asp:Label id="Label24" runat="server" CssClass="submenu" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Process Sparepart"></asp:Label></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left>Barang No</TD><TD align=left colSpan=3><asp:DropDownList id="spkdtloid_4" runat="server" Width="121px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList> <asp:Label id="i_u_spkprocessmaterial" runat="server" ForeColor="Red" Text="New Detail"></asp:Label> <asp:Label id="oldmatoid" runat="server" Visible="False"></asp:Label> <asp:Label id="i_u3" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 107px" align=left colSpan=1>Nama Process</TD><TD align=left colSpan=1><asp:DropDownList id="processname_2" runat="server" Width="302px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left>Gudang</TD><TD align=left colSpan=3><asp:DropDownList id="LocOid" runat="server" CssClass="inpText" __designer:wfdid="w2" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 107px" id="TD4" align=left colSpan=1 runat="server" Visible="false">Tolerance Qty</TD><TD id="TD3" align=left colSpan=1 runat="server" Visible="false"><asp:TextBox id="mattolerancepct" runat="server" Width="115px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="3"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left>Sparepart</TD><TD align=left colSpan=3><asp:TextBox id="matdesc" runat="server" Width="250px" CssClass="inpText" Font-Size="X-Small" Text=""></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" onclick="btnSearchMat_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD style="WIDTH: 107px" align=left colSpan=1></TD><TD align=left colSpan=1></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left></TD><TD align=left colSpan=3><asp:GridView id="GVMat3" runat="server" Width="100%" Height="31px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w7" PageSize="8" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="matoid,matcode,matlongdesc,matunit,matunitoid,Qty" AllowPaging="True" EmptyDataText="No data in database." GridLines="None" Visible="False" OnSelectedIndexChanged="GVMat3_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="65%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="65%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Qty" HeaderText="Qty Stok">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="10%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Sparepart on List" __designer:wfdid="w44"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD style="WIDTH: 107px" align=left colSpan=1></TD><TD align=left colSpan=1><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender7" runat="server" TargetControlID="mattolerancepct" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left>Note</TD><TD align=left colSpan=5><asp:TextBox id="notedtlprosesmat" runat="server" Width="250px" CssClass="inpText" MaxLength="150"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left>Qty <asp:Label id="Label33" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="matqty" runat="server" Width="115px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="6" AutoPostBack="True"></asp:TextBox> <asp:DropDownList id="matunit" runat="server" Width="74px" CssClass="inpText" Font-Size="X-Small"></asp:DropDownList>&nbsp;<asp:Label id="conv1_2" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="StockAkhir" runat="server" __designer:wfdid="w3" Visible="False">0</asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" TargetControlID="matqty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="WIDTH: 107px" align=left colSpan=1>Harga Sparepart</TD><TD align=left colSpan=1><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left><asp:TextBox id="wastestd" runat="server" Width="75px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="8"></asp:TextBox></TD><TD align=right><asp:ImageButton id="btnAddToList4" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl4" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left></TD><TD align=left colSpan=3><asp:TextBox id="matqty2" runat="server" Width="115px" CssClass="inpText" Font-Size="X-Small" Text="" MaxLength="10" Visible="False"></asp:TextBox> <asp:DropDownList id="matunit2" runat="server" Width="74px" CssClass="inpText" Font-Size="X-Small" Visible="False"></asp:DropDownList> <asp:Label id="conv2_3" runat="server" Visible="False"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender5" runat="server" TargetControlID="matqty2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD><TD style="WIDTH: 107px" align=left colSpan=1><asp:TextBox id="matref" runat="server" Width="23px" CssClass="inpTextDisabled" Visible="False">QL_MSTMAT</asp:TextBox> <asp:TextBox id="matoid" runat="server" Width="24px" CssClass="inpTextDisabled" Font-Size="X-Small" Text="" MaxLength="10" Visible="False" ReadOnly="True"></asp:TextBox></TD><TD align=left colSpan=1><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender6" runat="server" TargetControlID="wastestd" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left colSpan=6><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset8"><DIV id="Div8"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 160px"><asp:GridView id="GVprocessmaterial" runat="server" Width="98%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="spkdtloid,spkprocessmatoid,spkprocessoid,matoid,matlongdesc,matref,qty1,unitoid1,unit1,qty2,unitoid2,unit2,wastestd,matroutetolerancepct,note" EmptyDataText="Data Not Found" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkdtloid" HeaderText="No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessname" HeaderText="NamaProcess">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Sparepart">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty1" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit1" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty2" HeaderText="Qty2" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="6%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit2" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wastestd" HeaderText="Harga">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="spkprocessmatoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spkprocessoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrlocoid" HeaderText="mtrlocoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Sparepart on List" __designer:wfdid="w43"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView></TD></TR><TR><TD style="COLOR: #585858" colSpan=3>Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[Updtime]"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text="[upduser]"></asp:Label></TD></TR><TR><TD style="HEIGHT: 27px" colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" tabIndex=39 runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapprove" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w6"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnDelete" tabIndex=41 runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" CommandName="Delete"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" tabIndex=40 runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="left">
                            <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" DropShadow="True" Drag="True" PopupDragHandleControlID="lblCaption" PopupControlID="Panelvalidasi" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
               </td>
        </tr>
    </table>
     <asp:UpdatePanel id="uppoppic" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgvpic" runat="server" CssClass="modalBox" DefaultButton="ibpopfindpic" Visible="False">&nbsp;<DIV style="OVERFLOW: auto"><TABLE style="WIDTH: 650px"><TBODY><TR><TD style="WIDTH: 64px">Filter</TD><TD style="WIDTH: 7px">:</TD><TD><asp:DropDownList id="ddlpopfilterpic" runat="server" Width="136px" CssClass="inpText"><asp:ListItem Value="Username">Nama PIC</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:TextBox id="tbpopfilterpic" runat="server" Width="224px" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp; <asp:ImageButton id="ibpopfindpic" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibpopviewallpic" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD colSpan=3></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 98%; HEIGHT: 168px; BACKGROUND-COLOR: beige">&nbsp;<asp:GridView style="WIDTH: 99%" id="gvpoppic" runat="server" ForeColor="#333333" GridLines="None" DataKeyNames="personnip,personname" CellPadding="4" AutoGenerateColumns="False" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Underline="True" ForeColor="Blue" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personnip" HeaderText="Userid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Nama PIC">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: center" colSpan=3><asp:ImageButton id="ibpopcancelpic" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepic" runat="server" TargetControlID="bepopgvpic" BackgroundCssClass="modalBackground" PopupControlID="panpopgvpic" PopupDragHandleControlID="ibpopcancelpic" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgvpic" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>    
</asp:Content>

