Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.Web

Partial Class rptJualTriWulan
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim vReport As New ReportDocument
    Dim fCabang As String = ""
#End Region

#Region "Fucntion"
    Public Sub GenerateBulan(ByVal bln1 As String, ByVal note As String)
        If bln1 = "1" Then
            If note = "nol" Then
                lblbln4.Text = "Januari"
            ElseIf note = "satu" Then
                lblbln1.Text = "Januari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Januari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Januari"
            End If
        ElseIf bln1 = "2" Then
            If note = "nol" Then
                lblbln4.Text = "Februari"
            ElseIf note = "satu" Then
                lblbln1.Text = "Februari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Februari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Februari"
            End If
        ElseIf bln1 = "3" Then
            If note = "nol" Then
                lblbln4.Text = "Maret"
            ElseIf note = "satu" Then
                lblbln1.Text = "Maret"
            ElseIf note = "dua" Then
                lblbln2.Text = "Maret"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Maret"
            End If
        ElseIf bln1 = "4" Then
            If note = "nol" Then
                lblbln4.Text = "April"
            ElseIf note = "satu" Then
                lblbln1.Text = "April"
            ElseIf note = "dua" Then
                lblbln2.Text = "April"
            ElseIf note = "tiga" Then
                lblbln3.Text = "April"
            End If
        ElseIf bln1 = "5" Then
            If note = "nol" Then
                lblbln4.Text = "Mei"
            ElseIf note = "satu" Then
                lblbln1.Text = "Mei"
            ElseIf note = "dua" Then
                lblbln2.Text = "Mei"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Mei"
            End If
        ElseIf bln1 = "6" Then
            If note = "satu" Then
                lblbln4.Text = "Juni"
            ElseIf note = "satu" Then
                lblbln1.Text = "Juni"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juni"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juni"
            End If
        ElseIf bln1 = "7" Then
            If note = "nol" Then
                lblbln4.Text = "Juli"
            ElseIf note = "satu" Then
                lblbln1.Text = "Juli"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juli"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juli"
            End If
        ElseIf bln1 = "8" Then
            If note = "nol" Then
                lblbln4.Text = "Agustus"
            ElseIf note = "satu" Then
                lblbln1.Text = "Agustus"
            ElseIf note = "dua" Then
                lblbln2.Text = "Agustus"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Agustus"
            End If
        ElseIf bln1 = "9" Then
            If note = "nol" Then
                lblbln4.Text = "September"
            ElseIf note = "satu" Then
                lblbln1.Text = "September"
            ElseIf note = "dua" Then
                lblbln2.Text = "September"
            ElseIf note = "tiga" Then
                lblbln3.Text = "September"
            End If
        ElseIf bln1 = "10" Then
            If note = "nol" Then
                lblbln4.Text = "Oktober"
            ElseIf note = "satu" Then
                lblbln1.Text = "Oktober"
            ElseIf note = "dua" Then
                lblbln2.Text = "Oktober"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Oktober"
            End If
        ElseIf bln1 = "11" Then
            If note = "nol" Then
                lblbln4.Text = "November"
            ElseIf note = "satu" Then
                lblbln1.Text = "November"
            ElseIf note = "dua" Then
                lblbln2.Text = "November"
            ElseIf note = "tiga" Then
                lblbln3.Text = "November"
            End If
        ElseIf bln1 = "12" Then
            If note = "nol" Then
                lblbln4.Text = "Desember"
            ElseIf note = "satu" Then
                lblbln1.Text = "Desember"
            ElseIf note = "dua" Then
                lblbln2.Text = "Desember"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Desember"
            End If
        End If
    End Sub

    Private Function UpdateCheckedItem() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataitem") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitem")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("dataitem") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedItem2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataitemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitem")
            Dim dtTbl2 As DataTable = Session("dataitemView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("dataitem") = dtTbl
                Session("dataitemView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedure"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        'If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
        '    showMessage("Period 2 must be more than Period 1 !", 2)
        '    Return False
        'End If
        Return True
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode, gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Public Sub BindDataItem()
        sSql = "Select 'False' checkvalue, i.itemoid, i.itemcode, i.itemdesc, 'BUAH' Unit From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join ql_mstitem i ON i.itemoid=jd.itemoid Left Join (Select rm.branch_code, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur, typeSO From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code WHere rm.trnjualstatus='APPROVED' Group By rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid, typeSO) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where rm.typeSO='Konsinyasi' AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='IN COMPLETE' AND jm.trnjualdate <= '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND jm.branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        sSql &= " Group By i.itemoid, i.itemcode, i.itemdesc"
        Dim dtitem As DataTable = ckon.ambiltabel(sSql, "dataitem")
        Session("dataitem") = dtitem
    End Sub

    Public Sub showPrint(ByVal tipe As String, ByVal sType As String)
        Try
            Dim date2 As Date = CDate(toDate(dateAkhir.Text))
            Dim sDate As Integer = Day(DateAdd(DateInterval.Day, 0, date2))
            Dim sYear As String = Year(DateAdd(DateInterval.Month, 0, date2))
            Dim sYear1 As String = Year(DateAdd(DateInterval.Month, -1, date2))
            Dim sYear2 As String = Year(DateAdd(DateInterval.Month, -2, date2))
            Dim sYear3 As String = Year(DateAdd(DateInterval.Month, -3, date2))

            Dim sBln0 As String = Month(date2) - 0
            Dim sBln1 As String = Month(date2) - 1
            Dim sBln2 As String = Month(date2) - 2
            Dim sBln3 As String = Month(date2) - 3

            If sBln0 <= 0 Then
                sBln0 = sBln0 + 12
            End If

            If sBln1 <= 0 Then
                sBln1 = sBln1 + 12
            End If

            If sBln2 <= 0 Then
                sBln2 = sBln2 + 12
            End If

            If sBln3 <= 0 Then
                sBln3 = sBln3 + 12
            End If

            GenerateBulan(sBln0, "nol")
            GenerateBulan(sBln1, "satu")
            GenerateBulan(sBln2, "dua")
            GenerateBulan(sBln3, "tiga")

            'Dim sCabang As String = ""
            'If dCabangNya.SelectedValue <> "ALL" Then
            '    sCabang &= " And v.branch_code='" & dCabangNya.SelectedValue & "'"
            'End If

            sSql = "Declare @sPeriod as Varchar(20);" & _
                " Declare @sCabang as Varchar(20);" & _
                " Set @sPeriod='" & GetPeriodAcctg(date2) & "';" & _
                " Set @sCabang='" & dCabangNya.SelectedValue & "';" & _
                " Select bc.gencode AS [branch code], i.itemrefoid, i.itemoid, i.itemcode, i.itemdesc, bc.gendesc Cabang, 'BUAH' as unit, '" & lblbln3.Text & "' Bulan3, '" & lblbln4.Text & "' Bulan0, '" & lblbln1.Text & "' Bulan1, '" & lblbln2.Text & "' Bulan2, Isnull(c.qtystok,0.0) qtystok, ISNULL((SELECT v1.jualqty-v1.returqty),0.0) [bln berjalan], ISNULL((SELECT v1.jualqty-v1.returqty),0.0) [jbln1], ISNULL((SELECT v2.jualqty-v2.returqty),0.0) [jbln2], ISNULL((SELECT v3.jualqty-v3.returqty),0.0) [jbln3], ISNULL(QtyStok, 0.00) + ISNULL((Select QtyStok From View_ConStok con Where con.refoid=(Select c.itemrefoid from QL_mstitem c Where c.itemoid=i.itemoid AND c.flag_item='CLONING') AND con.periodacctg=c.periodacctg AND con.branch_code=c.branch_code), 0.00) QtyCabang, ISNULL(QtyStok, 0.00) + ISNULL((Select QtyStok From View_ConStok con Where con.refoid=(Select itemrefoid from QL_mstitem c Where c.itemoid=i.itemoid AND c.flag_item='CLONING') AND periodacctg=c.periodacctg AND con.branch_code='01'), 0.00) QtyGBB " & _
                " FROM QL_mstItem i inner join QL_mstgen bc ON bc.cmpcode=i.cmpcode AND bc.gengroup='CABANG'" & _
                " left join view_TriWulan_Per_Branch v0 on v0.branch_code=bc.gencode AND v0.itemoid=i.itemoid AND v0.bln=" & sBln0 & " And v0.thn=" & sYear & _
                " left join view_TriWulan_Per_Branch v1 on v1.branch_code=bc.gencode AND v1.itemoid=i.itemoid AND v1.bln=" & sBln1 & " And v1.thn=" & sYear1 & _
                " left join view_TriWulan_Per_Branch v2 on v2.branch_code=bc.gencode AND v2.itemoid=i.itemoid AND v2.bln=" & sBln2 & " and v2.thn=" & sYear2 & _
                " left join view_TriWulan_Per_Branch v3 on v3.branch_code=bc.gencode AND v3.itemoid=i.itemoid AND v3.bln=" & sBln3 & " and v3.thn=" & sYear3 & _
                " left join View_ConStok c on c.refoid=i.itemoid And c.periodacctg=@sPeriod and c.branch_code=bc.gencode"

            If TxtItemOid.Text <> "0" Then
                sSql &= " AND i.itemoid IN (" & TxtItemOid.Text & ")"
            End If

            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= " AND c.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            Dim dt As DataTable = ckon.ambiltabel(sSql, "DataNya")
            Dim dvTbl As DataView = dt.DefaultView
            vReport.Load(Server.MapPath("~\Report\rptJualTriWulan" & sType & ".rpt"))
            vReport.SetDataSource(dt)
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sPeriode", "Periode : " & dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "View" Then
                CrvTriWulan.DisplayGroupTree = False
                CrvTriWulan.ReportSource = vReport
            ElseIf tipe = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "PenjualanTriWulan" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "EXL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "PenjualanTriWulan" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptJualTriWulan.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Penjualan Tri Wulan"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            fDDLBranch() : dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("View", "PDF")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptJualTriWulan.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("EXL", "EXL")
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("PDF", "PDF")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("emptylistItem") Is Nothing And Session("emptylistItem") <> "" Then
            If lblPopUpMsg.Text = Session("emptylistItem") Then
                Session("emptylistItem") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
            End If
        End If
        If Not Session("warninglistItem") Is Nothing And Session("warninglistItem") <> "" Then
            If lblPopUpMsg.Text = Session("warninglistItem") Then
                Session("warninglistItem") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
            End If
        End If
    End Sub

    Private Sub CrvTriWulan_Navigate(ByVal source As Object, ByVal e As NavigateEventArgs) Handles CrvTriWulan.Navigate
        showPrint("", "PDF")
    End Sub

    Private Sub BtnItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnItem.Click
        If IsValidPeriod() Then
            DDLFilterItem.SelectedIndex = -1 : TxtFilterItem.Text = ""
            Session("dataitem") = Nothing : Session("dataitemView") = Nothing
            gvItem.DataSource = Nothing : gvItem.DataBind()
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub BtnFindItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnFindItem.Click
        If Session("dataitem") Is Nothing Then
            BindDataItem()
            If Session("dataitem").Rows.Count <= 0 Then
                Session("emptylistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistItem"), 2)
                Exit Sub
            End If
        End If

        Dim sPlus As String = DDLFilterItem.SelectedValue & " LIKE '%" & TcharNoTrim(TxtFilterItem.Text) & "%'"
        If UpdateCheckedItem() Then
            Dim dv As DataView = Session("dataitem").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("dataitemView") = dv.ToTable
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
            Else
                dv.RowFilter = ""
                Session("dataitemView") = Nothing
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
                Session("warninglistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("warninglistItem"), 2)
            End If
        Else
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        End If
    End Sub

    Private Sub BtnViewAllItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnViewAllItem.Click
        DDLFilterItem.SelectedIndex = -1 : TxtFilterItem.Text = ""
        If Session("dataitem") Is Nothing Then
            BindDataItem()
            If Session("dataitem").Rows.Count <= 0 Then
                Session("emptylistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistItem"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedItem() Then
            Dim dt As DataTable = Session("dataitem")
            Session("dataitemView") = dt
            gvItem.DataSource = Session("dataitemView")
            gvItem.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
    End Sub

    Private Sub BtnCheckAllItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckAllItem.Click
        If Not Session("dataitemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitemView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataitem")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataitem") = objTbl
                Session("dataitemView") = dtTbl
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Session("warninglistItem") = "Maaf, Silahkan pilih data..!!"
            showMessage(Session("warninglistItem"), 2)
        End If
    End Sub

    Private Sub dataitemView(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckNoneItem.Click
        If Not Session("dataretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitemView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataitem")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataitem") = objTbl
                Session("dataitemView") = dtTbl
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Session("warninglistItem") = "Please show data first!"
            showMessage(Session("warninglistItem"), 2)
        End If
    End Sub

    Private Sub LkbAddToListItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbAddToListItem.Click
        Try
            For c1 As Integer = 0 To gvItem.Rows.Count - 1
                Dim chk As CheckBox = TryCast(gvItem.Rows(c1).Cells(0).FindControl("cbItem"), CheckBox)
                If chk.Checked Then
                    If TxtItemOid.Text = "0" Then
                        TxtItemOid.Text = gvItem.DataKeys(c1).Values("itemoid")
                        TxtKatalog.Text = gvItem.DataKeys(c1).Values("itemcode")
                    Else
                        TxtItemOid.Text &= "," & gvItem.DataKeys(c1).Values("itemoid")
                        TxtKatalog.Text &= ";" & +vbCrLf + gvItem.DataKeys(c1).Values("itemcode")
                    End If
                End If
            Next
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub LkbCloseItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbCloseItem.Click
        cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
    End Sub

    Private Sub BtnEraseItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnEraseItem.Click
        TxtItemOid.Text = "0" : TxtKatalog.Text = ""
    End Sub

#End Region
End Class
