<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptDOProject.aspx.vb" Inherits="ReportForm_rptDO"  %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left; height: 23px;" valign="center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Report Delivery Order Project :."></asp:Label></th>
        </tr>
    </table>
    <table>
        <tr>
            <td align="center" style="text-align: center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=1><TABLE style="TEXT-ALIGN: center"><TBODY><TR><TD style="Z-INDEX: 100; POSITION: relative" class="Label" align=center colSpan=3 runat="server" Visible="true"></TD></TR><TR><TD style="WIDTH: 81px" id="TD12" class="Label" align=right runat="server" Visible="true">Periode</TD><TD style="TEXT-ALIGN: left" id="TD11" class="Label" align=center runat="server" Visible="true">:</TD><TD style="Z-INDEX: 100; POSITION: relative; TEXT-ALIGN: left" id="TD10" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="date1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox> <asp:ImageButton id="search1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w27"></asp:Label> <asp:TextBox id="date2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w28"></asp:TextBox> <asp:ImageButton id="search2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w30"></asp:Label>&nbsp;&nbsp;</TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"><asp:Label id="lblStatus1" runat="server" Text="Report Type" __designer:wfdid="w16"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="true"><asp:Label id="lblStatus2" runat="server" Text=":" __designer:wfdid="w17"></asp:Label></TD><TD class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="dView" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True" OnSelectedIndexChanged="dView_SelectedIndexChanged"><asp:ListItem Value="sum">Summary</asp:ListItem>
<asp:ListItem Value="Detail">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true">Delivery Order</TD><TD class="Label" align=center runat="server" Visible="true">:</TD><TD class="Label" align=left runat="server" Visible="true"><asp:TextBox id="DOno" runat="server" Width="148px" CssClass="inpText" __designer:wfdid="w50" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearhDO" onclick="btnsearhorder_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w51"></asp:ImageButton>&nbsp;<asp:ImageButton id="eraseDO" onclick="eraseDO_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w52"></asp:ImageButton>&nbsp;<asp:Label id="DOoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w53"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"></TD><TD class="Label" align=center runat="server" Visible="true"></TD><TD class="Label" align=left runat="server" Visible="true"><asp:GridView id="gvListDO" runat="server" Width="98%" Font-Size="X-Small" Visible="False" __designer:wfdid="w49" OnSelectedIndexChanged="gvListDO_SelectedIndexChanged" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" DataKeyNames="trnsjjualmstoid,DOdate,trnsjjualno,trnsjjualstatus,orderno,ordermstoid,custoid,custname" BorderColor="#DEDFDE" AllowPaging="True" OnPageIndexChanging="gvListDO_PageIndexChanging">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualno" HeaderText="DO No">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="DO Date">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualstatus" HeaderText="Status">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label89" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true">Sales Order</TD><TD class="Label" align=center runat="server" Visible="true">:</TD><TD class="Label" align=left runat="server" Visible="true"><asp:TextBox id="SOno" runat="server" Width="148px" CssClass="inpText" __designer:wfdid="w2" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSO" onclick="btnSearchSO_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="eraseSO" onclick="eraseSO_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="SOoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w53"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"></TD><TD class="Label" align=center runat="server" Visible="true"></TD><TD class="Label" align=left runat="server" Visible="true"><asp:GridView id="GVSO" runat="server" Width="98%" Font-Size="X-Small" Visible="False" __designer:wfdid="w1" OnSelectedIndexChanged="GVSO_SelectedIndexChanged" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" DataKeyNames="ordermstoid,orderno,SOdate,trnorderstatus,custoid,custname" BorderColor="#DEDFDE" AllowPaging="True">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="SO No">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SOdate" HeaderText="SO Date">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderstatus" HeaderText="Status">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label89" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 81px" id="TD8" class="Label" align=right runat="server" Visible="true">Customer</TD><TD id="TD7" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TD9" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="custname" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w31"></asp:TextBox> <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w32"></asp:ImageButton> <asp:ImageButton id="imbClearSupp" onclick="imbClearSupp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton> <asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w34"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"></TD><TD class="Label" align=center runat="server" Visible="true"></TD><TD class="Label" align=left runat="server" Visible="true"><asp:GridView id="gvSupplier" runat="server" Width="98%" Font-Size="X-Small" BackColor="White" Visible="False" __designer:wfdid="w9" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" DataKeyNames="ID,Name" BorderColor="#DEDFDE" AllowPaging="True" OnPageIndexChanging="gvSupplier_PageIndexChanging" DataSourceID="SqlDataSource1" AllowSorting="True">
<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="False">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" Width="350px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdatasupp" runat="server" CssClass="Important" Text="No data found !!" __designer:wfdid="w90"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="Navy"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"><asp:Label id="Label2" runat="server" Text="Item" __designer:wfdid="w35"></asp:Label></TD><TD class="Label" align=center runat="server" Visible="true"><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w36"></asp:Label></TD><TD class="Label" align=left runat="server" Visible="true"><asp:TextBox id="txtItemName" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w37"></asp:TextBox> <asp:ImageButton id="imbSearchItem" onclick="imbSearchItem_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w38"></asp:ImageButton> <asp:ImageButton id="imbClearItem" onclick="imbClearItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <asp:Label id="lblItemId" runat="server" Visible="False" __designer:wfdid="w40"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"></TD><TD class="Label" align=center runat="server" Visible="true"></TD><TD class="Label" align=left runat="server" Visible="true"><asp:GridView id="gvItem" runat="server" Width="98%" Font-Size="X-Small" BackColor="White" Visible="False" __designer:wfdid="w10" OnSelectedIndexChanged="gvItem_SelectedIndexChanged" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" DataKeyNames="ID,Item Name" BorderColor="#DEDFDE" AllowPaging="True" OnPageIndexChanging="gvItem_PageIndexChanging" DataSourceID="SqlDataSource2" EmptyDataText="No data found !">
<RowStyle BackColor="#F7F7DE" Font-Size="X-Small"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True"></asp:CommandField>
<asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code"></asp:BoundField>
<asp:BoundField DataField="Item Name" HeaderText="Item Name" SortExpression="Item Name"></asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdataitem" runat="server" CssClass="Important" Text="No data found !!" __designer:wfdid="w90"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="Navy"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 81px" id="TD6" class="Label" align=right runat="server" Visible="true"></TD><TD id="TD5" class="Label" align=center runat="server" Visible="true"></TD><TD id="TD3" class="Label" align=left runat="server" Visible="true"><asp:TextBox id="DPno" runat="server" Width="148px" CssClass="inpText" Visible="False" __designer:wfdid="w4" MaxLength="20"></asp:TextBox><asp:ImageButton id="btnSearchDP" onclick="btnSearchDP_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w5"></asp:ImageButton><asp:ImageButton id="EraseDP" onclick="EraseDP_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w6"></asp:ImageButton><asp:Label id="DPoid" runat="server" Font-Size="X-Small" Visible="False" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px" class="Label" align=right runat="server" Visible="true"></TD><TD class="Label" align=center runat="server" Visible="true"></TD><TD class="Label" align=left runat="server" Visible="true"><asp:GridView id="GVDP" runat="server" Width="98%" Font-Size="X-Small" Visible="False" __designer:wfdid="w8" OnSelectedIndexChanged="GVDP_SelectedIndexChanged" CellPadding="4" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" DataKeyNames="salesdeliveryoid,dodate,salesdeliveryno,salesdeliverystatus" BorderColor="#DEDFDE" AllowPaging="True">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="salesdeliveryno" HeaderText="DP No">
<HeaderStyle Font-Size="X-Small" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dodate" HeaderText="DP Date">
<HeaderStyle Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="salesdeliverystatus" HeaderText="Status">
<HeaderStyle Font-Size="X-Small" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
<asp:Label id="Label89" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow"></SelectedRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=center colSpan=1><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w45" TargetControlID="Date1" PopupButtonID="search1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w46" TargetControlID="Date2" PopupButtonID="search2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w47" TargetControlID="date1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w48" TargetControlID="date2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="TEXT-ALIGN: center" align=center colSpan=1><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" onclick="imbExport_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton>&nbsp;<asp:ImageButton id="ToPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton>&nbsp;<asp:ImageButton id="VAll" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w52"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w53"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=1><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w55" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer><CR:CrystalReportViewer id="CrystalReportViewer2" runat="server" Visible="False" __designer:wfdid="w56" AutoDataBind="true"></CR:CrystalReportViewer> </TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w57"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" Visible="False" __designer:wfdid="w58"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer" __designer:wfdid="w59"></asp:Label></TD></TR><TR><TD style="HEIGHT: 25px; TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText" __designer:wfdid="w60"><asp:ListItem Value="Name">Name</asp:ListItem>
<asp:ListItem Value="Code">Code</asp:ListItem>
<asp:ListItem Enabled="False">Type</asp:ListItem>
<asp:ListItem Enabled="False">Group</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w61"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" onclick="ibtnSuppID_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w62" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAll" onclick="imbViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w63" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 293px" align=left><FIELDSET style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 281px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 100%"></DIV></FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False" __designer:wfdid="w65">[ Close ]</asp:LinkButton><asp:SqlDataSource id="SqlDataSource1" runat="server" __designer:wfdid="w66" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" SelectCommand="SELECT CUSTOID AS ID, CUSTCODE AS Code, CUSTNAME AS Name FROM QL_MSTCUST AS c WHERE (CMPCODE LIKE @cmpcode) AND (CUSTFLAG = 'ACTIVE') AND (CUSTCODE LIKE @code) AND (CUSTNAME LIKE @name) and custgroup='PROJECK' ORDER BY CUSTCODE" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"><SelectParameters>
<asp:Parameter Name="cmpcode"></asp:Parameter>
<asp:Parameter Name="code"></asp:Parameter>
<asp:Parameter Name="name"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w67" TargetControlID="hiddenbtn2" PopupDragHandleControlID="lblSuppdata" PopupControlID="Panel1" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2" runat="server" Visible="False" __designer:wfdid="w68"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:wfdid="w69"><ContentTemplate>
<asp:Panel id="pnlSearchItem" runat="server" Width="500px" CssClass="modalBox" Visible="False" __designer:wfdid="w70"><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 15px" align=center><asp:Label id="lblCustData" runat="server" Width="177px" Font-Size="Medium" Font-Bold="True" Text="List of Items" __designer:wfdid="w71"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" align=center>Filter : <asp:DropDownList id="DDLItemID" runat="server" CssClass="inpText" __designer:wfdid="w72"><asp:ListItem Value="Name">Name</asp:ListItem>
<asp:ListItem Value="Code">Code</asp:ListItem>
<asp:ListItem Enabled="False">Type</asp:ListItem>
<asp:ListItem Enabled="False">Group</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindItemID" runat="server" Width="121px" CssClass="inpText" __designer:wfdid="w73"></asp:TextBox> <asp:ImageButton id="btnFindItemID" onclick="btnFindItemID_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w74" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="btnViewAllItem" onclick="btnViewAllItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w75" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 17px" align=right>&nbsp;<FIELDSET style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 281px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 105%"></DIV></FIELDSET>&nbsp; </TD></TR><TR><TD style="HEIGHT: 15px" align=center><asp:LinkButton id="CloseItem" onclick="CloseItem_Click" runat="server" CausesValidation="False" Font-Bold="False" __designer:wfdid="w77">[ Close ]</asp:LinkButton><asp:SqlDataSource id="SqlDataSource2" runat="server" __designer:wfdid="w78" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" SelectCommand="SELECT mi.itemoid AS ID, mi.itemcode AS Code, mi.itemdesc AS [Item Name],mi.merk FROM QL_mstitem as mi WHERE (mi.cmpcode LIKE @icmpcode) AND (mi.itemcode LIKE @icode) AND (mi.itemdesc LIKE @iname) or (mi.merk LIKE @iname) ORDER BY mi.itemcode" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>"><SelectParameters>
<asp:Parameter Name="icmpcode"></asp:Parameter>
<asp:Parameter Name="icode"></asp:Parameter>
<asp:Parameter Name="iname"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" __designer:wfdid="w79" TargetControlID="btnHideItem" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlSearchItem" PopupDragHandleControlID="lblCustData"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" Visible="False" __designer:wfdid="w80"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanelValidasi" runat="server" __designer:dtid="1125899906842658" __designer:wfdid="w81"><ContentTemplate __designer:dtid="1125899906842659">
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w82"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align=left colSpan=2><asp:Label id="Labelvalidasi" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w83"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px" __designer:wfdid="w84"></asp:Image></TD><TD vAlign=middle align=left><asp:Label id="Validasi" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w85"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Visible="False" __designer:wfdid="w86"></asp:Label></TD></TR><TR><TD colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w87"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" __designer:wfdid="w88" TargetControlID="ButtonExtendervalidasi" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelValidasi" PopupDragHandleControlID="Labelvalidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w89"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <BR />
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="CrystalReportViewer1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="CrystalReportViewer2"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

