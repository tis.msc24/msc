<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptLRPerItem.aspx.vb" Inherits="rptPerItem" title="Toko Ali - Laporan Laba Rugi Per Item" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias">
        <tr>
            <th align="left" class="header" valign="center" style="width: 975px">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Laba Rugi Per Item"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="width: 100%; background-color: transparent" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD vAlign=top align=left runat="server" Visible="true">Cabang</TD><TD vAlign=top align=left runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="DDLcbg" runat="server" Width="154px" CssClass="inpText" __designer:wfdid="w35" AutoPostBack="True" visible="true"></asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" vAlign=top align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w2"></asp:Label></TD><TD id="Td1" vAlign=top align=left runat="server" Visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;- <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w5" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w7">(DD/MM/YYY)</asp:Label></TD></TR><TR><TD id="Td2" vAlign=top align=left runat="server" Visible="true">Sales</TD><TD id="Td3" vAlign=top align=left runat="server" Visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlSales" runat="server" Width="154px" CssClass="inpText" __designer:wfdid="w12"></asp:DropDownList></TD></TR><TR><TD vAlign=top align=left runat="server" Visible="true">Customer</TD><TD vAlign=top align=left runat="server" Visible="true">:</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="custcode" runat="server" Width="158px" CssClass="inpTextDisabled" __designer:wfdid="w32" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w33"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w34"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=left>Katalog</TD><TD vAlign=top align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="ItemName" runat="server" Width="158px" CssClass="inpTextDisabled" __designer:wfdid="w60" TextMode="MultiLine" Rows="2" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w61"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w62"></asp:ImageButton> </TD></TR><TR><TD vAlign=top align=left colSpan=5><asp:ImageButton id="btnviewreportasli" onclick="btnviewreportasli_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w25"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w29"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Sedang proses....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w30"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w100" Format="dd/MM/yyyy" TargetControlID="dateAwal" PopupButtonID="imageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w101" Format="dd/MM/yyyy" TargetControlID="dateAkhir" PopupButtonID="imageButton2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w102" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w103" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w31" ShowAllPageIds="True" AutoDataBind="true" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" HasPrintButton="False" HasSearchButton="False" HasToggleGroupTreeButton="False" HasViewList="False"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnviewreportasli"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" Drag="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel>
                <asp:UpdatePanel id="upListCust" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText">
                                    <asp:ListItem Value="custcode">Code</asp:ListItem>
                                    <asp:ListItem Value="custname">Name</asp:ListItem>
                                    <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                </asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" CellPadding="4" PageSize="8" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                            <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" Drag="True" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Large" Font-Bold="True" Text="List Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText">
                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                        <asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp; </TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis : <asp:DropDownList id="stockFlag" runat="server" CssClass="inpText" Font-Size="Small">
                                                        <asp:ListItem>ALL</asp:ListItem>
                                                        <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                        <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                        <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                        <asp:ListItem>ASSET</asp:ListItem>
                                                    </asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnALLSelectItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedItem" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 247px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" CellPadding="4" PageSize="8" DataKeyNames="itemcode" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                <HeaderStyle CssClass="gvhdr" Font-Strikeout="False" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="merk" HeaderText="Merk">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="satuan3" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemflag" HeaderText="Status">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                            HorizontalAlign="Right" />
                                        <EmptyDataTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="Klik tombol View All atau Find untuk tampilkan data"></asp:Label>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView> </DIV>&nbsp;</TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

