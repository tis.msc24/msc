Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWhSupplier
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub fCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(drCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(drCabang, sSql)
            Else
                FillDDL(drCabang, sSql)
                drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                drCabang.SelectedValue = "SEMUA BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(drCabang, sSql)
            drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            drCabang.SelectedValue = "SEMUA BRANCH"
        End If
    End Sub

    Private Sub TujuanCabDDL()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(TujuanCb, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(TujuanCb, sSql)
            Else
                FillDDL(TujuanCb, sSql) 
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(TujuanCb, sSql) 
        End If
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim transdate As New Date
        Dim fromlocationoid As Integer = 0 : Dim tolocationoid As Integer = 0

        sSql = "SELECT TOP 1 a.InTransferNo, a.trntrfdate, a.trnTrfToReturNo, a.flag, b.mtrlocoid_from, b.mtrlocoid_to, a.Trntrfnote, a.status, a.updtime, a.upduser, fromBranch, toBranch,(Select Distinct FromMtrlocoid FROM QL_trntrfmtrmst t Where a.trnTrfToReturNo=transferno ANd t.fromMtrBranch=a.FromBranch) FromMtrloc FROM ql_InTransfermst a INNER JOIN ql_InTransferDtl b ON a.cmpcode = b.cmpcode AND a.InTransferoid = b.InTransferoid WHERE a.cmpcode = '" & cmpcode & "' AND a.InTransferoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                TujuanCb.SelectedValue = xreader("tobranch")
                transferno.Text = xreader("InTransferNo")
                transdate = xreader("trntrfdate")
                transferdate.Text = Format(xreader("trntrfdate"), "dd/MM/yyyy")
                noref.Text = xreader("trnTrfToReturNo")
                pooid.Text = xreader("trnTrfToReturNo")
                rblflag.SelectedValue = xreader("flag")
                InitAllDDL()
                FromLoc.Text = xreader("FromMtrloc")
                fromlocation.SelectedValue = xreader("mtrlocoid_from")
                tolocation.SelectedValue = xreader("mtrlocoid_to")
                note.Text = xreader("Trntrfnote")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                FromBranch.SelectedValue = xreader("fromBranch")
                ToBranch.SelectedValue = xreader("tobranch")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()
        rblflag_SelectedIndexChanged(Nothing, Nothing)
        sSql = "SELECT DISTINCT a.trfmtrmstoid, a.seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b.satuan1 satuan2, b.satuan1 satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, d.gendesc AS unit2, d.gendesc AS unit3, a.trfdtlnote AS note,isnull((case g.flag when 'Purchase' then ((select sum(qty_to) from QL_trntrfmtrmst p INNER JOIN QL_trntrfmtrdtl q ON p.cmpcode = q.cmpcode AND p.trfmtrmstoid = q.trfmtrmstoid where p.transferno = '" & Tchar(pooid.Text) & "' AND q.refoid = b.itemoid and q.refname = 'ql_mstitem') - (SELECT ISNULL(SUM(qty_to), 0) FROM ql_InTransferDtl v INNER JOIN ql_InTransfermst w ON v.cmpcode = w.cmpcode AND v.InTransferoid = w.InTransferoid WHERE w.trnTrfToReturNo = '" & Tchar(pooid.Text) & "' AND v.refoid = b.itemoid AND v.refname = 'ql_mstitem' and v.InTransferoid <> " & Integer.Parse(Session("oid")) & ") - (select isnull(sum(case ab.unitseq when 1 then (ab.trnbelireturdtlqty*cast(id.konversi1_2 as decimal(18,2))*cast(id.konversi2_3 as decimal(18,2))) when 2 then (ab.trnbelireturdtlqty*cast(id.konversi2_3 as decimal(18,2))) when 3 then (ab.trnbelireturdtlqty) end), 0) from QL_trnbeliReturdtl ab inner join QL_trnbeliReturmst cd on ab.cmpcode = cd.cmpcode and ab.trnbeliReturmstoid = cd.trnbeliReturmstoid inner join ql_mstitem id on ab.cmpcode = id.cmpcode and ab.itemoid = id.itemoid where cd.trnTrfToReturNo = '" & Tchar(pooid.Text) & "' and ab.itemoid = a.refoid and cd.trnbelistatus in ('Approved','POST'))) when 'Supplier' then (select saldoakhir-qtybooking from QL_crdmtr where a.refoid = refoid and mtrlocoid = " & fromlocationoid & " AND periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "') end),0) sisaretur,ISNULL(a.statusexp,'TIDAK') statusexp,ISNULL(a.typedimensi,0) typedimensi,ISNULL(a.beratvolume,0.00) beratvolume,ISNULL(a.beratbarang,0.00) beratbarang,ISNULL(a.jenisexp,'') jenisexp,ISNULL(a.amtexpedisi,0.00) amtexpedisi,ISNULL(a.nettoexp,0.00) nettoexp FROM ql_InTransferDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' INNER JOIN ql_InTransfermst g ON a.cmpcode = g.cmpcode AND a.InTransferoid = g.InTransferoid WHERE a.cmpcode = '" & cmpcode & "' AND a.InTransferoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailts")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "IN PROCESS" Then
            btnSave.Visible = True : btnDelete.Visible = True
            BtnCancel.Visible = True : btnPosting.Visible = True
        ElseIf trnstatus.Text = "POST" Then
            btnSave.Visible = False : btnDelete.Visible = False
            BtnCancel.Visible = True : btnPosting.Visible = False
        End If
        conn.Close()
    End Sub

    Private Sub BindData(ByVal sWhere As String)
        Dim sBranch As String = ""
        If CbTanggal.Checked = True Then
            Dim date1, date2 As New Date
            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)
            sWhere &= "AND CONVERT(DATE, c.trntrfdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "'"
        End If

        If ddlStatus.SelectedValue.ToUpper <> "ALL" Then
            sWhere &= " AND status='" & ddlStatus.SelectedValue.ToUpper & "'"
        End If

        If FilterText.Text.Trim <> "" Then
            sWhere &= "AND (c.InTransferNo LIKE '%" & Tchar(FilterText.Text.Trim) & "%' Or c.trnTrfToReturNo LIKE '%" & Tchar(FilterText.Text.Trim) & "%') "
        End If

        If drCabang.SelectedValue <> "SEMUA BRANCH" Then
            sWhere &= " AND c.tobranch='" & drCabang.SelectedValue & "'"
        End If

        sSql = "SELECT DISTINCT c.InTransferoid, c.InTransferNo, c.trntrfdate, c.upduser, c.status,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=c.ToBranch AND tb.cmpcode=c.cmpcode AND gengroup='CABANG') ToBranch,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=c.FromBranch AND tb.cmpcode=c.cmpcode AND gengroup='CABANG') FromBranch FROM ql_InTransfermst c WHERE c.cmpcode='MSC' " & sWhere & " ORDER BY c.InTransferoid DESC"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
        gvMaster.DataSource = dtab
        gvMaster.DataBind() : Session("ts") = dtab
    End Sub

    Private Sub BindItem(ByVal state As Integer)
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing) 
        sSql = "Select h.trfmtrmstoid,a.itemoid, a.itemcode, a.itemdesc, a.merk, 0.00 as sisa, a.satuan1, d.gendesc AS unit1, a.satuan1, d.gendesc AS unit2, a.satuan1 satuan2,a.satuan1 satuan3, d.gendesc AS unit3, 1 konversi1_2,1 konversi2_3, h.unitoid, j.gendesc as unit, i.transferno trnTrfToReturNo ,h.qty_to-(Select ISNULL(SUM(qty_to),0.00) from ql_InTransferDtl tcd Inner Join ql_InTransfermst tcm ON tcd.InTransferoid=tcm.InTransferoid Where tcm.trnTrfToReturNo='" & Tchar(noref.Text) & "' AND tcd.refoid=h.refoid AND tcd.trfmtrmstoid=h.trfmtrmstoid) sisaretur,ISNULL(h.statusexp,'TIDAK') statusexp,ISNULL(h.typedimensi,0) typedimensi,ISNULL(h.beratvolume,0.00) beratvolume,ISNULL(h.beratbarang,0.00) beratbarang,ISNULL(h.jenisexp,'') jenisexp,ISNULL(h.amtexpedisi,0.00) amtexpedisi,ISNULL(h.nettoexp,0.00) nettoexp From ql_mstitem a inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' inner join QL_trntrfmtrdtl h on a.cmpcode = h.cmpcode and a.itemoid = h.refoid And h.refname = 'ql_mstitem' inner join QL_trntrfmtrmst i on h.cmpcode = i.cmpcode and h.trfmtrmstoid = i.trfmtrmstoid inner join QL_mstgen j on h.cmpcode = j.cmpcode and h.unitoid = j.genoid Where a.cmpcode = 'MSC' and i.transferno like '%" & Tchar(noref.Text) & "%' And (select sum(qty_to) from QL_trntrfmtrdtl Where cmpcode = a.cmpcode and trfmtrmstoid = i.trfmtrmstoid and refoid = a.itemoid and refname = 'ql_mstitem') - (select isnull(sum(qty_to), 0) from ql_InTransferDtl m inner join ql_InTransfermst n on m.cmpcode = n.cmpcode and m.InTransferoid = n.InTransferoid and n.trnTrfToReturNo like '%" & Tchar(noref.Text) & "%' and m.refoid = a.itemoid and m.refname = 'ql_mstitem' " & IIf(i_u.Text <> "new", " and m.InTransferoid <> " & Session("oid") & "", "") & ") > 0 And (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%')"
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable
        GVItemList.DataBind()
        Session("itemlistwhretur") = objTable
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        sSql = "Select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & TujuanCb.SelectedValue & "'"
        FillDDL(ToBranch, sSql)

        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' --and gencode = '" & Session("branch_id") & "'"
        FillDDL(FromBranch, sSql)

        FillDDL(fromlocation, "SELECT a.genoid, a.gendesc FROM QL_mstgen a where a.gengroup = 'LOCATION' and a.gencode = 'EXPEDISI' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' and a.genother6='UMUM' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " and a.genother2 = (select genoid from ql_mstgen where gencode = '" & TujuanCb.SelectedValue & "' AND gengroup='CABANG') ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        tolocation.SelectedIndex = 0
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnWhSupplier.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Confirm"

        If Not Page.IsPostBack Then
            fCabang() : BindData("")
            transferno.Text = GenerateID("ql_InTransfermst", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            TujuanCabDDL() : InitAllDDL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0" : GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1" : labelkonversi1_2.Text = "1"
                labelkonversi2_3.Text = "1"
            End If
        End If

        'If Not Session("itemdetail") Is Nothing Then
        '    Dim dtab As DataTable = Session("itemdetail")
        '    GVItemDetail.DataSource = dtab
        '    GVItemDetail.DataBind()
        'End If

    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        CbTanggal.Checked = False : FilterText.Text = ""
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim Pesan As String = ""
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            Pesan &= "Tanggal period harus diisi..!!!<br />"
        End If
        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Pesan &= "Tanggal period 1 tidak valid..!!!<br />"
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Pesan &= "Tanggal period 2 tidak valid..!!<br />"
        End If

        If date1 > date2 Then
            Pesan &= "Tanggal period 1 tidak boleh lebih dari period 2..!!!<br />" 
        End If
        If Pesan <> "" Then
            showMessage(Pesan, 2)
            Exit Sub
        End If
        BindData("")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            If rblflag.SelectedValue.ToString = "Purchase" Then
                If noref.Text.Trim = "" Then
                    showMessage("Pilih nomor ref (TR) terlebih dahulu!", 2)
                    Exit Sub
                End If
                If noref.Text <> pooid.Text Then
                    showMessage("Pilih nomor ref (TR) terlebih dahulu!", 2)
                    Exit Sub
                End If
                BindItem(1)
            Else
                If fromlocation.Items.Count <= 0 Then
                    showMessage("Pilih gudang supplier terlebih dahulu!", 2)
                    Exit Sub
                End If
                BindItem(2)
            End If
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = "" : qty.Text = "0"
        labelmaxqty.Text = "0" : merk.Text = "" : notedtl.Text = ""

        labelsatuan1.Text = "" : labelsatuan2.Text = "" : labelsatuan3.Text = ""
        labelunit1.Text = "" : labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        If rblflag.SelectedValue.ToString = "Purchase" Then
            GVItemList.PageIndex = e.NewPageIndex
            BindItem(1)
        Else
            GVItemList.PageIndex = e.NewPageIndex
            BindItem(2)
        End If
        GVItemList.Visible = True
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.SelectedDataKey("itemdesc").ToString
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        labelmaxqty.Text = ToMaskEdit(GVItemList.SelectedDataKey("sisaretur"), 3)
        labeltempsisaretur.Text = GVItemList.SelectedDataKey("sisaretur")
        labelsatuan.Text = GVItemList.SelectedDataKey("unitoid")
        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.SelectedDataKey("unit3").ToString
        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")
        trfmtrmstoid.Text = GVItemList.SelectedDataKey("trfmtrmstoid")
        unit.Items.Clear()
        unit.Items.Add(GVItemList.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If

        sSql = "select genoid,gendesc from ql_mstgen where gengroup='ITEMUNIT'"
        FillDDL(unit, sSql)
        unit.SelectedValue = GVItemList.SelectedDataKey("unitoid")

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        StatusExp.Text = GVItemList.SelectedDataKey("statusexp").ToString
        If StatusExp.Text.ToUpper = "YA" Then
            typedimensi.Text = GVItemList.SelectedDataKey("typedimensi").ToString
            If typedimensi.Text = 1 Then
                beratvolume.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("beratvolume")), 3)
                beratbarang.Text = ToMaskEdit(ToDouble(0), 3)
            Else
                beratvolume.Text = ToMaskEdit(ToDouble(0), 3)
                beratbarang.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("beratbarang")), 3)
            End If
            jenisexp.Text = GVItemList.SelectedDataKey("jenisexp").ToString
            amtexpedisi.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("amtexpedisi")), 3)
            NettoExp.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("nettoexp")), 3)
        Else
            typedimensi.Text = 0
            beratvolume.Text = ToMaskEdit(ToDouble(0), 3)
            beratbarang.Text = ToMaskEdit(ToDouble(0), 3)
            jenisexp.Text = ""
            amtexpedisi.Text = ToMaskEdit(ToDouble(0), 3)
            NettoExp.Text = ToMaskEdit(ToDouble(0), 3)
        End If

        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()
        unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnWhSupplier.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        qty.Text = ToDouble(qty.Text)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari Nilai Max Quantity!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("sisaretur", Type.GetType("System.Double"))
                dtab.Columns.Add("trfmtrmstoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("statusexp", Type.GetType("System.String"))
                dtab.Columns.Add("typedimensi", Type.GetType("System.Int32"))
                dtab.Columns.Add("beratvolume", Type.GetType("System.Double"))
                dtab.Columns.Add("beratbarang", Type.GetType("System.Double"))
                dtab.Columns.Add("jenisexp", Type.GetType("System.String"))
                dtab.Columns.Add("amtexpedisi", Type.GetType("System.Double"))
                dtab.Columns.Add("nettoexp", Type.GetType("System.Double"))

                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        Dim dv As DataView = dtab.DefaultView
        If I_u2.Text = "new" Then
            dv.RowFilter = "itemoid=" & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "'"
        Else
            dv.RowFilter = "itemoid=" & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & ""
        End If

        If dv.Count > 0 Then
            showMessage("Maaf, Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list", 2)
            dv.RowFilter = ""
            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
            Exit Sub
        End If
        dv.RowFilter = ""

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text
            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text
            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)
            drow("trfmtrmstoid") = Double.Parse(trfmtrmstoid.Text)

            If StatusExp.Text.ToUpper = "YA" Then
                drow("statusexp") = StatusExp.Text
                drow("typedimensi") = Integer.Parse(typedimensi.Text)
                If drow("typedimensi") = 1 Then
                    drow("beratvolume") = ToMaskEdit(ToDouble(beratvolume.Text), 3)
                    drow("beratbarang") = ToMaskEdit(ToDouble(0), 3)
                Else
                    drow("beratvolume") = ToMaskEdit(ToDouble(0), 3)
                    drow("beratbarang") = ToMaskEdit(ToDouble(beratbarang.Text), 3)
                End If
                drow("jenisexp") = Tchar(jenisexp.Text)
                drow("amtexpedisi") = ToMaskEdit(ToDouble(amtexpedisi.Text), 3)
                drow("nettoexp") = ToMaskEdit(ToDouble(NettoExp.Text), 3)
            Else
                drow("statusexp") = "TIDAK"
                drow("typedimensi") = Integer.Parse(0)
                drow("beratvolume") = ToMaskEdit(ToDouble(0), 3)
                drow("beratbarang") = ToMaskEdit(ToDouble(0), 3)
                drow("jenisexp") = ""
                drow("amtexpedisi") = ToMaskEdit(ToDouble(0), 3)
                drow("nettoexp") = ToMaskEdit(ToDouble(0), 3)
            End If

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text
            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text
            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim
            drow("sisaretur") = Double.Parse(labeltempsisaretur.Text)
            drow("trfmtrmstoid") = Double.Parse(trfmtrmstoid.Text)

            If StatusExp.Text.ToUpper = "YA" Then
                drow("statusexp") = StatusExp.Text
                drow("typedimensi") = Integer.Parse(typedimensi.Text)
                If drow("typedimensi") = 1 Then
                    drow("beratvolume") = ToMaskEdit(ToDouble(beratvolume.Text), 3)
                    drow("beratbarang") = ToMaskEdit(ToDouble(0), 3)
                Else
                    drow("beratvolume") = ToMaskEdit(ToDouble(0), 3)
                    drow("beratbarang") = ToMaskEdit(ToDouble(beratbarang.Text), 3)
                End If
                drow("jenisexp") = Tchar(jenisexp.Text)
                drow("amtexpedisi") = ToMaskEdit(ToDouble(amtexpedisi.Text), 3)
                drow("nettoexp") = ToMaskEdit(ToDouble(NettoExp.Text), 3)
            Else
                drow("statusexp") = "TIDAK"
                drow("typedimensi") = Integer.Parse(0)
                drow("beratvolume") = ToMaskEdit(ToDouble(0), 3)
                drow("beratbarang") = ToMaskEdit(ToDouble(0), 3)
                drow("jenisexp") = ""
                drow("amtexpedisi") = ToMaskEdit(ToDouble(0), 3)
                drow("nettoexp") = ToMaskEdit(ToDouble(0), 3)
            End If

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = "" : qty.Text = "0" : labelmaxqty.Text = "0"
        unit.Items.Clear() : I_u2.Text = "new"
        notedtl.Text = "" : labelitemoid.Text = "" : item.Text = ""
        trfmtrmstoid.Text = ""

        StatusExp.Text = "TIDAK" : typedimensi.Text = 0
        beratvolume.Text = 0 : beratbarang.Text = 0
        jenisexp.Text = "" : amtexpedisi.Text = 0

        labelsatuan1.Text = "" : labelsatuan2.Text = "" : labelsatuan3.Text = ""
        labelunit1.Text = "" : labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0"
        item.Text = "" : labelmaxqty.Text = "0"
        unit.Items.Clear()
        notedtl.Text = "" : merk.Text = "" : trfmtrmstoid.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = "" : labelsatuan2.Text = "" : labelsatuan3.Text = ""
        labelunit1.Text = "" : labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If
        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text

        unit.Items.Clear()
        unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")

        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If

        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If

        unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")
        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")
        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")
        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text
        labeltempsisaretur.Text = Format(GVItemDetail.SelectedDataKey("sisaretur"))
        trfmtrmstoid.Text = Format(GVItemDetail.SelectedDataKey("trfmtrmstoid"))

        Dim maxqty As Double = 0.0
        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi1_2.Text) / Double.Parse(labelkonversi2_3.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If

        labelmaxqty.Text = Format(maxqty, "#,##0.0000")
        StatusExp.Text = GVItemDetail.SelectedDataKey("statusexp").ToString

        If StatusExp.Text.ToUpper = "YA" Then
            typedimensi.Text = GVItemDetail.SelectedDataKey("typedimensi").ToString
            If typedimensi.Text = 1 Then
                beratvolume.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("beratvolume")), 3)
                beratbarang.Text = ToMaskEdit(ToDouble(0), 4)
            Else
                beratvolume.Text = ToMaskEdit(ToDouble(0), 4)
                beratbarang.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("beratbarang")), 3)
            End If
            jenisexp.Text = GVItemDetail.SelectedDataKey("jenisexp").ToString
            amtexpedisi.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("beratbarang")), 3)
        Else
            typedimensi.Text = 0
            beratvolume.Text = ToMaskEdit(ToDouble(0), 4)
            beratbarang.Text = ToMaskEdit(ToDouble(0), 4)
            jenisexp.Text = ""
            amtexpedisi.Text = ToMaskEdit(ToDouble(0), 4)
        End If

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        Dim period As String = GetDateToPeriodAcctg(transdate)

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong ! <br />"
        End If
        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong ! <br />"
        End If
        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong ! <br />"
        End If

        If noref.Text = "" And pooid.Text = "" Then
            errmsg &= "Maaf, No. TW belum dipilih..!!"
        End If

        If Session("itemdetail") Is Nothing Then
            errmsg &= "Maaf, data detail belum ada..!!"
        End If

        'Cek period is open stock
        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & GetDateToPeriodAcctg(transdate) & "' AND closingdate = '1/1/1900'"
        Dim cekPeriod As Double = GetScalar(sSql)
        If ToDouble(cekPeriod) = 0 Then
            errmsg &= "Tanggal ini tidak dalam periode Open Stock!"
        End If

        If Not Session("itemdetail") Is Nothing Then
            Dim SaldoAkhire As Double = 0.0
            Dim dtab As DataTable = Session("itemdetail")
            For j As Integer = 0 To dtab.Rows.Count - 1
                sSql = "Select h.qty_to-(Select ISNULL(SUM(tcd.qty_to),0.00) from ql_InTransferDtl tcd Inner Join ql_InTransfermst tcm ON tcd.InTransferoid=tcm.InTransferoid Where tcd.trfmtrmstoid=h.trfmtrmstoid AND tcd.refoid=h.refoid AND status='POST') SisaNya From QL_trntrfmtrdtl h Where h.cmpcode = '" & cmpcode & "' AND h.trfmtrmstoid=" & dtab.Rows(j).Item("trfmtrmstoid") & " AND h.refoid=" & dtab.Rows(j).Item("itemoid") & ""
                SaldoAkhire = GetStrData(sSql)
                If ToDouble(dtab.Rows(j).Item("qty")) > ToDouble(SaldoAkhire) Then
                    errmsg &= "Maaf, Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir hanya tersedia " & SaldoAkhire & "..!!<br />"
                End If
            Next
        End If

        If i_u.Text <> "new" Then
            sSql = "SELECT status FROM ql_InTransfermst WHERE InTransferoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                errmsg &= "Maaf, Data transfer gudang tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain..!!<br />"
            Else
                If srest.ToLower = "post" Then
                    errmsg &= "Maaf, Data transfer gudang Status sudah terposting !<br />Klik tombol cancel dan periksa ulang data anda..!!<br />"
                End If
            End If
        End If

        If errmsg.Trim <> "" Then
            trnstatus.Text = "IN PROCESS"
            showMessage(errmsg, 2) : Exit Sub
        End If

        Dim trfmtrmstoid As Int32 = GenerateID("ql_InTransfermst", cmpcode)
        Dim trfmtrdtloid As Int32 = GenerateID("ql_InTransferDtl", cmpcode)
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)
        Dim crdmtroid As Int32 = GenerateID("ql_crdmtr", cmpcode)
        Dim tempoid As Int32 = GenerateID("ql_crdmtr", cmpcode)
        Dim glmstoid As Integer = GenerateID("QL_trnglmst", cmpcode)
        Dim gldtloid As Integer = GenerateID("QL_trngldtl", cmpcode)
        Dim glsequence As Integer = 1

        'Generate transfer number
        If trnstatus.Text = "POST" Then
            Dim iCurID As Integer = 0
            sSql = "select genother1 from ql_mstgen Where gencode='" & ToBranch.Text & "' And gengroup='CABANG'"
            Dim cabang As String = GetScalar(sSql)
            Dim trnno As String = "TC/" & cabang & "/" & Format(transdate, "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(InTransferNo, 4) AS INT)), 0) FROM ql_InTransfermst WHERE cmpcode = '" & cmpcode & "' AND InTransferNo LIKE '" & trnno & "%'"

            iCurID = GetStrData(sSql) + 1
            trnno = GenNumberString(trnno, "", iCurID, 4)
            transferno.Text = trnno
        Else
            If i_u.Text = "new" Then
                transferno.Text = trfmtrmstoid.ToString
            End If
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            'Insert new record
            If i_u.Text = "new" Then 
                sSql = "INSERT INTO ql_InTransfermst (cmpcode, InTransferoid, InTransferNo, trnTrfToReturNo, trntrfdate, personoid, Trntrfnote, updtime, upduser, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, flag, status,FromBranch,ToBranch) VALUES ('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "', '" & Tchar(pooid.Text) & "', '" & transdate & "', 0, '" & Tchar(note.Text.Trim) & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', '', '', '1/1/1900', '', '" & rblflag.SelectedValue & "', '" & trnstatus.Text & "','" & FromBranch.SelectedValue & "','" & ToBranch.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'ql_InTransfermst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else

                'Update master record
                sSql = "UPDATE ql_InTransfermst SET InTransferNo = '" & transferno.Text & "', trnTrfToReturNo = '" & Tchar(pooid.Text) & "', Trntrfnote = '" & Tchar(note.Text.Trim) & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, finalapprovaluser = '" & Session("UserID") & "', flag = '" & rblflag.SelectedValue & "', status = '" & trnstatus.Text & "', frombranch = '" & FromBranch.SelectedValue & "' , tobranch = '" & ToBranch.SelectedValue & "' WHERE InTransferoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM ql_InTransferDtl WHERE InTransferoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            '-------------------auto jurnal jika transfer gudang supplier --------------
            If trnstatus.Text = "POST" Then
                'Jurnal Supplier cz Sedian Supplier lawan Sedian Grosir
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,branch_code,type) VALUES ('" & cmpcode & "', " & glmstoid & ", '" & transdate & "', '" & period & "', 'TW No. " & transferno.Text & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & ToBranch.SelectedValue & "','TC')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & glmstoid & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0 : Dim qty_to As Double = 0.0
                Dim amthpp As Double = 0.0 : Dim COA_gudang As Integer = 0
                Dim vargudang As String = "" : Dim iMatAccount As String = ""
                Dim GudangCoa As Integer

                For i As Integer = 0 To objTable.Rows.Count - 1
                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If
                    qty_to = objTable.Rows(i).Item("qty")

                    sSql = "INSERT INTO ql_InTransferDtl (cmpcode, InTransferDtloid, InTransferoid, seq, refname, refoid, trfdtlnote, mtrlocoid_from, mtrlocoid_to, unitseq, qty, unitoid, unitseq_to, qty_to, unitoid_to, trfmtrmstoid,statusexp,typedimensi,beratvolume,beratbarang,jenisexp,amtexpedisi,nettoexp) " & _
                    "VALUES ('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("itemoid") & ", '" & Tchar(objTable.Rows(i).Item("note")) & "', " & fromlocation.SelectedValue & ", " & tolocation.SelectedValue & ", " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", 3, " & qty_to & ", " & objTable.Rows(i).Item("satuan3") & ", " & objTable.Rows(i).Item("trfmtrmstoid") & ",'" & Tchar(objTable.Rows(i).Item("statusexp").ToString.ToUpper) & "'," & objTable.Rows(i).Item("typedimensi") & "," & ToDouble(objTable.Rows(i).Item("beratvolume")) & "," & ToDouble(objTable.Rows(i).Item("beratbarang")) & ",'" & Tchar(objTable.Rows(i).Item("jenisexp").ToString.ToUpper) & "'," & ToDouble(objTable.Rows(i).Item("amtexpedisi")) & "," & ToDouble(objTable.Rows(i).Item("nettoexp")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then
                        '--------------------------

                        sSql = "select hpp from ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql
                        Dim lasthpp As Double = xCmd.ExecuteScalar
                        '---------------------------
                        amthpp = qty_to * lasthpp

                        'Update crdmtr for item out
                    
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note,HPP,Branch_Code) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TC', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_InTransferDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", 0, " & ToDouble(objTable.Rows(i).Item("qty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & ToBranch.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1

                        'Update crdmtr for item in
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code) VALUES ('" & cmpcode & "', " & conmtroid & ", 'TC', '" & transdate & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'ql_InTransferDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & tolocation.SelectedValue & ", " & objTable.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("note")) & "', " & ToDouble(lasthpp) & ",'" & ToBranch.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                    End If
                    trfmtrdtloid += 1
                    If trnstatus.Text.ToUpper = "POST" Then
                        'masukkan jurnal
                        '// ==================================
                        '// Persediaan Barang Supplier 10.000
                        '// persediaan barang Grosir   10.000
                        '// ==================================
                        '=======================AUTO JURNAL START ========================
                        ''insert ke jurnal
                        Dim xBRanch As String = "" : Dim sTockFlag As String
                        Dim CodeCoa As String = 0 : Dim mVar As String = ""
                        Dim sTxt As String = "" : Dim CbgDesc As String = ""
                        If tolocation.SelectedValue <> fromlocation.SelectedValue Then
                            sSql = "Select stockflag From ql_mstitem Where itemoid= " & Integer.Parse(objTable.Rows(i)("itemoid")) & ""
                            xCmd.CommandText = sSql : sTockFlag = xCmd.ExecuteScalar

                            sSql = "Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & tolocation.SelectedValue & " AND genother4='" & sTockFlag & "'"
                            xCmd.CommandText = sSql : xBRanch = xCmd.ExecuteScalar 
                            sSql = "Select gendesc From QL_mstgen WHERE gengroup='CABANG' AND gencode='" & xBRanch & "'"
                            xCmd.CommandText = sSql : CbgDesc = xCmd.ExecuteScalar 

                            If sTockFlag = "I" Then
                                mVar = "VAR_INVENTORY" : sTxt = "PERSEDIAAN NON JUAL"
                            ElseIf sTockFlag = "ASSET" Then
                                mVar = "VAR_GUDANG_ASSET" : sTxt = "PERSEDIAAN ASSET"
                            Else 
                                mVar = "VAR_GUDANG" : sTxt = "PERSEDIAAN BARANG DANGANGAN"
                            End If

                            iMatAccount = GetVarInterface(mVar, xBRanch)
                            If iMatAccount Is Nothing Or iMatAccount = "" Or iMatAccount = "?" Then
                                errmsg &= "Maaf, Interface " & mVar & " untuk COA " & sTxt & " cabang " & CbgDesc & " belum di setting silahkan hubungi staff accounting..!!<br />" 
                            Else
                                sSql = "Select acctgoid FROM ql_mstacctg WHERE acctgoid=(Select genother2 from QL_mstgen Where gengroup='COA' AND genother3='" & ToBranch.SelectedValue & "' AND genother1=" & tolocation.SelectedValue & " AND genother4='" & sTockFlag & "')"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                If COA_gudang = 0 Or COA_gudang = Nothing Then
                                    errmsg &= "Maaf, Interface " & mVar & " untuk COA " & sTxt & " cabang " & CbgDesc & " belum di setting silahkan hubungi staff accounting..!!<br />"
                                End If

                                If sTockFlag = "I" Or sTockFlag = "ASSET" Then
                                    mVar = "VAR_PERJALANAN_NON_JUAL" : sTxt = "PERSEDIAAN PERJALANAN NON JUAL"
                                Else
                                    mVar = "VAR_PERJALANAN" : sTxt = "PERSEDIAAN PERJALANAN BARANG DAGANGAN"
                                End If

                                CodeCoa = GetVarInterface(mVar, ToBranch.SelectedValue)
                                sSql = "Select acctgoid FROM ql_mstacctg WHERE acctgcode='" & CodeCoa & "'"
                                xCmd.CommandText = sSql : GudangCoa = xCmd.ExecuteScalar
                                If GudangCoa = 0 Or GudangCoa = Nothing Then
                                    errmsg &= "Maaf, Interface VAR_PERJALANAN untuk COA PERSEDIAAN DALAM PERJALANAN belum disetting, silahkan hubungi staff accounting..!!<br >"
                                End If
                            End If

                            If errmsg <> "" Then
                                trnstatus.Text = "IN PROCESS"
                                showMessage(errmsg, 2)
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            End If

                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime,glamtidr, branch_code) VALUES " & _
                            "('" & cmpcode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & ToDouble(amthpp) & ",'" & transferno.Text & "', 'Transfer Gudang No. " & transferno.Text & "','" & Integer.Parse(Session("oid")) & "','" & Integer.Parse(objTable.Rows(i)("itemoid")) & "','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & ToDouble(amthpp) & ", '" & ToBranch.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                            'Dim iMatSuppAccount As String = GetInterfaceValue("VAR_GUDANG")
                            'acctgoid = GetAccountOid(iMatAccount)
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime,glamtidr,branch_code) VALUES " & _
                            "('" & cmpcode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & GudangCoa & ", 'C', " & ToDouble(amthpp) & ", '" & transferno.Text & "', 'Transfer Gudang No. " & transferno.Text & "','" & Integer.Parse(Session("oid")) & "','" & Integer.Parse(objTable.Rows(i)("itemoid")) & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & ToDouble(amthpp) & ", '" & ToBranch.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                            sSql = "UPDATE QL_mstoid SET lastoid =" & gldtloid - 1 & "  WHERE tablename = 'QL_trngldtl' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If

                    If trnstatus.Text.ToUpper = "POST" Then
                        '----update price cabang tujuan----
                        '==================================
                        Dim PriceMst As Double = 0
                        sSql = "Select pricelist from ql_mstitem Where itemoid=" & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ""
                        xCmd.CommandText = sSql : PriceMst = xCmd.ExecuteScalar

                        If Tchar(objTable.Rows(i).Item("statusexp").ToString.ToUpper) = "YA" Then
                            Dim hrgCabang As Double = 0
                            '----------------------------------
                            hrgCabang = (ToDouble(PriceMst) * ToDouble(objTable.Rows(i).Item("qty"))) / ToDouble(objTable.Rows(i).Item("qty")) + (ToDouble(objTable.Rows(i).Item("qty")) * ToDouble(objTable.Rows(i).Item("nettoexp"))) / ToDouble(objTable.Rows(i).Item("qty"))
                            sSql = "UPDATE QL_mstItem_branch Set pricecabang=" & ToDouble(hrgCabang) & ",biayaExpedisi=" & ToDouble(objTable.Rows(i).Item("nettoexp")) & ",updtime=CURRENT_TIMESTAMP Where branch_code='" & ToBranch.SelectedValue & "' AND expedisi_type='" & objTable.Rows(i).Item("jenisexp") & "' AND itemoid=" & objTable.Rows(i).Item("itemoid") & ""
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    End If
                Next
                'Update lastoid ql_InTransferDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & " WHERE tablename = 'ql_InTransferDtl' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & crdmtroid - 1 & " WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit(): conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : trnstatus.Text = "IN PROCESS"
            conn.Close() : showMessage(sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhSupplier.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged

        Dim maxqty As Double = 0.0
        If unit.SelectedValue = Integer.Parse(labelsatuan1.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text) / Double.Parse(labelkonversi1_2.Text)
        ElseIf unit.SelectedValue = Integer.Parse(labelsatuan2.Text) Then
            maxqty = Double.Parse(labeltempsisaretur.Text) / Double.Parse(labelkonversi2_3.Text)
        Else
            maxqty = Double.Parse(labeltempsisaretur.Text)
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM ql_InTransfermst WHERE InTransferoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer gudang retur tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "POST" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer gudang retur tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_InTransfermst WHERE InTransferoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_InTransferDtl WHERE InTransferoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWhSupplier.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' AND a.genoid <> " & fromlocation.SelectedValue & " ORDER BY a.gendesc"
        FillDDL(tolocation, sSql)

        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0"
        item.Text = "" : labelmaxqty.Text = "0" : unit.Items.Clear()
        notedtl.Text = "" : merk.Text = "" : labelsatuan1.Text = ""
        labelsatuan2.Text = "" : labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = "" : labelkonversi1_2.Text = "1"
        labelkonversi2_3.Text = "1" : labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim sWhere As String = " WHERE a.InTransferoid = " & Integer.Parse(labeloid.Text) & ""
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "TWSprintoutConfirm1.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TC_" & labeloid.Text & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnWHSupplier.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        gvMaster.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub ibposearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposearch.Click
        bindDataPO()
        GVpo.Visible = True
    End Sub

    Protected Sub ibpoerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoerase.Click
        noref.Text = "" : pooid.Text = ""

        If GVpo.Visible = True Then
            GVpo.Visible = False
            GVpo.DataSource = Nothing
            GVpo.DataBind()
            Session("polistwhsupplier") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhsupplier") Is Nothing Then
            GVpo.DataSource = Session("polistwhsupplier")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Private Sub bindDataPO()
        sSql = "select a.transferno trnTrfToReturNo, a.trfmtrdate AS trntrfdate,trfmtrnote,ToMtrlocOid,fromMtrBranch,toMtrBranch ,(select sum(qty_to) From QL_trntrfmtrdtl Where cmpcode = a.cmpcode and trfmtrmstoid = a.trfmtrmstoid) totalqty, ((select isnull(sum(i.qty_to), 0) From ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) From ql_InTransfermst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid Where h.trnTrfToReturNo = a.transferno)) totaluse,a.FromMtrlocoid From QL_trntrfmtrmst a Where a.cmpcode = '" & cmpcode & "' And a.status = 'Approved' And toMtrBranch = '" & TujuanCb.SelectedValue & "' And ((select sum(qty_to) From QL_trntrfmtrdtl Where cmpcode = a.cmpcode and trfmtrmstoid = a.trfmtrmstoid)-((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid Where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) From ql_InTransfermst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid where h.trnTrfToReturNo = a.transferno))) > 0 And a.transferno like '%" & Tchar(noref.Text) & "%'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
        GVpo.DataSource = dtab
        GVpo.DataBind()
        Session("polistwhsupplier") = dtab
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        tolocation.SelectedValue = GVpo.SelectedDataKey("ToMtrlocOid")
        ToBranch.SelectedValue = GVpo.SelectedDataKey("toMtrBranch")
        TujuanCb.SelectedValue = GVpo.SelectedDataKey("toMtrBranch")
        FromBranch.SelectedValue = GVpo.SelectedDataKey("fromMtrBranch")
        FromLoc.Text = GVpo.SelectedDataKey("FromMtrlocoid")
        GVpo.Visible = False : GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhsupplier") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub rblflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblflag.SelectedIndexChanged
        If rblflag.SelectedValue = "Purchase" Then
            FillDDL(fromlocation, "SELECT a.genoid,  a.gendesc FROM QL_mstgen a where a.gengroup = 'LOCATION' and a.gencode = 'EXPEDISI' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
            If fromlocation.Items.Count = 0 Then
                showMessage("Please create Location data!", 3)
                Exit Sub
            End If

            labelnotr.Visible = True : noref.Visible = True
            ibposearch.Visible = True : ibpoerase.Visible = True
            unit.CssClass = "inpTextDisabled" : unit.Enabled = False
            labelfromloc.Visible = False : fromlocation.Visible = False
        Else
            FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'SUPPLIER' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & cmpcode & "' ORDER BY a.gendesc")
            If fromlocation.Items.Count = 0 Then
                showMessage("Please create Location data!", 3)
                Exit Sub
            End If

            labelnotr.Visible = False : noref.Visible = False
            ibposearch.Visible = False : ibpoerase.Visible = False
            unit.CssClass = "inpText" : unit.Enabled = True
            labelfromloc.Visible = True : fromlocation.Visible = True
        End If
        fromlocation.SelectedIndex = 0
        GVItemList.Visible = False : GVpo.Visible = False
        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
    End Sub
#End Region

    Protected Sub TujuanCb_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TujuanCb.SelectedIndexChanged
        InitAllDDL()
        GVpo.Visible = False
        pooid.Text = "" : noref.Text = ""
    End Sub
End Class
