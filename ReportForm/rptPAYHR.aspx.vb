Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class RptPiutangGantung
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(txtStart.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(txtFinish.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        'If IsValidDate(txtStart.Text, "MM/dd/yyyy", sErrTemp) And IsValidDate(txtFinish.Text, "MM/dd/yyyy", sErrTemp) Then
        '    If DateDiff(DateInterval.Day, CDate(txtStart.Text), CDate(txtFinish.Text)) < 0 Then
        '        sReturn &= "- End Date must be greater than Start Date.<BR>"
        '    End If
        'End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        Dim sWhere As String = ""
        sSql = "SELECT DISTINCT * FROM ( SELECT 0 selected,suppoid,Case suppcode When '' then CAST(suppoid as Char(10)) else suppcode End suppcode ,suppname,suppaddr FROM QL_mstsupp WHERE suppoid IN (Select suppoid from QL_trnnotahrmst jm Where jm.suppoid=suppoid )) AS cust where (suppcode LIKE '%" & Tchar(FilterTextSupplier.Text.Trim) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text.Trim) & "%') " & sWhere & " ORDER BY suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("QL_mstsupp") = dtSupp
        gvSupplier.Visible = True : gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstsupp") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstsupp")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "suppoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstsupp") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sMsg As String = ""
        Try
            Dim sSuppOid As String = ""
            If rbSupplier.SelectedValue = "SELECT" Then
                If Not (Session("QL_mstsupp") Is Nothing) Then
                    Dim dtSupp As DataTable : dtSupp = Session("QL_mstsupp")
                    If dtSupp.Rows.Count > 0 Then
                        Dim dvSupp As DataView = dtSupp.DefaultView
                        dvSupp.RowFilter = "selected='1'"
                        If dvSupp.Count < 1 Then
                            sMsg &= "- Maaf, Supplier belum anda pilih, Silahkan Klik icon loop kemudian pilih supplier!"
                        End If
                        dvSupp.RowFilter = ""
                    Else
                        sMsg &= "- Maaf, Filter Supplier belum anda pilih, Silahkan Klik icon loop kemudian pilih supplier!"
                    End If
                Else
                    sMsg &= "- Maaf, Filter Supplier belum anda pilih, Silahkan Klik icon loop kemudian pilih supplier!<BR>"
                End If
            End If

            If FilterType.SelectedValue.ToUpper <> "DETAIL PIUTANG" Then
                If CDate(toDate(txtStart.Text.Trim)) > CDate(toDate(txtFinish.Text.Trim)) Then
                    showMessage("-Period 2 harus lebih dari Period 1!.<BR>", 2)
                    Exit Sub
                End If
            End If
            If sMsg <> "" Then
                showMessage(sMsg, 2)
                Exit Sub
            End If
            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstsupp")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"

                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("suppoid").ToString & ","
                Next
            End If
            Dim sWhere_branch As String = ""
            Dim sWhere As String = "" : Dim swheresupp As String = ""
            If FilterType.SelectedValue = "KARTU PIUTANG" Then
                If sSuppOid <> "" Then sWhere &= " Where suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            ElseIf FilterType.SelectedValue = "DETAIL PIUTANG" Then
                If sSuppOid <> "" Then sWhere &= " Where custoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            Else
                If sSuppOid <> "" Then sWhere &= " where suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
                If sSuppOid <> "" Then swheresupp = " AND suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            End If

            Dim sFilter As String = ""
            sFilter = "ORDER BY"
            If FilterType.SelectedValue = "KARTU PIUTANG" Or FilterType.SelectedValue = "DETAIL PIUTANG" Then
                If DDLFilter.SelectedValue = "suppname" Then
                    sFilter &= " " & DDLFilter.SelectedValue & " "
                Else
                    sFilter &= " " & DDLFilter.SelectedValue & " "
                End If
                If DDLFilter.SelectedValue = "Asc" Then
                    sFilter &= " " & DDLFilter2.SelectedValue & " "
                Else
                    sFilter &= " " & DDLFilter2.SelectedValue & " "
                End If
            End If

            Dim PeriodBefore As Integer
            If Len(FilterDDLMonth.SelectedValue - 1) > 1 Then
                PeriodBefore = FilterDDLMonth.SelectedValue - 1
            Else
                PeriodBefore = "0" & FilterDDLMonth.SelectedValue - 1
            End If
            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                sSql = "DECLARE @periodacctgsa AS VARCHAR (10); " & _
"DECLARE @periodacctg AS VARCHAR (20); " & _
"DECLARE @periodacctg2 AS VARCHAR (20); " & _
"DECLARE @currency AS VARCHAR (10); " & _
"SET @periodacctgsa ='" & FilterDDLYear.SelectedValue & "-" & PeriodBefore & "'; " & _
"SET @currency ='" & FilterCurrency.SelectedValue & "'; " & _
"SET @periodacctg = '" & Format(CDate(toDate(txtStart.Text)), "MM/dd/yyyy") & "'; " & _
"SET @periodacctg2 = '" & Format(CDate(toDate(txtFinish.Text)), "MM/dd/yyyy") & "'; " & _
"Select suppoid, suppname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, SUM(saidr + beliidr - paymentidr) saldoidr, SUM(sausd + beliusd - paymentusd) saldousd,@currency currency FROM ( " & _
"/*SALDO AWAL PIUTANG*/ " & _
"SELECT s.suppoid, s.suppname, SUM(refamtidr) saidr , SUM(refamtusd) sausd, 0 beliidr, 0 beliusd, SUM(payrefamtidr) paymentidr, SUM(payrefamtusd) paymentusd , 0 saldoidr, 0 saldousd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd , 0.0 age3idr, 0.0 age3usd FROM QL_conhr con INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = con.refoid INNER JOIN QL_mstsupp s ON s.suppoid = hrm.suppoid WHERE reftype IN ('QL_trnnotahrmst') AND (Case When refdate = '01/01/1900' then @periodacctg else convert(varchar(10),refdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102) and Case When payrefdate = '01/01/1900' then @periodacctg else convert(varchar(10),payrefdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102)) GROUP BY s.suppoid, s.suppname " & _
"UNION ALL " & _
"/*PIUTANG INVOICE*/ " & _
"SELECT s.suppoid, s.suppname, 0 saidr , 0 sausd, SUM(refamtidr) beliidr, SUM(refamtusd) beliusd, 0 paymentidr, 0 paymentusd , 0 saldoidr, 0 saldousd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd , 0.0 age3idr, 0.0 age3usd FROM QL_conhr con INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = con.refoid INNER JOIN QL_mstsupp s ON s.suppoid = hrm.suppoid WHERE reftype = 'QL_trnnotahrmst' AND con.conhrtype in ('HR') AND con.refdate between '" & Format(CDate(toDate(txtStart.Text)), "MM/dd/yyyy") & " 0:0:0' and '" & Format(CDate(toDate(txtFinish.Text)), "MM/dd/yyyy") & " 23:59:59' GROUP BY s.suppoid, s.suppname " & _
"UNION ALL " & _
"/*PAYMENT*/ " & _
"SELECT s.suppoid, s.suppname, 0.0 saidr, 0.0 sausd , SUM (refamtidr) beliidr, SUM (refamtusd) beliusd, SUM(payrefamtidr) paymentidr, SUM(payrefamtusd) paymentusd , 0 saldoidr, 0 saldousd, 0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd , 0.0 age3idr, 0.0 age3usd FROM QL_conhr con INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = con.refoid INNER JOIN QL_mstsupp s ON s.suppoid = hrm.suppoid WHERE reftype in ('QL_trnnotahrmst') AND con.conhrtype IN ('PAYHR') AND con.payrefdate between @periodacctg and @periodacctg2 and con.payrefamt >= 0 AND con.conhrstatus='POST' GROUP BY s.suppoid, s.suppname  " & _
") AS tbl " & _
"" & sWhere & " " & _
"GROUP BY suppoid, suppname ORDER BY suppname ASC /*" & DDLFilterSum.SelectedValue & " " & DDLFilter2.SelectedValue & "*/"

                Dim dt As DataTable : Dim nFile As String = ""
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptHRExl.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptHR.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                Dim sFilter1 As String = "ORDER BY suppname ASC /*" & DDLFilterSum.SelectedValue & " " & DDLFilter2.SelectedValue & "*/"
                report.SetDataSource(dt)
                Session("strLastModified") = System.IO.File.GetLastWriteTime(nFile.ToString())
                cProc.SetDBLogonReport(report)
                report.SetParameterValue("sFilter", sFilter1)
                report.SetParameterValue("PrintLastModified", Session("strLastModified"))
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("periodreport", Format(CDate(toDate(txtStart.Text)), "dd/MM/yyyy"))
                report.SetParameterValue("periodreport2", Format(CDate(toDate(txtFinish.Text)), "dd/MM/yyyy"))
            ElseIf FilterType.SelectedValue.ToUpper = "KARTU PIUTANG" Then
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptPayHR_DtlExcl.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptPayHR_Detail.rpt"))
                End If

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                Dim periode As String = Format(CDate(toDate(txtStart.Text)), "MM/dd/yyyy")
                Dim periode2 As String = Format(CDate(toDate(txtFinish.Text)), "MM/dd/yyyy")
                Dim periodValue As String = txtStart.Text
                Dim periodValue2 As String = txtFinish.Text
                Dim periodacctg As String = GetPeriodAcctg(CDate(toDate(txtStart.Text)))
                report.SetParameterValue("cmpcode", CompnyCode)
                report.SetParameterValue("tglawal", periode)
                report.SetParameterValue("tglakhir", periode2)
                report.SetParameterValue("periodacctg", periodacctg)
                report.SetParameterValue("periodvalue", periodValue)
                report.SetParameterValue("periodvalue2", periodValue2)
                report.SetParameterValue("currency", FilterCurrency.SelectedValue)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("sFilter", sFilter)
                report.SetParameterValue("OnDate", periode)

            Else
                If sType = "Print Excel" Then
                    report.Load(Server.MapPath(folderReport & "rptHRDtlExcl.rpt"))
                Else
                    report.Load(Server.MapPath(folderReport & "rptHRDtl.rpt"))
                End If

                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                Dim periode As String = Format(CDate(toDate(txtStart.Text)), "MM/dd/yyyy")
                Dim periode2 As String = Format(CDate(toDate(txtFinish.Text)), "MM/dd/yyyy")
                Dim periodValue As String = txtStart.Text
                Dim periodValue2 As String = txtFinish.Text
                Dim periodacctg As String = GetPeriodAcctg(CDate(toDate(txtStart.Text)))
                report.SetParameterValue("tglawal", periode)
                report.SetParameterValue("tglakhir", periode2)
                report.SetParameterValue("periodacctg", periodacctg)
                report.SetParameterValue("periodvalue", periodValue)
                report.SetParameterValue("periodvalue2", periodValue2)
                report.SetParameterValue("currency", FilterCurrency.SelectedValue)
                report.SetParameterValue("sWhere", sWhere)
                'report.SetParameterValue("sFilter", sFilter)
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ARREPORT_" & FilterDDLMonth.SelectedItem.Text.ToUpper & FilterDDLYear.SelectedItem.Text)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\rptPAYHR.aspx")
        End If
        If checkPagePermission("~\ReportForm\rptPAYHR.aspx", Session("Role")) = False Then
            ' Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Laporan Piutang Gantung"
        If Not Page.IsPostBack Then
            FilterDDLMonth.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth.Items.Add(MonthName(C1))
                FilterDDLMonth.Items(FilterDDLMonth.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear.Items.Add(C1)
                FilterDDLYear.Items(FilterDDLYear.Items.Count - 1).Value = C1
            Next
            FilterDDLYear.SelectedValue = Now.Year

            FilterDDLMonth2.Items.Clear()
            For C1 As Int16 = 1 To 12
                FilterDDLMonth2.Items.Add(MonthName(C1))
                FilterDDLMonth2.Items(FilterDDLMonth2.Items.Count - 1).Value = Format(C1, "00")
            Next
            FilterDDLMonth2.SelectedValue = Format(Now.Month, "00")
            FilterDDLYear2.Items.Clear()
            For C1 As Int16 = Now.Year - 2 To Now.Year + 2
                FilterDDLYear2.Items.Add(C1)
                FilterDDLYear2.Items(FilterDDLYear2.Items.Count - 1).Value = C1
            Next
            FilterDDLYear2.SelectedValue = Now.Year

            ' DDL Division
            'sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
            'If Session("CompnyCode") <> CompnyCode Then
            '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            'End If
            'FillDDL(FilterDDLDiv, sSql)
            'branch
            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False

            pnlSupplier.CssClass = "popupControl"
            txtStart.Text = Format(GetServerTime, "01/MM/yyyy")
            txtFinish.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If FilterType.SelectedValue.ToUpper = "DETAIL PIUTANG" Then
            lblTo.Visible = False
            txtStart.Visible = False : imbStart.Visible = False
        Else
            lblTo.Visible = True
            txtStart.Visible = True : imbStart.Visible = True
        End If
        FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("QL_mstsupp") = Nothing : gvSupplier.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False : btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True : btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = "" : suppoid.Text = ""
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()
        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstsupp")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\rptPAYHR.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region
End Class
