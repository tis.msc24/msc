<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPayAP.aspx.vb" Inherits="Accounting_trnPayAP" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Pelunasan Hutang"></asp:Label></th>
        </tr>
        <tr>
            <th align="left" style="background-color: #ffffff" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                    <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w24"><TABLE width="100%" __designer:dtid="281474976710684"><TBODY><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710685"><TD style="WIDTH: 73px" align=left __designer:dtid="281474976710686">Filter</TD><TD align=left colSpan=6 __designer:dtid="281474976710687"><asp:DropDownList id="ddlFilter" runat="server" Width="110px" CssClass="inpText" __designer:dtid="281474976710688" __designer:wfdid="w25"><asp:ListItem Value="cashbankno" __designer:dtid="281474976710689">Pay AP No.</asp:ListItem>
<asp:ListItem Value="s.suppname" __designer:dtid="281474976710690">Supplier</asp:ListItem>
<asp:ListItem Value="b.trnbelino" __designer:dtid="281474976710691">Invoice No</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:dtid="281474976710692" __designer:wfdid="w26"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710693"><TD style="WIDTH: 73px" align=left __designer:dtid="281474976710694">Periode</TD><TD align=left colSpan=6 __designer:dtid="281474976710695"><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText" __designer:dtid="281474976710696" __designer:wfdid="w27"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:dtid="281474976710697" __designer:wfdid="w28"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" __designer:dtid="281474976710698" Text="to" __designer:wfdid="w29"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText" __designer:dtid="281474976710699" __designer:wfdid="w30"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:dtid="281474976710700" __designer:wfdid="w31"></asp:ImageButton> <asp:DropDownList id="ddlpaytype" runat="server" Width="55px" CssClass="inpText" __designer:dtid="281474976710708" __designer:wfdid="w32" Visible="False">
                                <asp:ListItem __designer:dtid="281474976710709" Text="CASH" Value="K"></asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710710" Text="NONCASH" Value="BB"></asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710711" Value="G">GIRO</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710712" Value="D">DP</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710713" Value="C">CREDIT CARD</asp:ListItem>
                            </asp:DropDownList> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710714" __designer:wfdid="w33" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710701"><TD style="WIDTH: 73px" align=left __designer:dtid="281474976710702">Status</TD><TD align=left colSpan=6 __designer:dtid="281474976710703"><asp:DropDownList id="postinge" runat="server" Width="110px" CssClass="inpText" __designer:dtid="281474976710704" __designer:wfdid="w34">
                                <asp:ListItem __designer:dtid="281474976710705">ALL</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710706" Value=" ">In Process</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710707" Value="POST">POST</asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710725" __designer:wfdid="w35"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710726" __designer:wfdid="w36"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710715"><TD style="WIDTH: 73px" id="TD4" align=left runat="server" __designer:dtid="281474976710716" Visible="false">Order By</TD><TD id="TD3" align=left colSpan=6 runat="server" __designer:dtid="281474976710717" Visible="false"><asp:DropDownList id="orderby" runat="server" Width="105px" CssClass="inpText" __designer:dtid="281474976710718" __designer:wfdid="w37">
                                <asp:ListItem __designer:dtid="281474976710719" Value="cashbankno desc">Pay AP No(Z-A)</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710720" Value="cashbankno">Pay AP No(A-Z)</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710721" Value="cashbankdate desc">Pay Date(Z-A)</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710722" Value="cashbankdate">Pay Date(A-Z)</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710723" Value="suppname desc">Supplier(Z-A)</asp:ListItem>
                                <asp:ListItem __designer:dtid="281474976710724" Value="suppname">Supplier(A-Z)</asp:ListItem>
                            </asp:DropDownList>&nbsp; </TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710727"><TD style="WIDTH: 73px" align=left __designer:dtid="281474976710728"></TD><TD align=left colSpan=6 __designer:dtid="281474976710729"><ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:dtid="281474976710730" __designer:wfdid="w38" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="txtperiode1">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" __designer:dtid="281474976710731" __designer:wfdid="w39" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="txtperiode2">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:dtid="281474976710732" __designer:wfdid="w40" TargetControlID="txtPeriode2" CultureName="id-ID" Mask="99/99/9999" MaskType="Date">
                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee4" runat="server" __designer:dtid="281474976710733" __designer:wfdid="w41" TargetControlID="txtPeriode1" CultureName="id-ID" Mask="99/99/9999" MaskType="Date">
                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710734"><TD align=left colSpan=7 __designer:dtid="281474976710735"><DIV style="OVERFLOW-Y: scroll; WIDTH: 950px; HEIGHT: 334px; BACKGROUND-COLOR: beige"><asp:GridView id="GVmstPAYAP" runat="server" Width="100%" ForeColor="#333333" __designer:dtid="281474976710736" __designer:wfdid="w21" AllowPaging="True" EnableModelValidation="True" OnRowCommand="gridCommand" OnPageIndexChanging="GVmstPAYAP_PageIndexChanging" GridLines="None" DataKeyNames="cashbankno" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="GVmstPAYAP_SelectedIndexChanged" PageSize="8">
<PagerSettings PageButtonCount="15" __designer:dtid="281474976710737"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333" __designer:dtid="281474976710738"></RowStyle>
<Columns __designer:dtid="281474976710739">
<asp:TemplateField HeaderText="No. Transaksi" SortExpression="cashbankno" __designer:dtid="281474976710740"><ItemTemplate __designer:dtid="281474976710741">
<asp:HyperLink id="HyperLink1" runat="server" __designer:dtid="281474976710742" Text='<%# Eval("cashbankno") %>' __designer:wfdid="w1" NavigateUrl='<%# Eval("cashbankoid", "trnPayAP.aspx?oid={0}")%>'>
</asp:HyperLink> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710743"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Navy" __designer:dtid="281474976710744"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppliername" HeaderText="Supplier" __designer:dtid="281474976710745">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710746"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" __designer:dtid="281474976710747"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="date" HeaderText="Tanggal Bayar" __designer:dtid="281474976710748">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710749"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="281474976710750"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="Pay Ref No" Visible="False" __designer:dtid="281474976710751">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710752"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="281474976710753"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Bayar" __designer:dtid="281474976710754">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710755"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" __designer:dtid="281474976710756"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No Nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan" __designer:dtid="281474976710764">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710765"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" __designer:dtid="281474976710766"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status" __designer:dtid="281474976710767">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710768"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" __designer:dtid="281474976710769"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image" __designer:dtid="281474976710770">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" __designer:dtid="281474976710771"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px" __designer:dtid="281474976710772"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" __designer:dtid="281474976710773"></FooterStyle>

<PagerStyle HorizontalAlign="Left" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red" __designer:dtid="281474976710774"></PagerStyle>
<EmptyDataTemplate __designer:dtid="281474976710775">
                                    <asp:Label __designer:dtid="281474976710776" ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Data tidak ditemukan !!</asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" __designer:dtid="281474976710777"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White" __designer:dtid="281474976710778"></HeaderStyle>

<AlternatingRowStyle BackColor="White" __designer:dtid="281474976710779"></AlternatingRowStyle>
</asp:GridView>&nbsp;</DIV> </TD></TR><TR style="FONT-SIZE: 8pt" __designer:dtid="281474976710780"></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="GVmstPAYAP"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong> Daftar Pelunasan Hutang :.</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>Form Pelunasan Hutang </strong><span style="font-size: 9pt">
                                <strong>:.</strong></span></span>&nbsp;</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi :" Font-Underline="True"></asp:Label> <asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" Visible="False"></asp:Label></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:Label id="cashbankoid" runat="server" Width="50px" Visible="False"></asp:Label> <asp:Label id="CutofDate" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; HEIGHT: 10px" class="Label" align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="cashbankno" runat="server" Visible="False"></asp:Label> <asp:Label id="lblcabang" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="NoBukti" runat="server" Width="128px" Text="No. Pembayaran"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="defcbno" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left>Supplier</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="suppnames" runat="server" Width="290px" CssClass="inpTextDisabled" __designer:wfdid="w93" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w94"></asp:ImageButton> <asp:Label id="Label9" runat="server" Font-Bold="False" __designer:wfdid="w95" Visible="False">0</asp:Label></TD></TR><TR><TD class="Label" align=left>Tipe Payment&nbsp;</TD><TD class="Label" align=left><asp:DropDownList id="payflag" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True">
                            <asp:ListItem>CASH</asp:ListItem>
<asp:ListItem Value="NONCASH">NON CASH</asp:ListItem>
<asp:ListItem>VOUCHER</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
<asp:ListItem>DP</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>Tanggal <asp:Label id="Label25" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="PaymentDate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w96" Enabled="False" OnTextChanged="PaymentDate_TextChanged"></asp:TextBox> <asp:ImageButton id="btnPayDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w97" Visible="False" Enabled="False" BorderColor="White"></asp:ImageButton> <asp:DropDownList id="CurrencyOid" runat="server" Width="92px" CssClass="inpTextDisabled" __designer:wfdid="w98" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblRealisasi" runat="server" Width="83px" Text="No. Realisasi" __designer:wfdid="w87" Visible="False"></asp:Label> <asp:Label id="AmtReal" runat="server" Text="AmtReal" __designer:wfdid="w88" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="RealNo" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w89" Visible="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="sRealBtn" onclick="sRealBtn_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w90" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseV" onclick="EraseV_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w91" Visible="False"></asp:ImageButton> <asp:Label id="RealOid" runat="server" Font-Bold="False" __designer:wfdid="w92" Visible="False">0</asp:Label></TD><TD class="Label" align=left>COA <asp:Label id="lblPayType" runat="server" Text="Cash" __designer:wfdid="w107"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="cashbankacctgoid" runat="server" Width="304px" CssClass="inpText" __designer:wfdid="w101" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left></TD><TD align=left colSpan=2><asp:GridView id="RealGv" runat="server" Width="100%" ForeColor="#333333" OnPageIndexChanging="RealGv_PageIndexChanging" GridLines="None" DataKeyNames="realisasioid,realisasino,realisasiamt,realisasistatus,voucherno,voucheroid,voucherdesc,realisasitype,trnbelipono" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="RealGv_SelectedIndexChanged" OnRowDataBound="RealGv_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="voucheroid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="realisasino" HeaderText="No. Realisasi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasiamt" HeaderText="Amount">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="realisasitype" HeaderText="Type">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label10" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD align=left colSpan=3></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblpayduedate" runat="server" Width="70px" Text="Jth. Tempo " Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" Visible="False" BorderColor="White"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="lblpayrefno" runat="server" Width="46px" Text="Ref No." Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w102" Visible="False" MaxLength="15"></asp:TextBox> <asp:TextBox id="code" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w103" Visible="False" MaxLength="30" Enabled="False"></asp:TextBox> <asp:ImageButton id="creditsearch" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w104" Visible="False"></asp:ImageButton> <asp:ImageButton id="CREDITCLEAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w105" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="lbldpno" runat="server" Text="DP No" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="trndpapoid" runat="server" Width="147px" CssClass="inpText" Visible="False" OnSelectedIndexChanged="trndpapoid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="lbldpbalance" runat="server" Width="70px" Text="DP Balance" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="dpbalance" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD class="Label" align=left>Catatan</TD><TD class="Label" align=left colSpan=5><asp:TextBox id="cashbanknote" runat="server" Width="474px" CssClass="inpText" __designer:wfdid="w100" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Total Payment</TD><TD class="Label" align=left><asp:TextBox id="amtbelinettodtl" runat="server" Width="111px" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:MaskedEditExtender id="meeCurrRate" runat="server" TargetControlID="currencyRate" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="PaymentDate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce1" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnPayDate" TargetControlID="PaymentDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnDueDate" TargetControlID="payduedate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="payduedate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <asp:HiddenField id="HiddenField2" runat="server"></asp:HiddenField> <asp:HiddenField id="HiddenField1" runat="server"></asp:HiddenField> <asp:Label id="lblBankName" runat="server" Text="Bank Name" Visible="False"></asp:Label> <asp:Label id="lblnotice" runat="server" Font-Size="X-Small"></asp:Label> <asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" Visible="False"></asp:Label> <asp:TextBox id="currencyRate" runat="server" Width="100px" CssClass="inpText" Visible="False" AutoPostBack="True" OnTextChanged="currencyRate_TextChanged"></asp:TextBox>&nbsp; <asp:TextBox id="NetPayment" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True">0.0000</asp:TextBox> <asp:DropDownList id="ddlBankName" runat="server" Width="151px" CssClass="inpText" Visible="False"></asp:DropDownList> <asp:TextBox id="TotalCost" runat="server" Width="150px" CssClass="inpTextDisabled" Visible="False" ReadOnly="True">0.0000</asp:TextBox></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" Text="Detail :" Font-Underline="True"></asp:Label> <asp:Label id="I_U2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:Label id="trnbelimstoid" runat="server" Font-Size="X-Small" Font-Bold="False" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left>&nbsp;<asp:CheckBox id="CBTax" runat="server" Width="72px" Font-Size="X-Small" Visible="False" AutoPostBack="True" OnCheckedChanged="CBTax_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:Label id="Payseq" runat="server" Visible="False"></asp:Label> <asp:Label id="acctgoid" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: small; WIDTH: 131px" align=left><asp:Label id="invCurrOid" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD><TD align=left colSpan=2><asp:Label id="invCurrCode" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left>Nota No <asp:Label id="Label26" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:TextBox id="trnbelino" runat="server" Width="123px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchPurchasing" onclick="btnSearchPurchasing_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:Label id="OidPO" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt" align=left>Akun Hutang</TD><TD align=left><asp:TextBox id="APAcc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD style="FONT-SIZE: small; WIDTH: 131px" align=left></TD><TD align=left colSpan=2></TD></TR><TR><TD align=left>Supplier</TD><TD align=left><asp:TextBox id="suppname" runat="server" Width="200px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox></TD><TD align=left>Currency</TD><TD align=left><asp:TextBox id="invCurrDesc" runat="server" Width="200px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD align=left>Invoice Rate</TD><TD align=left><asp:TextBox id="invCurrRate" runat="server" Width="121px" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD><TD align=left></TD></TR><TR><TD align=left>Amt. Invoice</TD><TD align=left><asp:TextBox id="amttrans" runat="server" Width="121px" CssClass="inpTextDisabled" MaxLength="100" Enabled="False">0</asp:TextBox></TD><TD align=left>Paid Amt</TD><TD align=left><asp:TextBox id="amtpaid" runat="server" Width="121px" CssClass="inpTextDisabled" MaxLength="100" Enabled="False">0</asp:TextBox></TD><TD align=left>Total retur</TD><TD align=left><asp:TextBox id="amtretur" runat="server" Width="121px" CssClass="inpTextDisabled" MaxLength="100" Enabled="False">0</asp:TextBox></TD><TD align=left></TD></TR><TR><TD align=left>A/P Balance</TD><TD align=left><asp:TextBox id="APAmt" runat="server" Width="121px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False">0</asp:TextBox></TD><TD align=left>Total Bayar <asp:Label id="Label8" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left><asp:TextBox id="amtpayment" runat="server" Width="121px" CssClass="inpText" MaxLength="20" AutoPostBack="True" OnTextChanged="amtpayment_TextChanged">0</asp:TextBox></TD><TD align=left>Sub Total</TD><TD align=left><asp:TextBox id="totalpayment" runat="server" Width="121px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False">0</asp:TextBox></TD><TD align=left></TD></TR><TR><TD align=left>Detail&nbsp;Note</TD><TD align=left colSpan=3><asp:TextBox id="txtNote" runat="server" Width="503px" CssClass="inpText"></asp:TextBox></TD><TD align=left><asp:Label id="RefTypeTbl" runat="server" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:CheckBox id="chkOther" runat="server" Width="110px" Text="Selisih bayar" AutoPostBack="True" OnCheckedChanged="chkOther_CheckedChanged"></asp:CheckBox></TD><TD align=left><asp:DropDownList id="DDLOtherType" runat="server" Width="155px" CssClass="inpTextDisabled" OnSelectedIndexChanged="DDLOtherType_SelectedIndexChanged" Enabled="False" AutoPostBack="True"><asp:ListItem Value="+">Kelebihan Bayar</asp:ListItem>
<asp:ListItem Value="-">Kurang Bayar</asp:ListItem>
</asp:DropDownList></TD><TD id="TD2" align=left runat="server" visible="true">Selisih&nbsp;</TD><TD id="TD1" align=left runat="server" visible="true"><asp:TextBox id="otheramt" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False" AutoPostBack="True" OnTextChanged="otheramt_TextChanged"></asp:TextBox></TD><TD align=left><asp:Label id="lblTaxPct" runat="server" Text="Tax (%)" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="TaxAmount" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" MaxLength="30" ReadOnly="True">0.00000</asp:TextBox> <asp:TextBox id="txtPaymentNo" runat="server" Width="25px" CssClass="inpText" Visible="False" MaxLength="10" ReadOnly="True"></asp:TextBox> </TD><TD align=left></TD></TR><TR><TD id="TD5" align=left runat="server" Visible="false"><asp:CheckBox id="cbDP" runat="server" Text="DP" Visible="False" AutoPostBack="True" OnCheckedChanged="DP_CheckedChanged"></asp:CheckBox></TD><TD id="TD6" align=left runat="server" Visible="false"><asp:DropDownList id="ddlDPNo" runat="server" CssClass="inpText" Visible="False" OnSelectedIndexChanged="ddlDPNo_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="lblDPAmount" runat="server" Text="DP Amount" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="DPAmt" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True">0.00</asp:TextBox> <asp:Label id="lblTaxAcc" runat="server" Text="Tax Account" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: small; WIDTH: 131px" align=left><asp:Label id="lblAmtTax" runat="server" Text="Tax Amount" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trnTaxPct" runat="server" Width="50px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD align=left></TD></TR><TR><TD align=left colSpan=7><asp:Label id="Label14" runat="server" Font-Bold="True" Text="Detail Selisih Bayar :" Font-Underline="True"></asp:Label>&nbsp; <ajaxToolkit:FilteredTextBoxExtender id="ftbdpPayment" runat="server" TargetControlID="DPAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="fteDtlselisih" runat="server" TargetControlID="amtdtlselisih" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftx_payamt" runat="server" TargetControlID="amtpayment" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <asp:DropDownList id="TaxAccount" runat="server" Width="100px" CssClass="inpText" Visible="False" OnSelectedIndexChanged="cashbankacctgoid_SelectedIndexChanged"></asp:DropDownList> </TD></TR><TR><TD align=left>Akun selisih bayar</TD><TD align=left colSpan=3><asp:DropDownList id="otherAcctgoid" runat="server" Width="408px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList>&nbsp;</TD><TD align=left>Total selisih <asp:Label id="Label18" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="amtdtlselisih" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD vAlign=top align=left>Note<asp:Label id="stateDtlSls" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Selisih" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="dtlnoteselisih" runat="server" Width="503px" CssClass="inpText"></asp:TextBox></TD><TD vAlign=top align=left colSpan=3><asp:LinkButton id="lkbAddDtlSlisih" onclick="lkbAddDtlSlisih_Click" runat="server" Font-Bold="True" Visible="False">[Tambah Detail Selisih]</asp:LinkButton> <asp:LinkButton id="lkbClearDtlSlisih" onclick="lkbClearDtlSlisih_Click" runat="server" Font-Bold="True" Visible="False">[Batal]</asp:LinkButton> </TD></TR><TR><TD vAlign=top align=left colSpan=7><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 100%; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div1"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="gvDtlSelisih" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" DataKeyNames="selisihseq" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="gvDtlSelisih_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="selisihseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun selisih bayar">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="529px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdtlselisih" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dtlnoteselisih" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl=" " DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada data detail selisih bayar..!!" __designer:wfdid="w108"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=right colSpan=13><asp:ImageButton id="ibtn" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" AlternateText="Add to List"></asp:ImageButton><asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=right colSpan=7><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div2"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlPayAP" runat="server" Width="100%" Height="17px" ForeColor="#333333" GridLines="None" DataKeyNames="payseq" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No Nota">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="101px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="COA">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrCode" HeaderText="Inv. Currency" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="invCurrRate" HeaderText="Inv. Rate" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Amt DN/CN">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total Kembali">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Pembayaran">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" Font-Overline="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="RealOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="payres1" HeaderText="FlagCon" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada data detail Invoice..!!" __designer:wfdid="w110"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV><asp:Label id="Label21" runat="server" Font-Bold="True" Text="Grand Total :  "></asp:Label>&nbsp;<asp:Label id="amtbelinettodtl4" runat="server" Font-Bold="True">0.00</asp:Label><BR /><asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp." __designer:wfdid="w67" Visible="False"></asp:Label><asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" __designer:wfdid="w68" Visible="False">0.00</asp:Label></FIELDSET> </TD></TR><TR><TD vAlign=top align=left colSpan=7>Last Updated By <asp:Label id="updUser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="updTime" runat="server" Font-Bold="True"></asp:Label> </TD></TR><TR><TD vAlign=top align=left colSpan=7><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton style="WIDTH: 60px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" AlternateText="Posting"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton style="WIDTH: 60px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=7><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w1" AssociatedUpdatePanelID="UpdatePanel11" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w4"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel4s" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Supplier"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppID" runat="server" CssClass="inpText">
                                                                                    <asp:ListItem Value="Code">Code</asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Value="Name">Name</asp:ListItem>
                                                                                </asp:DropDownList> <asp:TextBox id="txtFindSuppID" runat="server" Width="121px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibtnSuppID" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAlls" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset7"><DIV id="Div5"></DIV><TABLE class="gvhdr" width="100%"><TBODY><TR><TD style="WIDTH: 56px"></TD><TD style="WIDTH: 91px">Kode</TD><TD>Nama</TD></TR></TBODY></TABLE><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 91%; BACKGROUND-COLOR: beige"><asp:GridView id="gvSupplier" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" DataKeyNames="ID,Name" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" DataSourceID="SqlDataSource1" EmptyDataText="No data in database." ShowHeader="False">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Code" HeaderText="Kode" SortExpression="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Name" HeaderText="Nama" SortExpression="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="lblstatusdatasupp" runat="server" ForeColor="Red" Text="No Suppplier Data !" Visible="False"></asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSupp" onclick="CloseSupp_Click" runat="server" CausesValidation="False" Font-Bold="False">[ Close ]</asp:LinkButton><asp:SqlDataSource id="SqlDataSource1" runat="server" ProviderName="<%$ ConnectionStrings:QL_JPTConnectionString.ProviderName %>" SelectCommand="SELECT suppOID AS ID,suppCODE AS Code, suppNAME AS Name FROM QL_MSTSUPP  AS c WHERE (CMPCODE LIKE @cmpcode)  AND (suppCODE LIKE @code) AND (suppNAME LIKE @name) and suppoid in ( &#13;&#10;SELECT m.trnsuppoid from ql_trnbelimst m where m.amtbelinetto>(m.accumpayment+m.amtretur)  and trnbelistatus in ('Approved','POST') UNION ALL SELECT fa.suppoid FROM QL_trnbelimst_fa fa WHERE fa.cmpcode=c.cmpcode AND fa.suppoid=c.suppoid AND fa.trnbelifamststatus='POST'&#13;&#10; )  ORDER BY suppCODE" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"><SelectParameters>
<asp:Parameter Name="cmpcode"></asp:Parameter>
<asp:Parameter Name="code"></asp:Parameter>
<asp:Parameter Name="name"></asp:Parameter>
</SelectParameters>
</asp:SqlDataSource> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3s" runat="server" TargetControlID="hiddenbtn2s" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2s" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel6" runat="server"><ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel7" runat="server"><ContentTemplate>
<asp:Panel id="Panel2" runat="server" Width="99%" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblPurcdata" runat="server" Font-Size="Medium" Font-Bold="True" Text="Akun Pembayaran(Nota)"></asp:Label></TD></TR><TR><TD align=center colSpan=3><asp:DropDownList id="FilterTxt" runat="server" CssClass="inpText"><asp:ListItem Value="trnbelino">No. Nota</asp:ListItem>
<asp:ListItem Value="trnbelinote">No. SJ (Note)</asp:ListItem>
</asp:DropDownList>&nbsp;: <asp:TextBox id="txtInputNotaBeli" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="imbFindInv" onclick="imbFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click1" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" Visible="False"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=3><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 260px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset8"><DIV id="Div7"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 87%; BACKGROUND-COLOR: beige"><asp:GridView id="gvPurchasing" runat="server" Width="100%" ForeColor="#333333" GridLines="None" DataKeyNames="trnbelimstoid,trnbelino,suppname,trnbelidate,amttrans,amtpaid,acctgoid,trntaxpct,currencyoid,currencyrate,currencycode,currencydesc,amtretur,payduedate,reftype,AmtVo,trnbelipono" CellPadding="4" AutoGenerateColumns="False" OnSelectedIndexChanged="gvPurchasing_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="40px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="Nota. No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PreFixSupp" HeaderText="ID Supplier">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Tanggal nota">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AmtVo" HeaderText="Voucher">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelinote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="Total Nota">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtpaid" HeaderText="Total bayar">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtretur" HeaderText="Total retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="95px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelimstoid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                                                                                <asp:Label ID="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">Tidak ada data hutang !!</asp:Label>
                                                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="ClosePurc" onclick="ClosePurc_Click" runat="server" CausesValidation="False" Font-Bold="True">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" TargetControlID="hiddenbtnpur" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel2" PopupDragHandleControlID="lblPurcdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtnpur" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel4" runat="server">
                                                        <ContentTemplate>
                                                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="hiddenbtn2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender>
                                                            <asp:Button ID="hiddenbtn2" runat="server" Visible="False"></asp:Button>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel> </asp:View> &nbsp;</asp:MultiView> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>


                        </ContentTemplate>

                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </th>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>

