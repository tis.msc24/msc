Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions


Partial Class rptPRogPoint
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub
#End Region

#Region "Procedure"

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        Dim namaPDF As String = ""
        Dim sWhere, itemoid As String
        Dim join As String = ""
        Dim Dtl As String = ""
        Dim grp As String = ""
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            If ddlStatus.SelectedValue = "DETAIL" Then
                If ddlType.SelectedValue = "Cabang" Then
                    If tipe = "Print Excel" Then
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointCabangDetailExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointCabangDetail.rpt"))
                    End If
                Else
                    If tipe = "Print Excel" Then
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointDetailExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointDetail.rpt"))
                    End If
                End If
            Else
                'SUMMARY
                If ddlType.SelectedValue = "Cabang" Then
                    If tipe = "Print Excel" Then
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointCabangSumExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointCabangSum.rpt"))
                    End If
                Else
                    If tipe = "Print Excel" Then
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointSumExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath(folderReport & "rptProgPointSum.rpt"))
                    End If
                End If
            End If

            If ddlStatus.SelectedValue = "DETAIL" Then
                If ddlType.SelectedValue = "Customer" Then
                    Dtl = " ,c.custname, c.custoid"
                    join = " INNER JOIN QL_mstcust c ON c.custoid = jm.trncustoid"
                ElseIf ddlType.SelectedValue <> "Cabang" Then
                    Dtl = " ,per.personoid AS custoid, per.personname AS custname"
                    join = " INNER JOIN QL_MSTPERSON per ON per.PERSONOID = jm.spgoid"
                End If
                sSql = "select p.progpointmstoid,p.progpointcode,p.progpointdesc, CONVERT(varchar(20), p.progpointstartdate, 103) AS progpointstartdate,CONVERT(varchar(20), p.progpointfinishdate, 103) AS progpointfinishdate, p.progpointnote, p.opsipiutanglunas, p.opsipiutangontime, jm.trnjualno, jm.trnjualmstoid,itemcode,i.itemdesc, g.gendesc AS Cabang,jd.trnjualdtlqty AS qty, pd.point, ((jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point) AS totalpoint, ISNULL(rmd.trnjualreturno,'') returno, ISNULL(rmd.trnjualdtlqty,0) AS qtyretur, (CASE WHEN (trnamtjual  - accumpayment -  amtretur) = 0 THEN 'Lunas' ELSE 'Belum Lunas' END) flagpiutang, ISNULL((SELECT personname FROM QL_mstperson where personoid = jm.spgoid),'') AS sales " & Dtl & " from QL_mstprogpointdtl pd INNER JOIN QL_mstprogpoint p on p.progpointmstoid = pd.progpointmstoid INNER JOIN QL_trnjualdtl jd ON pd.itemoid = jd.itemoid and jd.branch_code = pd.branch_code INNER JOIN QL_mstitem i ON i.itemoid = pd.itemoid INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code INNER JOIN QL_mstgen g ON g.gencode = pd.branch_code and g.gengroup = 'CABANG' " & join & " LEFT JOIN ( select rm.trnjualreturno, rd.trnjualdtlqty, rd.itemoid, rm.refnotaretur from QL_trnjualreturdtl rd INNER JOIN QL_trnjualreturmst rm ON rm.trnjualreturmstoid = rd.trnjualreturmstoid and rm.branch_code = rd.branch_code ) rmd ON rmd.itemoid = i.itemoid and rmd.refnotaretur = jm.trnjualno "
            End If
            Dim dtlo As String = ""
            If ddlStatus.SelectedValue = "SUMMARY" Then
                If ddlType.SelectedValue = "Customer" Then
                    Dtl = " , g.gencode,c.custname"
                    dtlo = " , gencode,custname"
                    join = " INNER JOIN QL_mstcust c ON c.custoid = jm.trncustoid"
                ElseIf ddlType.SelectedValue <> "Cabang" Then
                    Dtl = " ,g.gencode, per.personname AS custname"
                    grp = " ,gencode, custname"
                    join = " INNER JOIN QL_MSTPERSON per ON per.PERSONOID = jm.spgoid"
                End If
                sSql = "SELECT progpointmstoid, progpointcode, progpointdesc, progpointstartdate, progpointfinishdate, progpointnote, Cabang, SUM(qty) qty, SUM(qtyretur) qtyretur, SUM(totalqty) totalqty, SUM(point) point, SUM(totalpoint) totalpoint, SUM(totalamount) totalamount,ISNULL((select TOP 1 pd2.itemdesc + ' (' + CAST(pd2.point as varchar(100)) + ')' from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = gg.progpointmstoid and  pd2.point = ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = gg.progpointmstoid and pd2.point <= SUM(totalpoint) order by pd2.point DESC),0) order by point DESC),'') itemreward,(case when SUM(totalpoint) >= ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = gg.progpointmstoid and pd2.point >= SUM(totalpoint) order by pd2.point DESC),0) then ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = gg.progpointmstoid and pd2.point <= SUM(totalpoint) order by pd2.point DESC),0) Else ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = gg.progpointmstoid and pd2.point >= SUM(totalpoint) order by pd2.point ASC),0) end ) AS targetpoint, SUM(targetamount) targetamount " & IIf(ddlType.SelectedValue <> "Cabang", ",gencode, custname", "") & " FROM (select p.progpointmstoid,p.progpointcode,p.progpointdesc, CONVERT(varchar(20), p.progpointstartdate, 103) AS progpointstartdate, CONVERT(varchar(20), p.progpointfinishdate, 103) AS progpointfinishdate, p.progpointnote, g.gendesc AS Cabang,jd.trnjualdtlqty /*- sum(jd.trnjualdtlqty_retur)*/ AS qty, ISNULL(rmd.trnjualdtlqty,0) AS qtyretur, jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0) AS totalqty , pd.point point, (jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point AS totalpoint, (jd.trnjualdtlqty * (trnjualdtlprice - trnjualdtldiscqty)) AS totalamount,ISNULL((select TOP 1 pd2.itemdesc + ' (' + CAST(pd2.point as varchar(100)) + ')' from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.point <= (jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point order by point DESC),'') itemreward,(case when ((jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point) >= ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.point >= (jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point order by pd2.point DESC),0) then ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.point <= (jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point order by point DESC),0) Else ISNULL((select TOP 1 pd2.point from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.point >= (jd.trnjualdtlqty - ISNULL(rmd.trnjualdtlqty,0)) * pd.point order by point ASC),0) end ) AS targetpoint,(case when (jd.trnjualdtlqty * (trnjualdtlprice - trnjualdtldiscqty)) >= ISNULL((select TOP 1 pd2.targetamount from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.targetamount >= (jd.trnjualdtlqty * (trnjualdtlprice - trnjualdtldiscqty)) order by pd2.targetamount DESC),0) then ISNULL((select TOP 1 pd2.targetamount from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.targetamount <= (jd.trnjualdtlqty * (trnjualdtlprice - trnjualdtldiscqty)) order by pd2.targetamount DESC),0) Else ISNULL((select TOP 1 pd2.targetamount from QL_mstprogpointdtl2 pd2 where pd2.progpointmstoid = p.progpointmstoid and pd2.targetamount >= (jd.trnjualdtlqty * (trnjualdtlprice - trnjualdtldiscqty)) order by pd2.targetamount ASC),0) end ) AS targetamount " & Dtl & " from QL_mstprogpointdtl pd INNER JOIN QL_mstprogpoint p on p.progpointmstoid = pd.progpointmstoid INNER JOIN QL_trnjualdtl jd ON pd.itemoid = jd.itemoid and jd.branch_code = pd.branch_code INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code INNER JOIN QL_mstgen g ON g.gencode = pd.branch_code and g.gengroup = 'CABANG' " & join & " LEFT JOIN (select rm.trnjualreturno, rd.trnjualdtlqty, rd.itemoid, rm.refnotaretur from QL_trnjualreturdtl rd INNER JOIN QL_trnjualreturmst rm ON rm.trnjualreturmstoid = rd.trnjualreturmstoid and rm.branch_code = rd.branch_code ) rmd ON rmd.itemoid = jd.itemoid and rmd.refnotaretur = jm.trnjualno "
            End If

            sSql &= "Where p.cmpcode = '" & CompnyCode & "' and jm.trnjualdate between p.progpointstartdate and p.progpointfinishdate"
            If ddlCabang.SelectedValue <> "ALL" Then
                sSql &= " AND g.gencode = '" & ddlCabang.SelectedValue & "'"
            End If
            'Type Program Point
            sSql &= " AND p.progpointtype = '" & ddlType.SelectedValue & "'"
            If GVItemList.Visible = True Then
                UpdateCheckedMat()
                Dim dBar As DataTable = Session("TblListMatView")
                Dim MisV As DataView = dBar.DefaultView
                MisV.RowFilter = "checkvalue='True'"
                If MisV.Count > 0 Then
                    For L1 As Int32 = 0 To MisV.Count - 1
                        itemoid &= MisV(L1)("itemoid").ToString & ","
                    Next
                    sSql &= " AND pd.itemoid IN (" & Left(itemoid, itemoid.Length - 1) & ")"
                Else
                    lblkonfirmasi.Text = "- Maaf, Siliahkan pilih Katalog..!!<br />"
                End If
            End If

            If Oid.Text <> "" Then
                sSql &= " AND p.progpointmstoid=" & Oid.Text & ""
            End If

            If ddlType.SelectedValue = "Customer" Then
                sSql &= IIf(suppoid.Text.Trim = "", " ", " AND c.custoid = " & suppoid.Text & "")
            ElseIf ddlType.SelectedValue = "Sales" Then
                sSql &= IIf(suppoid.Text.Trim = "", " ", " AND per.personoid = " & suppoid.Text & "")
            End If
            If ddlStatus.SelectedValue = "SUMMARY" Then
                sSql &= ") GG "
            End If
            'GROUP BY
            If ddlStatus.SelectedValue = "SUMMARY" Then
                If ddlType.SelectedValue = "Customer" Then
                    'sSql &= " group by p.progpointmstoid, p.progpointcode, p.progpointdesc, p.progpointstartdate, p.progpointfinishdate, p.progpointnote, g.gendesc/*, pd.point*/ " & Dtl & ""
                    sSql &= "group by progpointmstoid, progpointcode, progpointdesc, progpointstartdate, progpointfinishdate, progpointnote, Cabang " & dtlo & ""
                ElseIf ddlType.SelectedValue = "Sales" Then
                    sSql &= " group by progpointmstoid, progpointcode, progpointdesc, progpointstartdate, progpointfinishdate, progpointnote, Cabang " & grp & ""
                Else
                    sSql &= " group by progpointmstoid, progpointcode, progpointdesc, progpointstartdate, progpointfinishdate, progpointnote, Cabang "
                End If
            End If

            'ORDER BY
            If ddlType.SelectedValue = "Customer" Then
                If ddlStatus.SelectedValue = "SUMMARY" Then
                    sSql &= " order by custname ASC"
                Else
                    sSql &= " order by p.progpointmstoid,c.custoid, jm.trnjualmstoid ASC"
                End If
            ElseIf ddlType.SelectedValue = "Sales" Then
                If ddlStatus.SelectedValue = "SUMMARY" Then
                    sSql &= " order by progpointmstoid " & grp & " ASC"
                Else
                    sSql &= " order by p.progpointmstoid,per.personoid, jm.trnjualmstoid ASC"
                End If
            Else
                If ddlStatus.SelectedValue = "SUMMARY" Then
                    sSql &= " order by progpointmstoid, Cabang ASC"
                Else
                    sSql &= " order by p.progpointmstoid, jm.trnjualmstoid, g.gencode ASC"
                End If
            End If

            Dim sHeader As String = ""
            If ddlType.SelectedValue = "Customer" Then
                sHeader = "Customer"
            ElseIf ddlType.SelectedValue <> "Cabang" Then
                sHeader = "Sales"
            End If

            Dim dtTbl As DataTable = ckon.ambiltabel(sSql, "QL_mstprogpoint")
            Dim dvTbl As DataView = dtTbl.DefaultView
            vReport.SetDataSource(dvTbl.ToTable)

            namaPDF = "ProgPointDtl_"
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sHeader", sHeader)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "View" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "Print Excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.ToString
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub BindSupp()
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        sSql = "SELECT custoid, custcode, custname FROM ql_mstcust WHERE (custname LIKE '%" & Tchar(suppname.Text) & "%' OR custcode LIKE '%" & Tchar(suppname.Text) & "%') /*AND suppoid IN (SELECT suppoid FROM QL_mstpromosupp Where suppoid=trnsuppoid AND branch_code='" & Session("branch_id") & "' AND trnbelipono LIKE '%" & Tchar(PromoNya.Text) & "%' AND trnbelipodate BETWEEN '" & CDate(toDate(dateAwal.Text)) & "' And '" & CDate(toDate(dateAkhir.Text)) & "')*/"
        FillGV(gvSupp, sSql, "ql_mstcust")
        gvSupp.Columns(1).HeaderText = "Kode Customer"
        gvSupp.Columns(2).HeaderText = "Nama Customer"
        gvSupp.Visible = True
    End Sub

    Public Sub BindSales()
        Dim sCabang As String = ""
        If ddlCabang.SelectedValue <> "ALL" Then
            sCabang = " AND pr.branch_code = '" & ddlCabang.SelectedValue & "'"
        End If
        sSql = "select PERSONOID AS custoid, PERSONNIP AS custcode, PERSONNAME AS custname from QL_MSTPERSON p INNER JOIN QL_MSTPROF pr ON pr.USERNAME = p.PERSONNAME where PERSONSTATUS in (3118, 987) and STATUS = 'AKTIF' and (personname LIKE '%" & Tchar(suppname.Text) & "%' OR personnip LIKE '%" & Tchar(suppname.Text) & "%') " & sCabang & ""
        FillGV(gvSupp, sSql, "ql_mstcust")
        gvSupp.Columns(1).HeaderText = "NIP"
        gvSupp.Columns(2).HeaderText = "Name"
        gvSupp.Visible = True
    End Sub

    Public Sub BindPromo()
        sSql = "select distinct p.progpointmstoid,p.progpointcode,p.progpointdesc from QL_mstprogpointdtl pd INNER JOIN QL_mstprogpoint p on p.progpointmstoid = pd.progpointmstoid INNER JOIN QL_trnjualdtl jd ON pd.itemoid = jd.itemoid and jd.branch_code = pd.branch_code INNER JOIN QL_mstitem i ON i.itemoid = pd.itemoid INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code INNER JOIN QL_mstgen g ON g.gencode = pd.branch_code and g.gengroup = 'CABANG' INNER JOIN QL_mstcust c ON c.custoid = jm.trncustoid ORDER BY p.progpointmstoid"
        FillGV(GVPromo, sSql, "ql_pomst")
        GVPromo.Visible = True
    End Sub

    Private Sub BindItemNya(ByVal iFilter As String)
        sSql = "Select 'False' AS checkvalue,i.itemoid,i.itemcode,i.itemdesc,i.statusitem,i.pricelist From ql_mstitem i Where itemflag='AKTIF' and (itemcode like '%" & Tchar(iFilter) & "%' or itemdesc like '%" & Tchar(iFilter) & "%')"
        Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        End If
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlCabang, sSql)
            Else
                FillDDL(ddlCabang, sSql)
                ddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                ddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlCabang, sSql)
            ddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            ddlCabang.SelectedValue = "ALL"
        End If
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptProgPoint.aspx")
        End If
        Page.Title = CompnyName & " - Rekap Program Point"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitDDLBranch()
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("View")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptProgPoint.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("Print Excel")
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddlType.SelectedValue = "Customer" Then
            BindSupp()
        Else
            BindSales()
        End If
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindPromo()
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = "" : suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("custname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("custoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        If ddlType.SelectedValue = "Customer" Then
            BindSupp()
        Else
            BindSales()
        End If
    End Sub

    Protected Sub GVPromo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVPromo.PageIndexChanging
        GVPromo.PageIndex = e.NewPageIndex
        BindPromo()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PromoNya.Text = "" : Oid.Text = ""
        GVPromo.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub GVPromo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVPromo.SelectedIndexChanged
        Oid.Text = GVPromo.SelectedDataKey("progpointmstoid")
        PromoNya.Text = GVPromo.SelectedDataKey("progpointdesc")
        GVPromo.Visible = False
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("View")
    End Sub

    Protected Sub sItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sItemNya.Click
        If GVItemList.Visible = True Then
            UpdateCheckedMat()
            Dim dBar As DataTable = Session("TblListMatView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "(itemcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemNya.Text.Trim) & "%')"
            If MisV.Count > 0 Then
                Session("DataItem") = MisV.ToTable
                GVItemList.DataSource = Session("DataItem")
                GVItemList.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindItemNya(ItemNya.Text)
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        UpdateCheckedMat()
        GVItemList.PageIndex = e.NewPageIndex
        GVItemList.DataSource = Session("TblListMatView")
        GVItemList.DataBind()
    End Sub

    Protected Sub BtnViewList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewList.Click
        If GVItemList.Visible = True Then
            UpdateCheckedMat()
            Dim dBar As DataTable = Session("TblListMatView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                Session("DataItem") = MisV.ToTable
                GVItemList.DataSource = Session("DataItem")
                GVItemList.DataBind() : MisV.RowFilter = ""
            End If
        End If
    End Sub

    Protected Sub eItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eItemNya.Click
        ItemNya.Text = ""
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Customer" Then
            Label5.Text = "Customer"
            TD1.Visible = True : TD2.Visible = True
            TD7.Visible = True : TD8.Visible = True
        ElseIf ddlType.SelectedValue = "Sales" Then
            Label5.Text = "Sales"
            TD1.Visible = True : TD2.Visible = True
            TD7.Visible = True : TD8.Visible = True
        Else
            TD1.Visible = False : TD2.Visible = False
            TD7.Visible = False : TD8.Visible = False
        End If
        gvSupp.Visible = False
        If ddlType.SelectedValue = "Customer" Then
            gvSupp.Columns(1).HeaderText = "Kode Customer"
            gvSupp.Columns(2).HeaderText = "Nama Customer"
        Else
            gvSupp.Columns(1).HeaderText = "NIP"
            gvSupp.Columns(2).HeaderText = "Name"
        End If
        suppoid.Text = "" : suppname.Text = ""
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        If ddlStatus.SelectedValue = "SUMMARY" Then
            TD9.Visible = False : TD10.Visible = False
            TD11.Visible = False : TD12.Visible = False
        Else
            TD9.Visible = True : TD10.Visible = True
            TD11.Visible = True : TD12.Visible = True
        End If
    End Sub
#End Region
    
End Class
