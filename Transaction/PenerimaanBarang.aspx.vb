Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_PenerimaanBarang
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader : Dim xKon As New Koneksi
    Dim sSql As String = "" : Dim dsData As New DataSet
    Dim dv As DataView : Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure
    'Dim cClass As New ClassProc
    Dim cust As String = "" : Dim jenis As String = "" : Dim brand As String = ""
    Dim flag As String = "" : Dim sColom As String : Dim sValue As String
    Dim iVal As Integer : Dim iCount As Integer = 0
    Dim report As New ReportDocument : Dim vreport As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Function TambahRow()
        If lblUpdNo.Text <> "" Then
            TambahRow = lblUpdNo.Text + 1
            'ElseIf lblUpdNo.Text = Session("Row") Then
            '    TambahRow = gvNoKerusakan.Rows.Count + 1
        Else
            TambahRow = gvNoKerusakan.Rows.Count + 1
        End If

        Return TambahRow
    End Function

    Private Function generateMerk(ByVal txtCodemerk As String) As String
        Dim codeVal As String = ""
        txtCodemerk = "MB"
        Dim merkPrefix As String = txtCodemerk.Substring(0, 2)
        sSql = "select gencode from QL_mstgen where gencode like '" & merkPrefix & "%' order by gencode desc "
        Dim x As Object = ckoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode merk seperti yg diminta tidak ada sama sekali, generate kode baru
            codeVal = UCase(merkPrefix) & "00001"
        Else
            If x = "" Then
                ' kode merk seperti yg diminta tidak ada sama sekali, generate kode baru
                codeVal = UCase(merkPrefix) & "00001"
            Else
                ' kode merk seperti yg diminta ada, tinggal generate angka
                'Dim nomer As Integer = CInt(x.ToString.Substring(2, 5))
                'nomer += 1
                'codeVal = UCase(merkPrefix) & tNol(nomer)
            End If
        End If
        Return codeVal
    End Function

    Private Function generateCustCode(ByVal namacustomer As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = namacustomer.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select custcode from QL_mstcust where custcode like '" & custPrefix & "%' order by custcode desc "
        Dim x As Object = ckoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tNol(angka)
            End If
        End If
        Return retVal
    End Function

    Function tNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function

    Private Function setTableDetail() As DataTable
        Dim dt As DataTable = New DataTable("trnRequest")
        Dim ds As New DataSet
        dt.Columns.Add("Row", Type.GetType("System.Int32"))
        dt.Columns.Add("sequence", Type.GetType("System.Int32"))
        dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemdesc", Type.GetType("System.String"))
        dt.Columns.Add("trnjualoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnjualno", Type.GetType("System.String"))
        dt.Columns.Add("snno", Type.GetType("System.String"))
        dt.Columns.Add("kelengkapan", Type.GetType("System.String"))
        dt.Columns.Add("qty", Type.GetType("System.Double"))
        dt.Columns.Add("garansi", Type.GetType("System.String"))
        dt.Columns.Add("kerusakan", Type.GetType("System.String"))
        dt.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        ds.Tables.Add(dt)
        Return dt
    End Function

    Function GenerateDtlID()
        iVal = GenerateID("ql_trnrequestdtl", CompnyCode)
        Return iVal
    End Function

    Private Function generateBarcode()
        Dim jenis As String = "" : Dim merk As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim urut As String = ""
        'jenis = JenisDDL.SelectedItem.Text.Substring(0, 1)
        merk = MerkDDL.SelectedItem.Text.Substring(0, 1)
      
        Dim sBarcode As String = merk & "/" & Format(GetServerTime, "yy/MM/dd") & "/"
        '& urut

        If i_u.Text = "New" Then
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(barcode,4) AS INTEGER))+1,1) AS IDNEW FROM QL_trnrequest where cmpcode='" & CompnyCode & "' and barcode like '" & sBarcode & "%'"
        ElseIf lblBarcode.Text = "update" Then
            sSql = "select (CAST(RIGHT(barcode,4) AS INTEGER)) FROM QL_trnrequest where cmpcode='" & CompnyCode & "' and barcode like '" & sBarcode & "%'"
        End If

        xCmd.CommandText = sSql : urut = Format(xCmd.ExecuteScalar(), "0000")
        sBarcode = sBarcode & urut : txtBarcode.Text = sBarcode
        Return sBarcode
    End Function

    Private Function genBarcode() As String
        Dim retBarcode As String = ""
        Dim jenis As String = JenisDDL.SelectedValue.Substring(0, 1) ' 1 digit jenis barang
        Dim merk As String = MerkDDL.SelectedValue.Substring(0, 3) ' 3 digit merk
        Dim tahun As String = Format(GetServerTime, "yy") ' ambil dua digit tahun paling belakang
        Dim burom As String = getRomawi(Format(GetServerTime, "MM")) ' ambil bulan romawi
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "select barcode from ql_trnrequest where barcode like '" & jenis & "%'"
        Dim cekJenis As Object = ckoneksi.ambilscalar(sSql)
        sSql = "select barcode from ql_trnrequest where barcode like '%" & merk & "%'"
        Dim cekMerk As Object = ckoneksi.ambilscalar(sSql)
        sSql = "select barcode from ql_trnrequest where barcode like '%" & tahun & "%'"
        Dim cekTahun As Object = ckoneksi.ambilscalar(sSql)
        sSql = "select barcode from ql_trnrequest where barcode like '%" & burom & "%'"
        Dim cekBurom As Object = ckoneksi.ambilscalar(sSql)
        sSql = "select (lastoid+1) from QL_mstoid where tablename='barcode' and cmpcode like '%" & CompnyCode & "%'"
        Dim urut As Object = ckoneksi.ambilscalar(sSql)
        Dim barcode As String = cekJenis & "/" & cekMerk & "/" & cekTahun & "/" & cekBurom & "/" '& urut
        If barcode Is Nothing Then
            'barcode yang diminta tidak ada yang sama
            retBarcode = jenis & "/" & merk & "/" & tahun & "/" & burom & "/" & "0001"
        Else
            If barcode = "" Then
                'barcode yang diminta tidak ada yang sama
                retBarcode = jenis & "/" & merk & "/" & tahun & "/" & burom & "/" & "0001"
            Else
                'barcode ada tinggal digenerate
                'Dim angka As Integer = CInt(barcode.ToString.Substring(10, 1))
                'angka += 1
                retBarcode = barcode & urut
            End If
        End If
        Return retBarcode
    End Function

    Function tambahNol(ByVal iAngka As Integer)
        Dim sValue = "000"
        If iAngka >= 10 Then : sValue = "00" : End If
        If iAngka >= 100 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 1000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function
#End Region

#Region "Procedure"
    Private Sub InitMtrlOc()
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother6='UMUM' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & DdlCabang.SelectedValue & "' and gengroup = 'cabang')"
        FillDDL(MtrLocDDL, sSql) 
        If MtrLocDDL.Items.Count = 0 Then
            showMessage("Please create/fill Data General in group LOCATION!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal message As String)
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String) 

        sSql = "SELECT DISTINCT rq.REQOID,rq.REQCODE,cust.phone1 ,rq.BARCODE,cust.custname, REQSERVICECAT, REQFLAG,convert(char(20), rq.createtime, 103) as tanggal, rq.REQSTATUS,Case rq.REQSERVICECAT When 'M' Then 'Mutasi' When 'Y' then 'Saldo Awal' When 'N' Then 'Service' Else 'Service' End TypeTTS FROM QL_TRNREQUEST rq inner join QL_mstcust cust on rq.REQCUSTOID = cust.custoid WHERE cust.cmpcode like '%" & CompnyCode & "%'" & sWhere & " And rq.REQSTATUS <> 'paid'"

        If fCbangDDL.SelectedValue <> "ALL" Then
            sSql &= " And rq.branch_code='" & fCbangDDL.SelectedValue & "'"
        End If

        If DDLTypeTTS.SelectedValue <> "ALL" Then
            sSql &= " And rq.REQSERVICECAT='" & DDLTypeTTS.SelectedValue & "'"
        End If

        sSql &= " Order by REQOID desc "
        Session("TblMst") = ckoneksi.ambiltabel(sSql, "PenerimaanBarang")
        gvPenerimaan.DataSource = Session("TblMst")
        gvPenerimaan.DataBind()
        gvPenerimaan.SelectedIndex = -1
    End Sub

    Private Sub FillTextBox(ByVal foid As Integer)
        sSql = "select r.branch_code,r.mtrwhoid,r.cmpcode,r.reqoid,r.reqcode,r.reqcustoid,r.reqservicecat,r.reqperson,r.reqstatus,r.reqflag,r.barcode,r.createuser,convert(char(20), r.reqdate, 103) as tanggal,convert(char(20), r.reqdate, 108) as waktu,r.upduser,r.updtime,r.chkdate,c.custname, c.custaddr,c.phone1,c.contactperson1,c.phone2,r.reqdate,r.reqperson,isnull(t.PERSONNAME,'') PERSONNAME, reqservicecat,r.mtrlocoid,r.createtime from QL_TRNREQUEST r inner join QL_MSTCUST c ON r.reqcustoid = c.custoid left join ql_mstperson t on r.reqperson = t.personoid where r.cmpcode= '" & CompnyCode & "' AND r.reqoid='" & foid & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                lbloid.Text = Integer.Parse(xreader("REQOID"))
                txtNoTanda.Text = xreader("REQCODE").ToString.Trim
                txtTglTerima.Text = xreader("tanggal").ToString.Trim
                txtWaktuTerima.Text = xreader("waktu").ToString.Trim
                txtNamaCust.Text = xreader("custname").ToString.Trim
                txtAlamat.Text = xreader("custaddr").ToString.Trim
                txtHP.Text = xreader("phone1").ToString.Trim
                txtStatus.Text = xreader("REQSTATUS").ToString.Trim
                txtBarcode.Text = xreader("BARCODE").ToString.Trim
                txtreqdate.Text = xreader("REQDATE").ToString.Trim
                UpdTime.Text = xreader("updtime").ToString.Trim
                lblCust.Text = xreader("reqcustoid").ToString.Trim
                cperson1.Text = xreader("contactperson1").ToString.Trim
                telp2.Text = xreader("phone2").ToString.Trim
                createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss.fff")

                If xreader("reqservicecat").ToString.Trim = "" Then
                    saldoawal.SelectedValue = "N"
                Else
                    saldoawal.SelectedValue = xreader("reqservicecat").ToString.Trim
                End If
                DdlCabang.SelectedValue = xreader("branch_code").ToString.Trim
                InitDDLLocation()
                DDLLocation.SelectedValue = xreader("mtrwhoid").ToString.Trim
                InitMtrlOc()
                MtrLocDDL.SelectedValue = xreader("mtrlocoid").ToString.Trim
            End While
        End If
        xreader.Close()
        conn.Close()

        saldoawal_SelectedIndexChanged(Nothing, Nothing)
        If txtStatus.Text.ToUpper = "IN PROCESS" Then
            rblgaransi.Enabled = True
            If saldoawal.SelectedValue = "N" Then
                btnPost.Visible = True : BtnSendApp.Visible = False
            Else
                btnPost.Visible = False : BtnSendApp.Visible = True
            End If
        Else
            BtnSendApp.Visible = False
            btnPost.Visible = False : btnAddToList.Visible = False
            btnClear.Visible = False : btnBarcode.Visible = False
            btnSave.Visible = False : gvNoKerusakan.Enabled = False
            btnDelete.Visible = False : btnSearchCust.Enabled = False
            btnErase.Enabled = False : JenisDDL.Enabled = False
            MerkDDL.Enabled = False : txtNamaBarang.Enabled = False
            printmanualpenerimaan.Visible = True : rblgaransi.Enabled = False
        End If

        sSql = "select distinct ROW_NUMBER() OVER(ORDER BY d.reqdtloid) sequence,d.itemoid, d.itemdesc,CAST(d.reqqty AS DECIMAL(18,4)) qty, d.typegaransi garansi, d.reqdtljob as kerusakan, d.reqdtloid as reqdtloid, trnjualoid, Isnull((select jm.trnjualno from ql_trnjualmst jm where jm.trnjualmstoid = d.trnjualoid),'None SI') trnjualno, snno, kelengkapan from QL_TRNREQUESTDTL d inner join QL_TRNREQUEST r ON r.reqoid = d.reqmstoid where r.cmpcode like '%" & CompnyCode & "%' AND d.reqmstoid = '" & foid & "' And r.branch_code='" & DdlCabang.SelectedValue & "'"
        Dim data As DataTable = ckoneksi.ambiltabel(sSql, "data")
        gvNoKerusakan.DataSource = data
        gvNoKerusakan.DataBind()
        gvNoKerusakan.SelectedIndex = -1
        gvNoKerusakan.Visible = True
        Session("gvNoKerusakan") = data
    End Sub

    Private Sub FilterGVList(ByVal barcode As String, ByVal jenis As String, ByVal merk As String, ByVal nama As String, ByVal kategori As String)
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub fInitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCbangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCbangDDL, sSql)
            Else
                FillDDL(fCbangDDL, sSql)
                fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
                fCbangDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCbangDDL, sSql)
            fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
            fCbangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitAllDDL()
        'Jenis Barang
        sSql = "Select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & CompnyCode & "' And genoid IN (Select Typejob From QL_mstitemserv it Where it.Typejob=genoid) ORDER BY gendesc"
        FillDDL(JenisDDL, sSql)
        'Merk Barang
        sSql = "Select genoid ,g2.gendesc from ql_mstgen g2 where cmpcode = '" & CompnyCode & "' and gengroup = 'ITEMMERK' ORDER BY g2.GENDESC"
        FillDDL(MerkDDL, sSql)
        InitDDLLocation()
    End Sub

    Private Sub GenerateReqCode()
        'If i_u.Text = "New" Then
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim code As String = ""
            sSql = "Select genother1 from ql_mstgen where gengroup='cabang' and gencode='" & DdlCabang.SelectedValue & "'"
            xCmd.CommandText = sSql : Dim sCabang As String = xCmd.ExecuteScalar()
            code = "SRV/" & sCabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(reqcode,4) AS INT)),0) FROM QL_TRNREQUEST WHERE reqcode LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            Session("reqcode") = code & sequence
            txtNoTanda.Text = Session("reqcode")

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        'End If
    End Sub

    Private Sub ClearDetail()
        iuAdd.Text = "new" : txtDetailKerusakan.Text = ""
        itemoid.Text = "" : txtNamaBarang.Text = ""
        txtQty.Text = "" : snno.Text = ""
        trnjualoid.Text = "" : trnjualno.Text = ""
        txtKelengkapan.Text = ""
    End Sub

    Private Sub clearRef()
        txtNamaCust.Text = "" : txtAlamat.Text = "" : txtHP.Text = "" : cperson1.Text = "" : telp2.Text = ""
        Session("gvListCust") = Nothing : gvNoKerusakan.DataSource = Nothing : gvNoKerusakan.DataBind()
    End Sub

    Private Sub BindDataCust(ByVal sWhere As String)
        sSql = "Select distinct c.custoid, c.custcode,c.custname, c.custaddr, c.phone1,c.contactperson1 ,c.phone2,(Select gendesc from QL_mstgen cb Where cb.gencode=c.branch_code AND cb.gengroup='CABANG') NmCabang From QL_MSTCUST c where c.cmpcode like '%" & CompnyCode & "%' " & sWhere & " And branch_code='" & DdlCabang.SelectedValue & "'"
        Dim xTableItem2 As DataTable = ckoneksi.ambiltabel(sSql, "Customer")
        gvListCust.DataSource = xTableItem2
        gvListCust.DataBind() : gvListCust.SelectedIndex = -1
        gvListCust.Visible = True
    End Sub

    Private Sub BindDataItem(ByVal sWhere As String)
        Dim sNone As String = ""
        
        If lblCust.Text <> "" Then
            If rblgaransi.SelectedValue = "Garansi" Then
                If DdlCabang.SelectedValue = "10" Or DdlCabang.SelectedValue = "11" Then
                    sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir From ql_mstitem i Where i.stockflag<>'ASSET' AND cmpcode='" & CompnyCode & "'" & sWhere & ""
                Else
                    If trnjualno.Text <> "None SI" Then
                        sNone = " AND jm.trnjualmstoid = " & trnjualoid.Text & ""
                        sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir from ql_mstitem i Where i.stockflag<>'ASSET' And itemoid IN (select itemoid from QL_trnjualdtl jd Inner Join QL_trnjualmst jm On jm.trnjualmstoid=jd.trnjualmstoid Where jd.itemoid=i.itemoid AND jm.branch_code='" & DdlCabang.SelectedValue & "' AND jm.trncustoid=" & lblCust.Text & " " & sNone & ") " & sWhere & ""
                    Else
                        sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir From ql_mstitem i Where cmpcode='" & CompnyCode & "' And stockflag<>'ASSET'" & sWhere & ""
                    End If
                End If
            Else
                If DdlCabang.SelectedValue = "10" Or DdlCabang.SelectedValue = "11" Then
                    sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir From ql_mstitem i Where i.stockflag<>'ASSET' AND cmpcode='" & CompnyCode & "'" & sWhere & ""
                Else
                    If trnjualno.Text <> "None SI" Then
                        sNone = " AND jm.trnjualmstoid = " & trnjualoid.Text & ""
                        sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir from ql_mstitem i Where i.stockflag<>'ASSET' And itemoid IN (select itemoid from QL_trnjualdtl jd Inner Join QL_trnjualmst jm On jm.trnjualmstoid=jd.trnjualmstoid Where jd.itemoid=i.itemoid AND jm.branch_code='" & DdlCabang.SelectedValue & "' AND jm.trncustoid=" & lblCust.Text & " " & sNone & ") " & sWhere & ""
                    Else
                        sSql = "Select itemoid, itemcode, itemdesc,Case i.stockflag When 'T' Then 'Transaction' When 'I' Then 'Material Usage' When 'V' Then 'Barang Hadiah' Else 'ASSET' End stockflag,(Select ISNULL(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) from QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & MtrLocDDL.SelectedValue & ") StokAkhir From ql_mstitem i Where cmpcode='" & CompnyCode & "' And stockflag<>'ASSET'" & sWhere & ""
                    End If
                End If
            End If
            sSql &= " Order By itemdesc"
            Dim xTableItem2 As DataTable = ckoneksi.ambiltabel(sSql, "Item")
            gvListItem.DataSource = xTableItem2
            gvListItem.DataBind() : gvListItem.SelectedIndex = -1
            If saldoawal.SelectedValue = "M" Then
                gvListItem.Columns(4).Visible = True
            Else
                gvListItem.Columns(4).Visible = False
            End If
            gvListItem.Visible = True
        Else
            showMessage("Maaf, Nama Customer belum anda pilih..!!", CompnyName & "- Warning", 3)
        End If
    End Sub

    Private Sub BindDataSI(ByVal sWhere As String)
        sSql = "select distinct jm.trnjualmstoid, jm.trnjualno, CONVERT(varchar(10),jm.trnjualdate,103) trnjualdate from QL_trnjualdtl jd INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid = jd.trnjualmstoid and jm.branch_code = jd.branch_code where jm.cmpcode  = '" & CompnyCode & "' and jm.branch_code = '" & DdlCabang.SelectedValue & "' and jm.trncustoid = " & lblCust.Text & " " & sWhere & " UNION ALL select -9999 AS trnjualmstoid, 'None SI' AS trnjualno,CONVERT(varchar(10),CURRENT_TIMESTAMP,103) AS trnjualdate order by trnjualmstoid, trnjualno"
        Dim xTableItem2 As DataTable = ckoneksi.ambiltabel(sSql, "salesinvoice")
        gvListSI.DataSource = xTableItem2
        gvListSI.DataBind()
        gvListSI.SelectedIndex = -1
        gvListSI.Visible = True
    End Sub

    Private Sub InitDDLLocation()
        'From Locotion
        sSql = "Select a.genoid, a.gendesc from QL_mstgen a Where a.gengroup='LOCATION' and a.genother6 = 'TITIPAN' AND a.genother2 IN (Select genoid from QL_mstgen br Where br.genoid=a.genother2 AND br.gencode='" & DdlCabang.SelectedValue & "' AND gengroup='CABANG') /*AND genother1 IN (Select genoid from QL_mstgen be Where be.genoid=a.genother1 AND be.gengroup='WAREHOUSE')*/"
        FillDDL(DDLLocation, sSql)
        If DDLLocation.Items.Count = 0 Then
            showMessage("Maaf, Gudang belum ada, Silahkan hubungi admin untuk input gudang baru di form master general,dengan <br> Nama Kolom type warehouse : gudang service <br> Kolom type gudang : Gudang Titipan,..!!", CompnyName & "- Warning", 3)
            Exit Sub
        End If
    End Sub 

    Private Sub FillTextCust(ByVal Code As Integer)
        sSql = "select distinct c.custoid, c.custcode,c.custname, c.custaddr, c.phone1,c.contactperson1 ,c.phone2 from QL_MSTCUST c where c.cmpcode='" & CompnyCode & "' AND c.custoid='" & Code & "' And branch_code='" & DdlCabang.SelectedValue & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                lblCust.Text = xreader("custoid").ToString.Trim
                txtNamaCust.Text = xreader("custname").ToString.Trim
                txtAlamat.Text = xreader("custaddr").ToString.Trim
                txtHP.Text = xreader("phone1").ToString.Trim
                cperson1.Text = xreader("contactperson1").ToString.Trim
                telp2.Text = xreader("phone2").ToString.Trim
            End While
        End If
    End Sub

    Private Sub FillTextItem(ByVal Code As Integer)
        sSql = "select itemoid, itemcode, itemdesc from ql_mstitem where cmpcode = '" & CompnyCode & "' and itemoid = " & Code & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                itemoid.Text = xreader("itemoid").ToString.Trim
                txtNamaBarang.Text = xreader("itemdesc").ToString.Trim
            End While
        End If
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String)
        Dim code As String = ckoneksi.ambilscalar("Select reqoid From ql_trnrequest Where reqoid='" & id & "'")
        Dim nooid As String = ckoneksi.ambilscalar("Select reqcode From ql_trnrequest Where reqoid='" & id & "'")
        report.Load(Server.MapPath(folderReport & "printnota.rpt"))
        report.SetParameterValue("sWhere", id)
        report.SetParameterValue("cmpcode", CompnyCode)
        report.SetParameterValue("namaPencetak", Session("UserID"))
        CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        Try
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "DetPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose() : Session("no") = Nothing
            Response.Redirect("~\Transaction\PenerimaanBarang.aspx?awal=true")
        Catch ex As Exception
            report.Close() : report.Dispose()
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub imbSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchItem.Click
        If txtNamaCust.Text = "" Then
            showMessage("Isi Nama Customer Terlebih Dahulu !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If
        If trnjualno.Text = "" Then
            showMessage("Isi No. SI Terlebih Dahulu (Jika Tidak ada No. SI Bisa Menggunakan 'None SI')!!", CompnyName & "- Warning", 2)
            Exit Sub
        End If
        panelItem.Visible = True : btnHideItem.Visible = True
        mpeItem.Show() : BindDataItem("")
    End Sub

    Protected Sub imbViewItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewItem.Click
        panelItem.Visible = True
        btnHideItem.Visible = True
        mpeItem.Show() : BindDataItem("")
        txtFilterItem.Text = "" : DDLFilterItem.SelectedIndex = 0
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        gvListItem.PageIndex = e.NewPageIndex
        panelItem.Visible = True
        btnHideItem.Visible = True : mpeItem.Show()
        Dim sWhere As String = " AND " & DDLFilterItem.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterItem.Text) & "%'"
        BindDataItem(sWhere)
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListItem.SelectedIndexChanged
        Dim sCode As Integer
        btnHideItem.Visible = False : panelItem.Visible = False
        mpeItem.Show()
        sCode = gvListItem.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextItem(sCode)
        txtFilterItem.Text = ""
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemoid.Text = "" : txtNamaBarang.Text = ""
        txtQty.Text = "" : txtDetailKerusakan.Text = ""
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        panelItem.Visible = True
        btnHideItem.Visible = True
        mpeItem.Show()
        Dim sWhere As String = " AND " & DDLFilterItem.SelectedValue & " LIKE '%" & Tchar(txtFilterItem.Text) & "%'"
        BindDataItem(sWhere)
        DDLFilterItem.SelectedIndex = 0
    End Sub

    Protected Sub gvNoKerusakan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\PenerimaanBarang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Penerimaan Barang Service"
        Session("oid") = Request.QueryString("oid")

        Me.btnPost.Attributes.Add("onclick", "return confirm('Data sudah dipost tidak dapat diupdate');")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah anda yakin akan menghapus data ini ??');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            InitCabang() : fInitCabang()
            BindData("") : InitAllDDL()
            InitDDLLocation() : InitMtrlOc()
            saldoawal_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                i_u.Text = "New" : Session("new") = True
                'GenerateReqCode()
                txtNoTanda.Text = GenerateID("ql_trnrequest", CompnyCode)
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                dtlseq.Text = 1 : Session("sequence") = dtlseq.Text
                txtTglTerima.Text = Format(GetServerTime(), "dd/MM/yyyy")
                txtWaktuTerima.Text = Format(GetServerTime(), "HH:mm")
                TabContainer1.ActiveTabIndex = 0 : txtStatus.Text = "In Process"
                btnClear.Visible = False : btnPost.Visible = False
                btnDelete.Visible = False : lblUpd.Text = "Created"
                Session("gvNoKerusakan") = Nothing
                iuAdd.Text = "new" ': rblgaransi.Visible = True
            Else
                Session("new") = False
                FillTextBox(Session("oid"))
                i_u.Text = "Update" : lblBarcode.Text = "update"
                TabContainer1.ActiveTabIndex = 1
                UpdUser.Text = Session("UserID")
                lblUpd.Text = "Last Update"
            End If
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
        Dim objTable As DataTable
        objTable = Session("gvNoKerusakan")
        gvNoKerusakan.DataSource = objTable
        gvNoKerusakan.DataBind()

        If txtStatus.Text.ToUpper = "POST" Then
            btnSave.Visible = False : btnAddToList.Visible = False
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        'gvPenerimaan.PageIndex = 0
        Dim sWhere As String = "" : Dim vMsg As String = ""
        sWhere &= " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        If checkPeriod.Checked Then
            Try
                Dim date1 As Date = toDate(FilterPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If FilterPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    vMsg &= "- Maaf, Isi Tanggal dulu..!!<br>"
                End If

                If date1 < toDate("01/01/1900") Then
                    vMsg &= "- Maaf, Tanggal Awal Tidak Valid !!<br>"
                End If

                If date2 < toDate("01/01/1900") Then
                    vMsg &= "- Maaf, Tanggal Akhir Tidak Valid !!<br>"
                End If

                If date1 > date2 Then
                    vMsg &= "- Maaf, Tanggal Awal harus lebih kecil dari Tanggal Akhir !!<br>"
                End If

                sWhere &= " AND CONVERT(varchar(10),rq.createtime,103) between '" & FilterPeriod1.Text & "' AND '" & txtPeriod2.Text & "'"

            Catch ex As Exception
                vMsg &= "Maaf, Format Tanggal tidak sesuai !!<br>"
            End Try
        End If

        If checkStatus.Checked Then
            If StatusDDL.SelectedValue <> "All" Then
                sWhere &= " AND rq.reqstatus='" & StatusDDL.SelectedValue & "'"
            End If
        End If

        If vMsg <> "" Then
            showMessage(vMsg, CompnyName & "- Warning", 2)
            Exit Sub
        End If
        BindData(sWhere)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        itemoid.Text = "" : txtNamaBarang.Text = ""
        trnjualoid.Text = "" : trnjualno.Text = ""
        snno.Text = "" : txtKelengkapan.Text = ""
        txtQty.Text = 0
        txtDetailKerusakan.Text = "" : dtlseq.Text = ""
        lblUpdNo.Text = "" : iuAdd.Text = "new"
        gvNoKerusakan.SelectedIndex = -1
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("gvNoKerusakan") = Nothing : Session("oid") = Nothing
        Response.Redirect("PenerimaanBarang.aspx")
    End Sub

    Protected Sub imbViewCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewCust.Click
        panelCust.Visible = True
        btnHideCust.Visible = True
        mpeCust.Show() : BindDataCust("")
        txtFilterCust.Text = "" : DDLFilterCust.SelectedIndex = 0
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        FilterText.Text = "" : FilterDDL.SelectedIndex = 0
        FilterGVList("", "", "", "", "")
        checkPeriod.Checked = False : checkStatus.Checked = False
        fInitCabang() : BindData("")
        StatusDDL.SelectedIndex = 0 : gvPenerimaan.PageIndex = 0
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim xMsg As String = ""
        Dim period As String = GetDateToPeriodAcctg(GetServerTime())
        Dim MtrLocOid As Integer = 0
        If saldoawal.SelectedValue = "M" Then
            MtrLocOid = MtrLocDDL.SelectedValue
        Else
            MtrLocOid = 0
        End If

        If Session("new") = True Then
            Session("oid") = Nothing
        End If

        If txtNamaCust.Text = "" Then
            xMsg &= "Isi nama customer !!<br>"
        End If

        If Session("gvNoKerusakan") Is Nothing Then
            xMsg &= "Buat detail kerusakan !!<br>"
        Else
            Dim objTableCek As DataTable
            Dim objRowCek() As DataRow
            objTableCek = Session("gvNoKerusakan")
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                xMsg &= "- Maaf, Buat detail kerusakan !!<br>"
            End If
        End If

        If saldoawal.SelectedValue = "M" Then
            If Not Session("gvNoKerusakan") Is Nothing Then
                Dim Obj As DataTable = Session("gvNoKerusakan")
                For d1 As Int32 = 0 To Obj.Rows.Count - 1

                    Dim CekStok As Double = GetScalar("SELECT ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) FROM QL_conmtr wHERE refoid=" & Integer.Parse(Obj.Rows(d1).Item("itemoid")) & " And mtrlocoid=" & MtrLocDDL.SelectedValue & " and branch_code='" & DdlCabang.SelectedValue & "' and periodacctg='" & period & "'")
                    If ToDouble(txtQty.Text) > ToDouble(CekStok) Then
                        xMsg &= "- Maaf,Quantity Katalog " & Integer.Parse(Obj.Rows(d1).Item("itemdesc")) & " yang anda input melebihi stok akhir " & ToMaskEdit(ToDouble(CekStok), 3) & "..!!"
                    End If

                Next
            End If
        End If

        If checkApproval("QL_TRNREQUEST", "QL_TRNREQUEST", Session("oid"), "New", "FINAL", DdlCabang.SelectedValue) > 0 Then
            xMsg &= "- Maaf, data ini sudah send Approval..!!<br>"
        End If


        If txtStatus.Text.ToLower = "in approval" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNREQUEST' And branch_code LIKE '%" & DdlCabang.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = ckoneksi.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                xMsg &= "- Maaf, User untuk Cabang " & DdlCabang.SelectedItem.Text & " belum disetting, Silahkan hubungi admin..!!<br>"
            End If
        ElseIf txtStatus.Text = "Rejected" Then
            txtStatus.Text = "IN PROCESS"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_TRNREQUEST WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                xMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT reqstatus FROM QL_TRNREQUEST WHERE reqoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = 'MSC'"

            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then 
                xMsg &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    xMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If

        If xMsg <> "" Then
            txtStatus.Text = "IN PROCESS"
            showMessage(xMsg, CompnyName & "- Warning", 2)
            Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            lbloid.Text = GenerateID("QL_TRNREQUEST", CompnyCode)
        Else
            lbloid.Text = Session("oid")
        End If

        If txtStatus.Text = "POST" Then
            GenerateReqCode()
        Else
            txtNoTanda.Text = lbloid.Text
        End If

        Dim AppOid As Integer = GenerateID("QL_Approval", CompnyCode)
        Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
        Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
        Dim iGlSeq As Int16 = 1 : Dim lasthpp As Double = 0
        Dim objTrans As SqlClient.SqlTransaction

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            'insert
            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim sCok As String = MerkDDL.SelectedValue
                Dim jenis As String = "" : Dim merk As String = ""
                Dim urut As String = ""
                'jenis = JenisDDL.SelectedItem.Text.Substring(0, 1)
                merk = MerkDDL.SelectedItem.Text.Substring(0, 1)
                Dim sBarcode As String = merk & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                If i_u.Text = "New" Then
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(barcode,4) AS INTEGER))+1,1) AS IDNEW FROM QL_trnrequest where cmpcode='" & CompnyCode & "' and barcode like '" & sBarcode & "%'"
                ElseIf lblBarcode.Text = "update" Then
                    sSql = "select (CAST( RIGHT(barcode,4) AS INTEGER)) FROM QL_trnrequest where cmpcode='" & CompnyCode & "' and barcode like '" & sBarcode & "%'"
                End If

                xCmd.CommandText = sSql : urut = Format(xCmd.ExecuteScalar, "0000")
                sBarcode = sBarcode & urut : txtBarcode.Text = sBarcode

                If Not Session("gvNoKerusakan") Is Nothing Then
                    Dim objTable As DataTable
                    'MsgBox(lbloid.Text)
                    objTable = Session("gvNoKerusakan")
                    sSql = "INSERT INTO QL_TRNREQUEST (cmpcode,reqoid,reqcode,reqcustoid,reqstatus,reqflag,barcode,createuser,createtime,upduser,updtime,reqdate,reqperson,branch_code, mtrwhoid,reqservicecat,mtrlocoid) VALUES " & _
                    "('" & CompnyCode & "','" & lbloid.Text & "','" & txtNoTanda.Text & "'," & Integer.Parse(lblCust.Text) & ", '" & Tchar(txtStatus.Text) & "','','" & txtBarcode.Text & "','" & Session("UserID") & "','" & CDate(toDate(createtime.Text)) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0,'" & DdlCabang.SelectedValue & "', " & DDLLocation.SelectedValue & ",'" & saldoawal.SelectedValue & "'," & Integer.Parse(MtrLocOid) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If Not Session("gvNoKerusakan") Is Nothing Then
                        Dim objTableNo As DataTable = Session("gvNoKerusakan")
                        Dim norusak = GenerateID("ql_trnrequestdtl", CompnyCode)
                        objTableNo = Session("gvNoKerusakan")
                        For C1 As Integer = 0 To objTableNo.Rows.Count - 1
                            sSql = "insert into ql_trnrequestdtl(cmpcode,reqdtloid,reqmstoid,reqdtljob,itemoid,itemdesc,reqqty,typegaransi,createtime,updtime,branch_code, trnjualoid, snno, kelengkapan) values " & _
                            " ('" & CompnyCode & "'," & norusak & ",'" & lbloid.Text & "','" & Tchar(objTableNo.Rows(C1).Item("kerusakan")) & "','" & Tchar(objTableNo.Rows(C1).Item("itemoid")) & "','" & Tchar(objTableNo.Rows(C1).Item("itemdesc")) & "'," & objTableNo.Rows(C1).Item("qty") & ",'" & Tchar(objTableNo.Rows(C1).Item("garansi")) & "',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "','" & Tchar(objTableNo.Rows(C1).Item("trnjualoid")) & "','" & Tchar(objTableNo.Rows(C1).Item("snno")) & "','" & Tchar(objTableNo.Rows(C1).Item("kelengkapan")) & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            norusak += 1
                        Next
                        sSql = "UPDATE QL_mstoid set lastoid=(select max (reqdtloid) from ql_trnrequestdtl) where tablename='ql_trnrequestdtl' and cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    sSql = "UPDATE ql_mstoid set lastoid=" & Convert.ToInt32(lbloid.Text) & " From QL_mstoid where tablename ='QL_TRNREQUEST' and cmpcode like '%" & CompnyCode & "%'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Else 'UPDATE
                sSql = "UPDATE QL_TRNREQUEST set reqcustoid=" & Integer.Parse(lblCust.Text) & ", reqstatus='" & txtStatus.Text & "', reqflag='', barcode='" & txtBarcode.Text & "', createuser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "', reqdate=CURRENT_TIMESTAMP, reqperson= 0, mtrwhoid = " & DDLLocation.SelectedValue & " , reqservicecat = '" & saldoawal.SelectedValue & "', reqcode='" & txtNoTanda.Text & "', mtrlocoid=" & Integer.Parse(MtrLocOid) & ", branch_code='" & DdlCabang.SelectedValue & "' Where cmpcode='" & CompnyCode & "' and reqoid='" & lbloid.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Generate crdmtr ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

                'Generate conmtr ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
                Dim tempoid As Int32 = crdmtroid
                'Generate trnglmst ID
                sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim glmstoid As Integer = xCmd.ExecuteScalar + 1

                'Generate trngldtl ID
                sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim gldtloid As Integer = xCmd.ExecuteScalar + 1

                If Not Session("gvNoKerusakan") Is Nothing Then
                    Dim objTable As DataTable
                    Dim oid As Integer = GenerateID("ql_trnrequestdtl", CompnyCode)
                    objTable = Session("gvNoKerusakan")
                    If objTable.Rows.Count <> 0 Then
                        sSql = "DELETE ql_trnrequestdtl Where reqmstoid=" & Session("oid") & ""
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    Dim qty_to As Double = 0.0 : Dim amthpp As Double = 0.0

                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO ql_trnrequestdtl (cmpcode,reqdtloid,reqmstoid,reqdtljob,itemoid,itemdesc,reqqty,typegaransi,createtime,updtime,branch_code, trnjualoid, snno, kelengkapan) Values " & _
                        "('" & CompnyCode & "'," & Integer.Parse(oid) & ",'" & Integer.Parse(lbloid.Text) & "','" & Tchar(objTable.Rows(C1).Item("kerusakan")) & "','" & Integer.Parse(objTable.Rows(C1).Item("itemoid")) & "','" & Tchar(objTable.Rows(C1).Item("itemdesc")) & "'," & ToDouble(objTable.Rows(C1).Item("qty")) & ",'" & Tchar(objTable.Rows(C1).Item("garansi")) & "',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'" & DdlCabang.SelectedValue & "','" & Tchar(objTable.Rows(C1).Item("trnjualoid")) & "','" & Tchar(objTable.Rows(C1).Item("snno")) & "','" & Tchar(objTable.Rows(C1).Item("kelengkapan")) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Dim ReqdtlOid As Integer = Integer.Parse(oid)
                        oid += 1

                        qty_to = ToDouble(objTable.Rows(C1).Item("qty"))
                        If txtStatus.Text = "POST" Then
                            '--------------------------
                            sSql = "select hpp from ql_mstitem where itemoid=" & Integer.Parse(objTable.Rows(C1).Item("itemoid")) & ""
                            xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                            If ToDouble(lasthpp) = 0.0 Then
                                lasthpp = 1.0
                            End If
                            '---------------------------
                            amthpp = qty_to * lasthpp

                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note,HPP,Branch_Code,conrefoid) VALUES " & _
                            "('" & CompnyCode & "', " & conmtroid & ", 'PENERIMAAN', '" & GetServerTime() & "', '" & period & "', '" & Tchar(txtNoTanda.Text) & "', " & Integer.Parse(ReqdtlOid) & ", 'ql_trnrequestdtl', " & Integer.Parse(objTable.Rows(C1).Item("itemoid")) & ", 'QL_MSTITEM', 945, " & Integer.Parse(DDLLocation.SelectedValue) & ", " & ToDouble(objTable.Rows(C1).Item("qty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0,'" & Tchar(objTable.Rows(C1).Item("kerusakan")) & "', " & ToDouble(lasthpp) & ",'" & DdlCabang.SelectedValue & "'," & Integer.Parse(ReqdtlOid) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            conmtroid += 1
                        End If
                    Next

                    sSql = "UPDATE QL_mstoid set lastoid=(select max (reqdtloid) from ql_trnrequestdtl) Where tablename='ql_trnrequestdtl' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'UPDATE lastoid QL_conmtr
                    sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

            End If

            If txtStatus.Text = "IN APPROVAL" Then
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,branch_code,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus) VALUES" & _
                            " ('" & CompnyCode & "'," & AppOid + c1 & ",'" & DdlCabang.SelectedValue & "','" & "SRV" & AppOid & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNREQUEST','" & Session("oid") & "','IN APPROVAL','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & AppOid + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    objTrans.Rollback() : conn.Close() : txtStatus.Text = "IN PROCESS"
                    showMessage("- Silahkan Input Data", CompnyName & "- INFORMATION", 2)
                    Exit Sub
                End If
            End If

            'sSql = "Update ql_mstoid set lastoid=(SELECT ISNULL(MAX(CAST(RIGHT(barcode,3) AS INTEGER))+0,0) FROM QL_trnrequest) Where tablename='barcode' and cmpcode='" & CompnyCode & "'"
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : txtStatus.Text = "IN PROCESS"
            showMessage(ex.ToString & " - " & sSql, CompnyName & "- Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\PenerimaanBarang.aspx?awal=true")
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        Dim sCode As Integer
        btnHideCust.Visible = False : panelCust.Visible = False
        mpeCust.Show()
        sCode = gvListCust.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextCust(sCode)
        txtFilterCust.Text = ""
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        panelCust.Visible = True : btnHideCust.Visible = True
        mpeCust.Show() : BindDataCust("")
    End Sub

    Protected Sub imbFindCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindCust.Click
        panelCust.Visible = True
        btnHideCust.Visible = True
        mpeCust.Show()

        Dim sWhere As String = " AND " & DDLFilterCust.SelectedValue & " LIKE '%" & Tchar(txtFilterCust.Text) & "%'"
        BindDataCust(sWhere)
        DDLFilterCust.SelectedIndex = 0
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        txtStatus.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErase.Click
        txtNamaCust.Text = "" : txtAlamat.Text = ""
        txtHP.Text = "" : cperson1.Text = ""
        telp2.Text = "" : gvListCust.Visible = False
    End Sub

    Protected Sub gvPenerimaan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPenerimaan.PageIndexChanging
        gvPenerimaan.PageIndex = e.NewPageIndex
        gvPenerimaan.DataSource = Session("TblMst")
        gvPenerimaan.DataBind()
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Try
            Dim nMsg As String = "" : Dim sJual As String = "" : Dim MtrLocOid As Integer = 0
            Dim period As String = GetDateToPeriodAcctg(GetServerTime())

            If txtNamaCust.Text.Trim = "" Then
                nMsg &= "- Maaf, Isi nama customer..!!<br>"
            End If

            If txtNamaBarang.Text.Trim = "" Then
                nMsg &= "- Maaf, Isikan nama barang terlebih dahulu..!!<br>"
            Else
                If itemoid.Text = "" Then
                    nMsg &= "- Maaf, Nama Barang Tidak Sesuai Dengan Data Yang Terdapat di Sistem..!<br>"
                End If
            End If

            If DdlCabang.SelectedValue = "10" Or DdlCabang.SelectedValue = "11" Then
                If trnjualno.Text.Trim = "" Then
                    trnjualoid.Text = ""
                End If
            Else
                If trnjualno.Text.Trim = "" Then
                    nMsg &= "- Maaf, Silahkan Memilih No. SI (Jika Tidak ada No. SI Bisa Menggunakan 'None SI')!!<br>"
                End If
            End If

            If txtDetailKerusakan.Text.Trim = "" Then
                nMsg &= "- Maaf, Isi detail kerusakan...!!<br>"
            End If

            If txtNamaBarang.Text = "" Then
                nMsg &= "- Maaf, Nama Barang Belum di isi, silahkan isi terlebih dahulu...!!"
            End If

            If txtQty.Text = "0" Then
                nMsg &= "- Maaf, Qty tidak boleh nol..!!<br>"
            End If

            If txtQty.Text = "" Then
                nMsg &= "- Maaf, Qty tidak boleh nol..!!<br>"
            End If

            If ToDouble(txtQty.Text) = 0.0 Then
                nMsg &= "- Maaf, Qty tidak boleh nol..!!<br>"
            End If

            If txtDetailKerusakan.Text.Length > 150 Then
                nMsg &= "- Maaf, Maksimal panjang karakter 150..!!<br>"
            End If

            If Tchar(snno.Text) = "" Then
                nMsg &= "- Maaf, Nomer SN Kosong..!!<br>, Jika barang tidak mempunyai nomer SN silahkan isi dengan angka..!!"
            End If

            If nMsg <> "" Then
                showMessage(nMsg, CompnyName & "- Warning", 2)
                Exit Sub
            End If

            If saldoawal.SelectedValue = "M" Then
                Dim CekStok As Double = GetScalar("SELECT iSNULL(SUM(qtyIn)-SUM(qtyOut),0.00) FROM QL_conmtr wHERE refoid=" & Integer.Parse(itemoid.Text) & " and mtrlocoid=" & MtrLocDDL.SelectedValue & " and branch_code='" & DdlCabang.SelectedValue & "' and periodacctg='" & period & "'")
                If ToDouble(txtQty.Text) > ToDouble(CekStok) Then
                    nMsg &= "- Maaf,Quantity Katalog " & txtNamaBarang.Text.Trim & " yang anda input melebihi stok akhir " & ToMaskEdit(ToDouble(CekStok), 3) & "..!!"
                End If
            End If

            'generateBarcode()
            If nMsg <> "" Then
                showMessage(nMsg, CompnyName & "- Warning", 2)
                Exit Sub
            End If

            If Session("gvNoKerusakan") Is Nothing Then
                Dim dtlTable As DataTable = setTableDetail()
                Session("gvNoKerusakan") = dtlTable
            End If
            btnClear.Visible = True

            Dim dtab As DataTable
            Dim objTable As DataTable
            objTable = Session("gvNoKerusakan")
            Dim dv As DataView = objTable.DefaultView

            If iuAdd.Text = "new" Then
                If Session("gvNoKerusakan") Is Nothing Then
                    Dim dtlTable As DataTable = setTableDetail()
                    Session("gvNoKerusakan") = dtlTable
                    dtlseq.Text = "1"
                Else
                    dtab = Session("gvNoKerusakan")
                    dtlseq.Text = (dtab.Rows.Count + 1).ToString
                End If
            Else
                dtab = Session("gvNoKerusakan")
            End If

            If DdlCabang.SelectedValue = "10" Or DdlCabang.SelectedValue = "11" Then
                If trnjualoid.Text = "" Then
                    trnjualoid.Text = 0
                Else
                    sJual = "AND trnjualoid = " & Integer.Parse(trnjualoid.Text)
                End If
            Else
                sJual = "AND trnjualoid = " & Integer.Parse(trnjualoid.Text)
            End If

            '-- Buka Koneksi ke sql server --
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            If trnjualno.Text <> "None SI" Then
                sSql = "Select trnjualdtlqty from QL_trnjualdtl Where trnjualmstoid=" & Integer.Parse(trnjualoid.Text) & " AND branch_code='" & DdlCabang.SelectedValue & "' AND itemoid=" & Integer.Parse(itemoid.Text) & ""
                xCmd.CommandText = sSql : Dim QtySI As Double = ToDouble(xCmd.ExecuteScalar)
                If Integer.Parse(trnjualoid.Text) <> 0 Then
                    If ToDouble(txtQty.Text) > ToDouble(QtySI) Then
                        nMsg &= "- Maaf, Qty " & txtNamaBarang.Text & " " & ToMaskEdit(txtQty.Text, 4) & " melebihi Qty SI " & ToMaskEdit(QtySI, 4) & "..!!<br>"
                    End If
                End If
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close() '-- Tutup koneksi dari sql server --
            End If

            If iuAdd.Text = "new" Then
                dv.RowFilter = "snno='" & Tchar(snno.Text) & "' AND itemoid=" & itemoid.Text & ""
            Else
                dv.RowFilter = "sequence <> " & dtlseq.Text & " AND snno='" & Tchar(snno.Text) & "'"
            End If

            If dv.Count > 0 Then
                dv.RowFilter = ""
                nMsg &= "- Maaf, Data sudah ditambahkan " & txtNamaBarang.Text & " dengan type " & rblgaransi.SelectedValue & " Nomer SN '" & Tchar(snno.Text) & "' mohon input dengan nomer SN yang berbeda..!!<br>"
            End If
            dv.RowFilter = ""

            If nMsg <> "" Then
                showMessage(nMsg, CompnyName & "- Warning", 2)
                Exit Sub
            End If

            Dim drow As DataRow : Dim drowedit() As DataRow
            If iuAdd.Text = "new" Then
                drow = dtab.NewRow
                drow("sequence") = dtlseq.Text
                drow("itemoid") = itemoid.Text
                drow("itemdesc") = txtNamaBarang.Text
                drow("trnjualoid") = trnjualoid.Text
                drow("trnjualno") = trnjualno.Text
                drow("snno") = snno.Text
                drow("kelengkapan") = txtKelengkapan.Text
                drow("qty") = ToDouble(txtQty.Text)
                drow("garansi") = rblgaransi.SelectedValue
                drow("kerusakan") = txtDetailKerusakan.Text
                dtab.Rows.Add(drow)
                dtab.AcceptChanges()
            Else
                drowedit = dtab.Select("sequence = " & Integer.Parse(dtlseq.Text) & "", "")
                drow = drowedit(0)
                drowedit(0).BeginEdit()
                drow("itemoid") = itemoid.Text
                drow("itemdesc") = txtNamaBarang.Text
                drow("trnjualoid") = trnjualoid.Text
                drow("trnjualno") = trnjualno.Text
                drow("snno") = snno.Text
                drow("kelengkapan") = txtKelengkapan.Text
                drow("qty") = ToDouble(txtQty.Text)
                drow("garansi") = rblgaransi.SelectedValue
                drow("kerusakan") = txtDetailKerusakan.Text
                drowedit(0).EndEdit()
                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()
            End If

            gvNoKerusakan.DataSource = dtab
            gvNoKerusakan.DataBind()
            Session("gvNoKerusakan") = dtab
            dtlseq.Text = (gvNoKerusakan.Rows.Count + 1).ToString
            iuAdd.Text = "new" : ClearDetail()
            gvNoKerusakan.SelectedIndex = -1
            lblUpdNo.Text = gvNoKerusakan.Rows.Count
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False
        panelMsg.Visible = False
    End Sub

    Protected Sub gvNoKerusakan_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvNoKerusakan.RowDeleting
        Try
            Dim iIndex As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("gvNoKerusakan")
            objTable.Rows.RemoveAt(iIndex)
            'resequence
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("sequence") = C1 + 1
                dr.EndEdit()
            Next

            Session("gvNoKerusakan") = objTable
            gvNoKerusakan.DataSource = objTable
            gvNoKerusakan.DataBind()

            Session("sequence") = objTable.Rows.Count + 1
            If gvNoKerusakan.Rows.Count = 0 Then
                lblUpdNo.Text = "0"
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        txtDetailKerusakan.Text = ""
        ClearDetail()
    End Sub

    Protected Sub gvNoKerusakan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvNoKerusakan.SelectedIndexChanged
        Session("index") = gvNoKerusakan.SelectedIndex
        If Session("gvNoKerusakan") Is Nothing = False Then
            dtlseq.Text = gvNoKerusakan.SelectedDataKey("sequence").ToString().Trim
            iuAdd.Text = "update"
            lblUpdNo.Text = gvNoKerusakan.SelectedIndex.ToString
            Dim objTable As DataTable
            objTable = Session("gvNoKerusakan")
            Dim edRow() As DataRow = objTable.Select("sequence=" & dtlseq.Text)
            If edRow.Length > 0 Then
                itemoid.Text = edRow(0).Item("itemoid").ToString
                txtNamaBarang.Text = edRow(0).Item("itemdesc").ToString
                trnjualoid.Text = edRow(0).Item("trnjualoid").ToString
                trnjualno.Text = edRow(0).Item("trnjualno").ToString
                snno.Text = edRow(0).Item("snno").ToString
                txtKelengkapan.Text = edRow(0).Item("kelengkapan").ToString
                txtQty.Text = ToMaskEdit(ToDouble(edRow(0).Item("qty").ToString), 1)
                rblgaransi.SelectedValue = edRow(0).Item("garansi").ToString
                txtDetailKerusakan.Text = edRow(0).Item("kerusakan").ToString
            Else
                showMessage("tidak dapat load detail !!", CompnyName & "- Warning", 2)
            End If
            iuAdd.Text = "update"
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        panelCust.Visible = True
        btnHideCust.Visible = True : mpeCust.Show()
        Dim sWhere As String = " AND " & DDLFilterCust.SelectedValue & " LIKE '%" & Tchar(txtFilterCust.Text) & "%'"
        BindDataCust(sWhere)
    End Sub

    Protected Sub btnBarcode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBarcode.Click
        generateBarcode()
    End Sub

    Protected Sub txtDetailKerusakan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDetailKerusakan.TextChanged
        txtreqdate.Text = Format(GetServerTime(), "MM/dd/yyyy HH:mm:ss")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        Dim sColom() As String = {"MSTREQOID"}
        Dim sTable() As String = {"QL_TRNMECHCHECK"}

        If ClassFunction.CheckDataExists(Session("oid"), sColom, sTable) = True Then
            showMessage("tidak dapat hapus data, karena digunakan di tabel lain !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If
        'hapus tabel detail
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try

            strSQL = "delete from QL_TRNREQUESTDTL where reqmstoid='" & Session("oid") & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            'hapus tabel master
            strSQL = "delete from QL_TRNREQUEST where reqoid='" & Session("oid") & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            objCmd.Connection.Close()
            showMessage(ex.ToString) : Exit Sub
        End Try

        Response.Redirect("~\Transaction\PenerimaanBarang.aspx?awal=true")
    End Sub

    Protected Sub JenisDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles JenisDDL.SelectedIndexChanged
        txtBarcode.Text = ""
    End Sub

    Protected Sub MerkDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MerkDDL.SelectedIndexChanged
        txtBarcode.Text = ""
    End Sub

    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim status As String = gvr.Cells(5).Text.ToString
        If status = "In Process" Then
            showMessage("Nota penerimaan dengan status In Process belum bisa dicetak !", CompnyName & " - WARNING", 2)
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        PrintReport(sender.ToolTip, lbloid.Text)
    End Sub 

    Private Function GetSortDirection(ByVal column As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column
        Return sortDirection
    End Function

    Protected Sub gvPenerimaan_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPenerimaan.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvPenerimaan.DataSource = Session("TblMst")
            gvPenerimaan.DataBind()
        End If
    End Sub

    Protected Sub printmanualpenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles printmanualpenerimaan.Click
        ID = Session("oid")
        PrintReport(ID, txtNoTanda.Text)
    End Sub

    Protected Sub imbSeacrhSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSeacrhSI.Click
        If txtNamaCust.Text = "" Then
            showMessage("Isi Nama Customer Terlebih Dahulu !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If
 
        panelSI.Visible = True : btnHideSI.Visible = True
        mpeSI.Show() : BindDataSI("")
    End Sub

    Protected Sub gvListSI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSI.PageIndexChanging
        gvListSI.PageIndex = e.NewPageIndex
        panelSI.Visible = True : btnHideSI.Visible = True
        mpeSI.Show()
        Dim sWhere As String = " AND " & DDLFilterSI.SelectedValue & " LIKE '%" & Tchar(txtFilterSI.Text) & "%'"
        BindDataSI(sWhere)
        DDLFilterSI.SelectedIndex = 0
    End Sub

    Protected Sub gvListSI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSI.SelectedIndexChanged
        btnHideSI.Visible = False : panelSI.Visible = False
        mpeSI.Show()
        trnjualoid.Text = gvListSI.SelectedDataKey("trnjualmstoid").ToString.Trim
        trnjualno.Text = gvListSI.SelectedDataKey(1).ToString.Trim
        CProc.DisposeGridView(sender)
        txtFilterSI.Text = ""
    End Sub

    Protected Sub imbFindSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSI.Click
        panelSI.Visible = True
        btnHideSI.Visible = True
        mpeSI.Show()

        Dim sWhere As String = " AND " & DDLFilterSI.SelectedValue & " LIKE '%" & Tchar(txtFilterSI.Text) & "%'"
        BindDataSI(sWhere)
        DDLFilterSI.SelectedIndex = 0
    End Sub

    Protected Sub imbViewSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewSI.Click
        panelSI.Visible = True
        btnHideSI.Visible = True
        mpeSI.Show() : BindDataSI("")
        txtFilterSI.Text = "" : DDLFilterSI.SelectedIndex = 0
    End Sub

    Protected Sub imbClearSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearSI.Click
        If trnjualno.Text <> "None SI" Then
            txtNamaBarang.Text = "" : itemoid.Text = ""
        End If
        trnjualoid.Text = "" : trnjualno.Text = ""
    End Sub

    Protected Sub lkbCloseSI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseSI.Click
        panelSI.Visible = False : btnHideSI.Visible = False
        CProc.DisposeGridView(gvListSI)
    End Sub

    Protected Sub DdlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlCabang.SelectedIndexChanged
        'GenerateReqCode()
        If Page.IsPostBack Then
            InitDDLLocation() : InitMtrlOc()
        End If
        lblCust.Text = "" : txtNamaCust.Text = ""
        txtAlamat.Text = "" : txtHP.Text = ""
        cperson1.Text = "" : telp2.Text = ""
    End Sub

    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        If txtQty.Text = "" Then
            txtQty.Text = "0.0000"
        Else
            txtQty.Text = ToMaskEdit(txtQty.Text, 4)
        End If
    End Sub

    Protected Sub saldoawal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles saldoawal.SelectedIndexChanged
        'If saldoawal.SelectedValue = "M" Then
        '    TxtLoc.Visible = True
        '    Label21.Visible = True : MtrLocDDL.Visible = True
        'Else
        '    TxtLoc.Visible = False
        '    Label21.Visible = False : MtrLocDDL.Visible = False
        'End If
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        panelItem.Visible = True : btnHideItem.Visible = True
        CProc.DisposeGridView(gvListItem)
    End Sub

    Protected Sub lkbCloseCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCust.Click
        panelCust.Visible = False : btnHideCust.Visible = False
        CProc.DisposeGridView(gvListCust)
    End Sub

    Protected Sub BtnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendApp.Click
        txtStatus.Text = "IN APPROVAL" : btnSave_Click(sender, e)
    End Sub
#End Region
End Class