Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptTItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim cProc As New ClassProcedure
    Dim cKon As New Koneksi
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim sSql As String = ""
#End Region

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Private Sub showPrint(ByVal tipe As String)
        Try
            Dim sWhere As String = ""

            If ddltype.SelectedValue.ToString.ToLower = "create" Then
                vReport.Load(Server.MapPath("~\Report\rpttitemcreatenew.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rpttitemrelease.rpt"))
            End If


            sWhere = " where a.transformdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
            If ddltype.SelectedValue.ToString.ToLower = "create" Then
                sWhere &= " and a.transformtype='create'"
            Else
                sWhere &= " and a.transformtype='release'"
            End If

            If DDLbranch.SelectedValue.ToString.ToLower <> "all" Then
                sWhere &= " and a.branch_code='" & DDLbranch.SelectedValue & "'"
            End If

            If ddllocawal.SelectedValue.ToString.ToLower <> "all" Then
                sWhere &= " and b.locoid=" & ddllocawal.SelectedValue & ""
            End If

            If ddllocakhir.SelectedValue.ToString.ToLower <> "all" Then
                If ddltype.SelectedValue.ToString.ToLower = "create" Then
                    sWhere &= " and exists (select locoid from QL_TransformItemDtl where transformoid=a.transformoid and locoid=" & ddllocakhir.SelectedValue & ")"
                Else
                    sWhere &= " and exists (select locoid from QL_TransformItemDtlMst where transformoid=a.transformoid and locoid=" & ddllocakhir.SelectedValue & ")"
                End If
            End If

            If barang.Text.Trim <> "" Then
                sWhere &= " and exists (select h.itemdesc from QL_TransformItemDtl g inner join QL_mstItem h on g.itemoid=h.itemoid where g.transformoid=a.transformoid and h.itemdesc like '%" & Tchar(barang.Text) & "%' and h.Merk like '%" & Tchar(merk.Text) & "%' union all select h.itemdesc from QL_TransformItemDtlMst g inner join QL_mstItem h on g.itemoid=h.itemoid where g.transformoid=a.transformoid and h.itemdesc like '%" & Tchar(barang.Text) & "%' and h.Merk like '%" & Tchar(merk.Text) & "%')"
            End If

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("periode", range1.Text & " - " & range2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "" Then
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TTS_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TTS_" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            vReport.Close()
            vReport.Dispose()
            showMessage(ex.ToString, CompnyName, 1)
        End Try
    End Sub

    Sub dllfile()
        If Session("branch_id") = "10" Then
            sSql = "SELECT a.gencode, a.gencode +' - '+ a.gendesc FROM QL_mstgen a WHERE a.gengroup = 'cabang' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gencode"
            FillDDL(DDLbranch, sSql)
            DDLbranch.Items.Add("All")
            DDLbranch.Items(DDLbranch.Items.Count - 1).Value = "All"
            DDLbranch.SelectedValue = "All"

            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc"
            FillDDL(ddllocawal, sSql)
            ddllocawal.Items.Add("All")
            ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
            ddllocawal.SelectedValue = "All"

            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc"
            FillDDL(ddllocakhir, sSql)
            ddllocakhir.Items.Add("All")
            ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
            ddllocakhir.SelectedValue = "All"
        Else
            sSql = "SELECT a.gencode, a.gencode +' - '+ a.gendesc FROM QL_mstgen a WHERE a.gengroup = 'cabang' AND a.cmpcode='" & CompnyCode & "' and a.gencode = '" & Session("branch_id") & "' ORDER BY a.gencode"
            FillDDL(DDLbranch, sSql)

            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & Session("branch_id") & "'"

            FillDDL(ddllocawal, sSql)
            ddllocawal.Items.Add("All")
            ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
            ddllocawal.SelectedValue = "All"

            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & Session("branch_id") & "'"
            FillDDL(ddllocakhir, sSql)
            ddllocakhir.Items.Add("All")
            ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
            ddllocakhir.SelectedValue = "All"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Session("branch_id") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptTItem.aspx")
        End If

        Page.Title = CompnyCode & " - Laporan Transformasi Item"
        Label2.Text = ""

        If Not Page.IsPostBack Then
            dllfile()

            range1.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
            range2.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        PanelErrMsg.Visible = False
        btnExtender.Visible = False
        MPEErrMsg.Hide()
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowprint.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty..!!<br />"
            Exit Sub
        End If

        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty..!!<br />"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid..!!<br />"
        End If

        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid..!!<br />"
            Exit Sub
        End If

        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period..!!<br />"
        End If

        If Label2.Text <> "" Then
            showMessage(Label2.Text, CompnyName, 2)
            Exit Sub
        End If
        showPrint("")
    End Sub

    Protected Sub btnpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty..!!<br />"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty..!!<br />"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid..!!<br />"
        End If

        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid..!!<br />"
            Exit Sub
        End If

        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period..!!<br />"
        End If

        If Label2.Text <> "" Then
            showMessage(Label2.Text, CompnyName, 2)
            Exit Sub
        End If

        showPrint("pdf")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        If range1.Text.Trim = "" Then
            Label2.Text = "First period cannot empty..!!<br />"
            Exit Sub
        End If
        If range2.Text.Trim = "" Then
            Label2.Text = "Second period cannot empty..!!<br />"
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(range1.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            Label2.Text = "Date format for first period is invalid..!!<br />"
        End If

        If Date.TryParseExact(range2.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            Label2.Text = "Date format for second period is invalid..!!<br />"
            Exit Sub
        End If

        If date2 < date1 Then
            Label2.Text = "Second period cannot less than first period..!!<br />"
        End If

        If Label2.Text <> "" Then
            showMessage(Label2.Text, CompnyName, 2)
            Exit Sub
        End If

        showPrint("excel")
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Response.Redirect("rptTItem.aspx?awal=true")
    End Sub

    Protected Sub barangsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangsearch.Click
        sSql = "select itemoid,itemcode,itemdesc,Merk from QL_mstitem where itemflag='Aktif' and cmpcode='" & CompnyCode & "' and (itemcode like '%" & Tchar(barang.Text) & "%' or itemdesc like '%" & Tchar(barang.Text) & "%' or Merk like '%" & Tchar(barang.Text) & "%')"
        Dim dtab As DataTable = CKon.ambiltabel(sSql, "GVBarang")
        GVBarang.DataSource = dtab
        GVBarang.DataBind()
        GVBarang.Visible = True
        Session("GVBarang") = dtab
    End Sub

    Protected Sub barangerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles barangerase.Click
        barang.Text = ""
        barangoid.Text = ""
        merk.Text = ""

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
    End Sub

    Protected Sub GVBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVBarang.PageIndexChanging
        If Not Session("GVBarang") Is Nothing Then
            GVBarang.DataSource = Session("GVBarang")
            GVBarang.PageIndex = e.NewPageIndex
            GVBarang.DataBind()
        End If
    End Sub

    Protected Sub GVBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVBarang.SelectedIndexChanged
        barang.Text = GVBarang.SelectedDataKey("itemdesc")
        barangoid.Text = GVBarang.SelectedDataKey("itemoid")
        merk.Text = GVBarang.SelectedDataKey("Merk")

        GVBarang.DataSource = Nothing
        GVBarang.DataBind()
        GVBarang.Visible = False
        Session("GVBarang") = Nothing
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub DDLbranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLbranch.SelectedIndexChanged
        If Page.IsPostBack Then
            If Session("branch_id") = "10" Then
                If DDLbranch.SelectedValue = "All" Then
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 "
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                Else
                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                    FillDDL(ddllocawal, sSql)
                    ddllocawal.Items.Add("All")
                    ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                    ddllocawal.SelectedValue = "All"

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                End If
            Else
                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"

                FillDDL(ddllocawal, sSql)
                ddllocawal.Items.Add("All")
                ddllocawal.Items(ddllocawal.Items.Count - 1).Value = "All"
                ddllocawal.SelectedValue = "All"

                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                FillDDL(ddllocakhir, sSql)
                ddllocakhir.Items.Add("All")
                ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                ddllocakhir.SelectedValue = "All"
            End If

        End If
    End Sub

    Protected Sub ddllocawal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllocawal.SelectedIndexChanged
        If Page.IsPostBack Then
            If Session("branch_id") = "01" Then
                If DDLbranch.SelectedValue = "All" Then
                        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2"
                        FillDDL(ddllocakhir, sSql)
                        ddllocakhir.Items.Add("All")
                        ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                        ddllocakhir.SelectedValue = "All"
                    Else

                    sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                    FillDDL(ddllocakhir, sSql)
                    ddllocakhir.Items.Add("All")
                    ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                    ddllocakhir.SelectedValue = "All"
                End If
            Else

                sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'INNER JOIN QL_mstgen c ON c.genoid = a.genother2 WHERE c.gencode = '" & DDLbranch.SelectedValue & "'"
                FillDDL(ddllocakhir, sSql)
                ddllocakhir.Items.Add("All")
                ddllocakhir.Items(ddllocakhir.Items.Count - 1).Value = "All"
                ddllocakhir.SelectedValue = "All"

            End If
           
        End If
    End Sub
End Class
