Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing


Partial Class Transaction_trnSalesReturnTB
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
    Dim crate As ClassRate
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub bindalldata(ByVal swhere As String)
        sSql = "select a.trnjualreturmstoid, a.trnjualreturno, a.trnjualdate,  b.custname, a.trnjualstatus from QL_trnjualreturTBmst a inner join ql_mstcust b on a.cmpcode = b.cmpcode and a.trncustoid = b.custoid  where a.branch_code='" & Session("branch_id") & "' and  a.cmpcode = '" & CompnyCode & "'" & swhere & " order by a.trnjualreturno"
        ViewState("salesretur") = Nothing
        ViewState("salesretur") = cKon.ambiltabel(sSql, "salesretur")
        GVTrn.DataSource = ViewState("salesretur")
        GVTrn.DataBind()
    End Sub

    Private Sub initallddl()
        FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid  AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode = '" & CompnyCode & "'  where a.genoid like '%" & itemloc.Text & "%' ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        fromlocation.SelectedIndex = 0
    End Sub

    Private Sub printinvoiceservices(ByVal name As String)
        'Try
        '    rpt = New ReportDocument
        '    rpt.Load(Server.MapPath("~\Report\Invoiceservices.rpt"))
        '    'cProc.SetDBLogonForReport(rpt)
        '    rpt.SetParameterValue("sWhere", " AND reqoid = " & Session("oid") & "")
        '    Response.Buffer = False
        '    Response.ClearHeaders()
        '    Response.ClearContent()
        '    rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INVSVC_" & name)
        '    rpt.Close() : rpt.Dispose()
        'Catch ex As Exception
        '    showMessage(ex.ToString, 2)
        'End Try
    End Sub

    Private Sub filltextbox(ByVal id As Integer)
        Dim oid As Integer = 0
        If Integer.TryParse(id, oid) = False Then
            Response.Redirect("trnSalesReturnTB.aspx?awal=true")
        Else
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "select (select trnjualmstoid from QL_trnjualmst where trnjualno = a.orderno)trnjualmstoid,a.orderno, a.trnjualreturno, a.refSJjualno, a.refnotaretur, a.trnjualdate, a.trncustoid, a.trncustname, a.trnpaytype,a.typeretur, a.trnjualref, a.trnamtjual, a.trntaxpct, a.trnamttax, a.trnamtjualnetto, a.trnjualnote, a.trnjualstatus,b.itemloc, (select trnjualdate from QL_trnjualmst where trnjualno = a.orderno and cmpcode = a.cmpcode) jualdate,a.currencyoid, a.upduser, a.updtime from QL_trnjualreturTBmst a inner join QL_trnjualreturTBdtl b on a.branch_code=b.branch_code and a.trnjualreturmstoid=b.trnjualreturmstoid  where a.trnjualreturmstoid = " & oid & " and a.cmpcode = '" & CompnyCode & "' and a.branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    noreturn.Text = xreader("trnjualreturno")
                    returndate.Text = Format(xreader("trnjualdate"), "dd/MM/yyyy")
                    paytype.Text = xreader("trnpaytype")
                    fromlocation.SelectedValue = xreader("itemloc")
                    trdate.Text = Format(xreader("jualdate"), "dd/MM/yyyy")
                    invoiceno.Text = xreader("refnotaretur")
                    sjno.Text = xreader("refSJjualno")
                    sono.Text = xreader("orderno")
                    sino.Text = xreader("trnjualmstoid")
                    lblcurr.Text = xreader("currencyoid")
                    DDLtype.SelectedValue = xreader("typeretur")
                    If lblcurr.Text = "1" Then
                        txtcurr.Text = "Rupiah"
                    Else
                        txtcurr.Text = "D0llar"
                    End If
                    custname.Text = xreader("trncustname")
                    custoid.Text = xreader("trncustoid")
                    noref.Text = xreader("trnjualref")
                    grossreturn.Text = Format(xreader("trnamtjual"), "#,##0.00")
                    taxpct.Text = Format(xreader("trntaxpct"), "#,##0.00")
                    taxamount.Text = xreader("trnamttax")
                    totalamount.Text = Format(xreader("trnamtjualnetto"), "#,##0.00")
                    status.Text = xreader("trnjualstatus")
                    note.Text = xreader("trnjualnote")
                    upduser.Text = xreader("upduser")
                    updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt")
                End While

            End If
            xreader.Close()
            conn.Close()
            sSql = "select a.itemoid,i.itemcode,i.itemdesc,i.Merk,a.trnjualdtlqty,(select gendesc from QL_mstgen where genoid = a.trnjualdtlunitoid) unit from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_MSTITEM i on i.itemoid = a.itemoid where b.trnjualreturno = '" & invoiceno.Text & "' and a.branch_code = '" & Session("branch_id") & "' "

            Session("returitem") = cKon.ambiltabel(sSql, "returitem")
            GVretur.DataSource = Session("returitem")
            GVretur.DataBind()
            'FillGV(GVretur, sSql, "returitem")
            GVretur.Visible = True


            sSql = "select a.trnjualdtlseq seq, c.itemcode, c.itemdesc itemname, a.trnjualdtlqty qty, d.gendesc unit, a.trnjualdtlprice price, a.trnjualnote note, c.itemoid, a.amtjualnetto netamt, a.trnjualdtlunitoid unitoid, a.unitSeq unitseq, a.trnjualdtloid,a.trnjualdtlqty maxqty, (select y.gendesc + ' - ' + x.gendesc from ql_mstgen x inner join ql_mstgen y on x.cmpcode = y.cmpcode and x.genother1 = y.genoid and x.gengroup = 'location' and y.gengroup = 'warehouse' and x.cmpcode = a.cmpcode and x.genoid = a.itemloc) location, a.itemloc locationoid, a.trnjualdtldiscqty promodisc, a.amtjualdisc promodiscamt from QL_trnjualreturTBdtl a inner join QL_trnjualreturTBmst b on a.cmpcode = b.cmpcode and a.trnjualreturmstoid = b.trnjualreturmstoid and  a.branch_code = b.branch_code inner join QL_mstItem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.trnjualdtlunitoid = d.genoid where a.cmpcode = '" & CompnyCode & "' and a.trnjualreturmstoid = " & oid & " and b.branch_code='" & Session("branch_id") & "'"
            ViewState("dtlsalesreturn") = cKon.ambiltabel(sSql, "dtlsalesreturn")
            GVDtl.DataSource = ViewState("dtlsalesreturn")
            GVDtl.DataBind()

            calculateheader()




            If status.Text = "POST" Then
                ibcancel.Visible = True
                ibsave.Visible = False
                ibdelete.Visible = False
                ibapproval.Visible = False
                ibposting.Visible = False
                btnPrint.Visible = False
            Else
                ibcancel.Visible = True
                ibsave.Visible = True
                ibdelete.Visible = True
                ibapproval.Visible = False
                ibposting.Visible = True
                btnPrint.Visible = False
            End If
        End If
    End Sub

    Private Sub inittax()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "select genother1 from ql_mstgen where gengroup = 'tax' and cmpcode = '" & CompnyCode & "'"
        xCmd.CommandText = sSql
        taxpct.Text = ToDouble(xCmd.ExecuteScalar)

        conn.Close()
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lbldate.Text = Format(Now, "yyyy-MM-dd")
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        'Session.Timeout = 60

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("Role") = xsetRole
            Session("ApprovalLimit") = appLimit

            Response.Redirect("~\Transaction\trnSalesReturnTB.aspx")
        End If

        Page.Title = CompnyName & " - Sales Return"
        Session("oid") = Request.QueryString("oid")

        ibapproval.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin mengirimkan permintaan approval pada data ini?');")
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")
        ibposting.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin melakukan posting data ini?');")

        If Not Page.IsPostBack Then
            tbperiodstart.Text = Format(New Date(Date.Now.Year, Date.Now.Month, 1), "dd/MM/yyyy")
            tbperiodend.Text = Format(Date.Now, "dd/MM/yyyy")
            noreturn.Text = GenerateID("QL_trnjualreturTBmst", CompnyCode)
            initallddl()
            bindalldata(" and convert(date,a.trnjualdate,103) between '" & New Date(Date.Now.Year, Date.Now.Month, 1) & "' and '" & Date.Now & "'")

            If Session("oid") = "" Or Session("oid") Is Nothing Then
                TabContainer1.ActiveTabIndex = 0
                MultiView1.ActiveViewIndex = 0
                masterstate.Text = "new"
                returndate.Text = Format(Date.Now, "dd/MM/yyyy")
                upduser.Text = Session("UserID")
                updtime.Text = Format(Date.Now, "dd/MM/yyyy hh:mm:ss tt")
                lblcreate.Text = "Create On"

            Else
                TabContainer1.ActiveTabIndex = 1
                masterstate.Text = "edit"
                filltextbox(Session("oid"))
                MultiView1.ActiveViewIndex = 0
                lblcreate.Text = "Last Update By"
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("trnSalesReturnTB.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        Dim date1, date2 As New Date
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            showMessage("All period cannot empty!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("First period is not valid!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("second period is not valid!", 2)
            Exit Sub
        End If
        If CDate(toDate(tbperiodstart.Text)) > CDate(toDate(tbperiodend.Text)) Then
            showMessage("Last periode must be greather than First Periode!", 2)
            Exit Sub
        End If

        bindalldata(" and " & ddlfilter.SelectedValue.ToString & " like '%" & Tchar(tbfilter.Text) & "%' and convert(date,a.trnjualdate,103) between '" & date1 & "' and '" & date2 & "'" & IIf(ddlfilterstatus.SelectedValue.ToString <> "All", " and a.trnjualstatus = '" & ddlfilterstatus.SelectedValue.ToString & "'", "") & "")
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        tbperiodstart.Text = Format(New Date(Date.Now.Year, Date.Now.Month, 1), "dd/MM/yyyy")
        tbperiodend.Text = Format(Date.Now, "dd/MM/yyyy")

        Dim date1, date2 As New Date
        If tbperiodstart.Text.Trim = "" Or tbperiodend.Text.Trim = "" Then
            showMessage("All period cannot empty!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodstart.Text, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("First period is not valid!", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tbperiodend.Text, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Second period is not valid!", 2)
            Exit Sub
        End If

        bindalldata(" and convert(date,a.trnjualdate,103) between '" & date1 & "' and '" & date2 & "'")
        ddlfilter.SelectedIndex = 0
        tbfilter.Text = ""
        tbperiodstart.Text = Format(New Date(Now.Year, Now.Month, 1), "dd/MM/yyyy")
        tbperiodend.Text = Format(Date.Now, "dd/MM/yyyy")
        ddlfilterstatus.SelectedIndex = 0
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click

        If status.Text = "In Process" Then
            Dim cek_count As String = GetStrData("select count (0) from (select a.itemoid,i.itemcode,i.itemdesc,i.Merk,a.trnjualdtlqty,(select gendesc from QL_mstgen where genoid = a.trnjualdtlunitoid) unit from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_MSTITEM i on i.itemoid = a.itemoid where b.trnjualreturno = '" & invoiceno.Text & "' and a.branch_code = '" & Session("branch_id") & "' and b.statusTB=0) dt")

            Dim dtab As DataTable = ViewState("dtlsalesreturn")
            Dim count As Int16 = dtab.Rows.Count
            If cek_count <> count Then
                showMessage("Jumlah item yang di tukar harus sama dengan yang di retur!", 2)
                Exit Sub
            End If
        End If

        Dim hppne As Decimal = 0.0


        If grossreturn.Text = "" Then
            showMessage("Retur amount cannot empty!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        If ToDouble(grossreturn.Text) = 0.0 Then
            showMessage("Retur amount cannot 0!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        If returndate.Text = "" Then
            showMessage("Retur date cannot empty!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        Dim returdate As New Date
        If Date.TryParseExact(returndate.Text, "dd/MM/yyyy", Nothing, Nothing, returdate) = False Then
            showMessage("Retur date is not valid!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        If returdate < New Date(1900, 1, 1) Then
            showMessage("Retur date cannot less than '1/1/1900'!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        If returdate > New Date(2079, 6, 6) Then
            showMessage("Retur date cannot more than '6/6/2079'!", 2)
            status.Text = "In Process"
            Exit Sub
        End If
        If GVDtl.Rows.Count <= 0 Then
            showMessage("Item detail in sales retur cannot empty!", 2)
            status.Text = "In Process"
            Exit Sub
        End If

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(lblcurr.Text), toDate(trdate.Text))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, 2)
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, 2)
            Exit Sub
        End If

        Try
            Dim period As String = GetDateToPeriodAcctg(returdate).Trim
            Dim alastpaydate As New Date
            sSql = "select genoid from ql_mstgen where gengroup = 'PAYTYPE' and gendesc = 'cash' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = Integer.Parse(paytype.Text) Then
                alastpaydate = returdate
            Else
                alastpaydate = CDate("1/1/1900")
            End If

            sSql = "select lastoid from ql_mstoid where tablename = 'QL_trnjualreturTBmst' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            Dim returmstoid As Integer = xCmd.ExecuteScalar + 1

            sSql = "select lastoid from ql_mstoid where tablename = 'QL_HistHPP' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim oidTemp As Int64 = xCmd.ExecuteScalar

            sSql = "select lastoid from ql_mstoid where tablename = 'QL_trnjualreturTBdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            Dim returdtloid As Integer = xCmd.ExecuteScalar

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_TRNGLMST' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim glmstoid As Int32 = xCmd.ExecuteScalar + 1

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_TRNGLDTL' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim gldtloid As Int32 = xCmd.ExecuteScalar
            If status.Text = "POST" Then
                Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & Session("branch_id") & "'")
                Dim retno As String = "SRTB/" & Format(returdate, "yy") & "/" & Format(returdate, "MM") & "/" & cabang & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualreturno,4) AS INT)),0) FROM QL_trnjualreturTBmst WHERE trnjualreturno LIKE '" & retno & "%'"
                xCmd.CommandText = sSql
                retno = retno & Format(xCmd.ExecuteScalar + 1, "0000")
                noreturn.Text = retno
            Else
                If masterstate.Text = "new" Then
                    noreturn.Text = (returmstoid).ToString
                End If
            End If

            If masterstate.Text = "new" Then
                If Not status.Text = "POST" Then
                    noreturn.Text = (returmstoid).ToString
                End If

                sSql = "insert into QL_trnjualreturTBmst (cmpcode,branch_code, trnjualreturmstoid, orderno, periodacctg, trnjualtype, trnjualreturno, refSJjualno, refnotaretur, trnjualdate, trnjualref, trncustoid, trncustname, trnpaytype, trnamtjual,trnamtjualidr,trnamtjualusd, trntaxpct, trnamttax,trnamttaxidr,trnamttaxusd, trndisctype, trndiscamt, trnamtjualnetto, trnamtjualnettoidr, trnamtjualnettousd, trnjualnote, trnjualstatus, trnjualres1, amtdischdr, amtdiscdtl, accumpayment, lastpaymentdate, userpay, createuser, upduser, updtime, refNotaRevisi, salesoid, trnorderstatus, ekspedisioid, amtdischdr2, trndisctype2, trndiscamt2, postacctg, amtretur_piutang, amtpayretur_piutang, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote,currencyoid,typeretur) values ('" & CompnyCode & "','" & Session("branch_id") & "', " & returmstoid & ", '" & sono.Text & "', '" & GetDateToPeriodAcctg(returdate) & "', 'GROSIR', '" & noreturn.Text & "', '" & sjno.Text & "', '" & invoiceno.Text & "', '" & returdate & "', '" & Tchar(noref.Text) & "', " & Integer.Parse(custoid.Text) & ", '" & custname.Text & "', " & Integer.Parse(paytype.Text) & ", " & ToDouble(grossreturn.Text) & ", " & ToDouble(grossreturn.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(grossreturn.Text) * cRate.GetRateMonthlyUSDValue & ", " & ToDouble(taxpct.Text) & ", " & ToDouble(taxamount.Text) & ", " & ToDouble(taxamount.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(taxamount.Text) * cRate.GetRateMonthlyUSDValue & ", 'AMT', 0.0, " & ToDouble(totalamount.Text) & ", " & ToDouble(totalamount.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(totalamount.Text) * cRate.GetRateMonthlyUSDValue & ", '" & Tchar(note.Text) & "', '" & status.Text & "', '', 0.0, 0.0, 0.0, '" & alastpaydate & "', 0.0, '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, isnull((select trnorderstatus from QL_trnordermst where orderno = '" & sono.Text & "' and cmpcode = '" & CompnyCode & "'), ''), 0, 0.0, 'AMT', 0.0, '', 0.0, 0.0, '1/1/1900', '', '', '', '1/1/1900', '','" & lblcurr.Text & "','" & DDLtype.SelectedValue & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & returmstoid & " where tablename = 'QL_trnjualreturTBmst' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_trnjualreturmst set statusTB = 1 where trnjualreturno = '" & invoiceno.Text & "' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                '@@@@@@@@@@@@@@@@@@@@ Update SN Return saat Save @@@@@@@@@@@@@@@@
                If Session("TblSN") Is Nothing Then

                Else
                    Dim objTableSN As DataTable
                    objTableSN = Session("TblSN")
                    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                        Dim itemcodesn As String = cKon.ambilscalar(sSql)
                        sSql = "update QL_Mst_SN set trnreturjualmstoid='" & returmstoid & "',status_in_out='ReturJualTukarBarang',tglreturjual='" & toDate(returndate.Text) & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "' and (trnreturjualmstoid  = '" & invoiceno.Text & "' or trnreturjualmstoid is null)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                End If
            Else
                returmstoid = Session("oid")

                sSql = "select trnjualstatus from QL_trnjualreturTBmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                Dim stat As String = xCmd.ExecuteScalar
                If stat.ToLower = "post" Then
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    showMessage("Sales return been posted!<br />It can't be modified!", 2)
                    status.Text = "In Process"
                    Exit Sub
                ElseIf stat Is Nothing Or stat = "" Then
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    showMessage("Sales return data can't be found!", 2)
                    status.Text = "In Process"
                    Exit Sub
                End If

                sSql = "update QL_trnjualreturTBmst set orderno = '" & sono.Text & "', periodacctg = '" & GetDateToPeriodAcctg(returdate) & "', trnjualreturno = '" & noreturn.Text & "', refSJjualno = '" & sjno.Text & "', refnotaretur = '" & invoiceno.Text & "', trnjualdate = '" & returdate & "', trnjualref = '" & Tchar(noref.Text) & "', trncustoid = " & Integer.Parse(custoid.Text) & ", trncustname = '" & custname.Text & "', trnpaytype = " & Integer.Parse(paytype.Text) & ", trnamtjual = " & ToDouble(grossreturn.Text) & ",trnamtjualidr = " & ToDouble(grossreturn.Text) * cRate.GetRateMonthlyIDRValue & ",trnamtjualusd = " & ToDouble(grossreturn.Text) * cRate.GetRateMonthlyUSDValue & ", trntaxpct = " & ToDouble(taxpct.Text) & ", trnamttax = " & ToDouble(taxamount.Text) & ", trnamttaxidr = " & ToDouble(taxamount.Text) * cRate.GetRateMonthlyIDRValue & ", trnamttaxusd = " & ToDouble(taxamount.Text) * cRate.GetRateMonthlyUSDValue & ", trnamtjualnetto = " & ToDouble(totalamount.Text) & ", trnamtjualnettoidr = " & ToDouble(totalamount.Text) * cRate.GetRateMonthlyIDRValue & ", trnamtjualnettousd = " & ToDouble(totalamount.Text) * cRate.GetRateMonthlyUSDValue & ", trnjualnote = '" & Tchar(note.Text) & "', trnjualstatus = '" & status.Text & "', lastpaymentdate = '" & alastpaydate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP,currencyoid='" & lblcurr.Text & "',typeretur='" & DDLtype.SelectedValue & "' where trnjualreturmstoid = " & returmstoid & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "delete from QL_trnjualreturTBdtl where trnjualreturmstoid = " & Session("oid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Session("TblSN") Is Nothing Then
                    '@@@@@@@@@@@@@@@@@@@@ Update SN Return saat update @@@@@@@@@@@@@@@@
                    sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturjualmstoid = '" & noreturn.Text & "'"
                    Session("TblSN") = cKon.ambiltabel(sSql, "TbSN")
                    Dim objTableSN As DataTable
                    objTableSN = Session("TblSN")
                    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                        Dim itemcodesn As String = cKon.ambilscalar(sSql)
                        sSql = "update QL_Mst_SN set trnreturjualmstoid='" & returmstoid & "',status_in_out='ReturJual',tglreturjual='" & toDate(returndate.Text) & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "' and (trnreturjualmstoid  = '" & invoiceno.Text & "' or or trnreturjualmstoid is null)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                Else
                    Dim objTableSN As DataTable
                    objTableSN = Session("TblSN")
                    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                        Dim itemcodesn As String = cKon.ambilscalar(sSql)
                        sSql = "update QL_Mst_SN set trnreturjualmstoid='" & returmstoid & "',status_in_out='ReturJual',tglreturjual='" & toDate(returndate.Text) & "' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "' and (trnreturjualmstoid = '" & invoiceno.Text & "' or trnreturjualmstoid is null )"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                End If
            End If

            Dim qty_to As Double = 0.0

            If Not ViewState("dtlsalesreturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    returdtloid = returdtloid + 1
                    sSql = "insert into QL_trnjualreturTBdtl (cmpcode,branch_code,trnjualreturdtloid, trnjualreturmstoid, trnjualdtlseq, itemloc, itemoid, trnjualdtlqty, trnjualdtlunitoid, unitSeq, trnjualdtlprice, trnjualdtlpriceidr, trnjualdtlpriceusd, trnjualdtlpriceunitoid, trnjualdtldisctype, trnjualdtldiscqty, trnjualflag, trnjualnote, trnjualstatus, amtjualdisc, amtjualnetto,amtjualnettoidr,amtjualnettousd, createuser, upduser, updtime, trnjualdtldisctype2, trnjualdtldiscqty2, amtjualdisc2, trnjualdtloid) values ('" & CompnyCode & "','" & Session("branch_id") & "', " & returdtloid & ", " & returmstoid & ", " & dtab.Rows(i).Item("seq") & ", " & fromlocation.SelectedValue & ", " & dtab.Rows(i).Item("itemoid") & ", " & dtab.Rows(i).Item("qty") & ", " & dtab.Rows(i).Item("unitoid") & ", " & dtab.Rows(i).Item("unitseq") & ", " & dtab.Rows(i).Item("price") & ", " & dtab.Rows(i).Item("price") * cRate.GetRateMonthlyIDRValue & ", " & dtab.Rows(i).Item("price") * cRate.GetRateMonthlyUSDValue & ", " & dtab.Rows(i).Item("price") & ", 'AMT', " & dtab.Rows(i).Item("promodisc") & ", '" & status.Text & "', '" & Tchar(dtab.Rows(i).Item("note")) & "', '" & status.Text & "', " & dtab.Rows(i).Item("promodiscamt") & ", " & dtab.Rows(i).Item("netamt") & ", " & dtab.Rows(i).Item("netamt") * cRate.GetRateMonthlyIDRValue & ", " & dtab.Rows(i).Item("netamt") * cRate.GetRateMonthlyUSDValue & ", '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'AMT', 0, 0, " & dtab.Rows(i).Item("trnjualdtloid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = trnjualdtlqty_retur + " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 = trnjualdtlqty_retur_unit3  + (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()

                    If status.Text = "POST" Then
                        'set HPP
                        Dim HPPold As Double = 0.0
                        Dim HPPnew As Double = 0.0 : Dim sisastock As Double = 0.0

                        sSql = "select isnull(HPP,0) FROM QL_mstitem where cmpcode='" & CompnyCode & "' and itemoid='" & dtab.Rows(i)("itemoid") & "'"
                        xCmd.CommandText = sSql
                        HPPold = xCmd.ExecuteScalar

                        'Dim dHargaNetto_qty As Double = 0
                        'sSql = "select isnull((select poamtdtlnetto/podtlqty  from ql_podtl where podtloid=" & objTable.Rows(C1).Item("trnpodtloid") & "),0) data"
                        'xCmd.CommandText = sSql
                        'dHargaNetto_qty = xCmd.ExecuteScalar

                        Dim qtyterkonversi As Double = 0
                        If dtab.Rows(i)("unitseq") = 1 Then
                            sSql = "select konversi1_2 * konversi2_3 from ql_mstitem where itemoid =" & dtab.Rows(i)("itemoid") & ""
                            xCmd.CommandText = sSql
                            qtyterkonversi = dtab.Rows(i)("qty") * xCmd.ExecuteScalar
                        ElseIf dtab.Rows(i)("unitseq") = 2 Then
                            sSql = "select konversi2_3 from ql_mstitem where itemoid =" & dtab.Rows(i)("itemoid") & ""
                            xCmd.CommandText = sSql
                            qtyterkonversi = dtab.Rows(i)("qty") * xCmd.ExecuteScalar
                        Else
                            qtyterkonversi = dtab.Rows(i)("qty")
                        End If

                        sSql = "select (sum(mtr.saldoAwal)+sum(mtr.qtyin + mtr.qtyAdjIn)-sum(mtr.qtyout + mtr.qtyAdjOut)) stock from QL_crdmtr mtr left join QL_mstgen g on mtr.mtrlocoid=g.genoid inner join QL_mstitem i on i.itemoid= mtr.refoid where mtr.refname='QL_MSTITEM' and mtr.refoid=" & dtab.Rows(i)("itemoid") & " and crdmatoid IN (select top 1 crdmatoid from QL_crdmtr c2 where c2.refoid=mtr.refoid and c2.refname=mtr.refname and c2.mtrlocoid=mtr.mtrlocoid and c2.branch_code='" & Session("branch_id") & "' order by RIGHT(periodacctg,4)+''+LEFT(periodacctg,2) desc) group by mtr.cmpcode, mtr.mtrlocoid, mtr.refoid, g.gendesc, mtr.refname"
                        xCmd.CommandText = sSql
                        sisastock = xCmd.ExecuteScalar
                        Dim rateidr As String = cRate.GetRateMonthlyIDRValue
                        Dim rateusd As String = cRate.GetRateMonthlyUSDValue
                        If lblcurr.Text = "1" Then
                            HPPnew = ((sisastock * HPPold) + ((dtab.Rows(i)("qty") * ((dtab.Rows(i).Item("price") * rateidr) - (dtab.Rows(i).Item("promodisc") * rateusd))))) / (sisastock + qtyterkonversi)
                        Else
                            HPPnew = ((sisastock * HPPold) + ((dtab.Rows(i)("qty") * ((dtab.Rows(i).Item("price") * rateidr) - (dtab.Rows(i).Item("promodisc") * rateidr))))) / (sisastock + qtyterkonversi)

                        End If

                        hppne = hppne + HPPnew * qtyterkonversi
                        sSql = "update QL_mstitem set HPP=" & HPPnew & " where cmpcode='" & CompnyCode & "' and itemoid=" & dtab.Rows(i).Item("itemoid").ToString
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        oidTemp = oidTemp + 1
                        sSql = "INSERT INTO QL_HistHPP(cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem) values('" & CompnyCode & "'," & oidTemp & ", " & returdtloid & ",'QL_trnjualreturTBdtl','QL_MSTITEM'," & dtab.Rows(i).Item("itemoid") & ",0," & ToDouble(HPPold) & "," & ToDouble(HPPnew) & "," & ToDouble(sisastock) & "," & ToDouble(dtab.Rows(i)("qty")) & "," & ToDouble(((dtab.Rows(i)("qty") * (dtab.Rows(i).Item("price") - dtab.Rows(i).Item("promodisc")))) * 1) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        'end set HPP

                        'start update MST_SN  ketika POSTING 
                        sSql = "select  itemcode,id_sn from ql_mst_SN where trnreturjualmstoid ='" & returmstoid & "' "
                        Dim objTableSN As DataTable : objTableSN = cKon.ambiltabel(sSql, "SN")

                        For SN As Integer = 0 To objTableSN.Rows.Count - 1

                            sSql = "update QL_Mst_SN set trnreturjualmstoid='" & noreturn.Text & "',status_in_out='OUT',tglreturjual='" & toDate(returndate.Text) & "',status_approval='POSTING',last_trans_type = 'Retur Jual TB' where id_sn ='" & objTableSN.Rows(SN).Item("id_sn") & "' and itemcode ='" & objTableSN.Rows(SN).Item("itemcode") & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "insert into ql_trans_sn (itemcode,id_sn,id_trans,trans_type)values('" & objTableSN.Rows(SN).Item("itemcode") & "','" & objTableSN.Rows(SN).Item("id_sn") & "','" & noreturn.Text & "','OUT')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "update ql_mstitemdtl set branch_code='" & Session("branch_id") & "',status_in_out='OUT',locoid='" & fromlocation.SelectedValue & "',createtime= CURRENT_TIMESTAMP,createuser='" & Session("UserID") & "',status_item='POSTING',last_trans_type='Retur Jual TB' where sn='" & objTableSN.Rows(SN).Item("id_sn") & "' and itemcode='" & objTableSN.Rows(SN).Item("itemcode") & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        Next
                        'end update MST_SN  ketika POSTING  

                        conmtroid = conmtroid + 1
                        sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code,conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "', " & conmtroid & ", 'SRTB', '" & returdate & "', '" & period & "', '" & noreturn.Text & "', " & returdtloid & ", 'QL_trnjualreturTBdtl', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("unitoid") & ", " & fromlocation.SelectedValue & ",0, " & dtab.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(i)("netamt")) & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & ToDouble(HPPnew) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_crdmtr SET qtyOut = qtyOut + (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & "), saldoakhir = saldoakhir - (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & "), LastTransType = 'SRTB', lastTrans = '" & returdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "' AND mtrlocoid = " & fromlocation.SelectedValue & " AND refoid = " & dtab.Rows(i).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteNonQuery() <= 0 Then
                            crdmtroid = crdmtroid + 1
                            sSql = "insert into QL_crdmtr ( cmpcode,branch_code,crdmatoid, periodacctg, refoid, refname, mtrlocoid, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) values ('" & CompnyCode & "','" & Session("branch_id") & "', " & crdmtroid & ", '" & period & "', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & fromlocation.SelectedValue & ", (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & "), 0, 0, 0, 0, (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & "), 0, 'SRTB', '" & returdate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                        End If
                        If i = 0 Then
                        End If
                        sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = " & dtab.Rows(i).Item("qty") - dtab.Rows(i).Item("qty") & ",statusTB=1, trnjualdtlqty_retur_unit3 =  (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Next

                If status.Text = "POST" Then
                    'If DDLtype.SelectedValue = 1 Then
                    'Cek apakah retur dilakukan padahal barang sudah lunas
                    Dim amtretur_hutang As Double = 0
                    If lblcurr.Text = "1" Then
                        sSql = "select trnamtjualnetto-(accumpayment+amtretur) from QL_trnjualmst where trnjualno='" & invoiceno.Text & "' and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql
                        amtretur_hutang = xCmd.ExecuteScalar
                    Else
                        sSql = "select trnamtjualnettousd-(accumpaymentusd+amtreturusd) from QL_trnjualmst where trnjualno='" & invoiceno.Text & "' and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql
                        amtretur_hutang = xCmd.ExecuteScalar
                    End If
                    If amtretur_hutang = 0 Then
                        amtretur_hutang = ToDouble(totalamount.Text)
                    ElseIf amtretur_hutang < 0 Then
                        amtretur_hutang = ToDouble(totalamount.Text)
                    Else
                        If lblcurr.Text = "1" Then
                            sSql = "select trnamtjualnetto-(accumpayment+amtretur+" & ToDouble(totalamount.Text) & ") from QL_trnjualmst where trnjualno='" & invoiceno.Text & "' and branch_code='" & Session("branch_id") & "'"
                            xCmd.CommandText = sSql
                            amtretur_hutang = xCmd.ExecuteScalar
                        Else
                            sSql = "select trnamtjualnettousd-(accumpaymentusd+amtreturusd+" & ToDouble(totalamount.Text) & ") from QL_trnjualmst where trnjualno='" & invoiceno.Text & "' and branch_code='" & Session("branch_id") & "'"
                            xCmd.CommandText = sSql
                            amtretur_hutang = xCmd.ExecuteScalar
                        End If

                        If amtretur_hutang > 0 Then
                            amtretur_hutang = 0
                        Else
                            amtretur_hutang = amtretur_hutang * -1
                        End If
                    End If

                    sSql = "UPDATE QL_trnjualreturTBmst SET amtretur_piutang=" & amtretur_hutang & ",amtretur_piutangidr=" & amtretur_hutang * cRate.GetRateMonthlyIDRValue & ",amtretur_piutangusd=" & amtretur_hutang * cRate.GetRateMonthlyUSDValue & " where trnjualreturmstoid = " & returmstoid & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "update QL_trnjualmst set amtretur = amtretur - " & ToDouble(totalamount.Text) & ",amtreturidr = amtretur - " & (ToDouble(totalamount.Text) * cRate.GetRateMonthlyIDRValue) & ",amtreturusd = amtretur - " & (ToDouble(totalamount.Text) * cRate.GetRateMonthlyUSDValue) & " where trnjualno = '" & sono.Text & "' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                End If

                'End If

                sSql = "update ql_mstoid set lastoid = " & oidTemp & " where tablename = 'QL_histHPP' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & returdtloid & " where tablename = 'QL_trnjualreturTBdtl' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & conmtroid & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & crdmtroid & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Else
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Missing something '>_<'", 2)
                status.Text = "In Process"
                Exit Sub
            End If

            If status.Text = "POST" Then
                '==================Accounting
                Dim erracctmsg As String = ""
                If lblcurr.Text = "1" Then
                    sSql = "select isnull((select m.trnamtjualnetto-(m.amtretur+" & ToDouble(totalamount.Text) & ") from ql_trnjualmst m where cmpcode='" & CompnyCode & "' and trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "') and (select SUM(d.trnjualdtlqty_unit3) from QL_trnjualdtl d where d.trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "'))=(select SUM(d.trnjualdtlqty_retur_unit3) from QL_trnjualdtl d where d.trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "') and branch_code='" & Session("branch_id") & "' and trnjualdtlqty_retur_unit3>0)),0)"
                    xCmd.CommandText = sSql
                Else
                    sSql = "select isnull((select m.trnamtjualnettousd-(m.amtreturusd+" & ToDouble(totalamount.Text) & ") from ql_trnjualmst m where cmpcode='" & CompnyCode & "' and trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "') and (select SUM(d.trnjualdtlqty_unit3) from QL_trnjualdtl d where d.trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "'))=(select SUM(d.trnjualdtlqty_retur_unit3) from QL_trnjualdtl d where d.trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "') and branch_code='" & Session("branch_id") & "' and trnjualdtlqty_retur_unit3>0)),0)"
                    xCmd.CommandText = sSql
                End If

                'Close krn qty telah di retur semua namun amountnya masih ada sisa
                Dim amt_qtyclose As Double = xCmd.ExecuteScalar()

                Dim oidcostar As Integer = 0
                If amt_qtyclose <= 0 Or amt_qtyclose = Nothing Then
                    amt_qtyclose = 0
                Else
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_REVENUE+_RETURN' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim varrev As String = xCmd.ExecuteScalar
                    If varrev <> "" And Not varrev Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varrev & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql
                        oidcostar = xCmd.ExecuteScalar
                        If oidcostar = Nothing Or oidcostar = 0 Then
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_REVENUE+_RETURN'!<br />"
                        End If
                    End If
                End If

                sSql = "INSERT into QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & glmstoid & ",'" & CDate(toDate(returndate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(returndate.Text))) & "','Sales Return No.: " & noreturn.Text.Trim & "','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'TRN')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim glseq As Integer = 0

                'Akun retur penjualan (D)
                Dim returacctg As Integer = 0
                If ToDouble(returamount.Text) > 0 Then
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_RETJUAL' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim varretJual As String = xCmd.ExecuteScalar
                    If varretJual <> "" And Not varretJual Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varretJual & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : returacctg = xCmd.ExecuteScalar
                        If returacctg > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & returacctg & ",'C'," & ToDouble(returamount.Text) & "," & ToDouble(returamount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(returamount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun retur penjualan','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_RETJUAL'!<br />"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_RETJUAL'!<br />"
                    End If
                End If

                'Akun PPN (D)
                If ToDouble(amtppn.Text) > 0 Then

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_PPN_JUAL' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim varPPN As String = xCmd.ExecuteScalar
                    Dim ppnaccount As Integer = 0
                    If varPPN <> "" And Not varPPN Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varPPN & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ppnaccount = xCmd.ExecuteScalar
                        If ppnaccount > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & ppnaccount & ",'D'," & ToDouble(amtppn.Text) & "," & ToDouble(amtppn.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amtppn.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun PPN','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_PPN_JUAL'!<br />"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_PPN_JUAL'!<br />"
                    End If
                End If

                'Akun biaya (D)
                If ToDouble(costamount.Text) > 0 Then
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_COST_AR' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim varCost As String = xCmd.ExecuteScalar
                    Dim costacctg As Integer = 0
                    If varCost <> "" And Not varCost Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varCost & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : costacctg = xCmd.ExecuteScalar
                        If costacctg > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & costacctg & ",'D'," & ToDouble(costamount.Text) & "," & ToDouble(costamount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(costamount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun biaya','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_COST_AR'!<br />"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_COST_AR'!<br />"
                    End If
                End If

                'Akun piutang (C)
                Dim araccount As Integer = 0
                If ToDouble(amtpiutang.Text) > 0 Then
                    sSql = "select s.custacctgoid from ql_mstcust s inner join QL_mstacctg a on a.acctgoid=s.custacctgoid and s.custoid=" & Integer.Parse(custoid.Text)
                    xCmd.CommandText = sSql : araccount = xCmd.ExecuteScalar
                    If araccount > 0 Then
                        gldtloid = gldtloid + 1 : glseq = glseq + 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & araccount & ",'D'," & ToDouble(amtpiutang.Text) & "," & ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun piutang','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        erracctmsg = erracctmsg & "-Please set A/R account for this customer!<br />"
                    End If
                End If

                'Akun potongan penjualan retur (C)
                If ToDouble(amtpotpenjualan.Text) > 0 Then
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DISC_JUAL' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim vardiscJual As String = xCmd.ExecuteScalar
                    Dim potonganaccount As Integer = 0
                    If vardiscJual <> "" And Not vardiscJual Is Nothing Then
                        sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & vardiscJual & "' --AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : potonganaccount = xCmd.ExecuteScalar
                        If potonganaccount > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & potonganaccount & ",'D'," & ToDouble(amtpotpenjualan.Text) & "," & ToDouble(amtpotpenjualan.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amtpotpenjualan.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun potongan penjualan retur','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_DISC_JUAL'!<br/>"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_DISC_JUAL'!<br/>"
                    End If
                End If

                'Akun pendapatan kelebihan retur (C)
                If ToDouble(cashamount.Text) > 0 Then
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_REVENUE+_RETURN' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim varCash As String = xCmd.ExecuteScalar
                    Dim cashacctg As Integer = 0
                    If varCash <> "" And Not varCash Is Nothing Then
                        sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varCash & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : cashacctg = xCmd.ExecuteScalar
                        If cashacctg > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & cashacctg & ",'D'," & ToDouble(cashamount.Text) & "," & ToDouble(cashamount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(cashamount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun pendapatan kelebihan retur','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_REVENUE+_RETURN'!<br />"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_REVENUE+_RETURN'!<br />"
                    End If
                End If

                If ToDouble(returpiutangamount.Text) > 0 Then
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DPAR' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : Dim vardiscJual As String = xCmd.ExecuteScalar
                    Dim potonganaccount As Integer = 0
                    If vardiscJual <> "" And Not vardiscJual Is Nothing Then
                        sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & vardiscJual & "' --AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql
                        potonganaccount = xCmd.ExecuteScalar
                        If potonganaccount > 0 Then
                            gldtloid = gldtloid + 1 : glseq = glseq + 1
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & potonganaccount & ",'D'," & ToDouble(returpiutangamount.Text) & "," & ToDouble(returpiutangamount.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(returpiutangamount.Text) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun DP Retur','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Else
                            erracctmsg = erracctmsg & "-Please set account for 'VAR_DPAR'!<br />"
                        End If
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_DPAR'!<br />"
                    End If
                End If

                'Persediaan/stock
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_GUDANG' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : Dim varGudang As String = xCmd.ExecuteScalar
                Dim coa_gudang As Integer = 0
                If varGudang <> "" And Not varGudang Is Nothing Then
                    'sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varGudang & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                    sSql = "Select TOP 1 acctgoid FROM ql_mstacctg Where acctgoid=(Select genother2 FROM QL_mstgen Where gengroup='COA' AND genother1=" & fromlocation.SelectedValue & " And genother3='" & Session("branch_id") & "')"
                    xCmd.CommandText = sSql : coa_gudang = xCmd.ExecuteScalar
                    If coa_gudang > 0 Then
                        gldtloid = gldtloid + 1 : glseq = glseq + 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & coa_gudang & ",'C'," & ToDouble(hppne) & "," & ToDouble(hppne) & "," & ToDouble(hppne) / cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun Retur Sediaan','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_GUDANG'!<br />"
                    End If
                Else
                    erracctmsg = erracctmsg & "-Please set account for 'VAR_GUDANG'!<br />"
                End If

                ''HPP
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_HPP' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim varHPP As String = xCmd.ExecuteScalar
                Dim coa_hpp As Integer = 0
                If varHPP <> "" And Not varHPP Is Nothing Then
                    sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varHPP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                    xCmd.CommandText = sSql : coa_hpp = xCmd.ExecuteScalar
                    If coa_hpp > 0 Then
                        gldtloid = gldtloid + 1 : glseq = glseq + 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & coa_hpp & ",'D'," & ToDouble(hppne) & "," & ToDouble(hppne) & "," & ToDouble(hppne) / cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Akun Retur HPP','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        erracctmsg = erracctmsg & "-Please set account for 'VAR_HPP'!<br />"
                    End If
                Else
                    erracctmsg = erracctmsg & "-Please set account for 'VAR_HPP'!<br />"
                End If

                If amt_qtyclose > 0 Then
                    'Piutang dagang/kas (C)
                    If araccount > 0 Then
                        gldtloid = gldtloid + 1 : glseq = glseq + 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & araccount & ",'D'," & ToDouble(amt_qtyclose) & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Piutang dagang/kas','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        erracctmsg = erracctmsg & "-Please set A/R account (2) for this customer!<br/>"
                    End If

                    'Pendapatan retur (D)
                    gldtloid = gldtloid + 1
                    glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & oidcostar & ",'C'," & ToDouble(amt_qtyclose) & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyUSDValue & ",'" & noreturn.Text & "','Pendapatan retur','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                'If DDLtype.SelectedValue = 1 Then
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_trnpayar' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim payaroid As Int32 = xCmd.ExecuteScalar

                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conar' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim conaroid As Int32 = xCmd.ExecuteScalar

                If ToDouble(amtpiutang.Text) > 0 Then
                    'Input payar
                    sSql = "update ql_trnpayar set payamt=payamt -" & (ToDouble(amtpiutang.Text)) & ",payamtidr=payamtidr -" & (ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyIDRValue) & ",payamtusd=payamtusd -" & (ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyUSDValue) & " where payrefno = '" & invoiceno.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update ql_conar set amtbayar = amtbayar-" & (ToDouble(amtpiutang.Text)) & ",amtbayaridr = amtbayaridr-" & (ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyIDRValue) & ",amtbayarusd = amtbayarusd-" & (ToDouble(amtpiutang.Text) * cRate.GetRateMonthlyUSDValue) & " where payrefno='" & invoiceno.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If amt_qtyclose > 0 Then
                    'Input payar
                    payaroid = payaroid + 1
                    sSql = "INSERT into QL_trnpayar(cmpcode,branch_code, paymentoid, cashbankoid, custoid, payreftype, payrefoid, payflag, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt,payamtidr,payamtusd,payrefflag, paystatus, upduser, updtime, trndparoid, DPAmt, returoid) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & payaroid & ",0," & Integer.Parse(custoid.Text) & ",'QL_trnjualmst',(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "'),''," & oidcostar & ",'" & noreturn.Text & "',0,'" & CDate(toDate(returndate.Text)) & "','Hutang(D) - Kelebihan Retur(C)'," & amt_qtyclose & "," & amt_qtyclose * cRate.GetRateMonthlyIDRValue & "," & amt_qtyclose * cRate.GetRateMonthlyUSDValue & ",'','POST','" & Session("UserID") & "',current_timestamp,0,0," & Session("oid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'Input conar
                    conaroid = conaroid + 1
                    sSql = "insert into QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnarflag, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime, returoid) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & conaroid & ",'QL_trnpayar',(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "')," & payaroid & "," & Integer.Parse(custoid.Text) & "," & araccount & ",'POST','','AR','" & CDate(toDate(returndate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(returndate.Text))) & "'," & oidcostar & ",'" & CDate(toDate(returndate.Text)) & "','" & noreturn.Text & "',0,'" & CDate(toDate(returndate.Text)) & "',0," & ToDouble(amt_qtyclose) & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amt_qtyclose) * cRate.GetRateMonthlyUSDValue & ",'Sales Retur " & noreturn.Text & "','','" & Session("UserID") & "',current_timestamp," & Session("oid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                sSql = "update ql_mstoid set lastoid = " & glmstoid & " where tablename = 'QL_TRNGLMST' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & gldtloid & " where tablename = 'QL_TRNGLDTL' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If erracctmsg <> "" Then
                    otrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    showMessage(erracctmsg, 2)
                    status.Text = "In Process"
                    Exit Sub
                End If

                sSql = "update ql_mstoid set lastoid = " & glmstoid & " where tablename = 'QL_TRNGLMST' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & gldtloid & " where tablename = 'QL_TRNGLDTL' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


                sSql = "update ql_mstoid set lastoid = " & payaroid & " where tablename = 'QL_trnpayar' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & conaroid & " where tablename = 'QL_conar' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '==================Accounting
                '==================Credit Limit

                Dim climit As Double = 0.0 : Dim cbayar As Double = 0.0

                sSql = "select trnamtjualnettoacctg - accumpayment from QL_trnjualmst where trnjualno='" & invoiceno.Text & "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : cbayar = xCmd.ExecuteScalar

                If cbayar * Session("rate") < ToDouble((totalamount.Text) * Session("rate")) Then
                    climit = ToDouble(cbayar)
                Else
                    climit = ToDouble((totalamount.Text) * Session("rate"))
                End If

                'climit = ToDouble(totalamount.Text)
                If cbayar > 0.0 Then 'cek kalau udah lunas maka belum motong credit limit, kalau udah lunas.. tidak ngurangin
                    If lblcurr.Text = "1" Then
                        sSql = "update ql_mstcust set custcreditlimitusagerupiah=abs(custcreditlimitusagerupiah)-" & ToDouble(climit) & " where custoid=" & Integer.Parse(custoid.Text) & " and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        sSql = "update ql_mstcust set custcreditlimitusagerupiah=abs(custcreditlimitusagerupiah)-" & ToDouble(climit) * cRate.GetRateMonthlyIDRValue & " where custoid=" & Integer.Parse(custoid.Text) & " and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
            Else

            End If
            'End If
            '==================Credit Limit
            'End If

            otrans.Commit()
            conn.Close()
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            status.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnSalesReturnTB.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try
            'If Not ViewState("dtlsalesreturn") Is Nothing Then
            '    Dim dtab As DataTable = ViewState("dtlsalesreturn")
            '    For i As Integer = 0 To dtab.Rows.Count - 1
            '        'returdtloid = returdtloid + 1
            '        sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = trnjualdtlqty_retur - " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 = trnjualdtlqty_retur_unit3 - (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
            '        xCmd.CommandText = sSql
            '        xCmd.ExecuteNonQuery()

            '    Next
            'End If

            'sSql = "select  itemcode,id_sn from ql_mst_SN where trnreturjualmstoid ='" & noreturn.Text & "' "
            'Dim objTableSN As DataTable
            'objTableSN = cKon.ambiltabel(sSql, "SN")

            'For SN As Integer = 0 To objTableSN.Rows.Count - 1

            '    sSql = "update QL_Mst_SN set trnreturjualmstoid='" & noreturn.Text & "',status_in_out='out',tglreturjual='" & toDate(returndate.Text) & "',status_approval='In process',last_trans_type = 'Jual' where id_sn ='" & objTableSN.Rows(SN).Item("id_sn") & "' and itemcode ='" & objTableSN.Rows(SN).Item("itemcode") & "'"
            '    xCmd.CommandText = sSql
            '    xCmd.ExecuteNonQuery()
            'Next

            sSql = "select trnjualstatus from QL_trnjualreturTBmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return has been posted!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return data cannot be found!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnjualreturTBmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code = '" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualreturTBdtl where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & " and branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "update ql_trnjualreturmst set statusTB = 0 where trnjualreturno = '" & invoiceno.Text & "' and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            otrans.Commit()
            conn.Close()

            Response.Redirect("trnSalesReturnTB.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub
#End Region

    Protected Sub btnsearchtr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join  ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and a.trnsjjualno = d.trnsjjualno inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0" & _
        " union all " & _
        "select a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid, e.custname,d.trntaxpct, d.trnpaytype from ql_trnTTSmst a inner join  ql_TrnTTSDtl b on a.TTSoid = b.ttsoid inner join ql_mstitem c on b.refoid = c.itemoid inner join QL_trnjualdtl dd on a.cmpcode = dd.cmpcode and a.trnsjjualno = dd.trnsjjualno and b.refoid = dd.itemoid  inner join QL_trnjualmst d on a.cmpcode = d.cmpcode and dd.trnjualmstoid  = d.trnjualmstoid  inner join QL_mstcust e on a.cmpcode = e.cmpcode and a.custoid = e.custoid where a.cmpcode = '" & CompnyCode & "' and a.TTSstatus = 'post' and d.trnjualstatus = 'POST'  group by a.TTSoid, a.TTSNo, d.trnjualno, a.trnsjjualno, d.orderno, d.trnjualdate, e.custoid,d.trntaxpct, e.custname, d.trnpaytype having (sum(b.qty)-(select isnull(sum(m.trnjualdtlqty), 0) from QL_trnjualreturdtl m inner join QL_trnjualreturmst n on m.cmpcode = n.cmpcode and m.trnjualreturmstoid = n.trnjualreturmstoid inner join ql_mstitem o on m.cmpcode = o.cmpcode and m.itemoid = o.itemoid where n.ttsoid = a.TTSoid)) > 0"

        ViewState("trsalesreturn") = cKon.ambiltabel(sSql, "trsalesreturn")
        GVTr.DataSource = ViewState("trsalesreturn")
        GVTr.DataBind()

        GVTr.Visible = True
        GVTr.PageIndex = 0
    End Sub

    Protected Sub GVTr_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTr.PageIndexChanging
        GVTr.DataSource = ViewState("trnjualmst")
        GVTr.PageIndex = e.NewPageIndex
        GVTr.DataBind()
    End Sub

    Protected Sub btnerasetr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        trdate.Text = ""
        paytype.Text = ""
        custname.Text = ""
        custoid.Text = ""
        invoiceno.Text = ""
        sjno.Text = ""
        sono.Text = ""
        taxpct.Text = "0.00"

        If GVTr.Visible = True Then
            ViewState("trsalesreturn") = Nothing
            GVTr.DataSource = Nothing
            GVTr.DataBind()
            GVTr.Visible = False
        End If

        itemname.Text = ""
        itemcode.Text = ""
        itemoid.Text = ""
        gudangreturqty.Text = "0.00"
        poprice.Text = "0.00"
        qty.Text = "0.00"
        unit.Text = ""
        satuan1.Text = ""
        priceperunit.Text = "0.00"
        disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0
        disc3type.SelectedIndex = 0
        disc1.Text = "0.00"
        disc2.Text = "0.00"
        disc3.Text = "0.00"
        discamount1.Text = "0.00"
        discamount2.Text = "0.00"
        discamount3.Text = "0.00"
        totalamountdtl.Text = "0.00"
        totaldisc.Text = "0.00"
        netamount.Text = "0.00"
        notedtl.Text = ""
        labelseq.Text = ""
        detailstate.Text = "new"

        If GVItem.Visible = True Then
            ViewState("itemsalesreturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If

        GVDtl.DataSource = Nothing
        GVDtl.DataBind()
        ViewState("dtlsalesreturn") = Nothing
    End Sub

    Protected Sub btnsearchinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If custoid.Text = "" Then
            showMessage("Please choose Customer!", 2)
            Exit Sub
        End If

        sSql = "select distinct top 100 a.trnjualreturmstoid,a.refsjjualno trnsjjualno,a.trnjualreturno,a.trnpaytype,a.refnotaretur,a.orderno,a.trncustoid,a.currencyoid,a.trnjualdate,a.trncustname,a.typeretur from QL_trnjualreturmst a where a.cmpcode = 'MSC' and a.trnjualreturno like '%" & invoiceno.Text & "%' and a.trnjualstatus  in ('POST') and a.branch_code='" & Session("branch_id") & "' and a.trncustoid='" & custoid.Text & "' and typeretur <> 1 and a.statusTB = 0"

        ViewState("retur") = cKon.ambiltabel(sSql, "retur")
        FillGV(GVTr, sSql, "retur")
        GVTr.Visible = True
    End Sub

    Protected Sub GVTr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVTr.SelectedIndexChanged
        invoiceno.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(1).Text
        sono.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(2).Text
        sjno.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(3).Text
        trdate.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(4).Text
        custname.Text = GVTr.Rows(GVTr.SelectedIndex).Cells(5).Text
        taxamount.Text = GVTr.SelectedDataKey.Item("trnamttax")
        sino.Text = GVTr.SelectedDataKey.Item("trnjualreturmstoid")
        custoid.Text = GVTr.SelectedDataKey.Item("trncustoid")
        DDLtype.SelectedValue = GVTr.SelectedDataKey.Item("typeretur")
        lblcurr.Text = GVTr.SelectedDataKey.Item("currencyoid")
        If lblcurr.Text = 1 Then
            txtcurr.Text = "Rupiah"
        Else
            txtcurr.Text = "Dollar"
        End If

        If GVTr.SelectedDataKey("ordertaxamt") <> 0 Then
            inittax()
        Else
            taxpct.Text = "0.00"
        End If
        paytype.Text = GVTr.SelectedDataKey.Item("trnpaytype")

        GVTr.Visible = False
        btnsearchitemretur_click(Nothing, Nothing)
    End Sub
    Protected Sub btnsearchitemretur_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If invoiceno.Text = "" Then
            showMessage("Please choose No Retur!", 2)
            Exit Sub
        End If
        sSql = "select a.itemoid,i.itemcode,i.itemdesc,i.Merk,a.trnjualdtlqty,(select gendesc from QL_mstgen where genoid = a.trnjualdtlunitoid) unit from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_MSTITEM i on i.itemoid = a.itemoid where b.trnjualreturno = '" & invoiceno.Text & "' and a.branch_code = '" & Session("branch_id") & "' and b.statusTB=0"
        ViewState("returitem") = cKon.ambiltabel(sSql, "returitem")
        FillGV(GVretur, sSql, "returitem")
        GVretur.Visible = True
    End Sub
    Protected Sub lbviewdetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbviewdetail.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbviewinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbviewinfo.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnsearchitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearchitem.Click
        If DDLtype.SelectedValue = 2 Then
            sSql = "select distinct a.itemoid, a.trnjualdtlunitoid  satuan1, a.unitseq, c.itemdesc, c.itemcode, c.Merk,a.trnjualdtlqty ,crd.saldoakhir  qty ,d.gendesc unit,(select n.trnjualdtloid from QL_trnjualmst m inner join QL_trnjualdtl n on m.cmpcode = n.cmpcode and m.trnjualmstoid = n.trnjualmstoid inner join QL_trnjualreturmst c on c.refnotaretur = m.trnjualno where c.trnjualreturno = '" & Tchar(invoiceno.Text) & "' and n.itemoid = a.itemoid  ) trnjualdtloid,(select n.trnjualdtlprice from QL_trnjualmst m inner join QL_trnjualdtl n on m.cmpcode = n.cmpcode and m.trnjualmstoid = n.trnjualmstoid inner join QL_trnjualreturmst c on c.refnotaretur = m.trnjualno where c.trnjualreturno = '" & Tchar(invoiceno.Text) & "' and n.itemoid = a.itemoid ) trnjualdtlprice,a.itemloc from ql_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_trnjualmst e on e.trnjualno = b.refnotaretur and e.branch_code = b.branch_code inner join QL_trnjualdtl f on f.trnjualmstoid = e.trnjualmstoid inner join QL_MSTITEM c on a.itemoid = c.itemoid inner join QL_mstgen d on a.trnjualdtlunitoid = d.genoid inner join QL_crdmtr crd on crd.refoid = a.itemoid and crd.branch_code = a.branch_code and crd.closingdate <> '1900-01-01' where b.trnjualreturmstoid ='" & sino.Text & "' and b.typeretur='" & DDLtype.SelectedValue & "' and b.branch_code = '" & Session("branch_id") & "'"
            ViewState("itemsalesreturn") = cKon.ambiltabel(sSql, "itemsalesreturn")
            GVItem.DataSource = ViewState("itemsalesreturn")
            GVItem.DataBind()
            GVItem.Visible = True
            GVItem.PageIndex = 0
        ElseIf DDLtype.SelectedValue = 3 Then
            sSql = "select distinct a.itemoid,a.satuan1,a.itemcode,a.itemdesc,a.merk,a.pricelist,b.saldoakhir,b.mtrlocoid itemloc,(select gendesc from QL_mstgen where genoid = a.satuan1)unit from ql_mstitem a inner join QL_crdmtr b on a.itemoid=b.refoid where b.saldoAkhir <> 0 and a.itemdesc like '%" & itemname.Text & "%'"
            ViewState("itemtukarbarang") = cKon.ambiltabel(sSql, "itemtukarbarang")
            GVBB.DataSource = ViewState("itemtukarbarang")
            GVBB.DataBind()
            GVBB.Visible = True
            GVBB.PageIndex = 0
        End If

    End Sub
    Protected Sub GVBB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemcode.Text = GVBB.Rows(GVBB.SelectedIndex).Cells(1).Text
        itemname.Text = GVBB.Rows(GVBB.SelectedIndex).Cells(2).Text
        itemoid.Text = GVBB.SelectedDataKey("itemoid")
        qty.Text = GetStrData("select a.trnjualdtlqty from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_MSTITEM i on i.itemoid = a.itemoid where b.trnjualreturno = '" & invoiceno.Text & "' and a.branch_code = '" & Session("branch_id") & "'")
        qty.Enabled = False
        qty.CssClass = "inpTextDisabled"
        maxqty.Text = qty.Text
        priceperunit.Text = Format(ToDouble(GVBB.SelectedDataKey("pricelist")), "#,##0.00")
        priceperunit.Enabled = True
        priceperunit.CssClass = "inpText"
        unit.Text = GVBB.Rows(GVBB.SelectedIndex).Cells(6).Text
        satuan1.Text = GVBB.SelectedDataKey("satuan1")
        unitseq.Text = GVBB.SelectedDataKey("unitseq")
        'trnjualdtloid.Text = GVItem.SelectedDataKey("trnjualdtloid")
        itemloc.Text = GVBB.SelectedDataKey("itemloc")
        initallddl()

        ViewState("itemtukarbarang") = Nothing
        GVBB.DataSource = Nothing
        GVBB.DataBind()
        GVBB.Visible = False

        calculatedetail()
    End Sub
    Protected Sub btneraseitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btneraseitem.Click
        itemname.Text = ""
        itemcode.Text = ""
        itemoid.Text = ""
        gudangreturqty.Text = "0.00"
        poprice.Text = "0.00"
        qty.Text = "0.00"
        maxqty.Text = "0.00"
        unit.Text = ""
        satuan1.Text = ""
        unitseq.Text = ""
        trnjualdtloid.Text = ""
        promodisc.Text = "0.00"
        promodiscamt.Text = "0.00"

        If GVItem.Visible = True Then
            ViewState("itemsalesreturn") = Nothing
            GVItem.DataSource = Nothing
            GVItem.DataBind()
            GVItem.Visible = False
        End If
    End Sub

    Protected Sub GVItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItem.PageIndexChanging
        GVItem.DataSource = ViewState("itemsalesreturn")
        GVItem.PageIndex = e.NewPageIndex
        GVItem.DataBind()
    End Sub

    Protected Sub GVItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItem.SelectedIndexChanged
        itemname.Text = GVItem.Rows(GVItem.SelectedIndex).Cells(1).Text
        itemcode.Text = GVItem.Rows(GVItem.SelectedIndex).Cells(2).Text
        itemoid.Text = GVItem.SelectedDataKey("itemoid")
        'qty.Text = Format(ToDouble(GVretur.Rows(GVretur.SelectedIndex).Cells(4).Text), "#,##0.00")
        qty.Text = GetStrData("select a.trnjualdtlqty from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_MSTITEM i on i.itemoid = a.itemoid where b.trnjualreturno = '" & invoiceno.Text & "' and a.branch_code = '" & Session("branch_id") & "'")
        qty.Enabled = False
        qty.CssClass = "inpTextDisabled"
        'maxqty.Text = Format(ToDouble(GVItem.Rows(GVItem.SelectedIndex).Cells(4).Text), "#,##0.00")
        maxqty.Text = qty.Text
        priceperunit.Text = Format(ToDouble(GVItem.SelectedDataKey("trnjualdtlprice")), "#,##0.00")
        unit.Text = GVItem.Rows(GVItem.SelectedIndex).Cells(5).Text
        satuan1.Text = GVItem.SelectedDataKey("satuan1")
        unitseq.Text = GVItem.SelectedDataKey("unitseq")
        trnjualdtloid.Text = GVItem.SelectedDataKey("trnjualdtloid")
        itemloc.Text = GVItem.SelectedDataKey("itemloc")
        initallddl()
        'promodisc.Text = Format(ToDouble(GVItem.SelectedDataKey("promodisc")), "#,##0.00")
        'promodiscamt.Text = Format(ToDouble(GVItem.Rows(GVItem.SelectedIndex).Cells(4).Text) * ToDouble(GVItem.SelectedDataKey("promodisc")), "#,##0.00")

        ViewState("itemsalesreturn") = Nothing
        GVItem.DataSource = Nothing
        GVItem.DataBind()
        GVItem.Visible = False

        calculatedetail()

        'gudangreturqty.Text = Format(ToDouble(GVItem.Rows(GVItem.SelectedIndex).Cells(4).Text) - GVItem.SelectedDataKey("qtyusage"), "#,##0.00")
        'poprice.Text = Format(GVItem.SelectedDataKey("poprice"), "#,##0.00")
    End Sub
    Private Function checkotherretur(ByVal sino As String, ByVal iItemOid As Integer, ByVal cmpcode As String) As Boolean

        sSql = "select COUNT (-1) from ql_trnjualreturmst a inner join QL_trnjualreturdtl b on a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b. branch_code  where a.trnjualstatus ='In process' and a.cmpcode = '" & cmpcode & "' and a.refnotaretur ='" & sino & "' and  b.itemoid='" & iItemOid & "'"


        If masterstate.Text = "edit" Then
            sSql &= " AND a.trnjualreturmstoid <>" & Session("oid")
        ElseIf masterstate.Text = "new" Then
            sSql &= " AND a.trnjualreturmstoid <> '' "
        End If
        Return cKon.ambilscalar(sSql) > 0
    End Function
    Protected Sub btnaddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtolist.Click

        If itemoid.Text <> "" Then
            If checkotherretur(invoiceno.Text, itemoid.Text, CompnyCode) Then
                GVDtl.SelectedIndex = -1
                btnaddtolist.Visible = True
                showMessage("There is other return that using same item.<BR>Please DELETE or POSTING that ", 2)
                Exit Sub
            End If
        End If

        If itemname.Text = "" Or itemoid.Text = "" Then
            showMessage("Please choose item!", 2)
            Exit Sub
        End If
        If ToDouble(qty.Text) > ToDouble(maxqty.Text) Then
            showMessage("Quantity cannot more than maximum quantity!", 2)
            Exit Sub
        End If

        If detailstate.Text = "new" Then
            labelseq.Text = GVDtl.Rows.Count + 1
        End If



        Dim dtab As New DataTable
        If ViewState("dtlsalesreturn") Is Nothing Then
            dtab.Columns.Add("seq", Type.GetType("System.Int32"))
            dtab.Columns.Add("itemcode", Type.GetType("System.String"))
            dtab.Columns.Add("itemname", Type.GetType("System.String"))
            dtab.Columns.Add("qty", Type.GetType("System.Double"))
            dtab.Columns.Add("unit", Type.GetType("System.String"))
            dtab.Columns.Add("price", Type.GetType("System.Double"))
            dtab.Columns.Add("note", Type.GetType("System.String"))
            dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("netamt", Type.GetType("System.Double"))
            dtab.Columns.Add("unitoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtab.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
            dtab.Columns.Add("maxqty", Type.GetType("System.Double"))
            dtab.Columns.Add("location", Type.GetType("System.String"))
            dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
            dtab.Columns.Add("promodisc", Type.GetType("System.Double"))
            dtab.Columns.Add("promodiscamt", Type.GetType("System.Double"))
            dtab.Columns.Add("itemloc", Type.GetType("System.Double"))
            ViewState("dtlsalesreturn") = dtab
        Else
            dtab = ViewState("dtlsalesreturn")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(itemoid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("This item already exist in the list!<br />It can't be added!", 2)
                Exit Sub
            End If

        End If

        sSql = "SELECT isnull(has_SN,0) has_SN from QL_mstItem WHERE itemoid = '" & itemoid.Text & "'"
        Dim AdaSN As Integer = cKon.ambilscalar(sSql)

        If AdaSN = 1 Then
            TextSN.Focus()
            If Not ViewState("dtlsalesreturn") Is Nothing Or status.Text = "In Process" Or status.Text = "POST" Then
                pnlInvnSN.Visible = True
                btnHideSN.Visible = True
                TextItem.Text = itemname.Text
                KodeItem.Text = itemoid.Text
                TextQty.Text = ToDouble(qty.Text)
                TextSatuan.Text = unit.Text
                GVSN.DataSource = Nothing
                GVSN.DataBind()
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
                pnlInvnSN.Visible = True
                btnHideSN.Visible = True
                sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturjualmstoid = '" & noreturn.Text & "' AND i.itemoid = '" & itemoid.Text & "' "
                Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                Dim objDs As New DataSet
                mySqlDA.Fill(objDs, "dataSN")
                GVSN.DataSource = Nothing
                GVSN.DataSource = objDs.Tables("dataSN")
                GVSN.DataBind()
                Session("TblSN") = objDs.Tables("dataSN")
                TextItem.Text = itemname.Text
                KodeItem.Text = itemoid.Text
                TextQty.Text = ToDouble(qty.Text)
                TextSatuan.Text = unit.Text
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
            Else
                Dim objTableSN As DataTable
                objTableSN = Session("TblSN")
                Dim dvjum As DataView = objTableSN.DefaultView
                dvjum.RowFilter = "itemoid= '" & KodeItem.Text & "'"

                pnlInvnSN.Visible = True
                btnHideSN.Visible = True
                TextItem.Text = itemname.Text
                KodeItem.Text = itemoid.Text
                TextQty.Text = ToDouble(qty.Text)
                TextSatuan.Text = unit.Text
                GVSN.DataSource = Nothing
                GVSN.DataSource = objTableSN
                GVSN.DataSource = objTableSN
                GVSN.DataBind()
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
            End If
        Else
            If detailstate.Text = "new" Then
                labelseq.Text = GVDtl.Rows.Count + 1
                If DDLtype.SelectedValue = 3 Then
                    unitseq.Text = 1
                    trnjualdtloid.Text = 0
                End If
                Dim drow As DataRow = dtab.NewRow
                drow("seq") = Integer.Parse(labelseq.Text)
                drow("itemcode") = itemcode.Text
                drow("itemname") = itemname.Text
                drow("qty") = ToDouble(qty.Text)
                drow("unit") = unit.Text
                drow("price") = ToDouble(priceperunit.Text)
                drow("note") = notedtl.Text
                drow("itemoid") = Integer.Parse(itemoid.Text)
                drow("netamt") = ToDouble(totalamountdtl.Text)
                drow("unitoid") = Integer.Parse(satuan1.Text)
                drow("maxqty") = ToDouble(maxqty.Text)
                drow("unitseq") = Integer.Parse(unitseq.Text)
                drow("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                drow("location") = fromlocation.SelectedItem.ToString
                drow("locationoid") = fromlocation.SelectedValue
                drow("promodisc") = ToDouble(promodisc.Text)
                drow("promodiscamt") = ToDouble(promodiscamt.Text)
                'drow("itemloc") = ToDouble(itemloc.Text)
                dtab.Rows.Add(drow)
            Else
                Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
                If drow.Length > 0 Then
                    drow(0).BeginEdit()
                    drow(0)("itemcode") = itemcode.Text
                    drow(0)("itemname") = itemname.Text
                    drow(0)("qty") = ToDouble(qty.Text)
                    drow(0)("unit") = unit.Text
                    drow(0)("price") = ToDouble(priceperunit.Text)
                    drow(0)("note") = notedtl.Text
                    drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                    drow(0)("netamt") = ToDouble(totalamountdtl.Text)
                    drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                    drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                    drow(0)("maxqty") = ToDouble(maxqty.Text)
                    drow(0)("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                    drow(0)("location") = fromlocation.SelectedItem.ToString
                    drow(0)("locationoid") = fromlocation.SelectedValue
                    drow(0)("promodisc") = ToDouble(promodisc.Text)
                    drow(0)("promodiscamt") = ToDouble(promodiscamt.Text)
                    'drow(0)("itemloc") = ToDouble(itemloc.Text)
                    drow(0).EndEdit()
                Else
                    showMessage("Sorry, missing something!", 2)
                    Exit Sub
                End If
            End If
            dtab.AcceptChanges()
            dtab.Select("", "seq")

            GVDtl.DataSource = dtab
            GVDtl.DataBind()
            ViewState("dtlsalesreturn") = dtab

            itemname.Text = ""
            itemoid.Text = ""
            itemcode.Text = ""
            gudangreturqty.Text = "0.00"
            poprice.Text = "0.00"
            qty.Text = "0.00"
            maxqty.Text = "0.00"
            unit.Text = ""
            unitseq.Text = ""
            satuan1.Text = ""

            priceperunit.Text = "0.00"
            disc1type.SelectedIndex = 0
            disc2type.SelectedIndex = 0
            disc3type.SelectedIndex = 0
            disc1.Text = "0.00"
            disc2.Text = "0.00"
            disc3.Text = "0.00"
            discamount1.Text = "0.00"
            discamount2.Text = "0.00"
            discamount3.Text = "0.00"
            totalamountdtl.Text = "0.00"
            totaldisc.Text = "0.00"
            netamount.Text = "0.00"
            notedtl.Text = ""

            promodisc.Text = "0.00"
            promodiscamt.Text = "0.00"

            labelseq.Text = ""
            detailstate.Text = "new"

            GVDtl.SelectedIndex = -1
            GVDtl.Columns(10).Visible = True

            calculateheader()
        End If

    End Sub

    Protected Sub btnclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclear.Click
        itemname.Text = ""
        itemcode.Text = ""
        itemoid.Text = ""
        gudangreturqty.Text = "0.00"
        poprice.Text = "0.00"
        qty.Text = "0.00"
        maxqty.Text = "0.00"
        unit.Text = ""
        unitseq.Text = ""
        satuan1.Text = ""
        trnjualdtloid.Text = ""
        promodisc.Text = "0.00"
        promodiscamt.Text = "0.00"

        priceperunit.Text = "0.00"
        disc1type.SelectedIndex = 0
        disc2type.SelectedIndex = 0
        disc3type.SelectedIndex = 0
        disc1.Text = "0.00"
        disc2.Text = "0.00"
        disc3.Text = "0.00"
        discamount1.Text = "0.00"
        discamount2.Text = "0.00"
        discamount3.Text = "0.00"
        totalamountdtl.Text = "0.00"
        totaldisc.Text = "0.00"
        netamount.Text = "0.00"
        notedtl.Text = ""

        labelseq.Text = ""
        detailstate.Text = "new"

        GVDtl.SelectedIndex = -1
        GVDtl.Columns(10).Visible = True
    End Sub

    Protected Sub GVDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtl.RowDeleting
        If Not ViewState("dtlsalesreturn") Is Nothing Then
            Dim dtab As DataTable = ViewState("dtlsalesreturn")
            Dim seq As Integer = Integer.Parse(GVDtl.Rows(e.RowIndex).Cells(1).Text)
            Dim drow() As DataRow = dtab.Select("seq = " & seq & "")
            drow(0).Delete()
            dtab.Select(Nothing)
            dtab.AcceptChanges()

            Dim arow As DataRow = Nothing
            For i As Integer = 0 To dtab.Rows.Count - 1
                If dtab.Rows(i).Item("seq") > seq Then
                    arow = dtab.Rows(i)
                    arow.BeginEdit()
                    arow("seq") = arow("seq") - 1
                    arow.EndEdit()
                End If
            Next
            dtab.AcceptChanges()
            If dtab.Rows.Count = 0 Then
                grossreturn.Text = "0.00"
                totalamount.Text = "0.00"
            End If
            ViewState("dtlsalesreturn") = dtab
            GVDtl.DataSource = dtab
            GVDtl.DataBind()
        End If
    End Sub

    Protected Sub GVDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtl.SelectedIndexChanged
        labelseq.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(1).Text
        detailstate.Text = "edit"

        itemname.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(2).Text
        itemcode.Text = GVDtl.SelectedDataKey("itemcode")
        itemoid.Text = GVDtl.SelectedDataKey("itemoid")
        qty.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(3).Text
        maxqty.Text = qty.Text
        'maxqty.Text = GetStrData("select (a.trnjualdtlqty - a.trnjualdtlqty_retur) qty from QL_trnjualdtl a inner join ql_trnjualmst b on a.trnjualmstoid = b.trnjualmstoid and a.branch_code = b.branch_code where a.itemoid = '" & GVDtl.SelectedDataKey("itemoid") & "' and b.trnjualno='" & invoiceno.Text & "'")
        qty.Enabled = False
        qty.CssClass = "inpTextDisabled"
        unit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(4).Text
        unitseq.Text = GVDtl.SelectedDataKey("unitseq")
        satuan1.Text = GVDtl.SelectedDataKey("unitoid")
        itemloc.Text = GVDtl.SelectedDataKey("locationoid")
        notedtl.Text = HttpUtility.HtmlDecode(GVDtl.Rows(GVDtl.SelectedIndex).Cells(8).Text)
        trnjualdtloid.Text = GVDtl.SelectedDataKey("trnjualdtloid")

        promodisc.Text = Format(GVDtl.SelectedDataKey("promodisc"), "#,##0.00")
        promodiscamt.Text = Format(GVDtl.SelectedDataKey("promodiscamt"), "#,##0.00")

        priceperunit.Text = GVDtl.Rows(GVDtl.SelectedIndex).Cells(5).Text
        totalamountdtl.Text = Format(GVDtl.SelectedDataKey("netamt"), "#,##0.00")

        GVDtl.Columns(10).Visible = False
    End Sub

    Protected Sub discheader_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheader.TextChanged
        calculateheader()
    End Sub

    Protected Sub disc1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc1.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc2.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc3.TextChanged
        calculatedetail()
    End Sub

    Private Sub calculateheader()
        If GVDtl.Rows.Count > 0 Then
            Dim agrossreturn As Double = 0.0
            Dim ataxamount As Double = 0.0
            Dim adiscamount As Double = 0.0
            Dim atotaldischeader As Double = 0.0
            Dim atotalamount As Double = 0.0

            Dim drow As DataRow = Nothing
            Dim dtab As New DataTable

            If Not ViewState("dtlsalesreturn") Is Nothing Then
                dtab = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    drow = dtab.Rows(i)
                    agrossreturn = agrossreturn + drow.Item("netamt")
                    adiscamount = adiscamount + drow.Item("promodiscamt")
                Next
            Else
                showMessage("Missing something '>_<'", 2)
                Exit Sub
            End If

            grossreturn.Text = Format(agrossreturn, "#,##0.00")
            totalamount.Text = Format(agrossreturn + (agrossreturn * (ToDouble(taxpct.Text) / 100)), "#,##0.00")
            taxamount.Text = Format(agrossreturn * (ToDouble(taxpct.Text) / 100), "#,##0.00")
            amtdiscdtl.Text = adiscamount

        Else
            grossreturn.Text = "0.00"
            taxamount.Text = "0.00"
            discamount.Text = "0.00"
            totaldischeader.Text = IIf(discheadertype.SelectedValue = "AMT", Format(ToDouble(discheader.Text), "#,##0.00"), Format((ToDouble(grossreturn.Text) - ToDouble(discamount.Text)) * ToDouble(discheader.Text), "#,##0.00"))
            totalamount.Text = "0.00"
            amtdiscdtl.Text = "0.00"
        End If
    End Sub

    Protected Sub discheadertype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles discheadertype.SelectedIndexChanged
        calculateheader()
    End Sub

    Private Sub calculatedetail()


        promodiscamt.Text = Format(ToDouble(qty.Text) * ToDouble(promodisc.Text), "#,##0.00")
        totalamountdtl.Text = Format((ToDouble(qty.Text) * ToDouble(priceperunit.Text)) - (ToDouble(promodiscamt.Text)), "#,##0.00")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        calculatedetail()
    End Sub

    Protected Sub priceperunit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles priceperunit.TextChanged
        calculatedetail()
    End Sub

    Protected Sub disc1type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc1type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub disc2type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc2type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub disc3type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disc3type.SelectedIndexChanged
        calculatedetail()
    End Sub

    Protected Sub ibapproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapproval.Click
        status.Text = "In Approval"
        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ibposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposting.Click

        status.Text = "POST"

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "select (trnamtjualnetto - accumpayment - abs(amtretur+" & ToDouble(totalamount.Text) & ")) hutang from ql_trnjualmst where cmpcode='" & CompnyCode & "'  and trnjualmstoid=(select trnjualmstoid from ql_trnjualmst where trnjualno='" & invoiceno.Text & "')"
        xCmd.CommandText = sSql
        amtpiutang.Text = xCmd.ExecuteScalar
        conn.Close()
        returamount.Text = totalamount.Text

        If ToDouble(amtpiutang.Text) > ToDouble(returamount.Text) Then
            amtpiutang.Text = ToDouble(returamount.Text) + (ToDouble(returpiutangamount.Text))
        ElseIf ToDouble(amtpiutang.Text) = ToDouble(returamount.Text) Then
            amtpiutang.Text = ToDouble(returamount.Text)
        ElseIf ToDouble(amtpiutang.Text) < 0 Then
            returpiutangamount.Text = (ToDouble(amtpiutang.Text) * -1)
            If returpiutangamount.Text < 0 Then
                returpiutangamount.Text = 0
            ElseIf ToDouble(returamount.Text) - ToDouble(returpiutangamount.Text) < 0 Then
                returpiutangamount.Text = ToDouble(returamount.Text)
            End If
            amtpiutang.Text = 0
        End If

        amtppn.Text = ToMaskEdit(ToDouble(taxamount.Text), 2)
        amtpotpenjualan.Text = amtdiscdtl.Text
        returamount.Text = ToDouble(returamount.Text) - ToDouble(amtppn.Text)
        If ToDouble(returpiutangamount.Text) = 0 Then
            amtpiutang.Text = (ToDouble(returamount.Text) + ToDouble(amtppn.Text) + (ToDouble(costamount.Text)))
        ElseIf ToDouble(returpiutangamount.Text) > 0 Then
            amtpiutang.Text = (ToDouble(returamount.Text) - ToDouble(returpiutangamount.Text) + ToDouble(amtppn.Text) + (ToDouble(costamount.Text)))
        End If
        returamount.Text = ToDouble(returamount.Text) + ToDouble(amtpotpenjualan.Text)

        ibsave_Click(Nothing, Nothing)
    End Sub

    Protected Sub GVTrn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVTrn.PageIndexChanging
        If Not ViewState("salesretur") Is Nothing Then
            GVTrn.DataSource = ViewState("salesretur")
            GVTrn.PageIndex = e.NewPageIndex
            GVTrn.DataBind()
        End If
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim str As String() = lb.ToolTip.Split(",")

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Integer.Parse(str(0)) & " and a.branch_code='" & Session("branch_id") & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Retur_" & str(1))
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\printsalesreturn.rpt"))
            cProc.SetDBLogonForReport(rpt, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            rpt.SetParameterValue("sWhere", " where a.trnjualreturmstoid = " & Session("oid") & " and a.branch_code='" & Session("branch_id") & "'")
            rpt.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Return_" & noreturn.Text)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        End Try
    End Sub



    Protected Sub btneraseinvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        invoiceno.Text = ""
        trdate.Text = ""
        paytype.Text = ""
        sjno.Text = ""
        sono.Text = ""
        sino.Text = ""
        custoid.Text = ""
        custname.Text = ""
        GVTr.Visible = False
        GVretur.Visible = False
    End Sub

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Protected Sub btncustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select distinct b.trncustoid,a.custcode,b.trncustname from QL_mstcust a inner join QL_trnjualreturmst b on a.cmpcode=b.cmpcode and a.custoid = b.trncustoid where b.trncustname like '%" & custname.Text & "%' and  a.branch_code ='" & Session("branch_id") & "' and typeretur <> 1"
        FillGV(gvCustomer, sSql, "customer")
        gvCustomer.Visible = True
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCustomer.Rows(gvCustomer.SelectedIndex).Cells(2).Text
        custoid.Text = gvCustomer.SelectedDataKey.Item("trncustoid")
        gvCustomer.Visible = False
        btnsearchinvoice_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnerasercustomer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = ""
        custname.Text = ""
        gvCustomer.Visible = False
        GVTr.Visible = False
        GVretur.Visible = False
        invoiceno.Text = ""
        trdate.Text = ""
        paytype.Text = ""
        sjno.Text = ""
        sono.Text = ""
        sino.Text = ""
    End Sub

    Protected Sub ibdelete_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim otrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans

        Try


            sSql = "select trnjualstatus from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "post" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return has been posted!<br />It can't be deleted!", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Sales return data cannot be found!", 2)
                Exit Sub
            End If

            sSql = "delete from QL_trnjualreturmst where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualreturdtl where trnjualreturmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Not ViewState("dtlsalesreturn") Is Nothing Then
                Dim dtab As DataTable = ViewState("dtlsalesreturn")
                For i As Integer = 0 To dtab.Rows.Count - 1
                    'returdtloid = returdtloid + 1
                    sSql = "update QL_trnjualdtl set trnjualdtlqty_retur = trnjualdtlqty_retur - " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 = trnjualdtlqty_retur_unit3 - (" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & "*(select cast(konversi1_2*konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & "*(select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                Next
            End If
            otrans.Commit()
            conn.Close()

            Response.Redirect("trnSalesReturnTB.aspx?awal=true")
        Catch ex As Exception
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVSN_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim iIndex As Int16 = e.RowIndex
        Dim objTableSN As DataTable = Session("TblSN")
        objTableSN.Rows.RemoveAt(iIndex)

        'resequence sjDtlsequence 
        For C1 As Int16 = 0 To objTableSN.Rows.Count - 1
            Dim dr As DataRow = objTableSN.Rows(C1)
            dr.BeginEdit()
            dr("SNseq") = C1 + 1
            dr.EndEdit()
        Next

        Session("TblSN") = objTableSN
        GVSN.Visible = True

        GVSN.DataSource = objTableSN
        GVSN.DataBind()
        ModalPopupSN.Show()
        TextSN.Text = ""
        TextSN.Focus()
    End Sub

    Protected Sub EnterSN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EnterSN.Click
        TextSN.Focus()
        'sSql = "select COUNT(a.sn) FROM QL_mstitemDtl a INNER JOIN QL_Mst_SN b ON a.sn = b.id_sn WHERE b.status_in_out ='jual' AND a.sn = '" & TextSN.Text & "' and a.itemoid = " & KodeItem.Text & " "

        sSql = "select COUNT(a.sn)  FROM QL_mstitemDtl a  INNER JOIN QL_Mst_SN b ON a.sn = b.id_sn  WHERE b.status_in_out ='in' and (trnreturjualmstoid  = '" & invoiceno.Text & "' or trnreturjualmstoid is null) AND a.sn = '" & TextSN.Text & "' and a.itemoid = '" & KodeItem.Text & "' "
        Dim SN As Object = cKon.ambilscalar(sSql)

        If Not SN = 0 Then
            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If

            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView

            'Cek apa sudah ada item yang sama dalam Tabel Detail

            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "'"

            Else
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "' AND SNseq <>" & SNseq.Text

            End If

            If dv.Count > 0 Then
                LabelPesanSn.Text = "Serial Number Sudah Entry"
                dv.RowFilter = ""
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
                Exit Sub
            End If


            dv.RowFilter = ""
            'insert/update to list data
            Dim objRow As DataRow
            If NewSN.Text = "New SN" Then
                objRow = objTableSN.NewRow()
                objRow("SNseq") = objTableSN.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTableSN.Select("SNseq=" & SNseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If

            objRow("itemoid") = KodeItem.Text
            objRow("SN") = TextSN.Text

            If NewSN.Text = "New SN" Then
                objTableSN.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If


            Session("TblSN") = objTableSN
            GVSN.Visible = True
            GVSN.DataSource = Nothing
            GVSN.DataSource = objTableSN
            GVSN.DataBind()
            LabelPesanSn.Text = ""
            SNseq.Text = objTableSN.Rows.Count + 1
            GVSN.SelectedIndex = -1
            TextSN.Focus()
        Else
            LabelPesanSn.Text = "Serial Number Unknow"
        End If
        ModalPopupSN.Show()
        TextSN.Text = ""
        TextSN.Focus()
    End Sub

    Protected Sub lkbPilihItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPilihItem.Click

        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dvjum As DataView = objTableSN.DefaultView
            dvjum.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            If dvjum.Count = TextQty.Text Then
                dvjum.RowFilter = ""
                Dim dtab As New DataTable
                If ViewState("dtlsalesreturn") Is Nothing Then
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    dtab.Columns.Add("itemname", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("price", Type.GetType("System.Double"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("netamt", Type.GetType("System.Double"))
                    dtab.Columns.Add("unitoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("unitseq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("maxqty", Type.GetType("System.Double"))
                    dtab.Columns.Add("location", Type.GetType("System.String"))
                    dtab.Columns.Add("locationoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("promodisc", Type.GetType("System.Double"))
                    dtab.Columns.Add("promodiscamt", Type.GetType("System.Double"))
                    dtab.Columns.Add("itemloc", Type.GetType("System.Double"))
                    ViewState("dtlsalesreturn") = dtab
                Else
                    dtab = ViewState("dtlsalesreturn")
                End If
                If detailstate.Text = "new" Then
                    labelseq.Text = GVDtl.Rows.Count + 1

                    Dim drow As DataRow = dtab.NewRow
                    drow("seq") = Integer.Parse(labelseq.Text)
                    drow("itemcode") = itemcode.Text
                    drow("itemname") = itemname.Text
                    drow("qty") = ToDouble(qty.Text)
                    drow("unit") = unit.Text
                    drow("price") = ToDouble(priceperunit.Text)
                    drow("note") = notedtl.Text
                    drow("itemoid") = Integer.Parse(itemoid.Text)
                    drow("netamt") = ToDouble(totalamountdtl.Text)
                    drow("unitoid") = Integer.Parse(satuan1.Text)
                    drow("maxqty") = ToDouble(maxqty.Text)
                    drow("unitseq") = Integer.Parse(unitseq.Text)
                    drow("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                    drow("location") = fromlocation.SelectedItem.ToString
                    drow("locationoid") = fromlocation.SelectedValue
                    drow("promodisc") = ToDouble(promodisc.Text)
                    drow("promodiscamt") = ToDouble(promodiscamt.Text)
                    drow("itemloc") = ToDouble(itemloc.Text)
                    dtab.Rows.Add(drow)
                Else
                    Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "")
                    If drow.Length > 0 Then
                        drow(0).BeginEdit()
                        drow(0)("itemcode") = itemcode.Text
                        drow(0)("itemname") = itemname.Text
                        drow(0)("qty") = ToDouble(qty.Text)
                        drow(0)("unit") = unit.Text
                        drow(0)("price") = ToDouble(priceperunit.Text)
                        drow(0)("note") = notedtl.Text
                        drow(0)("itemoid") = Integer.Parse(itemoid.Text)
                        drow(0)("netamt") = ToDouble(totalamountdtl.Text)
                        drow(0)("unitoid") = Integer.Parse(satuan1.Text)
                        drow(0)("unitseq") = Integer.Parse(unitseq.Text)
                        drow(0)("maxqty") = ToDouble(maxqty.Text)
                        drow(0)("trnjualdtloid") = Integer.Parse(trnjualdtloid.Text)
                        drow(0)("location") = fromlocation.SelectedItem.ToString
                        drow(0)("locationoid") = fromlocation.SelectedValue
                        drow(0)("promodisc") = ToDouble(promodisc.Text)
                        drow(0)("promodiscamt") = ToDouble(promodiscamt.Text)
                        'drow(0)("itemloc") = ToDouble(itemloc.Text)
                        drow(0).EndEdit()
                    Else
                        showMessage("Sorry, missing something!", 2)
                        Exit Sub
                    End If
                End If
                dtab.AcceptChanges()
                dtab.Select("", "seq")

                GVDtl.DataSource = dtab
                GVDtl.DataBind()
                ViewState("dtlsalesreturn") = dtab

                itemname.Text = ""
                itemoid.Text = ""
                itemcode.Text = ""
                gudangreturqty.Text = "0.00"
                poprice.Text = "0.00"
                qty.Text = "0.00"
                maxqty.Text = "0.00"
                unit.Text = ""
                unitseq.Text = ""
                satuan1.Text = ""

                priceperunit.Text = "0.00"
                disc1type.SelectedIndex = 0
                disc2type.SelectedIndex = 0
                disc3type.SelectedIndex = 0
                disc1.Text = "0.00"
                disc2.Text = "0.00"
                disc3.Text = "0.00"
                discamount1.Text = "0.00"
                discamount2.Text = "0.00"
                discamount3.Text = "0.00"
                totalamountdtl.Text = "0.00"
                totaldisc.Text = "0.00"
                netamount.Text = "0.00"
                notedtl.Text = ""

                promodisc.Text = "0.00"
                promodiscamt.Text = "0.00"

                labelseq.Text = ""
                detailstate.Text = "new"

                GVDtl.SelectedIndex = -1
                GVDtl.Columns(10).Visible = True

                calculateheader()
            Else
                LabelPesanSn.Text = "Jumlah Item Tidak Sesuai Dengan jumlah Serial Number"
                ModalPopupSN.Show()
                TextSN.Text = ""
                TextSN.Focus()
            End If

        Else
            LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
            ModalPopupSN.Show()
            TextSN.Text = ""
            TextSN.Focus()
        End If

    End Sub

    Protected Sub lkbCloseSN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbCloseSN.Click
        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@

        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            dvSN.RowFilter = "itemoid = " & GVDtl.DataKeyNames(0)
            For kdsn As Integer = 0 To dvSN.Count - 1
                objTableSN.Rows.RemoveAt(0)
            Next
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    End Sub



End Class
