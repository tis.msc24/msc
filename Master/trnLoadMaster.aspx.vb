﻿Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.Office.Interop.Excel
Imports System.IO.Directory

Partial Class trnLoadMaster
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure 
    '@@@@@@@@@@@ Konek Exel @@@@@@@@@
    Dim KodeITemExcel As String
    Dim JumItemExcel As Integer
    Dim adap As OleDbDataAdapter

#End Region

#Region "Functions"  
    Public Function ConvertDtToExc(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Public Function ConvertDtToExcl(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            strCaption &= " - INFORMATION"
            lblCaption.ForeColor = Drawing.Color.White
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitCabangNya()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
        End If
    End Sub

    Private Sub OrderMst()
        Response.Clear()

        Response.AddHeader("content-disposition", "inline;filename=OrderMst.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If

        sSql = "Select * from QL_trnordermst"
        Dim ds As New DataSet : Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd : da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToExc(dt))
        conn.Close() : Response.End()
    End Sub

    Private Sub OrderDtl()
        Response.Clear()

        Response.AddHeader("content-disposition", "inline;filename=OrderDtl.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        sSql = "Select * from QL_trnorderdtl"
        Dim ds As New DataSet : Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd : da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)

        Response.Write(ConvertDtToExcl(dt))
        conn.Close() : Response.End()
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            'dv = Session("Role").DefaultView
            'MsgBox(dv.Count)
            Response.Redirect("~\Master\trnLoadMaster.aspx")
        End If

        Page.Title = CompnyName & " - Load Data Online"
        Session("oid") = Request.QueryString("oid") 
        Page.Title = CompnyName & " - Load Data Online"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
 
        If Not Page.IsPostBack Then
            InitCabangNya()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing 
            End If
        End If
    End Sub

#End Region

    Protected Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpload.Click
        Dim excelconn As New OleDbConnection
        Dim fileSOHdr As String = FileSOHeader.PostedFile.FileName
        Dim fileNameSOHdr As String = Path.GetFileName(fileSOHdr)
        Dim ext As String = Path.GetExtension(fileNameSOHdr)
        Dim type As String = String.Empty
        Dim uploadName As String = "" ' & Format("12/28/2016", "MMddyyyyHHmm")

        Select Case ext
            Case ".xls"
                type = "application/vnd.ms-excel"
            Case ".xlsx"
                type = "application/vnd.ms-excel"
        End Select

        'Try
        If FileSOHeader.HasFile Then

            If type <> String.Empty Then
                FileSOHeader.SaveAs(Server.MapPath("~/Upload/" & uploadName & ext))

                If File.Exists(Server.MapPath("~/Upload/" & uploadName & ext)) Then
                    Dim sHeaderMsg As String = "" : Dim excelconnstr As String = ""
                    Try
                        If ext = ".xlsx" Then
                            excelconnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 12.0;Persist Security Info=False"
                        ElseIf ext = ".xls" Then
                            excelconnstr = "Provider=Microsoft.JET.OLEDB.4.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 8.0;Persist Security Info=False"
                        End If

                        excelconn.ConnectionString = excelconnstr
                        excelconn.Open()
                    Catch ex As Exception
                        excelconn.Close()
                        showMessage("Maaf, File Excel mohon untuk save ulang dengan klik save as type excel 97-2003 Workbook..!!", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End Try

                    Dim ds As New DataSet

                    Try
                        If fileNameSOHdr <> "OrderMst.xls" Then
                            excelconn.Close()
                            showMessage("Maaf, file yang anda ambil salah, pilih file dengan nama OrderMst.xls..!!", CompnyName & " - WARNING", 2)
                            Exit Sub
                        End If

                        Dim sqlstring As String = "SELECT * FROM [OrderMst$]"
                        Dim oda As New OleDb.OleDbDataAdapter(sqlstring, excelconnstr)
                        oda.Fill(ds, "mstitem")
                        excelconn.Close()

                    Catch ex As Exception
                        excelconn.Close()
                        showMessage("Format Template Salah, Ganti Nama Sheet Menjadi OrderMst ..!!", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End Try

                    Dim dts As DataTable = ds.Tables("mstitem")
                    Dim dvs As DataView = dts.DefaultView

                    Dim objTrans As SqlClient.SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If

                    objTrans = conn.BeginTransaction()
                    xCmd.Transaction = objTrans

                    Try
                        For i As Integer = 0 To dts.Rows.Count - 1
                            Dim iCurID As Integer = 0 : Dim sTYpe As String = ""
                            dvs.RowFilter = "ordermstoid=" & Integer.Parse(dts.Rows(i)("ordermstoid"))

                            sSql = "Select COUNT(*) from QL_trnordermst Where ordermstoid=" & Integer.Parse(dts.Rows(i)("ordermstoid")) & " AND branch_code='" & CabangNya.SelectedValue & "'"
                            xCmd.CommandText = sSql : Dim SekOidSo As Double = xCmd.ExecuteScalar()

                            If SekOidSo = 0 Then
                                For I2 As Integer = 0 To 1 - 1
                                    If dvs.Item(0)("typeSO").ToString = "Konsinyasi" Then : sTYpe = "K" : End If
                                    Dim hh As Integer = dvs.Item(0)("ordermstoid")

                                    Dim CaBang As String = GetStrData("Select genother1 from ql_mstgen where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
                                    Dim sNo As String = "SO" & dvs.Item(0)("flagTax") & sTYpe & "/" & CaBang & "/" & Format(dvs.Item(0)("trnorderdate"), "yy/MM/dd") & "/"

                                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(orderno,4) AS INT)),0) AS IDNEW FROM ql_trnordermst WHERE branch_code='" & CabangNya.SelectedValue & "' and cmpcode='" & CompnyCode & "' AND orderno LIKE '" & sNo & "%'"
                                    xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar() + 1

                                    sSql = "INSERT INTO [QL_MSC].[dbo].[QL_trnordermst] ([cmpcode],[ordermstoid],[to_branch],[branch_code],[orderno],[trnordertype],[trnorderdate],[trncustoid],[trncustname],[trnpaytype],[salesoid],[updtime],[upduser],[createuser],[trnorderstatus],[amtjualnetto],[amtjualnettoidr],[amtjualnettousd],[ekspedisioid],[spgoid],[delivdate],[consigneeoid],[trntaxpct],[periodacctg],[promooid],[createtime],[SOBO],[flagbottomprice],[digit],[currencyoid],[rateoid],[rate2oid],[timeofpaymentSO],[ordertaxamt],[ordertaxtype],[flagTax],[typeExpedisi],[typeSO],[bundling],[amtdisc1],[amtdisc2]) VALUES " & _
                                    "('" & CompnyCode & "'," & Integer.Parse(dvs.Item(0)("ordermstoid")) & ",'" & CabangNya.SelectedValue & "','" & CabangNya.SelectedValue & "','" & GenNumberString(sNo, "", iCurID, 4) & "','" & Tchar(dvs.Item(0)("trnordertype")) & "'," & Format(dvs.Item(0)("trnorderdate"), "yy/MM/dd") & ",'" & Integer.Parse(dvs.Item(0)("trncustoid")) & "','" & Tchar(dvs.Item(0)("trncustname")) & "','" & Integer.Parse(dvs.Item(0)("trnpaytype")) & "','" & Integer.Parse(dvs.Item(0)("salesoid")) & "'," & Format(dvs.Item(0)("updtime"), "yy/MM/dd") & ",'" & Tchar(dvs.Item(0)("upduser")) & "','" & Tchar(dvs.Item(0)("createuser")) & "','" & Tchar(dvs.Item(0)("trnorderstatus")) & "'," & dvs.Item(0)("amtjualnetto") & "," & dvs.Item(0)("amtjualnettoidr") & "," & dvs.Item(0)("amtjualnettousd") & ",'" & Integer.Parse(dvs.Item(0)("ekspedisioid")) & "'," & Integer.Parse(dvs.Item(0)("spgoid")) & "," & Format(dvs.Item(0)("delivdate"), "yy/MM/dd") & "," & Integer.Parse(dvs.Item(0)("consigneeoid")) & "," & dvs.Item(0)("trntaxpct") & ",'" & (dvs.Item(0)("periodacctg")) & "'," & Integer.Parse(dvs.Item(0)("promooid")) & "," & Format(dvs.Item(0)("createtime"), "yy/MM/dd") & ",'" & dvs.Item(0)("SOBO").ToString & "','" & dvs.Item(0)("flagbottomprice").ToString & "'," & Integer.Parse(dvs.Item(0)("digit")) & "," & Integer.Parse(dvs.Item(0)("currencyoid")) & "," & Integer.Parse(dvs.Item(0)("rateoid")) & "," & Integer.Parse(dvs.Item(0)("rate2oid")) & "," & Integer.Parse(dvs.Item(0)("timeofpaymentSO")) & "," & dvs.Item(0)("ordertaxamt") & ",'" & dvs.Item(0)("ordertaxtype") & "'," & Integer.Parse(dvs.Item(0)("flagTax")) & ",'" & Tchar(dvs.Item(0)("typeExpedisi")) & "','" & Tchar(dvs.Item(0)("typeSO")) & "'," & Integer.Parse(dvs.Item(0)("bundling")) & "," & dvs.Item(0)("amtdisc1") & "," & dvs.Item(0)("amtdisc2") & ")"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                Next
                            End If
                            
                            dvs.RowFilter = ""
                            'For v As Integer = 0 To dvs.Count - 1
                            sSql = "INSERT INTO [QL_MSC].[dbo].[QL_trnorderdtl] ([cmpcode],[trnorderdtloid],[branch_code],[to_branch],[trnordermstoid],[trnorderdtlseq],[itemoid],[trnorderdtlqty],[trnorderdtlunitoid],[unitseq],[upduser],[updtime],[currencyoid],[varprice],[trnorderprice],[trnorderpriceidr],[trnorderpriceusd],[trnamountdtl],[trnamountdtlidr],[trnamountdtlusd],[orderdelivqty],[trndiskonpromo],[trndiskonpromoidr],[trndiskonpromousd],[trnamountbruto],[trnamountbrutoidr],[trnamountbrutousd],[promooid],[flagbottompricedtl],[bottomprice],[amountbottomprice],[statusDelivery],[tempAmtUsd],[amtdtldisc1],[amtdtldisc2],[jenisprice],[trnjualreturmstoid],[trnjualreturdtloid]) " & _
                            "VALUES ('" & CompnyCode & "'," & Integer.Parse(dts.Rows(i)("trnorderdtloid")) & ",'" & CabangNya.SelectedValue & "','" & CabangNya.SelectedValue & "'," & Integer.Parse(dts.Rows(i)("trnordermstoid")) & "," & Integer.Parse(dts.Rows(i)("trnorderdtlseq")) & "," & Integer.Parse(dts.Rows(i)("itemoid")) & "," & dts.Rows(i)("trnorderdtlqty") & "," & Integer.Parse(dts.Rows(i)("trnorderdtlunitoid")) & "," & Integer.Parse(dts.Rows(i)("unitseq")) & ",'" & Tchar(dts.Rows(i)("upduser")) & "'," & Format(dts.Rows(i)("updtime"), "yy/MM/dd") & ",'" & dts.Rows(i)("currencyoid") & "'," & dts.Rows(i)("varprice") & "," & dts.Rows(i)("trnorderprice") & "," & dts.Rows(i)("trnorderpriceidr") & "," & dts.Rows(i)("trnorderpriceusd") & "," & dts.Rows(i)("trnamountdtl") & "," & dts.Rows(i)("trnamountdtlidr") & "," & dts.Rows(i)("trnamountdtlusd") & "," & dts.Rows(i)("orderdelivqty") & "," & dts.Rows(i)("trndiskonpromo") & "," & dts.Rows(i)("trndiskonpromoidr") & "," & dts.Rows(i)("trndiskonpromousd") & "," & dts.Rows(i)("trnamountbruto") & "," & dts.Rows(i)("trnamountbrutoidr") & "," & dts.Rows(i)("trnamountbrutousd") & "," & Integer.Parse(dts.Rows(i)("promooid")) & ",'" & dts.Rows(i)("flagbottompricedtl") & "'," & dts.Rows(i)("bottomprice") & "," & dts.Rows(i)("amountbottomprice") & ",'" & dts.Rows(i)("statusDelivery") & "'," & dts.Rows(i)("tempAmtUsd") & "," & dts.Rows(i)("amtdtldisc1") & "," & dts.Rows(i)("amtdtldisc2") & ",'" & dts.Rows(i)("jenisprice") & "'," & Integer.Parse(dts.Rows(i)("trnjualreturmstoid")) & "," & Integer.Parse(dts.Rows(i)("trnjualreturdtloid")) & ")"

                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            'Next
                        Next
                        objTrans.Commit() : conn.Close()
                    Catch ex As Exception
                        objTrans.Rollback() : conn.Close()
                        showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - WARNING", 1)
                        Exit Sub
                    End Try
                    Response.Redirect("~\Master\trnLoadMaster.aspx?awal=true")
                Else
                    showMessage("Upload Gagal sodara..!!", CompnyName & " - WARNING", 2)
                    Exit Sub
                End If
            Else
                excelconn.Close() : conn.Close()
                showMessage("type file harus .xls..!!", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        Else
            showMessage("Maaf, anda belum pilih file excelnya..!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        'Catch ex As Exception
        '    excelconn.Close() : conn.Close()
        '    showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - WARNING", 2)
        '    Exit Sub
        'End Try
    End Sub

    Protected Sub BtnBatal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBatal.Click
        Response.Redirect("~\Master\trnLoadMaster.aspx?awal=true")
    End Sub
End Class
