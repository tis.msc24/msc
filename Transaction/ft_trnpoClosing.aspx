<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="ft_trnpoClosing.aspx.vb" Inherits="trnpo" title="PT. MULTI SARANA COMPUTER - PO CLOSE" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">

 
    <table id="Table3" align="center" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th class="header" colspan="5" valign="top">
                <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Purchase Order - Closing"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"  Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="TabPanel10">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w153" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w154"><asp:ListItem Value="pono">PO No</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w155"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w156" TargetControlID="FilterPeriod1" MaskType="Date" UserDateFormat="DayMonthYear" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w157" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w158" Visible="False" Enabled="False" Checked="True" AutoPostBack="False"></asp:CheckBox>Period</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w159"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w160"></asp:ImageButton>&nbsp;<asp:Label id="Label76" runat="server" Text="to" __designer:wfdid="w161"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w162"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w163"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w164"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w165" Visible="False"></asp:CheckBox></TD><TD class="Label" align=left></TD><TD align=left colSpan=4><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w166" Visible="False"><asp:ListItem Value="custname">All</asp:ListItem>
<asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w167"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w168"></asp:ImageButton><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w169" TargetControlID="FilterPeriod2" MaskType="Date" UserDateFormat="DayMonthYear" Mask="99/99/9999" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w170" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="tbldata" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w151" DataKeyNames="pomstoid,pono" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="pomstoid" HeaderText="PO ID" Visible="False">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pono" HeaderText="PO No">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="podateclosed" DataFormatString="{0:d}" HeaderText="Date Close">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="5%"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="5%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="supplier" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="postatus" HeaderText="PO Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="20%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ponoteclosed" HeaderText="PO Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="30%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="osPO" HeaderText="O/S PO" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" SelectText="Print" ShowSelectButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" /> <strong><span style="font-size: 9pt">
                                :: List of Purchase Order</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel20" runat="server" HeaderText="TabPanel20">
                        <ContentTemplate>
<div style="text-align:left">
    <asp:UpdatePanel id="UpdatePanel4" runat="server">
    <ContentTemplate>
<TABLE id="Table2" cellSpacing=2 cellPadding=4 width="100%" align=center border=0><TBODY><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD>No. PO&nbsp;<asp:Label id="Label29" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w20"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099"><asp:TextBox id="suppliername" runat="server" Width="129px" CssClass="inpText" __designer:wfdid="w21"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSupp" onclick="btnSearchSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w22"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w23"></asp:ImageButton></TD><TD colSpan=2><asp:TextBox id="IOID" runat="server" Width="1px" __designer:wfdid="w24" Visible="False"></asp:TextBox>Supplier</TD><TD><asp:TextBox id="suppname" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w25"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD>Reason Note <asp:Label id="Label1" runat="server" Width="8px" ForeColor="#C00000" Text="*" __designer:wfdid="w26"></asp:Label></TD><TD colSpan=4><asp:TextBox id="txtNote" runat="server" Width="353px" CssClass="inpText" __designer:wfdid="w27" MaxLength="30"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD><asp:Label id="osPO" runat="server" __designer:wfdid="w28" Visible="False"></asp:Label></TD><TD colSpan=5><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvSupplier" runat="server" Width="98%" Height="1px" ForeColor="#333333" __designer:wfdid="w29" Visible="False" PageSize="5" AllowPaging="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="pomstoid,pono,suppname">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="pono" HeaderText="PO no">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" Font-Size="X-Small" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Purchase Order data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD colSpan=6>&nbsp;<FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 181px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 95%; BACKGROUND-COLOR: beige; TEXT-ALIGN: left"><asp:GridView id="tbldtl" runat="server" Width="99%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w30" Visible="False" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="seq">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="material" HeaderText="Material">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyPO" HeaderText="Qty PO">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtydelivery" HeaderText="Qty Delivery">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="osQty" HeaderText="Qty OS">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></DIV></FIELDSET></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD colSpan=6>Last Update on <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1970324836974620" __designer:wfdid="w31"></asp:Label> by <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:dtid="1970324836974621" __designer:wfdid="w32"></asp:Label> </TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD colSpan=6><asp:ImageButton id="btnsavemstr" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974624" __designer:wfdid="w33"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:dtid="1970324836974625" __designer:wfdid="w34"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #000099"><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w35" AssociatedUpdatePanelID="UpdatePanel4"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w36"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</ContentTemplate>
    </asp:UpdatePanel>
                                        </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">
                            :: Form Purchase Order </span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upMsgbox" runat="server">
                    <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupControlID="PanelMsgBox" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>
