Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnTCSupplier
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"

    Private Function SetTblDetail() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("trfwhserviceoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("trfwhservicedtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("trntcserviceqty", Type.GetType("System.Double"))
        dtab.Columns.Add("trntcnotedtl", Type.GetType("System.String"))
        dtab.Columns.Add("unit", Type.GetType("System.String"))
        dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
        dtab.Columns.Add("trntcservicedtloid", Type.GetType("System.Int32"))
        Session("itemdetail") = dtab
        Return dtab
    End Function

    Private Function GenerateNoPost(ByVal sTujuan As String)
        Dim iCurID As Integer = 0
        sSql = "Select genother1 from ql_mstgen Where gencode='" & sTujuan & "' And gengroup='CABANG'"
        Dim trnno As String = "TKS/" & GetScalar(sSql) & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trftcserviceno, 4) AS INT)), 0) FROM QL_trntcsuppmst WHERE cmpcode = '" & cmpcode & "' AND trftcserviceno LIKE '" & trnno & "%'"
        iCurID = GetStrData(sSql) + 1
        trnno = GenNumberString(trnno, "", iCurID, 4)
        Return trnno
    End Function

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "trfwhservicedtloid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("Qty") = ToDouble(GetTextValue(row.Cells(4).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "trfwhservicedtloid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("Qty") = ToDouble(GetTextValue(row.Cells(5).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("Qty") = ToDouble(GetTextValue(row.Cells(5).Controls))
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Sub fCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(drCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(drCabang, sSql)
            Else
                FillDDL(drCabang, sSql)
                drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
                drCabang.SelectedValue = "SEMUA BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(drCabang, sSql)
            drCabang.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            drCabang.SelectedValue = "SEMUA BRANCH"
        End If

    End Sub

    Private Sub TujuanCabDDL()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(TujuanCb, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(TujuanCb, sSql)
            Else
                FillDDL(TujuanCb, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(TujuanCb, sSql)
        End If
    End Sub

    Private Sub BindSuppData()
        sSql = "Select sup.suppoid,sup.suppcode,sup.suppname,sup.suppaddr from QL_mstsupp sup Where cmpcode='" & cmpcode & "' AND (sup.suppcode LIKE '%" & Tchar(SuppName.Text) & "%' OR suppname LIKE '%" & Tchar(SuppName.Text) & "%') AND suppoid IN (Select suppoid From (Select a.suppoid,(select sum(trfwhserviceqty) From ql_trfwhservicedtl dt Where cmpcode = a.cmpcode and a.Trfwhserviceoid = dt.trfwhserviceoid)-ISNULL((Select isnull(sum(tcd.trntcserviceqty), 0.00) From QL_trntcsuppdtl tcd inner join QL_trntcsuppmst tcm on tcm.cmpcode = tcd.cmpcode AND tcm.trntcserviceoid=tcd.trntcserviceoid And tcm.branch_code=tcd.branch_code Where tcm.trfwhserviceoid = a.trfwhserviceoid AND tcm.branch_code=a.FromBranch),0.00) QtySisa From ql_trfwhservicemst a Where a.cmpcode = '" & cmpcode & "' And a.Trfwhservicestatus IN ('Approved','POST') And a.FromBranch= '" & TujuanCb.SelectedValue & "') Twc Where QtySisa>0.00)"
        Dim dts As DataTable = ckon.ambiltabel(sSql, "Ql_supplier")
        GVSupp.DataSource = dts : GVSupp.DataBind()
        GVSupp.SelectedIndex = -1 : GVSupp.Visible = True
    End Sub

    Private Sub bindDataPO()
        Dim sWhere As String = ""
        If SuppOid.Text = "" Then
            showMessage("Maaf, Anda belum memilih supplier...!!!", 2)
            Exit Sub
        End If
        sSql = "Select * From (Select a.Trfwhserviceoid,a.Trfwhserviceno, a.Trfwhservicedate,a.Trfwhservicenote,a.FromMtrLocOid,a.FromBranch,(select sum(trfwhserviceqty) From ql_trfwhservicedtl dt Where cmpcode = a.cmpcode and a.Trfwhserviceoid = dt.trfwhserviceoid) QtyTw,ISNULL((Select isnull(sum(tcd.trntcserviceqty), 0.00) From QL_trntcsuppdtl tcd Where tcd.trfwhserviceoid = a.trfwhserviceoid AND tcd.branch_code=a.FromBranch),0.00) QtyTc,(select sum(trfwhserviceqty) From ql_trfwhservicedtl dt Where cmpcode = a.cmpcode and a.Trfwhserviceoid = dt.trfwhserviceoid) - ISNULL((Select isnull(sum(tcd.trntcserviceqty), 0.00) From QL_trntcsuppdtl tcd Where tcd.trfwhserviceoid = a.trfwhserviceoid AND tcd.branch_code=a.FromBranch),0.00) QtySisa,(Select gd.genother5 from QL_mstgen gd Where gd.genoid=a.FromMtrLocOid AND gd.gengroup='LOCATION') TypeGudang From ql_trfwhservicemst a Where a.cmpcode = '" & cmpcode & "' And a.Trfwhservicestatus IN ('Approved','POST') And a.FromBranch= '" & TujuanCb.SelectedValue & "' And a.TrfwhserviceNo like '%" & Tchar(noref.Text.Trim) & "%' AND a.suppoid=" & SuppOid.Text & ") Twc Where QtySisa>0.00"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
        GVpo.DataSource = dtab : GVpo.DataBind()
        Session("polistwhsupplier") = dtab
    End Sub

    Private Sub BindData(ByVal sWhere As String)
        If CbTanggal.Checked = True Then
            Dim date1, date2 As New Date
            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)
            sWhere &= " AND CONVERT(DATE, c.trntcservicedate, 101) BETWEEN '" & date1 & " 00:00:00' And '" & date2 & " 23:59:59'"
        End If

        If ddlStatus.SelectedValue.ToUpper <> "ALL" Then
            sWhere &= " AND c.trntcstatus='" & ddlStatus.SelectedValue.ToUpper & "'"
        End If

        If drCabang.SelectedValue <> "SEMUA BRANCH" Then
            sWhere &= " AND FromBranch='" & drCabang.SelectedValue & "'"
        End If

        sSql = "SELECT DISTINCT c.trntcserviceoid, c.trftcserviceno, c.trntcservicedate, c.upduser, c.trntcstatus,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=c.branch_code AND tb.cmpcode=c.cmpcode AND gengroup='CABANG') FromBranch,sp.suppname,tw.TrfwhserviceNo,c.branch_code FROM QL_trntcsuppmst c Inner Join ql_trfwhservicemst tw ON tw.Trfwhserviceoid=c.trfwhserviceoid AND c.branch_code=tw.FromBranch ANd c.suppoid=tw.suppoid Inner Join QL_mstsupp sp ON sp.suppoid=c.suppoid WHERE c.cmpcode='" & cmpcode & "' And " & sFilteNo.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%' " & sWhere & " ORDER BY c.trntcserviceoid DESC"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
        gvMaster.DataSource = dtab : gvMaster.DataBind()
        Session("ts") = dtab
    End Sub

    Private Sub BindItem()
        If pooid.Text = "" Then
            showMessage("Maaf, Anda belum memilih Nomer TW...!!!", 2)
            Exit Sub
        End If
        sSql = "Select * from (Select 'False' AS checkvalue, twm.Trfwhserviceoid, a.itemoid, a.itemcode, a.itemdesc, a.merk, a.satuan1, 'BUAH' as unit, twm.TrfwhserviceNo, twd.trfwhservicedtlseq, twd.trfwhserviceqty QtyTw, ISNULL(QtyTc,0.00) QtyTc, Isnull(twd.trfwhserviceqty,0.00)-ISNULL(QtyTc,0.00) SisaQty, Isnull(twd.trfwhserviceqty,0.00)-ISNULL(QtyTc,0.00) Qty, twd.reqoid, twd.reqdtloid, req.reqcode, twd.trfwhservicedtloid, twd.trfwhservicedtlnote, twm.FromBranch From ql_mstitem a Inner join ql_trfwhservicedtl twd on a.itemoid = twd.itemoid inner join ql_trfwhservicemst twm on twm.cmpcode = twd.cmpcode and twd.trfwhserviceoid=twm.Trfwhserviceoid Inner Join QL_TRNREQUEST req On twd.reqoid=req.reqoid LEFT JOIN (Select ISNULL(SUM(trntcserviceqty),0.0) QtyTC, tcd.itemoid, tcd.trfwhservicedtloid, tcd.trfwhserviceoid, tcd.reqdtloid from QL_trntcsuppdtl tcd Group BY tcd.itemoid, tcd.trfwhservicedtloid, tcd.trfwhserviceoid, tcd.reqdtloid) tcd ON tcd.itemoid=twd.itemoid AND tcd.trfwhservicedtloid=twd.trfwhservicedtloid AND tcd.trfwhserviceoid=twd.trfwhserviceoid AND tcd.reqdtloid=twd.reqdtloid Where a.cmpcode = '" & cmpcode & "' And twm.FromBranch='" & TujuanCb.SelectedValue & "' AND twm.suppoid=" & SuppOid.Text & " AND twm.Trfwhservicetype='EXTERNAL' And twm.TrfwhserviceNo like '%" & Tchar(noref.Text) & "%' ) td Where " & DDLFilterItem.SelectedValue & " LIKE '%" & Tchar(txtFilterItem.Text) & "%' AND td.Trfwhserviceoid=" & pooid.Text & " AND SisaQty>0.00"
        Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                dt.Rows(i)("Qty") = Math.Round(dt.Rows(i)("Qty"), 2)
            Next
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitUnitDDL()
        sSql = "Select genoid,gendesc from ql_mstgen where gengroup='ITEMUNIT'"
        FillDDL(unit, sSql)
    End Sub

    Private Sub InitLocDDL()
        sSql = "Select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & TujuanCb.SelectedValue & "' and gengroup = 'CABANG')"

        If TypeGudang.Text = "TITIPAN" Then
            sSql &= "AND a.genother6 IN ('TITIPAN')"
        ElseIf TypeGudang.Text = "RUSAK" Then
            sSql &= "AND a.genother6 IN ('RUSAK','UMUM')"
        Else
            sSql &= "AND a.genother6 IN ('RUSAK','UMUM','TITIPAN')"
        End If

        FillDDL(fromlocation, sSql)
        If fromlocation.Items.Count = 0 Then
            showMessage("Maaf, Gudang belum ada..!!", 3)
            Exit Sub
        End If
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Sub FillTextbox(ByVal lCabang As String, ByVal OidTcs As Integer)
        Try
            sSql = "SELECT TOP 1 tcm.branch_code, tcm.trntcserviceoid, twm.Trfwhserviceoid, tcm.trftcserviceno, tcm.trntcservicedate, twm.TrfwhserviceNo, tcm.trntcstatus, tcm.mtrlocoid, tcm.trntcnote, tcm.updtime, tcm.upduser, twm.FromBranch, sp.suppoid, sp.suppname, tcm.createtime FROM QL_trntcsuppmst tcm Inner Join ql_trfwhservicemst twm ON twm.Trfwhserviceoid=tcm.trfwhserviceoid AND tcm.branch_code=twm.FromBranch Inner Join QL_mstsupp sp ON sp.suppoid=tcm.suppoid WHERE tcm.cmpcode = '" & cmpcode & "' AND tcm.trntcserviceoid = " & OidTcs & " AND tcm.branch_code='" & lCabang & "'"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim transdate As New Date
            xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    OidTC.Text = Integer.Parse(xreader("trntcserviceoid"))
                    TujuanCb.SelectedValue = xreader("branch_code").ToString
                    transferno.Text = xreader("trftcserviceno").ToString
                    transdate = xreader("trntcservicedate")
                    SuppOid.Text = Integer.Parse(xreader("suppoid"))
                    SuppName.Text = xreader("suppname").ToString
                    transferdate.Text = Format(xreader("trntcservicedate"), "dd/MM/yyyy")
                    noref.Text = xreader("TrfwhserviceNo").ToString
                    pooid.Text = Integer.Parse(xreader("Trfwhserviceoid"))
                    InitLocDDL()
                    fromlocation.SelectedValue = xreader("mtrlocoid")
                    note.Text = xreader("trntcnote").ToString
                    trnstatus.Text = xreader("trntcstatus").ToString.Trim
                    upduser.Text = xreader("upduser").ToString
                    updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                    createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
                End While
            End If
            xreader.Close() : conn.Close()

            sSql = "Select tcd.seq,it.itemoid,it.itemcode,it.itemdesc,tcd.trntcserviceqty,tcd.reqdtloid,tcd.reqoid,gu.gendesc unit,it.satuan1 satuan,tcd.trntcnotedtl,tcd.trfwhserviceoid,tcd.trfwhservicedtloid,tcd.trntcservicedtloid From QL_trntcsuppdtl tcd INNER JOIN QL_trntcsuppmst tcm ON tcd.trntcserviceoid=tcm.trntcserviceoid AND tcd.branch_code=tcm.branch_code Inner Join ql_mstitem it ON it.itemoid=tcd.itemoid Inner Join QL_mstgen gu On gu.genoid=it.satuan1 And gu.gengroup='ITEMUNIT' Where tcm.branch_code='" & lCabang & "' AND tcm.trntcserviceoid=" & OidTcs & " ORDER BY tcd.seq"
            Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailts")
            Session("itemdetail") = dtab
            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
            If trnstatus.Text = "IN PROCESS" Then
                btnSave.Visible = True : btnDelete.Visible = True
                BtnCancel.Visible = True : btnPosting.Visible = True
            ElseIf trnstatus.Text = "POST" Then
                btnSave.Visible = False : btnDelete.Visible = False
                BtnCancel.Visible = True : btnPosting.Visible = False
            ElseIf trnstatus.Text = "In Process" Then
                btnSave.Visible = True : btnDelete.Visible = True
                BtnCancel.Visible = True : btnPosting.Visible = True
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnTcSupplier.aspx")
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")
        Page.Title = CompnyName & " - Transfer Confirm Supplier"

        If Not Page.IsPostBack Then
            fCabang() : BindData("") : InitUnitDDL()
            transferno.Text = GenerateID("ql_InTransfermst", cmpcode)
            transferdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            TujuanCabDDL() : InitLocDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox(Session("branch_code"), Session("oid"))
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0.00" : GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        ddlStatus.SelectedValue = "ALL"
        drCabang.SelectedValue = "SEMUA BRANCH"
        CbTanggal.Checked = False : FilterText.Text = ""
        tgl1.Text = Format(GetServerTime(), "dd/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If CbTanggal.Checked = True Then
            If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
                showMessage("Tanggal period harus diisi !", 2)
                Exit Sub
            End If

            Dim date1, date2 As New Date
            If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
                showMessage("Tanggal period 1 tidak valid !", 2)
                Exit Sub
            End If
            If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
                showMessage("Tanggal period 2 tidak valid !", 2)
                Exit Sub
            End If

            If date1 > date2 Then
                showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
                Exit Sub
            End If
        End If
        BindData("")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If sValidasi.Text <> "" Then
            UpdateCheckedMat()
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.Visible = True
            sValidasi.Text = ""
            mpeItem.Show()
        End If
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            If pooid.Text = "" Then
                showMessage("Maaf, Anda belum Pilih Nomer TW..!!", 2)
            Else
                Session("TblListMat") = Nothing
                Session("TblListMatView") = Nothing
                GVItemList.DataSource = Session("TblListMat")
                GVItemList.DataBind() : BindItem()
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            End If
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = "" : qty.Text = "0.0000"
        MaxQty.Text = "0.0000" : notedtl.Text = ""
        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        GVItemList.PageIndex = e.NewPageIndex
        BindItem() : GVItemList.Visible = True
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        'item.Text = GVItemList.SelectedDataKey("itemdesc").ToString
        'labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        'UnitOid.Text = GVItemList.SelectedDataKey("satuan1")
        'trfmtrmstoid.Text = GVItemList.SelectedDataKey("Trfwhserviceoid")
        'TrfWhDtlOid.Text = GVItemList.SelectedDataKey("trfwhservicedtloid")
        'sSql = "Select genoid,gendesc from ql_mstgen where gengroup='ITEMUNIT'"
        'FillDDL(unit, sSql)
        'unit.SelectedValue = GVItemList.SelectedDataKey("satuan")
        'ReqOid.Text = GVItemList.SelectedDataKey("reqdtloid")
        'ReqDtlOid.Text = GVItemList.SelectedDataKey("trfwhservicedtloid")
        'qty.Text = ToMaskEdit(GVItemList.SelectedDataKey("QtyTw"), 4)
        'MaxQty.Text = ToMaskEdit(GVItemList.SelectedDataKey("QtyTw"), 4)

        'If Session("itemdetail") Is Nothing = False Then
        '    Dim objTable As DataTable = Session("itemdetail")
        '    labelseq.Text = objTable.Rows.Count + 1
        'Else
        '    labelseq.Text = 1
        'End If

        'GVItemList.Visible = False
        'GVItemList.DataSource = Nothing
        'GVItemList.DataBind()
        'unit_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnTcSupplier.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        qty.Text = ToMaskEdit(ToDouble(qty.Text), 4)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim sErr As String = ""
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            sErr &= "- Maaf, Item tidak boleh kosong..!!<br />"
        End If

        If Double.Parse(qty.Text.Trim) = 0.0 Then
            sErr &= "- Maaf, Qty harus lebih dari 0..!!!<br />"
        End If

        If ToDouble(qty.Text.Trim) > ToDouble(MaxQty.Text.Trim) Then
            sErr &= "- Qty tidak boleh lebih dari Nilai Max Quantity..!!!<br />"
        End If

        Dim dtab As DataTable
        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = SetTblDetail()
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1)
            End If
        Else
            dtab = Session("itemdetail")
        End If

        'If dtab.Rows.Count > 0 Then
        '    Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & "")
        '    If drowc.Length > 0 Then
        '        sErr &= "Maaf, Item tidak bisa ditambahkan ke dalam list, Item ini telah ada di dalam list..!!!<br />"
        '    End If
        'End If

        If sErr <> "" Then
            showMessage(sErr, 2)
            sErr = "" : Exit Sub
        End If
        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text
            drow("note") = notedtl.Text.Trim
            drow("trfmtrmstoid") = Double.Parse(trfmtrmstoid.Text)
            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()

            drow("seq") = labelseq.Text
            drow("trfwhserviceoid") = Integer.Parse(pooid.Text)
            drow("trfwhservicedtloid") = Integer.Parse(TrfWhDtlOid.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("reqoid") = Integer.Parse(ReqOid.Text)
            drow("reqdtloid") = Integer.Parse(ReqDtlOid.Text)
            drow("trntcserviceqty") = Double.Parse(qty.Text)
            drow("trntcnotedtl") = notedtl.Text.Trim.ToString
            drow("unit") = unit.SelectedItem.Text
            drow("satuan") = Integer.Parse(unit.SelectedValue)
            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
        labelseq.Text = (GVItemDetail.Rows.Count + 1)
        qty.Text = "0.0000" : MaxQty.Text = "0.0000"
        unit.Items.Clear() : I_u2.Text = "new"
        notedtl.Text = "" : labelitemoid.Text = "" : item.Text = ""
        trfmtrmstoid.Text = "" : GVItemDetail.SelectedIndex = -1
        'GVItemDetail.Columns(7).Visible = True

        'If GVItemList.Visible = True Then
        '    GVItemList.DataSource = Nothing
        '    GVItemList.DataBind()
        '    GVItemList.Visible = False
        'End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0.00"
        item.Text = "" : MaxQty.Text = "0.00"
        unit.Items.Clear()
        notedtl.Text = "" : trfmtrmstoid.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        'GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If
        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("itemoid"))
        item.Text = GVItemDetail.SelectedDataKey("itemdesc").ToString
        qty.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("trntcserviceqty")), 4)
        notedtl.Text = GVItemDetail.SelectedDataKey("trntcnotedtl").ToString
        Dim Sat As String = GVItemDetail.SelectedDataKey("satuan")
        sSql = "Select genoid,gendesc from ql_mstgen where gengroup='ITEMUNIT'"
        FillDDL(unit, sSql)
        unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")
        labelseq.Text = GVItemDetail.SelectedDataKey("seq")
        ReqOid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("reqoid"))
        ReqDtlOid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("reqdtloid"))
        TrfWhDtlOid.Text = Integer.Parse(GVItemDetail.SelectedDataKey("trfwhservicedtloid"))
        sSql = "Select Case When isnull(twd.trfwhserviceqty,0.0000)-ISNULL((Select Isnull(tcd.trntcserviceqty,0.0000) from QL_trntcsuppdtl tcd Where tcd.trfwhservicedtloid=tcd.trfwhservicedtloid AND tcd.trfwhserviceoid=twd.trfwhserviceoid And tcd.itemoid=twd.itemoid),0.0000)=0 Then twd.trfwhserviceqty Else isnull(twd.trfwhserviceqty,0.0000)-ISNULL((Select Isnull(tcd.trntcserviceqty,0.0000) from QL_trntcsuppdtl tcd Where tcd.trfwhservicedtloid=tcd.trfwhservicedtloid AND tcd.trfwhserviceoid=twd.trfwhserviceoid And tcd.itemoid=twd.itemoid),0.0000) End From ql_trfwhservicedtl twd Where twd.trfwhservicedtloid=" & TrfWhDtlOid.Text & " AND twd.trfwhserviceoid=" & pooid.Text & " AND twd.itemoid=" & labelitemoid.Text & ""
        MaxQty.Text = ToMaskEdit(ToDouble(GetScalar(sSql)), 4)
        'GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        'Dim transdate As Date = Format(GetServerTime(), "dd/MM/yyyy")
        Dim period As String = GetDateToPeriodAcctg(GetServerTime())
        'If transdate < CDate(Session("tgltranswh")) Then
        '    errmsg &= "Maaf, Tanggal Transaksi Tidak Boleh Kurang dari Tanggal Transfer TW..!! <br />"
        'End If

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Maaf, Lokasi asal transfer tidak boleh kosong ! <br />"
        End If

        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Maaf, Detail transfer tidak boleh kosong ! <br />"
        End If

        'Cek period is open stock
        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
        If GetScalar(sSql) = 0 Then
            errmsg &= "Maaf, Tanggal ini tidak dalam periode Open Stock..!!<br />"
        End If

        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
        If GetStrData(sSql) = 0 Then
            errmsg &= "- Maaf, Maaf, Tanggal ini tidak dalam periode Open Stock..!!<br />"
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trntcsuppmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                errmsg &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<BR />"
            End If
        Else

            sSql = "SELECT trntcstatus FROM QL_trntcsuppmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            Dim srest As String = GetStrData(sSql)
            If srest Is Nothing Or srest = "" Then
                errmsg &= "- Maaf, Data service tidak ditemukan..!!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    errmsg &= "- Maaf, Data sudah terinput, klik tombol cancel..!!<br />Periksa bila data telah dalam status 'Post'..!!<br>"
                End If
            End If

        End If

        If errmsg.Trim <> "" Then
            trnstatus.Text = "IN PROCESS"
            showMessage(errmsg, 2)
            Exit Sub
        End If

        Dim trfmtrmstoid As Int32 = GenerateID("QL_trntcsuppmst", cmpcode)
        Dim trfmtrdtloid As Int32 = GenerateID("QL_trntcsuppdtl", cmpcode)
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", cmpcode)
        Dim crdmtroid As Int32 = GenerateID("ql_crdmtr", cmpcode)
        Dim tempoid As Int32 = GenerateID("ql_crdmtr", cmpcode)
        Dim glsequence As Integer = 1

        'Generate transfer number
        If trnstatus.Text = "POST" Then
            transferno.Text = GenerateNoPost(TujuanCb.SelectedValue)
        Else
            If i_u.Text = "new" Then
                transferno.Text = trfmtrmstoid
            End If
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'Insert new record
            If i_u.Text = "new" Then
                sSql = "INSERT INTO [QL_trntcsuppmst] ([cmpcode], [trntcserviceoid], [branch_code], [trftcserviceno], [trfwhserviceoid], [trntcservicedate], [suppoid], [mtrlocoid], [trntcstatus], [trntcres1], [trntcres2], [trntcres3], [trntcnote], [createtime], [createuser], [updtime], [upduser]) VALUES " & _
                "('" & cmpcode & "', " & Integer.Parse(trfmtrmstoid) & ", '" & TujuanCb.SelectedValue & "', '" & transferno.Text & "', " & pooid.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & SuppOid.Text & ", " & fromlocation.SelectedValue & ", '" & trnstatus.Text & "', '', '', '', '" & Tchar(note.Text) & "', '" & CDate(toDate(createtime.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'QL_trntcsuppmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                'Update master record
                sSql = "UPDATE [QL_trntcsuppmst] SET [trftcserviceno] = '" & transferno.Text & "', [trfwhserviceoid] = " & Tchar(pooid.Text) & ", [trntcservicedate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [suppoid]=" & SuppOid.Text & ", [mtrlocoid]=" & fromlocation.SelectedValue & ", [trntcstatus] = '" & trnstatus.Text & "', [trntcnote]='" & Tchar(note.Text) & "', [updtime] =CURRENT_TIMESTAMP, [upduser]= '" & Session("UserID") & "' WHERE trntcserviceoid=" & Integer.Parse(Session("oid"))
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_trntcsuppdtl WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0, qty_to As Double = 0.0, amthpp As Double = 0.0 : Dim COA_gudang As Integer = 0, vargudang As String = "", iMatAccount As String = ""
                'Dim GudangCoa As Integer
                For i As Integer = 0 To objTable.Rows.Count - 1
                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If
                    qty_to = ToDouble(objTable.Rows(i).Item("trntcserviceqty"))
                    sSql = "INSERT INTO [QL_trntcsuppdtl] ([cmpcode], [branch_code], [trntcservicedtloid], [trntcserviceoid], [trfwhserviceoid], [trfwhservicedtloid], [reqoid], [reqdtloid], [itemoid], [mtrlocoid], [trntcserviceqty], [trntcstatusdtl], [trntcresdtl1], [trntcresdtl2], [trntcresdtl3], [trntcnotedtl], [updtime], [upduser], [seq]) VALUES " & _
                    "('" & cmpcode & "', '" & TujuanCb.SelectedValue & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & pooid.Text & ", " & Integer.Parse(objTable.Rows(i).Item("trfwhservicedtloid")) & ", " & Integer.Parse(objTable.Rows(i).Item("reqoid")) & ", " & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ", " & Integer.Parse(objTable.Rows(i).Item("itemoid")) & ", " & fromlocation.SelectedValue & ", " & ToDouble(objTable.Rows(i).Item("trntcserviceqty")) & ", 'IN PROCESS', '', '', '', '" & Tchar(objTable.Rows(i).Item("trntcnotedtl")) & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', " & Integer.Parse(objTable.Rows(i).Item("seq")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then
                        '--------------------------
                        sSql = "select hpp From ql_mstitem Where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql
                        Dim lasthpp As Double = xCmd.ExecuteScalar
                        '---------------------------
                        amthpp = qty_to * lasthpp
                        sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, Branch_Code, conrefoid) VALUES " & _
                        "('" & cmpcode & "', " & Integer.Parse(conmtroid) & ", 'TKS', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & transferno.Text & "', " & Integer.Parse(trfmtrdtloid) & ", 'QL_trntcsuppdtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", " & ToDouble(objTable.Rows(i).Item("trntcserviceqty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(objTable.Rows(i).Item("trntcnotedtl")) & "', " & ToDouble(lasthpp) & ", '" & TujuanCb.SelectedValue & "', " & Integer.Parse(objTable.Rows(i).Item("reqdtloid")) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                    End If
                    trfmtrdtloid += 1

                Next
                'Update lastoid ql_InTransferDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & Integer.Parse(trfmtrdtloid) - 1 & " WHERE tablename = 'QL_trntcsuppdtl' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & Integer.Parse(crdmtroid) - 1 & " WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & Integer.Parse(conmtroid) - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close() : trnstatus.Text = "IN PROCESS"
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTcSupplier.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sCerr As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT trntcstatus FROM QL_trntcsuppmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql : Dim srest As String = xCmd.ExecuteScalar

            If srest Is Nothing Or srest = "" Then
                sCerr &= " - Maaf Data Ini tidak bisa dihapus, cek data apakah data sudah terposting..!!<br />"
            Else
                If srest = "POST" Then
                    sCerr &= " - Maaf Data Ini tidak bisa dihapus, cek data apakah data sudah terposting..!!<br />"
                End If
            End If

            If sCerr <> "" Then
                objTrans.Rollback() : conn.Close()
                showMessage(sCerr, 2)
                Exit Sub
            End If

            sSql = "DELETE FROM QL_trntcsuppmst WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trntcsuppdtl WHERE trntcserviceoid = " & Integer.Parse(Session("oid")) & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTcSupplier.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new" : labelitemoid.Text = "" : qty.Text = "0.00"
        item.Text = "" : MaxQty.Text = "0.00" : unit.Items.Clear()
        notedtl.Text = "" : labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
        btnClear_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim tcOid As Integer = sender.ToolTip
            'Dim sWhere As String = " where tcm.trntcserviceoid = " & Integer.Parse(tcOid) & ""
            'report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "PrintOutTCS.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", Integer.Parse(tcOid))
            'report.SetParameterValue("userID", Session("UserID"))
            'report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TransferConfirmServicePrintOut_" & tcOid & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnTCSupplier.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        gvMaster.PageIndex = e.NewPageIndex
        BindData("")
    End Sub

    Protected Sub ibposearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibposearch.Click
        bindDataPO() : GVpo.Visible = True
    End Sub

    Protected Sub ibpoerase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoerase.Click
        noref.Text = "" : pooid.Text = ""

        If GVpo.Visible = True Then
            GVpo.Visible = False
            GVpo.DataSource = Nothing
            GVpo.DataBind()
            Session("polistwhsupplier") = Nothing
        End If

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If

        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhsupplier") Is Nothing Then
            GVpo.DataSource = Session("polistwhsupplier")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        TujuanCb.SelectedValue = GVpo.SelectedDataKey("toMtrBranch")
        GVpo.Visible = False : GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhsupplier") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub TujuanCb_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TujuanCb.SelectedIndexChanged
        InitLocDDL() : GVpo.Visible = False
        pooid.Text = "" : noref.Text = ""
    End Sub

    Protected Sub ImgBtnSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSupp.Click
        BindSuppData()
    End Sub

    Protected Sub ImgBtnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnErase.Click
        SuppOid.Text = "" : SuppName.Text = ""
        GVSupp.Visible = False
    End Sub

    Protected Sub GVSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSupp.PageIndexChanging
        GVSupp.PageIndex = e.NewPageIndex : BindSuppData()
    End Sub

    Protected Sub GVSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVSupp.SelectedIndexChanged
        SuppOid.Text = GVSupp.SelectedDataKey("suppoid")
        SuppName.Text = GVSupp.SelectedDataKey("suppname")
        GVSupp.Visible = False
    End Sub

    Protected Sub GVpo_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        pooid.Text = GVpo.SelectedDataKey("Trfwhserviceoid")
        noref.Text = GVpo.SelectedDataKey("Trfwhserviceno").ToString
        Session("tgltranswh") = GVpo.SelectedDataKey("Trfwhservicedate")
        TypeGudang.Text = GVpo.SelectedDataKey("TypeGudang").ToString
        InitLocDDL()
        GVpo.Visible = False
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "" & DDLFilterItem.SelectedValue & " LIKE '%" & Tchar(txtFilterItem.Text) & "%' AND FromBranch='" & TujuanCb.SelectedValue & "'"

                'dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub imbViewItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewItem.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                DDLFilterItem.SelectedIndex = -1
                txtFilterItem.Text = ""
                Session("TblListMatView") = Session("TblListMat")
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind()
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "Maaf, data yang anda maksud tidak ada..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "checkvalue='True'"

            If dv.Count > 0 Then
                For q As Integer = 0 To dv.Count - 1
                    If ToDouble(dv(q)("Qty")) > ToDouble(dv(q)("SisaQty")) Then
                        sValidasi.Text &= "- Maaf, Qty " & dv(q)("itemdesc") & " yang anda input melebihi jumlah qty tw..!!<br />"
                    End If

                    If ToDouble(dv(q)("Qty")) = 0 Then
                        sValidasi.Text &= "- Maaf, Qty " & dv(q)("itemdesc") & " belum di isi..!!<br />"
                    End If
                Next
            End If

            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, 2)
                Exit Sub
            End If

            If dv.Count > 0 Then
                '--Create Tbl detail--
                '=====================
                Dim dtab As DataTable
                If Session("itemdetail") Is Nothing Then
                    dtab = SetTblDetail() : Session("itemdetail") = dtab
                    labelseq.Text = 0 + 1
                Else
                    dtab = Session("itemdetail")
                    'labelseq.Text = dtab.Rows.Count + 1
                End If

                labelseq.Text = 0 + 1
                Dim dFuck As DataView = dtab.DefaultView
                For d As Integer = 0 To dv.Count - 1
                    dFuck.RowFilter = " Itemoid=" & Integer.Parse(dv(d)("itemoid")) & " And trfwhservicedtloid=" & Integer.Parse(dv(d)("trfwhservicedtloid")) & ""

                    Dim drow As DataRow
                    If dFuck.Count <= 0 Then
                        drow = dtab.NewRow
                        drow("seq") = labelseq.Text
                        drow("trfwhserviceoid") = Integer.Parse(dv(d)("trfwhserviceoid"))
                        drow("trfwhservicedtloid") = Integer.Parse(dv(d)("trfwhservicedtloid"))
                        drow("itemoid") = Integer.Parse(dv(d)("itemoid"))
                        drow("itemdesc") = dv(d)("itemdesc").ToString
                        drow("reqoid") = Integer.Parse(dv(d)("reqoid"))
                        drow("reqdtloid") = Integer.Parse(dv(d)("reqdtloid"))
                        drow("trntcserviceqty") = ToDouble(dv(d)("qty"))
                        drow("trntcnotedtl") = dv(d)("trfwhservicedtlnote").ToString
                        drow("unit") = dv(d)("unit").ToString
                        drow("satuan") = Integer.Parse(dv(d)("satuan1"))
                        dtab.Rows.Add(drow) : dtab.AcceptChanges()
                    Else
                        Dim drowedit() As DataRow
                        drowedit = dtab.Select("itemoid = " & Integer.Parse(dv(d)("itemoid")) & " And trfwhservicedtloid=" & Integer.Parse(dv(d)("trfwhservicedtloid")) & "", "")
                        drow = drowedit(0)
                        drowedit(0).BeginEdit()
                        drow("trfwhserviceoid") = Integer.Parse(dv(d)("trfwhserviceoid"))
                        drow("trfwhservicedtloid") = Integer.Parse(dv(d)("trfwhservicedtloid"))
                        drow("itemoid") = Integer.Parse(dv(d)("itemoid"))
                        drow("itemdesc") = dv(d)("itemdesc").ToString
                        drow("reqoid") = Integer.Parse(dv(d)("reqoid"))
                        drow("reqdtloid") = Integer.Parse(dv(d)("reqdtloid"))
                        drow("trntcserviceqty") = ToDouble(dv(d)("qty"))
                        drow("trntcnotedtl") = dv(d)("trfwhservicedtlnote").ToString
                        drow("unit") = dv(d)("unit").ToString
                        drow("satuan") = Integer.Parse(dv(d)("satuan1"))
                        drowedit(0).EndEdit()
                        dtab.Select(Nothing, Nothing)
                        dtab.AcceptChanges()
                    End If
                    drow.EndEdit()
                    labelseq.Text += 1
                    dFuck.RowFilter = ""
                Next
                btnHideItem.Visible = False : panelItem.Visible = False : mpeItem.Show()
                GVItemDetail.DataSource = dtab
                GVItemDetail.DataBind()
                Session("itemdetail") = dtab
            Else
                sValidasi.Text = "- Maaf, Anda belum memilih katalog sama sekali, pilih dan isi jumlah Qty nya...!!<br />"
                If sValidasi.Text <> "" Then
                    showMessage(sValidasi.Text, 2)
                    Exit Sub
                End If
            End If

        End If
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        txtFilterItem.Text = ""
        btnHideItem.Visible = False : panelItem.Visible = False
    End Sub

    Protected Sub gvMaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaster.SelectedIndexChanged
        Response.Redirect("trnTcSupplier.aspx?branch_code=" & gvMaster.SelectedDataKey("branch_code").ToString & "&oid=" & gvMaster.SelectedDataKey("trntcserviceoid") & "")
    End Sub
#End Region
End Class
