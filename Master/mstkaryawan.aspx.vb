' Programmer: ^Valz_niK^ '
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_mstkaryawan
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public EmployeeImageUrl As String = "~/Images/EmpImage/"
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure

    Private mImageFile As Image
    Private mImageFilePath As String
    Dim oMatches As MatchCollection

    Dim toexcel As Boolean = False
    Dim sWhereCari As String
    Dim report As New ReportDocument
    Dim folderReport As String = "~/Report/"
    Dim vReport As New ReportDocument
#End Region

#Region "Procedure"
    Sub initdtlprovince()
        sSql = "select genoid,gendesc from QL_mstgen where gengroup IN ('PROVINCE') and (genother1 = '" & ddlDtlCountry.SelectedValue & "' OR genother1 = '')and cmpcode like '%" & CompnyCode & "%' order by gendesc"
        FillDDL(ddlDtlProvince, sSql)
    End Sub

    Sub initdtlcity()
        sSql = "select genoid,gendesc from QL_mstgen where gengroup IN ('CITY') and (genother2 = '" & ddlDtlProvince.SelectedValue & "' or genother2 = '') and (genother1 ='" & ddlDtlCountry.SelectedValue & "' or genother1 ='') and cmpcode like '%" & CompnyCode & "%' order by gendesc"
        FillDDL(ddlDtlCity, sSql)
    End Sub

    Sub initdtlcountry()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='COUNTRY' order by gendesc"
        FillDDL(ddlDtlCountry, sSql)
    End Sub

    Sub ClearDetail()
        companyname.Text = ""
        divisi.Text = ""
        startperiod.Text = Format(Now, "dd/MM/yyyy")
        endperiod.Text = Format(Now, "dd/MM/yyyy")
        historyseq.Text = "1"
        initdtlcountry() : initdtlprovince() : initdtlcity()
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            historyseq.Text = objTable.Rows.Count + 1
        End If
    End Sub

    'Public Sub LoadPic()
    '    Try
    '        If (Me.txtTitle.Text = String.Empty) Then
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '    End Try

    '    Dim fs As FileStream = New FileStream(mImageFilePath.ToString(), FileMode.Open)
    '    Dim img As Byte() = New Byte(fs.Length) {}
    '    fs.Read(img, 0, fs.Length)
    '    fs.Close()

    '    'Dim saveImageUrl As String = ""
    '    'If Session("uploadImage_mstkaryawan") IsNot Nothing Then
    '    '    saveImageUrl = EmployeeImageUrl & personoid.Text & "." & Session("uploadImage_mstkaryawan")
    '    '    If File.Exists(Server.MapPath(saveImageUrl)) Then
    '    '        File.Delete(Server.MapPath(saveImageUrl))
    '    '    End If
    '    '    File.Copy(Server.MapPath(EmployeeImageUrl & "temp"), Server.MapPath(saveImageUrl))
    '    'Else
    '    '    saveImageUrl = lblpersonpicture.Text
    '    'End If

    '    mImageFile = Image.FromFile(mImageFilePath.ToString())

    '    Dim imgHeight As Integer = mImageFile.Height
    '    Dim imgWidth As Integer = mImageFile.Width
    '    Dim imgLength As Integer = mImageFile.PropertyItems.Length
    '    Dim imgType As String = Path.GetExtension(mImageFilePath)

    '    mImageFile = Nothing

    '    Dim strConnect As String
    '    strConnect = "Data Source=bxswlt;Initial Catalog=ImageGallery;User ID=sa"
    '    Dim conn As SqlConnection = New SqlConnection(strConnect)

    '    Dim sSQL As String = "INSERT INTO ImageCollection (ImageContent, ImageTitle, ImageType, ImageHeight, ImageWidth) VALUES(" & _

    '            "@pic, @title, @itype, @iheight, @iwidth)"



    '    Dim cmd As SqlCommand = New SqlCommand(sSql, conn)



    '    ' image content

    '    Dim pic As SqlParameter = New SqlParameter("@pic", SqlDbType.Image)

    '    pic.Value = img

    '    cmd.Parameters.Add(pic)



    '    ' title

    '    Dim title As SqlParameter = New SqlParameter("@title", System.Data.SqlDbType.VarChar, 50)
    '    title.Value = txtTitle.Text.ToString()
    '    cmd.Parameters.Add(Title)

    '    ' type
    '    Dim itype As SqlParameter = New SqlParameter("@itype", System.Data.SqlDbType.Char, 4)
    '    itype.Value = imgType.ToString()
    '    cmd.Parameters.Add(itype)

    '    ' height
    '    Dim iheight As SqlParameter = New SqlParameter("@iheight", System.Data.SqlDbType.Int)
    '    iheight.Value = imgHeight
    '    cmd.Parameters.Add(iheight)

    '    ' width
    '    Dim iwidth As SqlParameter = New SqlParameter("@iwidth", System.Data.SqlDbType.Int)
    '    iwidth.Value = imgWidth
    '    cmd.Parameters.Add(iwidth)

    '    Try

    '        conn.Open()
    '        cmd.ExecuteNonQuery()
    '        conn.Close()
    '        MessageBox.Show("Query executed.", "Image Load")

    '    Catch ex As Exception
    '        Exit Sub

    '    End Try
    'End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        Image1.ImageUrl = "~/Images/warn.png"
        btnErrOK.ImageUrl = "~/Images/ok.png"
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub showImage(ByVal imageUrl As String)
        'Image1.Visible = False
        lblMessage.Visible = False
        lblCaption2.Text = "Image Full View"
        Image4.Width = 450
        Image4.Height = 400

        Image4.Visible = True
        Image4.ImageUrl = imageUrl
        beMsgBox.Visible = True
        PanelMsgBox.Visible = True
        mpeMsgbox.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere, oidtemp As String
        sWhere = "" : oidtemp = ""

        FilterText.Text = Session("FilterText_mstperson")
        FilterDDL.SelectedIndex = Session("FilterDDL_mstperson")

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sWhere = " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "

        If chkTitle.Checked Then
            sWhere &= (IIf(sWhere = "", " WHERE ", " AND ") & " persontitleoid='" & FilterTitle.SelectedValue & "'")
        End If

        If chkDept.Checked Then
            sWhere &= (IIf(sWhere = "", " WHERE ", " AND ") & " deptoid='" & FilterDept.SelectedValue & "'")
        End If

        'disimpan ke dalam session untuk keperluan print
        Session("sWhere_mstperson") = sWhere
        sWhereCari = sWhere

        If sCompCode <> "" Then
            sSql = "SELECT p.personoid, p.nip, p.personname, (select gendesc from QL_mstgen where genoid=p.persontitleoid) as persontitle, (select gendesc from QL_mstgen where genoid=p.deptoid) as division FROM QL_mstperson p where 1=1 " & sWhere & " and upper(p.cmpcode)='" & sCompCode.ToUpper & "' ORDER BY p.personoid"
        Else
            sSql = "SELECT p.personoid, p.nip, p.personname, (select gendesc from QL_mstgen where genoid=p.persontitleoid) as persontitle, (select gendesc from QL_mstgen where genoid=p.deptoid) as division FROM QL_mstperson p where 1=1 " & sWhere & " ORDER BY p.personoid"
        End If


        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstperson.DataSource = objDs.Tables("data")
        GVmstperson.DataBind()
    End Sub

    Public Sub InitAllDDL()
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='CITY'"
        FillDDL(citytinggaloid, sSql)
        FillDDL(asalcityoid, sSql)
        FillDDL(tempatlahir, sSql)
        FillDDL(sekolahcityoid, sSql)
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='PERSON TITLE'"
        FillDDL(persontitleoid, sSql)
        FillDDL(FilterTitle, sSql)
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='DEPARTMENT'"
        FillDDL(deptoid, sSql)
        FillDDL(FilterDept, sSql)
        initdtlcountry() : initdtlprovince() : initdtlcity()
    End Sub

    Public Sub FillTextBox(ByVal vPersonoid As String)
        sSql = "SELECT cmpcode, personoid, nip, personname, persontitleoid, personsex, maritalstatus, convert(char(10),tglmasuk,103) tglmasuk, deptoid, noktp, tempatlahir, convert(char(10),tgllahir,103) tgllahir, alamattinggal, citytinggaloid, alamatasal, " & _
                "asalcityoid, phone1, phone2, emergencyphone, pendidikanterakhir, asalsekolah, sekolahcityoid, ijazah1, ijazah2, ijazah3, catatanlainlain, status, religion, personflag, personres1, " & _
                "personres2, amtcontract, personpicture, userprof, profpass, upduser, updtime,email FROM QL_mstperson where cmpcode='" & CompnyCode & "' and personoid = '" & vPersonoid & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            personoid.Text = Trim(xreader("personoid").ToString)
            nip.Text = Trim(xreader("nip").ToString)
            personname.Text = Trim(xreader("personname").ToString)
            persontitleoid.Text = Trim(xreader("persontitleoid").ToString)
            personsex.Text = Trim(xreader("personsex").ToString)
            maritalstatus.Text = Trim(xreader("maritalstatus").ToString)
            tglmasuk.Text = Trim(xreader("tglmasuk").ToString)
            deptoid.Text = Trim(xreader("deptoid").ToString)
            noktp.Text = Trim(xreader("noktp").ToString)
            tempatlahir.Text = Trim(xreader("tempatlahir").ToString)
            tgllahir.Text = Trim(xreader("tgllahir").ToString)
            alamattinggal.Text = Trim(xreader("alamattinggal").ToString)
            citytinggaloid.Text = Trim(xreader("citytinggaloid").ToString)
            alamatasal.Text = Trim(xreader("alamatasal").ToString)
            asalcityoid.Text = Trim(xreader("asalcityoid").ToString)
            phone1.Text = Trim(xreader("phone1").ToString)
            phone2.Text = Trim(xreader("phone2").ToString)
            emergencyphone.Text = Trim(xreader("emergencyphone").ToString)
            pendidikanterakhir.Text = Trim(xreader("pendidikanterakhir").ToString)
            asalsekolah.Text = Trim(xreader("asalsekolah").ToString)
            sekolahcityoid.Text = Trim(xreader("sekolahcityoid").ToString)
            ijazah1.Text = Trim(xreader("ijazah1").ToString)
            ijazah2.Text = Trim(xreader("ijazah2").ToString)
            ijazah3.Text = Trim(xreader("ijazah3").ToString)
            catatanlainlain.Text = Trim(xreader("catatanlainlain").ToString)
            status.Text = Trim(xreader("status").ToString)
            religion.Text = Trim(xreader("religion").ToString)
            amtcontract.Text = Trim(xreader("amtcontract").ToString)
            lblpersonpicture.Text = Trim(xreader("personpicture").ToString)
            Upduser.Text = Trim(xreader("upduser").ToString)
            Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss tt")
            email.Text = Trim(xreader("email").ToString)
            If Trim(xreader("personpicture").ToString) <> "" Then
                prspicture.ImageUrl = Trim(xreader("personpicture").ToString)
                prspicture.Visible = True
                lkbFullView.Visible = True
            End If
        End While
        xreader.Close()
        conn.Close()

        Session("TblDtl") = Nothing
        sSql = "SELECT historyseq,personoid,compname,position,country countryoid,province provinceoid,city cityoid, g1.gendesc country, g2.gendesc province, g3.gendesc city, convert(char(10),startperiod,103) startperiod, convert(char(10),endperiod, 103) endperiod FROM QL_mstpersonhist inner join Ql_mstgen g1 on g1.genoid =  country inner join Ql_mstgen g2 on g2.genoid =  province inner join Ql_mstgen g3 on g3.genoid =  city where personoid = " & personoid.Text & " order by historyseq"

        Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sSql, conn)
        Dim objTable As New DataTable
        mySqlDAdtl.Fill(objTable)
        Session("TblDtl") = objTable
        If objTable.Rows.Count > 0 Then
            tbldtl.Visible = True
            tbldtl.DataSource = objTable
            tbldtl.DataBind()
            historyseq.Text = objTable.Rows.Count + 1
        End If
    End Sub

    Public Sub GeneratePersonID()
        personoid.Text = GenerateID("QL_mstperson", CompnyCode)
    End Sub

    Sub clearItem()
        Upduser.Text = Session("UserID")
        Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
    End Sub

#End Region

#Region "Function"
    Private Function generatePersonCode(ByVal personname As String) As String
        Dim retVal As String = ""
        Dim personPrefix As String = personname.Substring(0, 3) ' 3 karakter nama supplier
        sSql = "select nip from QL_mstperson where nip like '" & personPrefix & "%' order by nip desc "
        Dim x As Object = cKon.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(personPrefix) & "001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(personPrefix) & "001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(3, 3))
                angka += 1
                retVal = UCase(personPrefix) & tambahNol(angka)
            End If
        End If

        Return retVal
    End Function

    Private Function tambahNol(ByVal angka As Integer) As String
        Dim retVal As String = ""
        For i As Integer = 0 To angka.ToString.Length
            retVal &= "0"
        Next
        retVal &= angka

        Return retVal
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("historyseq", Type.GetType("System.Int32"))
        dt.Columns.Add("personoid", Type.GetType("System.Int32"))
        dt.Columns.Add("compname", Type.GetType("System.String"))
        dt.Columns.Add("position", Type.GetType("System.String"))
        dt.Columns.Add("countryoid", Type.GetType("System.Int32"))
        dt.Columns.Add("provinceoid", Type.GetType("System.Int32"))
        dt.Columns.Add("cityoid", Type.GetType("System.Int32"))
        dt.Columns.Add("country", Type.GetType("System.String"))
        dt.Columns.Add("province", Type.GetType("System.String"))
        dt.Columns.Add("city", Type.GetType("System.String"))
        dt.Columns.Add("startperiod", Type.GetType("System.String"))
        dt.Columns.Add("endperiod", Type.GetType("System.String"))
        Return dt
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx?target=~/Master/mstkaryawan.aspx")
        End If
        Session.Timeout = 15
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session.Remove("oid_mstperson")
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~/master/mstkaryawan.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid_mstperson") = Request.QueryString("oid")
        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
        Page.Title = CompnyName & " - Master Person"

        If Not Page.IsPostBack Then
            MultiView1.ActiveViewIndex = 0
            BindData(CompnyCode)
            InitAllDDL()
            tglmasuk.Text = Format(Now, "dd/MM/yyyy") : tgllahir.Text = Format(Now, "dd/MM/yyyy")
            If Session("oid_mstperson") IsNot Nothing And Session("oid_mstperson") <> "" Then
                BtnDelete.Visible = True
                PanelUpdate.Visible = True
                FillTextBox(Session("oid_mstperson"))
                TabContainer1.ActiveTabIndex = 1
            Else
                GeneratePersonID()
                BtnDelete.Visible = False
                PanelUpdate.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText_mstperson") = FilterText.Text
        Session("FilterDDL_mstperson") = FilterDDL.SelectedIndex
        BindData(CompnyCode)

        'If GetStrData("select count(-1) from QL_mstperson where 1=1 " & sWhereCari) = 0 Then
        '    BtnPdf.Visible = False
        'Else
        '    BtnPdf.Visible = True
        'End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session.Remove("FilterText_mstperson")
        Session.Remove("FilterDDL_mstperson")
        chkTitle.Checked = False : chkDept.Checked = False
        FilterTitle.SelectedIndex = 0
        FilterDept.SelectedIndex = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstkaryawan.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        If personoid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill Person ID"))
            Exit Sub
        End If

        ''cek relationship database to table
        'Dim sColomnName() As String = {"custcityoid", "itemcategoryoid", "itemsubcategoryoid", "itemtypeoid", "itemunit1oid", "itemunit2oid", "matcategoryoid", "matsubcategoryoid", _
        '    "mattypeoid", "matmoqunit1oid", "matmoqunit2oid", _
        '    "MATMOQUNIT1UOM", "MATMOQUNIT2UOM", "MATMOQSGWUOM", "MATMOQSNWUOM", "MATMOQAGWUOM", "MATMOQANWUOM", _
        '    "MATSAFEUNIT1UOM", "MATSAFEUNIT2UOM", "MATSAFESGWUOM", "MATSAFESNWUOM", "MATSAFEAGWUOM", "MATSAFEANWUOM", _
        '    "MATSTOCKUNIT1UOM", "MATSTOCKUNIT2UOM", "MATSTOCKSGWUOM", "MATSTOCKSNWUOM", "MATSTOCKAGWUOM", "MATSTOCKANWUOM"}
        'Dim sTable() As String = {"QL_mstcust", "QL_mstitem", "QL_mstitem", "QL_mstitem", "QL_mstitem", "QL_mstitem", "QL_mstmat", _
        '    "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", _
        '    "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", _
        '    "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", _
        '    "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat", "QL_mstmat"}

        'If CheckDataExists(genoid.Text, sColomnName, sTable) = True Then
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "You cannot delete this data, cause this data used by another table!"))
        '    Exit Sub
        'End If

        Dim imageUrl As String = GetStrData("select personpicture from QL_mstperson where personoid=" & personoid.Text)

        If DeleteData("QL_mstperson", "personoid", personoid.Text, CompnyCode) = True Then
            If File.Exists(Server.MapPath(imageUrl)) Then
                File.Delete(Server.MapPath(imageUrl))
            End If
            GeneratePersonID()
            clearItem()
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data has been deleted!"))
        Else
            Exit Sub
        End If
        Session.Remove("oid_mstperson")
        Session.Remove("uploadImage_mstperson")
        Response.Redirect("~\Master\mstkaryawan.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If Session("oid_mstperson") = Nothing Or Session("oid_mstperson") = "" Then
            sSql = "SELECT COUNT(-1) FROM QL_mstperson WHERE cmpcode='" & CompnyCode & "' AND personoid='" & personoid.Text & "' "
            If cKon.ambilscalar(sSql) > 0 Then
                'GenerateNo()
            End If
        End If

        Dim errMessage As String = ""
        Dim personmstoid As Int32 = GenerateID("QL_mstperson", CompnyCode)
        Dim persondtloid As Int32 = GenerateID("QL_mstpersonhist", CompnyCode)

        If personoid.Text = "" Then
            errMessage &= "Please Fill Person ID!<BR>"
        End If
        If nip.Text.Trim = "" Then
            errMessage &= "Please Fill NIP!<BR>"
        End If
        If personname.Text.Trim = "" Then
            errMessage &= "Please Fill Person Name!<BR>"
        End If
        If alamattinggal.Text.Trim = "" Then
            errMessage &= "Please Fill Current Address!<BR>"
        End If
        If phone1.Text.Trim = "" Then
            errMessage &= "Please Fill Phone 1!<BR>"
        End If
        If Tchar(catatanlainlain.Text.Length) > 50 Then
            errMessage &= "Maximum 50 characters for Note!<BR>"
        End If

        Dim ConvertedDate1 As Date
        Dim st1 As Boolean
        Try
            ConvertedDate1 = CDate(tglmasuk.Text.Substring(3, 2) + "-" + tglmasuk.Text.Remove(2) + "-" + tglmasuk.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
            st1 = True
        Catch ex As Exception
            st1 = False
    End Try
        If Not st1 Then
            errMessage &= "Invalid Join Date!<BR>"
    End If



    If st1 Then
      If CheckYearIsValid(ToDate(tglmasuk.Text)) Then
        errMessage &= "Join Date can't Less than 2000 and can't More than 2072 <br />"
      End If
    End If

        Dim ConvertedDate2 As Date
        Dim st2 As Boolean
        Try
            ConvertedDate2 = CDate(tgllahir.Text.Substring(3, 2) + "-" + tgllahir.Text.Remove(2) + "-" + tgllahir.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
            st2 = True
        Catch ex As Exception
            st2 = False
    End Try


    'If st1 Then
    '  If CheckYearIsValid(ToDate(tglmasuk.Text)) Then
    '    errMessage &= "Birth Date can't Less than 2000 and can't More than 2072 <br />"
    '  End If
    'End If

        If Not st2 Then
            errMessage &= "Invalid Date of Birth!<BR>"
        End If

        'cek nip [QL_mstperson] yang kembar (nip tidak boleh kembar)
        Dim sSqlCheck As String = "SELECT COUNT(-1) FROM QL_mstperson WHERE nip = '" & Tchar(nip.Text) & "'"
        If Session("oid_mstperson") IsNot Nothing And Session("oid_mstperson") <> "" Then
            sSqlCheck &= " AND personoid <> " & personoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck) > 0 Then
            errMessage &= "NIP already used!<BR>"
        End If

        If email.Text <> "" Then
            oMatches = Regex.Matches(email.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                errMessage &= "- Invalid email, Ex : mail@sample.com !!<BR>"
            End If
        End If

        If errMessage <> "" Then
      showMessage(errMessage, "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If

    Dim objTrans As SqlClient.SqlTransaction
    If conn.State = ConnectionState.Closed Then
      conn.Open()
    End If
    objTrans = conn.BeginTransaction()
    xCmd.Transaction = objTrans
    Try
      Upduser.Text = Session("UserID")
      Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
      If amtcontract.Text = "" Then
        amtcontract.Text = 0
      End If

      Dim saveImageUrl As String = ""
      If Session("uploadImage_mstkaryawan") IsNot Nothing Then
        saveImageUrl = EmployeeImageUrl & personoid.Text & "." & Session("uploadImage_mstkaryawan")
        If File.Exists(Server.MapPath(saveImageUrl)) Then
          File.Delete(Server.MapPath(saveImageUrl))
        End If
        File.Copy(Server.MapPath(EmployeeImageUrl & "temp.jpg"), Server.MapPath(saveImageUrl))
      Else
        saveImageUrl = lblpersonpicture.Text
      End If

      'mode insert
      If Session("oid_mstperson") Is Nothing Or Session("oid_mstperson") = "" Then
        GeneratePersonID()
        sSql = "INSERT into QL_mstperson(" & _
        "cmpcode, " & _
        "personoid, " & _
        "nip, " & _
        "personname, " & _
        "persontitleoid, " & _
        "personsex, " & _
        "maritalstatus, " & _
        "tglmasuk, " & _
        "deptoid, " & _
        "noktp, " & _
        "tempatlahir, " & _
        "tgllahir, " & _
        "alamattinggal, " & _
        "citytinggaloid, " & _
        "alamatasal, " & _
        "asalcityoid, " & _
        "phone1, " & _
        "phone2, " & _
        "emergencyphone, " & _
        "pendidikanterakhir, " & _
        "asalsekolah, " & _
        "sekolahcityoid, " & _
        "ijazah1, " & _
        "ijazah2, " & _
        "ijazah3, " & _
        "catatanlainlain, " & _
        "status, " & _
        "religion, " & _
        "personflag, " & _
        "personres1, " & _
        "personres2, " & _
        "amtcontract, " & _
        "personpicture, " & _
        "userprof, " & _
        "profpass, " & _
        "upduser, " & _
        "updtime, " & _
        "email) " & _
        "VALUES (" & _
        "'" & CompnyCode & "'," & _
        " " & personoid.Text & "," & _
        "'" & Tchar(nip.Text) & "'," & _
        "'" & Tchar(personname.Text) & "'," & _
        "'" & persontitleoid.SelectedValue & "'," & _
        "'" & personsex.Text & "'," & _
        "'" & maritalstatus.Text & "'," & _
        "'" & ConvertedDate1 & "'," & _
        "'" & deptoid.SelectedValue & "'," & _
        "'" & Tchar(noktp.Text) & "'," & _
        "'" & tempatlahir.Text & "'," & _
        "'" & ConvertedDate2 & "'," & _
        "'" & Tchar(alamattinggal.Text) & "'," & _
        " " & citytinggaloid.SelectedValue & "," & _
        "'" & Tchar(alamatasal.Text) & "'," & _
        " " & asalcityoid.Text & "," & _
        "'" & Tchar(phone1.Text) & "'," & _
        "'" & Tchar(phone2.Text) & "'," & _
        "'" & Tchar(emergencyphone.Text) & "'," & _
        "'" & Tchar(pendidikanterakhir.Text) & "'," & _
        "'" & Tchar(asalsekolah.Text) & "'," & _
        " " & sekolahcityoid.Text & "," & _
        "'" & Tchar(ijazah1.Text) & "'," & _
        "'" & Tchar(ijazah2.Text) & "'," & _
        "'" & Tchar(ijazah3.Text) & "'," & _
        "'" & Tchar(catatanlainlain.Text) & "'," & _
        "'" & status.Text & "'," & _
        "'" & religion.Text & "'," & _
        "'" & " '," & _
        "'" & " '," & _
        "'" & " '," & _
        "'" & amtcontract.Text & "'," & _
        "'" & saveImageUrl & "'," & _
        "'" & " '," & _
        "'" & " '," & _
        "'" & Upduser.Text & "'," & _
        " " & "current_timestamp," & _
        "'" & Tchar(email.Text) & "')"
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        If Not Session("TblDtl") Is Nothing Then
          Dim objTable As DataTable
          objTable = Session("TblDtl")
          For c1 As Int16 = 0 To objTable.Rows.Count - 1

            sSql = "insert into QL_mstpersonhist(" & _
            "cmpcode, " & _
            "persondtloid, " & _
            "historyseq, " & _
            "personoid, " & _
            "compname, " & _
            "position, " & _
            "country, " & _
            "province, " & _
            "city, " & _
            "startperiod, " & _
            "endperiod, " & _
            "upduser, " & _
            "updtime)" & _
            "values (" & _
            "'" & CompnyCode & "'," & _
            " " & c1 + CInt(persondtloid) & "," & _
            " " & objTable.Rows(c1).Item("historyseq") & "," & _
            " " & personmstoid & "," & _
            "'" & Tchar(objTable.Rows(c1).Item("compname")) & "'," & _
            "'" & Tchar(objTable.Rows(c1).Item("position")) & "'," & _
            " " & objTable.Rows(c1).Item("countryoid") & "," & _
            " " & objTable.Rows(c1).Item("provinceoid") & "," & _
            " " & objTable.Rows(c1).Item("cityoid") & "," & _
            "'" & ToDate(objTable.Rows(c1).Item("startperiod")) & "'," & _
            "'" & ToDate(objTable.Rows(c1).Item("endperiod")) & "'," & _
            "'" & Session("UserID") & "'," & _
            " " & "current_timestamp)"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
          Next
          sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + persondtloid) & " where " & _
                 "tablename = 'QL_mstpersonhist' and cmpcode = '" & CompnyCode & "' "
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()
        End If

      Else 'mode update
        sSql = "UPDATE QL_mstperson SET " & _
        "cmpcode='" & CompnyCode & "', " & _
        "personoid=" & personoid.Text & ", " & _
        "nip='" & Tchar(nip.Text) & "', " & _
        "personname='" & Tchar(personname.Text) & "', " & _
        "persontitleoid='" & persontitleoid.Text & "', " & _
        "personsex='" & personsex.Text & "', " & _
        "maritalstatus='" & maritalstatus.Text & "', " & _
        "tglmasuk='" & ConvertedDate1 & "', " & _
        "deptoid='" & deptoid.Text & "', " & _
        "noktp='" & Tchar(noktp.Text) & "', " & _
        "tempatlahir='" & tempatlahir.Text & "', " & _
        "tgllahir='" & ConvertedDate2 & "', " & _
        "alamattinggal='" & Tchar(alamattinggal.Text) & "', " & _
        "citytinggaloid=" & citytinggaloid.Text & ", " & _
        "alamatasal='" & Tchar(alamatasal.Text) & "', " & _
        "asalcityoid=" & asalcityoid.Text & ", " & _
        "phone1='" & phone1.Text & "', " & _
        "phone2='" & phone2.Text & "', " & _
        "emergencyphone='" & emergencyphone.Text & "', " & _
        "pendidikanterakhir='" & Tchar(pendidikanterakhir.Text) & "', " & _
        "asalsekolah='" & Tchar(asalsekolah.Text) & "', " & _
        "sekolahcityoid=" & sekolahcityoid.Text & ", " & _
        "ijazah1='" & Tchar(ijazah1.Text) & "', " & _
        "ijazah2='" & Tchar(ijazah2.Text) & "', " & _
        "ijazah3='" & Tchar(ijazah3.Text) & "', " & _
        "catatanlainlain='" & Tchar(catatanlainlain.Text) & "', " & _
        "status='" & status.Text & "', " & _
        "religion='" & religion.Text & "', " & _
        "personflag='" & "" & " ', " & _
        "personres1='" & "" & " ', " & _
        "personres2='" & "" & " ', " & _
        "amtcontract='" & amtcontract.Text & "', " & _
        "personpicture='" & saveImageUrl & "', " & _
        "userprof='" & "" & " ', " & _
        "profpass='" & "" & " ', " & _
        "email='" & Tchar(email.Text) & "', " & _
        "upduser='" & Upduser.Text & "', " & _
        "updtime=" & "current_timestamp" & " " & _
        " WHERE cmpcode like '%" & CompnyCode & "%' and personoid=" & personoid.Text
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        If Not Session("TblDtl") Is Nothing Then
          sSql = "Delete from QL_mstpersonhist where personoid = " & Session("oid_mstperson")
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()


          Dim objTable As DataTable
          objTable = Session("TblDtl")
          For c1 As Int16 = 0 To objTable.Rows.Count - 1

            sSql = "insert into QL_mstpersonhist(" & _
            "cmpcode, " & _
            "persondtloid, " & _
            "historyseq, " & _
            "personoid, " & _
            "compname, " & _
            "position, " & _
            "country, " & _
            "province, " & _
            "city, " & _
            "startperiod, " & _
            "endperiod, " & _
            "upduser, " & _
            "updtime)" & _
            "values (" & _
            "'" & CompnyCode & "'," & _
            " " & persondtloid + c1 & "," & _
            " " & objTable.Rows(c1).Item("historyseq") & "," & _
            " " & Session("oid_mstperson") & "," & _
            "'" & Tchar(objTable.Rows(c1).Item("compname")) & "'," & _
            "'" & Tchar(objTable.Rows(c1).Item("position")) & "'," & _
            " " & objTable.Rows(c1).Item("countryoid") & "," & _
            " " & objTable.Rows(c1).Item("provinceoid") & "," & _
            " " & objTable.Rows(c1).Item("cityoid") & "," & _
            "'" & ToDate(objTable.Rows(c1).Item("startperiod")) & "'," & _
            "'" & ToDate(objTable.Rows(c1).Item("endperiod")) & "'," & _
            "'" & Session("UserID") & "'," & _
            " " & "current_timestamp)"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
          Next
          sSql = "update  QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + persondtloid) & " where " & _
                 "tablename = 'QL_mstpersonhist' and cmpcode = '" & CompnyCode & "' "
          xCmd.CommandText = sSql
          xCmd.ExecuteNonQuery()
        End If

      End If

      'jika insert data baru, update mstoid
      If Session("oid_mstperson") Is Nothing Or Session("oid_mstperson") = "" Then
        sSql = "update QL_mstoid set lastoid=" & personoid.Text & " where tablename like '%QL_mstperson%' and cmpcode like '%" & CompnyCode & "%' "
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        sSql = "update ql_mstoid set lastoid=" & persondtloid & " where tablename like '%QL_mstpersonhist%' and cmpcode like '%" & CompnyCode & "%' "
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()
      End If
      objTrans.Commit()
      conn.Close()
    Catch ex As Exception
      objTrans.Rollback()
      conn.Close()
      showMessage(ex.Message, "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End Try
    Session.Remove("oid_mstperson")
    Session.Remove("uploadImage_mstperson")
    Response.Redirect("~\Master\mstkaryawan.aspx?awal=true")
  End Sub

  Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
    Dim imgext, imgname As String
    Dim allow As Boolean = False
    imgname = matpictureloc.FileName
    imgext = imgname.Substring(imgname.LastIndexOf(".") + 1)
    If imgname = "" Then
      showMessage("Please choose picture first !!", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    Else
      Dim arr() As String = {"JPG", "BMP"}
      Dim i As Integer = 0
      For Each ci As String In imgname.Split(".")
        For cii As Integer = 0 To arr.Length - 1
          If arr(cii) = ci.ToUpper() Then
            allow = True
          End If
        Next
      Next
    End If
    If (allow = True) Then
      Session("uploadImage_mstkaryawan") = imgext
      matpictureloc.SaveAs(Server.MapPath(EmployeeImageUrl & "temp." & Session("uploadImage_mstkaryawan")))
      prspicture.Visible = True
      lkbFullView.Visible = True
      prspicture.ImageUrl = EmployeeImageUrl & "temp." & Session("uploadImage_mstkaryawan")
    Else
      showMessage("Picture Extention is not valid format please choose picture with format jpg or bmp", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If
  End Sub

  Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
    CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
  End Sub

  Protected Sub lkbFullView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbFullView.Click
    If Session("uploadImage_mstkaryawan") IsNot Nothing Then
      showImage(EmployeeImageUrl & "temp." & Session("uploadImage_mstkaryawan"))
    Else
      showImage(lblpersonpicture.Text)
    End If
  End Sub

  Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
    Try
      report.Load(Server.MapPath(folderReport & "rptMstPerson.rpt"))

      report.SetParameterValue("cmpcode", CompnyCode)
      report.SetParameterValue("sWhere", Session("sWhere_mstperson"))

      CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
          System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

      report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

      Response.Buffer = False

      Response.ClearContent()
      Response.ClearHeaders()
      report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "_" & Format(GetServerTime(), "dd_MM_yy"))
      report.Close() : report.Dispose()
      Response.Redirect("~\Master\mstkaryawan.aspx?awal=true")
    Catch ex As Exception
      showMessage(ex.Message, "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End Try
  End Sub

  Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    If (IsDate(ToDate(startperiod.Text))) And (IsDate(ToDate(endperiod.Text))) Then
      If CDate(ToDate(endperiod.Text)) < CDate(ToDate(startperiod.Text)) Then
        showMessage("Date end must be larger or equals than start date !!", "PT. SUMBER PLASTIK - WARNING")
        Exit Sub
      End If
    Else
      showMessage("Invalid Time Period Date !!", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If

    If CheckYearIsValid(ToDate(startperiod.Text)) Then
      showMessage("Start Period can't Less than 2000 and can't More than 2072 <br />", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If

    If CheckYearIsValid(ToDate(endperiod.Text)) Then
      showMessage("End Period can't Less than 2000 and can't More than 2072 <br />", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If

    If companyname.Text = "" Then
      showMessage("Please Fill Company Name !!", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If
    If divisi.Text = "" Then
      showMessage("Please Fill Position !!", "PT. SUMBER PLASTIK - WARNING")
      Exit Sub
    End If

    If Session("TblDtl") Is Nothing Then
      Dim dtlTable As DataTable = setTabelDetail() : Session("TblDtl") = dtlTable
    End If

    Dim objTable As New DataTable : objTable = Session("TblDtl")

    'Cek apa sudah ada item yang sama dalam Tabel Detail
    If I_u2.Text = "New Detail" Then
      Dim dv As DataView = objTable.DefaultView
      'dv.RowFilter = " historyseq=" & historyseq.Text
      '& " and itemoid=" & itemoid.Text
      'If dv.Count > 0 Then
      '    showMessage("This Data has been added before, please check!", "PT. SUMBER PLASTIK - WARNING")
      '    dv.RowFilter = "" : Exit Sub
      'End If
      dv.RowFilter = ""
      'Else
      '   dv.RowFilter = " matoid=" & matoid.Text & "AND trnpodtlseq<>" & trnpodtlseq.Text
      '& " and itemoid=" & itemoid.Text & " " & _
    End If



    'insert/update to list data
    Dim objRow As DataRow
    If I_u2.Text = "New Detail" Then
      objRow = objTable.NewRow()
      objRow("historyseq") = objTable.Rows.Count + 1
    Else
      objRow = objTable.Rows(historyseq.Text - 1)
      objRow.BeginEdit()
    End If

    objRow("personoid") = personoid.Text
    objRow("compname") = companyname.Text
    objRow("position") = divisi.Text
    objRow("countryoid") = ddlDtlCountry.SelectedValue
    objRow("provinceoid") = ddlDtlProvince.SelectedValue
    objRow("cityoid") = ddlDtlCity.SelectedValue
    objRow("country") = ddlDtlCountry.SelectedItem.Text
    objRow("province") = ddlDtlProvince.SelectedItem.Text
    objRow("city") = ddlDtlCity.SelectedItem.Text
    objRow("startperiod") = startperiod.Text
    objRow("endperiod") = endperiod.Text

    If I_u2.Text = "New Detail" Then
      objTable.Rows.Add(objRow)
    Else
      objRow.EndEdit()
    End If


    Session("TblDtl") = objTable
    tbldtl.Visible = True
    tbldtl.DataSource = Nothing
    tbldtl.DataSource = objTable
    tbldtl.DataBind()
    historyseq.Text = objTable.Rows.Count + 1
    ClearDetail()
    MultiView1.ActiveViewIndex = 1
  End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub lbv12_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbv21_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub ddlDtlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDtlProvince.SelectedIndexChanged
        initdtlcity()
    End Sub

    Protected Sub ddlDtlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDtlCountry.SelectedIndexChanged
        initdtlprovince()
        initdtlcity()
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        historyseq.Text = tbldtl.SelectedDataKey("historyseq")
        'If Session("TblDtl") Is Nothing = False Then

        I_u2.Text = "Update Detail"
        Dim objTable As DataTable
        objTable = Session("TblDtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "historyseq=" & historyseq.Text

        personoid.Text = tbldtl.SelectedDataKey("personoid")
        companyname.Text = tbldtl.SelectedDataKey("compname")
        divisi.Text = tbldtl.SelectedDataKey("position")
        ddlDtlCountry.SelectedValue = tbldtl.SelectedDataKey("countryoid")
        ddlDtlProvince.SelectedValue = tbldtl.SelectedDataKey("provinceoid")
        ddlDtlCity.SelectedValue = tbldtl.SelectedDataKey("cityoid")
        startperiod.Text = tbldtl.SelectedDataKey("startperiod")
        endperiod.Text = tbldtl.SelectedDataKey("endperiod")

        'MultiView1.ActiveViewIndex = 1

        'End If
    End Sub

    Protected Sub tblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))

        'resequence po detail ID + sequenceDtl + hitung total Amount
        'Dim dAmtbeli As Double = 0
        'For C1 As Int16 = 0 To objTable.Rows.Count - 1
        '    Dim dr As DataRow = objTable.Rows(C1)
        '    dr.BeginEdit()
        '    dr(2) = C1 + 1
        '    dr.EndEdit()
        '    dAmtbeli += ToDouble(objTable.Rows(C1).Item(8)) 'amtnettoheader
        'Next

        'amtbeli.Text = ToMaskEdit(dAmtbeli, 2)
        'ReAmount()

        Session("TblDtl") = objTable
        tbldtl.Visible = True
        historyseq.Text = objTable.Rows.Count + 1
        ClearDetail()
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
    End Sub

    Protected Sub personname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If Session("oid") = Nothing Or Session("oid") = "" Then
        '    'generateKode(txtNama.Text.Trim)
        '    If personname.Text.Trim <> "" Then
        '        nip.Text = generatePersonCode(personname.Text.Trim)
        '    End If
        'End If
    End Sub
#End Region

    Protected Sub status_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles status.TextChanged
        If status.SelectedValue = "Tetap" Then
            amtcontract.Text = "0"
            amtcontract.CssClass = "inpTextDisabled"
            amtcontract.Enabled = False
        Else
            amtcontract.CssClass = "inpText"
            amtcontract.Enabled = True

        End If
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMsgBoxOK.Click
        CProc.SetModalPopUpExtender(beMsgBox, PanelMsgBox, mpeMsgbox, False)
    End Sub

    Protected Sub BtnPrintTittle_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If checkdate.Checked = False Then
        '    toPdf("")
        'Else
        '    If bindCekDate() = True Then
        toPdf("")
        '    End If
        'End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub toPdf(ByVal sWhere As String)
        Try
            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\PrintMstPerson.rpt"))
            vReport.SetParameterValue("UserPrint", Session("UserID"))
            vReport.SetParameterValue("TittleHeader", Replace(Label23.Text, ".:", ""))
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With

            SetDBLogonForReport(crConnInfo, vReport)
            CrystalReportViewer1.DisplayGroupTree = False
            CrystalReportViewer1.SeparatePages = False

            If FilterText.Text <> "" Then

                If chkDept.Checked = False And chkTitle.Checked = False Then
                    sWhere &= " WHERE per." & FilterDDL.Text & " like '%" & FilterText.Text & "%' order by per.personname asc"
                End If

                If chkDept.Checked = True And chkTitle.Checked = True Then
                    sWhere &= " WHERE per." & FilterDDL.Text & " like '%" & FilterText.Text & "%'and per.deptoid='" & FilterDept.SelectedValue & "' and per.persontitleoid='" & FilterTitle.SelectedValue & "' order by per.personname asc"
                End If

                If chkDept.Checked = True And chkTitle.Checked = False Then
                    sWhere &= " WHERE per." & FilterDDL.Text & " like '%" & FilterText.Text & "%' and per.deptoid='" & FilterDept.SelectedValue & "' order by per.personname asc"
                End If

                If chkDept.Checked = False And chkTitle.Checked = True Then
                    sWhere &= " WHERE per." & FilterDDL.Text & " like '%" & FilterText.Text & "%' and per.persontitleoid='" & FilterTitle.SelectedValue & "' order by per.personname asc"
                End If

            Else
                If chkDept.Checked = True And chkTitle.Checked = True Then
                    sWhere &= " WHERE per.deptoid='" & FilterDept.SelectedValue & "' and per.persontitleoid='" & FilterTitle.SelectedValue & "' order by per.personname asc"
                End If

                If chkDept.Checked = True And chkTitle.Checked = False Then
                    sWhere &= " WHERE per.deptoid='" & FilterDept.SelectedValue & "' order by per.personname asc"
                End If

                If chkDept.Checked = False And chkTitle.Checked = True Then
                    sWhere &= " WHERE per.persontitleoid='" & FilterTitle.SelectedValue & "' order by per.personname asc"
                End If
            End If

            Dim tglFile As String = Format(GetServerTime(), "dd/MM/yyyy")
            Dim nmFile As String
            nmFile = "Report Karyawan " & Replace(tglFile, "/", "")

            vReport.SetParameterValue("sWhere", sWhere)
            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("DB-Server"), _
            System.Configuration.ConfigurationManager.AppSettings("DB-Name"))

            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in Excel format and file name Customers
            If toexcel = True Then
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, nmFile)
                toexcel = False
            Else
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, nmFile)
            End If

        Catch ex As Exception
            showMessage(ex.ToString(), 2)
        End Try
        report.Close() : report.Dispose()
    End Sub

    Protected Sub BtnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        toPdf("")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        toexcel = True
        toPdf("")
    End Sub
End Class