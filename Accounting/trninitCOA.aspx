<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trninitCOA.aspx.vb" Inherits="Accounting_trninitCOA" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Text=".: COA Balance" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px"></td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<DIV><TABLE style="WIDTH: 960px"><TBODY><TR><TD style="WIDTH: 127px" align=left>&nbsp;Branch</TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:DropDownList id="dd_branchCari" runat="server" Width="214px" CssClass="inpText" __designer:wfdid="w20" AutoPostBack="True">
                                                        </asp:DropDownList> &nbsp;&nbsp; &nbsp; </TD></TR><TR><TD align=left><asp:CheckBox id="cbBlmPosting" runat="server" Width="115px" Text="Not posted yet" __designer:wfdid="w21" AutoPostBack="True" Visible="False"></asp:CheckBox></TD><TD align=left colSpan=2><asp:ImageButton id="imbFindBoM" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w22" AlternateText="Find BoM"></asp:ImageButton><asp:ImageButton id="imbAllBoM" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w23" AlternateText="View All BoM"></asp:ImageButton><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="txtPeriode2" __designer:wfdid="w24" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnCheckAll" runat="server" ImageUrl="~/Images/selectall.png" __designer:wfdid="w25" Visible="False" AlternateText="SELECT ALL"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnUncheckAll" runat="server" ImageUrl="~/Images/selectnone.png" __designer:wfdid="w26" Visible="False"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/postselect.png" __designer:wfdid="w27" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="GVtrnjurnal" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w28" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" DataKeyNames="crdgloid,cmpcode" GridLines="None" AllowPaging="True" OnPageIndexChanging="GVtrnjurnal_PageIndexChanging" AllowSorting="True" OnSelectedIndexChanged="GVtrnjurnal_SelectedIndexChanged">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
                                                                            <asp:CheckBox ID="cbPosting" runat="server" ToolTip="<%# GetIDCB() %>"></asp:CheckBox>
                                                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="True" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:HyperLinkField DataTextField="account" HeaderText="Akun">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Width="300px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="outlet" HeaderText="Branch">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="amtopen" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glmstoid" HeaderText="glmstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" Width="5%"></HeaderStyle>

<ItemStyle Width="5%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="crdgldate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="periodacctg" HeaderText="Period">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dbcr" HeaderText="Debet/Credit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data not found !!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><asp:TextBox id="txtPeriode1" runat="server" Width="67px" CssClass="inpText" __designer:wfdid="w29" Visible="False"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="period1cari" runat="server" TargetControlID="txtPeriode1" __designer:wfdid="w30" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                    </ajaxToolkit:MaskedEditExtender> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w31" Visible="False"></asp:ImageButton><asp:TextBox id="txtPeriode2" runat="server" Width="67px" CssClass="inpText" __designer:wfdid="w32" Visible="False"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="period2cari" runat="server" TargetControlID="txtPeriode2" __designer:wfdid="w33" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                    </ajaxToolkit:MaskedEditExtender> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34" Visible="False"></asp:ImageButton><BR /><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="txtPeriode1" __designer:wfdid="w35" Enabled="True"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="txtPeriode2" __designer:wfdid="w36" Enabled="True"></ajaxToolkit:CalendarExtender> </DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: List of COA Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 623px"><TBODY><TR><TD align=left></TD><TD align=left><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w67" TargetControlID="gldate" PopupButtonID="imbDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w68" TargetControlID="gldate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=left>Branch</TD><TD align=left><asp:DropDownList id="dd_branch" runat="server" Width="214px" CssClass="inpText" __designer:wfdid="w69" AutoPostBack="True"></asp:DropDownList> <asp:Label id="crdgloid" runat="server" __designer:wfdid="w70" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Tanggal" __designer:wfdid="w71"></asp:Label>&nbsp;<asp:TextBox id="txtPostDate" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w72" Visible="False" MaxLength="30" ReadOnly="True" BorderStyle="Solid"></asp:TextBox></TD><TD align=left><asp:TextBox id="gldate" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w73" MaxLength="15" BorderStyle="Solid"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w74"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w75"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Font-Size="9pt" Text="Akun" __designer:wfdid="w76"></asp:Label> <asp:Label id="Label22" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w77"></asp:Label></TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w78">
                                                        <asp:ListItem Value="acctgcode">Account No.</asp:ListItem>
                                                        <asp:ListItem Value="acctgdesc">Account Desc.</asp:ListItem>
                                                    </asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="346px" CssClass="inpText" __designer:wfdid="w79" MaxLength="30" BorderStyle="Solid"></asp:TextBox>&nbsp; </TD></TR><TR><TD align=left></TD><TD align=left><asp:ImageButton id="btnShowData" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w80" AlternateText="Show Data"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=2><asp:Panel id="Panel1" runat="server" Width="100%" Height="250px" __designer:wfdid="w81" ScrollBars="Vertical"><asp:GridView id="gvTblDtl" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w82" Visible="False" GridLines="None" DataKeyNames="acctgoid,acctgcode,acctgdesc" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data found" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="True" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="D/C"><ItemTemplate>
                                                                        <asp:DropDownList ID="FilterDDL" runat="server" Width="75px" CssClass="inpText" SelectedValue='<%# eval("acctgdbcr") %>'>
                                                                            <asp:ListItem Value="D" Selected="True">DEBET</asp:ListItem>
                                                                            <asp:ListItem Value="C">CREDIT</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Total ( IDR )"><ItemTemplate>
                                                                        <asp:TextBox ID="initprice" runat="server" Width="100px" CssClass="inpText" Text='<%# eval("amount") %>' MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender ID="ftbPrice" runat="server" TargetControlID="initprice" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender>
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvfooter" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </asp:Panel> </TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Font-Size="9pt" Text="Note" __designer:wfdid="w83"></asp:Label></TD><TD align=left><asp:TextBox id="crdglnote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w84" MaxLength="50" OnTextChanged="crdglnote_TextChanged"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Width="50px" Font-Size="9pt" Text="Status" __designer:wfdid="w85"></asp:Label></TD><TD align=left><asp:TextBox id="crdglflag" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w86" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left colSpan=2></TD></TR><TR><TD align=left colSpan=2><asp:Label id="create" runat="server" Font-Bold="False" __designer:wfdid="w87"></asp:Label>&nbsp;<asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w88"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:ImageButton style="MARGIN-RIGHT: 10px; HEIGHT: 23px" id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w89" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w90"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w91"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="imbDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w92" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=2><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w93"><ProgressTemplate>
&nbsp;Please Wait ...<BR />&nbsp;<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w94"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>&nbsp;&nbsp;&nbsp; 
</ContentTemplate>
                            </asp:UpdatePanel>
                            
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: Form Saldo Awal COA</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
   <asp:UpdatePanel ID="upMsgbox" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelMsgBox" runat="server" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK"
                            Visible="False" Width="496px">
                            <table border="0" cellpadding="1" cellspacing="1" style="width: 495px">
                                <tbody>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px" valign="top">
                                            <asp:Panel ID="panelPesan" runat="server" BackColor="Yellow" Height="25px" Width="100%">
                                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="Black"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="width: 46px" valign="top">
                                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                Width="24px" />
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="height: 10px; text-align: center" valign="top">
                                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" style="text-align: center" valign="top">
                                            <asp:ImageButton ID="btnMsgBoxOK" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/ok.png"/></td>
                                    </tr>
                                </tbody>
                            </table>
 
                        </asp:Panel>

                        <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" BackgroundCssClass="modalBackground"
                            Drag="True" DropShadow="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption"
                            TargetControlID="beMsgBox">
                        </ajaxToolkit:ModalPopupExtender>
                        <span style="display: none">
<asp:Button ID="beMsgBox" runat="server" CausesValidation="False"
                            Width="130px" Visible="False" />
                        </span>
                       
                    </ContentTemplate>
                </asp:UpdatePanel>
</asp:Content>
