<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="FrmServicePlan.aspx.vb" Inherits="Report_RptServicePlan" title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="970">
        <tr>
           <td align="left" class="header" colspan="3" style="background-color: silver">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="XX-Large" ForeColor="Navy" Text=".: Laporan Rencana Service"></asp:Label></td>
        </tr>
    </table>
    <table width="970">
        <tr>
            <td align="center" colspan="3">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><TABLE><TBODY><TR><TD align=left>Cabang</TD><TD>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLcabang" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w154"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w122"></asp:Label></TD><TD><asp:Label id="Label2" runat="server" Text=":" __designer:wfdid="w123"></asp:Label></TD><TD colSpan=2><asp:DropDownList id="DDLFilter" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w124"><asp:ListItem Value="QLR.REQCODE">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="QLC.CUSTNAME">Nama Customer</asp:ListItem>
<asp:ListItem Value="QLR.REQITEMNAME">Nama Barang</asp:ListItem>
<asp:ListItem Value="QLR.BARCODE">Barcode</asp:ListItem>
<asp:ListItem Value="QLC.phone1">Telepon 1</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w125"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w126"></asp:Label></TD><TD><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w127"></asp:Label></TD><TD align=left colSpan=2><asp:TextBox id="txtPeriod1" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w128"></asp:TextBox> <asp:ImageButton id="btnCalendar1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w129"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w130"></asp:Label> <asp:TextBox id="txtPeriod2" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w131"></asp:TextBox> <asp:ImageButton id="btnCalendar2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w133"></asp:Label></TD></TR><TR><TD id="TD2" align=left runat="server" Visible="false"><asp:RadioButtonList id="cbstatus" runat="server" Width="117px" __designer:wfdid="w134" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Status</asp:ListItem>
<asp:ListItem>Paid</asp:ListItem>
</asp:RadioButtonList></TD><TD id="TD3" runat="server" Visible="false">:</TD><TD id="TD1" align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="DDLStatus" runat="server" Width="91px" CssClass="inpText" __designer:wfdid="w135"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Check</asp:ListItem>
<asp:ListItem>CheckOut</asp:ListItem>
<asp:ListItem>Ready</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Close</asp:ListItem>
<asp:ListItem>Start</asp:ListItem>
<asp:ListItem>Finish</asp:ListItem>
<asp:ListItem>Final</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnview" onclick="btnview_Click" runat="server" ImageUrl="~/Images/viewreport.png" __designer:wfdid="w136"></asp:ImageButton> <asp:ImageButton id="btnETPdf" runat="server" ImageUrl="~/Images/topdf.png" __designer:wfdid="w137" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnETExcel" onclick="btnETExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" __designer:wfdid="w138" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w139"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 150px" align=center colSpan=4><ajaxToolkit:MaskedEditExtender id="MEEPeriod1" runat="server" __designer:wfdid="w141" TargetControlID="txtPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEEPeriod2" runat="server" __designer:wfdid="w142" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEPeriod1" runat="server" __designer:wfdid="w143" TargetControlID="txtPeriod1" Format="dd/MM/yyyy" PopupButtonID="btnCalendar1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEPeriod2" runat="server" __designer:wfdid="w144" TargetControlID="txtPeriod2" Format="dd/MM/yyyy" PopupButtonID="btnCalendar2"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="RCVPLANSERV" runat="server" __designer:wfdid="w145" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel2" runat="server" __designer:dtid="2251799813685260" __designer:wfdid="w146"><ContentTemplate __designer:dtid="2251799813685261">
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w147" Visible="False"><TABLE width=250><TBODY><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w148"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" __designer:wfdid="w149"></asp:ImageButton></TD><TD><asp:Label id="lblValidasi" runat="server" ForeColor="Red" __designer:wfdid="w150"></asp:Label></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD></TD><TD></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w151"></asp:ImageButton></TD><TD></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" __designer:wfdid="w152" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" __designer:wfdid="w153" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnETExcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnETPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnview"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

