<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnWhTransfer.aspx.vb" Inherits="trnWhTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Transfer Warehouse"></asp:Label>
            </th>
        </tr>
        <tr>
            <th align="left" style="background-color: transparent" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Warehouse :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Transfer No </TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Tanggal</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText"></asp:TextBox> &nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> - <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText"></asp:TextBox> &nbsp;<asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label> </TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="DDLTypeTW" runat="server" Width="163px" CssClass="inpText" AutoPostBack="True">
                                                                            <asp:ListItem>Send TW</asp:ListItem>
                                                                            <asp:ListItem>Receive TW</asp:ListItem>
                                                                        </asp:DropDownList> </TD></TR><TR><TD class="Label" align=left><asp:Label id="FromBranch" runat="server" Text="From Branch"></asp:Label></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="dd_branch" runat="server" Width="163px" CssClass="inpText" AutoPostBack="True">
                                                                        </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="ToCab" runat="server" Text="To Branch"></asp:Label></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="dd_branchTO" runat="server" Width="163px" CssClass="inpText">
                                                                        </asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="ddl_status" runat="server" Width="163px" CssClass="inpText">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem>In Process</asp:ListItem>
                                                                            <asp:ListItem>In Approval</asp:ListItem>
                                                                            <asp:ListItem>Approved</asp:ListItem>
                                                                            <asp:ListItem>Rejected</asp:ListItem>
                                                                        </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 108px; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 13px; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=2><asp:ImageButton style="MARGIN-RIGHT: 0px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 0px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="tgl1" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="tgl2" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left colSpan=4><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None" PageSize="8">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333"  />
                                                        <Columns>
                                                            <asp:HyperLinkField DataNavigateUrlFields="trfmtrmstoid" 
                                                                DataNavigateUrlFormatString="trnWHTransfer.aspx?oid={0}" 
                                                                DataTextField="transferno" HeaderText="Transfer No">
                                                            <ControlStyle ForeColor="Red"  />
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"  />
                                                            <ItemStyle ForeColor="Black" HorizontalAlign="Left"  />
                                                            </asp:HyperLinkField>
                                                            <asp:BoundField DataField="trfmtrdate" DataFormatString="{0:dd/MM/yyyy}" 
                                                                HeaderText="Transfer Date" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" 
                                                                Wrap="False"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="upduser" HeaderText="Username" >
                                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left"  />
                                                            <ItemStyle BorderColor="Black" HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FromBranch" HeaderText="From Branch" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" 
                                                                Wrap="False"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ToBranch" HeaderText="To Branch" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"  />
                                                            <ItemStyle HorizontalAlign="Left" Wrap="False"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="status" HeaderText="Status" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"  />
                                                            <ItemStyle HorizontalAlign="Left" Wrap="False"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Note" HeaderText="Note" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    &nbsp;<asp:LinkButton ID="lbprint" runat="server" OnClick="lbprint_Click" 
                                                                        ToolTip='<%# eval("trfmtrmstoid") %>'>Print</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="labeloid" runat="server" Text='<%# Eval("trfmtrmstoid") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                        <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" 
                                                            Font-Bold="True"  />
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"  />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                        <AlternatingRowStyle BackColor="White"  />
                                                    </asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Warehouse :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 92px" class="Label" align=left>Transfer No.</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 92px" class="Label" align=left>Date</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" Enabled="False" ValidationGroup="MKE"></asp:TextBox> &nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="Label" align=left>No. Ref</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="noref" runat="server" Width="150px" CssClass="inpText" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 92px; WHITE-SPACE: nowrap; HEIGHT: 11px" class="Label" align=left>From Branch</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 11px" class="Label" align=left>:</TD><TD style="WIDTH: 331px; HEIGHT: 11px" class="Label" align=left><asp:DropDownList id="fromBramch" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList> <asp:Label style="FONT-WEIGHT: 700; FONT-SIZE: small" id="Lb_FromBranch" runat="server"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px; HEIGHT: 11px" class="Label" align=left>To Branch</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 11px" class="Label" align=left>:</TD><TD style="HEIGHT: 11px" class="Label" align=left><asp:DropDownList id="toBranch" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList><asp:Label style="FONT-WEIGHT: bold; FONT-SIZE: small" id="Lb_ToBranch" runat="server"></asp:Label> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 92px" class="Label" align=left>From Location</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:DropDownList id="fromlocationBranch" runat="server" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList> <asp:Label style="FONT-WEIGHT: 700; FONT-SIZE: small" id="Lb_FromLoc" runat="server"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="Label" align=left>To Location</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="tolocationBranch" runat="server" CssClass="inpText">
                                                    </asp:DropDownList><asp:Label style="FONT-WEIGHT: bold; FONT-SIZE: small" id="Lb_ToLoc" runat="server"></asp:Label> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 92px; HEIGHT: 6px" class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 6px" class="Label" align=left>:</TD><TD style="WIDTH: 331px; HEIGHT: 6px" class="Label" align=left><asp:TextBox id="note" runat="server" Width="251px" CssClass="inpText" MaxLength="200"></asp:TextBox> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px; HEIGHT: 6px" class="Label" align=left>Status</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 6px" class="Label" align=left></TD><TD style="HEIGHT: 6px" class="Label" align=left><asp:TextBox id="trnstatus" runat="server" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">In Process</asp:TextBox> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=3><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Usage Detail :" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" visible="false"><asp:Label id="LblExpedisi" runat="server" Width="1px" Text="Expedisi"></asp:Label></TD><TD id="TD2" class="Label" align=left runat="server" visible="false">:</TD><TD id="TD3" class="Label" align=left colSpan=4 runat="server" visible="false"><asp:CheckBox id="CbExp" runat="server" Width="49px" Font-Size="7pt" Text="Ya" AutoPostBack="True"></asp:CheckBox> <asp:CheckBox id="cb1" runat="server" Width="123px" Font-Size="7pt" Text="Berat Barang /Kg" Visible="False" AutoPostBack="True" OnCheckedChanged="cb1_CheckedChanged"></asp:CheckBox> &nbsp;<asp:CheckBox id="cb2" runat="server" Width="118px" Font-Size="7pt" Text="Berat Volume/cm3" Visible="False" AutoPostBack="True" OnCheckedChanged="cb2_CheckedChanged"></asp:CheckBox> <asp:Label id="typeDimensi" runat="server" Width="35px" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Width="83px" Text="Type Expedisi" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label12" runat="server" Width="0px" Text=":" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="ddtype" runat="server" CssClass="inpText" Visible="False">
<asp:ListItem Selected="True" Value="DARAT">Darat</asp:ListItem>
<asp:ListItem Value="LAUT">Laut</asp:ListItem>
<asp:ListItem Value="UDARA">Udara</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Width="35px" Text="Item"></asp:Label> <asp:Label id="Label10" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="300px" CssClass="inpText" Font-Bold="False" ForeColor="Black"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" Visible="False"></asp:Label> <asp:Label id="LabelItemCode" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="stockflag" runat="server" Visible="False" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" Visible="False" PageSize="8" GridLines="None" EnableModelValidation="True" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,itemcode,itemdesc,merk,sisa,satuan1,unit1,satuan2,unit2,satuan3,unit3,konversi1_2,konversi2_3,dimensi_p,dimensi_l,dimensi_t,beratvolume,beratbarang,ByExp,stockflag" EmptyDataRowStyle-ForeColor="Red">
<PagerSettings FirstPageText="&amp;gt;" LastPageText="Next"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="No" HeaderText="No.">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock">
<HeaderStyle HorizontalAlign="Right" Wrap="True" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle CssClass="gvhdr" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left>Merk</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" MaxLength="200" Enabled="False"></asp:TextBox> <asp:Label id="labelkonversi1_2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelkonversi2_3" runat="server" Visible="False"></asp:Label> <asp:Label id="labelunit1" runat="server" Visible="False"></asp:Label><asp:Label id="labelunit2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelunit3" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD id="TD9" align=left visible="false"><asp:Label id="Label9" runat="server" Width="92px" Text="Berat Barang" Visible="False"></asp:Label></TD><TD id="TD8" align=left visible="false"><asp:Label id="Label6" runat="server" Width="1px" Text=":" Visible="False"></asp:Label></TD><TD id="TD5" align=left visible="false"><asp:TextBox id="beratBarang" runat="server" CssClass="inpText" Enabled="False" Visible="False">0</asp:TextBox> <asp:Label id="labelseq" runat="server" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left>&nbsp;</TD></TR><TR><TD id="TD6" align=left visible="false"><asp:Label id="LblBiaya" runat="server" Width="101px" Text="Biaya Expedisi" Visible="False"></asp:Label></TD><TD id="TD4" align=left visible="false"><asp:Label id="Label7" runat="server" Width="1px" Text=":" Visible="False"></asp:Label></TD><TD id="TD7" align=left visible="false"><asp:TextBox id="AmtExpedisi" runat="server" CssClass="inpText" Enabled="False" Visible="False">0</asp:TextBox></TD><TD align=left></TD><TD align=left></TD><TD align=left>&nbsp;</TD></TR><TR><TD class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial" ForeColor="Red" Text="*"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="150px" CssClass="inpText" MaxLength="10" ValidationGroup="MKE" AutoPostBack="True"></asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="'<='" w153=""></asp:Label> <asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red">0.00</asp:Label> <asp:DropDownList id="unit" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList>&nbsp; </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:Label id="labelsatuan1" runat="server" Visible="False"></asp:Label> <asp:Label id="labelsatuan2" runat="server" Visible="False"></asp:Label> <asp:Label id="labelsatuan3" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Note</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w2" GridLines="None" EnableModelValidation="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,seq,satuan,satuan1,satuan2,satuan3,unit1,unit2,unit3,konversi1_2,konversi2_3,typeDimensi,AmtExpedisi,jenisexp,beratvolume,beratbarang,statusexp,nettoexp,stockflag">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="ID Item" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="statusexp" HeaderText="Expedisi" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratvolume" HeaderText="Berat volume">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="beratbarang" HeaderText="Berat barang">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="jenisexp" HeaderText="Type Exp">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtexpedisi" HeaderText="Amt expedisi">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="nettoexp" HeaderText="Amt Rata2">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
                                                                    <asp:LinkButton ID="lbdelete" runat="server" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
                                                                
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="typeDimensi" HeaderText="Type dimensi" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockflag" HeaderText="stockflag" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                        <ProgressTemplate>
                                                            <div id="Div1" class="progressBackgroundFilter">
                                                            </div>
                                                            <div id="Div3" class="processMessage">
                                                                <span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                    <asp:Image ID="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"  /><br  />
Please Wait .....</span><br  />
                                                            </div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="FTEBrtBarang" runat="server" TargetControlID="beratBarang" ValidChars="0123456789.,">
</ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEAmtExp" runat="server" TargetControlID="AmtExpedisi" ValidChars="0123456789.,">
</ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> &nbsp;&nbsp;</TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="UpdatePanelSN" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlInvnSN" runat="server" Width="600px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabelDescSn" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Enter SN of Item"></asp:Label> </TD></TR><TR><TD><asp:Label id="NewSN" runat="server" ForeColor="Red" Text="New SN"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextItem" runat="server" Width="195px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp;<asp:TextBox id="MAXQTYSN" runat="server" Width="48px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="Label31" runat="server" ForeColor="Red" Text="Max Qty"></asp:Label> <asp:Label id="KodeItem" runat="server" Visible="False"></asp:Label> &nbsp;<asp:Label id="SNseq" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Scan Serial Number &nbsp; &nbsp;&nbsp; : <asp:TextBox id="TextSN" onkeypress="return EnterEvent(event)" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> &nbsp; <asp:ImageButton style="HEIGHT: 23px" id="EnterSN" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY><CAPTION><DT style="TEXT-ALIGN: center"></DT></CAPTION><TR><TD style="HEIGHT: 255px" align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV style="TEXT-ALIGN: center" id="Div2"><STRONG><asp:Label id="LabelPesanSn" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label> </STRONG></DIV><DIV></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 58%"><asp:GridView id="GVSN" runat="server" Width="549px" CssClass="MyTabStyle" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None">
                                                                                        <AlternatingRowStyle BackColor="White"  />
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False" >
                                                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                                                            </asp:BoundField>
                                                                                            <asp:BoundField DataField="SN" HeaderText="Serial Number">
                                                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                                                            <ItemStyle Width="450px"  />
                                                                                            </asp:BoundField>
                                                                                            <asp:CommandField ShowDeleteButton="True" >
                                                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                                                            </asp:CommandField>
                                                                                        </Columns>
                                                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                                                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"  />
                                                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333"  />
                                                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"  />
                                                                                    </asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSN" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton> &nbsp; <asp:LinkButton id="lkbPilihItem" runat="server" Font-Size="X-Small">[ OK ]</asp:LinkButton> </TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupSN" runat="server" TargetControlID="btnHideSN" PopupControlID="UpdatePanelSN" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaptInvtry" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSN" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="TextSN"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></th>
        </tr>
    </table>
    
</asp:Content>