'prgrmr : zipi ; Last Update By zipi On 29.07.2013
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

'=== IMPORTANT NOTICE ===
'1. Invoice TUNAI
'   a. Biaya Exclude : Menambah Invoice, Pengeluaran Lewat "Form Cash/Bank Expense"
'      - INSERT Invoice : Auto Insert Into Cash/Bank Module (Full Invoice)
'      - POSTING GL : Kas/Bank terhadap Penjualan 
'   b. Biaya Include : Mengurangi Invoice
'      - INSERT Invoice : Auto Insert Into Cash/Bank Module (Full Invoice)
'      - INSERT Pembayaran Biaya : Auto Insert Into Cash/Bank Module (Pengeluaran untuk pembayaran biaya)
'      - POSTING GL : Kas/Bank terhadap Penjualan 
'2. Invoice KREDIT
'   a. INSERT Piutang : Auto Insert Into A/R Module (Full Invoice)
'   b. POSTING GL : Piutang Terhadap Penjualan
'   c. Biaya Exclude : Menambah Invoice, Pengeluaran Lewat "Form Cash/Bank Expense"
'   d. Biaya Include : Tidak mempengaruhi Invoice, baru berpengaruh saat Payment A/R
'========================

Partial Class Transaction_trnnotajual
    Inherits System.Web.UI.Page

#Region "Variable"
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    Dim report As New ReportDocument
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim dv As DataView
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
    Dim crate As ClassRate
#End Region

#Region "Function"
    Private Function UpdatePrintCounter(ByVal sBranchCode As String, ByVal iOid As Integer) As String
        ' ===== Update counter utk Printout SI - 4n7JuK (20190227)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "Select REPLACE(convert(cHAR(20),updtime_edit,101), ' ', '') From QL_trnjualmst WHERE trnjualmstoid='" & iOid & "' And branch_Code='" & sBranchCode & "'"
            xCmd.CommandText = sSql
            Dim updtime_edit As String = xCmd.ExecuteScalar()
            
            sSql = "UPDATE QL_trnjualmst SET printke=ISNULL(printke,0)+1, upduser_edit='" & Session("UserID") & "', updtime_edit=CURRENT_TIMESTAMP"
            If TcharNoTrim(updtime_edit) = "01/01/1900" Then
                sSql &= ", datefirstprint=CURRENT_TIMESTAMP"
            End If
            sSql &= " WHERE trnjualmstoid='" & iOid & "' And branch_Code='" & sBranchCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
            UpdatePrintCounter = ""
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            UpdatePrintCounter = "Error Update Counter Print SI : " & ex.ToString & "<BR>" & sSql
        End Try
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String, ByVal sBranch As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "' AND interfaceres1='" & sBranch & "'"
        Return GetStrData(sSql)
    End Function

    Private Function ResetPrintCounter(ByVal sOid As String, ByVal sBranch As String) As String
        ' ===== Update counter utk Printout SI - 4n7JuK (20190227)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE QL_trnjualmst SET printke=0, canceluser='" & Session("UserID") & "', canceltime=CURRENT_TIMESTAMP,cancelnote=CASE WHEN cancelnote='' THEN 1 ELSE CAST(cancelnote AS INTEGER)+1 END WHERE trnjualmstoid='" & sOid & "' and branch_Code='" & sBranch & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
            ResetPrintCounter = "Print SI Counter has been reset."
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            ResetPrintCounter = "Error Reset Counter Print SI : " & ex.ToString & "<BR>" & sSql
        End Try
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' "
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Public Function GetToolTipCounter() As String
        Dim sText As String = "- Jumlah Print = " & ToDouble(Eval("printke").ToString) & vbCrLf & _
            "- Last Print By = " & Eval("lastprintby") & vbCrLf & _
            "- Last Print On = " & Eval("lastprinton") & vbCrLf & _
            "- Reset Counter = " & ToDouble(Eval("resetke").ToString) & vbCrLf & _
            "- Last Reset By = " & Eval("lastresetby") & vbCrLf & _
            "- Last Reset On = " & Eval("lastreseton")
        Return sText
    End Function

    Public Function getOID() As String
        Return Eval("trnjualmstoid")
    End Function

    Public Function GetSelected() As Boolean
        If Eval("efaktur") = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function GetEnabled() As Boolean
        ' Enabled = false jika SO1/Nontax
        Return True '(Eval("taxable") <> "NONTAX")
    End Function

    Public Function getCompany() As String
        Return CompnyCode
    End Function

    Public Function getStr() As String
        Return Server.MapPath("~/reportform/PrintReport.aspx") & "?type=SI&cmpcode=" & CompnyCode & "&oid=" & Request.QueryString("oid")
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("trnjualdtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnjualmstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("trnjualdtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("sjrefoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemgroupcode", Type.GetType("System.String"))
        dt.Columns.Add("salesdeliveryqty", Type.GetType("System.Decimal"))
        dt.Columns.Add("trnjualdtluom", Type.GetType("System.Int32"))
        dt.Columns.Add("orderprice", Type.GetType("System.Decimal"))
        dt.Columns.Add("trnjualdtlnote", Type.GetType("System.String"))
        dt.Columns.Add("trnjualstatus", Type.GetType("System.String"))
        dt.Columns.Add("amtjualnetto", Type.GetType("System.Decimal"))
        dt.Columns.Add("upduser", Type.GetType("System.String"))
        dt.Columns.Add("updtime", Type.GetType("System.DateTime"))
        dt.Columns.Add("salesdeliveryno", Type.GetType("System.String"))

        dt.Columns.Add("gendesc", Type.GetType("System.String"))
        dt.Columns.Add("salesdeliveryoid", Type.GetType("System.Int32"))
        dt.Columns.Add("salesdeliverydtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("width", Type.GetType("System.Decimal"))
        dt.Columns.Add("lenght", Type.GetType("System.Decimal"))
        dt.Columns.Add("weight", Type.GetType("System.Decimal"))
        'dt.Columns.Add("trnjualpackingqty", Type.GetType("System.Decimal"))
        Return dt
    End Function

    Private Function setTabelDetailBiaya() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("jualbiayaseq", Type.GetType("System.Int32"))
        dt.Columns.Add("itembiayaoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itembiayanote", Type.GetType("System.String"))
        dt.Columns.Add("itembiayaamt", Type.GetType("System.Decimal"))
        dt.Columns.Add("upduser", Type.GetType("System.String"))
        dt.Columns.Add("updtime", Type.GetType("System.DateTime"))
        dt.Columns.Add("acctgdesc", Type.GetType("System.String"))
        Return dt
    End Function

    Private Function InitTaxPct(ByVal taxoid As String) As Double
        Return cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND genoid=" & taxoid)
    End Function

    Private Function InitAcctgBasedOnMasterData(ByVal sMasterTable As String, ByVal sMasterColumn As String, ByVal sAcctgColumn As String, _
    ByVal iMasterOid As Integer) As Integer
        sSql = "SELECT ISNULL(" & sAcctgColumn & ",0) FROM " & sMasterTable & " WHERE cmpcode='" & CompnyCode & "' AND " & sMasterColumn & "=" & iMasterOid
        Return cKoneksi.ambilscalar(sSql)
    End Function
#End Region

#Region "Procedure"
    Private Sub BindDataCust(ByVal sQueryplus As String)
        mysql = "SELECT [CMPCODE], [CUSTOID], [CUSTCODE], [CUSTNAME], [CUSTFLAG] FROM [QL_MSTCUST] WHERE (([CMPCODE] LIKE '%" & CompnyCode & "%') and [CUSTFLAG] = 'Active'  " & sQueryplus & " ) ORDER BY custcode"
        FillGV(gvCustomer, mysql, "QL_MSTCUST")
    End Sub

    Public Sub showPrintExcel(ByVal oid As Integer)

        ' lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        Response.Clear()
        Dim swhere As String = ""
        Dim shaving As String = ""
        Dim swhere2 As String = ""
        ' If dView.SelectedValue = "Master" Then
        sSql = "SELECT d.trnjualdtloid,d.trnjualmstoid,d.trnjualdtlseq,d.sjrefoid,d.itemoid,d.itemgroupcode,d.trnjualdtlqty salesdeliveryqty,d.trnjualdtluom,d.trnjualdtlprice orderprice,d.trnjualnote AS trnjualdtlnote,d.trnjualstatus,d.amtjualnetto,d.upduser,d.updtime,sj.salesdeliveryno,i.itemlongdesc,g.gendesc,sj.salesdeliveryoid FROM QL_trnjualdtl d  INNER JOIN ql_trnsalesdelivmst sj ON d.cmpcode=sj.cmpcode AND d.sjrefoid=sj.salesdeliveryoid INNER JOIN QL_mstitem i ON d.cmpcode=i.cmpcode AND i.itemoid=d.itemoid INNER JOIN ql_mstgen g ON g.cmpcode=d.cmpcode AND d.trnjualdtluom=g.genoid WHERE d.trnjualmstoid=" & oid & " AND d.cmpcode='SIP' ORDER BY d.trnjualdtlseq"

        Response.AddHeader("content-disposition", "inline;filename= Status Order Pembelian.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()


    End Sub

    Private Sub FillDDLAccounting(ByVal oDDLAccount As DropDownList, ByVal sFilterCode As String)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(oDDLAccount, sSql)
    End Sub

    Private Sub BindLastSearch()
        FillGV(Tbldata, Session("SearchSI"), "ql_trnjualmst")
    End Sub

    Private Sub UpdateCheckedList()
        If Session("JualMst") IsNot Nothing Then
            Dim dt As DataTable = Session("JualMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To Tbldata.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = Tbldata.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(13).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "trnjualmstoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("efaktur") = 1
                        Else
                            dv(0)("efaktur") = 0
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("JualMst") = dt
        End If
    End Sub

    Private Sub SumPriceInInvoiceDetail(ByVal oTblDtl2 As DataTable)
        Dim dAmountDetail As Double = 0
        'Dim dDiscDetail As Double = 0
        Try
            For C1 As Int16 = 0 To oTblDtl2.Rows.Count - 1
                dAmountDetail += ToDouble(oTblDtl2.Rows(C1).Item("amtjualnetto"))
                'dDiscDetail += oTblDtl2.Rows(C1).Item("trnjualdtldisc")
            Next
            'trna.Text = ToMaskEdit(dAmountDetail, 3)
            'trnamtjualnetto.Text = ToMaskEdit(dAmountDetail - dDiscDetail, 3)
            If currate.SelectedValue = "1" Then
                trnamtjual.Text = ToMaskEdit(dAmountDetail, 3)
            Else
                trnamtjual.Text = NewMaskEdit(dAmountDetail)
            End If
            'amtdiscdtl.Text = ToMaskEdit(dDiscDetail, 3)
        Catch ex As Exception
            If currate.SelectedValue = "1" Then
                trnamtjual.Text = ToMaskEdit(dAmountDetail, 3)
            Else
                trnamtjual.Text = NewMaskEdit(dAmountDetail)
            End If
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, Optional ByVal sState As String = "")
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage : lblState.Text = sState
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub SumBiayaDetail(ByVal oTblDtl2 As DataTable)
        Dim dAmountDetail As Double = 0
        Try
            For C1 As Int16 = 0 To oTblDtl2.Rows.Count - 1
                dAmountDetail += oTblDtl2.Rows(C1).Item("itembiayaamt")
            Next
            If currate.SelectedValue = "1" Then
                trnjualamtcost.Text = ToMaskEdit(dAmountDetail, 3)
            Else
                trnjualamtcost.Text = NewMaskEdit(dAmountDetail)
            End If
        Catch ex As Exception
            If currate.SelectedValue = "1" Then
                trnjualamtcost.Text = ToMaskEdit(dAmountDetail, 3)
            Else
                trnjualamtcost.Text = NewMaskEdit(dAmountDetail)
            End If
        End Try
    End Sub

    Private Sub InitDdlCabang()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='cabang'"

        Tbldata.Columns(13).Visible = False
        Tbldata.Columns(14).Visible = False
        imbExportXLS.Visible = False

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.Items.Add(New ListItem("ALL", "ALL"))
                DdlCabang.SelectedValue = "ALL"
                Tbldata.Columns(13).Visible = True
                imbExportXLS.Visible = True
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.Items.Add(New ListItem("ALL", "ALL"))
            DdlCabang.SelectedValue = "ALL"
            Tbldata.Columns(13).Visible = True
            imbExportXLS.Visible = True
        End If

        ' Check Setting Reset Counter di Approval Hirarki
        sSql = "SELECT COUNT(-1) FROM QL_approvalstructure WHERE tablename='RESET COUNTER PRINT SI' AND approvaluser='" & Session("UserID") & "'"
        Tbldata.Columns(14).Visible = (ToDouble(GetScalar(sSql)) > 0)
    End Sub

    Public Sub initDDLPosting(ByVal sTypePayment As String, ByVal sInvoiceType As String)
        Dim varCash As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH'")
        Dim varBank As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK'")

        If sInvoiceType = "CSH00" Or sInvoiceType.ToUpper = "CASH" Then
            If Trim(sTypePayment) = "CASH" Then
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varCash & "%' AND acctgoid not in " & _
                    "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
                FillDDL(cashbankacctg, sSql)
                Label22.Visible = False
                referensino.Visible = False
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Cash Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                    lkbPosting.Visible = False
                Else
                    lkbPosting.Visible = True
                End If
                lblcashbankacctg.Text = "Cash Account"
            Else
                sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varBank & "%' AND acctgoid not in " & _
                    "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
                FillDDL(cashbankacctg, sSql)
                Label22.Visible = True
                referensino.Visible = True
                If cashbankacctg.Items.Count < 1 Then
                    showMessage("Please create/fill Bank Account in Chart of Accounts!!", _
                        CompnyName & " - WARNING", 2)
                    lkbPosting.Visible = False
                Else
                    lkbPosting.Visible = True
                End If
                lblcashbankacctg.Text = "Bank Account"
            End If
            lblpaymentype.Visible = True : paymentype.Visible = True
            lblcashbankacctg.Visible = True : cashbankacctg.Visible = True
            lblapaccount.Visible = False : apaccount.Visible = False
        Else

            sSql = "SELECT distinct  acctgoid,CAST(acctgcode AS VARCHAR) + ' - ' + acctgdesc FROM QL_mstacctg W inner join QL_mstinterface i on W.acctgcode like i.interfacevalue + '%'   and i.interfacevar like 'VAR_AR%' AND  acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=W.cmpcode) AND W.cmpcode='" & CompnyCode & "'    ORDER BY 2  "

            FillDDL(apaccount, sSql)
            Label22.Visible = False
            referensino.Visible = False
            If apaccount.Items.Count < 1 Then
                showMessage("Please create/fill AP Account in Chart of Accounts!!", _
                    CompnyName & " - WARNING", 2)
                lkbPosting.Visible = False
            Else
                lkbPosting.Visible = True
            End If
            lblpaymentype.Visible = False : paymentype.Visible = False
            lblcashbankacctg.Visible = False : cashbankacctg.Visible = False
            lblapaccount.Visible = True : apaccount.Visible = True
        End If
        ' Set Cost Cash/Bank Account Visibility Based on Cost Amount
        If ToDouble(trnjualamtcost.Text) > 0 Then
            sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varCash & "%' OR acctgcode LIKE '" & varBank & "%') AND acctgoid not in " & _
                "(select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
            FillDDL(CashForCost, sSql)
            ' Set Cost Cash/Bank Account Visibility Based on Cost Type
            If trnjualcosttype.SelectedValue.ToUpper = "EXCLUDE" Then
                lblCostCashBank.Visible = False : CashForCost.Visible = False
                Exit Sub
            Else
                lblCostCashBank.Visible = True : CashForCost.Visible = True
            End If
            If CashForCost.Items.Count < 1 Then
                showMessage("Please create/fill Cash/Bank Account in Chart of Accounts!!", _
                    CompnyName & " - WARNING", 2)
                lkbPosting.Visible = False
            Else
                lkbPosting.Visible = True
            End If
        Else
            lblCostCashBank.Visible = False : CashForCost.Visible = False
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub ClearDetail() 
        TblDtl.Columns(9).Visible = True
    End Sub

    Private Sub generateNo()
        Try
            Dim type As String = ""
            If referenceno.Text.Substring(1, 1) = "C" Then
                type = "IV" 
            Else
                type = "IT" 
            End If
            If referenceno.Text.Substring(2, 1) = "1" Then
                taxtype.SelectedIndex = 0
            Else
                taxtype.SelectedIndex = 1
            End If
            Dim sNo As String = type & referenceno.Text.Substring(2, 1) & "." & Format(CDate(toDate(trnjualdate.Text)), "yy") & "." & Format(CDate(toDate(trnjualdate.Text)), "MM") & "." & referenceno.Text.Substring(10, 1) & "."
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno,4) AS INTEGER))+1,1) AS IDNEW FROM QL_trnjualmst " & _
                "WHERE cmpcode='" & CompnyCode & "' AND trnjualno LIKE '" & sNo & "%'"
            trnjualno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        Catch ex As Exception
            trnjualno.Text = "IV2." & Format(CDate(toDate(trnjualdate.Text)), "yy") & "." & Format(CDate(toDate(trnjualdate.Text)), "MM") & ".J.0001"
        End Try 
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & CompnyCode & "' and curroid=" & iOid & " order by currdate desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyrate.Text = ToMaskEdit(xCmd.ExecuteScalar, 3)
        conn.Close()
    End Sub

    Private Sub ReAmount()
        If currencyrate.Text.Trim = "" Then currencyrate.Text = "1.00"
        If trndiscamt.Text.Trim = "" Then trndiscamt.Text = "0.00"
        If trntaxpct.Text.Trim = "" Then trntaxpct.Text = "0.00"
        If trnamtjual.Text.Trim = "" Then trnamtjual.Text = "0.00"
        If amtdischdr.Text.Trim = "" Then amtdischdr.Text = "0.00"
        If amtdiscdtl.Text.Trim = "" Then amtdiscdtl.Text = "0.00"

        If currate.SelectedValue = "1" Then
            If trndisctype.SelectedValue = "AMT" Then 'amount
                amtdischdr.Text = ToMaskEdit(CDbl(ToDouble(trndiscamt.Text)), 4)
            Else
                If ToDouble(trndiscamt.Text) > 100 Then
                    showMessage("Discount Persent can't be more than 100 !!!", CompnyName & " - WARNING", 2)
                    trndiscamt.Text = "0.0000"
                End If
                amtdischdr.Text = ToMaskEdit(CDbl(((ToDouble(trndiscamt.Text) / 100) * (ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text)))), 4)
            End If
            If amtdischdr.Text.Trim = "" Then
                amtdischdr.Text = "0.0000"
            End If

            Dim tempnilai As Double
            tempnilai = ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text) - ToDouble(amtdischdr.Text)

            trnamttax.Text = ToMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai))), 4)
            'trnamttax.Text = ToMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai)) * ToDouble(currencyrate.Text)), 3)
            If trnamttax.Text.Trim = "" Then
                trnamttax.Text = "0.00"
            End If

            totalinvoice.Text = ToMaskEdit(CDbl(ToDouble(tempnilai) + ToDouble(trnamttax.Text)), 4)
            If totalinvoice.Text.Trim = "" Then
                totalinvoice.Text = "0.0000"
            End If
            totalinvoicerp.Text = ToMaskEdit(ToDouble(totalinvoice.Text) * ToDouble(currencyrate.Text), 4)
            If totalinvoicerp.Text.Trim = "" Then totalinvoicerp.Text = "0.00"
        Else
            If trndisctype.SelectedValue = "AMT" Then 'amount
                amtdischdr.Text = NewMaskEdit(CDbl(ToDouble(trndiscamt.Text)))
            Else
                If ToDouble(trndiscamt.Text) > 100 Then
                    showMessage("Discount Persent can't be more than 100 !!!", CompnyName & " - WARNING", 2)
                    trndiscamt.Text = "0.0000"
                End If
                amtdischdr.Text = NewMaskEdit(CDbl(((ToDouble(trndiscamt.Text) / 100) * (ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text)))))
            End If
            If amtdischdr.Text.Trim = "" Then
                amtdischdr.Text = "0.0000"
            End If

            Dim tempnilai As Double
            tempnilai = ToDouble(trnamtjual.Text) - ToDouble(amtdiscdtl.Text) - ToDouble(amtdischdr.Text)

            trnamttax.Text = NewMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai))))
            'trnamttax.Text = ToMaskEdit(CDbl(((trntaxpct.Text / 100) * ToDouble(tempnilai)) * ToDouble(currencyrate.Text)), 3)
            If trnamttax.Text.Trim = "" Then
                trnamttax.Text = "0.00"
            End If

            totalinvoice.Text = NewMaskEdit(CDbl(ToDouble(tempnilai) + ToDouble(trnamttax.Text)))
            If totalinvoice.Text.Trim = "" Then
                totalinvoice.Text = "0.00"
            End If
            totalinvoicerp.Text = NewMaskEdit(ToDouble(totalinvoice.Text) * ToDouble(currencyrate.Text))
            If totalinvoicerp.Text.Trim = "" Then totalinvoicerp.Text = "0.00"
        End If
    End Sub

    Private Sub InitAllDDL()
        sSql = "select currencyoid,currencycode+'-'+currencydesc from QL_mstcurr where cmpcode like '%" & CompnyCode & "%'"
        FillDDL(currate, sSql) : FillCurrencyRate(currate.SelectedValue)
        FillDDLGen("PAYTYPE", trnpaytype) 
    End Sub

    Private Sub Filltextbox(ByVal Branch As String, ByVal iOid As Int64)
        Session("branch") = Branch
        sSql = "SELECT distinct a.trnjualmstoid,a.trnjualno,a.trnjualdate,b.ordermstoid ,a.trnjualres1,a.trnjualref,a.orderno,b.trnorderdate orderdate,a.trncustoid,c.custname,b.timeofpaymentSO trnpaytype,b.currencyoid,(select rate.rate2idrvalue from QL_mstrate2 rate where rate.rate2oid = b.rate2oid) currencyrate,b.rate2oid,a.trnamtjual,a.trntaxpct,a.trnamttax,a.trnjualnote,a.trnjualstatus,b.ordertaxtype,a.trnjualres1,a.upduser,a.updtime,b.ordertaxtype as trntaxtype ,2 as digit, a.trncustoid custref, a.trndisctype, a.trndiscamt, a.amtdischdr FROM QL_trnjualmst a LEFT JOIN ql_trnordermst b ON a.cmpcode=b.cmpcode AND a.branch_code = b.branch_code AND a.ordermstoid=b.ordermstoid inner join ql_trnorderdtl e on e.trnordermstoid = b.ordermstoid and e.branch_code=a.branch_code inner join QL_MSTITEM d on d.itemoid=e.itemoid INNER JOIN QL_mstcust c ON a.cmpcode=c.cmpcode AND a.trncustoid=c.custoid WHERE a.branch_Code='" & Branch & "' and a.cmpcode ='" & CompnyCode & "' AND a.trnjualmstoid=" & iOid

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                oid.Text = Trim(xreader.Item("trnjualmstoid"))
                trnjualdate.Text = Format(CDate(Trim(xreader.Item("trnjualdate").ToString)), "dd/MM/yyyy")
                trnjualno.Text = Trim(xreader.Item("trnjualno"))
                dbsino.Text = Trim(xreader.Item("trnjualno"))
                trnjualres1.Text = Trim(xreader.Item("trnjualres1"))
                refoid.Text = Trim(xreader.Item("ordermstoid"))
                orderamt.Text = GetStrData("select isnull(sum(trnorderdtlQTY * trnorderprice),0) from QL_trnorderdtl where trnordermstoid = " & refoid.Text & " and branch_code = '" & Branch & "'")

                referenceno.Text = Trim(xreader.Item("orderno"))
                trncustoid.Text = Trim(xreader.Item("trncustoid"))
                trnjualcust.Text = Trim(xreader.Item("custname"))
                trnorderdate.Text = Format(CDate(Trim(xreader.Item("orderdate").ToString)), "dd/MM/yyyy")
                'identifierno.Text = xreader("identifierno").ToString
                trnpaytype.SelectedValue = Trim(xreader.Item("trnpaytype"))
                currate.SelectedValue = Trim(xreader.Item("currencyoid"))
                currencyrate.Text = ToMaskEdit(1, 3)
                If currate.SelectedValue = "1" Then
                    trnamtjual.Text = ToMaskEdit(((xreader.Item("trnamtjual"))), 3)
                    trndiscamt.Text = ToMaskEdit(((xreader.Item("trndiscamt"))), 3)
                    amtdischdr.Text = ToMaskEdit(((xreader.Item("amtdischdr"))), 3)
                    trnamttax.Text = ToMaskEdit(((xreader.Item("trnamttax"))), 3)
                    trntaxpct.Text = ToMaskEdit(((xreader.Item("trntaxpct"))), 3)
                    If trndisctype.SelectedValue = "AMT" Then
                        disc.Text = (ToDouble(trnamtjual.Text) / ToDouble(orderamt.Text)) * ToDouble(trndiscamt.Text)
                    Else
                        disc.Text = ToDouble(trndiscamt.Text)
                    End If

                    If ToDouble(trntaxpct.Text) > 0 Then
                        SetControlTax(True)
                    Else
                        SetControlTax(False)
                    End If
                Else
                    trnamtjual.Text = NewMaskEdit(Trim(xreader.Item("trnamtjual")))
                    trndiscamt.Text = NewMaskEdit(Trim(xreader.Item("trndiscamt")))
                    amtdischdr.Text = NewMaskEdit(Trim(xreader.Item("amtdischdr")))
                    trnamttax.Text = NewMaskEdit(Trim(xreader.Item("trnamttax")))
                    trntaxpct.Text = Trim(xreader.Item("trntaxpct"))
                    If trndisctype.SelectedValue = "AMT" Then
                        disc.Text = ((trnamtjual.Text) / (orderamt.Text)) * trndiscamt.Text
                    Else
                        disc.Text = trndiscamt.Text
                    End If
                    If trntaxpct.Text > 0 Then
                        SetControlTax(True)
                    Else
                        SetControlTax(False)
                    End If

                End If
                taxtype.SelectedValue = Trim(xreader("trntaxtype").ToString)
                trndisctype.SelectedValue = Trim(xreader.Item("trndisctype"))
                taxtype.SelectedValue = Trim(xreader.Item("ordertaxtype"))
                trnjualnote.Text = Trim(xreader.Item("trnjualnote"))
                posting.Text = Trim(xreader.Item("trnjualstatus"))
                updUser.Text = Trim(xreader.Item("upduser"))
                updTime.Text = xreader.Item("updtime")
                custoid.Text = xreader.Item("custref")
                If posting.Text = "POST" Then
                    btnSave.Visible = False : btnPosting.Visible = False
                    btnDelete.Visible = False : imbPrint.Visible = True 
                Else
                    btnSave.Visible = False : btnPosting.Visible = True
                    btnDelete.Visible = False : imbPrint.Visible = False 
                End If
            End While
        End If
        xreader.Close()
        conn.Close()
        help.Visible = False

        help.Text = "../reportform/PrintReport.aspx?type=SI&cmpcode=" & CompnyCode & "&oid=" & iOid

        sSql = "SELECT distinct d.trnjualdtloid,d.trnjualmstoid,d.trnjualdtlseq,jm.trnsjjualmstoid sjrefoid,d.itemoid,(select gendesc from ql_mstgen where genoid = i.itemgroupoid) itemgroupcode,d.trnjualdtlqty salesdeliveryqty,d.trnjualdtlunitoid ,d.trnjualdtlprice orderprice,d.trnjualnote AS trnjualdtlnote,d.trnjualstatus,(select ordermstoid FROM ql_trnordermst o WHERE o.orderno = m.orderno AND o.ordermstoid=m.ordermstoid) ordermstoid,sj.trnsjjualdtloid sjrefdtloid,d.amtjualnetto,CASE ISNULL((SELECT promtype FROM ql_mstpromo p INNER JOIN  ql_trnordermst o ON p.promoid=o.promooid AND o.orderno = m.orderno AND o.ordermstoid=m.ordermstoid),'') WHEN 'BUNDLING' then (amtjualdisc*qty) else d.amtjualdisc+d.amtjualdisc1 END amtjualdisc,d.upduser,d.updtime,m.trnsjjualno salesdeliveryno,i.itemcode,i.itemdesc itemlongdesc,g.gendesc,sj.trnsjjualmstoid salesdeliveryoid, d.unitseq From ql_trnjualmst m inner join QL_trnjualdtl d on m.trnjualmstoid = d.trnjualmstoid and m.branch_code=d.branch_code inner join ql_trnsjjualmst jm on m.trnsjjualno=jm.trnsjjualno AND jm.branch_code=m.branch_code inner join ql_trnsjjualdtl sj on sj.trnsjjualmstoid = jm.trnsjjualmstoid AND sj.refoid=d.itemoid AND sj.Branch_code=d.branch_code INNER JOIN ql_mstgen g ON g.cmpcode=d.cmpcode AND d.trnjualdtlunitoid=g.genoid inner join QL_MSTITEM i on i.itemoid = d.itemoid WHERE d.trnjualmstoid=" & iOid & " AND d.cmpcode='" & CompnyCode & "' AND m.branch_Code='" & Branch & "' ORDER BY d.trnjualdtlseq"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet : mySqlDA.Fill(objDs, "data1")
        TblDtl.DataSource = objDs.Tables("data1")
        TblDtl.DataBind()
        Session("ItemLine") = objDs.Tables("data1").Rows.Count + 1
        Session("TblDtl") = objDs.Tables("data1")
        trnjualdtlseq.Text = Session("ItemLine")

        Dim userlogin As String = Session("UserID")
        If (lblCollection.Text = "") Then
            sSql = "SELECT  updtime_status [updtime], createuser [user] FROM ql_collectiondtl WHERE invoiceoid = '" & iOid & "'"
            Dim collection As DataTable = CreateDataTableFromSQL(sSql)

            sSql = "SELECT useroid_status[user], updtime_status [updtime] FROM ql_collectionstatus where invoiceoid = '" & iOid & "'"
            Dim collectionStatus As DataTable = CreateDataTableFromSQL(sSql)

            For i As Integer = 0 To collection.Rows.Count - 1
                lblCollection.Text &= "Collection <b>Out</b> date (" & collection.Rows(i).Item("updtime") & ") and user update '" & collection.Rows(i).Item("user") & "' <br />"
                If (i <= collectionStatus.Rows.Count - 1) Then
                    lblCollection.Text &= "Collection <b>Status</b> date (" & collection.Rows(i).Item("updtime") & ") and user update '" & collection.Rows(i).Item("user") & "' <br />"
                End If
            Next
        End If
    End Sub

    Public Sub SetControlForecast(ByVal status As Boolean)

    End Sub

    Public Sub SetControlSalesOrder(ByVal status As Boolean)
        tblDetilDso.Visible = status
        ' btnAddAllList.Visible = status

    End Sub

    Public Sub BindData(ByVal sWhere As String)
        Dim sls As String = GetStrData("select personoid from QL_MSTPERSON s INNER JOIN QL_MSTPROF pf on PERSONNAME=USERNAME Where USERID='" & Session("UserID").ToString.ToUpper & "' AND pf.BRANCH_CODE='" & DdlCabang.SelectedValue & "'")
        'End If
        If DdlCabang.SelectedValue <> "ALL" Then
            sWhere &= " AND so.branch_code='" & DdlCabang.SelectedValue & "'"
        End If

        Try
            sSql = "SELECT TOP 500 a.trnjualmstoid,a.trnjualno,a.trnjualdate,a.trnjualstatus,s.custname,trnjualnote,trnjualres1,a.orderno,a.trnsjjualno,curr.currencycode,curr.currencyoid,so.trnorderref,CONVERT(VARCHAR(8), a.updtime, 108) JamBuat,a.branch_code,0 AS efaktur,so.ordertaxtype AS taxable,a.printke,a.upduser_edit lastprintby,a.updtime_edit lastprinton,a.canceluser lastresetby,a.canceltime lastreseton,a.cancelnote resetke FROM QL_trnjualmst a INNER JOIN QL_mstcust s ON a.cmpcode=s.cmpcode AND a.trncustoid=s.custoid LEft JOIN ql_trnordermst so ON so.cmpcode=a.cmpcode AND so.branch_code=a.branch_code AND so.ordermstoid=a.ordermstoid inner join QL_mstcurr curr on so.currencyoid = curr.currencyoid inner join QL_MSTPERSON p on so.salesoid = p.PERSONOID WHERE a.cmpcode='" & CompnyCode & "'"
            Dim sOther As String = ""
            If chkPeriod.Checked = True Then
                sOther &= " AND a.trnjualdate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "yyyy-MM-dd") & " 23:59:59'"
            End If

            If chkStatus.Checked Then
                If DDLStatus.SelectedIndex <> 0 Then
                    sOther &= " AND a.trnjualstatus LIKE '%" & DDLStatus.SelectedValue & "%'"
                Else
                    sOther &= " AND a.trnjualstatus LIKE '%%' "
                End If
            Else
                sOther &= " AND a.trnjualstatus LIKE '%%' "
            End If
            Dim salesperson As String = Session("UserID")
            sSql &= sWhere & " " & sOther & " ORDER BY trnjualmstoid DESC, trnjualno"
            Dim dtData As DataTable = CreateDataTableFromSQL(sSql)
            Tbldata.DataSource = dtData
            Tbldata.DataBind()
            Session("JualMst") = dtData
        Catch ex As Exception
            showMessage(ex.ToString, "", 1)
            Exit Sub
        End Try

    End Sub

    Public Sub BindDataSJjual()
        sSql = "select sd.salesdeliveryoid as trnsjjualmstoid,sd.salesdeliveryno ,so.orderno,so.orderoid trnrefoid,sd.salesdeliverynote trnsjjualnote,so.orderdate," & _
        "sd.salesdeliverydate trnsjjualdate,sd.salesdeliveryshipdate trnsjjualsenddate,sd.custoid ," & _
        "c.custname,so.ordercurroid,so.ordercurrate " & _
        "from ql_trnordermst so inner join ql_trnsalesdelivmst sd on so.orderoid=sd.ordermstoid  " & _
        "and sd.salesdeliverystatus='POST' or sd.salesdeliverystatus='Approved' AND sd.cmpcode=so.cmpcode " & _
        "inner join ql_trnsalesdelivdtl sdd on sdd.salesdeliverymstoid =sd.salesdeliveryoid " & _
        "inner join ql_trnorderdtl sod on so.ioref=sod.ordermstoid and sod.orderdtloid=sdd.orderdtloid " & _
        "and sod.free='N' inner join ql_mstcust c ON sd.custoid=c.custoid AND sd.cmpcode=c.cmpcode " & _
        "WHERE so.cmpcode='" & CompnyCode & "' and so.orderoid='" & refoid.Text & "'" & _
        " group BY sd.salesdeliveryoid ,sd.salesdeliveryno ,so.orderno,so.orderoid" & _
        " ,sd.salesdeliverynote ,so.orderdate,sd.salesdeliverydate ," & _
        "sd.salesdeliveryshipdate ,sd.custoid ,c.custname,so.ordercurroid,so.ordercurrate"
        FillGV(gvSJ, sSql, "ql_trnsjjualmst")
    End Sub

    Public Sub BindDataListOrder(ByVal sWhere As String)
        sSql = "select so.orderno,so.orderoid,convert(char(10),so.orderdate,103)orderdate,sd.custoid ,c.custname,so.ordercurroid,convert(char(10),so.ordercurrate,103)ordercurrate,so.ordertaxpct,so.hdiscamtpct,ISNULL(so.digit,2) as digit, so.paytermoid, so.hdisctype, so.taxable from ql_trnordermst so inner join ql_trnsalesdelivmst sd on so.orderoid=sd.ordermstoid and sd.salesdeliverystatus='POST' or sd.salesdeliverystatus='Approved' AND sd.cmpcode=so.cmpcode inner join ql_trnsalesdelivdtl sdd on sdd.salesdeliverymstoid =sd.salesdeliveryoid inner join ql_trnorderdtl sod on  so.ioref=sod.ordermstoid and sod.orderdtloid=sdd.orderdtloid and sod.free='N' inner join ql_mstcust c ON sd.custoid=c.custoid  AND sd.cmpcode=c.cmpcode WHERE so.cmpcode='" & CompnyCode & "' and so.orderno not like '%E' " & sWhere & " AND so.market='Local' group by so.orderno,so.orderoid,so.orderdate,sd.custoid ,c.custname,so.ordercurroid,so.ordercurrate,so. ordertaxpct,so.hdiscamtpct,digit, so.paytermoid,so.hdisctype, so.taxable"
        ClassFunction.FillGV(gvListSO, sSql, "ql_trnordermst")
    End Sub

    Private Sub binddataSJDetil(ByVal sjMstoid As Integer)
        sSql = "SELECT i.itemlongdesc,sjd.salesdeliveryqty trnsjjualdtlqty,od.itemgroupcode,od.free,g.gendesc FROM ql_trnsalesdelivdtl sjd inner join ql_trnorderdtl od on od.orderdtloid=sjd.orderdtloid inner join ql_trnsalesdelivdtlbatch b on sjd.salesdeliverydtloid=b.salesdeliverydtloid INNER JOIN ql_mstitem i ON b.cmpcode=i.cmpcode AND b.itemoid=i.itemoid INNER JOIN ql_mstgen g ON b.delivdtlunitoid=g.genoid AND sjd.cmpcode=g.cmpcode AND sjd.salesdeliverymstoid=" & sjMstoid & " AND sjd.cmpcode='" & CompnyCode & "'"
        FillGV(tblDetilDso, sSql, "ql_trnsjjualdtl")
        PanelDetilDSO.Visible = True : btnHideDetilDso.Visible = True
        ModalPopupExtenderDetilDSO.Show()
    End Sub 

    Private Sub SetControlTax(ByVal state As Boolean)
        lblTaxPct.Visible = state : trntaxpct.Visible = state
        lblTaxAmt.Visible = state : trnamttax.Visible = state
    End Sub 
#End Region

#Region "Event"

    Protected Sub btnsearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If trnjualref.SelectedValue = "ql_trnordermst" Then
            BindDataListOrder("")
            PanelSO.Visible = True
            btnHideso.Visible = True
            mpeso.Show()
        End If 
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branch As String = Session("branch")
            Dim branchId As String = Session("branch_id")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSI")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("branch") = branch
            Session("branch_id") = branchId
            Session("Role") = xsetRole
            Session("SearchSI") = sqlSearch
            Session("click_post") = False
            Response.Redirect("~\Transaction\trnnotajual.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Sales Invoice"

        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            InitDdlCabang() : BindData("") : InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update"
                Filltextbox(Session("branch_code"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                btnsearchSO.Visible = False : ImageButton2.Visible = False
                lblUpdate.Text = "Last Update By"
                lblOn.Text = "On"
            Else
                i_u.Text = "New"
                updUser.Text = Session("UserID")
                updTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                trnjualdtlseq.Text = 1 : Session("ItemLine") = trnjualdtlseq.Text 
                trnjualdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                'generateNo()
                periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text)))
                btnDelete.Visible = False : imbPrint.Visible = False
                btnsearchSO.Visible = False : ImageButton2.Visible = False
                btnSave.Visible = False
                TabContainer1.ActiveTabIndex = 0
                lblUpdate.Text = "Create By"
                lblOn.Text = "On"
            End If
            If Session("UserID") = "admin" Then
                btnPosting.Visible = False
            End If
            Dim objTable As New DataTable : objTable = Session("TblDtl")
            TblDtl.DataSource = objTable : TblDtl.DataBind()
            SumPriceInInvoiceDetail(objTable)

            Dim objTable2 As New DataTable : objTable2 = Session("DtlBiaya") 
            SumBiayaDetail(objTable2) : ReAmount()
        End If
    End Sub

    Protected Sub trnjualref_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TblDtl") = Nothing

        referenceno.Text = ""
        refoid.Text = ""
        trnjualcust.Text = ""
        trncustoid.Text = ""
        If trnjualref.SelectedValue = "ql_trnforcmst" Then
            SetControlForecast(True)
            SetControlSalesOrder(False)
        End If
        If trnjualref.SelectedValue = "ql_trnordermst" Then
            SetControlForecast(False)
            SetControlSalesOrder(True)
        End If
    End Sub

    Protected Sub gvSJ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Cek SJ
        Dim dtlTable As DataTable
        If Session("TblDtl") Is Nothing Then
            dtlTable = setTabelDetail() : Session("TblDtl") = dtlTable
        End If
        dtlTable = Session("TblDtl")
        Dim dv As DataView = dtlTable.DefaultView
        dv.RowFilter = "salesdeliveryoid='" & gvSJ.SelectedDataKey.Item(0) & "'"
        If dv.Count > 0 Then
            lblState.Text = "SDO"
            showMessage("This data has been added before. Please check.", CompnyName & " - WARNING", 2)
            Exit Sub
        End If : dv.RowFilter = ""

        sSql = "SELECT sj.salesdeliveryno,sum(sjd.salesdeliveryqty) salesdeliveryqty, sod.orderprice, sod.itemgroupcode, g.gendesc, sjb.delivdtlunitoid,sj.salesdeliveryoid,sjd.salesdeliverydtloid,sjb.itemoid FROM ql_trnsalesdelivdtl sjd INNER JOIN ql_trnsalesdelivmst sj ON sjd.cmpcode=sj.cmpcode AND sjd.salesdeliverymstoid=sj.salesdeliveryoid inner join ql_trnsalesdelivdtlbatch sjb on sjd.cmpcode=sjb.cmpcode and sjd.salesdeliverydtloid=sjb.salesdeliverydtloid inner join ql_trnorderdtl sod on sjd.orderdtloid=sod.orderdtloid  INNER JOIN ql_mstgen g ON sjd.cmpcode=g.cmpcode AND sjb.delivdtlunitoid=g.genoid WHERE sod.free='N' and sjd.cmpcode='" & CompnyCode & "' AND sj.salesdeliveryoid=" & gvSJ.SelectedDataKey.Item(0) & " group by sj.salesdeliveryno,sod.orderprice, sod.itemgroupcode, g.gendesc, sjb.delivdtlunitoid,sj.salesdeliveryoid,sjd.salesdeliverydtloid,sjb.itemoid"

        Dim objSjdetil As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
        Dim objTbldetil As DataTable = Session("TblDtl")
        For c1 As Int16 = 0 To objSjdetil.Rows.Count - 1
            Dim objrow As DataRow = objTbldetil.NewRow
            objrow("trnjualdtlseq") = Session("ItemLine") + c1
            objrow("trnjualdtloid") = 0
            objrow("trnjualmstoid") = 0 
            objrow("sjrefoid") = objSjdetil.Rows(c1).Item("salesdeliveryoid")
            objrow("itemoid") = objSjdetil.Rows(c1).Item("itemoid")
            objrow("salesdeliveryqty") = objSjdetil.Rows(c1).Item("salesdeliveryqty")
            objrow("trnjualdtluom") = objSjdetil.Rows(c1).Item("delivdtlunitoid")
            objrow("orderprice") = objSjdetil.Rows(c1).Item("orderprice")
            objrow("trnjualdtlnote") = ""
            objrow("trnjualstatus") = ""
            objrow("amtjualnetto") = objSjdetil.Rows(c1).Item("salesdeliveryqty") * objSjdetil.Rows(c1).Item("orderprice")
            objrow("upduser") = Session("UserID")
            objrow("updtime") = GetServerTime()
            objrow("salesdeliveryno") = gvSJ.SelectedDataKey.Item(1)
            objrow("itemgroupcode") = objSjdetil.Rows(c1).Item("Itemgroupcode")
            objrow("gendesc") = objSjdetil.Rows(c1).Item("gendesc")
            objrow("salesdeliveryoid") = objSjdetil.Rows(c1).Item("salesdeliveryoid")
            objrow("salesdeliverydtloid") = objSjdetil.Rows(c1).Item("salesdeliverydtloid")
            'objrow("trnjualpackingqty") = objSjdetil.Rows(c1).Item("packingqty")
            objTbldetil.Rows.Add(objrow)
        Next
        Session("ItemLine") += objSjdetil.Rows.Count
        trnjualdtlseq.Text = Session("ItemLine")
        Session("TblDtl") = objTbldetil
        SumPriceInInvoiceDetail(Session("TblDtl")) : ReAmount()

        TblDtl.DataSource = objTbldetil : TblDtl.DataBind()
        'ReAmount()
        panelDso.Visible = False : btnHideDSO.Visible = False : ModalPopupExtenderdso.Hide()
        TblDtl.SelectedIndex = -1
        cProc.DisposeGridView(gvSJ)
    End Sub  

    Protected Sub trndisctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trndisctype.SelectedValue = "AMT" Then 'amount
            lbl1.Text = "Discount (Amt)"
        Else
            lbl1.Text = "Discount (%)"
        End If
        trndiscamt.Text = "0.00"
        ReAmount()
    End Sub

    Protected Sub lkbCloseSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelSO.Visible = False : btnHideso.Visible = False
        mpeso.Hide() : cProc.DisposeGridView(gvListSO)
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblState.Text = "SDO" Then
            ModalPopupExtenderdso.Show()
            lblState.Text = ""
        ElseIf lblState.Text = "REDIR" Then
            lblState.Text = ""
            Response.Redirect("~\Transaction\trnnotajual.aspx?awal=true")
        Else
            lblState.Text = ""
        End If
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        referenceno.Text = "" : refoid.Text = ""
        trnjualcust.Text = "" : trncustoid.Text = ""
        trnorderdate.Text = "" : currate.SelectedIndex = 0
        currate_SelectedIndexChanged(Nothing, Nothing)
        identifierno.Text = ""
        Session("TblDtl") = Nothing : cProc.DisposeGridView(TblDtl)
        Session("ItemLine") = 1 : trnjualdtlseq.Text = Session("ItemLine")
        SumPriceInInvoiceDetail(Session("TblDtl")) : ReAmount()
    End Sub 
 
    Protected Sub closeDSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        panelDso.Visible = False : btnHideDSO.Visible = False
        ModalPopupExtenderdso.Hide() : cProc.DisposeGridView(gvSJ)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(currate.SelectedValue), toDate(trnjualdate.Text))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim sMsg As String = "" : Dim pay As Double

        pay = GetStrData("select genother1 from QL_mstgen where gengroup = 'paytype' and genoid =" & trnpaytype.SelectedValue & " ")
        If trnjualno.Text.Trim = "" Then
            sMsg &= "- Please fill Invoice No!!<BR>" 'Please select a PO Number 
        Else
            ' Check same TrnJualNo
            Dim sCheck As String = "SELECT count(-1) FROM ql_trnjualmst WHERE trnjualno='" & trnjualno.Text & _
                "' AND cmpcode='" & CompnyCode & "' and branch_code='" & Session("branch_id") & "'"
            If (Session("oid") = Nothing Or Session("oid") = "") Then
                If cKoneksi.ambilscalar(sCheck) > 0 Then
                    generateNo()
                End If
            Else
                If cKoneksi.ambilscalar(sCheck & " AND trnjualmstoid<>" & oid.Text) > 0 Then
                    generateNo()
                End If
            End If
        End If
        If refoid.Text.Trim = "" Then
            sMsg &= "- Please select Sales Order!!<BR>" 'Please select a Supplier
        End If
        If IsDate(toDate(trnjualdate.Text)) = False Then
            sMsg &= "- Invalid Invoice Date!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            posting.Text = "" : Exit Sub
        End If

        'cek apa sudah ada ITEM dari SO
        If Session("TblDtl") Is Nothing Then
            showMessage("No Invoice detail!!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            posting.Text = "" : Exit Sub
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                showMessage("No Invoice detail!!", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                posting.Text = "" : Exit Sub
            End If
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnjualno.Text, "trnjualno", "ql_trnjualmst") Then
                generateNo()
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            
        Else
            'update tabel master    
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                'sSql = "UPDATE QL_trnjualmst SET periodacctg= '" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & "', trnjualdate ='" & CDate(toDate(trnjualdate.Text)) & "',trnjualref = '" & Tchar(trnjualres1.Text) & "', trncustoid= " & trncustoid.Text & ", trnpaytype = " & trnpaytype.SelectedValue & ", trnamtjual = " & ToDouble(trnamtjual.Text) & ", trnamtjualidr = " & ToDouble(trnamtjual.Text) * cRate.GetRateMonthlyIDRValue & ", trnamtjualusd = " & ToDouble(trnamtjual.Text) * cRate.GetRateMonthlyUSDValue & ", trntaxpct = " & ToDouble(trntaxpct.Text) & ", trnamttax = " & ToDouble(trnamttax.Text) & ", trnamttaxidr = " & ToDouble(trnamttax.Text) * cRate.GetRateMonthlyIDRValue & ", trnamttaxusd = " & ToDouble(trnamttax.Text) * cRate.GetRateMonthlyUSDValue & ",trndisctype = '" & trndisctype.SelectedValue & "',trndiscamt = " & ToDouble(trndiscamt.Text) & ",trndiscamtidr = " & ToDouble(trndiscamt.Text) * cRate.GetRateMonthlyIDRValue & ",trndiscamtusd = " & ToDouble(trndiscamt.Text) * cRate.GetRateMonthlyUSDValue & ", trnamtjualnetto= " & ToDouble(totalinvoice.Text) & ",trnamtjualnettoidr= " & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyIDRValue & ",trnamtjualnettousd= " & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyUSDValue & ", trnjualnote = '" & Tchar(trnjualnote.Text) & "',trnjualstatus = '" & posting.Text & "', trnjualres1 = '" & Tchar(trnjualres1.Text) & "',amtdischdr = " & ToDouble(amtdischdr.Text) & ",currencyoid = " & currate.SelectedValue & ",currencyrate = " & ToDouble(currencyrate.Text) & ",upduser = '" & Session("UserID") & "',updtime= CURRENT_TIMESTAMP,trncustname = '" & Tchar(trnjualcust.Text) & "' WHERE branch_code='" & Session("branch_id") & "' and cmpcode='" & CompnyCode & "' and trnjualmstoid=" & oid.Text
                sSql = "UPDATE QL_trnjualmst SET trnjualstatus = '" & posting.Text & "' WHERE branch_code='" & Session("branch_id") & "' and cmpcode='" & CompnyCode & "' and trnjualmstoid=" & oid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Reverse SJDetail Status (Invoiced) data lama
                'sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus='Approved' WHERE trnsjjualno IN (SELECT si.trnsjjualno FROM ql_trnjualmst si WHERE si.branch_code='" & Session("branch_id") & "' and si.cmpcode='" & CompnyCode & "' AND si.trnjualmstoid=" & oid.Text & ")"
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    'insert tabel detail
                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim trnjualdtloid As Int64 = GenerateID("QL_trnjualdtl", CompnyCode)
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1

                        'setting satuan 3
                        Dim setupsatuan3 As Decimal
                        'update to last price 
                        Dim konv1_2 As Integer : Dim konv2_3 As Integer
                        sSql = "select konversi1_2 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql : konv1_2 = xCmd.ExecuteScalar

                        sSql = "select konversi2_3 from ql_mstitem where itemoid =" & objTable.Rows(c1).Item("itemoid") & ""
                        xCmd.CommandText = sSql : konv2_3 = xCmd.ExecuteScalar

                        If objTable.Rows(c1).Item("unitseq") = 1 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv1_2 * konv2_3
                        ElseIf objTable.Rows(c1).Item("unitseq") = 2 Then
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty")) * konv2_3
                        Else
                            setupsatuan3 = ToDouble(objTable.Rows(c1).Item("salesdeliveryqty"))
                        End If

                        'sSql = "update ql_trnjualdtl set trnjualdtlqty_unit3 = " & setupsatuan3 & " where trnjualdtloid = " & objTable.Rows(c1).Item("trnjualdtloid") & " and branch_code='" & Session("branch_id") & "'"
                        'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        ' Update SJDetail status to Invoiced
                        sSql = "UPDATE ql_trnsjjualmst SET trnsjjualstatus='INVOICED' WHERE trnsjjualmstoid='" & objTable.Rows(c1).Item("salesdeliveryoid") & "' and branch_code='" & Session("branch_id") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                End If

                'if posting
                If posting.Text = "POST" Then
                    Dim totMat As Double = 0 : Dim objTable1 As DataTable = Session("TblDtl")
                    'update amount to ql_conmtr->trnsjjualdtl

                    'For c3 As Int16 = 0 To objTable1.Rows.Count - 1
                    '    sSql = "update Ql_conmtr set amount=" & ToDouble(objTable1.Rows(c3).Item("salesdeliveryqty")) * (ToDouble(objTable1.Rows(c3).Item("orderprice")) * ToDouble(currencyrate.Text)) & ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE formoid=" & objTable1.Rows(c3).Item("sjrefoid") & " and branch_code='" & Session("branch_id") & "' and formname='QL_trnsjjualdtl' and type='JUAL' and refoid=" & objTable1.Rows(c3).Item("itemoid") & ""
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'Next

                    Dim cekday As String = GetStrData("select gencode from QL_mstgen where gengroup = 'paytype' and genoid = " & trnpaytype.SelectedValue)
                    Dim days As Double = CInt(cekday.Trim.Replace("CSH", "").Replace("CRD", ""))
                    Dim vConARId As Integer = GenerateID("QL_conar", CompnyCode)
                    Dim dPayDueDate As Date = CDate(toDate(trnjualdate.Text)).AddDays(days)
                    Dim sVarAR As String = GetInterfaceValue("VAR_AR", Session("branch_id"))
                    If sVarAR = "?" Then
                        showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim iAROid As Integer = GetAccountOid(sVarAR, False)

                    ' =============== QL_conar
                    'sSql = "INSERT INTO QL_conar(cmpcode,branch_code,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnarflag,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,trnarnote, trnarres1,upduser,updtime) VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & vConARId & ",'QL_trnjualmst'," & oid.Text & ",0," & trncustoid.Text & ",'" & iAROid & "','" & posting.Text & "','','PIUTANG','" & CDate(toDate(trnjualdate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text))) & "',0,'1/1/1900','',0,'" & dPayDueDate & "'," & ToDouble(totalinvoice.Text) & "," & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(totalinvoice.Text) * cRate.GetRateMonthlyUSDValue & ",0,'SALES INVOICE (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'sSql = "update QL_mstoid set lastoid=" & vConARId & " where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "' "
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    ' ===================== POSTING GL
                    Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
                    Dim dvJurnal As DataView = dtJurnal.DefaultView
                    Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
                    Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
                    Dim vSeqDtl As Integer = 1 : dvJurnal.RowFilter = "seq=1"
                    ' JURNAL SALES INVOICE
                    If dvJurnal.Count > 0 Then
                        ' ============== QL_trnglmst
                        sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,branch_code,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime)" & _
                            "VALUES ('" & CompnyCode & "'," & vIDMst & ",'" & Session("branch_id") & "',CURRENT_TIMESTAMP,'" & GetDateToPeriodAcctg(GetServerTime()) & _
                            "','SALES INVOICE (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")','POST','" & Now().Date & "'," & _
                            "'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        For C2 As Integer = 0 To dvJurnal.Count - 1
                            Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                            If ToDouble(dvJurnal(C2)("debet").ToString) > 0 Then
                                dAmt = ToDouble(dvJurnal(C2)("debet").ToString) : sDBCR = "D"
                            Else
                                dAmt = ToDouble(dvJurnal(C2)("credit").ToString) : sDBCR = "C"
                            End If
                            ' ============== QL_trngldtl
                            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,upduser,updtime,glpostdate) " & _
                                "VALUES ('" & CompnyCode & "','" & Session("branch_id") & "'," & vIDDtl & "," & vSeqDtl & "," & vIDMst & "," & dvJurnal(C2)("id").ToString & ",'" & sDBCR & "'," & _
                                "" & dAmt & "," & dAmt * cRate.GetRateMonthlyIDRValue & "," & dAmt * cRate.GetRateMonthlyUSDValue & ",'" & trnjualno.Text & "','SALES INVOICE (" & Tchar(trnjualcust.Text) & "|NO=" & trnjualno.Text & ")',''," & _
                                "'" & Session("oid") & "','" & posting.Text & "','" & Session("UserID") & "',current_timestamp,current_timestamp)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            vSeqDtl += 1 : vIDDtl += 1
                        Next
                    End If

                    dvJurnal.RowFilter = ""
                    vIDMst += 1 : vSeqDtl = 1

                    'update lastoid ql_trnglmst
                    sSql = "update QL_mstoid set lastoid=" & vIDMst & " where tablename ='QL_trnglmst' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'update lastoid ql_trngldtl
                    sSql = "update QL_mstoid set lastoid=" & vIDDtl - 1 & " where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close() : posting.Text = ""
                pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Session("click_post") = "false"
                Exit Sub
            End Try
        End If
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        Response.Redirect("~\Transaction\trnnotajual.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnNotaJUAL.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If oid.Text.Trim = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data No Faktur Tidak Boleh Kosong!")) 'Please Choose Invoice No
            Exit Sub
        End If

        'without check data in other table
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            ' Delete data biaya lama
            Dim jualbiayaoid As Integer = cKoneksi.ambilscalar("SELECT jualbiayamstoid FROM ql_trnjualbiayamst WHERE cmpcode='" & _
                CompnyCode & "' AND trnjualmstoid=" & oid.Text)
            sSql = "DELETE ql_trnjualbiayadtl where cmpcode='" & CompnyCode & "' and jualbiayamstoid=" & jualbiayaoid
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE ql_trnjualbiayamst where cmpcode='" & CompnyCode & "' and jualbiayamstoid=" & jualbiayaoid
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            ' Reverse SJDetail Status (Invoiced) data lama

            sSql = "UPDATE ql_trnsalesdelivmst SET salesdeliverystatus='POST' WHERE salesdeliveryoid IN " & _
                   "(SELECT si.sjrefoid FROM ql_trnjualdtl si WHERE si.cmpcode='" & CompnyCode & _
                   "' AND si.trnjualmstoid=" & oid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from QL_trnjualdtl where trnjualmstoid=" & oid.Text & " AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from  QL_trnjualmst  where trnjualmstoid=" & oid.Text & " AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, "Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnnotajual.aspx?awal=true")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If chkPeriod.Checked = True Then
            Dim st1, st2 As Boolean : Dim sMsg As String = ""
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
            Catch ex As Exception
                sMsg &= "- Invalid Start date!!<BR>" : st1 = False
            End Try

            Try
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Invalid End date!!<BR>" : st2 = False
            End Try

            If st1 And st2 Then
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                    sMsg &= "- End date can't be less than Start Date!!"
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        Dim sWhere As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        BindData(sWhere)
        Session("SearchSI") = sqlTempSearch
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
        BindData("")
        ddlFilter.SelectedIndex = 0 : txtFilter.Text = ""
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles btnPosting.Click
        Dim cRate As New ClassRate()
        cRate.SetRateValue(CInt(currate.SelectedValue), toDate(trnjualdate.Text))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If Session("click_post") = "true" Then
            showMessage("Data ini Sudah di click posting !, Tekan Cancel kemudian cek di List Information", CompnyName & " - Warning", 2) 'Please select a PO Number
            btnPosting.Visible = False : btnSave.Visible = False
            btnDelete.Visible = False
            Exit Sub
        Else
            Session("click_post") = "true"
        End If

        Session("click_post") = True
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        dtlTable.Columns.Add("code", Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32")) ' utk seq bila 1 transaksi ada lebih 1 jurnal
        dtlTable.Columns.Add("seqdtl", Type.GetType("System.Int32")) ' utk seq bila 1 account bisa muncul dipakai berkali2
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
        Dim nuRow As DataRow
        'For C1 As Integer = 0 To dtDtl.Rows.Count - 1
        '    xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4='T'")
        'Next
        ' ============ JURNAL 1
        ' PIUTANG
        ' POTONGAN PENJUALAN
        '       PENJUALAN (salesdeliveryqty*orderprice)
        '       PPN KELUARAN

        ' ============ PIUTANG
        Dim sVarAR As String = GetInterfaceValue("VAR_AR", Session("branch_id"))
        If sVarAR = "?" Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAR, False)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (PIUTANG USAHA)"
            nuRow("debet") = ToDouble(totalinvoice.Text)
            nuRow("credit") = 0 : nuRow("id") = iAPOid
            nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        ' ============ POTONGAN PENJUALAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and jumlah diskon
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                Dim iItemAccount As String = GetInterfaceValue("VAR_DISC_JUAL", Session("branch_id"))
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_DISC_JUAL' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If

                Dim dItemDiscdtl As Double = ToDouble(dtDtl.Rows(C1)("amtjualdisc").ToString)
                Dim iAPdiscOid As Integer = GetAccountOid(iItemAccount, False)

                If dItemDiscdtl > 0 Then
                    If dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                        Dim oRow As DataRow = dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl)(0)
                        oRow.BeginEdit()
                        oRow("debet") += (dItemDiscdtl)
                        oRow.EndEdit()
                        dtJurnal.AcceptChanges()
                    Else
                        nuRow = dtJurnal.NewRow
                        nuRow("code") = GetCodeAcctg(iAPdiscOid)
                        nuRow("desc") = GetDescAcctg(iAPdiscOid) & " (POTONGAN PENJUALAN)"
                        'nuRow("debet") = (dItemDiscdtl * ToDouble(currencyrate.Text))
                        nuRow("debet") = (dItemDiscdtl)
                        nuRow("credit") = 0 : nuRow("id") = iAPdiscOid
                        nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                        dtJurnal.Rows.Add(nuRow)
                    End If
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PENJUALAN
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and HPP
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                'sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                'Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim iItemAccount As String = GetInterfaceValue("VAR_SALES", Session("branch_id"))
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_SALES' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If
                Dim iAPGudOid As Integer = GetAccountOid(iItemAccount, False)

                If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (ToDouble(dtDtl.Rows(C1)("orderprice") * cRate.GetRateMonthlyIDRValue)))
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()

                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iAPGudOid)
                    nuRow("desc") = GetDescAcctg(iAPGudOid) & " (PENJUALAN)"
                    nuRow("debet") = 0
                   nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("orderprice").ToString))
                    nuRow("id") = iAPGudOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PPN KELUARAN
        If ToDouble(trnamttax.Text) > 0 Then
            Dim sPPNK As String = GetInterfaceValue("VAR_PPN_JUAL", Session("branch_id"))
            If sPPNK = "?" Then
                showMessage("Please Setup Interface for PPN Keluaran on Variable 'VAR_PPN_JUAL'", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                Exit Sub
            Else
                Dim iPPNKOid As Integer = GetAccountOid(sPPNK, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(iPPNKOid)
                nuRow("desc") = GetDescAcctg(iPPNKOid) & " (PPN KELUARAN)"
                nuRow("debet") = 0
                nuRow("credit") = (ToDouble(trnamttax.Text))
                nuRow("id") = iPPNKOid : nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If

        'iSeq += 1
        ' ============ 1 JURNAL
        ' BEBAN POKOK PENJUALAN
        ' PERSEDIAAN

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET HPPValue and HPPAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarHPP As String = ""

                sVarHPP = GetInterfaceValue("VAR_HPP", "MSC")
                If sVarHPP = "?" Then
                    showMessage("Please Setup Interface for HPP on Variable 'VAR_HPP'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarHPP, True)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("debet") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (BEBAN POKOK PENJUALAN(HPP))"
                    nuRow("debet") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (dItemHPP / currencyrate.Text))
                    nuRow("credit") = 0 : nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET PersediaanValue and PersediaanAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarStock As String = ""
                sVarStock = GetInterfaceValue("VAR_GUDANG", Session("branch_id"))
                If sVarStock = "?" Then
                    showMessage("Please Setup Interface for Stock on Variable 'VAR_GUDANG'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarStock, False)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))
                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (PERSEDIAAN)"
                    nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (dItemHPP / currencyrate.Text))
                    nuRow("id") = iItemAccount : nuRow("debet") = 0
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If
        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
        posting.Text = "POST" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub Tbldata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Tbldata.PageIndexChanging
        UpdateCheckedList()
        Dim dt As DataTable = Session("JualMst")
        Tbldata.DataSource = dt
        Tbldata.PageIndex = e.NewPageIndex
        Tbldata.DataBind() 
    End Sub

    Protected Sub Tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate((e.Row.Cells(2).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub currate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FillCurrencyRate(currate.SelectedValue)
    End Sub

    Protected Sub taxtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If taxtype.SelectedIndex = 0 Then
            SetControlTax(False)
            trntaxpct.Text = "0.00"
        ElseIf taxtype.SelectedIndex = 1 Then
            SetControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 3)
        Else
            SetControlTax(True)
            trntaxpct.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT genother1 FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='TAX'")), 3)
        End If
        ReAmount()
    End Sub

    Protected Sub imbAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        DDLFindSO.SelectedIndex = 0 : txtFindSO.Text = ""
        BindDataListOrder("") : mpeso.Show()
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = " AND " & DDLFindSO.SelectedValue & " LIKE '%" & Tchar(txtFindSO.Text) & "%' "
        BindDataListOrder(sWhere) : mpeso.Show()
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(toDate(e.Row.Cells(2).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub lkbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        binddataSJDetil(sender.ToolTip)
    End Sub

    Protected Sub btnCloseDSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHideDetilDso.Visible = False : PanelDetilDSO.Visible = False : ModalPopupExtenderDetilDSO.Hide()
        ModalPopupExtenderdso.Show()
        cProc.DisposeGridView(tblDetilDso)
    End Sub

    Protected Sub gvSJ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSJ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate((e.Row.Cells(2).Text)), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub TblDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TblDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
        End If
    End Sub 

    Protected Sub TblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles TblDtl.RowDeleting
        Dim objTable As DataTable
        Dim objRow(), objRow2() As DataRow
        objTable = Session("TblDtl")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objRow2 = objTable.Select("salesdeliveryoid='" & objRow(e.RowIndex).Item("salesdeliveryoid") & "'")
        For C1 As Integer = 0 To objRow2.Length - 1
            objTable.Rows.Remove(objRow2(C1))
        Next
        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C2)
            dr.BeginEdit() : dr(2) = C2 + 1 : dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        TblDtl.DataSource = Session("TblDtl")
        TblDtl.DataBind()
        Session("ItemLine") = objTable.Rows.Count + 1
        trnjualdtlseq.Text = Session("ItemLine")
    End Sub  

    Protected Sub trnjualcosttype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub currencyrate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub imbPrintInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip

        Dim MisV As String = "" : Dim sMsg As String = ""
        Dim iPrintLimit As Integer = 3
        If FlagPrint.Text = "INTERNAL" Then
            MisV = Session("branch")
        Else
            MisV = sender.CommandName
        End If
 
        Dim sls As String = GetStrData("SELECT personoid from QL_MSTPERSON s INNER JOIN QL_MSTPROF pf on pf.personnoid=s.PERSONOID Where USERID='" & Session("UserID").ToString.ToUpper & "' AND pf.BRANCH_CODE='" & MisV & "'")
 
        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If

        ' ===== Update counter utk Printout SI - Muji (20190227)
        sSql = "SELECT trnjualmstoid, trnjualno, printke, trnjualstatus, upduser_edit, updtime_edit FROM QL_trnjualmst WHERE trnjualmstoid=" & sender.ToolTip & " AND branch_code='" & MisV & "'"
        Dim cek As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnjualmst")
        If cek.Rows.Count > 0 Then
            Dim sCek As DataView = cek.DefaultView
            sCek.RowFilter = "trnjualstatus='In Posting'"
            If sCek.Count > 0 Then
                sMsg &= "Maaf, Silahkan Posting Terlebih Dulu..!!<br />"
            End If

            sCek.RowFilter = "" : sCek.RowFilter = "ISNULL(printke,0)>=" & iPrintLimit
            If sCek.Count > 0 Then
                sMsg &= "Maaf, No SI : " & sCek.Item(0)("trnjualno") & " sudah pernah dicetak " & iPrintLimit & "x (Last Print By " & sCek.Item(0)("upduser_edit").ToString & " On " & sCek.Item(0)("updtime_edit").ToString & ")<br />Silakan kontak Team Pusat G24 atau administrator untuk Reset Counter Print untuk nota ini."
            End If
            sCek.RowFilter = ""
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        ' ===== Update counter utk Printout SI - 4n7JuK (20190227)
        Dim sUpdateCounter As String = UpdatePrintCounter(MisV, ToDouble(sender.ToolTip))
        If sUpdateCounter = "" Then
            Try
                If FlagPrint.Text = "INTERNAL" Then
                    report.Load(Server.MapPath("~/report/rptNotaJualIDR.rpt"))
                Else
                    report.Load(Server.MapPath("~/report/rptNotaJualIDR.rpt"))
                End If
                Dim sWhere As String = "Where m.trnjualmstoid='" & sender.ToolTip & "' and m.branch_Code='" & MisV & "'"
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("namaPencetak", Session("UserID"))
                cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
                Response.Buffer = False
                'Clear the response content and headers
                Response.ClearContent()
                Response.ClearHeaders()
                ' Export the Report to Response stream in PDF format and file name Customers
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sender.CommandArgument & "Sales_Invoice_" & Format(GetServerTime(), "dd_MM_yy"))

                report.Close() : report.Dispose()
            Catch ex As Exception
                report.Close() : report.Dispose()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Exit Sub
            End Try
        Else
            showMessage(sUpdateCounter, CompnyName & " - ERROR", 1)
        End If
    End Sub

    Protected Sub imbPrintPromo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text

        End If

        Dim cekSI As String = GetStrData("select trnjualstatus from ql_trnjualmst where trnjualmstoid=" & sender.ToolTip & " and branch_code='" & Session("branch_id") & "'")
        If cekSI = "In Posting" Then
            showMessage("Silahkan Posting Terlebih Dahulu!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Try
            report.Load(Server.MapPath("~/report/rptNotaJualIDR.rpt"))
            report.SetParameterValue("sWhere", " Where m.trnjualmstoid='" & sender.ToolTip & "' and m.branch_Code='" & Session("branch_id") & "' ")
            report.SetParameterValue("namaPencetak", Session("UserID"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sender.CommandArgument & "Sales_Invoice_" & Format(GetServerTime(), "dd_MM_yy"))

            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        'Response.Redirect("~\Transaction\trnnotajual.aspx?awal=true")
    End Sub

    Protected Sub imbPrintDeliveryOrder_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip
        Dim MisV As String = sender.CommandName
        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If

        Dim cekSI As String = GetStrData("select trnjualstatus from ql_trnjualmst where trnjualmstoid=" & sender.ToolTip & " and branch_code='" & MisV & "'")
        If cekSI = "In Posting" Then
            showMessage("Silahkan Posting Terlebih Dahulu!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim currency As String = GetStrData("select currencyoid from ql_trnjualmst where trnjualmstoid=" & sender.ToolTip & " and branch_code='" & MisV & "'")
        Try
            sSql = "SELECT j.trnsjjualmstoid FROM QL_trnjualmst a INNER JOIN QL_trnsjjualmst j ON j.trnsjjualno = a.trnsjjualno INNER JOIN QL_mstcust s ON a.cmpcode=s.cmpcode AND a.trncustoid=s.custoid INNER JOIN ql_trnordermst so ON so.cmpcode=a.cmpcode AND so.orderno=a.orderno inner join QL_mstcurr curr on so.currencyoid = curr.currencyoid inner join QL_MSTPERSON p on so.salesoid = p.PERSONOID WHERE a.cmpcode='MSC' and so.branch_code= '" & MisV & "' and a.trnjualmstoid = '" & sender.ToolTip & "' "
            Dim NoDO As String = cKoneksi.ambilscalar(sSql)

            report.Load(Server.MapPath("~/report/rptDeliveryOrderInvoice.rpt"))
            report.SetParameterValue("printSJ", "DELIVERY ORDER")
            report.SetParameterValue("id", NoDO)
            report.SetParameterValue("branch_id", MisV)
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'report.PrintOptions.PaperSize = PaperSize.PaperA4
            'report.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Delivery_Order" & Format(GetServerTime(), "dd_MM_yy"))

            report.Close()
            report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub lkbPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
        posting.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting.Visible = False : btnHidePosting.Visible = False : mpePosting.Hide()
    End Sub

    Protected Sub paymentype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLPosting(paymentype.SelectedValue, trnpaytype.SelectedItem.Text.Trim)
        mpePosting.Show()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Session("SearchSI") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Protected Sub trnjualdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(toDate(trnjualdate.Text)) Then
            showMessage("Invalid Invoice date !!", CompnyName & " - ERROR", 1) : Exit Sub
        End If
        periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(trnjualdate.Text)))
        If Format(CDate(toDate(trnjualdate.Text)), "yy") <> Mid(dbsino.Text, 4, 2) Then
            'generateNo()
        Else
            If dbsino.Text <> "" Then
                trnjualno.Text = dbsino.Text
            End If
        End If
    End Sub

    Protected Sub tblDetilDso_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 1)

        End If
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        FlagPrint.Text = "INTERNAL"
        imbPrintInvoice_Click(sender, e)
    End Sub 

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"
        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print
            If orderflag.Text = "MANUF" Then
                report.Load(Server.MapPath("~/report/FakturPajak.rpt"))
            ElseIf orderflag.Text = "TRADE" Then
                report.Load(Server.MapPath("~/report/FakturPajakTrading.rpt"))
            End If
            'report.Load(Server.MapPath("~/report/rptSalesInvoice.rpt"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            'report.PrintOptions.PrinterName = printerPOS
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btncust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Panelcust.Visible = True
        Buttoncust.Visible = True
        BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Panelcust.Visible = False : Buttoncust.Visible = False
        ModalPopupExtender1.Hide() : cProc.DisposeGridView(gvCustomer)
    End Sub

    Protected Sub imbFindcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sTempSql As String = " AND " & DDLcustid.SelectedValue & " LIKE '%" & Tchar(txtFindCustID.Text) & "%' "
        BindDataCust(sTempSql)
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub imbAllcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Panelcust.Visible = True
        Buttoncust.Visible = True
        BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.Visible = True
        Dim sTempSql As String = " AND " & DDLcustid.SelectedValue & " LIKE '%" & Tchar(txtFindCustID.Text) & "%' "
        BindDataCust(sTempSql)
        'BindDataCust("")
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCustomer.SelectedDataKey(0).ToString
        trnjualcust.Text = gvCustomer.SelectedDataKey(1).ToString
        Panelcust.Visible = False
        Buttoncust.Visible = False
        ModalPopupExtender1.Hide()
        txtFindCustID.Text = ""
        cProc.DisposeGridView(sender)
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub lkbPostPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        posting.Text = "POST" : btnSave_Click(Nothing, Nothing)
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
    End Sub

    Protected Sub lkbClosePreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, False)
        Response.Redirect("~\Transaction\trnNotaJUAL.aspx?awal=true")
    End Sub

    Protected Sub gvPreview_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPreview.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub postingfaktur_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        sSql = "UPDATE ql_trnjualmst SET nofakturpajak='" & Tchar(txtnofakturpajak.Text) & "', statusfakturpajak='POST', userfakturpajak='" & Session("UserID") & "' WHERE trnjualmstoid=" & oid.Text
        xCmd.CommandText = sSql
        xCmd.ExecuteNonQuery()

        objTrans.Commit() : xCmd.Connection.Close()
        Response.Redirect("~\Transaction\trnNotaJUAL.aspx?awal=true")
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.ToolTip ' OnClientClick="javascript:popup2('<%#getOID() %>')"

        If sender.ToolTip = "" Then
            sender.ToolTip = oid.Text
        End If
        Try
            'untuk print
            If orderflag.Text = "MANUF" Then
                report.Load(Server.MapPath("~/report/FakturPajak.rpt"))
            ElseIf orderflag.Text = "TRADE" Then
                report.Load(Server.MapPath("~/report/FakturPajakTrading.rpt"))
            End If
            'report.Load(Server.MapPath("~/report/rptSalesInvoice.rpt"))
            report.SetParameterValue("cmpcode", CompnyCode)
            report.SetParameterValue("trnjualmstoid", sender.ToolTip)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)
            'report.PrintOptions.PrinterName = printerPOS
            report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            'report.PrintToPrinter(1, False, 0, 0)
            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, sender.CommandArgument & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

            report.Close() : report.Dispose()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub trndiscamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndiscamt.TextChanged
        ReAmount()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(trnjualno.Text, Session("branch"), gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub Tbldata_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tbldata.SelectedIndexChanged
        Response.Redirect("trnnotajual.aspx?branch_code=" & Tbldata.SelectedDataKey("branch_code").ToString & "&oid=" & Tbldata.SelectedDataKey("trnjualmstoid") & "")
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Protected Sub imbXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("InvoiceOID") = sender.CommandArgument
        Session("InvoiceNO") = sender.ToolTip
        Try
            Dim sOid As String = Session("InvoiceOID") 
            report.Load(Server.MapPath("~/report/rptFakturXls.rpt"))
            Dim sWhere As String = ""
            sWhere = " WHERE jm.cmpcode='" & CompnyCode & "' And jm.trnjualmstoid in (" & sOid & ")"
            report.SetParameterValue("sWhere", sWhere)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "FP_" & Session("InvoiceNO") & "_" & Format(GetServerTime(), "yyyyMMdd"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1)
            report.Close()
            report.Dispose()
        End Try
    End Sub

    Protected Sub imbExportXLS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbExportXLS.Click
        UpdateCheckedList()
        Try
            Dim sOid As String = ""
            If Session("JualMst") IsNot Nothing Then
                Dim dv As DataView = Session("JualMst").DefaultView
                dv.RowFilter = "efaktur=1"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("trnjualmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            If sOid.Length > 0 Then
                sOid = Microsoft.VisualBasic.Left(sOid, sOid.Length - 1)
            Else
                showMessage("Tidak ada Invoice yang dipilih.", CompnyName & " - WARNING", 2)
                Exit Sub
            End If

            report.Load(Server.MapPath("~/report/rptFakturXls.rpt"))
            Dim sWhere As String = ""

            sWhere = " WHERE jm.cmpcode='" & CompnyCode & "' and jm.trnjualmstoid in (" & sOid & ")"

            report.SetParameterValue("sWhere", sWhere)

            Dim crConnInfo As New ConnectionInfo()
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, report)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "FP_" & Format(GetServerTime(), "yyyyMMdd"))
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1)
            report.Close()
            report.Dispose()
        End Try
    End Sub

    Protected Sub imbResetCounter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sOid() As String = sender.CommandArgument.ToString.Split(",")
        Dim sMsg As String = ""
        If sOid.Length > 0 Then
            sMsg = ResetPrintCounter(sOid(0), sOid(1))
            showMessage(sMsg, CompnyName & " - INFORMATION", 3, "REDIR")
        Else
            sMsg = "Can't Reset Print Counter because of invalid ID (" & sOid.ToString & ")"
            showMessage(sMsg, CompnyName & " - WARNING", 2, "REDIR")
        End If
    End Sub

#End Region  
End Class
