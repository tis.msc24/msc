<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMA.aspx.vb" Inherits="Transaction_MaterialAdjustment" title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" colspan="2" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Stock Adjusment"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                   <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Stock Adjustment :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlList" runat="server" Width="100%" __designer:wfdid="w27" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:Label id="Label4x" runat="server" Text="Filter" __designer:wfdid="w28"></asp:Label></TD><TD style="WIDTH: 2%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLMst" runat="server" CssClass="inpText" __designer:wfdid="w29"><asp:ListItem Value="resfield1">Draft No</asp:ListItem>
<asp:ListItem Value="stockadjno">Adj. No</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextMst" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" __designer:wfdid="w31"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w32" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w33"></asp:ImageButton> <asp:Label id="Label5x" runat="server" Text="to" __designer:wfdid="w34"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w35" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w37"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Status" __designer:wfdid="w38"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" __designer:wfdid="w39"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>Revised</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w40" Enabled="True" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w41" Enabled="True" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w42" Enabled="True" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w43" Enabled="True" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton> <asp:ImageButton id="btnAll" onclick="btnAll_Click1" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton> <asp:ImageButton id="btnPrintHdr" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="True" DataKeyNames="stockadjno" PageSize="8">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="Draft No"><ItemTemplate>
                                            <asp:LinkButton ID="lkbSelect" runat="server" OnClick="lkbSelect_Click" Text='<%# Eval("resfield1") %>'
                                                ToolTip="<%# GetTransID() %>"></asp:LinkButton>
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="stockadjno" HeaderText="Adj. No" SortExpression="stockadjno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockadjdate" HeaderText="Adj. Date" SortExpression="stockadjdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Business Unit" SortExpression="divname" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjcount" HeaderText="Adj Count" SortExpression="adjcount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="stockadjstatus" HeaderText="Status" SortExpression="stockadjstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
                                            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# GetTransID() %>' />
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="No data found."></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w48"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrintHdr"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="imgForm" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Stock Adjustment :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:RadioButton id="rbAdjust" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="ADJUSTMENT" Visible="False" GroupName="AdjustGroup" Checked="True" __designer:wfdid="w102"></asp:RadioButton> <asp:RadioButton id="rbInit" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="INITIAL" Visible="False" GroupName="AdjustGroup" __designer:wfdid="w103"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Cabang" __designer:wfdid="w126"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="dd_branch" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w127" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNo" runat="server" Text="Draft No" __designer:wfdid="w104"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="resfield1" runat="server" __designer:wfdid="w105"></asp:Label><asp:TextBox id="stockadjno" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w106" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Adj. Date" __designer:wfdid="w107"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockadjdate" runat="server" Width="60px" CssClass="inpTextDisabled" __designer:wfdid="w108" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w109"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False" __designer:wfdid="w110"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Status" __designer:wfdid="w111"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="stockadjstatus" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w112" Enabled="False">In Process</asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w113" TargetControlID="stockadjdate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w114" TargetControlID="stockadjdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label7" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Find Stock Filter :" __designer:wfdid="w115" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" Text="PIC" __designer:wfdid="w116"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="person" runat="server" Width="191px" CssClass="inpText" __designer:wfdid="w117"></asp:DropDownList> <asp:Label id="lblWarnType" runat="server" CssClass="Important" __designer:wfdid="w118"></asp:Label></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label3" runat="server" Text="Grup Barang" __designer:wfdid="w119"></asp:Label></TD><TD id="TD6" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="typeM" runat="server" CssClass="inpText" __designer:wfdid="w120" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label4" runat="server" Text="Sub Grup Barang" __designer:wfdid="w121"></asp:Label></TD><TD id="TD2" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD4" class="Label" align=left runat="server" Visible="false"><DIV style="TEXT-ALIGN: left"><asp:DropDownList id="mattype" runat="server" Width="135px" CssClass="inpText" __designer:wfdid="w122"></asp:DropDownList>&nbsp;<asp:DropDownList id="ddlJenis" runat="server" Width="52px" CssClass="inpText" Visible="False" __designer:wfdid="w123">
                                    <asp:ListItem Value="QL_MSTITEM">ITEM</asp:ListItem>
                                </asp:DropDownList></DIV></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label12" runat="server" Width="114px" Text="Nama/Kode Barang" __designer:wfdid="w124"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterText" runat="server" Width="192px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w125"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Lokasi" __designer:wfdid="w128"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="itemloc" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w129" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Width="98px" Font-Size="Small" Text="Status" __designer:wfdid="w130"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="status" runat="server" Width="76px" CssClass="inpText" __designer:wfdid="w131">
                                    <asp:ListItem>Aktif</asp:ListItem>
                                    <asp:ListItem>Tidak Aktif</asp:ListItem>
                                </asp:DropDownList>&nbsp;<asp:ImageButton id="imbFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w132"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w133"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=right><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 22px" class="Label" align=left><asp:Label id="lblCount" runat="server" __designer:wfdid="w134"></asp:Label></TD><TD style="HEIGHT: 22px" class="Label" align=right><asp:ImageButton id="imbAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w135"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=right><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w136"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvFindCrd" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w137" OnPageIndexChanging="gvFindCrd_PageIndexChanging" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastqty" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="New Qty"><ItemTemplate>
<asp:TextBox id="qtyin" runat="server" Width="75px" CssClass="inpText" Text='<%# String.Format("{0:#,##0.00}", GetAdjQty()) %>' __designer:wfdid="w1" ToolTip='<%# Eval("refoid") %>' OnTextChanged="qtyin_TextChanged" MaxLength="13"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w2" TargetControlID="qtyin" ValidChars="0123456789.,-"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="Note" runat="server" Width="224px" CssClass="inpText" Text='<%# Eval("note") %>' MaxLength="50" __designer:wfdid="w42"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="location" HeaderText="Location" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" __designer:wfdid="w15"></asp:TextBox> 
</EditItemTemplate>
<HeaderTemplate>
<asp:CheckBox id="chkSelectAll" runat="server" ToolTip="Select/Unselect All for Current Page" __designer:wfdid="w16" AutoPostBack="True" OnCheckedChanged="chkSelectAll_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelected" runat="server" Checked="<%# GetSelected() %>" ToolTip='<%# Eval("seq") %>' __designer:wfdid="w17"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleDtl" runat="server" Font-Bold="True" Text="Detail Stock Adjustment :" __designer:wfdid="w138" Font-Underline="True"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w139" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="Gainsboro"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refcode" HeaderText="Item Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="lastqty" DataFormatString="{0:#,##0.0000}" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjqty" DataFormatString="{0:#,##0.0000}" HeaderText="New Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:LinkButton id="lkbDelete" onclick="lkbDelete_Click" runat="server" CausesValidation="False" ForeColor="Red" Text="X" ToolTip='<%# Eval("seq") %>' __designer:wfdid="w18" CommandName="Delete"></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Text="No Stock Adjustment detail." CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w140"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w141"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left colSpan=3>Last Updated By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w142">-</asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w143">-</asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/save.png" ImageAlign="AbsMiddle" __designer:wfdid="w144"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w145"></asp:ImageButton> <asp:ImageButton id="imbPosting" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w146"></asp:ImageButton> <asp:ImageButton id="imbDelete" runat="server" ImageUrl="~/Images/delete.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w147"></asp:ImageButton> <asp:ImageButton id="imbPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w148"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w149" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w150"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbSave"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="700px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="WIDTH: 660px; TEXT-ALIGN: left" class="Label" align=left><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2><asp:Label id="lblState" runat="server" CssClass="Important" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

