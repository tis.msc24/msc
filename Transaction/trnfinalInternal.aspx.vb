Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Windows.Forms

Partial Class TrnFinalIntern
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument

    Dim xSet As Data.DataSet
    Dim xAdapter As New SqlDataAdapter
#End Region

#Region "Procedure"
    Private Sub ClearDtlSrv()
        ItemOid.Text = "0" : SerialNo.Text = ""
        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kerusakan.Text = "" : Qty.Text = ToMaskEdit(0.0, 3)
        reqdtloid.Text = 0 : FlagBarang.Text = ""
    End Sub

    Private Sub ClearSparts()
       lblfinitemname.Text = ""
        TypeBarang.Text = "" : ItemOid.Text = 0
        Kerusakan.Text = "" : Qty.Text = "0.00"
    End Sub

    Private Sub BindBarang()
        Try
            If lblreqoid.Text = "" Then
                showMessage("- Maaf, Anda belum memilih nomer Tanda Terima Sementara (TTS)...!!", 2)
                Exit Sub
            End If

            Dim dti As DataTable = Session("itemdetail")
            Dim iditem As String = ""
            If Session("itemdetail") Is Nothing Then
                iditem = "''"
            Else
                If dti.Rows.Count = 0 Then
                    iditem = "''"
                Else
                    If Not dti Is Nothing Then
                        For it As Integer = 0 To dti.Rows.Count - 1
                            iditem &= dti.Rows(it).Item("itemoid")
                            If it < dti.Rows.Count - 1 Then
                                iditem &= ","
                            End If
                        Next
                    Else
                        iditem = "''"
                    End If
                End If
            End If

            sSql = "SELECT con.conrefoid reqdtloid, rd.reqmstoid, con.itemoid, con.itemcode, con.itemdesc, ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00) reqqty, 'SERVICE' TypeFinal, con.branch_code ToBranch, Rd.snno, rd.kelengkapan, rd.reqdtljob, rd.typegaransi, 'BUAH' gendesc, con.mtrlocoid OidLoc, 'RUSAK' FlagBarang FROM (Select con.branch_code, con.conrefoid, i.itemoid, i.itemcode, i.itemdesc, qtyIn, qtyOut, gd.genoid mtrlocoid From QL_conmtr con Inner Join QL_mstitem i ON i.itemoid=con.refoid Inner Join QL_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' Where con.mtrlocoid NOT IN (-10,-9)) con Inner Join QL_TRNREQUESTDTL rd ON rd.reqdtloid=con.conrefoid Inner Join QL_TRNREQUEST rq ON rd.reqmstoid=rq.reqoid Inner Join QL_mstcust c ON c.custoid=rq.reqcustoid Where con.mtrlocoid=(SELECT l.genoid FROM QL_mstgen l INNER JOIN QL_mstgen c ON c.genoid=CAST(l.genother2 AS integer) AND l.gengroup='LOCATION' AND c.gengroup='CABANG' AND l.genother6='RUSAK' AND c.gencode=con.branch_code) AND rq.reqoid=" & lblreqoid.Text & " AND con.branch_code='" & DdlCabang.SelectedValue & "'  And (con.itemcode Like '%" & TcharNoTrim(lblfinitemname.Text) & "%' Or con.Itemdesc Like '%" & TcharNoTrim(lblfinitemname.Text) & "%') GROUP BY con.conrefoid, Rd.reqmstoid, con.itemoid, con.itemcode, con.itemdesc, con.branch_code, Rd.snno, rd.kelengkapan, rd.reqdtljob, rd.typegaransi, con.mtrlocoid HAVING ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00)>0.00"

            'sSql = "Select Rq.reqdtloid, Rq.reqmstoid, i.itemoid,i.itemcode ,i.itemdesc, ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00) reqqty, 'SERVICE' TypeFinal, con.branch_code ToBranch, Rq.snno, rq.kelengkapan, rq.reqdtljob, rq.typegaransi, 'BUAH' gendesc, con.mtrlocoid OidLoc, 'RUSAK' FlagBarang From QL_TRNREQUESTDTL Rq INNER JOIN QL_conmtr con ON con.conrefoid=Rq.reqdtloid INNER JOIN QL_mstitem i ON i.itemoid=con.refoid WHERE con.mtrlocoid=(SELECT c.genoid FROM QL_mstgen c INNER JOIN QL_mstgen g ON g.genoid=c.genother2 AND g.gengroup='CABANG' WHERE c.gengroup='LOCATION' AND c.genother5='RUSAK' AND g.gencode='" & DdlCabang.SelectedValue & "') AND Rq.reqmstoid=" & lblreqoid.Text & " AND con.branch_code='" & DdlCabang.SelectedValue & "' And (i.itemcode Like '%" & TcharNoTrim(lblfinitemname.Text) & "%' Or i.Itemdesc Like '%" & TcharNoTrim(lblfinitemname.Text) & "%') GROUP BY Rq.reqdtloid, Rq.reqmstoid, i.itemoid, i.itemcode, i.itemdesc, con.branch_code, Rq.snno, rq.kelengkapan, rq.reqdtljob, rq.typegaransi, con.mtrlocoid HAVING ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00)>0.00"

            Dim dtb As DataTable = cKon.ambiltabel(sSql, "QL_TRNREQUESTDTL")
            GvBarang.DataSource = dtb : Session("GvBarang") = dtb
            GvBarang.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub GeneratedID()
        If i_u.Text = "new" Then
            FinOid.Text = GenerateID("QL_TRNFINAL", CompnyCode)
            TrnFinalNo.Text = GenerateID("QL_TRNFINAL", CompnyCode)
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangnya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangnya, sSql)
            Else
                FillDDL(dCabangnya, sSql)
                dCabangnya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangnya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangnya, sSql)
            dCabangnya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangnya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Private Sub clearform()
        ddlfilter.SelectedIndex = 0 : cbperiod.Checked = False
        tbperiodstart.Text = Date.Now.ToString("dd/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        cbstatus.Checked = False : ddlfilterstatus.SelectedIndex = 0
        tbservicesno.Text = "" : lblreqoid.Text = ""

        TypeBarang.Text = "" : lblfinitemname.Text = ""
        lblfinstatus.Text = "In Process" : phone.Text = ""

        gvrequest.DataSource = Nothing : gvrequest.DataBind()
        ibdelete.Visible = False
        ibsearchsvc.Visible = True : ibdelsvc.Visible = True
        ddlpopfilter.SelectedIndex = 0 : tbpopfilter.Text = ""

        tbsparepartjob.Text = "" : lblspartstate.Text = "new" 

        lblcreate.Text = "Create On <span style='font-weight: bold;'>" & Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & Session("UserID") & "</span>"
    End Sub

    Private Sub bindDataFinal()
        Try
            sSql = "Select fm.FINOID,fm.BRANCH_CODE,cbp.gendesc CabangInp,fm.trnfinalintno trnfinalno,Convert(VarChar(20),fm.finaldate,103) FinalDate,rqm.reqcode,cba.gencode codeAs,cba.gendesc DescCbAs,fm.cabangasal,fm.finitemstatus finstatus,cu.custoid,cu.custname,cu.custcode,fm.typetts From QL_trnfinalintmst fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.reqoid Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.BRANCH_CODE AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cba.gengroup='Cabang' Where " & ddlfilter.SelectedValue & " LIKE '%" & TcharNoTrim(tbfilter.Text) & "%'"

            If cbperiod.Checked = True Then
                sSql &= " AND fm.finaldate Between '" & CDate(toDate(tbperiodstart.Text)) & "' AND '" & CDate(toDate(tbperiodend.Text)) & "'"
            End If

            If cbstatus.Checked = True Then
                sSql &= " AND finitemstatus='" & ddlfilterstatus.SelectedValue & "'"
            End If

            If dCabangnya.SelectedValue <> "ALL" Then
                sSql &= " And fm.BRANCH_CODE='" & dCabangnya.SelectedValue & "'"
            End If
            sSql &= " Order By fm.updtime Desc"
            Session("datafinal") = Nothing
            Session("datafinal") = cKon.ambiltabel(sSql, "datafinal")
            gvdata.DataSource = Session("datafinal")
            gvdata.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & "<br />; " & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindItemRombeng()
        sSql = "Select 'False' AS CheckValue, itemoid AS itemrombengoid,itemcode,itemdesc,Case stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya,hpp,0.00 qty,'' notedtl3,0 mtrlocoid,'' gudang,0 seq,0 reqdtloid,0 itemrusakoid FROM QL_mstitem Where itemflag='AKTIF' AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' And stockflag NOT IN ('ASSET','V') Order By itemdesc"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
        If dtTbl.Rows.Count > 0 Then
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows(C1)("mtrlocoid") = DDLRombeng.SelectedValue
                dtTbl.Rows(C1)("gudang") = DDLRombeng.SelectedItem.Text
            Next
            dtTbl.AcceptChanges()
        End If
        Session("TblMat") = dtTbl
        Session("TblMatView") = dtTbl
    End Sub

    Private Sub BindItemBagus()
        sSql = "Select 'False' AS CheckValue, itemoid AS itembagusoid,itemcode,itemdesc,Case stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya,hpp,0.00 qty,'' notedtl4,0 mtrlocoid,'' gudang,0 seq,0 reqdtloid,0 itemrusakoid FROM QL_mstitem Where itemflag='AKTIF' AND " & DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%' And stockflag NOT IN('ASSET','V') Order By itemdesc"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstmat1")
        If dtTbl.Rows.Count > 0 Then
            For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                dtTbl.Rows(C1)("mtrlocoid") = LocoidBagusDDL.SelectedValue
                dtTbl.Rows(C1)("gudang") = LocoidBagusDDL.SelectedItem.Text
            Next
            dtTbl.AcceptChanges()
        End If
        Session("TblSO") = dtTbl
        Session("TblSOView") = dtTbl
    End Sub

    Private Sub fillTextForm(ByVal OidFinal As Integer)
        DdlCabang.Enabled = False
        Try
            Dim oid As Integer = Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "Select fm.FINOID,fm.BRANCH_CODE,cbp.gendesc DescCbInpt,fm.trnfinalintno trnfinalno,fm.finaldate,rqm.reqcode,cba.gencode CdCabangAs ,cba.gendesc CabangAsal,fm.finitemstatus FINSTATUS,cu.custoid,cu.custname,rqm.reqoid,cu.phone1,fm.UPDTIME,fm.UPDUSER,fm.CREATETIME,fm.CREATEUSER,fm.locoidrusak,typetts,flagfinal From QL_trnfinalintmst fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.reqoid Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.BRANCH_CODE AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cbp.gengroup='Cabang' Where fm.FINOID=" & OidFinal
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                'rowstate = 1
                While xreader.Read
                    Dim Sql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang' AND gencode='" & xreader("BRANCH_CODE").ToString & "'"
                    FillDDL(DdlCabang, Sql)
                    DdlCabang.SelectedValue = xreader("BRANCH_CODE").ToString
                    CabangAsal.Text = xreader("CdCabangAs").ToString
                    lblreqoid.Text = Integer.Parse(xreader("reqoid"))
                    FinOid.Text = Integer.Parse(xreader("FINOID"))
                    TrnFinalNo.Text = xreader("trnfinalno").ToString
                    custoid.Text = Integer.Parse(xreader("custoid"))
                    tbservicesno.Text = xreader("reqcode").ToString
                    DDLTypeTTS.SelectedValue = xreader("typetts").ToString
                    TypeTTS.Text = xreader("flagfinal").ToString
                    'tbsparepartjob.Text = xreader("trnfinalnote").ToString
                    lblfincust.Text = xreader("custname").ToString
                    phone.Text = xreader("phone1").ToString
                    tglFinal.Text = Format(xreader("finaldate"), "dd/MM/yyyy").ToString
                    lblfinstatus.Text = xreader("FINSTATUS").ToString
                    createtime.Text = Format(xreader("CREATETIME"), "dd/MM/yyyy HH:mm:ss tt")

                    If Not IsDBNull(xreader("upduser")) Or xreader("upduser").ToString <> "" Then
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("upduser").ToString & "</span>"
                    Else
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("createtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("createuser").ToString & "</span>"
                    End If
                End While
            End If
            DdlCabang_SelectedIndexChanged(Nothing, Nothing)
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            sSql = "Select fd1.finaldtloid1,fd1.finoid,fd1.seq,fd1.hpp,snno,fd1.notedtl1,fd1.reqdtloid,i.itemdesc,mt.gendesc gudang,fd1.mtrlocoid mtrloc,fd1.qty,i.ItemOid From QL_trnfinalintdtl1 fd1 Inner Join QL_mstitem i On i.itemoid=fd1.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd1.mtrlocoid AND mt.gengroup='LOCATION' Where fd1.finoid=" & Integer.Parse(OidFinal) & " Order By fd1.seq"

            Dim dtab As DataTable = cKon.ambiltabel2(sSql, "itemdetail")
            Session("itemdetail") = dtab : gvrequest.DataSource = dtab
            gvrequest.DataBind()

            sSql = "Select fd2.seq,fd2.trnfinaldtloid2 finaldtloid2,fd2.itemoid PartOid,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl2 notedtl1,mt.gendesc Gudang,fd2.reqoid,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp,fd2.finoid,fd2.itemoid itemrusakoid From QL_trnfinalintdtl2 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & OidFinal & " Order By fd2.seq"
            Dim dtab1 As DataTable = cKon.ambiltabel2(sSql, "SpartDtl")
            Session("SpartDtl") = dtab1 : GvParts.DataSource = dtab1
            GvParts.DataBind()

            sSql = "Select fd2.seq,fd2.itemoid itemrombengoid,fd2.itemrusakoid,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl3,mt.gendesc Gudang,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp From QL_trnfinalintdtl3 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & OidFinal & " Order By fd2.seq"
            Dim dtab2 As DataTable = cKon.ambiltabel2(sSql, "ItemRombeng")
            Session("ItemRombeng") = dtab2 : GvRombeng.DataSource = dtab2
            GvRombeng.DataBind()

            sSql = "Select fd2.seq,fd2.itemoid itembagusoid,itemrusakoid,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl4,mt.gendesc Gudang,fd2.reqoid,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp,fd2.finoid,fd2.itemoid itemrusakoid From QL_trnfinalintdtl4 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & OidFinal & " Order By fd2.seq"
            Dim dtab3 As DataTable = cKon.ambiltabel2(sSql, "GvBagus")
            Session("ItemBagus") = dtab3 : GvBagus.DataSource = dtab3
            GvBagus.DataBind()

            If lblfinstatus.Text = "APPROVED" Or lblfinstatus.Text = "in approval" Then
                ibdelete.Visible = False : BtnSendApp.Visible = False : ibsave.Visible = False
            ElseIf lblfinstatus.Text = "Rejected" Then
                ibdelete.Visible = False : BtnSendApp.Visible = False : ibsave.Visible = False
            Else
                ibdelete.Visible = True : BtnSendApp.Visible = True
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 2)
        Finally
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub BindOrderData()
        Try
            sSql = "SELECT con.branch_code CabangAsal, rq.reqoid, rq.reqcode, Convert(Varchar(20),rq.reqdate,103) reqdate, c.custname, c.custoid, c.phone1, rq.reqservicecat Cate, Case rq.reqservicecat When 'N' Then 'Service' When 'Y' Then 'Saldo Awal' When 'M' then 'Mutasi' Else 'Service' End sType FROM (Select con.branch_code, con.conrefoid, qtyIn, qtyOut, gd.genoid mtrlocoid From QL_conmtr con Inner Join QL_mstitem i ON i.itemoid=con.refoid Inner Join QL_mstgen gc ON gc.gencode=con.branch_code AND gc.gengroup='CABANG' Inner Join QL_mstgen gd ON gd.genoid=con.mtrlocoid AND gd.gengroup='LOCATION' Where con.mtrlocoid NOT IN (-10,-9)) con Inner Join QL_TRNREQUESTDTL rd ON rd.reqdtloid=con.conrefoid Inner Join QL_TRNREQUEST rq ON rd.reqmstoid=rq.reqoid Inner Join QL_mstcust c ON c.custoid=rq.reqcustoid Where con.mtrlocoid=(SELECT l.genoid FROM QL_mstgen l INNER JOIN QL_mstgen c ON c.genoid=CAST(l.genother2 AS integer) AND l.gengroup='LOCATION' AND c.gengroup='CABANG' AND l.genother6='RUSAK' AND c.gencode=con.branch_code) AND " & ddlpopfilter.SelectedValue & " LIKE '%" & Tchar(tbpopfilter.Text) & "%' AND con.branch_code = '" & DdlCabang.SelectedValue & "' Group BY con.branch_code, rq.reqoid, rq.reqcode, rq.reqdate, c.custname, c.custoid, c.phone1, rq.reqservicecat, rq.reqservicecat HAVING (SUM(qtyIn)-SUM(qtyOut)>0) ORDER BY rq.reqdate DESC"

            'sSql = "SELECT rq.branch_code CabangAsal, rq.reqoid, rq.reqcode, Convert(Varchar(20),rq.reqdate,103) reqdate, c.custname, c.custoid, c.phone1, rq.reqservicecat Cate, Case rq.reqservicecat When 'N' Then 'Service' When 'Y' Then 'Saldo Awal' When 'M' then 'Mutasi' Else 'Service' End sType FROM QL_TRNREQUEST rq INNER JOIN QL_mstcust c ON c.custoid=rq.reqcustoid INNER JOIN QL_TRNFINALSPART fd ON fd.reqoid=rq.reqoid WHERE rq.reqservicecat<>'' AND ISNULL((SELECT SUM(qtyIn)-SUM(qtyout) FROM QL_conmtr con WHERE con.mtrlocoid=(SELECT l.genoid FROM QL_mstgen l INNER JOIN QL_mstgen c ON c.genoid=CAST(l.genother2 AS integer) AND l.gengroup='LOCATION' AND c.gengroup='CABANG' AND l.genother6='RUSAK' AND c.gencode='" & DdlCabang.SelectedValue & "') AND con.conrefoid=fd.reqdtloid AND con.branch_code='" & DdlCabang.SelectedValue & "'),0.00) > 0.00 AND fd.trnfinaloid IN (SELECT FINOID FROM QL_TRNFINAL f WHERE (FINSTATUS NOT IN ('IN APPROVAL','In Process') OR FINITEMSTATUS NOT IN ('IN APPROVAL','In Process')) AND fd.branch_code=f.BRANCH_CODE) AND " & ddlpopfilter.SelectedValue & " LIKE '%" & Tchar(tbpopfilter.Text) & "%' GROUP BY rq.branch_code, rq.reqoid, rq.reqcode, rq.reqdate, c.custname, c.custoid, c.phone1, rq.reqservicecat ORDER BY rq.reqdate DESC"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "dataorderpop")
            gvpoporder.DataSource = dt : gvpoporder.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub createlist()
        Dim dtable As New DataTable
        dtable.Columns.Add("seqbarang", Type.GetType("System.Int32"))
        dtable.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtable.Columns.Add("reqitemoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("spartoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("spartmoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("partdescshort", Type.GetType("System.String"))
        dtable.Columns.Add("merk", Type.GetType("System.String"))
        dtable.Columns.Add("spartqty", Type.GetType("System.Decimal"))
        dtable.Columns.Add("spartprice", Type.GetType("System.Decimal"))
        dtable.Columns.Add("totalprice", Type.GetType("System.Decimal"))
        dtable.Columns.Add("mtrloc", Type.GetType("System.Int32"))
        Session("datadetail") = dtable
    End Sub

    Private Sub createdel()
        Dim objTable As New DataTable
        objTable.Columns.Add("spartoid", Type.GetType("System.Int32"))
        Session("delete") = objTable
    End Sub

    Private Sub InitGudangRusak()
        sSql = "SELECT a.genoid,a.gendesc AS gendesc From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND c.gencode='" & DdlCabang.SelectedValue & "' AND a.genother6='RUSAK'"
        FillDDL(DDLGudangRusak, sSql)
    End Sub

    Private Sub InitGudangRombeng()
        sSql = "SELECT a.genoid,a.gendesc AS gendesc From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND c.gencode='" & DdlCabang.SelectedValue & "' AND a.genother6 IN ('ROMBENG','RUSAK')"
        FillDDL(DDLRombeng, sSql)
    End Sub

    Private Sub InitLoc()
        sSql = "SELECT a.genoid,a.gendesc AS gendesc From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND a.gencode <> 'EXPEDISI' AND c.gencode='" & DdlCabang.SelectedValue & "' AND a.genother6='UMUM'"
        FillDDL(matLoc, sSql)
        FillDDL(LocoidBagusDDL, sSql)
    End Sub

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "PartOid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If


        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "PartOid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("QtyOs") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(4).Controls)), 3)
                            dv(0)("spartprice") = ToMaskEdit(ToDouble(GetTextValue(row.Cells(5).Controls)), 3)
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub Bindpartdata()
        If DDLTypeTTS.SelectedValue = "SERVICE" Then
            lblCaptItem.Text = "List Spareparts"
            LabelNya.Visible = True
        Else
            lblCaptItem.Text = "List Katalog Replace"
            LabelNya.Visible = False
        End If
        sSql = "Select *," & ItemOid.Text & " itemoid ,'' TypeFinal FROM (Select 'False' Checkvalue,i.itemcode partcode, i.itemdesc partdescshort,i.itemoid PartOid, g.gendesc satuan3, i.merk brand,i.hpp spartprice,0.00 partsqty,0.00 QtyOs,Isnull((Select SUM(qtyIn)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid And con.branch_code='" & DdlCabang.SelectedValue & "' AND con.periodacctg='" & GetDateToPeriodAcctg(GetServerTime()) & "' AND con.mtrlocoid=" & matLoc.SelectedValue & "),0.00) saldoAkhir,i.satuan1 PartUnitOid,g.gendesc partsUnit,'PARTS' FlagBarang From ql_mstitem i INNER JOIN ql_mstgen g on g.genoid=satuan1 and g.gengroup='ITEMUNIT' and itemflag='AKTIF' AND stockflag='T') dt Order by partdescshort"
        Dim dta As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        If dta.Rows.Count > 0 Then
            Session("TblListMat") = dta
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        Else
            showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
        End If
    End Sub

    Private Sub ClearDetail()
        SeqBarang.Text = "1"
        If Session("datarequest") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("datarequest")
            SeqBarang.Text = objTable.Rows.Count + 1
            GvBarang.SelectedIndex = -1
        End If
        I_Text2.Text = "New Detail"
        ItemOid.Text = "" : SerialNo.Text = ""
        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kerusakan.Text = "" : Qty.Text = ""
        reqdtloid.Text = ""
    End Sub 
#End Region

#Region "Function"
    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itembagusoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("notedtl4") = Tchar(GetTextBoxValue(row.Cells(4).Controls))
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                Dim pricelistmstoid As Integer = 0, pricelistdtloid As Integer = 0
                                dtView.RowFilter = "itembagusoid=" & cbOid
                                dtView2.RowFilter = "itembagusoid=" & cbOid

                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("notedtl4") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("notedtl4") = Tchar(GetTextBoxValue(row.Cells(4).Controls))
                                    End If
                                End If

                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemrombengoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("notedtl3") = Tchar(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("mtrlocoid") = GetDDLValue(row.Cells(5).Controls)
                                    dtView(0)("gudang") = GetDDLText(row.Cells(5).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                Dim pricelistmstoid As Integer = 0, pricelistdtloid As Integer = 0
                                dtView.RowFilter = "itemrombengoid=" & cbOid
                                dtView2.RowFilter = "itemrombengoid=" & cbOid

                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("notedtl3") = Tchar(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("mtrlocoid") = GetDDLValue(row.Cells(5).Controls)
                                    dtView(0)("gudang") = GetDDLText(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("qty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("notedtl3") = Tchar(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("mtrlocoid") = GetDDLValue(row.Cells(5).Controls)
                                        dtView2(0)("gudang") = GetDDLText(row.Cells(5).Controls)
                                    End If
                                End If

                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Public Function GetStatus() As Boolean
        If Eval("TypeFinal") = "REPLACE" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function deleterecord() As String
        Dim result As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "DELETE FROM QL_trnfinalintmst WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnfinalintdtl1 WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnfinalintdtl2 WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnfinalintdtl3 WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnfinalintdtl4 WHERE finoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try

        Return result
    End Function

    Private Function CrtDtlRusak() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("finoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("finaldtloid1", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqmstoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("Itemoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("snno", Type.GetType("System.String"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("qty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("mtrloc", Type.GetType("System.Int32"))
        dtab.Columns.Add("gudang", Type.GetType("System.String"))
        dtab.Columns.Add("notedtl1", Type.GetType("System.String"))
        Session("itemdetail") = dtab
        Return dtab
    End Function

    Private Function CrtDtlSpart() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("finoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("finaldtloid2", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("PartOid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemrusakoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemcode", Type.GetType("System.String"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("qty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("hpp", Type.GetType("System.Decimal"))
        dtab.Columns.Add("gudang", Type.GetType("System.String"))
        dtab.Columns.Add("notedtl1", Type.GetType("System.String"))
        Session("SpartDtl") = dtab
        Return dtab
    End Function

    Private Function CrtDtlRombeng() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemrombengoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemrusakoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemcode", Type.GetType("System.String"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("qty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("hpp", Type.GetType("System.Decimal"))
        dtab.Columns.Add("gudang", Type.GetType("System.String"))
        dtab.Columns.Add("notedtl3", Type.GetType("System.String"))
        Session("ItemRombeng") = dtab
        Return dtab
    End Function

    Private Function CrtDtlBagus() As DataTable
        Dim dtab = New DataTable
        dtab.Columns.Add("seq", Type.GetType("System.Int32"))
        dtab.Columns.Add("finoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("finaldtloid3", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itembagusoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemrusakoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("itemcode", Type.GetType("System.String"))
        dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtab.Columns.Add("qty", Type.GetType("System.Decimal"))
        dtab.Columns.Add("mtrlocoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("hpp", Type.GetType("System.Decimal"))
        dtab.Columns.Add("gudang", Type.GetType("System.String"))
        dtab.Columns.Add("notedtl4", Type.GetType("System.String"))
        Session("ItemBagus") = dtab
        Return dtab
    End Function

    Private Function AmbilData(ByVal query As String, ByVal nama As String) As DataTable
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'xSet.Clear()
        xSet = New Data.DataSet
        xAdapter = New SqlDataAdapter(query, conn)
        xAdapter.Fill(xSet, nama)
        Return xSet.Tables(nama)
        conn.Close()
    End Function

    Private Function AmbilData1(ByVal query As String, ByVal nama As String) As DataTable
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'xSet.Clear()
        xSet = New Data.DataSet
        xAdapter = New SqlDataAdapter(query, conn)
        xAdapter.Fill(xSet, nama)
        Return xSet.Tables(nama)
        conn.Close()
    End Function

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If 

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'Session.Timeout = 60
            Response.Redirect("trnfinalInternal.aspx")
        End If

        Page.Title = CompnyName & " - Final Services"
        Session("oid") = Request.QueryString("oid")

        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah Anda ingin menghapus data ini?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            lblspartstate.Text = "new" : fDDLBranch()
            DdlCabang_SelectedIndexChanged(Nothing, Nothing)
            Session("delete") = Nothing : Session("datadetail") = Nothing
            bindDataFinal() : InitCabang() : InitLoc() : InitGudangRombeng()
            InitGudangRusak()
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                lbltitle.Text = "New Final Services"
                clearform() : TabContainer1.ActiveTabIndex = 0
                tglFinal.Text = Format(GetServerTime, "01/MM/yyyy")
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                GeneratedID()
            Else
                lbltitle.Text = "Update Final Services"
                fillTextForm(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        Response.Redirect("~\Transaction\trnfinalInternal.aspx?awal=true")
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        bindDataFinal()
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        bindDataFinal()
        ddlfilter.SelectedIndex = 0 : tbfilter.Text = ""
        tbperiodstart.Text = Format(GetServerTime(), "01/MM/yyyy")
        tbperiodend.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbstatus.Checked = False : cbperiod.Checked = False
        ddlfilterstatus.SelectedIndex = 0 : lblerror.Text = ""
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click

        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblpopupmsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If

        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblpopupmsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblpopupmsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
            End If
        End If

        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblpopupmsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
            End If
        End If

        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblpopupmsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnSOItem.aspx?awal=true")
            End If
        End If

        If sValidasi.Text <> "" Then
            UpdateCheckedMat()
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.Visible = True
            sValidasi.Text = "" : mpeItem.Show()
        End If
    End Sub 

    Protected Sub ibsearchsvc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchsvc.Click
        bindorderdata() : cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, True)
    End Sub

    Protected Sub ibpopcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancel.Click
        ddlpopfilter.SelectedIndex = 0
        tbpopfilter.Text = ""
        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        lblerror.Text = ""
    End Sub

    Protected Sub ibdelsvc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelsvc.Click
        clearform()
    End Sub

    Protected Sub gvpoporder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpoporder.SelectedIndexChanged
        lblreqoid.Text = Integer.Parse(gvpoporder.SelectedDataKey.Item("reqoid"))
        tbservicesno.Text = gvpoporder.SelectedDataKey.Item("reqcode").ToString
        lblfincust.Text = gvpoporder.SelectedDataKey.Item("custname").ToString
        custoid.Text = gvpoporder.SelectedDataKey.Item("custoid").ToString
        phone.Text = gvpoporder.SelectedDataKey.Item("phone1").ToString
        CabangAsal.Text = gvpoporder.SelectedDataKey.Item("CabangAsal").ToString
        TypeTTS.Text = gvpoporder.SelectedDataKey.Item("Cate").ToString

        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        lblerror.Text = "" : lblspartstate.Text = "new"  
    End Sub

    Protected Sub ibpopfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfind.Click
        bindorderdata() : mpepopgv.Show()
    End Sub

    Protected Sub gvdata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdata.PageIndexChanging
        gvdata.PageIndex = e.NewPageIndex
        bindDataFinal()
        'gvdata.DataSource = Session("datafinal")
        'gvdata.DataBind()
    End Sub

    Protected Sub ibpopviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewall.Click
        ddlpopfilter.SelectedIndex = 0
        tbpopfilter.Text = ""
        bindorderdata() : mpepopgv.Show()
    End Sub

    Protected Sub ibsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsave.Click
        
        Dim result As String = "" : Dim serviceno As String = tbservicesno.Text
        Dim TotalHpp As Double = 0 : Dim ItemParts As Double = 0

        If DDLTypeTTS.SelectedValue = "SERVICE" Then
            lblCaptItem.Text = "Barang Spareparts"
        Else
            lblCaptItem.Text = "Barang Replace"
        End If

        If Not Session("itemdetail") Is Nothing Then
            Dim dtd As DataTable = Session("itemdetail")
            If ToDouble(dtd.Rows.Count) = 0 Then
                result &= "- Maaf, anda belum input Detail Barang yang di service..!!<br>"
            End If
        Else
            If gvrequest.Visible = False Then
                result &= "- Maaf, anda belum input Detail Barang yang di service..!!<br>"
            End If
        End If

        If ToDouble(lblreqoid.Text) = 0 Then
            result &= "- Maaf, Silahkan pilih nomor tanda terima dahulu..!!<br>"
        End If

        If Session("ItemRombeng") Is Nothing And Session("ItemBagus") Is Nothing Then
            result &= "- Maaf, Detail Barang Rusak Atau Rombeng hasil service dan Detail Barang Bagus salah satu harus terisi..!!<br>"
        End If

        sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & CompnyCode & "' AND periodacctg = '" & GetPeriodAcctg(GetServerTime()) & "' AND closingdate = '1/1/1900'"
        If GetScalar(sSql) = 0 Then
            result &= "- Mohon Maaf, Tanggal Ini Tidak Dalam Open Stock !!<br>"
            lblfinstatus.Text = "In Process"
        End If

        If Session("oid") <> "" Or Not Session("oid") Is Nothing Then
            If Not Session("itemdetail") Is Nothing Then
                Dim SaldoAkhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")

                If TypeTTS.Text <> "Y" Then
                    For j As Integer = 0 To dtab.Rows.Count - 1
                        sSql = "Select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab.Rows(j).Item("ItemOid")) & " AND mtrlocoid=" & Integer.Parse(dtab.Rows(j).Item("mtrloc")) & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND branch_code='" & DdlCabang.SelectedValue & "' Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.itemoid) tblitem"
                        If Integer.Parse(dtab.Rows(j).Item("ItemOid")) <> 0 Then
                            SaldoAkhire = GetScalar(sSql)
                            If SaldoAkhire = Nothing Or SaldoAkhire = 0 Then
                                result &= "- Maaf, Stok akhir untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' pada gudang " & dtab.Rows(j).Item("gudang") & " tidak memenuhi..!!<br>"
                                lblfinstatus.Text = "In Process"
                            End If
                        End If
                    Next

                    Dim dtab1 As DataTable = Session("SpartDtl")
                    For k As Integer = 0 To dtab1.Rows.Count - 1
                        sSql = "Select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab1.Rows(k).Item("PartOid")) & " AND mtrlocoid=" & Integer.Parse(dtab1.Rows(k).Item("mtrlocoid")) & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND branch_code='" & DdlCabang.SelectedValue & "' Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.itemoid) tblitem"
                        If Integer.Parse(dtab1.Rows(k).Item("PartOid")) <> 0 Then
                            SaldoAkhire = GetScalar(sSql)
                            If SaldoAkhire = Nothing Or SaldoAkhire = 0 Then
                                result &= "- Maaf, Stok akhir untuk barang '" & dtab1.Rows(k).Item("itemdesc") & "' pada gudang " & dtab1.Rows(k).Item("gudang") & " tidak memenuhi..!!<br>"
                                lblfinstatus.Text = "In Process"
                            End If
                        End If
                    Next
                End If

            End If
        End If

        If lblfinstatus.Text = "IN APPROVAL" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnfinalintmst' And branch_code LIKE '%" & DdlCabang.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                result &= "- Maaf, User untuk Cabang " & DdlCabang.SelectedItem.Text & " belum disetting, Silahkan hubungi admin..!!<br>"
            End If
        ElseIf lblfinstatus.Text = "Rejected" Then
            lblfinstatus.Text = "IN PROCESS"
        End If

        If checkApproval("QL_trnfinalintmst", "QL_trnfinalintmst", Session("oid"), "New", "FINAL", DdlCabang.SelectedValue) > 0 Then
            result &= "- Mohon Maaf, data ini sudah send Approval..!!<br>"
            lblfinstatus.Text = "IN APPROVAL"
        End If

        '-- Validasi untuk antisipasi dobel klik --
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnfinalintmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                result &= "- Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        Else
            ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
            sSql = "SELECT finitemstatus FROM QL_trnfinalintmst WHERE finoid = " & Integer.Parse(Session("oid")) & " AND cmpcode='" & CompnyCode & "'"
            Dim srest As String = GetStrData(sSql)

            If srest Is Nothing Or srest = "" Then
                result &= "- Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
            Else
                If srest.ToLower = "post" Then
                    result &= "- Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                End If
            End If
        End If
        '-- End Validasi untuk antisipasi dobel klik --

        If result <> "" Then
            lblfinstatus.Text = "in process"
            showMessage(result, 2)
            Exit Sub
        End If

        'GENERATED ULANG NO TRANSAKSI ANTISIPASI JIKA ADA USER INPUT BERSAMAN
        If Session("oid") = "" Or Session("oid") Is Nothing Then
            FinOid.Text = GenerateID("QL_trnfinalintmst", CompnyCode)
            TrnFinalNo.Text = GenerateID("QL_trnfinalintmst", CompnyCode)
        Else
            FinOid.Text = Session("oid")
            TrnFinalNo.Text = Session("oid")
        End If

        Dim AppOid As Integer = GenerateID("QL_Approval", CompnyCode)
        Dim finaldtloid1 As Integer = GenerateID("QL_trnfinalintdtl1", CompnyCode)
        Dim finaldtloid2 As Integer = GenerateID("QL_trnfinalintdtl2", CompnyCode)
        Dim finaldtloid3 As Integer = GenerateID("QL_trnfinalintdtl3", CompnyCode)
        Dim finaldtloid4 As Integer = GenerateID("QL_trnfinalintdtl4", CompnyCode)
        Dim LastHpp As Double = 0 
        '--------------------------------------------------------------------

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        'FIND CABANG BERDASAR PILIHAN GUDANG
        sSql = "SELECT gencode FROM QL_mstgen WHERE gengroup='CABANG' AND genoid=(SELECT genother2 FROM QL_mstgen WHERE gengroup='LOCATION' and genoid=" & matLoc.SelectedValue & ")"
        xCmd.CommandText = sSql : Dim CabangInp As String = xCmd.ExecuteScalar
        'Cek saldoakhir

        Try
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                sSql = "INSERT INTO [QL_trnfinalintmst] ([cmpcode],[branch_code],[finoid],[trnfinalintno],[finaldate],[reqoid],[custoid],[finitemstatus],[cabangasal],[typetts],[locoidrusak],[flagfinal],[createuser],[createtime],[upduser],[updtime])" & _
                " VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(FinOid.Text) & ",'" & TrnFinalNo.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(custoid.Text) & ",'" & lblfinstatus.Text & "','" & CabangAsal.Text & "','" & TypeTTS.Text & "',0,'INTERNAL','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '-- Update mstoid table ql_trnfinailntmst --
                sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(FinOid.Text) & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinalintmst'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE [QL_trnfinalintmst] SET [branch_code]='" & DdlCabang.SelectedValue & "',[trnfinalintno]='" & Session("oid") & "',[finaldate]=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),[reqoid] =" & Integer.Parse(lblreqoid.Text) & ",[custoid] =" & Integer.Parse(custoid.Text) & ",[finitemstatus] ='" & lblfinstatus.Text.ToLower & "',[cabangasal] ='" & CabangAsal.Text & "',[upduser] ='" & Session("UserID") & "',[updtime]=CURRENT_TIMESTAMP WHERE [finoid]=" & Integer.Parse(FinOid.Text) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '--- Insert Detail Barang Rusak --- 
            If Not Session("itemdetail") Is Nothing Then
                '-- Drop Tabel QL_trnfinalintdtl1 --
                sSql = "DELETE FROM QL_trnfinalintdtl1 WHERE finoid=" & Integer.Parse(FinOid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dtab As DataTable = Session("itemdetail")
                If dtab.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab.Rows.Count - 1
                        '-- Amlbil Hpp Terakhirnya --
                        sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab.Rows(i).Item("itemoid")) & ""
                        xCmd.CommandText = sSql : LastHpp = xCmd.ExecuteScalar

                        '-- Create Tabel QL_trnfinalintdtl1 --
                        sSql = "INSERT INTO [QL_trnfinalintdtl1] ([cmpcode],[branch_code],[finaldtloid1],[finoid],[seq],[reqoid],[reqdtloid],[itemoid],[mtrlocoid],[qty],[hpp],[snno],[notedtl1],[upduser],[updtime]) " & _
                        "VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(finaldtloid1) & "," & Integer.Parse(FinOid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("seq")) & "," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & "," & Integer.Parse(dtab.Rows(i).Item("itemoid")) & "," & Integer.Parse(dtab.Rows(i).Item("mtrloc")) & "," & ToDouble(dtab.Rows(i).Item("qty")) & "," & ToDouble(LastHpp) & ",'" & Tchar(dtab.Rows(i).Item("snno")) & "','" & Tchar(dtab.Rows(i).Item("notedtl1")) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        finaldtloid1 += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(finaldtloid1) - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinalintdtl1'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            '--- Insert Detail Spareparts ---
            If Not Session("SpartDtl") Is Nothing Then
                '-- Drop Tabel QL_trnfinalintdtl1 --
                sSql = "DELETE FROM QL_trnfinalintdtl2 WHERE finoid=" & Integer.Parse(FinOid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dtab As DataTable = Session("SpartDtl")
                If dtab.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab.Rows.Count - 1
                        '-- Ambil Hpp Terakhirnya --
                        sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab.Rows(i).Item("PartOid")) & ""
                        xCmd.CommandText = sSql : LastHpp = xCmd.ExecuteScalar

                        '-- Create Tabel QL_trnfinalintdtl2 --
                        sSql = "INSERT INTO [QL_trnfinalintdtl2] ([cmpcode],[branch_code],[trnfinaldtloid2],[finoid],[reqoid],[reqdtloid],[seq],[itemrusakoid],[itemoid],[qty],[hpp],[mtrlocoid],[notedtl2],[updtime],[upduser])  VALUES " & _
                        "('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(finaldtloid2) & "," & Integer.Parse(FinOid.Text) & "," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & "," & Integer.Parse(dtab.Rows(i).Item("seq")) & "," & Integer.Parse(dtab.Rows(i).Item("itemrusakoid")) & "," & Integer.Parse(dtab.Rows(i).Item("PartOid")) & "," & ToDouble(dtab.Rows(i).Item("qty")) & "," & ToDouble(LastHpp) & "," & Integer.Parse(dtab.Rows(i).Item("mtrlocoid")) & ",'" & Tchar(dtab.Rows(i).Item("notedtl1")) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        finaldtloid2 += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(finaldtloid2) - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinalintdtl2'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            '--- Detail Barang Rombeng ---
            If Not Session("ItemRombeng") Is Nothing Then
                '-- Drop Tabel QL_trnfinalintdtl3 --
                sSql = "DELETE FROM QL_trnfinalintdtl3 WHERE finoid=" & Integer.Parse(FinOid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dtab As DataTable = Session("ItemRombeng")
                If dtab.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab.Rows.Count - 1
                        '-- Ambil Hpp Terakhirnya --
                        sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab.Rows(i).Item("itemrombengoid")) & ""
                        xCmd.CommandText = sSql : LastHpp = xCmd.ExecuteScalar

                        sSql = "INSERT INTO [QL_trnfinalintdtl3] ([cmpcode],[branch_code],[trnfinaldtloid3],[finoid],[reqoid],[reqdtloid],[seq],[itemoid],[itemrusakoid],[qty],[hpp],[mtrlocoid],[notedtl3],[updtime],[upduser]) VALUES " & _
                        "('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(finaldtloid3) & "," & Integer.Parse(FinOid.Text) & "," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & "," & Integer.Parse(dtab.Rows(i).Item("seq")) & "," & Integer.Parse(dtab.Rows(i).Item("itemrombengoid")) & "," & Integer.Parse(dtab.Rows(i).Item("itemrusakoid")) & "," & ToDouble(dtab.Rows(i).Item("qty")) & "," & ToDouble(LastHpp) & "," & Integer.Parse(dtab.Rows(i).Item("mtrlocoid")) & ",'" & Tchar(dtab.Rows(i).Item("notedtl3")) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        finaldtloid3 += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(finaldtloid3) - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_trnfinalintdtl3'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            '--- Detail Barang Bagus ---
            If Not Session("ItemBagus") Is Nothing Then
                '-- Drop Tabel QL_trnfinalintdtl4 --
                sSql = "DELETE FROM QL_trnfinalintdtl4 WHERE finoid=" & Integer.Parse(FinOid.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dtab As DataTable = Session("ItemBagus")
                If dtab.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab.Rows.Count - 1
                        '-- Ambil Hpp Terakhirnya --
                        sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab.Rows(i).Item("itembagusoid")) & ""
                        xCmd.CommandText = sSql : LastHpp = xCmd.ExecuteScalar

                        sSql = "INSERT INTO [QL_trnfinalintdtl4] ([cmpcode],[branch_code],[finaldtloid4],[finoid],[reqoid],[reqdtloid],[seq],[itemoid],[itemrusakoid],[qty],[hpp],[mtrlocoid],[notedtl4],[updtime],[upduser]) VALUES " & _
                        "('" & CompnyCode & "','" & DdlCabang.SelectedValue & "'," & Integer.Parse(finaldtloid4) & "," & Integer.Parse(FinOid.Text) & "," & Integer.Parse(lblreqoid.Text) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & "," & Integer.Parse(dtab.Rows(i).Item("seq")) & "," & Integer.Parse(dtab.Rows(i).Item("itembagusoid")) & "," & Integer.Parse(dtab.Rows(i).Item("itemrusakoid")) & "," & ToDouble(dtab.Rows(i).Item("qty")) & "," & ToDouble(LastHpp) & "," & Integer.Parse(dtab.Rows(i).Item("mtrlocoid")) & ",'" & Tchar(dtab.Rows(i).Item("notedtl4")) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        finaldtloid4 += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(finaldtloid4) - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnfinalintdtl4'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If lblfinstatus.Text = "IN APPROVAL" Then
                If Session("oid") <> Nothing Or Session("oid") <> "" Then
                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,branch_code,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus) VALUES" & _
                            " ('" & CompnyCode & "'," & AppOid & ",'" & DdlCabang.SelectedValue & "','" & "FINT" & AppOid & "_" & AppOid & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_trnfinalintmst','" & Session("oid") & "','IN APPROVAL','0', '" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            AppOid += 1
                        Next
                        sSql = "UPDATE ql_mstoid set lastoid = " & AppOid - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Else
                    showMessage("- Mohon Maaf, Silahkan Input Data dulu...!!", 2)
                    Exit Sub
                End If
            End If
            otrans.Commit() : conn.Close()
        Catch ex As Exception
            otrans.Rollback() : conn.Close()
            lblfinstatus.Text = "In Process"
            showMessage(ex.ToString & "; " & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnfinalInternal.aspx?awal=true")
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim result As String = ""
        result = deleterecord()
        If result = "" Then
            Response.Redirect("~\Transaction\trnfinalInternal.aspx?awal=true")
        Else
            showMessage(result, 2)
        End If
    End Sub

    Protected Sub ibpoppartfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoppartfind.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                Dim sFilter As String = ddlpoppartfilter.SelectedValue & " LIKE '%" & TcharNoTrim(tbpoppartfilter.Text) & "%'"

                dv.RowFilter = sFilter
                Session("TblListMatView") = dv.ToTable
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind() : dv.RowFilter = ""
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
            End If
        Else
            showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
        End If
    End Sub

    Protected Sub ibpoppartviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpoppartviewall.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                ddlpoppartfilter.SelectedIndex = -1
                tbpoppartfilter.Text = ""
                Session("TblListMatView") = Session("TblListMat")
                GVItemList.DataSource = Session("TblListMatView")
                GVItemList.DataBind()
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show()
            Else
                showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
            End If
        Else
            showMessage("- Maaf, data yang anda maksud tidak ada..!!", 2)
        End If
    End Sub

    Protected Sub DdlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitGudangRusak() : InitLoc() : InitGudangRombeng()
    End Sub

    Protected Sub getdetailspart(ByVal sender As Object, ByVal e As EventArgs)
        lblspartstate.Text = "edit"
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        matLoc.SelectedValue = ""
    End Sub

    Protected Sub deletespart(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        If Session("delete") Is Nothing Then
            createdel()
        End If
        Dim spartoid As Int32 = 0
        Dim dtab As DataTable = Session("delete")
        Dim drow As DataRow = dtab.NewRow
        drow("spartoid") = spartoid : dtab.Rows.Add(drow)
        Session("delete") = dtab : Dim otab As DataTable = Session("datadetail")
        otab.Rows.RemoveAt(gvr.RowIndex)
        Session("datadetail") = otab
    End Sub

    Protected Sub ImbEbarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbEbarang.Click
        ItemOid.Text = "" : SerialNo.Text = ""
        lblfinitemname.Text = "" : TypeBarang.Text = ""
        Kerusakan.Text = "" : FlagBarang.Text = ""
        Qty.Text = "" : reqdtloid.Text = ""
        GvBarang.Visible = False
    End Sub

    Protected Sub GvBarang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvBarang.PageIndexChanging
        GvBarang.PageIndex = e.NewPageIndex
        BindBarang()
    End Sub

    Protected Sub GvBarang_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvBarang.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub GvBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvBarang.SelectedIndexChanged
        Dim jMsg As String = "reqqty"
        ItemOid.Text = Integer.Parse(GvBarang.SelectedDataKey.Item("itemoid"))
        SerialNo.Text = GvBarang.SelectedDataKey.Item("snno").ToString
        lblfinitemname.Text = GvBarang.SelectedDataKey.Item("itemdesc").ToString
        TypeBarang.Text = GvBarang.SelectedDataKey.Item("typegaransi").ToString
        Kerusakan.Text = GvBarang.SelectedDataKey.Item("reqdtljob").ToString
        Qty.Text = ToMaskEdit(GvBarang.SelectedDataKey.Item("reqqty"), 3)
        reqdtloid.Text = Integer.Parse(GvBarang.SelectedDataKey.Item("reqdtloid"))
        FlagBarang.Text = GvBarang.SelectedDataKey.Item("FlagBarang").ToString
        GvBarang.Visible = False
    End Sub

    Protected Sub BtnAddToListBarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddToListBarang.Click
        Dim sErr As String = "" : Dim dtab As DataTable
        Try
            If tbservicesno.Text = "" Then
                sErr &= "- Maaf, Tolong Pilih nomer No. Tanda Terima dulu..!!<br>"
            End If

            If ItemOid.Text = "" And reqdtloid.Text = "" Then
                sErr &= "- Maaf, Tolong Pilih barang rusaknya dulu..!!<br>"
            End If
 
            If sErr <> "" Then
                showMessage(sErr, 2)
                Exit Sub
            End If

            sSql = "Select Isnull(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) From QL_conmtr con Where conrefoid=" & reqdtloid.Text & " AND con.mtrlocoid IN (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup='Location' AND a.genother5='RUSAK' AND c.gencode=con.branch_code)"
            Dim CekQty As Double = GetScalar(sSql)

            If ToDouble(Qty.Text) > ToDouble(CekQty) Then
                sErr &= "- Maaf, Tolong Qty " & ToMaskEdit(Qty.Text, 3) & " tidak lebih dari sisa " & ToMaskEdit(CekQty, 3) & " ..!!<br>"
            End If

            If Session("itemdetail") Is Nothing Then
                dtab = CrtDtlRusak() : Session("itemdetail") = dtab
                SeqBarang.Text = 0 + 1
            Else
                dtab = Session("itemdetail")
                SeqBarang.Text = dtab.Rows.Count + 1
            End If

            Dim dtView As DataView = dtab.DefaultView
            If I_Text2.Text = "New Detail" Then
                dtView.RowFilter = "reqdtloid='" & reqdtloid.Text & "' AND itemoid=" & ItemOid.Text & ""
            Else
                dtView.RowFilter = "reqdtloid='" & reqdtloid.Text & "' AND seq<>" & SeqBarang.Text
            End If

            If dtView.Count > 0 Then
                If ItemOid.Text <> "" Then
                    sErr &= "Maaf, " & lblfinitemname.Text & " Sudah ditambahkan, cek pada tabel..!!!<BR>"
                End If
                dtView.RowFilter = ""
            End If
            dtView.RowFilter = ""

            If sErr <> "" Then
                showMessage(sErr, 2)
                Exit Sub
            End If

            Dim objRow As DataRow
            If I_Text2.Text = "New Detail" Then
                objRow = dtab.NewRow()
                objRow("seq") = dtab.Rows.Count + 1
            Else
                Dim selrow As DataRow() = dtab.Select("seq=" & SeqBarang.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If

            Dim drow As DataRow : Dim drowedit() As DataRow
            If I_Text2.Text = "New Detail" Then
                drow = dtab.NewRow
                drow("seq") = SeqBarang.Text 
                drow("finaldtloid1") = Integer.Parse(0) 
                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                drow("itemoid") = Integer.Parse(ItemOid.Text)
                drow("snno") = SerialNo.Text
                drow("itemdesc") = lblfinitemname.Text
                drow("qty") = ToMaskEdit(ToDouble(Qty.Text), 3)
                drow("mtrloc") = DDLGudangRusak.SelectedValue
                drow("gudang") = DDLGudangRusak.SelectedItem.Text
                drow("notedtl1") = Kerusakan.Text
                dtab.Rows.Add(drow) : dtab.AcceptChanges()
            Else
                drowedit = dtab.Select("reqdtloid=" & Integer.Parse("reqdtloid") & "", "")
                drow = drowedit(0)
                drowedit(0).BeginEdit() 
                drow("finaldtloid1") = Integer.Parse(0)
                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                drow("itemoid") = Integer.Parse(ItemOid.Text)
                drow("snno") = SerialNo.Text
                drow("itemdesc") = lblfinitemname.Text
                drow("qty") = ToMaskEdit(ToDouble(Qty.Text), 3)
                drow("mtrloc") = DDLGudangRusak.SelectedValue
                drow("gudang") = DDLGudangRusak.SelectedItem.Text
                drow("notedtl1") = Kerusakan.Text
                drowedit(0).EndEdit()
                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()
            End If
            drow.EndEdit()
            dtView.RowFilter = ""

            DDLTypeTTS.Enabled = False

            gvrequest.DataSource = dtab
            gvrequest.DataBind() : Session("itemdetail") = dtab
            ClearDetail()

            If sErr <> "" Then
                showMessage(sErr, 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub BtnParts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Bindpartdata()
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        btnHideItem.Visible = False : panelItem.Visible = False
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListMat.Click
        Try
            UpdateCheckedMat()
            If Session("TblListMat") IsNot Nothing Then
                Dim dt As DataTable = Session("TblListMat")
                If dt.Rows.Count > 0 Then
                    Dim dv As DataView = dt.DefaultView
                    dv.RowFilter = "checkvalue='True'"

                    If dv.Count > 0 Then
                        Dim sErr As String = ""

                        For C1 As Integer = 0 To dv.Count - 1
                            If ToDouble(dv(C1)("QtyOs")) <= 0.0 Then
                                sValidasi.Text &= "- Maaf, " & dv(C1)("partdescshort") & " belum di input qty..!!<br />"
                            End If
                        Next

                        Dim dtab As DataTable
                        If Session("SpartDtl") Is Nothing Then
                            dtab = CrtDtlSpart() : Session("SpartDtl") = dtab
                            SeqBarang.Text = 0 + 1
                        Else
                            dtab = Session("SpartDtl")
                            SeqBarang.Text = dtab.Rows.Count + 1
                        End If

                        Dim dtView As DataView = dtab.DefaultView
                        If lblspartstate.Text = "new" Then
                            dtView.RowFilter = "reqdtloid=" & Integer.Parse(reqdtloid.Text) & ""
                        Else
                            dtView.RowFilter = "reqdtloid= '" & Integer.Parse(reqdtloid.Text) & "' AND seq<>" & SeqBarang.Text
                        End If

                        If dtView.Count > 0 Then
                            If ItemOid.Text <> "" Then
                                sValidasi.Text &= "Maaf, Sudah ditambahkan, cek pada tabel..!!!<BR>"
                            End If
                            dtView.RowFilter = ""
                        End If
                        dtView.RowFilter = ""

                        sSql = "Select Isnull(SUM(qtyIn),0.00)-Isnull(SUM(qtyOut),0.00) From QL_conmtr con Where conrefoid=" & reqdtloid.Text & " AND con.mtrlocoid IN (SELECT a.genoid From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup='Location' AND a.genother5='RUSAK' AND c.gencode=con.branch_code)"
                        Dim CekQty As Double = GetScalar(sSql)

                        If ToDouble(Qty.Text) > ToDouble(CekQty) Then
                            sValidasi.Text &= "- Maaf, Tolong Qty " & ToMaskEdit(Qty.Text, 3) & " tidak lebih dari sisa " & ToMaskEdit(CekQty, 3) & " ..!!<br>"
                        End If

                        If sValidasi.Text <> "" Then
                            showMessage(sValidasi.Text, 2)
                            Exit Sub
                        End If

                        Dim objRow As DataRow
                        If lblspartstate.Text = "new" Then
                            objRow = dtab.NewRow()
                            objRow("seq") = dtab.Rows.Count + 1
                        Else
                            Dim selrow As DataRow() = dtab.Select("seq=" & SeqBarang.Text)
                            objRow = selrow(0)
                            objRow.BeginEdit()
                        End If

                        For C1 As Integer = 0 To dv.Count - 1
                            dtView.RowFilter = "PartOid=" & Integer.Parse(dv(C1)("PartOid")) & ""
                            Dim drow As DataRow : Dim drowedit() As DataRow
                            If lblspartstate.Text = "new" Then
                                drow = dtab.NewRow
                                drow("seq") = SeqBarang.Text
                                drow("finoid") = Integer.Parse(FinOid.Text)
                                drow("finaldtloid2") = Integer.Parse(0)
                                drow("reqoid") = Integer.Parse(lblreqoid.Text)
                                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                drow("PartOid") = Integer.Parse(dv(C1)("PartOid"))
                                drow("itemrusakoid") = Integer.Parse(ItemOid.Text)
                                drow("itemcode") = dv(C1)("partcode").ToString
                                drow("itemdesc") = dv(C1)("partdescshort").ToString
                                drow("qty") = ToMaskEdit(dv(C1)("QtyOs"), 3)
                                drow("hpp") = ToMaskEdit(dv(C1)("spartprice"), 3)
                                drow("mtrlocoid") = matLoc.SelectedValue
                                drow("gudang") = matLoc.SelectedItem.Text
                                drow("notedtl1") = ""
                                dtab.Rows.Add(drow) : dtab.AcceptChanges()
                            Else
                                drowedit = dtab.Select("PartOid=" & Integer.Parse(dv(C1)("PartOid")) & "", "")
                                drow = drowedit(0)
                                drowedit(0).BeginEdit()
                                drow("seq") = SeqBarang.Text
                                drow("finoid") = Integer.Parse(FinOid.Text)
                                drow("finaldtloid2") = Integer.Parse(0)
                                drow("reqoid") = Integer.Parse(lblreqoid.Text)
                                drow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                                drow("itemoid") = Integer.Parse(dv(C1)("PartOid"))
                                drow("itemrusakoid") = Integer.Parse(ItemOid.Text)
                                drow("itemcode") = dv(C1)("partcode").ToString
                                drow("itemdesc") = dv(C1)("partdescshort").ToString
                                drow("qty") = ToMaskEdit(dv(C1)("QtyOs"), 3)
                                drow("hpp") = ToMaskEdit(dv(C1)("spartprice"), 3)
                                drow("mtrlocoid") = matLoc.SelectedValue
                                drow("gudang") = matLoc.SelectedItem.Text
                                drow("notedtl1") = ""

                                drowedit(0).EndEdit()
                                dtab.Select(Nothing, Nothing)
                                dtab.AcceptChanges()
                            End If
                            dtView.RowFilter = "" : SeqBarang.Text += 1
                        Next 
                        dv.RowFilter = ""

                        GvParts.DataSource = dtab
                        GvParts.DataBind() : Session("SpartDtl") = dtab

                        btnHideItem.Visible = False : panelItem.Visible = False
                        mpeItem.Show() : ClearDtlSrv()
                    End If
                End If
            Else
                sValidasi.Text &= "- Maaf, Silahkan klik add to list dulu..!!<br>"
            End If

            If sValidasi.Text <> "" Then
                showMessage(sValidasi.Text, 2)
                Exit Sub
            End If
            btnHideItem.Visible = False : panelItem.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        GVItemList.PageIndex = e.NewPageIndex
        'If UpdateCheckedMat() Then
        UpdateCheckedMat()

        Dim dtTbl As DataTable = Session("TblListMat")
        Dim dtView As DataView = dtTbl.DefaultView
        Dim sFilter As String = ddlpoppartfilter.SelectedValue & " LIKE '%" & TcharNoTrim(tbpoppartfilter.Text) & "%'"
        dtView.RowFilter = sFilter
        If dtView.Count > 0 Then
            Session("TblListMatView") = dtView.ToTable
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : dtView.RowFilter = ""
            mpeItem.Show()
        Else
            dtView.RowFilter = ""
            showMessage("Maaf, Data Barang tidak ditemukan..!!", 2)
        End If
        'Else
        'mpeItem.Show()
        'End If
    End Sub

    Protected Sub gvrequest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvrequest.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub BtnSbarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSbarang.Click
        BindBarang() : GvBarang.Visible = True
    End Sub

    Protected Sub GVItemList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemList.RowDataBound

    End Sub

    Protected Sub BtnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendApp.Click
        lblfinstatus.Text = "IN APPROVAL" : ibsave_Click(sender, e)
    End Sub

    Protected Sub GvParts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvParts.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub GvRombeng_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Try
            Dim iIndex As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("ItemRombeng")
            objTable.Rows.RemoveAt(iIndex)
            'resequence
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("seq") = C1 + 1
                dr.EndEdit()
            Next

            Session("ItemRombeng") = objTable
            GvRombeng.DataSource = objTable
            GvRombeng.DataBind()
            Session("seq") = objTable.Rows.Count + 1
        Catch ex As Exception
            showMessage(ex.ToString, 2) : Exit Sub
        End Try
    End Sub 
 
    Protected Sub GvRombeng_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvRombeng.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub 

    Protected Sub GvBagus_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvBagus.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub DeleteRombeng_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sq As String = sender.ToolTip
        Dim dtab As DataTable
        If Not Session("ItemRombeng") Is Nothing Then
            dtab = Session("ItemRombeng")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(sq) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GvRombeng.DataSource = dtab
        GvRombeng.DataBind()
        Session("ItemRombeng") = dtab
    End Sub

    Protected Sub DelBagus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sq As String = sender.ToolTip
        Dim dtab As DataTable
        If Not Session("ItemBagus") Is Nothing Then
            dtab = Session("ItemBagus")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(sq) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GvBagus.DataSource = dtab
        GvBagus.DataBind()
        Session("ItemBagus") = dtab
    End Sub 

    Protected Sub DelPart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Sq As String = sender.tooltip
        Dim dtab As DataTable
        If Not Session("SpartDtl") Is Nothing Then
            dtab = Session("SpartDtl")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(Sq) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GvParts.DataSource = dtab
        GvParts.DataBind()
        Session("SpartDtl") = dtab
    End Sub

    Protected Sub DelRusak_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sq As String = sender.ToolTip
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Error Program..!!", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(sq) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvrequest.DataSource = dtab
        gvrequest.DataBind()
        Session("Else") = dtab
    End Sub

    Protected Sub BtnDtlSparepart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        reqdtloid.Text = sender.ToolTip
        ItemOid.Text = sender.CommandName
        panelItem.Visible = True : btnHideItem.Visible = True
        mpeItem.Show() : Bindpartdata()
    End Sub

    Protected Sub BtnRombeng_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        reqdtloid.Text = sender.ToolTip
        ItemOid.Text = sender.CommandName
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindItemRombeng()
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Maaf, katalog tidak ketemu..!!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindItemRombeng()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Maaf, katalog tidak ketemu..!!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If

        If UpdCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' "
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Maaf, katalog tidak ketemu..!!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindItemRombeng()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Maaf, katalog tidak ketemu..!!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If

        If UpdCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemrombengoid=" & dtTbl.Rows(C1)("itemrombengoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemrombengoid=" & dtTbl.Rows(C1)("itemrombengoid")
                    objView(0)("CheckValue") = "False"
                    objView(0)("qty") = 0
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    dtTbl.Rows(C1)("qty") = 0
                    objView.RowFilter = ""
                Next
                Session("TblMat") = objView.ToTable
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
#End Region

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        If UpdCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND qty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Maaf, Qty yang di pilih harus lebih dari 0..!!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If
                
                If Session("ItemRombeng") Is Nothing Then
                    CrtDtlRombeng()
                End If

                Dim objTable As DataTable
                objTable = Session("ItemRombeng")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count

                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "itemrombengoid=" & dtView(C1)("itemrombengoid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True   
                        dv(0)("reqdtloid") = Integer.Parse(reqdtloid.Text) 
                        dv(0)("itemrombengoid") = Integer.Parse(dtView(C1)("itemrombengoid"))
                        dv(0)("itemrusakoid") = Integer.Parse(ItemOid.Text)
                        dv(0)("itemcode") = dtView(C1)("itemcode")
                        dv(0)("itemdesc") = dtView(C1)("itemdesc")
                        dv(0)("qty") = ToMaskEdit(dtView(C1)("qty"), 3)
                        dv(0)("mtrlocoid") = Integer.Parse(dtView(C1)("mtrlocoid"))
                        dv(0)("gudang") = dtView(C1)("gudang").ToString
                        dv(0)("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                        dv(0)("notedtl3") = Tchar(dtView(C1)("notedtl3").ToString.ToUpper)
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("seq") = counter
                        objRow("reqdtloid") = Integer.Parse(reqdtloid.Text) 
                        objRow("itemrombengoid") = dtView(C1)("itemrombengoid")
                        objRow("itemrusakoid") = Integer.Parse(ItemOid.Text)
                        objRow("itemcode") = dtView(C1)("itemcode").ToString.ToUpper
                        objRow("itemdesc") = dtView(C1)("itemdesc").ToString.ToUpper 
                        objRow("qty") = dtView(C1)("qty")
                        objRow("mtrlocoid") = Integer.Parse(dtView(C1)("mtrlocoid"))
                        objRow("gudang") = dtView(C1)("gudang").ToString
                        objRow("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                        objRow("notedtl3") = Tchar(dtView(C1)("notedtl3").ToString.ToUpper)
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("ItemRombeng") = objTable
                GvRombeng.DataSource = objTable
                GvRombeng.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                ClearDetail() 
            Else
                Session("WarningListMat") = "Please select material to add to list!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                sSql = "SELECT a.genoid,a.gendesc AS gendesc From QL_mstgen a INNER JOIN QL_mstgen c ON c.genoid=a.genother2 AND c.gengroup='CABANG' WHERE a.gengroup = 'Location' AND c.gencode='" & DdlCabang.SelectedValue & "' AND a.genother6 IN ('ROMBENG','RUSAK')"
                Dim ods As New SqlDataAdapter(sSql, conn)
                Dim objTablee As New DataTable
                ods.Fill(objTablee)

                Dim cc3 As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
                Dim tempString As String = ""
                For Each myControl As System.Web.UI.Control In cc3
                    If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                        For x As Integer = 0 To objTablee.Rows.Count - 1
                            Dim textTemp As String = objTablee.Rows(x).Item("gendesc")
                            Dim oidTemp As String = objTablee.Rows(x).Item("genoid")
                            CType(myControl, System.Web.UI.WebControls.DropDownList).Items.Add(New ListItem(textTemp, oidTemp))
                            If Not Session("TblMatView") Is Nothing Then
                                Dim objTable As DataTable = Session("TblMatView")
                                Dim iIndex As Integer = e.Row.RowIndex
                                iIndex = iIndex + (gvListMat.PageIndex * gvListMat.PageSize)
                                CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue = objTable.Rows(iIndex)("mtrlocoid")
                            End If
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
      
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindItemBagus()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindItemBagus()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            Dim iCheck As Integer = dtView.Count
            If iCheck > 0 Then
                dtView.RowFilter = ""
                dtView.RowFilter = "CheckValue='True' AND qty>0"
                If dtView.Count <> iCheck Then
                    Session("WarningListMat") = "Maaf, Qty yang di pilih harus lebih dari 0..!!"
                    showMessage(Session("WarningListMat"), 2)
                    dtView.RowFilter = ""
                    Exit Sub
                End If

                If Session("ItemBagus") Is Nothing Then
                    CrtDtlBagus()
                End If

                Dim objTable As DataTable
                objTable = Session("ItemBagus")
                Dim dv As DataView = objTable.DefaultView
                Dim objRow As DataRow
                Dim counter As Integer = objTable.Rows.Count

                For C1 As Integer = 0 To dtView.Count - 1
                    dv.RowFilter = "itembagusoid=" & dtView(C1)("itembagusoid")
                    If dv.Count > 0 Then
                        dv.AllowEdit = True
                        dv(0)("reqdtloid") = Integer.Parse(reqdtloid.Text)
                        dv(0)("itembagusoid") = Integer.Parse(dtView(C1)("itembagusoid"))
                        dv(0)("itemrusakoid") = Integer.Parse(ItemOid.Text)
                        dv(0)("itemcode") = dtView(C1)("itemcode")
                        dv(0)("itemdesc") = dtView(C1)("itemdesc")
                        dv(0)("qty") = ToMaskEdit(dtView(C1)("qty"), 3)
                        dv(0)("mtrlocoid") = Integer.Parse(dtView(C1)("mtrlocoid"))
                        dv(0)("gudang") = dtView(C1)("gudang").ToString
                        dv(0)("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                        dv(0)("notedtl4") = Tchar(dtView(C1)("notedtl4").ToString.ToUpper)
                    Else
                        counter += 1
                        objRow = objTable.NewRow()
                        objRow("seq") = counter
                        objRow("reqdtloid") = Integer.Parse(reqdtloid.Text)
                        objRow("itembagusoid") = dtView(C1)("itembagusoid")
                        objRow("itemrusakoid") = Integer.Parse(ItemOid.Text)
                        objRow("itemcode") = dtView(C1)("itemcode").ToString.ToUpper
                        objRow("itemdesc") = dtView(C1)("itemdesc").ToString.ToUpper
                        objRow("qty") = dtView(C1)("qty")
                        objRow("mtrlocoid") = Integer.Parse(dtView(C1)("mtrlocoid"))
                        objRow("gudang") = dtView(C1)("gudang").ToString
                        objRow("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                        objRow("notedtl4") = Tchar(dtView(C1)("notedtl4").ToString.ToUpper)
                        objTable.Rows.Add(objRow)
                    End If
                    dv.RowFilter = ""
                Next
                dtView.RowFilter = ""
                Session("ItemBagus") = objTable
                GvBagus.DataSource = objTable
                GvBagus.DataBind()
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                ClearDetail()
            Else
                Session("WarningListSO") = "Maaf, Pilih katalog dulu...!!"
                showMessage(Session("WarningListSO"), 2)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itembagusoid=" & dtTbl.Rows(C1)("itembagusoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itembagusoid=" & dtTbl.Rows(C1)("itembagusoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub 

    Protected Sub BtnDtlBagus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        reqdtloid.Text = sender.ToolTip
        ItemOid.Text = sender.CommandName
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindItemBagus()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SO data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub 
End Class