Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnDeliveryOrder
    Inherits System.Web.UI.Page

#Region "Variables"
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public toleranceSDO As Double = ConfigurationSettings.AppSettings("tolerance")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    'Dim cFunction As New ClassFunction
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim reportSJ As New ReportDocument
    Dim cKoneksi As New Koneksi
    Dim cfunction As New ClassFunction
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String
    Dim sudahcek As Integer
    Dim ceksave As Integer = 0
    Dim ceklist As Integer = 0
    Dim expiredSO As DataTable

#End Region

#Region "Functions"
    Private Function GetInterfaceValue(ByVal sInterfaceVar As String, ByVal sBranch As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "' AND interfaceres1='" & sBranch & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' "
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
       ByVal sRequestUser As String, ByVal sStatus As String, ByVal sbranch As String) As DataTable
        sSql = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & _
             "' and RequestUser LIKE '" & sRequestUser & "%' and STATUSREQUEST = '" & sStatus & "' " & _
             " AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' and branch_code='" & sbranch & "' ORDER BY REQUESTDATE DESC"
        Return cKoneksi.ambiltabel(sSql, "QL_APPROVAL")
    End Function

    Private Function CekStock(ByVal oiditem As String)
        sSql = "Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) stoknya From QL_conmtr con Where con.refoid=" & oiditem & " AND con.branch_code='" & CabangDDL.SelectedValue & "' AND con.periodacctg='" & GetServerTime.ToString("yyyy-MM") & "' AND con.mtrlocoid=" & itemLoc.SelectedValue
        Dim dtstok As DataTable = cKoneksi.ambiltabel(sSql, "QL_conmtr")
        Return dtstok.Rows(0).Item("stoknya")
    End Function

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("trnsjjualdtlseq", Type.GetType("System.Int32"))
        dt.Columns.Add("trnorderdtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemcode", Type.GetType("System.String"))
        dt.Columns.Add("itembarcode1", Type.GetType("System.String"))
        dt.Columns.Add("itemlongdesc", Type.GetType("System.String"))
        dt.Columns.Add("QtySJ", Type.GetType("System.Decimal"))
        dt.Columns.Add("itemloc", Type.GetType("System.Int32"))
        dt.Columns.Add("location", Type.GetType("System.String"))
        dt.Columns.Add("jenisprice", Type.GetType("System.String"))
        dt.Columns.Add("sjjualdtlnote", Type.GetType("System.String"))
        dt.Columns.Add("unit", Type.GetType("System.String"))
        dt.Columns.Add("SisaQty", Type.GetType("System.Decimal"))
        Return dt
    End Function

    Private Function checkOtherShipment(ByVal iRefoid As Integer, ByVal iItemOid As Integer) As Boolean
        sSql = "SELECT COUNT(-1) FROM ql_trnsjjualdtl jd INNER JOIN ql_trnsjjualmst j ON jd.trnsjjualmstoid =j.trnsjjualmstoid  AND j.cmpcode=jd.cmpcode AND j.trnsjjualstatus  not in ('Approved','INVOICED','HISTORY','Rejected') inner join QL_trnordermst o on o.orderno = j.orderno  WHERE jd.refoid =" & iItemOid & " and refname = 'QL_mstitem' AND jd.cmpcode='" & CompnyCode & "' and o.branch_code=j.branch_code AND o.ordermstoid=" & iRefoid & " AND o.branch_code='" & CabangDDL.SelectedValue & "'"
        If I_U.Text <> "New" Then
            sSql &= " AND j.trnsjjualmstoid<>" & Session("oid")
        End If
        Return cKoneksi.ambilscalar(sSql) > 0
    End Function

    Private Function cektypebundling(ByVal NoSo As String) As Boolean
        sSql = "SELECT ISNULL(bundling, 0) FROM QL_trnordermst WHERE orderno = '" & NoSo & "'"
        If cKoneksi.ambilscalar(sSql) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function cektypePromo(ByVal NoSo As String, ByVal itemOidSo As Integer) As Boolean
        sSql = "SELECT ISNULL(d.promooid, 0) FROM QL_trnordermst m INNER JOIN QL_trnorderdtl d ON m.ordermstoid = d.trnordermstoid WHERE m.orderno = '" & NoSo & "' AND d.itemoid = '" & itemOidSo & "'"
        If cKoneksi.ambilscalar(sSql) = 0 Then
            Return True
        Else
            Return False
        End If

    End Function
#End Region

#Region "Procedures"
    Private Sub setJurnalSI()
        sSql = "SELECT trnordermstoid oid, seq trnsjjualdtlseq, d.trnsjjualdtloid, (od.amtdtldisc1/od.trnorderdtlqty) + (od.amtdtldisc2/od.trnorderdtlqty) discperqty, od.trnorderprice, d.trnorderdtloid, d.qty, d.note, p.trnsjjualmstoid, d.refoid itemoid, od.trndiskonpromo, d.unitoid as itemunitoid, ISNULL((Select sum(qty) From ql_trnsjjualdtl Where trnsjjualmstoid in (select trnsjjualmstoid from ql_trnsjjualmst Where trnsjjualstatus IN ('Approved','INVOICED') AND trnorderdtloid = od.trnorderdtloid and od.itemoid=d.refoid AND d.Branch_code=od.branch_code)),0) as deliveredqty, od.trnorderdtlqty, od.trnordermstoid, od.trnorderprice*qty AmtJual, d.mtrlocoid, ISNULL((od.amtdtldisc1/od.trnorderdtlqty) * d.qty,0.00) disc1, ISNULL((od.amtdtldisc2/od.trnorderdtlqty) * d.qty,0.00) disc2, ISNULL(trndiskonpromo/qty,0.00) discpromoperqty, ((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-(ISNULL((od.amtdtldisc1/od.trnorderdtlqty) * d.qty,0.00)+ISNULL((od.amtdtldisc2/od.trnorderdtlqty) * d.qty,0.00)) NettoAmt, d.refoid FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid INNER JOIN ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid AND d.refoid=od.itemoid AND od.branch_code=p.branch_code INNER JOIN QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'LOCATION' INNER JOIN ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' INNER JOIN ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid WHERE d.trnsjjualmstoid=p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid AND d.trnsjjualmstoid = " & trnsjjualmstoid.Text & " AND od.trnordermstoid = " & ordermstoid.Text & " AND p.branch_code='" & CabangDDL.SelectedValue & "' ORDER BY trnsjjualdtlseq"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjurnal")
        Session("TblDtl1") = objTable
        amtjualnetto.Text = objTable.Compute("SUM(NettoAmt)", "")
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        dtlTable.Columns.Add("code", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seqdtl", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("dbcr", System.Type.GetType("System.String"))
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable = Session("JurnalPreview")
        Dim nuRow As DataRow : Dim xBRanch As String = ""
        If Not IsNothing(Session("TblDtl1")) Then
            Dim dtDtl As DataTable = Session("TblDtl1")
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & itemLoc.SelectedValue & " AND genother4='T'")
            Next
        End If

        Dim sVarAR As String = GetInterfaceValue("VAR_AR", CabangDDL.SelectedValue)
        If sVarAR = "?" Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAR, False)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (PIUTANG USAHA)"
            nuRow("debet") = ToMaskEdit(amtjualnetto.Text, 3)
            nuRow("credit") = 0 : nuRow("id") = iAPOid
            nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
            nuRow("dbcr") = "D"
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        ' ============ PENJUALAN ============
        If Not IsNothing(Session("TblDtl1")) Then
            Dim dtDtl As DataTable = Session("TblDtl1")
            ' ====== GET itemacctgoid and HPP
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                Dim iItemAccount As String = GetInterfaceValue("VAR_SALES", CabangDDL.SelectedValue)
                If iItemAccount = "?" Then
                    showMessage("Maaf, Interface untuk 'VAR_SALES' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If
                Dim iAPGudOid As Integer = GetAccountOid(iItemAccount, False)

                If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("NettoAmt").ToString), 3)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()

                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iAPGudOid)
                    nuRow("desc") = GetDescAcctg(iAPGudOid) & " (PENJUALAN)"
                    nuRow("debet") = 0
                    nuRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("NettoAmt").ToString), 3)
                    nuRow("id") = iAPGudOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "C"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        iSeq += 1
        ' ============ 1 JURNAL
        ' BEBAN POKOK PENJUALAN
        ' PERSEDIAAN

        If Not IsNothing(Session("TblDtl1")) Then
            Dim dtDtl As DataTable = Session("TblDtl1")
            ' ====== GET HPPValue and HPPAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarHPP As String = ""

                sVarHPP = GetStrData("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_HPP' AND interfaceres1='" & CabangDDL.SelectedValue & "'")
                If sVarHPP = "?" Then
                    showMessage("Maaf, Interface untuk 'VAR_HPP' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarHPP, True)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("debet") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * dItemHPP, 3)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (BEBAN POKOK PENJUALAN(HPP))"
                    nuRow("debet") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * (dItemHPP / 1), 3)
                    nuRow("credit") = 0 : nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "D"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        If Not IsNothing(Session("TblDtl1")) Then
            Dim dtDtl As DataTable = Session("TblDtl1")
            ' ====== GET PersediaanValue and PersediaanAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarStock As String = ""

                sVarStock = GetInterfaceValue("VAR_GUDANG", CabangDDL.SelectedValue)
                If sVarStock = "?" Then
                    showMessage("Please Setup Interface for Stock on Variable 'VAR_GUDANG'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarStock, False)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * dItemHPP, 3)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (PERSEDIAAN)"
                    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * (dItemHPP / 1), 3)
                    nuRow("id") = iItemAccount : nuRow("debet") = 0
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "C"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If
        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal
        'cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
    End Sub

    Private Sub SubFunction()
        I_u2.Text = "New Detail"
        If posting.Text = "Approved" Or posting.Text = "POST" Then
            btnSave.Visible = False : btnDelete.Visible = False
            imbSendApproval.Visible = False : btnPost.Visible = False
            orderflag.Enabled = True : orderflag.CssClass = "inpTextDisabled"
            itemLoc.Enabled = False : itemLoc.CssClass = "inpTextDisabled"
        ElseIf posting.Text = "Closed" Or posting.Text = "Canceled" Or posting.Text = "INVOICED" Then
            btnSave.Visible = False : btnDelete.Visible = False
            imbSendApproval.Visible = False : btnPost.Visible = False
            orderflag.Enabled = True : orderflag.CssClass = "inpTextDisabled"
            itemLoc.Enabled = False : itemLoc.CssClass = "inpTextDisabled"
        ElseIf posting.Text = "In Approval" Then
            btnSave.Visible = False : btnDelete.Visible = False
            imbSendApproval.Visible = False : orderflag.Enabled = True
            orderflag.CssClass = "inpTextDisabled" : btnPost.Visible = False
        ElseIf posting.Text = "In Process" Then
            orderflag.Enabled = True : orderflag.CssClass = "inpText"
            If typeSO.Text = "Kanvas" Then
                btnPost.Visible = True : imbSendApproval.Visible = False
            Else
                btnPost.Visible = False : imbSendApproval.Visible = True
            End If
        ElseIf posting.Text = "Revised" Then
            imbSendApproval.Visible = True
            orderflag.Enabled = True : orderflag.CssClass = "inpText"
            btnPost.Visible = False
        ElseIf posting.Text = "Rejected" Then
            btnSave.Visible = False : btnDelete.Visible = False
            imbSendApproval.Visible = False
            orderflag.Enabled = True : orderflag.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub ItemBonus()
        Try
            sSql = "Select od.trnorderdtloid, i.itembarcode1, i.itemcode, i.itemoid, i.itemdesc, trnorderdtlqty, od.trnorderdtlqty-(Select Isnull(SUM(qty), 0.00) From ql_trnsjjualdtl d inner join ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid and d.Branch_code = od.branch_code AND d.refoid = od.itemoid and m.ordermstoid = " & ordermstoid.Text & " Where d.trnorderdtloid=od.trnorderdtloid) QtySJ, ISNULL((select SUM(qty) From ql_trnsjjualdtl where trnsjjualmstoid IN (Select trnsjjualmstoid From ql_trnsjjualmst Where trnorderdtloid = od.trnorderdtloid and Branch_code = od.branch_code AND refoid = od.itemoid AND branch_code = od.branch_code and ordermstoid = " & ordermstoid.Text & ")),0) as deliveryqty, od.jenisprice From QL_trnorderdtl od Inner Join QL_mstitem i ON i.itemoid=od.itemoid Where trnordermstoid IN (select om.ordermstoid from QL_trnordermst om Where om.ordermstoid=" & ordermstoid.Text & " AND om.branch_code='" & CabangDDL.SelectedValue & "' AND promooid=" & Integer.Parse(PromoOid.Text) & ") AND od.jenisprice='BONUS' AND od.branch_code='" & CabangDDL.SelectedValue & "'"
            Dim data As DataTable = cKoneksi.ambiltabel(sSql, "ItemBonus")
            Session("ItemBonus") = data
        Catch ex As Exception
            showMessage(ex.ToString & "<br />", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub SelectChanged(ByVal Flag As String)
        Try
            Dim sMsg As String = "", sQtySisa As Double = 0
            Dim dt As DataTable = Session("TblMat")
            If dt.Rows.Count > 0 Then
                ItemCode.Text = dt.Rows(0).Item("itemcode")
                itemname.Text = dt.Rows(0).Item("itemlongdesc")
                ItemBarcode.Text = dt.Rows(0).Item("itembarcode1")
                itemoid.Text = dt.Rows(0).Item("itemoid")
                SisaQty.Text = ToDouble(dt.Rows(0).Item("SisaQty"))
                orderdtloid.Text = dt.Rows(0).Item("trnorderdtloid").ToString
                delivorder.Text = ToDouble(dt.Rows(0).Item("QtySJ"))
                StokQty.Text = ToDouble(dt.Rows(0).Item("StokAkhir"))
                trnsjjualdtlqty.Text = dt.Rows(0).Item("QtySJ")
                jenisprice.Text = dt.Rows(0).Item("jenisprice").ToString

                sSql = "Select COUNT(*) from QL_trnorderdtl Where branch_code='" & CabangDDL.SelectedValue & "' AND jenisprice='PROMO' AND trnordermstoid IN (Select ordermstoid From QL_trnordermst Where orderno='" & TcharNoTrim(orderno.Text) & "' AND branch_code='" & CabangDDL.SelectedValue & "') AND itemoid=" & itemoid.Text & ""
                Dim cPorm As Integer = GetScalar(sSql)
                'If cPorm > 0 Then
                '    If ToDouble(trnsjjualdtlqty.Text) < ToDouble(SisaQty.Text) Then
                '        sMsg &= "Maaf, Item " & itemname.Text & " tidak bisa di parsial karena type promo..!!<br>"
                '    End If
                'End If

                'If sMsg <> "" Then
                '    showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
                'End If

                If Session("TblDtl") Is Nothing Then
                    Dim dtlTable As DataTable = setTabelDetail()
                    Session("TblDtl") = dtlTable
                End If

                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                If I_u2.Text = "New Detail" Then
                    dv.RowFilter = "itemoid=" & itemoid.Text & " And itemloc = " & itemLoc.SelectedValue
                Else
                    dv.RowFilter = "itemoid=" & itemoid.Text & " And itemloc = " & itemLoc.SelectedValue & " AND trnsjjualdtlseq<>" & trnsjjualdtlseq.Text
                End If

                Dim objRow As DataRow
                Dim Counter As Integer = objTable.Rows.Count + 1
                Dim CountQty As Integer = 0
                If ToDouble(dv.Count) > 0 Then
                    CountQty = ToDouble(dv(0).Item("QtySJ")) + 1
                Else
                    CountQty = CountQty + 1
                End If

                If ToDouble(CountQty) > ToDouble(dt.Rows(0)("QtySO")) Then
                    showMessage("- Maaf, Katalog " & dt.Rows(0).Item("itemlongdesc") & " SO Qty " & ToMaskEdit(dt.Rows(0).Item("QtySO"), 3) & " <br />", CompnyName & " - WARNING", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If

                If dv.Count > 0 Then
                    dv.AllowEdit = True
                    dv(0)("trnorderdtloid") = ToDouble(orderdtloid.Text)
                    dv(0)("itemoid") = itemoid.Text
                    dv(0)("itemcode") = ItemCode.Text
                    dv(0)("itembarcode1") = ItemBarcode.Text
                    dv(0)("itemlongdesc") = itemname.Text
                    If cPorm > 0 Then
                        dv(0)("QtySJ") = ToDouble(trnsjjualdtlqty.Text)
                    Else
                        dv(0)("QtySJ") = ToDouble(CountQty)
                    End If
                    dv(0)("unit") = "BUAH"
                    dv(0)("itemloc") = itemLoc.SelectedValue
                    dv(0)("location") = itemLoc.SelectedItem
                    dv(0)("jenisprice") = jenisprice.Text
                    dv(0)("sjjualdtlnote") = trnsjjualdtlnote.Text
                    dv(0)("SisaQty") = SisaQty.Text
                Else
                    objRow = objTable.NewRow()
                    objRow("trnsjjualdtlseq") = objTable.Rows.Count + 1
                    objRow("trnorderdtloid") = Integer.Parse(orderdtloid.Text)
                    objRow("itemoid") = itemoid.Text
                    objRow("itemcode") = ItemCode.Text
                    objRow("itembarcode1") = ItemBarcode.Text
                    objRow("itemlongdesc") = itemname.Text
                    If cPorm > 0 Then
                        objRow("QtySJ") = ToDouble(trnsjjualdtlqty.Text)
                    Else
                        objRow("QtySJ") = ToDouble(CountQty)
                    End If
                    objRow("unit") = "BUAH"
                    objRow("itemloc") = itemLoc.SelectedValue
                    objRow("location") = itemLoc.SelectedItem
                    objRow("jenisprice") = jenisprice.Text
                    objRow("sjjualdtlnote") = trnsjjualdtlnote.Text
                    objRow("SisaQty") = SisaQty.Text
                    objTable.Rows.Add(objRow)
                End If
                dv.RowFilter = ""

                If Integer.Parse(PromoOid.Text) <> 0 Then
                    sSql = "Select COUNT(itemoid) from QL_mstpromodtl Where promoid=" & Integer.Parse(PromoOid.Text) & " AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA'"
                    Dim cekPro As Integer = GetScalar(sSql)
                    If cekPro > 0 Then
                        ItemBonus() : Dim data As DataTable = Session("ItemBonus")
                        If data.Rows.Count > 0 Then
                            For a1 As Integer = 0 To data.Rows.Count - 1
                                If I_u2.Text = "New Detail" Then
                                    objRow = objTable.NewRow()
                                    objRow("trnsjjualdtlseq") = objTable.Rows.Count + 1
                                Else
                                    Dim selrow As DataRow() = objTable.Select("trnsjjualdtlseq=" & trnsjjualdtlseq.Text)
                                    objRow = selrow(0)
                                    objRow.BeginEdit()
                                End If
                                objRow("trnorderdtloid") = Integer.Parse(data.Rows(a1).Item("trnorderdtloid"))
                                objRow("itemoid") = data.Rows(a1).Item("itemoid")
                                objRow("itemcode") = data.Rows(a1).Item("itemcode").ToString
                                objRow("itembarcode1") = data.Rows(a1).Item("itembarcode1").ToString
                                objRow("itemlongdesc") = data.Rows(a1).Item("itemdesc").ToString
                                objRow("QtySJ") = ToMaskEdit(data.Rows(a1).Item("trnorderdtlqty"), 3)
                                objRow("unit") = "BUAH"
                                objRow("itemloc") = itemLoc.SelectedValue
                                objRow("location") = itemLoc.SelectedItem
                                objRow("SisaQty") = ToMaskEdit(data.Rows(a1).Item("QtySJ"), 3)
                                objRow("jenisprice") = data.Rows(a1).Item("jenisprice").ToString
                                objRow("sjjualdtlnote") = trnsjjualdtlnote.Text
                                If I_u2.Text = "New Detail" Then
                                    objTable.Rows.Add(objRow)
                                Else
                                    objRow.EndEdit()
                                End If
                            Next
                        End If

                    End If
                End If
                ClearDetail() : Session("TblDtl") = objTable
                tbldtl.Visible = True : tbldtl.DataSource = Nothing
                tbldtl.DataSource = objTable : tbldtl.DataBind()
                trnsjjualdtlseq.Text = objTable.Rows.Count + 1
                tbldtl.SelectedIndex = -1
            Else
                sMsg &= "Maaf, Katalog " & itemname.Text & " tidak ada pada SO " & orderno.Text & "..!!<br>"
                showMessage(sMsg, CompnyName & " - INFORMATION", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub clearRef()
        orderno.Text = "" : ordermstoid.Text = ""
        trncustoid.Text = "" : custname.Text = ""
        trnsjjualdate.Text = "" 'identifierno.Text = ""
        Session("TblDtl") = Nothing
        tbldtl.DataSource = Nothing
        tbldtl.DataBind()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub InitAllDDL()
        sSql = "Select a.genoid, a.gendesc gendesc From QL_mstgen a WHERE a.gengroup = 'Location' AND a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & CabangDDL.SelectedValue & "' And gengroup = 'cabang')"
        If typeSO.Text = "ecommers" Then
            sSql &= " AND a.genother6 = 'ECOMMERCE'"
        ElseIf typeSO.Text = "Kanvas" Then
            sSql &= " AND a.genother4 = 'KANVAS' AND a.genother6 = 'UMUM'"
        ElseIf typeSO.Text = "Konsinyasi" Then
            sSql &= " AND a.genother6 = 'UMUM'"
        Else
            sSql &= " AND a.genother6 = 'UMUM' AND a.genother4 <> 'KANVAS'"
        End If
        FillDDL(itemLoc, sSql)

        btnCancel.Visible = True
        If itemLoc.Items.Count = 0 Then
            showMessage("Please create/fill Data General in group LOCATION!", CompnyName & " - Warning", 2)
            btnSave.Visible = False
            btnDelete.Visible = False
            btnCancel.Visible = False
        End If

        FillDDL(Ekspedisi, "select genoid, gendesc, genother1 from QL_mstgen where cmpcode='" & CompnyCode & "' and gengroup='EKSPEDISI' order by gendesc")
        Ekspedisi.Items.Add("NONE")
        Ekspedisi.SelectedValue = 3255
    End Sub

    Private Sub InitCbngNya()
        sSql = "select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangDDL, sSql)
            Else
                FillDDL(CabangDDL, sSql)
                'ddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(CabangDDL, sSql)
            'ddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub ddlCabang()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fDdlcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fDdlcabang, sSql)
            Else
                FillDDL(fDdlcabang, sSql)
                fDdlcabang.Items.Add(New ListItem("ALL", "ALL"))
                fDdlcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(fDdlcabang, sSql)
            fDdlcabang.Items.Add(New ListItem("ALL", "ALL"))
            fDdlcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub FillTextBox(ByVal Branch As String, ByVal iID As Int64)
        Try
            lbltext.Text = "Last Update"
            sSql = "SELECT a.branch_code, a.trnsjjualmstoid, a.trnsjjualno ,a.trnsjjualdate ,o.ordermstoid,a.trnsjjualdate ,a.trnsjjualstatus, s.custoid, s.custname, a.expedisioid, a.noExpedisi, a.note, a.upduser, a.updtime, a.finalapprovalcode, a.finalapprovaluser, a.finalappovaldatetime, o.orderno, o.trnorderdate, o.delivdate, a.mtrlocoid, o.promooid, a.createtime, o.typeSO, o.promooid, o.typeSO, o.salesoid, o.spgoid, o.trnpaytype, o.timeofpaymentSO, s.custflag, o.amtjualnetto, (Select CAST(g.genother1 as integer) genother1 From QL_mstgen g Where g.genoid = s.timeofpayment and g.gengroup = 'PAYTYPE') termin, s.custcreditlimitrupiah, s.custcreditlimitusagerupiah, o.flagCash, o.ordertaxtype, o.flagTax FROM ql_trnsjjualmst a inner join QL_trnordermst o on o.cmpcode=a.cmpcode AND o.ordermstoid=a.ordermstoid AND o.branch_code=a.branch_code inner join QL_mstcust s on a.cmpcode=s.cmpcode AND o.trncustoid=s.custoid Where a.cmpcode ='" & CompnyCode & "' AND a.trnsjjualmstoid ='" & iID & "' and a.branch_code = '" & Branch & "' and o.to_branch = '" & Branch & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    CabangDDL.SelectedValue = xreader.Item("branch_code").ToString
                    trnsjjualmstoid.Text = Trim(xreader.Item("trnsjjualmstoid"))
                    trnsjjualno.Text = Trim(xreader.Item("trnsjjualno"))
                    typeSO.Text = xreader.Item("typeSO")
                    trnsjjualsenddate.Text = Format(CDate(xreader.Item("trnsjjualdate").ToString), "dd/MM/yyyy")
                    salesdeliveryshipdate.Text = Format(CDate(xreader.Item("delivdate").ToString), "dd/MM/yyyy")
                    ordermstoid.Text = Trim(xreader.Item("ordermstoid"))
                    orderno.Text = Trim(xreader.Item("orderno"))
                    trncustoid.Text = Trim(xreader.Item("custoid"))
                    custname.Text = Trim(xreader.Item("custname"))
                    note.Text = Trim(xreader.Item("note"))
                    posting.Text = Trim(xreader.Item("trnsjjualstatus"))
                    trnsjjualdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                    PromoOid.Text = Integer.Parse(xreader.Item("promooid"))

                    SpgOid.Text = xreader.Item("spgoid")
                    trnpaytypeoid.Text = xreader.Item("trnpaytype")
                    custflag.Text = xreader.Item("custflag")
                    crUsage.Text = ToDouble(xreader.Item("custcreditlimitusagerupiah"))
                    crLimit.Text = ToDouble(xreader.Item("custcreditlimitrupiah"))
                    amtjualnetto.Text = ToDouble(xreader.Item("amtjualnetto"))
                    termin.Text = ToDouble(xreader.Item("termin"))
                    flagTax.Text = xreader.Item("flagTax")

                    InitAllDDL()
                    itemLoc.SelectedValue = Trim(xreader.Item("mtrlocoid").ToString)
                    nopolisi.Text = Trim(xreader("noExpedisi").ToString)
                    Ekspedisi.SelectedValue = Trim(xreader("expedisioid").ToString)
                    updUser.Text = Trim(xreader.Item("upduser"))
                    updTime.Text = Trim(xreader.Item("updtime"))
                    createtime.Text = Format(xreader.Item("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
                End While

            End If
            xreader.Close() : conn.Close()

            sSql = "SELECT row_number() OVER(ORDER BY d.trnsjjualdtloid) trnsjjualdtlseq, d.trnsjjualdtloid, d.trnsjjualmstoid, d.trnorderdtloid, od.trnorderdtlqty-d.qty SisaQty, d.qty QtySJ, d.note sjjualdtlnote, d.refoid itemoid, i.itemdesc itemlongdesc, 'BUAH' AS unit, ISNULL((SELECT SUM(qty) FROM ql_trnsjjualdtl sd WHERE sd.trnsjjualmstoid IN (SELECT trnsjjualmstoid FROM ql_trnsjjualmst sj WHERE sj.trnsjjualstatus IN ('Approved', 'INVOICED') AND sd.trnorderdtloid = od.trnorderdtloid AND sj.branch_code=od.branch_code)), 0 ) AS deliveredqty, od.trnorderdtlqty AS soqty, d.refoid, d.mtrlocoid itemloc, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = g23.genother1 ) +' - '+ g23.gendesc location, ISNULL(( SELECT SUM(qtyin)-SUM(qtyout) FROM QL_conmtr com WHERE refoid = i.itemoid AND PeriodAcctg = '" & GetServerTime.ToString("yyyy-MM") & "' AND mtrlocoid = p.mtrlocoid and com.branch_code = p.branch_code ), 0.00 ) StockQty, od.jenisprice, i.itemcode, i.itembarcode1 FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst p ON d.cmpcode = p.cmpcode AND d.trnsjjualmstoid = p.trnsjjualmstoid INNER JOIN ql_trnorderdtl od ON od.branch_code = d.branch_code AND od.trnorderdtloid = d.trnorderdtloid INNER JOIN QL_mstgen g23 ON g23.genoid = d.mtrlocoid AND g23.gengroup IN ( 'Cabang', 'warehouse', 'LOCATION' ) INNER JOIN ql_mstitem i ON i.itemoid = d.refoid AND d.refname = 'ql_mstitem' WHERE d.trnsjjualmstoid = p.trnsjjualmstoid AND d.branch_code = p.branch_code AND i.cmpcode = d.cmpcode AND i.itemoid = d.refoid AND d.trnsjjualmstoid = " & trnsjjualmstoid.Text & " AND d.cmpcode='" & CompnyCode & "' And p.Branch_code = '" & Branch & "' and od.branch_code = od.branch_code ORDER BY d.seq"
            Dim dtlTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
            Session("TblDtl") = dtlTable : tbldtl.DataSource = dtlTable
            tbldtl.DataBind() : tbldtl.Visible = True

            If PromoOid.Text <> "0" Then
                tbldtl.Columns(0).Visible = False
            Else
                tbldtl.Columns(0).Visible = True
            End If
            trnsjjualdtlseq.Text = dtlTable.Rows.Count + 1
            SubFunction()
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindData(ByVal sqlPlus As String)
        Try
            sSql = "SELECT a.branch_code, a.trnsjjualmstoid, a.trnsjjualno, a.trnsjjualdate, p.delivdate, p.trncustoid, p.ordermstoid, case when a.trnsjjualstatus='Revised' then a.note + '<br>' + 'Revisi:' + '<span style=color:red;>' + a.revisenote + '</span>' else a.note end note, a.trnsjjualstatus, a.upduser, a.updtime, s.custname, p.orderno FROM ql_trnsjjualmst a inner join QL_trnordermst p on p.ordermstoid=a.ordermstoid AND p.branch_code=a.branch_code inner join QL_mstcust s on p.trncustoid = s.custoid Where a.trnsjjualstatus <> 'HISTORY' " & sqlPlus & ""
            If fDdlcabang.SelectedValue <> "ALL" Then
                sSql &= "And a.branch_code = '" & fDdlcabang.SelectedValue & "'"
            End If
            If chkPeriod.Checked = True Then
                If chkPeriod.Checked Then
                    Dim st1, st2 As Boolean : Dim sMsg As String = ""
                    Try
                        Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text)) : st1 = True
                    Catch ex As Exception
                        sMsg &= "- Invalid Start date!!<BR>" : st1 = False
                    End Try
                    Try
                        Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text)) : st2 = True
                    Catch ex As Exception
                        sMsg &= "- Invalid End date!!<BR>" : st2 = False
                    End Try
                    If st1 And st2 Then
                        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
                            sMsg &= "- End date can't be less than Start Date!!"
                        End If
                    End If
                    If sMsg <> "" Then : showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
                    End If
                End If
                sSql &= " AND a.trnsjjualdate BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy") & " 0:0:0' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "MM/dd/yyyy") & " 23:59:59'"
            End If

            If chkStatus.Checked = True Then
                If DDLStatus.SelectedValue <> "All" Then
                    sSql &= " AND trnsjjualstatus LIKE '%" & DDLStatus.SelectedValue & "%'"
                End If
            End If
            sSql &= " AND a.cmpcode='" & CompnyCode & "' Order By a.trnsjjualdate DESC"
            sqlTempSearch = sSql

            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")
            Session("tbldata") = objDs.Tables("data")
            gvTblData.DataSource = objDs.Tables("data")
            gvTblData.DataBind()
            gvTblData.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindKatalog(ByVal sQuery As String)
        Try
            sSql = "SELECT DISTINCT i.itemoid, i.itemcode, i.itembarcode1, i.itemdesc as itemlongdesc, '' itemgroupcode, merk, sod.trnorderdtlqty QtySO, sod.trnorderdtlqty-(SELECT ISNULL(SUM(qty),0.00) FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid AND d.Branch_code = sod.branch_code AND d.refoid = sod.itemoid AND m.orderno = so.orderno AND m.ordermstoid=so.ordermstoid WHERE d.trnorderdtloid=sod.trnorderdtloid AND m.trnsjjualstatus<>'REJECTED') SisaQty, ISNULL((SELECT SUM(qty) FROM ql_trnsjjualdtl WHERE trnsjjualmstoid IN (SELECT trnsjjualmstoid FROM ql_trnsjjualmst sj WHERE trnorderdtloid = sod.trnorderdtloid AND Branch_code = sod.branch_code AND refoid = sod.itemoid AND branch_code = sod.branch_code AND sj.orderno = so.orderno AND sj.ordermstoid=so.ordermstoid AND sj.trnsjjualstatus<>'REJECTED')),0.00) as QtySJ, sod.itemoid, sod.trnorderdtloid, 'BUAH' AS unit, sod.trnordermstoid, ISNULL((SELECT SUM(qtyin)-SUM(qtyout) FROM QL_conmtr con WHERE con.refoid = i.itemoid AND con.PeriodAcctg ='" & GetServerTime.ToString("yyyy-MM") & "' AND con.mtrlocoid = " & itemLoc.SelectedValue & " AND con.branch_code = '" & CabangDDL.SelectedValue & "'),0.00) StokAkhir, sod.jenisprice FROM QL_trnorderdtl sod INNER JOIN QL_mstitem i on i.itemoid = sod.itemoid INNER JOIN QL_trnordermst so on sod.trnordermstoid = so.ordermstoid WHERE sod.trnordermstoid = '" & ordermstoid.Text & "' AND sod.to_branch = '" & CabangDDL.SelectedValue & "' AND so.to_branch = '" & CabangDDL.SelectedValue & "' AND sod.branch_code IN (SELECT branch_code FROM QL_trnorderdtl WHERE trnorderdtloid = sod.trnorderdtloid ) AND so.orderno = '" & orderno.Text & "' AND sod.trnorderdtlqty - (SELECT ISNULL(SUM(qty),0.00) FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid WHERE d.trnorderdtloid=sod.trnorderdtloid AND d.Branch_code = sod.branch_code AND d.refoid = sod.itemoid AND m.orderno = so.orderno AND trnsjjualstatus in ('Approved', 'INVOICED'))>0.00 AND sod.jenisprice NOT IN ('BONUS') AND sod.cmpcode='" & CompnyCode & "'" & sQuery
            Session("TblMat") = cKoneksi.ambiltabel(sSql, "QL_mstkatalog")
            gvReference.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataSalesOrder(ByVal sQuery As String)
        Try
            sSql = "SELECT DISTINCT i.itemoid, i.itemcode, i.itembarcode1, i.itemdesc as itemlongdesc, '' itemgroupcode, merk, sod.trnorderdtlqty QtySO, sod.trnorderdtlqty-(SELECT ISNULL(SUM(qty),0.00) FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid AND d.Branch_code = sod.branch_code AND d.refoid = sod.itemoid AND m.orderno = so.orderno AND m.ordermstoid=so.ordermstoid WHERE d.trnorderdtloid=sod.trnorderdtloid AND m.trnsjjualstatus<>'REJECTED') SisaQty, ISNULL((SELECT SUM(qty) FROM ql_trnsjjualdtl WHERE trnsjjualmstoid IN (SELECT trnsjjualmstoid FROM ql_trnsjjualmst sj WHERE trnorderdtloid = sod.trnorderdtloid AND Branch_code = sod.branch_code AND refoid = sod.itemoid AND branch_code = sod.branch_code AND sj.orderno = so.orderno AND sj.ordermstoid=so.ordermstoid AND sj.trnsjjualstatus<>'REJECTED')),0.00) as QtySJ, sod.itemoid, sod.trnorderdtloid, 'BUAH' AS unit, sod.trnordermstoid, ISNULL((SELECT SUM(qtyin)-SUM(qtyout) FROM QL_conmtr con WHERE con.refoid = i.itemoid AND con.PeriodAcctg ='" & GetServerTime.ToString("yyyy-MM") & "' AND con.mtrlocoid = " & itemLoc.SelectedValue & " AND con.branch_code = '" & CabangDDL.SelectedValue & "'),0.00) StokAkhir, sod.jenisprice FROM QL_trnorderdtl sod INNER JOIN QL_mstitem i on i.itemoid = sod.itemoid INNER JOIN QL_trnordermst so on sod.trnordermstoid = so.ordermstoid WHERE sod.trnordermstoid = '" & ordermstoid.Text & "' AND sod.to_branch = '" & CabangDDL.SelectedValue & "' AND so.to_branch = '" & CabangDDL.SelectedValue & "' AND sod.branch_code IN (SELECT branch_code FROM QL_trnorderdtl WHERE trnorderdtloid = sod.trnorderdtloid ) AND so.orderno = '" & orderno.Text & "' AND sod.trnorderdtlqty - (SELECT ISNULL(SUM(qty),0.00) FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid WHERE d.trnorderdtloid=sod.trnorderdtloid AND d.Branch_code = sod.branch_code AND d.refoid = sod.itemoid AND m.orderno = so.orderno AND trnsjjualstatus in ('Approved', 'INVOICED'))>0.00 AND sod.jenisprice NOT IN ('BONUS') AND sod.cmpcode='" & CompnyCode & "'" & sQuery
            Session("TblMat") = cKoneksi.ambiltabel(sSql, "QL_mstkatalog")
            FillGV(gvReference, sSql, "QL_mstkatalog")
            gvReference.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub ClearDetail()
        Try
            trnsjjualdtlseq.Text = "1"
            If Session("TblDtl") Is Nothing = False Then
                Dim objTable As DataTable = Session("TblDtl")
                trnsjjualdtlseq.Text = objTable.Rows.Count + 1
            Else
                Session("TblDtl") = Nothing
            End If

            If PromoOid.Text <> "0" Then
                tbldtl.Columns(0).Visible = False
            Else
                tbldtl.Columns(0).Visible = True
            End If

            I_u2.Text = "New Detail"
            tbldtl.Columns(6).Visible = True
            itemoid.Text = "" : itemname.Text = ""
            SisaQty.Text = 0 : StokQty.Text = 0
            delivorder.Text = "0.00"
            trnsjjualdtlqty.Text = ""
            trnsjjualdtlnote.Text = ""
            cProc.DisposeGridView(gvReference)
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try

    End Sub

    Private Sub generateNo()
        trnsjjualno.Text = GenerateID("ql_trnsjjualmst", CompnyCode)
        trnsjjualmstoid.Text = GenerateID("ql_trnsjjualmst", CompnyCode)
    End Sub

    Public Sub BindDataOrder()
        Try
            sSql = "SELECT so.branch_code, so.ordermstoid, so.orderno, so.trnorderdate, so.trncustoid, s.custname, so.delivdate, Case When so.typeSO='ecommers' Then 'e-Commerce' Else so.typeSO End SOType, CASE WHEN DATEDIFF(d, so.trnorderdate, GETDATE()) - 3 >= 0 THEN 'H-0' ELSE 'H' + CONVERT(VARCHAR,(DATEDIFF(d, so.trnorderdate, GETDATE()) - 3)) END ExpSO, so.promooid, so.typeSO, so.salesoid, so.spgoid, so.trnpaytype, so.timeofpaymentSO, custflag, so.amtjualnetto, (Select CAST(g.genother1 as integer) genother1 From QL_mstgen g Where g.genoid = s.timeofpayment and g.gengroup = 'PAYTYPE') termin, s.custcreditlimitrupiah, s.custcreditlimitusagerupiah, so.flagCash, so.ordertaxtype, so.flagTax From ql_trnordermst so INNER JOIN ql_mstcust s ON s.custoid=so.trncustoid AND s.branch_code=so.branch_code Where so.trncustoid=s.custoid and so.cmpcode=s.cmpcode and so.trnorderstatus IN ('Approved','POST') and (so.orderno like '%" & TcharNoTrim(orderno.Text) & "%' OR s.custname like '%" & TcharNoTrim(orderno.Text) & "%' Or so.ordermstoid LIKE '%" & TcharNoTrim(orderno.Text) & "%') and so.ordermstoid in (select trnordermstoid from QL_trnorderdtl d Where(trnorderdtlstatus in ('','In Process')) and trnorderdtlstatus NOT IN ('Completed') and d.branch_code = so.branch_code) and so.branch_code = '" & CabangDDL.SelectedValue & "' AND ISNULL((Select ISNULL(SUM(od.trnorderdtlqty),0.00) FROM QL_trnorderdtl od Where od.trnordermstoid=so.ordermstoid AND od.branch_code ='" & CabangDDL.SelectedValue & "'),0.00) - ISNULL((Select SUM(qty) FROM ql_trnsjjualdtl dt INNER JOIN ql_trnsjjualmst dm ON dt.trnsjjualmstoid=dm.trnsjjualmstoid And dt.Branch_code=dm.branch_code AND dm.ordermstoid=so.ordermstoid And dm.branch_code=so.Branch_code),0.00) <> 0.00 ORDER BY so.trnorderdate DESC, so.orderno DESC"
            Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnordermst")
            Session("gvListSO") = dt : gvListSO.DataSource = dt
            gvListSO.DataBind() : gvListSO.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Private Sub PrintSJ(ByVal oid As String, ByVal no As String)
        'untuk print
        Dim nomor As String = cKoneksi.ambilscalar("select salesdeliveryno from QL_trndelivordermst where salesdeliveryoid = " & oid & " ")
        If nomor.Substring(1, 1) = "T" Then
            reportSJ.Load(Server.MapPath("~/report/DeliveryOrderTrading.rpt"))
            reportSJ.SetParameterValue("id", oid)
            'reportSJ.SetParameterValue("cmpcode", CompnyCode)
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
        Else
            If nomor.Substring(3, 1) = "0" Then
                reportSJ.Load(Server.MapPath("~/report/DeliveryPlan.rpt"))
            Else
                reportSJ.Load(Server.MapPath("~/report/DeliveryPlan_nologo.rpt"))
            End If

            reportSJ.SetParameterValue("id", oid)
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Response.Buffer = False
            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            ' Export the Report to Response stream in PDF format and file name Customers
            Try
                reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
                reportSJ.Close()
                reportSJ.Dispose()
            Catch ex As Exception
                reportSJ.Close() : reportSJ.Dispose()
                showMessage(ex.ToString, CompnyName & " - ERROR", 1)
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub BindLastSearch()
        Dim mySqlDA As New SqlDataAdapter(Session("SearchSDO"), conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        Session("tbldata") = objDs.Tables("data")
        gvTblData.DataSource = objDs.Tables("data")
        gvTblData.DataBind()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        'showMessage(Mid("SO1/15/03/L/01/0062", 13, 2), "", 4)
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trndeliveryorder.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Delivery Order"
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")
        lbltext.Text = "Create"
        imbSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to Approval?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data ?');")
        btnPrintSJ.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to PRINT this data ?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            ddlCabang() : InitCbngNya()
            Session("ddlFilterIndex") = Nothing
            FilterPeriod1.Text = Format(GetServerTime().AddDays(-1), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData("") : InitAllDDL()
            Session("salesdeliverydtloid") = GenerateID("ql_trnsjjualdtl", CompnyCode)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update"
                FillTextBox(Session("branch_code"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                I_U.Text = "New" : generateNo()
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                Session("ItemLine") = 1
                trnsjjualdtlseq.Text = Session("ItemLine")
                trnsjjualsenddate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                salesdeliveryshipdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                btnDelete.Visible = False : btnPrintSJ.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        'SubFunction()
    End Sub

    Protected Sub btnAddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddtolist.Click
        Try
            Dim sMsg As String = ""
            If itemoid.Text <> "" Then
                sSql = "Select COUNT(*) From QL_trnorderdtl Where branch_code='" & CabangDDL.SelectedValue & "' AND jenisprice='PROMO' AND trnordermstoid IN (Select ordermstoid from QL_trnordermst Where orderno='" & Tchar(orderno.Text) & "' AND branch_code='" & CabangDDL.SelectedValue & "') AND itemoid=" & itemoid.Text & ""
                Dim cPorm As Integer = GetScalar(sSql)
                If cPorm > 0 Then
                    If ToDouble(trnsjjualdtlqty.Text) < ToDouble(SisaQty.Text) Then
                        sMsg &= "Maaf, Item " & itemname.Text & " tidak bisa di parsial karena type promo..!!<br>"
                    End If
                End If
            End If

            If itemoid.Text <> "" Then
                If checkOtherShipment(ordermstoid.Text, itemoid.Text) Then
                    ClearDetail()
                    tbldtl.SelectedIndex = -1
                    btnAddtolist.Visible = True
                    sMsg &= "There is other Delivery that using same item.<BR>Please DELETE or wait for APPROVAL that Delivery first to ensure<BR>you get up-to-date Delivery data..!!<br>"
                End If
            End If

            If itemoid.Text = "" Then
                sMsg &= "- Maaf, Silahkan pilih item !!<BR>"
            End If

            If ToDouble(trnsjjualdtlqty.Text) <= 0 Then
                sMsg &= "- Maaf, Quantity tidak boleh 0 !!<BR>"
            End If

            If trnsjjualdtlnote.Text.Length > 200 Then
                sMsg &= "Maaf, Note tidak bisa lebih dari 200 character !!<BR>"
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If

            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = setTabelDetail()
                Session("TblDtl") = dtlTable
            End If

            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView

            If I_u2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & itemoid.Text & " and itemloc = " & itemLoc.SelectedValue & ""
            Else
                dv.RowFilter = "itemoid=" & itemoid.Text & " and itemloc = " & itemLoc.SelectedValue & " AND trnsjjualdtlseq<>" & trnsjjualdtlseq.Text
            End If

            If dv.Count > 0 Then
                showMessage("Maaf, Item sudah di add to list silahkan pilih yang lain..!!", CompnyName & " - WARNING", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""

            Dim objRow As DataRow
            If I_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("trnsjjualdtlseq") = objTable.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTable.Select("trnsjjualdtlseq=" & trnsjjualdtlseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If
            objRow("trnorderdtloid") = ToDouble(orderdtloid.Text)
            objRow("itemoid") = itemoid.Text
            objRow("itemcode") = ItemCode.Text
            objRow("itembarcode1") = ItemBarcode.Text
            objRow("itemlongdesc") = itemname.Text
            objRow("QtySJ") = ToDouble(trnsjjualdtlqty.Text)
            objRow("unit") = sounitdesc.Text
            objRow("itemloc") = itemLoc.SelectedValue
            objRow("location") = itemLoc.SelectedItem
            objRow("jenisprice") = jenisprice.Text
            objRow("sjjualdtlnote") = trnsjjualdtlnote.Text
            objRow("SisaQty") = SisaQty.Text
            If I_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If

            If Integer.Parse(PromoOid.Text) <> 0 Then
                sSql = "Select COUNT(itemoid) from QL_mstpromodtl Where promoid=" & Integer.Parse(PromoOid.Text) & " AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA'"
                Dim cekPro As Integer = GetScalar(sSql)
                If cekPro > 0 Then
                    ItemBonus() : Dim data As DataTable = Session("ItemBonus")
                    If data.Rows.Count > 0 Then
                        For a1 As Integer = 0 To data.Rows.Count - 1
                            'Dim objRow As DataRow
                            If I_u2.Text = "New Detail" Then
                                objRow = objTable.NewRow()
                                objRow("trnsjjualdtlseq") = objTable.Rows.Count + 1
                            Else
                                Dim selrow As DataRow() = objTable.Select("trnsjjualdtlseq=" & trnsjjualdtlseq.Text)
                                objRow = selrow(0)
                                objRow.BeginEdit()
                            End If

                            objRow("trnorderdtloid") = data.Rows(a1).Item("trnorderdtloid")
                            objRow("itemoid") = data.Rows(a1).Item("itemoid")
                            objRow("itemcode") = data.Rows(a1).Item("itemcode").ToString
                            objRow("itembarcode1") = data.Rows(a1).Item("itembarcode1").ToString
                            objRow("itemlongdesc") = data.Rows(a1).Item("itemdesc").ToString
                            objRow("QtySJ") = ToMaskEdit(data.Rows(a1).Item("trnorderdtlqty"), 3)
                            objRow("unit") = sounitdesc.Text
                            objRow("itemloc") = itemLoc.SelectedValue
                            objRow("location") = itemLoc.SelectedItem
                            objRow("SisaQty") = ToMaskEdit(data.Rows(a1).Item("QtySJ"), 3)
                            objRow("jenisprice") = data.Rows(a1).Item("jenisprice").ToString
                            objRow("sjjualdtlnote") = trnsjjualdtlnote.Text

                            If I_u2.Text = "New Detail" Then
                                objTable.Rows.Add(objRow)
                            Else
                                objRow.EndEdit()
                            End If
                        Next
                    End If

                End If
            End If

            ClearDetail()
            Session("TblDtl") = objTable
            tbldtl.Visible = True : tbldtl.DataSource = Nothing
            tbldtl.DataSource = objTable
            tbldtl.DataBind()
            trnsjjualdtlseq.Text = objTable.Rows.Count + 1
            tbldtl.SelectedIndex = -1

            If I_U.Text = "New" Then
                imbSendApproval.Visible = False
            Else
                If posting.Text = "In Approval" Or posting.Text = "Approved" Or posting.Text = "INVOICED" Then
                    imbSendApproval.Visible = False
                Else
                    imbSendApproval.Visible = True
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2)
            Session("TblDtl") = Nothing
            Exit Sub
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim conmtroid As Int64 = 0, sMsg As String = ""
        If ordermstoid.Text.Trim = "" Then
            sMsg &= "- Maaf, Pilih Sales Order dulu..!<BR>"
        End If

        'cek apa sudah ada item dari Detail SJ
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- Maaf, Data detail belum ada, Silahkan pilih item kemudian klik tombol add to list !!<BR>"
        Else
            Dim objTableCek As DataTable = Session("TblDtl")
            If objTableCek.Rows.Count = 0 Then
                sMsg &= "- - Maaf, Data detail belum ada, Silahkan pilih item kemudian klik tombol add to list<BR>"
            End If
            For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                Dim totale As Decimal = objTableCek.Compute("Sum(QtySJ)", "trnorderdtloid=" & objTableCek.Rows(c1).Item("trnorderdtloid"))
                sSql = "Select (trnorderdtlqty) - isnull((select sum(d.qty) from ql_trnsjjualdtl d inner join ql_trnsjjualmst  m on m.trnsjjualmstoid=d.trnsjjualmstoid and m.trnsjjualstatus NOT IN ('CANCEL','HISTORY','Rejected') and m.branch_code='" & CabangDDL.SelectedValue & "' AND m.orderno='" & orderno.Text & "' inner join QL_trnorderdtl o on o.trnorderdtloid=d.trnorderdtloid and o.trnorderdtloid=" & objTableCek.Rows(c1).Item("trnorderdtloid") & " and o.branch_code='" & CabangDDL.SelectedValue & "' and m.trnsjjualmstoid <>" & ToDouble(trnsjjualmstoid.Text) & "),0.00) from QL_trnorderdtl x where x.to_branch='" & CabangDDL.SelectedValue & "' and x.branch_code = (SELECT xx.branch_code FROM QL_trnordermst xx WHERE xx.orderno = '" & orderno.Text & "' AND xx.ordermstoid=" & Integer.Parse(ordermstoid.Text) & ") and x.trnorderdtloid=" & objTableCek.Rows(c1).Item("trnorderdtloid")

                Dim orderqty As Decimal = ToDouble(GetStrData(sSql))
                If totale > orderqty Then
                    sMsg &= "Maaf, Maximal Qty untuk item '" & objTableCek.Rows(c1).Item("itemlongdesc") & "' adalah " & orderqty & " !!<br />"
                End If
            Next
        End If

        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(trnsjjualno.Text, "trnsjjualno", "ql_trnsjjualmst") Then
                generateNo()
            End If
        Else 'cek data sudah dihapus?
            If CheckDataExists(trnsjjualmstoid.Text, "trnsjjualmstoid", "ql_trnsjjualmst") = False Then
                sMsg &= "Maaf, Data sudah terhapus !!<br />"
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM ql_trnsjjualmst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            posting.Text = "In Process" : Exit Sub
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            ' insert table master
            trnjualdtloid.Text = GenerateID("ql_trnsjjualdtl", CompnyCode)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            If Ekspedisi.SelectedValue = "" Or Ekspedisi.SelectedValue = "NONE" Then
                Ekspedisi.SelectedValue = 0
            End If

            Try
                'CDate(toDate(trnsjjualsenddate.Text))
                trnsjjualmstoid.Text = GenerateID("ql_trnsjjualmst", CompnyCode)
                sSql = "INSERT INTO ql_trnsjjualmst (cmpcode, trnsjjualmstoid, trnsjjualdate, trnsjjualno,note,  trnsjjualstatus, orderno, upduser, updtime, finalapprovaluser, finalappovaldatetime, expedisioid, noExpedisi, branch_code, to_branch, createtime, ordermstoid, mtrlocoid ) VALUES " & _
                    "('" & CompnyCode & "', " & trnsjjualmstoid.Text & ", '" & GetServerTime() & "', '" & trnsjjualmstoid.Text & "', '" & Tchar(note.Text) & "', '" & posting.Text & "', '" & orderno.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', '" & CDate("01/01/1900") & "', " & Ekspedisi.SelectedValue & ", '" & Tchar(nopolisi.Text) & "', '" & CabangDDL.SelectedValue & "', '" & CabangDDL.SelectedValue & "', '" & CDate(toDate(createtime.Text)) & "', " & Integer.Parse(ordermstoid.Text) & ", " & itemLoc.SelectedValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & trnsjjualmstoid.Text & " Where tablename ='ql_trnsjjualmst' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    'INSERT TO QL_trnsjjualdtl
                    Dim counter As Integer = 0, lastRollOid As Integer = 0
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO ql_trnsjjualdtl (cmpcode, trnsjjualdtloid, trnsjjualmstoid, qty, trnorderdtloid, note, qtypak, mtrlocoid, refname, refoid, unitoid, seq, branch_code, to_branch) " & _
                        "VALUES ('" & CompnyCode & "', " & (CInt(trnjualdtloid.Text) + C1) & ", " & trnsjjualmstoid.Text & ", " & ToDouble(objTable.Rows(C1).Item("QtySJ")) & ", " & objTable.Rows(C1)("trnorderdtloid") & ", '" & Tchar(objTable.Rows(C1)("sjjualdtlnote")) & "', 0, " & itemLoc.SelectedValue & ", 'QL_MSTITEM', " & (objTable.Rows(C1).Item("itemoid")) & ", 945, " & objTable.Rows.Count + 1 & ", '" & CabangDDL.SelectedValue & "', '" & CabangDDL.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    'update oid in ql_mstoid where tablename = 'QL_trndelivorderdtl'
                    sSql = "update QL_mstoid set lastoid=" & trnjualdtloid.Text + objTable.Rows.Count - 1 & " Where tablename ='ql_trnsjjualdtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 1)
                posting.Text = "In Process" : Exit Sub
            End Try
        Else
            'update tabel
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try

                sSql = "UPDATE ql_trnsjjualmst SET trnsjjualdate='" & GetServerTime() & "', trnsjjualno='" & Session("oid") & "', note='" & Tchar(note.Text) & "', trnsjjualstatus='" & posting.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, orderno='" & Tchar(orderno.Text) & "', noExpedisi='" & Tchar(nopolisi.Text) & "', expedisioid = " & Ekspedisi.SelectedValue & ", ordermstoid=" & Integer.Parse(ordermstoid.Text) & " WHERE cmpcode='" & CompnyCode & "' and trnsjjualmstoid='" & Session("oid") & "' AND branch_code='" & CabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    trnjualdtloid.Text = GenerateID("ql_trnsjjualdtl", CompnyCode)
                    sSql = "Delete from ql_trnsjjualdtl Where trnsjjualmstoid=" & Session("oid") & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'INSERT TO QL_trnsjjualdtl
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO ql_trnsjjualdtl(cmpcode, trnsjjualdtloid, trnsjjualmstoid, qty, trnorderdtloid, note, qtypak, mtrlocoid, refname, refoid, unitoid, seq, branch_code, to_branch) " & _
                    "VALUES ('" & CompnyCode & "', " & (CInt(trnjualdtloid.Text) + C1) & ", " & Session("oid") & ", " & ToDouble(objTable.Rows(C1).Item("QtySJ")) & ", " & objTable.Rows(C1)("trnorderdtloid") & ", '" & Tchar(objTable.Rows(C1)("sjjualdtlnote")) & "', 0," & (objTable.Rows(C1).Item("itemloc")) & ",'QL_MSTITEM', " & (objTable.Rows(C1).Item("itemoid")) & ", 945," & (objTable.Rows(C1).Item("trnsjjualdtlseq")) & ", '" & CabangDDL.SelectedValue & "', '" & CabangDDL.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If posting.Text = "POST" Then
                            sSql = "update ql_trnorderdtl set orderdelivqty = (IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("QtySJ") & "), trnorderdtlstatus=(select case when orderqty <= IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("QtySJ") & " then 'Completed' else 'In Process' end) where ql_trnorderdtl.itemoid=" & objTable.Rows(C1).Item("itemoid") & " and ql_trnorderdtl.trnorderdtloid=" & objTable.Rows(C1).Item("trnorderdtloid") & " and ql_trnorderdtl.trnordermstoid= " & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    Next

                    'update oid in ql_mstoid where tablename = 'QL_trndelivorderdtl'
                    sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnjualdtloid.Text)) & " Where tablename = 'ql_trnsjjualdtl' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    If posting.Text = "Revised" Then
                        'Reset Revise note
                        sSql = "UPDATE QL_trnsjjualmst set trnsjjualstatus='In Process', revisenote = '' Where trnsjjualmstoid = " & Session("oid") & " and branch_code ='" & CabangDDL.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                'cek data sudah di send?
                If checkApproval("QL_trnsjjualmst", "In Approval", Session("oid"), "New", "FINAL", CabangDDL.SelectedValue) > 0 Then
                    objTrans.Rollback() : xCmd.Connection.Close()
                    showMessage("Data ini sudah pernah Send for Approval, tekan Cancel dan cek ulang data transaksi ini.", CompnyName & " - WARNING", 2)
                    Exit Sub
                End If

                If posting.Text = "In Approval" Then

                    sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjjualmst' and branch_code Like '%" & CabangDDL.SelectedValue & "%' order by approvallevel"
                    Dim dtData2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_approvalstructure")

                    If dtData2.Rows.Count > 0 Then
                        Session("TblApproval") = dtData2
                    Else
                        objTrans.Rollback() : xCmd.Connection.Close()
                        showMessage("Setting Approval untuk SDO belum ada, silakan hubungi administrator.", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If

                    Session("AppOid") = GenerateID("QL_Approval", CompnyCode)

                    If Not Session("TblApproval") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("TblApproval")
                        For c1 As Int16 = 0 To objTable.Rows.Count - 1
                            sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) " & _
                                " VALUES ('" & CompnyCode & "', " & Session("AppOid") + c1 & ", '" & "DO" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_trnsjjualmst', '" & Session("oid") & "', 'In Approval', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "', '" & CabangDDL.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next

                        sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 1)
                posting.Text = "In Process" : Exit Sub
            End Try
        End If
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If trnsjjualmstoid.Text.Trim = "" Then
            showMessage("Please fill General ID!", CompnyName & " - ERROR", 1)
            Exit Sub
        End If
        Dim dtTempWeb As DataTable = checkApprovaldata("QL_trnsjjualmst", "In Approval", trnsjjualmstoid.Text, "", "New", CabangDDL.SelectedValue)
        'cek data sudah di send?
        If checkApproval("QL_trnsjjualmst", "In Approval", Session("oid"), "New", "FINAL", Session("branch_id")) > 0 Then
            showMessage("Data Delivery Order sudah send for Approval", CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        Try
            xCmd.Transaction = objTrans
            ' Update status Approval if exist to DELETED
            If dtTempWeb.Rows.Count > 0 Then
                For C1 As Integer = 0 To dtTempWeb.Rows.Count - 1
                    sSql = "UPDATE ql_approval SET statusrequest='Deleted' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid='" & dtTempWeb.Rows(C1)("approvaloid").ToString & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            'DELETE QL_trnsjbelidtl
            sSql = "Delete from ql_trnsjjualdtl where trnsjjualmstoid = " & Session("oid") & " AND branch_code='" & CabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete from ql_trnsjjualmst where cmpcode='" & CompnyCode & "' and trnsjjualmstoid=" & Session("oid") & " AND branch_code='" & CabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = ""
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub

    Protected Sub btnsearhorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsearhorder.Click
        BindDataOrder()
        gvListSO.Visible = True : sudahcek = 0
    End Sub

    Protected Sub gvListSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSO.SelectedIndexChanged
        Try
            clearRef() : ClearDetail()
            Dim dtSO As DataTable = Session("gvListSO")
            Dim dvSO As DataView = dtSO.DefaultView
            dvSO.RowFilter = "ordermstoid=" & gvListSO.SelectedDataKey.Item("ordermstoid") & "AND branch_code='" & CabangDDL.SelectedValue & "'"
            If dvSO.Count > 0 Then
                ordermstoid.Text = dvSO(0).Item("ordermstoid")
                orderno.Text = dvSO(0).Item("orderno").ToString
                SpgOid.Text = dvSO(0).Item("spgoid")
                trnpaytypeoid.Text = dvSO(0).Item("trnpaytype")
                custflag.Text = dvSO(0).Item("custflag")
                crUsage.Text = ToDouble(dvSO(0).Item("custcreditlimitusagerupiah"))
                crLimit.Text = ToDouble(dvSO(0).Item("custcreditlimitrupiah"))
                amtjualnetto.Text = ToDouble(dvSO(0).Item("amtjualnetto"))
                termin.Text = ToDouble(dvSO(0).Item("termin"))
                typeSO.Text = dvSO(0).Item("typeSO")
                flagTax.Text = dvSO(0).Item("flagTax")
                sudahcek = 1
                custname.Text = dvSO(0).Item("custname")
                trncustoid.Text = dvSO(0).Item("trncustoid")
                PromoOid.Text = dvSO(0).Item("promooid")
                trnsjjualdate.Text = Format(CDate((dvSO(0).Item("trnorderdate"))), "dd/MM/yyyy")
                salesdeliveryshipdate.Text = Format(CDate(dvSO(0).Item("delivdate")), "dd/MM/yyyy")

                cProc.DisposeGridView(gvListSO)
                gvListSO.Visible = False
            End If

            sSql = "SELECT a.genoid, a1.gendesc +' - '+ a.gendesc gendesc from QL_mstgen a INNER JOIN QL_mstgen a1 ON a1.genoid = a.genother1 AND a1.gengroup = 'WAREHOUSE' WHERE a.gengroup = 'LOCATION' AND a1.genother1 IN ('GROSIR','SUPPLIER') And a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & CabangDDL.SelectedValue & "' and gengroup = 'CABANG')"
            If typeSO.Text = "ecommers" Then
                sSql &= " AND a.genother6 = 'ECOMMERCE'"
            ElseIf typeSO.Text = "Kanvas" Then
                sSql &= " AND a.genother4 = 'KANVAS' AND a.genother6 = 'UMUM'"
            ElseIf typeSO.Text = "Konsinyasi" Then
                sSql &= " AND a.genother6 = 'UMUM'"
            Else
                sSql &= " AND a.genother6 = 'UMUM' AND a.genother4 <> 'KANVAS'"
            End If
            FillDDL(itemLoc, sSql)

            btnCancel.Visible = True
            If itemLoc.Items.Count = 0 Then
                showMessage("Please create/fill Data General in group LOCATION! <br/> ", CompnyName & " - Warning", 2)
                clearRef() : ClearDetail()
                cProc.DisposeGridView(gvListSO)
                gvListSO.Visible = False
                InitAllDDL()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try

    End Sub

    Protected Sub lkbCloseSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.DisposeGridView(gvListSO)
    End Sub

    Protected Sub gvReference_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReference.SelectedIndexChanged
        Try
            itemoid.Text = gvReference.SelectedDataKey.Item("itemoid")
            If Not Session("TblMat") Is Nothing Then
                Dim dt As DataTable = Session("TblMat")
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "itemoid=" & itemoid.Text
                If dv.Count > 0 Then
                    jenisprice.Text = dv(0).Item("jenisprice").ToString
                    ItemCode.Text = dv(0).Item("itemcode").ToString
                    ItemBarcode.Text = dv(0).Item("itembarcode1").ToString
                    itemname.Text = dv(0).Item("itemlongdesc")
                    SisaQty.Text = ToMaskEdit(dv(0).Item("SisaQty"), 3)
                    orderdtloid.Text = dv(0).Item("trnorderdtloid").ToString
                    delivorder.Text = ToMaskEdit(dv(0).Item("QtySJ"), 3)
                    StokQty.Text = ToMaskEdit(CekStock(itemoid.Text), 3)
                    jenisprice.Text = dv(0).Item("jenisprice").ToString
                    itemLoc.Enabled = False : gvReference.Visible = False
                End If
                dv.RowFilter = ""
            End If
            itemname.AutoPostBack = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />", CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
        tbldtl.SelectedIndex = -1
        btnAddtolist.Visible = True
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False
        mpeMsgbox.Hide()
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        If ordermstoid.Text = "" Then
            showMessage("Please choose Order first!!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        itemname.AutoPostBack = False
        If itemname.Text.Trim() <> "" Then
            BindDataSalesOrder(" AND itemdesc LIKE '%" & Tchar(itemname.Text) & "%'")
        Else
            BindDataSalesOrder("")
        End If
    End Sub

    Protected Sub gvListSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clearRef() : ClearDetail()
    End Sub

    Protected Sub trnsjjualtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        gvListSO.PageIndex = e.NewPageIndex
        BindDataOrder()
    End Sub

    Protected Sub gvListSO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListSO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        Session("index") = tbldtl.SelectedIndex
        If Session("TblDtl") Is Nothing = False Then
            I_u2.Text = "Update Detail"
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "trnsjjualdtlseq=" & tbldtl.SelectedDataKey(0).ToString().Trim
            trnsjjualdtlseq.Text = dv.Item(0).Item("trnsjjualdtlseq").ToString
            itemoid.Text = dv.Item(0).Item("itemoid").ToString
            itemname.Text = dv.Item(0).Item("itemlongdesc").ToString
            ItemCode.Text = dv.Item(0).Item("itemcode").ToString
            ItemBarcode.Text = dv.Item(0).Item("itembarcode1").ToString
            SisaQty.Text = ToMaskEdit(dv.Item(0).Item("SisaQty").ToString, 3)
            orderdtloid.Text = dv.Item(0).Item("trnorderdtloid").ToString
            StokQty.Text = ToMaskEdit(CekStock(itemoid.Text), 3)
            itemLoc.SelectedValue = dv.Item(0).Item("itemloc").ToString
            trnsjjualdtlqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("QtySJ")), 3)
            trnsjjualdtlnote.Text = dv.Item(0).Item("sjjualdtlnote").ToString
            dv.RowFilter = "" : btnAddtolist.Visible = True
        End If
    End Sub

    Protected Sub trnsjjualdtlqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trnsjjualdtlqty.TextChanged
        If Not itemoid.Text = "" Then
            If ToDouble(trnsjjualdtlqty.Text) > ToDouble(SisaQty.Text) Then
                showMessage("Quantity is Over !!", CompnyName & " - WARNING", 2)
                btnAddtolist.Visible = False
            Else
                btnAddtolist.Visible = True
            End If
        Else
            showMessage("Pilih Item Terlebihdahulu !!", CompnyName & " - WARNING", 2)
            trnsjjualdtlqty.Text = ""
        End If
    End Sub

    Protected Sub gvReference_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReference.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            'e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub tbldtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTable As DataTable = Session("TblDtl")
        Dim objrow() As DataRow

        Dim dvTemp As DataView = objTable.DefaultView
        dvTemp.RowFilter = "trnsjjualdtlseq=" & iIndex + 1 & ""

        If dvTemp.Count > 0 Then
            If dvTemp(0)("jenisprice") = "BONUS" Then
                showMessage("Maaf, Anda menghapus barang bonus, pilih jenis promo jika menghapus barang jenis bonus..!!", CompnyName & " - WARNING", 2)
                dvTemp.RowFilter = ""
                Exit Sub
            End If
        End If

        objrow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim iItemOid As Integer = objTable.Rows(e.RowIndex)("itemoid")

        If dvTemp(0)("jenisprice") = "PROMO" Then
            dvTemp.RowFilter = ""
            dvTemp.RowFilter = "jenisprice NOT IN ('NORMAL','KHUSUS','NOTA')"
            If dvTemp.Count > 0 Then
                For C1 As Integer = dvTemp.Count - 1 To 0 Step -1
                    dvTemp(C1).Row.Delete()
                Next
            End If
            dvTemp.RowFilter = ""
        Else
            dvTemp.RowFilter = ""
            objTable.Rows.RemoveAt(e.RowIndex)
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("trnsjjualdtlseq") = C1 + 1
                dr.EndEdit()
            Next
        End If

        Session("TblDtl") = objTable
        tbldtl.Visible = True
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        clearRef() : ClearDetail()
        cProc.DisposeGridView(gvListSO)
        gvListSO.Visible = False
        InitAllDDL()
    End Sub

    Protected Sub gvTblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTblData.PageIndexChanging
        gvTblData.PageIndex = e.NewPageIndex
        Dim sPlus As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFindSJ.Text) & "%' "
        BindData(sPlus)
    End Sub

    Protected Sub gvTblData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTblData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("TblDtl") = Nothing : Session("oid") = Nothing
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reportSJ.Close() : reportSJ.Dispose()
    End Sub

    Protected Sub btnPrintSJ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintSJ.Click
        Try
            reportSJ.Load(Server.MapPath("~/report/rptDeliveryOrder2.rpt"))
            reportSJ.SetParameterValue("printSJ", "DELIVERY ORDER")
            reportSJ.SetParameterValue("id", trnsjjualmstoid.Text)
            reportSJ.SetParameterValue("branch_id", CabangDDL.SelectedValue)
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'reportSJ.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, trnsjjualno.Text & "_" & Format(GetServerTime(), "dd_MM_yy"))
            reportSJ.Close() : reportSJ.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : reportSJ.Close()
            reportSJ.Dispose() : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub

    Protected Sub imbPrintFromList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sCabang As String = Session("branch_id")
        Try
            'PrintSJ(sender.CommandArgument, sender.ToolTip)
            reportSJ.Load(Server.MapPath("~/report/rptDeliveryOrder2.rpt"))
            reportSJ.SetParameterValue("printSJ", "DELIVERY ORDER")
            reportSJ.SetParameterValue("id", sender.commandargument)
            reportSJ.SetParameterValue("branch_id", sCabang)
            cProc.SetDBLogonForReport(reportSJ, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'reportSJ.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()

            sSql = "SELECT trnsjjualno from ql_trnsjjualmst WHERE trnsjjualmstoid = " & sender.commandargument & ""
            Dim NODO As Object = cKoneksi.ambilscalar(sSql)
            reportSJ.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, NODO & "_" & Format(GetServerTime(), "dd_MM_yy"))
            reportSJ.Close() : reportSJ.Dispose()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : reportSJ.Close() : reportSJ.Dispose() : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub

    Protected Sub lkbCloseUser_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvListUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imbSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSendApproval.Click

        If CheckDateIsClosedMtr(CDate(toDate(trnsjjualsenddate.Text)), CompnyCode) Then
            showMessage("This Periode was closed !", CompnyName & " - Warning", 2)
            posting.Text = "IN PROCESS"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(trnsjjualsenddate.Text), CompnyCode) = False Then
            showMessage("This is not active periode !", CompnyName & " - Warning", 2)
            posting.Text = "IN PROCESS"
            Exit Sub
        End If

        sSql = "SELECT count(*) from QL_approvalstructure WHERE tablename = 'ql_trnsjjualmst' AND approvaltype = 'FINAL' AND Branch_code like '%" & CabangDDL.SelectedValue & "%'"
        Dim XApproval As Integer = cKoneksi.ambilscalar(sSql)
        If XApproval = 0 Then
            showMessage("Approval User tidak ditemukan, silahkan hubungi administrator", CompnyCode, 3)
            Exit Sub
        Else
            posting.Text = "In Approval"
            btnSave_Click(sender, e)
        End If

    End Sub

    Protected Sub gvTblData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTblData.SelectedIndexChanged
        Response.Redirect("trndeliveryorder.aspx?branch_code=" & gvTblData.SelectedDataKey("branch_code").ToString & "&oid=" & gvTblData.SelectedDataKey("trnsjjualmstoid") & "")
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
        InitAllDDL()
    End Sub

    Protected Sub DDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLStatus.SelectedIndexChanged
        chkStatus.Checked = True
    End Sub

    Protected Sub btnfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfind.Click
        Dim sPlus As String = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFindSJ.Text) & "%' "
        BindData(sPlus) : Session("SearchSDO") = sqlTempSearch
    End Sub

    Protected Sub btnviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnviewall.Click
        ddlFilter.SelectedIndex = 0 : txtFindSJ.Text = ""
        chkPeriod.Checked = False : chkStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbLastSearch.Click
        If Session("SearchSDO") Is Nothing = False Then
            BindLastSearch()
        End If
    End Sub

    Protected Sub itemname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemname.TextChanged
        sSql = "SELECT genother7 from QL_mstgen Where gengroup='CABANG' AND gencode='" & CabangDDL.SelectedValue & "'"
        If GetStrData(sSql) = "YA" Then
            If ordermstoid.Text = "" Then
                showMessage("Maaf, Pilih nomer SO dulu..!!", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            BindKatalog(" AND (itembarcode1 LIKE '%" & TcharNoTrim(itemname.Text) & "%' OR itemcode LIKE '%" & TcharNoTrim(itemname.Text) & "%')")
            SelectChanged(GetStrData(sSql))
        End If
    End Sub

    Protected Sub imbClearItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearItem.Click
        ClearDetail() : tbldtl.SelectedIndex = -1 : itemname.AutoPostBack = True
    End Sub

#End Region

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        Dim conmtroid As Int64 = 0, sMsg As String = "", trnamtjual As Double = 0, trnamtjualnetto As Double = 0, cekDO As Double = 0, CekSI As Double = 0, diskondtl As Double = 0, statusUser As String = "", trnjualmstoid As Integer = 0, jualdtloid As Integer = 0, vIDMst As Integer = 0, vIDDtl As Integer = 0, totale As Decimal = 0, trnjualno As String = "", LastHpp As Double = 0, sVarAR As String = "", iAROid As Integer = 0, TaxFlag As String = "1"
        setJurnalSI()
        Dim objdt As DataTable = Session("TblDtl")
        trnjualmstoid = GenerateID("QL_trnjualmst", CompnyCode)
        jualdtloid = GenerateID("QL_trnjualdtl", CompnyCode)
        vIDMst = GenerateID("QL_trnglmst", CompnyCode)
        vIDDtl = GenerateID("QL_trngldtl", CompnyCode)
        conmtroid = GenerateID("QL_conmtr", CompnyCode)
        posting.Text = "Approved"
        Dim vConARId As Integer = GenerateID("QL_conar", CompnyCode)
        Dim dPayDueDate As Date = GetServerTime().AddDays(termin.Text)
        If termin.Text <> 0 Then
            If crLimit.Text = 0 Then
                sMsg &= "Maaf, Customer " & custname.Text & " Memiliki Termin " & termin.Text & " Hari, Credit Limit 0 hanya untuk customer dengan termin cash, Silahkan Hubungi Admin untuk pengajuan credit limit!<br>"
            End If
        End If

        If ordermstoid.Text.Trim = "" Then
            sMsg &= "- Maaf, Pilih Sales Order dulu..!<BR>"
        End If

        'cek apa sudah ada item dari Detail SJ
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- Maaf, Data detail belum ada, Silahkan pilih item kemudian klik tombol add to list !!<BR>"
        Else
            If objdt.Rows.Count = 0 Then
                sMsg &= "- - Maaf, Data detail belum ada, Silahkan pilih item kemudian klik tombol add to list<BR>"
            End If

            For c1 As Int16 = 0 To objdt.Rows.Count - 1
                totale = objdt.Compute("Sum(QtySJ)", "trnorderdtloid=" & objdt.Rows(c1).Item("trnorderdtloid"))
                If totale > ToDouble(objdt.Rows(c1).Item("soqty")) Then
                    sMsg &= "Maaf, Maximal Qty untuk item '" & objdt.Rows(c1).Item("itemlongdesc") & "' adalah " & ToMaskEdit(objdt.Rows(c1).Item("soqty"), 3) & " !!<br />"
                End If
            Next
        End If

        If CheckDataExists(trnsjjualmstoid.Text, "trnsjjualmstoid", "ql_trnsjjualmst") = False Then
            sMsg &= "Maaf, Data sudah terhapus !!<br />"
        End If

        Dim objTable As DataTable = Session("TblDtl1")
        If objTable.Rows.Count < 1 Then
            sMsg &= -"Maaf, Detail Sales Delivery Order data tidak ada, silahkan cek di form transaksi Sales delivery Order data!! <br>"
        End If

        trnamtjual = objTable.Compute("SUM(AmtJual)", "")
        trnamtjualnetto = objTable.Compute("SUM(NettoAmt)", "")
        diskondtl = objTable.Compute("SUM(discperqty)", "")
        amtjualnetto.Text = Math.Round(ToDouble(trnamtjualnetto), 2)

        sSql = "Select COUNT(trnsjjualmstoid) From ql_trnsjjualmst Where trnsjjualstatus='Approved' And trnsjjualmstoid=" & trnsjjualmstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "' AND ordermstoid='" & ordermstoid.Text & "'"
        cekDO = GetScalar(sSql)
        If ToDouble(cekDO) > 0 Then
            sMsg &= "- Maaf, Nomer Draft Ini sudah di approved..!!<br>"
        End If

        sSql = "Select COUNT(trnsjjualmstoid) From QL_trnjualmst Where trnjualstatus='Approved' And branch_code='" & CabangDDL.SelectedValue & "' And trnsjjualmstoid=" & trnsjjualmstoid.Text & " AND ordermstoid='" & ordermstoid.Text & "'"
        CekSI = GetScalar(sSql)

        If ToDouble(CekSI) > 0 Then
            sMsg &= "- Maaf, Nomer Draft Ini sudah dicetak invoice..!!<br>"
        End If

        For C1 As Integer = 0 To objTable.Rows.Count - 1
            sSql = "Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) stokqty FROM QL_conmtr con Inner Join ql_mstitem i ON i.itemoid=con.refoid Where con.refoid=" & objTable.Rows(C1).Item("itemoid") & " AND mtrlocoid=" & itemLoc.SelectedValue & " AND periodacctg='" & GetServerTime.ToString("yyyy-MM") & "' AND branch_code='" & CabangDDL.SelectedValue & "' GROUP BY refoid, mtrlocoid, branch_code, i.stockflag, periodacctg, i.itemdesc Order By con.refoid"
            Dim sTock As Double = GetScalar(sSql)
            sSql = "Select itemdesc from ql_mstitem Where itemoid=" & objTable.Rows(C1).Item("itemoid")
            Dim item As String = GetStrData(sSql)
            If ToDouble(objTable.Rows(C1).Item("qty").ToString) > ToDouble(sTock) Then
                sMsg &= "- Maaf, Item " & item & " Jumlah Stock " & ToMaskEdit(sTock, 3) & ", Stock tidak memenuhi !! <br>"
            End If 
        Next

        If flagTax.Text = "1" Then
            TaxFlag = "0"
        End If

        sVarAR = GetInterfaceValue("VAR_AR", CabangDDL.SelectedValue)
        If sVarAR = "?" Then
            sMsg &= "Interface VAR_AR belum disetting, silahkan hubungi staff accounting,,!!<br>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            posting.Text = "In Process" : Exit Sub
        End If

        iAROid = GetAccountOid(sVarAR, False)
        sSql = "SELECT trnorderdate FROM QL_trnordermst WHERE ordermstoid=" & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
        Dim sorderdate As Date = GetScalar(sSql)
        Dim dPayDueDateOff As Date = sorderdate.AddDays(termin.Text)

        '------- genrated no DO ------
        '=============================
        sSql = "Select genother1 From ql_mstgen Where gengroup = 'CABANG' and gencode='" & CabangDDL.SelectedValue & "'"
        Dim CabangKirim As String = GetStrData(sSql)
        Dim sNo As String = "DO" & TaxFlag & "/" & CabangKirim & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnsjjualno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trnsjjualmst WHERE cmpcode='" & CompnyCode & "' AND trnsjjualno LIKE '" & sNo & "%'"
        trnsjjualno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)

        '------- genrated no SI ------
        '=============================
        sSql = "Select genother1 From ql_mstgen where gengroup = 'Cabang' And gencode='" & CabangDDL.SelectedValue & "'"
        Dim CbgBuatSI As String = cKoneksi.ambilscalar(sSql)
        Dim sNoSI As String = "SI" & TaxFlag & "/" & CabangKirim & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trnjualmst WHERE cmpcode='" & CompnyCode & "' AND trnjualno LIKE '" & sNoSI & "%'"
        trnjualno = GenNumberString(sNoSI, "", cKoneksi.ambilscalar(sSql), 4)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE ql_trnsjjualmst SET trnsjjualdate='" & GetServerTime() & "', trnsjjualno='" & trnsjjualno.Text & "',note='" & Tchar(note.Text) & "', trnsjjualstatus='" & posting.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, orderno='" & Tchar(orderno.Text) & "', noExpedisi='" & Tchar(nopolisi.Text) & "', expedisioid = " & Ekspedisi.SelectedValue & ", ordermstoid=" & Integer.Parse(ordermstoid.Text) & " WHERE cmpcode='" & CompnyCode & "' and trnsjjualmstoid='" & Session("oid") & "' AND branch_code='" & CabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Not Session("TblDtl") Is Nothing Then
                trnjualdtloid.Text = GenerateID("ql_trnsjjualdtl", CompnyCode)
                sSql = "Delete from ql_trnsjjualdtl Where trnsjjualmstoid=" & Session("oid") & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'INSERT TO QL_trnsjjualdtl
                For C1 As Int16 = 0 To objTable.Rows.Count - 1
                    sSql = "INSERT INTO ql_trnsjjualdtl (cmpcode, trnsjjualdtloid, trnsjjualmstoid, qty, trnorderdtloid, note, qtypak, mtrlocoid, refname, refoid, unitoid, seq, branch_code, to_branch) " & _
                "VALUES ('" & CompnyCode & "', " & (CInt(trnjualdtloid.Text) + C1) & ", " & Session("oid") & ", " & ToDouble(objdt.Rows(C1).Item("QtySJ")) & ", " & objdt.Rows(C1)("trnorderdtloid") & ", '" & Tchar(objdt.Rows(C1)("sjjualdtlnote")) & "', 0," & (objdt.Rows(C1).Item("itemloc")) & ",'QL_MSTITEM', " & (objdt.Rows(C1).Item("itemoid")) & ", 945," & (objdt.Rows(C1).Item("trnsjjualdtlseq")) & ", '" & CabangDDL.SelectedValue & "', '" & CabangDDL.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update ql_trnorderdtl set orderdelivqty = (IsNull(orderdelivqty,0) + " & objdt.Rows(C1).Item("QtySJ") & "), trnorderdtlstatus=(select case when trnorderdtlqty <= IsNull(orderdelivqty,0) + " & objdt.Rows(C1).Item("QtySJ") & " then 'Completed' else 'In Process' end) where itemoid=" & objdt.Rows(C1).Item("itemoid") & " and trnorderdtloid=" & objdt.Rows(C1).Item("trnorderdtloid") & " and trnordermstoid= " & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                'update oid in ql_mstoid where tablename = 'QL_trndelivorderdtl'
                sSql = "update QL_mstoid set lastoid=" & (objdt.Rows.Count - 1 + CInt(trnjualdtloid.Text)) & " Where tablename = 'ql_trnsjjualdtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If posting.Text = "Revised" Then
                    'Reset Revise note
                    sSql = "UPDATE QL_trnsjjualmst set trnsjjualstatus='In Process', revisenote = '' Where trnsjjualmstoid = " & Session("oid") & " and branch_code ='" & CabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If crLimit.Text > 0 Then
                sSql = "Select Count(genoid) from QL_mstgen where genother4='Y' and gengroup='CABANG' AND gencode='" & CabangDDL.SelectedValue & "'"
                Dim skip As Integer = GetStrData(sSql)
                'If confirmCR.Text.Trim = "" Then
                ' DO bisa di-Approve selama USAGE+DO < CL
                If skip = 0 Then
                    Dim g As Double = ToDouble(crLimit.Text) - (ToDouble(crUsage.Text) + ToDouble(amtjualnetto.Text))
                    If ToDouble(g) < 0 Then
                        showMessage("Maaf, Sisa Credit Limit Untuk customer " & custname.Text & " tidak mencukupi." & _
                            "<BR>- Sisa Credit Limit = " & ToMaskEdit((ToDouble(crLimit.Text) - ToDouble(crUsage.Text)), 3) & "<BR>" & _
                            "- Jumlah Transaksi = " & ToMaskEdit(ToDouble(amtjualnetto.Text), 3), CompnyName & " - WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If
                End If
                'End If

                If custflag.Text.ToUpper = "INACTIVE" Then
                    showMessage("Maaf, Customer " & custname.Text & " Dalam Status " & custflag.Text, CompnyName & " - WARNING", 2)
                    objTrans.Rollback() : conn.Close()
                    Exit Sub
                End If

                sSql = "select isnull(custdtlstatus,'') From ql_mstcustdtl Where custoid = " & trncustoid.Text & " and custdtlstatus = 'Enabled' and custdtlflag = 'Over Due'"
                xCmd.CommandText = sSql : Dim custflagoverdue As String = xCmd.ExecuteScalar

                If custflagoverdue <> "Enabled" Then
                    sSql = "SELECT COUNT(-1) FROM (" & _
                        "select j.trnjualmstoid, j.trnamtjualnettoidr, j.accumpaymentidr, j.amtreturidr, j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today From QL_trnjualmst j WHERE j.trncustoid=" & trncustoid.Text & " AND j.trnjualstatus='POST' AND branch_code='" & CabangDDL.SelectedValue & "') AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)"
                    xCmd.CommandText = sSql : Dim countOD As Integer = xCmd.ExecuteScalar

                    ' New Query, langsung ambil dr trnjualmst = trnamtjualnettoidr-(accumpaymentidr+amtreturidr)
                    sSql = "SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM (" & _
                        "select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate," & _
                        "(SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate," & _
                        "CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today " & _
                        "from QL_trnjualmst j WHERE j.trncustoid=" & trncustoid.Text & " AND j.trnjualstatus='POST' AND branch_code='" & CabangDDL.SelectedValue & "') AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)"
                    xCmd.CommandText = sSql : Dim sumOD As Double = xCmd.ExecuteScalar

                    If countOD > 0 Then
                        showMessage("Maaf, Customer " & custname.Text & " Masih Memiliki Nota Outstanding." & _
                        "<BR>- Total Nota yang Masih Outstanding " & countOD & "<BR>" & _
                        "- Jumlah Transaksi = " & ToMaskEdit(ToDouble(sumOD), 3), CompnyName & " - WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If
                End If

                sSql = "update ql_mstcustgroup set custgroupcreditlimitusage=ISNULL(custgroupcreditlimitusage,0.00)+" & ToDouble(amtjualnetto.Text) & ",custgroupcreditlimitusagerupiah=ISNULL(custgroupcreditlimitusagerupiah,0.00)+" & ToDouble(amtjualnetto.Text) & " WHERE cmpcode = '" & CompnyCode & "' And custgroupoid=(Select custgroupoid from QL_mstcust Where custoid=" & trncustoid.Text & " And branch_code='" & CabangDDL.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstcust set custcreditlimitusage=ISNULL(custcreditlimitusage,0.00)+" & ToDouble(amtjualnetto.Text) & ", custcreditlimitusagerupiah=ISNULL(custcreditlimitusagerupiah,0.00)+" & ToDouble(amtjualnetto.Text) & " Where custoid=" & trncustoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "Insert into QL_trnjualmst ([cmpcode],[trnjualmstoid], [orderno],[trnsjjualno], [periodacctg], [trnjualtype], [trnjualno], [trnjualdate], [trnjualref], [trncustoid], [trncustname], [trnpaytype], [trnamtjual], [trnamtjualidr], [trnamtjualusd], [trntaxpct], [trnamttax],[trnamttaxidr], [trnamttaxusd], [trndisctype], [trndiscamt], [trndiscamtidr], [trndiscamtusd], [trnamtjualnetto], [trnamtjualnettoidr], [trnamtjualnettousd],[trnjualnote], [trnjualstatus], [trnjualres1],[amtdischdr],[amtdiscdtl],[accumpayment],[lastpaymentdate], [userpay], [createuser], [upduser], [updtime], [refNotaRevisi], [salesoid], [trnorderstatus], [ekspedisioid], [amtdischdr2], [trndisctype2], [trndiscamt2], [postacctg], [amtretur], [currencyoid], [currencyrate], [spgoid], [upduser_edit], [updtime_edit], [finalappovaldatetime], [finalapprovaluser], [finalapprovalcode], [canceluser], [canceltime], [cancelnote], [trnamtjualnettoacctg], [statusDN], [statusCN], [branch_code], trnsjjualmstoid, flagCash, ordermstoid) " & _
            " VALUES ('" & CompnyCode & "', " & trnjualmstoid & ", '" & orderno.Text & "', '" & trnsjjualno.Text & "', '" & GetDateToPeriodAcctg(GetServerTime()) & "', '" & typeSO.Text & "', '" & trnjualno & "', '" & GetServerTime() & "', '', " & trncustoid.Text & ", '" & Tchar(custname.Text) & "', " & trnpaytypeoid.Text & ", " & ToDouble(trnamtjual) & ", " & ToDouble(trnamtjual) & ", " & ToDouble(trnamtjual) & ", " & 0 & ", " & ToDouble(trnamtjual) & ", " & ToDouble(trnamtjual) & ", " & (ToDouble(trnamtjual)) & ", 'AMT', 0, 0, 0, " & ToDouble(amtjualnetto.Text) & ", " & ToDouble(amtjualnetto.Text) & ", " & ToDouble(amtjualnetto.Text) & ", '" & Tchar(note.Text) & "', 'POST', '', 0, '" & ToDouble(diskondtl) & "', 0, '1/1/1900', 0, '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, '', " & Ekspedisi.Text & ", 0, 'AMT', 0, '', 0, '" & 1 & "', '" & 1 & "', " & SpgOid.Text & ", '', '1/1/1900', '1/1/1900', '', '', '', '1/1/1900', '', " & ToDouble(amtjualnetto.Text) & ", 0, 0, '" & CabangDDL.SelectedValue & "', " & trnsjjualmstoid.Text & ", '" & flagCash.Text & "', " & Integer.Parse(ordermstoid.Text) & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & trnjualmstoid & " where tablename ='QL_trnjualmst' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim cntr As Integer = 0, seq As Integer = 0, tampstatus(objTable.Rows.Count - 1) As String
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                seq = seq + 1
                sSql = "insert into QL_trnjualdtl ([cmpcode], [trnjualdtloid], [trnjualmstoid], [trnjualdtlseq], [itemloc], [itemoid], [trnjualdtlqty], [trnjualdtlunitoid], [unitSeq], [trnjualdtlprice], [trnjualdtlpriceunitoid], [trnjualdtldisctype], [trnjualdtldiscqty], [trnjualflag], [trnjualnote], [trnjualstatus], [amtjualdisc], [amtjualnetto], [createuser], [upduser], [updtime], [trnjualdtldisctype2], [trnjualdtldiscqty2], [amtjualdisc2], [trnorderdtloid], [trnjualdtlqty_retur], [trnjualdtlqty_retur_unit3], [trnjualdtlqty_unit3], trnsjjualdtloid, branch_code, amtjualdisc1)" & _
                " VALUES ('" & CompnyCode & "', " & (CInt(jualdtloid) + C1) & ", " & trnjualmstoid & ", " & seq & ", " & (objTable.Rows(C1).Item("mtrlocoid")) & ", " & (objTable.Rows(C1).Item("itemoid")) & ", " & ToDouble(objTable.Rows(C1).Item("qty")) & ", " & (objTable.Rows(C1).Item("itemunitoid")) & ", " & (objTable.Rows(C1).Item("trnsjjualdtlseq")) & ", " & ToDouble(objTable.Rows(C1).Item("trnorderprice")) & ", " & (objTable.Rows(C1).Item("itemunitoid")) & ", 'AMT', " & ToDouble(objTable.Rows(C1).Item("discperqty")) & ", '', '', 'In Process', " & ToDouble(objTable.Rows(C1).Item("trndiskonpromo")) & ", " & ToDouble(objTable.Rows(C1).Item("NettoAmt")) & ", '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'AMT', 0, " & ToDouble(objTable.Rows(C1)("disc2")) & ", " & objTable.Rows(C1)("trnorderdtloid") & ", 0, 0, 0, " & objTable.Rows(C1)("trnsjjualdtloid") & ", '" & CabangDDL.SelectedValue & "', " & ToDouble(objTable.Rows(C1)("disc1")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'If posting.Text = "POST" Then
                sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update ql_trnorderdtl set orderdelivqty = (IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("qty") & "), trnorderdtlstatus=(select case when trnorderdtlqty <= IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("qty") & " then 'Completed' else 'In Process' End) Where itemoid=" & objTable.Rows(C1).Item("itemoid") & " And trnorderdtloid=" & objTable.Rows(C1).Item("trnorderdtloid") & " and trnordermstoid= " & ordermstoid.Text & " AND branch_code='" & CabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '--------------------------
                sSql = "Select hpp from ql_mstitem where itemoid=" & objTable.Rows(C1).Item("itemoid") & ""
                xCmd.CommandText = sSql : LastHpp = xCmd.ExecuteScalar

                '------------------------------
                'insert into connmtr if posting
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, formoid, formname, type, trndate, periodacctg, refoid, refname, qtyout, amount, note, formaction, upduser, updtime, mtrlocoid, unitoid, hpp, branch_code) " & _
                " VALUES ('" & CompnyCode & "', " & conmtroid & ", " & (objTable.Rows(C1).Item("trnsjjualdtloid")) & ", 'QL_trnsjjualdtl', 'JUAL', '" & GetServerTime() & "', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & objTable.Rows(C1).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(C1).Item("qty") & ", " & objTable.Rows(C1).Item("qty") & " * " & ToMaskEdit(ToDouble(" (select trnamountdtl/trnorderdtlqty from ql_trnorderdtl Where to_branch='" & CabangDDL.SelectedValue & "' And trnorderdtloid = " & objTable.Rows(C1).Item("trnorderdtloid") & ")"), 4) & ", '" & Tchar(objTable.Rows(C1).Item("note")) & "', '" & trnsjjualno.Text & "', '" & Session("UserID") & "', current_timestamp, " & objTable.Rows(C1).Item("mtrlocoid") & ", 945, " & ToDouble(LastHpp) & ", '" & CabangDDL.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                conmtroid += 1

                sSql = "update QL_mstoid set lastoid=" & (conmtroid - 1) & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            sSql = "SELECT Case When SUM(trnorderdtlqty)-(Select SUM(qty) from ql_trnsjjualdtl sj INNER JOIN ql_trnsjjualmst sm ON sm.trnsjjualmstoid=sj.trnsjjualmstoid AND sm.ordermstoid=o.trnordermstoid AND o.branch_code=sm.branch_code)=0 Then 'CLOSED' Else 'NO' End From ql_trnorderdtl o Where o.branch_code='" & CabangDDL.SelectedValue & "' AND o.trnordermstoid=" & ordermstoid.Text & " GROUP BY o.trnordermstoid, o.branch_code"
            xCmd.CommandText = sSql : Dim jumProsess As String = xCmd.ExecuteScalar

            If jumProsess = "CLOSED" Then
                sSql = "UPDATE QL_trnordermst SET trnorderstatus = 'Closed' WHERE ordermstoid = " & ordermstoid.Text & " And branch_code = '" & CabangDDL.SelectedValue & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(jualdtloid)) & " Where tablename = 'ql_trnjualdtl' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Dim orderdate As String = GetStrData(sSql)
            ' =============== QL_conar ================
            sSql = "INSERT INTO QL_conar (cmpcode, branch_code, conaroid,reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnarflag, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amttransidr, amttransusd, amtbayar, trnarnote, trnarres1, upduser, updtime) VALUES " & _
            "('" & CompnyCode & "', '" & CabangDDL.SelectedValue & "', " & vConARId & ", 'QL_trnjualmst', " & trnjualmstoid & ", 0, " & trncustoid.Text & ", '" & iAROid & "', 'POST', '', 'PIUTANG', '" & GetServerTime() & "', '" & GetDateToPeriodAcctg(GetServerTime()) & "', 0, '1/1/1900', '', 0, '" & IIf(ordermstoid.Text > 0, dPayDueDate, dPayDueDateOff) & "', " & ToDouble(amtjualnetto.Text) & ", " & ToDouble(amtjualnetto.Text) & "," & ToDouble(amtjualnetto.Text) & ", 0, 'SALES INVOICE (" & Tchar(custname.Text) & "| NO=" & trnjualno & ")', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & vConARId & " where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '=================Insert Auto Jurnal=========================
            Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
            Dim dvJurnal As DataView = dtJurnal.DefaultView

            Dim vSeqDtl As Integer = 1
            ' dvJurnal.RowFilter = "seq=1"
            ' JURNAL SALES INVOICE
            If dvJurnal.Count > 0 Then
                ' ============== QL_trnglmst
                ' Cust.[NAMA CUST] | Note.[ISI NOTE] | No. Nota.[NO SI]
                sSql = "INSERT into QL_trnglmst (cmpcode, glmstoid, branch_code, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime, type)" & _
                    "VALUES ('" & CompnyCode & "', " & vIDMst & ", '" & CabangDDL.SelectedValue & "', '" & GetServerTime() & "', '" & GetDateToPeriodAcctg(GetServerTime()) & "', 'Cust." & Tchar(custname.Text) & " | Note." & Tchar(note.Text) & " | No. Nota." & trnjualno & "', 'POST', '" & GetServerTime().Date & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'SI')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim dtj As String = dvJurnal.Count
                For C2 As Integer = 0 To dvJurnal.Count - 1
                    Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                    If dvJurnal(C2)("dbcr").ToString = "D" Then
                        dAmt = ToDouble(dvJurnal(C2)("debet").ToString) : sDBCR = "D"
                    Else
                        dAmt = ToDouble(dvJurnal(C2)("credit").ToString) : sDBCR = "C"
                    End If
                    ' ============== QL_trngldtl =============
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, upduser, updtime, glpostdate) " & _
                        "VALUES ('" & CompnyCode & "', '" & CabangDDL.SelectedValue & "', " & vIDDtl & ", " & vSeqDtl & ", " & vIDMst & ", " & dvJurnal(C2)("id").ToString & ", '" & sDBCR & "', " & dAmt & ", " & dAmt & ", " & dAmt & ", '" & trnjualno & "', 'Cust." & Tchar(custname.Text) & " | Note." & Tchar(note.Text) & " | No. Nota." & trnjualno & "', '', '" & trnjualmstoid & "', 'POST', '" & Tchar(Session("UserID")) & "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vSeqDtl += 1 : vIDDtl += 1
                Next
            End If
            dvJurnal.RowFilter = "" : vIDMst += 1 : vSeqDtl = 1

            'update lastoid ql_trnglmst
            sSql = "update QL_mstoid set lastoid=" & vIDMst & " Where tablename ='QL_trnglmst' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'update lastoid ql_trngldtl
            sSql = "update QL_mstoid set lastoid=" & vIDDtl - 1 & " Where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'recovery credit limit
            sSql = "UPDATE c set custcreditlimitusagerupiah= isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur)) climit FROM QL_trnjualmst WHERE trnjualstatus = 'POST' AND trnamtjualnetto - (accumpayment + amtretur) > 0 AND custoid = trncustoid GROUP BY trncustoid),0),custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur)) climit FROM QL_trnjualmst WHERE trnjualstatus = 'POST' AND trnamtjualnetto - (accumpayment + amtretur ) > 0 AND custoid = trncustoid GROUP BY trncustoid ),0) FROm QL_mstcust c"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '============================================================
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 1)
            posting.Text = "In Process" : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trndeliveryorder.aspx?awal=true")
    End Sub
End Class