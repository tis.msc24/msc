Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_incomingGiroReal
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        'Dim sErr As String = ""
        'If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 1 is invalid. " & sErr, 2)
        '    Return False
        'End If
        'If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 2 is invalid. " & sErr, 2)
        '    Return False
        'End If
        'If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
        '    showMessage("Period 2 must be more than Period 1 !", 2)
        '    Return False
        'End If
        If IsDate(toDate(FilterPeriod1.Text)) = False Or IsDate(toDate(FilterPeriod2.Text)) = False Then
            showMessage("Format periode salah !!", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text.Trim)) > CDate(toDate(FilterPeriod2.Text.Trim)) Then
            showMessage("Periode 2 tidak boleh lebih besar dari periode 1!!", 2)
            Return False
        End If

        If CheckYearIsValid(toDate(FilterPeriod1.Text)) Then
            showMessage("Tahun periode awal tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", 2)
            Return False
        End If
        If CheckYearIsValid(toDate(FilterPeriod2.Text)) Then
            showMessage("Tahun periode akhir tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", 2)
            Return False
        End If

        Return True
    End Function

    Private Function FillDDLWithALL(ByRef oDDLObject As DropDownList, ByVal sSql As String) As Boolean
        ' To fill data in dropdown list, return true when all data exist
        FillDDLWithALL = True
        oDDLObject.Items.Clear()
        oDDLObject.Items.Add("ALL")
        oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = "ALL"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            oDDLObject.Items.Add(xreader.GetValue(1))
            oDDLObject.Items.Item(oDDLObject.Items.Count - 1).Value = xreader.GetValue(0)
        End While
        xreader.Close()
        conn.Close()
        ' Cek if all data exits
        If oDDLObject.Items.Count = 0 Then
            FillDDLWithALL = False
        End If
        Return FillDDLWithALL
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Dim sWhere As String = "" : Dim rptName As String = ""
        Dim Dtl As String = "" : Dim Join As String = ""
        Dim cbNo As String = ""

        Try
            report.Load(Server.MapPath(folderReport & "rptGiroInRealisasi.rpt"))
            rptName = "GiroRealizationReport"

            If FilterPeriod1.Text <> "" And FilterPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    sWhere &= " WHERE cbref.cashbankgroup='GIRO IN' and cb.cashbankdate >= '" & Format(CDate(toDate(FilterPeriod1.Text)), "yyyy-MM-dd") & " 00:00:00' AND cb.cashbankdate <= '" & Format(CDate(toDate(FilterPeriod2.Text)), "yyyy-MM-dd") & " 23:59:59'"
                Else
                    Exit Sub
                End If
            End If

            If Session("branch_id") = "01" Then
                sWhere &= IIf(DDLBusUnit.SelectedValue = "ALL", "", " AND cb.branch_code = '" & DDLBusUnit.SelectedValue & "'")
            Else
                sWhere &= " AND cb.branch_code = '" & DDLBusUnit.SelectedValue & "'"
            End If
            cbNo = " WHERE " & DDLCashBankNo.SelectedValue & " LIKE '%" & Tchar(cashbankno.Text.Trim) & "%'"
            'If cashbankno.Text.Trim <> "" Then
            '    If DDLCashBankNo.SelectedValue = "Cash/Bank No." Then
            '        Dim sCashBankno() As String = Split(cashbankno.Text, ";")
            '        sWhere &= " AND ("
            '        For c1 As Integer = 0 To sCashBankno.Length - 1
            '            sWhere &= DDLCashBankNo.SelectedValue & " LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
            '            If c1 < sCashBankno.Length - 1 Then
            '                sWhere &= " OR"
            '            End If
            '        Next
            '        sWhere &= ")"
            '    Else
            '        Dim sCashBankno() As String = Split(cashbankno.Text, ";")
            '        sWhere &= " AND ("
            '        For c1 As Integer = 0 To sCashBankno.Length - 1
            '            sWhere &= " cbref.cashbankno LIKE '%" & Tchar(sCashBankno(c1)) & "%'"
            '            If c1 < sCashBankno.Length - 1 Then
            '                sWhere &= " OR"
            '            End If
            '        Next
            '        sWhere &= ")"
            '    End If
            'End If

            If cbCOA.Checked Then
                If ddlCOA.Items.Count > 0 Then
                    sWhere &= " AND cb.cashbankacctgoid = " & ddlCOA.SelectedValue
                End If
            End If
            report.SetParameterValue("cbNo", cbNo)
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", Session("UserID"))
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, rptName)
                report.Close()
                report.Dispose()
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, rptName)
                report.Close()
                report.Dispose()
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub InitAllDDL()
        ' DDL Business Unit / cabang
        sSql = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='MSC'"
        'End If
        FillDDL(DDLBusUnit, sSql)
        DDLBusUnit.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "01" Then
            DDLBusUnit.SelectedValue = Session("branch_id")
        Else
            DDLBusUnit.SelectedIndex = DDLBusUnit.Items.Count - 1
        End If
    End Sub

    Private Sub InitCOA()
        ' Fill DDL Negotiating Bank
        FillDDLAcctg(ddlCOA, "VAR_BANK", DDLBusUnit.SelectedValue)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\frmGiroInReal.aspx")

        End If

        'If checkPagePermission("~\ReportForm\frmGiroInReal.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        Page.Title = CompnyName & " - Incoming Giro Realization Report"
        If Not Page.IsPostBack Then
            InitAllDDL()
            DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitCOA()
    End Sub

    Protected Sub DDLCashBankNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLCashBankNo.SelectedIndexChanged
        cashbankno.Text = ""
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        ShowReport("View")
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        ShowReport("Print PDF")
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        ShowReport("Print Excel")
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\frmGiroInReal.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not report Is Nothing Then
            If report.IsLoaded Then
                report.Dispose()
                report.Close()
            End If
        End If
    End Sub
#End Region

End Class
