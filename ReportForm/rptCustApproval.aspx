<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptCustApproval.aspx.vb" Inherits="rptCustApproval" Title="MSC - Laporan Customer Approval" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Lap. Cust. Approval"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: white" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD id="tdPeriod1" vAlign=top align=left runat="server" visible="true"><asp:Label id="Tanggal" runat="server" Text="Tanggal"></asp:Label> </TD><TD id="Td1" vAlign=top align=left runat="server" visible="true">:</TD><TD id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="txtPeriod1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to&nbsp;<asp:TextBox id="txtPeriod2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)"></asp:Label> </TD></TR><TR><TD id="Td2" vAlign=top align=left runat="server" visible="true">Cabang</TD><TD id="Td3" vAlign=top align=left runat="server" visible="true">:</TD><TD id="Td4" align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="dd_branch" runat="server" Width="185px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left runat="server" visible="true">Type</TD><TD vAlign=top align=left runat="server" visible="true">:</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="custapprovaltype" runat="server" CssClass="inpText" __designer:wfdid="w61"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="CL AWAL">CL AWAL</asp:ListItem>
<asp:ListItem Value="CL TEMPORARY">CL TEMPORARY</asp:ListItem>
<asp:ListItem>TERMIN</asp:ListItem>
<asp:ListItem Value="OVERDUE">OVER DUE</asp:ListItem>
<asp:ListItem Value="REVISI DATA">REVISI DATA</asp:ListItem>
<asp:ListItem></asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD6" align=left runat="server" visible="true"><asp:Label id="Label3" runat="server" Width="91px" Text="Customer"></asp:Label></TD><TD id="TD8" align=left runat="server" visible="true">:</TD><TD id="TD9" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="CustName" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sCust" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eCust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD id="TD7" align=left runat="server" visible="true"></TD><TD id="TD10" align=left runat="server" visible="true"></TD><TD id="TD5" align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvCust" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrOid" runat="server" AutoPostBack="true" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrOid_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="SelectOid" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Cust. code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Cabang" HeaderText="Cabang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" visible="true"><asp:Label id="LblSi" runat="server" Width="91px" Text="No. Transaksi"></asp:Label></TD><TD align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Width="2px" Text=":"></asp:Label></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="TxtNoTrans" runat="server" Width="250px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="sBtnNoTrans" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom"></asp:ImageButton>&nbsp;<asp:ImageButton id="eBtnNoTrans" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true"></TD><TD align=left runat="server" visible="true"></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:GridView id="GvNotrans" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" UseAccessibleHeader="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="CbHdrNo" runat="server" AutoPostBack="true" Checked="<%# GetSessionCheckNota() %>" OnCheckedChanged="CbHdrNo_CheckedChanged"></asp:CheckBox> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="CekSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("JualOid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:BoundField DataField="notrans" HeaderText="No. Transaksi">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trncustname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnDate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Black"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left runat="server" visible="true">Status</TD><TD align=left runat="server" visible="true">:</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="StatusNya" runat="server" Width="119px" CssClass="inpText" __designer:wfdid="w62"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>IN APPROVAL</asp:ListItem>
<asp:ListItem>APPROVED</asp:ListItem>
<asp:ListItem>REJECTED</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibpdf" onclick="ibpdf_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
<ProgressTemplate>
<div id="progressBackgroundFilter" class="progressBackgroundFilter">
</div>
<div id="processMessage" class="processMessage">
<span style="font-weight: bold; font-size: 10pt; color: purple">
<asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" /><br />
Please Wait .....</span><br />
</div>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" TargetControlID="txtPeriod1" Enabled="True" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureThousandsPlaceholder="" CultureDecimalPlaceholder="" CultureTimePlaceholder="" CultureDatePlaceholder="" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" TargetControlID="txtPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEtxtPeriod2" runat="server" TargetControlID="txtPeriod2" Enabled="True" PopupButtonID="ibPeriod2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CEFilterPeriod1" runat="server" TargetControlID="txtPeriod1" Enabled="True" PopupButtonID="ibPeriod1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

