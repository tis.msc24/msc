<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnReqRetRaw.aspx.vb" Inherits="Transaction_RawMaterialRequestReturn" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="SaddleBrown" Text=".: Material Request Return" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px"></td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Material Request Return :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w30"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w31"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDL" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem Value="reqm.retrawmstoid">Draft No.</asp:ListItem>
<asp:ListItem Value="reqm.retrawno">Request No.</asp:ListItem>
<asp:ListItem Value="wom.wono">SPK No.</asp:ListItem>
<asp:ListItem Value="reqm.retrawmstnote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w34"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=3><asp:TextBox id="FilterPeriod1" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w4"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w6"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w7"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w9"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w41"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=3><asp:DropDownList id="FilterDDLStatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w10">
                                                            <asp:ListItem>In Process</asp:ListItem>
                                                            <asp:ListItem>Post</asp:ListItem>
                                                            <asp:ListItem>Closed</asp:ListItem>
                                                            <asp:ListItem>Cancel</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD align=left colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w11" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w12" TargetControlID="FilterPeriod1" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w13" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w14" TargetControlID="FilterPeriod2" UserDateFormat="DayMonthYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w17"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w18"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=5><asp:LinkButton id="lkbReqInProcess" runat="server" __designer:wfdid="w19"></asp:LinkButton></TD></TR><TR><TD align=center colSpan=5></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="gvTRN" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w20" AllowSorting="True" AllowPaging="True" PageSize="8" CellPadding="4" DataKeyNames="retrawmstoid" AutoGenerateColumns="False" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="reqrawmstoid" DataNavigateUrlFormatString="~\Transaction\trnReqRetRaw.aspx?oid={0}" DataTextField="reqrawmstoid" HeaderText="Draft No." SortExpression="reqrawmstoid">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="reqrawno" HeaderText="Req Return No." SortExpression="reqrawno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqrawdate" HeaderText="Req Return Date" SortExpression="reqrawdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wono" HeaderText="SPK No." SortExpression="wono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqrawmststatus" HeaderText="Status" SortExpression="reqrawmststatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqrawmstnote" HeaderText="Note" SortExpression="reqrawmstnote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" __designer:wfdid="w24" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("retrawmstoid") %>' __designer:wfdid="w25" Visible="False"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Blue"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Gainsboro" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w21"></asp:Label></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w22" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w23"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Material Request Header" Font-Underline="False" __designer:wfdid="w97"></asp:Label></TD></TR><TR><TD style="WIDTH: 87px" id="TD9" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Width="83px" Text="Business Unit" __designer:wfdid="w103" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w98"></asp:Label></TD><TD id="TD7" class="Label" align=center runat="server" Visible="true"></TD><TD id="TD8" class="Label" align=left runat="server" Visible="true"><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" __designer:wfdid="w104" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="womstoid" runat="server" __designer:wfdid="w101" Visible="False"></asp:Label> <asp:Label id="periodacctg" runat="server" __designer:wfdid="w99" Visible="False"></asp:Label> </TD><TD class="Label" align=left><asp:Label id="woflag" runat="server" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="lblsprno" runat="server" Text="Draft No." __designer:wfdid="w106"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="BranchCode" runat="server" __designer:wfdid="w107" Visible="False"></asp:Label> <asp:Label id="ToBranch" runat="server" __designer:wfdid="w107" Visible="False"></asp:Label> <asp:Label id="retmstoid" runat="server" __designer:wfdid="w107"></asp:Label> <asp:TextBox id="reqno" runat="server" Width="260px" CssClass="inpTextDisabled" __designer:wfdid="w108" Visible="False" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Return Date" __designer:wfdid="w109"></asp:Label> <asp:Label id="Label26" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w110"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqdate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w111" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="imbPRDate" runat="server" Width="16px" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w112"></asp:ImageButton> <asp:Label id="Label7" runat="server" Height="16px" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w113"></asp:Label></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="Label10" runat="server" Text="SPK No." __designer:wfdid="w114"></asp:Label> <asp:Label id="Label15" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w115" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="wono" runat="server" Width="260px" CssClass="inpTextDisabled" __designer:wfdid="w116" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchKIK" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w57"></asp:ImageButton> <asp:ImageButton id="btnClearKIK" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w118"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Proses" __designer:wfdid="w119"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="deptoid" runat="server" Width="305px" CssClass="inpTextDisabled" __designer:wfdid="w120" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w126"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqmstnote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w127" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Header Status" __designer:wfdid="w123"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqmststatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w124" Enabled="False"></asp:TextBox> <asp:Label id="seqprocess" runat="server" __designer:wfdid="w125" Visible="False"></asp:Label> <asp:Label id="flagrequest" runat="server" __designer:wfdid="w128" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w129" TargetControlID="reqdate" PopupButtonID="imbPRDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w130" TargetControlID="reqdate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label12" runat="server" Text="Gudang" __designer:wfdid="w121" Visible="False"></asp:Label> <asp:DropDownList id="reqwhoid" runat="server" Width="305px" CssClass="inpTextDisabled" __designer:wfdid="w122" Visible="False" Enabled="False"></asp:DropDownList> <asp:Label id="LblGd" runat="server" Text="Gudang Tujuan" __designer:wfdid="w123" Visible="False"></asp:Label> <asp:DropDownList id="ToMtrlocOid" runat="server" Width="305px" CssClass="inpTextDisabled" __designer:wfdid="w58" Visible="False" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 6px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Material Request Detail" Font-Underline="False" __designer:wfdid="w131"></asp:Label></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w132" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="reqdtlseq" runat="server" __designer:wfdid="w133" Visible="False">1</asp:Label>&nbsp;<asp:Label id="reqmstoid" runat="server" __designer:wfdid="w107" Visible="False"></asp:Label> <asp:Label id="reqdtloid" runat="server" __designer:wfdid="w134" Visible="False"></asp:Label> <asp:Label id="matoid" runat="server" __designer:wfdid="w135" Visible="False"></asp:Label> <asp:Label id="matcode" runat="server" __designer:wfdid="w136" Visible="False"></asp:Label>&nbsp; <asp:Label id="retdtloid" runat="server" __designer:wfdid="w134" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="refoid" runat="server" __designer:wfdid="w137" Visible="False"></asp:Label> <asp:Label id="reftype" runat="server" __designer:wfdid="w138" Visible="False"></asp:Label> <asp:Label id="tolerance" runat="server" __designer:wfdid="w139" Visible="False"></asp:Label> <asp:Label id="wodtl3qty" runat="server" __designer:wfdid="w140" Visible="False"></asp:Label> <asp:Label id="wodtl2oid" runat="server" __designer:wfdid="w67" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="Label11" runat="server" Text="Material" __designer:wfdid="w143"></asp:Label> <asp:Label id="Label20" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w144"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matlongdesc" runat="server" Width="280px" CssClass="inpTextDisabled" __designer:wfdid="w145" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchMat" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w56"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Kode Material" __designer:wfdid="w147"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="matcodedtl1" runat="server" Width="280px" CssClass="inpTextDisabled" __designer:wfdid="w148" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="Label22" runat="server" Text="Return Qty" __designer:wfdid="w2"></asp:Label> <asp:Label id="Label21" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqqtybiji" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w1" MaxLength="16"></asp:TextBox>&nbsp;- <asp:DropDownList id="DDLunitRet" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w2" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Width="103px" Text="Max Qty Return" __designer:wfdid="w167"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqmaxqtybiji" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w169" Enabled="False"></asp:TextBox>&nbsp;- &nbsp;<asp:DropDownList id="DDLunit1" runat="server" Width="86px" CssClass="inpTextDisabled" __designer:wfdid="w160" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 87px" class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w165"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="reqdtlnote" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w166" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 87px" id="TD15" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label25" runat="server" Width="117px" Text="Kode Baru Material" __designer:wfdid="w59"></asp:Label></TD><TD id="TD13" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD14" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="matoldcodedtl1" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w154" Enabled="False"></asp:TextBox> <asp:TextBox id="ReqMaxWeight" runat="server" Width="50px" CssClass="inpTextDisabled" __designer:wfdid="w159" Visible="False" Enabled="False"></asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" Text="Qty Stock" __designer:wfdid="w158" Visible="False"></asp:Label> <asp:DropDownList id="requnitbijioid" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w157" Enabled="False"></asp:DropDownList></TD><TD id="TD12" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label16" runat="server" Text="Berat" __designer:wfdid="w149" Visible="False"></asp:Label></TD><TD id="TD10" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD11" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="reqqty" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w151" Visible="False" MaxLength="16"></asp:TextBox> <asp:DropDownList id="requnitoid" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w152" Visible="False" Enabled="False"></asp:DropDownList> <asp:DropDownList id="DDLFG" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w142" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDLFG_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w172" TargetControlID="reqqty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:Label id="reqfgoid" runat="server" __designer:wfdid="w174" Visible="False"></asp:Label> <asp:Label id="reqfgdesc" runat="server" __designer:wfdid="w175" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w176"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvReqDtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w178" PageSize="5" CellPadding="4" DataKeyNames="retdtlseq" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="retdtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="retqty" HeaderText="Return Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" HeaderText="Max. Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="retunit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="retunitoid" HeaderText="unitoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="retnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="GhostWhite" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="Gainsboro" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!" __designer:wfdid="w2"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w179"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w180"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w181"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w182"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w183" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w184" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w185" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w186" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w187" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Imageloading" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w188"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Material Request Return :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upListKIK" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListKIK" runat="server" Width="550px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListKIK" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Surat Perintah Kerja" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFindListKIK" runat="server" Width="100%" CssClass="Label" DefaultButton="btnFindListKIK">Filter : <asp:DropDownList id="FilterDDLListKIK" runat="server" Width="100px" CssClass="inpText">
                                        <asp:ListItem Value="wom.wono">SPK No.</asp:ListItem>
                                    </asp:DropDownList> <asp:TextBox id="FilterTextListKIK" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListKIK" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListKIK" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListKIK" runat="server" Width="99%" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="womstoid,wono,wodate,woflag,branch_code,ToBranch" CellPadding="4" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#F7F7DE" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="wono" HeaderText="SPK No.">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="wodate" HeaderText="SPK Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Gainsboro" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>

                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#EFF3FB" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="Blue" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbCloseListKIK" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListKIK" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListKIK" runat="server" TargetControlID="btnHideListKIK" PopupDragHandleControlID="lblListKIK" BackgroundCssClass="modalBackground" PopupControlID="pnlListKIK">
            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListMat2" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat2" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Material" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat2" runat="server" Width="100%" DefaultButton="btnFindListMat2">Filter : <asp:DropDownList id="FilterDDLListMat2" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemcode">Code</asp:ListItem>
<asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="unitdesc">Unit</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat2" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat2" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat2" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat2" runat="server" Width="99%" ForeColor="#333333" PageSize="7" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#F7F7DE" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
                                                <asp:CheckBox ID="cbHdrListMat" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrListMat_CheckedChanged" />

                                            
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbListMat2" runat="server" __designer:wfdid="w1" ToolTip='<%# eval("dtloid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="reqno" HeaderText="Request No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Return Qty"><ItemTemplate>
<asp:TextBox id="tbBOMQtybiji" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("retqty") %>' __designer:wfdid="w4" MaxLength="10"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="ftbQtybiji" runat="server" __designer:wfdid="w5" TargetControlID="tbBOMQtybiji" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="reqqty" HeaderText="Max Qty">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unitdesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Note"><ItemTemplate>
<asp:TextBox id="tbBOMNote" runat="server" Width="150px" CssClass="inpText" Text='<%# eval("note") %>' __designer:wfdid="w2" MaxLength="100"></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Gainsboro" CssClass="gvhdr" Font-Bold="True" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
                                        <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>

                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=3><asp:LinkButton id="lkbAddToList" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListMat2" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat2" runat="server" TargetControlID="btnHideListMat2" PopupControlID="pnlListMat2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat2" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat2" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

