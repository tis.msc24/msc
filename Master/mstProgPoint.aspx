<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstProgPoint.aspx.vb" Inherits="Master_mstProgPoint" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: Master Program Point"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="width: 56px">
                                                Filter</td>
                                            <td align="left">:</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText" Font-Size="8pt">
                                                    <asp:ListItem Value="progpointmstoid">No Draft .</asp:ListItem>
                                                    <asp:ListItem Value="progpointcode">Code</asp:ListItem>
                                                    <asp:ListItem Value="progpointdesc"> Prog Point Name</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText" Font-Size="8pt"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Status</td>
                                            <td align="left">:</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inpText" Font-Size="8pt">
                                                    <asp:ListItem>ALL</asp:ListItem>
                                                    <asp:ListItem Value="Active">ACTIVE</asp:ListItem>
                                                    <asp:ListItem Value="Inactive">INACTIVE</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Middle" />
                                                <asp:ImageButton ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Middle" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="6" style="height: 10px">
                                                <asp:GridView ID="GVMst" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                                    Width="98%" ForeColor="#333333" GridLines="None" Height="63px" AllowPaging="True" PageSize="5">
                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                    <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <Columns>
                                                        <asp:HyperLinkField DataNavigateUrlFields="progpointmstoid" DataNavigateUrlFormatString="mstProgPoint.aspx?oid={0}"
                                                            DataTextField="progpointmstoid" HeaderText="No Draft.">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" />
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="progpointcode" HeaderText="Code">
                                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="progpointdesc" HeaderText="Prog Point Name">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="progpointtype" HeaderText="Type">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="progpointstartdate" HeaderText="Start Date">
                                                            <HeaderStyle CssClass="gvhdr" Font-Bold="False" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="progpointfinishdate" HeaderText="End Date">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="progpointflag" HeaderText="Status">
                                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imbPrintFromList" runat="server" CommandArgument='<%# Eval("progpointmstoid") %>'
                                                                    ImageAlign="AbsMiddle" ImageUrl="~/Images/print.gif" OnClick="imbPrintFromList_Click"
                                                                    ToolTip='<%# Eval("progpointmstoid") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>&nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List Master Program Point :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=7><asp:Label id="Label48" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Master Program Point" Font-Underline="False" __designer:wfdid="w142"></asp:Label> <asp:Label id="lblamp" runat="server" __designer:wfdid="w143" Visible="False"></asp:Label> <asp:Label id="promoid" runat="server" __designer:wfdid="w144" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Code&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w145"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="promeventcode" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w146" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Button id="btnAMP" onclick="btnSales_Click" runat="server" CssClass="inpText" Text="Get Data AMP" __designer:wfdid="w147" Visible="False" BorderColor="Maroon" BorderWidth="1px"></asp:Button></TD><TD class="Label" align=left>Program Name&nbsp;<asp:Label id="Label55" runat="server" CssClass="Important" Text="*" __designer:wfdid="w148"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="promeventname" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w149" MaxLength="50"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Start Date&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w150"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="promstartdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w151" MaxLength="6"></asp:TextBox>&nbsp;<asp:ImageButton id="ibstartdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w152">
                                                        </asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w153"></asp:Label><ajaxToolkit:CalendarExtender id="ceStartDate" runat="server" __designer:wfdid="w154" TargetControlID="promstartdate" PopupButtonID="ibstartdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEpromstartdate" runat="server" __designer:wfdid="w155" Enabled="true" TargetControlID="promstartdate" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left>End Date&nbsp;<asp:Label id="Label57" runat="server" CssClass="Important" Text="*" __designer:wfdid="w156"></asp:Label></TD><TD class="Label" align=left colSpan=3><asp:TextBox id="promfinishdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w157"></asp:TextBox>&nbsp;<asp:ImageButton id="ibfinishdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w158">
                                                        </asp:ImageButton>&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w159"></asp:Label><ajaxToolkit:CalendarExtender id="ceFinishDate" runat="server" __designer:wfdid="w160" TargetControlID="promfinishdate" PopupButtonID="ibfinishdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEpromfinishdate" runat="server" __designer:wfdid="w161" Enabled="true" TargetControlID="promfinishdate" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 90px" id="TD4" class="Label" align=left runat="server" Visible="false">Start Time&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w162"></asp:Label></TD><TD id="TD5" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="promstarttime" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w163" MaxLength="5"></asp:TextBox> <ajaxToolkit:MaskedEditExtender id="MEpromstarttime" runat="server" __designer:wfdid="w164" Enabled="true" TargetControlID="promstarttime" CultureName="en-US" Mask="99:99:99" MaskType="Time" UserTimeFormat="TwentyFourHour" AcceptAMPM="True"></ajaxToolkit:MaskedEditExtender> </TD><TD id="TD2" class="Label" align=left runat="server" Visible="false">End Time&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w165"></asp:Label></TD><TD id="TD1" class="Label" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="promfinishtime" runat="server" Width="58px" CssClass="inpText" __designer:wfdid="w166" MaxLength="5"></asp:TextBox><ajaxToolkit:MaskedEditExtender id="MEpromfinishtime" runat="server" __designer:wfdid="w167" TargetControlID="promfinishtime" Mask="99:99:99" MaskType="Time" UserTimeFormat="TwentyFourHour" AcceptAMPM="True"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="promtype" runat="server" CssClass="inpText" __designer:wfdid="w168" AutoPostBack="True" OnSelectedIndexChanged="promtype_SelectedIndexChanged"><asp:ListItem>Customer</asp:ListItem>
<asp:ListItem>Sales</asp:ListItem>
<asp:ListItem>Cabang</asp:ListItem>
</asp:DropDownList> </TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="status" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w169" Enabled="False"></asp:TextBox>&nbsp;<asp:DropDownList id="membercardrole" runat="server" CssClass="inpText" __designer:wfdid="w170" Visible="False"><asp:ListItem>None</asp:ListItem>
<asp:ListItem>Plus</asp:ListItem>
<asp:ListItem>Not Define</asp:ListItem>
<asp:ListItem>Choose</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 90px" id="TD8" class="Label" align=left runat="server" Visible="false">Opsi Piutang</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false">:</TD><TD id="TD7" class="Label" align=left colSpan=5 runat="server" Visible="false"><asp:CheckBox id="cbPiutangLunas" runat="server" BackColor="#FFFFC0" Text="Piutang Lunas" __designer:wfdid="w171" BorderColor="Black"></asp:CheckBox> <asp:CheckBox id="cbPiutangOntime" runat="server" BackColor="#FFFFC0" Text="Piutang On Time" __designer:wfdid="w172" BorderColor="Black"></asp:CheckBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:TextBox id="note" runat="server" Width="400px" CssClass="inpText" __designer:wfdid="w173" MaxLength="150"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=5 rowSpan=1><asp:CheckBox id="cbCheckAll" runat="server" BackColor="#FFFFC0" Text="CHECK ALL" __designer:wfdid="w174" BorderColor="Black" AutoPostBack="True"></asp:CheckBox> <DIV style="OVERFLOW-Y: scroll; WIDTH: 65%; HEIGHT: 143px; BACKGROUND-COLOR: beige"><asp:CheckBoxList id="cbCabang" runat="server" Width="540px" BackColor="#FFFFC0" __designer:wfdid="w175" BorderColor="Black" CellPadding="5" RepeatDirection="Horizontal" RepeatColumns="4"></asp:CheckBoxList></DIV></TD></TR><TR><TD style="WIDTH: 90px; HEIGHT: 32px" class="Label" align=left>Katalog</TD><TD style="HEIGHT: 32px" class="Label" align=left>:</TD><TD style="HEIGHT: 32px" class="Label" align=left colSpan=5 rowSpan=1><asp:TextBox id="itemdesc1" runat="server" Width="255px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w176" Enabled="False" TextMode="MultiLine" Rows="2"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMenu1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w177"></asp:ImageButton> <asp:ImageButton id="imbClearMenu1" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w178"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=right colSpan=5 rowSpan=1><asp:Button id="btnGenerate" runat="server" CssClass="btn green" Font-Bold="True" ForeColor="White" Text="Generate" __designer:wfdid="w179"></asp:Button> <asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" ImageUrl="~/Images/Clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w180" Visible="False" AlternateText="Add to List"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=right colSpan=7 rowSpan=1><asp:GridView id="gvList" runat="server" Width="100%" ForeColor="Red" __designer:wfdid="w59" CellPadding="4" PageSize="8" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Data" DataKeyNames="itemoid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="GBB" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb01" runat="server" __designer:wfdid="w8" TargetControlID="cb01" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <asp:TextBox id="cb01" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point01") %>' __designer:wfdid="w9" MaxLength="10" ToolTip='<%# "01" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="MYTECH" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb02" runat="server" __designer:wfdid="w12" TargetControlID="cb02" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb02" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point02") %>' __designer:wfdid="w10" MaxLength="10" ToolTip='<%# "02" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="MODERN" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb03" runat="server" __designer:wfdid="w10" TargetControlID="cb03" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb03" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point03") %>' __designer:wfdid="w9" MaxLength="10" ToolTip='<%# "03" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="INDOCOM" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb04" runat="server" __designer:wfdid="w16" TargetControlID="cb04" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb04" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point04") %>' __designer:wfdid="w14" MaxLength="10" ToolTip='<%# "04" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="AURORA" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb05" runat="server" __designer:wfdid="w18" TargetControlID="cb05" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb05" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point05") %>' __designer:wfdid="w16" MaxLength="10" ToolTip='<%# "05" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="BIOTECH" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb06" runat="server" __designer:wfdid="w20" TargetControlID="cb06" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb06" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point06") %>' __designer:wfdid="w18" MaxLength="10" ToolTip='<%# "06" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="LION" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb07" runat="server" __designer:wfdid="w22" TargetControlID="cb07" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb07" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point07") %>' __designer:wfdid="w20" MaxLength="10" ToolTip='<%# "07" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="VISIONTECH" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb08" runat="server" __designer:wfdid="w24" TargetControlID="cb08" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb08" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point08") %>' __designer:wfdid="w22" MaxLength="10" ToolTip='<%# "08" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="NASIONAL" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb09" runat="server" __designer:wfdid="w26" TargetControlID="cb09" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb09" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point09") %>' __designer:wfdid="w24" MaxLength="10" ToolTip='<%# "09" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="G24" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb10" runat="server" __designer:wfdid="w30" TargetControlID="cb10" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb10" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point10") %>' __designer:wfdid="w29" MaxLength="10" ToolTip='<%# "10" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="SERVIS" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb11" runat="server" __designer:wfdid="w32" TargetControlID="cb11" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb11" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point11") %>' __designer:wfdid="w32" MaxLength="10" ToolTip='<%# "11" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="MULTI MALL" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb13" runat="server" __designer:wfdid="w38" TargetControlID="cb13" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb13" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point13") %>' __designer:wfdid="w37" MaxLength="10" ToolTip='<%# "13" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="MULTI SARANA, CV" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb14" runat="server" __designer:wfdid="w5" TargetControlID="cb14" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb14" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point14") %>' __designer:wfdid="w6" MaxLength="10" ToolTip='<%# "14" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="MSC YOGYA" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb15" runat="server" __designer:wfdid="w3" TargetControlID="cb15" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb15" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point15") %>' __designer:wfdid="w4" MaxLength="10" ToolTip='<%# "15" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="INFORCE" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb16" runat="server" __designer:wfdid="w1" TargetControlID="cb16" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb16" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point16") %>' __designer:wfdid="w2" MaxLength="10" ToolTip='<%# "16" %>'></asp:TextBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="SERVICE BLOK I NO 4" Visible="False"><ItemTemplate>
<ajaxToolkit:FilteredTextBoxExtender id="ftbCb17" runat="server" __designer:wfdid="w3" TargetControlID="cb17" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><asp:TextBox id="cb17" runat="server" Width="30px" CssClass="inpText" Text='<%# eval("point16") %>' __designer:wfdid="w4" MaxLength="10" ToolTip='<%# "16" %>'></asp:TextBox>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg3" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w98"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE><asp:Label id="Detail" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Detail Point" Font-Underline="True" __designer:wfdid="w182"></asp:Label><BR /><TABLE id="tblMenu" width="100%" runat="server" visible="false"><TBODY><TR><TD class="Label" colSpan=10><asp:Label id="I_u2" runat="server" ForeColor="Red" Text="New Detail" __designer:wfdid="w183" Visible="False"></asp:Label><asp:Label id="lblmenuygada" runat="server" __designer:wfdid="w184"></asp:Label><asp:Label id="TypeBarang" runat="server" __designer:wfdid="w185" Visible="False"></asp:Label><asp:Label id="seq" runat="server" __designer:wfdid="w186" Visible="False"></asp:Label><asp:Label id="itemoid2" runat="server" __designer:wfdid="w187" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="*" __designer:wfdid="w188" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=right colSpan=6><asp:ImageButton id="BtnAddList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w189" AlternateText="Add to List"></asp:ImageButton> <asp:ImageButton id="btnClearDtl" runat="server" ImageUrl="~/Images/Clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w190" Visible="False" AlternateText="Add to List"></asp:ImageButton> </TD></TR><TR><TD class="Label" align=center colSpan=10><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 178px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPointDtl" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w191" CellPadding="4" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No Data" DataKeyNames="itemoid">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point01" HeaderText="GBB" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point02" HeaderText="MYTECH" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point03" HeaderText="MODERN" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point04" HeaderText="INDOCOM" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point05" HeaderText="AURORA" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point06" HeaderText="BIOTECH" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point07" HeaderText="LION" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point08" HeaderText="VISIONTECH" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point09" HeaderText="NASIONAL" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point10" HeaderText="G24" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point11" HeaderText="SERVIS" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point13" HeaderText="MULTI MALL" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point14" HeaderText="MULTI SARANA, CV" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point15" HeaderText="MSC YOGYA" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point16" HeaderText="INFORCE" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True" Width="2px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data Detail katalog belum di input..!!" __designer:wfdid="w7"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=left colSpan=10><asp:Label id="Label11" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="#001050" Text="Detail Reward" Font-Underline="True" __designer:wfdid="w192"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=10><asp:Label id="seq2" runat="server" __designer:wfdid="w193" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left colSpan=1>:</TD><TD class="Label" align=left><asp:DropDownList id="ddlTypeBarang" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w194" AutoPostBack="True"><asp:ListItem>BARANG</asp:ListItem>
<asp:ListItem>NON BARANG</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=2></TD></TR><TR><TD class="Label" align=left>Katalog</TD><TD class="Label" align=left colSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="itemdesc" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w195"></asp:TextBox> <asp:ImageButton id="imbFindItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w196"></asp:ImageButton> <asp:ImageButton id="imbClearItem" onclick="imbClearItem_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w197"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="itemoid" runat="server" __designer:wfdid="w198" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=2></TD></TR><TR><TD class="Label" align=left>Qty Reward</TD><TD class="Label" align=left colSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="qty" runat="server" CssClass="inpText" __designer:wfdid="w199" MaxLength="9" AutoPostBack="True"></asp:TextBox></TD><TD class="Label" align=left>Target Point</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="point" runat="server" CssClass="inpText" __designer:wfdid="w200" MaxLength="9" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Amount Reward</TD><TD class="Label" align=left colSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="amount" runat="server" CssClass="inpText" __designer:wfdid="w201" MaxLength="10" AutoPostBack="True"></asp:TextBox></TD><TD class="Label" align=left>Target Amt</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="targetamount" runat="server" CssClass="inpText" __designer:wfdid="w202" MaxLength="10" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=10><ajaxToolkit:FilteredTextBoxExtender id="ftbItem" runat="server" __designer:wfdid="w203" TargetControlID="point" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbTargetAmt" runat="server" __designer:wfdid="w204" TargetControlID="targetamount" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbQty" runat="server" __designer:wfdid="w205" TargetControlID="qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w206" TargetControlID="amount" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=right colSpan=10><asp:ImageButton id="btnAddListRwd" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w207" AlternateText="Add to List"></asp:ImageButton><asp:ImageButton id="btnClearRwd" runat="server" ImageUrl="~/Images/Clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w208" Visible="False" AlternateText="Add to List"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=10><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 178px; BACKGROUND-COLOR: beige"><asp:GridView id="gvReward" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w209" CellPadding="4" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No Data" DataKeyNames="seq,itemoid,itemdesc,qty,amount,point,targetamount">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="point" HeaderText="Target Point">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="targetamount" HeaderText="Target Amount">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typebarang" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle Font-Bold="True" Width="2px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg3" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada data detail Reward." __designer:wfdid="w98"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=left colSpan=10><asp:Label id="Label10" runat="server" Font-Size="Large" Font-Bold="True" Text="Total Amount : Rp. " __designer:wfdid="w210" Visible="False"></asp:Label> <asp:Label id="totKotor" runat="server" Font-Size="Large" Font-Bold="True" Text="0.0" __designer:wfdid="w211" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=10>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w212"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w213"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=10><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w214" CommandName="Update"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w215" CommandName="Cancel"></asp:ImageButton> <asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w216" CommandName="Delete"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=10><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w217" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w218"></asp:Image><BR />LOADING....</SPAN><BR /></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;&nbsp; <ajaxToolkit:FilteredTextBoxExtender id="FTEpersubgroupdisc" runat="server" __designer:wfdid="w219" TargetControlID="persubgroupdisc" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEpergroupdisc" runat="server" __designer:wfdid="w220" TargetControlID="pergroupdisc" FilterType="Numbers">
                                                    </ajaxToolkit:FilteredTextBoxExtender>&nbsp;&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=10><TABLE style="WIDTH: 100%" id="tblGroup" runat="server"><TBODY><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label9" runat="server" Text="Per : " __designer:wfdid="w221" font-size="X-Small"></asp:Label> <asp:DropDownList id="ddlInsertType" runat="server" CssClass="inpText" __designer:wfdid="w222" AutoPostBack="true"><asp:ListItem>Item</asp:ListItem>
<asp:ListItem Value="GROUP">Group Item</asp:ListItem>
<asp:ListItem Value="SUBGROUP">Sub Group Item</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Per&nbsp;Group</TD><TD class="Label" align=left><asp:DropDownList id="ddlGroup" runat="server" CssClass="inpText" __designer:wfdid="w223">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=100>Discount</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="pergroupdisc" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w224"></asp:TextBox> (%)</TD></TR></TBODY></TABLE><TABLE style="WIDTH: 100%" id="tblSubGroup" runat="server" visible="false"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Width="105px" Text="Per Sub Group" __designer:wfdid="w225"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="ddlSubGroup" runat="server" CssClass="inpText" __designer:wfdid="w226">
                                                    </asp:DropDownList></TD><TD class="Label" align=left width=100>Discount</TD><TD class="Label" align=left colSpan=3><asp:TextBox id="persubgroupdisc" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w227"></asp:TextBox> (%)</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
<asp:Panel id="PanelUser" runat="server" Width="600px" CssClass="modalBox" __designer:wfdid="w229" Visible="False" DefaultButton="btnSearchItem"><TABLE width="100%"><TBODY><TR><TD style="TEXT-ALIGN: center"><asp:Label id="lblListUser" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Katalog" __designer:wfdid="w230"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center">Item&nbsp;: <asp:TextBox id="filterMenuItem" runat="server" CssClass="inpText" __designer:wfdid="w231"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" onclick="btnSearchItem_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w232">
                                                                    </asp:ImageButton> <asp:ImageButton id="btnListItem" onclick="btnListItem_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w233"></asp:ImageButton> <asp:Label id="lblstate" runat="server" __designer:wfdid="w234" Visible="False"></asp:Label></TD></TR><TR><TD><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 300px; BACKGROUND-COLOR: beige; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"><asp:GridView id="gvMenu" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w235" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="itemoid,itemshortdesc,pricelist" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemshortdesc" HeaderText="Katalog" SortExpression="itemshortdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center"><asp:LinkButton id="lbCloseMenu" runat="server" __designer:wfdid="w236">[Close]</asp:LinkButton></TD></TR><TR><TD style="TEXT-ALIGN: center">&nbsp;</TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MpeUser" runat="server" __designer:wfdid="w237" TargetControlID="BtnUser" PopupControlID="PanelUser" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListUser" Drag="True">
                                                </ajaxToolkit:ModalPopupExtender> <asp:Button id="BtnUser" runat="server" __designer:wfdid="w238" Visible="False"></asp:Button> 
</ContentTemplate>
                                        </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanelPerson" runat="server"><contenttemplate>
<asp:Panel id="PanelPerson" runat="server" Width="700px" CssClass="modalBox" __designer:wfdid="w240" Visible="False" DefaultButton="btnFindPerson"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabePerson" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of AMP" __designer:wfdid="w241"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLFilterPerson" runat="server" CssClass="inpText" __designer:wfdid="w242"><asp:ListItem Value="personname">Person Name</asp:ListItem>
<asp:ListItem Value="personnip">N.I.P</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterPerson" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w243"></asp:TextBox> <asp:ImageButton id="btnFindPerson" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w244"></asp:ImageButton> <asp:ImageButton id="btnAllPerson" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w245"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 97%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset4"><DIV id="Div4"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="GVPerson" runat="server" Width="97%" Height="31px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w246" OnSelectedIndexChanged="GVPerson_SelectedIndexChanged" CellPadding="4" GridLines="None" AutoGenerateColumns="False" DataKeyNames="nip,personname,personoid,divisi" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Red" Width="5%"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="nip" HeaderText="NIP / User ID">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="AMP Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="40%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divisi" HeaderText="Department">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text="No Person on List"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="LinkButton4" onclick="LinkButton4_Click" runat="server" __designer:wfdid="w247">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="ButtonPerson" runat="server" __designer:wfdid="w248" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="MPEPerson" runat="server" __designer:wfdid="w249" TargetControlID="ButtonPerson" PopupControlID="PanelPerson" BackgroundCssClass="modalBackground" PopupDragHandleControlID="LabelPerson" Drag="True">
                </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                                        </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w251" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" __designer:wfdid="w252" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w253"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w254"></asp:TextBox> </TD></TR><TR><TD align=center colSpan=4>Jenis Barang : &nbsp; <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w255"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w256"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w257"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px; BACKGROUND-COLOR: beige"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w258" CellPadding="4" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" PageSize="100" UseAccessibleHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Katalog">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListMat" onclick="lbAddToListMat_Click" runat="server" __designer:wfdid="w259">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server" __designer:wfdid="w260">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" __designer:wfdid="w261" TargetControlID="btnHideListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" __designer:wfdid="w262" Visible="False"></asp:Button> 
</contenttemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Master Program Point :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD vAlign=top><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelValidasi" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False">
            </asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
