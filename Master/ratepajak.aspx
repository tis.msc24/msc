﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="ratepajak.aspx.vb" Inherits="Master_ratepajak" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" 
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Rate Pajak" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px"></td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Rate Pajak :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w199" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 10%" class="Label" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Period" __designer:wfdid="w200"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w201" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w202"></asp:ImageButton>&nbsp;To&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w203" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w204"></asp:ImageButton> <asp:Label id="Label6" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w205"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbCurrency" runat="server" Text="Currency" __designer:wfdid="w206"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLCurrency" runat="server" Width="85px" CssClass="inpText" __designer:wfdid="w207"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w208" Visible="False"></asp:CheckBox></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" Width="85px" CssClass="inpText" __designer:wfdid="w209" Visible="False">
                                                            <asp:ListItem>ACTIVE</asp:ListItem>
                                                            <asp:ListItem>INACTIVE</asp:ListItem>
                                                        </asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=4><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" __designer:wfdid="w210" Format="MM/dd/yyyy" PopupButtonID="imbPeriod1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" __designer:wfdid="w211" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" __designer:wfdid="w212" Format="MM/dd/yyyy" PopupButtonID="imbPeriod2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" __designer:wfdid="w213" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w214"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w215"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" BackColor="White" __designer:wfdid="w216" AllowPaging="True" PageSize="8" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" CellPadding="4" AutoGenerateColumns="False">

                                                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

                                                            <RowStyle BackColor="white" Font-Size="X-Small"></RowStyle>

                                                            <EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
                                                            <RowStyle BackColor="#F7F7DE" Font-Size="X-Small" ForeColor="Black"></RowStyle>
                                                            <Columns>
                                                                <asp:HyperLinkField DataNavigateUrlFields="rateoid" DataNavigateUrlFormatString="~/Master/ratepajak.aspx?oid={0}" DataTextField="nmr" HeaderText="No" SortExpression="nmr">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                </asp:HyperLinkField>
                                                                <asp:BoundField DataField="currencyoid" HeaderText="Currency" SortExpression="currencyoid">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ratedate" HeaderText="Date" SortExpression="ratedate">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="rateidrvalue" HeaderText="Rate to IDR" SortExpression="rateidrvalue">
                                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="rateusdvalue" HeaderText="Rate to USD" SortExpression="rateusdvalue">
                                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ratenote" HeaderText="Note" SortExpression="ratenote">
                                                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="activeflag" HeaderText="Status" SortExpression="activeflag" Visible="False">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                </asp:BoundField>
                                                            </Columns>

                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                                                            <PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
                                                            <EmptyDataTemplate>
                                                                <asp:Label ID="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label>
                                                            </EmptyDataTemplate>

                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

                                                            <EditRowStyle BackColor="#2461BF"></EditRowStyle>

                                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                        </asp:GridView> <asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w217"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w96"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="rateoid" runat="server" __designer:wfdid="w97" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label5" runat="server" __designer:wfdid="w98">Currency</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" Width="85px" CssClass="inpText" __designer:wfdid="w99"></asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 37px" class="Label" align=left><asp:Label id="Label4" runat="server" __designer:wfdid="w100">Date</asp:Label> <asp:Label id="Label10" runat="server" CssClass="Important" Text="*" __designer:wfdid="w101"></asp:Label></TD><TD style="HEIGHT: 37px" class="Label" align=center>:</TD><TD style="HEIGHT: 37px" class="Label" align=left><asp:TextBox id="ratedate" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w102" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w103"></asp:ImageButton> <asp:Label id="Label12" runat="server" __designer:wfdid="w104">To</asp:Label> <asp:TextBox id="ratetodate" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w105" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w106"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w107"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label7" runat="server" __designer:wfdid="w108">Rate to IDR</asp:Label> <asp:Label id="Label13" runat="server" CssClass="Important" Text="*" __designer:wfdid="w109"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rateres1" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w110" AutoPostBack="True" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" __designer:wfdid="w111">Rate to USD</asp:Label> <asp:Label id="Label11" runat="server" CssClass="Important" Text="*" __designer:wfdid="w112"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="rateres2" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w113" AutoPostBack="True" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" __designer:wfdid="w114">Note</asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="ratenote" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w115" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" __designer:wfdid="w116" Visible="False">Status</asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="activeflag" runat="server" Width="85px" CssClass="inpText" __designer:wfdid="w117" Visible="False">
                                                        <asp:ListItem>ACTIVE</asp:ListItem>
                                                        <asp:ListItem>INACTIVE</asp:ListItem>
                                                    </asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:TextBox id="rateidrvalue" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w118" Visible="False"></asp:TextBox> <asp:TextBox id="rateusdvalue" runat="server" Width="25px" CssClass="inpText" __designer:wfdid="w119" Visible="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=3><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w120" TargetControlID="ratedate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w121" TargetControlID="ratedate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDate2" runat="server" __designer:wfdid="w122" TargetControlID="ratetodate" PopupButtonID="imbDate2" Format="MM/dd/yyyy">
                                                    </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate2" runat="server" __designer:wfdid="w123" TargetControlID="ratetodate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                                                    </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeValue" runat="server" __designer:wfdid="w124" TargetControlID="rateidrvalue" MaskType="Number" Mask="99,999,999.99" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeValueUSD" runat="server" __designer:wfdid="w125" TargetControlID="rateusdvalue" MaskType="Number" Mask="99,999,999.999999" InputDirection="RightToLeft">
                                                    </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbValueIDR" runat="server" __designer:wfdid="w126" TargetControlID="rateres1" ValidChars="1234567890,.">
                                                    </ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbValueUSD" runat="server" __designer:wfdid="w127" TargetControlID="rateres2" ValidChars="1234567890,.">
                                                    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w128"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w129"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w130"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w131"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w132" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w133" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w134" Visible="False" AlternateText="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w135" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w136"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Rate Pajak :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px" colspan="2"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td style="TEXT-ALIGN: left" class="Label">
                                <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 10px; TEXT-ALIGN: center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" colspan="2">&nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
