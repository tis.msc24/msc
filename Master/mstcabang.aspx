<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstcabang.aspx.vb" Inherits="MstCabang" Title="PT. MULTI SARANA COMPUTER - Master General" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Master Cabang"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label">Filter :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="gc.gendesc">Cabang</asp:ListItem>
                                                    <asp:ListItem Value="gc.genCode">Kode</asp:ListItem>
                                                    <asp:ListItem Value="gc.genother2">No. Rek. Bank</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton
                                                    ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="5">
                                                <fieldset id="field1">
                                    <div id='divTblData'>
                                        <asp:GridView ID="GVmstgen" runat="server" EmptyDataText="No data in database." AutoGenerateColumns="False" Width="100%" CellPadding="4" AllowPaging="True" EnableModelValidation="True" ForeColor="#333333" GridLines="None" PageSize="8">
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="mstcabang.aspx?oid={0}"
                                                    DataTextField="genCode" HeaderText="Kode">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gendesc" HeaderText="Cabang" SortExpression="gendesc">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="genother1" HeaderText="Init. Cabang">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="genother2" HeaderText="No. Rek. Bank">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="genother3" HeaderText="Rekening a/n">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Kota" HeaderText="Kota" SortExpression="Kota">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" CssClass="gvhdr" Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EmptyDataTemplate>



                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        &nbsp;</div>
                                </fieldset>
                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;</div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List Of Master Cabang</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 133px" class="Label" align=left>Code <asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w96"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="gencode" runat="server" Width="195px" CssClass="inpTextDisabled" __designer:wfdid="w97" size="20" Enabled="False" MaxLength="30"></asp:TextBox>&nbsp; </TD></TR><TR><TD style="WIDTH: 133px" class="Label" vAlign=top align=left><asp:Label id="Label5" runat="server" Width="84px" Text="Nama Cabang" __designer:wfdid="w98"></asp:Label><asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w99"></asp:Label></TD><TD class="Label" vAlign=top align=left><asp:TextBox id="gendesc" runat="server" Width="195px" CssClass="inpText" __designer:wfdid="w100"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left>Initial Cabang</TD><TD class="Label" vAlign=top align=left><asp:TextBox id="GenOther1" runat="server" Width="195px" CssClass="inpText" __designer:wfdid="w101"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="Label6" runat="server" Width="84px" Text="No. Rek. Bank" __designer:wfdid="w102"></asp:Label></TD><TD class="Label" vAlign=top align=left><asp:TextBox id="genother2" runat="server" Width="195px" CssClass="inpText" __designer:wfdid="w103"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left><asp:Label id="Label4" runat="server" Width="84px" Text="Rekening a/n" __designer:wfdid="w104"></asp:Label></TD><TD class="Label" vAlign=top align=left><asp:TextBox id="genother3" runat="server" Width="195px" CssClass="inpText" __designer:wfdid="w105"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left>Kota</TD><TD class="Label" align=left><asp:DropDownList id="Genother5" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w106" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 133px" class="Label" align=left>Spesial Akses</TD><TD class="Label" align=left><asp:DropDownList id="GenOther4" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w107"><asp:ListItem Value="YA">YA</asp:ListItem>
<asp:ListItem Value="TIDAK">TIDAK</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=2>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w108"></asp:Label> By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w109"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w110"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton><asp:DropDownList id="gengroup" runat="server" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w112" AutoPostBack="True" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w113" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w114"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="genoid" runat="server" __designer:wfdid="w115" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" __designer:wfdid="w116" Visible="False"></asp:Label></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: Form Of Master Cabang</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w1" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w2"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w3"></asp:Image></td>
                            <td align="left">
                                <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important" __designer:wfdid="w4"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPopUpOK" OnClick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w5"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" __designer:wfdid="w6" DropShadow="True" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w7" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
