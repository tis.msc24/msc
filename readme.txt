Wave Blue by Gavs_Designs design
http://www.gavssite.com/css/

This template is released under the Creative Commons Attributions 2.5 license, which
basically means you can do whatever you want with it provided you credit the author.
(Gavin Staniforth). In the case of this template, a link back to my site is more than sufficient.

If you want to read the license, go here:

http://creativecommons.org/licenses/by/2.5/
Full code http://creativecommons.org/licenses/by/2.5/legalcode

