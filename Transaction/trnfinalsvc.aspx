<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnfinalsvc.aspx.vb" Inherits="Transaction_trnfinalsvc" title="Service Final" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table width="100%">
        <tr>
            <td style="height: 100%">
    <table width="100%">
        <tr>
            <th align="left" class="header" style="width: 100%" valign="center">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Servis Final" Width="170px"></asp:Label>
            </th>
        </tr>
    </table>
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; List Of Servis Final </span><strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="dCabangnya" runat="server" Width="128px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList id="ddlfilter" runat="server" Width="128px" CssClass="inpText"><asp:ListItem Value="trnfinalno">No. Final</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="reqcode">No. TTS</asp:ListItem>
</asp:DropDownList>&nbsp;<SPAN style="COLOR: #ff0000"><asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;</SPAN></TD></TR><TR><TD><asp:CheckBox id="cbperiod" runat="server" Text="Periode"></asp:CheckBox></TD><TD>:</TD><TD><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="64px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;to&nbsp;<asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="64px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></SPAN></TD></TR><TR><TD><asp:CheckBox id="cbstatus" runat="server" Text="Status"></asp:CheckBox></TD><TD>:</TD><TD><asp:DropDownList id="ddlfilterstatus" runat="server" Width="120px" CssClass="inpText"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem Value="POST">Post</asp:ListItem>
<asp:ListItem>Invoiced</asp:ListItem>
<asp:ListItem Enabled="False" Value="finish">Finish</asp:ListItem>
<asp:ListItem Enabled="False" Value="final">Final</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibprintdaftar" runat="server" ImageUrl="~/Images/print.png" ImageAlign="Top" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 10px" colSpan=3><asp:GridView style="WIDTH: 100%" id="gvdata" runat="server" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="FINOID" DataNavigateUrlFormatString="~/Transaction/trnfinalsvc.aspx?oid={0}" DataTextField="trnfinalno" HeaderText="No. Final">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="180px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="180px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="finaldate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typetts" HeaderText="Type Final">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="finstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data not found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><ajaxToolkit:MaskedEditExtender id="MEE3" runat="server" TargetControlID="tbperiodstart" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEE4" runat="server" TargetControlID="tbperiodend" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CE1" runat="server" TargetControlID="tbperiodstart" Format="dd/MM/yyyy" PopupButtonID="ibperiodstart"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CE2" runat="server" TargetControlID="tbperiodend" Format="dd/MM/yyyy" PopupButtonID="ibperiodend"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                             <img align="absMiddle" alt="" src="../Images/corner.gif"/>&nbsp;Form Servis Final</span>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: middle" colSpan=6><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label><asp:Label id="lbltitle" runat="server" Font-Size="10px" ForeColor="Red" Visible="False" Font-Underline="True"></asp:Label><asp:Label id="lblreqoid" runat="server" Visible="False"></asp:Label><asp:Label id="lblcreqoid" runat="server" Visible="False"></asp:Label><asp:Label id="lblcurrentuser" runat="server" Visible="False"></asp:Label><asp:Label id="custoid" runat="server" Visible="False"></asp:Label><asp:Label id="CabangAsal" runat="server" Visible="False"></asp:Label><asp:Label id="FinOid" runat="server" Visible="False"></asp:Label><asp:Label id="i_u" runat="server" ForeColor="Red" Visible="False">new</asp:Label><asp:Label id="createtime" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="165px" CssClass="inpText" AutoPostBack="True" OnSelectedIndexChanged="DdlCabang_SelectedIndexChanged"></asp:DropDownList></TD><TD id="TD10" Visible="false">Type Final</TD><TD id="TD12" Visible="false">:</TD><TD id="TD11" Visible="false"><asp:DropDownList id="DDLTypeTTS" runat="server" Height="20px" CssClass="inpText"><asp:ListItem>SERVICE</asp:ListItem>
<asp:ListItem>REPLACE</asp:ListItem>
<asp:ListItem Enabled="False" Value="SA">SALDO AWAL</asp:ListItem>
</asp:DropDownList><asp:Label id="TypeTTS" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD>No. Final</TD><TD>:</TD><TD><asp:TextBox id="TrnFinalNo" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD Visible="false">Tanggal</TD><TD Visible="false">:</TD><TD Visible="false"><asp:TextBox id="tglFinal" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>No. Tanda Terima</TD><TD>:</TD><TD><asp:TextBox id="tbservicesno" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibsearchsvc" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibdelsvc" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD>Customer</TD><TD>:</TD><TD><asp:TextBox id="lblfincust" runat="server" Width="224px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>Telepon</TD><TD>:</TD><TD><asp:TextBox id="phone" runat="server" Width="165px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD><TD>Biaya Servis</TD><TD>:</TD><TD align=left><asp:TextBox id="BiayaSrv" runat="server" Width="70px" CssClass="inpText" AutoPostBack="True">0.00</asp:TextBox></TD></TR><TR><TD>Desc. Servis</TD><TD style="VERTICAL-ALIGN: top">:</TD><TD><asp:TextBox id="tbsparepartjob" runat="server" Width="165px" CssClass="inpText"></asp:TextBox></TD><TD>Total Amt. Detail</TD><TD>:</TD><TD><asp:TextBox id="TotalPriceDtl" runat="server" Width="70px" CssClass="inpTextDisabled" Enabled="False">0.00</asp:TextBox></TD></TR><TR><TD><asp:Label id="Label700" runat="server" Width="102px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang" Font-Underline="True"></asp:Label></TD><TD style="VERTICAL-ALIGN: top"></TD><TD><asp:Label id="reqdtloid" runat="server" Visible="False"></asp:Label><asp:Label id="SeqBarang" runat="server" Visible="False"></asp:Label><asp:Label id="I_Text2" runat="server" Font-Bold="True" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD><asp:Label id="SerialNo" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD><asp:TextBox id="lblfinstatus" runat="server" CssClass="inpTextDisabled" Visible="False" Enabled="False">In Process</asp:TextBox></TD></TR><TR><TD>Katalog</TD><TD style="VERTICAL-ALIGN: top">:</TD><TD><asp:TextBox id="lblfinitemname" runat="server" Width="167px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnSbarang" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImbEbarang" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="ItemOid" runat="server" Visible="False"></asp:Label></TD><TD>Type Barang</TD><TD>:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="TypeBarang" runat="server" Width="102px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox> <asp:Label id="OidLocTitipan" runat="server" Visible="False"></asp:Label> <asp:Label id="OidLocRusak" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top" colSpan=4><asp:GridView style="WIDTH: 99%" id="GvBarang" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="reqmstoid,reqdtloid,itemoid,itemcode,itemdesc,reqqty,snno,kelengkapan,reqdtljob,typegaransi,satuan1,OidLoc,FlagBarang,QtySts,osqty" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Underline="False" ForeColor="SaddleBrown" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typegaransi" HeaderText="Type Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty Input"><ItemTemplate>
<asp:TextBox id="QtyTitipan" runat="server" Width="70px" CssClass="inpText" Text='<%# eval("osqty") %>' MaxLength="15" __designer:wfdid="w1"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEQtyTitipan" runat="server" __designer:wfdid="w4" ValidChars="0123456789,." TargetControlID="QtyTitipan"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="reqqty" HeaderText="Qty TTS">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typegaransi" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kelengkapan" HeaderText="kelengkapan" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" Visible="False">
<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="OidLoc" HeaderText="OidLoc" Visible="False"></asp:BoundField>
<asp:BoundField DataField="FlagBarang" HeaderText="FlagBarang" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="QtySts" HeaderText="QtySts" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD>Kelengkapan</TD><TD>:</TD><TD><asp:TextBox id="Kelengkapan" runat="server" Width="261px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="FlagBarang" runat="server" Visible="False"></asp:Label></TD><TD>Kerusakan</TD><TD>:</TD><TD><asp:TextBox id="Kerusakan" runat="server" Width="222px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>Qty</TD><TD>:</TD><TD><asp:TextBox id="Qty" runat="server" Width="53px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:DropDownList id="DdlUnit" runat="server" CssClass="inpTextDisabled" AutoPostBack="True" OnSelectedIndexChanged="DdlCabang_SelectedIndexChanged" Enabled="False"></asp:DropDownList></TD><TD></TD><TD style="VERTICAL-ALIGN: top"></TD><TD><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnAddToListBarang" runat="server" ImageUrl="~/Images/addtolist.png" Visible="False"></asp:ImageButton> <asp:ImageButton id="BtnClearDtl" runat="server" ImageUrl="~/Images/clear.png" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><STRONG><SPAN style="TEXT-DECORATION: underline"><asp:Label id="Label6" runat="server" Width="130px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Spareparts" Font-Underline="True"></asp:Label> <asp:Label id="lblspartstate" runat="server" Font-Bold="True" ForeColor="Red" Visible="False">new</asp:Label></SPAN></STRONG></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD></TD></TR><TR><TD>Spareparts</TD><TD>:</TD><TD><asp:TextBox style="MARGIN-RIGHT: 5px" id="tbsparepart" runat="server" Width="152px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibsearchspart" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibclearspart" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top"></asp:ImageButton></TD><TD>Gudang</TD><TD>:</TD><TD><asp:DropDownList id="matLoc" runat="server" Height="20px" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD></TR><TR><TD>Spareparts Qty</TD><TD>:</TD><TD><asp:TextBox id="tbsparepartqty" runat="server" Width="60px" CssClass="inpText" AutoPostBack="True" MaxLength="5">0.00</asp:TextBox> <asp:Label id="Stock" runat="server" Text="Stock" Visible="False"></asp:Label> <asp:Label id="ss" runat="server" Width="4px" Text=":" Visible="False"></asp:Label> <asp:Label id="saldoakhir" runat="server" Visible="False"></asp:Label></TD><TD>Merk</TD><TD>:</TD><TD><asp:TextBox id="merkspr" runat="server" Width="152px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD>Total Price</TD><TD>:</TD><TD><asp:TextBox id="TotPrice" runat="server" Width="101px" CssClass="inpTextDisabled" Enabled="False" MaxLength="15">0.00</asp:TextBox></TD><TD id="TD17" runat="server">Price</TD><TD id="TD18" runat="server">:</TD><TD id="TD16" runat="server"><asp:TextBox id="tbsparepartprice" runat="server" Width="101px" CssClass="inpText" AutoPostBack="True" MaxLength="15">0.00</asp:TextBox></TD></TR><TR><TD colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvrequest" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemOid" HeaderText="ItemOid" Visible="False">
<HeaderStyle HorizontalAlign="Left" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" BorderColor="CornflowerBlue" BorderWidth="1px" BorderStyle="Solid"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="snno" HeaderText="Serial No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqqty" DataFormatString="{0:#,###.0000}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Kelengkapan" HeaderText="Kelengkapan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdtljob" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="satuan1" Visible="False"></asp:BoundField>
<asp:BoundField DataField="typegaransi" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="lblreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="PARTOID" HeaderText="PARTOID" Visible="False"></asp:BoundField>
<asp:BoundField DataField="partdescshort">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PartUnitOid" HeaderText="PartUnitOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="partsqty" HeaderText="Parts Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PartsUnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="mtrloc" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="spartprice" HeaderText="Parts Price">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="totalprice" HeaderText="Total Nett Parts">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagbarang" HeaderText="Flag Barang" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" onclick="lbdelete_Click" runat="server"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD><asp:Label id="Label3" runat="server" Width="155px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail Barang Rusak" Font-Underline="True"></asp:Label></TD><TD></TD><TD><asp:Label id="trnfinalpartoid" runat="server" Visible="False"></asp:Label><asp:Label id="qtyreq" runat="server" Visible="False"></asp:Label><asp:Label id="qtypart" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label id="I_Text3" runat="server" Font-Bold="True" ForeColor="Red" Visible="False" __designer:wfdid="w1">NEW</asp:Label></TD><TD runat="server"></TD><TD runat="server"></TD><TD runat="server"><asp:Label id="ItemOidRusak" runat="server" Visible="False"></asp:Label><asp:Label id="itempartoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 14px">Katalog</TD><TD style="HEIGHT: 14px">:</TD><TD style="HEIGHT: 14px"><asp:TextBox style="MARGIN-RIGHT: 5px" id="ItemRusak" runat="server" Width="303px" CssClass="inpText"></asp:TextBox><asp:ImageButton style="MARGIN-RIGHT: 5px" id="BtnItemRusak" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="EraseBtnRusak" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top"></asp:ImageButton>&nbsp; </TD><TD style="HEIGHT: 14px" runat="server">Qty</TD><TD style="HEIGHT: 14px" runat="server">:</TD><TD style="HEIGHT: 14px" runat="server"><asp:TextBox id="QtyRusak" runat="server" Width="60px" CssClass="inpText" AutoPostBack="True" MaxLength="5">0.00</asp:TextBox></TD></TR><TR><TD></TD><TD></TD><TD><asp:GridView style="WIDTH: 99%" id="GvItemRusak" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="itemoid,itemcode,itemdesc" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Underline="False" ForeColor="SaddleBrown" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD runat="server"></TD><TD runat="server"></TD><TD runat="server"></TD></TR><TR><TD colSpan=6 runat="server"><asp:ImageButton style="MARGIN-RIGHT: 10px" id="AddToListRusak" runat="server" ImageUrl="~/Images/addtolist.png"></asp:ImageButton><asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/clear.png" Visible="False"></asp:ImageButton></TD></TR><TR><TD id="TblRusak" colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 140px; BACKGROUND-COLOR: beige" id="Scroll"><asp:GridView style="WIDTH: 99%" id="GvRusak" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="itemrusakoid" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Edit" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="LinkTambah" onclick="LinkTambah_Click" runat="server" __designer:wfdid="w4" CommandName='<%# Eval("itemreqoid") %>' ToolTip='<%# Eval("reqdtloid") %>'>+ Tambah</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog Rusak">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemtitipan" HeaderText="Katalog Titipan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyrusak" HeaderText="Qty Rusak">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtypart" HeaderText="Qty Part" Visible="False"></asp:BoundField>
<asp:BoundField DataField="qtyreq" HeaderText="Qty Req" Visible="False">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelrusak" onclick="lbdelrusak_Click" runat="server" ToolTip='<%# Eval("Seq") %>'><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="locoidrusak" HeaderText="mtrloc" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqmstoid" HeaderText="reqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemrusakoid" HeaderText="itemrusakoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itempartoid" HeaderText="itempartoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemreqoid" HeaderText="itemreqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnfinaloid" HeaderText="trnfinaloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trnfinalpartoid" HeaderText="trnfinalpartoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" ForeColor="Red">Data not found!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><STRONG><SPAN style="FONT-SIZE: 12pt">Total Netto : Rp.</SPAN> </STRONG><asp:Label id="NettNya" runat="server" Font-Size="Medium" Font-Bold="True" Text="0.00"></asp:Label><SPAN style="FONT-SIZE: 12pt"><STRONG>,-</STRONG></SPAN></TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibposting" runat="server" ImageUrl="~/Images/posting.png" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnSendApp" runat="server" ImageUrl="~/Images/sendapproval.png" CausesValidation="False" Visible="False"></asp:ImageButton></TD></TR><TR><TD colSpan=6><asp:Label id="lblcreate" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:FilteredTextBoxExtender id="FTEqty" runat="server" ValidChars="0123456789,." TargetControlID="tbsparepartqty"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEHrg" runat="server" ValidChars="0123456789,." TargetControlID="tbsparepartprice"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEBSrv" runat="server" ValidChars="0123456789,." TargetControlID="BiayaSrv"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEQtyRusak" runat="server" ValidChars="0123456789,." TargetControlID="QtyRusak"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" DropShadow="True" PopupControlID="pnlpopupmsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblcaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="uppupgv" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgv" runat="server" CssClass="modalBox" Visible="False" DefaultButton="ibpopfind"><DIV style="OVERFLOW: auto"><TABLE width="100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="Label480" runat="server" Width="220px" Font-Size="15pt" Font-Bold="True" Text="List tanda terima"></asp:Label></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList id="ddlpopfilter" runat="server" Width="112px" CssClass="inpText"><asp:ListItem Value="reqcode">No. TTS</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="tbpopfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibpopfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton> <asp:ImageButton id="ibpopviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 99%; HEIGHT: 168px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvpoporder" runat="server" ForeColor="#333333" PageSize="15" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="reqoid,reqcode,reqdate,custname,phone1,custoid,branch_code,Cate ,CabangAsal">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Font-Underline="False" ForeColor="SaddleBrown" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="reqcode" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tgl. TTS">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sType" HeaderText="Type TTS">
<HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Cabang Asal" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Cate" HeaderText="Cate" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red">Data not found!</asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:Label id="lblerror" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3><asp:ImageButton id="ibpopcancel" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopgv" runat="server" TargetControlID="bepopgv" Drag="True" PopupDragHandleControlID="ibpopcancel" BackgroundCssClass="modalBackground" PopupControlID="panpopgv" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgv" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderColor="Transparent" BorderStyle="None"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel8" runat="server">
        <contenttemplate>
<asp:Panel id="panelItem" runat="server" Width="80%" CssClass="modalBox" Visible="False" DefaultButton="ibpoppartfind"><TABLE width="100%"><TBODY><TR><TD align=center><asp:Label id="lblCaptItem" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Daftar Spareparts"></asp:Label> </TD></TR><TR><TD align=center>Filter : <asp:DropDownList id="ddlpoppartfilter" runat="server" Width="112px" CssClass="inpText"><asp:ListItem Value="partdescshort">Deskripsi</asp:ListItem>
<asp:ListItem Value="partcode">Spart Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="tbpoppartfilter" runat="server" Width="160px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:CheckBox id="CBGantiBarang" runat="server" Font-Bold="True" Text="Ganti Barang" AutoPostBack="True"></asp:CheckBox>&nbsp;<asp:ImageButton id="ibpoppartfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoppartviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton> </TD></TR><TR><TD align=center><asp:GridView id="GVItemList" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" Visible="False" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("PARTOID") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="partcode" HeaderText="Parts Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="partdescshort" HeaderText="Spare Parts">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="brand" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Parts Qty"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("QtyOs") %>' MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" ValidChars="1234567890.," TargetControlID="tbQty">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Harga"><ItemTemplate>
<asp:TextBox id="PriceParts" runat="server" Width="100px" CssClass="inpText" Text='<%# eval("spartprice") %>' Enabled="<%# GetStatus() %>" MaxLength="15"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftEPrice" runat="server" TargetControlID="PriceParts" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="partsUnit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Stock Akhir">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="5px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="UnitOid" HeaderText="UnitOid" Visible="False">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeFinal" HeaderText="TypeFinal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="Label91" runat="server" 
                                                                        Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:LinkButton id="LabelNya" runat="server" ForeColor="Red" Visible="False">*Tetap klik add to list dengan/tanpa spareparts</asp:LinkButton></TD></TR><TR><TD align=center><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseItem" runat="server">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="WIDTH: 525px"><asp:Label id="sValidasi" runat="server" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="btnHideItem" PopupDragHandleControlID="lblCaptItem" BackgroundCssClass="modalBackground" PopupControlID="panelItem"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideItem" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

