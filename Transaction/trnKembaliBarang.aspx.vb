Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class KembaliBarang
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi : Dim sSql As String = ""
    Dim dsData As New DataSet : Dim dv As DataView
    Dim ckoneksi As New Koneksi : Dim CProc As New ClassProcedure
    'Dim cClass As New ClassProc
    Dim cust As String = "" : Dim jenis As String = ""
    Dim brand As String = "" : Dim flag As String = ""
    Dim sColom As String : Dim sValue As String
    Dim iVal As Integer : Dim iCount As Integer = 0
    Dim report As New ReportDocument : Dim vreport As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Procedure"
    Private Sub fDDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(sFillterCbng, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(sFillterCbng, sSql)
            Else
                FillDDL(sFillterCbng, sSql)
                sFillterCbng.Items.Add(New ListItem("ALL", "ALL"))
                sFillterCbng.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(sFillterCbng, sSql)
            sFillterCbng.Items.Add(New ListItem("ALL", "ALL"))
            sFillterCbng.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub DDLBranch()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlCabangNya, sSql)
            Else
                FillDDL(ddlCabangNya, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(ddlCabangNya, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption
        Validasi.Text = message
        panelMsg.Visible = True : btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal message As String)
        Validasi.Text = message
        panelMsg.Visible = True : btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        If sFillterCbng.SelectedValue <> "ALL" Then
            sWhere &= " AND km.branch_code='" & sFillterCbng.SelectedValue & "'"
        End If
        sSql = "Select km.branch_code,km.trnkembalimstoid,km.trnkembalino,pm.trnpinjamno,pm.namapeminjam,Convert(Char(10),pm.trndatepinjam,103) trndatepinjam,Convert(Char(10),km.trnkembalidate,103) trnkembalidate,km.Status,km.note,km.updtime,km.upduser From QL_trnkembalimst km INNER JOIN QL_trnpinjammst pm ON pm.trnpinjammstoid=km.trnpinjammstoid AND km.branch_code=pm.branch_code WHERE km.cmpcode='" & CompnyCode & "'" & sWhere & " Order By trnkembalimstoid Desc"
        Session("TblMst") = ckoneksi.ambiltabel(sSql, "PenerimaanBarang")
        gvPenerimaan.DataSource = Session("TblMst")
        gvPenerimaan.DataBind() : gvPenerimaan.SelectedIndex = -1
    End Sub

    Private Sub bindPinjam()
        sSql = "Select cmpcode,branch_code,trnpinjammstoid,trnpinjamno,namapeminjam,status FROM QL_trnpinjammst p Where (trnpinjamno LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%' OR namapeminjam LIKE '%" & Tchar(TxtPinjamNo.Text.Trim) & "%') AND status = 'POST' and branch_code='" & ddlCabangNya.SelectedValue & "' "
        Dim dtab As DataTable = ckoneksi.ambiltabel(sSql, "TblPinjam")
        If dtab.Rows.Count > 0 Then
            GvPinjam.DataSource = dtab
            GvPinjam.DataBind()
            Session("PinjamNo") = dtab
            GvPinjam.SelectedIndex = -1
            GvPinjam.Visible = True
        Else
            showMessage("Tidak ada data,,!!", CompnyName, 2)
            Exit Sub
        End If
    End Sub

    Private Sub FillTextBox(ByVal Branch As String, ByVal foid As Integer)
        sSql = "Select km.branch_code,km.trnkembalimstoid,km.trnkembalino,pm.trnpinjammstoid,pm.trnpinjamno,pm.namapeminjam,Convert(Char(10),pm.trndatepinjam,103) trndatepinjam,Convert(Char(10),km.trnkembalidate,103) trnkembalidate,km.Status,km.note,km.updtime,km.upduser From QL_trnkembalimst km INNER JOIN QL_trnpinjammst pm ON pm.trnpinjammstoid=km.trnpinjammstoid AND km.branch_code=pm.branch_code WHERE km.cmpcode='" & CompnyCode & "' AND trnkembalimstoid=" & foid & " AND km.branch_code='" & Branch & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                ddlCabangNya.SelectedValue = xreader("branch_code").ToString
                Kembalioid.Text = xreader("trnkembalimstoid")
                lbloid.Text = xreader("trnkembalimstoid")
                txtNoTanda.Text = xreader("trnkembalino").ToString.Trim
                TxtPinjamNo.Text = xreader("trnpinjamno").ToString.Trim
                TrnPinjamOid.Text = xreader("trnpinjammstoid").ToString.Trim
                txtTglTerima.Text = xreader("trnkembalidate").ToString.Trim
                txtNamaCust.Text = xreader("namapeminjam").ToString.Trim
                txtStatus.Text = xreader("Status").ToString.Trim
                UpdTime.Text = xreader("updtime").ToString.Trim
                UpdUser.Text = xreader("upduser").ToString.Trim
                txtDetailPinjam.Text = xreader("note").ToString.Trim
            End While
        End If
        xreader.Close()
        conn.Close()

        If txtStatus.Text = "In Process" Then
            btnPost.Visible = True
        Else
            btnPost.Visible = False : btnAddToList.Visible = False
            btnClear.Visible = False : btnSave.Visible = False : btnDelete.Visible = False
            gvPinjamBarang.Enabled = False : printmanualpenerimaan.Visible = False
        End If

        sSql = "Select Distinct seqdtl sequence,trnpinjamdtloid,trnkembalidtloid,m.itemdesc,m.itemcode,kd.itemqty,kd.itemoid,qtypinjam,kd.flagbarang,kd.mtrlocoid From QL_trnkembalidtl kd INNER JOIN QL_mstitem m ON m.itemoid=kd.itemoid AND branch_code='" & Branch & "' AND kd.trnkembalimstoid=" & foid & " AND kd.cmpcode='" & CompnyCode & "'"

        Session("tblitemdtl") = ckoneksi.ambiltabel(sSql, "TblDtlTerima")
        gvPinjamBarang.DataSource = Session("tblitemdtl")
        gvPinjamBarang.DataBind()
    End Sub

    Private Sub FilterGVList(ByVal barcode As String, ByVal jenis As String, ByVal merk As String, ByVal nama As String, ByVal kategori As String)
        gvPenerimaan.DataBind()
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND a.gengroup = 'LOCATION' and a.genother6='UMUM' AND b.gengroup = 'WAREHOUSE' AND a.genother2=(Select genoid From ql_mstgen Where gencode ='" & ddlCabangNya.SelectedValue & "' And gengroup='Cabang') AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc Desc"
        FillDDL(LocationDDL, sSql)
    End Sub

    Private Sub GenerateReqCode()
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = "" : Dim iCurID As Integer = 0
            Dim sCode As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & ddlCabangNya.SelectedValue & "' AND gengroup='CABANG'")
            code = "REF/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(ABS(replace(trnkembalino,'" & code & "',''))),0) trnkembalino FROM QL_trnkembalimst WHERE trnkembalino LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            If Not IsDBNull(xCmd.ExecuteScalar) Then
                iCurID = xCmd.ExecuteScalar + 1
            Else
                iCurID = 1
            End If
            code = GenNumberString(code, "", iCurID, 4)
            txtNoTanda.Text = code

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub ClearDetail()
        dtlseq.Text = "1" : flagbarang.Text = ""
        If Session("tblitemdtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("tblitemdtl")
            If objTable.Rows.Count > 0 Then
                dtlseq.Text = objTable.Rows.Count + 1
            End If
        End If
        i_u2.Text = "New" : txtBarang.Text = "" : gvPinjamBarang.SelectedIndex = -1
        trnpinjamdtlqty.Text = "" : itemoid.Text = "" : trnpinjamdtloid.Text = "0"
    End Sub

    Private Sub clearRef()
        Session("gvListCust") = Nothing : gvPinjamBarang.DataSource = Nothing
        gvPinjamBarang.DataBind()
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String)
        Try
            vreport.Load(Server.MapPath(folderReport & "printnotaptp.rpt"))
            vreport.SetParameterValue("sWhere", id)
            vreport.SetParameterValue("cmpcode", CompnyCode)
            CProc.SetDBLogonForReport(vreport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vreport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
            vreport.PrintOptions.PrinterName = "EPSON SERVICE"
            vreport.PrintToPrinter(1, False, 0, 0)
            vreport.Close() : vreport.Dispose()

        Catch ex As Exception
            showMessage("Print Failed :" & ex.Message, CompnyName & " - WARNING", 2)
            vreport.Close() : vreport.Dispose() : Exit Sub
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub gvPenerimaan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPenerimaan.SelectedIndexChanged
        Response.Redirect("trnKembaliBarang.aspx?branch_code=" & gvPenerimaan.SelectedDataKey("branch_code").ToString & "&oid=" & gvPenerimaan.SelectedDataKey("trnkembalimstoid") & "")
    End Sub

    Protected Sub gvPenerimaan_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPenerimaan.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvPenerimaan.DataSource = Session("TblMst")
            gvPenerimaan.DataBind()
        End If
    End Sub

    Protected Sub printmanualpenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles printmanualpenerimaan.Click
        'ID = Session("oid")
        'If ddlprinter.SelectedValue.ToLower = "auto" Then
        '    Try
        '        vreport.Load(Server.MapPath(folderReport & "printnotaptp.rpt"))
        '        vreport.SetParameterValue("sWhere", ID)
        '        vreport.SetParameterValue("cmpcode", CompnyCode)
        '        CProc.SetDBLogonForReport(vreport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        '        vreport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        '        vreport.PrintOptions.PrinterName = "EPSON SERVICE"
        '        vreport.PrintToPrinter(1, False, 0, 0)
        '        vreport.Close() : vreport.Dispose()

        '    Catch ex As Exception
        '        showMessage("Print Failed :" & ex.Message, CompnyName & " - WARNING", 2)
        '        vreport.Close() : vreport.Dispose() : Exit Sub
        '    End Try
        'Else

        '    'untuk(Print)
        '    Dim code As String = ckoneksi.ambilscalar("select reqoid from ql_trnrequest where reqoid='" & ID & "'")
        '    Dim no As String = ckoneksi.ambilscalar("select reqcode from ql_trnrequest where reqoid='" & ID & "'")

        '    report.Load(Server.MapPath(folderReport & "printnota.rpt"))
        '    report.SetParameterValue("sWhere", code)
        '    report.SetParameterValue("cmpcode", CompnyCode)
        '    CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        '    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        '    Response.Buffer = False
        '    Response.ClearContent()
        '    Response.ClearHeaders()

        '    Try
        '        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "DetPenerimaanService" & Format(GetServerTime(), "dd_MM_yy"))
        '        report.Close() : report.Dispose() : Session("no") = Nothing
        '        Response.Redirect("~\Transaction\trnKembaliBarang.aspx?awal=true")
        '    Catch ex As Exception
        '        report.Close() : report.Dispose()
        '    End Try
        'End If
    End Sub

    Private Sub binddataItem()
        If TxtPinjamNo.Text.Trim = "" Then
            showMessage("No Tanda Terima belum dipilih,,!!", CompnyName, 2)
            Exit Sub
        End If

        sSql = "SELECT * FROM (Select pd.trnpinjamdtloid,pd.itemoid,m.itemcode,m.itemdesc,ISNULL(pd.itemqty,0.00)-(SELECT ISNULL(SUM(kd.itemqty),0.00) FROM QL_trnkembalidtl kd INNER JOIN QL_trnkembalimst km ON kd.trnkembalimstoid=km.trnkembalimstoid AND kd.branch_code=km.branch_code WHERE km.trnpinjammstoid=pm.trnpinjammstoid AND kd.itemoid=pd.itemoid AND kd.statusdtl IN ('POST','COMPLETE')) itemqty,pd.flagbarang,pd.mtrlocoid,m.stockflag From QL_trnpinjamdtl pd INNER JOIN QL_trnpinjammst pm ON pm.trnpinjammstoid=pd.trnpinjammstoid AND pd.branch_code=pm.branch_code INNER JOIN ql_mstitem m ON m.itemoid=pd.itemoid Where (m.itemcode LIKE '%" & Tchar(txtBarang.Text.Trim) & "%' OR m.itemdesc LIKE '%" & Tchar(txtBarang.Text.Trim) & "%') AND pm.trnpinjamno ='" & Tchar(TxtPinjamNo.Text.Trim) & "') AS Tb Where itemqty > 0"
        FillGV(gvMaterial, sSql, "QL_mstitem")
        gvMaterial.Visible = True
    End Sub

    Private Sub GeneratedID()
        If i_u.Text = "new" Then
            Kembalioid.Text = GenerateID("QL_trnkembalimst", CompnyCode)
        End If
    End Sub

#End Region

#Region "Function"
    Function TambahRow()
        If lblUpdNo.Text <> "" Then
            TambahRow = lblUpdNo.Text + 1
            'ElseIf lblUpdNo.Text = Session("Row") Then
            '    TambahRow = gvPinjamBarang.Rows.Count + 1
        Else
            TambahRow = gvPinjamBarang.Rows.Count + 1
        End If

        Return TambahRow
    End Function

    Function tNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function

    Private Function setTableDetail() As DataTable
        Dim dt As DataTable = New DataTable("trnRequest")
        Dim ds As New DataSet
        dt.Columns.Add("sequence", Type.GetType("System.Int32"))
        dt.Columns.Add("trnpinjamdtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dt.Columns.Add("itemdesc", Type.GetType("System.String"))
        dt.Columns.Add("itemQty", Type.GetType("System.Int32"))
        dt.Columns.Add("qtypinjam", Type.GetType("System.Int32"))
        dt.Columns.Add("itemcode", Type.GetType("System.String"))
        dt.Columns.Add("flagbarang", Type.GetType("System.String"))
        dt.Columns.Add("mtrlocoid", Type.GetType("System.String"))
        ds.Tables.Add(dt)
        Session("tblitemdtl") = dt
        Return dt
    End Function

    Function GenerateDtlID()
        iVal = GenerateID("ql_trnrequestdtl", CompnyCode)
        Return iVal
    End Function

    Function tambahNol(ByVal iAngka As Integer)
        Dim sValue = "000"
        If iAngka >= 10 Then : sValue = "00" : End If
        If iAngka >= 100 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 1000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function

    Private Function GetSortDirection(ByVal column As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column
        Return sortDirection
    End Function

    Function Get_Branch() As String
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As String = String.Empty

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT gencode FROM QL_mstgen where gengroup = 'cabang' and genoid = " & Session("branch_id") & ""
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            refcode = rdr("gencode")
        End While
        conn2.Close()
        Return refcode

    End Function

    Function Get_USDRate() As Single
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim rate As Single

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "select top 1 isnull(rate2idrvalue,0) rate2idrvalue from QL_mstrate2 where rate2month = MONTH(getdate()) and rate2year = YEAR(GETDATE()) and currencyoid = 2"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            rate = rdr("rate2idrvalue")
        End While
        conn2.Close()
        Return rate
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
         If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\trnKembaliBarang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Me.btnPost.Attributes.Add("onclick", "return confirm('Data sudah dipost tidak dapat diupdate');")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah anda yakin akan menghapus data ini ??');")
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")

        Me.Title = CompnyName & " - Pengembalian Barang"

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            fDDLBranch() : DDLBranch()
            BindData("") : InitAllDDL()
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                Session("new") = True
                UpdUser.Text = Session("UserID") : UpdTime.Text = GetServerTime()
                i_u.Text = "new" : dtlseq.Text = 1 : i_u2.Text = "New"
                Session("sequence") = dtlseq.Text : GenerateReqCode()
                txtTglTerima.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0 : txtStatus.Text = "In Process"
                btnClear.Visible = False : btnPost.Visible = False
                btnDelete.Visible = False : lblUpd.Text = "Created"
                Session("gvPinjamBarang") = Nothing
                iuAdd.Text = "new" : InitAllDDL()
                GeneratedID()
            Else
                Session("new") = False
                FillTextBox(Session("branch_code"), Session("oid"))
                i_u.Text = "update" : i_u2.Text = "Update"
                TabContainer1.ActiveTabIndex = 1
                iuAdd.Text = "update" : lblUpd.Text = "Last Update"
            End If
            FilterPeriod1.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If

        If txtStatus.Text = "In" Then
            btnSave.Visible = False : btnAddToList.Visible = False
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        sWhere &= " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        If checkPeriod.Checked Then
            Try
                Dim date1 As Date = toDate(FilterPeriod1.Text)
                Dim date2 As Date = toDate(txtPeriod2.Text)
                If FilterPeriod1.Text = "" And txtPeriod2.Text = "" Then
                    showMessage("Isi Tanggal !!", CompnyName & "- Warning", 2)
                End If
                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal Awal Tidak Valid !!", CompnyName & " - Warning", 2)
                    Exit Sub
                End If
                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal Akhir Tidak Valid !!", CompnyName & " - Warning", 2)
                    Exit Sub
                End If
                If date1 > date2 Then
                    showMessage("Tanggal Awal harus lebih kecil dari Tanggal Akhir !!", CompnyName & " - Warning", 2)
                    Exit Sub
                    FilterPeriod1.Text = "" : txtPeriod2.Text = ""
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Format Tanggal tidak sesuai !!", CompnyName & " - Warning", 2)
            End Try
        End If
        If checkStatus.Checked Then
            If StatusDDL.SelectedValue <> "All" Then
                sWhere &= " AND km.Status='" & StatusDDL.SelectedValue & "'"
            End If
        End If
        BindData(sWhere)
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        i_u2.Text = "new" : itemoid.Text = ""
        trnpinjamdtlqty.Text = "0.00" : txtBarang.Text = ""
        If Session("tblitemdtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("tblitemdtl")
            dtlseq.Text = objTable.Rows.Count + 1
        Else
            dtlseq.Text = 1
        End If
        txtDetailPinjam.Text = ""
        ClearDetail()
        gvPinjamBarang.SelectedIndex = -1
        gvPinjamBarang.Columns(5).Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("gvPinjamBarang") = Nothing
        Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnKembaliBarang.aspx?awal=true")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        FilterText.Text = "" : FilterDDL.SelectedIndex = 0
        FilterGVList("", "", "", "", "")
        checkPeriod.Checked = False : checkStatus.Checked = False
        BindData("") : StatusDDL.SelectedIndex = 0
        gvPenerimaan.PageIndex = 0
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If Session("new") = True Then
            Session("oid") = Nothing
        End If

        Dim sErr As String = ""
        GeneratedID()

        If Session("tblitemdtl") Is Nothing Then
            sErr &= "Buat detail item pengembalian !!<br>"
            Exit Sub
        Else
            Dim objTableCek As DataTable
            Dim objRowCek() As DataRow
            objTableCek = Session("tblitemdtl")
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                sErr &= "Buat detail item pinjaman !!<br>" 
            End If
        End If

        If gvPinjamBarang.Visible = False Then
            sErr &= "Data detail belum diisi..!!<br>"
        End If

        If TrnPinjamOid.Text = "" Then
            sErr &= "Nama Peminjam belum diisi..!!<br>"
        End If

        Dim objdtlNo As DataTable = Session("tblitemdtl")
        
        For C1 As Integer = 0 To objdtlNo.Rows.Count - 1
            If ToDouble(objdtlNo.Rows(C1).Item("itemqty")) > ToDouble(objdtlNo.Rows(C1).Item("qtypinjam")) Then
                sErr &= "Qty Kembali tidak bisa lebih dari qty pinjam..!!!<br>" 
            End If
        Next

        If sErr <> "" Then
            showMessage(sErr, CompnyName & "- Warning", 2)
            Exit Sub
        End If

        Dim conmtroid As Integer = GenerateID("QL_conmtr", CompnyCode)
        Dim crdmtroid As Integer = GenerateID("QL_crdmtr", CompnyCode)
        Dim oidTemp As Int64 = GenerateID("QL_histhpp", CompnyCode)
        Dim qPeriod As String = GetDateToPeriodAcctg(CDate(toDate(txtTglTerima.Text)))
        trnpinjamdtloid.Text = GenerateID("QL_trnkembalidtl", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        If txtNamaCust.Text = "" Then
            showMessage("Isi nama customer !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        Try
            'insert  
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnkembalimst (cmpcode,branch_code,trnkembalimstoid,trnkembalino,trnpinjammstoid,trnkembalidate,namapeminjam,Status,Note,updtime,upduser) VALUES " & _
                    " ('" & CompnyCode & "','" & ddlCabangNya.SelectedValue & "'," & Kembalioid.Text & ",'" & Tchar(txtNoTanda.Text.Trim) & "'," & TrnPinjamOid.Text & ",'" & CDate(toDate(txtTglTerima.Text)) & "','" & Tchar(txtNamaCust.Text) & "','" & txtStatus.Text & "','" & Tchar(txtDetailPinjam.Text) & "',current_timestamp,'" & Session("UserID") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update ql_mstoid Set lastoid=" & Kembalioid.Text & " From QL_mstoid where tablename ='QL_trnkembalimst' And cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                'update
                sSql = "UPDATE QL_trnkembalimst SET branch_code = '" & ddlCabangNya.SelectedValue & "', trnkembalino = '" & Tchar(txtNoTanda.Text.Trim) & "',trnpinjammstoid = " & TrnPinjamOid.Text & ", namapeminjam = '" & Tchar(txtNamaCust.Text) & "',Status = '" & txtStatus.Text & "', Note = '" & Tchar(txtDetailPinjam.Text) & "', updtime = current_timestamp, upduser = '" & Session("UserID") & "' WHERE trnkembalimstoid=" & Kembalioid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Not Session("tblitemdtl") Is Nothing Then
                Dim kHpp As Double
                Dim objTableNo As DataTable = Session("tblitemdtl")
                sSql = "DELETE QL_trnkembalidtl Where trnkembalimstoid=" & Kembalioid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                For C1 As Integer = 0 To objTableNo.Rows.Count - 1

                    sSql = "Select HPP from QL_mstitem Where itemoid=" & objTableNo.Rows(C1).Item("itemoid") & ""
                    xCmd.CommandText = sSql : kHpp = xCmd.ExecuteScalar

                    sSql = "INSERT INTO QL_trnkembalidtl (cmpcode,trnkembalidtloid,branch_code,trnkembalimstoid,itemoid,itemqty,statusdtl,notedtl,updtime,upduser,seqdtl,qtypinjam,trnpinjamdtloid,flagbarang,mtrlocoid,hpp) VALUES" & _
                    " ('" & CompnyCode & "', " & C1 + CInt(trnpinjamdtloid.Text) & ",'" & ddlCabangNya.SelectedValue & "'," & Kembalioid.Text & "," & objTableNo.Rows(C1).Item("itemoid") & "," & ToDouble(objTableNo.Rows(C1).Item("itemqty")) & ",'" & txtStatus.Text & "','No. TT ||" & Tchar(txtNoTanda.Text) & "',current_timestamp,'" & Session("UserID") & "'," & ToDouble(objTableNo.Rows(C1).Item("sequence")) & "," & ToDouble(objTableNo.Rows(C1).Item("qtypinjam")) & "," & ToDouble(objTableNo.Rows(C1).Item("trnpinjamdtloid")) & ",'" & ToDouble(objTableNo.Rows(C1).Item("flagbarang")) & "'," & objTableNo.Rows(C1).Item("mtrlocoid") & "," & ToDouble(kHpp) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    trnpinjamdtloid.Text += 1
                Next

                sSql = "Update QL_mstoid set lastoid=(Select MAX(trnkembalidtloid) FROM QL_trnkembalidtl) Where tablename='QL_trnkembalidtl' And cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            Dim COA_Hutang_Usaha As Integer = 0
            Dim varadjustplus As String = GetVarInterface("VAR_HPP", CompnyCode)
            If varadjustplus Is Nothing Or varadjustplus = "" Then
                showMessage("Interface untuk Akun VAR_HPP tidak ditemukan!", 2, "")
                objTrans.Rollback()
                conn.Close()
                Exit Sub
            Else
                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & varadjustplus & "'"
                xCmd.CommandText = sSql : COA_Hutang_Usaha = xCmd.ExecuteScalar
                If COA_Hutang_Usaha = 0 Or COA_Hutang_Usaha = Nothing Then
                    showMessage("Akun COA untuk VAR_AP tidak ditemukan!", 2, "")
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                End If
            End If

            If txtStatus.Text = "POST" Then
                Dim glValue, glUSDValue, hpp, AmtCredit As Double
                Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
                Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
                Dim iGlSeq As Int16 = 1
                Dim periodene As String = GetDateToPeriodAcctg(GetServerTime).Trim
                Dim UsdRate As Single = Get_USDRate()
                If ToDouble(UsdRate) = 0 Then
                    showMessage("Rate Currency Belum Di isi !", 2, "")
                    Exit Sub
                End If
                If Not Session("tblitemdtl") Is Nothing Then
                    Dim objTableNo As DataTable = Session("tblitemdtl")
                    Dim dvfilter As DataView = objdtlNo.DefaultView
                    dvfilter.RowFilter = "flagbarang = 1"

                    If dvfilter.Count > 0 Then
                        sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,branch_code,type) VALUES " & _
                        "('" & CompnyCode & "'," & iglmst & ",Convert(date,getdate()),'" & periodene & "','KEMBALI(" & txtNoTanda.Text & ")','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlCabangNya.SelectedValue & "','REF')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid = " & iglmst & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    For C1 As Integer = 0 To objTableNo.Rows.Count - 1
                        sSql = "Select ISNULL((SELECT ISNULL(itemqty,0.00) FROM QL_trnpinjamdtl pd Where pd.trnpinjammstoid=pm.trnpinjammstoid AND pm.branch_code=pd.branch_code AND kd.itemoid=pd.itemoid),0.00)-SUM(kd.itemqty) FROM QL_trnkembalidtl kd INNER JOIN QL_trnkembalimst km ON km.trnkembalimstoid=kd.trnkembalimstoid AND kd.branch_code=km.branch_code INNER JOIN QL_trnpinjammst pm ON km.trnpinjammstoid=pm.trnpinjammstoid AND km.branch_code=pm.branch_code WHERE pm.trnpinjammstoid=" & TrnPinjamOid.Text & " AND kd.itemoid=" & objTableNo.Rows(C1).Item("itemoid") & " GROUP BY pm.branch_code,pm.trnpinjammstoid,kd.itemoid"
                        xCmd.CommandText = sSql
                        If xCmd.ExecuteScalar() = 0 Then
                            sSql = "UPDATE QL_trnkembalidtl Set statusdtl='COMPLETE' Where trnpinjamdtloid=" & Integer.Parse(objTableNo.Rows(C1).Item("trnpinjamdtloid")) & " AND itemoid=" & objTableNo.Rows(C1).Item("itemoid") & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If

                        'Select Last HPP 
                        sSql = "Select HPP from QL_mstitem Where itemoid=" & objTableNo.Rows(C1).Item("itemoid") & "" : xCmd.CommandText = sSql : hpp = xCmd.ExecuteScalar

                        sSql = "Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) From QL_conmtr Where periodacctg='" & qPeriod & "' AND refoid=" & Integer.Parse(objTableNo.Rows(C1).Item("itemoid")) & " And branch_code='" & ddlCabangNya.SelectedValue & "' AND mtrlocoid=" & Integer.Parse(objTableNo.Rows(C1).Item("mtrlocoid")) & ""
                        xCmd.CommandText = sSql : Dim QtyAkhir As Double = xCmd.ExecuteScalar

                        If QtyAkhir < 0 Then
                            QtyAkhir = 0 
                        End If

                        'Rumus Hitung Hpp
                        Dim HppNew As Double = ((ToDouble(hpp) * ToDouble(objTableNo.Rows(C1).Item("itemqty"))) + (hpp * QtyAkhir)) / (ToDouble(objTableNo.Rows(C1).Item("itemqty")) + QtyAkhir)
                        If objTableNo.Rows(C1).Item("flagbarang") = "1" Then
                            'Insert History Hpp
                            sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser,branch_code) Values" & _
                            " ('" & CompnyCode & "'," & oidTemp + C1 & "," & CInt(trnpinjamdtloid.Text) & ",'QL_trnkembalidtl','QL_MSTITEM'," & objTableNo.Rows(C1).Item("itemoid") & ",0," & ToDouble(hpp) & "," & ToDouble(HppNew) & "," & ToDouble(QtyAkhir) & "," & ToDouble(objTableNo.Rows(C1).Item("itemqty")) & "," & ToDouble(hpp) & ",'" & qPeriod & "',current_timestamp,'" & Session("UserID") & "','" & ddlCabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            'UPdate New Hpp di Mstitem
                            sSql = "Update ql_mstitem set HPP=" & ToDouble(HppNew) & " Where ql_mstitem.cmpcode='" & CompnyCode & "' And ql_mstitem.itemoid=" & objTableNo.Rows(C1).Item("itemoid") & ""
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            Dim COA_gudang As Integer = 0
                            Dim vargudang As String = "" : Dim iMatAccount As String = ""
                            sSql = "Select stockflag From ql_mstitem Where itemoid= " & objTableNo.Rows(C1)("itemoid").ToString & ""
                            xCmd.CommandText = sSql
                            Dim sTockFlag As String = xCmd.ExecuteScalar

                            sSql = "Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & objTableNo.Rows(C1).Item("mtrlocoid") & " AND genother4='" & sTockFlag & "'"
                            xCmd.CommandText = sSql : Dim xBRanch As String = xCmd.ExecuteScalar

                            Dim mVar As String = ""
                            If sTockFlag = "I" Then
                                mVar = "VAR_INVENTORY"
                            ElseIf sTockFlag = "ASSET" Then
                                mVar = "VAR_GUDANG_ASSET"
                            Else
                                mVar = "VAR_GUDANG"
                            End If

                            iMatAccount = GetVarInterface(mVar, xBRanch)
                            If iMatAccount = "?" Or iMatAccount = "0" Or iMatAccount = "" Then
                                showMessage("Maaf,interface " & mVar & " belum di setting silahkan hubungi admin untuk seting interface !!", CompnyName & " - WARNING", "")
                                Session("click_post") = "false"
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            End If

                            If iMatAccount Is Nothing Or iMatAccount = "" Then
                                showMessage("Interface untuk Akun " & mVar & " tidak ditemukan!", 2, "")
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            Else
                                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAccount & "'"
                                xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                                If COA_gudang = 0 Or COA_gudang = Nothing Then
                                    showMessage("Akun COA untuk " & mVar & " tidak ditemukan!", 2, "")
                                    objTrans.Rollback() : conn.Close()
                                    Exit Sub
                                End If
                            End If

                            'gl Value
                            glValue = ToMaskEdit(ToDouble(objTableNo.Rows(C1).Item("itemqty")) * ToDouble(hpp), 2)
                            'glIDRValue = glValue

                            ' jurnal detail
                            glUSDValue = ToDouble(glValue) / ToDouble(UsdRate)
                            sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid, acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate,upduser, updtime,branch_code) VALUES" & _
                            "('" & CompnyCode & "'," & igldtl & "," & iGlSeq & "," & iglmst & "," & COA_gudang & ",'D'," & glValue & "," & glValue & "," & glUSDValue & ",'" & txtNoTanda.Text & "','KEMBALI(" & objTableNo.Rows(C1).Item("itemdesc").ToString & ")','','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlCabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            igldtl += 1 : iGlSeq += 1

                            'Insert Conmtr
                            sSql = " INSERT INTO QL_conmtr (cmpcode,conmtroid,formoid,Formname,type,trndate,periodacctg,mtrlocoid,refoid,qtyIn,amount,note,FormAction,upduser,updtime,unitoid,refname,hpp,branch_code) VALUES" & _
                            " ('" & CompnyCode & "'," & (conmtroid + C1) & "," & CInt(trnpinjamdtloid.Text) & ",'QL_trnkembalidtl','KEMBALI','" & CDate(toDate(txtTglTerima.Text)) & "','" & qPeriod & "'," & objTableNo.Rows(C1).Item("mtrlocoid") & "," & objTableNo.Rows(C1).Item("itemoid") & "," & ToDouble(objTableNo.Rows(C1).Item("itemqty")) & "," & ToDouble(objTableNo.Rows(C1).Item("itemqty")) * ToDouble(HppNew) & ",'" & txtDetailPinjam.Text & " - " & Tchar(txtNoTanda.Text) & " ','" & Tchar(txtNoTanda.Text.Trim) & "','" & Session("UserID") & "',current_timestamp,945,'QL_MSTITEM'," & ToDouble(HppNew) & ",'" & ddlCabangNya.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                        AmtCredit += ToMaskEdit(ToDouble(objTableNo.Rows(C1).Item("itemqty")) * ToDouble(hpp), 2)
                    Next

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime,branch_code) VALUES " & _
                    "('" & CompnyCode & "'," & igldtl & "," & iGlSeq & "," & iglmst & "," & COA_Hutang_Usaha & ",'C'," & ToDouble(AmtCredit) & "," & ToDouble(AmtCredit) & "," & 0 & ",'" & txtNoTanda.Text & "','KEMBALI(" & txtNoTanda.Text & ")','','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlCabangNya.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Select SUM(itemqty)-(SELECT SUM(itemqty) FROM QL_trnkembalidtl kd INNER JOIN QL_trnkembalimst km ON km.trnkembalimstoid=kd.trnkembalimstoid AND km.branch_code=kd.branch_code WHERE km.trnpinjammstoid=pm.trnpinjammstoid AND pm.branch_code=km.branch_code) FROM QL_trnpinjamdtl pd INNER JOIN QL_trnpinjammst pm ON pm.trnpinjammstoid = pd.trnpinjammstoid AND pm.branch_code=pd.branch_code WHERE pm.trnpinjammstoid=" & TrnPinjamOid.Text & " AND status='POST' GROUP BY pm.trnpinjammstoid,pm.branch_code "
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteScalar() = 0 Then
                        sSql = "Update QL_trnpinjammst set Status='CLOSED' Where trnpinjammstoid=" & TrnPinjamOid.Text & ""
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    sSql = "Update QL_mstoid set lastoid=" & (objTableNo.Rows.Count - 1 + CInt(oidTemp)) & " Where tablename = 'QL_HistHPP' And cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstoid set lastoid=" & (objTableNo.Rows.Count - 1 + conmtroid) & " Where tablename = 'QL_conmtr' And cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstoid set lastoid=" & (objTableNo.Rows.Count - 1 + crdmtroid) & " Where tablename = 'QL_crdmtr' And cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstoid set lastoid = " & igldtl & " where tablename = 'QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnKembaliBarang.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        txtStatus.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub gvPenerimaan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPenerimaan.PageIndexChanging
        gvPenerimaan.PageIndex = e.NewPageIndex
        gvPenerimaan.DataSource = Session("TblMst")
        gvPenerimaan.DataBind()
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
 
        If Session("tblitemdtl") Is Nothing Then
            setTableDetail()
        End If
        Dim objTable As DataTable = Session("tblitemdtl")
        Dim dv As DataView = objTable.DefaultView

        Dim dd As String = dtlseq.Text
        If trnpinjamdtloid.Text = 0 Then
            showMessage("Detail Item Belum dipilih!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        If trnpinjamdtlqty.Text = 0 Then
            showMessage("Quantity Tidak boleh Nol..!!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        If i_u2.Text = "New" Then
            dv.RowFilter = "itemoid =" & itemoid.Text
        Else
            dv.RowFilter = "itemoid =" & itemoid.Text & " AND sequence <> " & dtlseq.Text & ""
        End If

        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("Data ini sudah ditambahkan!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        dv.RowFilter = ""
        Dim objRow As DataRow
        If i_u2.Text = "New" Then
            objRow = objTable.NewRow()
            objRow("sequence") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(dtlseq.Text - 1)
            objRow.BeginEdit()
        End If
        'objRow("sequence") = Integer.Parse(dtlseq.Text)
        objRow("trnpinjamdtloid") = trnpinjamdtloid.Text
        objRow("itemoid") = Integer.Parse(itemoid.Text)
        objRow("itemdesc") = txtBarang.Text
        objRow("itemQty") = trnpinjamdtlqty.Text
        objRow("qtypinjam") = QtyV.Text
        objRow("itemcode") = matcode.Text
        objRow("flagbarang") = flagbarang.Text
        If flagbarang.Text <> 0 Then
            objRow("mtrlocoid") = LocationDDL.SelectedValue
        Else
            objRow("mtrlocoid") = 0
        End If
        If i_u2.Text = "New" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        Session("tblitemdtl") = objTable
        gvPinjamBarang.DataSource = Session("tblitemdtl")
        gvPinjamBarang.DataBind() : ClearDetail()
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False : panelMsg.Visible = False
    End Sub

    Protected Sub gvPinjamBarang_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPinjamBarang.RowDeleting
        Try
            Dim iIndex As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tblitemdtl")
            objTable.Rows.RemoveAt(iIndex)
            'resequence
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit()
                dr("sequence") = C1 + 1
                dr.EndEdit()
            Next

            Session("tblitemdtl") = objTable
            gvPinjamBarang.DataSource = objTable
            gvPinjamBarang.DataBind()

            Session("sequence") = objTable.Rows.Count + 1
            If gvPinjamBarang.Rows.Count = 0 Then
                lblUpdNo.Text = "0"
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        txtDetailPinjam.Text = ""
        ClearDetail()
    End Sub

    Protected Sub gvPinjamBarang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPinjamBarang.SelectedIndexChanged
        Try
            dtlseq.Text = gvPinjamBarang.SelectedDataKey.Item("sequence").ToString
            i_u2.Text = "Update"
            Dim objTable As DataTable = Session("tblitemdtl")
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "sequence=" & dtlseq.Text
            itemoid.Text = dv.Item(0).Item("itemoid").ToString
            txtBarang.Text = dv.Item(0).Item("itemdesc").ToString
            trnpinjamdtlqty.Text = dv.Item(0).Item("itemqty").ToString
            trnpinjamdtloid.Text = dv.Item(0).Item("trnpinjamdtloid").ToString
            QtyV.Text = dv.Item(0).Item("qtypinjam").ToString
            flagbarang.Text = dv.Item(0).Item("flagbarang")
            If dv.Item(0).Item("flagbarang") <> 0 Then
                LocationDDL.SelectedValue = dv.Item(0).Item("mtrlocoid").ToString
                LocationDDL.Visible = True : lblTitik.Visible = True
                Location.Visible = True
            Else
                LocationDDL.Visible = False : lblTitik.Visible = False
                Location.Visible = False
            End If
            dv.RowFilter = ""

        Catch ex As Exception
            showMessage(ex.Message, CompnyName & "- Warning", 1)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        Dim sColom() As String = {"MSTREQOID"}
        Dim sTable() As String = {"QL_TRNMECHCHECK"}

        If ClassFunction.CheckDataExists(Kembalioid.Text, sColom, sTable) = True Then
            showMessage("tidak dapat hapus data, karena digunakan di tabel lain !!", CompnyName & "- Warning", 2)
            Exit Sub
        End If

        'hapus tabel detail
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            strSQL = "Delete QL_trnkembalimst Where trnkembalimstoid='" & Kembalioid.Text & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
            strSQL = "Delete QL_trnkembalidtl Where trnkembalimstoid='" & Kembalioid.Text & "'"
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()
            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            objCmd.Connection.Close()
            showMessage(ex.ToString)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnKembaliBarang.aspx?awal=true")
    End Sub

    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim status As String = gvr.Cells(6).Text.ToString
        If status = "In Process" Then
            showMessage("Nota penerimaan dengan status In Process belum bisa dicetak !", CompnyName & " - WARNING", 2)
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        PrintReport(sender.ToolTip, lbloid.Text)
    End Sub

    Protected Sub sBarang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sBarang.Click
        binddataItem()
    End Sub

    Protected Sub gvMaterial_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvMaterial.PageIndex = e.NewPageIndex
        binddataItem()
    End Sub

    Protected Sub gvMaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemoid.Text = gvMaterial.SelectedDataKey.Item("itemoid").ToString
        txtBarang.Text = gvMaterial.SelectedDataKey.Item("itemdesc").ToString
        trnpinjamdtlqty.Text = ToDouble(gvMaterial.SelectedDataKey.Item("itemqty").ToString)
        QtyV.Text = ToDouble(gvMaterial.SelectedDataKey.Item("itemqty").ToString)
        trnpinjamdtloid.Text = gvMaterial.SelectedDataKey.Item("trnpinjamdtloid").ToString
        matcode.Text = gvMaterial.SelectedDataKey.Item("itemcode").ToString
        flagbarang.Text = gvMaterial.SelectedDataKey.Item("flagbarang").ToString
        If gvMaterial.SelectedDataKey.Item("mtrlocoid").ToString <> 0 Then
            LocationDDL.SelectedValue = gvMaterial.SelectedDataKey.Item("mtrlocoid").ToString
            LocationDDL.Visible = True : Location.Visible = True : lblTitik.Visible = True
        Else
            LocationDDL.Visible = False : Location.Visible = False : lblTitik.Visible = False
        End If
        gvMaterial.Visible = False : dtlseq.Text = 1
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("tblitemdtl") Is Nothing Then
            dtab = Session("tblitemdtl")
        Else
            showMessage("Missing detail list session !", CompnyName & "WARNING", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("sequence = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("sequence") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvPinjamBarang.DataSource = dtab
        gvPinjamBarang.DataBind()
        Session("tblitemdtl") = dtab
        ClearDetail()
    End Sub

    Protected Sub sBtnPinjamNO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindPinjam()
    End Sub

    Protected Sub GvPinjam_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvPinjam.PageIndex = e.NewPageIndex
        bindPinjam()
    End Sub

    Protected Sub GvPinjam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TxtPinjamNo.Text = GvPinjam.SelectedDataKey.Item("trnpinjamno")
        TrnPinjamOid.Text = GvPinjam.SelectedDataKey.Item("trnpinjammstoid")
        txtNamaCust.Text = GvPinjam.SelectedDataKey.Item("namapeminjam")
        flagbarang.Text = GvPinjam.SelectedDataKey.Item("flagbarang")
        GvPinjam.Visible = False
    End Sub

    Protected Sub ErasePinjamNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        TxtPinjamNo.Text = "" : TrnPinjamOid.Text = "" : txtNamaCust.Text = "" : flagbarang.Text = ""
    End Sub

    Protected Sub gvPinjamBarang_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
            'e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
        End If
    End Sub

    Protected Sub gvMaterial_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)
        End If
    End Sub

    Protected Sub gvPenerimaan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(3).Text = Format(e.Row.Cells(3).Text, "dd/MM/yyyy")
        '    e.Row.Cells(4).Text = Format(e.Row.Cells(4).Text, "dd/MM/yyyy")
        'End If
    End Sub

    Protected Sub EraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtBarang.Text = "" : itemoid.Text = ""
        trnpinjamdtlqty.Text = 0 : QtyV.Text = 0
        matcode.Text = ""
    End Sub
#End Region

    Protected Sub ddlCabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCabangNya.SelectedIndexChanged
        InitAllDDL()
        GenerateReqCode()
    End Sub
End Class