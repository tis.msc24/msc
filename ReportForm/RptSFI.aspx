<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="RptSFI.aspx.vb" Inherits="ReportForm_SFI" title="" EnableEventValidation="false" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Laporan Final Internal" CssClass="Title" ForeColor="Navy" Font-Size="X-Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: transparent" valign="center">
                            <table width="100%">
                                <tr>
                                    <td align="center" valign="top" char="Label">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR id="BusinessUnitA" runat="server" visible="true"><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Cabang"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="BranchDDL" runat="server" CssClass="inpText" Width="155px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left id="TD2" runat="server" visible="false"><asp:Label id="Label8" runat="server" Text="Type"></asp:Label></TD><TD class="Label" align=center id="TD3" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD1" runat="server" visible="false"><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True">
<asp:ListItem>Detail</asp:ListItem>
<asp:ListItem Value="Summary" Enabled="False">Summary</asp:ListItem>
</asp:DropDownList></TD></TR>
    <tr>
        <td align="left" class="Label">
            Tanggal</td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></td>
    </tr>
    <TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2">
                                                                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear">
                                                                            </ajaxToolkit:MaskedEditExtender> </TD></TR>
    <tr>
        <td align="left" class="Label" valign="top">
            <asp:DropDownList ID="DDLDONo" runat="server" AutoPostBack="True" CssClass="inpText"
                Width="100px">
                <asp:ListItem Value="NOTTS">No. TTS</asp:ListItem>
                <asp:ListItem Value="DRAFT">Draft No.</asp:ListItem>
            </asp:DropDownList></td>
        <td align="center" class="Label" valign="top">
            :</td>
        <td align="left" class="Label" valign="top">
            <asp:TextBox ID="dono" runat="server" CssClass="inpText" TextMode="MultiLine" Width="158px"></asp:TextBox>&nbsp;<asp:ImageButton
                ID="imbFindDO" runat="server" Height="26px" ImageAlign="Top" ImageUrl="~/Images/search.gif" />&nbsp;<asp:ImageButton
                    ID="imbEraseDO" runat="server" ImageAlign="Top" ImageUrl="~/Images/erase.bmp"
                    Width="25px" /></td>
    </tr>
    <TR><TD class="Label" align=left valign="top"><asp:DropDownList id="DDLSONo" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True"><asp:ListItem Value="NOSFI">No. Final Int.</asp:ListItem>
                                                                                <asp:ListItem Value="DRAFT">No. Draft</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=center valign="top">:</TD><TD class="Label" align=left valign="top"><asp:TextBox id="sono" runat="server" CssClass="inpText" Width="158px" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="imbFindSO" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="26px" Width="25px"></asp:ImageButton> <asp:ImageButton id="imbEraseSO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Width="25px"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left valign="top"><asp:Label id="lblMat" runat="server" Text="Katalog"></asp:Label></TD><TD class="Label" align=center valign="top"><asp:Label id="lbl2" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left valign="top"><asp:TextBox id="itemcode" runat="server" CssClass="inpText" Width="178px" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="26px" Width="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Width="25px"></asp:ImageButton></TD></TR>
    <tr>
        <td align="left" class="Label">
            <asp:Label id="Label2" runat="server" Text="Status"></asp:Label></td>
        <td align="center" class="Label">
            :</td>
        <td align="left" class="Label">
            <asp:CheckBoxList id="cblStatus" runat="server" CssClass="inpText" Width="300px" AutoPostBack="True" RepeatColumns="3"><asp:ListItem Selected="True">In Process</asp:ListItem>
<asp:ListItem Selected="True">In Approval</asp:ListItem>
<asp:ListItem Selected="True">Revised</asp:ListItem>
<asp:ListItem Selected="True">Approved</asp:ListItem>
<asp:ListItem Selected="True">Rejected</asp:ListItem>
</asp:CheckBoxList></td>
    </tr>
    <TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3>
    <asp:UpdateProgress ID="uProgReportForm" runat="server" AssociatedUpdatePanelID="upReportForm">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
            </div>
            <div id="processMessage" class="processMessage">
                <span style="font-weight: bold; font-size: 10pt; color: purple">
                    <asp:Image ID="Image4" runat="server" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif"
                        Width="81px" /><br />
                    Please Wait .....</span><br />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
                                                </TD></TR></TBODY></TABLE></asp:Panel> 
                                                    <CR:CrystalReportViewer ID="crvReportForm" runat="server" AutoDataBind="True" HasCrystalLogo="False"
                                                        HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" HasToggleGroupTreeButton="False"
                                                        HasViewList="False" />
</ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnViewReport" />
                                                            <asp:PostBackTrigger ControlID="btnExportToPdf" />
                                                            <asp:PostBackTrigger ControlID="btnExportToExcel" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                <asp:UpdatePanel ID="upListDO" runat="server">
                    <ContentTemplate>
<asp:Panel id="PanelListDO" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Penerimaan (TTS)"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" DefaultButton="btnFindListDO"><asp:Label id="LblFilter" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="doitemmstoid">No. TTS</asp:ListItem>
<asp:ListItem Value="doitemno">No. Penerimaan</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="98%" ForeColor="RosyBrown" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                <Columns>
                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbLMDO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("doitemmstoid") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="doitemmstoid" HeaderText="Draft No">
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="doitemno" HeaderText="No. TTS">
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="dodate" HeaderText="Tanggal">
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="custname" HeaderText="Customer">
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="doitemmststatus" HeaderText="Status">
                                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                    HorizontalAlign="Right" />
                                                <EmptyDataTemplate>
                                                    &nbsp;
                                                </EmptyDataTemplate>
                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHiddenListDO" PopupDragHandleControlID="lblListDO" PopupControlID="PanelListDO" BackgroundCssClass="modalBackground">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListSO" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelListSO" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List Final Internal"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label24" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="trnfinalintno">No. Final</asp:ListItem>
<asp:ListItem Value="reqcode">No. Penerimaan</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" PageSize="5" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMSO" runat="server" ToolTip='<%# eval("finoid") %>' Checked='<%# eval("checkvalue") %>' __designer:wfdid="w1"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="finoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnfinalintno" HeaderText="No. Final">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="finaldate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqcode" HeaderText="Penerimaan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="finitemstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" CssClass="modalBox" Width="900px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Large" Font-Bold="True" Text="List Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemlongdesc">Katalog</asp:ListItem>
    <asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" ForeColor="#333333" Width="99%" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemcode" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Red" BackColor="#FFCC66" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>&nbsp;- <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
            </th>
        </tr>
    </table>
</asp:Content>

