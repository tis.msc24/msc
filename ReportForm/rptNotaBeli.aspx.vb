Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptNotaBeli
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function UpdateCheckedPI() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblPI") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblPI")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListPI.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListPI.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnbelimstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblPI") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedPI2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblPIView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblPI")
            Dim dtTbl2 As DataTable = Session("TblPIView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListPI.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListPI.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnbelimstoid=" & cbOid
                                dtView2.RowFilter = "trnbelimstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblPI") = dtTbl
                Session("TblPIView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedure"

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        'If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 1 is invalid. " & sErr, 2)
        '    Return False
        'End If
        'If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
        '    showMessage("Your Period 2 is invalid. " & sErr, 2)
        '    Return False
        'End If
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitddlGroup()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen WHERE gengroup='ITEMGROUP' AND genoid IN (SELECT i.itemgroupoid FROM QL_mstitem i) ORDER BY gendesc"
        FillDDL(DDLItemGroup, sSql)
        DDLItemGroup.Items.Add(New ListItem("ALL", "ALL"))
        DDLItemGroup.SelectedValue = "ALL"
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        Dim personoid As Integer = GetStrData("Select pr.PERSONOID,PERSONNAME from QL_MSTPERSON pr INNER JOIN QL_MSTPROF pf ON pf.personnoid=pr.PERSONOID WHERE pr.PERSONOID>0 AND pf.USERID='" & Session("UserID") & "'")

        Try 
            Dim sWhere As String = "Where mst.cmpcode='" & CompnyCode & "'" : Dim namaPDF As String = ""

            'If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            '    sWhere &= " AND mst.trnbelimstoid IN (Select trnbelimstoid FROM QL_trnbelidtl dt INNER JOIN ql_mstitem i ON i.itemoid=dt.itemoid AND i.personoid =" & personoid & ")"
            'End If

            If trnbelino.Text <> "" Then
                Dim sBelino() As String = Split(trnbelino.Text, ";")
                sSql &= " AND ("
                For c1 As Integer = 0 To sBelino.Length - 1
                    sSql &= " mst.trnbelino LIKE '%" & Tchar(sBelino(c1)) & "%'"
                    If c1 < sBelino.Length - 1 Then
                        sSql &= " OR"
                    End If
                Next
                sSql &= ")"
                sWhere &= sSql
            End If

            vReport = New ReportDocument
            If type.SelectedValue = "Summary" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptPurchSumExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptPurchSum.rpt"))
                End If

                sWhere &= " AND mst.trnbelidate Between '" & toDate(dateAwal.Text) & "' And '" & toDate(dateAkhir.Text) & "'" & IIf(ToDouble(suppoid.Text) > 0, " And mst.trnsuppoid =" & suppoid.Text, "")
                namaPDF = "Nota_Beli_(Sum)_"
            Else
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptPurchDetailExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptPurchDetail.rpt"))
                End If

                sWhere &= " AND mst.trnbelidate BETWEEN '" & toDate(dateAwal.Text) & "' AND '" & toDate(dateAkhir.Text) & "' " & IIf(ToDouble(suppoid.Text) > 0, " And mst.trnsuppoid=" & suppoid.Text, "") & " AND beli.trnsjbelino LIKE '%" & Tchar(txtSjNo.Text.Trim) & "%'"

                If itemcode.Text <> "" Then
                    Dim sMatcode() As String = Split(itemcode.Text, ";")
                    sSql &= " AND ("
                    For c1 As Integer = 0 To sMatcode.Length - 1
                        sSql &= " i.itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                        If c1 < sMatcode.Length - 1 Then
                            sSql &= " OR"
                        End If
                    Next
                    sSql &= ")"
                    sWhere &= sSql
                End If

                namaPDF = "Nota_Beli_(Dtl)_"
            End If
            If dCabangNya.SelectedValue <> "ALL" Then
                sWhere &= "AND mst.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            If TypeNota.SelectedValue <> "ALL" Then
                sWhere &= "AND mst.typepo='" & TypeNota.SelectedValue & "'"
            End If
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            If tipe = "" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataPI()
        sSql = "SELECT 'False' AS checkvalue,trnbelimstoid,trnbelino,CONVERT(CHAR(20),trnbelidate,103) trnbelidate,trnbelistatus,(SELECT suppname FROM ql_mstsupp WHERE suppoid=bm.trnsuppoid) suppname FROM QL_trnbelimst bm WHERE cmpcode='" & CompnyCode & "'"
        If suppoid.Text <> "" Then
            sSql &= " AND bm.trnsuppoid=" & suppoid.Text & ""
        End If
        sSql &= " ORDER BY trnbelimstoid DESC"
        Session("TblPI") = ckon.ambiltabel(sSql, "QL_trnbelimst")
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT 'False' AS checkvalue, i.itemoid,i.itemcode, i.itemdesc, i.itemcode,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya,i.itemgroupoid FROM QL_mstitem i INNER JOIN QL_trnbelidtl som ON som.cmpcode=i.cmpcode AND som.itemoid=i.itemoid WHERE som.trnbelimstoid IN (SELECT trnbelimstoid FROM ql_trnbelimst WHERE cmpcode='" & CompnyCode & "'"
        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        If trnbelino.Text <> "" Then
            Dim sBelino() As String = Split(trnbelino.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sBelino.Length - 1
                sSql &= " trnbelino LIKE '%" & Tchar(sBelino(c1)) & "%'"
                If c1 < sBelino.Length - 1 Then
                    sSql &= " OR"
                End If
            Next
            sSql &= ")" 
        End If

        sSql &= ") GROUP BY i.itemoid, i.itemdesc, i.itemcode, i.stockflag,i.itemgroupoid ORDER BY itemdesc"
        Session("TblMat") = ckon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub BindSj()
        sSql = " Select DISTINCT bd.trnsjbelino,sjm.trnbelipono,bm.trnbelino,suppname,bm.trnbelidate from QL_trnbelidtl bd Inner Join ql_trnsjbelimst sjm ON sjm.trnsjbelino=bd.trnsjbelino Inner Join QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid Inner JOin QL_mstsupp sup ON sup.suppoid=bm.trnsuppoid Where bd.trnsjbelino LIKE '%" & Tchar(txtSjNo.Text.Trim) & "%' AND sup.suppname LIKE '%" & Tchar(suppname.Text.Trim) & "%' AND bm.trnbelidate BETWEEN '" & CDate(toDate(dateAwal.Text.Trim)) & "' AND '" & CDate(toDate(dateAkhir.Text.Trim)) & "'"

        If trnbelino.Text <> "" Then
            Dim sBelino() As String = Split(trnbelino.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sBelino.Length - 1
                sSql &= " bm.trnbelino LIKE '%" & Tchar(sBelino(c1)) & "%'"
                If c1 < sBelino.Length - 1 Then
                    sSql &= " OR"
                End If
            Next
            sSql &= ")"
        End If

        sSql &= " Order By bm.trnbelino"
        FillGV(GvSj, sSql, "ql_mstitem")
        GvSj.Visible = True
    End Sub

    Public Sub BindDataSupp()
        sSql = "SELECT suppoid, suppcode, suppname, supptype  FROM ql_mstsupp s WHERE (suppname like '%" & Tchar(suppname.Text.Trim) & "%' or suppcode like '%" & Tchar(suppname.Text.Trim) & "%') AND s.suppoid IN (SELECT b.trnsuppoid FROM QL_trnbelimst b WHERE s.suppoid=b.trnsuppoid AND b.cmpcode=s.cmpcode AND b.trnbelidate BETWEEN '" & CDate(toDate(dateAwal.Text)) & "' AND '" & CDate(toDate(dateAkhir.Text)) & "') ORDER BY suppname"
        FillGV(gvSupp, sSql, "ql_mstsupp")
        gvSupp.Visible = True
    End Sub
#End Region  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_id") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If
            Response.Redirect("~\ReportForm\rptNotaBeli.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Nota Pembelian "
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            fDDLBranch() : InitddlGroup()
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click 
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptNotaBeli.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        showPrint("excel") 
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataSupp()
    End Sub 

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = ""
        suppoid.Text = ""
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        gvSupp.PageIndex = e.NewPageIndex
        BindDataSupp()
    End Sub 

    Protected Sub btntopdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntopdf.Click 
        showPrint("pdf")
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        If type.SelectedValue = "Summary" Then
            tritem.Visible = False
            Label3.Visible = False : txtSjNo.Visible = False
            crISjno.Visible = False  : DelSjNo.Visible = False
        Else 
            tritem.Visible = True
            Label3.Visible = True : txtSjNo.Visible = True
            crISjno.Visible = True : DelSjNo.Visible = True
        End If
    End Sub

    Protected Sub crISjno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSj()
    End Sub

    Protected Sub GvSj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
        txtSjNo.Text = GvSj.SelectedDataKey.Item("trnsjbelino")
        GvSj.Visible = False
    End Sub

    Protected Sub GvSj_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvSj.PageIndex = e.NewPageIndex
        BindSj()
    End Sub

    Protected Sub DelSjNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtSjNo.Text = "" : GvSj.Visible = False
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5"
            gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListPI") Is Nothing And Session("EmptyListPI") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListPI") Then
                Session("EmptyListPI") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListPI, PanelListPI, mpeListPI, True)
            End If
        End If
        If Not Session("WarningListPI") Is Nothing And Session("WarningListPI") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListPI") Then
                Session("WarningListPI") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListPI, PanelListPI, mpeListPI, True)
            End If
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        Dim sPlus As String = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        If DDLItemGroup.SelectedValue <> "ALL" Then
            sPlus &= " AND itemgroupoid=" & DDLItemGroup.SelectedValue & ""
        End If

        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        DDLItemGroup.SelectedValue = "ALL"
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub 

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPI.Click
        If IsValidPeriod() Then
            DDLFilterListPI.SelectedIndex = -1 : txtFilterListPI.Text = ""
            Session("TblPI") = Nothing : Session("TblPIView") = Nothing
            gvListPI.DataSource = Nothing : gvListPI.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListPI, PanelListPI, mpeListPI, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub gvListPI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListPI.PageIndexChanging
        If UpdateCheckedPI2() Then
            gvListPI.PageIndex = e.NewPageIndex
            gvListPI.DataSource = Session("TblPIView")
            gvListPI.DataBind()
        End If
        mpeListPI.Show()
    End Sub

    Protected Sub lkbCloseListPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListPI.Click
        cProc.SetModalPopUpExtender(btnHiddenListPI, PanelListPI, mpeListPI, False)
    End Sub

    Protected Sub lkbAddToListPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListPI.Click
        If Not Session("TblPI") Is Nothing Then
            If UpdateCheckedPI() Then
                Dim dtTbl As DataTable = Session("TblPI")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If trnbelino.Text <> "" Then
                            trnbelino.Text &= ";" + vbCrLf + dtView(C1)("trnbelino")
                        Else
                            trnbelino.Text = dtView(C1)("trnbelino")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListPI, PanelListPI, mpeListPI, False)
                Else
                    Session("WarningListPI") = "- Maaf, Silahkan pilih data dulu..!!"
                    showMessage(Session("WarningListPI"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListPI") = "- Maaf, Silahkan pilih data dulu..!!"
            showMessage(Session("WarningListPI"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnViewAllListPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPI.Click
        DDLFilterListPI.SelectedIndex = -1 : txtFilterListPI.Text = ""
        If Session("TblPI") Is Nothing Then
            BindDataPI()
            If Session("TblPI").Rows.Count <= 0 Then
                Session("EmptyListPI") = "PI data can't be found!"
                showMessage(Session("EmptyListPI"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedPI() Then
            Dim dt As DataTable = Session("TblPI")
            Session("TblPIView") = dt
            gvListPI.DataSource = Session("TblPIView")
            gvListPI.DataBind()
        End If
        mpeListPI.Show()
    End Sub

    Protected Sub btnFindListPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPI.Click
        If Session("TblSO") Is Nothing Then
            BindDataPI()
            If Session("TblPI").Rows.Count <= 0 Then
                Session("EmptyListPI") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("EmptyListPI"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListPI.SelectedValue & " LIKE '%" & Tchar(txtFilterListPI.Text) & "%'"
        If UpdateCheckedPI() Then
            Dim dv As DataView = Session("TblPI").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblPIView") = dv.ToTable
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
                dv.RowFilter = ""
                mpeListPI.Show()
            Else
                dv.RowFilter = ""
                Session("TblPIView") = Nothing
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
                Session("WarningListPI") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("WarningListPI"), 2)
            End If
        Else
            mpeListPI.Show()
        End If
    End Sub

    Protected Sub imbErasePI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePI.Click
        trnbelino.Text = ""
    End Sub

    Protected Sub btnViewCheckedPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedPI.Click
        If Session("TblPI") Is Nothing Then
            Session("WarningListPI") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListPI"), 2)
            Exit Sub
        End If
        If UpdateCheckedPI() Then
            Dim dtTbl As DataTable = Session("TblPI")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListPI.SelectedIndex = -1 : txtFilterListPI.Text = ""
                Session("TblPIView") = dtView.ToTable
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
                dtView.RowFilter = ""
                mpeListPI.Show()
            Else
                dtView.RowFilter = ""
                Session("TblPIView") = Nothing
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
                Session("WarningListPI") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListPI"), 2)
            End If
        Else
            mpeListPI.Show()
        End If
    End Sub

    Protected Sub btnSelectNonePI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNonePI.Click
        If Not Session("TblPIView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblPIView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblPI")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnbelimstoid=" & dtTbl.Rows(C1)("trnbelimstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblPI") = objTbl
                Session("TblPIView") = dtTbl
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
            End If
            mpeListPI.Show()
        Else
            Session("WarningListPI") = "Mohon maaf, data tidak ditemukan..!!"
            showMessage(Session("WarningListPI"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnALLSelectItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectItem.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Maaf, Anda belum pilih katalog..!!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedItem.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Mohon maaf, data tidak ditemukan..!! !"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub   

    Protected Sub btnALLSelectPI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnALLSelectPI.Click
        If Not Session("TblPIView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblPIView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblPI")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnbelimstoid=" & dtTbl.Rows(C1)("trnbelimstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblPI") = objTbl
                Session("TblPIView") = dtTbl
                gvListPI.DataSource = Session("TblPIView")
                gvListPI.DataBind()
            End If
            mpeListPI.Show()
        Else
            Session("WarningListPI") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListPI"), 2)
        End If
    End Sub
End Class
