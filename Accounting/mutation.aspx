<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mutation.aspx.vb" Inherits="Accounting_mutation" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server"> 

    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Mutation Accounting"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel2" runat="server" HorizontalAlign="Left" DefaultButton="btnSearch" __designer:wfdid="w329"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="cbDDL" runat="server" Width="91px" CssClass="inpText" __designer:wfdid="w330"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value=" ">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:CheckBox id="cbPeriode" runat="server" Font-Size="X-Small" Text="Period" __designer:wfdid="w352" Checked="True"></asp:CheckBox></TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w331"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w332"></asp:ImageButton> <asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w333"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w334"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w335"></asp:ImageButton> </TD></TR><TR><TD align=left>No Bukti </TD><TD align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="nobukti" runat="server" Width="193px" CssClass="inpText" __designer:wfdid="w336"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="statuse" runat="server" Width="91px" CssClass="inpText" __designer:wfdid="w337"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value=" ">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w339"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w340"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewLast" onclick="btnViewLast_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w341"></asp:ImageButton></TD></TR><TR><TD id="TD3" align=left colSpan=4 runat="server" Visible="true"><asp:ImageButton id="btnCheck" onclick="btnCheck_Click" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle" __designer:wfdid="w342" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnUncheck" onclick="btnUncheck_Click" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" __designer:wfdid="w343" Visible="False"></asp:ImageButton> <asp:ImageButton id="BtnPostSelected" onclick="BtnPostSelected_Click" runat="server" ImageUrl="~/Images/postselect.png" ImageAlign="AbsMiddle" __designer:wfdid="w344" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=4 runat="server" Visible="true"><STRONG><SPAN style="COLOR: #ff0033">Pemberitahuan : * Data transaksi dengan status in procces akan otomatis closed jika melewati tanggal sekarang, mohon untuk segera di posting</SPAN></STRONG></TD></TR><TR><TD align=left colSpan=4 runat="server"><asp:GridView id="gvmstcost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w345" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankno,cashbankoid,cashbankstatus,branch_code" PageSize="8">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" __designer:wfdid="w180"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Transaksi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="source" HeaderText="Sumber/Asal">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:ImageButton id="ImgPrint" runat="server" ImageUrl="~/Images/print.gif" __designer:wfdid="w181" ToolTip='<%# Eval("cashbankno") %>' OnClick="ImgPrint_Click"></asp:ImageButton>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=4 runat="server"><asp:Label id="Label13" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Mutation: Rp. " __designer:wfdid="w346" Visible="False"></asp:Label> <asp:Label id="lblgrandtotal" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w347" Visible="False"></asp:Label> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w348" CultureName="id-ID" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriode1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w349" CultureName="id-ID" MaskType="Date" Mask="99/99/9999" TargetControlID="txtPeriode2"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w350" TargetControlID="txtperiode1" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" Enabled="True"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w351" TargetControlID="txtperiode2" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" Enabled="True"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvmstcost"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                              <strong><span style="font-size: 9pt">  
                                  <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                  Daftar Mutation</span></strong>&nbsp;<strong><span style="font-size: 9pt">:.</span></strong>
                          
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel5" runat="server">
                                <contenttemplate>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD align=left><TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label8" runat="server" Font-Size="Small" Font-Bold="True" Text="Information :" __designer:wfdid="w441" Font-Underline="True"></asp:Label> </TD><TD style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w442" Visible="False"></asp:Label><asp:Label id="link_nobukti" runat="server" Font-Size="X-Small" __designer:wfdid="w443"></asp:Label><asp:Label id="lblIO" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" Text="" __designer:wfdid="w444" Visible="False"></asp:Label> <asp:Label id="trnid" runat="server" Font-Size="X-Small" __designer:wfdid="w443" Visible="False"></asp:Label></TD><TD style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099" align=left><asp:Label id="lblPOST" runat="server" Font-Size="X-Small" __designer:wfdid="w445" Visible="False"></asp:Label></TD><TD align=left colSpan=5></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #696969"><TD align=left><asp:Label id="Label6" runat="server" Width="45px" Font-Size="X-Small" Text="Cabang" __designer:wfdid="w446"></asp:Label></TD><TD align=left><asp:DropDownList id="dd_branch" runat="server" Width="127px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w447" OnSelectedIndexChanged="payflag_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><asp:DropDownList id="ddlcabangto" runat="server" Width="127px" CssClass="inpTextDisabled" Font-Bold="False" __designer:wfdid="w448" Visible="False" Enabled="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="Label12" runat="server" Width="45px" Font-Size="X-Small" Text="Tanggal" __designer:wfdid="w449"></asp:Label> <asp:Label id="Label22" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w450"></asp:Label></TD><TD align=left colSpan=5><asp:TextBox id="cashbankdate" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w451" Enabled="False"></asp:TextBox> <asp:ImageButton id="imbCBDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w452" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 10pt; COLOR: #696969"><TD align=left><asp:Label id="Label9" runat="server" Font-Size="X-Small" Text="Sumber/Asal" __designer:wfdid="w453"></asp:Label></TD><TD align=left><asp:DropDownList id="payflag" runat="server" Width="127px" CssClass="inpText" Font-Bold="False" __designer:wfdid="w454" OnSelectedIndexChanged="payflag_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Text="CASH" Value="CASH"></asp:ListItem>
<asp:ListItem Text="BANK" Value="BANK"></asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Label id="Label20" runat="server" Font-Size="X-Small" Text="No. Transaksi" __designer:wfdid="w455"></asp:Label></TD><TD align=left colSpan=5><asp:TextBox id="cashbankno" runat="server" Width="127px" CssClass="inpTextDisabled" __designer:wfdid="w456" Enabled="False"></asp:TextBox> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w457" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label16" runat="server" Width="76px" Font-Size="X-Small" Text="Cash Account" __designer:wfdid="w458"></asp:Label> <asp:Label id="Label23" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w459"></asp:Label></TD><TD align=left><asp:TextBox id="txtCoa" runat="server" Width="300px" CssClass="inpText " __designer:wfdid="w460"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCariCoa" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w461"></asp:ImageButton>&nbsp;<asp:ImageButton id="ImageButton2" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w462"></asp:ImageButton>&nbsp;<asp:Label id="lblCoa" runat="server" Text="0" __designer:wfdid="w463" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label17" runat="server" Width="77px" Font-Size="X-Small" Text="No. Transfer" __designer:wfdid="w464" Visible="False"></asp:Label></TD><TD align=left colSpan=5><asp:TextBox id="payrefno" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w465" Visible="False" MaxLength="20"></asp:TextBox>&nbsp;</TD></TR><TR><TD align=left></TD><TD align=left><asp:GridView id="GvCoa" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w380" PageSize="5" DataKeyNames="acctgoid,acctgcode,acctgdesc" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" GridLines="None" OnSelectedIndexChanged="GvCoa_SelectedIndexChanged" OnPageIndexChanging="GvCoa_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<FooterStyle ForeColor="#FFC080"></FooterStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvfooter" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White" CssClass="gvrowalternate"></AlternatingRowStyle>
</asp:GridView> </TD><TD align=left><asp:Label id="Label25" runat="server" Font-Size="X-Small" Text="Rate" __designer:wfdid="w467" Visible="False"></asp:Label> <asp:Label id="Label19" runat="server" Width="71px" Font-Size="X-Small" Text="Mata Uang" __designer:wfdid="w468" Visible="False"></asp:Label> <asp:Label id="lblAcctgOid" runat="server" Font-Size="X-Small" __designer:wfdid="w469" Visible="False"></asp:Label> <asp:Label id="AddToList" runat="server" Font-Size="X-Small" Text="Add To List" __designer:wfdid="w470" Visible="False"></asp:Label></TD><TD align=left colSpan=5><asp:TextBox id="currencyrate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w471" Visible="False" Enabled="False">0.00</asp:TextBox><asp:DropDownList id="currate" runat="server" Width="84px" CssClass="inpTextDisabled" __designer:wfdid="w472" Visible="False" Enabled="False" OnSelectedIndexChanged="currate_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="TD4" align=left runat="server" Visible="true"><asp:Label id="Label4" runat="server" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w473"></asp:Label></TD><TD id="TD6" align=left runat="server" Visible="true"><asp:TextBox id="cashbanknote" runat="server" Width="350px" CssClass="inpText" __designer:wfdid="w474" MaxLength="50"></asp:TextBox></TD><TD id="TD2" align=left runat="server" Visible="false"><asp:Label id="Label10" runat="server" Width="80px" Font-Size="X-Small" Text="Jatuh Tempo" __designer:wfdid="w475" Visible="False"></asp:Label></TD><TD id="TD1" align=left colSpan=5 runat="server" Visible="false"><asp:TextBox id="payduedate" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w476" Visible="False"></asp:TextBox> <asp:ImageButton id="btnDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" BackColor="White" __designer:wfdid="w477" Visible="False" BorderColor="White"></asp:ImageButton></TD></TR><TR><TD id="Td9" align=left colSpan=2 runat="server" Visible="true"><asp:Label id="Label11" runat="server" Font-Size="Small" Font-Bold="True" Text="Mutation Detail :" __designer:wfdid="w478" Font-Underline="True"></asp:Label><asp:TextBox id="cashbankgloid" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w479" Visible="False" MaxLength="10" Wrap="False" ReadOnly="True"></asp:TextBox><asp:Label id="createtime" runat="server" __designer:wfdid="w480" Visible="False"></asp:Label></TD><TD id="Td11" align=left runat="server"><asp:Label id="acctgcode" runat="server" Font-Size="X-Small" __designer:wfdid="w481" Visible="False"></asp:Label></TD><TD id="Td12" align=left colSpan=3 runat="server"><asp:DropDownList id="cashbankacctgoid" runat="server" CssClass="inpText" __designer:wfdid="w482" Visible="False"></asp:DropDownList></TD><TD id="Td16" align=left colSpan=1 runat="server"><asp:Label id="Label7" runat="server" Font-Size="X-Small" Text="Cost ID" __designer:wfdid="w483" Visible="False" Font-Italic="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 15px" id="Td5" align=left runat="server" Visible="true"><asp:Label id="Label15" runat="server" Width="99px" Font-Size="X-Small" Text="Mutation Account" __designer:wfdid="w484"></asp:Label></TD><TD style="HEIGHT: 15px" id="Td7" align=left runat="server" Visible="true"><asp:TextBox id="acctgoid" runat="server" Width="314px" CssClass="inpTextDisabled" __designer:wfdid="w485" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="BtnCari" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w486"></asp:ImageButton></TD><TD style="HEIGHT: 15px" id="Td8" align=left runat="server"><asp:Label id="Label14" runat="server" Font-Size="X-Small" Text="Amount" __designer:wfdid="w487"></asp:Label></TD><TD style="HEIGHT: 15px" id="Td10" align=left colSpan=4 runat="server"><asp:TextBox id="cashbankglamt" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w488" AutoPostBack="True" MaxLength="15">0</asp:TextBox></TD></TR><TR><TD id="Td17" align=left runat="server" Visible="true"><asp:Label id="lblItemDesc" runat="server" Width="64px" Font-Size="X-Small" Text="Keterangan" __designer:wfdid="w489"></asp:Label></TD><TD id="Td18" align=left runat="server" Visible="true"><asp:TextBox id="cashbankglnote" runat="server" Width="352px" CssClass="inpText" __designer:wfdid="w490" MaxLength="50"></asp:TextBox></TD><TD id="Td19" align=right runat="server"><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click1" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w491"></asp:ImageButton></TD><TD id="Td20" align=left colSpan=4 runat="server"><asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w492"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=9><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 186px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 91%; BACKGROUND-COLOR: beige"><asp:GridView id="GVDtlCost" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w407" DataKeyNames="cashbankglnote,cashbankglamt,acctgOid,acctgdesc,acctgcode,branch_code,seq" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="GVDtlCost_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AcctgDesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="400px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="400px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="branch_code" Visible="False"></asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No detail mutation."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></TD></TR><TR><TD align=left colSpan=9><asp:Label id="lblTotalMutation" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Mutation : " __designer:wfdid="w494"></asp:Label> <asp:Label id="amtcost" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w495"></asp:Label></TD></TR><TR><TD align=left colSpan=9><SPAN style="FONT-SIZE: 10pt; COLOR: #696969">Last update On </SPAN><asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w496"></asp:Label> By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w497"></asp:Label></TD></TR><TR><TD align=left colSpan=9><asp:ImageButton id="btnsave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w498"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:wfdid="w499"></asp:ImageButton> <asp:ImageButton id="btnPosting2" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w500"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w501"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w502"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w503" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=9><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w504" AssociatedUpdatePanelID="UpdatePanel5" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div6" class="progressBackgroundFilter"></DIV><DIV id="Div7" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w505"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="ce7" runat="server" __designer:wfdid="w506" TargetControlID="cashbankdate" PopupButtonID="imbCBDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w507" TargetControlID="payduedate" PopupButtonID="btnDueDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee3" runat="server" __designer:wfdid="w508" TargetControlID="payduedate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <asp:HiddenField id="HiddenField1" runat="server" __designer:wfdid="w509"></asp:HiddenField> <asp:HiddenField id="HiddenField2" runat="server" __designer:wfdid="w510"></asp:HiddenField> <ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w511" TargetControlID="cashbankdate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" ClearMaskOnLostFocus="true" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="Ftecashbankglamt" runat="server" __designer:wfdid="w512" TargetControlID="cashbankglamt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><asp:UpdatePanel id="UpdatePanel4" runat="server" __designer:wfdid="w513"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="500px" CssClass="modalBox" __designer:wfdid="w514" Visible="False" BorderStyle="Solid"><TABLE id="Table2" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD style="HEIGHT: 14px; TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=1><asp:Label id="Label1" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun" __designer:wfdid="w515"></asp:Label></TD></TR><TR><TD style="HEIGHT: 14px; TEXT-ALIGN: center" vAlign=middle colSpan=5 rowSpan=2><asp:Label id="lbldesc" runat="server" Font-Size="X-Small" Font-Bold="False" Text="Filter : " __designer:wfdid="w516"></asp:Label> <asp:DropDownList id="ddlFindAcctg" runat="server" CssClass="inpText" __designer:wfdid="w517"><asp:ListItem Value="acctgdesc">Description</asp:ListItem>
<asp:ListItem Value="acctgcode">Code</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFind" runat="server" CssClass="inpText" __designer:wfdid="w518"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w519"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w520"></asp:ImageButton></TD></TR><TR></TR><TR><TD colSpan=5><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div8"></DIV><TABLE class="gvhdr" width="100%"><TBODY><TR><TD style="WIDTH: 57px"></TD><TD style="WIDTH: 91px">Kode</TD><TD>Deskripsi</TD></TR></TBODY></TABLE><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 93%; BACKGROUND-COLOR: beige"><asp:GridView id="GvAccCost" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w521" PageSize="5" DataKeyNames="acctgoid,acctgcode,acctgdesc" CellPadding="4" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="GvAccCost_SelectedIndexChanged" ShowHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="#3366FF" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Deskripsi">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label18" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET> </TD></TR><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: center" colSpan=5>&nbsp; <asp:LinkButton id="LinkButton4" onclick="LinkButton4_Click" runat="server" __designer:wfdid="w522">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHidden" runat="server" __designer:wfdid="w523" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" __designer:wfdid="w524" TargetControlID="btnHidden" PopupControlID="Panel1" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="Label1"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <span style="font-size: 9pt">
                             <strong><span>
                                 <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                 Form Mutation</span></strong></span> <strong><span style="font-size: 9pt">:.</span></strong>
                            
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" DropShadow="True" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>