Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class Other_WaitingAction
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim cKoneksi As New Koneksi
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim sSql As String = ""
    Dim dgt As Integer
    Dim xreader As SqlDataReader
    Private cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lbldate.Text = Format(Now, "yyyy-MM-dd")
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim Position As String = Session("Position")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("Type") = ""
            Session("Position") = Position
            Response.Redirect("~\Other\WaitingAction.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        'lblWelcome.Text = "User Active, " & Session("UserID")

        Page.Title = CompnyName & " - Waiting for Approval"

        ibapprovesj.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to APPROVE this Purchase Requisition ?');")

        ibrejectsj.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REJECT this Purchase Requisition ?');")

        imgRejectAS.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to REJECT this Adjustment Stock Requisition ?');")

        ibapprovepreturn.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin melakukan approval terhadap purchase return ini?');")

        ibrejectpreturn.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin me-reject purchase return ini?');")
        ' tambahan tgl 3-04/2015
        imbRejectSO.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin me-reject sales order ini?');")

        imbRejectSDO.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin me-reject dilevery order ini?');")

        btnTwServiceApp.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin Approve Transfer Warehouse Service ini?');")

        btnTwServiceRej.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda ingin me-reject Transfer Warehouse Service ini?');")

        sj.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_trnsjalan b ON a.oid = b.sjloid WHERE a.approvaluser = '" & Session("UserID") & "' AND a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_TRNSJALAN' AND a.statusrequest = 'New' AND a.event = 'In Approval' AND b.sjlstatus = 'In Approval'")

        'COMMENT LAGI CZ DA BUG DB PO
        RealV.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN QL_trnrealisasi rm ON a.cmpcode=rm.cmpcode AND a.oid=rm.realisasioid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnrealisasi' AND statusrequest='New' AND event='In Approval' AND rm.realisasistatus='In Approval'")

        Dim sql As String = ""
        Dim sSqli As String = "Select potypeapp from QL_mstprof Where USERID='" & Session("UserID") & "'" : Dim sType As String = GetStrData(sSqli)

        If sType.ToString = "True" Then
            sql &= " AND po.typepo='Selisih'"
        Else
            sql &= " AND po.typepo='Normal'"
        End If

        Dim QlPO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_pomst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePO As String = ""
        Dim POCode() As String = QlPO.Split(",")
        For C1 As Integer = 0 To POCode.Length - 1
            If POCode(C1) <> "" Then
                sCodePO &= "'" & POCode(C1).Trim & "',"
            End If
        Next

        po.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_pomst po ON a.cmpcode=po.cmpcode AND a.oid=po.trnbelimstoid WHERE po.trnbelitype='Grosir' and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_pomst' AND statusrequest='New' AND event='In Approval' AND po.trnbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePO, sCodePO.Length - 1) & ") " & sql & "")

        Dim QlADJ As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnstockadj' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeAdj As String = ""
        Dim AdjCode() As String = QlADJ.Split(",")
        For C1 As Integer = 0 To AdjCode.Length - 1
            If AdjCode(C1) <> "" Then
                sCodeAdj &= "'" & AdjCode(C1).Trim & "',"
            End If
        Next

        stockadj.Text = GetScalar("SELECT count(*) FROM ql_approval a WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnstockadj' AND statusrequest='New' AND event='In Approval' AND a.branch_code IN (" & Left(sCodeAdj, sCodeAdj.Length - 1) & ")")

        creditnote.Text = GetScalar("SELECT count(*) FROM ql_approval a WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' And a.tablename='ql_creditnote' AND statusrequest='New' AND event='In Approval'")

        debitnote.Text = GetScalar("SELECT count(*) FROM ql_approval a WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_debitnote' AND statusrequest='New' AND event='In Approval'")

        Dim QlPOC As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNPOCLOSE' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePOC As String = ""
        Dim POCCode() As String = QlPOC.Split(",")
        For C1 As Integer = 0 To POCCode.Length - 1
            If POCCode(C1) <> "" Then
                sCodePOC &= "'" & POCCode(C1).Trim & "',"
            End If
        Next

        poclose.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_pomst po ON a.cmpcode=po.cmpcode AND a.oid=po.trnbelimstoid WHERE po.trnbelitype='Grosir' and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='QL_TRNPOCLOSE' AND po.trnbelires1='POCLOSE' AND statusrequest='New' AND event='In Approval' AND po.trnbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePOC, sCodePOC.Length - 1) & ") " & sql & "")

        po_r.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_pomst po ON a.cmpcode=po.cmpcode AND a.oid=po.trnbelimstoid WHERE po.trnbelitype='Retail' and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_pomst' AND statusrequest='New' AND event='In Approval' AND po.trnbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePO, sCodePO.Length - 1) & ") " & sql & "")

        Dim QlPDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjbelimst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePdO As String = ""
        Dim PdOCode() As String = QlPDO.Split(",")
        For C1 As Integer = 0 To PdOCode.Length - 1
            If PdOCode(C1) <> "" Then
                sCodePdO &= "'" & PdOCode(C1).Trim & "',"
            End If
        Next

        pdo.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_trnsjbelimst sjb ON a.cmpcode=sjb.cmpcode AND a.oid=sjb.trnsjbelioid WHERE a.approvaluser= '" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnsjbelimst' AND statusrequest='New' AND event='In Approval' AND sjb.trnsjbelistatus='In Approval' AND a.branch_code IN (" & Left(sCodePdO, sCodePdO.Length - 1) & ")")

        Dim QlSO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNORDERMST' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCode As String = "" : Dim sCodeSO As String = ""
        Dim SoCode() As String = QlSO.Split(",")
        For C1 As Integer = 0 To SoCode.Length - 1
            If SoCode(C1) <> "" Then
                sCodeSO &= "'" & SoCode(C1).Trim & "',"
            End If
        Next

        so.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN QL_trnordermst so ON a.cmpcode=so.cmpcode and so.branch_code= a.branch_code AND a.oid=so.ordermstoid WHERE so.branch_code IN (" & Left(sCodeSO, sCodeSO.Length - 1) & ") and a.approvaluser='" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='QL_TRNORDERMST' AND statusrequest='New' AND event='In Approval' AND so.trnorderstatus='In Approval'")

        Dim QlDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjjualmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeDO As String = ""
        Dim DOCode() As String = QlDO.Split(",")
        For C1 As Integer = 0 To DOCode.Length - 1
            If DOCode(C1) <> "" Then
                sCodeDO &= "'" & DOCode(C1).Trim & "',"
            End If
        Next

        sdo.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN ql_trnsjjualmst sjb ON a.cmpcode=sjb.cmpcode AND a.oid=sjb.trnsjjualmstoid WHERE a.approvaluser= '" & Session("UserID") & "' AND a.cmpcode='" & CompnyCode & "' and a.tablename='ql_trnsjjualmst' AND statusrequest='New' AND event='In Approval' AND sjb.trnsjjualstatus='In Approval' AND sjb.branch_code IN (" & Left(sCodeDO, sCodeDO.Length - 1) & ")")

        Dim QlPR As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbelireturmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePr As String = ""
        Dim PrCode() As String = QlPR.Split(",")
        For C1 As Integer = 0 To PrCode.Length - 1
            If PrCode(C1) <> "" Then
                sCodePr &= "'" & PrCode(C1).Trim & "',"
            End If
        Next

        preturn0.Text = GetScalar("SELECT count(*) FROM ql_approval a INNER JOIN QL_trnbelireturmst b ON a.cmpcode = b.cmpcode AND a.oid = b.trnbeliReturmstoid WHERE a.approvaluser = '" & Session("UserID") & "' AND a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trnbelireturmst' AND a.statusrequest = 'New' AND a.event='In Approval' AND b.trnbelistatus = 'In Approval' AND a.branch_code IN (" & Left(sCodePr, sCodePr.Length - 1) & ")")

        Dim QlTw As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trntrfmtrmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeTw As String = ""
        Dim TwCode() As String = QlTw.Split(",")
        For C1 As Integer = 0 To TwCode.Length - 1
            If TwCode(C1) <> "" Then
                sCodeTw &= "'" & TwCode(C1).Trim & "',"
            End If
        Next 

        tw0.Text = GetScalar("SELECT count(*) from QL_approval a inner join QL_trntrfmtrmst b on a.cmpcode = b.cmpcode and a.oid = b.trfmtrmstoid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trntrfmtrmst' AND statusrequest = 'New' AND event = 'In Approval' AND b.status = 'In Approval' AND a.branch_code IN (" & Left(sCodeTw, sCodeTw.Length - 1) & ")")

        Dim QlRnpg As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnnotahrretur' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeRnpg As String = ""
        Dim RnpgCode() As String = QlRnpg.Split(",")
        For C1 As Integer = 0 To RnpgCode.Length - 1
            If RnpgCode(C1) <> "" Then
                sCodeRnpg &= "'" & RnpgCode(C1).Trim & "',"
            End If
        Next

        rnpg0.Text = GetScalar("SELECT count(*) from QL_approval a inner join QL_trnnotahrretur b on a.cmpcode = b.cmpcode and a.oid = b.trnnotahrreturoid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode = '" & CompnyCode & "' and a.tablename = 'QL_trnnotahrretur' AND statusrequest = 'New' AND event = 'In Approval' AND b.trnnotahrreturstatus = 'In Approval' AND a.branch_code IN (" & Left(sCodeRnpg, sCodeRnpg.Length - 1) & ")")


        Dim QlTws As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trfwhservicemst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeTws As String = ""
        Dim TwsCode() As String = QlTws.Split(",")
        For C1 As Integer = 0 To TwsCode.Length - 1
            If TwsCode(C1) <> "" Then
                sCodeTws &= "'" & TwsCode(C1).Trim & "',"
            End If
        Next
        TwsCok.Text = GetScalar("SELECT count(*) from QL_approval a inner join ql_trfwhservicemst b on a.cmpcode = b.cmpcode and a.oid = b.Trfwhserviceoid WHERE a.approvaluser='" & Session("UserID") & "' AND a.cmpcode = '" & CompnyCode & "' and a.tablename = 'ql_trfwhservicemst' AND statusrequest = 'New' AND event = 'In Approval' AND b.Trfwhservicestatus = 'In Approval' AND a.branch_code IN (" & Left(sCodeTws, sCodeTws.Length - 1) & ")")


        If Session("branch_id") = "01" Or Session("branch_id") = "10" Then
            lkbPR.Visible = False
        Else
            lkbPR.Visible = False
        End If

        If lblError.Text = "showMsg" Then
            showValidation(refSO.Text)
            lblError.Text = ""
        End If

    End Sub

    Private Sub bindPO_R()
        ' Binding PO_R
        FillGV(gvPO, "SELECT po.trnbelimstoid pomstoid, po.trnbelipono pono, po.trnbelipodate podate, s.suppname, po.trnbelinote pohdrnote, ap.approvaloid,ap.requestuser,ap.requestdate FROM QL_pomst po,QL_APPROVAL ap,ql_mstsupp s WHERE po.cmpcode = ap.CMPCODE AND po.trnbelimstoid = ap.OID   and po.trnbelitype='Retail' AND ap.statusrequest = 'New' AND " & "(ap.EVENT = 'In Approval') AND (po.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'ql_pomst') AND (po.trnbelistatus = 'In Approval') AND po.trnsuppoid = s.suppoid ORDER BY ap.REQUESTDATE DESC", "PO")
    End Sub

    Function Get_USDRate() As Single
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim rate As Single

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "select top 1 isnull(rate2idrvalue,0) rate2idrvalue from QL_mstrate2 where rate2month = MONTH(getdate()) and rate2year = YEAR(GETDATE()) and currencyoid = 2"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            rate = rdr("rate2idrvalue")
        End While
        conn2.Close()
        Return rate
    End Function

    Private Sub bindPO()
        ' Binding PO
        Dim QlPO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_pomst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCode As String = "" : Dim sCodePO As String = ""
        Dim PoCode() As String = QlPO.Split(",")

        For C1 As Integer = 0 To PoCode.Length - 1
            If PoCode(C1) <> "" Then
                sCodePO &= "'" & PoCode(C1).Trim & "',"
            End If
        Next

        Dim sql As String = ""
        Dim sSqli As String = "Select potypeapp from QL_mstprof Where USERID='" & Session("UserID") & "'" : Dim sType As String = GetStrData(sSqli)

        If sType.ToString = "True" Then
            sql &= " AND po.typepo='Selisih'"
        Else
            sql &= " AND po.typepo='Normal'"
        End If

        sSql = "SELECT po.branch_code,po.trnbelimstoid pomstoid, po.trnbelipono pono, po.trnbelipodate podate, s.suppname, po.trnbelinote pohdrnote, ap.approvaloid,ap.requestuser,ap.requestdate FROM QL_pomst po Inner Join QL_APPROVAL ap ON po.trnbelimstoid = ap.OID AND po.branch_code=ap.branch_code Inner Join ql_mstsupp s on po.trnsuppoid = s.suppoid WHERE po.cmpcode = ap.CMPCODE And po.trnbelitype='Grosir' AND ap.statusrequest = 'New' AND " & "(ap.EVENT = 'In Approval') AND (po.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'ql_pomst') AND (po.trnbelistatus = 'In Approval') AND po.branch_code IN (" & Left(sCodePO, sCodePO.Length - 1) & ") " & sql & "ORDER BY ap.REQUESTDATE DESC"
        FillGV(gvPO, sSql, "PO")
    End Sub

    Private Sub bindAS()
        Dim QlADJ As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnstockadj' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeAdj As String = ""
        Dim AdjCode() As String = QlADJ.Split(",")
        For C1 As Integer = 0 To AdjCode.Length - 1
            If AdjCode(C1) <> "" Then
                sCodeAdj &= "'" & AdjCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select distinct ap.approvaloid, resfield1 draftno, convert(varchar(10), ad.stockadjdate, 101) adjdate, ad.adjtype type, ap.REQUESTUSER, ap.REQUESTDATE,ad.branch_code,bc.gendesc Cabang from QL_trnstockadj ad inner join QL_APPROVAL ap on ap.OID=ad.resfield1 and ap.CMPCODE=ad.cmpcode Inner Join QL_mstgen bc ON bc.gencode=ad.branch_code AND bc.gengroup='CABANG' Where ap.TABLENAME='ql_trnstockadj' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' And ad.cmpcode='" & CompnyCode & "' And ap.APPROVALUSER='" & Session("UserID") & "' And ad.stockadjstatus='In Approval' AND ad.branch_code IN (" & Left(sCodeAdj, sCodeAdj.Length - 1) & ") order by ap.REQUESTDATE DESC"
        FillGV(gvASMst, sSql, "AS")
    End Sub

    Private Sub bindCN()
        Dim jbt As String = GetStrData("Select POSITION from QL_MSTPROF Where BRANCH_CODE='" & Session("branch_id") & "' AND USERID='" & Session("UserID") & "'")
        Dim fCb As String = ""
        If jbt.ToString.ToUpper <> "ANALISA KEU. PUSAT" Then
            fCb &= "AND ap.branch_code='" & Session("branch_id") & "'"
        End If
        FillGV(gvCNMst, "Select distinct ap.approvaloid, cn.oid draftno, convert(varchar(10), cn.tgl, 103) tgl, cn.reftype, ap.REQUESTUSER, ap.REQUESTDATE, cn.amount from ql_creditnote cn inner join QL_APPROVAL ap on ap.OID=cn.oid and ap.CMPCODE=cn.cmpcode Where ap.TABLENAME='ql_creditnote' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' And cn.cmpcode like '%" & CompnyCode & "%' " & fCb & " And ap.APPROVALUSER='" & Session("UserID") & "' And cn.cnstatus='In Approval' order by ap.REQUESTDATE DESC", "CN")
    End Sub

    Private Sub bindDN()
        Dim jbt As String = GetStrData("Select POSITION from QL_MSTPROF Where BRANCH_CODE='" & Session("branch_id") & "' AND USERID='" & Session("UserID") & "'")
        Dim fCb As String = ""
        If jbt.ToString.ToUpper <> "ANALISA KEU. PUSAT" Then
            fCb &= "AND ap.branch_code='" & Session("branch_id") & "'"
        End If
        FillGV(gvDNMst, "Select distinct ap.approvaloid, cn.oid draftno, convert(varchar(10), cn.tgl, 103) tgl, cn.reftype, ap.REQUESTUSER, ap.REQUESTDATE, cn.amount from ql_debitnote cn inner join QL_APPROVAL ap on ap.OID=cn.oid and ap.CMPCODE=cn.cmpcode Where ap.TABLENAME='ql_debitnote' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' And cn.cmpcode like '%" & CompnyCode & "%' " & fCb & " And ap.APPROVALUSER='" & Session("UserID") & "' And cn.dnstatus='In Approval' order by ap.REQUESTDATE DESC", "DN")
    End Sub

    Private Sub bindPoClose()
        Dim QlPO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNPOCLOSE' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCode As String = "" : Dim sCodePO As String = ""
        Dim PoCode() As String = QlPO.Split(",")

        For C1 As Integer = 0 To PoCode.Length - 1
            If PoCode(C1) <> "" Then
                sCodePO &= "'" & PoCode(C1).Trim & "',"
            End If
        Next

        Dim sql As String = ""
        Dim sSqli As String = "Select potypeapp from QL_mstprof Where USERID='" & Session("UserID") & "'"
        Dim sType As String = GetStrData(sSqli)
        If sType.ToString = "True" Then
            sql &= " AND po.typepo='Selisih'"
        Else
            sql &= " AND po.typepo='Normal'"
        End If

        sSql = "SELECT po.trnbelimstoid pomstoid, po.trnbelipono pono, po.trnbelipodate podate, s.suppname, po.closenote pohdrnote, ap.approvaloid,ap.requestuser, ap.requestdate,po.branch_code FROM QL_pomst po INNER JOIN QL_APPROVAL ap ON po.cmpcode = ap.CMPCODE AND po.trnbelimstoid = ap.OID INNER JOIN ql_mstsupp s ON s.suppoid=po.trnsuppoid WHERE po.trnbelitype='Grosir' AND ap.statusrequest = 'New' AND (ap.EVENT = 'In Approval') AND po.trnbelires1='POCLOSE' AND (po.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'QL_TRNPOCLOSE') AND (po.trnbelistatus = 'IN APPROVAL') AND po.trnsuppoid = s.suppoid AND po.branch_code IN (" & Left(sCodePO, sCodePO.Length - 1) & ") " & sql & " ORDER BY ap.REQUESTDATE DESC"
        FillGV(gvPO, sSql, "POCLOSE")
    End Sub

    Private Sub bindPO_RH()
        ' Binding PO
        FillGV(gvPO, "SELECT po.trnbelimstoid pomstoid, po.trnbelipono pono, po.trnbelipodate podate, s.suppname, po.trnbelinote pohdrnote, ap.approvaloid,ap.requestuser, " & _
            "ap.requestdate FROM QL_pomst po,QL_APPROVAL ap,ql_mstsupp s WHERE po.cmpcode = ap.CMPCODE AND po.trnbelimstoid = ap.OID and po.trnbelitype='Retail-Holding'   AND " & _
            "ap.statusrequest = 'New' AND " & "(ap.EVENT = 'In Approval') AND (po.cmpcode LIKE '" & CompnyCode & _
            "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'ql_pomst') " & _
            "AND (po.trnbelistatus = 'In Approval') AND po.trnsuppoid = s.suppoid  ORDER BY ap.REQUESTDATE DESC", "PO")
    End Sub

    Private Sub bindSQ()
        ' Binding SO
        FillGV(gvSQ, "SELECT so.ordermstoid quotoid, so.orderno quotno, so.trnorderdate quotdate, c.custname, so.trnordernote quotnote, ap.approvaloid,ap.requestuser, ap.requestdate requestdate,so.periodacctg FROM QL_trnordermst so,QL_APPROVAL ap,ql_mstcust c WHERE so.cmpcode = ap.CMPCODE AND so.ordermstoid = ap.OID AND ap.statusrequest = 'New' AND " & "(ap.EVENT = 'In Approval') AND (so.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'ql_trnordermstpj') AND (so.trnorderstatus = 'In Approval') and so.trnOrdertype = 'PROJECK' AND so.trncustoid = c.custoid ORDER BY ap.REQUESTDATE DESC", "SOJ")
    End Sub

    Private Sub bindSO()
        Dim QlSO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNORDERMST' And approvaluser='" & Session("UserID") & "' Order By approvallevel")

        Dim sCode As String = "" : Dim sCodeSO As String = ""
        Dim SoCode() As String = QlSO.Split(",")
        For C1 As Integer = 0 To SoCode.Length - 1
            If SoCode(C1) <> "" Then
                sCodeSO &= "'" & SoCode(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT so.branch_code,so.ordermstoid orderoid, so.orderno, so.trnorderdate orderdate, c.custname, so.trnordernote ordernote, ap.approvaloid,ap.requestuser, ap.requestdate,so.periodacctg,(select cb.gendesc From QL_mstgen cb Where cb.gencode=so.branch_code AND cb.gengroup='CABANG') Cabang FROM QL_trnordermst so Inner Join QL_APPROVAL ap ON so.ordermstoid = ap.OID AND ap.branch_code = so.branch_code Inner Join ql_mstcust c ON so.trncustoid = c.custoid AND so.branch_code=c.branch_code WHERE so.cmpcode = ap.CMPCODE AND ap.statusrequest = 'New' AND " & "(ap.EVENT = 'In Approval') AND (so.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'ql_trnordermst') AND (so.trnorderstatus = 'In Approval') and so.branch_code IN (" & Left(sCodeSO, sCodeSO.Length - 1) & ") ORDER BY ap.REQUESTDATE DESC"
        FillGV(gvSO, sSql, "SO")
    End Sub

    Private Sub bindSDO()
        Dim QlDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjjualmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCode As String = "" : Dim sCodeDO As String = ""
        Dim DoCode() As String = QlDO.Split(",")
        For C1 As Integer = 0 To DoCode.Length - 1
            If DoCode(C1) <> "" Then
                sCodeDO &= "'" & DoCode(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT so.branch_code,jj.trnsjjualmstoid salesdeliveryoid, jj.trnsjjualno salesdeliveryno, jj.trnsjjualdate salesdeliverydate, (SELECT c.custname FROM QL_mstcust c Where c.custoid=so.trncustoid AND so.branch_code=c.branch_code) custname, ap.approvaloid,ap.requestuser, ap.requestdate FROM ql_trnsjjualmst jj inner JOIN QL_APPROVAL ap ON ap.OID=jj.trnsjjualmstoid AND jj.branch_code=ap.branch_code INNER JOIN QL_trnordermst so on so.orderno=jj.orderno AND so.branch_code=jj.branch_code AND so.ordermstoid=jj.ordermstoid WHERE(jj.cmpcode = ap.CMPCODE And jj.cmpcode = so.cmpcode) AND jj.orderno=so.orderno AND jj.trnsjjualmstoid = ap.OID AND ap.statusrequest = 'New' AND (ap.EVENT = 'In Approval') AND (jj.cmpcode ='" & CompnyCode & "') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'QL_trnsjjualmst') AND (jj.trnsjjualstatus = 'In Approval') And jj.branch_code IN (" & Left(sCodeDO, sCodeDO.Length - 1) & ") ORDER BY ap.REQUESTDATE DESC"
        FillGV(gvSDO, sSql, "SDO")
    End Sub

    Private Sub bindPDO()
        ' Binding PDO
        Dim QlPDO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjbelimst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePdO As String = ""
        Dim PdOCode() As String = QlPDO.Split(",")
        For C1 As Integer = 0 To PdOCode.Length - 1
            If PdOCode(C1) <> "" Then
                sCodePdO &= "'" & PdOCode(C1).Trim & "',"
            End If
        Next
        sSql = "SELECT jb.trnsjbelioid,jb.trnsjbelino,jb.trnsjbelidate trnsjreceivedate,s.suppname,ap.approvaloid, ap.requestuser,ap.requestdate requestdate FROM QL_trnsjbelimst jb Inner Join QL_APPROVAL ap on jb.branch_code=ap.branch_code AND jb.trnsjbelioid=ap.OID Inner Join QL_pomst po On po.trnbelipono=jb.trnbelipono Inner Join QL_mstsupp s ON s.suppoid=po.trnsuppoid WHERE jb.cmpcode=ap.CMPCODE AND jb.cmpcode=po.cmpcode AND ap.statusrequest='New' AND (ap.EVENT='In Approval') AND (jb.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER= '" & Session("UserID") & "' ) AND (ap.TABLENAME='ql_trnsjbelimst') AND (jb.trnsjbelistatus='In Approval') AND ap.branch_code IN (" & Left(sCodePdO, sCodePdO.Length - 1) & ") ORDER BY ap.REQUESTDATE DESC"
        FillGV(gvPDO, sSql, "PDO")
    End Sub

    Function checkDataWeb(ByVal tablename As String, ByVal oid As String) As DataTable
        Dim ssql As String = "select CMPCODE,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME," & _
             "OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval where CmpCode='" & CompnyCode & "' and ApprovalUser='" & Session("UserID") & "' and STATUSREQUEST LIKE 'Send' " & _
             " AND TABLENAME='" & tablename & "' and OID=" & oid & " and EVENT='In Approval'"
        Return cKoneksi.ambiltabel(ssql, "APPROVALWEB")
    End Function

    Private Sub UpdateMst(ByVal tablename As String, ByVal statusCol As String, ByVal primaryCol As String, ByVal primaryval As Integer, ByVal ApprovalVal As String)
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Dim strSQL As String
        Try

            'If Session("oid") <> Nothing Or Session("oid") <> "" Then
            strSQL = "UPDATE " & tablename & " SET " & statusCol & "='" & ApprovalVal & "' WHERE " & primaryCol & "=" & primaryval & " and cmpcode='" & CompnyCode & "'"
            objCmd.CommandText = strSQL
            objCmd.ExecuteNonQuery()
            'End If

            objTrans.Commit()
        Catch ex As OleDb.OleDbException
            objTrans.Rollback()
        Finally
            objCmd.Connection.Close()
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data has been saved."))

        End Try
    End Sub

    Protected Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub imbApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("prmstoid")) Then
            showMessage("Please select a PR first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("prmstoid") = "" Then
                showMessage("Please select a PR first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
         
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE ql_prmst SET prstatus = 'Approved',approvaluser='" & Session("UserID") & "',approvaldatetime=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND prmstoid = '" & Session("prmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
         
    End Sub

    Protected Sub imbReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("prmstoid")) Then
            showMessage("Please select a PR first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("prmstoid") = "" Then
                showMessage("Please select a PR first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If 
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim lblStsPo As String = ""

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PR
            sSql = "UPDATE ql_prmst SET prstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND prmstoid = '" & Session("prmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
         
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Protected Sub lkbPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPO.Click
        setPOasActive()
    End Sub

    Protected Sub lkbAS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAS.Click
        setASasActive()
    End Sub

    Protected Sub lkb_POClose_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkb_POClose.Click
        setPOCLOSE()
    End Sub

    Private Sub setPOasActive()
        bindPO() : trnbelipono.Text = "" : trnbelipodate.Text = ""
        suppname.Text = "" : trnbelinote.Text = "" : poidentifierno.Text = ""
        trnbelitype.Text = "" : labelPO.Text = "Purchase Order List :"
        pocreate.Text = "" : posentapproval.Text = ""
        Session("approvaloid") = Nothing : Session("pomstoid") = Nothing
        MultiView1.SetActiveView(View5)
    End Sub

    Private Sub setASasActive()
        bindAS() : draftno.Text = "" : adjdate.Text = ""
        Type.Text = "" : totaladj.Text = "" : lastupdby.Text = ""
        lastupdon.Text = "" : lblas2.Text = "Adjustment Stock List :"
        Session("approvaloid") = Nothing : Session("stockadjoid") = Nothing
        MultiView1.SetActiveView(View30)
    End Sub

    Private Sub setCNasActive()
        bindCN() : oid.Text = "" : tgl.Text = ""
        reftype.Text = "" : amount.Text = "" : updusercn.Text = ""
        updtimecn.Text = "" : lblCN2.Text = "Credit Note List:"
        branch_code_cn.Text = "" : cust_supp_oid.Text = "" : trnjualbelioid.Text = ""
        trnjualbelino.Text = "" : cnstatus.Text = "" : coa_credit.Text = ""
        coa_debet.Text = ""
        Session("approvaloid") = Nothing : Session("oid") = Nothing
        MultiView1.SetActiveView(View36)
    End Sub

    Private Sub setDNasActive()
        bindDN() : oiddn.Text = "" : tgldn.Text = ""
        reftypedn.Text = "" : amountdn.Text = "" : upduserdn.Text = ""
        updtimedn.Text = "" : lblDN2.Text = "Debit Note List:"
        branch_code_dn.Text = "" : cust_supp_oiddn.Text = "" : trnjualbelioiddn.Text = ""
        trnjualbelinodn.Text = "" : cnstatusdn.Text = "" : coa_creditdn.Text = ""
        coa_debetdn.Text = ""
        Session("approvaloid") = Nothing : Session("oiddn") = Nothing
        MultiView1.SetActiveView(View38)
    End Sub

    Private Sub setPOCLOSE()
        bindPoClose() : trnbelipono.Text = "" : trnbelipodate.Text = ""
        suppname.Text = "" : trnbelinote.Text = "" : poidentifierno.Text = ""
        trnbelitype.Text = "" : labelPO.Text = "PO Close List :" : Label3.Text = "PO Close"
        Session("approvaloid") = Nothing : Session("pomstoid") = Nothing
        MultiView1.SetActiveView(View5)
    End Sub

    Protected Sub lkbSelectPO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("pomstoid") = sender.ToolTip : MultiView1.SetActiveView(View6)
            Session("approvaloid") = sender.CommandArgument
            Session("CodeC") = sender.CommandName

            Dim bln1 As String = Month(DateAdd(DateInterval.Month, -1, GetServerTime()))
            Dim bln2 As String = Month(DateAdd(DateInterval.Month, -2, GetServerTime()))
            Dim bln3 As String = Month(DateAdd(DateInterval.Month, -3, GetServerTime()))

            Dim thn1 As String = Year(DateAdd(DateInterval.Month, -1, GetServerTime()))
            Dim thn2 As String = Year(DateAdd(DateInterval.Month, -2, GetServerTime()))
            Dim thn3 As String = Year(DateAdd(DateInterval.Month, -3, GetServerTime()))

            If bln1 <= 0 Then : bln1 = bln1 + 12 : End If
            If bln2 <= 0 Then : bln2 = bln2 + 12 : End If
            If bln3 <= 0 Then : bln3 = bln3 + 12 : End If
            GenerateBulan(bln1, "satu")
            GenerateBulan(bln2, "dua")
            GenerateBulan(bln3, "tiga")

            gvPODetail.Columns(5).HeaderText = lblbln1.Text
            gvPODetail.Columns(4).HeaderText = lblbln2.Text
            gvPODetail.Columns(3).HeaderText = lblbln3.Text

            gvPODetail.Columns(14).HeaderText = lblbln1.Text
            gvPODetail.Columns(13).HeaderText = lblbln2.Text
            gvPODetail.Columns(12).HeaderText = lblbln3.Text

            sSql = "SELECT trnbelipono pono, case trntaxpct when 0 then 'No' else 'Yes' end as Tax, amtdischdr, amtbelinetto, CONVERT(varchar(10),trnbelipodate,103) podate, s.suppname, closenote pohdrnote, market, trnbelistatus AS PoStatus, trnbelires1, trnbelitype, periodacctg, pom.branch_code cabangAsal, pom.tocabang KeCabang, pom.typepo TypePO, pom.upduser, pom.updtime, (Select kc.gendesc from QL_mstgen kc Where kc.gencode=pom.tocabang AND kc.gengroup='CABANG') Tujuan FROM ql_pomst pom INNER JOIN ql_mstsupp s ON pom.cmpcode=s.cmpcode AND pom.cmpcode='" & CompnyCode & "' AND pom.trnsuppoid=s.suppoid WHERE trnbelimstoid='" & Session("pomstoid") & "'"
            Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_pomst")
            AsCabang.Text = dt.Rows(0).Item("cabangAsal").ToString
            KeCabang.Text = dt.Rows(0).Item("KeCabang").ToString
            trnbelipono.Text = dt.Rows(0).Item("pono").ToString
            trnbelipodate.Text = dt.Rows(0).Item("podate")
            suppname.Text = dt.Rows(0).Item("suppname").ToString
            Tujuan.Text = dt.Rows(0).Item("Tujuan").ToString
            trnbelinote.Text = dt.Rows(0).Item("pohdrnote").ToString
            taxpo.Text = dt.Rows(0).Item("Tax")
            StatusPO.Text = dt.Rows(0).Item("PoStatus").ToString
            dischdrpo.Text = ToMaskEdit(dt.Rows(0).Item("amtdischdr"), 3)
            amtbelinetto.Text = ToMaskEdit(dt.Rows(0).Item("amtbelinetto"), 3)
            trnbelitype.Text = dt.Rows(0).Item("trnbelitype")
            market.Text = dt.Rows(0).Item("market")
            pores1.Text = dt.Rows(0).Item("trnbelires1")
            TypePO.Text = dt.Rows(0).Item("TypePO").ToString
            Dim periodacctg As String = GetPeriodAcctg(GetServerTime())
            pocreate.Text = "<B>PO Last Update By</B> " & dt.Rows(0).Item("upduser").ToString & " <B>On</B> " & dt.Rows(0).Item("updtime").ToString
            posentapproval.Text = ""

            Dim xDT As DataTable = cKoneksi.ambiltabel("SELECT ISNULL(po.digit,2) as digitX FROM QL_pomst po WHERE trnbelimstoid='" & Session("pomstoid") & "'", "tbSmt")
            dgt = xDT.Rows(0).Item("digitX")

            Dim pCls As String = "", cb As String = ""
            If pores1.Text.ToUpper = "POCLOSE" Then
                pCls = "-ISNULL((Select SUM(qty) FROM ql_trnsjbelidtl sjd INNER JOIN ql_trnsjbelimst sjm ON sjm.trnsjbelioid=sjd.trnsjbelioid AND sjd.trnbelidtloid=pod.trnbelidtloid),0.00)"
                btnRevisePO.Visible = False
            Else
                Label3.Text = "Purchase Order Data :"
                btnRevisePO.Visible = True
            End If

            If TypePO.Text = "Selisih" Then
                cb &= " AND con.branch_code='" & AsCabang.Text & "'"
            End If

            gvPODetail.Columns(15).HeaderText = "Stok " & Tujuan.Text & "" 
            sSql = "SELECT po.tocabang, pod.trnbelidtlseq podtlseq, i.itemdesc + ' - ' + merk AS matlongdesc, i.itemrefoid, i.itemoid, 0 bln1, 0 bln2, 0 bln3, 0 jbln1, 0 jbln2, 0 jbln3, pod.trnbelidtlqty podtlqqty, 'BUAH' gendesc, pod.trnbelidtlunit as unit, pod.trnbelidtlqty podtlqty, pod.trnbelidtlprice podtlprice, (pod.trnbelidtlqty * pod.trnbelidtlprice) - (amtdisc) totalprice, amtdisc poamtdltdisc, amtdisc2 voucher, pod.trnbelidtlnote pobelidtlnote, 0.0 QtyAkhir, 0.0 QtyCabang, 0.0 LastPrice FROM QL_pomst po INNER JOIN QL_podtl pod ON po.trnbelimstoid = pod.trnbelimstoid AND po.branch_code=pod.branch_code INNER JOIN QL_mstitem i on i.itemoid=pod.itemoid WHERE (po.cmpcode = '" & CompnyCode & "') AND (po.trnbelimstoid=" & Session("pomstoid") & ") AND pod.trnbelidtlstatus <> 'completed'"
            Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_podtl")
            For C1 As Integer = 0 To dtv.Rows.Count - 1
                dtv.Rows(C1).Item("bln1") = ToDouble(GetScalar("select v.jualqty-v.returqty from view_JualBersih_All_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln1 & " and v.thn=" & thn1 & ""))

                dtv.Rows(C1).Item("bln2") = ToDouble(GetScalar("select v.jualqty-v.returqty from view_JualBersih_All_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln2 & " and v.thn=" & thn2 & ""))
                dtv.Rows(C1).Item("bln3") = ToDouble(GetScalar("select v.jualqty-v.returqty from view_JualBersih_All_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln3 & " and v.thn=" & thn3 & ""))

                dtv.Rows(C1).Item("jbln1") = ToDouble(GetScalar("select v.jualqty-v.returqty from view_JualBersih_Per_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln1 & " and v.thn=" & thn1 & " and v.branch_code='" & dtv.Rows(C1)("tocabang") & "'"))

                dtv.Rows(C1).Item("jbln2") = ToDouble(GetScalar("select v.jualqty-v.returqty from view_JualBersih_Per_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln2 & " and v.thn=" & thn2 & " and v.branch_code='" & dtv.Rows(C1)("tocabang") & "'"))

                dtv.Rows(C1).Item("jbln3") = ToDouble(GetStrData("select v.jualqty-v.returqty from view_JualBersih_Per_Branch v where v.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") AND v.bln=" & bln3 & " and v.thn=" & thn3 & " and v.branch_code='" & dtv.Rows(C1)("tocabang") & "'"))

                dtv.Rows(C1).Item("LastPrice") = ToDouble(GetStrData("Select top 1 d.trnbelidtlprice from QL_podtl d INNER JOIN QL_pomst po ON po.trnbelimstoid=d.trnbelimstoid Where po.trnbelistatus='Approved' AND d.itemoid=ISNULL((select i2.itemoid FROM QL_mstitem i2 WHERE i2.itemoid=" & dtv.Rows(C1)("itemrefoid") & "), " & dtv.Rows(C1)("itemoid") & ") Order By d.updtime DESC"))

                dtv.Rows(C1).Item("QtyAkhir") = ToDouble(GetStrData("Select ISNULL(SUM(QtyStok), 0.00) from (Select ISNULL(SUM(QtyStok), 0.00) QtyStok, periodacctg From View_ConStok con Where con.refoid=" & dtv.Rows(C1)("itemoid") & " Group bY periodacctg UNION ALL Select ISNULL(SUM(QtyStok),0.00) QtyStok, periodacctg From View_ConStok con Where con.refoid=(Select c.itemrefoid from QL_mstitem c Where c.itemoid=" & dtv.Rows(C1)("itemoid") & " AND c.flag_item='CLONING') Group bY periodacctg) con Where periodacctg='" & periodacctg & "'"))
                dtv.Rows(C1).Item("QtyCabang") = ToDouble(GetStrData("Select ISNULL(SUM(QtyStok), 0.00) from (Select ISNULL(SUM(QtyStok), 0.00) QtyStok, periodacctg From View_ConStok con Where con.refoid=" & dtv.Rows(C1)("itemoid") & " AND con.branch_code='" & dtv.Rows(C1)("tocabang") & "' Group By periodacctg UNION ALL Select ISNULL(SUM(QtyStok),0.00) QtyStok, periodacctg From View_ConStok con Where con.refoid=(Select c.itemrefoid from QL_mstitem c Where c.itemoid=" & dtv.Rows(C1)("itemoid") & " AND c.flag_item='CLONING') AND con.branch_code='" & dtv.Rows(C1)("tocabang") & "' Group By periodacctg) con Where periodacctg='" & periodacctg & "'"))
            Next
            dtv.AcceptChanges()
            gvPODetail.DataSource = dtv
            gvPODetail.DataBind() : gvPODetail.Visible = True
            'sSql = "SELECT pod.trnbelidtlseq podtlseq, i.itemdesc + ' - ' + merk AS matlongdesc, i.itemrefoid, i.itemoid, ISNULL(bln1.jualqty-bln1.returqty,0.0) bln1, ISNULL(bln2.jualqty-bln2.returqty,0.0) bln2, ISNULL(bln3.jualqty-bln3.returqty,0.0) bln3, ISNULL(jbln1.jualqty-jbln1.returqty,0.0) jbln1, ISNULL(jbln2.jualqty-jbln2.returqty,0.0) jbln2, ISNULL(jbln3.jualqty-jbln3.returqty,0.0) jbln3, pod.trnbelidtlqty podtlqqty, 'BUAH' gendesc, pod.trnbelidtlunit as unit, pod.trnbelidtlqty podtlqty, pod.trnbelidtlprice podtlprice, (pod.trnbelidtlqty * pod.trnbelidtlprice) - (amtdisc) totalprice, amtdisc poamtdltdisc, amtdisc2 voucher, pod.trnbelidtlnote pobelidtlnote, ISNULL((Select ISNULL(SUM(QtyStok), 0.00) From View_ConStok con Where con.refoid=i.itemoid AND periodacctg='" & periodacctg & "'), 0.00)+ISNULL((Select ISNULL(SUM(QtyStok),0.00) From View_ConStok con Where con.refoid=(Select c.itemrefoid from QL_mstitem c Where c.itemoid=pod.itemoid AND c.flag_item='CLONING') AND periodacctg='" & periodacctg & "'), 0.00) QtyAkhir, ISNULL((Select ISNULL(SUM(QtyStok),0.00) From View_ConStok con Where con.refoid=i.itemoid AND periodacctg='" & periodacctg & "' AND con.branch_code=po.tocabang),0.00) + ISNULL((Select ISNULL(SUM(QtyStok),0.00) From View_ConStok con Where con.refoid=(Select itemrefoid from QL_mstitem c Where c.itemoid=pod.itemoid AND c.flag_item='CLONING') AND periodacctg='" & periodacctg & "' AND con.branch_code=po.tocabang), 0.00) QtyCabang, Isnull((Select top 1 dt.trnbelidtlprice From QL_podtl dt Inner Join QL_pomst pom ON pom.trnbelimstoid=dt.trnbelimstoid Where dt.itemoid=(Case When i.flag_item='CLONING' Then i.itemrefoid Else i.itemoid End) AND pom.trnbelistatus='Approved' Order by pom.updtime Desc),0.00) LastPrice FROM QL_pomst po " & _
            '"INNER JOIN QL_podtl pod ON po.trnbelimstoid = pod.trnbelimstoid AND po.branch_code=pod.branch_code " & _
            '"INNER JOIN QL_mstitem i on i.itemoid=pod.itemoid " & _
            '"left join view_JualBersih_All_Branch bln1 on bln1.itemoid=(Case When i.itemrefoid=0 Then pod.itemoid Else i.itemrefoid End) AND bln1.bln=" & bln1 & " And bln1.thn=" & thn1 & " " & _
            '"left join view_JualBersih_All_Branch bln2 on bln2.itemoid=pod.itemoid AND bln2.itemoid=(Case When i.itemrefoid=0 Then i.itemoid Else i.itemrefoid End) AND bln2.bln=" & bln2 & " And bln2.thn=" & thn2 & " " & _
            '"left join view_JualBersih_All_Branch bln3 on bln3.itemoid=pod.itemoid AND bln3.itemoid=(Case When i.itemrefoid=0 Then i.itemoid Else i.itemrefoid End) AND bln3.bln=" & bln3 & " And bln3.thn=" & thn3 & " " & _
            '"left join view_JualBersih_Per_Branch jbln1 on jbln1.itemoid=(Case When i.itemrefoid=0 Then i.itemoid Else i.itemrefoid End) AND jbln1.bln=" & bln1 & " And jbln1.thn=" & thn1 & " AND jbln1.branch_code=po.tocabang " & _
            '"left join view_JualBersih_Per_Branch jbln2 on jbln2.itemoid=(Case When i.itemrefoid=0 Then i.itemoid Else i.itemrefoid End) AND jbln2.bln=" & bln2 & " And jbln2.thn=" & thn2 & " AND jbln2.branch_code=po.tocabang " & _
            '"left join view_JualBersih_Per_Branch jbln3 on jbln3.itemoid=(Case When i.itemrefoid=0 Then i.itemoid Else i.itemrefoid End) AND jbln3.bln=" & bln3 & " And jbln3.thn=" & thn3 & " AND jbln3.branch_code=po.tocabang " & _
            '"WHERE (po.cmpcode = '" & CompnyCode & "') AND (po.trnbelimstoid=" & Session("pomstoid") & ") AND pod.trnbelidtlstatus <> 'completed'"
            'FillGV(gvPODetail, sSql, "ql_podtl") 
        Catch ex As Exception
            showMessage(ex.ToString & "<br >" & sSql, "ERROR,,!!", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub lkbSelectAS_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("draftno") = sender.ToolTip : MultiView1.SetActiveView(View31)
        Session("approvaloid") = sender.CommandArgument
        Session("Cabang") = sender.CommandName
        sSql = "Select distinct ad.branch_code,ad.stockadjstatus, count(ad.stockadjoid) as totaladj, ad.upduser lastupdby, CONVERT(varchar(10), ad.updtime, 101) lastupdon,ap.approvaloid, ad.resfield1 draftno, ad.stockadjdate adjdate, ad.adjtype type, ap.REQUESTUSER, ap.REQUESTDATE from QL_trnstockadj ad inner join QL_APPROVAL ap on ap.OID=ad.resfield1 and ap.CMPCODE=ad.cmpcode where ap.TABLENAME='ql_trnstockadj' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' and ad.cmpcode like '%" & CompnyCode & "%' AND ad.branch_code='" & Session("Cabang") & "' And ap.APPROVALUSER='" & Session("UserID") & "' and ad.stockadjstatus='In Approval' and ad.resfield1='" & Session("draftno") & "' group by ad.stockadjstatus, ad.stockadjnote, ad.upduser, ad.updtime, ap.APPROVALOID, ad.stockadjoid, ad.stockadjdate, ad.adjtype, ap.APPROVALUSER, ap.REQUESTUSER, ap.REQUESTDATE, ad.resfield1,ad.branch_code"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnstockadj")

        draftno.Text = dt.Rows(0).Item("draftno")
        adjdate.Text = dt.Rows(0).Item("adjdate")
        type.Text = dt.Rows(0).Item("type")
        totaladj.Text = dt.Rows(0).Item("totaladj")
        lastupdby.Text = dt.Rows(0).Item("lastupdby")
        lastupdon.Text = dt.Rows(0).Item("lastupdon")
        branch.Text = dt.Rows(0).Item("branch_code")

        Dim periodacctg As String = GetPeriodAcctg(GetServerTime()) 
        sSql = "select ad.branch_code, ad.resfield1,ad.stockadjnote note,ad.mtrwhoid,ad.stockadjoid,ad.resfield2 stockacctgoid,ROW_NUMBER() OVER (ORDER BY itemoid) seq,i.itemoid, i.itemcode, i.itemdesc, g.gendesc unit, isnull((select sum(qtyIn)-SUM(qtyOut) from QL_conmtr cr where cr.refoid=ad.refoid and cr.cmpcode=ad.cmpcode and cr.branch_code = ad.branch_code and cr.periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' And cr.mtrlocoid=ad.mtrwhoid),0.00) as currqty, ad.stockadjqty newqty, Isnull(ad.stockadjamtidr,0.00) value, ad.stockadjnote noteas, (select gn.gendesc from QL_mstgen gn where gn.genoid=ad.mtrwhoid and gengroup='location') location from QL_MSTITEM i inner join QL_trnstockadj ad on ad.refoid=i.itemoid and ad.cmpcode=i.cmpcode inner join QL_mstgen g on g.genoid=i.satuan2 and g.cmpcode=i.cmpcode Where ad.cmpcode='" & CompnyCode & "' and ad.resfield1=" & Session("draftno") & " and ad.stockadjstatus <> 'completed' AND ad.branch_code='" & branch.Text & "'"
        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "ql_trnstockadjdtl")
        Session("TblDtlAS") = tbDtl
        FillGV(gvASDtl, sSql, "ql_trnstockadjdtl")
    End Sub

    Protected Sub lkbSelectCN_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("draftno") = sender.ToolTip : MultiView1.SetActiveView(View37)
        Session("approvaloid") = sender.CommandArgument

        Dim jbt As String = GetStrData("Select POSITION from QL_MSTPROF Where BRANCH_CODE='" & Session("branch_id") & "' AND USERID='" & Session("UserID") & "'")
        Dim fCb As String = ""
        If jbt.ToString.ToUpper <> "ANALISA KEU. PUSAT" Then
            fCb &= "AND ad.branch_code='" & Session("branch_id") & "'"
        End If

        Dim dt As DataTable = cKoneksi.ambiltabel("Select distinct (Case When cn.reftype = 'AR' then (Select j.trnjualmstoid From QL_trnjualmst j Where j.trnjualmstoid = cn.refoid) when cn.reftype = 'NT' then (select j.trnbiayaeksoid from ql_trnbiayaeksmst j Where j.trnbiayaeksoid = cn.refoid) When cn.reftype = 'SRV' then (Select j.SOID From QL_TRNINVOICE j Where j.SOID = cn.refoid) else (select b.trnbelimstoid from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) End) trnjualbelioid, (Case when cn.reftype = 'AR' then (select j.trnjualno from QL_trnjualmst j where j.trnjualmstoid = cn.refoid) when cn.reftype = 'NT' then (select j.trnbiayaeksno from ql_trnbiayaeksmst j where j.trnbiayaeksoid = cn.refoid) when cn.reftype = 'SRV' then (select j.invoiceno from QL_TRNINVOICE j where j.SOID = cn.refoid) else (select b.trnbelino from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) end) trnjualbelino, (Case when cn.reftype IN ('AR','NT','SRV') then (select c.custoid from QL_mstcust c where c.custoid = cn.cust_supp_oid) else (select s.suppoid from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) end) custsuppoid,(case when cn.reftype IN ('AR','NT','SRV') then (select c.custname from QL_mstcust c where c.custoid = cn.cust_supp_oid) else (select s.suppname from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) end) custsuppname, coa_credit, coa_debet,(case when cn.reftype IN ('AR','NT','SRV') then cn.branch_code else (select b.branch_code from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) end) branch_code,cn.cnstatus, cn.amount, cn.upduser lastupdby, CONVERT(varchar(10), cn.updtime, 101) lastupdon,ap.approvaloid, cn.oid draftno, convert(varchar(10),cn.tgl,103) tgl, cn.reftype, ap.REQUESTUSER, ap.REQUESTDATE From QL_CreditNote cn inner join QL_APPROVAL ap on ap.OID=cn.oid and ap.CMPCODE=cn.cmpcode where ap.TABLENAME='ql_creditnote' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' and cn.cmpcode like '%" & CompnyCode & "%' " & fCb & "  And ap.APPROVALUSER='" & Session("UserID") & "' and cn.cnstatus='In Approval' and cn.oid='" & Session("draftno") & "'", "ql_creditnote")

        oid.Text = dt.Rows(0).Item("draftno")
        tgl.Text = CDate(toDate(dt.Rows(0).Item("tgl")))
        reftype.Text = dt.Rows(0).Item("reftype")
        amount.Text = ToMaskEdit(dt.Rows(0).Item("amount"), 4)
        updusercn.Text = dt.Rows(0).Item("lastupdby")
        updtimecn.Text = dt.Rows(0).Item("lastupdon")
        branch_code_cn.Text = dt.Rows(0).Item("branch_code")
        cust_supp_oid.Text = dt.Rows(0).Item("custsuppoid")
        trnjualbelioid.Text = dt.Rows(0).Item("trnjualbelioid")
        trnjualbelino.Text = dt.Rows(0).Item("trnjualbelino")
        cnstatus.Text = dt.Rows(0).Item("cnstatus")
        coa_credit.Text = dt.Rows(0).Item("coa_credit")
        coa_debet.Text = dt.Rows(0).Item("coa_debet")
        Dim periodacctg As String = GetPeriodAcctg(GetServerTime())
        sSql = "Select oid, 0 seq, (case when reftype = 'AR' then (Select j.trnjualno from QL_trnjualmst j where j.trnjualmstoid = cn.refoid) when reftype = 'NT' then (select j.trnbiayaeksno from ql_trnbiayaeksmst j where j.trnbiayaeksoid = cn.refoid) when reftype = 'SRV' Then (Select j.invoiceno from QL_TRNINVOICE j where j.SOID = cn.refoid) Else (select j.trnbelino from ql_trnbelimst j where j.trnbelimstoid = cn.refoid) end) trnjualno, (case when reftype IN ('AR','NT','SRV') then (select c.custname from QL_mstcust c where c.custoid = cn.cust_supp_oid) else (select s.suppname from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) end) custname, amount, note from QL_CreditNote cn where cn.cmpcode='" & CompnyCode & "' and cn.oid=" & Session("draftno") & " and cn.cnstatus <> 'Approved'"
        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "ql_creditnotedtl")
        Session("TblDtlCN") = tbDtl
        FillGV(gvCNDtl, sSql, "ql_creditnotedtl")
    End Sub

    Protected Sub lkbPO_r_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPO_R.Click
        setPO_R_asActive()
    End Sub

    Private Sub setPO_R_asActive()
        bindPO_R() : trnbelipono.Text = "" : trnbelipodate.Text = "" : suppname.Text = "" : trnbelinote.Text = "" : poidentifierno.Text = "" : trnbelitype.Text = "" : labelPO.Text = "Purchase Order Retail List :"
        Session("approvaloid") = Nothing : Session("pomstoid") = Nothing : MultiView1.SetActiveView(View5)
    End Sub

    Protected Sub lkbPO_rh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPO_RH.Click
        setPO_Rh_asActive()
    End Sub

    Private Sub setPO_Rh_asActive()
        bindPO_RH() : trnbelipono.Text = "" : trnbelipodate.Text = "" : suppname.Text = "" : trnbelinote.Text = "" : poidentifierno.Text = "" : trnbelitype.Text = "" : labelPO.Text = "Purchase Order Retail-Holding List :"
        Session("approvaloid") = Nothing : Session("pomstoid") = Nothing : MultiView1.SetActiveView(View5)
    End Sub

    Protected Sub gvPODetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODetail.RowDataBound
        Dim fmt As Integer = 0
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
                e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
                e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
                e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
                e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
                e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
                e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
                e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
                e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 3)
                e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
                e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 3)
                e.Row.Cells(14).Text = ToMaskEdit(ToDouble(e.Row.Cells(14).Text), 3)
                e.Row.Cells(15).Text = ToMaskEdit(ToDouble(e.Row.Cells(15).Text), 3)
            End If

            Dim dTotal As Double = 0
            For i As Int16 = 0 To gvPODetail.Rows.Count - 1
                dTotal += ToDouble(gvPODetail.Rows(i).Cells(7).Text)
            Next
            lblTotalOrder.Text = "Total Purchase Order = " & ToMaskEdit(dTotal, 3)

            'If dgt = 2 Or dgt = 6 Then
            '    lblTotalOrder.Text = "Total Purchase Order = " & ToMaskEdit(dTotal, fmt)
            'Else
            '    lblTotalOrder.Text = "Total Purchase Order = " & Format(dTotal, "###,###,###,###.####") & ".00"
            'End If

        Catch ex As Exception
            showMessage(ex.ToString, "", 1)
        End Try
    End Sub

    Protected Sub imbBackPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If trnbelitype.Text.ToLower = "grosir" Then
            setPOasActive()
        ElseIf trnbelitype.Text.ToLower = "retail" Then
            setPO_R_asActive()
        Else
            setPO_Rh_asActive()
        End If

    End Sub

    Protected Sub lkbSQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSQ.Click
        Session("Type") = "SOJ"
        'setSOasActive()
        setSQasActive()
    End Sub

    Private Sub setSQasActive()
        'bindSO() : orderno.Text = "" : orderdate.Text = "" : custnameorder.Text = "" : ordernote.Text = "" ': soidentifierno.Text = ""
        'Session("approvaloid") = Nothing : Session("orderoid") = Nothing : MultiView1.SetActiveView(View11)
        bindSQ() : quotno.Text = "" : quotdate.Text = "" : custnamequot.Text = "" : quotnote.Text = ""
        Session("approvaloid") = Nothing : Session("orderoid") = Nothing : MultiView1.SetActiveView(View9)
    End Sub

    Protected Sub lkbSelectSQ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("orderoid") = sender.ToolTip
        Session("approvaloid") = sender.CommandArgument
        Dim fmt As Integer = 0 : dgt = 2
        Dim dt As DataTable = cKoneksi.ambiltabel("SELECT ordermstoid,orderno,trnorderdate trnorderdate, c.custname, trnordernote,periodacctg,isnull((select s.consaddress  + ' ' +(select gendesc from ql_mstgen where genoid=s.conscityoid)  + ' ' + (select gendesc from ql_mstgen where genoid=s.consprovoid) + ' ' +(select gendesc from ql_mstgen where genoid=s.conscountryoid) from ql_mstcustcons s where s.consoid = consigneeoid  ),'') as deliveryaddr ,case trntaxpct when 0 then 'NO' else 'YES' end as taxable, case SOBO when 0 then 'NO' else 'YES' end SOBO,delivdate as SOdevdate FROM ql_trnordermst INNER JOIN ql_mstcust c ON ql_trnordermst.cmpcode=c.cmpcode AND ql_trnordermst.cmpcode='" & CompnyCode & "' AND ql_trnordermst.trncustoid=c.custoid  WHERE ordermstoid='" & Session("orderoid") & "'", "ql_trnordermst")

        quotno.Text = dt.Rows(0).Item("orderno") : quotdate.Text = Format(CDate(dt.Rows(0).Item("trnorderdate")), "MM/dd/yyyy")
        custnamequot.Text = dt.Rows(0).Item("custname") : quotnote.Text = dt.Rows(0).Item("trnordernote")
        quotax.Text = dt.Rows(0).Item("taxable")

        Dim tblso As DataTable = cKoneksi.ambiltabel("SELECT 'N' free,i.itemoid,od.trnorderdtloid quotdtloid, i.itemdesc + ' - ' + i.merk itemlongdesc,gunit.gendesc unit, od.trnorderdtlqty quotqty, od.trnorderprice AS unitprice, od.trnamountdtl AS totalprice, od.trnorderdtlnote itemnote FROM QL_trnordermst o INNER JOIN QL_trnorderdtl od ON o.cmpcode = od.cmpcode AND o.ordermstoid = od.trnordermstoid INNER JOIN QL_MSTITEM i ON od.cmpcode = i.cmpcode AND od.itemoid = i.itemoid inner join ql_mstgen gunit on od.trnorderdtlunitoid = gunit.genoid  WHERE (o.cmpcode = '" & CompnyCode & "') AND (od.trnordermstoid = '" & Session("orderoid") & "') ORDER BY i.itemdesc", "ql_trnorderdtl")
        Session("gvSOdTl") = tblso
        gvSQDetail.DataSource = tblso
        gvSQDetail.DataBind()
        Dim objX As Object = tblso.Compute("sum(totalprice)", "free='N'")
        If IsDBNull(objX) Then objX = "0"
        lblTotalSQ.Text = " Total Sales Order Projeck = " & ToMaskEdit(objX, 3)
        MultiView1.SetActiveView(View10)
    End Sub

    Protected Sub gvSQDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSQDetail.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(1).Text), 3)
            e.Row.Cells(2).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(3).Text), 3)
        End If
        Dim dTotal As Double = 0
        For i As Int16 = 0 To gvSQDetail.Rows.Count - 1
            dTotal += ClassFunction.ToDouble(gvSQDetail.Rows(i).Cells(3).Text)
        Next
        lblTotalSQ.Text = "Total Sales Quotation = " & ClassFunction.ToMaskEdit(dTotal, 3)
    End Sub

    Protected Sub imbApproveSQ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("orderoid")) Then
            showMessage("Please select a Sales Order Projeck first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("orderoid") = "" Then
                showMessage("Please select a Sales Order Projeck first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If quotno.Text = "" Then
            showMessage("Please select a Sales Order Projeck first", CompnyName & " - Warning", 2) : Exit Sub
        End If
        '-------------------Cek Prize item < HPP---------------------
        Dim HppCek As DataTable : HppCek = Session("gvSOdTl")
        Dim dv As DataView = HppCek.DefaultView
        'For C4 As Integer = 0 To HppCek.Rows.Count - 1
        '    Dim Pri As Double = ToDouble(HppCek.Rows(C4)("unitprice"))
        '    sSql = "SELECT HPP FROM ql_mstitem WHERE itemoid=" & HppCek.Rows(C4)("itemoid") & ""
        '    Dim sCok As Double = GetStrData(sSql)
        '    dv.RowFilter = "unitprice < " & sCok & ""
        '    If ToDouble(HppCek.Rows(C4)("unitprice")) < ToDouble(sCok) Then
        '        showMessage("Harga Katalog " & HppCek.Rows(C4)("itemLDesc") & " Kurang dari Nilai Hpp", CompnyName & " - WARNING", 2)
        '        Exit Sub
        '    End If
        'Next

        '-------------------------BEGIN ZIPI--------------------------        
        'ambil rate,totalPriceDtl dan custoid dr SO yg mau di approved
        Dim rate As Decimal = 1 'kunci
        Dim custoid As String = GetStrData("select trncustoid from ql_trnordermst Where cmpcode='" & CompnyCode & "' and ordermstoid = '" & Session("orderoid") & "'")
        Dim priceDtl As Decimal = GetStrData("select amtjualnetto from QL_trnordermst m where m.cmpcode='" & CompnyCode & "' and m.ordermstoid = '" & Session("orderoid") & "'")
        '---------------------------------------------------------------------------------

        'Cek apakah creditlimit,dan creditusage di customer tersebut > 0, Jika 0 maka tdk ada pengecekan, tp jika > 0 ada pengecekan kredit limit
        Dim crLimit As Decimal = GetStrData("select isnull(custcreditlimitrupiah,0) from ql_mstcust where custoid=" & custoid & "")
        Dim crUsage As Decimal = GetStrData("select isnull(custcreditlimitusagerupiah,0) from ql_mstcust where custoid=" & custoid & "")
        '---------------------------------------------------------------------------------
        If crLimit = 0 Then
            'lolos cz un-limitied
        Else
            'cek apakah crusage-total SO msh >=0, jika >=0 maka lolos, tp klo minus tdk diloloskan
            If (ToDouble(crLimit) - ToDouble(crUsage)) - (ToDouble(priceDtl) * ToDouble(rate)) < 0 Then
                showMessage("Credit Limit for this customer not enough, total SO must be less than " & NewMaskEdit((ToDouble(crLimit) - ToDouble(crUsage)) / ToDouble(rate)), CompnyName & " - Warning", 2)
                Exit Sub
            End If
        End If

        '--------------------- cek otorisasi ---------------------
        Dim orderlimitbottom As String = GetStrData("select flagbottomprice from ql_trnordermst where cmpcode='" & CompnyCode & "' and ordermstoid = '" & Session("orderoid") & "'")

        '---------------Warning Done---------------
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Dim statusUser As String = ""
            Dim level As String = "" : Dim maxLevel As String = ""
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_TRNORDERMSTPJ' and approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_TRNORDERMSTPJ' ")
            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_TRNORDERMSTPJ' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNORDERMSTPJ' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                If orderlimitbottom = "T" Then
                    If SOPJconfimID.Text.Trim = "" Then
                        'get data item2 bawah bottomprice
                        sSql = "select i.itemdesc ,d.trnorderdtlqty,unit.gendesc unit ,d.trnorderprice ,trnamountbruto ,trndiskonpromo,trnamountdtl ,ISNULL(pro.promeventcode  ,'-') promocode ,bottomprice ,amountbottomprice  from QL_trnorderdtl d " & _
" inner join QL_mstItem i on d.itemoid = i.itemoid " & _
" inner join QL_mstgen unit on d.trnorderdtlunitoid = unit.genoid " & _
" left join QL_mstpromo pro on pro.promoid = d.promooid " & _
" where d.cmpcode='" & CompnyCode & "' and trnordermstoid = '" & Session("orderoid") & "' and d.flagbottompricedtl = 'T'"

                        'tampung di notif
                        Dim objTablecek As DataTable
                        Dim objRowcek() As DataRow
                        objTablecek = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
                        objRowcek = objTablecek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                        lblMessageBottom.Text = "Transaksi berikut terdapat penjualan dibawah BottomPrice Grosir <br><br>"
                        For c1 As Integer = 0 To objRowcek.Length - 1
                            lblMessageBottom.Text &= "Item : " & objTablecek.Rows(c1)("itemdesc") & " | Qty : " & objTablecek.Rows(c1)("trnorderdtlqty") & " " & objTablecek.Rows(c1)("unit") & " | Price : " & ToMaskEdit(objTablecek.Rows(c1)("trnorderprice"), 3) & " | DiscPromo : " & ToMaskEdit(objTablecek.Rows(c1)("trndiskonpromo"), 3) & " | AmountNetto : " & ToMaskEdit(objTablecek.Rows(c1)("trnamountdtl"), 3) & " | PromoCode : " & objTablecek.Rows(c1)("promocode") & " | BottomPrice : " & ToMaskEdit(objTablecek.Rows(c1)("bottomprice"), 3) & " | <br>"
                        Next
                        showValidation("PROJECK")
                        Exit Sub
                    End If
                End If

                '----------------------------- generate cok -----------------------------
                Dim flag As String = "" : flag = "SOJ" : Dim tax As String = ""
                If quotax.Text = "YES" Then
                    tax = "0"
                Else
                    tax = "1"
                End If
                Dim tmbhno As String = "" : tmbhno = "1"
                Dim exportno As String = "" : Dim BO As String = ""
                'If SOBO.Text = "YES" Then
                '    BO = "B"
                'Else
                BO = "L"
                'End If
                Dim sNo As String = flag & tax & "/" & Format(Now, "yy") & "/" & Format(Now, "MM") & "/" & BO & "/"
                sSql = "SELECT ISNULL(MAX(CAST(substring(orderno,14,4) AS INTEGER))+'" & tmbhno & "',1) AS IDNEW FROM ql_trnordermst WHERE cmpcode='" & CompnyCode & "' AND orderno LIKE '" & sNo & "%'"
                quotno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
                quotno.Text = quotno.Text
                'End If
                '-------------------------------------------------------------------------
                'update creditusage di master customer
                If crLimit > 0 Then
                    sSql = "update ql_mstcust set custcreditlimitusagerupiah=custcreditlimitusagerupiah+" & ToDouble(priceDtl) * ToDouble(rate) & " WHERE cmpcode = '" & CompnyCode & "' and custoid = " & custoid & """"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                sSql = "UPDATE ql_trnordermst SET  orderno = '" & quotno.Text & "', trnorderstatus = 'Approved',trnorderdate = '" & Format(Now, "MM/dd/yyyy") & "' ,periodacctg = '" & Format(Now, "yyyy-MM") & "',finalapprovaluser='" & Session("UserID") & "',finalappovaldatetime=CURRENT_TIMESTAMP,otorisasiuser = '" & Tchar(SOPJconfimID.Text) & "' WHERE cmpcode = '" & CompnyCode & "' AND ordermstoid = '" & Session("orderoid") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Approved', " & _
                       "event='Approved', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_TRNORDERMSTPJ' " & _
                       "and oid=" & Session("orderoid") & " " & _
                       "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_TRNORDERMSTPJ' " & _
                       "and oid=" & Session("orderoid") & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "update ql_trnordermst set updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' and  ordermstoid=" & Session("orderoid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_TRNORDERMSTPJ' " & _
                        "and oid=" & Session("orderoid") & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                          "approvaldate=CURRENT_TIMESTAMP, " & _
                          "approvalstatus='Ignore', " & _
                          "event='Ignore', " & _
                          "statusrequest='End' " & _
                          "WHERE cmpcode='" & CompnyCode & "' " & _
                          "and tablename='QL_TRNORDERMSTPJ' " & _
                          "and oid=" & Session("orderoid") & " " & _
                          "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insertkan untuk user approval yg levelnya diatasnya lagi
                If Not Session("TblApproval") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblApproval")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode, requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode,approvaluser,approvaldate,approvaltype, approvallevel,approvalstatus) " & _
                               "VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ",'" & "SOJ" & Session("orderoid") & "_" & Session("AppOid") + c1 & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNORDERMSTPJ','" & Session("orderoid") & "','In Approval','0','" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        SOPJconfimID.Text = ""
        refSO.Text = ""
        setSQasActive()
    End Sub

    Protected Sub imbRejectSQ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("orderoid")) Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("orderoid") = "" Then
                showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If quotno.Text = "" Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE ql_trnordermst SET trnorderstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND ordermstoid = '" & Session("orderoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='QL_TRNORDERMST' and oid=" & Session("orderoid") & " "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setSQasActive()
    End Sub

    Protected Sub imbBackSQ_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setSQasActive()
    End Sub

    Private Sub setSOasActive()
        bindSO() : orderno.Text = "" : orderdate.Text = ""
        custnameorder.Text = "" : ordernote.Text = "" ': soidentifierno.Text = ""
        Session("approvaloid") = Nothing : Session("orderoid") = Nothing
        MultiView1.SetActiveView(View11)
    End Sub

    Protected Sub lkbSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSO.Click
        Session("Type") = "SO"
        setSOasActive()
    End Sub

    Protected Sub lkbSelectSO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("orderoid") = sender.ToolTip
        Session("approvaloid") = sender.CommandArgument
        Session("CodeC") = sender.CommandName
        Dim fmt As Integer = 0
        dgt = 2
        sSql = "SELECT ordermstoid,orderno,(select gendesc from QL_mstgen where gencode='" & Session("CodeC") & "' And gengroup='CABANG') cabang,flagbottomprice,rate2oid, trnorderdate,gen1.gendesc timeofpaymentSO,gen2.gendesc timeofpaymentCust,c.custname, trnordernote,periodacctg,isnull((select s.consaddress + ' ' +(select gendesc from ql_mstgen where genoid=s.conscityoid) + ' ' + (select gendesc from ql_mstgen where genoid=s.consprovoid) + ' ' +(select gendesc from ql_mstgen where genoid=s.conscountryoid) from ql_mstcustcons s where s.consoid = consigneeoid  ),'') as deliveryaddr, ordertaxtype as taxable, case SOBO when 0 then 'NO' else 'YES' end SOBO, delivdate as SOdevdate,curr.currencycode,so.typeSO FROM ql_trnordermst so INNER JOIN ql_mstcurr curr on so.currencyoid=curr.currencyoid inner join ql_mstcust c ON so.cmpcode=c.cmpcode And so.branch_Code = c.branch_code AND so.cmpcode='" & CompnyCode & "' AND so.trncustoid=c.custoid Inner join ql_mstgen gen1 on timeofpaymentSO=gen1.genoid AND gen1.gengroup='PAYTYPE' inner join QL_mstgen gen2 on gen2.genoid=c.timeofpayment AND gen2.gengroup='PAYTYPE' WHERE so.branch_code='" & Session("CodeC") & "' and ordermstoid='" & Session("orderoid") & "'"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnordermst")

        orderno.Text = dt.Rows(0).Item("orderno")
        orderdate.Text = Format(CDate(dt.Rows(0).Item("trnorderdate")), "MM/dd/yyyy")
        custnameorder.Text = dt.Rows(0).Item("custname")
        ordernote.Text = dt.Rows(0).Item("trnordernote")
        SOdeliveadd.Text = dt.Rows(0).Item("deliveryaddr")
        SOdevdate.Text = Format(CDate(dt.Rows(0).Item("SOdevdate")), "MM/dd/yyyy")
        SOtax.Text = dt.Rows(0).Item("taxable")
        SOBO.Text = dt.Rows(0).Item("SOBO")
        timeOP.Text = dt.Rows(0).Item("timeofpaymentSO")
        topCust.Text = dt.Rows(0).Item("timeofpaymentCust")
        curr.Text = dt.Rows(0).Item("currencycode")
        SOType.Text = dt.Rows(0).Item("typeSO").ToString
        If curr.Text = "USD" Then
            lblcurr1.Text = "Dollar"
        Else
            lblcurr1.Text = "Rupiah"
        End If
        lblcabang.Text = dt.Rows(0).Item("cabang").ToString
        lblrate2oid.Text = dt.Rows(0).Item("rate2oid")
        lblrate.Text = NewMaskEdit(GetStrData("select rate2idrvalue from ql_mstrate2 where rate2oid = " & lblrate2oid.Text & ""))

        If timeOP.Text <> topCust.Text Then
            lblflag.Visible = True
            note1.Visible = True
        ElseIf timeOP.Text = topCust.Text Then
            lblflag.Visible = False
            If timeOP.Text = "CASH" Then
                Label50.Visible = False
            Else
                Label50.Visible = True
            End If
        End If
        lblflagprice.Text = dt.Rows(0).Item("flagbottomprice")
        If lblflagprice.Text = "T" Then
            note2.Visible = True
        End If
        sSql = "SELECT 'N' free,od.trnorderdtloid orderdtloid,od.flagbottompricedtl, i.itemdesc + ' - ' + i.merk itemdesc,i.pricelist,gunit.gendesc unit, od.trnorderdtlqty orderqty, od.trnorderprice AS unitprice,od.trnorderpriceidr, od.trnamountdtl AS totalprice,(select r.rateidrvalue from QL_mstrate r where r.rateoid = o.rate2oid ) * od.trnorderprice + o.ordertaxamt unitpriceidr,(select r.rateidrvalue from QL_mstrate r where r.rateoid = o.rate2oid ) * od.trnamountdtl + o.ordertaxamt totalpriceidr, o.ordertaxamt, od.trnorderdtlnote itemnote,od.flagpromo " & _
                    "FROM QL_trnordermst o INNER JOIN QL_trnorderdtl od ON o.cmpcode = od.cmpcode AND o.branch_code=od.branch_code and o.ordermstoid = od.trnordermstoid INNER JOIN QL_MSTITEM i ON od.cmpcode = i.cmpcode AND od.itemoid = i.itemoid inner join ql_mstgen gunit on od.trnorderdtlunitoid = gunit.genoid WHERE o.branch_code='" & Session("CodeC") & "' and (o.cmpcode = '" & CompnyCode & "') AND (od.trnordermstoid = '" & Session("orderoid") & "') ORDER BY i.itemdesc"
        Dim tblso As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnorderdtl")

        lblflagdtl.Text = tblso.Rows(0).Item("flagbottompricedtl").ToString
        If tblso.Rows(0).Item("unitprice").ToString <> tblso.Rows(0).Item("trnorderpriceidr").ToString Then
            lblpricelist.Visible = True
        End If
        'ClassFunction.FillGV(gvSODetail, )
        gvSODetail.DataSource = tblso
        Session("tblso") = tblso
        gvSODetail.DataBind()
        Dim objX As Object = tblso.Compute("sum(totalprice)", "free='N'")

        If IsDBNull(objX) Then objX = "0"
        If curr.Text = "IDR" Then
            lblTotalSO.Text = ToMaskEdit(objX, 3)
            amttax.Text = ToMaskEdit(tblso.Rows(0).Item("ordertaxamt"), 3)
            netto.Text = ToMaskEdit(objX - ToDouble(diskon.Text) + ToDouble(amttax.Text), 3)
            txtdpp.Text = ToMaskEdit((netto.Text - amttax.Text), 3)
        Else
            lblTotalSO.Text = NewMaskEdit(objX)
            amttax.Text = NewMaskEdit(tblso.Rows(0).Item("ordertaxamt"))
            netto.Text = NewMaskEdit(objX - ToDouble(diskon.Text) + ToDouble(amttax.Text))
            txtdpp.Text = NewMaskEdit((netto.Text - amttax.Text))
        End If
        MultiView1.SetActiveView(View12)
    End Sub

    Protected Sub gvSODetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(1).Text), 3)
            e.Row.Cells(3).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(8).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(8).Text), 3)

            If e.Row.Cells(7).Text = "T" Then
                e.Row.Cells(3).ForeColor = Drawing.Color.Red
            End If
            If e.Row.Cells(3).Text <> e.Row.Cells(8).Text Then
                lblpricelist.Visible = True
                e.Row.Cells(8).ForeColor = Drawing.Color.Green
            Else
                lblpricelist.Visible = False
            End If

        End If

    End Sub

    Protected Sub imbRejectSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("orderoid")) Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2)
            Exit Sub
        Else
            If Session("orderoid") = "" Then
                showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If orderno.Text = "" Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If

        sSql = "Select ordermstoid, orderno, trnorderstatus From QL_trnordermst WHere trnorderstatus IN ('Canceled','Approved','Closed','Rejected','POST') AND branch_code='" & Session("CodeC") & "' AND ordermstoid=" & Session("orderoid")
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                showMessage("- Maaf, No. draft SO '" & dta.Rows(i).Item("ordermstoid") & "' sudah di '" & dta.Rows(i).Item("trnorderstatus") & "' dengan nomer '" & dta.Rows(i).Item("orderno") & "'..!!<br>", CompnyName & " - WARNING", 2)
            Next
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SO
            sSql = "UPDATE ql_trnordermst SET trnorderstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND ordermstoid = '" & Session("orderoid") & "' And branch_code='" & Session("CodeC") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '' Update IO
            'sSql = "UPDATE ql_trnordermst SET orderstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND orderoid = (select ioref from ql_trnordermst where orderoid='" & Session("orderoid") & "') "
            'xCmd.CommandText = sSql
            'xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='QL_TRNORDERMST' and oid=" & Session("orderoid") & " And branch_code='" & Session("CodeC") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setSOasActive()
    End Sub

    Protected Sub imbBackSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'setSOasActive()
        Response.Redirect("~\Other\WaitingAction.aspx")
    End Sub

    Protected Sub lkbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbBack.Click
        MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub gvSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub lkbSDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSDO.Click
        setSDOasActive()
    End Sub

    Private Sub setSDOasActive()
        bindSDO() : trnsjjualno.Text = "" : trnsjjualsenddate.Text = ""
        sjcustname.Text = "" : sjorderno.Text = "" : sjidentifierno.Text = ""
        Session("approvaloid") = Nothing : Session("trnsjjualmstoid") = Nothing
        MultiView1.SetActiveView(View2)
        'cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
    End Sub

    Protected Sub lkbSelectSDO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trnsjjualmstoid") = sender.ToolTip
        Session("approvaloid") = sender.CommandArgument
        Session("Cbang") = sender.CommandName

        sSql = "SELECT jj.to_branch, so.branch_code, trnsjjualmstoid, jj.trnsjjualno salesdeliveryno, jj.trnsjjualdate salesdeliverydate, c.custname, so.orderno, c.custaddr, jj.noExpedisi, g.gendesc, so.salesoid, so.spgoid, so.trncustoid, so.ordermstoid, so.typeSO FROM ql_trnsjjualmst jj INNER JOIN QL_trnordermst so ON jj.cmpcode=so.cmpcode AND jj.orderno=so.orderno AND jj.ordermstoid=so.ordermstoid AND jj.orderno=so.orderno INNER JOIN ql_mstcust c ON jj.cmpcode=c.cmpcode AND jj.cmpcode='" & CompnyCode & "' AND so.trncustoid=c.custoid INNER JOIN QL_mstgen g ON jj.expedisioid = g.genoid WHERE trnsjjualmstoid ='" & Session("trnsjjualmstoid") & "' AND so.branch_code='" & Session("Cbang") & "'"

        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualmst")
        trnsjjualno.Text = dt.Rows(0).Item("salesdeliveryno")
        trnsjjualsenddate.Text = Format(CDate(dt.Rows(0).Item("salesdeliverydate")), "dd/MM/yyyy")
        sjjualmstoid.Text = dt.Rows(0).Item("trnsjjualmstoid").ToString
        sjcustname.Text = dt.Rows(0).Item("custname").ToString
        sjorderno.Text = dt.Rows(0).Item("orderno").ToString
        CustAddress.Text = dt.Rows(0).Item("custaddr").ToString
        SjPolisce.Text = dt.Rows(0).Item("noExpedisi").ToString
        EkspedisiSj.Text = dt.Rows(0).Item("gendesc").ToString
        SalesOid.Text = dt.Rows(0).Item("salesoid")
        SpgOid.Text = dt.Rows(0).Item("salesoid")
        zCabang.Text = Session("Cbang")
        ToBranchSO.Text = Session("Cbang")
        OidCust.Text = dt.Rows(0).Item("trncustoid")
        OidSO.Text = dt.Rows(0).Item("ordermstoid")
        SeTipe.Text = dt.Rows(0).Item("typeSO").ToString
        ApprovedOid.Text = Integer.Parse(Session("approvaloid"))
        FillDDL(TOP, "Select distinct (select g.genoid from QL_mstgen g where g.genoid = (case om.trnpaytype when 0 then 34 else om.trnpaytype end) and g.gengroup = 'paytype') payoid,(select g.gendesc from QL_mstgen g where g.genoid = (case om.trnpaytype when 0 then 34 else om.trnpaytype end) and g.gengroup = 'paytype') gendesc from ql_trnsjjualdtl d inner join QL_trnorderdtl od on od.trnorderdtloid = d.trnorderdtloid AND d.Branch_code=od.branch_code inner join QL_trnordermst om on om.ordermstoid = od.trnordermstoid and om.branch_code = od.branch_code where d.cmpcode = '" & CompnyCode & "' and d.trnsjjualmstoid = " & Session("trnsjjualmstoid") & " AND d.Branch_code='" & zCabang.Text & "'")

        sSql = "SELECT seq trnsjjualdtlseq, d.trnsjjualdtloid salesdeliverydtloid, d.trnsjjualmstoid salesdeliverymstoid, d.trnorderdtloid orderdtloid, d.qty salesdeliveryqty, d.note itemdeliverynote, p.trnsjjualmstoid salesdeliveryoid, d.refoid itemoid, d.unitoid as itemunitoid, i.itemdesc + ' - ' + i.merk itemlongdesc, g.gendesc AS unit, ISNULL((select sum(qty) from ql_trnsjjualdtl Where trnsjjualmstoid in (select trnsjjualmstoid from ql_trnsjjualmst where trnsjjualstatus in ('Approved','INVOICED') and trnorderdtloid = od.trnorderdtloid and od.itemoid=refoid and od.branch_code=branch_code)),0) as deliveredqty, od.trnorderdtlqty as referedqty, d.qtypak, d.mtrlocoid itemloc, (SELECT gendesc FROM QL_mstgen WHERE genoid = (SELECT genother1 FROM QL_mstgen WHERE genoid = d.mtrlocoid)) +' - ' + (SELECT gendesc FROM QL_mstgen WHERE genoid = d.mtrlocoid) location, (od.trnorderprice * d.qty) trnorderprice,od.amtdtldisc1,amtdtldisc2,od.trnorderdtlqty, (od.amtdtldisc1/od.trnorderdtlqty)*d.qty disc1, (od.amtdtldisc2/od.trnorderdtlqty)*d.qty disc2 FROM ql_trnsjjualdtl d inner join ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid inner join ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' inner join ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid inner join ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid And od.itemoid=d.refoid AND od.branch_code=p.branch_code WHERE d.trnsjjualmstoid =" & Session("trnsjjualmstoid") & " and d.Branch_code = '" & zCabang.Text & "' and od.branch_code ='" & zCabang.Text & "' AND d.cmpcode='" & CompnyCode & "' ORDER BY d.seq,d.trnsjjualdtloid"
        FillGV(gvSDODetail, sSql, "QL_trnsalesdelivdtl")
        MultiView1.SetActiveView(View13)
    End Sub

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String, ByVal sBranch As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='" & sInterfaceVar & "' AND interfaceres1='" & sBranch & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String, ByVal bParentAllowed As Boolean) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' "
        If Not bParentAllowed Then
            sSql &= " AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) "
        End If
        sSql &= " ORDER BY acctgcode "
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Function GetCodeAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgcode FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Function GetDescAcctg(ByVal iAcctgOid As Integer) As String
        sSql = "SELECT acctgdesc FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgflag='A' AND acctgoid=" & iAcctgOid
        Return GetStrData(sSql)
    End Function

    Private Sub setJurnalSI()
        Dim objtbl As DataTable = Session("TblDtl")
        Dim awal As String = "", awalSI As String = "", sql As String = ""
        Dim lokasi As String = "" : Dim tax As String = "" : Dim amtjualnetto As Double = 0
        If orderno.Text <> "" Then
            Dim cekawal As String = orderno.Text.Substring(1, 1)
            'If cekawal = "O" Then
            If trnSjtype.Text = "GROSIR" Then
                awal = "DO" : awalSI = "SI" : Else : awal = "DOJ" : awalSI = "SIJ"
            End If
            lokasi = orderno.Text.Substring(10, 1)
            tax = orderno.Text.Substring(2, 1)
        Else
            If trnSjtype.Text = "GROSIR" Then
                awal = "DO" : awalSI = "SI" : Else : awal = "DOJ" : awalSI = "SIJ"
            End If
            lokasi = "L" : tax = 1
        End If

        'sSql = "select sum((sod.trnamountdtl/sod.trnorderdtlqty) * d.qty ) from ql_trnsjjualdtl d inner join QL_mstItem i on i.itemoid = d.refoid and d.refname = 'ql_mstitem' inner join ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid inner join QL_mstgen g on i.payoid = g.genoid inner join QL_trnorderdtl sod on d.trnorderdtloid = sod.trnorderdtloid AND sod.trnordermstoid=m.ordermstoid Where d.cmpcode = '" & CompnyCode & "' and sod.branch_code='" & zCabang.Text & "' and d.trnsjjualmstoid = " & sjjualmstoid.Text & ""
        'xCmd.CommandText = sSql : Dim trnamtjualnetto As Double = xCmd.ExecuteScalar
        'Dim amtjualnetto As Double = 0
        'If tax = "0" Then
        '    amtjualnetto = Math.Round(ToDouble(trnamtjualnetto) + ToDouble(trnamtjualnetto) * ToDouble(trntaxpct.Text) / 100)
        'Else
        '    amtjualnetto = Math.Round(ToDouble(trnamtjualnetto), 2)
        'End If

        conn.Close()
        'If PromType.Text = "DISCAMT" Then
        '    sql = "((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'ElseIf PromType.Text = "BUNDLING" Then
        '    sql = "((od.trnorderprice*qty))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'ElseIf PromType.Text = "Buy x Get y" Then
        '    sql = "((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'ElseIf PromType.Text = "Buy nX get nY" Then
        '    sql = "((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'ElseIf PromType.Text = "Disc % (Item Only)" Then
        '    sql = "((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'Else
        '    sql = "((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty))"
        'End If
        sSql = "SELECT trnordermstoid oid,seq trnsjjualdtlseq,d.trnsjjualdtloid,(od.amtdtldisc1/od.trnorderdtlqty) + (od.amtdtldisc2/od.trnorderdtlqty) discperqty,od.trnorderprice, d.trnorderdtloid,d.qty, d.note, p.trnsjjualmstoid,d.refoid itemoid,od.trndiskonpromo,d.unitoid as itemunitoid,ISNULL((Select sum(qty) From ql_trnsjjualdtl Where trnsjjualmstoid in (select trnsjjualmstoid from ql_trnsjjualmst Where trnsjjualstatus IN ('Approved','INVOICED') AND trnorderdtloid = od.trnorderdtloid and od.itemoid=d.refoid AND d.Branch_code=od.branch_code)),0) as deliveredqty,od.trnorderdtlqty,od.trnordermstoid,od.trnorderprice*qty AmtJual, d.mtrlocoid,ISNULL((od.amtdtldisc1/od.trnorderdtlqty) * d.qty,0.00) disc1,ISNULL((od.amtdtldisc2/od.trnorderdtlqty) * d.qty,0.00) disc2,ISNULL(trndiskonpromo/qty,0.00) discpromoperqty,((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-((od.amtdtldisc1/od.trnorderdtlqty)+(od.amtdtldisc2/od.trnorderdtlqty)) NettoAmt ,d.refoid FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid INNER JOIN ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid AND d.refoid=od.itemoid AND od.branch_code=p.branch_code INNER JOIN QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'LOCATION' INNER JOIN ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' INNER JOIN ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid WHERE d.trnsjjualmstoid =p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid AND d.trnsjjualmstoid = " & sjjualmstoid.Text & " AND d.cmpcode='" & CompnyCode & "' AND od.trnordermstoid = " & OidSO.Text & " AND p.branch_code='" & zCabang.Text & "' ORDER BY trnsjjualdtlseq"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjurnal")
        Session("TblDtl") = objTable
        amtjualnetto = objTable.Compute("SUM(NettoAmt)", "")
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        dtlTable.Columns.Add("code", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", System.Type.GetType("System.Int32")) ' utk seq bila 1 transaksi ada lebih 1 jurnal
        dtlTable.Columns.Add("seqdtl", System.Type.GetType("System.Int32")) ' utk seq bila 1 account bisa muncul dipakai berkali2
        dtlTable.Columns.Add("dbcr", System.Type.GetType("System.String"))
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
        Dim nuRow As DataRow : Dim xBRanch As String = ""
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("mtrlocoid").ToString & " AND genother4='T'")
            Next
        End If

        Dim sVarAR As String = GetInterfaceValue("VAR_AR", zCabang.Text)
        If sVarAR = "?" Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAR, False)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (PIUTANG USAHA)"
            nuRow("debet") = ToMaskEdit(amtjualnetto, 3)
            nuRow("credit") = 0 : nuRow("id") = iAPOid
            nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
            nuRow("dbcr") = "D"
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        'sSql = "Select branch_code from QL_trnordermst Where orderno ='" & orderno.Text & "' AND ordermstoid=" & OidSO.Text & ""
        ' ============ POTONGAN PENJUALAN
        'If Not IsNothing(Session("TblDtl")) Then
        '    Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
        '    ' ====== GET itemacctgoid and jumlah diskon ======
        '    For C1 As Integer = 0 To dtDtl.Rows.Count - 1
        '        Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
        '        Dim iItemAccount As String = GetInterfaceValue("VAR_DISC_JUAL", zCabang.Text)
        '        If iItemAccount = "?" Then
        '            showMessage("Maaf, Interface untuk 'VAR_DISC_JUAL' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
        '            Session("click_post") = "false"
        '            Exit Sub
        '        End If

        '        Dim dItemDiscdtl As Double = ToDouble(dtDtl.Rows(C1)("discperqty").ToString)
        '        Dim iAPdiscOid As Integer = GetAccountOid(iItemAccount, False)

        '        If dItemDiscdtl > 0 Then
        '            If dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
        '                Dim oRow As DataRow = dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl)(0)
        '                oRow.BeginEdit()
        '                oRow("debet") += (dItemDiscdtl)
        '                oRow.EndEdit()
        '                dtJurnal.AcceptChanges()
        '            Else
        '                nuRow = dtJurnal.NewRow
        '                nuRow("code") = GetCodeAcctg(iAPdiscOid)
        '                nuRow("desc") = GetDescAcctg(iAPdiscOid) & " (POTONGAN PENJUALAN)"
        '                nuRow("debet") = ToMaskEdit(dItemDiscdtl, 2)
        '                nuRow("credit") = 0 : nuRow("id") = iAPdiscOid
        '                nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
        '                dtJurnal.Rows.Add(nuRow)
        '            End If
        '        End If
        '    Next
        iSeqDtl += 1
        'End If

        ' ============ PENJUALAN ============
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and HPP
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                Dim iItemAccount As String = GetInterfaceValue("VAR_SALES", zCabang.Text)
                If iItemAccount = "?" Then
                    showMessage("Maaf, Interface untuk 'VAR_SALES' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If
                Dim iAPGudOid As Integer = GetAccountOid(iItemAccount, False)

                If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("NettoAmt").ToString), 3)
                    'If dtDtl.Rows(C1).Item("promtype").ToString = "BUNDLING" Then
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodiscamt").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Disc % (Item Only)" Then
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "DISCAMT" Then
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Buy x Get y" Then
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Buy nX get nY" Then
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'Else
                    '    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'End If
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()

                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iAPGudOid)
                    nuRow("desc") = GetDescAcctg(iAPGudOid) & " (PENJUALAN)"
                    nuRow("debet") = 0
                    nuRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("NettoAmt").ToString), 3)
                    'If dtDtl.Rows(C1).Item("promtype").ToString = "BUNDLING" Then
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodiscamt").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Disc % (Item Only)" Then
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "DISCAMT" Then
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Buy x Get y" Then
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'ElseIf dtDtl.Rows(C1).Item("promtype").ToString = "Buy nX get nY" Then
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'Else
                    '    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("nettodispersen").ToString), 3)
                    'End If

                    nuRow("id") = iAPGudOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "C"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        'sSql = "Select Isnull(ordertaxamt,0.00) from QL_trnordermst Where orderno='" & sjorderno.Text & "' And branch_code='" & zCabang.Text & "' And cmpcode='" & CompnyCode & "' AND ordermstoid=" & OidSO.Text & ""
        'Dim amtTax As Double = cKoneksi.ambilscalar(sSql)
        ''============ PPN KELUARAN ============
        'If ToDouble(amtTax) > 0 Then
        '    Dim sPPNK As String = GetInterfaceValue("VAR_PPN_JUAL", zCabang.Text)
        '    If sPPNK = "?" Then
        '        showMessage("Maaf, Interface untuk 'VAR_PPN_JUAL' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
        '        Session("click_post") = "false"
        '        Exit Sub
        '    Else
        '        Dim iPPNKOid As Integer = GetAccountOid(sPPNK, False)
        '        nuRow = dtJurnal.NewRow
        '        nuRow("code") = GetCodeAcctg(iPPNKOid)
        '        nuRow("desc") = GetDescAcctg(iPPNKOid) & " (PPN KELUARAN)"
        '        nuRow("debet") = 0
        '        nuRow("credit") = (ToDouble(amtTax))
        '        nuRow("id") = iPPNKOid : nuRow("seq") = iSeq
        '        nuRow("seqdtl") = iSeqDtl
        '        dtJurnal.Rows.Add(nuRow)
        '    End If
        '    iSeqDtl += 1
        'End If
        iSeq += 1
        ' ============ 1 JURNAL
        ' BEBAN POKOK PENJUALAN
        ' PERSEDIAAN

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET HPPValue and HPPAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarHPP As String = ""

                sVarHPP = GetStrData("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_HPP' AND interfaceres1='" & zCabang.Text & "'")
                If sVarHPP = "?" Then
                    showMessage("Maaf, Interface untuk 'VAR_HPP' belum disetting silahkan hubungi staff accounting untuk setting interface..!!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarHPP, True)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("debet") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * dItemHPP, 3)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (BEBAN POKOK PENJUALAN(HPP))"
                    nuRow("debet") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * (dItemHPP / 1), 3)
                    nuRow("credit") = 0 : nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "D"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET PersediaanValue and PersediaanAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("refoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarStock As String = ""

                sVarStock = GetInterfaceValue("VAR_GUDANG", ToBranchSO.Text)
                If sVarStock = "?" Then
                    showMessage("Please Setup Interface for Stock on Variable 'VAR_GUDANG'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarStock, False)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * dItemHPP, 3)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (PERSEDIAAN)"
                    nuRow("credit") = ToMaskEdit(ToDouble(dtDtl.Rows(C1)("qty").ToString) * (dItemHPP / 1), 3)
                    nuRow("id") = iItemAccount : nuRow("debet") = 0
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    nuRow("dbcr") = "C"
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If
        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal
        'cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
    End Sub

    Protected Sub gvSDODetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSDODetail.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(4).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub imbApproveSDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApproveSDO.Click
        Dim errMsg As String = "", trnamtjual As Double = 0, trnamtjualnetto As Double = 0, amtjualnetto As Double = 0, cekDO As Double = 0, crLimit As Double = 0, CekSI As Double = 0, diskondtl As Double = 0
        '=======Posting Otomatis Sales Invoice=======
        '============================================
        Dim crtUser As String = GetStrData("Select createuser From QL_trnordermst Where orderno='" & sjorderno.Text & "'")
        If IsDBNull(Session("trnsjjualmstoid")) Then
            errMsg &= "- Maaf, Pilih data sales delivery order dulu..!!<br>"
        Else
            If Session("trnsjjualmstoid") = "" Then
                errMsg &= "- Maaf, Pilih data sales delivery order dulu..!!<br>"
            End If
        End If

        If trnsjjualno.Text = "" Then
            errMsg &= "Maaf, Silahkan pilih data DO dulu..!!<br>"
        End If

        Dim priceDtl As Decimal = GetStrData("select amtjualnetto from QL_trnordermst m where m.cmpcode='" & CompnyCode & "' and branch_code='" & zCabang.Text & "'and m.ordermstoid = '" & OidSO.Text & "'")

        Dim statusUser As String = ""
        Dim level As String = "" : Dim maxLevel As String = ""

        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure Where cmpcode='" & CompnyCode & "' and tablename='ql_trnsjjualmst' And approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) From QL_approvalstructure Where cmpcode='" & CompnyCode & "' And tablename='ql_trnsjjualmst'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' And tablename='ql_trnsjjualmst' And approvaluser='" & Session("UserId") & "' And approvallevel=" & level & "")

        'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
        If level + 1 <= maxLevel Then
            'ambil data approval, tampung di datatable
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjjualmst' and approvallevel=" & level + 1 & ""

            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            End If
        End If

        'Cek apakah creditlimit,dan creditusage di customer tersebut > 0, Jika 0 maka tdk ada pengecekan, tp jika > 0 ada pengecekan kredit limit

        sSql = "Select isnull(custcreditlimitrupiah,0) From ql_mstcust Where custoid=" & OidCust.Text & " And branch_code='" & zCabang.Text & "'"
        crLimit = GetScalar(sSql)

        sSql = "Select CAST(g.genother1 as integer) genother1 From ql_mstcust c INNER JOIN QL_mstgen g ON g.genoid = c.timeofpayment and g.gengroup = 'PAYTYPE' Where custoid=" & OidCust.Text & " And branch_code='" & zCabang.Text & "'"
        Dim termin As Integer = GetScalar(sSql)

        sSql = "Select isnull(custcreditlimitusagerupiah,0) From ql_mstcust Where custoid=" & OidCust.Text & " And branch_code='" & zCabang.Text & "'"
        Dim crUsage As Double = GetScalar(sSql)

        sSql = "Select COUNT(genoid) from ql_mstcust cu INNER JOIN QL_mstgen ge ON ge.genoid=cu.timeofpayment Where gengroup='PAYTYPE' AND gendesc<>'CASH' And custoid=" & OidCust.Text & " And branch_code='" & zCabang.Text & "'"
        Dim sTer As Double = GetScalar(sSql)
        '-----------------------------------------------------------------------
        'SEMENTARA DI REMARK DULU KARENA ACCOUNTING BELUM JALAN - yudha
        ' UPDATE 20180124 By 4n7JuK - Blokir CL yang diminta adalah ketika amount DO tidak mencukupi, tidak bisa di-Approve
        ' Jika USAGE+DO > CL tidak bisa Approve, harus minta tambahan CL Temporary
        ' DO bisa di-Approve selama USAGE+DO < CL
        ' Pindah ke bawah krn ada hitungan dari dalam DO dan SO nya 
        'If sTer > 0 Then
        '    If crLimit = 0 Then
        '    Else
        '        sSql = "Select Count(genoid) from QL_mstgen where genother4='Y' and gengroup='cabang' AND gencode='" & zCabang.Text & "'"
        '        Dim skip As Integer = GetStrData(sSql)
        '        If confirmCR.Text.Trim = "" Then
        '            'cek apakah crusage-total SO msh >=0, jika >=0 maka lolos, tp klo minus tdk diloloskan
        '            If skip = 0 Then
        '                Dim g As Double = ToDouble(crLimit) - ToDouble(crUsage) '- ToDouble(priceDtl)
        '                '(ToDouble(crLimit) - ToDouble(crUsage)) - (ToDouble(priceDtl)) 
        '                'Dim dn As Double = ToDouble(priceDtl)
        '                If ToDouble(g) < 0 Then
        '                    'showMessage("Credit Limit for this customer not enough, total SO must be less than " & NewMaskEdit((ToDouble(crLimit) - ToDouble(crUsage)) / ToDouble(rate)), CompnyName & " - Warning", 2)
        '                    lblMessageBottom.Text = "Maaf, Credit Limit Untuk customer " & sjcustname.Text & ", " & ToMaskEdit((ToDouble(crLimit) - ToDouble(crUsage)), 4) & ""
        '                    showValidation("CR")
        '                    Exit Sub
        '                End If
        '            End If

        '        Else
        '            confirmCR.Text = ""
        '        End If
        '    End If
        'End If

        ' Cek Apakah Masih Ada Tagihan yang belum terbayar
        'Dim sisapiutang As String = GetStrData("select count (-1) [jumlah piutang] from (select distinct trncustoid,trncustname,trnjualno,trnjualdate,[sisa piutang],payduedate from (select  a.trncustoid,a.trncustname,a.trnjualno,trnjualdate,(select c.gendesc from QL_mstgen c where c.genoid = d.timeofpaymentSO and b.branch_code='" & zCabang.Text & "') as Paytype,b.payduedate,(a.trnamtjualnetto - a.accumpayment) [sisa piutang] from QL_trnjualmst a inner join QL_conar b on b.custoid = a.trncustoid and b.refoid = a.trnjualmstoid and a.branch_code = b.branch_code inner join QL_trnordermst d on d.orderno = a.orderno and d.branch_code = a.branch_code where b.trnartype='piutang' and b.reftype='ql_trnjualmst' and (a.trnamtjualnetto - a.accumpayment) <> 0 and trnjualstatus = 'POST' and a.trnpaytype <> 34 and b.payduedate < GETDATE()) dt where trncustoid = " & OidCust.Text & " ) dt2")

        'If sisapiutang = 0 Then : Else
        '    If lblpiutang.Text.Trim = "" Then
        '        lblMessageBottom.Text = "Customer " & sjcustname.Text & " Masih Memiliki " & sisapiutang & " Piutang Yang Belum Lunas"
        '        showValidation("piutang")
        '        Exit Sub
        '    Else
        '        '
        '    End If
        'End If

        Dim trnjualmstoid As Integer = GenerateID("QL_trnjualmst", CompnyCode)
        Dim trnjualdtloid As Integer = GenerateID("QL_trnjualdtl", CompnyCode)
        Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim conmtroid As Int64 = GenerateID("QL_conmtr", CompnyCode)
        Dim crdmtroid As Int64 = GenerateID("QL_crdmtr", CompnyCode)
        Dim sql As String = ""
        '--- Set Jurnal SI ---
        setJurnalSI()
        'ambil mst

        sSql = "SELECT a.trnsjjualmstoid ,a.trnsjjualno ,a.trnsjjualdate, o.ordermstoid, o.trntaxpct, a.trnsjjualdate, a.trnsjjualstatus, s.custoid, s.custname, a.expedisioid, a.noExpedisi , a.note, a.upduser, a.updtime, a.finalapprovalcode, a.finalapprovaluser, a.finalappovaldatetime, o.orderno, o.trnorderdate, o.delivdate, o.currencyoid, o.trnordertype FROM ql_trnsjjualmst a inner join QL_trnordermst o on o.cmpcode=a.cmpcode AND o.ordermstoid=a.ordermstoid and a.branch_code=o.branch_code AND o.orderno=a.orderno inner join QL_mstcust s on a.cmpcode=s.cmpcode AND o.trncustoid=s.custoid and s.branch_code=o.branch_code AND o.ordermstoid=a.ordermstoid Where a.cmpcode ='" & CompnyCode & "' AND a.trnsjjualmstoid ='" & sjjualmstoid.Text & "' And o.branch_code='" & zCabang.Text & "'"

        Dim objtablemst As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualmst")
        If objtablemst.Rows.Count < 1 Then
            errMsg &= "Maaf, Sales Delivery Order data tidak ada..!!<br>"
        Else
            For c1 As Integer = 0 To objtablemst.Rows.Count - 1
                trnsjjualno.Text = objtablemst.Rows(c1).Item("trnsjjualno")
                trnsjjualsenddate.Text = Format(CDate(objtablemst.Rows(c1).Item("trnsjjualdate")), "dd/MM/yyyy")
                salesdeliveryshipdate.Text = Format(CDate(objtablemst.Rows(c1).Item("delivdate")), "dd/MM/yyyy")
                OidSO.Text = objtablemst.Rows(c1).Item("ordermstoid")
                orderno.Text = sjorderno.Text
                trncustoid.Text = objtablemst.Rows(c1).Item("custoid")
                note.Text = objtablemst.Rows(c1).Item("note").ToString
                posting.Text = objtablemst.Rows(c1).Item("trnsjjualstatus").ToString
                trntaxpct.Text = ToDouble(objtablemst.Rows(c1).Item("trntaxpct"))
                nopolisi.Text = Trim(objtablemst.Rows(c1).Item("noExpedisi").ToString)
                Ekspedisi.Text = Trim(objtablemst.Rows(c1).Item("expedisioid"))
                currencyoidSo.Text = Trim(objtablemst.Rows(c1).Item("currencyoid"))
                trnSjtype.Text = Trim(objtablemst.Rows(c1).Item("trnordertype").ToString)
            Next
        End If

        sSql = "SELECT trnordermstoid oid, seq trnsjjualdtlseq, d.trnsjjualdtloid, (od.amtdtldisc1/od.trnorderdtlqty) + (od.amtdtldisc2/od.trnorderdtlqty) discperqty, od.trnorderprice, d.trnorderdtloid,d.qty, d.note, p.trnsjjualmstoid,d.refoid itemoid, od.trndiskonpromo, d.unitoid as itemunitoid, ISNULL((Select sum(qty) From ql_trnsjjualdtl Where trnsjjualmstoid in (select trnsjjualmstoid from ql_trnsjjualmst Where trnsjjualstatus IN ('Approved','INVOICED') AND trnorderdtloid = od.trnorderdtloid and od.itemoid=d.refoid AND d.Branch_code=od.branch_code)),0.00) as deliveredqty, od.trnorderdtlqty, od.trnordermstoid, od.trnorderprice*qty AmtJual, d.mtrlocoid, ISNULL((od.amtdtldisc1/od.trnorderdtlqty) * d.qty,0.00) disc1, ISNULL((od.amtdtldisc2/od.trnorderdtlqty) * d.qty,0.00) disc2, ISNULL(trndiskonpromo/qty,0.00) discpromoperqty, ((od.trnorderprice*qty)-ISNULL(trndiskonpromo,0.00))-(ISNULL((od.amtdtldisc1/od.trnorderdtlqty) * d.qty,0.00)+ISNULL((od.amtdtldisc2/od.trnorderdtlqty) * d.qty,0.00)) NettoAmt, d.refoid FROM ql_trnsjjualdtl d INNER JOIN ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid INNER JOIN ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid AND d.refoid=od.itemoid AND od.branch_code=p.branch_code INNER JOIN QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'LOCATION' INNER JOIN ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' INNER JOIN ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid WHERE d.trnsjjualmstoid =p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid AND d.trnsjjualmstoid = " & sjjualmstoid.Text & " AND d.cmpcode='" & CompnyCode & "' AND od.trnordermstoid = " & OidSO.Text & " AND p.branch_code='" & zCabang.Text & "' ORDER BY trnsjjualdtlseq"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
        Session("TblDtl") = objTable

        If objTable.Rows.Count < 1 Then
            errMsg &= -"Maaf, Detail Sales Delivery Order data tidak ada, silahkan cek di form transaksi Sales delivery Order data!! <br>"
        End If

        Dim sMsg As String = "" : Dim csQty As String = 0

        sSql = "Select COUNT(trnsjjualmstoid) From ql_trnsjjualmst Where trnsjjualstatus='Approved' And trnsjjualmstoid=" & sjjualmstoid.Text & " AND branch_code='" & zCabang.Text & "' AND to_branch='" & zCabang.Text & "' AND ordermstoid='" & OidSO.Text & "'"
        cekDO = GetScalar(sSql)
        If ToDouble(cekDO) > 0 Then
            errMsg &= "- Maaf, Nomer Draft Ini sudah di approved..!!<br>"
            MsgNya.Text = "1"
        End If

        sSql = "Select COUNT(trnsjjualmstoid) From QL_trnjualmst Where trnjualstatus='POST' And branch_code='" & zCabang.Text & "' And trnsjjualmstoid=" & sjjualmstoid.Text & " AND ordermstoid='" & OidSO.Text & "'"
        CekSI = GetScalar(sSql)

        If ToDouble(CekSI) > 0 Then
            errMsg &= "- Maaf, Nomer Draft Ini sudah dicetak invoice..!!<br>"
            MsgNya.Text = "1"
        End If 

        For C1 As Integer = 0 To objTable.Rows.Count - 1
            sSql = "Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) FROM QL_conmtr con Inner Join ql_mstitem i ON i.itemoid=con.refoid Where con.refoid=" & objTable.Rows(C1).Item("itemoid") & " AND mtrlocoid=" & objTable.Rows(C1).Item("mtrlocoid") & " AND periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(trnsjjualsenddate.Text))) & "' AND branch_code='" & zCabang.Text & "' GROUP BY refoid, mtrlocoid, branch_code, i.stockflag, periodacctg Order By con.refoid"
            Dim sTock As Double = GetScalar(sSql)
            sSql = "Select itemdesc from ql_mstitem Where itemoid = " & objTable.Rows(C1).Item("itemoid").ToString & ""
            Dim Item As String = GetScalar(sSql)
            If ToDouble(objTable.Rows(C1).Item("qty").ToString) > sTock Then
                errMsg &= "- Maaf, Item " & Item & " Jumlah Stock " & sTock & ", Stock tidak memenuhi !! <br>"
            End If
        Next

        If errMsg <> "" Then
            showMessage(errMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        'generate NO
        Dim awal As String = "" : Dim awalSI As String = ""
        Dim lokasi As String = "" : Dim tax As String = ""
        Dim Konsi As String = ""

        If sjorderno.Text <> "" Then
            Dim cekawal As String = sjorderno.Text.Substring(1, 1)
            'If cekawal = "O" Then
            If trnSjtype.Text = "GROSIR" Then : awal = "DO" : awalSI = "SI"
            Else : awal = "DOJ" : awalSI = "SIJ" : End If
            lokasi = sjorderno.Text.Substring(10, 1)
            tax = sjorderno.Text.Substring(2, 1)
        Else
            If trnSjtype.Text = "GROSIR" Then : awal = "DO" : awalSI = "SI"
            Else : awal = "DOJ" : awalSI = "SIJ" : End If
            lokasi = "L" : tax = "1"
        End If

        If SeTipe.Text = "Konsinyasi" Then : Konsi = "K" : End If
        '------- genrated no DO ------
        '=============================
        sSql = "Select genother1 From ql_mstgen Where gengroup = 'CABANG' and gencode='" & zCabang.Text & "'"
        Dim CabangKirim As String = GetStrData(sSql)
        Dim sNo As String = awal & tax & Konsi & "/" & CabangKirim & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnsjjualno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trnsjjualmst WHERE cmpcode='" & CompnyCode & "' AND trnsjjualno LIKE '" & sNo & "%'"
        trnsjjualno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)

        '------- genrated no SI ------
        '=============================
        'sSql = "Select genother1 From ql_mstgen where gengroup = 'Cabang' and gencode='" & zCabang.Text & "'"
        Dim CbgBuatSI As String = cKoneksi.ambilscalar(sSql)
        Dim sNoSI As String = awalSI & tax & Konsi & "/" & CabangKirim & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trnjualmst WHERE cmpcode='" & CompnyCode & "' AND trnjualno LIKE '" & sNoSI & "%'"
        trnjualno.Text = GenNumberString(sNoSI, "", cKoneksi.ambilscalar(sSql), 4)
        Dim SnApproval As String = trnsjjualno.Text

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If statusUser = "FINAL" Then
                sSql = "select count(distinct i.payoid) from ql_trnsjjualdtl d inner join QL_mstItem i on i.itemoid = d.refoid and d.refname = 'ql_mstitem' inner join ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid And m.branch_code=d.branch_code inner join QL_mstgen g on i.payoid = g.genoid Where d.cmpcode = '" & CompnyCode & "' And d.branch_code='" & zCabang.Text & "' And d.trnsjjualmstoid = " & sjjualmstoid.Text & " AND m.ordermstoid=" & Integer.Parse(OidSO.Text) & ""
                xCmd.CommandText = sSql
                Dim insertxali As Integer = xCmd.ExecuteScalar
                TOP.SelectedIndex = 0

                sSql = "Update ql_trnsjjualmst SET trnsjjualno='" & Tchar(trnsjjualno.Text) & "', trnsjjualstatus='Approved', updtime=CURRENT_TIMESTAMP, trnsjjualdate='" & GetServerTime() & "' WHERE branch_code='" & zCabang.Text & "' AND ordermstoid='" & OidSO.Text & "' And cmpcode='" & CompnyCode & "' And trnsjjualmstoid=" & sjjualmstoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dt As DataTable = Session("TblDtl")
                trnamtjual = dt.Compute("SUM(AmtJual)", "")
                trnamtjualnetto = dt.Compute("SUM(NettoAmt)", "")
                diskondtl = dt.Compute("SUM(discperqty)", "")
                amtjualnetto = 0
                If tax = "0" Then
                    amtjualnetto = Math.Round(ToDouble(trnamtjualnetto) + ToDouble(trnamtjualnetto) * ToDouble(trntaxpct.Text) / 100)
                Else
                    amtjualnetto = Math.Round(ToDouble(trnamtjualnetto), 2)
                End If


                If termin <> 0 Then
                    If crLimit = 0 Then
                        showMessage("Maaf, Customer " & sjcustname.Text & " Memiliki Termin " & termin & " Hari, Credit Limit 0 hanya untuk customer dengan termin cash, Silahkan Hubungi Admin untuk pengajuan credit limit!", CompnyName & " - WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If
                End If

                If crLimit > 0 Then
                    sSql = "Select Count(genoid) from QL_mstgen where genother4='Y' and gengroup='cabang' AND gencode='" & zCabang.Text & "'"
                    Dim skip As Integer = GetStrData(sSql)
                    If confirmCR.Text.Trim = "" Then
                        ' DO bisa di-Approve selama USAGE+DO < CL
                        If skip = 0 Then
                            Dim g As Double = ToDouble(crLimit) - (ToDouble(crUsage) + ToDouble(amtjualnetto))
                            If ToDouble(g) < 0 Then
                                showMessage("Maaf, Sisa Credit Limit Untuk customer " & sjcustname.Text & " tidak mencukupi." & _
                                    "<BR>- Sisa Credit Limit = " & ToMaskEdit((ToDouble(crLimit) - ToDouble(crUsage)), 4) & "<BR>" & _
                                    "- Jumlah Transaksi = " & ToMaskEdit(ToDouble(amtjualnetto), 4), CompnyName & " - WARNING", 2)
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            End If
                        End If
                    End If

                    ' Cek Customer Flag
                    sSql = "select custflag from ql_mstcust where custoid = " & trncustoid.Text & ""
                    xCmd.CommandText = sSql : Dim custflag As String = xCmd.ExecuteScalar

                    If custflag.ToString.ToUpper = "INACTIVE" Then
                        showMessage("Maaf, Customer " & sjcustname.Text & " Dalam Status " & custflag, CompnyName & " - WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    End If

                    sSql = "select isnull(custdtlstatus,'') from ql_mstcustdtl where custoid = " & trncustoid.Text & " and custdtlstatus = 'Enabled' and custdtlflag = 'Over Due'"
                    xCmd.CommandText = sSql : Dim custflagoverdue As String = xCmd.ExecuteScalar

                    If custflagoverdue <> "Enabled" Then
                        'Cek piutang jika DD + 7 < datenow() 
                        sSql = "SELECT COUNT(-1) FROM (" & _
                            "select j.trnjualmstoid, j.trnamtjualnettoidr, j.accumpaymentidr,j.amtreturidr, j.trnjualdate," & _
                            " (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate," & _
                            " CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today " & _
                            "From QL_trnjualmst j WHERE j.trncustoid=" & trncustoid.Text & " AND j.trnjualstatus='POST' AND branch_code='" & zCabang.Text & "') AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)"
                        xCmd.CommandText = sSql : Dim countOD As Integer = xCmd.ExecuteScalar
 
                        sSql = "SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM (" & _
                            "select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate," & _
                            "(SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate," & _
                            "CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today " & _
                            "from QL_trnjualmst j WHERE j.trncustoid=" & trncustoid.Text & " AND j.trnjualstatus='POST' AND branch_code='" & zCabang.Text & "') AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)"
                        xCmd.CommandText = sSql : Dim sumOD As Double = xCmd.ExecuteScalar

                        If countOD > 0 Then
                            showMessage("Maaf, Customer " & sjcustname.Text & " Masih Memiliki Nota Outstanding." & _
                            "<BR>- Total Nota yang Masih Outstanding " & countOD & "<BR>" & _
                            "- Jumlah Transaksi = " & ToMaskEdit(ToDouble(sumOD), 4), CompnyName & " - WARNING", 2)
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    sSql = "update ql_mstcustgroup set custgroupcreditlimitusage=ISNULL(custgroupcreditlimitusage,0.00)+" & ToDouble(amtjualnetto) & ",custgroupcreditlimitusagerupiah=ISNULL(custgroupcreditlimitusagerupiah,0.00)+" & ToDouble(amtjualnetto) & " WHERE cmpcode = '" & CompnyCode & "' And custgroupoid=(Select custgroupoid from QL_mstcust Where custoid=" & trncustoid.Text & " And branch_code='" & zCabang.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update QL_mstcust set custcreditlimitusage=ISNULL(custcreditlimitusage,0.00)+" & ToDouble(amtjualnetto) & ",custcreditlimitusagerupiah=ISNULL(custcreditlimitusagerupiah,0.00)+" & ToDouble(amtjualnetto) & " Where custoid=" & trncustoid.Text & " AND branch_code='" & zCabang.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                Dim flagCash As String = ""
                sSql = " SELECT flagCash FROM QL_TRNORDERMST Where ordermstoid=" & OidSO.Text & " AND branch_code='" & zCabang.Text & "'"
                xCmd.CommandText = sSql : flagCash = xCmd.ExecuteScalar

                sSql = "insert into QL_trnjualmst ([cmpcode],[trnjualmstoid],[orderno],[trnsjjualno],[periodacctg],[trnjualtype],[trnjualno],[trnjualdate],[trnjualref],[trncustoid],[trncustname],[trnpaytype],[trnamtjual],[trnamtjualidr],[trnamtjualusd],[trntaxpct],[trnamttax],[trnamttaxidr],[trnamttaxusd],[trndisctype],[trndiscamt],[trndiscamtidr],[trndiscamtusd],[trnamtjualnetto],[trnamtjualnettoidr],[trnamtjualnettousd],[trnjualnote],[trnjualstatus],[trnjualres1],[amtdischdr],[amtdiscdtl],[accumpayment],[lastpaymentdate],[userpay],[createuser],[upduser],[updtime],[refNotaRevisi],[salesoid],[trnorderstatus],[ekspedisioid],[amtdischdr2],[trndisctype2],[trndiscamt2],[postacctg],[amtretur],[currencyoid],[currencyrate],[spgoid],[upduser_edit],[updtime_edit],[finalappovaldatetime], [finalapprovaluser],[finalapprovalcode],[canceluser],[canceltime],[cancelnote],[trnamtjualnettoacctg], [statusDN], [statusCN], [branch_code],trnsjjualmstoid, flagCash, ordermstoid) " & _
                " VALUES ('" & CompnyCode & "'," & trnjualmstoid & ",'" & sjorderno.Text & "','" & trnsjjualno.Text & "','" & GetDateToPeriodAcctg(GetServerTime()) & "','" & trnSjtype.Text & "','" & trnjualno.Text & "','" & GetServerTime() & "',''," & trncustoid.Text & ",'" & Tchar(sjcustname.Text) & "'," & TOP.SelectedValue & "," & ToDouble(trnamtjual) & "," & ToDouble(trnamtjual) & "," & ToDouble(trnamtjual) & "," & ToDouble(trntaxpct.Text) & "," & ToDouble(trnamtjual) * ToDouble(trntaxpct.Text) / 100 & "," & (ToDouble(trnamtjual) * ToDouble(trntaxpct.Text) / 100) & "," & (ToDouble(trnamtjual) * ToDouble(trntaxpct.Text) / 100) & ",'AMT',0,0,0," & ToDouble(amtjualnetto) & "," & ToDouble(amtjualnetto) & "," & ToDouble(amtjualnetto) & ",'','POST','',0,'" & ToDouble(diskondtl) & "',0,'1/1/1900',0,'" & crtUser & "','" & crtUser & "',CURRENT_TIMESTAMP,'',0,''," & Ekspedisi.Text & ",0,'AMT',0,'',0,'" & 1 & "','" & 1 & "'," & SpgOid.Text & ",'','1/1/1900','1/1/1900','','','','1/1/1900',''," & ToDouble(amtjualnetto) & ",0,0, '" & zCabang.Text & "'," & sjjualmstoid.Text & ", '" & flagCash & "'," & Integer.Parse(OidSO.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & trnjualmstoid & " where tablename ='QL_trnjualmst' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim cntr As Integer = 0 : Dim lstRollOid As Integer = 0
                Dim seq As Integer = 0 : Dim tampstatus(objTable.Rows.Count - 1) As String

                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    seq = seq + 1

                    sSql = "insert into QL_trnjualdtl ([cmpcode], [trnjualdtloid], [trnjualmstoid], [trnjualdtlseq], [itemloc], [itemoid], [trnjualdtlqty], [trnjualdtlunitoid], [unitSeq], [trnjualdtlprice], [trnjualdtlpriceunitoid], [trnjualdtldisctype], [trnjualdtldiscqty], [trnjualflag], [trnjualnote], [trnjualstatus], [amtjualdisc], [amtjualnetto], [createuser], [upduser], [updtime], [trnjualdtldisctype2], [trnjualdtldiscqty2], [amtjualdisc2], [trnorderdtloid], [trnjualdtlqty_retur], [trnjualdtlqty_retur_unit3], [trnjualdtlqty_unit3], trnsjjualdtloid, branch_code, amtjualdisc1)" & _
                    " VALUES ('" & CompnyCode & "', " & (CInt(trnjualdtloid) + C1) & ", " & trnjualmstoid & ", " & seq & ", " & (objTable.Rows(C1).Item("mtrlocoid")) & ", " & (objTable.Rows(C1).Item("itemoid")) & ", " & ToDouble(objTable.Rows(C1).Item("qty")) & ", " & (objTable.Rows(C1).Item("itemunitoid")) & ", " & (objTable.Rows(C1).Item("trnsjjualdtlseq")) & ", " & ToDouble(objTable.Rows(C1).Item("trnorderprice")) & ", " & (objTable.Rows(C1).Item("itemunitoid")) & ", 'AMT', " & ToDouble(objTable.Rows(C1).Item("discperqty")) & ", '', '', 'In Process', " & ToDouble(objTable.Rows(C1).Item("trndiskonpromo")) & ", " & ToDouble(objTable.Rows(C1).Item("NettoAmt")) & ", '" & Session("UserID") & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'AMT', 0, " & ToDouble(objTable.Rows(C1)("disc2")) & ", " & objTable.Rows(C1)("trnorderdtloid") & ", 0, 0, 0, " & objTable.Rows(C1)("trnsjjualdtloid") & ", '" & zCabang.Text & "', " & ToDouble(objTable.Rows(C1)("disc1")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'If posting.Text = "POST" Then
                    sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "Update ql_trnorderdtl set orderdelivqty = (IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("qty") & "),trnorderdtlstatus=(select case when trnorderdtlqty <= IsNull(orderdelivqty,0) + " & objTable.Rows(C1).Item("qty") & " then 'Completed' else 'In Process' End) Where ql_trnorderdtl.itemoid=" & objTable.Rows(C1).Item("itemoid") & " And ql_trnorderdtl.trnorderdtloid=" & objTable.Rows(C1).Item("trnorderdtloid") & " and ql_trnorderdtl.trnordermstoid= " & OidSO.Text & " AND branch_code='" & zCabang.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Dim cek As String = 1 : Dim qtyterkonversi As Double = 1
                    qtyterkonversi = ToDouble(cek) * ToDouble(objTable.Rows(C1).Item("qty"))
                    '--------------------------
                    Dim lasthpp As Double = 0
                    sSql = "Select hpp from ql_mstitem where itemoid=" & objTable.Rows(C1).Item("itemoid") & ""
                    xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                    '------------------------------
                    'insert into connmtr if posting
                    sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, formoid, formname, type, trndate, periodacctg, refoid, refname, qtyout, amount, note, formaction, upduser, updtime, mtrlocoid, unitoid, hpp, branch_code) " & _
                    " VALUES ('" & CompnyCode & "', " & conmtroid & ", " & (objTable.Rows(C1).Item("trnsjjualdtloid")) & ", 'QL_trnsjjualdtl', 'JUAL','" & GetServerTime() & "', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & objTable.Rows(C1).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(C1).Item("qty") & ", " & objTable.Rows(C1).Item("qty") & " * " & ToMaskEdit(ToDouble(" (select trnamountdtl/trnorderdtlqty from ql_trnorderdtl Where to_branch='" & zCabang.Text & "' And trnorderdtloid = " & objTable.Rows(C1).Item("trnorderdtloid") & ")"), 4) & ", '" & Tchar(objTable.Rows(C1).Item("note")) & "', '" & trnsjjualno.Text & "', '" & Session("UserID") & "', current_timestamp, " & objTable.Rows(C1).Item("mtrlocoid") & ", " & (objTable.Rows(C1).Item("itemunitoid")) & ", " & ToDouble(lasthpp) & ", '" & zCabang.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    conmtroid += 1

                    sSql = "update QL_mstoid set lastoid=" & (conmtroid - 1) & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "SELECT Case When SUM(trnorderdtlqty-orderdelivqty)=0 Then 'CLOSED' Else 'NO' End From ql_trnorderdtl Where branch_code='" & zCabang.Text & "' AND trnordermstoid=" & OidSO.Text & ""
                xCmd.CommandText = sSql
                Dim jumProsess As String = xCmd.ExecuteScalar

                If jumProsess = "CLOSED" Then
                    sSql = "UPDATE QL_trnordermst SET trnorderstatus = 'Closed' WHERE ordermstoid = " & OidSO.Text & " And branch_code = '" & zCabang.Text & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update SN
                'sSql = "SELECT s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnjualmstoid = '" & SnApproval & "'"
                'Dim TampungSN As DataTable = cKon.ambiltabel(sSql, "TampungSN")

                'For iSN As Integer = 0 To TampungSN.Rows.Count - 1
                '    sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = '" & CDate(toDate(trnsjjualsenddate.Text)) & "', status_item = 'Approved', status_in_out = 'Out', last_trans_type = 'Jual' Where sn = '" & TampungSN.Rows(iSN).Item("SN") & "'"
                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '    sSql = "update QL_Mst_SN set status_approval = 'Approved',trnjualmstoid = '" & trnsjjualno.Text & "', tgljual = '" & CDate(toDate(trnsjjualsenddate.Text)) & "', status_in_out = 'Out', last_trans_type = 'Jual' Where id_sn = '" & TampungSN.Rows(iSN).Item("SN") & "'"
                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Next

                sSql = "update QL_mstoid set lastoid=" & (objTable.Rows.Count - 1 + CInt(trnjualdtloid)) & " Where tablename = 'ql_trnjualdtl' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'done insert ke stock

                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='ql_trnsjjualmst' " & _
                       " And branch_code='" & zCabang.Text & "'" & _
                       "and oid=" & sjjualmstoid.Text & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            sSql = "Select genother1 from QL_mstgen Where gengroup = 'paytype' and genoid = " & TOP.SelectedValue
            xCmd.CommandText = sSql : Dim cekday As String = xCmd.ExecuteScalar

            'Dim cekday As String = GetStrData("Select genother1 from QL_mstgen Where gengroup = 'paytype' and genoid = " & TOP.SelectedValue)
            Dim days As Double = 0
            If cekday = "?" Or cekday = "" Or cekday = Nothing Then
                cekday = 0
            End If

            days = CInt(cekday)
            Dim vConARId As Integer = GenerateID("QL_conar", CompnyCode)
            Dim dPayDueDate As Date = GetServerTime().AddDays(days)

            Dim sVarAR As String = GetInterfaceValue("VAR_AR", zCabang.Text)
            If sVarAR = "?" Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            Dim iAROid As Integer = GetAccountOid(sVarAR, False)
            sSql = "SELECT trnorderdate FROM QL_trnordermst WHERE ordermstoid=" & OidSO.Text & " AND branch_code='" & zCabang.Text & "'"
            xCmd.CommandText = sSql : Dim sorderdate As Date = xCmd.ExecuteScalar()
            Dim dPayDueDateOff As Date = sorderdate.AddDays(days)

            ' Dim orderdate As String = GetStrData(sSql)
            ' =============== QL_conar ================
            sSql = "INSERT INTO QL_conar (cmpcode,branch_code,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnarflag,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,trnarnote, trnarres1,upduser,updtime) VALUES " & _
            "('" & CompnyCode & "','" & zCabang.Text & "'," & vConARId & ",'QL_trnjualmst'," & trnjualmstoid & ",0," & trncustoid.Text & ",'" & iAROid & "','POST','','PIUTANG','" & GetServerTime() & "','" & GetDateToPeriodAcctg(GetServerTime()) & "',0,'1/1/1900','',0,'" & IIf(OidSO.Text > 0, dPayDueDate, dPayDueDateOff) & "'," & ToDouble(amtjualnetto) & "," & ToDouble(amtjualnetto) & "," & ToDouble(amtjualnetto) & ",0,'SALES INVOICE (" & Tchar(sjcustname.Text) & "|NO=" & trnjualno.Text & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & vConARId & " where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '=================Insert Auto Jurnal=========================
            Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
            Dim dvJurnal As DataView = dtJurnal.DefaultView

            Dim vSeqDtl As Integer = 1
            ' dvJurnal.RowFilter = "seq=1"
            ' JURNAL SALES INVOICE
            If dvJurnal.Count > 0 Then
                ' ============== QL_trnglmst
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,branch_code,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,type)" & _
                    "VALUES ('" & CompnyCode & "'," & vIDMst & ",'" & zCabang.Text & "','" & GetServerTime() & "','" & GetDateToPeriodAcctg(GetServerTime()) & "','SALES INVOICE (" & Tchar(sjcustname.Text) & "|NO=" & trnjualno.Text & ")','POST','" & GetServerTime().Date & "', '" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'SI')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim dtj As String = dvJurnal.Count
                For C2 As Integer = 0 To dvJurnal.Count - 1
                    Dim dAmt As Double = 0 : Dim sDBCR As String = ""
                    If dvJurnal(C2)("dbcr").ToString = "D" Then
                        dAmt = ToDouble(dvJurnal(C2)("debet").ToString) : sDBCR = "D"
                    Else
                        dAmt = ToDouble(dvJurnal(C2)("credit").ToString) : sDBCR = "C"
                    End If
                    ' ============== QL_trngldtl =============
                    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,upduser,updtime,glpostdate) " & _
                        "VALUES ('" & CompnyCode & "','" & zCabang.Text & "'," & vIDDtl & "," & vSeqDtl & "," & vIDMst & "," & dvJurnal(C2)("id").ToString & ",'" & sDBCR & "'," & dAmt & "," & dAmt & "," & dAmt & ",'" & trnjualno.Text & "','SALES INVOICE (" & Tchar(sjcustname.Text) & "|NO=" & trnjualno.Text & ")','','" & trnjualreturmstoid.Text & "','POST','" & Session("UserID") & "',current_timestamp,current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vSeqDtl += 1 : vIDDtl += 1
                Next
            End If
            dvJurnal.RowFilter = "" : vIDMst += 1 : vSeqDtl = 1

            'update lastoid ql_trnglmst
            sSql = "update QL_mstoid set lastoid=" & vIDMst & " Where tablename ='QL_trnglmst' and cmpcode = '" & CompnyCode & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'update lastoid ql_trngldtl
            sSql = "update QL_mstoid set lastoid=" & vIDDtl - 1 & " Where tablename ='QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'recovery credit limit
            sSql = "UPDATE c set custcreditlimitusagerupiah= isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur)) climit FROM QL_trnjualmst WHERE trnjualstatus = 'POST' AND trnamtjualnetto - (accumpayment + amtretur) > 0 AND custoid = trncustoid GROUP BY trncustoid),0),custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur)) climit FROM QL_trnjualmst WHERE trnjualstatus = 'POST' AND trnamtjualnetto - (accumpayment + amtretur ) > 0 AND custoid = trncustoid GROUP BY trncustoid ),0) FROm QL_mstcust c"

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '============================================================
            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setSDOasActive()
    End Sub 

    Protected Sub imbBackSDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setSDOasActive()
    End Sub

    Protected Sub lkbPDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPDO.Click
        setPDOasActive()
    End Sub

    Private Sub setPDOasActive()
        bindPDO() : trnsjbelino.Text = "" : trnsjreceivedate.Text = ""
        suppnamepdo.Text = "" : trnbeliponosj.Text = "" : identifierpo.Text = ""
        Session("approvaloid") = Nothing : Session("trnsjbelimstoid") = Nothing : MultiView1.SetActiveView(View14)
    End Sub

    Sub createTableDtlBooking()
        Dim dtlDS As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("TabelDtlBonusPromo")
        dtlTable.Columns.Add("cmpcode", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("allocorderoid", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("ordermstoid", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("acctgperiod", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("locseq", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("whlocoid", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("unitoid", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("packingqty", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("allocationqty", System.Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("deliveredqty", System.Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("allocstatus", System.Type.GetType("System.String"))
        dtlDS.Tables.Add(dtlTable)
        Session("TabelDtlBooking") = dtlTable
    End Sub

    Sub processbooking()

        Session("TabelDtlBooking") = Nothing
        gvMst.DataSource = Nothing
        gvMst.DataBind()

        gvMst.Visible = True

        createTableDtlBooking()
        Dim Warning As String = ""

        Dim objTablePromoTrigger As DataTable = Session("bookingTrigger") '--> objtable utk promo
        Dim objTable As DataTable = Session("TabelDtl") '--> Objtable utk detail order
        Dim tempBookingString As String = ""
        Dim tempItemoid As String = 0



        'looping order utk mencari itemBO nya
        For c4 As Integer = 0 To objTable.Rows.Count - 1
            tempItemoid = objTable.Rows(c4).Item("matoid") & " , " & tempItemoid
        Next

        sSql = "select m.ordermstoid, m.orderno ,CONVERT(varchar(10),m.trnorderdate,103) orderdate,  m.trncustname ,case m.consigneeoid when 0 then (select gendesc from QL_mstgen where genoid = c.custcityoid ) else (select gendesc from QL_mstgen where genoid in (select conscityoid from QL_mstcustcons where custoid = c.custoid and consoid = m.consigneeoid)) end delivery ,d.itemoid ,i.itemcode ,i.itemdesc + ' - ' + i.merk itemdesc ,satuan3 unitoid, unit.gendesc unit , d.trnorderdtlqty qtyorder, isnull(aloc.allocationqty,0) allocationqty,d.trnorderdtlqty - isnull(aloc.allocationqty,0) needbooking,isnull(aloc.deliveredqty,0) deliveredqty,0.0 saldoawal, '' note,sjdtl.mtrlocoid  genoid, 0.0 amount from QL_trnorderdtl d inner join QL_trnordermst m on d.trnordermstoid = m.ordermstoid and m.orderno like '%B%' inner join QL_mstcust c on m.trncustoid = c.custoid  inner join QL_mstItem i on d.itemoid = i.itemoid  inner join QL_mstgen unit on unit.genoid = i.satuan3  inner join ql_trnsjbelidtl sjdtl on sjdtl.refname = 'QL_MSTITEM' and sjdtl.refoid = d.itemoid and trnsjbelioid = " & Session("trnsjbelimstoid") & " left join QL_trnorderallocdtl aloc on m.ordermstoid = aloc.ordermstoid and d.itemoid = aloc.itemoid where m.trnorderstatus = 'Approved' and d.trnorderdtlstatus <> 'Completed' and d.itemoid in (" & tempItemoid & ")"
        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "TabelDtlBooking")
        gvMst.DataSource = tbDtl
        gvMst.DataBind()
        Session("TabelDtlBooking") = tbDtl
    End Sub

    Protected Sub newprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TabelDtlBooking") Is Nothing Then
            lblwarningBO.Text = ""
            Dim iError As Int16 = 0
            Dim objTable As DataTable = Session("TabelDtlBooking")
            Dim objTablecek As DataTable = Session("TabelDtl")
            Dim Alloc As Decimal = 0
            Dim tempitemoid As String = ""
            Dim tempQTYpo As Decimal = 0
            Dim gvr As GridViewRow
            Dim tbqty As System.Web.UI.WebControls.TextBox

            Dim tbox As System.Web.UI.WebControls.TextBox = TryCast(sender, System.Web.UI.WebControls.TextBox)
            Dim tboxval As Double = 0.0
            If Double.TryParse(tbox.Text, tboxval) = True Then
                tbox.Text = Format(tboxval, "#,##0.00")
            Else
                tbox.Text = Format(0, "#,##0.00")
            End If
            For c2 As Integer = 0 To objTable.Rows.Count - 1
                gvr = gvMst.Rows(c2)
                tbqty = gvr.FindControl("newqty")
                If objTable.Rows(c2).Item("needbooking") < tbqty.Text Then
                    lblwarningBO.Text = "Allocation is over, Please cek again !!"
                    tbox.Text = Format(0, "#,##0.00")
                    Exit Sub
                End If
            Next
            'cek smua allocation
            For c1 As Integer = 0 To objTablecek.Rows.Count - 1
                tempitemoid = 0
                Alloc = 0
                tempitemoid = objTablecek.Rows(c1).Item("matoid")
                tempQTYpo = objTablecek.Rows(c1).Item("trnsjbelidtlqty")

                For c4 As Integer = 0 To objTable.Rows.Count - 1
                    gvr = gvMst.Rows(c4)
                    tbqty = gvr.FindControl("newqty")
                    If objTable.Rows(c4).Item("itemoid") = tempitemoid Then
                        Alloc = ToDouble(tbqty.Text) + ToDouble(Alloc)
                    End If
                Next

                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                Dim qtyterkonversi As Decimal = 0
                sSql = "select satuan1 from ql_mstitem where itemoid =" & objTablecek.Rows(c1)("matoid") & ""
                xCmd.CommandText = sSql
                If xCmd.ExecuteScalar = objTablecek.Rows(c1)("trnsjbelidtlunitoid") Then
                    sSql = "select konversi1_2 * konversi2_3 from ql_mstitem where itemoid =" & objTablecek.Rows(c1)("matoid") & ""
                    xCmd.CommandText = sSql
                    qtyterkonversi = objTablecek.Rows(c1)("trnsjbelidtlqty") * xCmd.ExecuteScalar
                Else
                    sSql = "select satuan2 from ql_mstitem where itemoid =" & objTablecek.Rows(c1)("matoid") & ""
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteScalar = objTablecek.Rows(c1)("trnsjbelidtlunitoid") Then
                        sSql = "select konversi2_3 from ql_mstitem where itemoid =" & objTablecek.Rows(c1)("matoid") & ""
                        xCmd.CommandText = sSql
                        qtyterkonversi = objTablecek.Rows(c1)("trnsjbelidtlqty") * xCmd.ExecuteScalar
                    Else
                        qtyterkonversi = objTablecek.Rows(c1)("trnsjbelidtlqty")
                    End If
                End If

                If Alloc > qtyterkonversi Then
                    lblwarningBO.Text = "Allocation must be less than detail Purchase Order, Please cek again !!"
                    tbox.Text = Format(0, "#,##0.00")
                    Exit Sub
                End If
            Next

            conn.Close()
        End If

    End Sub

    Protected Sub lkbSelectPDO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trnsjbelimstoid") = sender.ToolTip
        Session("approvaloid") = sender.CommandArgument

        Dim dt As DataTable = cKoneksi.ambiltabel("SELECT jb.branch_code,jb.trnsjbelioid, jb.trnsjbelino,jb.trnsjbelidate trnsjreceivedate,s.suppname,po.TRNBELIPONO pono ,jb.trnsjbelitype FROM ql_trnsjbelimst jb INNER JOIN QL_pomst po ON jb.cmpcode=po.cmpcode AND jb.trnsjbelitype in ('PO','BO') AND jb.trnbelipono=po.trnbelipono INNER JOIN ql_mstsupp s ON jb.cmpcode=s.cmpcode AND jb.cmpcode='" & CompnyCode & "' AND po.trnsuppoid=s.suppoid WHERE trnsjbelioid='" & Session("trnsjbelimstoid") & "'", "ql_trnsjbelimst")
        trnsjbelino.Text = dt.Rows(0).Item("trnsjbelino")
        trnsjreceivedate.Text = Format(CDate(dt.Rows(0).Item("trnsjreceivedate")), "dd/MM/yyyy")
        suppnamepdo.Text = dt.Rows(0).Item("suppname").ToString
        trnbeliponosj.Text = dt.Rows(0).Item("pono").ToString
        trnsjbelitype.Text = dt.Rows(0).Item("trnsjbelitype").ToString
        trnsjbelioid.Text = dt.Rows(0).Item("trnsjbelioid")
        sCbg.Text = dt.Rows(0).Item("branch_code").ToString

        'identifierpo.Text = dt.Rows(0).Item("identifierno").ToString
        sSql = "select row_number() OVER(ORDER BY d.trnsjbelidtloid) AS 'No', d.trnsjbelidtloid,d.trnsjbelioid trnsjbelimstoid,d.unitoid trnsjbelidtlunitoid,d.qty trnsjbelidtlqty,d.trnsjbelidtlseq,d.mtrlocoid matloc ,d.refoid matoid,d.trnsjbelinotedtl trnsjbelidtlnote,d.trnbelidtloid trnpodtloid,d.refname, (select itemdesc + ' - ' + merk from QL_mstitem where itemoid = pod.itemoid) as item, pod.trnbelidtlqty AS referedqty, isnull((select SUM(qty) From ql_trnsjbelidtl Where pod.trnbelidtloid=trnbelidtloid AND trnsjbelioid in (select trnsjbelioid From ql_trnsjbelimst where trnsjbelistatus in ('Approved','INVOICED') ) ),0) as receivedqty, 'setascomplete'='false',(SELECT G2.GENDESC FROM QL_MSTGEN G2 WHERE G2.GENOID = G.genother1 AND G2.gengroup = 'WAREHOUSE') + ' - ' + G.gendesc AS location,g1.gendesc AS unit,pod.trnbelidtlunit podtlunit, g3.gendesc AS unit2desc,d.expireddate,ISNULL(d.itembarcode,'') itembarcode1 FROM QL_trnsjbelidtl d INNER JOIN QL_trnsjbelimst p ON p.cmpcode=d.cmpcode AND p.trnsjbelioid=d.trnsjbelioid INNER JOIN QL_trnsjbelidtl pd ON pd.cmpcode=p.cmpcode AND pd.trnsjbelioid=p.trnsjbelioid INNER JOIN QL_podtl pod ON pod.cmpcode=p.cmpcode AND pod.trnbelimstoid=(select trnbelimstoid from ql_pomst where trnbelipono= p.trnbelipono) and pod.trnbelidtloid = d.trnbelidtloid AND pod.itemoid=d.refoid INNER JOIN QL_mstgen g ON g.cmpcode=d.cmpcode AND g.genoid=d.mtrlocoid INNER JOIN ql_mstgen g1 ON g1.cmpcode=d.cmpcode AND g1.genoid=d.unitoid INNER JOIN QL_mstgen g3 ON g3.cmpcode=pod.cmpcode AND g3.genoid=pod.trnbelidtlunit WHERE d.trnsjbelioid='" & Session("trnsjbelimstoid") & "' AND d.cmpcode='" & CompnyCode & "' group by d.trnsjbelidtlseq,pod.trnbelidtloid, d.trnsjbelidtloid,d.trnsjbelioid,d.unitoid,d.qty, d.mtrlocoid,d.refoid, d.unitoid,d.trnsjbelinotedtl,d.trnbelidtloid, pod.trnbelidtlqty ,g.gendesc ,g1.gendesc ,pod.trnbelidtlunit,pod.itemoid,d.refname,g3.gendesc,G.genother1,d.itembarcode,d.expireddate"

        FillGV(gvPDODetail, sSql, "QL_trnsjbelidtl")
        '---------------------- begin zipi ----------------------
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        gvPDODetail.DataSource = objDs.Tables("data")
        gvPDODetail.DataBind()
        Session("TabelDtl") = objDs.Tables("data")

        If trnsjbelitype.Text = "BO" Then
            processbooking()
        Else
            Session("TabelDtlBooking") = Nothing
            gvMst.DataSource = Nothing
            gvMst.DataBind()
            gvMst.Visible = False
        End If

        MultiView1.SetActiveView(View15)
    End Sub

    Protected Sub imbApprovePDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApprovePDO.Click
        If IsDBNull(Session("trnsjbelimstoid")) Then
            showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trnsjbelimstoid") = "" Then
                showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If trnsjbelino.Text = "" Then
            showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If

        sSql = "select genother1 from ql_mstgen where gengroup = 'cabang' and gencode='" & sCbg.Text & "'"
        Dim FrombarchPembuat As String = cKon.ambilscalar(sSql)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            Session("conmtroid") = GenerateID("QL_conmtr", CompnyCode)
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Dim statusUser As String = "" : Dim level As String = ""
            Dim maxLevel As String = ""
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnsjbelimst' and approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnsjbelimst'")

            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnsjbelimst' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnsjbelimst' and approvallevel=" & level + 1 & ""
                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                Dim awal As String = "" : Dim tax As String = ""
                If sjorderno.Text <> "" Then
                    awal = "LP" : tax = sjorderno.Text.Substring(2, 1)
                Else
                    awal = "LP" : tax = "1"
                End If
                Dim iCurID As Integer = 0
                Dim sNo As String = awal & "/" & FrombarchPembuat & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnsjbelino, 4) AS INT)), 0) FROM QL_trnsjbelimst WHERE cmpcode = '" & CompnyCode & "' AND trnsjbelino LIKE '" & sNo & "%'"
                iCurID = cKoneksi.ambilscalar(sSql) + 1
                Dim nomor As String = GenNumberString(sNo, "", iCurID, 4)
                trnsjbelino.Text = nomor

                'update SJ ke stock
                '-----------------------------------------------------------------------
                ' SELECT ql_trnsjbelimst tanggal ketika PO
                ' Select Tanggal PDO utk ambil stok akhir hari ini (sebelumnya pakai tanggal Delivery dari PO
                sSql = "SELECT a.trnsjbelidate trnsjreceivedate, trnsjbelitype FROM ql_trnsjbelimst a inner join QL_pomst b on a.trnbelipono = b.trnbelipono WHERE a.cmpcode='" & CompnyCode & "' AND a.trnsjbelioid=" & Session("trnsjbelimstoid")
                Dim tblMaster As DataTable = cKoneksi.ambiltabel(sSql, "QL_trnsjbelimst")
                Dim sSendDate As String : Dim Booking As String
                If tblMaster.Rows.Count > 0 Then
                    sSendDate = Format(CDate(tblMaster.Rows(0).Item("trnsjreceivedate").ToString), "dd/MM/yyyy")
                    Booking = tblMaster.Rows(0).Item("trnsjbelitype")
                Else
                    objTrans.Rollback() : conn.Close()
                    showMessage("Unable to load Purchase Delivery Order data!!", CompnyName & " - ERROR", 1)
                    Exit Sub
                End If

                sSql = "Select row_number() OVER(ORDER BY d.trnsjbelidtloid) AS 'No', d.trnsjbelidtloid, d.trnsjbelioid trnsjbelimstoid, d.unitoid trnsjbelidtlunitoid, d.qty trnsjbelidtlqty, d.trnsjbelidtlseq,d.mtrlocoid matloc, d.refoid matoid, d.trnsjbelinotedtl trnsjbelidtlnote, d.trnbelidtloid trnpodtloid, d.refname, (select itemdesc From QL_mstitem Where itemoid = pod.itemoid) as item, pod.trnbelidtlqty AS referedqty, isnull((select SUM(qty) from ql_trnsjbelidtl Where pod.trnbelidtloid=trnbelidtloid AND trnsjbelioid in (select trnsjbelioid from ql_trnsjbelimst Where trnsjbelistatus in ('Approved','INVOICED') ) ),0) as receivedqty,'setascomplete'='false',(SELECT G2.GENDESC FROM QL_MSTGEN G2 WHERE G2.GENOID = G.genother1 AND G2.gengroup = 'WAREHOUSE') + ' - ' + G.gendesc AS location, g1.gendesc AS unit, pod.trnbelidtlunit podtlunit, g3.gendesc AS unit2desc, 0 bookingstock, d.branch_code, pod.trnbelimstoid, d.expireddate, ISNULL(d.itembarcode,'') itembarcode1 FROM QL_trnsjbelidtl d INNER JOIN QL_trnsjbelimst p ON p.cmpcode=d.cmpcode AND p.trnsjbelioid=d.trnsjbelioid INNER JOIN QL_trnsjbelidtl pd ON pd.cmpcode=p.cmpcode AND pd.trnsjbelioid=p.trnsjbelioid INNER JOIN QL_podtl pod ON pod.cmpcode=p.cmpcode AND pod.trnbelimstoid=(select trnbelimstoid from ql_pomst where trnbelipono= p.trnbelipono) and pod.trnbelidtloid = d.trnbelidtloid AND pod.itemoid=d.refoid INNER JOIN QL_mstgen g ON g.cmpcode=d.cmpcode AND g.genoid=d.mtrlocoid INNER JOIN ql_mstgen g1 ON g1.cmpcode=d.cmpcode AND g1.genoid=d.unitoid INNER JOIN QL_mstgen g3 ON g3.cmpcode=pod.cmpcode AND g3.genoid=pod.trnbelidtlunit WHERE d.trnsjbelioid='" & Session("trnsjbelimstoid") & "' AND d.cmpcode='" & CompnyCode & "' group by d.trnsjbelidtlseq, pod.trnbelidtloid, d.trnsjbelidtloid, d.trnsjbelioid, d.unitoid, d.qty, d.mtrlocoid, d.refoid, d.unitoid, d.trnsjbelinotedtl, d.trnbelidtloid, pod.trnbelidtlqty, g.gendesc, g1.gendesc, pod.trnbelidtlunit, pod.itemoid, d.refname, g3.gendesc, G.genother1, d.branch_code, pod.trnbelimstoid, d.expireddate, d.itembarcode"
                Dim tblTemp As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjbelidtl")
                gvPDODetail.DataSource = tblTemp
                gvPDODetail.DataBind()
                Session("TabelDtl") = tblTemp
                sSql = "SELECT id_sn FROM QL_Mst_SN WHERE last_trans_type = 'Beli' AND trnbelimstoid = '" & Session("trnsjbelimstoid") & "'"
                Dim dtSNPO As DataTable = cKon.ambiltabel2(sSql, "dtSNHapus")
                Session("dtSNPO") = dtSNPO

                If tblTemp.Rows.Count < 1 Then
                    objTrans.Rollback() : conn.Close()
                    showMessage("Unable to load Detail Purchase Delivery Order data!!", CompnyName & " - ERROR", 1)
                    Exit Sub
                End If

                Dim conmtroid As Integer = GenerateID("QL_conmtr", CompnyCode)
                Dim crdmtroid As Integer = GenerateID("QL_crdmtr", CompnyCode)
                Dim oidTemp As Int64 = GenerateID("QL_histhpp", CompnyCode)
                Dim refnamepost As String = "QL_MSTITEM"

                For C1 As Integer = 0 To tblTemp.Rows.Count - 1
                    Dim MainQty As Double = 0, secQty As Double = 0, dHargaNetto_qty As Double = 0, dhargabarang As Double = 0, mainUnit As Integer = 0, secUnit As Integer = 0, QtyBeli As Decimal = 0, LastStock As Decimal = 0, last_hpp As Decimal = 0, ekspedisi As Decimal = 0, discdtl As Decimal = 0, dischdr As Decimal = 0, totaldischdr As Decimal = 0, jumlahPO As Decimal = 0, Voucher As Decimal = 0, RumusVoucher As Decimal = 0, flagVcr As Integer = 0, oidHis As Integer = 0, HargaNet, HppItem, StokMin As Double
                    If tblTemp.Rows(C1)("podtlunit") = tblTemp.Rows(C1)("trnsjbelidtlunitoid") Then
                        MainQty = ToDouble(tblTemp.Rows(C1)("trnsjbelidtlqty"))
                        mainUnit = tblTemp.Rows(C1)("trnsjbelidtlunitoid")
                    Else
                        secQty = ToDouble(tblTemp.Rows(C1)("trnsjbelidtlqty"))
                        secUnit = tblTemp.Rows(C1)("trnsjbelidtlunitoid")
                    End If
                    QtyBeli = tblTemp.Rows(C1)("trnsjbelidtlqty")

                    sSql = "select isnull((select (amtbelinetto/trnbelidtlqty) From ql_podtl where trnbelidtloid=" & tblTemp.Rows(C1).Item("trnpodtloid") & "),0.00) data"
                    xCmd.CommandText = sSql : dHargaNetto_qty = xCmd.ExecuteScalar

                    sSql = "select isnull((select trnbelidtlpriceidr From ql_podtl where trnbelidtloid=" & tblTemp.Rows(C1).Item("trnpodtloid") & "),0) data"
                    xCmd.CommandText = sSql : dhargabarang = xCmd.ExecuteScalar

                    sSql = "Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) From QL_conmtr Where refoid=" & tblTemp.Rows(C1).Item("matoid") & " AND periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(sSendDate))) & "'"
                    xCmd.CommandText = sSql : LastStock = xCmd.ExecuteScalar

                    sSql = "select hpp from QL_mstItem where itemoid=" & tblTemp.Rows(C1).Item("matoid")
                    xCmd.CommandText = sSql : last_hpp = xCmd.ExecuteScalar

                    sSql = "select isnull(biayaexpedisi,0.00) from ql_trnsjbelidtl where trnsjbelioid = " & trnsjbelioid.Text & " and refoid = " & tblTemp.Rows(C1).Item("matoid") & " And trnbelidtloid=" & tblTemp.Rows(C1).Item("trnpodtloid") & " "
                    xCmd.CommandText = sSql : ekspedisi = xCmd.ExecuteScalar

                    sSql = "SELECT CONVERT( DECIMAL ,p.amtdischdridr/(SELECT SUM(d.trnbelidtlqty) FROM ql_podtl d WHERE d.trnbelimstoid=p.trnbelimstoid )) From QL_pomst p WHERE p.trnbelimstoid = " & tblTemp.Rows(C1).Item("trnbelimstoid")
                    xCmd.CommandText = sSql : dischdr = xCmd.ExecuteScalar
                    totaldischdr = dischdr * tblTemp.Rows(C1).Item("trnsjbelidtlqty")

                    sSql = "SELECT amtdiscidr FROM QL_podtl where itemoid = " & tblTemp.Rows(C1).Item("matoid")
                    xCmd.CommandText = sSql : discdtl = xCmd.ExecuteScalar

                    sSql = "SELECT SUM(trnbelidtlqty) FROM QL_podtl WHERE trnbelimstoid = " & tblTemp.Rows(C1).Item("trnbelimstoid") : xCmd.CommandText = sSql : jumlahPO = xCmd.ExecuteScalar

                    sSql = "SELECT flagvcr from QL_pomst WHERE trnbelipono = '" & trnbeliponosj.Text & "'"
                    xCmd.CommandText = sSql : flagVcr = xCmd.ExecuteScalar
                    If flagVcr <> 0 Then
                        sSql = "SELECT ISNULL(SUM(amtdisc2idr)/" & tblTemp.Rows(C1).Item("trnsjbelidtlqty") & ", 0.00) FROM ql_podtl WHERE trnbelidtloid=" & tblTemp.Rows(C1).Item("trnpodtloid") & ""
                        xCmd.CommandText = sSql : Voucher = xCmd.ExecuteScalar
                        RumusVoucher = Voucher * tblTemp.Rows(C1).Item("trnsjbelidtlqty")
                    End If
                    '((lasthpp*laststok)+(amountin*qtytrans))/(laststok+qtytrans)
                    HargaNet = (dHargaNetto_qty - Voucher) + (ekspedisi / tblTemp.Rows(C1).Item("trnsjbelidtlqty").ToString)
                    StokMin = (tblTemp.Rows(C1).Item("trnsjbelidtlqty") + LastStock)
                    If ToDouble(StokMin) <= 0 Then
                        HppItem = ((last_hpp * LastStock) + (HargaNet * tblTemp.Rows(C1).Item("trnsjbelidtlqty"))) / (tblTemp.Rows(C1).Item("trnsjbelidtlqty"))
                    Else
                        HppItem = ((last_hpp * LastStock) + (HargaNet * tblTemp.Rows(C1).Item("trnsjbelidtlqty"))) / (tblTemp.Rows(C1).Item("trnsjbelidtlqty") + LastStock)
                    End If

                    sSql = "Select max(OID) From QL_HistHPP"
                    xCmd.CommandText = sSql : oidHis = xCmd.ExecuteScalar()
                    sSql = "INSERT INTO QL_HistHPP (cmpcode,oid, transOid, transName, refname, refoid, groupOid, lastHpp, newHpp, totalOldStock, qtyTrans, pricePerItem, periodacctg, updtime, upduser, branch_code) Values " & _
                    " ('" & CompnyCode & "', " & oidHis + 1 & ", " & tblTemp.Rows(C1)("trnsjbelidtloid") & ", 'QL_trnsjbelidtl', '" & refnamepost & "', " & tblTemp.Rows(C1).Item("matoid") & ", 0, " & ToDouble(last_hpp) & ", " & ToDouble(HppItem) & ", " & ToDouble(LastStock) & ", " & ToDouble(tblTemp.Rows(C1).Item("trnsjbelidtlqty")) & ", " & ToDouble(HargaNet) & ", '" & GetDateToPeriodAcctg(CDate(toDate(trnsjreceivedate.Text))) & "', current_timestamp, '" & Session("UserID") & "', '" & tblTemp.Rows(C1).Item("branch_code") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '-----End History HPP Purchase DO'-----
                    'update oid in ql_mstoid where tablename = 'QL_HistHPP'

                    sSql = "update ql_mstitem set HPP= " & HppItem & " Where ql_mstitem.cmpcode='" & CompnyCode & "' and ql_mstitem.itemoid=" & tblTemp.Rows(C1).Item("matoid") & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update QL_mstoid set lastoid=" & oidHis + 1 & " Where tablename = 'QL_HistHPP' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'end set HPP

                    sSql = " INSERT INTO QL_conmtr (cmpcode, conmtroid, formoid, Formname, type, trndate, periodacctg, mtrlocoid, refoid, qtyIn, amount, note, FormAction, upduser, updtime, unitoid, refname, hpp, branch_code, itembarcode, expireddate) VALUES ('" & CompnyCode & "', " & (conmtroid + C1) & ", " & tblTemp.Rows(C1)("trnsjbelidtloid") & ", 'QL_trnsjbelidtl', 'BELI', '" & CDate(toDate(trnsjreceivedate.Text)) & "', '" & GetDateToPeriodAcctg(CDate(toDate(trnsjreceivedate.Text))) & "', " & tblTemp.Rows(C1).Item("matloc") & ", " & tblTemp.Rows(C1).Item("matoid") & ", " & MainQty & ", " & tblTemp.Rows(C1).Item("trnsjbelidtlqty") * HargaNet & ", '" & Tchar(tblTemp.Rows(C1).Item("trnsjbelidtlnote")) & "', '" & trnsjbelino.Text & "', '" & Session("UserID") & "', current_timestamp, " & mainUnit & ", '" & refnamepost & "', " & ToDouble(HppItem) & ", '" & tblTemp.Rows(C1).Item("branch_code") & "', '" & tblTemp.Rows(C1).Item("itembarcode1").ToString & "', '1/1/1900')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                '------------------ header LPB done save ------------------
                '-----------------------Update PDO'------------------------
                sSql = "UPDATE ql_trnsjbelimst SET trnsjbelino = '" & trnsjbelino.Text & "', trnsjbelistatus = 'Approved', finalapprovaluser='" & Session("UserID") & "', finalapprovaldatetime=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND trnsjbelioid = '" & Session("trnsjbelimstoid") & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ' Update Approval
                sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '--------------------------------
                'update oid in ql_mstoid where tablename = 'QL_conmtr'
                sSql = "update QL_mstoid set lastoid=" & (tblTemp.Rows.Count - 1 + conmtroid) & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "update QL_mstoid set lastoid=" & (tblTemp.Rows.Count - 1 + crdmtroid) & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '-------------------------------------------------------------------------
                'done insert ke stock
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Update SN @@@@@@@@@@@@@@@@@@@@@@@@@@@@@2
                If Not Session("dtSNPO") Is Nothing Then
                    Dim Postpo As DataTable = Session("dtSNPO")
                    For isn As Integer = 0 To Postpo.Rows.Count - 1
                        sSql = "update QL_mstitemDtl set status_item = 'POST' WHERE sn = '" & Postpo.Rows(isn).Item("id_sn") & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "update Ql_mst_sn set status_approval = 'POST' where id_sn = '" & Postpo.Rows(isn).Item("id_sn") & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                End If

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='ql_trnsjbelimst' " & _
                       "and branch_code='" & sCbg.Text & "'" & _
                       "and oid=" & Session("trnsjbelimstoid") & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                'jika tidak final
                'update ke SJ
                sSql = "update ql_trnsjbelimst set updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' and trnsjbelioid=" & Session("trnsjbelimstoid") & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='ql_trnsjbelimst' " & _
                        " and branch_code='" & sCbg.Text & "'" & _
                        "and oid=" & Session("trnsjbelimstoid") & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                          "approvaldate=CURRENT_TIMESTAMP, " & _
                          "approvalstatus='Ignore', " & _
                          "event='Ignore', " & _
                          "statusrequest='End' " & _
                          "WHERE cmpcode='" & CompnyCode & "' " & _
                          "and tablename='ql_trnsjbelimst' " & _
                          "and branch_code='" & sCbg.Text & "'" & _
                          "and oid=" & Session("trnsjbelimstoid") & " " & _
                          "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insertkan untuk user approval yg levelnya diatasnya lagi
                If Not Session("TblApproval") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblApproval")

                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) " & _
                        "VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ",'" & "LPB" & Session("trnsjbelimstoid") & "_" & Session("AppOid") + c1 & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_trnsjbelimst','" & Session("trnsjbelimstoid") & "','In Approval','0','" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "', '" & objTable.Rows(c1).Item("branch_code") & "')" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setPDOasActive()
    End Sub

    Protected Sub imbBackPDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setPDOasActive()
    End Sub

    Protected Sub gvPODetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvPR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub bindFS() 

        FillGV(GvFS, "SELECT jj.trnsjjualmstoid  orderoid, jj.trnsjjualno  orderno, jj.note ordernote ,jj.trnsjjualdate  orderdate, c.custname username, ap.approvaloid,ap.requestuser, ap.requestdate  FROM ql_trnsjjualmst jj,QL_APPROVAL ap,ql_mstcust c,QL_trnordermst so WHERE(jj.cmpcode = ap.CMPCODE And jj.cmpcode = so.cmpcode) AND jj.orderno=so.orderno  AND jj.trnsjjualmstoid  = ap.OID AND ap.statusrequest = 'New' AND (ap.EVENT = 'In Approval') AND (jj.cmpcode LIKE '" & CompnyCode & "%') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'QL_trnsjjualmstPJ') AND (jj.trnsjjualstatus  = 'In Approval') AND so.trncustoid = c.custoid ORDER BY ap.REQUESTDATE DESC", "SDOPJ")
    End Sub

    Private Sub setFSasActive()
        bindFS() : OrdernoFS.Text = "" : OrderDateFS.Text = "" : custnameorderFS.Text = "" : ordernoteFS.Text = "" : sjidentifierno.Text = ""
        Session("approvaloid") = Nothing : Session("trnsjjualmstoid") = Nothing : MultiView1.SetActiveView(View18)

        'bindFS() : OrdernoFS.Text = "" : OrderDateFS.Text = "" : custnameorderFS.Text = "" : ordernoteFS.Text = "" ': soidentifierno.Text = ""
        'Session("approvaloid") = Nothing : Session("orderoid") = Nothing : MultiView1.SetActiveView(View18)
    End Sub

    Protected Sub imbRejectIO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    End Sub

    Protected Sub GVioDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(1).Text), 3)
            e.Row.Cells(2).Text = ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(4).Text), 3)
        End If
        'Dim dTotal As Double = 0
        'For i As Int16 = 0 To GVioDetail.Rows.Count - 1
        '    dTotal += ClassFunction.ToDouble(GVioDetail.Rows(i).Cells(3).Text)
        'Next
        'lblTotalIO.Text = "Total Internal Order = " & ClassFunction.ToMaskEdit(dTotal, 3)
    End Sub

    Protected Sub LkbFS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbFS.Click
        'Session("Type") = "FS"
        setFSasActive()
    End Sub

    Protected Sub GVio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imbBackFS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setFSasActive()
    End Sub

    Protected Sub GVfsDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = ToMaskEdit(ToDouble(e.Row.Cells(1).Text), 3)
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
        Dim dTotal As Double = 0
        
        lblTotalFS.Text = "Total Free Stock = " & ToMaskEdit(dTotal, 3)
    End Sub

    Protected Sub lkbSelectFS_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Session("trnsjjualmstoid") = sender.ToolTip : Session("approvaloid") = sender.CommandArgument

        Dim dt As DataTable = cKoneksi.ambiltabel("SELECT jj.trnsjjualno salesdeliveryno,jj.trnsjjualdate salesdeliverydate,c.custname,so.orderno FROM ql_trnsjjualmst jj INNER JOIN QL_trnordermst so ON jj.cmpcode=so.cmpcode AND jj.orderno =so.orderno INNER JOIN ql_mstcust c ON jj.cmpcode=c.cmpcode AND jj.cmpcode='" & CompnyCode & "' AND so.trncustoid=c.custoid WHERE trnsjjualmstoid ='" & Session("trnsjjualmstoid") & "'", "ql_trnsjjualmst")

        OrdernoFS.Text = dt.Rows(0).Item("salesdeliveryno") : OrderDateFS.Text = Format(CDate(dt.Rows(0).Item("salesdeliverydate")), "dd/MM/yyyy")
        custnameorderFS.Text = dt.Rows(0).Item("custname") : ordernoteFS.Text = dt.Rows(0).Item("orderno")


        sSql = "SELECT seq trnsjjualdtlseq,d.trnsjjualdtloid salesdeliverydtloid, d.trnsjjualmstoid salesdeliverymstoid, d.trnorderdtloid orderdtloid, d.qty salesdeliveryqty, d.note itemdeliverynote, p.trnsjjualmstoid salesdeliveryoid, d.refoid itemoid, d.unitoid as itemunitoid, i.itemdesc + ' - ' + i.merk itemlongdesc, g.gendesc AS unit ,ISNULL (( select sum(qty) from ql_trnsjjualdtl where trnsjjualmstoid  in (select trnsjjualmstoid from ql_trnsjjualmst where trnsjjualstatus in ('Approved','INVOICED') and trnorderdtloid = od.trnorderdtloid )),0) as deliveredqty,od.trnorderdtlqty as referedqty,d.qtypak, d.mtrlocoid itemloc, g23.gendesc location, 0 allocorderoid FROM ql_trnsjjualdtl d inner join ql_trnsjjualmst p on d.cmpcode=p.cmpcode and  d.trnsjjualmstoid =p.trnsjjualmstoid inner join ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid  inner join QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'location' inner join QL_mstgen g3 on g23.genother1 = g3.genoid and g3.gengroup = 'warehouse' and g3.genother1 = 'GROSIR' inner join ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' inner join ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid WHERE d.trnsjjualmstoid =p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid  AND  d.trnsjjualmstoid =" & Session("trnsjjualmstoid") & " AND d.cmpcode='" & CompnyCode & "' ORDER BY d.seq,d.trnsjjualdtloid"
        FillGV(GVfsDetail, sSql, "QL_trnsalesdelivdtl")
        MultiView1.SetActiveView(View19)
    End Sub

    Protected Sub GVioDetile_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
        
    End Sub

    Protected Sub lkbSJ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSJ.Click
        setSJasActive()
    End Sub

    Private Sub setSJasActive()
        bindSJ() : lblsjno.Text = "" : lblsjpic.Text = "" : lblsjexpedisi.Text = "" : lblsjsend.Text = "" : lblsjdriver.Text = "" : lblsjsupplier.Text = "" : lblsjloid.Text = ""
        MultiView1.SetActiveView(View20)
    End Sub

    Private Sub bindSJ()
        sSql = "SELECT a.sjloid, b.approvaloid, a.sjlperson, a.sjlno, a.sjlsend, d.username personname, a.sjlnopol, a.sjldriver, c.suppname, b.requestuser, b.requestdate FROM ql_trnsjalan a INNER JOIN ql_approval b ON a.sjloid = b.oid INNER JOIN ql_mstsupp c ON a.sjlsupp = c.suppoid INNER JOIN QL_MSTPROF d ON a.sjlperson = d.USERID  WHERE a.cmpcode = '" & CompnyCode & "' AND b.event = 'In Approval' AND b.statusrequest = 'New' AND a.sjlstatus = 'In Approval' AND b.tablename = 'QL_TRNSJALAN' AND b.approvaluser = '" & Session("UserID") & "' ORDER BY a.sjloid DESC"
        gvSJ.DataSource = cKoneksi.ambiltabel(sSql, "approvalsj")
        gvSJ.DataBind()
    End Sub

    Protected Sub lkbSelectSJ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
            Dim sjloid As Integer = gvSJ.DataKeys(gvr.RowIndex).Values("sjloid")
            lblsjloid.Text = sjloid
            Dim approvaloid As Integer = gvSJ.DataKeys(gvr.RowIndex).Values("approvaloid")
            lblapprovaloid.Text = approvaloid
            Dim personnip As String = gvSJ.DataKeys(gvr.RowIndex).Values("sjlperson")
            lblpersonnip.Text = personnip

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT a.sjloid, a.sjlno, a.sjlsend, 0 personoid, c.USERID personnip, c.username personname, a.sjlnopol, a.sjldriver, b.suppname FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid INNER JOIN QL_MSTPROF c ON a.sjlperson = c.USERID  AND a.cmpcode = '" & CompnyCode & "' AND a.sjloid = " & sjloid & ""

            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    lblsjno.Text = xreader("sjlno").ToString
                    lblsjsend.Text = Format(xreader("sjlsend"), "dd/MM/yyyy")
                    lblsjpic.Text = xreader("personname").ToString
                    lblsjdriver.Text = xreader("sjldriver").ToString
                    lblsjexpedisi.Text = xreader("sjlnopol").ToString
                    lblsjsupplier.Text = xreader("suppname").ToString
                End While

                sSql = "SELECT a.barcode, a.reqitemname, c.gendesc as type, d.gendesc as brand, a.itemqty, a.note FROM ql_trnsjalandtl a INNER JOIN ql_trnsjalan b ON a.sjloid = b.sjloid INNER JOIN ql_mstgen c ON a.reqitemtype = c.genoid INNER JOIN ql_mstgen d ON a.reqitembrand = d.genoid INNER JOIN ql_trnservices e ON a.reqoid = e.mstreqoid AND a.cmpcode = '" & CompnyCode & "' AND a.sjloid = " & sjloid & " ORDER BY a.sjldtloid"
                Dim dtable As DataTable = cKoneksi.ambiltabel(sSql, "sjdetail")
                gvSJDetail.DataSource = dtable
                gvSJDetail.DataBind()

                MultiView1.SetActiveView(View21)
            End If

            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, CompnyName, 2)
        End Try
    End Sub

    Protected Sub ibbacksj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibbacksj.Click
        setSJasActive()
    End Sub

    Protected Sub ibapprovesj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapprovesj.Click
        Dim sjloid As Integer = 0
        Dim approvaloid As Integer = 0
        If Integer.TryParse(lblsjloid.Text, sjloid) Then
            If Integer.TryParse(lblapprovaloid.Text, approvaloid) Then
                If sjloid > 0 Then
                    Dim otrans As SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    otrans = conn.BeginTransaction
                    xCmd.Transaction = otrans
                    Try
                        sSql = "SELECT sjlperson FROM ql_trnsjalan WHERE sjloid = " & sjloid & ""
                        xCmd.CommandText = sSql
                        Dim sjlperson As String = xCmd.ExecuteScalar
                        If sjlperson = lblpersonnip.Text Then
                            sSql = "UPDATE ql_approval SET statusrequest = 'Approved' WHERE approvaloid = " & approvaloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "UPDATE ql_trnsjalan SET sjlstatus = 'Send' WHERE sjloid = " & sjloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "UPDATE ql_trnrequest SET reqstatus = 'Send' WHERE reqoid IN (SELECT reqoid FROM ql_trnsjalandtl WHERE sjloid = " & sjloid & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            otrans.Commit()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Response.Redirect("~/Other/WaitingAction.aspx")
                        Else
                            showMessage("Anda tidak berhak melakukan approval terhadap Surat Jalan ini!", CompnyName, 2)
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                        End If
                    Catch ex As Exception
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        showMessage(ex.ToString, CompnyName, 2)
                    End Try
                Else
                    showMessage("Surat Jalan tidak valid!", CompnyName, 2)
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub ibrejectsj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibrejectsj.Click
        Dim sjloid As Integer = 0
        Dim approvaloid As Integer = 0
        If Integer.TryParse(lblsjloid.Text, sjloid) Then
            If Integer.TryParse(lblapprovaloid.Text, approvaloid) Then
                If sjloid > 0 Then
                    Dim otrans As SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    otrans = conn.BeginTransaction
                    xCmd.Transaction = otrans
                    Try
                        sSql = "SELECT sjlperson FROM ql_trnsjalan WHERE sjloid = " & sjloid & ""
                        xCmd.CommandText = sSql
                        Dim sjlperson As String = xCmd.ExecuteScalar
                        If sjlperson = lblpersonnip.Text Then
                            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE approvaloid = " & approvaloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "UPDATE ql_trnsjalan SET sjlstatus = 'Rejected' WHERE sjloid = " & sjloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            otrans.Commit()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Response.Redirect("~/Other/WaitingAction.aspx")

                        Else
                            showMessage("Anda tidak berhak melakukan approval terhadap Surat Jalan ini!", CompnyName, 2)
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                        End If
                    Catch ex As Exception
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        showMessage(ex.ToString, CompnyName, 2)
                    End Try
                Else
                    showMessage("Surat Jalan tidak valid!", CompnyName, 2)
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub lkbSPK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbSPK.Click
        setSPKasActive()
    End Sub

    Private Sub setSPKasActive()
        bindSPK() : lblspkno.Text = "" : lblspkdate.Text = "" : lblspkplanstart.Text = "" : lblspkplanend.Text = "" : lblspkperson.Text = "" : lblspkapprovaloid.Text = "" : lblspkmainoid.Text = ""
        MultiView1.SetActiveView(View22)
    End Sub

    Private Sub bindSPK()
        sSql = "SELECT a.womstoid spkmainoid, a.personoid spkpic, b.approvaloid, a.womstoid spkno, a.wodate spkopendate, a.wostartproduksi spkplanstart, a.wofinishproduksi spkplanend, a.womstnote spkaddnote, b.requestuser, b.requestdate FROM ql_trnwomst a INNER JOIN ql_approval b ON a.womstoid = b.oid WHERE a.cmpcode = '" & CompnyCode & "' AND b.event = 'In Approval' AND b.statusrequest = 'New' AND a.womststatus = 'In Approval' AND b.tablename = 'QL_TRNWOMST' AND b.approvaluser = '" & Session("UserID") & "' And a.tobranch_code='" & Session("branch_id") & "' ORDER BY a.womstoid DESC"
        gvSPK.DataSource = cKoneksi.ambiltabel(sSql, "approvalspk")
        gvSPK.DataBind()
    End Sub

    Protected Sub lkbSelectSPK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = TryCast(sender, LinkButton)
            Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
            Dim spkmainoid As Integer = gvSPK.DataKeys(gvr.RowIndex).Values("spkmainoid")
            lblspkmainoid.Text = spkmainoid
            Dim spkapprovaloid As Integer = gvSPK.DataKeys(gvr.RowIndex).Values("approvaloid")
            lblspkapprovaloid.Text = spkapprovaloid
            Dim spkpic As String = gvSPK.DataKeys(gvr.RowIndex).Values("spkpic")
            lblspkperson.Text = spkpic

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT womstoid spkmainoid, womstoid spkno, wodate spkopendate, wostartproduksi spkplanstart, wofinishproduksi spkplanend FROM ql_trnwomst WHERE cmpcode = '" & CompnyCode & "' AND womstoid = " & spkmainoid & ""

            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    lblspkno.Text = xreader("spkno").ToString
                    lblspkdate.Text = Format(xreader("spkopendate"), "dd/MM/yyyy")
                    lblspkplanstart.Text = Format(xreader("spkplanstart"), "dd/MM/yyyy")
                    lblspkplanend.Text = Format(xreader("spkplanend"), "dd/MM/yyyy")
                End While

                sSql = "select DISTINCT wod1.womstoid,wod1.itemcode barcode, i.itemdesc reqitemname, ISNULL(i.merk,' - ') brand, wod1.qtyproduksi, '' type, '' custname from ql_trnwodtl1 wod1 inner join ql_mstitem i on i.itemoid=wod1.itemoid where wod1.cmpcode='" & CompnyCode & "' AND womstoid =  " & spkmainoid & " ORDER BY wod1.itemcode"
                Dim dtable As DataTable = cKoneksi.ambiltabel(sSql, "spkdetail")
                gvSPKDetail.DataSource = dtable
                gvSPKDetail.DataBind()

                sSql = "select distinct wod2.womstoid spkdtloid, wod2.wodtl2oid spkprocessoid, g1.gendesc spkprocessname, g2.gendesc todept, wod2.operator1, wod2.operator2, wod2.quality, '' typejob, '' outputname, '' outputype, 0 outputqty1, '' outputunit1, 0 outputqty2, '' outputunit2 from ql_trnwodtl2 wod2 inner join ql_mstgen g1 on wod2.deptoid=g1.genoid AND g1.gengroup='PROCESS' inner join ql_mstgen g2 on wod2.todeptoid=g2.genoid AND g1.gengroup='PROCESS' where wod2.cmpcode='" & CompnyCode & "' AND wod2.womstoid = " & spkmainoid & " order by wod2.womstoid asc "
                Dim dtable2 As DataTable = cKoneksi.ambiltabel(sSql, "spkdetailjob")
                gvprocessitemresult.DataSource = dtable2
                gvprocessitemresult.DataBind()

                sSql = " select distinct wod3.womstoid,wod3.wodtl3oid spkdtloid, wod2.wodtl2desc spkprocessname, i.itemcode, wod3.itemshortdesc matlongdesc, CONVERT(FLOAT,wod3.wodtl3qtybiji) qty1, g.gendesc unit1, 0 qty2, '' unit2, 0 wastestd, wod3.wodtl3refoid spkprocessmatoid, wod3.wodtl2oid from ql_trnwodtl3 wod3 inner join ql_trnwodtl2 wod2 on wod3.wodtl2oid=wod2.wodtl2oid inner join ql_mstitem i on i.itemoid=wod3.itemoid inner join ql_mstgen g ON g.genoid = i.satuan2 and g.gengroup='itemunit' where wod2.cmpcode='" & CompnyCode & "' AND wod3.womstoid = " & spkmainoid & " order by wod3.womstoid asc "
                Dim dtable3 As DataTable = cKoneksi.ambiltabel(sSql, "spkdetailsparepart")
                GVprocessmaterial.DataSource = dtable3
                GVprocessmaterial.DataBind()

                MultiView1.SetActiveView(View23)
            End If

            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, CompnyName, 2)
        End Try
    End Sub

    Protected Sub ibbackspk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibbackspk.Click
        setSPKasActive()
    End Sub

    Protected Sub ibapprovespk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapprovespk.Click
        Dim spkmainoid As Integer = 0
        Dim spkapprovaloid As Integer = 0
        If Integer.TryParse(lblspkmainoid.Text, spkmainoid) Then
            If Integer.TryParse(lblspkapprovaloid.Text, spkapprovaloid) Then
                If spkmainoid > 0 Then
                    Dim otrans As SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    otrans = conn.BeginTransaction
                    xCmd.Transaction = otrans
                    Try
                        sSql = "SELECT personoid FROM ql_trnwomst WHERE womstoid = " & spkmainoid & ""
                        xCmd.CommandText = sSql
                        Dim spkperson As String = xCmd.ExecuteScalar
                        If spkperson = lblspkperson.Text Then
                            sSql = "UPDATE ql_approval SET statusrequest = 'Approved' WHERE approvaloid = " & spkapprovaloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            Dim spkno As String = generatenoSPK()
                            sSql = "UPDATE ql_trnwomst SET wono = '" & spkno & "', womststatus = 'Approved' WHERE womstoid = " & spkmainoid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            'sSql = "UPDATE ql_trnrequest SET reqstatus = 'SPK' WHERE reqoid IN (SELECT iodtloid FROM ql_trnspkresultitem WHERE spkmainoid = " & spkmainoid & ")"
                            'xCmd.CommandText = sSql
                            'xCmd.ExecuteNonQuery()
                            otrans.Commit()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Response.Redirect("~/Other/WaitingAction.aspx")
                        Else
                            showMessage("Anda tidak berhak melakukan approval terhadap SPK ini!", CompnyName, 2)
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                        End If
                    Catch ex As Exception
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        showMessage(ex.ToString, CompnyName, 2)
                    End Try
                Else
                    showMessage("SPK tidak valid!", CompnyName, 2)
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub ibrejectspk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibrejectspk.Click
        Dim spkmainoid As Integer = 0
        Dim spkapprovaloid As Integer = 0
        If Integer.TryParse(lblspkmainoid.Text, spkmainoid) Then
            If Integer.TryParse(lblspkapprovaloid.Text, spkapprovaloid) Then
                If spkmainoid > 0 Then
                    Dim otrans As SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    otrans = conn.BeginTransaction
                    xCmd.Transaction = otrans
                    Try
                        sSql = "SELECT personoid FROM ql_trnwomst WHERE womstoid = " & spkmainoid & ""
                        xCmd.CommandText = sSql
                        Dim spkperson As String = xCmd.ExecuteScalar
                        If spkperson = lblspkperson.Text Then
                            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE approvaloid = " & spkapprovaloid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            sSql = "UPDATE ql_trnwomst SET womststatus = 'Rejected' WHERE womstoid = " & spkmainoid & ""
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            otrans.Commit()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Response.Redirect("~/Other/WaitingAction.aspx")

                        Else
                            showMessage("Anda tidak berhak melakukan rejection terhadap Surat Jalan ini!", CompnyName, 2)
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                        End If
                    Catch ex As Exception
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        showMessage(ex.ToString, CompnyName, 2)
                    End Try
                Else
                    showMessage("SPK tidak valid!", CompnyName, 2)
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End If
            End If
        End If


    End Sub

    Private Function generatenoSPK() As String
        Dim nospk As String = ""

        Dim sPrefik As String = "/SPK/" & getRomawi(CDate(toDate(lblspkdate.Text)).Month) & Format(CDate(toDate(lblspkdate.Text)), "/yyyy")
        sSql = "SELECT isnull(max(abs(replace(spkno, '" & sPrefik & "',''))),0)  from QL_trnspkmainprod WHERE cmpcode='" & CompnyCode & "' AND  spkno like '%" & sPrefik & "'"
        Dim iNextNo As Integer = cKoneksi.ambilscalar(sSql) + 1
        nospk = GenNumberString("", "", iNextNo, 3) & sPrefik

        Return nospk
    End Function

    Protected Sub gvSPK_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSPK.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Format(CDate(toDate(e.Row.Cells(1).Text)), "dd/MM/yyyy")
            e.Row.Cells(2).Text = Format(CDate((e.Row.Cells(2).Text)), "dd/MM/yyyy HH:mm")
            e.Row.Cells(3).Text = Format(CDate((e.Row.Cells(3).Text)), "dd/MM/yyyy HH:mm")
        End If
    End Sub

    Protected Sub lkbPReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPReturn.Click
        Session("Type") = "PR"
        setPReturnasActive()
    End Sub

    Private Sub setPReturnasActive()
        bindPReturn() : MultiView1.SetActiveView(View24)
    End Sub

    Private Sub bindPReturn()
        Dim QlPR As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbelireturmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodePr As String = ""
        Dim PrCode() As String = QlPR.Split(",")
        For C1 As Integer = 0 To PrCode.Length - 1
            If PrCode(C1) <> "" Then
                sCodePr &= "'" & PrCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select a.trnbeliReturmstoid, b.approvaloid, a.trnbelireturno, a.trnbelidate, a.trnTrfToReturNo, a.trnbelinote, b.requestuser, b.requestdate from QL_trnbeliReturmst a inner join ql_approval b on a.trnbeliReturmstoid = b.oid where a.cmpcode = '" & CompnyCode & "' and b.event = 'in approval' and b.statusrequest = 'new' and a.trnbelistatus = 'in approval' and b.tablename = 'ql_trnbelireturmst' and b.approvaluser = '" & Session("userid") & "' And b.branch_code IN (" & Left(sCodePr, sCodePr.Length - 1) & ") order by a.trnbeliReturmstoid desc"
        FillGV(GVPreturn, sSql, "GVPreturn")
    End Sub

    Public Function GetHargaBeli() As String
        Return ToMaskEdit(ToDouble(Eval("hargabeli")), 4)
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = GVPreturnDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function UpdateDetailData() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblRetdtl") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRetdtl")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True

                For C1 As Integer = 0 To GVPreturnDtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVPreturnDtl.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    Dim hargabeli As Double = ToDouble(GetTextBoxValue(C1, 2))
                    dtView(0)("hargabeli") = ToDouble(GetTextBoxValue(C1, 2))
                    dtView.RowFilter = ""
                Next

                dtTbl = dtView.ToTable
                Session("TblRetdtl") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Protected Sub lkbSelectPReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        FillDDL(paytype, "select genoid,gencode from ql_mstgen where gengroup='PAYTYPE'")
        Session("trnbeliReturmstoid") = sender.ToolTip : MultiView1.SetActiveView(View25)
        Session("approvaloid") = sender.CommandArgument

        Dim dt As DataTable = cKoneksi.ambiltabel("SELECT trnbelireturno, trnbelidate, trnTrfToReturNo, trnbelinote, amtbelinetto, trnbelino, amtdiscdtl+amtdischdr amtdiscdtl,amtvoucher,amtekspedisi,amtdischdr, trnamttax, amtretur_hutang, trnpaytype, trnsuppoid,branch_code,isnull((Select DISTINCT sj.branch_code From ql_trnsjbelimst sj Where sj.trnsjbelino=rm.trnsjbelino), rm.branch_code ) cbgret,rm.typeret,(Select suppname from QL_mstsupp sup Where sup.suppoid=rm.trnsuppoid) SuppName FROM QL_trnbeliReturmst rm WHERE trnbeliReturmstoid = " & Session("trnbeliReturmstoid") & "", "ql_trnbelireturmst")

        preturnno.Text = dt.Rows(0).Item("trnbelireturno")
        preturndate.Text = Format(dt.Rows(0).Item("trnbelidate"), "dd/MM/yyyy")
        preturntrno.Text = dt.Rows(0).Item("trnTrfToReturNo")
        preturnnote.Text = dt.Rows(0).Item("trnbelinote")
        preturnnetto.Text = ToMaskEdit(dt.Rows(0).Item("amtbelinetto"), 3)
        preturninvoice.Text = dt.Rows(0).Item("trnbelino")
        suppoid.Text = Integer.Parse(dt.Rows(0).Item("trnsuppoid"))
        CabangNya.Text = dt.Rows(0).Item("cbgret")
        amtpotonganpembelian.Text = dt.Rows(0).Item("amtdischdr")
        ppnamount.Text = dt.Rows(0).Item("trnamttax")
        returamount.Text = dt.Rows(0).Item("amtbelinetto")
        amtvoucher.Text = dt.Rows(0).Item("amtvoucher")
        amtexpedisi.Text = dt.Rows(0).Item("amtekspedisi")
        returhutangamount.Text = dt.Rows(0).Item("amtretur_hutang")
        typeret.Text = dt.Rows(0).Item("typeret")
        cBngPusat.Text = Session("branch_id") 'dt.Rows(0).Item("branch_code")
        NamaSupp.Text = dt.Rows(0).Item("SuppName")
        hutangamount.Text = ToMaskEdit(ToDouble(GetStrData("select (amtbelinetto - accumpayment - abs(amtretur+" & ToDouble(preturnnetto.Text) & ")) hutang from ql_trnbelimst where cmpcode='" & CompnyCode & "' and trnbelino='" & preturninvoice.Text & "'")), 3)
        If ToDouble(hutangamount.Text) > ToDouble(returamount.Text) Then
            hutangamount.Text = ToMaskEdit(ToDouble(returamount.Text) + (ToDouble(returhutangamount.Text)), 3)
        ElseIf ToDouble(hutangamount.Text) = ToDouble(returamount.Text) Then
            hutangamount.Text = ToMaskEdit(ToDouble(returamount.Text), 3)
        End If
        If ToDouble(hutangamount.Text) < 0 Then
            returhutangamount.Text = (ToDouble(hutangamount.Text) * -1)
            If returhutangamount.Text < 0 Then
                returhutangamount.Text = 0
            End If
            hutangamount.Text = 0
        End If
        returamount.Text = ToDouble(returamount.Text) - ToDouble(ppnamount.Text)
        If ToDouble(returhutangamount.Text) = 0 Then
            hutangamount.Text = ToMaskEdit((ToDouble(returamount.Text) + ToDouble(ppnamount.Text) + (ToDouble(cashamount.Text))), 3)
        ElseIf ToDouble(returhutangamount.Text) > 0 Then
            hutangamount.Text = ToMaskEdit((ToDouble(returamount.Text) - ToDouble(returhutangamount.Text) + ToDouble(ppnamount.Text) + (ToDouble(cashamount.Text))), 3)
        End If
        returamount.Text = ToDouble(returamount.Text) + ToDouble(amtpotonganpembelian.Text)

        paytype.SelectedValue = dt.Rows(0).Item("trnpaytype")
        sSql = "Select distinct a.itemoid,isnull(sjd.mtrlocoid,a.itemloc) mtrlocoid,a.trnbelireturdtlseq as seq, b.itemdesc + ' - ' + b.merk itemdesc, a.trnbelireturdtlqty as qty,a.trnbelireturdtlqty, c.gendesc as unit, a.trnbelireturdtlnote as note,a.trnbelireturdtlprice hargabeli,a.trnbelireturdtlqtydisc*a.trnbelireturdtlqty DiscAmt,a.amtdisc,a.trnbelireturdtlqtydisc,a.amtbelinetto,a.trnbeliReturdtloid,a.trnbelireturdtlunitoid,b.stockflag,b.lastPricebuyUnit1,rm.trnsjbelino,a.trnbeliReturdtloid from QL_trnbeliReturdtl a inner join QL_mstItem b on a.cmpcode = b.cmpcode and a.itemoid = b.itemoid inner join QL_mstgen c on b.cmpcode = a.cmpcode and a.trnbelireturdtlunitoid = c.genoid inner join QL_trnbeliReturmst rm on rm.trnbeliReturmstoid = a.trnbeliReturmstoid left join ql_trnsjbelimst sjm on sjm.trnsjbelino = rm.trnsjbelino left join ql_trnsjbelidtl sjd on sjd.trnsjbelioid = sjm.trnsjbelioid Where a.trnbeliReturmstoid = " & Session("trnbeliReturmstoid") & " and a.cmpcode = '" & CompnyCode & "' Order By a.trnbelireturdtlseq"

        Session("TblRetdtl") = cKon.ambiltabel(sSql, "retdtl")
        GVPreturnDtl.DataSource = Session("TblRetdtl")
        GVPreturnDtl.DataBind()
        GVPreturnDtl.Visible = True

        sSql = "select distinct a.itemoid,isnull(sjd.mtrlocoid,a.itemloc ) mtrlocoid,a.trnbelireturdtlseq as seq, b.itemdesc + ' - ' + b.merk itemdesc, a.trnbelireturdtlqty as qty,a.trnbelireturdtlqty, c.gendesc as unit, a.trnbelireturdtlnote as note,a.trnbelireturdtlprice hargabeli,a.amtbelinetto,a.trnbeliReturdtloid,a.trnbelireturdtlunitoid,b.stockflag,b.lastPricebuyUnit1,a.trnbelireturdtlqtydisc from QL_trnbeliReturdtl a inner join QL_mstItem b on a.cmpcode = b.cmpcode and a.itemoid = b.itemoid inner join QL_mstgen c on b.cmpcode = a.cmpcode and a.trnbelireturdtlunitoid = c.genoid inner join QL_trnbeliReturmst rm on rm.trnbeliReturmstoid = a.trnbeliReturmstoid left join ql_trnsjbelimst sjm on sjm.trnsjbelino = rm.trnsjbelino left join ql_trnsjbelidtl sjd on sjd.trnsjbelioid = sjm.trnsjbelioid where a.trnbeliReturmstoid = " & Session("trnbeliReturmstoid") & " and a.cmpcode = '" & CompnyCode & "'"
        Dim dtl As DataTable = cKoneksi.ambiltabel(sSql, "ql_prdtl")
        Session("dtlitem") = dtl
    End Sub

    Protected Sub ibapprovepreturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapprovepreturn.Click
        UpdateDetailData()
        Dim dMsg As String = ""
        Dim period As String = GetDateToPeriodAcctg(GetServerTime()).Trim
        Dim dta As DataTable = Session("TblRetdtl")

        For c1 As Int16 = 0 To dta.Rows.Count - 1
            If dta.Rows(c1).Item("hargabeli").ToString.Trim = "" Then
                dta.Rows(c1).Item("hargabeli") = 0.0
            End If
            If ToDouble(dta.Rows(c1).Item("hargabeli")) <= 0.0 Then
                dMsg &= "Maaf, harga beli tidak boleh Nol Atau kurang dari Nol..!!!<BR />"
            End If
        Next

        If IsDBNull(Session("trnbeliReturmstoid")) Then
            dMsg &= "Maaf, Pilih purchase return yang akan direject terlebih dahulu..!!<BR />"
        Else
            If Session("trnbeliReturmstoid") = "" Then
                dMsg &= "Maaf, Pilih purchase return yang akan direject terlebih dahulu..!!!<BR />"
            End If
        End If

        If preturnno.Text = "" Then
            dMsg &= "Pilih purchase return yang akan direject terlebih dahulu..!!<BR />"
        End If

        sSql = "Select genother1 From ql_mstgen Where gengroup = 'CABANG' And gencode='" & cBngPusat.Text & "'"
        Dim FrombarchPembuat As String = cKon.ambilscalar(sSql)

        sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn SN From QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturbelimstoid = '" & Tchar(preturnno.Text) & "'"
        Dim objTableSN As DataTable = cKon.ambiltabel(sSql, "TbSN")

        Dim DpOid As Integer = GenerateID("QL_TRNDPAP", CompnyCode)
        Dim glmstoid As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim gldtloid As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim PayApOid As Integer = GenerateID("QL_trnpayap", CompnyCode)
        Dim conapoid As Integer = GenerateID("QL_conap", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "select trnbelistatus from QL_trnbeliReturmst Where trnbeliReturmstoid = " & Integer.Parse(Session("trnbeliReturmstoid")) & ""
            xCmd.CommandText = sSql : Dim stat As String = xCmd.ExecuteScalar

            If stat.ToLower = "approved" Then
                dMsg &= "Purchase return tidak bisa diapprove karena telah berstatus 'Approved'..!!<BR />"
            ElseIf stat Is Nothing Or stat = "" Then
                dMsg &= "Maaf, Purchase return tidak ditemukan..!!<BR />"
            End If

            If dMsg <> "" Then
                showMessage(dMsg, CompnyName & " - Warning", 2)
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Exit Sub
            End If

            sSql = "SELECT curroid FROM QL_trnbeliReturmst WHERE trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "'"
            xCmd.CommandText = sSql
            Dim ratecurr As Integer = xCmd.ExecuteScalar

            sSql = "select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & ratecurr & "' order by rate2date desc"
            xCmd.CommandText = sSql : Dim rateidr As Decimal = xCmd.ExecuteScalar

            sSql = "select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & ratecurr & "' order by rate2date desc"
            xCmd.CommandText = sSql
            Dim rateusd As Decimal = xCmd.ExecuteScalar

            sSql = "UPDATE QL_trnbeliReturdtl SET trnbelireturdtlflag = 'Approved', trnbelireturdtlstatus = 'Approved' WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_trnbeliReturmst' " & _
                        "and oid=" & Session("trnbeliReturmstoid") & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & cBngPusat.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "UPDATE QL_approval SET " & _
                   "approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore', " & _
                   "event='Ignore', " & _
                   "statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='QL_trnbeliReturmst' " & _
                   "and oid=" & Session("trnbeliReturmstoid") & " " & _
                   "and approvaluser<>'" & Session("UserId") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Select trnbelidate from QL_trnbeliReturmst WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = " & Session("trnbeliReturmstoid") & ""
            xCmd.CommandText = sSql
            Dim returdate As Date = xCmd.ExecuteScalar

            '------------ Generated Nomer -----------
            '----------------------------------------
            sSql = "Select genother1 From ql_mstgen Where gengroup = 'CABANG' And gencode='" & CabangNya.Text & "'"
            Dim CabangRet As String = cKon.ambilscalar(sSql)

            Dim iCurID As Integer = 0 : Dim DpNo As String = ""
            Dim retno As String = "PR/" & CabangRet & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnbelireturno,4) AS INT)),0) FROM QL_trnbeliReturmst WHERE trnbelireturno LIKE '" & retno & "%'"
            xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
            retno = GenNumberString(retno, "", iCurID, 4)

            'If typeret.Text.ToUpper = "JADI DP" Then
            sSql = "Select genother1 from ql_mstgen Where gengroup = 'cabang' and gencode='" & CabangNya.Text & "'"
            xCmd.CommandText = sSql : Dim cb As String = xCmd.ExecuteScalar

            DpNo = "DP/" & cb & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndpapno,4) AS INT)),0) FROM QL_trndpap WHERE trndpapno LIKE '" & DpNo & "%'"
            xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
            DpNo = GenNumberString(DpNo, "", iCurID, 4)
            'End If

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar

            sSql = "SELECT p.trnsjbelino From ql_trnsjbelimst p INNER JOIN QL_trnbeliReturmst r ON p.trnsjbelino = r.trnsjbelino WHERE r.trnbeliReturmstoid = " & Session("trnbeliReturmstoid") & ""
            xCmd.CommandText = sSql : Dim sjno As String = xCmd.ExecuteScalar

            Dim dtab As DataTable = Session("TblRetdtl")
            Dim glseq As Integer = 0 : Dim AmtNettdtl As Double
            For i As Integer = 0 To dtab.Rows.Count - 1
                '------------Cek Hpp Terakhir--------------
                Dim lasthpp As Double = 0 : Dim lastQty As Double
                sSql = "select hpp from ql_mstitem Where itemoid=" & dtab.Rows(i).Item("itemoid") & ""
                xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                '-----Cek Stok akhir gudang------
                sSql = "Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) From QL_conmtr Where branch_code='" & CabangNya.Text & "' AND refoid=" & dtab.Rows(i).Item("itemoid") & " AND mtrlocoid=" & dtab.Rows(i).Item("mtrlocoid") & " ANd periodacctg='" & period & "'"
                xCmd.CommandText = sSql : lastQty = xCmd.ExecuteScalar
                If lastQty <= 0 Then
                    objTrans.Rollback() : conn.Close()
                    showMessage("Maaf, Proses 'Approve' tidak bisa dilakukan, Jumlah Stok akhir 0!", CompnyName & " - Warning", 2)
                    Exit Sub
                End If
                '===== Rumus mencari amount netto PI yang diretur ====
                AmtNettdtl += (ToDouble(dtab.Rows(i).Item("hargabeli") * dtab.Rows(i).Item("qty"))) - (ToDouble(dtab.Rows(i).Item("trnbelireturdtlqtydisc") * dtab.Rows(i).Item("qty")))

                sSql = "UPDATE QL_trnbeliReturdtl SET trnbelireturdtlflag = 'Approved', trnbelireturdtlstatus = 'Approved', amtbelinetto=" & (ToDouble(dtab.Rows(i).Item("hargabeli") * dtab.Rows(i).Item("qty"))) - (ToDouble(dtab.Rows(i).Item("trnbelireturdtlqtydisc") * dtab.Rows(i).Item("qty"))) & ",trnbelireturdtlprice=" & ToDouble(dtab.Rows(i).Item("hargabeli")) & " WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "' AND trnbeliReturdtloid=" & dtab.Rows(i).Item("trnbeliReturdtloid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                conmtroid = conmtroid + 1
                sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES " & _
                "('" & CompnyCode & "','" & CabangNya.Text & "', " & conmtroid & ", 'PR', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & period & "', '" & retno & "', " & dtab.Rows(i).Item("trnbeliReturdtloid") & ", 'QL_trnbeliReturdtl', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("trnbelireturdtlunitoid") & "," & dtab.Rows(i).Item("mtrlocoid") & ", 0, " & dtab.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(i).Item("hargabeli") * dtab.Rows(i).Item("qty")) & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & ToDouble(dtab.Rows(i).Item("hargabeli")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_trnbelidtl set trnbelidtlqty_retur = " & dtab.Rows(i).Item("trnbelireturdtlqty") & ", trnbelidtlqty_retur_unit3 = " & dtab.Rows(i).Item("trnbelireturdtlqty") & " Where trnsjbelino = '" & sjno & "' and itemoid = " & dtab.Rows(i).Item("itemoid") & ""
                'and itemloc =  " & dtab.Rows(i).Item("itemloc") & " and branch_code = '" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            '==== UPDATE TABEL RETUR BELI MST ====
            sSql = "UPDATE QL_trnbeliReturmst SET trnbelireturno = '" & retno & "',amtbeli=" & AmtNettdtl & ",amtbelinett=" & AmtNettdtl & ",amtbelinetto=" & AmtNettdtl & ",trnbelistatus = 'Approved', finalapprovaluser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP,trnbelidate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim SisaHutang, Amtret, AmtReturNya As Double
            Dim OidCoaDP, OidCoaPesediaan, ApAccount As Integer
            Dim Var_AP As String = ""
            '===== Inser Auto Jurnal ======
            sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type,branch_code) VALUES" & _
            " ('" & CompnyCode & "'," & glmstoid & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & period & "','Purchase Return No.: " & retno & "','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'PR','" & CabangNya.Text & "')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & glmstoid & " Where tablename = 'QL_TRNGLMST' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '----- Input DP jika invoice sudah lunas -------
            '===============================================
            Dim iMatAcctg As String = "" : Dim varmat As String = ""
            If typeret.Text.ToUpper = "JADI DP" Then
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DPAP' and cmpcode='" & CompnyCode & "' and interfaceres1='" & cBngPusat.Text & "'"
                xCmd.CommandText = sSql : Dim Var_DP As String = xCmd.ExecuteScalar

                If Var_DP <> "" And Not Var_DP Is Nothing Then
                    sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & Var_DP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                    xCmd.CommandText = sSql : OidCoaDP = xCmd.ExecuteScalar
                    If OidCoaDP <= 0 Then
                        dMsg &= "Maaf, VAR_DPAP belum disetting silahkan hubungi admin..!!<br />"
                    End If
                Else
                    dMsg &= "Maaf, VAR_DPAP belum disetting silahkan hubungi admin..!!<br />"
                End If

                If dMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(dMsg, CompnyName & " - Warning", 2)
                    Exit Sub
                End If

                sSql = "INSERT INTO QL_trndpap (cmpcode,trnDPAPoid,branch_code,trnDPAPno,trnDPAPdate,suppoid,cashbankoid,trnDPAPacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trnDPAPamt,trnDPAPamtidr,trnDPAPamtusd, taxtype, taxoid, taxpct, taxamt,trnDPAPnote, trnDPAPflag, trnDPAPacumamt,trnDPAPacumamtidr,trnDPAPacumamtusd, trnDPAPstatus, createuser,createtime,upduser,updtime) VALUES" & _
                " ('" & CompnyCode & "', " & DpOid & ",'" & CabangNya.Text & "','" & DpNo & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & Integer.Parse(suppoid.Text) & ",'" & 0 & "', " & OidCoaDP & ",'RETUR', " & OidCoaPesediaan & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(retno) & "', " & 1 & ",0," & ToDouble(AmtNettdtl) & "," & ToDouble(AmtNettdtl) & "," & ToDouble(AmtNettdtl) & ",'NONTAX','',0,0,'" & Tchar(retno) & "','',0,0,0,'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & DpOid & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnbelimst SET amtreturdp = amtreturdp + " & ToDouble(AmtNettdtl) & ",amtreturdpidr = amtreturdpidr + " & ToDouble(AmtNettdtl) & " WHERE cmpcode = '" & CompnyCode & "' AND trnbelino = (Select trnbelino from QL_trnbeliReturmst Where trnbeliReturmstoid = " & Integer.Parse(Session("trnbeliReturmstoid")) & ") "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update ql_mstoid set lastoid = " & conmtroid & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                glseq = glseq + 1
                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime,branch_code)" & _
                " VALUES ('" & CompnyCode & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & OidCoaDP & ",'D'," & ToDouble(AmtNettdtl) & "," & ToDouble(AmtNettdtl) & "," & ToDouble(AmtNettdtl) & ",'" & retno & "','Akun Hutang DP','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '--------- Akhir Kondisi Jika retur sudah lunas dan jadi DP --------- 
                '====================================================================
            Else

                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_AP' and cmpcode='" & CompnyCode & "' and interfaceres1='" & cBngPusat.Text & "'"
                xCmd.CommandText = sSql : Var_AP = xCmd.ExecuteScalar

                sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & Var_AP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                xCmd.CommandText = sSql : ApAccount = xCmd.ExecuteScalar

                If ApAccount <= 0 Then
                    dMsg &= "Maaf, VAR_AP belum disetting silahkan hubungi admin..!!<br />"
                End If

                For V1 As Integer = 0 To dtab.Rows.Count - 1
                    Amtret += (ToDouble(dtab.Rows(V1).Item("hargabeli") * dtab.Rows(V1).Item("qty"))) - (ToDouble(dtab.Rows(V1).Item("trnbelireturdtlqtydisc") * dtab.Rows(V1).Item("qty")))
                Next

                sSql = "Select amtbelinetto-(accumpayment+amtretur+" & Amtret & ") From QL_trnbelimst Where trnbelino='" & preturninvoice.Text & "'"
                xCmd.CommandText = sSql : SisaHutang = xCmd.ExecuteScalar
                AmtReturNya = SisaHutang + Amtret

                '--------- Kondisi jika sebagian jadi DP -------------
                '-----------------------------------------------------
                If ToDouble(SisaHutang) < 0.0 Then
                    '---- Update Trnbeli ----
                    sSql = "UPDATE QL_trnbelimst SET amtreturdp = amtreturdp + (" & ToDouble(SisaHutang) * -1 & "),amtreturdpidr = amtreturdpidr + (" & ToDouble(SisaHutang) * -1 & "),amtreturdpusd = amtreturdpusd + (" & ToDouble(SisaHutang) * -1 & "),amtretur=amtretur+" & ToDouble(Amtret) & ",amtreturidr=amtreturidr+" & ToDouble(Amtret) & " WHERE cmpcode = '" & CompnyCode & "' AND trnbelino = (select trnbelino from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("trnbeliReturmstoid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DPAP' and cmpcode='" & CompnyCode & "' and interfaceres1='" & cBngPusat.Text & "'"
                    xCmd.CommandText = sSql : Dim Var_DP As String = xCmd.ExecuteScalar
                    If Var_DP <> "" And Not Var_DP Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & Var_DP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : OidCoaDP = xCmd.ExecuteScalar
                        If OidCoaDP <= 0 Then
                            dMsg &= "Maaf, VAR_DPAP belum disetting silahkan hubungi admin..!!<br />"
                        End If
                    Else
                        dMsg &= "Maaf, VAR_DPAP belum disetting silahkan hubungi admin..!!<br />"
                    End If

                    If dMsg <> "" Then
                        objTrans.Rollback() : conn.Close()
                        showMessage(dMsg, CompnyName & " - Warning", 2)
                        Exit Sub
                    End If

                    sSql = "INSERT INTO QL_trndpap (cmpcode,trnDPAPoid,branch_code,trnDPAPno,trnDPAPdate,suppoid,cashbankoid,trnDPAPacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,trnDPAPamt,trnDPAPamtidr,trnDPAPamtusd, taxtype, taxoid, taxpct, taxamt,trnDPAPnote, trnDPAPflag, trnDPAPacumamt,trnDPAPacumamtidr,trnDPAPacumamtusd, trnDPAPstatus, createuser,createtime,upduser,updtime) VALUES" & _
               " ('" & CompnyCode & "', " & DpOid & ",'" & CabangNya.Text & "','" & DpNo & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & Integer.Parse(suppoid.Text) & ",'" & 0 & "', " & OidCoaDP & ",'RETUR', " & OidCoaPesediaan & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(retno) & "', " & 1 & ",0," & ToDouble(SisaHutang) * -1 & "," & ToDouble(SisaHutang) * -1 & "," & ToDouble(SisaHutang) * -1 & ",'NONTAX','',0,0,'" & Tchar(retno) & "','',0,0,0,'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & DpOid & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'conapoid = conapoid + 1
                    sSql = "INSERT into QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnapnote, trnapres1, upduser, updtime, returoid,branch_code) VALUES " & _
                    "('" & CompnyCode & "'," & conapoid & ",'QL_trnpayap',(select trnbelimstoid from QL_trnbelimst where trnbelino='" & preturninvoice.Text & "')," & PayApOid & "," & Integer.Parse(suppoid.Text) & "," & ApAccount & ",'POST','AP','" & GetServerTime() & "','" & GetDateToPeriodAcctg(GetServerTime()) & "'," & 0 & ",'" & CDate(toDate(preturndate.Text)) & "','" & retno & "',0,'" & GetServerTime() & "',0,0,0," & ToDouble(AmtReturNya) & "," & ToDouble(AmtReturNya) & "," & ToDouble(ToDouble(AmtReturNya)) & ",'Purchase retur " & retno & "','','" & Session("UserID") & "',current_timestamp," & Session("trnbeliReturmstoid") & ",'" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "insert into QL_trnpayap (cmpcode, paymentoid, cashbankoid, suppoid, payreftype, payrefoid, payflag, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt,payamtidr,payamtusd, payrefflag, paystatus, upduser, updtime, trndpapoid, DPAmt, returoid, branch_code) VALUES" & _
                    " ('" & CompnyCode & "'," & PayApOid & ",0," & Integer.Parse(suppoid.Text) & ",'QL_trnbelimst',(select trnbelimstoid from QL_trnbelimst where trnbelino='" & preturninvoice.Text & "'),''," & ApAccount & ",'" & retno & "',0,'" & GetServerTime() & "','Hutang(D) - Persediaan/retur(C)'," & ToDouble(AmtReturNya) & "," & ToDouble(AmtReturNya) & "," & ToDouble(AmtReturNya) & ",'','POST','" & Session("UserID") & "',current_timestamp,0,0," & Session("trnbeliReturmstoid") & ", '" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '---- Insert Auto Jurnal DP ----
                    '-------------------------------
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime,branch_code)" & _
                    " VALUES ('" & CompnyCode & "'," & gldtloid & ",1," & glmstoid & "," & OidCoaDP & ",'D'," & ToDouble(SisaHutang) * -1 & "," & ToDouble(SisaHutang) * -1 & "," & ToDouble(SisaHutang) * -1 & ",'" & retno & "','Akun Hutang DP','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '---- Insert Auto Jurnal AP ----
                    '------------------------------- 
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime,branch_code)" & _
                    " VALUES ('" & CompnyCode & "'," & gldtloid & ",2," & glmstoid & "," & ApAccount & ",'D'," & ToDouble(AmtReturNya) & "," & ToDouble(AmtReturNya) & "," & ToDouble(AmtReturNya) & ",'" & retno & "','Akun Hutang','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Else '---- Akhir Proses Jika sisa retur jadi dp ----- 
                    sSql = "UPDATE QL_trnbelireturMst SET amtretur_hutang=" & ToDouble(Amtret) & ",amtbelinetto=" & ToDouble(Amtret) & " WHERE trnbelireturmstoid=" & Integer.Parse(Session("trnbeliReturmstoid")) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnbelimst SET Amtretur=Amtretur +" & ToDouble(Amtret) & ",amtreturidr=amtreturidr+" & ToDouble(Amtret) & " WHERE cmpcode = '" & CompnyCode & "' AND trnbelino = (select trnbelino from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("trnbeliReturmstoid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'conapoid = conapoid + 1
                    sSql = "INSERT into QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnapnote, trnapres1, upduser, updtime, returoid,branch_code) VALUES " & _
                    "('" & CompnyCode & "'," & conapoid & ",'QL_trnpayap',(select trnbelimstoid from QL_trnbelimst where trnbelino='" & preturninvoice.Text & "')," & PayApOid & "," & Integer.Parse(suppoid.Text) & "," & ApAccount & ",'POST','AP','" & GetServerTime() & "','" & GetDateToPeriodAcctg(GetServerTime()) & "',0,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & retno & "',0,'" & GetServerTime() & "',0,0,0," & ToDouble(Amtret) & "," & ToDouble(Amtret) & "," & ToDouble(ToDouble(Amtret)) & ",'Purchase retur " & retno & "','','" & Session("UserID") & "',current_timestamp," & Session("trnbeliReturmstoid") & ",'" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "insert into QL_trnpayap (cmpcode, paymentoid, cashbankoid, suppoid, payreftype, payrefoid, payflag, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt,payamtidr,payamtusd, payrefflag, paystatus, upduser, updtime, trndpapoid, DPAmt, returoid, branch_code) VALUES" & _
                    " ('" & CompnyCode & "'," & PayApOid & ",0," & Integer.Parse(suppoid.Text) & ",'QL_trnbelimst',(select trnbelimstoid from QL_trnbelimst where trnbelino='" & preturninvoice.Text & "'),''," & ApAccount & ",'" & retno & "',0,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'Hutang(D) - Persediaan/retur(C)'," & ToDouble(Amtret) & "," & ToDouble(Amtret) & "," & ToDouble(Amtret) & ",'','POST','" & Session("UserID") & "',current_timestamp,0,0," & Session("trnbeliReturmstoid") & ", '" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '---- Insert Auto Jurnal AP ----
                    '-------------------------------  
                    sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime,branch_code)" & _
                    " VALUES ('" & CompnyCode & "'," & gldtloid & "," & glseq + 1 & "," & glmstoid & "," & ApAccount & ",'D'," & ToDouble(Amtret) & "," & ToDouble(Amtret) & "," & ToDouble(Amtret) & ",'" & retno & "','Akun Hutang','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            sSql = "Update ql_mstoid set lastoid = " & conapoid & " where tablename = 'QL_conap' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Update ql_mstoid set lastoid = " & PayApOid & " Where tablename = 'QL_trnpayap' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Update ql_mstoid set lastoid = " & conmtroid & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Update ql_mstoid set lastoid = " & gldtloid & " Where tablename = 'QL_TRNGLDTL' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '---- Insert Jurnal Persediaan ----
            '==================================
            gldtloid = gldtloid + 1 : glseq = glseq + 1
            For Vi As Integer = 0 To dtab.Rows.Count - 1
                sSql = "Select stockflag From ql_mstitem Where itemoid= " & Integer.Parse(dtab.Rows(Vi)("itemoid")) & ""
                xCmd.CommandText = sSql : Dim sTockFlag As String = xCmd.ExecuteScalar

                sSql = "Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & Integer.Parse(dtab.Rows(Vi).Item("mtrlocoid")) & " AND genother4='" & sTockFlag & "'"
                xCmd.CommandText = sSql : Dim nBranch As String = xCmd.ExecuteScalar

                If sTockFlag = "I" Then
                    varmat = "VAR_INVENTORY"
                    iMatAcctg = GetVarInterface(varmat, CabangNya.Text)
                ElseIf sTockFlag = "ASSET" Then
                    varmat = "VAR_GUDANG_ASSET"
                    iMatAcctg = GetVarInterface(varmat, CabangNya.Text)
                Else
                    varmat = "VAR_GUDANG"
                    iMatAcctg = GetVarInterface(varmat, CabangNya.Text)
                End If

                If iMatAcctg = "?" Then
                    dMsg &= "Maaf, Akun' " & varmat & "' belum disetting silahkan hubungi admin accounting..!!<br />"
                End If

                If iMatAcctg Is Nothing Or iMatAcctg = "" Then
                    dMsg &= "Interface untuk Akun VAR_GUDANG tidak ditemukan..!!<br />"
                End If

                If dMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(dMsg, CompnyName & " - Warning", 2)
                    Exit Sub
                End If

                sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAcctg & "'"
                xCmd.CommandText = sSql : OidCoaPesediaan = xCmd.ExecuteScalar

                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                "('" & CompnyCode & "'," & gldtloid & "," & glseq & "," & glmstoid & "," & OidCoaPesediaan & ",'C'," & (dtab.Rows(Vi).Item("hargabeli") * dtab.Rows(Vi).Item("qty")) - (ToDouble(dtab.Rows(Vi).Item("trnbelireturdtlqtydisc") * dtab.Rows(Vi).Item("qty"))) & "," & (dtab.Rows(Vi).Item("hargabeli") * dtab.Rows(Vi).Item("qty")) - (ToDouble(dtab.Rows(Vi).Item("trnbelireturdtlqtydisc") * dtab.Rows(Vi).Item("qty"))) & "," & dtab.Rows(Vi).Item("hargabeli") & ",'" & retno & "','Akun persediaan','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & CabangNya.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                gldtloid += 1 : glseq += 1
            Next
            sSql = "Update ql_mstoid set lastoid = " & gldtloid - 1 & " where tablename = 'QL_TRNGLDTL' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & ";" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setPReturnasActive()
    End Sub

    Protected Sub ibrejectpreturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibrejectpreturn.Click
        If IsDBNull(Session("trnbeliReturmstoid")) Then
            showMessage("Pilih purchase return yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trnbeliReturmstoid") = "" Then
                showMessage("Pilih purchase return yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If preturnno.Text = "" Then
            showMessage("Pilih purchase return yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "select trnbelistatus from QL_trnbeliReturmst where trnbeliReturmstoid = " & Integer.Parse(Session("trnbeliReturmstoid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Purchase return tidak bisa direject karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Purchase return tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trnbeliReturmst SET trnbelistatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_trnbeliReturdtl SET trnbelireturdtlflag = 'Rejected', trnbelireturdtlstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnbeliReturmstoid = '" & Session("trnbeliReturmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            '@@@@@@@@@@@@@@@@@@@@ update QL_Mst_SN  Ql_Mstitemdtl ketika Edit @@@@@@@@@@@@@@@@
            sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trnreturbelimstoid = '" & Session("trnbeliReturmstoid") & "'"

            Dim objTableSN As DataTable = cKon.ambiltabel(sSql, "TbSN")
            For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                Dim itemcodesn As String = cKon.ambilscalar(sSql)

                sSql = "update QL_Mst_SN set status_approval = 'POST',trnreturbelimstoid = NULL, tglreturbeli = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstitemDtl set  status_item= 'POST'  where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setPReturnasActive()
    End Sub

    Protected Sub ibbackpreturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibbackpreturn.Click
        setPReturnasActive()
    End Sub

    Protected Sub lkbTW_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbTW.Click
        Session("Type") = "TW"
        setTWasActive()
    End Sub

    Private Sub setTWasActive()
        bindTW() : MultiView1.SetActiveView(View26)
    End Sub

    Private Sub bindTW()
        Dim QlTw As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trntrfmtrmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")

        Dim sCodeTw As String = ""
        Dim TwCode() As String = QlTw.Split(",")
        For C1 As Integer = 0 To TwCode.Length - 1
            If TwCode(C1) <> "" Then
                sCodeTw &= "'" & TwCode(C1).Trim & "',"
            End If
        Next

        sSql = "select a.trfmtrmstoid, b.approvaloid, a.transferno, a.trfmtrdate, a.upduser, b.requestuser, b.requestdate from QL_trntrfmtrmst a inner join ql_approval b on a.trfmtrmstoid = b.oid where a.cmpcode = '" & CompnyCode & "' And b.approvaluser = '" & Session("userid") & "' and b.event = 'in approval' and b.statusrequest = 'new' and a.status = 'in approval' and b.tablename = 'QL_trntrfmtrmst' And b.branch_code IN (" & Left(sCodeTw, sCodeTw.Length - 1) & ") order by a.trfmtrmstoid desc"
        'and b.approvaluser = '" & Session("userid") & "'
        FillGV(GVTw, sSql, "GVTw")
    End Sub

    Protected Sub lkbSelectTW_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trfmtrmstoid") = sender.ToolTip : MultiView1.SetActiveView(View27)
        Session("approvaloid") = sender.CommandArgument
        sSql = "SELECT a.transferno, a.trfmtrdate, a.upduser,a.toMtrBranch,(select gendesc from QL_mstgen where gencode = a.fromMtrBranch AND gengroup='CABANG') fromMtrBranch,((select gendesc from QL_mstgen where genoid = (select genother1 from QL_mstgen where genoid = a.FromMtrlocoid AND gengroup='LOCATION'))+' - '+ b.gendesc) gudangasal,(select gendesc from QL_mstgen where gencode = a.toMtrBranch AND gengroup='CABANG') toMtrBranch,((select gendesc from QL_mstgen where genoid = (select genother1 from QL_mstgen where genoid = a.ToMtrlocOid AND gengroup='LOCATION'))+' - '+ c.gendesc) gudangtujuan, c.genoid tujuancode,fromMtrBranch DariBranch, toMtrBranch KirimKe FROM QL_trntrfmtrmst a inner join QL_mstgen b on a.cmpcode = b.cmpcode and a.FromMtrlocoid = b.genoid inner join QL_mstgen c on a.cmpcode = c.cmpcode and a.ToMtrlocOid = c.genoid WHERE trfmtrmstoid = " & Session("trfmtrmstoid") & " and a.cmpcode = '" & CompnyCode & "'"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "twdtl")
        twno.Text = dt.Rows(0).Item("transferno")
        twdate.Text = Format(dt.Rows(0).Item("trfmtrdate"), "dd/MM/yyyy")
        twuser.Text = dt.Rows(0).Item("upduser")
        FromBranch.Text = dt.Rows(0).Item("fromMtrBranch")
        twfrom.Text = dt.Rows(0).Item("gudangasal")
        ToBranch.Text = dt.Rows(0).Item("toMtrBranch")
        twto.Text = dt.Rows(0).Item("gudangtujuan")
        Session("DariBranch") = dt.Rows(0).Item("DariBranch")
        Session("KirimKe") = dt.Rows(0).Item("KirimKe")
        Session("codegudangtujuan") = dt.Rows(0).Item("tujuancode")
        sSql = "Select a.seq, b.itemdesc + ' - ' + b.merk itemdesc, a.qty, c.gendesc unit, a.trfdtlnote note,ISNULL(a.statusexp,'') statusexp,Isnull(a.typedimensi,0) typedimensi,ISNULL(a.beratvolume,0) beratvolume,ISNULL(a.beratbarang,0) beratbarang,ISNULL(a.jenisexp,'') jenisexp,ISNULL(a.amtexpedisi,0.00) amtexpedisi From ql_trntrfmtrdtl a inner join ql_mstitem b on a.cmpcode = b.cmpcode and a.refoid = b.itemoid and a.refname = 'ql_mstitem' inner join QL_mstgen c on a.cmpcode = c.cmpcode and a.unitoid = c.genoid Where a.trfmtrmstoid = " & Session("trfmtrmstoid") & " and a.cmpcode = '" & CompnyCode & "'"
        FillGV(GVTwDtl, sSql, "GVTwDtl")
    End Sub

    Protected Sub imbReviseTW_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbReviseTW.Click
        lbl_reviseTW.Visible = True
        lbl_reviseTW.Text = "Reason For Revision"
        txt_reviseTW.Visible = True
        txt_reviseTW.Focus()
        btnTwApprove.Visible = False
        btnTwReject.Visible = False
        imbReviseTW.Visible = False
        btnTwBack.Visible = False

        btnReviseTW.Visible = True
        btnCancelReviseTW.Visible = True
    End Sub

    Protected Sub btnCancelReviseTW_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelReviseTW.Click
        lbl_reviseTW.Visible = False
        lbl_reviseTW.Text = "Reason For Revision"
        txt_reviseTW.Visible = False
        txt_reviseTW.Focus()
        btnTwApprove.Visible = True
        btnTwReject.Visible = True
        imbReviseTW.Visible = True
        btnTwBack.Visible = True

        btnReviseTW.Visible = False
        btnCancelReviseTW.Visible = False
    End Sub

    Protected Sub btnReviseTW_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReviseTW.Click
        If txt_reviseTW.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_trntrfmtrmst SET status = 'Revised' , revisenote = '" & Tchar(txt_reviseTW.Text) & "' WHERE trfmtrmstoid = " & twno.Text & " and fromMtrBranch = '" & Session("DariBranch") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='QL_trntrfmtrmst' and oid=" & twno.Text & " and branch_code ='" & Session("DariBranch") & "' AND tablename = 'QL_trntrfmtrmst'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub btnTwApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTwApprove.Click
        If IsDBNull(Session("trfmtrmstoid")) Then
            showMessage("Pilih transfer warehouse yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trfmtrmstoid") = "" Then
                showMessage("Pilih transfer warehouse yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If twno.Text = "" Then
            showMessage("Pilih transfer warehouse yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "select status from QL_trntrfmtrmst where trfmtrmstoid = " & Integer.Parse(Session("trfmtrmstoid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse tidak bisa diapprove karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trntrfmtrmst SET status = 'Approved', finalapprovaluser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND trfmtrmstoid = '" & Session("trfmtrmstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "' AND tablename = 'QL_trntrfmtrmst'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "select trfmtrdate from QL_trntrfmtrmst WHERE cmpcode = '" & CompnyCode & "' AND trfmtrmstoid = " & Session("trfmtrmstoid") & ""

            xCmd.CommandText = sSql
            Dim transdate As Date = xCmd.ExecuteScalar
            Dim iCurID As Integer = 0 : Dim DariCabang As String = Session("DariBranch")
            sSql = "select genother1 from ql_mstgen Where gencode='" & DariCabang.ToString & "' And gengroup='CABANG'"
            xCmd.CommandText = sSql
            Dim cabang As String = xCmd.ExecuteScalar

            Dim twno As String = "TW/" & cabang & "/" & Format(transdate, "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transferno,4) AS INT)),0) FROM QL_trntrfmtrmst WHERE transferno LIKE '" & twno & "%'"
            xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
            twno = GenNumberString(twno, "", iCurID, 4)

            '@@@@@@@@@@@@@@@@@@@@ Update SN WT @@@@@@@@@@@@@@@@@@@@@@@@@@
            'If Session("TblSN") Is Nothing Then
            '    sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntrfoid = '" & Session("trfmtrmstoid") & "'"

            '    Session("TblSN") = cKon.ambiltabel(sSql, "TbSN")
            '    Dim objTableSN As DataTable
            '    objTableSN = Session("TblSN")
            '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
            '        Dim itemcodesn As String = cKon.ambilscalar(sSql)
            '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = CURRENT_TIMESTAMP, status_in_out = 'In', branch_code = '" & Session("KirimKe") & "', locoid = '" & Session("codegudangtujuan") & "', status_item = 'Post', last_trans_type = 'TW'  where sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_Mst_SN set status_approval = 'No PROCESS  ',trntrfoid = '" & twno & "', tgljual = NULL, trnjualmstoid = NULL, tgltrf = CURRENT_TIMESTAMP, last_trans_type = 'TW', status_in_out = 'In' where id_sn ='" & objTableSN.Rows(SNID).Item("SN") & "' and itemcode = '" & itemcodesn & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next
            'End If
            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            sSql = "UPDATE QL_trntrfmtrmst SET transferno = '" & twno & "' WHERE cmpcode = '" & CompnyCode & "' AND trfmtrmstoid = '" & Session("trfmtrmstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar

            Dim period As String = GetDateToPeriodAcctg(transdate).Trim
            sSql = "Select a.trfmtrdtloid, a.refoid, a.qty, a.unitoid, a.qty_to, a.unitoid_to, a.trfdtlnote,b.fromMtrBranch, b.FromMtrlocoid,b.toMtrBranch, b.ToMtrlocOid,ISNULL(a.statusexp,'') statusexp,Isnull(a.typedimensi,0) typedimensi,ISNULL(a.beratvolume,0) beratvolume,ISNULL(a.beratbarang,0) beratbarang,ISNULL(a.jenisexp,'') jenisexp,ISNULL(a.amtexpedisi,0.00) amtexpedisi from QL_trntrfmtrdtl a inner join QL_trntrfmtrmst b on a.cmpcode = b.cmpcode and a.trfmtrmstoid = b.trfmtrmstoid WHERE a.cmpcode = '" & CompnyCode & "' AND a.trfmtrmstoid = " & Session("trfmtrmstoid") & ""

            Dim dset As DataSet = New Data.DataSet
            Dim dadapter As SqlDataAdapter = New SqlDataAdapter(sSql, conn)
            dadapter.SelectCommand.Transaction = objTrans
            dadapter.Fill(dset, "dtab")
            Dim dtab As DataTable = dset.Tables("dtab")
            Dim SumHpp As Double = 0

            'Generate trnglmst ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : Dim glmstoid As Integer = xCmd.ExecuteScalar + 1

            'Generate trngldtl ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim gldtloid As Integer = xCmd.ExecuteScalar + 1
            Dim glsequence As Integer = 1

            sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime, type) VALUES ('" & CompnyCode & "', " & glmstoid & ", '" & transdate & "', '" & period & "', 'Transfer Gudang No. " & twno & "', 'POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'TW')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_mstoid SET lastoid = " & glmstoid & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For i As Integer = 0 To dtab.Rows.Count - 1
                '--------------------------
                Dim lasthpp As Double = 0
                sSql = "select hpp from ql_mstitem where itemoid=" & dtab.Rows(i).Item("refoid") & ""
                xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                'SumHpp += ToDouble(lasthpp)
                '---------------------------
                'Update crdmtr for item out

                'sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & dtab.Rows(i).Item("qty_to") & ", saldoakhir = saldoakhir - " & dtab.Rows(i).Item("qty_to") & ", LastTransType = 'TW', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' and branch_code = '" & dtab.Rows(i).Item("fromMtrBranch") & "' AND mtrlocoid = " & dtab.Rows(i).Item("FromMtrlocoid") & " AND refoid = " & dtab.Rows(i).Item("refoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                conmtroid = conmtroid + 1
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TW', '" & transdate & "', '" & period & "', '" & twno & "', " & dtab.Rows(i).Item("trfmtrdtloid") & ", 'QL_trntrfmtrdtl', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("unitoid") & ", " & dtab.Rows(i).Item("FromMtrlocoid") & ", 0, " & dtab.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtab.Rows(i).Item("trfdtlnote")) & "', " & ToDouble(lasthpp) & ", '" & dtab.Rows(i).Item("fromMtrBranch") & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update crdmtr for item in
                '(NEW KASUS , Approved Gudang Masuk ke BARANG PERJALANAN ) ID -10
                'sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & dtab.Rows(i).Item("qty_to") & ", saldoakhir = saldoakhir + " & dtab.Rows(i).Item("qty_to") & ", LastTransType = 'TW', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' And branch_code = '" & dtab.Rows(i).Item("toMtrBranch") & "' AND mtrlocoid = -10 AND refoid = " & dtab.Rows(i).Item("refoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                'xCmd.CommandText = sSql

                'If xCmd.ExecuteNonQuery() <= 0 Then
                '    'Insert crdmtr if no record found
                '    crdmtroid = crdmtroid + 1
                '    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & CompnyCode & "', " & crdmtroid & ", '" & period & "', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', -10 , '" & dtab.Rows(i).Item("toMtrBranch") & "', " & dtab.Rows(i).Item("qty_to") & ", 0, 0, 0, 0, " & dtab.Rows(i).Item("qty_to") & ", 0, 'TW', '" & transdate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'End If

                conmtroid = conmtroid + 1
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, branch_code, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TW', '" & transdate & "', '" & period & "', '" & twno & "', " & dtab.Rows(i).Item("trfmtrdtloid") & ", 'QL_trntrfmtrdtl', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("unitoid") & ", -10, '" & dtab.Rows(i).Item("toMtrBranch") & "', " & dtab.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtab.Rows(i).Item("trfdtlnote")) & "', " & ToDouble(lasthpp) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ''( DONE NEW KASUS , Approved Gudang Masuk ke BARANG PERJALANAN ) ID -10
            Next

            sSql = "update ql_mstoid set lastoid = " & conmtroid & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'sSql = "update ql_mstoid set lastoid = " & crdmtroid & " Where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '--------Insert Auto Jurnal----------
            '====================================
            Dim xBRanch As String = "" : Dim sTockFlag As String
            Dim COA_gudang As Integer = 0 : Dim vargudang As String = ""
            Dim iMatAccount As String = "" : Dim GudangCoa As Integer
            Dim hppnya As Double = 0 : Dim mVar As String = ""
            Dim sTxt As String = "" : Dim CbgDesc As String = ""
            Dim AsalCbg As String = ""

            For i As Integer = 0 To dtab.Rows.Count - 1
                sSql = "Select stockflag From ql_mstitem Where itemoid= " & dtab.Rows(i)("refoid").ToString & "" : xCmd.CommandText = sSql : sTockFlag = xCmd.ExecuteScalar

                If sTockFlag = "I" Or sTockFlag = "ASSET" Then
                    mVar = "VAR_PERJALANAN_NON_JUAL" : sTxt = "PERSEDIAAN PERJALANAN NON JUAL"
                Else
                    mVar = "VAR_PERJALANAN" : sTxt = "PERSEDIAAN PERJALANAN BARANG DAGANGAN"
                End If

                iMatAccount = GetVarInterface(mVar, dtab.Rows(i).Item("toMtrBranch"))

                If iMatAccount Is Nothing Or iMatAccount = "" Or iMatAccount = "?" Then
                    showMessage("Maaf, " & mVar & " untuk COA " & sTxt & " belum di setting, silahkan hubungi admin..!!", CompnyName & " - ERROR", 2)
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                Else
                    sSql = "Select acctgoid FROM ql_mstacctg WHERE acctgcode='" & iMatAccount & "'"
                    xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                    If COA_gudang = 0 Or COA_gudang = Nothing Then
                        showMessage("Maaf, " & mVar & " untuk COA " & sTxt & " belum di setting, silahkan hubungi admin..!!", CompnyName & " - ERROR", 2)
                        objTrans.Rollback()
                        conn.Close()
                        Exit Sub
                    End If
                End If

                'iMatAccount = GetVarInterface("VAR_PERJALANAN", CompnyCode)               
                sSql = "select hpp from ql_mstitem where itemoid=" & dtab.Rows(i).Item("refoid") & ""
                xCmd.CommandText = sSql : hppnya = xCmd.ExecuteScalar

                '-------- Cek jenis COA persediaan -------
                '-----------------------------------------

                sSql = "Select stockflag From ql_mstitem Where itemoid= " & dtab.Rows(i)("refoid").ToString & "" : xCmd.CommandText = sSql : sTockFlag = xCmd.ExecuteScalar

                sSql = "Select gencode From ql_mstgen Where gengroup='CABANG' AND gencode='" & dtab.Rows(i).Item("fromMtrBranch") & "'" : xCmd.CommandText = sSql : AsalCbg = xCmd.ExecuteScalar

                If sTockFlag = "I" Then
                    mVar = "VAR_INVENTORY" : sTxt = "PERSEDIAAN NON JUAL"
                ElseIf sTockFlag = "ASSET" Then
                    mVar = "VAR_GUDANG_ASSET" : sTxt = "PERSEDIAAN ASSET"
                Else
                    mVar = "VAR_GUDANG" : sTxt = "PERSEDIAAN BARANG DAGANGAN"
                End If

                iMatAccount = GetVarInterface(mVar, AsalCbg)

                If iMatAccount Is Nothing Or iMatAccount = "" Or iMatAccount = "?" Then
                    showMessage("Maaf, " & mVar & " untuk COA persediaan dalam perjalanan belum di setting, silahkan hubungi admin..!!", CompnyName & " - ERROR", 2)
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                Else
                    sSql = "Select acctgoid FROM ql_mstacctg WHERE acctgcode='" & iMatAccount & "'"
                    xCmd.CommandText = sSql : GudangCoa = xCmd.ExecuteScalar
                    If GudangCoa = 0 Or GudangCoa = Nothing Then
                        showMessage("Maaf, " & mVar & " untuk COA " & sTxt & " belum di setting, silahkan hubungi admin..!!", CompnyName & " - ERROR", 2)
                        objTrans.Rollback()
                        conn.Close()
                        Exit Sub
                    End If
                End If

                '---insert jurnal debet---
                '=========================
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & COA_gudang & ", 'D', " & ToDouble(hppnya) * ToDouble(dtab.Rows(i).Item("qty_to")) & "," & ToDouble(hppnya) * ToDouble(dtab.Rows(i).Item("qty_to")) & ",'" & Tchar(twno) & "', 'Transfer Gudang No. " & Tchar(twno) & "','" & Session("trfmtrmstoid") & "','" & dtab.Rows(i)("refoid").ToString & "','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                '---insert jurnal credit---
                '========================= 
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "', " & gldtloid & ", " & glsequence & ", " & glmstoid & ", " & GudangCoa & ", 'C', " & ToDouble(hppnya) * ToDouble(dtab.Rows(i).Item("qty_to")) & "," & ToDouble(hppnya) * ToDouble(dtab.Rows(i).Item("qty_to")) & ", '" & Tchar(twno) & "','Transfer Gudang No. " & Tchar(twno) & "','" & Session("trfmtrmstoid") & "', '" & dtab.Rows(i)("refoid").ToString & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                gldtloid = gldtloid + 1 : glsequence = glsequence + 1

                sSql = "UPDATE QL_mstoid SET lastoid =" & gldtloid - 1 & " WHERE tablename = 'QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setTWasActive()
    End Sub

    Protected Sub btnTwReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTwReject.Click
        If IsDBNull(Session("trfmtrmstoid")) Then
            showMessage("Pilih transfer warehouse yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trfmtrmstoid") = "" Then
                showMessage("Pilih transfer warehouse yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If twno.Text = "" Then
            showMessage("Pilih transfer warehouse yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "select status from QL_trntrfmtrmst where trfmtrmstoid = " & Integer.Parse(Session("trfmtrmstoid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse tidak bisa direject karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trntrfmtrmst SET status = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trfmtrmstoid = '" & Session("trfmtrmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setTWasActive()
    End Sub

    Protected Sub btnTwBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTwBack.Click
        setTWasActive()
    End Sub

    Protected Sub imbApproveFS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("trnsjjualmstoid")) Then
            showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trnsjjualmstoid") = "" Then
                showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If OrdernoFS.Text = "" Then
            showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Session("conmtroid") = ClassFunction.GenerateID("QL_conmtr", CompnyCode)

            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Dim statusUser As String = ""
            Dim level As String = ""
            Dim maxLevel As String = ""
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where " & _
                               "cmpcode='" & CompnyCode & "' and tablename='ql_trnsjjualmstPJ' and " & _
                               "approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where " & _
                                  "cmpcode='" & CompnyCode & "' and tablename='ql_trnsjjualmstPJ' ")
            statusUser = GetStrData("select approvaltype from QL_approvalstructure where " & _
                                    "cmpcode='" & CompnyCode & "' and tablename='ql_trnsjjualmstPJ' " & _
                                    "and approvaluser='" & Session("UserId") & "' " & _
                                    "and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus " & _
                       "from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' " & _
                       "AND tablename='ql_trnsjjualmstPJ' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then

                sSql = "SELECT seq trnsjjualdtlseq,d.trnsjjualdtloid salesdeliverydtloid,od.unitseq,od.trnorderprice, d.trnsjjualmstoid salesdeliverymstoid, d.trnorderdtloid orderdtloid, d.qty salesdeliveryqty, d.note itemdeliverynote, p.trnsjjualmstoid salesdeliveryoid, d.refoid itemoid, d.unitoid as itemunitoid, i.itemdesc itemlongdesc, g.gendesc AS unit ,ISNULL (( select sum(qty) from ql_trnsjjualdtl where trnsjjualmstoid  in (select trnsjjualmstoid from ql_trnsjjualmst  where trnsjjualstatus in ('Approved','INVOICED') and trnorderdtloid = od.trnorderdtloid )),0) as deliveredqty,od.trnorderdtlqty as referedqty,od.trnordermstoid,d.qtypak, d.mtrlocoid itemloc, g3.gendesc + ' - ' + g23.gendesc location, 0 allocorderoid FROM ql_trnsjjualdtl d inner join ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid inner join QL_trnordermst o on p.orderno = o.orderno and o.trnordertype = 'PROJECK' inner join ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid inner join QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'location' inner join QL_mstgen g3 on g23.genother1 = g3.genoid and g3.gengroup = 'warehouse' and g3.genother1 = 'GROSIR' inner join ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' inner join ql_mstgen g on d.cmpcode=p.cmpcode and d.unitoid = g.genoid WHERE d.trnsjjualmstoid =p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid AND d.trnsjjualmstoid =" & Session("trnsjjualmstoid") & " AND d.cmpcode='" & CompnyCode & "' ORDER BY d.seq,d.trnsjjualdtloid "

                Dim tblTemp As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtlPJ")
                GVfsDetail.DataSource = tblTemp
                GVfsDetail.DataBind()


                Dim conmtroid As Int64 = GenerateID("QL_conmtr", CompnyCode)
                Dim crdmtroid As Int64 = GenerateID("QL_crdmtr", CompnyCode)
                'lblRollOid.Text = GenerateID("QL_roll", CompnyCode)

                Dim awal As String = ""
                Dim lokasi As String = ""
                Dim tax As String = ""
                If ordernoteFS.Text <> "" Then
                    Dim cekawal As String = ordernoteFS.Text.Substring(1, 1)
                    'If cekawal = "O" Then
                    awal = "DOJ"
                    'ElseIf cekawal = "T" Then
                    '    awal = "DT"
                    'End If
                    lokasi = ordernoteFS.Text.Substring(11, 1)
                    tax = ordernoteFS.Text.Substring(3, 1)
                Else
                    awal = "DOJ"
                    lokasi = "L"
                    tax = "1"
                End If

                ' Dim sNo As String = "." & Format(GetServerTime, "yy") & "." & Format(GetServerTime, "MM") & "."
                Dim sNo As String = awal & tax & "/" & Format(GetServerTime, "yy") & "/" & Format(GetServerTime, "MM") & "/" & lokasi & "/"

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnsjjualno,4) AS INTEGER))+1,1) AS IDNEW FROM ql_trnsjjualmst " & _
                    "WHERE cmpcode='" & CompnyCode & "' AND trnsjjualno LIKE '" & sNo & "%'"
                OrdernoFS.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)


                'insert master SJ dan SI

                sSql = "update ql_trnsjjualmst set trnsjjualno = '" & OrdernoFS.Text & "',trnsjjualstatus = 'Approved' where trnsjjualmstoid = " & Session("trnsjjualmstoid") & " and cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'INSERT TO QL_trnsjjualdtl
                Dim cntr As Integer = 0
                Dim lstRollOid As Integer = 0
                Dim seq As Integer = 0

                For C1 As Integer = 0 To tblTemp.Rows.Count - 1
                    seq = seq + 1

                    'If posting.Text = "POST" Then


                    sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate=CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "update ql_trnorderdtl set orderdelivqty = (IsNull(orderdelivqty,0) + " & tblTemp.Rows(C1).Item("salesdeliveryqty") & "),trnorderdtlstatus=(select case when  trnorderdtlqty <= IsNull(orderdelivqty,0) + " & tblTemp.Rows(C1).Item("salesdeliveryqty") & " then 'Completed' else 'In Process' end) where ql_trnorderdtl.itemoid=" & tblTemp.Rows(C1).Item("itemoid") & " and ql_trnorderdtl.trnorderdtloid=" & tblTemp.Rows(C1).Item("orderdtloid") & " and ql_trnorderdtl.trnordermstoid= " & tblTemp.Rows(C1).Item("trnordermstoid") & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    sSql = "select case unitseq when 1 then konversi1_2 * konversi2_3 when 2 then konversi2_3 else 1 end konversi from ql_trnorderdtl d inner join ql_mstitem i on d.itemoid = i.itemoid where d.itemoid = " & tblTemp.Rows(C1).Item("itemoid") & " and d.trnorderdtloid = " & tblTemp.Rows(C1).Item("orderdtloid") & ""
                    xCmd.CommandText = sSql
                    Dim cek As String = xCmd.ExecuteScalar

                    Dim qtyterkonversi As Integer = 0
                    qtyterkonversi = ToDouble(cek) * ToDouble(tblTemp.Rows(C1).Item("salesdeliveryqty"))

                    'sSql = "update ql_trnorderallocdtl set deliveredqty = (IsNull(deliveredqty,0)+" & qtyterkonversi & "),allocstatus=(select case when allocationqty <= IsNull(deliveredqty,0) + " & qtyterkonversi & " then 'Completed' else 'In Process' end) where ql_trnorderallocdtl.allocorderoid=" & objTable.Rows(C1).Item("allocorderoid")
                    'xCmd.CommandText = sSql
                    'xCmd.ExecuteNonQuery()

                    '--------------------------
                    Dim lasthpp As Double = 0
                    sSql = "select hpp  from ql_mstitem where itemoid=" & tblTemp.Rows(C1).Item("itemoid") & " "
                    xCmd.CommandText = sSql
                    lasthpp = xCmd.ExecuteScalar
                    '---------------------------

                    'insert into connmtr if posting
                    sSql = "INSERT INTO QL_conmtr(cmpcode,conmtroid,formoid,formname,type,trndate," & _
                        "periodacctg,refoid,refname, qtyout,amount," & _
                        "note,formaction,upduser,updtime,  mtrlocoid,unitoid,hpp) VALUES ('" & CompnyCode & "'," & conmtroid & _
                        "," & (tblTemp.Rows(C1).Item("salesdeliverydtloid")) & ",'QL_trnsjjualdtl','PROJECK','" & CDate(toDate(OrderDateFS.Text)) & "','" & _
                        GetDateToPeriodAcctg(CDate(toDate(OrderDateFS.Text))) & "' " & _
                        "," & tblTemp.Rows(C1).Item("itemoid") & ",'QL_MSTITEM'," & tblTemp.Rows(C1).Item("salesdeliveryqty") & "," & tblTemp.Rows(C1).Item("salesdeliveryqty") & "*(select trnamountdtl/trnorderdtlqty from ql_trnorderdtl where trnorderdtloid = " & tblTemp.Rows(C1).Item("orderdtloid") & "),'" & Tchar(tblTemp.Rows(C1).Item("itemdeliverynote")) & "','" & OrdernoFS.Text & "','" & _
                        Session("UserID") & "',current_timestamp," & tblTemp.Rows(C1).Item("itemloc") & "," & (tblTemp.Rows(C1).Item("itemunitoid")) & "," & ToDouble(lasthpp) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    conmtroid += 1

                    ''
                    sSql = "select count(*) from QL_crdmtr where  periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(OrderDateFS.Text))) & "' and refoid = " & tblTemp.Rows(C1).Item("itemoid") & " and refname = 'QL_MSTITEM' and mtrlocoid = " & tblTemp.Rows(C1).Item("itemloc") & " and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteScalar() <= 0 Then
                        sSql = "insert into QL_crdmtr(cmpcode, crdmatoid, lasttrans, lasttranstype,qtyout, periodacctg,  refoid,saldoakhir, upduser, updtime, refname, mtrlocoid) VALUES ('" & CompnyCode & "', '" & (crdmtroid + C1) & "','" & CDate(toDate(OrderDateFS.Text)) & "', 'PROJECK'," & qtyterkonversi & ",'" & GetDateToPeriodAcctg(CDate(toDate(OrderDateFS.Text))) & "'," & tblTemp.Rows(C1).Item("itemoid") & "," & qtyterkonversi & ", '" & Session("UserID") & "', current_timestamp, 'QL_MSTITEM'," & tblTemp.Rows(C1).Item("itemloc") & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        'End If
                    Else

                        sSql = "update QL_crdmtr set lasttrans='" & CDate(toDate(OrderDateFS.Text)) & "',lasttranstype='PROJECK', qtyout =  qtyout +  " & qtyterkonversi & ", saldoakhir = saldoakhir -  " & qtyterkonversi & " where  periodacctg = '" & GetDateToPeriodAcctg(CDate(toDate(OrderDateFS.Text))) & "' and refoid = " & tblTemp.Rows(C1).Item("itemoid") & " and refname = 'QL_MSTITEM' and mtrlocoid = " & tblTemp.Rows(C1).Item("itemloc") & " and cmpcode = '" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    'End If


                Next


                ''update oid in ql_mstoid where tablename = 'QL_trndelivorderbatch'
                'sSql = "update QL_mstoid set lastoid=" & trnjualdtlbatchoid.Text + objTable.Rows.Count - 1 & " where tablename ='QL_trndelivorderbatch' and cmpcode = '" & CompnyCode & "' "
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

                'If posting.Text = "POST" Then
                ''update oid in ql_mstoid where tablename = 'QL_conmtr'
                sSql = "update  QL_mstoid set lastoid=" & (tblTemp.Rows.Count - 1 + conmtroid) & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "update  QL_mstoid set lastoid=" & (tblTemp.Rows.Count - 1 + crdmtroid) & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                'End If

                'done insert ke stock
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='ql_trnsjjualmstPJ' " & _
                       "and oid=" & Session("trnsjjualmstoid") & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else 'jika tidak final

                'update ke SJ
                sSql = "update ql_trnsjjualmst set updtime=CURRENT_TIMESTAMP " & _
                                   "WHERE cmpcode='" & CompnyCode & "' and  trnsjjualmstoid=" & Session("trnsjjualmstoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='ql_trnsjjualmstPJ' " & _
                        "and oid=" & Session("trnsjjualmstoid") & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                          "approvaldate=CURRENT_TIMESTAMP, " & _
                          "approvalstatus='Ignore', " & _
                          "event='Ignore', " & _
                          "statusrequest='End' " & _
                          "WHERE cmpcode='" & CompnyCode & "' " & _
                          "and tablename='ql_trnsjjualmstPJ' " & _
                          "and oid=" & Session("trnsjjualmstoid") & " " & _
                          "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'insertkan untuk user approval yg levelnya diatasnya lagi
                If Not Session("TblApproval") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblApproval")

                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL(cmpcode,approvaloid,requestcode, " & _
                               "requestuser,requestdate,statusrequest,tablename,oid,event, " & _
                               "approvalcode,approvaluser,approvaldate,approvaltype, " & _
                               "approvallevel,approvalstatus) " & _
                               "VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", " & _
                               "'" & "DOJ" & Session("trnsjjualmstoid") & "_" & Session("AppOid") + c1 & "', " & _
                               "'" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_trnsjjualmstPJ', " & _
                               "'" & Session("trnsjjualmstoid") & "','In Approval','0', " & _
                               "'" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900', " & _
                               "'" & objTable.Rows(c1).Item("approvaltype") & "','1', " & _
                               "'" & objTable.Rows(c1).Item("approvalstatus") & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()

                    Next
                    sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                           objTable.Rows.Count - 1 & " " & _
                           "where tablename = 'QL_Approval'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If



            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try

        setFSasActive()
    End Sub

    Protected Sub imbRejectFS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("trnsjjualmstoid")) Then
            showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trnsjjualmstoid") = "" Then
                showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If OrdernoFS.Text = "" Then
            showMessage("Please select a Sales Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update SDO
            sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnsjjualmstoid = '" & Session("trnsjjualmstoid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='ql_trnsjjualmstPJ' and oid=" & Session("trnsjjualmstoid") & " "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setFSasActive()

    End Sub

    Protected Sub btnColApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnColReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
       
    End Sub

    Protected Sub btnColBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvCollectionInvoice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")

            Dim objGrid As GridView = CType(e.Row.Cells(6).FindControl("gvInvoice"), GridView)
            sSql = "select mst.trnjualreturno returno, mst.trnjualdate date, mst.trnamtjualnetto returAmount  " & _
                   "from QL_trnjualreturmst mst where mst.trnjualref = '" & e.Row.Cells(3).Text & "'"

            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
            objGrid.DataSource = objTablee.DefaultView
            objGrid.DataBind()
        End If
    End Sub

    Private Sub showValidation(ByVal sotype As String)
        PanelPass.Visible = True : btnPass.Visible = True
        MPEPass.Show()
        refSO.Text = sotype
    End Sub

    Private Function checkValidation(ByVal sType As String, ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If userconfim.Text.Trim = "" Then
            showMessage("UserID harus diisi", CompnyName & " - WARNING", 2)
            lblError.Text = "showMsg"
            Return False
            Exit Function
        End If

        If txtPass.Text.Trim = "" Then
            showMessage("Password harus diisi", CompnyName & " - WARNING", 2)
            lblError.Text = "showMsg"
            Return False
            Exit Function
        End If

        If userconfim.Text.Trim <> "" Then
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            sSql = "SELECT count(userid) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval' And divisicreditlimit='Approval Credit Limit' And PROFPASS='" & Tchar(txtPass.Text) & "'"
            xCmd.CommandText = sSql
            'If sType = "CR" Then
            '    sSql = "SELECT count(userid) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval' and divisicreditlimit='Approval Credit Limit'"
            '    xCmd.CommandText = sSql
            'ElseIf sType = "piutang" Then
            '    sSql = "SELECT count(userid) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval'"
            '    xCmd.CommandText = sSql
            'Else
            '    sSql = "SELECT count(userid) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval'"
            '    xCmd.CommandText = sSql
            'End If

            Dim id As Integer = xCmd.ExecuteScalar
            If id = 0 Then
                userconfim.Text = "" : txtPass.Text = ""
                showMessage("Maaf, Login dengan UserID tersebut tidak bisa Approved data ini, Silakan hubungi Admin", CompnyName & " - WARNING", 2)
                lblError.Text = "showMsg"
                Return False
                Exit Function
            Else
                If sType = "CR" Then
                    SOconfimID.Text = userconfim.Text
                    confirmCR.Text = "ACC"
                    imbApproveSDO_Click(sender, e) 
                End If
            End If

            conn.Close()
        End If

        'If txtPass.Text.Trim <> "" Then
        '    If conn.State = ConnectionState.Closed Then
        '        conn.Open()
        '    End If

        '    If sType = "CR" Then
        '        sSql = "SELECT LOWER(profpass) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval' and divisicreditlimit = 'Approval Credit Limit'"
        '        xCmd.CommandText = sSql
        '    ElseIf sType = "piutang" Then
        '        sSql = "SELECT LOWER(profpass) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval'"
        '        xCmd.CommandText = sSql
        '    Else
        '        sSql = "SELECT LOWER(profpass) FROM QL_mstprof WHERE userid = '" & Tchar(userconfim.Text) & "' and divisi = 'Approval'"
        '        xCmd.CommandText = sSql
        '    End If
        '    Dim pass As String = xCmd.ExecuteScalar

        '    conn.Close()

        '    If txtPass.Text.ToLower = pass Then
        '        If sType = "ORDER" Then
        '            SOconfimID.Text = userconfim.Text
        '            imbApproveSO_Click(sender, e)
        '        ElseIf sType = "CR" Then
        '            confirmCR.Text = userconfim.Text
        '            imbApproveSO_Click(sender, e)
        '        ElseIf sType = "piutang" Then
        '            lblpiutang.Text = userconfim.Text
        '            imbApproveSO_Click(sender, e)
        '        Else
        '            SOPJconfimID.Text = userconfim.Text
        '            imbApproveSQ_Click(sender, e)
        '        End If
        '        'showPrint(sType)
        '    Else
        '        txtPass.Text = ""
        '        showMessage("Password yang Anda inputkan salah", CompnyName & " - WARNING", 2)
        '        lblError.Text = "showMsg"
        '        Return False
        '        Exit Function
        '    End If
        'End If
    End Function

    Protected Sub btnOKPass_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelPass.Visible = False
        btnPass.Visible = False
        MPEPass.Hide()
        checkValidation(refSO.Text, sender, e)
    End Sub

    Protected Sub btnCancelPass_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelPass.Visible = False
        btnPass.Visible = False
        MPEPass.Hide()
    End Sub

    Protected Sub lbkPricelist_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub linkSalesActv_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub lkbselectSa_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnReviseSubmitSO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReviseSubmitSO.Click
        If txt_reviseSO.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_trnordermst SET trnorderstatus = 'Revised', revisenote = '" & Tchar(txt_reviseSO.Text) & "' WHERE ordermstoid = " & Session("orderoid") & " and branch_code ='" & Session("CodeC") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='ql_trnordermst' and oid=" & Session("orderoid") & " and branch_code ='" & Session("CodeC") & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If

    End Sub

    Protected Sub ImbReviseSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImbReviseSO.Click
        imbApproveSO.Visible = False : imbRejectSO.Visible = False
        btnReviseSubmitSO.Enabled = True
        lbl_ReviseSO.Visible = True : txt_reviseSO.Visible = True
        ImbReviseSO.Visible = False : imbBackSO.Visible = False
        btncancelreviseSO.Visible = True
        lbl_ReviseSO.Text = "Reason For Revision" : btnReviseSubmitSO.Visible = True
    End Sub

    Protected Sub btncancelreviseSO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncancelreviseSO.Click
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub imbReviseDO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbReviseDO.Click
        lbl_revisedo.Visible = True
        lbl_revisedo.Text = "Reason For Revision"
        txt_revisedo.Visible = True
        txt_revisedo.Focus()
        imbApproveSDO.Visible = False
        imbRejectSDO.Visible = False
        imbReviseDO.Visible = False
        imbBackSDO.Visible = False
        btnReviseDO.Visible = True
        btnCancelReviseDO.Visible = True
    End Sub

    Protected Sub btnReviseDO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReviseDO.Click
        If txt_revisedo.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus = 'Revised' , revisenote = '" & Tchar(txt_revisedo.Text) & "' WHERE trnsjjualmstoid = " & Integer.Parse(sjjualmstoid.Text) & " and branch_code = '" & zCabang.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_approval SET event='Revised',statusrequest='Revised' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Integer.Parse(ApprovedOid.Text) & "' And branch_code='" & zCabang.Text & "' And tablename='QL_trnsjjualmst'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Update Approval user yg lain 
                sSql = "update QL_approval set event='Ignore',statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='QL_trnsjjualmst' And oid=" & Integer.Parse(sjjualmstoid.Text) & " AND approvaloid <> '" & ApprovedOid.Text & "' And branch_code='" & zCabang.Text & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub btnCancelReviseDO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelReviseDO.Click
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub imbApproveSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("orderoid")) Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2)
            Exit Sub
        Else
            If Session("orderoid") = "" Then
                showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2)
                Exit Sub
            End If
        End If
        If orderno.Text = "" Then
            showMessage("Please select a Sales Order first", CompnyName & " - Warning", 2)
            Exit Sub
        End If

        sSql = "Select ordermstoid, orderno, trnorderstatus From QL_trnordermst WHere trnorderstatus IN ('Canceled','Approved','Closed','Rejected','POST') AND branch_code='" & Session("CodeC") & "' AND ordermstoid=" & Session("orderoid")
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                showMessage("- Maaf, No. draft SO '" & dta.Rows(i).Item("ordermstoid") & "' sudah di '" & dta.Rows(i).Item("trnorderstatus") & "' dengan nomer '" & dta.Rows(i).Item("orderno") & "'..!!<br>", CompnyName & " - WARNING", 2)
            Next
        End If

        '-------------------------------------BEGIN ZIPI-------------------------------------
        '------------ambil rate,totalPriceDtl dan custoid dr SO yg mau di approved-----------

        Dim cek_rate As String = ""
        cek_rate = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid =(select currencyoid from QL_trnordermst where ordermstoid='" & Session("orderoid") & "' and branch_code='" & Session("CodeC") & "') and activeflag ='active' AND '" & (lbldate.Text) & " 00:00:00'>=rate2date order by rate2oid desc")
        If cek_rate = "?" Then
            showMessage("rate untuk period ini belum tersedia, silahkan create rate terlebih dahulu!", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        Dim rate As Decimal = GetScalar("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid =(select currencyoid from QL_trnordermst where ordermstoid='" & Session("orderoid") & "' and branch_code='" & Session("CodeC") & "') and rate2oid = (select rate2oid from ql_trnordermst where ordermstoid='" & Session("orderoid") & "' and branch_code='" & Session("CodeC") & "' ) and activeflag ='active'") 'kunci

        'Dim custoid As String = GetStrData("select ISNULL((select custgroupoid from QL_mstcustgroup where custgroupoid = c.custgroupoid ),'0') custgroupoid from ql_trnordermst od inner join QL_mstcust c on c.custoid = od.trncustoid Where od.cmpcode='" & CompnyCode & "' and od.branch_code='" & Session("CodeC") & "' and ordermstoid = '" & Session("orderoid") & "'")

        'Dim priceDtl As Decimal = GetStrData("select amtjualnetto from QL_trnordermst m where m.cmpcode='" & CompnyCode & "' and branch_code='" & Session("CodeC") & "'and  m.ordermstoid = '" & Session("orderoid") & "'")
        '--------------------------------------------------------------------------
        '-------------------------------Warning Done-------------------------------

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Dim statusUser As String = "" : Dim level As String = "" : Dim maxLevel As String = ""
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnordermst' and approvaluser='" & Session("UserId") & "'")

            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnordermst'")

            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnordermst' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnordermst' And approvallevel=" & level + 1 & ""
                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                '-----------------------------------------------------------------------
                '----------------------------------- cek otorisasi ---------------------
                Dim orderlimitbottom As String = GetStrData("select flagbottomprice from ql_trnordermst where cmpcode='" & CompnyCode & "' and ordermstoid = '" & Session("orderoid") & "' and branch_code='" & Session("CodeC") & "'")

                'If orderlimitbottom = "T" Then
                '    If SOconfimID.Text.Trim = "" Then
                '        'get data item2 bawah bottomprice
                '        sSql = "select i.itemdesc ,d.trnorderdtlqty,unit.gendesc unit ,d.trnorderprice ,trnamountbruto ,trndiskonpromo,trnamountdtl ,ISNULL(pro.promeventcode  ,'-') promocode ,bottomprice ,amountbottomprice from QL_trnorderdtl d " & _
                '        " inner join QL_mstItem i on d.itemoid = i.itemoid " & _
                '        " inner join QL_mstgen unit on d.trnorderdtlunitoid = unit.genoid " & _
                '        " left join QL_mstpromo pro on pro.promoid = d.promooid " & _
                '        " Where d.cmpcode='" & CompnyCode & "' And trnordermstoid = '" & Session("orderoid") & "' and d.flagbottompricedtl = 'T'"
                '        'tampung di notif
                '        Dim objTablecek As DataTable
                '        Dim objRowcek() As DataRow
                '        objTablecek = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
                '        objRowcek = objTablecek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                '        lblMessageBottom.Text = "Transaksi berikut terdapat penjualan dibawah BottomPrice <br><br>"
                '        For c1 As Integer = 0 To objRowcek.Length - 1
                '            lblMessageBottom.Text &= "Item : " & objTablecek.Rows(c1)("itemdesc") & " | Qty : " & objTablecek.Rows(c1)("trnorderdtlqty") & " " & objTablecek.Rows(c1)("unit") & " | Price : " & ToMaskEdit(objTablecek.Rows(c1)("trnorderprice"), 3) & " | DiscPromo : " & ToMaskEdit(objTablecek.Rows(c1)("trndiskonpromo"), 3) & " | AmountNetto : " & ToMaskEdit(objTablecek.Rows(c1)("trnamountdtl"), 3) & " | PromoCode : " & objTablecek.Rows(c1)("promocode") & " | BottomPrice : " & ToMaskEdit(objTablecek.Rows(c1)("bottomprice"), 3) & " | <br>"
                '        Next
                '        'showValidation("ORDER")
                '        'Exit Sub
                '    End If
                'End If
                '--------------------- generate NO ---------------------
                Dim flag As String = "" : Dim sType As String = ""
                Dim typeso As String = GetStrData("select trnordertype from ql_trnordermst Where ordermstoid = '" & Session("orderoid") & "' and branch_code= '" & Session("CodeC") & "'")

                If typeso = "GROSIR" Then : flag = "SO"
                Else : flag = "SOJ" : End If

                If SOType.Text = "Konsinyasi" Then : sType = "K" : End If

                Dim tax As String = ""
                If SOtax.Text = "INC" Then : tax = "0"
                ElseIf SOtax.Text = "NONTAX" Then : tax = "1" : End If

                Dim tmbhno As String = "" : tmbhno = "1"
                Dim exportno As String = "" : Dim BO As String = ""
                If SOBO.Text = "YES" Then : BO = "B"
                Else : BO = "L" : End If

                Dim iCurID As Integer = 0

                Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & Session("CodeC") & "' AND gengroup='CABANG'")
                Dim sNo As String = flag & tax & sType & "/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(orderno,4) AS INT)),0) AS IDNEW FROM ql_trnordermst WHERE branch_code='" & Session("CodeC") & "' and cmpcode='" & CompnyCode & "' AND orderno LIKE '" & sNo & "%'"
                iCurID = cKoneksi.ambilscalar(sSql) + 1
                orderno.Text = GenNumberString(sNo, "", iCurID, 4)
                'End If

                ' Update SO
                sSql = "UPDATE ql_trnordermst SET orderno = '" & orderno.Text & "', trnorderstatus = 'Approved',trnorderdate = '" & Format(GetServerTime(), "MM/dd/yyyy") & "', periodacctg = '" & Format(GetServerTime(), "yyyy-MM") & "', finalapprovaluser='" & Session("UserID") & "', finalappovaldatetime=CURRENT_TIMESTAMP, otorisasiuser = '" & Tchar(SOconfimID.Text) & "' WHERE cmpcode = '" & CompnyCode & "' And branch_code='" & Session("CodeC") & "' AND ordermstoid = '" & Session("orderoid") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Update Approval
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='ql_trnordermst' " & _
                        "and oid=" & Session("orderoid") & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & Session("CodeC") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='ql_trnordermst' " & _
                       "and oid=" & Session("orderoid") & " " & _
                       "and approvaluser<>'" & Session("UserId") & _
                       "' and branch_code='" & Session("CodeC") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else 'Process

                sSql = "Update ql_trnordermst set updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' And branch_code='" & Session("CodeC") & "' And ordermstoid=" & Session("orderoid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='ql_trnordermst' " & _
                        "and oid=" & Session("orderoid") & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & Session("CodeC") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                          "approvaldate=CURRENT_TIMESTAMP, " & _
                          "approvalstatus='Ignore', " & _
                          "event='Ignore', " & _
                          "statusrequest='End' " & _
                          "WHERE cmpcode='" & CompnyCode & "' " & _
                          "and tablename='ql_trnordermst' " & _
                          "and oid=" & Session("orderoid") & " " & _
                          "and approvaluser<>'" & Session("UserId") & _
                          "' and branch_code='" & Session("CodeC") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insertkan untuk user approval yg levelnya diatasnya lagi
                If Not Session("TblApproval") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("TblApproval")

                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode, requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode,approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus),branch_code " & _
                               "VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", '" & "SO" & Session("orderoid") & "_" & Session("AppOid") + c1 & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','ql_trnordermst','" & Session("orderoid") & "','In Approval','0','" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900','" & objTable.Rows(c1).Item("approvaltype") & "','1','" & objTable.Rows(c1).Item("approvalstatus") & "','" & Session("CodeC") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                           objTable.Rows.Count - 1 & " " & _
                           "Where tablename = 'QL_Approval'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1) : Exit Sub
        End Try
        SOconfimID.Text = "" : refSO.Text = ""
        If Session("Type") = "SO" Then
            setSOasActive()
        ElseIf Session("Type") = "IO" Then
            'setIOasActive()
        Else
            setFSasActive()
        End If
    End Sub

    Protected Sub gvSODetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imb_revise_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lbl_revisespk.Visible = True
        lbl_revisespk.Text = "Reason For Revision"
        txtrevise.Visible = True
        txtrevise.Focus()
        ibapprovespk.Visible = False
        ibrejectspk.Visible = False
        imb_revise.Visible = False
        ibbackspk.Visible = False

        imbsubmitrevisi.Visible = True
        imbcancelrevisi.Visible = True
    End Sub

    Protected Sub imbsubmitrevisi_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtrevise.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_trnwomst SET womststatus = 'Revised' , worevisenote = '" & Tchar(txtrevise.Text) & "' WHERE womstoid = " & lblspkmainoid.Text & " and tobranch_code = '" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='QL_TRNWOMST' and oid=" & lblspkmainoid.Text & "  "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                imbsubmitrevisi.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub imbcancelrevisi_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lbl_revisespk.Visible = False
        lbl_revisespk.Text = "Reason For Revision"
        txtrevise.Visible = False
        txtrevise.Focus()
        ibapprovespk.Visible = True
        ibrejectspk.Visible = True
        imb_revise.Visible = True
        ibbackspk.Visible = True

        imbsubmitrevisi.Visible = False
        imbcancelrevisi.Visible = False
    End Sub

    Public Sub GenerateBulan(ByVal bln1 As String, ByVal note As String)
        If bln1 = "1" Then
            If note = "satu" Then
                lblbln1.Text = "Januari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Januari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Januari"
            End If
        ElseIf bln1 = "2" Then
            If note = "satu" Then
                lblbln1.Text = "Februari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Februari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Februari"
            End If
        ElseIf bln1 = "3" Then
            If note = "satu" Then
                lblbln1.Text = "Maret"
            ElseIf note = "dua" Then
                lblbln2.Text = "Maret"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Maret"
            End If
        ElseIf bln1 = "4" Then
            If note = "satu" Then
                lblbln1.Text = "April"
            ElseIf note = "dua" Then
                lblbln2.Text = "April"
            ElseIf note = "tiga" Then
                lblbln3.Text = "April"
            End If
        ElseIf bln1 = "5" Then
            If note = "satu" Then
                lblbln1.Text = "Mei"
            ElseIf note = "dua" Then
                lblbln2.Text = "Mei"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Mei"
            End If
        ElseIf bln1 = "6" Then
            If note = "satu" Then
                lblbln1.Text = "Juni"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juni"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juni"
            End If
        ElseIf bln1 = "7" Then
            If note = "satu" Then
                lblbln1.Text = "Juli"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juli"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juli"
            End If
        ElseIf bln1 = "8" Then
            If note = "satu" Then
                lblbln1.Text = "Agustus"
            ElseIf note = "dua" Then
                lblbln2.Text = "Agustus"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Agustus"
            End If
        ElseIf bln1 = "9" Then
            If note = "satu" Then
                lblbln1.Text = "September"
            ElseIf note = "dua" Then
                lblbln2.Text = "September"
            ElseIf note = "tiga" Then
                lblbln3.Text = "September"
            End If
        ElseIf bln1 = "10" Then
            If note = "satu" Then
                lblbln1.Text = "Oktober"
            ElseIf note = "dua" Then
                lblbln2.Text = "Oktober"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Oktober"
            End If
        ElseIf bln1 = "11" Then
            If note = "satu" Then
                lblbln1.Text = "November"
            ElseIf note = "dua" Then
                lblbln2.Text = "November"
            ElseIf note = "tiga" Then
                lblbln3.Text = "November"
            End If
        ElseIf bln1 = "12" Then
            If note = "satu" Then
                lblbln1.Text = "Desember"
            ElseIf note = "dua" Then
                lblbln2.Text = "Desember"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Desember"
            End If
        End If
    End Sub

    Private Sub GenerateCBNo()
        Dim iCurID As Integer = 0 : Dim bType As String = ""
        If payflag.SelectedValue = "CASH" Then
            bType = "BKM"
        Else
            bType = "BBM"
        End If
        Dim cabang As String = GetStrData("select substring(gendesc,1,3) from ql_mstgen Where gencode='" & Session("branch_id") & "' And gengroup='CABANG'")
        Session("vNoCashBank") = bType & "/" & cabang & "/" & Format(CDate(toDate(RealDate.Text)), "yy/MM/dd") & "/"

        sSql = "SELECT MAX(replace(cashbankno,'" & Session("vNoCashBank") & "','')) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank") & "%' AND cmpcode='" & CompnyCode & "'"

        If Not IsDBNull(GetScalar(sSql)) Then
            iCurID = GetScalar(sSql) + 1
        Else
            iCurID = 1
        End If
        Session("sNo") = GenNumberString(Session("vNoCashBank"), "", iCurID, 4)
    End Sub

    Private Sub ddlCOACb(ByVal cVar As String)
        FillDDLAcctg(DdlAkun, cVar, Session("branch_id"))
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sVar As String = "" : Dim dVal As Boolean = False
        If payflag.SelectedValue = "BANK" Then
            sVar = "VAR_BANK" : dVal = True : Titik2.Visible = True
            LblAkun.Visible = True : DdlAkun.Visible = True
            acctgoidreal.Text = DdlAkun.SelectedValue
        ElseIf payflag.SelectedValue = "CASH" Then
            sVar = "VAR_CASH" : dVal = True : Titik2.Visible = True
            LblAkun.Visible = True : DdlAkun.Visible = True
            acctgoidreal.Text = DdlAkun.SelectedValue
        Else
            LblAkun.Visible = False : DdlAkun.Visible = False
            Titik2.Visible = False : acctgoidreal.Text = DdlAkun.SelectedValue
        End If
        ddlCOACb(sVar)
    End Sub

    Protected Sub PostBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cRate As New ClassRate()
        Dim objtbl As DataTable = Session("TblDtl")
        sSql = "select top 1 isnull(curratestoIDRbeli,1) from QL_mstcurrhist where cmpcode='" & CompnyCode & "' and curroid=" & 1 & " order by currdate desc"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim currencyrate As Integer = ToMaskEdit(xCmd.ExecuteScalar, 3)

        cRate.SetRateValue(CInt(1), Format(GetServerTime, "MM/dd/yyyy"))
        If cRate.GetRateDailyLastError <> "" Then
            showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim awal As String = "" : Dim awalSI As String = ""
        Dim lokasi As String = "" : Dim tax As String = ""
        If orderno.Text <> "" Then
            Dim cekawal As String = orderno.Text.Substring(1, 1)
            'If cekawal = "O" Then
            If trnSjtype.Text = "GROSIR" Then
                awal = "DO" : awalSI = "SI"
            Else
                awal = "DOJ" : awalSI = "SIJ"
            End If
            lokasi = orderno.Text.Substring(10, 1)
            tax = orderno.Text.Substring(2, 1)
        Else
            If trnSjtype.Text = "GROSIR" Then
                awal = "DO" : awalSI = "SI"
            Else
                awal = "DOJ" : awalSI = "SIJ"
            End If
            lokasi = "L" : tax = 1
        End If

        sSql = "select sum((sod.trnamountdtl/sod.trnorderdtlqty) * d.qty ) from ql_trnsjjualdtl d inner join QL_mstItem i on i.itemoid = d.refoid and d.refname = 'ql_mstitem' inner join ql_trnsjjualmst m on d.trnsjjualmstoid = m.trnsjjualmstoid inner join QL_mstgen g on i.payoid = g.genoid inner join QL_trnorderdtl sod on d.trnorderdtloid = sod.trnorderdtloid Where d.cmpcode = '" & CompnyCode & "' and sod.branch_code='" & Session("branch_id") & "' and d.trnsjjualmstoid = " & Session("trnsjjualmstoid") & " /*and i.payoid = " & TOP.SelectedValue & "*/"
        xCmd.CommandText = sSql : Dim trnamtjualnetto As Double = xCmd.ExecuteScalar
        Dim amtjualnetto As Double = 0
        If tax = 0 Then
            amtjualnetto = Math.Round(ToDouble(trnamtjualnetto) + ToDouble(trnamtjualnetto) * ToDouble(trntaxpct.Text) / 100)
        Else
            amtjualnetto = Math.Round(ToDouble(trnamtjualnetto), 2)
        End If

        conn.Close()

        sSql = "SELECT seq seqdtl,d.trnsjjualdtloid salesdeliverydtloid,od.unitseq,(ISNULL(od.amtdtldisc1,0.0)/od.trnorderdtlqty*d.qty)+(ISNULL(od.amtdtldisc2,0.0)/od.trnorderdtlqty*d.qty)+ISNULL(trndiskonpromoidr,0.0) discperqty,Case od.typebarang When 'BONUS' Then 0.0 Else od.trnorderprice End trnorderprice, d.trnsjjualmstoid salesdeliverymstoid, d.trnorderdtloid orderdtloid,d.qty salesdeliveryqty, p.trnsjjualmstoid salesdeliveryoid,d.refoid itemoid,ISNULL((select sum(qty) from ql_trnsjjualdtl Where trnsjjualmstoid IN (select trnsjjualmstoid from ql_trnsjjualmst Where trnsjjualstatus in ('Approved','INVOICED') and trnorderdtloid = od.trnorderdtloid )),0) as deliveredqty,od.trnorderdtlqty as referedqty,od.trnordermstoid,d.mtrlocoid as itemloc " & _
               "FROM ql_trnsjjualdtl d " & _
               "inner join ql_trnsjjualmst p on d.cmpcode=p.cmpcode and d.trnsjjualmstoid =p.trnsjjualmstoid inner join ql_trnorderdtl od on od.cmpcode=d.cmpcode and od.trnorderdtloid=d.trnorderdtloid inner join QL_mstgen g23 on g23.genoid = d.mtrlocoid and g23.gengroup = 'location' inner join QL_mstgen g3 on g23.genother1 = g3.genoid and g3.gengroup = 'warehouse' inner join ql_mstitem i on i.cmpcode=d.cmpcode and i.itemoid=d.refoid and d.refname = 'ql_mstitem' " & _
               "WHERE d.trnsjjualmstoid =p.trnsjjualmstoid AND d.cmpcode=p.cmpcode AND i.cmpcode=d.cmpcode AND i.itemoid=d.refoid AND d.trnsjjualmstoid =" & Session("trnsjjualmstoid") & " AND d.cmpcode='" & CompnyCode & "' AND od.trnordermstoid = (SELECT ordermstoid FROM QL_trnordermst WHERE orderno = '" & sjorderno.Text & "') and p.branch_code='" & Session("branch_id") & "' ORDER BY d.seq,d.trnsjjualdtloid"

        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnsjjurnal")
        Dim dtlTable As DataTable = New DataTable("JurnalPreview")
        Session("TblDtl") = objTable
        dtlTable.Columns.Add("code", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("desc", System.Type.GetType("System.String"))
        dtlTable.Columns.Add("debet", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("credit", System.Type.GetType("System.Double"))
        dtlTable.Columns.Add("id", System.Type.GetType("System.Int32"))
        dtlTable.Columns.Add("seq", System.Type.GetType("System.Int32")) ' utk seq bila 1 transaksi ada lebih 1 jurnal
        dtlTable.Columns.Add("seqdtl", System.Type.GetType("System.Int32")) ' utk seq bila 1 account bisa muncul dipakai berkali2
        Session("JurnalPreview") = dtlTable

        Dim iSeq As Integer = 1 : Dim iSeqDtl As Integer = 1
        Dim dtJurnal As DataTable : dtJurnal = Session("JurnalPreview")
        Dim nuRow As DataRow : Dim xBRanch As String = ""
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4 IN ('V','T')")
            Next
        End If

        Dim sVarAR As String = GetInterfaceValue("VAR_AR", xBRanch)
        If sVarAR = "?" Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_AR' !!", CompnyName & " - WARNING", 2)
            Session("click_post") = "false"
            Exit Sub
        Else
            Dim iAPOid As Integer = GetAccountOid(sVarAR, False)
            nuRow = dtJurnal.NewRow
            nuRow("code") = GetCodeAcctg(iAPOid)
            nuRow("desc") = GetDescAcctg(iAPOid) & " (PIUTANG USAHA)"
            nuRow("debet") = ToDouble(amtjualnetto)
            nuRow("credit") = 0 : nuRow("id") = iAPOid
            nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
            dtJurnal.Rows.Add(nuRow)
        End If
        iSeqDtl += 1

        sSql = "select branch_code from QL_trnordermst where orderno ='" & orderno.Text & "'"
        ' ============ POTONGAN PENJUALAN ============
        ' ============================================
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and jumlah diskon ======
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                Dim iItemAccount As String = GetInterfaceValue("VAR_DISC_JUAL", xBRanch)
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_DISC_JUAL' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If

                Dim dItemDiscdtl As Double = ToDouble(dtDtl.Rows(C1)("discperqty").ToString)
                Dim iAPdiscOid As Integer = GetAccountOid(iItemAccount, False)

                If dItemDiscdtl > 0 Then
                    If dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                        Dim oRow As DataRow = dtJurnal.Select("id=" & iAPdiscOid & " AND seqdtl=" & iSeqDtl)(0)
                        oRow.BeginEdit()
                        oRow("debet") += (dItemDiscdtl)
                        oRow.EndEdit()
                        dtJurnal.AcceptChanges()
                    Else
                        nuRow = dtJurnal.NewRow
                        nuRow("code") = GetCodeAcctg(iAPdiscOid)
                        nuRow("desc") = GetDescAcctg(iAPdiscOid) & " (POTONGAN PENJUALAN)"
                        nuRow("debet") = ToMaskEdit(dItemDiscdtl, 2)
                        nuRow("credit") = 0 : nuRow("id") = iAPdiscOid
                        nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                        dtJurnal.Rows.Add(nuRow)
                    End If
                End If
            Next
            iSeqDtl += 1
        End If

        ' ============ PENJUALAN ============
        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET itemacctgoid and HPP
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                'sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                'Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim iItemAccount As String = GetInterfaceValue("VAR_SALES", xBRanch)
                If iItemAccount = "?" Then
                    showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_SALES' !!", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                End If
                Dim iAPGudOid As Integer = GetAccountOid(iItemAccount, False)

                If dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iAPGudOid & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (ToDouble(dtDtl.Rows(C1)("trnorderprice") * cRate.GetRateMonthlyIDRValue)))
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iAPGudOid)
                    nuRow("desc") = GetDescAcctg(iAPGudOid) & " (PENJUALAN)"
                    nuRow("debet") = 0
                    nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * ToDouble(dtDtl.Rows(C1)("trnorderprice").ToString))
                    nuRow("id") = iAPGudOid
                    nuRow("seq") = iSeq
                    nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        sSql = "Select Isnull(ordertaxamt,0.00) from QL_trnordermst Where orderno='" & sjorderno.Text & "' And branch_code='" & Session("branch_id") & "' And cmpcode='" & CompnyCode & "'"
        Dim amtTax As Double = cKoneksi.ambilscalar(sSql)
        '============ PPN KELUARAN
        If ToDouble(amtTax) > 0 Then
            Dim sPPNK As String = GetInterfaceValue("VAR_PPN_JUAL", xBRanch)
            If sPPNK = "?" Then
                showMessage("Please Setup Interface for PPN Keluaran on Variable 'VAR_PPN_JUAL'", CompnyName & " - WARNING", 2)
                Session("click_post") = "false"
                Exit Sub
            Else
                Dim iPPNKOid As Integer = GetAccountOid(sPPNK, False)
                nuRow = dtJurnal.NewRow
                nuRow("code") = GetCodeAcctg(iPPNKOid)
                nuRow("desc") = GetDescAcctg(iPPNKOid) & " (PPN KELUARAN)"
                nuRow("debet") = 0
                nuRow("credit") = (ToDouble(amtTax))
                nuRow("id") = iPPNKOid : nuRow("seq") = iSeq
                nuRow("seqdtl") = iSeqDtl
                dtJurnal.Rows.Add(nuRow)
            End If
            iSeqDtl += 1
        End If
        iSeq += 1
        ' ============ 1 JURNAL
        ' BEBAN POKOK PENJUALAN
        ' PERSEDIAAN

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET HPPValue and HPPAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarHPP As String = ""

                sVarHPP = GetStrData("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_HPP'")
                If sVarHPP = "?" Then
                    showMessage("Please Setup Interface for HPP on Variable 'VAR_HPP'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarHPP, True)
                End If

                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("debet") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (BEBAN POKOK PENJUALAN(HPP))"
                    nuRow("debet") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (dItemHPP / 1))
                    nuRow("credit") = 0 : nuRow("id") = iItemAccount
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If

        If Not IsNothing(Session("TblDtl")) Then
            Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
            ' ====== GET PersediaanValue and PersediaanAccount
            For C1 As Integer = 0 To dtDtl.Rows.Count - 1
                Dim iItemOid As Integer = dtDtl.Rows(C1)("itemoid").ToString
                sSql = "SELECT acctgoid FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim iItemAccount As Integer = cKoneksi.ambilscalar(sSql)
                Dim sAccountDesc As String = GetDescAcctg(iItemAccount)
                Dim sVarStock As String = ""

                sVarStock = GetInterfaceValue("VAR_GUDANG", xBRanch)
                If sVarStock = "?" Then
                    showMessage("Please Setup Interface for Stock on Variable 'VAR_GUDANG'", CompnyName & " - WARNING", 2)
                    Session("click_post") = "false"
                    Exit Sub
                Else
                    iItemAccount = GetAccountOid(sVarStock, False)
                End If
                'xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dtDtl.Rows(C1)("itemloc").ToString & " AND genother4='" & sTockFlag & "'")
                sSql = "SELECT hpp FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND itemoid=" & iItemOid
                Dim dItemHPP As Double = ToDouble(cKoneksi.ambilscalar(sSql))

                If dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl).Length > 0 Then
                    Dim oRow As DataRow = dtJurnal.Select("id=" & iItemAccount & " AND seqdtl=" & iSeqDtl)(0)
                    oRow.BeginEdit()
                    oRow("credit") += (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * dItemHPP)
                    oRow.EndEdit()
                    dtJurnal.AcceptChanges()
                Else
                    nuRow = dtJurnal.NewRow
                    nuRow("code") = GetCodeAcctg(iItemAccount)
                    nuRow("desc") = GetDescAcctg(iItemAccount) & " (PERSEDIAAN)"
                    nuRow("credit") = (ToDouble(dtDtl.Rows(C1)("salesdeliveryqty").ToString) * (dItemHPP / 1))
                    nuRow("id") = iItemAccount : nuRow("debet") = 0
                    nuRow("seq") = iSeq : nuRow("seqdtl") = iSeqDtl
                    dtJurnal.Rows.Add(nuRow)
                End If
            Next
            iSeqDtl += 1
        End If
        gvPreview.DataSource = dtJurnal : gvPreview.DataBind()
        Session("JurnalPreview") = dtJurnal
        cProc.SetModalPopUpExtender(beHidePreview, pnlPreview, mpePreview, True)
    End Sub

    Protected Sub imgBackAS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setASasActive()
    End Sub

    Protected Sub imgRejectAS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("draftno")) Then
            showMessage("Please select a Adjustment Stock First", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("draftno") = "" Then
                showMessage("Please select a Adjustment Stock First", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If draftno.Text = "" Then
            showMessage("Please select a Adjustment Stock First", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            sSql = "UPDATE ql_trnstockadj SET stockadjstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND resfield1 = '" & Session("draftno") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='ql_trnstockadj' and oid=" & Session("draftno") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        setASasActive()
    End Sub

    Protected Sub lkSelectReal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("realOid") = sender.ToolTip : MultiView1.SetActiveView(View33)
        Session("approvaloid") = sender.CommandArgument
        btnAddToList.Visible = False ': btnClear.Visible = False
        sSql = "Select po.branch_code,r.cmpcode,realisasioid,realisasino,CONVERT(Char(10),realisasidate,103) realisasidate,realisasitype,po.trnbelimstoid,po.trnbelipono trnbelino,Realisasinote,ap.APPROVALOID,(Select suppname From QL_mstsupp sp Where sp.suppoid=po.trnsuppoid) suppname,realisasiamt,acctgoidreal,amtvoucherpi,amtsisa FROM QL_trnrealisasi r INNER JOIN QL_pomst po ON r.pomstoid=po.trnbelimstoid INNER JOIN QL_APPROVAL ap ON ap.OID=r.realisasioid Where realisasistatus='IN APPROVAL' and ap.EVENT='In Approval' and r.cmpcode ='" & CompnyCode & "' and ap.APPROVALUSER='admin' And ap.TABLENAME='QL_trnrealisasi' and ap.STATUSREQUEST='new' And realisasioid=" & Session("realOid") & ""
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_RealTbl")
        RealisasiOid.Text = dt.Rows(0).Item("realisasioid")
        CodeCb.Text = dt.Rows(0).Item("branch_code").ToString
        OidPO.Text = dt.Rows(0).Item("trnbelimstoid")
        RealNo.Text = dt.Rows(0).Item("realisasino").ToString
        RealDate.Text = dt.Rows(0).Item("realisasidate")
        'payflag.SelectedValue = dt.Rows(0).Item("realisasitype").ToString
        NoPi.Text = dt.Rows(0).Item("trnbelino").ToString
        SuppReal.Text = dt.Rows(0).Item("suppname").ToString
        AmtReal.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("amtvoucherpi")), 3)
        acctgoidreal.Text = dt.Rows(0).Item("acctgoidreal")
        LblTampung.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("realisasiamt")), 3)
        amtsisa.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("amtsisa")), 3)
        ApprovalOid.Text = dt.Rows(0).Item("APPROVALOID")

        sSql = "Select rd.seqdtl, rd.voucheroid, vd.voucherdesc, rd.amtvoucherdtl, rd.amtrealdtl, rd.amtsisadtl, rd.notedtl, rd.amtdtltemp, rd.amtdtltempsisa From QL_trnrealisasidtl rd INNER JOIN QL_voucherdtl vd ON vd.voucheroid=rd.voucheroid Where rd.trnrealisasioid=" & Session("realOid") & ""
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "detailtw")
        GVvOucherDtl.DataSource = dtab
        GVvOucherDtl.DataBind()
        Session("QL_trnrealisasidtl") = dtab
    End Sub

    Private Sub setRealActive()
        RealVoucher() : MultiView1.SetActiveView(View32)
    End Sub

    Private Sub RealVoucher()
        sSql = "Select po.branch_code,r.cmpcode,realisasioid,realisasino,CONVERT(Char(10),realisasidate,103) realisasidate,realisasitype,po.trnbelimstoid,po.trnbelipono trnbelino,ap.APPROVALOID,Realisasinote FROM QL_trnrealisasi r INNER JOIN QL_pomst po ON r.pomstoid=po.trnbelimstoid INNER JOIN QL_APPROVAL ap ON ap.OID=r.realisasioid Where realisasistatus='IN APPROVAL' and ap.EVENT='In Approval' and r.cmpcode ='" & CompnyCode & "' and ap.APPROVALUSER='admin' And ap.TABLENAME='QL_trnrealisasi' and ap.STATUSREQUEST='new' ORDER BY realisasioid DESC  "
        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnrealisasi")
        Session("tbldata") = objTable : RealGV.DataSource = objTable
        RealGV.DataBind()
    End Sub

    Protected Sub RealTomb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RealTomb.Click
        setRealActive()
    End Sub

    Protected Sub ApRealBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim dtotal As Double
        Dim cRate As New ClassRate()
        Dim vCMst As Integer = GenerateID("QL_trncashbankmst", CompnyCode)
        Dim vIDgl As Integer = GenerateID("QL_cashbankgl", CompnyCode)
        Dim vIDMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim vIDDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
        'Dim amtdt As Double
        Dim Closing As String = "" : Dim realisasi As String = ""
        Dim gantungPiutang As String = "" : Dim dAcctg As String = ""
        Dim cAcctg As String = "" : Dim varselisih As String = ""
        Dim varCash As String = "" : Dim varBank As String = ""
        Dim RealAcctgOid, vAcctoid, CbAcctgOId, AcctgLabaOid As Integer

        If cKon.ambilscalar("select COUNT(trnbelimstoid) from ql_trnrealisasi where trnbelimstoid = " & OidPO.Text & "") > 0 Then
            sSql = "SELECT bm.trnbelimstoid,trnbelidtloid,trnbelipono,selisih amtdisc2idr,ISNULL((bd.amtdisc2idr-realisasiamt),0.00) AS amtrealisasi,ISNULL(realisasiamt,0.00) As AmtMurni,selisih FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelino='" & NoPi.Text.Trim & "' ORDER BY trnbelidtloid ASC"
        Else
            sSql = "SELECT bm.trnbelimstoid,trnbelidtloid,trnbelipono,Case When trnbelidtlres1 ='In Process' then bd.amtdisc2idr else ISNULL(bd.amtdisc2idr - realisasiamt,0.00) end amtdisc2idr,ISNULL((bd.amtdisc2idr-realisasiamt),0.00) AS amtrealisasi,ISNULL(realisasiamt,0.00) As AmtMurni,selisih FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelino='" & NoPi.Text.Trim & "' ORDER BY trnbelidtloid ASC"
        End If

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnbelidtl")
        If objTbl.Rows.Count = 0 Then
        Else
            'If Session("oid") = Nothing Or Session("oid") = "" Then
            '    showMessage("Tidak Ada Detil PI <br>", CompnyName & " - WARNING", 2)
            '    Exit Sub
            'End If
        End If

        sSql = "SELECT realisasiamt FROM QL_trnbelidtl dtl INNER JOIN QL_trnbelimst mst ON mst.trnbelimstoid = dtl.trnbelimstoid WHERE mst.trnbelino='" & NoPi.Text & "'"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "realisasiamt")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = " realisasiamt > 0"
        If dv.Count > 0 Then
            For C2 As Integer = 0 To dv.Count - 1
                dtotal += dtotal + dv.Item(C2).Item("realisasiamt").ToString
            Next
        End If
        dv.RowFilter = ""

        'AMBIL VAR VAR_REALISASI_VOUCHER
        realisasi = GetVarInterface("VAR_REALISASI_VOUCHER", CodeCb.Text)
        If realisasi = "0" Or realisasi Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_REALISASI_VOUCHER'", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        RealAcctgOid = GetAccountOid(realisasi, False)
        'AMBIL VAR VAR_VOUCHER
        gantungPiutang = GetVarInterface("VAR_VOUCHER", CodeCb.Text)
        If gantungPiutang = "0" Or gantungPiutang Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_VOUCHER'", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        vAcctoid = GetAccountOid(gantungPiutang, False)

        'AMBIL VAR VAR_PENDAPATAN_VOUCHER UNTUK COA PENAPATAN LAIN-LAIN
        varselisih = GetVarInterface("VAR_PENDAPATAN_VOUCHER", CodeCb.Text)
        If varselisih = "0" Or varselisih Is Nothing Then
            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_PENDAPATAN_VOUCHER'", CompnyName & " - WARNING", 2)
            Exit Sub
        End If
        AcctgLabaOid = GetAccountOid(varselisih, False)
        '----------KONDISI TYPE REALISASI---------
        '-----------------------------------------
        If payflag.SelectedValue = "CASH" Then
            'AMBIL VAR_CASH
            varCash = GetVarInterface("VAR_CASH", CodeCb.Text)
            If varCash = "0" Or varCash Is Nothing Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_CASH'", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            CbAcctgOId = DdlAkun.SelectedValue
        ElseIf payflag.SelectedValue = "BANK" Then
            varBank = GetVarInterface("VAR_BANK", CodeCb.Text)
            If varBank = "0" Or varBank Is Nothing Then
                showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_BANK'", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
            CbAcctgOId = DdlAkun.SelectedValue
         Else
            CbAcctgOId = 0
        End If

        If payflag.SelectedValue = "CLOSED" Then
            dAcctg = AcctgLabaOid : cAcctg = vAcctoid
        ElseIf payflag.SelectedValue = "CASH" Then
            dAcctg = CbAcctgOId : cAcctg = vAcctoid
            GenerateCBNo()
        ElseIf payflag.SelectedValue = "BANK" Then
            dAcctg = CbAcctgOId : cAcctg = vAcctoid
            GenerateCBNo()
        Else
            dAcctg = RealAcctgOid : cAcctg = vAcctoid
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            '===== UPdate tabel beli detail ======
            Session("TblDtl") = objTbl
            Dim Tb As DataView = objTbl.DefaultView
            Dim amts, cekamt, sisarealisasi, selisihrealisasi As Double
            Dim realselisih As Double = 0.0 : Dim StatusPI As String = ""

            For c1 As Int16 = 0 To objTbl.Rows.Count - 1
                sisarealisasi = ToDouble(AmtReal.Text) - ToDouble(dtotal)
                If sisarealisasi > 0 Then
                    cekamt = ToDouble(sisarealisasi) - (ToDouble(objTbl.Rows(c1).Item("amtdisc2idr")))
                    If cekamt >= 0 Then
                        amts = ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                    Else
                        amts = sisarealisasi
                        'If i_u2.Text = "New Data" Then
                        '    If ToDouble(objTbl.Rows(c1).Item("AmtMurni")) > 0 Then
                        '        amts = ToDouble(amtreal.Text) + ToDouble(objTbl.Rows(c1).Item("AmtMurni"))
                        '    Else
                        '        amts = ToDouble(amtreal.Text) - mTotal
                        '    End If
                        'Else
                        '    If ToDouble(objTbl.Rows(c1).Item("amtrealisasi")) = 0 Then
                        '        amts = ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                        '    Else
                        '        amts = ToDouble(amtreal.Text) - mTotal
                        '    End If
                        'End If
                    End If

                    dtotal += ToDouble(objTbl.Rows(c1).Item("amtdisc2idr"))
                    Session("dtotal") = dtotal

                    'sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = '" & lblPOST.Text.Trim & "',amtreal =amtreal + " & amts & ",realisasioid=" & RealisasiOid.Text & ",selisih = " & ToDouble(objTbl.Rows(c1).Item("amtdisc2idr")) - amts & "  WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "'"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    'sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = 'IN PROCESS',amtreal =0 ,realisasioid=0 WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "'"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            'sSql = "SELECT bm.trnbelimstoid FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-amtreal),0.00)) <> 0 and amtreal <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : mstoid = xCmd.ExecuteScalar()

            'sSql = "SELECT trnbelidtloid FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-amtreal),0.00)) <> 0 and amtreal <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : dtloid = xCmd.ExecuteScalar()

            'sSql = "SELECT ISNULL((bd.amtdisc2idr-amtreal),0.00) AS amtrealisasi FROM QL_trnbelidtl bd INNER JOIN QL_trnbelimst bm ON bm.trnbelimstoid=bd.trnbelimstoid AND bm.branch_code=bd.branch_code WHERE amtdisc2idr <> 0 AND ISNULL(bd.trnbelidtlres1,'') NOT IN ('CLOSED','POST') AND trnbelipono='" & Tchar(NoPoTxt.Text.Trim) & "' AND (ISNULL((bd.amtdisc2idr-amtreal),0.00)) <> 0 and amtreal <> 0 ORDER BY trnbelidtloid ASC"
            'xCmd.CommandText = sSql : realselisih = xCmd.ExecuteScalar()

            'sSql = "UPDATE QL_trnbelidtl SET selisih=" & ToDouble(realselisih) & " WHERE trnbelimstoid=" & mstoid & " AND trnbelidtloid=" & dtloid & " And branch_code='" & Session("branch_id") & "'"

            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'KELEBIHAN AMOUNT REALISASI TERHADAP PIUTANG GANTUNG
            If selisihrealisasi = ToDouble(AmtReal.Text) - ToDouble(LblTampung.Text) < 0 Then
                selisihrealisasi = 0
            End If
            sSql = "UPDATE QL_trnrealisasi Set realisasidate='" & CDate(toDate(RealDate.Text)) & "', realisasistatus='APPROVED', realisasiamt=" & ToDouble(LblTampung.Text) & ", pomstoid=" & OidPO.Text & ", upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, realisasitype='" & payflag.SelectedValue & "' , Realisasinote='" & Tchar(cashbanknote.Text.Trim) & "',selisihamt=" & ToDouble(selisihrealisasi) & ",amtsisa=" & ToDouble(amtsisa.Text) & " Where realisasioid=" & RealisasiOid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

            sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & ApprovalOid.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '---Proses Insert Detail---
            '--------------------------
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("QL_trnrealisasidtl")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

            If payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "BANK" Then
                'INSERT CASHBANKMST 
                Dim bType As String = ""
                If payflag.SelectedValue = "CASH" Then
                    bType = "BKM" : Else : bType = "BBM"
                End If

                sSql = "INSERT into QL_trncashbankmst(cmpcode,branch_code,cashbankoid,cashbankno,cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote,createuser,createtime,upduser,updtime,pic,PIC_REFNAME,link_nobukti,status_giro,girodtloid,giroref,cashbankduedate,cashbankrefno,cashbankamount,cashbankamountidr,cashbankamountusd,cashbankcurroid,cashbankcurrate) VALUES" & _
                " ('" & CompnyCode & "','" & Session("branch_id") & "'," & vCMst & ",'" & Session("sNo") & "','POST','" & bType & "','VOUCHER'," & CbAcctgOId & ",'" & CDate(toDate(RealDate.Text)) & "','" & Tchar(RealNote.Text) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'0','','','','0','','" & CDate(toDate(RealDate.Text)) & "'," & IIf(LblTampung.Visible, "'" & Tchar(LblTampung.Text) & "'", "1/1/1900") & ",'" & ToDouble(LblTampung.Text) & "','" & ToDouble(LblTampung.Text) & "','" & ToDouble(LblTampung.Text) * cRate.GetRateMonthlyUSDValue & "','1','" & cRate.GetRateMonthlyIDRValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & vCMst & " WHERE tablename='QL_trncashbankmst' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '====INSERT CASHBANKGL====
                '-------------------------
                For C1 As Int16 = 0 To objRow.Length - 1
                    sSql = "INSERT into QL_cashbankgl(cmpcode,branch_code,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote, cashbankglstatus, cashbankglres1, duedate, refno, createuser, createtime, upduser, updtime) VALUES " & _
                    " ('" & CompnyCode & "','" & Session("branch_id") & "'," & vIDgl & "," & vCMst & "," & cAcctg & "," & ToDouble(objRow(C1)("amtrealdtl")) & "," & ToDouble(objRow(C1)("amtrealdtl")) & "," & ToDouble(objRow(C1)("amtrealdtl") * cRate.GetRateMonthlyUSDValue) & ",'" & Tchar(RealNote.Text) & "','POST','" & RealisasiOid.Text & "', '" & CDate(toDate(RealDate.Text)) & "','" & RealNo.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDgl += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & vIDgl + 1 & " WHERE tablename='QL_cashbankgl' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '-- Insert Detail Realisasi --
            '-----------------------------
            If Not Session("QL_trnrealisasidtl") Is Nothing Then
                sSql = "Delete QL_trnrealisasidtl Where trnrealisasioid=" & RealisasiOid.Text & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Session("vIDCost") = GenerateID("QL_trnrealisasidtl", CompnyCode)
                Dim dateStr As String = "'" & toDate(RealDate.Text) & "'"

                For C1 As Int16 = 0 To objRow.Length - 1
                    sSql = "INSERT INTO QL_trnrealisasidtl (cmpcode,trnrealisasidtloid,trnrealisasioid,seqdtl,voucheroid,amtrealdtl,amtvoucherdtl,amtsisadtl,dtlstatus,updtime,upduser,notedtl,amtdtltemp,amtdtltempsisa)" & _
                    " VALUES ('" & CompnyCode & "'," & Session("vIDCost") & "," & RealisasiOid.Text & ",'" & Tchar(objRow(C1)("seqdtl")) & "'," & objRow(C1)("voucheroid") & "," & ToDouble(objRow(C1)("amtrealdtl")) & "," & ToDouble(objRow(C1)("amtvoucherdtl")) & "," & ToDouble(objRow(C1)("amtsisadtl")) & ",'POST',current_timestamp,'" & Session("UserID") & "','" & Tchar(objRow(C1)("notedtl")) & "'," & ToDouble(objRow(C1)("amtdtltemp")) & "," & ToDouble(objRow(C1)("amtdtltempsisa")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & (Session("vIDCost") + objRow.Length) & " WHERE tablename='QL_trnrealisasidtl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'For c1 As Int16 = 0 To objTbl.Rows.Count - 1
            '    If ToDouble(objTbl.Rows(c1).Item("amtrealisasi")) <> 0 Then
            '        StatusPI = "IN PROCESS"
            '    Else
            '        StatusPI = "POST"
            '    End If

            '    sSql = "UPDATE QL_trnbelidtl SET trnbelidtlres1 = '" & StatusPI & "' WHERE trnbelimstoid=" & objTbl.Rows(c1).Item("trnbelimstoid") & " AND trnbelidtloid=" & objTbl.Rows(c1).Item("trnbelidtloid") & " And branch_code='" & Session("branch_id") & "'"
            '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'Next

            '----- Kondisi input auto jurnal jika type realisasi CLOSED -----
            Dim iSeq As Integer = 1
            Dim gldbcr1, gldbcr2 As String : Dim RealDtlAmt As Double
            sSql = "UPDATE QL_trnbelimst SET flagvoucher='POST' WHERE trnbelimstoid=" & OidPO.Text & " AND branch_code='" & CodeCb.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '//////INSERT INTO TRN GLMST
            sSql = "INSERT into QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,type) " & _
            "VALUES ('" & CompnyCode & "','" & Session("branch_id") & "', '" & vIDMst & "','" & CDate(toDate(RealDate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(RealDate.Text))) & "','- Realisasi " & RealNo.Text & "','POST','" & GetServerTime().Date & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UseID") & "',CURRENT_TIMESTAMP,'RECEIPT') "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


            For C1 As Int16 = 0 To objRow.Length - 1

                If ToDouble(objRow(C1)("amtsisadtl")) <= 0 Then
                    StatusPI = "CLOSED" : gldbcr1 = "C" : gldbcr2 = "D"
                    sSql = "Update QL_voucherdtl Set voucherstatus='" & StatusPI & "' Where voucheroid=" & objRow(C1)("voucheroid") & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    RealDtlAmt = ToDouble(objRow(C1)("amtvoucherdtl"))
                Else
                    StatusPI = "IN PROCESS" : gldbcr1 = "D" : gldbcr2 = "C"
                    RealDtlAmt = ToDouble(objRow(C1)("amtrealdtl"))
                End If
                'LblTampung.Text += ToDouble(objRow(C1)("amtrealdtl"))
                'amtsisa.Text += ToDouble(objRow(C1)("amtsisadtl"))

                sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother2,glpostdate,upduser,updtime) VALUES" & _
                            " ('" & CompnyCode & "','" & Session("branch_id") & "','" & vIDDtl & "'," & objRow(C1)("seqdtl") & ",'" & vIDMst & "'," & dAcctg & ",'D','" & ToDouble(objRow(C1)("amtrealdtl")) & "','" & ToDouble(objRow(C1)("amtrealdtl")) & "','" & ToDouble(objRow(C1)("amtrealdtl")) * cRate.GetRateMonthlyUSDValue & "','" & RealNo.Text & "','(" & Tchar(objRow(C1)("voucherdesc")) & ") " & Tchar(Tchar(objRow(C1)("notedtl"))) & "','" & objRow(C1)("voucheroid") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                vIDDtl += 1 : iSeq += 1

                sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother2,glpostdate,upduser,updtime) VALUES " & _
                "('" & CompnyCode & "','" & Session("branch_id") & "','" & vIDDtl & "'," & objRow(C1)("seqdtl") & ",'" & vIDMst & "'," & cAcctg & ",'C','" & ToDouble(RealDtlAmt) & "','" & ToDouble(RealDtlAmt) & "','" & ToDouble(RealDtlAmt) * cRate.GetRateMonthlyUSDValue & "','" & RealNo.Text & "','(" & Tchar(objRow(C1)("voucherdesc")) & ") " & Tchar(Tchar(objRow(C1)("notedtl"))) & "','" & objRow(C1)("voucheroid") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                vIDDtl += 1 : iSeq += 1

                If ToDouble(objRow(C1)("amtsisadtl")) < 0.0 Then
                    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother2,glpostdate,upduser,updtime) VALUES " & _
                    "('" & CompnyCode & "','" & Session("branch_id") & "','" & vIDDtl & "',1,'" & vIDMst & "'," & AcctgLabaOid & ",'" & gldbcr1 & "','" & ToDouble(objRow(C1)("amtsisadtl")) * -1 & "','" & ToDouble(objRow(C1)("amtsisadtl")) * -1 & "','" & (ToDouble(objRow(C1)("amtsisadtl")) * -1) * cRate.GetRateMonthlyUSDValue & "','" & RealNo.Text & "','(" & Tchar(objRow(C1)("voucherdesc")) & ") " & Tchar(Tchar(objRow(C1)("notedtl"))) & "','" & objRow(C1)("voucheroid") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    vIDDtl += 1 : iSeq += 1

                    'sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother2,glpostdate,upduser,updtime) VALUES " & _
                    '"('" & CompnyCode & "','" & Session("branch_id") & "','" & vIDDtl & "',2,'" & vIDMst & "'," & vAcctoid & ",'" & gldbcr2 & "','" & ToDouble(objRow(C1)("amtsisadtl")) * -1 & "','" & ToDouble(objRow(C1)("amtsisadtl")) * -1 & "','" & (ToDouble(objRow(C1)("amtsisadtl")) * -1) * cRate.GetRateMonthlyUSDValue & "','" & RealNo.Text & "','(" & Tchar(objRow(C1)("voucherdesc")) & ") " & Tchar(Tchar(objRow(C1)("notedtl"))) & "','" & objRow(C1)("voucheroid") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP)"
                    'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'vIDDtl += 1 : iSeq += 1
                End If
            Next
            'insert data detail
            
            sSql = "UPDATE QL_mstoid SET lastoid=" & vIDDtl & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trngldtl'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_mstoid SET lastoid=" & vIDMst & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnglmst'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setRealActive()
    End Sub

    Protected Sub ImageButton9_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setRealActive()
    End Sub

    Protected Sub RejectBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        If IsDBNull(Session("realOid")) Then
            showMessage("Pilih Realisasi Data yang akan direject!", CompnyName & " - Warning", 2)
            Exit Sub
        Else
            If Session("realOid") = "" Then
                showMessage("Pilih Realisasi Data yang akan direject!", CompnyName & " - Warning", 2)
                Exit Sub
            End If
        End If
        If RealNo.Text = "" Then
            showMessage("Pilih Realisasi Data yang akan direject!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "select realisasistatus from QL_trnrealisasi where realisasioid = " & Integer.Parse(Session("realOid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Realisasi tidak bisa direject karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Maaf, Realisasi Voucher tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trnrealisasi SET realisasistatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND realisasioid = " & Integer.Parse(Session("realOid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & ApprovalOid.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setRealActive()
    End Sub

    Protected Sub GVvOucherDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub

    Protected Sub GVvOucherDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        I_Udtl.Text = "Edit"
        btnAddToList.Visible = True ': btnClear.Visible = True
        VoucherOid.Text = GVvOucherDtl.SelectedDataKey("voucheroid")
        txtVoucher.Text = GVvOucherDtl.SelectedDataKey("voucherdesc").ToString
        AmtVdtl.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtrealdtl")), 3)
        vDtlAmt.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtvoucherdtl")), 3)
        AmtSisaDtl.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtsisadtl")), 3)
        cashbanknote.Text = GVvOucherDtl.SelectedDataKey("notedtl").ToString
        labelseq.Text = GVvOucherDtl.SelectedDataKey("seqdtl").ToString
        amtdtltemp.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtdtltemp")), 3)
        amtdtltempsisa.Text = ToMaskEdit(ToDouble(GVvOucherDtl.SelectedDataKey("amtdtltempsisa")), 3)
        Dim transdate As Date = Date.ParseExact(RealDate.Text, "dd/MM/yyyy", Nothing)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        '--Create Table Detail--
        '-----------------------
        btnAddToList.Visible = False ': btnClear.Visible = False
        Dim dtab As DataTable
        If I_Udtl.Text = "Data Detail" Then
            If Session("QL_trnrealisasidtl") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seqdtl", System.Type.GetType("System.Int32"))
                dtab.Columns.Add("voucheroid", System.Type.GetType("System.Int32"))
                dtab.Columns.Add("voucherdesc", System.Type.GetType("System.String"))
                dtab.Columns.Add("amtrealdtl", System.Type.GetType("System.Double"))
                dtab.Columns.Add("amtvoucherdtl", System.Type.GetType("System.Double"))
                dtab.Columns.Add("amtsisadtl", System.Type.GetType("System.Double"))
                dtab.Columns.Add("amtdtltemp", System.Type.GetType("System.Double"))
                dtab.Columns.Add("amtdtltempsisa", System.Type.GetType("System.Double"))
                dtab.Columns.Add("notedtl", System.Type.GetType("System.String"))
                Session("QL_trnrealisasidtl") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("QL_trnrealisasidtl")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("QL_trnrealisasidtl")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("voucheroid = " & Integer.Parse(VoucherOid.Text) & " AND seqdtl <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Maaf, Voucher tidak bisa ditambahkan ke dalam list ! Voucher ini Sudah ada di dalam list !", CompnyName & " - WARNING", 2)
                 Exit Sub
            End If
        End If
        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_Udtl.Text = "New Detail" Then
            drow = dtab.NewRow
            drow("seqdtl") = Integer.Parse(labelseq.Text)
            drow("voucheroid") = Integer.Parse(VoucherOid.Text)
            drow("voucherdesc") = txtVoucher.Text.Trim
            drow("amtrealdtl") = ToDouble(AmtVdtl.Text)
            drow("amtdtltemp") = ToDouble(amtdtltemp.Text)
            drow("amtvoucherdtl") = ToDouble(vDtlAmt.Text)
            If ToDouble(drow("amtrealdtl")) > drow("amtvoucherdtl") Then
                AmtSisaDtl.Text = ToDouble(drow("amtrealdtl")) - drow("amtvoucherdtl")
            ElseIf ToDouble(drow("amtrealdtl")) < drow("amtvoucherdtl") Then
                AmtSisaDtl.Text = drow("amtvoucherdtl") - ToDouble(drow("amtrealdtl"))
            Else
                AmtSisaDtl.Text = 0
            End If
            drow("amtsisadtl") = ToDouble(AmtSisaDtl.Text)
            drow("amtdtltempsisa") = ToDouble(amtdtltempsisa.Text)
            drow("notedtl") = cashbanknote.Text.Trim
            dtab.Rows.Add(drow) : dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seqdtl = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0) : drowedit(0).BeginEdit()

            drow("voucheroid") = Integer.Parse(VoucherOid.Text)
            drow("voucherdesc") = txtVoucher.Text.Trim
            drow("amtrealdtl") = ToDouble(AmtVdtl.Text)
            drow("amtdtltemp") = ToDouble(amtdtltemp.Text)
            drow("amtvoucherdtl") = ToDouble(vDtlAmt.Text)
            If ToDouble(drow("amtrealdtl")) > drow("amtvoucherdtl") Then
                AmtSisaDtl.Text = ToDouble(drow("amtrealdtl")) - drow("amtvoucherdtl")
            ElseIf ToDouble(drow("amtrealdtl")) < drow("amtvoucherdtl") Then
                AmtSisaDtl.Text = drow("amtvoucherdtl") - ToDouble(drow("amtrealdtl"))
            Else
                AmtSisaDtl.Text = 0
            End If
            drow("amtsisadtl") = ToDouble(AmtSisaDtl.Text)
            drow("amtdtltempsisa") = ToDouble(amtdtltempsisa.Text)
            drow("notedtl") = cashbanknote.Text
            drowedit(0).EndEdit() : dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVvOucherDtl.DataSource = dtab : GVvOucherDtl.DataBind()
        Session("QL_trnrealisasidtl") = dtab

        labelseq.Text = (GVvOucherDtl.Rows.Count + 1).ToString
        txtVoucher.Text = "" : AmtVdtl.Text = "0.00"
        vDtlAmt.Text = "0.00" : cashbanknote.Text = ""
        VoucherOid.Text = "" : AmtSisaDtl.Text = "0.00"
        amtdtltempsisa.Text = "0.00" : amtdtltemp.Text = "0.00"
        GVvOucherDtl.SelectedIndex = -1 ' GVvOucherDtl.Columns(7).Visible = True
        GVvOucherDtl.DataSource = Session("QL_trnrealisasidtl")
        GVvOucherDtl.DataBind()
        HitungDtlAmt() : GVvOucherDtl.Visible = True
    End Sub

    Protected Sub AmtVdtl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        AmtVdtl.Text = ToMaskEdit(AmtVdtl.Text, 3)
        HitungVoucher()
    End Sub

    Protected Sub ClearDtlVoucher()
        txtVoucher.Text = "" : VoucherOid.Text = ""
        cashbanknote.Text = "" : AmtVdtl.Text = "0.00"
        RealNo.Text = "" : AmtSisaDtl.Text = "0.00" : vDtlAmt.Text = "0.00"
        I_Udtl.Text = "Data Detail" : labelseq.Text = "1"
        LblTampung.Text = "0.00" : amtsisa.Text = "0.00"
    End Sub

    Private Sub HitungVoucher()
        '--Hitung Sisa Amount Header
        amtsisa.Text = ToMaskEdit(AmtReal.Text - LblTampung.Text, 3)
        If ToDouble(LblTampung.Text) < ToDouble(AmtReal.Text) Then
            amtsisa.Text = ToMaskEdit(AmtReal.Text - LblTampung.Text, 3)
        ElseIf ToDouble(LblTampung.Text) > ToDouble(AmtReal.Text) Then
            amtsisa.Text = ToMaskEdit(LblTampung.Text - AmtReal.Text, 3)
        End If
        '--Hitung Sisa Amount Detail
        If ToDouble(AmtVdtl.Text) > ToDouble(vDtlAmt.Text) Then
            AmtSisaDtl.Text = ToMaskEdit(ToDouble(AmtVdtl.Text) - ToDouble(vDtlAmt.Text), 3)
        Else
            AmtSisaDtl.Text = ToMaskEdit(ToDouble(vDtlAmt.Text) - ToDouble(AmtVdtl.Text), 3)
        End If
    End Sub

    Protected Sub HitungDtlAmt()
        Dim iCountrow As Integer = GVvOucherDtl.Rows.Count
        Dim iGtotal As Double = 0 : Dim SisaTotal As Double = 0
        Dim dt_temp As New DataTable : dt_temp = Session("QL_trnrealisasidtl")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVvOucherDtl.Rows(i).Cells(4).Text)
            SisaTotal += ToDouble(GVvOucherDtl.Rows(i).Cells(6).Text)
            dt_temp.Rows(i).BeginEdit()
            dt_temp.Rows(i).Item(0) = i + 1
            dt_temp.Rows(i).EndEdit()
        Next
        Session("QL_trnrealisasidtl") = dt_temp
        GVvOucherDtl.DataSource = dt_temp
        GVvOucherDtl.DataBind()
        lblTampung.Text = ToMaskEdit(ToDouble(iGtotal), 3)
        amtsisa.Text = ToMaskEdit(ToDouble(SisaTotal), 3)
        HitungVoucher()
    End Sub

    Protected Sub imbRejectPDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRejectPDO.Click
        If IsDBNull(Session("trnsjbelimstoid")) Then
            showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trnsjbelimstoid") = "" Then
                showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        If trnsjbelino.Text = "" Then
            showMessage("Please select a Purchase Delivery Order first", CompnyName & " - Warning", 2) : Exit Sub
        End If

        sSql = "SELECT id_sn FROM QL_Mst_SN WHERE last_trans_type = 'Beli' AND trnbelimstoid = '" & Session("trnsjbelimstoid") & "'"
        Dim dtSNReject As DataTable = cKon.ambiltabel(sSql, "dtSNHapus")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PDO
            sSql = "UPDATE ql_trnsjbelimst SET trnsjbelistatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnsjbelioid = '" & Session("trnsjbelimstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For dtsn As Integer = 0 To dtSNReject.Rows.Count - 1
                sSql = "Delete from QL_mstitemDtl where sn = '" & dtSNReject.Rows(dtsn).Item("id_sn") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Delete from QL_Mst_SN where id_sn = '" & dtSNReject.Rows(dtsn).Item("id_sn") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, approvalstatus='Ignore',event='Ignore',statusrequest='End' WHERE cmpcode='" & CompnyCode & "' and tablename='ql_trnsjbelimst' and oid=" & Session("trnsjbelimstoid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setPDOasActive()
    End Sub 

    Protected Sub btnCanRevisePO_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("WaitingAction.aspx")
    End Sub
 
    Protected Sub LkbSalesReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbSalesReturn.Click
        setSalesReturnActive()
    End Sub

    Private Sub setSalesReturnActive()
        bindSalesReturn() : trnsjjualno.Text = "" : trnsjjualsenddate.Text = ""
        sjcustname.Text = "" : sjorderno.Text = "" : sjidentifierno.Text = ""
        Session("approvaloid") = Nothing : Session("trnsjjualmstoid") = Nothing
        MultiView1.SetActiveView(View34)
    End Sub

    Private Sub bindSalesReturn()
        Dim QlSR As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnjualreturmst' And approvaluser='" & Session("UserID") & "' Order By approvallevel")

        Dim sCode As String = "" : Dim sCodeSr As String = ""
        Dim SrCode() As String = QlSR.Split(",")
        For C1 As Integer = 0 To SrCode.Length - 1
            If SrCode(C1) <> "" Then
                sCodeSR &= "'" & SrCode(C1).Trim & "',"
            End If
        Next

        sSql = "SELECT jj.trnjualreturmstoid, jj.trnjualreturno, jj.trnjualdate, c.custname, ap.approvaloid,ap.requestuser, ap.requestdate,jj.branch_code FROM QL_trnjualreturmst jj INNER JOIN QL_APPROVAL ap ON ap.OID=jj.trnjualreturmstoid AND jj.branch_code=ap.branch_code INNER JOIN ql_mstcust c ON c.custoid=jj.trncustoid AND jj.branch_code=c.branch_code INNER JOIN QL_trnjualmst jm ON jm.trnjualno=jj.refnotaretur AND jj.branch_code=jm.branch_code WHERE ap.statusrequest = 'New' AND (ap.EVENT = 'In Approval') AND (jj.cmpcode = '" & CompnyCode & "') AND (ap.APPROVALUSER = '" & Session("UserID") & "') AND (ap.TABLENAME = 'QL_trnjualreturmst') AND (jj.trnjualstatus = 'In Approval') AND jj.trncustoid = c.custoid And jj.branch_code IN (" & Left(sCodeSr, sCodeSr.Length - 1) & ") ORDER BY ap.REQUESTDATE DESC"
        FillGV(sRetGV, sSql, "SR")
    End Sub

    Protected Sub lkbSelectSR_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("retOid") = sender.ToolTip
        MultiView1.SetActiveView(View35)
        Session("sAppOid") = sender.CommandArgument
        Session("sCodeCbg") = sender.CommandName.ToString

        sSql = "Select a.branch_code,a.trnjualreturmstoid,a.trnjualmstoid, (select trnjualno From QL_trnjualmst Where trnjualmstoid = a.trnjualmstoid AND a.branch_code=branch_code) trnjualno, a.orderno, a.trnjualreturno, a.refSJjualno, a.refnotaretur, a.typeretur, a.trnjualdate, a.trncustoid, a.trncustname, a.trncustoid, a.trnpaytype, a.trnjualref, a.trnamtjual, a.trntaxpct, a.trnamttax, a.trnamtjualnetto, a.trnjualnote, a.trnjualstatus, b.itemloc, (Select trnjualdate From QL_trnjualmst Where trnjualmstoid = a.trnjualmstoid And a.branch_code=branch_code) jualdate, a.currencyoid, a.upduser, a.updtime, a.typeSO From QL_trnjualreturmst a INNER JOIN QL_trnjualreturdtl b on a.branch_code=b.branch_code And a.trnjualreturmstoid=b.trnjualreturmstoid Where a.trnjualreturmstoid = " & Session("retOid") & " And a.cmpcode = '" & CompnyCode & "' AND a.branch_code='" & Session("sCodeCbg").ToString & "'"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_RetTbl")
        trnjualreturmstoid.Text = dt.Rows(0).Item("trnjualreturmstoid")
        TrnJualmstoid.Text = dt.Rows(0).Item("trnjualmstoid")
        sRetNo.Text = dt.Rows(0).Item("trnjualreturno").ToString
        sCodeCbg.Text = dt.Rows(0).Item("branch_code").ToString
        InvNoSI.Text = dt.Rows(0).Item("trnjualno").ToString
        sTypeRet.Text = dt.Rows(0).Item("typeretur").ToString
        DDLtype.SelectedValue = dt.Rows(0).Item("typeretur")
        sRetDate.Text = Format(dt.Rows(0).Item("trnjualdate"), "dd/MM/yyyy")
        sCustName.Text = dt.Rows(0).Item("trncustname").ToString
        sCustoid.Text = dt.Rows(0).Item("trncustoid").ToString
        sTaxAmt.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("trnamttax")), 3) ' PPN
        sTotAmt.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("trnamtjual")), 3) ' DPP
        sTotNetto.Text = ToMaskEdit(ToDouble(dt.Rows(0).Item("trnamtjualnetto")), 4) ' NETTO
        sAppOid.Text = Session("sAppOid")
        TypeSoRet.Text = dt.Rows(0).Item("typeSO").ToString
        Session("trnjualnote") = dt.Rows(0).Item("trnjualnote").ToString

        sSql = "Select a.branch_code,a.trnjualdtlseq seq, a.trnjualreturdtloid, c.itemcode, c.itemdesc itemname, a.trnjualdtlqty qty, d.gendesc unit, a.trnjualdtlprice price, a.trnjualnote note, c.itemoid, a.amtjualnetto netamt, a.trnjualdtlunitoid unitoid, a.unitSeq unitseq, a.trnjualdtloid,a.trnjualdtlqty maxqty, (select y.gendesc + ' - ' + x.gendesc from ql_mstgen x inner join ql_mstgen y on x.cmpcode = y.cmpcode and x.genother1 = y.genoid and x.gengroup = 'location' and y.gengroup = 'WAREHOUSE' And x.cmpcode = a.cmpcode and x.genoid = a.itemloc) location, a.itemloc locationoid, a.trnjualdtldiscqty promodisc, a.amtjualdisc promodiscamt,a.trnjualdtloid from QL_trnjualreturdtl a inner join QL_trnjualreturmst b on a.cmpcode = b.cmpcode and a.trnjualreturmstoid = b.trnjualreturmstoid and a.branch_code = b.branch_code inner join QL_mstItem c on a.cmpcode = c.cmpcode and a.itemoid = c.itemoid inner join QL_mstgen d on a.cmpcode = d.cmpcode and a.trnjualdtlunitoid = d.genoid Where a.cmpcode = '" & CompnyCode & "' and a.trnjualreturmstoid = " & Session("retOid") & " and a.branch_code='" & sCodeCbg.Text & "'"
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "DtlVTL")
        GVDtl.DataSource = dtab : GVDtl.DataBind()
        Session("dtlsalesreturn") = dtab
    End Sub 

    Protected Sub BtnRejectSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            sSql = "UPDATE QL_trnjualreturmst SET trnjualstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnjualreturmstoid = '" & trnjualreturmstoid.Text & "' And branch_code='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET event='Rejected',statusrequest='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & sAppOid.Text & "' And branch_code='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval user yg lain 
            sSql = "update QL_approval set event='Ignore',statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='QL_trnjualreturmst' And oid=" & trnjualreturmstoid.Text & " AND approvaloid <> '" & sAppOid.Text & "' And branch_code='" & sCodeCbg.Text & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        setSalesReturnActive()
    End Sub

    Protected Sub BtnReviseSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub SubRevisiBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub BtnAppSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppSret.Click
        Dim erracctmsg As String = "", returdate As New Date, sMsg As String = ""
        If Date.TryParseExact(sRetDate.Text, "dd/MM/yyyy", Nothing, Nothing, returdate) = False Then
            sMsg &= "Maaf, Tanggal retur salah koreksi kembali tanggal yang di input..!!<br />"
        End If

        If returdate < New Date(1900, 1, 1) Then
            sMsg &= "Maaf, tanggal Retur tidak bisa kurang dari tangggal'1/1/1900'..!!<br />"
        End If

        If returdate > New Date(2079, 6, 6) Then
            sMsg &= "Maaf, tanggal Retur tidak bisa lebih dari tangggal'1/1/2079'..!!<br />"
        End If

        ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
        sSql = "select trnjualstatus from QL_trnjualreturmst Where trnjualreturmstoid = " & Integer.Parse(trnjualreturmstoid.Text) & " and branch_code='" & sCodeCbg.Text & "'"
        Dim stat As String = GetStrData(sSql)
        If stat.ToLower = "approved" Then
            sMsg &= "- Maaf, status transaksi sudah '" & stat & "', tekan Tombol Back dan cek kembali status transaksi ini..!!<br />"
        ElseIf stat Is Nothing Or stat = "" Then
            sMsg &= "- Maaf, Sales return tidak ada..!!<br>"
        End If

        If sMsg <> "" Then
            status.Text = "In Approval"
            showMessage(sMsg, CompnyName & " - ERROR", 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        '------ Generated mstoid ------
        '==============================
        Dim crdmtroid As Integer = GenerateID("QL_crdmtr", CompnyCode)
        Dim glmstoid As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim gldtloid As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim oidTemp As Integer = GenerateID("QL_HistHPP", CompnyCode)
        Dim payaroid As Integer = GenerateID("QL_trnpayar", CompnyCode)
        Dim conaroid As Integer = GenerateID("QL_conar", CompnyCode)
        Dim dPOid As Integer = GenerateID("QL_TRNDPAR", CompnyCode)
        Dim retDate As Date = GetServerTime()
        Dim Periodacctg As String = GetDateToPeriodAcctg(Format(GetServerTime(), "MM/dd/yyyy"))

        '------ Generated No Return -------
        '==================================
        Dim iCurID As Integer = 0, sKon As String = "", Cabang As String = "", Retno As String = ""
        If TypeSoRet.Text = "Konsinyasi" Then
            sKon = "K"
        End If

        Cabang = GetStrData("Select genother1 From ql_mstgen Where gencode='" & sCodeCbg.Text & "' AND gengroup='CABANG'")
        Retno = "SR" & sKon & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trnjualreturno,4) AS INT)),0) FROM QL_trnjualreturmst WHERE trnjualreturno LIKE '" & Retno & "%'" : iCurID = GetStrData(sSql) + 1
        sRetNo.Text = GenNumberString(Retno, "", iCurID, 4)

        '----- Bind Data detail return untuk looping ------
        Dim dtab As DataTable = Session("dtlsalesreturn")
        '==================================================
        Dim hppne As Decimal = 0.0, conmtroid As Integer = 0, HPPold As Double = 0.0, HPPItemDO As Double = 0.0, HPPnew As Double = 0.0, SisaStock As Double = 0.0, noDP As String = "", sNo As String = "", oTrans As SqlTransaction

        '-------- Open Connection database --------
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        '=== Genereated No DP ===
        sNo = "DP/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndparno, 2) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" & CompnyCode & "' AND trndparno LIKE '%" & sNo & "%'"
        xCmd.CommandText = sSql : Dim dNo = xCmd.ExecuteScalar
        noDp = GenNumberString(sNo, "", dNo, 4)

        Try
            sSql = "UPDATE QL_trnjualreturmst SET trnjualreturno='" & sRetNo.Text.Trim & "', trnjualstatus='Approved', upduser='" & Session("UserID") & "', trnjualdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), updtime=CURRENT_TIMESTAMP Where trnjualreturmstoid = " & trnjualreturmstoid.Text & " And cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For i As Integer = 0 To dtab.Rows.Count - 1
                sSql = "select isnull(HPP,0) FROM QL_mstitem Where cmpcode='" & CompnyCode & "' and itemoid='" & dtab.Rows(i)("itemoid") & "'"
                xCmd.CommandText = sSql : HPPold = xCmd.ExecuteScalar

                sSql = "Select MAX(conmtroid)+1 fROM QL_conmtr Where cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : conmtroid = xCmd.ExecuteScalar

                '= GenerateID("QL_conmtr", CompnyCode)
                Dim QtyterKonversi As Double = ToDouble(dtab.Rows(i)("qty"))

                '-------- Cek Stock Akhir item --------
                '======================================
                ' Stock akhir utk dasar AVG HPP ambil dari conmtr all cabang
                sSql = "SELECT isnull(SUM(qtyIn),0.00)-isnull(sum(qtyOut),0.00) from QL_conmtr WHERE refoid = " & dtab.Rows(i)("itemoid").ToString & " AND cmpcode = '" & CompnyCode & "' and periodacctg = '" & GetDateToPeriodAcctg(GetServerTime()) & "' "
                xCmd.CommandText = sSql : SisaStock = xCmd.ExecuteScalar

                ' Pakai harga saat DO utk sbg harga utk average HPP baru karena stock masuk
                sSql = "select ISNULL(c.hpp,i.hpp) from QL_mstitem i INNER JOIN QL_trnjualreturdtl rd ON i.itemoid=rd.itemoid LEFT JOIN QL_trnjualdtl jd ON jd.trnjualdtloid=rd.trnjualdtloid AND jd.branch_code=rd.branch_code " & _
                    "LEFT JOIN QL_trnsjjualdtl sjd ON sjd.trnsjjualdtloid=jd.trnsjjualdtloid AND sjd.branch_code=jd.branch_code LEFT JOIN QL_conmtr c ON c.formoid=sjd.trnsjjualdtloid AND c.formname='QL_trnsjjualdtl' Where rd.branch_code='" & sCodeCbg.Text & "' and (jd.trnjualdtloid=" & dtab.Rows(i).Item("trnjualdtloid").ToString & " OR rd.trnjualreturdtloid=" & dtab.Rows(i).Item("trnjualreturdtloid").ToString & ")"
                xCmd.CommandText = sSql : HPPItemDO = xCmd.ExecuteScalar

                '------------ Proses Hitung hpp ------------
                '===========================================
                If ToDouble(SisaStock) < 0 Then
                    SisaStock = 0
                End If
                HPPnew = ((SisaStock * HPPold) + (ToDouble(dtab.Rows(i)("qty").ToString) * HPPItemDO)) / (SisaStock + ToDouble(dtab.Rows(i)("qty")))
                hppne = hppne + HPPnew * ToDouble(dtab.Rows(i)("qty"))

                sSql = "update QL_mstitem set HPP=" & HPPnew & " Where cmpcode='" & CompnyCode & "' And itemoid=" & dtab.Rows(i).Item("itemoid").ToString : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '--------- Insert Table histhpp ---------
                '========================================
                oidTemp = oidTemp + 1
                sSql = "INSERT INTO QL_HistHPP (cmpcode, oid, transOid, transName, refname, refoid, groupOid, lastHpp, newHpp, totalOldStock, qtyTrans, pricePerItem, periodacctg, updtime, upduser, branch_code) Values" & _
                "('" & CompnyCode & "', " & oidTemp + 1 & ", " & dtab.Rows(i).Item("trnjualreturdtloid") & ", 'QL_trnjualreturdtl', 'QL_MSTITEM', " & dtab.Rows(i).Item("itemoid") & ", 0, " & ToDouble(HPPold) & ", " & ToDouble(HPPnew) & ", " & ToDouble(SisaStock) & ", " & ToDouble(dtab.Rows(i)("qty").ToString) & ", " & HPPItemDO & ", '" & Periodacctg & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', '" & dtab.Rows(i).Item("branch_code").ToString & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & oidTemp + 1 & " where tablename = 'QL_HistHPP' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES" & _
                " ('" & CompnyCode & "', '" & dtab.Rows(i).Item("branch_code").ToString & "', " & conmtroid + 1 & ", 'SR', '" & GetServerTime() & "', '" & Periodacctg & "', '" & Tchar(sRetNo.Text) & "', " & dtab.Rows(i).Item("trnjualreturdtloid") & ", 'QL_trnjualreturdtl', " & dtab.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & dtab.Rows(i).Item("unitoid") & ", " & dtab.Rows(i).Item("locationoid") & ", " & dtab.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(dtab.Rows(i).Item("qty").ToString) * HPPItemDO & ", '" & Tchar(dtab.Rows(i).Item("note")) & "', " & ToDouble(HPPItemDO) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid = " & conmtroid + 1 & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "Update QL_trnjualdtl set trnjualdtlqty_retur = " & dtab.Rows(i).Item("qty") & ", trnjualdtlqty_retur_unit3 =(" & IIf(dtab.Rows(i).Item("unitseq") = 1, dtab.Rows(i).Item("qty") & " * (select cast(konversi1_2 * konversi2_3 as decimal(18,2)) From ql_mstitem Where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", IIf(dtab.Rows(i).Item("unitseq") = 2, dtab.Rows(i).Item("qty") & " * (select cast(konversi2_3 as decimal(18,2)) from ql_mstitem where itemoid = " & dtab.Rows(i).Item("itemoid") & ")", dtab.Rows(i).Item("qty"))) & ") Where trnjualdtloid = " & dtab.Rows(i).Item("trnjualdtloid") & " and cmpcode = '" & CompnyCode & "' And branch_code='" & dtab.Rows(i).Item("branch_code").ToString & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            ' Update Approval
            sSql = "UPDATE ql_approval SET event='Approved', statusrequest='Approved' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & sAppOid.Text & "' And branch_code='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval user yg lain 
            sSql = "update QL_approval set event='Ignore', statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='QL_trnjualreturmst' And oid=" & trnjualreturmstoid.Text & " AND approvaloid <> '" & sAppOid.Text & "' And branch_code='" & sCodeCbg.Text & "' "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Session("trnjualnote")
            Dim CoaDpOid As Integer, glseq As Integer = 0, AmtRetur As Double = 0, ReturacctgOid As Integer = 0, ArAcctgOid As Integer = 0

            sSql = "INSERT into QL_trnglmst (cmpcode, branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) VALUES" & _
            " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & glmstoid & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetDateToPeriodAcctg(GetServerTime()) & "', 'Sales Return No.: " & sRetNo.Text.Trim & "', 'POST', current_timestamp, '" & Session("UserID") & "',current_timestamp, '" & Session("UserID") & "', current_timestamp, 'SR')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '--------- Retur Dijadikan DP ---------
            '============= Accounting =============

            If DDLtype.SelectedValue = 4 Then
                '--- Create Auto Jurnal Ketika Jadi DP ---
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DPAR' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'"
                xCmd.CommandText = sSql : Dim varDP As String = xCmd.ExecuteScalar
                If varDP <> "" And Not varDP Is Nothing Then
                    sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varDP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a Where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                    xCmd.CommandText = sSql : CoaDpOid = xCmd.ExecuteScalar
                Else
                    erracctmsg &= "- Maaf,Interface 'VAR_DPAR' belum di setting oleh admin silahkan hubungi admin untuk setting interface..!!<br/>"
                End If

                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_RETJUAL' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql
                Dim VarRetJual As String = xCmd.ExecuteScalar
                If VarRetJual <> "" And Not VarRetJual Is Nothing Then
                    sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & VarRetJual & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                    xCmd.CommandText = sSql : ReturacctgOid = xCmd.ExecuteScalar
                Else
                    erracctmsg &= "Maaf, Interface VAR_RETJUAL belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                End If

                If erracctmsg <> "" Then
                    objTrans.Rollback() : conn.Close() : status.Text = "In Approval"
                    showMessage(erracctmsg, CompnyName & " - INFORMATION ", 2)
                    Exit Sub
                End If

                sSql = "INSERT INTO QL_trnDPAR (cmpcode, trnDPARoid,branch_code, trnDPARno, trnDPARdate, custoid, cashbankoid, trnDPARacctgoid, payreftype, cashbankacctgoid, payduedate, payrefno, currencyoid, currencyrate, rate2oid, trnDPARamt, trnDPARamtidr, trnDPARamtusd, taxtype, taxoid, taxpct, taxamt, taxamtidr, taxamtusd, trnDPARnote, trnDPARflag, trnDPARacumamt, trnDPARacumamtidr, trnDPARacumamtusd, trnDPARstatus, createuser, createtime, upduser, updtime) " & _
                " VALUES ('" & CompnyCode & "', " & dPOid & ",'" & sCodeCbg.Text & "','" & noDP & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & sCustoid.Text & "," & 0 & ", " & CoaDpOid & ", 'RETUR', 0,'1/1/1900','" & Tchar(sRetNo.Text) & "',1,0,1," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & ",'NONTAX','',0,0,0,0,'" & Tchar(note.Text) & "','',0,0,0,'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & dPOid & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnjualmst set amtreturdp = amtreturdp + " & ToDouble(sTotNetto.Text) & ",amtreturdpidr = amtreturdpidr + " & ToDouble(sTotNetto.Text) & " Where trnjualno = '" & InvNoSI.Text & "' and cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_trnjualreturmst Set amtretur_piutang=" & ToDouble(sTotNetto.Text) & ",amtretur_piutangidr=" & ToDouble(sTotNetto.Text) & " Where trnjualreturmstoid = " & trnjualreturmstoid.Text & " And cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '-- Create Jurnal DP --
                'Cust.[NAMA CUST] | No. Retur. [NOTA RETUR] | Note.[ISI NOTE] | No. Nota.[NO SI]
                'InvNoSI.Text = dt.Rows(0).Item("trnjualno").ToString - noDP
                'sCustName.Text = dt.Rows(0).Item("trncustname").ToString
                sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", " & glseq + 1 & ", " & glmstoid & ", " & ReturacctgOid & ", 'D', " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun DPAR - " & noDP & ") | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '--- Create Jurnal Retur Penjualan ---
                gldtloid = gldtloid + 1 : glseq = glseq + 1
                sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", " & glseq & ", " & glmstoid & ", " & CoaDpOid & ", 'C', " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun Retur Penjualan - " & noDP & ") | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else '---------Akhir Retur Dijadikan DP ---------

                Dim Sisahutang As Double = 0
                sSql = "Select trnamtjualnetto-Isnull((accumpayment+amtretur+" & ToDouble(sTotNetto.Text) & "),0.00) From QL_trnjualmst Where trnjualno='" & InvNoSI.Text & "' And branch_code='" & sCodeCbg.Text & "'"
                xCmd.CommandText = sSql : Sisahutang = xCmd.ExecuteScalar
                AmtRetur = Sisahutang + sTotNetto.Text

                '--- Kondisi retur ketika sisa hutang ada ---
                If ToDouble(Sisahutang) < 0.0 Then

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_DPAR' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql : Dim Var_DP As String = xCmd.ExecuteScalar

                    sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & Var_DP & "'"
                    xCmd.CommandText = sSql : CoaDpOid = xCmd.ExecuteScalar

                    If Var_DP <> "" And Not Var_DP Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & Var_DP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ArAcctgOid = xCmd.ExecuteScalar
                    Else
                        erracctmsg &= "Maaf, Interface Var_DP belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                    End If

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_AR' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql
                    Dim VAR_AR As String = xCmd.ExecuteScalar
                    If VAR_AR <> "" And Not VAR_AR Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & VAR_AR & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ArAcctgOid = xCmd.ExecuteScalar
                    Else
                        erracctmsg &= "Maaf, Interface VAR_AR belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                    End If

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_RETJUAL' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql
                    Dim VarRetJual As String = xCmd.ExecuteScalar
                    If VarRetJual <> "" And Not VarRetJual Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & VarRetJual & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ReturacctgOid = xCmd.ExecuteScalar
                    Else
                        erracctmsg &= "Maaf, Interface VAR_RETJUAL belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                    End If

                    If erracctmsg <> "" Then
                        objTrans.Rollback() : conn.Close() : status.Text = "In Approval"
                        showMessage(erracctmsg, CompnyName & " - INFORMATION ", 2)
                        Exit Sub
                    End If

                    '--------- Update table jual ---------
                    sSql = "UPDATE QL_trnjualmst set amtreturdp = amtreturdp+" & ToDouble(Sisahutang) * -1 & ",amtreturdpidr = amtreturdp+" & ToDouble(Sisahutang) * -1 & ", amtretur=amtretur+" & ToDouble(sTotNetto.Text) & ",amtreturidr=amtreturidr+" & ToDouble(sTotNetto.Text) & " Where trnjualno = '" & InvNoSI.Text & "' and cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '---- Update Tbale retur ketika ada lbh retur -----
                    sSql = "Update QL_trnjualreturmst Set amtretur_piutang=" & ToDouble(Sisahutang) * -1 & ", amtretur_piutangidr=" & ToDouble(Sisahutang) * -1 & " Where trnjualreturmstoid = " & trnjualreturmstoid.Text & " And cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '--- Insert ketika sisa jadi DP ---
                    sSql = "INSERT INTO QL_trnDPAR (cmpcode, trnDPARoid, branch_code, trnDPARno, trnDPARdate, custoid, cashbankoid, trnDPARacctgoid, payreftype, cashbankacctgoid, payduedate, payrefno, currencyoid, currencyrate, rate2oid, trnDPARamt, trnDPARamtidr, trnDPARamtusd, taxtype, taxoid, taxpct, taxamt, taxamtidr, taxamtusd, trnDPARnote, trnDPARflag, trnDPARacumamt, trnDPARacumamtidr, trnDPARacumamtusd, trnDPARstatus, createuser, createtime, upduser, updtime)" & _
                    " VALUES ('" & CompnyCode & "', " & dPOid & ", '" & sCodeCbg.Text & "', '" & noDP & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & sCustoid.Text & ", " & 0 & ", " & CoaDpOid & ", 'RETUR', 0, '1/1/1900', '" & Tchar(sRetNo.Text) & "', 1, 0, 1, " & ToDouble(Sisahutang) * -1 & ", " & ToDouble(Sisahutang) * -1 & ", " & ToDouble(Sisahutang) * -1 & ", 'NONTAX', '', 0, 0, 0, 0, '" & Tchar(note.Text) & "', '', 0, 0, 0, 'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '--- Insert Conar Untuk Memotong Piutang ---
                    sSql = "INSERT into QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnarflag, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime, returoid) VALUES" & _
                    " ('" & CompnyCode & "','" & sCodeCbg.Text & "'," & conaroid & ",'QL_trnpayar',(select trnjualmstoid from ql_trnjualmst where trnjualno='" & InvNoSI.Text & "')," & payaroid & "," & Integer.Parse(sCustoid.Text) & "," & ArAcctgOid & ",'POST','','AR',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(GetServerTime()) & "'," & ReturacctgOid & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & sRetNo.Text & "',0,'" & GetServerTime() & "',0," & ToDouble(AmtRetur) & "," & ToDouble(AmtRetur) & "," & ToDouble(AmtRetur) & ",'SALES RETURN (NO:" & sRetNo.Text & ", SI NO:" & InvNoSI.Text & "','','" & Session("UserID") & "',current_timestamp," & trnjualreturmstoid.Text & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '--- Insert Payar Untuk Memotong Piutang ---
                    sSql = "insert into QL_trnpayar (cmpcode,branch_code, paymentoid, cashbankoid, custoid, payreftype, payrefoid, payflag, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt, payamtidr, payamtusd, payrefflag, paystatus, upduser, updtime, trndparoid, DPAmt, returoid) VALUES" & _
                    " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & payaroid & ", 0, " & Integer.Parse(sCustoid.Text) & ", 'QL_trnjualmst', (select trnjualmstoid from ql_trnjualmst Where trnjualno='" & InvNoSI.Text & "'), '', " & ReturacctgOid & ", '" & sRetNo.Text & "', 0, (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), 'Hutang(D) - Persediaan/retur(C)', " & ToDouble(AmtRetur) & ", " & ToDouble(AmtRetur) & ", " & ToDouble(AmtRetur) & ", '', 'POST', '" & Session("UserID") & "', current_timestamp, 0, 0, " & trnjualreturmstoid.Text & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '-- Create Jurnal DP -- 
                    glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", " & glseq & ", " & glmstoid & ", " & CoaDpOid & ", 'C', " & ToDouble(Sisahutang) * -1 & ", " & ToDouble(Sisahutang) * -1 & ", " & ToDouble(Sisahutang) * -1 & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun Piutang DP - " & noDP & ") | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '-- Create Jurnal Piutang -- 
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", '" & glseq & "', " & glmstoid & ", " & ArAcctgOid & ", 'C', " & ToDouble(AmtRetur) & ", " & ToDouble(AmtRetur) & ", " & ToDouble(AmtRetur) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun piutang - " & noDP & ") | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '===========================

                    '-- Create Jurnal retur piutang -- 
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "',  '" & sCodeCbg.Text & "'," & gldtloid & ", '" & glseq & "', " & glmstoid & ", " & ReturacctgOid & ", 'D', " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", " & ToDouble(sTotNetto.Text) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun piutang retur - " & noDP & ") | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '==================================
                Else
                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_AR' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql
                    Dim VAR_AR As String = xCmd.ExecuteScalar
                    If VAR_AR <> "" And Not VAR_AR Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & VAR_AR & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ArAcctgOid = xCmd.ExecuteScalar
                    Else
                        erracctmsg &= "Maaf, Interface VAR_AR belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                    End If

                    sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_RETJUAL' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'" : xCmd.CommandText = sSql
                    Dim VarRetJual As String = xCmd.ExecuteScalar
                    If VarRetJual <> "" And Not VarRetJual Is Nothing Then
                        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE acctgcode = '" & VarRetJual & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a WHERE a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                        xCmd.CommandText = sSql : ReturacctgOid = xCmd.ExecuteScalar
                    Else
                        erracctmsg &= "Maaf, Interface VAR_RETJUAL belum di setting silahkan ubungi admin accounting untuk setting interface..!!!<br/>"
                    End If

                    If erracctmsg <> "" Then
                        objTrans.Rollback() : conn.Close() : status.Text = "In Approval"
                        showMessage(erracctmsg, CompnyName & " - INFORMATION ", 2)
                        Exit Sub
                    End If
                    '--------- Update table jual ---------
                    sSql = "UPDATE QL_trnjualmst set amtretur=amtretur+" & ToDouble(sTotNetto.Text) & ",amtreturidr=amtreturidr+" & ToDouble(sTotNetto.Text) & " Where trnjualno = '" & InvNoSI.Text & "' and cmpcode = '" & CompnyCode & "' And branch_code='" & sCodeCbg.Text & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '--- Insert Conar Untuk Memotong Piutang ---
                    sSql = "INSERT into QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnarflag, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime, returoid) VALUES" & _
                    " ('" & CompnyCode & "','" & sCodeCbg.Text & "'," & conaroid & ",'QL_trnpayar',(select trnjualmstoid from ql_trnjualmst Where trnjualno='" & InvNoSI.Text & "')," & payaroid & "," & Integer.Parse(sCustoid.Text) & "," & ArAcctgOid & ",'POST','','AR',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(GetServerTime()) & "'," & ReturacctgOid & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & sRetNo.Text & "',0,'" & GetServerTime() & "',0," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & ",'SALES RETURN (NO:" & sRetNo.Text & ", SI NO:" & InvNoSI.Text & "','','" & Session("UserID") & "',current_timestamp," & trnjualreturmstoid.Text & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '--- Insert Payar Untuk Memotong Piutang ---
                    sSql = "insert into QL_trnpayar (cmpcode,branch_code, paymentoid, cashbankoid, custoid, payreftype, payrefoid, payflag, payacctgoid, payrefno, paybankoid, payduedate, paynote, payamt,payamtidr,payamtusd, payrefflag, paystatus, upduser, updtime, trndparoid, DPAmt, returoid) VALUES" & _
                    " ('" & CompnyCode & "','" & sCodeCbg.Text & "'," & payaroid & ",0," & Integer.Parse(sCustoid.Text) & ",'QL_trnjualmst',(select trnjualmstoid from ql_trnjualmst Where trnjualno='" & InvNoSI.Text & "'),''," & ReturacctgOid & ",'" & sRetNo.Text & "',0,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'Hutang(D) - Persediaan/retur(C)'," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & ",'','POST','" & Session("UserID") & "',current_timestamp,0,0," & trnjualreturmstoid.Text & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '-- Create Jurnal Piutang -- 
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate,createuser,createtime,upduser,updtime) VALUES" & _
                    " ('" & CompnyCode & "','" & sCodeCbg.Text & "'," & gldtloid & ",'" & glseq & "'," & glmstoid & "," & ArAcctgOid & ",'C'," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & ",'" & sRetNo.Text & "','Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun piutang) | No. Nota." & InvNoSI.Text & "','','','POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '===========================

                    '-- Create Jurnal retur piutang -- 
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "','" & sCodeCbg.Text & "'," & gldtloid & ",'" & glseq & "'," & glmstoid & "," & ReturacctgOid & ",'D'," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & "," & ToDouble(sTotNetto.Text) & ",'" & sRetNo.Text & "','Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun retur penjualan) | No. Nota." & InvNoSI.Text & "', '', '', 'POST',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '==================================
                End If
            End If

            'Akun piutang (C)
            'Dim araccount As Integer = 0
            If ToDouble(sTotNetto.Text) > 0 Then

                If DDLtype.SelectedValue = 4 Then
                    '====== Jika type retur jadi dp =======
                    '--------------------------------------
                Else
                    'If TypeSoRet.Text = "Konsinyasi" Then 
                    '    '==== Jika Retur Type Konsinyasi ====
                    '    sSql = "UPDATE ql_trnordermst SET orderno = '" & orderno.Text & "', trnorderstatus = 'Approved', trnorderdate = '" & Format(GetServerTime(), "MM/dd/yyyy") & "' ,periodacctg = '" & Format(GetServerTime(), "yyyy-MM") & "', finalapprovaluser='" & Session("UserID") & "',finalappovaldatetime=CURRENT_TIMESTAMP/*, otorisasiuser = '" & Tchar(SOconfimID.Text) & "'*/ WHERE cmpcode = '" & CompnyCode & "' And branch_code='" & zCabang.Text & "' AND ordermstoid = '" & Integer.Parse(OrderOid.Text) & "'"
                    '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    '    Dim sVarAR As String = GetInterfaceValue("VAR_AR_KONSI", zCabang.Text)
                    '    If sVarAR = "?" Then
                    '        objTrans.Rollback() : conn.Close()
                    '        showMessage("- Maaf, Interface 'VAR_AR_KONSI' belum disetting..!!", CompnyName & " - WARNING", 2)
                    '        Exit Sub
                    '    End If
                    '    araccount = GetAccountOid(sVarAR, False)
                    'Else
                    '    '==== Jika Retur Type bukan Konsinyasi ====
                    'sSql = "Select s.custacctgoid from ql_mstcust s inner join QL_mstacctg a on a.acctgoid=s.custacctgoid and s.custoid=" & Integer.Parse(sCustoid.Text) & " AND s.branch_code='" & sCodeCbg.Text & "'"
                    'xCmd.CommandText = sSql : araccount = xCmd.ExecuteScalar
                    'End If
                End If
            End If

            'Persediaan/stock
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_GUDANG' And cmpcode='" & CompnyCode & "' AND interfaceres1='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : Dim varGudang As String = xCmd.ExecuteScalar
            Dim coa_gudang As Integer = 0
            If varGudang <> "" And Not varGudang Is Nothing Then
                Dim dtabl As DataTable = Session("dtlsalesreturn")
                For i As Integer = 0 To dtabl.Rows.Count - 1
                    sSql = "Select TOP 1 acctgoid FROM ql_mstacctg Where acctgoid=(Select distinct genother2 FROM QL_mstgen Where gengroup='COA' AND genother1=" & dtabl.Rows(i).Item("locationoid") & " And genother3='" & sCodeCbg.Text & "' AND genother4 IN ('T','V'))"
                    xCmd.CommandText = sSql : coa_gudang = xCmd.ExecuteScalar
                Next

                If coa_gudang > 0 Then
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", " & glseq & ", " & glmstoid & ", " & coa_gudang & ", 'D', " & ToDouble(hppne) & ", " & ToDouble(hppne) & ", " & ToDouble(hppne) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun Piutang Sediaan) | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    erracctmsg &= "- Maaf, 'VAR_GUDANG' Belum di setting..!!<br />- Silahkan hubungi pihak accounting..!!"
                End If
            Else
                erracctmsg &= "- Maaf, 'VAR_GUDANG' Belum di setting..!!<br />- Silahkan hubungi pihak accounting..!!"
            End If

            'HPP
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_HPP' and cmpcode='" & CompnyCode & "' And interfaceres1='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql
            Dim varHPP As String = xCmd.ExecuteScalar : Dim coa_hpp As Integer = 0
            If varHPP <> "" And Not varHPP Is Nothing Then
                sSql = "SELECT top 1 acctgoid FROM QL_mstacctg WHERE acctgcode = '" & varHPP & "' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode)"
                xCmd.CommandText = sSql : coa_hpp = xCmd.ExecuteScalar
                If coa_hpp > 0 Then
                    gldtloid = gldtloid + 1 : glseq = glseq + 1
                    sSql = "INSERT into QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, glamtidr, glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, createuser, createtime, upduser, updtime) VALUES" & _
                    " ('" & CompnyCode & "', '" & sCodeCbg.Text & "', " & gldtloid & ", " & glseq & ", " & glmstoid & ", " & coa_hpp & ", 'C', " & ToDouble(hppne) & ", " & ToDouble(hppne) & ", " & ToDouble(hppne) & ", '" & sRetNo.Text & "', 'Cust." & Tchar(sCustName.Text) & " | No. Retur." & sRetNo.Text & " | Note." & Tchar(Session("trnjualnote").ToString) & " (Akun Retur HPP) | No. Nota." & InvNoSI.Text & "', '', '', 'POST', current_timestamp, '" & Session("UserID") & "', current_timestamp, '" & Session("UserID") & "', current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    erracctmsg &= "Maaf, 'VAR_HPP' Belum di setting..!!<br/>- Silahkan hubungi pihak accounting..!!"
                End If
            Else
                erracctmsg &= "Maaf, 'VAR_HPP' Belum di setting..!!<br/>- Silahkan hubungi pihak accounting..!!"
            End If

            If erracctmsg <> "" Then
                objTrans.Rollback() : conn.Close() : status.Text = "In Approval"
                showMessage(erracctmsg, CompnyName & " - INFORMATION ", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_mstoid SET lastoid=" & dPOid & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & glmstoid & " Where tablename = 'QL_TRNGLMST' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & gldtloid & " Where tablename = 'QL_TRNGLDTL' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & payaroid & " Where tablename = 'QL_trnpayar' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & conaroid & " Where tablename = 'QL_conar' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '==================Accounting'===================
            '=================Credit Limit'==================
            Dim climit As Double = 0.0 : Dim cbayar As Double = 0.0
            sSql = "select trnamtjualnettoacctg - accumpayment from QL_trnjualmst Where trnjualno='" & InvNoSI.Text & "' and branch_code='" & sCodeCbg.Text & "'"
            xCmd.CommandText = sSql : cbayar = xCmd.ExecuteScalar
            If ToDouble(cbayar) < ToDouble((sTotNetto.Text)) Then
                climit = ToDouble(cbayar)
            Else
                climit = ToDouble((sTotNetto.Text))
            End If
            'climit = ToDouble(totalamount.Text)
            If cbayar > 0.0 Then
                Dim custgroupoid As Int32 = 0
                sSql = "Select ISNULL((select custgroupoid from QL_mstcustgroup Where custgroupoid = c.custgroupoid),0) custgroupoid from QL_mstcust c Where custoid = " & Integer.Parse(sCustoid.Text) & ""
                xCmd.CommandText = sSql : custgroupoid = xCmd.ExecuteScalar()

                sSql = "select ISNULL(custgroupcreditlimitusage,0.00) from QL_mstcustgroup Where custgroupoid IN (select custgroupoid from QL_mstcust Where custoid=" & Integer.Parse(sCustoid.Text) & " AND branch_code='" & sCodeCbg.Text & "')"
                xCmd.CommandText = sSql : Dim CustGroupUsg As Double = xCmd.ExecuteScalar()

                'cek kalau udah lunas maka belum motong credit limit, kalau udah lunas.. tidak ngurangin
                'If lblcurr.Text = "1" Then
                If ToDouble(CustGroupUsg) < 0 Then
                    sSql = "Update ql_mstcustgroup set custgroupcreditlimitusage=ISNULL(custgroupcreditlimitusage,0.00)+" & ToDouble(sTotNetto.Text) & ", custgroupcreditlimitusagerupiah=Isnull(custgroupcreditlimitusagerupiah,0.00)+" & ToDouble(sTotNetto.Text) & " Where custgroupoid=" & custgroupoid & " And branch_code='" & sCodeCbg.Text & "'"
                ElseIf ToDouble(CustGroupUsg) = 0 Then
                    sSql = "Update ql_mstcustgroup set custgroupcreditlimitusage=0,custgroupcreditlimitusagerupiah=0 Where custgroupoid=" & custgroupoid & " And branch_code='" & sCodeCbg.Text & "'"
                Else
                    sSql = "Update ql_mstcustgroup set custgroupcreditlimitusage=ISNULL(custgroupcreditlimitusage,0.00)-" & ToDouble(sTotNetto.Text) & ", custgroupcreditlimitusagerupiah=Isnull(custgroupcreditlimitusagerupiah,0.00)-" & ToDouble(sTotNetto.Text) & " Where custgroupoid=" & custgroupoid & " And branch_code='" & sCodeCbg.Text & "'"
                End If
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "select ISNULL(custcreditlimitusagerupiah,0.00) from QL_mstcust Where custoid=" & Integer.Parse(sCustoid.Text) & " AND branch_code='" & sCodeCbg.Text & "'"
                xCmd.CommandText = sSql : Dim CustUsg As Double = xCmd.ExecuteScalar()
                'Else
                If ToDouble(CustUsg) < 0 Then
                    sSql = "Update QL_mstcust set custcreditlimitusagerupiah=ISNULL(custcreditlimitusagerupiah,0.00)+" & ToDouble(sTotNetto.Text) & ", custcreditlimitusage=ISNULL(custcreditlimitusage,0.00)+" & ToDouble(sTotNetto.Text) & " Where custoid=" & Integer.Parse(sCustoid.Text) & ""
                ElseIf ToDouble(CustGroupUsg) = 0 Then
                    sSql = "Update QL_mstcust set custcreditlimitusagerupiah=0, custcreditlimitusage=0 Where custoid=" & Integer.Parse(sCustoid.Text) & ""
                Else
                    sSql = "Update QL_mstcust set custcreditlimitusagerupiah=ISNULL(custcreditlimitusagerupiah,0.00)-" & ToDouble(sTotNetto.Text) & ", custcreditlimitusage=ISNULL(custcreditlimitusage,0.00)-" & ToDouble(sTotNetto.Text) & " Where custoid=" & Integer.Parse(sCustoid.Text) & ""
                End If

                'recovery credit limit
                sSql = "Update c set custcreditlimitusagerupiah = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid ),0), custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid ),0) from QL_mstcust c"

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString & " " & sSql, CompnyName & " - ERROR", 2)
            status.Text = "In Approval"
            Exit Sub
        End Try
        setSalesReturnActive()
    End Sub

    Public Function GetHppNya() As String
        Return ToMaskEdit(ToDouble(Eval("value")), 4)
    End Function

    Private Function GetTextBoxHppNya(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvASDtl.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function

    Private Function UpdateHppNya() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDtlAS") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtlAS")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True

                For C1 As Integer = 0 To gvASDtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvASDtl.Rows(C1)
                    dtView.RowFilter = "seq=" & row.Cells(0).Text
                    Dim hargabeli As Double = ToDouble(GetTextBoxHppNya(C1, 6))
                    dtView(0)("value") = ToDouble(GetTextBoxHppNya(C1, 6))
                    dtView.RowFilter = ""
                Next

                dtTbl = dtView.ToTable
                Session("TblDtlAS") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Protected Sub imgAppAs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAppAs.Click
        UpdateHppNya()
        Dim Msg As String = ""
        If IsDBNull(Session("draftno")) Then
            Msg &= "Maaf, pilih data stock adjusment..!!<br />"
        Else
            If Session("draftno") = "" Then
                Msg &= "Maaf, pilih data stock adjusment..!!<br />"
            End If
        End If
        Dim dtc As DataTable = Session("TblDtlAS")
        For C1 As Integer = 0 To dtc.Rows.Count - 1
            If ToDouble(dtc.Rows(C1).Item("value")) = 0 Then
                Msg &= "Maaf, Amount hpp belum di input, mohon untuk input amount hpp..!!<br />"
            End If
        Next

        Dim UsdRate As Double = Get_USDRate()
        If ToDouble(UsdRate) = 0 Then
            Msg &= "Rate Currency Belum Di isi !<br />"
            Exit Sub
        End If

        For C1 As Int16 = 0 To dtc.Rows.Count - 1
            If ToDouble(dtc.Rows(C1)("currqty").ToString) + ToDouble(dtc.Rows(C1)("newqty").ToString) < 0 Then
                Msg &= "Stok " & dtc.Rows(C1)("itemdesc").ToString & " Tidak Boleh Minus <BR>"
            End If
        Next

        If Msg <> "" Then
            showMessage(Msg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & branch.Text & "' And gengroup='CABANG'")
        Dim code As String = "ADJ/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

        Dim TblName As String = "QL_TRNSTOCKADJ"
        Dim statusUser As String = "" : Dim level As String = ""
        Dim maxLevel As String = ""
        Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
        Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
        Dim iGlSeq As Int16 = 1
        Dim iConMtrOid, iCrdMatOid, iGLMstOid, iGLDtlOid, iCrdoid As Integer
        Dim glValue, glIDRValue, glUSDValue As Double
        Dim sNo As String = "" : Dim reqcode As String = ""

        iConMtrOid = GenerateID("ql_conmtr", CompnyCode)
        iCrdMatOid = GenerateID("ql_crdmtr", CompnyCode)
        iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
        iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)
        iCrdoid = GenerateID("QL_crdmtr", CompnyCode)
        Dim conmtroid As Int32 = GenerateID("QL_conmtr", CompnyCode)
        Dim crdmatoid As Int32 = GenerateID("QL_crdmtr", CompnyCode)
        Dim tempoid As Int32 = 0

        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim isExec As Boolean = False
            'Dim cRate As New ClassRate
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim arStockAcctgOid() As Integer = {0, 0}
            Dim dt As DataTable = Session("TblDtlAS")
            Dim dv As DataView = dt.DefaultView

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(stockadjno,1) AS INT)),0) FROM ql_trnstockadj WHERE stockadjno LIKE '" & code & "%'"
            xCmd.CommandText = sSql : Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence : sNo = reqcode

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='" & TblName & "' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='Approved', event='Approved', approvaldate=CURRENT_TIMESTAMP, approvalstatus='Approved' WHERE statusrequest='New' AND event='In Approval' AND tablename='QL_trnstockadj' AND oid=" & Session("draftno")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For C1 As Int16 = 0 To dt.Rows.Count - 1
                sSql = "UPDATE QL_trnstockadj SET stockadjstatus='Approved', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP "
                If statusUser = "FINAL" Then
                    sSql &= ", approvaluser='" & Session("UserID") & "', approvaldatetime=CURRENT_TIMESTAMP, stockadjno='" & sNo & "', stockadjamtidr=" & ToDouble(dt.Rows(C1)("value")) & ", stockadjamtusd=" & ToDouble(dt.Rows(C1)("value")) & ", resfield2='" & dt.Rows(C1)("stockacctgoid") & "'"
                End If
                sSql &= " WHERE cmpcode='" & CompnyCode & "' AND resfield1=" & dt.Rows(C1)("resfield1") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If statusUser = "FINAL" Then
                    Dim lasthpp As Double = 0, NewHpp As Double = 0
                    sSql = "Select ISNULL(hpp,0.00) from ql_mstitem Where itemoid=" & dt.Rows(C1).Item("itemoid") & "" : xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                    Dim NewQty As Double = ToDouble(dt.Rows(C1).Item("newqty")) + ToDouble(dt.Rows(C1).Item("currqty"))
                    If ToDouble(NewQty) > 0 Then
                        NewHpp = (((ToDouble(dt.Rows(C1).Item("value")) * dt.Rows(C1).Item("newqty"))) + (lasthpp * ToDouble(dt.Rows(C1).Item("currqty")))) / (dt.Rows(C1).Item("newqty") + ToDouble(dt.Rows(C1).Item("currqty")))
                    Else
                        NewHpp = ToDouble(dt.Rows(C1).Item("value"))
                    End If
                    glValue = (-1 * dt.Rows(C1).Item("newqty")) * ToDouble(lasthpp)
                    glIDRValue = glValue
                    sSql = "Select max(OID) From QL_HistHPP"
                    xCmd.CommandText = sSql : Dim oidHis As Integer = xCmd.ExecuteScalar()

                    sSql = "INSERT INTO QL_HistHPP (cmpcode,oid,transOid,transName,refname,refoid,groupOid,lastHpp,newHpp,totalOldStock,qtyTrans,pricePerItem,periodacctg,updtime,upduser) values " & _
                    "('" & CompnyCode & "', " & oidHis + 1 & ", " & Session("draftno") & ", 'QL_trnstockadj', 'ADJ', " & dt.Rows(C1)("itemoid") & ", 0, " & ToDouble(lasthpp) & ", " & ToDouble(NewHpp) & ", " & ToDouble(dt.Rows(C1).Item("currqty")) & ", " & ToDouble(dt.Rows(C1).Item("newqty")) & ", " & ToDouble(ToDouble(dt.Rows(C1).Item("value"))) & ", '" & GetDateToPeriodAcctg(GetServerTime()) & "', current_timestamp, '" & Session("UserID") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '-----End History HPP Purchase DO'-----
                    'update oid in ql_mstoid where tablename = 'QL_HistHPP'

                    sSql = "Update ql_mstitem set HPP= " & NewHpp & " Where ql_mstitem.cmpcode='" & CompnyCode & "' and ql_mstitem.itemoid=" & dt.Rows(C1)("itemoid")
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update QL_mstoid set lastoid=" & oidHis + 1 & " where tablename = 'QL_HistHPP' and cmpcode = '" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            If statusUser = "FINAL" Then
                ' Insert GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,type) VALUES ('" & CompnyCode & "','" & branch.Text & "', " & iGLMstOid & ",CURRENT_TIMESTAMP, '" & GetPeriodAcctg(GetServerTime) & "', 'AS', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'ADJ')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim xBRanch As String = ""
                For R1 As Integer = 0 To dt.Rows.Count - 1
                    Dim COA_gudang As Integer = 0
                    Dim vargudang As String = "" : Dim iMatAccount As String = ""
                    sSql = "Select stockflag From ql_mstitem Where itemoid= " & dt.Rows(R1)("itemoid") & ""
                    xCmd.CommandText = sSql : Dim sTockFlag As String = xCmd.ExecuteScalar

                    sSql = "Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & dt.Rows(R1).Item("mtrwhoid") & " AND genother4='" & sTockFlag & "'"
                    xCmd.CommandText = sSql : xBRanch = xCmd.ExecuteScalar

                    If sTockFlag = "I" Then
                        iMatAccount = GetVarInterface("VAR_INVENTORY", xBRanch)
                        If iMatAccount = "?" Or iMatAccount = "0" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_INVENTORY' !!", CompnyName & " - WARNING", 2)
                            Session("click_post") = "false"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    Else
                        iMatAccount = GetVarInterface("VAR_GUDANG", xBRanch)
                        If iMatAccount = "?" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_GUDANG' !!", CompnyName & " - WARNING", 2)
                            Session("click_post") = "false"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    If iMatAccount Is Nothing Or iMatAccount = "" Then
                        showMessage("Interface untuk Akun VAR_GUDANG tidak ditemukan!", "WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & iMatAccount & "'" : xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar

                        If COA_gudang = 0 Or COA_gudang = Nothing Then
                            showMessage("Akun COA untuk VAR_GUDANG tidak ditemukan!", "WARNING", 2)
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    Dim COA_adjust_stock_plus As Integer = 0
                    Dim varadjustplus As String = GetVarInterface("VAR_ADJUST_STOCK+", dt.Rows(R1).Item("branch_code").ToString)
                    If varadjustplus Is Nothing Or varadjustplus = "" Then
                        showMessage("Akun COA untuk VAR_ADJUST_STOCK+ tidak ditemukan!", "WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & varadjustplus & "'"
                        xCmd.CommandText = sSql : COA_adjust_stock_plus = xCmd.ExecuteScalar
                        If COA_adjust_stock_plus = 0 Or COA_adjust_stock_plus = Nothing Then
                            showMessage("Akun COA untuk VAR_ADJUST_STOCK+ tidak ditemukan!", "WARNING", 2)
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    Dim COA_stock_opname As Integer = 0
                    Dim varadjustmin As String = GetVarInterface("VAR_ADJUST_STOCK-", dt.Rows(R1).Item("branch_code").ToString)
                    If varadjustmin Is Nothing Or varadjustmin = "" Then
                        showMessage("Interface untuk Akun VAR_ADJUST_STOCK- tidak ditemukan!", "WARNING", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE = '" & varadjustmin & "'" : xCmd.CommandText = sSql : COA_stock_opname = xCmd.ExecuteScalar

                        If COA_stock_opname = 0 Or COA_stock_opname = Nothing Then
                            showMessage("Akun COA untuk VAR_ADJUST_STOCK- tidak ditemukan!", "WARNING", 2)
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    'update stock inventory(-)( biaya(D)-persediaan(C))
                    Dim NewHpp As Double = 0 : Dim lasthpp As Double = 0
                    If dt.Rows(R1).Item("newqty") < 0 Then
                        If ToDouble(dt.Rows(R1).Item("currqty")) > 0 Then
                            sSql = "Select hpp from ql_mstitem Where itemoid=" & dt.Rows(R1).Item("itemoid") & "" : xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                            glValue = (-1 * dt.Rows(R1).Item("newqty")) * ToDouble(lasthpp)
                            glIDRValue = glValue
                        Else
                            If ToDouble(dt.Rows(R1).Item("value")) <> 0 Then
                                lasthpp = ToDouble(dt.Rows(R1).Item("value"))
                                glValue = (-1 * dt.Rows(R1).Item("newqty")) * ToDouble(lasthpp)
                                glIDRValue = glValue
                            Else
                                glIDRValue = 0
                            End If
                        End If

                        sSql = "INSERT INTO ql_conmtr (cmpcode,conmtroid, type, trndate, periodacctg, formaction, formoid, Formname, refoid, refname,unitoid, mtrlocoid,branch_code, qtyin, qtyout, reason, upduser, updtime, typemin, personoid,amount,note,HPP) VALUES" & _
                        " ('" & CompnyCode & "'," & iConMtrOid & ",'AS',CURRENT_TIMESTAMP,'" & GetPeriodAcctg(GetServerTime()) & "','" & sNo & "',0,''," & dt.Rows(R1)("itemoid") & ",'QL_MSTITEM'," & 945 & ",'" & dt.Rows(R1)("mtrwhoid") & "','" & dt.Rows(R1).Item("branch_code").ToString & "',0," & -1 * dt.Rows(R1).Item("newqty") & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble((-1 * dt.Rows(R1).Item("newqty")) * ToDouble(dt.Rows(R1).Item("value"))) & ",'STOCK ADJUSTMENT - " & dt.Rows(R1)("note").ToString & "','" & ToDouble(dt.Rows(R1).Item("value")) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iConMtrOid += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & dt.Rows(R1).Item("branch_code").ToString & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", '" & COA_stock_opname & "','D', " & glValue & "," & glIDRValue & "," & glIDRValue & ",'" & sNo & "', 'ADJUSTMENT STOCK', '', '','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & dt.Rows(R1).Item("branch_code").ToString & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ",'C', " & (-1 * dt.Rows(R1).Item("newqty")) * ToDouble(lasthpp) & "," & (-1 * dt.Rows(R1).Item("newqty")) * ToDouble(lasthpp) & " , " & glIDRValue & " , '" & sNo & "', 'ADJUSTMENT STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1
                    End If

                    'jika ada stock yg positif(persediaan(D), pendapatan(C)
                    If dt.Rows(R1).Item("newqty") > 0 Then
                        If ToDouble(dt.Rows(R1).Item("currqty")) > 0 Then
                            sSql = "Select hpp from ql_mstitem Where itemoid=" & dt.Rows(R1).Item("itemoid") & "" : xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                            NewHpp = (((ToDouble(dt.Rows(R1).Item("value")) * dt.Rows(R1).Item("newqty"))) + (lasthpp * ToDouble(dt.Rows(R1).Item("currqty")))) / (dt.Rows(R1).Item("newqty") + ToDouble(dt.Rows(R1).Item("currqty")))
                        Else
                            If ToDouble(dt.Rows(R1).Item("value")) <> 0 Then
                                NewHpp = (((ToDouble(dt.Rows(R1).Item("value")) * dt.Rows(R1).Item("newqty"))) + (lasthpp * ToDouble(dt.Rows(R1).Item("currqty")))) / (dt.Rows(R1).Item("newqty") + ToDouble(dt.Rows(R1).Item("currqty")))
                            Else
                                showMessage("Maaf, Amount hpp belum di input, mohon untuk input amount hpp..!!", CompnyName & " - WARNING", 2)
                                objTrans.Rollback() : conn.Close()
                                Exit Sub
                            End If
                        End If

                        'update stock inventory(+)
                        sSql = "INSERT INTO ql_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, formaction, formoid, Formname, refoid, refname,unitoid, mtrlocoid,branch_code, qtyin, qtyout, reason, upduser, updtime, typemin, personoid,amount,note,HPP) VALUES " & _
                        "('" & CompnyCode & "', " & iConMtrOid & ", 'AS', CURRENT_TIMESTAMP, '" & GetPeriodAcctg(GetServerTime()) & "', '" & sNo & "'," & 0 & ",'QL_trnstockadj', " & dt.Rows(R1)("itemoid") & ", 'QL_MSTITEM'," & 945 & ",'" & dt.Rows(R1)("mtrwhoid") & "','" & dt.Rows(R1).Item("branch_code").ToString & "', " & dt.Rows(R1).Item("newqty") & ",0,'', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & 0 & "," & 0 & ",'" & dt.Rows(R1).Item("newqty") * ToDouble(NewHpp) & "', 'STOCK ADJUSTMENT - " & dt.Rows(R1)("note").ToString & "','" & ToDouble(NewHpp) & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iConMtrOid += 1

                        glUSDValue = ToDouble(glValue) / ToDouble(UsdRate)
                        sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & dt.Rows(R1).Item("branch_code").ToString & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ", 'D', " & dt.Rows(R1).Item("newqty") * ToDouble(NewHpp) & "," & dt.Rows(R1).Item("newqty") * ToDouble(NewHpp) & " , " & ToDouble(NewHpp) & " , '" & sNo & "','ADJUSTMENT STOCK', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime) VALUES ('" & CompnyCode & "','" & dt.Rows(R1).Item("branch_code").ToString & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", '" & COA_adjust_stock_plus & "','C', " & dt.Rows(R1).Item("newqty") * ToDouble(NewHpp) & "," & dt.Rows(R1).Item("newqty") * ToDouble(NewHpp) & "," & NewHpp & ",'" & sNo & "', 'ADJUSTMENT STOCK', '', '','POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1
                    End If
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & iConMtrOid - 1 & " WHERE tablename='ql_conmtr' AND cmpcode='" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & igldtl - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setASasActive()
        imgBackAS_Click(Nothing, Nothing)
    End Sub

    Protected Sub lkbCN_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCN.Click
        setCNasActive()
    End Sub

    Protected Sub gvCNDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    

    Protected Sub imgRejectCN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("draftno")) Then
            showMessage("Please select a Credit Note First", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("draftno") = "" Then
                showMessage("Please select a Credit Note First", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If oid.Text = "" Then
            showMessage("Please select a Credit Note First", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            sSql = "UPDATE ql_creditnote SET cnstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND oid = '" & Session("draftno") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='ql_creditnote' and oid=" & Session("draftno") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        setCNasActive()
    End Sub

    Protected Sub imgBackCN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setCNasActive()
    End Sub

    Protected Sub lkbDN_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDN.Click
        setDNasActive()
    End Sub

    Protected Sub lkbSelectDN_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("draftno") = sender.ToolTip : MultiView1.SetActiveView(View39)
        Session("approvaloid") = sender.CommandArgument

        Dim jbt As String = GetStrData("Select POSITION from QL_MSTPROF Where BRANCH_CODE='" & Session("branch_id") & "' AND USERID='" & Session("UserID") & "'")
        Dim fCb As String = ""
        If jbt.ToString.ToUpper <> "ANALISA KEU. PUSAT" Then
            fCb &= "AND ad.branch_code='" & Session("branch_id") & "'"
        End If

        Dim dt As DataTable = cKoneksi.ambiltabel("select distinct (case when cn.reftype = 'AR' then (select j.trnjualmstoid from QL_trnjualmst j where j.trnjualmstoid = cn.refoid) when cn.reftype = 'NT' then (select j.trnbiayaeksoid from ql_trnbiayaeksmst j where j.trnbiayaeksoid = cn.refoid) else (select b.trnbelimstoid from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) end) trnjualbelioid,(case when cn.reftype = 'AR' then (select j.trnjualno from QL_trnjualmst j where j.trnjualmstoid = cn.refoid) when cn.reftype = 'NT' then (select j.trnbiayaeksno from ql_trnbiayaeksmst j where j.trnbiayaeksoid = cn.refoid) else (select b.trnbelino from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) end) trnjualbelino,(case when cn.reftype = 'AP' then (select s.suppoid from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) else (select c.custoid from QL_mstcust c where c.custoid = cn.cust_supp_oid) end) custsuppoid,(case when cn.reftype = 'AR' then (select s.suppname from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) else (select c.custname from QL_mstcust c where c.custoid = cn.cust_supp_oid) end) custsuppname, coa_credit, coa_debet,(case when cn.reftype = 'AR' then cn.branch_code when cn.reftype = 'NT' then cn.branch_code  else (select b.branch_code  from QL_trnbelimst b where b.trnbelimstoid = cn.refoid) end) branch_code,cn.dnstatus, cn.amount, cn.upduser lastupdby, CONVERT(varchar(10), cn.updtime, 101) lastupdon,ap.approvaloid, cn.oid draftno, convert(varchar(10),cn.tgl,103) tgl, cn.reftype, ap.REQUESTUSER, ap.REQUESTDATE from QL_DebitNote cn inner join QL_APPROVAL ap on ap.OID=cn.oid and ap.CMPCODE=cn.cmpcode where ap.TABLENAME='ql_debitnote' and ap.STATUSREQUEST='new' and ap.EVENT='In Approval' and cn.cmpcode like '%" & CompnyCode & "%' " & fCb & "  And ap.APPROVALUSER='" & Session("UserID") & "' and cn.dnstatus='In Approval' and cn.oid='" & Session("draftno") & "'", "ql_debitnote")

        oiddn.Text = dt.Rows(0).Item("draftno")
        tgldn.Text = CDate(toDate(dt.Rows(0).Item("tgl")))
        reftypedn.Text = dt.Rows(0).Item("reftype")
        amountdn.Text = ToMaskEdit(dt.Rows(0).Item("amount"), 4)
        upduserdn.Text = dt.Rows(0).Item("lastupdby")
        updtimedn.Text = dt.Rows(0).Item("lastupdon")
        branch_code_dn.Text = dt.Rows(0).Item("branch_code")
        cust_supp_oiddn.Text = dt.Rows(0).Item("custsuppoid")
        trnjualbelioiddn.Text = dt.Rows(0).Item("trnjualbelioid")
        trnjualbelinodn.Text = dt.Rows(0).Item("trnjualbelino")
        cnstatusdn.Text = dt.Rows(0).Item("dnstatus")
        coa_creditdn.Text = dt.Rows(0).Item("coa_credit")
        coa_debetdn.Text = dt.Rows(0).Item("coa_debet")

        Dim periodacctg As String = GetPeriodAcctg(GetServerTime())
        'poidentifierno.Text = dt.Rows(0).Item("identifierno").ToString

        'Dim xDT As DataTable = cKoneksi.ambiltabel("SELECT ISNULL(po.digit,2) as digitX " & _
        '"FROM QL_pomst po WHERE trnbelimstoid='" & Session("pomstoid") & "'", "tbSmt")
        'dgt = xDT.Rows(0).Item("digitX")

        sSql = "select oid,0 seq,(case when reftype = 'AR' then (select j.trnjualno from QL_trnjualmst j where j.trnjualmstoid = cn.refoid) when reftype = 'NT' then (select j.trnbiayaeksno from QL_trnbiayaeksmst j where j.trnbiayaeksoid = cn.refoid) else (select j.trnbelino from ql_trnbelimst j where j.trnbelimstoid = cn.refoid) end) trnjualno, (case when reftype = 'AP' then (select s.suppname from QL_mstsupp s where s.suppoid = cn.cust_supp_oid) else (select c.custname from QL_mstcust c where c.custoid = cn.cust_supp_oid)  end) custname, amount, note from QL_debitNote cn where cn.cmpcode='" & CompnyCode & "' and cn.oid=" & Session("draftno") & " and cn.dnstatus <> 'Approved'"
        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "ql_debitnotedtl")
        Session("TblDtlDN") = tbDtl
        ClassFunction.FillGV(gvDNDtl, sSql, "ql_debitnotedtl")
    End Sub

    Protected Sub gvDNDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub imgAppDN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("draftno")) Then
            showMessage("Please select Debit Note Data first", CompnyName & " - WARNING", 2)
            Exit Sub
        Else
            If Session("draftno") = "" Then
                showMessage("Please select Debit Note Data first", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim isExec As Boolean = False
            Dim cRate As New ClassRate
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim idConaR As Integer, idConap As Integer, iGLMstOid As Integer, iGLDtlOid As Integer
            Dim arStockAcctgOid() As Integer = {0, 0}
            Dim dt As DataTable = Session("TblDtlDN")
            Dim dv As DataView = dt.DefaultView
            Dim UsdRate As Double = Get_USDRate()
            If ToDouble(UsdRate) = 0 Then
                showMessage("Rate Currency Belum Di isi !", "-WARNING", 2)
                Exit Sub
            End If
            Dim glValue, glIDRValue, glUSDValue As Double
            Dim sNo As String = "" : Dim reqcode As String = ""
            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & branch_code_dn.Text & "' And gengroup='CABANG'")
            code = "DN/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(no,4) AS INT)),0) FROM ql_debitnote WHERE no LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            sNo = reqcode

            Dim TblName As String = "" : TblName = "ql_debitnote"
            Dim statusUser As String = "" : Dim level As String = ""
            Dim maxLevel As String = ""
            Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
            Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
            Dim iGlSeq As Int16 = 1
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' ")
            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='" & TblName & "' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                cRate.SetRateValue(1, sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
                End If

                idConaR = GenerateID("ql_conar", CompnyCode)
                idConap = GenerateID("ql_conap", CompnyCode)
                iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
                iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)

                Dim conmtroid As Int32 = GenerateID("QL_conmtr", CompnyCode)
                Dim crdmatoid As Int32 = GenerateID("QL_crdmtr", CompnyCode)
                Dim tempoid As Int32 = 0
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='Approved', event='Approved', approvaldate=CURRENT_TIMESTAMP, approvalstatus='Approved' WHERE statusrequest='New' AND event='In Approval' AND tablename='ql_debitnote' AND oid=" & Session("draftno")
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            For C1 As Int16 = 0 To dt.Rows.Count - 1
                sSql = "UPDATE ql_debitnote SET dnstatus='Approved', tgl = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP "
                If statusUser = "FINAL" Then
                    sSql &= ", no='" & sNo & "', amountidr=" & ToDouble(dt.Rows(C1)("amount").ToString) & ", amountusd=" & ToDouble(dt.Rows(C1)("amount").ToString) * cRate.GetRateMonthlyIDRValue & ""
                End If
                sSql &= " WHERE cmpcode='" & CompnyCode & "' AND oid=" & dt.Rows(C1)("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            If reftypedn.Text = "AR" Then
                'sSql = "DELETE FROM QL_conar WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_debitnote' AND payrefoid=" & oiddn.Text & " and branch_code='" & branch_code_dn.Text & "'"
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


                sSql = " UPDATE QL_trnjualmst SET accumpayment=accumpayment-" & ToDouble(amountdn.Text) & ",accumpaymentidr = accumpayment-" & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue & ",accumpaymentusd = accumpaymentusd-" & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue & "   WHERE trnjualmstoid=" & trnjualbelioiddn.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & branch_code_dn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim custgroupoid As String = GetStrData("select custgroupoid from ql_mstcust where custoid = " & cust_supp_oiddn.Text & "")
                If custgroupoid = "" Then
                    showMessage("Customer ini belum mempunyai Customer Group", CompnyName & " - WARNING", 2) : Exit Sub
                End If
                'update credit limit customer
                sSql = "UPDATE QL_mstcustgroup SET custgroupcreditlimitusagerupiah=custgroupcreditlimitusagerupiah-" & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue & " WHERE custgroupoid=" & custgroupoid & " and branch_code = '" & branch_code_cn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim tglx As String = ""
                tglx = tgl.Text

                sSql = "INSERT INTO QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime) VALUES ('" & CompnyCode & "','" & branch_code_dn.Text & "', " & idConaR & ", 'ql_debitnote',  " & trnjualbelioiddn.Text & ", " & oiddn.Text & ", " & cust_supp_oiddn.Text & ", " & coa_debetdn.Text & ", 'POST', 'DNAR', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_creditdn.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & sNo & "', 0, CURRENT_TIMESTAMP, 0,0,0, " & ToDouble(amountdn.Text) * -1 & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue * -1 & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue * -1 & ", 'DN No. " & sNo & " for A/R | No. " & trnjualbelinodn.Text & "', '','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf reftypedn.Text = "NT" Then
                sSql = "INSERT INTO QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime) VALUES ('" & CompnyCode & "','" & branch_code_dn.Text & "', " & idConaR & ", 'ql_debitnote',  " & trnjualbelioiddn.Text & ", " & oiddn.Text & ", " & cust_supp_oiddn.Text & ", " & coa_debetdn.Text & ", 'POST', 'DNNT', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_creditdn.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & sNo & "', 0, CURRENT_TIMESTAMP, 0,0,0, " & ToDouble(amountdn.Text) * -1 & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue * -1 & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue * -1 & ", 'DN No. " & sNo & " for NT | No. " & trnjualbelinodn.Text & "', '','" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                'sSql = "DELETE FROM QL_conap WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_debitnote' AND payrefoid=" & oiddn.Text
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = " UPDATE QL_trnbelimst SET accumpayment=accumpayment+" & ToDouble(amountdn.Text) & ",accumpaymentidr=accumpayment+" & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue & ",accumpaymentusd=accumpaymentusd+" & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue & "  WHERE trnbelimstoid=" & trnjualbelioiddn.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & branch_code_dn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


                sSql = "INSERT INTO QL_conap (cmpcode,branch_code,conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnapnote, trnapres1, upduser, updtime) VALUES " & _
                "('" & CompnyCode & "','" & branch_code_dn.Text & "', " & idConap & ", 'ql_debitnote',  " & trnjualbelioiddn.Text & ", " & oiddn.Text & ", " & cust_supp_oiddn.Text & ", " & coa_debetdn.Text & ", 'POST', 'DNAP', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_creditdn.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '', 0, CURRENT_TIMESTAMP, 0,0,0, " & ToDouble(amountdn.Text) & "," & (ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue) & "," & (ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue) & ", 'DN No. " & sNo & " for A/P | No. " & trnjualbelinodn.Text & "', '', '" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & idConap & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conap'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If


            '//////INSERT INTO TRN GL MST
            sSql = "INSERT into QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type) VALUES ('" & CompnyCode & "','" & branch_code_dn.Text & "'," & iGLMstOid & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(tgldn.Text) & "','DN No. " & sNo & " for " & reftypedn.Text & " No. " & trnjualbelinodn.Text & "','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'DN')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' =================================
            '       
            '   BIAYA/OTHER     99000
            '           Piutang/hutang  100000
            ' =================================
            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
            " ('" & CompnyCode & "','" & branch_code_dn.Text & "'," & iGLDtlOid & ", 1," & iGLMstOid & "," & coa_debetdn.Text & ",'D'," & ToDouble(amountdn.Text) & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue & ",'" & sNo & "','DN No. " & sNo & " for " & reftypedn.Text & " No. " & trnjualbelinodn.Text & "','ql_debitnote','" & oiddn.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'POST')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            iGLDtlOid += 1

            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES ('" & CompnyCode & "','" & branch_code_dn.Text & "'," & iGLDtlOid & ",2," & iGLMstOid & "," & coa_creditdn.Text & ",'C'," & ToDouble(amountdn.Text) & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(amountdn.Text) * cRate.GetRateMonthlyUSDValue & ",'" & sNo & "','DN No. " & sNo & " for " & reftypedn.Text & " No. " & trnjualbelinodn.Text & "','ql_debitnote','" & oiddn.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'POST')"

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE  QL_mstoid SET lastoid=" & iGLDtlOid & " WHERE tablename='QL_trngldtl' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE  QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_trnglmst' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'recovery credit limit
            sSql = "Update c set custcreditlimitusagerupiah= isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid  ),0),custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid  ),0)  from QL_mstcust c  "

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    'UpdateMA(sNo, sStatus)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERROR", 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERROR", 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setDNasActive()
        imgBackDN_Click(Nothing, Nothing)
    End Sub

    Protected Sub imgBackDN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        setDNasActive()
    End Sub

    Protected Sub imgRejectDN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsDBNull(Session("draftno")) Then
            showMessage("Please select a Debit Note First", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("draftno") = "" Then
                showMessage("Please select a Debit Note First", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If oiddn.Text = "" Then
            showMessage("Please select a Debit Note First", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Update PO
            sSql = "UPDATE ql_debitnote SET dnstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND oid = '" & Session("draftno") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='ql_debitnote' and oid=" & Session("draftno") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        setDNasActive()
    End Sub

    Protected Sub lkbTWService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbTWService.Click
        Session("Type") = "TWService"
        setTWServiceasActive()
    End Sub

    Private Sub setTWServiceasActive()
        bindTWService() : MultiView1.SetActiveView(View3)
    End Sub

    Private Sub bindTWService()
        Dim QlSO As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trfwhservicemst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeSO As String = ""
        Dim SoCode() As String = QlSO.Split(",")
        For C1 As Integer = 0 To SoCode.Length - 1
            If SoCode(C1) <> "" Then
                sCodeSO &= "'" & SoCode(C1).Trim & "',"
            End If
        Next

        sSql = "select tw.Trfwhserviceoid, tw.tobranch, b.approvaloid, tw.TrfwhserviceNo, tw.Trfwhservicedate, tw.upduser, b.requestuser, b.requestdate, tw.frombranch, b.branch_code from ql_trfwhservicemst tw inner join ql_approval b on tw.Trfwhserviceoid = b.oid AND tw.ToBranch = b.branch_code where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("userid") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.Trfwhservicestatus = 'in approval' and b.tablename = 'QL_trfwhservicemst' and tw.tobranch IN (" & Left(sCodeSO, sCodeSO.Length - 1) & ") AND tw.Trfwhservicetype='INTERNAL' order by tw.Trfwhserviceoid desc"
        'and b.approvaluser = '" & Session("userid") & "'
        FillGV(gvTWService, sSql, "GVTWService")
    End Sub

    Protected Sub lkbSelectTWService_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trfwhserviceoid") = sender.ToolTip : MultiView1.SetActiveView(View4)
        Session("approvaloid") = sender.CommandArgument
        Session("CodeC") = sender.CommandName

        sSql = "SELECT tw.TrfwhserviceNo, tw.Trfwhservicedate, tw.upduser,(select gendesc from QL_mstgen where gencode = tw.frombranch AND gengroup='CABANG') fromMtrBranch,((select gendesc from QL_mstgen where genoid = (select genother1 from QL_mstgen where genoid = tw.FromMtrLocOid AND gengroup='LOCATION'))+' - '+ b.gendesc) gudangasal,(select gendesc from QL_mstgen where gencode = tw.toBranch AND gengroup='CABANG') toMtrBranch,((select gendesc from QL_mstgen where genoid = (select genother1 from QL_mstgen where genoid = tw.ToMtrlocOid AND gengroup='LOCATION'))+' - '+ c.gendesc) gudangtujuan, c.genoid tujuancode,tw.fromBranch DariBranch, tw.toBranch KirimKe FROM ql_trfwhservicemst tw inner join QL_mstgen b on tw.cmpcode = b.cmpcode and tw.FromMtrlocoid = b.genoid inner join QL_mstgen c on tw.cmpcode = c.cmpcode and tw.ToMtrlocOid = c.genoid WHERE tw.frombranch ='" & Session("CodeC") & "' and tw.Trfwhserviceoid = " & Session("trfwhserviceoid") & " and tw.cmpcode = '" & CompnyCode & "'"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "twsdtl")
        twsno.Text = dt.Rows(0).Item("TrfwhserviceNo")
        twsdate.Text = Format(dt.Rows(0).Item("Trfwhservicedate"), "dd/MM/yyyy")
        twsuser.Text = dt.Rows(0).Item("upduser")
        twsfrombranch.Text = dt.Rows(0).Item("fromMtrBranch")
        twsfromloc.Text = dt.Rows(0).Item("gudangasal")
        twstobranch.Text = dt.Rows(0).Item("toMtrBranch")
        twstoloc.Text = dt.Rows(0).Item("gudangtujuan")
        Session("DariBranch") = dt.Rows(0).Item("DariBranch")
        Session("KirimKe") = dt.Rows(0).Item("KirimKe")
        Session("codegudangtujuan") = dt.Rows(0).Item("tujuancode")

        sSql = "Select td.trfwhservicedtlseq seq, i.itemdesc + ' - ' + i.merk itemdesc, td.trfwhserviceqty qty, '' AS unit, td.trfwhservicedtlnote note,'' AS statusexp,'' AS typedimensi,'' AS beratvolume,'' AS beratbarang,'' AS jenisexp,'' AS amtexpedisi,Isnull(td.reqdtloid,0) reqdtloid,isnull(reqoid,0) reqoid, td.itemoid, isnull((select reqqty from QL_TRNREQUESTDTL rd where rd.reqdtloid = td.reqdtloid),0.00) reqqty From ql_trfwhservicedtl td inner join ql_mstitem i on td.cmpcode = i.cmpcode and td.itemoid = i.itemoid Where td.trfwhserviceoid = " & Session("trfwhserviceoid") & " and td.cmpcode = '" & CompnyCode & "'"
        'FillGV(gvTWServicedtl, sSql, "GVTWServiceDtl")
        Dim objdtl As DataTable = cKon.ambiltabel(sSql, "gvTWServicedtl")
        gvTWServicedtl.DataSource = objdtl : gvTWServicedtl.DataBind()
        gvTWServicedtl.SelectedIndex = -1 : gvTWServicedtl.Visible = True
        Session("gvTWServicedtl") = objdtl
    End Sub

    Protected Sub btnTwServiceApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTwServiceApp.Click
        If IsDBNull(Session("trfwhserviceoid")) Then
            showMessage("Pilih transfer warehouse service yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trfwhserviceoid") = "" Then
                showMessage("Pilih transfer warehouse service yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If

        If twsno.Text = "" Then
            showMessage("Pilih transfer warehouse service yang akan diapprove terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "select trfwhservicestatus from ql_trfwhservicemst where trfwhserviceoid = " & Integer.Parse(Session("trfwhserviceoid")) & ""
            xCmd.CommandText = sSql : Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse service tidak bisa diapprove karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse service tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Approved', finalapprovaluser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = '" & Session("trfwhserviceoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Approved', approvaldate = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "select trfwhservicedate from QL_trfwhservicemst WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = " & Session("trfwhserviceoid") & ""
            xCmd.CommandText = sSql
            Dim transdate As Date = xCmd.ExecuteScalar

            Dim iCurID As Integer = 0
            Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("CodeC") & "' And gengroup='CABANG'")

            Dim twno As String = "TWS/" & cabang & "/" & Format(transdate, "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trfwhserviceno,4) AS INT)),0) FROM QL_trfwhservicemst WHERE trfwhserviceno LIKE '" & twno & "%'"
            xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
            twno = GenNumberString(twno, "", iCurID, 4)

            sSql = "UPDATE QL_trfwhservicemst SET trfwhserviceno = '" & twno & "' WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = '" & Session("trfwhserviceoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar

            Dim period As String = GetDateToPeriodAcctg(transdate).Trim
            sSql = "Select td.trfwhservicedtloid, td.itemoid AS refoid, td.trfwhserviceqty AS qty, 0 AS unitoid, td.trfwhserviceqty AS qty_to,0 AS unitoid_to, td.trfwhservicedtlnote,tm.FromBranch, tm.FromMtrLocOid,tm.ToBranch, tm.ToMtrLocOId,Isnull(td.reqdtloid,0) reqdtloid From ql_trfwhservicedtl td inner join ql_trfwhservicemst tm on tm.cmpcode = td.cmpcode and tm.trfwhserviceoid = td.trfwhserviceoid WHERE td.cmpcode = '" & CompnyCode & "' AND tm.trfwhserviceoid = " & Session("trfwhserviceoid") & ""

            Dim dset As DataSet = New Data.DataSet
            Dim dadapter As SqlDataAdapter = New SqlDataAdapter(sSql, conn)
            dadapter.SelectCommand.Transaction = objTrans
            dadapter.Fill(dset, "dtab")
            Dim dtab As DataTable = dset.Tables("dtab")
            Dim SumHpp As Double = 0

            'Generate trnglmst ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql
            Dim glmstoid As Integer = xCmd.ExecuteScalar + 1

            'Generate trngldtl ID
            sSql = "SELECT ISNULL(lastoid, 0) FROM QL_mstoid WHERE tablename = 'QL_trngldtl' AND cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql

            Dim gldtloid As Integer = xCmd.ExecuteScalar + 1
            Dim glsequence As Integer = 1

            For i As Integer = 0 To dtab.Rows.Count - 1
                '--------------------------
                Dim lasthpp As Double = 0
                sSql = "select hpp from ql_mstitem where itemoid=" & dtab.Rows(i).Item("refoid") & "" : xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                'SumHpp += ToDouble(lasthpp)
                '--------------------------
                'Update crdmtr for item out

                sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & dtab.Rows(i).Item("qty_to") & ", saldoakhir = saldoakhir - " & dtab.Rows(i).Item("qty_to") & ", LastTransType = 'TWS', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "' and branch_code = '" & dtab.Rows(i).Item("fromBranch") & "' AND mtrlocoid = " & dtab.Rows(i).Item("FromMtrlocoid") & " AND refoid = " & dtab.Rows(i).Item("refoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                conmtroid = conmtroid + 1
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code,conrefoid) " & _
                "VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TWS', '" & transdate & "', '" & period & "', '" & twno & "', " & dtab.Rows(i).Item("trfwhservicedtloid") & ", 'QL_trfwhservicemst', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', 941, " & dtab.Rows(i).Item("FromMtrlocoid") & ", 0, " & dtab.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtab.Rows(i).Item("trfwhservicedtlnote")) & "', " & ToDouble(lasthpp) & ", '" & dtab.Rows(i).Item("fromBranch") & "'," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update crdmtr for item in
                '(NEW KASUS , Approved Gudang Masuk ke BARANG PERJALANAN ) ID -10
                sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & dtab.Rows(i).Item("qty_to") & ", saldoakhir = saldoakhir + " & dtab.Rows(i).Item("qty_to") & ", LastTransType = 'TWS', lastTrans = '" & transdate & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & CompnyCode & "'and branch_code = '" & dtab.Rows(i).Item("toBranch") & "' AND mtrlocoid = -9 AND refoid = " & dtab.Rows(i).Item("refoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & period & "'"
                xCmd.CommandText = sSql

                If xCmd.ExecuteNonQuery() <= 0 Then
                    'Insert crdmtr if no record found
                    crdmtroid = crdmtroid + 1
                    sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) " & _
                    "VALUES ('" & CompnyCode & "', " & crdmtroid & ", '" & period & "', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', -9, '" & dtab.Rows(i).Item("toBranch") & "', " & dtab.Rows(i).Item("qty_to") & ", 0, 0, 0, 0, " & dtab.Rows(i).Item("qty_to") & ", 0, 'TWS', '" & transdate & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                conmtroid = conmtroid + 1
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, branch_code, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                "VALUES ('" & CompnyCode & "', " & conmtroid & ", 'TWS', '" & transdate & "', '" & period & "', '" & twno & "', " & dtab.Rows(i).Item("trfwhservicedtloid") & ", 'QL_trfwhservicemst', " & dtab.Rows(i).Item("refoid") & ", 'QL_MSTITEM', 941, -9, '" & dtab.Rows(i).Item("toBranch") & "', " & dtab.Rows(i).Item("qty") & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, 0, '" & Tchar(dtab.Rows(i).Item("trfwhservicedtlnote")) & "', " & ToDouble(lasthpp) & "," & Integer.Parse(dtab.Rows(i).Item("reqdtloid")) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ''( DONE NEW KASUS , Approved Gudang Masuk ke BARANG PERJALANAN ) ID -10
            Next

            sSql = "update ql_mstoid set lastoid = " & conmtroid & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & crdmtroid & " Where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & conmtroid & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "update ql_mstoid set lastoid = " & crdmtroid & " Where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setTWServiceasActive()
    End Sub

    Protected Sub btnTWServiceBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTWServiceBack.Click
        setTWServiceasActive()
    End Sub

    Protected Sub btnTwServiceRej_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTwServiceRej.Click
        If IsDBNull(Session("trfwhserviceoid")) Then
            showMessage("Pilih transfer warehouse service yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        Else
            If Session("trfwhserviceoid") = "" Then
                showMessage("Pilih transfer warehouse service yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If

        If twsno.Text = "" Then
            showMessage("Pilih transfer warehouse service yang akan direject terlebih dahulu!", CompnyName & " - Warning", 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "select trfwhservicestatus from QL_trfwhservicemst where trfwhserviceoid = " & Integer.Parse(Session("trfwhserviceoid")) & ""
            xCmd.CommandText = sSql
            Dim stat As String = xCmd.ExecuteScalar
            If stat.ToLower = "approved" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse service tidak bisa direject karena telah berstatus 'Approved'!", CompnyName & " - Warning", 2)
                Exit Sub
            ElseIf stat Is Nothing Or stat = "" Then
                objTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage("Transfer warehouse service tidak ditemukan!", CompnyName & " - Warning", 2)
                Exit Sub
            End If

            sSql = "UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = '" & Session("trfwhserviceoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim dt As DataTable = Session("gvTWServicedtl")
            For i As Integer = 0 To dt.Rows.Count - 1
                If ToDouble(dt.Rows(i).Item("qty")) >= ToDouble(dt.Rows(i).Item("reqqty")) Then
                    sSql = "UPDATE QL_trnrequestdtl SET reqdtlres1='', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND reqdtloid=" & Integer.Parse(dt.Rows(i).Item("reqdtloid")) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnrequest SET reqres1='', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND reqoid=" & Integer.Parse(dt.Rows(i).Item("reqoid")) & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setTWServiceasActive()
    End Sub

    Protected Sub imbTWServiceRev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbTWServiceRev.Click
        lbl_reviseTWService.Visible = True
        lbl_reviseTWService.Text = "Reason For Revision"
        lbl_reviseTWService.Visible = True
        lbl_reviseTWService.Focus()
        btnTwServiceApp.Visible = False
        btnTwServiceRej.Visible = False
        imbTWServiceRev.Visible = False
        btnTWServiceBack.Visible = False

        btnReviseTWService.Visible = True
        btnCancelTWService.Visible = True
    End Sub

    Protected Sub btnReviseTWService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReviseTWService.Click
        If txt_reviseTWService.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_trfwhservicemst SET trfwhservicstatus = 'Revised' , revisenote = '" & Tchar(txt_reviseTW.Text) & "' WHERE trfwhserviceoid = " & twsno.Text & " and toBranch = '" & Session("branch_id") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='QL_trfwhservicemst' and oid=" & twno.Text & " and branch_code ='" & Session("KirimKe") & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub btnCancelTWService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTWService.Click
        lbl_reviseTWService.Visible = False
        lbl_reviseTWService.Text = "Reason For Revision"
        lbl_reviseTWService.Visible = False
        lbl_reviseTWService.Focus()
        btnTwServiceApp.Visible = True
        btnTwServiceRej.Visible = True
        imbTWServiceRev.Visible = True
        btnTWServiceBack.Visible = True

        btnReviseTWService.Visible = False
        btnCancelTWService.Visible = False
    End Sub

    Private Sub setPromSupp()
        BindPromSupp() : MultiView1.SetActiveView(View16)
    End Sub

    Private Sub BindPromSupp()
        Dim QlPro As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstpromosupp' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodePro As String = ""
        Dim ProCode() As String = QlPro.Split(",")
        For C1 As Integer = 0 To ProCode.Length - 1
            If ProCode(C1) <> "" Then
                sCodePro &= "'" & ProCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select pm.promooid,pm.promoname, pm.branch_code, b.approvaloid, pm.promocode, pm.crttime, pm.upduser, b.requestuser, b.requestdate,(Select gendesc from QL_mstgen gc Where gc.gencode=pm.branch_code) Cabang From QL_mstpromosupp pm inner join ql_approval b on pm.promooid= b.oid AND b.branch_code=pm.branch_code Where pm.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("UserID") & "' and b.event = 'in approval' and b.statusrequest = 'new' and pm.statuspromo = 'in approval' and b.tablename = 'QL_mstpromosupp' And pm.branch_code IN (" & Left(sCodePro, sCodePro.Length - 1) & ") order by pm.promooid desc"
        FillGV(GVPromSupp, sSql, "GVPromSupp")
    End Sub

    Private Sub EnabledField()
        If TypeNya.Text = "POINT" Then
            gvPromoDtl.Columns(4).Visible = True
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "AMOUNT" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "QTYBARANG" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = True
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "QTYTOTAL" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "ASPEND" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = True
        End If


        If TypeNya.Text = "POINT" Then
            gvPromoDtl.Columns(4).Visible = True
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "AMOUNT" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
        ElseIf TypeNya.Text = "QTYBARANG" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = True
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "QTYTOTAL" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = False
        ElseIf TypeNya.Text = "ASPEND" Then
            gvPromoDtl.Columns(4).Visible = False
            gvPromoDtl.Columns(5).Visible = False
            gvPromoDtl.Columns(6).Visible = True
        End If

        If TypeNya.Text = "POINT" Then
            GvRewardDtl.Columns(4).Visible = True
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = False
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeNya.Text = "AMOUNT" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = True
            GvRewardDtl.Columns(6).Visible = False
            GvRewardDtl.Columns(7).Visible = False
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeNya.Text = "QTYBARANG" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = False
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeNya.Text = "QTYTOTAL" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = False
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = True
            GvRewardDtl.Columns(8).Visible = True
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = False
        ElseIf TypeNya.Text = "ASPEND" Then
            GvRewardDtl.Columns(4).Visible = False
            GvRewardDtl.Columns(5).Visible = True
            GvRewardDtl.Columns(6).Visible = True
            GvRewardDtl.Columns(7).Visible = False
            GvRewardDtl.Columns(8).Visible = True
            GvRewardDtl.Columns(9).Visible = True
            GvRewardDtl.Columns(10).Visible = True
        End If
    End Sub
#End Region

    Protected Sub lkbTWServiceSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbTWServiceSupp.Click
        setTwSSupp()
    End Sub

    Private Sub setTwSSupp()
        BindTWSSupp() : MultiView1.SetActiveView(View7)
    End Sub

    Private Sub BindTWSSupp()
        Dim QlTwS As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trfwhservicemst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeTws As String = ""
        Dim TwsCode() As String = QlTwS.Split(",")
        For C1 As Integer = 0 To TwsCode.Length - 1
            If TwsCode(C1) <> "" Then
                sCodeTws &= "'" & TwsCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select tw.Trfwhserviceoid, tw.FromBranch, b.approvaloid, tw.TrfwhserviceNo, tw.Trfwhservicedate, tw.upduser, b.requestuser, b.requestdate,tb.gendesc SuppName,(Select gendesc from QL_mstgen gc Where gc.gencode=tw.FromBranch AND gc.gengroup='CABANG') Cabang From ql_trfwhservicemst tw inner join ql_approval b on tw.Trfwhserviceoid = b.oid AND b.branch_code=tw.ToBranch Inner Join QL_mstgen tb ON tb.gencode=tw.ToBranch AND tb.gengroup='CABANG' Where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("userid") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.Trfwhservicestatus = 'in approval' and b.tablename = 'QL_trfwhservicemst' and tw.ToBranch IN (" & Left(sCodeTws, sCodeTws.Length - 1) & ") AND tw.Trfwhservicetype='INTERNSUPP' order by tw.Trfwhserviceoid Desc"
        FillGV(GVTws, sSql, "GVTWService")
    End Sub

    Protected Sub lkbSelectTWS_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TwSuppOid") = sender.ToolTip : MultiView1.SetActiveView(View8)
        Session("approvaloid") = sender.CommandArgument
        Session("TwsSupp") = sender.CommandName
        OidTws.Text = sender.Tooltip
        sSql = "SELECT tw.TrfwhserviceNo, tw.Trfwhservicedate, tw.upduser,(select gendesc from QL_mstgen where gencode = tw.frombranch AND gengroup='CABANG') fromMtrBranch,((select gendesc from QL_mstgen where genoid = (select genother1 from QL_mstgen where genoid = tw.FromMtrLocOid AND gengroup='LOCATION'))) gudangasal,tw.fromBranch DariBranch, 0 suppoid,(select gendesc from QL_mstgen where gencode = tw.ToBranch AND gengroup='CABANG') suppname,tw.FromMtrLocOid,tw.ToMtrLocOId,((Select gendesc from QL_mstgen Where genoid = (select genother1 from QL_mstgen where genoid = tw.ToMtrLocOId AND gengroup='LOCATION'))) GudangTujuan,ToBranch FROM ql_trfwhservicemst tw Inner join QL_mstgen b on tw.cmpcode = b.cmpcode And tw.FromMtrlocoid = b.genoid Inner join QL_mstgen c on tw.cmpcode = c.cmpcode And tw.ToMtrlocOid = c.genoid WHERE tw.Trfwhserviceoid=" & Session("TwSuppOid") & " And tw.cmpcode='" & CompnyCode & "' AND tw.Trfwhservicetype='INTERNSUPP'"

        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "TwsMst")
        TwsSupNo.Text = dt.Rows(0).Item("TrfwhserviceNo").ToString
        TwsSupDate.Text = Format(dt.Rows(0).Item("Trfwhservicedate"), "dd/MM/yyyy")
        TwsSupUsr.Text = dt.Rows(0).Item("upduser").ToString
        DariCabang.Text = dt.Rows(0).Item("fromMtrBranch").ToString
        GudangLoc.Text = dt.Rows(0).Item("gudangasal").ToString
        TujuanNya.Text = dt.Rows(0).Item("suppname")
        OidTujuan.Text = dt.Rows(0).Item("ToMtrLocOId")
        kodeCab.Text = dt.Rows(0).Item("DariBranch").ToString
        KodeTujuan.Text = dt.Rows(0).Item("ToBranch").ToString

        sSql = "Select td.trfwhservicedtlseq seq, i.itemdesc + ' - ' + i.merk itemdesc, td.trfwhserviceqty qty, gendesc AS unit, td.trfwhservicedtlnote note,reqdtloid,reqoid From ql_trfwhservicedtl td inner join ql_mstitem i on td.cmpcode = i.cmpcode and td.itemoid = i.itemoid INNer Join QL_mstgen gu On gu.genoid=i.satuan1 Where td.trfwhserviceoid = " & Session("TwSuppOid") & " and td.cmpcode = '" & CompnyCode & "'"
        FillGV(GVTwsDtl, sSql, "Ql_GvTwsDtl")
    End Sub

    Protected Sub BtnAppSupTws_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppSupTws.Click
        Dim LblPesan As String = ""
        If IsDBNull(Session("TwSuppOid")) Then
            LblPesan &= "- Maaf, Pilih transfer warehouse service yang akan diapprove terlebih dahulu..!!!<br />"
        Else
            If Session("TwSuppOid") = "" Then
                LblPesan &= "- Maaf, Pilih transfer warehouse service yang akan diapprove terlebih dahulu..!!!<br />"
            End If
        End If

        Dim CekSts As Double = GetScalar("select Count(Trfwhserviceoid) from ql_trfwhservicemst where trfwhserviceoid = " & Integer.Parse(Session("TwSuppOid")) & " AND Trfwhservicetype='INTERNSUPP' AND Trfwhservicestatus='APPROVED'")

        If CekSts > 0 Then
            LblPesan &= "- Maaf, Transfer warehouse service tidak bisa diapprove karena telah berstatus 'Approved'..!!"
        End If

        If LblPesan <> "" Then
            showMessage(LblPesan, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim iCurID As Integer = 0
        Dim Cabang As String = GetStrData("Select genother1 from ql_mstgen Where gencode='" & kodeCab.Text & "' And gengroup='CABANG'")

        Dim TwsNo As String = "TSI/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trfwhserviceno,4) AS INT)),0) FROM QL_trfwhservicemst WHERE trfwhserviceno LIKE '" & TwsNo & "%' AND trfwhservicetype='INTERNSUPP'" : iCurID = GetScalar(sSql) + 1
        TwsNo = GenNumberString(TwsNo, "", iCurID, 4)

        Dim ConMtrOid As Int32
        ConMtrOid = GenerateID("QL_conmtr", CompnyCode)

        sSql = "Select td.trfwhservicedtloid,td.reqoid,td.reqdtloid, td.itemoid AS refoid, td.trfwhserviceqty AS qty, td.trfwhservicedtlnote,tm.FromBranch,tm.FromMtrLocOid from ql_trfwhservicedtl td inner join ql_trfwhservicemst tm on tm.cmpcode = td.cmpcode and tm.trfwhserviceoid = td.trfwhserviceoid Where tm.trfwhservicetype='INTERNSUPP' And td.cmpcode = '" & CompnyCode & "' AND tm.trfwhserviceoid = " & Session("TwSuppOid") & ""
        Dim tws As DataTable = cKon.ambiltabel(sSql, "Ql_twdtl")
        Session("TwDtl") = tws

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Approved', finalapprovaluser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, trfwhserviceno = '" & TwsNo & "' WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = '" & OidTws.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Dim dt As DataTable = Session("TwDtl")
            For r As Integer = 0 To dt.Rows.Count - 1
                Dim lasthpp As Double = 0 : Dim cQty As Double = 0 : Dim sTatusDtl As String = ""
                sSql = "select hpp from ql_mstitem Where itemoid=" & Integer.Parse(dt.Rows(r).Item("refoid")) & ""
                xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                '--- Insert Qty Out Gudang asal ---
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code,conrefoid) VALUES " & _
                "('" & CompnyCode & "', " & ConMtrOid & ",'TSI',CURRENT_TIMESTAMP, '" & GetDateToPeriodAcctg(GetServerTime()) & "','" & TwsNo & "'," & dt.Rows(r).Item("trfwhservicedtloid") & ",'QL_trfwhservicemst'," & dt.Rows(r).Item("refoid") & ", 'QL_MSTITEM', 941, " & dt.Rows(r).Item("FromMtrlocoid") & ",0," & dt.Rows(r).Item("qty") & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0,0,'" & Tchar(dt.Rows(r).Item("trfwhservicedtlnote")) & "', " & ToDouble(lasthpp) & ", '" & dt.Rows(r).Item("fromBranch") & "'," & dt.Rows(r).Item("reqdtloid") & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ConMtrOid += 1

                '--- Insert Qty In Gudang perjalanan Cabang tujuan ---
                sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, branch_code,conrefoid) VALUES " & _
                "('" & CompnyCode & "'," & ConMtrOid & ",'TSI',CURRENT_TIMESTAMP,'" & GetDateToPeriodAcctg(GetServerTime()) & "','" & TwsNo & "'," & dt.Rows(r).Item("trfwhservicedtloid") & ",'QL_trfwhservicemst'," & dt.Rows(r).Item("refoid") & ",'QL_MSTITEM',941,-9," & dt.Rows(r).Item("qty") & ",0,'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0,0,'" & Tchar(dt.Rows(r).Item("trfwhservicedtlnote")) & "'," & ToDouble(lasthpp) & ",'" & KodeTujuan.Text & "'," & dt.Rows(r).Item("reqdtloid") & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ConMtrOid += 1
            Next

            sSql = "UPDATE QL_mstoid SET lastoid=" & ConMtrOid - 1 & " WHERE tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Update Approval
            sSql = "UPDATE QL_approval SET " & _
                    "approvaldate=CURRENT_TIMESTAMP, " & _
                    "approvalstatus='Approved', " & _
                    "event='Approved', " & _
                    "statusrequest='End' " & _
                    "WHERE cmpcode='" & CompnyCode & "' " & _
                    "and tablename='QL_trfwhservicemst' " & _
                    "and oid=" & OidTws.Text & " " & _
                    "and approvaluser='" & Session("UserId") & _
                    "' And branch_code='" & KodeTujuan.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'update event user yg lain 
            sSql = "UPDATE QL_approval SET " & _
                   "approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore', " & _
                   "event='Ignore', " & _
                   "statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='QL_trfwhservicemst' " & _
                   "and oid=" & OidTws.Text & " " & _
                   "and approvaluser<>'" & Session("UserId") & _
                   "' and branch_code='" & KodeTujuan.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setTwSSupp()
    End Sub

    Protected Sub btnBackTws_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBackTws.Click
        setTwSSupp()
    End Sub

    Protected Sub BtnTwsReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnTwsReject.Click
        Dim lblPesan As String = ""
        If IsDBNull(Session("TwSuppOid")) Then
            lblPesan &= "Pilih transfer warehouse service yang akan direject terlebih dahulu..!!<br />"
        Else
            If Session("TwSuppOid") = "" Then
                lblPesan &= "- Maaf, Pilih transfer warehouse service yang akan direject terlebih dahulu..!!<br />"
            End If
        End If

        If TwsSupNo.Text = "" Then
            lblPesan &= "- Maaf, Pilih transfer warehouse service yang akan direject terlebih dahulu..!!<br />"
        End If

        Dim CekSts As Double = GetScalar("select Count(Trfwhserviceoid) from ql_trfwhservicemst Where trfwhserviceoid = " & OidTws.Text & " AND Trfwhservicetype='INTERNSUPP' AND Trfwhservicestatus='APPROVED'")

        If CekSts > 0 Then
            lblPesan &= "- Maaf, Transfer warehouse service tidak bisa direject karena telah berstatus 'Approved'..!!"
        End If

        If lblPesan <> "" Then
            showMessage(lblPesan, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trfwhserviceoid = '" & OidTws.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("TwSuppOid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='ql_trfwhservicemst' and oid=" & Session("TwSuppOid") & " "
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMsgBoxOK.Click
        If MsgNya.Text = "1" Then
            PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
            setSDOasActive()
        Else
            PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
        End If
    End Sub

    Protected Sub BtnSubMitRev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubMitRev.Click
        If txt_reviseTWService.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                'UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Approved'
                sSql = "UPDATE QL_trfwhservicemst SET trfwhservicestatus = 'Revised' , revisenote = '" & Tchar(TxtRevNoteTWsupp.Text) & "' WHERE trfwhserviceoid = " & OidTws.Text & " and toBranch = '" & kodeCab.Text & "' AND Trfwhservicetype='EXTERNAL'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' And tablename='QL_trfwhservicemst' and oid=" & OidTws.Text & " And branch_code ='" & kodeCab.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                BtnSubMitRev.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');") : Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception
                showMessage(ex.ToString & "<br />" & sSql, CompnyName, 1)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub ImgRevTws_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRevTws.Click
        TxtRevNoteTWsupp.Visible = True : BtnSubMitRev.Visible = True
        BtnBatalRev.Visible = True
    End Sub

    Protected Sub BtnBatalRev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBatalRev.Click
        TxtRevNoteTWsupp.Visible = False : BtnSubMitRev.Visible = False
        BtnBatalRev.Visible = False
    End Sub

    Protected Sub lkbPromoSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbPromoSupp.Click
        setPromSupp()
    End Sub

    Protected Sub lkbSelectPromSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("PromSuppOid") = sender.ToolTip : MultiView1.SetActiveView(View17)
        Session("approvaloid") = sender.CommandArgument
        Session("PromSupp") = sender.CommandName
        OidProm.Text = sender.Tooltip
        sSql = "Select promooid [promoid],promocode [promeventcode],promoname [promeventname], convert(varchar(10),promodate1,103) promstartdate, convert(varchar(10),promodate2,103) promfinishdate, getdate() [promstarttime],GETDATE() [promfinishtime],crtuser,crttime,ps.upduser,ps.updtime,notemst,Case When ps.typenya ='AMOUNT' Then 'TARGET AMOUNT' When ps.typenya ='POINT' Then 'TARGET POINT' When ps.typenya ='QTYBARANG' then 'TARGET QTY PER BARANG' When ps.typenya ='QTYTOTAL' Then 'TARGET QTY TOTAL' When ps.typenya ='ASPEND' Then 'TARGET ASPEND' Else '' End TypePromo,typenya ,ps.statuspromo FROM QL_mstpromosupp ps Where promooid=" & Integer.Parse(OidProm.Text) & ""
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstpromosupp")
        KodePromo.Text = dt.Rows(0).Item("promeventcode").ToString
        PromName.Text = dt.Rows(0).Item("promeventname").ToString
        sTartTgl.Text = dt.Rows(0).Item("promstartdate")
        FinishTgl.Text = dt.Rows(0).Item("promfinishdate")
        TypePromo.Text = dt.Rows(0).Item("TypePromo").ToString
        TypeNya.Text = dt.Rows(0).Item("typenya")

        sSql = "Select spd.seq,spd.suppoid,sup.suppaddr,sup.suppcode,sup.suppname from QL_mstsuppdtl spd INNER JOIN QL_mstsupp sup ON sup.suppoid=spd.suppoid Where spd.promooid=" & Integer.Parse(OidProm.Text) & " ORDER BY suppcode ASC"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet

        mySqlDA.Fill(objDs, "SetDtlSupp")
        GVdtlSupp.Visible = True
        EnabledField()
        Dim dv As DataView = objDs.Tables("SetDtlSupp").DefaultView
        GVdtlSupp.DataSource = objDs.Tables("SetDtlSupp")
        GVdtlSupp.DataBind()
        Session("SetDtlSupp") = objDs.Tables("SetDtlSupp")
        conn.Close()

        sSql = "select branch_code,promodtloid,promooid,i.itemoid,i.itemcode,i.itemdesc,pd.point,pd.seq,pd.targetqty,pd.priceitem,pd.aspend from QL_mstpromosuppdtl pd Inner Join QL_mstitem i ON i.itemoid=pd.itemoid Where pd.promooid=" & Integer.Parse(OidProm.Text) & ""
        FillGV(gvPromoDtl, sSql, "QL_mstpromosuppdtl")

        sSql = "Select rw.branch_code,rw.rewarddtloid RewardOid,promooid,isnull(i.itemoid,0) itemoid,ISNULL(i.itemcode,'') itemcode,Isnull(i.itemdesc,rw.itemdesc) itemdesc,seq,rw.targetpoint ,rw.targetamt,rw.qtyreward rQty,amtreward rAmount,typereward TypeNya,targetqty,PersenReward,PersenReward2 From ql_mstrewarddtl rw Left Join QL_mstitem i ON i.itemoid=rw.itemoid Where rw.promooid=" & Integer.Parse(OidProm.Text) & ""
        FillGV(GvRewardDtl, sSql, "QL_mstpromosuppdtl")
    End Sub

    Protected Sub BtnRejectProm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRejectProm.Click
        Dim lblPesan As String = ""
        If IsDBNull(Session("PromSuppOid")) Then
            lblPesan &= "- Maaf, Pilih data yang akan direject..!!<br />"
        Else
            If Session("PromSuppOid") = "" Then
                lblPesan &= "- Maaf, Pilih transfer warehouse service yang akan direject..!!<br />"
            End If
        End If

        Dim CekSts As Double = GetScalar("select Count(promooid) from QL_mstpromosupp Where promooid = " & OidProm.Text & " And statuspromo='APPROVED'")

        If CekSts > 0 Then
            lblPesan &= "- Maaf, Data tidak bisa direject karena telah berstatus 'Approved'..!!"
        End If

        If lblPesan <> "" Then
            showMessage(lblPesan, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "UPDATE QL_mstpromosupp SET statuspromo='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND promooid = '" & OidProm.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET statusrequest='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & OidProm.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub BtnAppPromo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppPromo.Click
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & Session("branch_id") & "' AND gengroup='CABANG'")

        Dim sNo As String = "PS/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(promocode,'" & sNo & "',''))),0)+1 FROM QL_mstpromosupp WHERE promocode LIKE '" & sNo & "%'"
        KodePromo.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)

        Dim statusUser As String = "" : Dim level As String = ""
        Dim maxLevel As String = ""
        '-- Approval Level bertingkat --
        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_mstpromosupp' and approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_mstpromosupp'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='QL_mstpromosupp' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

        'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
        If level + 1 <= maxLevel Then
            'ambil data approval, tampung di datatable
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstpromosupp' and approvallevel=" & level + 1 & ""

            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If statusUser = "FINAL" Then
                sSql = "UPDATE [QL_mstpromosupp] SET [statuspromo]='Approved',promocode='" & KodePromo.Text & "', [upduser]='" & Session("UserID") & "',[updtime]=current_timestamp WHERE promooid=" & Integer.Parse(OidProm.Text)
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Update Approval
                sSql = "UPDATE QL_approval SET approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_mstpromosupp' " & _
                        "and oid=" & OidProm.Text & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_mstpromosupp' " & _
                       "and oid=" & OidProm.Text & " " & _
                       "and approvaluser<>'" & Session("UserId") & _
                       "' and branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - WARNING", 1) : Exit Sub
        End Try
        setPromSupp()
    End Sub

    Protected Sub LkbReturNT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbReturNT.Click
        setReturNT()
    End Sub

    Private Sub setReturNT()
        BindReturNT() : MultiView1.SetActiveView(View28)
    End Sub

    Private Sub BindReturNT()
        Dim QlNT As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbiayaeksmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeNT As String = ""
        Dim NTCode() As String = QlNT.Split(",")
        For C1 As Integer = 0 To NTCode.Length - 1
            If NTCode(C1) <> "" Then
                sCodeNT &= "'" & NTCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select pm.trnbiayaeksoid, pm.branch_code, b.approvaloid, pm.trnbiayaeksno, pm.trnbiayaeksdate, pm.upduser, b.requestuser, b.requestdate,ISNULL((Select sp.custname From QL_mstcust sp Where sp.custoid=pm.custoid AND sp.branch_code=pm.branch_code),'') SuppName,(Select gendesc from QL_mstgen gc Where gc.gencode=pm.branch_code AND gc.gengroup='CABANG') Cabang,(select SUM(amttrans) - SUM(amtbayar) from QL_conar c where c.refoid = pm.trnbiayaeksoid and c.trnartype IN ('EXP', 'PAYAREXP', 'CNNT')) AS amtekspedisi From ql_trnbiayaeksmst pm inner join ql_approval b on pm.trnbiayaeksoid= b.oid AND b.branch_code=pm.branch_code Where pm.cmpcode = '" & CompnyCode & "' And b.approvaluser = '" & Session("UserID") & "' And b.event = 'In Approval' and b.statusrequest = 'new' and pm.approvalstatus='In Approval' And b.tablename = 'ql_trnbiayaeksmst' And pm.branch_code IN (" & Left(sCodeNT, sCodeNT.Length - 1) & ") order by pm.trnbiayaeksoid desc"
        FillGV(GVReturNT, sSql, "GVReturNT")
    End Sub

    Protected Sub BtnAppNT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppNT.Click
        Dim statusUser As String = "" : Dim level As String = ""
        Dim maxLevel As String = ""

        '-- Approval Level bertingkat --
        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure Where cmpcode='" & CompnyCode & "' and tablename='ql_trnbiayaeksmst' and approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnbiayaeksmst'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='ql_trnbiayaeksmst' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

        'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
        If level + 1 <= maxLevel Then
            'ambil data approval, tampung di datatable
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_trnbiayaeksmst' and approvallevel=" & level + 1 & ""

            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            End If
        End If

        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim PeriodAcctg As String = GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyy"))
        Dim sMsg As String = "" : Dim mVar As String = ""
        Dim VArNya As String = "" : Dim LblExp As String = ""
        Dim ConAROid As Integer = 0

        Dim CekData As Double = GetScalar("Select COUNT(*) from ql_trnbiayaeksmst Where approvalstatus='APPROVED' AND trnbiayaeksoid=" & Integer.Parse(ExpOid.Text) & "")

        If CekData > 0 Then
            sMsg &= "Maaf, Data sudah status approved, Mohon untuk cek pada form transaksi..!!<br />"
        End If

        If TypeNota.Text = "NON EXPEDISI" Then
            VArNya = "VAR_PIUTANG_LAIN"
        Else
            VArNya = "VAR_PIUTANG_EXP"
        End If

        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & VArNya & "' AND interfaceres1='" & Session("CbgExp") & "'"
        mVar = GetStrData(sSql)

        If mVar = "" Or mVar = "0" Then
            sMsg &= "Maaf, Interface " & VArNya & " belum di setting, Silahkan hubungi admin accounting untuk seting interface..!!<br />"
        End If

        sSql = "Select acctgoid from ql_mstacctg Where acctgcode='" & mVar & "'"
        Dim AcctgOid As Integer = GetScalar(sSql)

        '---- Binddata Show COA ----
        sSql = "Select ROW_NUMBER() OVER (ORDER BY a.acctgcode ASC) AS seq,a.acctgcode,a.acctgoid, d.gldbcr,a.acctgdesc, case d.gldbcr when 'D' then SUM(c.amttrans)-SUM(amtbayar) else 0 end 'Debet', case d.gldbcr when 'C' then SUM(c.amttrans)-SUM(amtbayar) else 0 end 'Kredit', glnote, SUM(c.amttrans)-SUM(amtbayar) AS glamt From ql_trngldtl d inner join ql_mstacctg a on a.acctgoid=d.acctgoid INNER JOIN ql_trnbiayaeksmst bm ON bm.trnbiayaeksno = d.noref INNER JOIN QL_conar c ON c.refoid = bm.trnbiayaeksoid and trnartype IN ('EXP', 'PAYAREXP', 'CNNT') Where noref ='" & biayaeksno.Text & "' and d.cmpcode='" & CompnyCode & "' GROUP BY a.acctgcode, a.acctgoid, a.acctgdesc, d.gldbcr, d.glamt, d.glnote, d.glmstoid order by d.glmstoid"

        Dim DtCoa As DataTable = cKon.ambiltabel2(sSql, "ql_mstacctg")
        Session("DtCoa") = DtCoa
        '==== End Bindata Show COA =====

        If DtCoa.Rows.Count < 0 Then
            sMsg &= "Maaf, Auto Jurnalnya engga ada..!!<br />"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
        End If

        sSql = "Select SUM(amttrans)-SUM(amtbayar) from QL_conar Where refoid=" & Integer.Parse(ExpOid.Text) & " AND trnartype in ('PAYAREXP','EXP', 'RETUREXP', 'CNNT') AND branch_code='" & Session("CbgExp") & "'"
        Dim AmtSisa As Double = GetScalar(sSql)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If ToDouble(AmtSisa) > 0.0 Then
                sSql = "Select ISNULL(MAX(conaroid),0)+1 From QL_conar Where cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : ConAROid = xCmd.ExecuteScalar()

                sSql = "INSERT INTO QL_conar (cmpcode,branch_code,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnarflag,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,trnarnote, trnarres1,upduser,updtime,amtbayaridr) VALUES " & _
                "('" & CompnyCode & "','" & Session("CbgExp") & "'," & Integer.Parse(ConAROid) & ",'QL_trnpayar'," & Integer.Parse(ExpOid.Text) & ",0," & Integer.Parse(NtCustoid.Text) & "," & Integer.Parse(AcctgOid) & ",'POST','','RETUREXP','1/1/1900','" & PeriodAcctg & "',0,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & biayaeksno.Text & "',0,(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),0,0,0," & ToDouble(amtekspedisi.Text) & ",'NOTA(NO=" & biayaeksno.Text & ")','','" & Session("UserID") & "',CURRENT_TIMESTAMP," & ToDouble(amtekspedisi.Text) & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & ConAROid & " Where tablename ='QL_conar' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '=========== Insert Auto Jurnal ===========
                '------------------------------------------
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid,branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) " & _
                    " VALUES ('" & CompnyCode & "', " & iGlMstOid & ",'" & Session("CbgExp") & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & PeriodAcctg & "','NT|No=" & Tchar(biayaeksno.Text) & "', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'seq

                If TypeNota.Text = "EXPEDISI" Then
                    sSql = "UPdate ql_trncashbankmst Set flagexpedisi='' Where cashbankoid=" & cashbankoid.Text & " AND branch_code='" & Session("CbgExp") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    Dim objTbl As DataTable = Session("EksDtl")
                    For C1 As Integer = 0 To objTbl.Rows.Count - 1
                        sSql = "Update ql_trnjualmst Set flagexpedisi='' Where trnjualmstoid=" & objTbl.Rows(C1).Item("trnjualoid") & " AND branch_code='" & Session("CbgExp") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                End If

                For i As Int32 = 0 To DtCoa.Rows.Count - 1
                    Dim gldbcr As String
                    If DtCoa.Rows(i).Item("gldbcr") = "D" Then : gldbcr = "C"
                    Else : gldbcr = "D" : End If

                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) VALUES " & _
                    " ('" & CompnyCode & "', " & iGlDtlOid & ",'" & Session("CbgExp") & "', " & DtCoa.Rows(i).Item("seq") & ", " & iGlMstOid & ", " & Integer.Parse(DtCoa.Rows(i).Item("acctgoid")) & ", '" & gldbcr & "', " & ToDouble(DtCoa.Rows(i).Item("glamt")) & ", '" & biayaeksno.Text & "', '', 'POST', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(DtCoa.Rows(i).Item("glamt")) & ", " & ToDouble(DtCoa.Rows(i).Item("glamt")) & ", 'ql_trnbiayaeksmst " & Integer.Parse(ExpOid.Text) & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                Next
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If statusUser = "FINAL" Then
                sSql = "update ql_trnbiayaeksmst set approvalstatus='APPROVED',updtime=current_timestamp, upduser='" & Session("UserID") & "',returdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) Where trnbiayaeksoid=" & Integer.Parse(ExpOid.Text) & " And cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update Approval
                sSql = "UPDATE QL_approval SET approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='ql_trnbiayaeksmst' " & _
                        "and oid=" & ExpOid.Text & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & Session("CbgExp") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_mstpromosupp' " & _
                       "and oid=" & ExpOid.Text & " " & _
                       "and approvaluser<>'" & Session("UserId") & _
                       "' and branch_code='" & Session("CbgExp") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                showMessage("Maaf, No Nota " & biayaeksno.Text & " tidak bisa diretur karena sudah pernah di lakukan pelunasan ataupun pembayaran", CompnyName & " - WARNING", 1) : Exit Sub
            End If
            '--- End Jika AmtSisa Lebih dari Nol ---
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - WARNING", 1) : Exit Sub
        End Try
        setReturNT()
    End Sub

    Protected Sub BtnBackNT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnBackNT.Click
        setReturNT()
    End Sub

    Protected Sub lkbSelectNTR_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trnbiayaeksoid") = sender.ToolTip : MultiView1.SetActiveView(View29)
        Session("approvaloid") = sender.CommandArgument
        Session("CbgExp") = sender.CommandName
        ExpOid.Text = sender.Tooltip
        sSql = "Select bm.branch_code,ISNULL(cb.cashbankoid,0) cashbankoid,bm.custoid,trnbiayaeksoid,trnbiayaeksno,ISNULL(cb.cashbankno,'-') cashbankno,trnbiayaeksdate,cu.custname,bm.acctgoid,bm.status,(select SUM(amttrans) - SUM(amtbayar) from QL_conar c where c.refoid = bm.trnbiayaeksoid and c.trnartype IN ('EXP', 'PAYAREXP', 'CNNT')) AS amtekspedisi,bm.trnbiayaeksnote,bm.updtime,bm.upduser,ISNULL(bm.cashbankdtloid,0) cashbankdtloid,ISNULL(typenota,'') typenota,cabangasal,(Select gendesc from QL_mstgen gc Where gc.gencode=bm.branch_code AND gc.gengroup='CABANG') CabangNya,bm.reasonretur From ql_trnbiayaeksmst bm LEFT JOIN QL_trncashbankmst cb ON cb.cashbankoid=(CASE typenota When 'NON EXPEDISI' then 0 else bm.cashbankoid end) And cb.branch_code=bm.cabangasal INNER JOIN QL_mstcust cu ON cu.custoid=bm.custoid AND cu.branch_code=bm.branch_code Where trnbiayaeksoid =" & Session("trnbiayaeksoid") & " AND bm.branch_code='" & Session("CbgExp") & "'"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "ql_trnbiayaeksmst")
        ExpOid.Text = Integer.Parse(dt.Rows(0).Item("trnbiayaeksoid"))
        cashbankoid.Text = Integer.Parse(dt.Rows(0).Item("cashbankoid"))
        CbgExp.Text = dt.Rows(0).Item("CabangNya").ToString
        CashBankNo.Text = dt.Rows(0).Item("cashbankno").ToString
        NtCustoid.Text = Integer.Parse(dt.Rows(0).Item("Custoid"))
        ntCustNames.Text = dt.Rows(0).Item("custname").ToString
        biayaeksno.Text = dt.Rows(0).Item("trnbiayaeksno").ToString
        amtekspedisi.Text = ToMaskEdit(dt.Rows(0).Item("amtekspedisi"), 3)
        PaymentDate.Text = Format(dt.Rows(0).Item("trnbiayaeksdate"), "dd/MM/yyy")
        CabangDr.Text = Trim(dt.Rows(0).Item("cabangasal").ToString)
        TypeNota.Text = dt.Rows(0).Item("typenota").ToString
        ReasonRetur.Text = dt.Rows(0).Item("reasonretur").ToString
        sSql = "Select acctgoid,('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc from QL_mstacctg a Where acctgoid=" & Integer.Parse(dt.Rows(0).Item("acctgoid")) & ""
        FillDDL(cashbankacctgoid, sSql)
        cashbankacctgoid.SelectedValue = Integer.Parse(dt.Rows(0).Item("acctgoid"))

        sSql = "Select trnbiayaeksdtloid,bd.trnbiayaeksoid,trnjualmstoid trnjualoid,payseq,trnjualno,notedtl,bd.amtekspedisi amttrans,bm.typenota,Case bm.typenota When 'EXPEDISI' Then bd.trnjualno Else (Select dc.gendesc from QL_mstgen dc Where dc.genoid=bd.trnjualmstoid AND gengroup='NOTA') End DescNya From ql_trnbiayaeksdtl bd Inner Join ql_trnbiayaeksmst bm ON bm.trnbiayaeksoid=bd.trnbiayaeksoid Where bm.trnbiayaeksoid='" & Session("trnbiayaeksoid") & "' AND bm.branch_code='" & Session("CbgExp") & "'"
        Dim EksDtl As DataTable = cKon.ambiltabel2(sSql, "ql_trnbiayaeksdtl")
        Session("EksDtl") = EksDtl : GVDtlEkp.DataSource = EksDtl
        GVDtlEkp.DataBind()

        If TypeNota.Text = "EXPEDISI" Then
            GVDtlEkp.Columns(3).Visible = False
        Else
            GVDtlEkp.Columns(3).Visible = False
        End If
    End Sub

    Protected Sub BtnBackProm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnBackProm.Click
        setPromSupp()
    End Sub

    Protected Sub gvPromoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPromoDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub GvRewardDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvRewardDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub BtnRejectNT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRejectNT.Click
        Dim lblPesan As String = ""
        If IsDBNull(Session("trnbiayaeksoid")) Then
            lblPesan &= "- Maaf, Pilih data yang akan direject..!!<br />"
        Else
            If Session("trnbiayaeksoid") = "" Then
                lblPesan &= "- Maaf, Pilih data yang akan direject..!!<br />"
            End If
        End If

        Dim CekSts As Double = GetScalar("Select COUNT(trnbiayaeksoid) from ql_trnbiayaeksmst Where approvalstatus='APPROVED' And trnbiayaeksoid=" & ExpOid.Text & "")

        If CekSts > 0 Then
            lblPesan &= "- Maaf, Data tidak bisa direject karena telah berstatus 'Approved'..!!"
        End If

        If lblPesan <> "" Then
            showMessage(lblPesan, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE ql_trnbiayaeksmst SET approvalstatus = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND trnbiayaeksoid = '" & ExpOid.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET event='Rejected', statusrequest='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = " & Session("approvaloid") & " And tablename='ql_trnbiayaeksmst'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Update Approval user yg lain 
            sSql = "update QL_approval set event='Ignore', statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='ql_trnbiayaeksmst' And oid=" & Integer.Parse(ExpOid.Text) & " AND approvaloid<>'" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub gvCNMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCNMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(2).Text), 3)
        End If
    End Sub

    Protected Sub gvDNMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDNMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ClassFunction.ToMaskEdit(ClassFunction.ToDouble(e.Row.Cells(2).Text), 3)
        End If
    End Sub 

    Protected Sub btnRevisePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRevisePO.Click
        imbApprovePO.Visible = False : imbRejectPO.Visible = False
        btnSubRevisePO.Enabled = True : lblRevisePO.Visible = True
        txtRevisePO.Visible = True : btnRevisePO.Visible = False
        imbBackPO.Visible = False : btnCanRevisePO.Visible = True
        lblRevisePO.Text = "Reason For Revision"
        btnSubRevisePO.Visible = True
    End Sub

    Protected Sub imbApprovePO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbApprovePO.Click
        If IsDBNull(Session("pomstoid")) Then
            showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("pomstoid") = "" Then
                showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If

        If trnbelipono.Text = "" Then
            showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            '--------------------------------------------------------------------
            'generate no PO
            sSql = "select genother1 from ql_mstgen where gengroup='cabang' and gencode='" & AsCabang.Text & "'"
            Dim cabang As String = cKon.ambilscalar(sSql)
            Dim tax As String = "" : Dim lblStsPo As String = ""
            Dim sNo As String = "" : Dim nomer As String = ""
            Dim type As String = "" : Dim TblName As String = ""

            If taxpo.Text = "Yes" Then
                tax = "0"
            Else
                tax = "1"
            End If

            If trnbelitype.Text.ToLower = "grosir" Then
                type = "PO"
            ElseIf trnbelitype.Text.ToLower = "retail" Then
                type = "PO.R"
            Else
                type = "PO.RH"
            End If
            Dim TimeClose As String = ""

            If StatusPO.Text = "In Approval" And pores1.Text = "POCLOSE" Then
                lblStsPo = "CLOSED MANUAL" : trnbelipono.Text = trnbelipono.Text
                TblName = "QL_TRNPOCLOSE" : TimeClose = ",closetime=CURRENT_TIMESTAMP"
            ElseIf StatusPO.Text = "In Approval" And pores1.Text = "" Then
                lblStsPo = "Approved" : TblName = "QL_pomst"
                sNo = type & tax & "/" & cabang & "/" & Format(GetServerTime, "yy/MM/dd") & "/"
                sSql = "SELECT isnull(max(abs(replace(trnbelipono, '" & sNo & "',''))),0) From ql_pomst WHERE cmpcode='" & CompnyCode & "' AND trnbelipono like '" & sNo & "%'"
                nomer = GenNumberString(sNo, "", cKon.ambilscalar(sSql) + 1, 4)
                trnbelipono.Text = nomer
            End If
            '---------------------------------------------------------------------
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
            Dim statusUser As String = "" : Dim level As String = ""
            Dim maxLevel As String = ""
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' ")

            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='" & TblName & "' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                ' Update PO
                sSql = "UPDATE ql_pomst SET trnbelipono = '" & trnbelipono.Text & "', trnbelistatus = '" & lblStsPo & "',finalapprovaluser='" & Session("UserID") & "',closeuser='" & Session("UserID") & "', finalapprovaldatetime=CURRENT_TIMESTAMP,trnbelipodate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) WHERE cmpcode = '" & CompnyCode & "' AND trnbelimstoid = '" & Session("pomstoid") & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Update Po Detail Jika Po akan di close
                If StatusPO.Text = "In Approval" And pores1.Text = "POCLOSE" Then
                    sSql = "UPDATE QL_podtl SET trnbelidtlstatus='Completed' WHERE trnbelimstoid='" & Session("pomstoid") & "' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                'update VoucherDtl
                sSql = "UPDATE ql_voucherdtl set trnbelino = '" & trnbelipono.Text & "' where trnbelimstoid = '" & Session("pomstoid") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ' Update Approval
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='" & TblName & "' " & _
                        "and oid=" & Session("pomstoid") & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='" & TblName & "' " & _
                       "and oid=" & Session("pomstoid") & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else 'Process

                sSql = "update QL_pomst set updtime=CURRENT_TIMESTAMP, trnbelipodate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)) WHERE cmpcode='" & CompnyCode & "' and  trnbelimstoid=" & Session("pomstoid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update Po Detail Jika Po akan di close
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='" & TblName & "'" & _
                        "and oid=" & Session("pomstoid") & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                          "approvaldate=CURRENT_TIMESTAMP, " & _
                          "approvalstatus='Ignore', " & _
                          "event='Ignore', " & _
                          "statusrequest='End' " & _
                          "WHERE cmpcode='" & CompnyCode & "' " & _
                          "and tablename='" & TblName & "' " & _
                          "and oid=" & Session("pomstoid") & " " & _
                          "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insertkan untuk user approval yg levelnya diatasnya lagi
                'If Not Session("TblApproval") Is Nothing Then
                '    Dim objTable As DataTable
                '    objTable = Session("TblApproval")

                '    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                '        sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,requestcode, " & _
                '               "requestuser,requestdate,statusrequest,tablename,oid,event, " & _
                '               "approvalcode,approvaluser,approvaldate,approvaltype, " & _
                '               "approvallevel,approvalstatus) " & _
                '               "VALUES ('" & CompnyCode & "'," & Session("AppOid") + c1 & ", " & _
                '               "'" & "PO" & Session("pomstoid") & "_" & Session("AppOid") + c1 & "', " & _
                '               "'" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','" & TblName & "', " & _
                '               "'" & Session("pomstoid") & "','In Approval','0', " & _
                '               "'" & objTable.Rows(c1).Item("approvaluser") & "','1/1/1900', " & _
                '               "'" & objTable.Rows(c1).Item("approvaltype") & "','1', " & _
                '               "'" & objTable.Rows(c1).Item("approvalstatus") & "')"
                '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '    Next
                '    sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + _
                '           objTable.Rows.Count - 1 & " " & _
                '           "where tablename = 'QL_Approval'"
                '    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'End If
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1) : Exit Sub
        End Try

        If trnbelitype.Text.ToLower = "grosir" Then
            If StatusPO.Text = "In Approval" And pores1.Text = "POCLOSE" Then
                setPOCLOSE()
            Else
                setPOasActive()
            End If
        ElseIf trnbelitype.Text.ToLower = "retail" Then
            setPO_R_asActive()
        Else
            setPO_Rh_asActive()
        End If
    End Sub

    Protected Sub imbRejectPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRejectPO.Click
        If IsDBNull(Session("pomstoid")) Then
            showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("pomstoid") = "" Then
                showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        If trnbelipono.Text = "" Then
            showMessage("Please select a PO first", CompnyName & " - WARNING", 2) : Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim lblStsPo As String = ""
            If StatusPO.Text = "In Approval" And pores1.Text = "POCLOSE" Then
                lblStsPo = "Approved"
            ElseIf StatusPO.Text = "In Approval" And pores1.Text = "" Then
                lblStsPo = "Rejected"
            End If

            sSql = "UPDATE ql_pomst SET trnbelistatus = '" & lblStsPo & "',trnbelires1='' WHERE cmpcode = '" & CompnyCode & "' AND trnbelimstoid = '" & Session("pomstoid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = '" & lblStsPo & "' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Session("approvaloid") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "and tablename='QL_pomst' and oid=" & Session("pomstoid") & " "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING", 1) : Exit Sub
        End Try
        If trnbelitype.Text.ToLower = "grosir" Then
            setPOasActive()
        ElseIf trnbelitype.Text.ToLower = "retail" Then
            setPO_R_asActive()
        Else
            setPO_Rh_asActive()
        End If
        ' Response.Redirect("~/Other/WaitingAction.aspx?awal=true")
    End Sub

    Protected Sub btnSubRevisePO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubRevisePO.Click
        If txtRevisePO.Text = "" Then
            showMessage("Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Dim sTs As String = "Revised"

            Try ' INSERT
                sSql = "UPDATE ql_pomst SET trnbelistatus = '" & sTs & "', cancelnote = '" & Tchar(txtRevisePO.Text) & "' WHERE trnbelimstoid = " & Session("pomstoid") & " "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='ql_pomst' and oid=" & Session("pomstoid") & " and branch_code ='" & Session("branch_id") & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                'Reset Approval

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception

                showMessage(ex.Message, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub LkbFinalNya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbFinalNya.Click
        SetFinalNya()
    End Sub

    Private Sub SetFinalNya()
        BindFinalNya() : MultiView1.SetActiveView(View42)
    End Sub

    Private Sub BindFinalNya()
        Dim QlFis As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNFINAL' And approvaluser='" & Session("UserID") & "' order by approvallevel")
        Dim sCodeFin As String = ""
        Dim FinCode() As String = QlFis.Split(",")
        For C1 As Integer = 0 To FinCode.Length - 1
            If FinCode(C1) <> "" Then
                sCodeFin &= "'" & FinCode(C1).Trim & "',"
            End If
        Next
        sSql = "Select tw.FINOID Trfwhserviceoid, tw.branch_code, b.approvaloid, tw.trnfinalno TrfwhserviceNo, tw.finaldate Trfwhservicedate, tw.upduser, b.requestuser, b.requestdate,(Select custname From QL_mstcust sp Where sp.custoid=tw.custoid) SuppName,(Select gendesc from QL_mstgen gc Where gc.gencode=tw.branch_code AND gc.gengroup='CABANG') Cabang From QL_TRNFINAL tw inner join ql_approval b on tw.FINOID = b.oid AND b.branch_code=tw.branch_code Where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("userid") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.FINSTATUS = 'in approval' and b.tablename = 'QL_TRNFINAL' and tw.branch_code IN (" & Left(sCodeFin, sCodeFin.Length - 1) & ") /*AND tw.FINOID IN (Select trnfinaloid from QL_TRNFINALSPART Where itempartoid<>0)*/ Order By tw.FINOID desc"
        FillGV(GvFinalData, sSql, "GvFinalNya")
    End Sub

    Protected Sub lkbSelectFinal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("trfwhserviceoid") = sender.ToolTip : MultiView1.SetActiveView(View43)
        Session("approvaloid") = sender.CommandArgument
        Session("BranchReq") = sender.CommandName

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "Select fm.FINOID,fm.BRANCH_CODE,cbp.gendesc DescCbInpt,fm.trnfinalno,fm.finaldate,rqm.reqcode,cba.gencode CdCabangAs ,cba.gendesc CabangAsal,fm.FINSTATUS,cu.custoid,cu.custname,rqm.reqoid,fm.mtrlocoid,cu.phone1,fm.trnfinalnote,fm.priceservis,fm.amtnettservis,fm.FINTIME,fm.receivetime,fm.UPDTIME,fm.UPDUSER,fm.CREATETIME,fm.CREATEUSER,fm.locoidrusak,fm.locoidtitipan,typetts,flagfinal From QL_TRNFINAL fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.MSTREQOID AND fm.cabangasal=rqm.branch_code Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.BRANCH_CODE AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cbp.gengroup='Cabang' Where fm.FINOID=" & Session("trfwhserviceoid")
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            'rowstate = 1
            While xreader.Read
                CabangFinalCode.Text = xreader("BRANCH_CODE").ToString
                CodeCabangAsal.Text = xreader("CdCabangAs").ToString
                OidReq.Text = Integer.Parse(xreader("reqoid"))
                FinOid.Text = Integer.Parse(xreader("FINOID"))
                FinalNo.Text = xreader("trnfinalno").ToString
                CustOid.Text = Integer.Parse(xreader("custoid"))
                NoTTS.Text = xreader("reqcode").ToString
                TypeFInal.Text = xreader("typetts").ToString
                OidGudangBagus.Text = Integer.Parse(xreader("mtrlocoid"))
                OidGudangRusak.Text = Integer.Parse(xreader("locoidrusak"))
                OidGudangTitipan.Text = Integer.Parse(xreader("locoidtitipan"))
                CustNameNya.Text = xreader("custname").ToString
                FinalDate.Text = Format(xreader("finaldate"), "dd/MM/yyyy").ToString
                FinalNote.Text = xreader("trnfinalnote").ToString
                CabangFinal.Text = xreader("DescCbInpt").ToString
            End While
        End If
        xreader.Close() : conn.Close()

        sSql = "Select fd.seq,fd.trnfinaloid finoid,fd.reqoid reqmstoid,fd.reqdtloid,rqd.itemoid,rqd.snno,rqd.itemdesc,rqd.itemoid itemreqoid,fd.itemqty reqqty,fd.itemqty,fd.unitoid satuan1,ur.gendesc Unit,rqd.kelengkapan,rqd.reqdtljob,rqd.typegaransi,fd.itempartoid PARTOID,Isnull(i.itemdesc,'') partdescshort,fd.itempartunitoid PartUnitOid,Case sp.gendesc When 'AWL' then '' else sp.gendesc End PartsUnit,fd.itemlocoid mtrloc,fd.itempartqty partsqty,fd.itempartprice spartprice,fd.itemtotalprice totalprice From QL_TRNFINALSPART fd Inner Join QL_TRNREQUESTDTL rqd ON rqd.reqdtloid=fd.reqdtloid AND fd.reqoid=rqd.reqmstoid AND rqd.itemoid=fd.itemreqoid Left Join ql_mstitem i ON i.itemoid=fd.itempartoid Inner Join QL_mstgen ur ON ur.genoid=fd.unitoid AND ur.gengroup='ITEMUNIT' Left Join QL_mstgen sp ON sp.genoid=fd.itempartunitoid AND ur.gengroup='ITEMUNIT' Where fd.trnfinaloid=" & FinOid.Text & " Order By fd.seq"
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "sPartDtl")
        Session("itemdetail") = dtab : gvrequest.DataSource = dtab
        gvrequest.DataBind()

        sSql = "Select rs.trnfinalpartoid,rs.trnfinaloid,rs.reqoid,rs.reqdtloid,rs.itemrusakoid,rs.itemreqoid,rs.itempartoid,rs.qtyrusak,rs.qtyreq,rs.qtypart,seq,rs.locoidrusak,i.itemdesc From QL_trnfinaldtl rs Inner Join QL_mstitem i ON i.itemoid=rs.itemrusakoid Where trnfinaloid=" & FinOid.Text & " Order By seq"
        Dim dtb As DataTable = cKon.ambiltabel2(sSql, "itemrusak")
        Session("itemrusak") = dtb : GvRusak.DataSource = dtb
        GvRusak.DataBind()
    End Sub

    Protected Sub BtnAppFinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppFinal.Click
        Dim iGlMst As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim igldtl As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim ConMtroid As Integer = GenerateID("QL_conmtr", CompnyCode)
        Dim PeriodAcctg As String = GetPeriodAcctg(Format(GetServerTime(), "MM/dd/yyy"))
        Dim sCode As String = "", lasthpp As Double = 0, sMsg As String = "", mVar As String = "", iGlSeq As Integer = 0, SaldoAkhire As Double = 0, iCurID As Integer = 0

        sSql = "Select FINOID, FINOID, FINSTATUS from QL_TRNFINAL Where FINSTATUS IN ('APPROVED','APPROVED','POST') and FINOID=" & Integer.Parse(FinOid.Text) & " AND BRANCH_CODE='" & CabangFinalCode.Text & "'"
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                sMsg &= "- Maaf, No. draft final '" & dta.Rows(i).Item("FINOID") & "' sudah di '" & dta.Rows(i).Item("FINSTATUS") & "' dengan nomer '" & dta.Rows(i).Item("FINOID") & "'..!!<br>"
            Next
        End If

        If Not Session("itemdetail") Is Nothing Then
            Dim dtab As DataTable = Session("itemdetail")
            If dtab.Rows.Count > 0 Then
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab.Rows(j).Item("PARTOID")) & " AND mtrlocoid=" & OidGudangBagus.Text & " AND con.mtrlocoid IN (SELECT genoid FROM QL_mstgen WHERE genother6='UMUM' and gengroup='LOCATION') AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND branch_code='" & CabangFinalCode.Text & "' Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.itemoid) tblitem"
                    If Integer.Parse(dtab.Rows(j).Item("PARTOID")) <> 0 Then
                        SaldoAkhire = GetScalar(sSql)
                        If SaldoAkhire = Nothing Or SaldoAkhire = 0 Then
                            sMsg &= "- Maaf, Stok akhir untuk barang '" & dtab.Rows(j).Item("partdescshort") & "' tidak memenuhi..!!<br>"
                        End If
                    End If
                Next
            End If
        Else
            sMsg &= "- Maaf data detail kosong mohon di cek data di list form service..!!"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim statusUser As String = "" : Dim level As String = "" : Dim maxLevel As String = ""
        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure Where cmpcode='" & CompnyCode & "' and tablename='QL_TRNFINAL' And approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) From QL_approvalstructure Where cmpcode='" & CompnyCode & "' And tablename='QL_TRNFINAL'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' And tablename='QL_TRNFINAL' And approvaluser='" & Session("UserId") & "' And approvallevel=" & level & "")

        'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
        If level + 1 <= maxLevel Then
            'ambil data approval, tampung di datatable
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_TRNFINAL' and approvallevel=" & level + 1 & ""

            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If statusUser = "FINAL" Then
                Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & CabangFinalCode.Text & "' AND gengroup='CABANG'")
                sCode = "FIN/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                sSql = "SELECT ISNULL(MAX(ABS(replace(trnfinalno,'" & sCode & "',''))),0) trnfinalno FROM QL_TRNFINAL WHERE trnfinalno LIKE '" & sCode & "%'"
                xCmd.CommandText = sSql
                If Not IsDBNull(xCmd.ExecuteScalar) Then
                    iCurID = xCmd.ExecuteScalar + 1
                Else
                    iCurID = 1
                End If
                sCode = GenNumberString(sCode, "", iCurID, 4)
                FinalNo.Text = sCode

                sSql = "UPDATE QL_TRNFINAL SET [finaldate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),[FINSTATUS] ='APPROVED',[UPDUSER] = '" & Session("UserID") & "',[UPDTIME]=Current_timestamp,[trnfinalno] = '" & Tchar(FinalNo.Text) & "' WHERE [FINOID]=" & Integer.Parse(FinOid.Text) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim dtab As DataTable = Session("itemdetail")
                If Not Session("itemdetail") Is Nothing Then
                    For t As Integer = 0 To dtab.Rows.Count - 1
                        '--------------------------------------------------------------
                        'mulai proses input stok keluar gudang dagangan Ambil sparepart
                        If Integer.Parse(dtab.Rows(t).Item("PARTOID")) <> 0 Then
                            'Insert Conmtr
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(t).Item("PARTOID") & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar

                            sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "','" & CabangFinalCode.Text & "'," & ConMtroid + 1 & ",'TRNFINAL',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & FinalNo.Text & "','" & FinOid.Text & "', 'QL_TRNFINALSPART', " & Integer.Parse(dtab.Rows(t).Item("PARTOID")) & ",'QL_MSTITEM'," & Integer.Parse(dtab.Rows(t).Item("satuan1")) & "," & OidGudangBagus.Text & ", 0," & ToDouble(dtab.Rows(t).Item("partsqty")) & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(hpp) * ToDouble(dtab.Rows(t).Item("partsqty")) & ",'" & FinalNo.Text & "','" & ToDouble(hpp) & "'," & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                        End If
                        '----------------------------------------------
                        'End proses input stok keluar gudang dagangan
                    Next
                End If

                '-- Kondisi Type Final --
                If TypeFInal.Text = "REPLACE" Then

                    If Not Session("itemdetail") Is Nothing Then
                        For t As Integer = 0 To dtab.Rows.Count - 1
                            '------------------------------------------
                            'mulai proses input stok masuk gudang rusak  
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(t).Item("itemreqoid") & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar

                            sSql = "INSERT INTO QL_conmtr (cmpcode, branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "', '" & CabangFinalCode.Text & "', " & ConMtroid + 1 & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & FinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & Integer.Parse(dtab.Rows(t).Item("itemreqoid")) & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & OidGudangRusak.Text & ", " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", 0, '' , '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(hpp) * ToDouble(dtab.Rows(t).Item("itemqty")) & ", '" & FinalNo.Text & "', '" & ToDouble(hpp) & "', " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1

                            '-------------------------------------------------------------------------------
                            'input stok masuk gudang titipan jika barang replace tdk sama dengan brg service
                            If dtab.Rows(t).Item("PARTOID") <> dtab.Rows(t).Item("itemreqoid") Then
                                sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(t).Item("itemreqoid") & ""
                                xCmd.CommandText = sSql : Dim hppi As Double = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP, conrefoid) " & _
                                "VALUES ('" & CompnyCode & "', '" & CabangFinalCode.Text & "', " & ConMtroid + 1 & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & FinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & Integer.Parse(dtab.Rows(t).Item("itemreqoid")) & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & OidGudangTitipan.Text & ", 0, " & ToDouble(dtab.Rows(t).Item("itemqty")) & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(hppi) * ToDouble(dtab.Rows(t).Item("itemqty")) & ", '" & FinalNo.Text & "', '" & ToDouble(hpp) & "', " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                ConMtroid += 1

                                sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                                "VALUES ('" & CompnyCode & "', '" & CabangFinalCode.Text & "', " & ConMtroid + 1 & ", 'TRNFINAL', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & GetPeriodAcctg(GetServerTime()) & "', '" & FinalNo.Text & "', '" & FinOid.Text & "', 'QL_TRNFINALSPART', " & Integer.Parse(dtab.Rows(t).Item("PARTOID")) & ", 'QL_MSTITEM', " & Integer.Parse(dtab.Rows(t).Item("satuan1")) & ", " & OidGudangTitipan.Text & ", " & ToDouble(dtab.Rows(t).Item("partsqty")) & ", 0, '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(hpp) * ToDouble(dtab.Rows(t).Item("partsqty")) & ", '" & FinalNo.Text & "', '" & ToDouble(hpp) & "', " & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                ConMtroid += 1
                            End If
                        Next

                    End If

                Else
                    If Not Session("itemrusak") Is Nothing Then
                        Dim dtb As DataTable = Session("itemrusak")
                        For ie As Int64 = 0 To dtb.Rows.Count - 1
                            If Integer.Parse(dtb.Rows(ie).Item("itemrusakoid")) <> 0 Then
                                '------------------------------------------
                                'mulai proses input stok masuk gudang rusak
                                sSql = "Select HPP from QL_mstitem Where itemoid=" & dtb.Rows(ie).Item("itemrusakoid") & ""
                                xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar

                                sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                                "VALUES ('" & CompnyCode & "','" & CabangFinalCode.Text & "'," & ConMtroid + 1 & ",'TRNFINAL',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & FinalNo.Text & "','" & Integer.Parse(FinOid.Text) & "', 'QL_trnfinaldtl', " & dtb.Rows(ie).Item("itemrusakoid") & ",'QL_MSTITEM',945," & Integer.Parse(OidGudangRusak.Text) & "," & ToDouble(dtb.Rows(ie).Item("qtyrusak")) & ",0,'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(hpp) * ToDouble(dtb.Rows(ie).Item("qtyrusak")) & ",'" & Tchar(ReqCode.Text) & "-" & FinalNo.Text & "'," & ToDouble(hpp) & "," & Integer.Parse(dtb.Rows(ie).Item("reqdtloid")) & ")"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                ConMtroid += 1
                                'End proses input stok masuk gudang rusak
                                '---------------------------------------- 
                            End If
                        Next
                    End If
                End If

                sSql = "update QL_mstoid set lastoid = " & ConMtroid & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '---- Proses Auto Jurnal ----
                Dim iMatAcctg As String = "" : Dim OidCoaGudang As Integer = 0
                Dim AkunSementara As String = "" : Dim OidCoaSementara As Integer = 0
                Dim TotalHpp As Double = 0

                '---- COA PERSEDIAAN BARANG DAGANGAN ----
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_GUDANG' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : iMatAcctg = xCmd.ExecuteScalar

                If iMatAcctg = "?" Or iMatAcctg = "0" Or iMatAcctg = "" Then
                    sMsg &= "- Maaf,interface 'VAR_GUDANG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAcctg & "'"
                    xCmd.CommandText = sSql : OidCoaGudang = xCmd.ExecuteScalar
                    If OidCoaGudang = 0 Or OidCoaGudang = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk VAR_GUDANG tidak ditemukan..!!<br>"
                    End If
                End If

                '---- COA PERSEDIAAN SEMENTARA ----
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_SERVIS' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : AkunSementara = xCmd.ExecuteScalar

                If AkunSementara = "?" Or AkunSementara = "0" Or AkunSementara = "" Then
                    sMsg &= "- Maaf,interface 'VAR_SERVIS' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & AkunSementara & "'"
                    xCmd.CommandText = sSql : OidCoaSementara = xCmd.ExecuteScalar
                    If OidCoaSementara = 0 Or OidCoaSementara = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk VAR_SERVIS tidak ditemukan..!!<br>"
                    End If
                End If

                If sMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(sMsg, CompnyName & " - WARNING", 2)
                    Exit Sub
                End If

                For T1 As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "Select HPP from QL_mstitem Where itemoid=" & dtab.Rows(T1).Item("PARTOID") & ""
                    xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                    TotalHpp += ToDouble(hpp) * ToDouble(dtab.Rows(T1).Item("partsqty"))
                Next

                '--------------------------------------
                'COA                D           C
                'Coa Sementara  1.000.0000,-
                'Persediaan                 1.000.000,-
                '--------------------------------------

                sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,type) VALUES " & _
                "('" & CompnyCode & "','" & CabangFinalCode.Text & "'," & Integer.Parse(iGlMst) & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','TRNFINAL(" & FinalNo.Text & ")','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'FIN')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(iGlMst) & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '--- GL DEBET ---
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 1 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaSementara) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & FinalNo.Text & "','TRNFINAL(" & FinalNo.Text & ")','" & FinOid.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & CabangFinalCode.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                igldtl = igldtl + 1

                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid, acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate, upduser,updtime,branch_code) VALUES " & _
                "('" & CompnyCode & "'," & Integer.Parse(igldtl) & "," & 2 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaGudang) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & ",'" & FinalNo.Text & "','TRNFINAL(" & FinalNo.Text & ")','" & FinOid.Text & "','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & CabangFinalCode.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '---- COA PERSEDIAAN BARANG RUSAK ----
                Dim OidCoaRusak As Integer = 0 : Dim CoaRusak As String = ""
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_STOK_RUSAK' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : CoaRusak = xCmd.ExecuteScalar
                If CoaRusak = "?" Or CoaRusak = "0" Or CoaRusak = "" Then
                    sMsg &= "- Maaf,interface 'VAR_STOK_RUSAK' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & CoaRusak & "'"
                    xCmd.CommandText = sSql : OidCoaRusak = xCmd.ExecuteScalar
                    If OidCoaRusak = 0 Or OidCoaRusak = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk PERSEDIAAN BARANG RUSAK CABANG " & CabangFinal.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                '---- COA BIAYA PEMAKAIAN MATERIAL ----
                Dim COAbiaya As String = "" : Dim OidCOAbiaya As Integer = 0
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_BIAYA_RUSAK' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : COAbiaya = xCmd.ExecuteScalar
                If COAbiaya = "?" Or COAbiaya = "0" Or COAbiaya = "" Then
                    sMsg &= "- Maaf,interface 'VAR_BIAYA_RUSAK' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & COAbiaya & "'"
                    xCmd.CommandText = sSql : OidCOAbiaya = xCmd.ExecuteScalar
                    If OidCOAbiaya = 0 Or OidCOAbiaya = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk COA BIAYA PEMAKAIAN SPAREPART CABANG " & CabangFinal.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                Dim HppRusak As Double = 0
                If TypeFInal.Text = "REPLACE" Then
                    If Not Session("itemdetail") Is Nothing Then
                        Dim dtb1 As DataTable = Session("itemdetail")
                        For T1 As Integer = 0 To dtb1.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & dtb1.Rows(T1).Item("itemreqoid") & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            HppRusak += ToDouble(hpp) * ToDouble(dtb1.Rows(T1).Item("itemqty"))
                        Next
                    End If
                Else
                    If Not Session("itemrusak") Is Nothing Then
                        Dim dtb1 As DataTable = Session("itemrusak")
                        For T1 As Integer = 0 To dtb1.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & dtb1.Rows(T1).Item("itemrusakoid") & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            HppRusak += ToDouble(hpp) * ToDouble(dtb1.Rows(T1).Item("qtyrusak"))
                        Next
                    End If
                End If

                If sMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(sMsg, CompnyName & " - WARNING", 2)
                    Exit Sub
                End If

                '---- COA BIAYA PEMAKAIAN MATERIAL ----
                '--------------------------------------------------
                'COA                            D           C
                'PERSEDIAAN BARANG RUSAK    1.000.0000,-
                'BIAYA PEMAKAIAN MATERIAL               1.000.000,-
                '--------------------------------------------------
                '--- GL DEBET ---
                igldtl = igldtl + 1
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 3 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaRusak) & ",'D'," & ToDouble(HppRusak) & "," & ToDouble(HppRusak) & ",0,'" & FinalNo.Text & "','TRNFINAL(" & FinalNo.Text & ")','" & FinOid.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & CabangFinalCode.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                igldtl = igldtl + 1

                '--- GL CREDIT ---
                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid, acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glflag,glpostdate, upduser,updtime,branch_code) VALUES " & _
                "('" & CompnyCode & "'," & Integer.Parse(igldtl) & "," & 4 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCOAbiaya) & ",'C'," & ToDouble(HppRusak) & "," & ToDouble(HppRusak) & ",0,'" & FinalNo.Text & "','TRNFINAL(" & FinalNo.Text & ")','" & FinOid.Text & "','','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & CabangFinalCode.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid = " & igldtl + 1 & " Where tablename = 'QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '---- End Proses Insert Auto Jurnal ---- 

                ' Update Approval
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_TRNFINAL' " & _
                        "and oid=" & FinOid.Text & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_TRNFINAL' " & _
                       "and oid=" & FinOid.Text & " " & _
                       "and approvaluser<>'" & Session("UserId") & _
                       "' and branch_code='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        SetFinalNya()
    End Sub

    Protected Sub BtnRejectFinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRejectFinal.Click
        Dim sMsg As String = ""
        If FinalNo.Text = "" Then
            sMsg &= "Maaf, Data transaksi Kosong..!!<br />"
        End If

        If IsDBNull(Session("trfwhserviceoid")) Then
            sMsg &= "Maaf, Data transaksi Kosong..!!<br />"
        Else
            If Session("itemdetail") Is Nothing Then
                sMsg &= "Maaf, Data detail Kosong..!!<br />"
            End If
        End If

        sSql = "Select FINOID, FINOID, FINSTATUS from QL_TRNFINAL Where FINSTATUS IN ('APPROVED','APPROVED','POST') and FINOID=" & Integer.Parse(FinOid.Text) & " AND BRANCH_CODE='" & CabangFinalCode.Text & "'"
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                sMsg &= "- Maaf, No. draft final '" & dta.Rows(i).Item("FINOID") & "' sudah di '" & dta.Rows(i).Item("FINSTATUS") & "' dengan nomer '" & dta.Rows(i).Item("FINOID") & "'..!!<br>"
            Next
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "UPDATE QL_TRNFINAL SET FINSTATUS = 'Rejected' Where Finoid=" & FinOid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & FinOid.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "Update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "And tablename='QL_TRNFINAL' and oid=" & FinOid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br /> " & sSql, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        SetFinalNya()
    End Sub

    Protected Sub BtnRevFinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRevFinal.Click
        'lbl_reviseTWService.Focus()
        KetRev.Text = "Keterangan Revisi :" : KetRevisi.Visible = True
        BtnAppFinal.Visible = False : BtnRejectFinal.Visible = False
        BtnRevFinal.Visible = False : BtnBackFinal.Visible = False
        BtnSubmitFinal.Visible = True : BtnCancelFinal.Visible = True
    End Sub

    Protected Sub BtnBackFinal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnBackFinal.Click
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub BtnSubmitFinal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubmitFinal.Click
        If KetRevisi.Text = "" Then
            showMessage("- Maaf, Revise note harus diisi", CompnyName, 2)
            Exit Sub
        Else
            sSql = "Select FINOID, FINOID, FINSTATUS from QL_TRNFINAL Where FINSTATUS IN ('APPROVED','APPROVED','POST') and FINOID=" & Integer.Parse(FinOid.Text) & " AND BRANCH_CODE='" & CabangFinalCode.Text & "'"
            Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
            If ToDouble(dta.Rows.Count) > 0 Then
                For i As Integer = 0 To dta.Rows.Count - 1
                    showMessage("- Maaf, No. draft final '" & dta.Rows(i).Item("FINOID") & "' sudah di '" & dta.Rows(i).Item("FINSTATUS") & "' dengan nomer '" & dta.Rows(i).Item("FINOID") & "'..!!<br>", CompnyName & " - WARNING", 2)
                Next
            End If

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans

            Try ' INSERT
                sSql = "UPDATE QL_TRNFINAL SET FINSTATUS='Revised', FINITEMSTATUS='Revised', trnfinalnote = '" & Tchar(FinalNote.Text) & " ~ " & Tchar(KetRevisi.Text) & "' WHERE finoid = " & FinOid.Text & " and toBranch = '" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "delete from QL_approval WHERE cmpcode='" & CompnyCode & "' and tablename='QL_TRNFINAL' and oid=" & FinOid.Text & " and branch_code ='" & CabangFinalCode.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If

                btnReviseSubmitSO.Attributes.Add("OnClick", "javascript:return confirm('Success Submit Revision');")
                Response.Redirect("WaitingAction.aspx")
            Catch ex As Exception
                showMessage(ex.ToString, CompnyName, 2)
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub BtnCancelFinal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelFinal.Click
        Response.Redirect("WaitingAction.aspx")
    End Sub

    Protected Sub LkbFinalInternal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbFinalInternal.Click
        SetFinalInt()
    End Sub

    Private Sub SetFinalInt()
        BindFinalInt() : MultiView1.SetActiveView(View44)
    End Sub

    Private Sub BindFinalInt()
        Try
            Dim QlFin As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnfinalintmst' And approvaluser='" & Session("UserID") & "' order by approvallevel")
            Dim sCodeFint As String = ""
            Dim FintCode() As String = QlFin.Split(",")
            For C1 As Integer = 0 To FintCode.Length - 1
                If FintCode(C1) <> "" Then
                    sCodeFint &= "'" & FintCode(C1).Trim & "',"
                End If
            Next

            sSql = "Select Distinct tw.finoid Trfwhserviceoid, tw.branch_code, b.approvaloid, tw.trnfinalintno TrfwhserviceNo, tw.finaldate Trfwhservicedate, tw.upduser, b.requestuser, b.requestdate,(Select custname From QL_mstcust sp Where sp.custoid=tw.custoid) SuppName,(Select gendesc from QL_mstgen gc Where gc.gencode=tw.branch_code AND gc.gengroup='CABANG') Cabang From QL_trnfinalintmst tw inner join ql_approval b on tw.FINOID = b.oid AND b.branch_code=tw.branch_code Where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("UserId") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.finitemstatus = 'in approval' and b.tablename = 'QL_trnfinalintmst' and tw.branch_code IN (" & Left(sCodeFint, sCodeFint.Length - 1) & ") Order By tw.finoid desc"
            FillGV(GvFinalInt, sSql, "GvFinalNya")
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub lkbSelectFinalInt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("trfwhserviceoid") = sender.ToolTip : MultiView1.SetActiveView(View45)
            Session("approvaloid") = sender.CommandArgument
            Session("BranchReq") = sender.CommandName

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "Select fm.finoid,fm.branch_code,cbp.gendesc FinalCabang,fm.trnfinalintno,fm.finaldate,rqm.reqcode,cba.gencode CdCabangAs ,cba.gendesc CabangAsal,fm.finitemstatus,cu.custoid,cu.custname,rqm.reqoid,cu.phone1,fm.finalnote,fm.finaldate,fm.updtime,fm.upduser,fm.createtime,fm.createuser,fm.locoidrusak,typetts,flagfinal From QL_trnfinalintmst fm Inner Join QL_TRNREQUEST rqm ON rqm.reqoid=fm.reqoid Inner Join QL_mstcust cu ON cu.custoid=rqm.reqcustoid AND rqm.branch_code=cu.branch_code Inner Join QL_mstgen cbp ON cbp.gencode=fm.branch_code AND cbp.gengroup='Cabang' Inner Join QL_mstgen cba ON cba.gencode=fm.cabangasal AND cbp.gengroup='Cabang' Where fm.finoid=" & Session("trfwhserviceoid") & ""
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                'rowstate = 1
                While xreader.Read
                    NoFinalInt.Text = xreader("trnfinalintno").ToString
                    TglFinalInt.Text = Format(xreader("finaldate"), "dd/MM/yyyy")
                    FinalCabang.Text = xreader("FinalCabang").ToString
                    FinalCust.Text = xreader("custname").ToString
                    ReqNoFinal.Text = xreader("reqcode").ToString
                    FinalIntNote.Text = xreader("finalnote").ToString
                    BranchCodeFinal.Text = xreader("branch_code").ToString
                    FinoidInt.Text = xreader("finoid")
                    CustOidIntFin.Text = Integer.Parse(xreader("custoid"))
                End While
            End If
            xreader.Close() : conn.Close()

            '--- Detail Barang Rusak ---
            sSql = "Select fd1.finaldtloid1,fd1.finoid,fd1.seq,fd1.hpp,snno,fd1.notedtl1,reqoid reqmstoid,fd1.reqdtloid,i.itemdesc,mt.gendesc gudang,fd1.mtrlocoid mtrloc,fd1.qty,fd1.itemoid,fd1.hpp From QL_trnfinalintdtl1 fd1 Inner Join QL_mstitem i On i.itemoid=fd1.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd1.mtrlocoid AND mt.gengroup='LOCATION' Where fd1.finoid=" & Integer.Parse(FinoidInt.Text) & " Order By fd1.seq"

            Dim dtab As DataTable = cKon.ambiltabel2(sSql, "itemdetail")
            Session("itemdetail") = dtab : GVFinIntRusak.DataSource = dtab
            GVFinIntRusak.DataBind()

            '--- Detail Sparepart ---
            sSql = "Select fd2.seq,fd2.itemoid PartOid,fd2.trnfinaldtloid2,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl2 notedtl1,mt.gendesc Gudang,fd2.reqoid,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp,fd2.finoid,fd2.itemoid itemrusakoid From QL_trnfinalintdtl2 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & Integer.Parse(FinoidInt.Text) & " Order By fd2.seq"
            Dim dtab1 As DataTable = cKon.ambiltabel2(sSql, "SpartDtl")
            Session("SpartDtl") = dtab1 : GvParts.DataSource = dtab1
            GvParts.DataBind()

            '--- Detail Barang Rombeng ---
            sSql = "Select fd2.seq,fd2.itemoid itemrombengoid,fd2.trnfinaldtloid3,itemrusakoid,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl3,mt.gendesc Gudang,fd2.reqoid,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp,fd2.finoid,fd2.itemoid itemrusakoid From QL_trnfinalintdtl3 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & Integer.Parse(FinoidInt.Text) & " Order By fd2.seq"
            Dim dtab2 As DataTable = cKon.ambiltabel2(sSql, "ItemRombeng")
            Session("ItemRombeng") = dtab2 : GvRombeng.DataSource = dtab2
            GvRombeng.DataBind()

            '--- Detail Barang Bagus ---
            sSql = "Select fd2.seq,fd2.itemoid itembagusoid,fd2.finaldtloid4,itemrusakoid,i.itemcode,i.itemdesc,fd2.qty,fd2.notedtl4,mt.gendesc Gudang,fd2.reqoid,fd2.reqdtloid,fd2.mtrlocoid,fd2.hpp,fd2.finoid,fd2.itemoid itemrusakoid From QL_trnfinalintdtl4 fd2 Inner Join QL_mstitem i On i.itemoid=fd2.itemoid Inner Join QL_mstgen mt ON mt.genoid=fd2.mtrlocoid AND mt.gengroup='LOCATION' Where fd2.finoid=" & Integer.Parse(FinoidInt.Text) & " Order By fd2.seq"
            Dim dtab3 As DataTable = cKon.ambiltabel2(sSql, "GvBagus")
            Session("ItemBagus") = dtab3 : GvBagus.DataSource = dtab3
            GvBagus.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub BtnAppFinInt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAppFinInt.Click
        Dim iGlMst As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim igldtl As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim ConMtroid As Integer = GenerateID("QL_conmtr", CompnyCode)
        Dim sCode As String = "", sMsg As String = "", iGlSeq As Integer = 0 : Dim SaldoAkhire As Double = 0, iCurID As Integer = 0, mVar As String = ""

        sSql = "Select finoid, trnfinalintno, finitemstatus from QL_trnfinalintmst Where finitemstatus IN ('APPROVED','Rejected') AND finoid=" & Integer.Parse(FinoidInt.Text)
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                sMsg &= "- Maaf, No. draft final '" & dta.Rows(i).Item("finoid") & "' sudah di '" & dta.Rows(i).Item("finitemstatus") & "' dengan nomer '" & dta.Rows(i).Item("trnfinalintno") & "'..!!<br>"
            Next
        End If

        If Not Session("itemdetail") Is Nothing Then
            Dim dtab As DataTable = Session("itemdetail")
            If dtab.Rows.Count > 0 Then
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "Select saldoakhir From (Select ISNULL(SUM(qtyIn)-SUM(qtyOut),0.00) saldoAkhir From QL_conmtr con INNER JOIN ql_mstitem m ON m.itemoid=con.refoid Where refoid=" & Integer.Parse(dtab.Rows(j).Item("ItemOid")) & " AND mtrlocoid=" & Integer.Parse(dtab.Rows(j).Item("mtrloc")) & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "' AND branch_code='" & BranchCodeFinal.Text & "' Group By mtrlocoid,periodacctg,branch_code,m.stockflag,m.itemoid) tblitem"
                    If Integer.Parse(dtab.Rows(j).Item("ItemOid")) <> 0 Then
                        SaldoAkhire = GetScalar(sSql)
                        If SaldoAkhire = Nothing Or SaldoAkhire = 0 Then
                            sMsg &= "- Maaf, Stok akhir untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' tidak memenuhi..!!<br>"
                        End If
                    End If
                Next
            End If
        Else
            sMsg &= "- Maaf data detail kosong mohon di cek data di list form service..!!"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        Dim statusUser As String = "", level As String = "", maxLevel As String = ""
        level = GetStrData("Select isnull(max(approvallevel),1) from QL_approvalstructure Where cmpcode='" & CompnyCode & "' and tablename='QL_trnfinalintmst' And approvaluser='" & Session("UserId") & "'")

        maxLevel = GetStrData("select isnull(max(approvallevel),1) From QL_approvalstructure Where cmpcode='" & CompnyCode & "' And tablename='QL_trnfinalintmst'")

        statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' And tablename='QL_trnfinalintmst' And approvaluser='" & Session("UserId") & "' And approvallevel=" & level & "")

        'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
        If level + 1 <= maxLevel Then
            'ambil data approval, tampung di datatable
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnfinalintmst' and approvallevel=" & level + 1 & ""

            Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If statusUser = "FINAL" Then
                Dim Branch As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & BranchCodeFinal.Text & "' AND gengroup='CABANG'")
                sCode = "FIT/" & Branch & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                sSql = "SELECT ISNULL(MAX(ABS(replace(trnfinalintno,'" & sCode & "',''))),0) trnfinalintno FROM QL_trnfinalintmst WHERE trnfinalintno LIKE '" & sCode & "%'"
                xCmd.CommandText = sSql
                If Not IsDBNull(xCmd.ExecuteScalar) Then
                    iCurID = xCmd.ExecuteScalar + 1
                Else
                    iCurID = 1
                End If
                sCode = GenNumberString(sCode, "", iCurID, 4)
                NoFinalInt.Text = sCode

                sSql = "UPDATE QL_trnfinalintmst SET [finaldate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [finitemstatus] ='APPROVED', [UPDUSER] = '" & Session("UserID") & "', [UPDTIME]=Current_timestamp, [trnfinalintno] = '" & Tchar(NoFinalInt.Text) & "' WHERE [finoid]=" & Integer.Parse(FinoidInt.Text) & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("itemdetail") Is Nothing Then
                    Dim dtab As DataTable = Session("itemdetail")
                    If dtab.Rows.Count > 0 Then
                        For t As Integer = 0 To dtab.Rows.Count - 1
                            '-----------------------------------------------------------
                            '--- Create Stok Out Detail Barang Rusak di gudang rusak ---
                            sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "','" & BranchCodeFinal.Text & "'," & ConMtroid + 1 & ",'FINALINT',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & NoFinalInt.Text & "','" & Integer.Parse(dtab.Rows(t).Item("finaldtloid1")) & "','QL_trnfinalintdtl1', " & Integer.Parse(dtab.Rows(t).Item("itemoid")) & ",'QL_MSTITEM',945," & Integer.Parse(dtab.Rows(t).Item("mtrloc")) & ", 0," & ToDouble(dtab.Rows(t).Item("qty")) & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(dtab.Rows(t).Item("hpp")) * ToDouble(dtab.Rows(t).Item("qty")) & ",'" & Tchar(ReqNoFinal.Text) & "-" & NoFinalInt.Text & "','" & ToDouble(dtab.Rows(t).Item("hpp")) & "'," & Integer.Parse(dtab.Rows(t).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                            '-------------------------------------------------------
                            'End Create Stok Out Detail Barang Rusak di gudang rusak
                        Next
                    End If
                End If

                '--- Create Stok Out Detail Sparepart ----
                If Not Session("SpartDtl") Is Nothing Then
                    Dim dtab1 As DataTable = Session("SpartDtl")
                    If dtab1.Rows.Count > 0 Then
                        For ie As Int64 = 0 To dtab1.Rows.Count - 1
                            sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "','" & BranchCodeFinal.Text & "'," & Integer.Parse(ConMtroid) + 1 & ",'FINALINT',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & NoFinalInt.Text & "','" & Integer.Parse(dtab1.Rows(ie).Item("trnfinaldtloid2")) & "','QL_trnfinalintdtl2'," & Integer.Parse(dtab1.Rows(ie).Item("PartOid")) & ",'QL_MSTITEM',945," & Integer.Parse(dtab1.Rows(ie).Item("mtrlocoid")) & ",0," & ToDouble(dtab1.Rows(ie).Item("qty")) & ",'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(dtab1.Rows(ie).Item("qty") * dtab1.Rows(ie).Item("hpp")) & ",'" & Tchar(ReqNoFinal.Text) & "-" & NoFinalInt.Text & "'," & ToDouble(dtab1.Rows(ie).Item("hpp")) & "," & Integer.Parse(dtab1.Rows(ie).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                            'End proses input stok Out Detail Sparepart
                            '------------------------------------------
                        Next
                    End If
                End If

                If Not Session("ItemRombeng") Is Nothing Then
                    Dim dtab2 As DataTable = Session("ItemRombeng")
                    If dtab2.Rows.Count > 0 Then
                        For t As Integer = 0 To dtab2.Rows.Count - 1
                            '---------------------------------------------------
                            'mulai proses input stok masuk Detail Barang Rombeng                 
                            sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "','" & BranchCodeFinal.Text & "'," & Integer.Parse(ConMtroid) + 1 & ",'FINALINT',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & NoFinalInt.Text & "','" & Integer.Parse(dtab2.Rows(t).Item("trnfinaldtloid3")) & "','QL_trnfinalintdtl3', " & Integer.Parse(dtab2.Rows(t).Item("itemrombengoid")) & ",'QL_MSTITEM',945," & Integer.Parse(dtab2.Rows(t).Item("mtrlocoid")) & "," & ToDouble(dtab2.Rows(t).Item("qty")) & ",0,'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(dtab2.Rows(t).Item("hpp")) * ToDouble(dtab2.Rows(t).Item("qty")) & ",'" & Tchar(ReqNoFinal.Text) & "-" & NoFinalInt.Text & "','" & ToDouble(dtab2.Rows(t).Item("hpp")) & "'," & Integer.Parse(dtab2.Rows(t).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                            '-------------------------------------------
                            'End Create Stok masuk Detail Barang Rombeng
                        Next
                    End If
                End If

                If Not Session("ItemBagus") Is Nothing Then
                    Dim dtab3 As DataTable = Session("ItemBagus")
                    If dtab3.Rows.Count > 0 Then
                        For t As Integer = 0 To dtab3.Rows.Count - 1
                            '-----------------------------------------------------------
                            '--- Create Stok Out Detail Barang Rusak di gudang rusak ---
                            sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP,conrefoid) " & _
                            "VALUES ('" & CompnyCode & "','" & BranchCodeFinal.Text & "'," & ConMtroid + 1 & ",'FINALINT',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','" & NoFinalInt.Text & "','" & Integer.Parse(dtab3.Rows(t).Item("finaldtloid4")) & "','QL_trnfinalintdtl4'," & Integer.Parse(dtab3.Rows(t).Item("itembagusoid")) & ",'QL_MSTITEM',945," & Integer.Parse(dtab3.Rows(t).Item("mtrlocoid")) & "," & ToDouble(dtab3.Rows(t).Item("qty")) & ",0,'','" & Session("UserID") & "',CURRENT_TIMESTAMP,0,0," & ToDouble(dtab3.Rows(t).Item("hpp")) * ToDouble(dtab3.Rows(t).Item("qty")) & ",'" & Tchar(ReqNoFinal.Text) & "-" & NoFinalInt.Text & "','" & ToDouble(dtab3.Rows(t).Item("hpp")) & "'," & Integer.Parse(dtab3.Rows(t).Item("reqdtloid")) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                            '-------------------------------------------------------
                            'End Create Stok Out Detail Barang Rusak di gudang rusak
                        Next
                    End If

                End If

                sSql = "update QL_mstoid set lastoid = " & Integer.Parse(ConMtroid) & " Where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime,type) VALUES " & _
                "('" & CompnyCode & "','" & BranchCodeFinal.Text & "'," & Integer.Parse(iGlMst) & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetPeriodAcctg(GetServerTime()) & "','FINALINT(" & ReqNoFinal.Text & ")','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'FIT')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid = " & Integer.Parse(iGlMst) & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '---- COA PERSEDIAAN BARANG RUSAK ----
                Dim TotalHpp As Double = 0
                Dim OidCoaRusak As Double = 0 : Dim CoaRusak As String = ""
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_STOK_RUSAK' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : CoaRusak = xCmd.ExecuteScalar
                If CoaRusak = "?" Or CoaRusak = "0" Or CoaRusak = "" Then
                    sMsg &= "- Maaf, interface 'VAR_STOK_RUSAK' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & CoaRusak & "'"
                    xCmd.CommandText = sSql : OidCoaRusak = xCmd.ExecuteScalar
                    If ToDouble(OidCoaRusak) = 0 Or ToDouble(OidCoaRusak) = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk PERSEDIAAN BARANG RUSAK CABANG " & FinalCabang.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                '---- Jurnal barang rombeng ----
                Dim OidCoaRombeng As Double = 0 : Dim CoaRombeng As String = ""
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_STOK_ROMBENG' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : CoaRombeng = xCmd.ExecuteScalar
                If CoaRombeng = "?" Or CoaRombeng = "0" Or CoaRombeng = "" Then
                    sMsg &= "- Maaf, interface 'VAR_STOK_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & CoaRombeng & "'"
                    xCmd.CommandText = sSql : OidCoaRombeng = xCmd.ExecuteScalar
                    If ToDouble(OidCoaRombeng) = 0 Or ToDouble(OidCoaRombeng) = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk PERSEDIAAN BARANG ROMBENG CABANG " & FinalCabang.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                '---- COA BIAYA PEMAKAIAN SPAREPART ----
                Dim COAbiayaRombeng As String = "", OidCOAbiayaRombeng As Integer = 0
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_BIAYA_ROMBENG' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : COAbiayaRombeng = xCmd.ExecuteScalar
                If COAbiayaRombeng = "?" Or COAbiayaRombeng = "0" Or COAbiayaRombeng = "" Then
                    sMsg &= "- Maaf, interface 'VAR_BIAYA_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & COAbiayaRombeng & "'"
                    xCmd.CommandText = sSql : OidCOAbiayaRombeng = xCmd.ExecuteScalar
                    If ToDouble(OidCOAbiayaRombeng) = 0 Or ToDouble(OidCOAbiayaRombeng) = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk COA BIAYA PEMAKAIAN SPAREPART CABANG " & FinalCabang.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                '--- COA PERSEDIAAN BARANG DAGANGAN ---
                Dim iMatAcctg As String = "", OidCoaGudang As Integer = 0
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_GUDANG' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : iMatAcctg = xCmd.ExecuteScalar

                If iMatAcctg = "?" Or iMatAcctg = "0" Or iMatAcctg = "" Then
                    sMsg &= "- Maaf, interface 'VAR_GUDANG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & iMatAcctg & "'"
                    xCmd.CommandText = sSql : OidCoaGudang = xCmd.ExecuteScalar
                    If ToDouble(OidCoaGudang) = 0 Or ToDouble(OidCoaGudang) = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk VAR_GUDANG tidak ditemukan..!!<br>"
                    End If
                End If

                '---- COA BIAYA PEMAKAIAN MATERIAL ---- 
                Dim COAbiayaRusak As String = "" : Dim OidCOAbiayaRusak As Integer = 0
                sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='VAR_BIAYA_RUSAK' and cmpcode='" & CompnyCode & "' AND interfaceres1='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : COAbiayaRusak = xCmd.ExecuteScalar
                If COAbiayaRusak = "?" Or COAbiayaRusak = "0" Or COAbiayaRusak = "" Then
                    sMsg &= "- Maaf,interface 'VAR_BIAYA_RUSAK' belum di setting silahkan hubungi admin untuk seting interface..!!<br>"
                Else
                    sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & CompnyCode & "' AND ACCTGCODE ='" & COAbiayaRusak & "'"
                    xCmd.CommandText = sSql : OidCOAbiayaRusak = xCmd.ExecuteScalar
                    If ToDouble(OidCOAbiayaRusak) = 0 Or ToDouble(OidCOAbiayaRusak) = Nothing Then
                        sMsg &= "- Maaf, Akun COA untuk COA BIAYA PEMAKAIAN SPAREPART CABANG " & CabangFinal.Text & " tidak ditemukan..!!<br>"
                    End If
                End If

                If sMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(sMsg, CompnyName & " - WARNING", 2)
                    Exit Sub
                End If

                If Not Session("itemrusak") Is Nothing Then
                    If ToDouble(OidCOAbiayaRusak) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, Akun COA untuk COA BIAYA PEMAKAIAN SPAREPART CABANG " & CabangFinal.Text & " tidak ditemukan..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If ToDouble(OidCoaRusak) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, Akun COA untuk PERSEDIAAN BARANG RUSAK CABANG " & FinalCabang.Text & " tidak ditemukan..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim dtb1 As DataTable = Session("itemdetail")
                    If dtb1.Rows.Count > 0 Then
                        For T1 As Integer = 0 To dtb1.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & dtb1.Rows(T1).Item("itemoid") & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            TotalHpp += ToDouble(hpp)
                        Next

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 1 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaRusak) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 2 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCOAbiayaRusak) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                    End If
                End If

                '---- COA Gudang bagus ---- 
                If Not Session("SpartDtl") Is Nothing Then
                    If ToDouble(OidCoaGudang) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_GUDANG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If ToDouble(OidCOAbiayaRombeng) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_BIAYA_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim dtab1 As DataTable = Session("SpartDtl")
                    If dtab1.Rows.Count > 0 Then
                        For ie As Integer = 0 To dtab1.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab1.Rows(ie).Item("PartOid")) & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            TotalHpp += ToDouble(hpp)
                        Next

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 3 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCOAbiayaRombeng) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                       " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 4 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaGudang) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1
                    End If
                End If


                If Not Session("ItemRombeng") Is Nothing Then
                    '---- COA PERSEDIAAN BARANG RUSAK ----
                    If ToDouble(OidCoaRombeng) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If ToDouble(OidCOAbiayaRombeng) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_BIAYA_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim dtab2 As DataTable = Session("ItemRombeng")
                    If dtab2.Rows.Count > 0 Then
                        For t As Integer = 0 To dtab2.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab2.Rows(t).Item("itemrombengoid")) & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            TotalHpp += ToDouble(hpp)
                        Next
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 5 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaRombeng) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                       " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 6 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCOAbiayaRombeng) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1
                    End If
                End If

                If Not Session("ItemBagus") Is Nothing Then
                    If ToDouble(OidCOAbiayaRombeng) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_BIAYA_ROMBENG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    If ToDouble(OidCoaGudang) = 0 Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("- Maaf, interface 'VAR_GUDANG' belum di setting silahkan hubungi admin untuk seting interface..!!<br>", CompnyName & " - WARNING", 2)
                        Exit Sub
                    End If
                    Dim dtab3 As DataTable = Session("ItemBagus")
                    If dtab3.Rows.Count > 0 Then
                        For t As Integer = 0 To dtab3.Rows.Count - 1
                            sSql = "Select HPP from QL_mstitem Where itemoid=" & Integer.Parse(dtab3.Rows(t).Item("itembagusoid")) & ""
                            xCmd.CommandText = sSql : Dim hpp As Double = xCmd.ExecuteScalar
                            TotalHpp += ToDouble(hpp)
                        Next
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                        " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 7 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCoaGudang) & ",'D'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt,glamtidr,glamtusd, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, branch_code) VALUES " & _
                       " ('" & CompnyCode & "', " & Integer.Parse(igldtl) & "," & 8 & "," & Integer.Parse(iGlMst) & "," & Integer.Parse(OidCOAbiayaRombeng) & ",'C'," & ToDouble(TotalHpp) & "," & ToDouble(TotalHpp) & "," & 0 & ",'" & NoFinalInt.Text & "','FINALINT(" & NoFinalInt.Text & ")','" & FinoidInt.Text & "','','POST',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & BranchCodeFinal.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl = igldtl + 1

                    End If
                End If

                sSql = "Update QL_mstoid set lastoid = " & igldtl + 1 & " Where tablename = 'QL_trngldtl' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If sMsg <> "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage(sMsg, CompnyName & " - WARNING", 2)
                    Exit Sub
                End If

                ' Update Approval
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_trnfinalintmst' " & _
                        "and oid=" & FinoidInt.Text & " " & _
                        "and approvaluser='" & Session("UserId") & _
                        "' And branch_code='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_trnfinalintmst' " & _
                       "and oid=" & FinoidInt.Text & " " & _
                       "and approvaluser<>'" & Session("UserId") & _
                       "' and branch_code='" & BranchCodeFinal.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        SetFinalInt()
    End Sub

    Protected Sub BtnRejectFinInt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRejectFinInt.Click
        If IsDBNull(Session("trfwhserviceoid")) Then
            showMessage("Please select a data first", CompnyName & " - WARNING", 2) : Exit Sub
        Else
            If Session("trfwhserviceoid") = "" Then
                showMessage("Maaf, data detail kosong", CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If

        sSql = "Select finoid, trnfinalintno, finitemstatus from QL_trnfinalintmst Where finitemstatus IN ('APPROVED','Rejected') AND finoid=" & Integer.Parse(FinoidInt.Text)
        Dim dta As DataTable = cKon.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                showMessage("- Maaf, No. draft final '" & dta.Rows(i).Item("finoid") & "' sudah di '" & dta.Rows(i).Item("finitemstatus") & "' dengan nomer '" & dta.Rows(i).Item("trnfinalintno") & "'..!!<br>", CompnyName & " - WARNING", 2)
            Next
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            sSql = "UPDATE QL_trnfinalintmst SET finitemstatus = 'Rejected' Where Finoid=" & NoFinalInt.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ' Update Approval
            sSql = "UPDATE ql_approval SET statusrequest = 'Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & FinOid.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'update event user yg lain 
            sSql = "Update QL_approval set approvaldate=CURRENT_TIMESTAMP, " & _
                   "approvalstatus='Ignore',event='Ignore',statusrequest='End' " & _
                   "WHERE cmpcode='" & CompnyCode & "' " & _
                   "And tablename='QL_trnfinalintmst' and oid=" & NoFinalInt.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - WARNING", 1)
            Exit Sub
        End Try
        SetFinalNya()
    End Sub
 
    Protected Sub gvASDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvASDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
        End If
    End Sub

    Protected Sub LkbPb_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LkbPb.Click
        Response.Redirect("WaitingActionNew.aspx?formvalue=QL_TRNREQUESTITEM&formtext=� Penerimaan Barang")
    End Sub

    Protected Sub Lkbrnpg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbRnpg.Click
        Response.Redirect("WaitingActionNew.aspx?formvalue=QL_trnnotahrretur&formtext=� Nota HR Return")
    End Sub

    Protected Sub GVReturNT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVReturNT.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            
        End If
    End Sub
 
    Protected Sub BtnBackSret_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnBackSret.Click
        setSalesReturnActive()
    End Sub

    Protected Sub imbRejectSDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRejectSDO.Click
        Dim lblPesan As String = ""
        If IsDBNull(Session("trnsjjualmstoid")) Then
            lblPesan &= "- Maaf, pilih data dulu..!!<br />"
        Else
            If Session("trnsjjualmstoid") = "" Then
                lblPesan &= "- Maaf, pilih data dulu..!!<br />"
            End If
        End If

        If trnsjjualno.Text = "" Then
            lblPesan &= "- Maaf, pilih data dulu..!!<br />"
        End If

        Dim CekSts As Double = GetScalar("Select Count(trnsjjualmstoid) from ql_trnsjjualmst Where trnsjjualstatus='Approved' And trnsjjualmstoid=" & Session("trnsjjualmstoid") & "")

        If CekSts > 0 Then
            lblPesan &= "- Maaf, Data tidak bisa direject karena telah berstatus 'Approved'..!!<br />"
        End If

        If lblPesan <> "" Then
            showMessage(lblPesan, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnsjjualmst SET trnsjjualstatus = 'Rejected' WHERE trnsjjualmstoid = " & Session("trnsjjualmstoid") & " and branch_code = '" & zCabang.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE ql_approval SET event='Rejected',statusrequest='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & Integer.Parse(ApprovedOid.Text) & "' And branch_code='" & zCabang.Text & "' And tablename='QL_trnsjjualmst'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' Update Approval user yg lain 
            sSql = "update QL_approval set event='Ignore',statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='QL_trnsjjualmst' And oid=" & Session("trnsjjualmstoid") & " AND approvaloid <> '" & ApprovedOid.Text & "' And branch_code='" & zCabang.Text & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1) : Exit Sub
        End Try
        setSDOasActive()
    End Sub

    Protected Sub LblLinkFC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblLinkFC.Click
        Response.Redirect("WaitingActionNew.aspx?formvalue=QL_TRNFORECASTMST&formtext=� Forecast PO")
    End Sub

    Protected Sub imgAppCN_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAppCN.Click
        If IsDBNull(Session("draftno")) Then
            showMessage("Please select Credit Note Data first", CompnyName & " - WARNING", 2)
            Exit Sub
        Else
            If Session("draftno") = "" Then
                showMessage("Please select Credit Note Data first", CompnyName & " - WARNING", 2)
                Exit Sub
            End If
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Dim isExec As Boolean = False
            Dim cRate As New ClassRate
            Dim sDate As String = Format(GetServerTime(), "MM/dd/yyyy")
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim idConaR As Integer, idConap As Integer, iGLMstOid As Integer, iGLDtlOid As Integer
            Dim arStockAcctgOid() As Integer = {0, 0}
            Dim dt As DataTable = Session("TblDtlCN")
            Dim dv As DataView = dt.DefaultView
            Dim UsdRate As Double = Get_USDRate()
            If ToDouble(UsdRate) = 0 Then
                showMessage("Rate Currency Belum Di isi !", "-WARNING", 2)
                Exit Sub
            End If
            'Dim glValue, glIDRValue, glUSDValue As Double
            Dim sNo As String = "" : Dim reqcode As String = ""
            Dim code As String = ""
            Dim BranchCode As String = GetStrData("Select genother1 from QL_mstgen Where gencode='" & branch_code_cn.Text & "' And gengroup='CABANG'")
            code = "CN/" & BranchCode & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(no,4) AS INT)),0) FROM ql_creditnote WHERE no LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            reqcode = code & sequence
            sNo = reqcode

            Dim TblName As String = "" : TblName = "ql_creditnote"
            Dim statusUser As String = "" : Dim level As String = ""
            Dim maxLevel As String = ""
            Dim iglmst As Int64 = GenerateID("QL_trnglmst", CompnyCode)
            Dim igldtl As Int64 = GenerateID("QL_trngldtl", CompnyCode)
            Dim iGlSeq As Int16 = 1
            level = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "'")
            maxLevel = GetStrData("select isnull(max(approvallevel),1) from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' ")
            statusUser = GetStrData("select approvaltype from QL_approvalstructure where cmpcode='" & CompnyCode & "' and tablename='" & TblName & "' and approvaluser='" & Session("UserId") & "' and approvallevel=" & level & "")

            'ambil data approval diatasnya dari master jika msh ada level diatasnya lagi
            If level + 1 <= maxLevel Then
                'ambil data approval, tampung di datatable
                sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus from QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='" & TblName & "' and approvallevel=" & level + 1 & ""

                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                End If
            End If

            If statusUser = "FINAL" Then
                cRate.SetRateValue(1, sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, CompnyName & " - WARNING", 2) : Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, CompnyName & " - WARNING", 2) : Exit Sub
                End If

                idConaR = GenerateID("ql_conar", CompnyCode)
                idConap = GenerateID("ql_conap", CompnyCode)
                iGLMstOid = GenerateID("QL_TRNGLMST", CompnyCode)
                iGLDtlOid = GenerateID("QL_TRNGLDTL", CompnyCode)

                Dim conmtroid As Int32 = GenerateID("QL_conmtr", CompnyCode)
                Dim crdmatoid As Int32 = GenerateID("QL_crdmtr", CompnyCode)
                Dim tempoid As Int32 = 0
            End If
            ' Update Approval
            sSql = "UPDATE QL_approval SET statusrequest='Approved', event='Approved', approvaldate=CURRENT_TIMESTAMP, approvalstatus='Approved' WHERE statusrequest='New' AND event='In Approval' AND tablename='ql_creditnote' AND oid=" & Session("draftno")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            For C1 As Int16 = 0 To dt.Rows.Count - 1
                sSql = "UPDATE ql_creditnote SET cnstatus='Approved', tgl = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP "
                If statusUser = "FINAL" Then
                    sSql &= ", no='" & sNo & "', amountidr=" & ToDouble(dt.Rows(C1)("amount").ToString) & ", amountusd=" & ToDouble(dt.Rows(C1)("amount").ToString) * cRate.GetRateMonthlyIDRValue & ""
                End If
                sSql &= " WHERE cmpcode='" & CompnyCode & "' AND oid=" & dt.Rows(C1)("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            If reftype.Text = "AR" Then
                sSql = " UPDATE QL_trnjualmst SET accumpayment=accumpayment+" & ToDouble(amount.Text) & ", accumpaymentidr = accumpayment+" & ToDouble(amount.Text) & ", accumpaymentusd = accumpaymentusd+" & ToDouble(amount.Text) & " WHERE trnjualmstoid=" & trnjualbelioid.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & branch_code_cn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Dim custgroupoid As String = GetStrData("select custgroupoid from ql_mstcust where custoid = " & cust_supp_oid.Text & "")
                If custgroupoid = "" Then
                    showMessage("Customer ini belum mempunyai Customer Group", CompnyName & " - WARNING", 2) : Exit Sub
                End If

                'update credit limit customer
                sSql = "UPDATE QL_mstcustgroup SET custgroupcreditlimitusagerupiah=custgroupcreditlimitusagerupiah-" & ToDouble(amount.Text) & " WHERE custgroupoid=" & custgroupoid & " and branch_code = '" & branch_code_cn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim tglx As String = ""
                tglx = tgl.Text

                sSql = "INSERT INTO QL_conar (cmpcode,branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans,amttransidr,amttransusd, amtbayar,amtbayaridr,amtbayarusd, trnarnote, trnarres1, upduser, updtime) VALUES ('" & CompnyCode & "', '" & branch_code_cn.Text & "', " & idConaR & ", 'ql_creditnote', " & trnjualbelioid.Text & ", " & oid.Text & ", " & cust_supp_oid.Text & ", " & coa_debet.Text & ", 'POST', 'CNAR', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_credit.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & sNo & "', 0, CURRENT_TIMESTAMP, 0, 0, 0, " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", 'CN No. " & sNo & " for A/R | No. " & trnjualbelino.Text & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ElseIf reftype.Text = "NT" Then
                sSql = "INSERT INTO QL_conar (cmpcode, branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amttransidr, amttransusd, amtbayar, amtbayaridr, amtbayarusd, trnarnote, trnarres1, upduser, updtime) VALUES ('" & CompnyCode & "', '" & branch_code_cn.Text & "', " & idConaR & ", 'ql_creditnote', " & trnjualbelioid.Text & ", " & oid.Text & ", " & cust_supp_oid.Text & ", " & coa_debet.Text & ", 'POST', 'CNNT', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_credit.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & sNo & "', 0, CURRENT_TIMESTAMP, 0, 0, 0, " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", 'CN No. " & sNo & " for NT | No. " & trnjualbelino.Text & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            ElseIf reftype.Text = "SRV" Then
                sSql = "INSERT INTO QL_conar (cmpcode, branch_code, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amttransidr, amttransusd, amtbayar, amtbayaridr, amtbayarusd, trnarnote, trnarres1, upduser, updtime) VALUES ('" & CompnyCode & "', '" & branch_code_cn.Text & "', " & idConaR & ", 'ql_creditnote', " & trnjualbelioid.Text & ", " & oid.Text & ", " & cust_supp_oid.Text & ", " & coa_debet.Text & ", 'POST', 'CNSRV', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_credit.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & sNo & "', 0, CURRENT_TIMESTAMP, 0, 0, 0, " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", 'CN No. " & sNo & " for NT | No. " & trnjualbelino.Text & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid=" & idConaR & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conar'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else

                sSql = " UPDATE QL_trnbelimst SET accumpayment=accumpayment-" & ToDouble(amount.Text) & ",accumpaymentidr=accumpayment-" & ToDouble(amount.Text) * cRate.GetRateMonthlyIDRValue & ",accumpaymentusd=accumpaymentusd-" & ToDouble(amount.Text) * cRate.GetRateMonthlyUSDValue & "  WHERE trnbelimstoid=" & trnjualbelioid.Text & "  AND cmpcode='" & CompnyCode & "' and branch_code = '" & branch_code_cn.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_conap (cmpcode, branch_code,conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amttransidr, amttransusd, amtbayar, amtbayaridr,amtbayarusd, trnapnote, trnapres1, upduser, updtime) VALUES " & _
                "('" & CompnyCode & "', '" & branch_code_cn.Text & "', " & idConap & ", 'ql_creditnote',  " & trnjualbelioid.Text & ", " & oid.Text & ", " & cust_supp_oid.Text & ", " & coa_debet.Text & ", 'POST', 'CNAP', '1/1/1900', '" & GetDateToPeriodAcctg(GetServerTime()) & "', " & coa_credit.Text & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '', 0, CURRENT_TIMESTAMP, 0, 0, 0, " & ToDouble(amount.Text) * -1 & ", " & (ToDouble(amount.Text)) * -1 & "," & (ToDouble(amount.Text)) * -1 & ", 'CN No. " & sNo & " for A/P | No. " & trnjualbelino.Text & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & idConap & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conap'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            '//////INSERT INTO TRN GL MST
            sSql = "INSERT into QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type) VALUES ('" & CompnyCode & "','" & branch_code_cn.Text & "'," & iGLMstOid & ", (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg(tgl.Text) & "','CN No. " & sNo & " for " & reftype.Text & " No. " & trnjualbelino.Text & "','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'CN')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            ' =================================
            '       
            '   BIAYA/OTHER     99000
            '           Piutang/hutang  100000
            ' =================================
            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
            " ('" & CompnyCode & "','" & branch_code_cn.Text & "'," & iGLDtlOid & ", 1," & iGLMstOid & "," & coa_debet.Text & ",'D'," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) & ",'" & sNo & "','CN No. " & sNo & " for " & reftype.Text & " No. " & trnjualbelino.Text & "','ql_creditnote','" & oid.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'POST')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            iGLDtlOid += 1

            sSql = "INSERT into QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES ('" & CompnyCode & "','" & branch_code_cn.Text & "'," & iGLDtlOid & ",2," & iGLMstOid & "," & coa_credit.Text & ",'C'," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) & "," & ToDouble(amount.Text) & ",'" & sNo & "','CN No. " & sNo & " for " & reftype.Text & " No. " & trnjualbelino.Text & "','ql_creditnote','" & oid.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'POST')"

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE  QL_mstoid SET lastoid=" & iGLDtlOid & " WHERE tablename='QL_trngldtl' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE  QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_trnglmst' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'recovery credit limit
            sSql = "Update c set custcreditlimitusagerupiah= isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid),0),custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid ),0) from QL_mstcust c "

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    'UpdateMA(sNo, sStatus)
                    Exit Sub
                Else
                    showMessage(exSql.Message, CompnyName & " - ERROR", 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, CompnyName & " - ERROR", 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message & sSql, CompnyName & " - ERROR", 1)
            Exit Sub
        End Try
        setCNasActive()
        imgBackCN_Click(Nothing, Nothing)
    End Sub
End Class
