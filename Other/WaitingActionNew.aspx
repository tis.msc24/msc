<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="WaitingActionNew.aspx.vb" Inherits="Other_WaitingActionNew" title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tableutama" align="center"
        width="100%">
        <tr>
            <td style="width: 200px;" valign="top">
                <table id="tbRight" bgcolor="white" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="height: 15px;" valign="middle" rowspan="2">
                            <asp:Image ID="Image2" runat="server" Height="16px" ImageUrl="~/Images/corner.gif"
                                            Width="16px" ImageAlign="AbsMiddle" />
                            ::
                            <asp:Label ID="lblWelcome" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Navy">Approval Type</asp:Label></th>
                    </tr>
                </table>
                <table class="tabelhias" style="width: 100%;">
                    <tr>
                        <td align="left" rowspan="2" valign="top">
                            <br />
                            <table width="100%">
                                <tr>
                                    <td style="height: 30px">
                            <asp:LinkButton ID="lkbBack" runat="server" CssClass="submenu" Font-Size="Small">� Back</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvMenu" runat="server" AutoGenerateColumns="False" GridLines="None"
                                            ShowHeader="False" Width="100%">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lkbMenuApp" runat="server" CommandArgument='<%# eval("tablename") %>'
                                                            CssClass="submenu" Font-Size="Small" OnClick="lkbMenuApp_Click" Text='<%# eval("appname") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Height="30px" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:UpdatePanel id="upMsgbox" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE cellSpacing=1 cellPadding=1 border=0><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" DropShadow="True" Drag="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel></td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
            <td style="width: 1%;" valign="top">
            </td>
            <td valign="top">
                <table id="Table1" bgcolor="white" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" style="height: 15px;" valign="middle">
                            <img align="absMiddle" alt="" src="../Images/corner1.gif" /> :: 
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                ForeColor="Navy">Approval Data</asp:Label></th>
                    </tr>
                    <tr>
                        <td style="text-align: center" valign="top">
                            <div style="width: 100%; text-align: left">
                                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><STRONG><BR />Following transactions are waiting for your approval :</STRONG><BR /><BR /><TABLE><TBODY><TR><TD align=left colSpan=2><asp:Label id="apppersonlevel" runat="server" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="apppersontype" runat="server" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="AppCmpCode" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:GridView id="gvListApp" runat="server" Width="250px" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="apptype">
<ItemStyle HorizontalAlign="Left" Height="15px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appsept">
<ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appcount">
<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR></TBODY></TABLE></asp:View> <asp:View id="ViewList" runat="server"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="lblAppType" runat="server" Font-Bold="False" Visible="False"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleList" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Font-Underline="True"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlViewList" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 15%" class="Label" align=left><asp:CheckBox id="cbPeriod" runat="server" Text="Period"></asp:CheckBox></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="60px" CssClass="inpText" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="60px" CssClass="inpText" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label9" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label1" runat="server" Text="Request User"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLUser" runat="server" Width="100px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="TDDept1" class="Label" align=left runat="server" visible="false"><asp:CheckBox id="cbDept" runat="server" Text="Department" AutoPostBack="True"></asp:CheckBox></TD><TD id="TDDept2" class="Label" align=center runat="server" visible="false">:</TD><TD id="TDDept3" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="DDLDept" runat="server" Width="200px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Draft No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="FilterDraftNo" runat="server" Width="195px" CssClass="inpText"></asp:TextBox> <asp:Label id="Label4" runat="server" CssClass="Important" Text="Separate each draft no with comma (,)"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="imbDate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="imbDate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbDraftNo" runat="server" TargetControlID="FilterDraftNo" ValidChars="1234567890,"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3><asp:Label id="lblRow" runat="server" Font-Bold="True" Text="Total Row :"></asp:Label> <asp:Label id="rowVal" runat="server" Font-Bold="True" Text="0"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvList" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowSorting="True" CellPadding="4" EmptyDataText="No data in database.">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvList2" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Visible="False" CellPadding="4" EmptyDataText="No data in database.">
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="cbCheckHdr" runat="server" AutoPostBack="True" OnCheckedChanged="cbCheckHdr_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="cbCheck" runat="server" ToolTip='<%# eval("trnoid") %>' />
                        </ItemTemplate>
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    <asp:Label ID="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnClose" runat="server" ImageUrl="~/Images/Close.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View> <asp:View id="ViewData" runat="server"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblTitleData" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Font-Underline="True"></asp:Label> <asp:Label id="updateuser" runat="server" Font-Bold="False" Visible="False"></asp:Label> <asp:Label id="createuser" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblBusUnit" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Visible="False" Font-Underline="False" Font-Strikeout="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataHeader" runat="server" Width="100%" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="datacaption">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Height="5px" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datasept">
<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datavalue">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD id="TD1" class="Label" align=left runat="server" visible="false"></TD><TD id="TD2" class="Label" align=center runat="server" visible="false"></TD><TD id="TD3" class="Label" align=left runat="server" visible="false"><asp:TextBox id="tbRevised" runat="server" Width="247px" CssClass="inpText" Visible="False" MaxLength="100"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlWOClose" runat="server" Width="100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 18%" class="Label" align=left id="TD23" runat="server" visible="false">A] Outstanding DM</TD><TD style="WIDTH: 2%" class="Label" align=center id="TD22" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD13" runat="server" visible="false"><asp:Label id="lbOutstandingDM" runat="server"></asp:Label> <asp:Label id="lbOutstandingDM_USD" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD19" runat="server" visible="false">B] Outstanding DL</TD><TD class="Label" align=center id="TD17" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD20" runat="server" visible="false"><asp:Label id="lbOutstandingDL" runat="server"></asp:Label> <asp:Label id="lbOutstandingDL_USD" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD16" runat="server" visible="false">C] Outstanding FOH</TD><TD class="Label" align=center id="TD24" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD6" runat="server" visible="false"><asp:Label id="lbOutstandingFOH" runat="server"></asp:Label> <asp:Label id="lbOutstandingFOH_USD" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD8" runat="server" visible="false">D] Saldo Awal WIP</TD><TD class="Label" align=center id="TD14" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD11" runat="server" visible="false"><asp:Label id="lbSAWIP" runat="server"></asp:Label> <asp:Label id="lbSAWIP_USD" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD12" runat="server" visible="false">E] Outstanding WIP</TD><TD class="Label" align=center id="TD4" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD9" runat="server" visible="false">A + B + C + D = <asp:Label id="lbOutstandingWIP" runat="server"></asp:Label> <asp:Label id="lbOutstandingWIP_USD" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD7" runat="server" visible="false">F] Outstanding Qty</TD><TD class="Label" align=center id="TD15" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD21" runat="server" visible="false"><asp:Label id="lbOutstandingQty" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left id="TD10" runat="server" visible="false">Outstanding Trans.</TD><TD class="Label" align=center id="TD5" runat="server" visible="false">:</TD><TD class="Label" align=left id="TD18" runat="server" visible="false"><asp:ImageButton id="btnViewOutTrans" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" AlternateText="View Report"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD style="WIDTH: 15%; HEIGHT: 22px" id="TD25" class="Label" align=left runat="server" visible="false"><asp:Label id="lblCaptReturn" runat="server" Visible="False" Text="Reason"></asp:Label></TD><TD style="WIDTH: 2%; HEIGHT: 22px" id="TD26" class="Label" align=center runat="server" visible="false"><asp:Label id="lblSeptReturn" runat="server" Visible="False" Text=":"></asp:Label></TD><TD style="HEIGHT: 22px" id="TD30" class="Label" align=left runat="server" visible="false"><asp:DropDownList id="DDLReturnType" runat="server" Width="150px" CssClass="inpText" Visible="False"><asp:ListItem>Retur Ganti Barang</asp:ListItem>
<asp:ListItem>Retur Tidak Ganti Barang</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD35" class="Label" align=left runat="server" visible="false"><asp:Label id="lblCaptValue" runat="server" Visible="False" Text="Value IDR Per Unit"></asp:Label></TD><TD id="TD34" class="Label" align=center runat="server" visible="false"><asp:Label id="lblSeptValue" runat="server" Visible="False" Text=":"></asp:Label></TD><TD id="TD36" class="Label" align=left runat="server" visible="false"><asp:TextBox id="tbValue" runat="server" Width="100px" CssClass="inpText" MaxLength="14"></asp:TextBox> <asp:ImageButton id="btnInsert" runat="server" ImageUrl="~/Images/download.png" ImageAlign="AbsMiddle" AlternateText="Insert Value"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 22px" id="TD29" class="Label" align=left runat="server" visible="false"><asp:Label id="lblCapTotalIDR" runat="server" Visible="False" Text="Total Value IDR"></asp:Label></TD><TD style="HEIGHT: 22px" id="TD32" class="Label" align=center runat="server" visible="false"><asp:Label id="lblSepTotalIDR" runat="server" Visible="False" Text=":"></asp:Label></TD><TD style="HEIGHT: 22px" id="TD31" class="Label" align=left runat="server" visible="false"><asp:TextBox id="TotalValueIDR" runat="server" Width="120px" CssClass="inpTextDisabled" Visible="False" MaxLength="14"></asp:TextBox> <asp:ImageButton id="btnRefresh" runat="server" ImageUrl="~/Images/calculate.png" ImageAlign="AbsMiddle" Visible="False" AlternateText="Insert Value"></asp:ImageButton></TD></TR><TR><TD id="TD27" class="Label" align=left runat="server" visible="false"></TD><TD id="TD33" class="Label" align=center runat="server" visible="false"></TD><TD id="TD28" class="Label" align=left runat="server" visible="false"><ajaxToolkit:FilteredTextBoxExtender id="ftbValue" runat="server" TargetControlID="tbValue" ValidChars="1234567890,.-">
            </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="lblRow2" runat="server" Font-Bold="True" Text="Total Row :"></asp:Label>&nbsp;<asp:Label id="rowVal2" runat="server" Font-Bold="True" Text="0"></asp:Label><asp:Label id="lblbln3" runat="server" Font-Size="Larger" Visible="False"></asp:Label><asp:Label id="lblbln1" runat="server" Font-Size="Larger" Visible="False"></asp:Label><asp:Label id="lblbln2" runat="server" Font-Size="Larger" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView style="WIDTH: 99%" id="gvPBdtl" runat="server" Width="100%" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln3">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln2">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bln1">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="saldoakhir" HeaderText="Stok Akhir">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ReqQty" HeaderText="Req. Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Accept Qty"><ItemTemplate>
<asp:TextBox id="HppNya" runat="server" Width="61px" CssClass="inpText" Text='<%# String.Format("{0:#,##0.00}", GetHppNya()) %>' ToolTip='<%# Eval("acceptqty") %>' MaxLength="20" __designer:wfdid="w1"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftHppNya" runat="server" TargetControlID="HppNya" ValidChars="1234567890.," __designer:wfdid="w2">
                                    </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Note" HeaderText="Keterangan">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:GridView id="gvData" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataAdj" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4" AutoPostBack="True" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refname" HeaderText="Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reflongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="location" HeaderText="Location">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="laststockroll" HeaderText="Current Roll">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjqtyroll" HeaderText="Adj. Roll">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="newqtyroll" HeaderText="New Roll">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="laststock" HeaderText="Current Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="adjqty" HeaderText="Adj. Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="newqty" HeaderText="New Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Value IDR Per Unit"><ItemTemplate>
<asp:TextBox id="tbValue" runat="server" CssClass="inpText" Font-Size="XX-Small" Width="75px" Text='<%# eval("stockvalueidr") %>' ToolTip='<%# eval("oid") %>' MaxLength="14"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="ftbValue" runat="server" TargetControlID="tbValue" ValidChars="1234567890,.-">
                            </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="stockadjnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small" ForeColor="#333333" BackColor="#FFCC66"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="TEXT-ALIGN: right" class="Label" align=left colSpan=3><asp:Label id="lblpqqty" runat="server" Font-Bold="True" Text="PQ Qty :"></asp:Label><asp:Label id="pqqty" runat="server" Font-Bold="True" Text="0"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataPQ" runat="server" Width="100%" Height="1px" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" EmptyDataText="No data in database." CellPadding="4" AutoPostBack="True" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="pqdtlseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqspec" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqminorder" HeaderText="Min Order">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqpriceex" HeaderText="Price (ex)">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqtax" HeaderText="Tax Amt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqongkir" HeaderText="Ongkos Kirim">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqtotalamt" HeaderText="Total Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqdiscamt" HeaderText="Disc.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqtotalnet" HeaderText="Total Netto">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqpaynote" HeaderText="Ket. Bayar (Hari)">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pqdelivnote">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Qty Acc"><ItemTemplate>
                                        <asp:TextBox ID="tbQtyAcc" runat="server" CssClass="inpText" Font-Size="XX-Small"
                                            MaxLength="14" Text='<%# eval("pqappqty") %>' ToolTip='<%# eval("pqdtloid") %>'
                                            Width="50px"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbQtyAcc" runat="server" TargetControlID="tbQtyAcc"
                                            ValidChars="1234567890,.-">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="XX-Small"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="pqtnt_note" HeaderText="In/Ex Tax" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="XX-Small" ForeColor="#333333" BackColor="#FFCC66"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:GridView id="gvDataFooter" runat="server" Width="100%" ShowHeader="False" GridLines="None" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="datacaption">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Height="5px" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datasept">
<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="2%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="datavalue">
<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="25%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dataempty">
<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnApprove" runat="server" ImageUrl="~/Images/approve.png" ImageAlign="AbsMiddle" AlternateText="Approve"></asp:ImageButton> <asp:ImageButton id="btnRevise" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" AlternateText="Revise"></asp:ImageButton> <asp:ImageButton id="btnReject" runat="server" ImageUrl="~/Images/reject.png" ImageAlign="AbsMiddle" AlternateText="Reject"></asp:ImageButton> <asp:ImageButton id="btnBack" runat="server" ImageUrl="~/Images/Back.png" ImageAlign="AbsMiddle" AlternateText="Back"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View></asp:MultiView> <DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="upProgWA" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgWA" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></DIV>
</contenttemplate>
                                    <Triggers>
<asp:PostBackTrigger ControlID="btnViewOutTrans"></asp:PostBackTrigger>
</Triggers>
                                </asp:UpdatePanel></div>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
</asp:Content>

