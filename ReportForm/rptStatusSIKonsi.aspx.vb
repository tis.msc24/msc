Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.Web

Partial Class rptStatusSIKonsi
    Inherits System.Web.UI.Page

#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim vReport As New ReportDocument
    Dim fCabang As String = ""
#End Region

#Region "Fucntion"
    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("datacust") Is Nothing Then
            Dim dtTbl As DataTable = Session("datacust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To GVCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("datacust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("datacustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("datacust")
            Dim dtTbl2 As DataTable = Session("datacustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To GVCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("datacust") = dtTbl
                Session("datacustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedNota() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To GVNota.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVNota.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedNota2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To GVNota.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVNota.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualmstoid=" & cbOid
                                dtView2.RowFilter = "trnjualmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSR() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataret") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataret")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvRet.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvRet.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualreturmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("dataret") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSR2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataret")
            Dim dtTbl2 As DataTable = Session("dataretView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvRet.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvRet.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnjualreturmstoid=" & cbOid
                                dtView2.RowFilter = "trnjualreturmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("dataret") = dtTbl
                Session("dataretView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedItem() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataitem") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitem")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("dataitem") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedItem2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("dataitemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitem")
            Dim dtTbl2 As DataTable = Session("dataitemView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("dataitem") = dtTbl
                Session("dataitemView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedure"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode, gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(dCabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dCabangNya, sSql)
            Else
                FillDDL(dCabangNya, sSql)
                dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
                dCabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(dCabangNya, sSql)
            dCabangNya.Items.Add(New ListItem("ALL", "ALL"))
            dCabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindDataCustomer()
        Try
            sSql = "Select 'False' checkvalue, c.custoid, gendesc, c.custcode, c.custname, c.custaddr From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join ql_mstitem i ON i.itemoid=jd.itemoid Inner Join QL_MSTPERSON p ON p.PERSONOID=jm.spgoid Inner join QL_mstcust c ON c.custoid=jm.trncustoid AND c.branch_code=jm.branch_code Inner Join QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' INNER JOIN QL_trnordermst so ON so.ordermstoid=jm.ordermstoid AND so.branch_code=jm.branch_code Left Join (Select rm.branch_code, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code WHere rm.trnjualstatus='APPROVED' Group By rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where so.typeSO='Konsinyasi' AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='IN COMPLETE' AND jm.trnjualdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= " And jm.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            sSql &= "Group By gendesc, c.custname, c.custoid, c.custaddr, c.custcode Order By c.custoid"
            Dim dtcust As DataTable = ckon.ambiltabel(sSql, "datacust")
            Session("datacust") = dtcust
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindTblSO()
        Try
            sSql = "Select 'False' checkvalue, gendesc, jm.trnjualmstoid, jm.trnjualno, Convert(Char(20), jm.trnjualdate, 103) trnjualdate, c.custname From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join ql_mstitem i ON i.itemoid=jd.itemoid Inner Join QL_MSTPERSON p ON p.PERSONOID=jm.spgoid Inner join QL_mstcust c ON c.custoid=jm.trncustoid AND c.branch_code=jm.branch_code Inner Join QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' INNER JOIN QL_trnordermst so ON so.ordermstoid=jm.ordermstoid AND so.branch_code=jm.branch_code Left Join (Select rm.branch_code, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code	WHere rm.trnjualstatus='APPROVED' Group By rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where so.typeSO='Konsinyasi' AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='IN COMPLETE' AND jm.trnjualdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= " And jm.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            If TxtCustoid.Text <> "0" Then
                sSql &= " AND c.custoid IN (" & TxtCustoid.Text & ")"
            End If

            sSql &= "GROUP BY gendesc, jm.trnjualmstoid, jm.trnjualno, JM.trnjualdate, c.custname Order By jm.trnjualdate"
            Dim dtsi As DataTable = ckon.ambiltabel(sSql, "TblSO")
            Session("TblSO") = dtsi
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try

    End Sub

    Private Sub BindDataRetur()
        sSql = "Select 'False' checkvalue, gendesc, rm.trnjualreturmstoid, rm.trnjualreturno, Convert(Char(20), rm.trnjualdate, 103) trnjualdate, c.custname From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join QL_MSTPERSON p ON p.PERSONOID=jm.spgoid Inner join QL_mstcust c ON c.custoid=jm.trncustoid AND c.branch_code=jm.branch_code Inner Join (Select rm.branch_code, gendesc, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur, typeSO From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code Inner Join QL_mstgen g ON g.gencode=rm.branch_code AND g.gengroup='Cabang'	WHere rm.trnjualstatus='APPROVED' Group By gendesc, rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid, typeSO) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where rm.typeSO='Konsinyasi' AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='IN COMPLETE' AND jm.trnjualdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " And jm.branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        If TxtCustoid.Text <> "0" Then
            sSql &= " AND c.custoid IN (" & TxtJualMstoid.Text & ")"
        End If

        If TxtJualMstoid.Text <> "0" Then
            sSql &= " AND jm.trnjualmstoid IN (" & TxtJualMstoid.Text & ")"
        End If

        sSql &= " Group BY gendesc, rm.trnjualreturmstoid, rm.trnjualreturno, Convert(Char(20), rm.trnjualdate, 103), c.custname"

        Dim dtret As DataTable = ckon.ambiltabel(sSql, "dataret")
        Session("dataret") = dtret
    End Sub

    Public Sub BindDataItem()
        sSql = "Select 'False' checkvalue, i.itemoid, i.itemcode, i.itemdesc, 'BUAH' Unit From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join ql_mstitem i ON i.itemoid=jd.itemoid Left Join (Select rm.branch_code, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur, typeSO From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code WHere rm.trnjualstatus='APPROVED' Group By rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid, typeSO) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where rm.typeSO='Konsinyasi' AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='IN COMPLETE' AND jm.trnjualdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

        If dCabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND jm.branch_code='" & dCabangNya.SelectedValue & "'"
        End If

        If TxtCustoid.Text <> "0" Then
            sSql &= " AND c.custoid IN (" & TxtJualMstoid.Text & ")"
        End If

        If TxtJualMstoid.Text <> "0" Then
            sSql &= " AND jm.trnjualmstoid IN (" & TxtJualMstoid.Text & ")"
        End If

        If TxtRetMstoid.Text <> "0" Then
            sSql &= " AND rm.trnjualreturmstoid IN (" & TxtRetMstoid.Text & ")"
        End If

        sSql &= " Group By i.itemoid, i.itemcode, i.itemdesc"
        Dim dtitem As DataTable = ckon.ambiltabel(sSql, "dataitem")
        Session("dataitem") = dtitem
    End Sub

    Public Sub showPrint(ByVal tipe As String, ByVal sType As String)
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("Maaf, Format tanggal salah..!!", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("Maaf, periode 2 lebih besar dari periode 1..!!", 2)
            Exit Sub
        End If

        Try
            sSql = "Select jm.branch_code, gendesc, jm.trnjualmstoid, jm.trnjualno, jm.trnjualdate, Isnull(rm.trnjualreturno, 'BELUM DI RETUR') trnjualreturno, Isnull(rm.trnjualdate,1/1/1900) ReturDate, c.custname, p.PERSONNAME sales, i.itemoid, itemcode, itemdesc, i.statusitem, jd.trnjualdtlqty, ISNULL(QtyRetur, 0.0) QtyRetur, jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) QtyOS, jd.trnjualdtlprice, jd.amtjualdisc1, jd.amtjualdisc2, jd.amtjualnetto, jm.trnjualnote, (Case jd.trnjualdtlqty - ISNULL((Select SUM(dr.trnjualdtlqty) From QL_trnjualreturdtl dr Where dr.trnjualreturmstoid=rm.trnjualreturmstoid AND dr.branch_code=rm.branch_code AND dr.itemoid=jd.itemoid Group By dr.itemoid), 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End) StatusDtl From ql_trnjualmst jm Inner Join QL_trnjualdtl jd ON jd.trnjualmstoid=jm.trnjualmstoid AND jm.branch_code=jd.branch_code Inner Join ql_mstitem i ON i.itemoid=jd.itemoid Inner Join QL_MSTPERSON p ON p.PERSONOID=jm.spgoid Inner join QL_mstcust c ON c.custoid=jm.trncustoid AND c.branch_code=jm.branch_code Inner Join QL_mstgen g ON g.gencode=jm.branch_code AND g.gengroup='CABANG' INNER JOIN QL_trnordermst so ON so.ordermstoid=jm.ordermstoid AND so.branch_code=jm.branch_code Left Join (Select rm.branch_code, rm.trnjualreturmstoid, rm.trnjualmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, SUM(rd.trnjualdtlqty) QtyRetur From QL_trnjualreturmst rm Inner Join QL_trnjualreturdtl rd ON rm.trnjualreturmstoid=rd.trnjualreturmstoid AND rm.branch_code=rd.branch_code WHere rm.trnjualstatus='APPROVED' Group By rm.branch_code, rm.trnjualreturmstoid, rd.trnjualdtloid, rm.trnjualreturno, rm.trnjualdate, rd.itemoid, rm.trnjualmstoid) rm ON rm.branch_code=jm.branch_code AND rm.trnjualmstoid=jm.trnjualmstoid AND rm.trnjualdtloid=jd.trnjualdtloid AND rm.itemoid=jd.itemoid Where so.typeSO='Konsinyasi' AND jm.trnjualdate BETWEEN '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & " 23:59:59'"

            If dCabangNya.SelectedValue <> "ALL" Then
                sSql &= "And jm.branch_code='" & dCabangNya.SelectedValue & "'"
            End If

            If TxtCustoid.Text <> "0" Then
                sSql &= " AND c.custoid IN (" & TxtJualMstoid.Text & ")"
            End If

            If TxtJualMstoid.Text <> "0" Then
                sSql &= " AND jm.trnjualmstoid IN (" & TxtJualMstoid.Text & ")"
            End If

            If statusDelivery.SelectedValue <> "ALL" Then
                sSql &= " AND (Case jd.trnjualdtlqty - ISNULL(QtyRetur, 0.0) When 0 Then 'COMPLETE' Else 'IN COMPLETE' End)='" & statusDelivery.SelectedValue.ToUpper & "'"
            End If

            If TxtItemOid.Text <> "0" Then
                sSql &= " AND i.itemoid IN (" & TxtItemOid.Text & ")"
            End If

            sSql &= " Order By jm.trnjualdate Desc"
            Dim dt As DataTable = ckon.ambiltabel(sSql, "DataNya")
            Dim dvTbl As DataView = dt.DefaultView
            vReport.Load(Server.MapPath("~\Report\rptStatusSIKonsi" & sType & ".rpt"))
            vReport.SetDataSource(dvTbl.ToTable)
            vReport.SetParameterValue("sPeriode", "Periode : " & dateAwal.Text & " s/d " & dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            cProc.SetDBLogonReport(vReport)

            If tipe = "" Then
                CrvSiKonsiNyasi.DisplayGroupTree = False
                CrvSiKonsiNyasi.ReportSource = vReport
            ElseIf tipe = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Status_SI_Konsinyasi" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "EXL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Status_SI_Konsinyasi" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptStatusSIKonsi.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Servis Supplier Status"
        Session("UserLevel") = 1 ' GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not IsPostBack Then
            fDDLBranch()
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("", "PDF")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptStatusSIKonsi.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showPrint("EXL", "EXL")
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        If UpdateCheckedNota2() Then
            GVNota.PageIndex = e.NewPageIndex
            GVNota.DataSource = Session("TblSOView")
            GVNota.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("PDF", "PDF")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("emptylistcust") Is Nothing And Session("emptylistcust") <> "" Then
            If lblPopUpMsg.Text = Session("emptylistcust") Then
                Session("emptylistcust") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, False)
            End If
        End If

        If Not Session("warninglistcust") Is Nothing And Session("warninglistcust") <> "" Then
            If lblPopUpMsg.Text = Session("warninglistcust") Then
                Session("warninglistcust") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
            End If
        End If

        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
            End If
        End If

        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
            End If
        End If

        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("emptylistSR") Is Nothing And Session("emptylistSR") <> "" Then
            If lblPopUpMsg.Text = Session("emptylistSR") Then
                Session("emptylistSR") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, False)
            End If
        End If
        If Not Session("warninglistSR") Is Nothing And Session("warninglistSR") <> "" Then
            If lblPopUpMsg.Text = Session("warninglistSR") Then
                Session("warninglistSR") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
            End If
        End If

        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("emptylistItem") Is Nothing And Session("emptylistItem") <> "" Then
            If lblPopUpMsg.Text = Session("emptylistItem") Then
                Session("emptylistItem") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
            End If
        End If
        If Not Session("warninglistItem") Is Nothing And Session("warninglistItem") <> "" Then
            If lblPopUpMsg.Text = Session("warninglistItem") Then
                Session("warninglistItem") = Nothing
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
            End If
        End If
    End Sub

    Private Sub CrvSiKonsiNyasi_Navigate(ByVal source As Object, ByVal e As NavigateEventArgs) Handles CrvSiKonsiNyasi.Navigate
        showPrint("", "PDF")
    End Sub

    Private Sub ImbBtnSI_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImbBtnSI.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            GVNota.DataSource = Nothing : GVNota.DataBind()
            cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindTblSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
        If UpdateCheckedNota() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
                dv.RowFilter = ""
                MpeListNota.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
                Session("WarningListSO") = "data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            MpeListNota.Show()
        End If
    End Sub

    Private Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindTblSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SI data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedNota() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            GVNota.DataSource = Session("TblSOView")
            GVNota.DataBind()
        End If
        MpeListNota.Show()
        'cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
    End Sub

    Private Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
            End If
            MpeListNota.Show()
        Else
            Session("WarningListSO") = "Please show some SR data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Private Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedNota() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
                dtView.RowFilter = ""
                cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
        End If

    End Sub

    Private Sub lkbCloseListNota_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbCloseListNota.Click
        cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, False)
    End Sub

    Private Sub lkbAddListNota_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbAddListNota.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedNota() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If TxtJualMstoid.Text <> "0" Then
                            TxtJualMstoid.Text &= "," & Integer.Parse(dtView(C1).Item("trnjualmstoid"))
                            TextNoSI.Text &= ";" & vbCrLf & dtView(C1)("trnjualno")
                        Else
                            TxtJualMstoid.Text &= Integer.Parse(dtView(C1).Item("trnjualmstoid"))
                            TextNoSI.Text = dtView(C1)("trnjualno")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, False)
                Else
                    Session("WarningListSO") = "Please select data to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        'Session("TblSO") = Nothing : Session("TblSOView") = Nothing
    End Sub

    Private Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                GVNota.DataSource = Session("TblSOView")
                GVNota.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListNota, PanelListNota, MpeListNota, True)
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Private Sub BtnCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCust.Click
        If IsValidPeriod() Then
            DDLCUstFilter.SelectedIndex = -1 : TxtFilterCust.Text = ""
            Session("datacust") = Nothing : Session("datacustView") = Nothing
            GVCust.DataSource = Nothing : GVCust.DataBind()
            cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
        Else
            Exit Sub
        End If

    End Sub

    Private Sub BtnFindCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnFindCust.Click
        If Session("datacust") Is Nothing Then
            BindDataCustomer()
            If Session("datacust").Rows.Count <= 0 Then
                Session("emptylistcust") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistcust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLCustFilter.SelectedValue & " LIKE '%" & TcharNoTrim(TxtFilterCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("datacust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("datacustView") = dv.ToTable
                GVCust.DataSource = Session("datacustView")
                GVCust.DataBind()
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
            Else
                dv.RowFilter = ""
                Session("datacustView") = Nothing
                GVCust.DataSource = Session("datacustView")
                GVCust.DataBind()
                Session("warninglistcust") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("warninglistcust"), 2)
            End If
        Else
            cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
        End If
    End Sub

    Private Sub BtnFindALLCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnFindALLCust.Click
        DDLCustFilter.SelectedIndex = -1 : TxtFilterCust.Text = ""
        If Session("datacust") Is Nothing Then
            BindDataCustomer()
            If Session("datacust").Rows.Count <= 0 Then
                Session("emptylistcust") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistcust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("datacust")
            Session("datacustView") = dt
            GVCust.DataSource = Session("datacustView")
            GVCust.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
    End Sub

    Private Sub BtnCheckAllCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckAllCust.Click
        If Not Session("datacustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("datacustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("datacust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("datacust") = objTbl
                Session("datacustView") = dtTbl
                GVCust.DataSource = Session("datacustView")
                GVCust.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
        Else
            Session("warninglistcust") = "Maaf, Silahkan pilih data..!!"
            showMessage(Session("warninglistcust"), 2)
        End If
    End Sub

    Private Sub BtnCheckNoneCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckNoneCust.Click
        If Not Session("datacustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("datacustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("datacust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("datacust") = objTbl
                Session("datacustView") = dtTbl
                GVCust.DataSource = Session("datacustView")
                GVCust.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
        Else
            Session("warninglistcust") = "Please show data first!"
            showMessage(Session("warninglistcust"), 2)
        End If
    End Sub

    Private Sub LblAddToListCust_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LblAddToListCust.Click
        If Not Session("datacust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("datacust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If TxtCustoid.Text <> "0" Then
                            TxtCustoid.Text &= "," & Integer.Parse(dtView(C1).Item("custoid"))
                            Custcode.Text &= ";" & vbCrLf & dtView(C1)("custcode")
                        Else
                            TxtCustoid.Text &= Integer.Parse(dtView(C1).Item("custoid"))
                            Custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, False)
                Else
                    Session("warninglistcust") = "Please select data to add to list!"
                    showMessage(Session("warninglistcust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("warninglistcust") = "Please show data first!"
            showMessage(Session("warninglistcust"), 2)
            Exit Sub
        End If
        Session("datacust") = Nothing : Session("datacustView") = Nothing
    End Sub

    Private Sub LblCloseCust_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LblCloseCust.Click
        cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, False)
    End Sub

    Private Sub BtnEraseCust_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnEraseCust.Click
        Custcode.Text = "" : TxtCustoid.Text = "0"
    End Sub

    Private Sub GVCust_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GVCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            GVCust.PageIndex = e.NewPageIndex
            GVCust.DataSource = Session("datacust")
            GVCust.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenCust, PanelCust, mpeCUst, True)
    End Sub

    Private Sub BtnSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnSR.Click
        If IsValidPeriod() Then
            DDLFilterSR.SelectedIndex = -1 : TxtFilterSR.Text = ""
            Session("dataret") = Nothing : Session("dataretView") = Nothing
            gvRet.DataSource = Nothing : gvRet.DataBind()
            cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub BtnFindSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnFindSR.Click
        If Session("dataret") Is Nothing Then
            BindDataRetur()
            If Session("dataret").Rows.Count <= 0 Then
                Session("emptylistSR") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistSR"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterSR.SelectedValue & " LIKE '%" & TcharNoTrim(TxtFilterSR.Text) & "%'"
        If UpdateCheckedSR() Then
            Dim dv As DataView = Session("dataret").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("dataretView") = dv.ToTable
                gvRet.DataSource = Session("dataretView")
                gvRet.DataBind()
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
            Else
                dv.RowFilter = ""
                Session("dataretView") = Nothing
                gvRet.DataSource = Session("dataretView")
                gvRet.DataBind()
                Session("warninglistSR") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("warninglistSR"), 2)
            End If
        Else
            cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
        End If
    End Sub

    Private Sub BtnViewallSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnViewallSR.Click
        DDLFilterSR.SelectedIndex = -1 : TxtFilterSR.Text = ""
        If Session("dataret") Is Nothing Then
            BindDataRetur()
            If Session("dataret").Rows.Count <= 0 Then
                Session("emptylistSR") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistSR"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSR() Then
            Dim dt As DataTable = Session("dataret")
            Session("dataretView") = dt
            gvRet.DataSource = Session("dataretView")
            gvRet.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
    End Sub

    Private Sub BtnCheckAllSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckAllSR.Click
        If Not Session("dataretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataretView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataret")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualreturmstoid=" & dtTbl.Rows(C1)("trnjualreturmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataret") = objTbl
                Session("dataretView") = dtTbl
                gvRet.DataSource = Session("dataretView")
                gvRet.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
        Else
            Session("warninglistSR") = "Maaf, Silahkan pilih data..!!"
            showMessage(Session("warninglistSR"), 2)
        End If
    End Sub

    Private Sub BtnCheckNoneSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckNoneSR.Click
        If Not Session("dataretView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataretView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataret")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualreturmstoid=" & dtTbl.Rows(C1)("trnjualreturmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataret") = objTbl
                Session("dataretView") = dtTbl
                gvRet.DataSource = Session("dataretView")
                gvRet.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, True)
        Else
            Session("warninglistSR") = "Please show data first!"
            showMessage(Session("warninglistSR"), 2)
        End If
    End Sub

    Private Sub LkbAddToListSR_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbAddToListSR.Click
        If Not Session("dataret") Is Nothing Then
            If UpdateCheckedSR() Then
                Dim dtTbl As DataTable = Session("dataret")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If TxtRetMstoid.Text <> "0" Then
                            TxtRetMstoid.Text &= "," & Integer.Parse(dtView(C1).Item("trnjualreturmstoid"))
                            TxtNoSR.Text &= ";" & vbCrLf & dtView(C1)("trnjualreturno")
                        Else
                            TxtRetMstoid.Text &= Integer.Parse(dtView(C1).Item("trnjualreturmstoid"))
                            TxtNoSR.Text = dtView(C1)("trnjualreturno")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, False)
                Else
                    Session("warninglistSR") = "Please select data to add to list!"
                    showMessage(Session("warninglistSR"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("warninglistSR") = "Please show data first!"
            showMessage(Session("warninglistSR"), 2)
            Exit Sub
        End If
        Session("dataret") = Nothing : Session("dataretView") = Nothing
        cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, False)
    End Sub

    Private Sub LkbCloseSR_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbCloseSR.Click
        cProc.SetModalPopUpExtender(BtnHiddenListSR, PanelSR, mpeSR, False)
    End Sub

    Private Sub BtnEraseSR_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnEraseSR.Click
        TxtRetMstoid.Text = "0" : TxtNoSR.Text = ""
    End Sub

    Private Sub BtnItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnItem.Click
        If IsValidPeriod() Then
            DDLFilterItem.SelectedIndex = -1 : TxtFilterItem.Text = ""
            Session("dataitem") = Nothing : Session("dataitemView") = Nothing
            gvItem.DataSource = Nothing : gvItem.DataBind()
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub BtnFindItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnFindItem.Click
        If Session("dataitem") Is Nothing Then
            BindDataItem()
            If Session("dataitem").Rows.Count <= 0 Then
                Session("emptylistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistItem"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterItem.SelectedValue & " LIKE '%" & TcharNoTrim(TxtFilterItem.Text) & "%'"
        If UpdateCheckedItem() Then
            Dim dv As DataView = Session("dataitem").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("dataitemView") = dv.ToTable
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
                dv.RowFilter = ""
                cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
            Else
                dv.RowFilter = ""
                Session("dataitemView") = Nothing
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
                Session("warninglistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("warninglistItem"), 2)
            End If
        Else
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        End If
    End Sub

    Private Sub BtnViewAllItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnViewAllItem.Click
        DDLFilterItem.SelectedIndex = -1 : TxtFilterItem.Text = ""
        If Session("dataitem") Is Nothing Then
            BindDataItem()
            If Session("dataitem").Rows.Count <= 0 Then
                Session("emptylistItem") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("emptylistItem"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedItem() Then
            Dim dt As DataTable = Session("dataitem")
            Session("dataitemView") = dt
            gvItem.DataSource = Session("dataitemView")
            gvItem.DataBind()
        End If
        cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
    End Sub

    Private Sub BtnCheckAllItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckAllItem.Click
        If Not Session("dataitemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitemView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataitem")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataitem") = objTbl
                Session("dataitemView") = dtTbl
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Session("warninglistItem") = "Maaf, Silahkan pilih data..!!"
            showMessage(Session("warninglistItem"), 2)
        End If
    End Sub

    Private Sub dataitemView(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnCheckNoneItem.Click
        If Not Session("dataitemView") Is Nothing Then
            Dim dtTbl As DataTable = Session("dataitemView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("dataitem")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("dataitem") = objTbl
                Session("dataitemView") = dtTbl
                gvItem.DataSource = Session("dataitemView")
                gvItem.DataBind()
            End If
            cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, True)
        Else
            Session("warninglistItem") = "Please show data first!"
            showMessage(Session("warninglistItem"), 2)
        End If
    End Sub

    Private Sub LkbAddToListItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbAddToListItem.Click
        If Not Session("dataitem") Is Nothing Then
            If UpdateCheckedItem() Then
                Dim dtTbl As DataTable = Session("dataitem")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If TxtItemOid.Text <> "0" Then
                            TxtItemOid.Text &= "," & Integer.Parse(dtView(C1).Item("itemoid"))
                            TxtKatalog.Text &= ";" & vbCrLf & dtView(C1)("itemcode")
                        Else
                            TxtItemOid.Text &= Integer.Parse(dtView(C1).Item("itemoid"))
                            TxtKatalog.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
                Else
                    Session("warninglistItem") = "Please select data to add to list!"
                    showMessage(Session("warninglistItem"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("warninglistItem") = "Please show data first!"
            showMessage(Session("warninglistItem"), 2)
            Exit Sub
        End If
        Session("dataitem") = Nothing : Session("dataitemView") = Nothing
        cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False) 

    End Sub

    Private Sub LkbCloseItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LkbCloseItem.Click
        cProc.SetModalPopUpExtender(BtnHiddenListItem, PanelItem, mpeItem, False)
    End Sub

    Private Sub BtnEraseItem_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles BtnEraseItem.Click
        TxtItemOid.Text = "0" : TxtKatalog.Text = ""
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        TextNoSI.Text = "" : TxtJualMstoid.Text = "0"
    End Sub
#End Region

End Class
