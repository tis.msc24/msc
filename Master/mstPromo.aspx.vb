Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports Microsoft.Office.Interop

Partial Class Master_mstPromo
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Private statusReport As New ReportDocument
    Private myDiskFileDestinationOptions As DiskFileDestinationOptions
    Private myExportOptions As ExportOptions
    Dim CProc As New ClassProcedure
    Public folderReport As String = "~/report/"
    Dim report As New ReportDocument
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
    Dim sValidasi As String = ""
#End Region

#Region "Procedure" 
    Private Sub CekDataSO(ByVal OidPromo As Integer)
        sSql = "SELECT p.promeventname FROM QL_trnordermst so INNER JOIN ql_mstpromo p ON so.promooid=p.promoid WHERE p. promoid=" & OidPromo
        Dim cekPromo As DataTable = cKoneksi.ambiltabel(sSql, "CekPromo")
        Session("CekPromo") = cekPromo
        If cekPromo.Rows.Count > 0 Then
            btnSave.Visible = False
            BtnDelete.Visible = False
            BtnAdd.Visible = False
        End If
    End Sub

    Private Sub BindDataPersonPopup(ByVal sfilter As String)
        sSql = "SELECT * FROM (SELECT p.personnip nip, p.personname, (SELECT GENDESC FROM QL_MSTGEN WHERE GENOID = P.PERSONSTATUS ) divisi, (SELECT genoid FROM QL_MSTGEN WHERE GENOID = P.PERSONSTATUS ) oidjabatan, personoid FROM QL_mstperson p WHERE p.cmpcode='" & CompnyCode & "' and p.PERSONOID IN (SELECT personnoid FROM QL_mstprof WHERE STATUSPROF='Active')) t WHERE oidjabatan IN (1004,961,4669) " & sfilter & " ORDER BY personname"
        Dim dtab As DataTable = cKoneksi.ambiltabel(sSql, "tbl_DataAmp")
        If dtab.Rows.Count > 0 Then
            GVPerson.DataSource = dtab
            GVPerson.DataBind()
            GVPerson.SelectedIndex = -1
            GVPerson.Visible = True
            GVPerson.SelectedIndex = -1
        Else
            Session("sValidasi") = "DATA AMP"
            showMessage("Maaf, Tidak ada data yang ditemukan,,!!", "INFOMATION..!!")
            Exit Sub
        End If
    End Sub

    Private Sub viewDetailPromo(ByVal oidpro As Integer)
        If Not (Session("tbldtlPromo")) Is Nothing Then
            sSql = "SELECT pd.promoid,pd.promdtloid,i.itemoid,i.itemdesc,qtyitemoid1 qty1,0 qty2,priceitem,subsidi1 subsidi,amountdisc,typebarang,seq,0 itemoid2,'' itemdesc2,totalcashback FROM QL_mstpromodtl pd INNER JOIN ql_mstitem i ON i.itemoid=pd.itemoid Where pd.promoid=" & oidpro & ""
            Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_promodtl")
            gvPromoDtl.Visible = True
            gvPromoDtl.DataSource = dtv
            Session("tbldtlPromo") = dtv
            gvPromoDtl.Visible = True
            gvPromoDtl.DataBind()
        Else
            gvPromoDtl.Visible = False
        End If
    End Sub

    Private Sub HitungAmt()
        Dim TitsNya As Double = 0.0
        Dim bRa As DataTable = Session("tbldtlPromo")
        Try
            For c1 As Integer = 0 To bRa.Rows.Count - 1
                TitsNya += bRa.Rows(c1)("totalamt")
            Next
        Catch ex As Exception : End Try
        totKotor.Text = ToMaskEdit(ToDouble(TitsNya), 4)
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' and gengroup='CABANG' ORDER BY gencode ASC"
        cbCabang.Items.Clear()
        'cbCabang.Items.Add("All")
        'cbCabang.Items(cbCabang.Items.Count - 1).Value = "All"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbCabang.Items.Add(xreader.GetValue(1).ToString)
            cbCabang.Items(cbCabang.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Private Sub FillDetailPrice(ByVal iOid As String)
        sSql = "SELECT genoid," & iOid & " promooid,left(gencode,2) gencode,gendesc,isnull((select itemavailable " & _
               "from QL_mstPromoAvailable Where cmpcode=g.cmpcode and promooid= " & iOid & "),'0') itemavailable " & _
               "FROM QL_mstGen g WHERE gengroup='OUTLET' "
        Dim tbDtl As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstGen")
        If tbDtl.Rows.Count > 0 Then
            Session("OutletDtl") = tbDtl

            Dim objTable2 As DataTable
            objTable2 = Session("PriceDtl")
            objTable2.Rows.Clear() : Dim objRow As DataRow

            For C1 As Integer = 0 To tbDtl.Rows.Count - 1
                objRow = objTable2.NewRow()
                objRow("outletoid") = tbDtl.Rows(C1)("genoid").ToString
                objRow("promooid") = tbDtl.Rows(C1)("promooid").ToString
                objRow("outletcode") = tbDtl.Rows(C1)("gencode").ToString
                objRow("outletdesc") = tbDtl.Rows(C1)("gendesc").ToString
                objRow("itemavailable") = tbDtl.Rows(C1)("itemavailable").ToString
                objTable2.Rows.Add(objRow)
            Next

            Session("PriceDtl") = objTable2
            'gvPriceDtl.DataSource = objTable2
            'gvPriceDtl.DataBind()
        Else
            Session("PriceDtl") = Nothing
            'gvPriceDtl.DataSource = Nothing : gvPriceDtl.DataBind()
            showMessage("Gagal membuka data Price !!", CompnyName & " - ERROR") : Exit Sub
        End If
        UpdateDetail()
    End Sub

    Private Function SetTableDetailPrice(ByVal sName As String) As DataTable
        Dim nuTable As New DataTable(sName)
        nuTable.Columns.Add("outletoid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("promooid", Type.GetType("System.Int32"))
        nuTable.Columns.Add("outletcode", Type.GetType("System.String"))
        nuTable.Columns.Add("outletdesc", Type.GetType("System.String"))
        nuTable.Columns.Add("itemavailable", Type.GetType("System.String"))
        Return nuTable
    End Function

    Protected Sub filledSelectedDay(ByVal promflag As String)
        For i As Integer = 0 To cbDay.Items.Count - 1
            cbDay.Items(i).Selected = False
        Next
        Dim selectedchar As String = ""
        For i As Integer = 1 To promflag.Length
            selectedchar = Mid(promflag, i, 1)
            Select Case Trim(selectedchar)
                Case "1"
                    cbDay.Items(0).Selected = True
                Case "2"
                    cbDay.Items(1).Selected = True
                Case "3"
                    cbDay.Items(2).Selected = True
                Case "4"
                    cbDay.Items(3).Selected = True
                Case "5"
                    cbDay.Items(4).Selected = True
                Case "6"
                    cbDay.Items(5).Selected = True
                Case "7"
                    cbDay.Items(6).Selected = True
            End Select
        Next
    End Sub

    Private Sub UpdateDetail()
        If Not Session("PriceDtl") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable
            objTable = Session("PriceDtl")
            Try
                Session("PriceDtl") = objTable
            Catch ex As Exception
                showMessage(ex.Message, CompnyName & " - ERROR ")
            End Try
        End If
    End Sub

    Private Sub initDDLAll()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemgroup'"
        FillDDL(ddlGroup, sSql)
        FillDDL(ddlSubGroup, "SELECT genoid,gendesc FROM QL_mstgen where gengroup='itemsubgroup'")
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub ClearDtlInput()
        I_u2.Text = "New Detail"
        ddlPromtype.Enabled = False
        ddlPromtype.CssClass = "inpTextDisabled"
        pergroupdisc.Text = "0" : persubgroupdisc.Text = "0"
        itemdesc2.Text = "" : itemoid2.Text = ""
        AmtDisc2.Text = 0 : Subsidi2.Text = 0
        TypeBarang.Text = "" : ddlGroup.SelectedIndex = 0
        If ddlPromtype.SelectedValue = "DISCAMT" Then
            itemoid.Text = 0 : Qty1.Text = ToMaskEdit(1, 3)
            DiscNya.Text = ToMaskEdit(0.0, 3)
            Subsidi.Text = ToMaskEdit(0.0, 3)
            itemdesc1.Text = "" : amountdisc1.Text = ToMaskEdit(0.0, 3)
        End If
    End Sub

    Private Sub ClearMstInput()
        promoid.Text = "" : promeventcode.Text = ""
        promeventname.Text = "" : promstartdate.Text = ""
        promfinishdate.Text = "" : promstarttime.Text = ""
        promfinishtime.Text = "" : promotypeddl.SelectedIndex = 0
        membercardrole.SelectedIndex = 0 : note.Text = ""
    End Sub

    Private Sub BindDataMenu()
        Try
            sSql = "SELECT * FROM (SELECT itemoid, itemdesc,i.itemcode,g.gendesc,merk,pricelist,i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya FROM QL_mstItem i INNER JOIN ql_mstgen g ON g.cmpcode=i.cmpcode AND g.genoid=i.itemgroupoid AND i.stockflag='T' Where i.cmpcode='" & CompnyCode & "') it WHERE " & ddlFkatalog.SelectedValue & " like '%" & Tchar(filterMenuItem.Text) & "%' Order By itemcode Desc"
            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")
            If objDs.Tables("data").Rows.Count > 0 Then
                gvMenu.Visible = True
                gvMenu.DataSource = objDs.Tables("data")
                Session("dataMenu") = objDs.Tables("data")
                gvMenu.DataBind()
            Else
                gvMenu.Visible = True
                gvMenu.DataSource = objDs.Tables("data")
                Session("dataMenu") = objDs.Tables("data")
                gvMenu.DataBind()
            End If
            gvMenu.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br>" & sSql, CompnyName & " - WARNING")
            Exit Sub
        End Try
    End Sub

    Private Sub insertingDtlPromo(ByVal promoid As Integer, ByVal OidTemp As Integer)
        Dim objTable As DataTable
        objTable = Session("tbldtlPromo")
        Dim totalcashback As Double = 0
        For c1 As Int16 = 0 To objTable.Rows.Count - 1
            If objTable.Rows(c1)("totalcashback").ToString = "" Then
                totalcashback = 0
            Else
                totalcashback = objTable.Rows(c1)("totalcashback").ToString
            End If
            sSql = "INSERT INTO [QL_MstPromoDtl]([cmpcode], [promdtloid], [promoid], [itemoid], [amountdisc], [itemoid2], qtyitemoid1, qtyitemoid2,subsidi1,typebarang,seq,totalamt,discnya,totalcashback)" & _
                    "VALUES('" & CompnyCode & "', " & OidTemp + c1 & ", " & promoid & ", " & Tnumber(objTable.Rows(c1).Item("itemoid").ToString) & ", " & Tnumber(objTable.Rows(c1).Item("amountdisc").ToString) & ", " & Tnumber(0) & ", " & objTable.Rows(c1).Item("qty1") & "," & 0 & "," & objTable.Rows(c1).Item("subsidi") & ",'" & objTable.Rows(c1).Item("typebarang") & "'," & objTable.Rows(c1).Item("seq") & "," & objTable.Rows(c1).Item("totalamt") & "," & objTable.Rows(c1).Item("discnya") & "," & totalcashback & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
        Next
        sSql = "update QL_mstoid set lastoid=" & OidTemp + objTable.Rows.Count - 1 & " where tablename = 'QL_MstPromoDtl' and cmpcode = '" & CompnyCode & "' "
        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
    End Sub

    Private Sub BindData(ByVal sSqlPlus As String)
        sSql = "SELECT [promoid], [promeventcode], [promeventname], convert(varchar(10),[promstartdate],101) promstartdate, convert(varchar(10),[promfinishdate],101) promfinishdate, [promstarttime], [promfinishtime],[promtype]FROM QL_MstPromo WHERE cmpcode='" & CompnyCode & "' " & sSqlPlus & ""
        If promotypeddl.SelectedValue <> "ALL" Then
            sSql &= " AND promtype='" & promotypeddl.SelectedValue & "'"
        End If
        sSql &= "ORDER BY promeventcode ASC"
        FillGV(GVMst, sSql, "QL_MstPromo")
    End Sub

    Private Sub FillTextBox(ByVal vSearchKey As String)
        Dim personid As String = ""
        sSql = "SELECT [cmpcode], [promoid], [promeventcode], [promeventname], [promstartdate], [promfinishdate], [promstarttime], [promfinishtime], [promtype], [promflag], [promnote], [crtuser], [upduser], [updtime],[dayFlag],[branchFlag] FROM [QL_MstPromo] WHERE promoid=" & vSearchKey
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xreader = xCmd.ExecuteReader
        BtnDelete.Enabled = True

        While xreader.Read
            promoid.Text = Trim(xreader("promoid"))
            promeventcode.Text = Trim(xreader("promeventcode").ToString)
            promeventname.Text = Trim(xreader("promeventname").ToString)
            promstartdate.Text = Format(CDate(xreader("promstartdate")), "dd/MM/yyyy")
            promfinishdate.Text = Format(CDate(xreader("promfinishdate")), "dd/MM/yyyy")
            promstarttime.Text = Trim(xreader("promstarttime"))
            promfinishtime.Text = Trim(xreader("promfinishtime"))
            ddlPromtype.SelectedValue = Trim(xreader("promtype").ToString)
            note.Text = Trim(xreader("promnote").ToString)
            filledSelectedDay(Trim(xreader("dayFlag")))
            Dim sValue() As String = xreader("branchFlag").ToString.Split(",")
            If sValue.Length > 0 Then
                If cbCabang.Items.Count > 0 Then
                    For C1 As Integer = 0 To sValue.Length - 1
                        Dim li As ListItem = cbCabang.Items.FindByValue(sValue(C1).Trim)
                        Dim i As Integer = cbCabang.Items.IndexOf(li)
                        If i >= 0 Then
                            cbCabang.Items(cbCabang.Items.IndexOf(li)).Selected = True
                        End If
                    Next
                End If
            End If
            status.Text = Trim(xreader("promflag"))
            UpdUser.Text = Trim(xreader("upduser"))
            Updtime.Text = Format(CDate(Trim(xreader("updtime").ToString)), "MM/dd/yyyy HH:mm:ss")
        End While : conn.Close() : xreader.Close()
        ddlPromtype_SelectedIndexChanged(Nothing, Nothing)

        sSql = "SELECT pd.promoid,pd.promdtloid,i.itemoid,i.itemdesc,qtyitemoid1 qty1,0 qty2,priceitem,subsidi1 subsidi,amountdisc,typebarang,seq,0 itemoid2,'' itemdesc2,totalamt,Isnull(discnya,0.0) discnya, totalcashback FROM QL_mstpromodtl pd INNER JOIN ql_mstitem i ON i.itemoid=pd.itemoid Where pd.promoid=" & vSearchKey & " Order by seq"
        Dim dtv As DataTable = cKoneksi.ambiltabel(sSql, "ql_promodtl")
        gvPromoDtl.DataSource = dtv
        Session("tbldtlPromo") = dtv
        gvPromoDtl.DataBind()
        If ddlPromtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            gvPromoDtl.Columns(8).Visible = True
            gvPromoDtl.Columns(9).Visible = False
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISCAMT" Then
            gvPromoDtl.Columns(7).HeaderText = "Cashback"
            gvPromoDtl.Columns(9).Visible = True
        Else 
            gvPromoDtl.Columns(8).Visible = False
            gvPromoDtl.Columns(9).Visible = False
        End If

        gvPromoDtl.Visible = True
        promotypeddl.Enabled = False
        promotypeddl.CssClass = "inpTextDisabled"
        HitungAmt()
        CekDataSO(Session("oid")) 

        'If status.Text = "INACTIVE" Then
        '    btnSave.Visible = False : BtnDelete.Visible = False
        'Else
        '    btnSave.Visible = True : BtnDelete.Visible = True
        'End If

    End Sub
#End Region

#Region "Functions"
    Public Function returnAvailable(ByVal sAvailable As String) As Boolean
        If sAvailable = "1" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function setTableDetailPromo() As DataTable
        If Session("tbldtlPromo") Is Nothing Then
            'Dim tblDetail As DataTable
            'tblDetail = New DataTable("QL_MstPromoDtl")
            Dim tblDetail As New DataTable
            tblDetail.Columns.Add("promdtloid", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("itemdesc", Type.GetType("System.String")) '2
            tblDetail.Columns.Add("amountdisc", Type.GetType("System.Decimal")) '3
            tblDetail.Columns.Add("itemoid2", Type.GetType("System.Int32")) '4
            tblDetail.Columns.Add("itemdesc2", Type.GetType("System.String")) '5
            tblDetail.Columns.Add("qty1", Type.GetType("System.Int32")) '6
            tblDetail.Columns.Add("qty2", Type.GetType("System.Int32")) '7
            tblDetail.Columns.Add("subsidi", Type.GetType("System.Decimal")) '8
            tblDetail.Columns.Add("typebarang", Type.GetType("System.String")) '9
            tblDetail.Columns.Add("seq", Type.GetType("System.String")) '10
            tblDetail.Columns.Add("totalamt", Type.GetType("System.Decimal")) '11
            tblDetail.Columns.Add("discnya", Type.GetType("System.Decimal")) '12
            tblDetail.Columns.Add("totalcashback", Type.GetType("System.Decimal")) '13
            Return tblDetail
        End If
    End Function

    Private Function listPromoDtl(ByVal itemoid As Integer, ByVal itemdesc1 As String, ByVal amountdisc1 As Double, ByVal itemoid2 As Integer, ByVal itemdesc2 As String, ByVal MenuSdhAda As String, ByVal subsidi As Double) As String
        Try
            Dim dv As DataView
            Dim dv2 As DataView

            Dim tbldtlPromo As DataTable = Session("tbldtlPromo")
            dv = tbldtlPromo.DefaultView
            dv.RowFilter = "itemoid=" & CStr(itemoid) & " or itemoid2= " & CStr(itemoid)

            If dv.Count > 0 Then
                '    showMessage("This item has been inputed!!", "INFORMATION", 2)
                '    Exit Sub
                MenuSdhAda += " " & itemdesc1 & ","
            End If
            dv.RowFilter = ""

            dv2 = tbldtlPromo.DefaultView
            dv2.RowFilter = "itemoid=" & CStr(itemoid2) & " or itemoid2= " & CStr(itemoid2)

            If itemoid2 <> 0 Then   'abaikan jika itemke2 tidak dipilih          
                If dv2.Count > 0 Then
                    MenuSdhAda += " " & itemdesc2 & ","
                End If
            End If

            dv2.RowFilter = ""

            If MenuSdhAda = "" Then
                Dim dr As DataRow = tbldtlPromo.NewRow
                dr(0) = 0
                dr(1) = itemoid
                If promotypeddl.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
                    dr(2) = Tchar(itemdesc1) & " (" & Qty1.Text & ")"
                    dr(5) = Tchar(itemdesc2) & " (" & Qty2.Text & ")"
                Else
                    dr(2) = Tchar(itemdesc1)
                    dr(5) = Tchar(itemdesc2)
                End If
                dr(3) = amountdisc1
                dr(4) = itemoid2
                dr(6) = Qty1.Text
                dr(7) = Qty2.Text
                dr(8) = subsidi
                tbldtlPromo.Rows.Add(dr)
            End If
            dv.RowFilter = ""
            dv2.RowFilter = ""
            Session("tbldtlPromo") = tbldtlPromo
        Catch ex As Exception
            showMessage(ex.ToString, "ERROR")
        End Try
        Return (MenuSdhAda)
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 15
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xoutletoid As String = Session("outletoid")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstpromo.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Master Promo"
        Session("oid") = Request.QueryString("oid")

        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are u sure want to delete ??');")

        If Not IsPostBack Then
            Dim sPlus As String = ""
            sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag='" & ddlStatus.SelectedValue & "'"
            BindData(sPlus) : initDDLAll()
            setTableDetailPromo() : InitCBL()
            If Session("PriceDtl") Is Nothing Then
                Dim dtlNew As DataTable = SetTableDetailPrice("QL_mstItemPrice")
                Session("PriceDtl") = dtlNew
            End If

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                promoid.Text = GenerateID("QL_mstpromo", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
                UpdUser.Text = Session("UserID")
                Updtime.Text = GetServerTime()
                BtnDelete.Enabled = False
                ClearDtlInput()
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(Nothing, Nothing)
                ddlPromtype_SelectedIndexChanged(Nothing, Nothing)
                BtnDelete.Visible = False
            Else
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                ddlInsertType.SelectedIndex = 0
                ddlInsertType_SelectedIndexChanged(Nothing, Nothing)
                ddlPromtype_SelectedIndexChanged(Nothing, Nothing)
                'PromoDtl
                Dim dttempPromoDtl As New DataTable
                If promotypeddl.SelectedValue.Trim.ToUpper = "BUY NX GET NY" Then
                    sSql = "SELECT pd.promoid,pd.promdtloid,i.itemoid,i.itemdesc,qtyitemoid1 qty1,0 qty2,priceitem,subsidi1 subsidi,amountdisc,typebarang,seq,0 itemoid2,'' itemdesc2,totalamt,discnya,totalcashback FROM QL_mstpromodtl pd INNER JOIN ql_mstitem i ON i.itemoid=pd.itemoid Where pd.promoid=" & Session("oid") & ""
                Else
                    sSql = "SELECT pd.promoid,pd.promdtloid,i.itemoid,i.itemdesc,qtyitemoid1 qty1,0 qty2,priceitem,subsidi1 subsidi,amountdisc,typebarang,seq,0 itemoid2,'' itemdesc2,totalamt,discnya,totalcashback FROM QL_mstpromodtl pd INNER JOIN ql_mstitem i ON i.itemoid=pd.itemoid Where pd.promoid=" & Session("oid") & ""
                End If

                dttempPromoDtl = cKoneksi.ambiltabel(sSql, "QL_MstPromoDtl")
                Session("tbldtlPromo") = dttempPromoDtl

                ClearDtlInput()
                If promotypeddl.SelectedValue.ToUpper.Trim <> "BUY NX GET NY" Then
                    Qty1.Text = 1 : Qty2.Text = 1
                End If
            End If
        End If
    End Sub

    Protected Sub ddlInsertType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInsertType.SelectedIndexChanged
        If ddlInsertType.SelectedValue = "GROUP" Then
            tblGroup.Visible = True
            tblSubGroup.Visible = False
            tblMenu.Visible = False
        ElseIf ddlInsertType.SelectedValue = "SUBGROUP" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = True
            tblMenu.Visible = False
        ElseIf ddlInsertType.SelectedValue = "Item" Then
            tblGroup.Visible = False
            tblSubGroup.Visible = False
            tblMenu.Visible = True
        End If
    End Sub

    Protected Sub imbFindMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMenu1.Click
        filterMenuItem.Text = "" : TypeBarang.Text = "UTAMA"
        filterMenuItem.Text = itemdesc1.Text
        BindDataMenu() : lblstate.Text = "imbFindMenu1"
        gvMenu.Visible = True : PanelUser.Visible = True
        BtnUser.Visible = True : MpeUser.Show()
    End Sub

    Protected Sub imbClearMenu1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearMenu1.Click
        itemdesc1.Text = ""
        itemoid.Text = ""
    End Sub

    Protected Sub imbFindMenu2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMenu2.Click
        filterMenuItem.Text = "" : filterMenuItem.Text = itemdesc2.Text
        BindDataMenu() : lblstate.Text = "imbFindMenu2"
        TypeBarang.Text = "BONUS" : gvMenu.Visible = True
        PanelUser.Visible = True : BtnUser.Visible = True
        MpeUser.Show()
    End Sub

    Protected Sub imbClearMenu2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearMenu2.Click
        itemdesc2.Text = ""
        itemoid2.Text = ""
    End Sub

    Protected Sub gvMenu_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMenu.PageIndexChanging
        gvMenu.PageIndex = e.NewPageIndex
        binddataMenu()
        gvMenu.Visible = True : PanelUser.Visible = True
        BtnUser.Visible = True : MpeUser.Show()
    End Sub

    Protected Sub gvMenu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMenu.SelectedIndexChanged
        If lblstate.Text = "imbFindMenu1" Then
            itemoid.Text = gvMenu.SelectedDataKey("itemoid").ToString
            itemdesc1.Text = gvMenu.SelectedDataKey.Item("itemdesc").ToString
            amountdisc1.Text = ToMaskEdit(ToDouble(gvMenu.SelectedDataKey.Item("pricelist").ToString), 3)
        ElseIf lblstate.Text = "imbFindMenu2" Then
            itemoid2.Text = gvMenu.SelectedDataKey.Item("itemoid").ToString
            itemdesc2.Text = gvMenu.SelectedDataKey.Item("itemdesc").ToString
            AmtDisc2.Text = ToMaskEdit(ToDouble(gvMenu.SelectedDataKey.Item("pricelist").ToString), 3)
        End If
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub LBCloseMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseMenu.Click
        PanelUser.Visible = False
        BtnUser.Visible = False
        MpeUser.Hide()
    End Sub

    Protected Sub gvPromoDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPromoDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
        End If
    End Sub

    Protected Sub gvPromoDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPromoDtl.RowDeleting
        Dim dtab As DataTable
        If Not Session("tbldtlPromo") Is Nothing Then
            dtab = Session("tbldtlPromo")
        Else
            showMessage("Maaf, Ada error detail list session !", 1)
            Exit Sub
        End If

        Dim df As DataView = dtab.DefaultView
        df.RowFilter = "seq=" & e.RowIndex + 1 & " AND typebarang='UTAMA'"
        If df.Count > 0 Then
            BtnAdd.Visible = True
        End If
        df.RowFilter = ""

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(e.RowIndex + 1) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        gvPromoDtl.DataSource = dtab
        gvPromoDtl.DataBind()
        Session("tbldtlPromo") = dtab
        If dtab.Rows.Count = 0 Then
            ddlPromtype.Enabled = True
            ddlPromtype.CssClass = "inpText"
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Validasi.Text = ""
        Dim s1, s2 As Boolean
        s1 = False : s2 = False
        Dim strcabang As String = ""

        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Value <> "All" And cbCabang.Items(C1).Selected = True Then
                strcabang &= cbCabang.Items(C1).Value & ", "
            End If
        Next

        If strcabang <> "" Then
            strcabang = Left(strcabang, strcabang.Length - 2)
        End If

        If promeventcode.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Code""! <br/>"
        End If
        If promeventname.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Promo Name""! <br/>"
        End If

        If promstartdate.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Start Date""! <br/>"
        Else

            Try
                Dim tgle As Date = CDate(toDate(promstartdate.Text))
                tgle = CDate(toDate(promstartdate.Text))
                s1 = True
            Catch ex As Exception
                Validasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s1 = False
            End Try
        End If

        If promfinishdate.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""End Date""! <br/>"
        Else
            Try
                Dim tgle As Date = CDate(toDate(promfinishdate.Text))
                tgle = CDate(toDate(promfinishdate.Text))
                s2 = True
            Catch ex As Exception
                Validasi.Text &= "Incorrect format date ""dd/MM/yyyy"" !" & "<br>"
                s2 = False
            End Try
        End If

        If promstarttime.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""Start Time""! <br/>"
        Else
            Try
                Dim TEST As DateTime = CDate("01/01/1900 " & promstarttime.Text)
            Catch ex As Exception
                Validasi.Text &= "Incorrect format ""Start Time"" ! <br/>"
            End Try
        End If
        If promfinishtime.Text.Trim = "" Then
            Validasi.Text &= "Please Fill ""End Time""! <br/>"
        Else
            Try
                Dim TEST As DateTime = CDate("01/01/1900 " & promfinishtime.Text)
            Catch ex As Exception
                Validasi.Text &= "Incorrect format ""End Time"" ! <br/>"
            End Try
        End If

        If Validasi.Text.Trim = "" Then
            If CDate("01/01/1900 " & promfinishtime.Text) < CDate("01/01/1900 " & promstarttime.Text) Then
                Validasi.Text &= "End Time must >= Start Time ! <br/>"
            End If
        End If

        If s1 And s2 Then
            If CDate(toDate(promstartdate.Text)) > CDate(toDate(promfinishdate.Text)) Then
                Validasi.Text &= "Finish Date must >= Start Date ! <br/>"
            End If
        End If

        If (Session("tbldtlPromo")) Is Nothing Then
            Validasi.Text &= "- No Item Data !!<br/>"
        Else
            Dim objTableCek As DataTable = (Session("tbldtlPromo"))
            If objTableCek.Rows.Count = 0 Then
                Validasi.Text &= "- No Item Data !!<br/>"
            End If
        End If

        If Validasi.Text <> "" Then
            showMessage(Validasi.Text, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim strday As String = ""
        For i As Integer = 0 To cbDay.Items.Count - 1
            If cbDay.Items(i).Selected Then
                strday = strday & cbDay.Items(i).Value
            End If
        Next

        'For i As Integer = 0 To cbCabang.Items.Count - 1
        '    If cbCabang.Items(i).Selected Then
        '        strcabang = strcabang & cbCabang.Items(i).Value
        '    End If
        'Next

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM QL_MstPromo WHERE promeventcode = '" & Tchar(promeventcode.Text) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND promoid <> " & promoid.Text
        End If
        If cKoneksi.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Code already used !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            UpdUser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)

            Dim OidTempDtl As Integer = GenerateID("QL_MstPromoDtl", CompnyCode)

            If Session("oid") = Nothing Or Session("oid") = "" Then
                promoid.Text = GenerateID("QL_MstPromo", CompnyCode)

                sSql = "INSERT INTO [QL_MstPromo] ([cmpcode], [promoid], [promeventcode], [promeventname], [promstartdate], [promfinishdate], [promstarttime], [promfinishtime], [promtype], [promflag], [promnote], [crtuser], [upduser], [updtime],[dayFlag],[branchFlag]) VALUES ('" & CompnyCode & "'," & promoid.Text & ",'" & Tchar(promeventcode.Text) & "','" & Tchar(promeventname.Text) & "','" & CDate(toDate(promstartdate.Text)) & " " & promstarttime.Text & "','" & CDate(toDate(promfinishdate.Text)) & " " & promfinishtime.Text & "','" & Tchar(promstarttime.Text) & "','" & Tchar(promfinishtime.Text) & "','" & Tchar(ddlPromtype.SelectedValue) & "', 'ACTIVE', '" & Tchar(note.Text) & "', '" & Tchar(UpdUser.Text) & "','" & Tchar(UpdUser.Text) & "',current_timestamp,'" & strday & "','" & strcabang & "')"

            Else
                sSql = "UPDATE [QL_MstPromo] " & _
                        "SET [cmpcode]='" & CompnyCode & "', " & _
                        "[promeventcode]='" & Tchar(promeventcode.Text) & "', " & _
                        "[promeventname]='" & Tchar(promeventname.Text) & "', " & _
                        "[promstartdate]='" & CDate(toDate(promstartdate.Text)) & " " & promstarttime.Text & "', " & _
                        "[promfinishdate]='" & CDate(toDate(promfinishdate.Text)) & " " & promfinishtime.Text & "', " & _
                        "[promstarttime]='" & Tchar(promstarttime.Text) & "', " & _
                        "[promfinishtime]='" & Tchar(promfinishtime.Text) & "', " & _
                        "[promtype]='" & Tchar(ddlPromtype.SelectedValue) & "', " & _
                        "[promnote]='" & Tchar(note.Text) & "', " & _
                        "[upduser]='" & Tchar(UpdUser.Text) & "', " & _
                        "[updtime]=current_timestamp,[dayFlag]='" & strday & "',[branchFlag]='" & strcabang & "' " & _
                        "WHERE promoid=" & Session("oid")
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Not (Session("oid") Is Nothing Or Session("oid") = "") Then
                'Data sudah ada
                sSql = "DELETE FROM QL_MstPromoDtl WHERE cmpcode = '" & CompnyCode & "' AND promoid = " & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                promoid.Text = Session("oid")

            Else
                'Data baru
                sSql = "Update QL_mstoid set lastoid=" & promoid.Text & " where tablename = 'QL_MstPromo' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            '=========================================
            insertingDtlPromo(promoid.Text, OidTempDtl)
            '-----------------------------------------
            objTrans.Commit() : conn.Close()
            ClearDtlInput() : ClearMstInput()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR..!!!!!!!!")
            Exit Sub
        End Try
        Response.Redirect("~\Master\mstPromo.aspx?awal=true")

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        ClearMstInput()
        ClearDtlInput()
        Response.Redirect("~\Master\mstPromo.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        Dim sMsg As String = ""
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sMsg &= "- Maaf, Data belum pernah di input kog..!!</ br>"
        End If

        CekDataSO(Session("oid"))
        'Dim cekPromo As DataTable = Session("CekPromo")
        'If cekPromo.Rows.Count > 0 Then
        '    sMsg &= "- Maaf, Promo " & cekPromo.Rows(0)("promeventname").ToString & " sudah di gunakan..!!</ br>"
        'End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE QL_MstPromo SET promflag='INACTIVE' WHERE promoid=" & Session("oid")
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - WARNING")
        Finally
            Response.Redirect("~\Master\mstPromo.aspx?awal=true")
            BtnDelete.Enabled = False
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sPlus As String = ""
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag='" & ddlStatus.SelectedValue & "'"
        BindData(sPlus)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        ddlStatus.SelectedIndex = 0 : promotypeddl.SelectedIndex = 0
        BindData("")
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddataMenu()
        gvMenu.Visible = True : PanelUser.Visible = True
        BtnUser.Visible = True : MpeUser.Show()
    End Sub

    Protected Sub btnListItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterMenuItem.Text = "" : BindDataMenu()
        gvMenu.Visible = True : PanelUser.Visible = True
        BtnUser.Visible = True : MpeUser.Show()
    End Sub

    Protected Sub itemavailable_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateDetail()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
        If Session("sValidasi") = "DATA AMP" Then
            btnAllPerson_Click(sender, e)
            Session("sValidasi") = Nothing
        End If
    End Sub

    Protected Sub GVMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVMst.PageIndexChanging
        Dim sPlus As String = ""
        GVMst.PageIndex = e.NewPageIndex
        sPlus = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' and promflag='" & ddlStatus.SelectedValue & "'"
        BindData(sPlus)
    End Sub

    Protected Sub GVMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnSales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterPerson.Text = ""
        PanelPerson.Visible = True : ButtonPerson.Visible = True
        MPEPerson.Show() : BindDataPersonPopup("")
    End Sub

    Protected Sub GVPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        MPEPerson.Hide()
        Dim no As String = GetStrData("SELECT substring('" & GVPerson.SelectedDataKey(1).ToString().Trim & "',1,5)")
        sSql = "SELECT ISNULL(MAX(CAST(substring(promeventcode,6,3) AS INTEGER))+ 1,1) AS IDNEW FROM ql_mstpromo WHERE cmpcode='" & CompnyCode & "' AND promeventcode LIKE '" & no & "%'"
        promeventcode.Text = GenNumberString(no, "", cKoneksi.ambilscalar(sSql), 3)
        promeventcode.Text = promeventcode.Text
        promeventcode.CssClass = "inpTextDisabled" : promeventcode.ReadOnly = True
        lblamp.Text = GVPerson.SelectedDataKey(2).ToString().Trim 'personoid
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        MPEPerson.Hide()
        CProc.DisposeGridView(GVPerson)
    End Sub

    Protected Sub BtnAddList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAddList.Click
        Dim sVali As String = ""
        If itemoid2.Text = "" Then
            sVali &= "Maaf, " & itemdesc2.Text & " belum di pilih..!!</ br>"
        End If

        If ToDouble(Subsidi2.Text) < 1 Then
            If promotypeddl.SelectedValue <> "BUNDLING" Then
                sVali &= "Maaf, Pricelist " & Tchar(itemdesc2.Text) & " masih nol,  Silahkan isi price promonya..!!</ br>"
            End If
        End If

        If itemoid2.Text <> "" Then
            If promotypeddl.SelectedValue <> "BUNDLING" Then
                If ToDouble(AmtDisc2.Text) <= 0 Then
                    sVali &= "Maaf, Pricelist " & Tchar(itemdesc2.Text) & " masih nol,  Silahkan kontak PIC untuk input price pada master barang + price..!!</ br>"
                End If
            End If
        End If

        If Session("tbldtlPromo") Is Nothing Then
            Dim dtlTable As DataTable = setTableDetailPromo()
            Session("tbldtlPromo") = dtlTable
        End If

        Dim objTable As DataTable
        objTable = Session("tbldtlPromo")
        Dim dv As DataView = objTable.DefaultView

        If ddlPromtype.SelectedValue = "Buy x Get y" Then
            dv.RowFilter = "typebarang= 'BONUS'"
            If dv.Count > 0 Then
                sVali &= "- Maaf, Promo type " & promotypeddl.SelectedItem.Text & " Jumlah jenis barang tidak boleh lebih dari satu..!!</ br>"
                dv.RowFilter = ""
            End If
        End If

        If I_u2.Text = "New Detail" Then
            If itemoid2.Text <> "" Then
                dv.RowFilter = "itemoid= '" & itemoid2.Text & "'"
            End If
        Else
            If itemoid2.Text <> "" Then
                dv.RowFilter = "itemoid= '" & itemoid2.Text & "' AND seq<>" & seq.Text
            End If
        End If

        If dv.Count > 0 Then
            If itemoid2.Text <> "" Then
                sVali &= "- Maaf, " & itemdesc1.Text & " Sudah ditambahkan, cek pada tabel..!!</ br>"
            End If
            dv.RowFilter = ""
            Exit Sub
        End If

        If sVali <> "" Then
            showMessage(sVali, "- WARNING")
            Exit Sub
        End If

        dv.RowFilter = ""
        Dim objRow As DataRow

        If itemoid2.Text <> "" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
            objRow("promdtloid") = 0
            objRow("itemoid") = itemoid2.Text
            objRow("itemdesc") = itemdesc2.Text
            objRow("amountdisc") = ToDouble(AmtDisc2.Text)
            objRow("itemoid2") = 0
            objRow("itemdesc2") = "-"
            objRow("qty1") = ToDouble(Qty2.Text)
            objRow("qty2") = 0.0
            objRow("subsidi") = ToDouble(Subsidi2.Text)
            objRow("typebarang") = "BONUS"
            objRow("discnya") = ToDouble(0)
            objRow("totalamt") = ToDouble(objRow("subsidi")) * ToDouble(objRow("qty1"))
            objRow("totalcashback") = 0.0
            objTable.Rows.Add(objRow)
        End If
        If promotypeddl.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            gvPromoDtl.Columns(8).Visible = True
        Else
            gvPromoDtl.Columns(8).Visible = False
        End If
        objTable.AcceptChanges()
        ClearDtlInput() : HitungAmt()

        Session("tbldtlPromo") = objTable
        gvPromoDtl.Visible = True
        gvPromoDtl.DataSource = Nothing
        gvPromoDtl.DataSource = objTable
        gvPromoDtl.DataBind()

        seq.Text = objTable.Rows.Count + 1
        gvPromoDtl.SelectedIndex = -1
        I_u2.Text = "New Detail"
        promotypeddl.Enabled = False
    End Sub

    Protected Sub amountdisc1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amountdisc1.TextChanged
        amountdisc1.Text = ToMaskEdit(amountdisc1.Text, 4)
    End Sub

    Protected Sub AmtDisc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AmtDisc2.TextChanged
        AmtDisc2.Text = ToMaskEdit(AmtDisc2.Text, 4)
    End Sub

    Protected Sub DiscNya_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DiscNya.TextChanged
        DiscNya.Text = ToMaskEdit(DiscNya.Text, 4)
    End Sub

    Protected Sub Subsidi2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Subsidi2.TextChanged
        Subsidi2.Text = ToMaskEdit(Subsidi2.Text, 4)
    End Sub

    Protected Sub Subsidi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Subsidi.TextChanged
        Subsidi.Text = ToMaskEdit(Subsidi.Text, 4)
    End Sub

    Protected Sub Qty1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty1.TextChanged
        Qty1.Text = ToMaskEdit(Qty1.Text, 4)
    End Sub

    Protected Sub Qty2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Qty2.TextChanged
        Qty2.Text = ToMaskEdit(Qty2.Text, 4)
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim sMsg As String = ""
        If itemoid.Text <> "" Then
            If ToDouble(Qty1.Text) <= 0 Then
                sMsg &= "Maaf, Quantity " & Tchar(itemdesc1.Text) & " masih nol, " & _
                "Silahkan isi QTY..!!</br>"
            End If
        End If

        If ToDouble(amountdisc1.Text) <= 0 Then
            sMsg &= "Maaf, Pricelist " & Tchar(itemdesc1.Text) & " masih nol, " & _
            "Silahkan kontak PIC untuk input price pada master barang + price..!!</br>"
        End If

        If ddlPromtype.SelectedValue <> "DISCAMT" Then
            If ToDouble(Subsidi.Text) < 1 Then
                sMsg &= "Maaf, Pricelist " & Tchar(itemdesc1.Text) & " masih nol, " & _
                "Silahkan isi price promonya..!!</br>"
            End If
        ElseIf ddlPromtype.SelectedValue = "DISCAMT" And Subsidi.Text = "0.00" Then
            If ToDouble(DiscNya.Text) > ToDouble(amountdisc1.Text) Then
                sMsg &= "Maaf, Amount " & DiscNyaLbl.Text & " melebihi price katalog " & ToMaskEdit(amountdisc1.Text, 3) & " Silahkan isi price promonya..!!</br>"
            ElseIf ToDouble(DiscNya.Text) = ToDouble(0.0) Then
                sMsg &= "Maaf, kolom " & DiscNyaLbl.Text & " belum anda isi..!!</br>"
            End If
        End If

        If ddlPromtype.SelectedValue = "Buy 1 Get 1" Then
            If ToDouble(Qty1.Text) > 1 Then
                sMsg &= "Maaf, type promo (" & promotypeddl.SelectedValue & ") jumlah Qty Utama " & _
                "tidak boleh lebih dari satu..!!</br>"
            End If

            If ToDouble(Qty1.Text) < 1 Then
                sMsg &= "Maaf, jumlah Qty Utama telah di isi kurang dari Nol..!!</br>"
            End If
        End If

        If Session("tbldtlPromo") Is Nothing Then
            Dim dtlTable As DataTable = setTableDetailPromo()
            Session("tbldtlPromo") = dtlTable
        End If

        Dim objTable As DataTable
        objTable = Session("tbldtlPromo")
        Dim dv As DataView = objTable.DefaultView

        If I_u2.Text = "New Detail" Then
            dv.RowFilter = "typebarang= 'UTAMA'"
            If dv.Count > 1 Then
                sMsg &= "Maaf, jumlah katalog utama tidak bisa lebih dari satu..!!</br>"
                dv.RowFilter = ""
            End If
        End If

        If I_u2.Text = "New Detail" Then
            dv.RowFilter = "itemoid= '" & itemoid.Text & "'"
        Else
            dv.RowFilter = "itemoid= '" & itemoid.Text & "' AND seq<>" & seq.Text
        End If

        If dv.Count > 0 Then
            If itemoid.Text <> "" Then
                sMsg &= "Maaf, " & itemdesc1.Text & " Sudah ditambahkan, cek pada tabel..!!!<br/>"
            End If
            dv.RowFilter = ""
        End If

        If sMsg <> "" Then
            showMessage(sMsg, "- WARNING")
            Exit Sub
        End If

        dv.RowFilter = ""
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("seq") = objTable.Rows.Count + 1
        Else
            Dim selrow As DataRow() = objTable.Select("seq=" & seq.Text)
            objRow = selrow(0)
            objRow.BeginEdit()
        End If

        objRow("promdtloid") = 0
        objRow("itemoid") = itemoid.Text
        objRow("itemdesc") = itemdesc1.Text
        objRow("amountdisc") = ToDouble(amountdisc1.Text)
        objRow("itemoid2") = 0
        objRow("itemdesc2") = "-"
        objRow("qty1") = ToDouble(Qty1.Text)
        objRow("qty2") = 0
        objRow("subsidi") = ToDouble(Subsidi.Text)
        objRow("discnya") = ToDouble(DiscNya.Text)
        objRow("typebarang") = "UTAMA"

        If ddlPromtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            Dim NilaiDisc As Double = (ToDouble(objRow("subsidi") * objRow("qty1"))) * ToDouble(objRow("discnya")) / 100
            objRow("totalamt") = ToDouble(objRow("subsidi")) * ToDouble(objRow("qty1")) - NilaiDisc
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISCAMT" Then
            objRow("totalamt") = ToDouble(Subsidi.Text) * ToDouble(Qty1.Text)
            objRow("totalcashback") = ToDouble(DiscNya.Text) * ToDouble(Qty1.Text)
        Else
            objRow("totalamt") = ToDouble(objRow("subsidi")) * ToDouble(objRow("qty1"))
            objRow("totalcashback") = 0.0
        End If

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        objTable.AcceptChanges()
        Session("tbldtlPromo") = objTable

        If ddlPromtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            BtnAdd.Visible = True : gvPromoDtl.Columns(8).Visible = True
            gvPromoDtl.Columns(9).Visible = False
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISCAMT" Then
            BtnAdd.Visible = True
            gvPromoDtl.Columns(7).HeaderText = "Cashback"
            gvPromoDtl.Columns(9).Visible = True
        Else
            BtnAdd.Visible = False
            gvPromoDtl.Columns(8).Visible = False
            gvPromoDtl.Columns(9).Visible = False
        End If

        gvPromoDtl.Visible = True
        gvPromoDtl.DataSource = Nothing
        gvPromoDtl.DataSource = objTable
        gvPromoDtl.DataBind()
        seq.Text = objTable.Rows.Count + 1
        gvPromoDtl.SelectedIndex = -1
        ClearDtlInput() : HitungAmt()
       
    End Sub
#End Region

    Protected Sub btnFindPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindPerson.Click
        BindDataPersonPopup("AND " & DDLFilterPerson.SelectedValue & " LIKE '%" & Tchar(FilterPerson.Text) & "%'")
        PanelPerson.Visible = True : ButtonPerson.Visible = True
        MPEPerson.Show()
    End Sub

    Protected Sub btnAllPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllPerson.Click
        FilterPerson.Text = ""
        BindDataPersonPopup("")
        PanelPerson.Visible = True : ButtonPerson.Visible = True
        MPEPerson.Show()
    End Sub

    Protected Sub GVPerson_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVPerson.PageIndexChanging
        GVPerson.PageIndex = e.NewPageIndex
        BindDataPersonPopup("")
        PanelPerson.Visible = True : ButtonPerson.Visible = True
        MPEPerson.Show()
    End Sub  

    Protected Sub ddlPromtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPromtype.SelectedIndexChanged
        If ddlPromtype.SelectedValue.ToUpper.Trim = "BUY NX GET NY" Then
            itemdesc2.CssClass = "inpText" : itemdesc2.Enabled = True
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False : ddlInsertType.SelectedIndex = 0
            DiscNyaLbl.Visible = False : DiscNya.Visible = False
            LblItem2.Visible = True : Label13.Visible = True
            itemdesc2.Visible = True : imbFindMenu2.Visible = True
            imbClearMenu2.Visible = True : LblQty2.Visible = True
            Qty2.Visible = True : LblPriceBrg2.Visible = True
            AmtDisc2.Visible = True : LblPricePorm2.Visible = True
            Subsidi2.Visible = True : Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            ddlPromtype.Enabled = True : ddlPromtype.CssClass = "inpText"
            BtnAddList.Visible = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "BUY X GET Y" Then
            itemdesc2.CssClass = "inpText" : itemdesc2.Enabled = True
            Qty1.Text = 1 : Qty2.Text = 1
            ddlInsertType.CssClass = "inpTextDisabled" : ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0 : ddlInsertType.Enabled = False
            DiscNyaLbl.Visible = False : DiscNya.Visible = False
            LblItem2.Visible = True : Label13.Visible = True
            itemdesc2.Visible = True : imbFindMenu2.Visible = True
            imbClearMenu2.Visible = True : LblQty2.Visible = True
            Qty2.Visible = True : LblPriceBrg2.Visible = True
            AmtDisc2.Visible = True : LblPricePorm2.Visible = True
            Subsidi2.Visible = True : Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            ddlPromtype.Enabled = True : ddlPromtype.CssClass = "inpText"
            BtnAddList.Visible = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISC % (ITEM ONLY)" Then
            Qty1.Text = 1 : Qty2.Text = 1
            ddlInsertType.CssClass = "inpText" : ddlInsertType.Enabled = True
            DiscNyaLbl.Visible = True : DiscNya.Visible = True
            LblItem2.Visible = False : Label13.Visible = False
            itemdesc2.Visible = False : imbFindMenu2.Visible = False
            imbClearMenu2.Visible = False : LblQty2.Visible = False
            Qty2.Visible = False : LblPriceBrg2.Visible = False
            AmtDisc2.Visible = False : LblPricePorm2.Visible = False
            Subsidi2.Visible = False : Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            BtnAddList.Visible = False : Label8.Visible = False
            DiscNyaLbl.Text = "Discount (%)"
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISC QUANTITY" Then
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "BUY 1 GET 1" Then
            Qty1.Text = 1 : Qty2.Text = 1
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            DiscNyaLbl.Visible = False : DiscNya.Visible = False
            LblItem2.Visible = True : Label13.Visible = True
            itemdesc2.Visible = True : imbFindMenu2.Visible = True
            imbClearMenu2.Visible = True : LblQty2.Visible = True
            Qty2.Visible = True : LblPriceBrg2.Visible = True
            AmtDisc2.Visible = True : LblPricePorm2.Visible = True
            Subsidi2.Visible = True : Qty1.Enabled = True
            Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        ElseIf ddlPromtype.SelectedValue.ToUpper.Trim = "DISCAMT" Then
            Qty1.Text = 1 : Qty2.Text = 1
            ddlInsertType.CssClass = "inpText" : ddlInsertType.Enabled = True
            DiscNyaLbl.Visible = True : DiscNya.Visible = True
            LblItem2.Visible = False : Label13.Visible = False
            itemdesc2.Visible = False : imbFindMenu2.Visible = False
            imbClearMenu2.Visible = False : LblQty2.Visible = False
            Qty2.Visible = False : LblPriceBrg2.Visible = False
            AmtDisc2.Visible = False : LblPricePorm2.Visible = False
            Subsidi2.Visible = False : Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            BtnAddList.Visible = False : Label8.Visible = False
            DiscNyaLbl.Text = "Disc / Cashback(Unit)"
            ddlInsertType_SelectedIndexChanged(sender, e)
        Else
            Qty1.Text = 1 : Qty2.Text = 1
            ddlInsertType.CssClass = "inpTextDisabled"
            ddlInsertType.Enabled = False
            ddlInsertType.SelectedIndex = 0
            DiscNyaLbl.Visible = False : DiscNya.Visible = False
            LblItem2.Visible = True : Label13.Visible = True
            itemdesc2.Visible = True : imbFindMenu2.Visible = True
            imbClearMenu2.Visible = True : LblQty2.Visible = True
            Qty2.Visible = True : LblPriceBrg2.Visible = True
            AmtDisc2.Visible = True : LblPricePorm2.Visible = True
            Subsidi2.Visible = True : Qty1.Enabled = True : Qty1.CssClass = "inpText"
            Label16.Visible = True : Subsidi.Visible = True
            Label8.Visible = True : BtnAddList.Visible = True
            ddlInsertType_SelectedIndexChanged(sender, e)
        End If
    End Sub
End Class