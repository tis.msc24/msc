<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="Mstjabatan.aspx.vb" Inherits="Mstjabatan" Title="PT. MULTI SARANA COMPUTER - Master Jabatan" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Master Jabatan"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">Filter :</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText" Font-Size="8pt">
                                                    <asp:ListItem Value="gc.gendesc">Jabatan</asp:ListItem>
                                                    <asp:ListItem Value="gc.genCode">Kode</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText" Font-Size="8pt"></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton
                                                    ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="5">
                                                <fieldset id="field1">
                                    <div id='divTblData'>
                                        <asp:GridView ID="GVmstgen" runat="server" EmptyDataText="No data in database." AutoGenerateColumns="False" Width="100%" CellPadding="4" AllowPaging="True" EnableModelValidation="True" ForeColor="#333333" GridLines="None" PageSize="8">
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="mstjabatan.aspx?oid={0}"
                                                    DataTextField="genCode" HeaderText="Kode">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gendesc" HeaderText="Jabatan" SortExpression="gendesc">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="upduser" HeaderText="Upd User">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="updtime" HeaderText="Upd time">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EmptyDataTemplate>



                                                <asp:Label ID="lblmsg" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        &nbsp;</div>
                                </fieldset>
                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;</div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: List Of Master Jabatan</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 133px" align=left>Code <asp:Label id="Label2" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left><asp:TextBox id="gencode" runat="server" Width="195px" CssClass="inpTextDisabled" MaxLength="30" Enabled="False" size="20"></asp:TextBox>&nbsp; </TD></TR><TR><TD style="WIDTH: 133px" vAlign=top align=left><asp:Label id="Label5" runat="server" Width="45px" Text="Jabatan"></asp:Label><asp:Label id="Label3" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD vAlign=top align=left><asp:TextBox id="gendesc" runat="server" Width="195px" CssClass="inpText" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 133px" align=left>
    Input SO</TD><TD align=left><asp:DropDownList id="GenOther2" runat="server" Width="205px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="YES">YA</asp:ListItem>
<asp:ListItem Value="NO">TIDAK</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=2>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="#585858"></asp:Label> By <asp:Label id="Upduser" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="#585858"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="genoid" runat="server" Visible="False" Font-Size="8pt"></asp:Label><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" Visible="False"></asp:Label></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">.: Form Of Master Jabatan</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" align="left" colspan="2">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></td>
                            <td align="left">
                                <asp:Label ID="lblPopUpMsg" runat="server" CssClass="Important"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 5px" align="left"></td>
                            <td style="HEIGHT: 5px" align="left"></td>
                        </tr>
                        <tr>
                            <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                <asp:ImageButton ID="imbPopUpOK" OnClick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" DropShadow="True" TargetControlID="bePopUpMsg" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
