Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_reportKartuHutang
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function ValidateParam() As String
        Dim sReturn As String = ""
        Dim sErrTemp As String = ""

        If Not IsValidDate(dateAwal.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid Start Date format: " & sErrTemp & ".<BR>"
        If Not IsValidDate(dateAkhir.Text, "dd/MM/yyyy", sErrTemp) Then sReturn &= "- Invalid End Date format: " & sErrTemp & ".<BR>"
        'If IsValidDate(dateAwal.Text, "dd/MM/yyyy", sErrTemp) And IsValidDate(dateAkhir.Text, "dd/MM/yyyy", sErrTemp) Then
        '    If DateDiff(DateInterval.Day, CDate(dateAwal.Text), CDate(dateAkhir.Text)) < 0 Then
        '        sReturn &= "- End Date must be greater than Start Date.<BR>"
        '    End If
        'End If

        Return sReturn
    End Function

    Private Function GetIndoMonth(ByVal sVal As String) As String
        Select Case sVal
            Case "01"
                Return "JANUARI"
            Case "02"
                Return "FEBRUARI"
            Case "03"
                Return "MARET"
            Case "04"
                Return "APRIL"
            Case "05"
                Return "MEI"
            Case "06"
                Return "JUNI"
            Case "07"
                Return "JULI"
            Case "08"
                Return "AGUSTUS"
            Case "09"
                Return "SEPTEMBER"
            Case "10"
                Return "OKTOBER"
            Case "11"
                Return "NOVEMBER"
            Case "12"
                Return "DESEMBER"
            Case Else
                Return "Unknown"
        End Select
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT DISTINCT 0 selected,suppoid custoid,suppcode custcode,suppname custname,suppaddr custaddr FROM QL_mstsupp s WHERE (suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%') and suppoid IN (SELECT DISTINCT trnsuppoid FROM QL_trnbelimst b WHERE b.trnsuppoid=s.suppoid /*AND b.branch_code='" & Session("branch_id") & "'*/ UNION ALL SELECT DISTINCT suppoid FROM QL_trnbelimst_fa b WHERE b.suppoid=s.suppoid AND b.branch_code='" & Session("branch_id") & "') /*UNION ALL SELECT DISTINCT 0 selected,s.suppoid custoid,suppcode custcode,suppname custname,suppaddr custaddr FROM QL_trnnotahrmst hr INNER JOIN QL_mstsupp s ON s.suppoid = hr.suppoid where (suppcode LIKE '%" & Tchar(FilterTextSupplier.Text) & "%' OR suppname LIKE '%" & Tchar(FilterTextSupplier.Text) & "%')*/ ORDER BY suppname"
        Dim dtSupp As DataTable = cKon.ambiltabel(sSql, "QL_mstcust")
        gvSupplier.DataSource = dtSupp : gvSupplier.DataBind()
        Session("QL_mstcust") = dtSupp
        gvSupplier.Visible = True
        gvSupplier.SelectedIndex = -1
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstcust") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstcust")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvSupplier.Rows.Count - 1
                    cb = gvSupplier.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "custoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstcust") = dtab
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim sSuppOid As String = ""
            Dim tglawal As String = Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy")
            Dim tglakhir As String = Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy")
            Dim periodacctg As String = GetPeriodAcctg(tglawal)
            Dim periodValue As String = Format(CDate(toDate(dateAwal.Text)), "MM/dd/yyyy")
            Dim periodValue2 As String = Format(CDate(toDate(dateAkhir.Text)), "MM/dd/yyyy")
            If rbSupplier.SelectedValue = "SELECT" Then
                If Not (Session("QL_mstcust") Is Nothing) Then
                    Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                    If dtSupp.Rows.Count > 0 Then
                        Dim dvSupp As DataView = dtSupp.DefaultView
                        dvSupp.RowFilter = "selected='1'"
                        If dvSupp.Count < 1 Then
                            showMessage("- Maaf, Silahkan pilih supplier..!!<BR>", 2)
                            Exit Sub
                        End If
                        dvSupp.RowFilter = ""
                    Else
                        showMessage("- Maaf, Silahkan pilih supplier..!!<BR>", 2)
                        Exit Sub
                    End If
                Else
                    showMessage("- Maaf, Silahkan pilih supplier..!!<BR>", 2)
                    Exit Sub
                End If
            End If

            If FilterType.SelectedValue.ToUpper <> "DETAILNYA" Then
                If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                    showMessage("-Period 2 harus lebih dari Period 1!.<BR>", 2)
                    Exit Sub
                End If
            End If

            If rbSupplier.SelectedValue = "SELECT" Then
                Dim dtSupp As DataTable : dtSupp = Session("QL_mstcust")
                Dim dvSupp As DataView = dtSupp.DefaultView
                dvSupp.RowFilter = "selected='1'"
                For R1 As Integer = 0 To dvSupp.Count - 1
                    sSuppOid &= dvSupp(R1)("custoid").ToString & ","
                Next
            End If

            Dim swhere_branch As String = ""
            Dim sWhere As String = "" : Dim swheresupp As String = ""
            sWhere = "WHERE branch_code LIKE '%%'"
            If FilterType.SelectedValue = "DETAIL" Then
                If sSuppOid <> "" Then swhere &= " AND suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            Else
                If sSuppOid <> "" Then swhere &= " AND suppoid IN (" & Left(sSuppOid, sSuppOid.Length - 1) & ")"
            End If
            Dim sFilter As String = ""
            sFilter = "ORDER BY"

            If FilterType.SelectedValue = "DETAIL" Then
                If DDLFilter.SelectedValue = "suppname" Then
                    sFilter &= " " & DDLFilter.SelectedValue & " "
                Else
                    sFilter &= " " & DDLFilter.SelectedValue & " "
                End If
                If DDLFilter.SelectedValue = "Asc" Then
                    sFilter &= " " & DDLFilter2.SelectedValue & " "
                Else
                    sFilter &= " " & DDLFilter2.SelectedValue & " "
                End If
            End If

            sSql = "DECLARE @periodacctg AS VARCHAR (10); DECLARE @currency AS VARCHAR (10); DECLARE @tglawal AS VARCHAR (10); DECLARE @tglakhir AS VARCHAR (10); SET @currency ='IDR'; SET @tglawal='" & tglawal & "' ; SET @tglakhir='" & tglakhir & "'; SET @periodacctg = '" & tglawal & "';" & _
"SELECT branch_code,'G24' cabang, suppoid, suppname, SUM(saidr) SaHutang,SUM (beliidr) HutangNya, SUM(SaBayar) SaPayment,SUM(paymentidr) PaymentNya,SUM(saidr)-SUM(SaBayar) SaNett, SUM (paymentusd) paymentusd, SUM(saidr)-SUM(SaBayar)+SUM(beliidr)-SUM(paymentidr) saldoidr,@currency currency,SUM(SaBayar)+SUM(paymentidr) TotPay,SUM(saidr)+SUM(beliidr) TotHutang, SUM(sahr) saHR, SUM(hramt) hramt, SUM(payrefamt) payrefamt FROM ( " & _
"/*SALDO AWAL PIUTANG*/  SELECT '10' branch_code, supp.suppoid, supp.suppname, SUM (amttransidr) saidr, SUM (0) sausd, 0 beliidr, 0 beliusd, SUM(amtbayaridr) paymentidr, sum(amtbayarusd) paymentusd, 0 saldoidr, 0 saldousd,SUM(0) SaBayar,'SALDO AWAL HUTANG' TypeNya, 0 sahr, 0 hramt, 0 refamt, 0 payrefamt FROM QL_conap con INNER JOIN ql_mstsupp supp ON con.suppoid = supp.suppoid INNER JOIN QL_trnbelimst jm ON jm.trnbelimstoid=con.refoid WHERE reftype IN ('ql_trnbelimst','QL_trnpayap','ql_creditnote','QL_DebitNote') AND (Case When trnapdate = '01/01/1900' then @periodacctg else convert(varchar(10),trnapdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102) and Case When paymentdate = '01/01/1900' then @periodacctg else convert(varchar(10),paymentdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102)) GROUP BY supp.suppoid, supp.suppname  " & _
" /*UNION ALL /*SALDO AWAL BAYAR*/ SELECT '10' branch_code, supp.suppoid, supp.suppname, SUM (0) saidr, 0 sausd, 0 beliidr, 0 beliusd, SUM(0) paymentidr, SUM(0) paymentusd, 0 saldoidr, 0 saldousd,SUM(amtbayaridr) SaBayar,'SALDO AWAL BAYAR' TypeNya FROM QL_conap con INNER JOIN ql_mstsupp supp ON con.suppoid = supp.suppoid INNER JOIN QL_trnbelimst jm ON jm.trnbelimstoid=con.refoid WHERE reftype IN ('ql_trnbelimst','QL_trnpayap','ql_creditnote','QL_DebitNote') AND paymentdate <= dateadd(DAY,-1,@tglawal) GROUP BY supp.suppoid, supp.suppname */ " & _
"/* Transaction*/ UNION ALL SELECT '10' branch_code, supp.suppoid, supp.suppname, 0.0 saidr, 0.0 sausd, SUM (amttransidr) beliidr, SUM (amttransusd) beliusd, 0 paymentidr, 0 paymentusd, 0 saldoidr, 0 saldousd,0 SaBayar,'HUTANG' TypeNyA, 0 sahr, 0 hramt, 0 refamt, 0 payrefamt FROM QL_conap con INNER JOIN ql_mstsupp supp ON con.suppoid = supp.suppoid WHERE reftype IN ('QL_trnbelimst','ql_trnbelimst_fa') AND con.trnaptype IN ('AP','HUTANG','APFA') AND trnapdate BETWEEN @tglawal And @tglakhir GROUP BY con.branch_code, supp.suppoid, supp.suppname " & _
"/*PAYMENT*/ UNION ALL  SELECT '10' AS branch_code, supp.suppoid, supp.suppname, 0.0 saidr, 0.0 sausd, 0 beliidr, 0 beliusd, ABS(SUM(amtbayaridr)) paymentidr, SUM (amtbayarusd) paymentusd, 0 saldoidr, 0 saldousd,0 SaBayar,'BAYAR' TypeNya, 0 sahr, 0 hramt, 0 refamt, 0 payrefamt FROM QL_conap con INNER JOIN ql_mstsupp supp ON con.suppoid = supp.suppoid WHERE reftype IN ('QL_trnpayap') AND paymentdate between @tglawal and @tglakhir and con.trnapstatus='POST' and amtbayar >= 0 GROUP BY con.branch_code, supp.suppoid, supp.suppname " & _
" UNION ALL /*DN/CN*/  SELECT '10' AS branch_code, cust.suppoid, cust.suppname, 0.0 saidr, 0.0 sausd , sum(amttransidr) beliidr,  sum(amttransusd) beliusd,case when amtbayaridr <= 0 then sum(amtbayaridr) else sum(amtbayaridr) end  paymentidr, case when amtbayarusd <= 0 then sum(amtbayarusd) else sum(amtbayaridr) end paymentusd , 0 saldoidr, 0 saldousd,0 SaBayar,'HUTANG' TypeNyA, 0 sahr, 0 hramt, 0 refamt, 0 payrefamt  FROM QL_conap con INNER JOIN ql_mstsupp cust ON con.suppoid = cust.suppoid WHERE reftype in ('ql_creditnote', 'QL_DebitNote') AND con.paymentdate between @tglawal and @tglakhir GROUP BY con.branch_code, cust.suppoid, cust.suppname , amtbayaridr, amtbayarusd " & _
"UNION ALL /*AP KOREKSI*/ SELECT '10' AS branch_code, cust.suppoid, cust.suppname, 0.0 saidr, 0.0 sausd , sum(amttransidr) beliidr,  sum(amttransusd) beliusd,case when amtbayaridr <= 0 then sum(amtbayaridr) else sum(amtbayaridr) end  paymentidr, case when amtbayarusd <= 0 then sum(amtbayarusd) else sum(amtbayaridr) end paymentusd , 0 saldoidr, 0 saldousd, 0 SaBayar,'HUTANG' TypeNyA, 0 sahr, 0 hramt, 0 refamt, 0 payrefamt FROM QL_conap con INNER JOIN ql_mstsupp cust ON con.suppoid = cust.suppoid WHERE reftype in ('ql_trnpayap') and (payrefno like '%BKM%' or payrefno like '%BBM%') AND con.paymentdate between @tglawal and @tglakhir and amtbayar <= 0 GROUP BY con.branch_code, cust.suppoid, cust.suppname , amtbayaridr, amtbayarusd " & _
" UNION ALL /*SALDO AWAL HR*/ " & _
"SELECT '10' branch_code, supp.suppoid, supp.suppname, 0 saidr, SUM (0) sausd, 0 beliidr, 0 beliusd, 0 paymentidr, 0 paymentusd, 0 saldoidr, 0 saldousd,SUM(0) SaBayar,'SALDO AWAL HUTANG' TypeNya, SUM(refamt) sahr, 0 hramt, 0 refamt, SUM(payrefamt) payrefamt FROM QL_conhr con INNER JOIN QL_trnnotahrmst jm ON jm.trnnotahroid=con.refoid INNER JOIN ql_mstsupp supp ON jm.suppoid = supp.suppoid  WHERE reftype IN ('QL_trnnotahrmst') AND (Case When refdate = '01/01/1900' then @periodacctg else convert(varchar(10),refdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102) and Case When payrefdate = '01/01/1900' then @periodacctg else convert(varchar(10),payrefdate,102) end <= convert(varchar(10),dateadd(DAY,-1,@periodacctg),102)) GROUP BY supp.suppoid, supp.suppname  " & _
" UNION ALL /*VCR*/" & _
" select '10' branch_code, s.suppoid, s.suppname, 0.0 saidr, 0.0 sausd, 0 beliidr, 0 beliusd, 0 paymentidr, 0 paymentusd, 0 saldoidr, 0 saldousd,0 SaBayar,'HUTANG' TypeNyA, 0 sahr, SUM(hrd.trnnotahrdtlamt) hramt, 0 refamt, 0 payrefamt FROM QL_trnnotahrmst hrm INNER JOIN QL_trnnotahrdtl hrd ON hrd.trnnotahroid = hrm.trnnotahroid INNER JOIN QL_mstsupp s ON s.suppoid = hrm.suppoid where hrm.trnnotahrdate BETWEEN @tglawal And @tglakhir group by s.suppoid, s.suppname " & _
" UNION ALL /*NOTA HR*/ " & _
" SELECT '10' AS branch_code, supp.suppoid, supp.suppname, 0.0 saidr, 0.0 sausd, 0 beliidr, 0 beliusd, 0 paymentidr, 0 paymentusd, 0 saldoidr, 0 saldousd,0 SaBayar,'BAYAR' TypeNya, 0 sahr, 0 hramt, SUM(refamt) refamt, 0 payrefamt FROM QL_conhr con INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = con.refoid INNER JOIN ql_mstsupp supp ON hrm.suppoid = supp.suppoid WHERE reftype IN ('QL_trnnotahrmst') AND con.conhrtype = 'HR' AND con.refdate between @tglawal and @tglakhir and con.conhrstatus='POST' and refamt >= 0 GROUP BY con.branch_code, supp.suppoid, supp.suppname " & _
" UNION ALL /*PAYMENT HR*/ " & _
" SELECT '10' AS branch_code, supp.suppoid, supp.suppname, 0.0 saidr, 0.0 sausd, 0 beliidr, 0 beliusd, 0 paymentidr, 0 paymentusd, 0 saldoidr, 0 saldousd,0 SaBayar,'BAYAR' TypeNya, 0 sahr, 0 hramt, 0 refamt, SUM(payrefamt) payrefamt FROM QL_conhr con INNER JOIN QL_trnnotahrmst hrm ON hrm.trnnotahroid = con.refoid INNER JOIN ql_mstsupp supp ON hrm.suppoid = supp.suppoid WHERE reftype IN ('QL_trnnotahrmst') AND con.conhrtype = 'PAYHR' AND con.payrefdate between @tglawal and @tglakhir and con.conhrstatus='POST' and payrefamt >= 0 GROUP BY con.branch_code, supp.suppoid, supp.suppname " & _
") AS tbl " & sWhere & " GROUP BY suppoid, suppname, branch_code /*HAVING (SUM (saidr) <> 0 OR SUM (sausd) <> 0 OR SUM (beliidr) <> 0 OR SUM (beliusd) <> 0 OR SUM (paymentidr) <> 0 OR SUM (paymentusd) <> 0 ) AND SUM (saidr + beliidr - paymentidr) <> 0*/ ORDER BY suppname ASC"

            Dim dt As DataTable
            Dim nFile As String = ""

            If FilterType.SelectedValue.ToUpper = "SUMMARY" Then
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptAPXls.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptAP.rpt")
                End If
                report.Load(nFile)
                dt = cKon.ambiltabel(sSql, "tbldata")
                report.SetDataSource(dt)
            ElseIf FilterType.SelectedValue.ToUpper = "DETAIL" Then
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptPayAP_DtlExcl.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptPayAP_Detail.rpt")
                End If
                report.Load(nFile)
            Else
                'sWhere &= " AND Sisanya <> 0 AND Sisanya > 0 "
                If sType = "Print Excel" Then
                    nFile = Server.MapPath(folderReport & "rptPayAPDtlXLS.rpt")
                Else
                    nFile = Server.MapPath(folderReport & "rptPayAPDtl.rpt")
                End If
                report.Load(nFile)
            End If

            If FilterType.SelectedValue.ToUpper = "DETAIL" Then
                report.SetParameterValue("cmpcode", "MSC")
                report.SetParameterValue("tglawal", tglawal)
                report.SetParameterValue("tglakhir", tglakhir)
                report.SetParameterValue("periodacctg", periodacctg)
                report.SetParameterValue("periodvalue", periodValue)
                report.SetParameterValue("periodvalue2", periodValue2)
                report.SetParameterValue("currency", FilterCurrency.SelectedValue)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("TglNya", periodValue)
                report.SetParameterValue("oNdate", periodValue)
            ElseIf FilterType.SelectedValue.ToUpper = "DETAILNYA" Then
                report.SetParameterValue("cmpcode", "MSC")
                report.SetParameterValue("tglawal", tglawal)
                report.SetParameterValue("tglakhir", tglakhir)
                report.SetParameterValue("periodacctg", periodacctg)
                report.SetParameterValue("periodvalue", periodValue)
                report.SetParameterValue("periodvalue2", periodValue2)
                report.SetParameterValue("currency", FilterCurrency.SelectedValue)
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("TglNya", periodValue)
            Else
                Dim strLastModified As String = System.IO.File.GetLastWriteTime(nFile.ToString())
                cProc.SetDBLogonReport(report)
                report.SetParameterValue("PrintLastModified", strLastModified)
                report.SetParameterValue("PrintUserID", Session("UserID"))
                report.SetParameterValue("periodreport", periodValue)
                report.SetParameterValue("periodreport2", periodValue2)
            End If

            If sType = "View" Then
                crvReportForm.DisplayGroupTree = False
                crvReportForm.ReportSource = report
                crvReportForm.SeparatePages = True
            ElseIf sType = "Print PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "APREPORT_" & periodacctg)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                    showMessage(ex.ToString, 1)
                    Exit Sub
                End Try
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Try
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "APREPORT_" & periodacctg)
                Catch ex As Exception
                    report.Close() : report.Dispose()
                    showMessage(ex.ToString, 1)
                    Exit Sub
                End Try
            End If
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\ReportForm\reportKartuHutang.aspx")
        End If

        Page.Title = CompnyName & " - Account Payable Report"
        If Not Page.IsPostBack Then
            ' DDL Division
            sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
            If Session("CompnyCode") <> CompnyCode Then
                sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
            End If
            'FillDDL(FilterDDLDiv, sSql)
            'branch

            FilterCurrency.Items.Clear()
            FilterCurrency.Items.Add(New ListItem("INDONESIAN RUPIAH", "IDR"))
            FilterCurrency.Items.Add(New ListItem("US DOLLAR", "USD"))
            FilterCurrency.Items.Add(New ListItem("TRANSACTION RATE", ""))
            FilterCurrency.Items(2).Enabled = False
            pnlSupplier.CssClass = "popupControl"
            dateAwal.Text = Format(GetServerTime, "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")

        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub FilterType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterType.SelectedIndexChanged
        If FilterType.SelectedValue.ToUpper = "DETAILNYA" Then
            Label5.Visible = False
            dateAwal.Visible = False : imgCal1.Visible = False
        Else
            Label5.Visible = True
            dateAwal.Visible = True : imgCal1.Visible = True
        End If
        FilterCurrency.Items(2).Enabled = (FilterType.SelectedIndex = 1)
    End Sub

    Protected Sub rbSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSupplier.SelectedIndexChanged
        gvSupplier.DataSource = Nothing : gvSupplier.DataBind()
        Session("QL_mstcust") = Nothing : gvSupplier.Visible = False
        If rbSupplier.SelectedValue = "ALL" Then
            FilterTextSupplier.Visible = False
            btnSearchSupplier.Visible = False
            btnClearSupplier.Visible = False
        Else
            FilterTextSupplier.Visible = True
            btnSearchSupplier.Visible = True
            btnClearSupplier.Visible = True
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        BindSupplierData()
    End Sub

    Protected Sub btnClearSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupplier.Click
        FilterTextSupplier.Text = ""
        custoid.Text = ""
        gvSupplier.DataSource = Nothing
        gvSupplier.DataBind()
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        UpdateCheckedGV()

        gvSupplier.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstcust")
        gvSupplier.DataSource = dtSupp
        gvSupplier.DataBind()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("View")
        End If
    End Sub

    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToPdf.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print PDF")
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnExportToExcel.Click
        UpdateCheckedGV()
        Dim sMsg As String = ValidateParam()
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        Else
            ShowReport("Print Excel")
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        Response.Redirect("~\ReportForm\reportKartuHutang.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        ShowReport("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        ShowReport("View")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Dispose() : report.Close()
    End Sub
#End Region
End Class
