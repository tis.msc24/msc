<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstkaryawan.aspx.vb" Inherits="Master_mstkaryawan" title="PT. Panverta Cakrakencana - Master Person" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%--
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit, Version=1.0.11119.20010, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e"
    Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
                <table id="Table2" bgcolor="white" border="0" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <td align="left" class="header" valign="center" style="background-color: gainsboro">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Master Person"></asp:Label>
                        </td>
                    </tr>
                </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
<ContentTemplate>
<asp:UpdatePanel id="UpdatePanelSearch" runat="server"><ContentTemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w42" DefaultButton="btnSearch"><TABLE><TBODY><TR><TD class="Label" align=left>Filter :</TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" Width="96px" __designer:wfdid="w43" CssClass="inpText"><asp:ListItem Value="nip">NIP</asp:ListItem>
<asp:ListItem Value="personname">Name</asp:ListItem>
</asp:DropDownList> </TD><TD align=left><asp:TextBox id="FilterText" runat="server" Width="122px" __designer:wfdid="w44" CssClass="inpText" MaxLength="30"></asp:TextBox> </TD><TD align=left><asp:ImageButton id="btnSearch" runat="server" __designer:wfdid="w45" CausesValidation="False" ImageUrl="~/Images/find.png"></asp:ImageButton> </TD><TD align=left><asp:ImageButton id="btnList" runat="server" __designer:wfdid="w46" CausesValidation="False" ImageUrl="~/Images/viewall.png"></asp:ImageButton> </TD><TD align=left><asp:ImageButton id="btnPrint" runat="server" __designer:wfdid="w47" ImageUrl="~/Images/print.png" Visible="False"></asp:ImageButton> </TD><TD style="WIDTH: 66px" align=left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD style="HEIGHT: 10px" align=left><asp:CheckBox id="chkTitle" runat="server" Text="Person Title" Width="90px" __designer:wfdid="w48"></asp:CheckBox></TD><TD style="HEIGHT: 10px" align=left colSpan=5><asp:DropDownList id="FilterTitle" runat="server" Width="230px" __designer:wfdid="w49" CssClass="inpText">
    </asp:DropDownList> </TD><TD style="WIDTH: 66px; HEIGHT: 10px" align=left colSpan=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD style="HEIGHT: 10px" align=left><asp:CheckBox id="chkDept" runat="server" Text="Department" Width="113px" __designer:wfdid="w50"></asp:CheckBox></TD><TD style="HEIGHT: 10px" align=left colSpan=5><asp:DropDownList id="FilterDept" runat="server" Width="230px" __designer:wfdid="w51" CssClass="inpText">
            </asp:DropDownList> </TD><TD style="WIDTH: 66px; HEIGHT: 10px" align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left colSpan=5></TD><TD style="WIDTH: 66px; HEIGHT: 10px" align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 8px" align=left colSpan=7><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 280px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="field1"><DIV id="divTblData"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 101%; HEIGHT: 95%"><asp:GridView id="GVmstperson" runat="server" ForeColor="Black" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" Width="98%" __designer:wfdid="w52" Height="64px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" BackColor="White" CellPadding="4" AutoGenerateColumns="False" EmptyDataText="No data in database.">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="personoid" DataNavigateUrlFormatString="~\master\mstkaryawan.aspx?oid={0}" DataTextField="personoid" HeaderText="Person ID">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" ForeColor="Navy" Width="8%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="8%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="nip" HeaderText="NIP">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Name">
<HeaderStyle Font-Size="X-Small" ForeColor="Navy" Width="35%"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="35%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="persontitle" HeaderText="Person Title">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="27%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="27%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="division" HeaderText="Department">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="15%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#999999" ForeColor="Black"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Text="No Data Found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=6><asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" __designer:dtid="281474976710688" __designer:wfdid="w53" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BtnPdf" onclick="BtnPdf_Click" runat="server" __designer:wfdid="w54" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD style="WIDTH: 66px; HEIGHT: 10px" align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left colSpan=5></TD><TD style="WIDTH: 66px; HEIGHT: 10px" align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="BtnPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> &nbsp;&nbsp;
</ContentTemplate>
<HeaderTemplate>
<IMG alt="" src="../Images/corner.gif" align=absMiddle /> <SPAN style="FONT-SIZE: 9pt"><STRONG>:: List of&nbsp;Person</STRONG></SPAN> 
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
<ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=4><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w96"><asp:View id="View1" runat="server" __designer:wfdid="w97"><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="Information" __designer:wfdid="w98"></asp:Label> <asp:Label id="Label4" runat="server" ForeColor="#585858" Text="|" Width="4px" __designer:wfdid="w99"></asp:Label> <asp:LinkButton id="lbv12" onclick="lbv12_Click" runat="server" Font-Size="Small" Font-Bold="False" __designer:wfdid="w100" CssClass="submenu">More Information</asp:LinkButton><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left>NIP <asp:Label id="Label7" runat="server" Text="*" __designer:wfdid="w101" CssClass="Important"></asp:Label></TD><TD align=left><asp:TextBox id="nip" runat="server" Width="88px" __designer:wfdid="w102" CssClass="inpText" MaxLength="10"></asp:TextBox></TD><TD class="Label" align=left>Person Title</TD><TD align=left><asp:DropDownList id="persontitleoid" runat="server" Font-Size="X-Small" Width="192px" __designer:wfdid="w103" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Person Name <asp:Label id="Label8" runat="server" Text="*" __designer:wfdid="w104" CssClass="Important"></asp:Label></TD><TD align=left><asp:TextBox id="personname" runat="server" Text="" Width="256px" __designer:wfdid="w105" CssClass="inpText" MaxLength="50" OnTextChanged="personname_TextChanged"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTPname" runat="server" __designer:wfdid="w106" TargetControlID="personname" FilterMode="InvalidChars" InvalidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender>&nbsp;</TD><TD class="Label" align=left>Department</TD><TD align=left><asp:DropDownList id="deptoid" runat="server" Font-Size="X-Small" Width="192px" __designer:wfdid="w107" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>ID Card No.</TD><TD align=left><asp:TextBox id="noktp" runat="server" Text="" Width="256px" __designer:wfdid="w108" CssClass="inpText" MaxLength="30"></asp:TextBox></TD><TD class="Label" align=left>Status</TD><TD align=left><asp:DropDownList id="status" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w109" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Tetap</asp:ListItem>
<asp:ListItem>Kontrak</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Gender</TD><TD align=left><asp:DropDownList id="personsex" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w110" CssClass="inpText"><asp:ListItem>Male</asp:ListItem>
<asp:ListItem>Female</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>Work Contract</TD><TD align=left><asp:TextBox id="amtcontract" runat="server" Font-Size="X-Small" Text="" Width="32px" __designer:wfdid="w111" CssClass="inpTextDisabled" MaxLength="2" Enabled="False"></asp:TextBox>&nbsp;Month(s)<ajaxToolkit:MaskedEditExtender id="meeAmtcontract" runat="server" __designer:wfdid="w112" TargetControlID="amtcontract" InputDirection="RightToLeft" MaskType="Number" Mask="99"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left>Marital Status</TD><TD align=left><asp:DropDownList id="maritalstatus" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w113" CssClass="inpText"><asp:ListItem>Single</asp:ListItem>
<asp:ListItem>Married</asp:ListItem>
<asp:ListItem>Widow</asp:ListItem>
<asp:ListItem>Widower</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>Join Date <asp:Label id="Label1" runat="server" Text="*" __designer:wfdid="w114" CssClass="Important"></asp:Label></TD><TD align=left><asp:TextBox id="tglmasuk" runat="server" Width="75px" __designer:wfdid="w115" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar2" runat="server" __designer:wfdid="w116" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label11" runat="server" Text="(dd/MM/yyyy)" __designer:wfdid="w117" CssClass="Important"></asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w118" TargetControlID="tglmasuk" Enabled="True" PopupButtonID="btnCalendar2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w119" TargetControlID="tglmasuk" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" vAlign=top align=left>Religion</TD><TD vAlign=top align=left><asp:DropDownList id="religion" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w120" CssClass="inpText"><asp:ListItem Value="Islam">Islam</asp:ListItem>
<asp:ListItem Value="Kristen">Kristen</asp:ListItem>
<asp:ListItem Value="Katolik">Katolik</asp:ListItem>
<asp:ListItem Value="Hindu">Hindu</asp:ListItem>
<asp:ListItem Value="Buddha">Buddha</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" vAlign=top align=left>City &amp; Date of Birth <asp:Label id="Label10" runat="server" Text="*" __designer:wfdid="w121" CssClass="Important"></asp:Label></TD><TD align=left><asp:DropDownList id="tempatlahir" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w122" CssClass="inpText"></asp:DropDownList><BR /><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w123" TargetControlID="tgllahir" Enabled="True" PopupButtonID="btnCalendar" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w124" TargetControlID="tgllahir" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left>&nbsp;</TD><TD align=left></TD><TD class="Label" align=left></TD><TD align=left><asp:TextBox id="tgllahir" runat="server" Width="75px" __designer:wfdid="w125" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar" runat="server" __designer:wfdid="w126" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label2" runat="server" Text="(dd/MM/yyyy)" __designer:wfdid="w127" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left>Current Address <asp:Label id="Label6" runat="server" Text="*" __designer:wfdid="w128" CssClass="Important"></asp:Label></TD><TD align=left><asp:TextBox id="alamattinggal" runat="server" Text="" Width="256px" __designer:wfdid="w129" CssClass="inpText" MaxLength="50"></asp:TextBox><BR /></TD><TD class="Label" vAlign=top align=left>Phone 1 <asp:Label id="Label5" runat="server" Text="*" __designer:wfdid="w130" CssClass="Important"></asp:Label><BR /></TD><TD align=left><asp:TextBox id="phone1" runat="server" Font-Size="X-Small" Text="" Width="208px" __designer:wfdid="w131" CssClass="inpText" MaxLength="20"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="FTEphone1" runat="server" __designer:wfdid="w132" TargetControlID="phone1" Enabled="True" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTEphone2" runat="server" __designer:wfdid="w133" TargetControlID="phone2" Enabled="True" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD class="Label" align=left></TD><TD align=left><asp:DropDownList id="citytinggaloid" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w134" CssClass="inpText"></asp:DropDownList></TD><TD class="Label" vAlign=top align=left>Phone 2</TD><TD align=left><asp:TextBox id="phone2" runat="server" Font-Size="X-Small" Text="" Width="208px" __designer:wfdid="w135" CssClass="inpText" MaxLength="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Original Address</TD><TD align=left><asp:TextBox id="alamatasal" runat="server" Text="" Width="256px" __designer:wfdid="w136" CssClass="inpText" MaxLength="50"></asp:TextBox><BR /></TD><TD class="Label" vAlign=top align=left>Emergency Phone<BR /></TD><TD vAlign=top align=left><asp:TextBox id="emergencyphone" runat="server" Font-Size="X-Small" Text="" Width="208px" __designer:wfdid="w137" CssClass="inpText" MaxLength="20"></asp:TextBox> <ajaxToolkit:FilteredTextBoxExtender id="FTEemergencyphone" runat="server" __designer:wfdid="w138" TargetControlID="emergencyphone" Enabled="True" ValidChars="1234567890 ()+"></ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left>&nbsp;</TD><TD align=left><asp:DropDownList id="asalcityoid" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w139" CssClass="inpText"></asp:DropDownList></TD><TD class="Label" align=left>Email</TD><TD align=left><asp:TextBox id="email" runat="server" Font-Size="X-Small" Text="" Width="208px" __designer:wfdid="w140" CssClass="inpText" MaxLength="70"></asp:TextBox>&nbsp;<asp:Label id="Label22" runat="server" Text="Ex : mail@sample.com" __designer:wfdid="w141" CssClass="Important"></asp:Label></TD></TR><TR><TD class="Label" align=left>Last Education</TD><TD align=left><asp:TextBox id="pendidikanterakhir" runat="server" Text="" Width="160px" __designer:wfdid="w142" CssClass="inpText" MaxLength="20"></asp:TextBox></TD><TD class="Label" align=left>Ceritificates / diplomas (1)</TD><TD align=left><asp:TextBox id="ijazah1" runat="server" Text="" Width="256px" __designer:wfdid="w143" CssClass="inpText" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Name of School</TD><TD align=left><asp:TextBox id="asalsekolah" runat="server" Text="" Width="256px" __designer:wfdid="w144" CssClass="inpText" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left>Ceritificates / diplomas (2)</TD><TD align=left><asp:TextBox id="ijazah2" runat="server" Text="" Width="256px" __designer:wfdid="w145" CssClass="inpText" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>City</TD><TD align=left><asp:DropDownList id="sekolahcityoid" runat="server" Font-Size="X-Small" Width="144px" __designer:wfdid="w146" CssClass="inpText"></asp:DropDownList></TD><TD class="Label" align=left>Ceritificates / diplomas (3)</TD><TD align=left><asp:TextBox id="ijazah3" runat="server" Text="" Width="256px" __designer:wfdid="w147" CssClass="inpText" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD align=left></TD><TD class="Label" align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left>Note</TD><TD align=left><asp:TextBox id="catatanlainlain" runat="server" Font-Size="8pt" Text="" Width="256px" __designer:wfdid="w148" CssClass="inpText" MaxLength="50" Height="32px" TextMode="MultiLine"></asp:TextBox><BR /><asp:Label id="Label12" runat="server" Text="* 50 characters" __designer:wfdid="w149" CssClass="Important"></asp:Label></TD><TD class="Label" align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left></TD><TD align=left></TD><TD class="Label" align=left></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:TextBox id="personoid" runat="server" Text="" Width="25px" __designer:wfdid="w150" CssClass="inpText" Visible="False" Enabled="False" ReadOnly="True"></asp:TextBox><asp:Label id="i_u" runat="server" Text="New" __designer:wfdid="w151" CssClass="Important" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtTitle" runat="server" __designer:wfdid="w152" CssClass="inpText" Visible="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD align=left><asp:Label id="lblpersonpicture" runat="server" __designer:wfdid="w153" Visible="False"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE style="VERTICAL-ALIGN: middle"><TBODY><TR><TD class="Label">Images</TD><TD vAlign=middle><asp:FileUpload id="matpictureloc" runat="server" Font-Size="X-Small" Width="250px" __designer:wfdid="w154" CssClass="inpText" Font-Overline="False"></asp:FileUpload>&nbsp;<asp:ImageButton id="btnUpload" runat="server" __designer:wfdid="w155" ImageUrl="~/Images/upload.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;</TD></TR><TR><TD></TD><TD class="Label"><asp:Image id="prspicture" runat="server" Width="77px" __designer:wfdid="w156" BorderStyle="Solid" BorderWidth="1px" BorderColor="#999999" Visible="False" Height="77px"></asp:Image>&nbsp;<asp:LinkButton id="lkbFullView" runat="server" __designer:wfdid="w157" Visible="False">[ Full View ]</asp:LinkButton><asp:Label id="lblMenuPic" runat="server" ForeColor="Red" Font-Size="8pt" Font-Names="Arial" __designer:wfdid="w158" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px"></TD><TD style="HEIGHT: 10px" class="Label"></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w159"><asp:LinkButton id="lbv21" onclick="lbv21_Click" runat="server" Font-Size="Small" Font-Bold="False" __designer:wfdid="w160" CssClass="submenu">Information</asp:LinkButton><asp:Label id="Label9" runat="server" ForeColor="#585858" Text="|" __designer:wfdid="w161"></asp:Label><asp:Label id="Label13" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="More Information" __designer:wfdid="w162"></asp:Label><BR /><TABLE><TBODY><TR><TD><asp:Label id="txtCompName" runat="server" Text="Company Name" Width="98px" __designer:wfdid="w163"></asp:Label></TD><TD style="WIDTH: 100%"><asp:TextBox id="companyname" runat="server" Width="241px" __designer:wfdid="w164" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:Label id="historyseq" runat="server" Text="HistorySeq" __designer:wfdid="w165" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" __designer:wfdid="w166" CssClass="Important">New Detail</asp:Label></TD></TR><TR><TD>Position</TD><TD style="WIDTH: 3px"><asp:TextBox id="divisi" runat="server" Width="240px" __designer:wfdid="w167" CssClass="inpText" MaxLength="50"></asp:TextBox> </TD></TR><TR><TD>Country</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlCountry" runat="server" Width="244px" __designer:wfdid="w168" CssClass="inpText" AutoPostBack="True">
                            </asp:DropDownList>&nbsp; </TD></TR><TR><TD>Province</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlProvince" runat="server" Width="244px" __designer:wfdid="w169" CssClass="inpText" AutoPostBack="True">
                            </asp:DropDownList></TD></TR><TR><TD>City</TD><TD style="WIDTH: 3px"><asp:DropDownList id="ddlDtlCity" runat="server" Width="244px" __designer:wfdid="w170" CssClass="inpText">
                            </asp:DropDownList></TD></TR><TR><TD>Time Period</TD><TD><asp:TextBox id="startperiod" runat="server" __designer:wfdid="w171" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnCalendar3" runat="server" __designer:wfdid="w172" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to <asp:TextBox id="endperiod" runat="server" __designer:wfdid="w173" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCalendar4" runat="server" __designer:wfdid="w174" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label14" runat="server" Text="(dd/MM/yyyy)" __designer:wfdid="w175" CssClass="Important"></asp:Label><BR /><ajaxToolkit:MaskedEditExtender id="meestart" runat="server" __designer:wfdid="w176" TargetControlID="startperiod" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID">
                            </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w177" TargetControlID="startperiod" PopupButtonID="btnCalendar3" Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceEnd" runat="server" __designer:wfdid="w178" TargetControlID="endperiod" PopupButtonID="btnCalendar4" Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeEnd" runat="server" __designer:wfdid="w179" TargetControlID="endperiod" MaskType="Date" Mask="99/99/9999" UserDateFormat="DayMonthYear" CultureName="id-ID">
                            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD></TD><TD><asp:ImageButton id="btnAddToList" onclick="btnAddToList_Click" runat="server" __designer:wfdid="w180" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click" runat="server" __designer:wfdid="w181" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="tbldtl" runat="server" Font-Size="X-Small" Width="98%" __designer:wfdid="w182" BorderStyle="Solid" BorderWidth="1px" BorderColor="#DEDFDE" BackColor="White" CellPadding="4" AutoGenerateColumns="False" Visible="False" DataKeyNames="historyseq,personoid,compname,position,City,Province,Country,startperiod,endperiod" OnSelectedIndexChanged="tbldtl_SelectedIndexChanged">
<RowStyle BackColor="#F7F7DE"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" SelectText="Show" ShowSelectButton="True">
<HeaderStyle Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="historyseq" HeaderText="No.">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="compname" HeaderText="Company Name">
<HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="position" HeaderText="Position">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="City" HeaderText="City">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Province" HeaderText="Province">
<HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Country" HeaderText="Country">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="startperiod" HeaderText="Join Date">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="endperiod" HeaderText="Retire Date"></asp:BoundField>
<asp:CommandField ShowDeleteButton="True"></asp:CommandField>
</Columns>

<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#E7E7FF" ForeColor="#4A3C8C"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="No Person History !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#F7F7F7"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" BorderStyle="Solid" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView><BR /></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" align=left><asp:Panel id="PanelUpdate" runat="server" __designer:wfdid="w183">Last Update on <asp:Label id="Updtime" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text="[Updtime]" __designer:wfdid="w184"></asp:Label>&nbsp;by <asp:Label id="Upduser" runat="server" ForeColor="#585858" Font-Size="X-Small" Font-Bold="True" Text="[upduser]" __designer:wfdid="w185"></asp:Label></asp:Panel></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" tabIndex=39 runat="server" __designer:wfdid="w186" ImageUrl="~/Images/Save.png" CausesValidation="False" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BtnCancel" tabIndex=40 runat="server" __designer:wfdid="w187" ImageUrl="~/Images/Cancel.png" CausesValidation="False" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="BtnDelete" tabIndex=41 runat="server" __designer:wfdid="w188" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" CommandName="Delete"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
    <br />
    <br />
</ContentTemplate>
<HeaderTemplate>
<IMG alt="" src="../Images/corner.gif" align=absMiddle /> <SPAN style="FONT-SIZE: 9pt"><STRONG>:: Form Person</STRONG></SPAN>&nbsp;
</HeaderTemplate>
</ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" __designer:wfdid="w219" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" Text="header" __designer:wfdid="w220"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" __designer:wfdid="w221" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red" __designer:wfdid="w222"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnErrOK" runat="server" __designer:wfdid="w223" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" __designer:wfdid="w224" TargetControlID="ButtonExtendervalidasi" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" __designer:wfdid="w225" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;&nbsp;&nbsp;<CR:CrystalReportViewer AutoDataBind="true" ID="CrystalReportViewer1" runat="server" /><br />
    &nbsp;<br />
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" __designer:wfdid="w289" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE height="100%" cellSpacing=1 cellPadding=1 width="100%" border=0><TBODY><TR><TD style="COLOR: white; HEIGHT: 18px; BACKGROUND-COLOR: blue; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption2" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w290"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left>&nbsp;</TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" CssClass="Important" __designer:wfdid="w291"></asp:Label> </TD></TR><TR><TD vAlign=top align=left colSpan=2><asp:Image id="Image4" runat="server" Width="450px" __designer:wfdid="w292" Height="400px" Visible="False"></asp:Image></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w293"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" __designer:wfdid="w294" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="1px" CausesValidation="False" __designer:wfdid="w295" Visible="False">
            </asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

