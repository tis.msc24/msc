'Prgmr:Bhie Nduuutt | LastUpdt:01.04.2013
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports Koneksi
Imports ClassFunction
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Partial Class Accounting_trnGiroIn
    Inherits System.Web.UI.Page

#Region "Variable"
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SP_ConnectionString")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim sprefik As String
#End Region

#Region "Function"

#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        Try
            sSql = "select distinct g.trngirooid,g.Trans_No ,convert(char,g.tgl,103) tgl,convert(char,gd.GiroDueDate,103) duedate, gd.girono , c.custname , g.status,amount,gd.girostatus AS girostatus from ql_trngiroin g inner join ql_trngiroindtl gd on gd.trngirooid = g.trngirooid inner join QL_mstcust c on c.custoid = g.custoid where g.cmpcode = '" & cmpcode & "' "

            Dim sOther As String = ""
            If cbPeriod.Checked Then
                sWhere &= " AND g.tgl BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & "' AND '" & CDate(toDate(FilterPeriod2.Text)) & "' "
            End If

            If DDLStatus.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= " AND g.status LIKE '%" & DDLStatus.SelectedValue & "%'"
            End If
            If ddlBranch.SelectedValue <> "ALL" Then
                sWhere &= " AND g.branch_code = '" & ddlBranch.SelectedValue & "'"
            End If
            If ddlPayStatus.SelectedValue <> "ALL" Then
                sWhere &= " and gd.girostatus = '" & ddlPayStatus.SelectedValue & "'"
            End If

            sOther &= " " & sWhere & " ORDER BY g.Trans_No DESC "
            sSql &= sOther
            'FillGV(gvListOf, sSql, "ql_trngiroin")
            Dim objTable As DataTable = ckon.ambiltabel(sSql, "ql_trncashbankmst")
            Session("tbldata") = objTable
            gvListOf.DataSource = objTable
            gvListOf.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try

    End Sub

    Public Sub filltextbox(ByVal id As Integer)
        dtltb.Visible = False
        sSql = "select isnull(g.branch_code,'') branch_code,g.trngirooid, g.Trans_No ,convert(char,g.tgl,103) tgl,c.custname,g.custoid , g.status from ql_trngiroin g inner join QL_mstcust c on c.custoid = g.custoid where g.trngirooid =" & id

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                oid.Text = Trim(xreader.Item("trngirooid").ToString.Trim)
                trnNo.Text = Trim(xreader.Item("Trans_No").ToString.Trim)
                custoid.Text = Trim(xreader.Item("custoid").ToString.Trim)
                custname.Text = Trim(xreader.Item("custname").ToString.Trim)
                transDate.Text = Trim(xreader.Item("tgl").ToString.Trim)
                status.Text = Trim(xreader.Item("status").ToString.Trim)
                ddlCabang.SelectedValue = Trim(xreader.Item("branch_code").ToString)
            End While
        End If
        xreader.Close()
        conn.Close()

        sSql = "select seq,g.girono,convert(char,g.giroduedate,103) DueDate,g1.gendesc bank,g.bankoid  ,g.noRekening RekNo, g.note,amount, g.girores1, girostatus, girooidnew, girononew,convert(char,g.giroduedatenew,103) AS giroduedatenew, bankoidnew, g2.gendesc AS banknew, norekeningnew from ql_trngiroindtl g inner join ql_mstgen g1 on g1.genoid = g.bankoid INNER JOIN QL_mstgen g2 ON g2.genoid = g.bankoidnew where g.trngirooid=" & id & " and g.branch_code = '" & ddlCabang.SelectedValue & "'"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        tbldtl.DataSource = objDs.Tables("data")
        tbldtl.DataBind()
        Session("dtlTbl") = objDs.Tables("data")


        If status.Text = "POST" Then
            imbSave.Visible = False : imbDelete.Visible = False : ibPosting.Visible = False
            imbAddToList.Visible = False : btnPrint.Visible = False

        ElseIf status.Text = "In Approval" Then
            imbSave.Visible = True : imbDelete.Visible = False : ibPosting.Visible = False
            imbAddToList.Visible = True
        ElseIf status.Text = "In Process" Then
            imbSave.Visible = True
            imbDelete.Visible = True : btnPrint.Visible = False
        ElseIf status.Text = "Rejected" Then
            imbSave.Visible = True : imbDelete.Visible = False
        End If
        btnPrint.Visible = False
        seq.Text = objDs.Tables("data").Rows.Count + 1
        I_u2.Text = "New Detail"
    End Sub

    Public Sub generateno()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & ddlCabang.SelectedValue & "' AND gengroup='CABANG'")
        Dim sprefik As String = "GRIS/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(Trans_No, '" & sprefik & "', ''))),0)+1 FROM ql_trngiroin WHERE (cmpcode = '" & cmpcode & "') AND (Trans_No LIKE '" & sprefik & "%')"
        'trnNo.Text = sprefik & GenNumberString("", "", GetStrData(sSql), 4)
        trnNo.Text = GenNumberString(sprefik, "", ckon.ambilscalar(sSql), 4)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub binddataCust(ByVal sqlPlus As String)
        sSql = "SELECT distinct c.custoid,c.custcode,c.custname from QL_mstcust c inner join ql_giro gd on gd.custoid = c.custoid inner join ql_girodtl g on g.girooid = gd.girooid where c.cmpcode='" & cmpcode & "' and c.branch_code = '" & ddlCabang.SelectedValue & "' AND g.girodtloid NOT IN (SELECT girodtloid FROM QL_GiroPaymentDtl) " & sqlPlus & " ORDER BY custname ASC"
        FillGV(gvCust, sSql, "QL_mstcust")
    End Sub

    Private Sub InitAllDDL()
        Dim sSql As String = "Select gencode,gendesc from ql_mstgen Where gengroup = 'cabang' order by gencode asc"
        FillDDL(ddlCabang, sSql)
        If Session("branch_id") <> "10" Then
            ddlCabang.SelectedValue = Session("branch_id")
            ddlCabang.Enabled = False
            ddlCabang.CssClass = "inpTextDisabled"
        End If

        Dim Sql As String = "Select gencode,gendesc from ql_mstgen Where gengroup = 'cabang' order by gencode asc"
        FillDDL(ddlBranch, Sql)
        If Session("branch_id") <> "10" Then
            ddlBranch.SelectedValue = Session("branch_id")
            ddlBranch.Enabled = False
            ddlBranch.CssClass = "inpTextDisabled"
        Else
            ddlBranch.Items.Add(New ListItem("ALL", "ALL"))
            ddlBranch.SelectedValue = "ALL"
        End If

        sSql = "select genoid, gendesc from ql_mstgen where gengroup='BANK NAME'"
        FillDDL(bank, sSql)

        sSql = "select genoid, gendesc from ql_mstgen where gengroup='BANK NAME'"
        FillDDL(ddlbanknew, sSql)
        generateno()
    End Sub

    Sub CreateTableTBLDTL()
        Dim dtlDS As DataSet = New DataSet
        Dim dtlTable As DataTable = New DataTable("trnGiro")
        dtlTable.Columns.Add("Seq", Type.GetType("System.String"))
        dtlTable.Columns.Add("GiroNo", Type.GetType("System.String"))
        dtlTable.Columns.Add("Bank", Type.GetType("System.String"))
        dtlTable.Columns.Add("bankoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("amount", Type.GetType("System.Decimal"))
        dtlTable.Columns.Add("RekNo", Type.GetType("System.String"))
        dtlTable.Columns.Add("DueDate", Type.GetType("System.String"))
        dtlTable.Columns.Add("Note", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnGiro", Type.GetType("System.String"))
        dtlTable.Columns.Add("girores1", Type.GetType("System.String"))
        dtlTable.Columns.Add("girostatus", Type.GetType("System.String"))
        dtlTable.Columns.Add("girononew", Type.GetType("System.String"))
        dtlTable.Columns.Add("giroduedatenew", Type.GetType("System.String"))
        dtlTable.Columns.Add("bankoidnew", Type.GetType("System.String"))
        dtlTable.Columns.Add("banknew", Type.GetType("System.String"))
        dtlTable.Columns.Add("norekeningnew", Type.GetType("System.String"))
        dtlTable.Columns.Add("girooidnew", Type.GetType("System.String"))
        dtlDS.Tables.Add(dtlTable)
        Session("dtlTbl") = dtlTable
    End Sub

    Private Sub BindListBGK(ByVal sqlPlus As String)
        Dim sWhere As String = ""
        Dim dtl As String = ""
        If ddlStatusGiro.SelectedValue = "Cancel" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Not Used'"
        ElseIf ddlStatusGiro.SelectedValue = "Bank" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Not Used'"
        ElseIf ddlStatusGiro.SelectedValue = "Change" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Bank' AND gim.status = 'POST'"
            dtl &= " INNER JOIN QL_trngiroindtl gid ON gid.girores1 = g.girodtloid INNER JOIN QL_trngiroin gim ON gim.trngirooid = gid.trngirooid "
        Else
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Bank' AND gim.status = 'POST'"
            dtl &= " LEFT JOIN QL_trngiroindtl gid ON gid.girores1 = g.girodtloid INNER JOIN QL_trngiroin gim ON gim.trngirooid = gid.trngirooid "
        End If
        sSql = "select 'False' AS checkvalue, g.girodtloid girooid, g.girono, convert(char,g.giroduedate,103) DueDate,g1.gendesc bank,g.bankoid, g.noRekening RekNo, g.note,g.amount from ql_girodtl g INNER JOIN QL_giro gd ON gd.girooid = g.girooid inner join ql_mstgen g1 on g1.genoid = g.bankoid INNER JOIN QL_mstcust c ON c.custoid = gd.custoid " & dtl & " WHERE g.cmpcode='" & cmpcode & "' and g.branch_code = '" & ddlCabang.SelectedValue & "' AND g.girodtloid NOT IN (SELECT girodtloid FROM QL_GiroPaymentDtl) AND  c.custoid = " & custoid.Text & " " & sWhere & " " & sqlPlus & " order by girooid desc"
        Dim dtBGK As DataTable = ckon.ambiltabel(sSql, "QL_trncashbankmst")
        If dtBGK.Rows.Count > 0 Then
            Session("TblListBGK") = dtBGK
            gvListBGK.DataSource = Session("TblListBGK")
            gvListBGK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, True)
        Else
            showMessage("Tidak ada data giro yang tersedia saat ini !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            btnHideListBGK.Visible = False
            pnlListBGK.Visible = False
            mpeListBGK.Hide()
        End If
    End Sub


    Private Sub BindGiroNew(ByVal sqlPlus As String)
        Dim sWhere As String = ""
        Dim dtl As String = ""
        If ddlStatusGiro.SelectedValue = "Cancel" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Not Used'"
        ElseIf ddlStatusGiro.SelectedValue = "Bank" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Not Used'"
        ElseIf ddlStatusGiro.SelectedValue = "Change" Then
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Bank' AND g.girodtloid <> " & girooid.Text & " "
        Else
            sWhere &= " AND gd.STATUS = 'POST' AND g.status = 'Bank' AND gim.status = 'POST'"
            dtl &= " LEFT JOIN QL_trngiroindtl gid ON gid.girores1 = g.girodtloid INNER JOIN QL_trngiroin gim ON gim.trngirooid = gid.trngirooid "
        End If
        sSql = "select 'False' AS checkvalue,g.girodtloid girooid, g.girono,convert(char,g.giroduedate,103) DueDate,g1.gendesc bank,g.bankoid, g.noRekening RekNo, g.note,g.amount from ql_girodtl g INNER JOIN QL_giro gd ON gd.girooid = g.girooid inner join ql_mstgen g1 on g1.genoid = g.bankoid INNER JOIN QL_mstcust c ON c.custoid = gd.custoid " & dtl & " WHERE g.cmpcode='" & CompnyName & "' and g.branch_code = '" & ddlCabang.SelectedValue & "' AND g.girodtloid NOT IN (SELECT girodtloid FROM QL_GiroPaymentDtl) AND  c.custoid = " & custoid.Text & " " & sWhere & " " & sqlPlus & " order by girooid desc"
        Dim dtBGK As DataTable = ckon.ambiltabel(sSql, "QL_trncashbankmst")
        If dtBGK.Rows.Count > 0 Then
            Session("TblListBGK") = dtBGK
            gvListBGK.DataSource = Session("TblListBGK")
            gvListBGK.DataBind()
            cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, True)
        Else
            showMessage("Tidak ada data giro yang tersedia saat ini !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            btnHideListBGK.Visible = False
            pnlListBGK.Visible = False
            mpeListBGK.Hide()
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Accounting\trnGiroIn.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        UpdUser.Text = Session("UserID")
        UpdTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
        Session("oid") = Request.QueryString("oid")
        imbDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        ibPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data ?');")
        Page.Title = CompnyName & " - Giro Flag Status"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "UPDATE"
        Else
            i_u.Text = "N E W"
        End If

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            BindData("") : InitAllDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update"
                filltextbox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                If status.Text = "POST" Then
                    btnPrint.Visible = False
                    ibPosting.Visible = False : imbDelete.Visible = False
                Else
                    ibPosting.Visible = True : imbDelete.Visible = True
                End If
            Else
                i_u.Text = "New"
                UpdUser.Text = Session("UserID")
                UpdTime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                seq.Text = 1 : Session("ItemLine") = seq.Text
                transDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                DueDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                ibPosting.Visible = True
                TabContainer1.ActiveTabIndex = 0
            End If
        End If

    End Sub

    Protected Sub imbAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        dtltb.Visible = False
        If GiroNo.Text = "" Then
            showMessage("Tolong isi no giro !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If girooid.Text > 0 Then
            If RekNo.Text = "" Then
                showMessage("Tolong isi no rekening !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        If Tchar(note.Text).Length > 50 Then
            showMessage("Karakter harus <= 50 !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If IsDate(toDate(DueDate.Text)) = False Then
            showMessage("- Tanggal Jatuh tempo salah!!<BR>", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If CheckYearIsValid(toDate(DueDate.Text)) Then
            showMessage("Tahun tanggal jatuh tempo tidak dapat kurang dari 2000 dan lebih dari 2072 <br />", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If IsDate(toDate(DueDate.Text)) = False Or IsDate(toDate(DueDate.Text)) = False Then
            showMessage("Periode awal salah !!", CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        'If CDate(toDate(DueDate.Text)) < CDate(toDate(transDate.Text)) Then
        '    showMessage("Tanggal jatuh tempo tidak dapat kurang tanggal transfer  " & transDate.Text & " <br />", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        '    Exit Sub
        'End If

        If ToDouble(amount.Text) = 0 Then
            showMessage("Jumlah harus >0 !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If ddlStatusGiro.SelectedValue = "Change" Then
            If girononew.Text = "" Then
                showMessage("No Giro Baru tidak boleh kosong !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If girooid.Text = girooidnew.Text Then
                showMessage("No Giro Lama dan No Giro Baru tidak boleh sama !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If

        If Session("dtlTbl") Is Nothing Then
            CreateTableTBLDTL()
        End If

        Dim objTable As New DataTable
        objTable = Session("dtlTbl")

        If I_u2.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView

            dv.RowFilter = "GiroNo='" & Tchar(GiroNo.Text) & "'"
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("No giro ini sudah ada sebelumnya, tolong di cek!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                objTable = Session("dtlTbl") : tbldtl.DataSource = objTable
                tbldtl.DataBind() : tbldtl.Visible = True
                Exit Sub
            End If
            dv.RowFilter = ""
        End If

        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("Seq") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(seq.Text - 1)
            objRow.BeginEdit()
        End If
        objRow("amount") = ToDouble(amount.Text)
        objRow("GiroNo") = GiroNo.Text
        objRow("Bank") = bank.SelectedItem.Text.ToString
        objRow("bankoid") = bank.SelectedValue
        objRow("RekNo") = RekNo.Text
        objRow("DueDate") = DueDate.Text
        objRow("Note") = note.Text
        objRow("girores1") = girooid.Text
        objRow("girostatus") = ddlStatusGiro.SelectedValue
        If ddlStatusGiro.SelectedValue = "Change" Then
            objRow("girononew") = girononew.Text
            objRow("giroduedatenew") = duedatenew.Text
            objRow("norekeningnew") = norekeningnew.Text
            objRow("girooidnew") = girooidnew.Text
            objRow("banknew") = ddlbanknew.SelectedItem.Text.ToString
            objRow("bankoidnew") = ddlbanknew.SelectedValue
        Else
            objRow("girononew") = ""
            objRow("giroduedatenew") = "01/01/1900"
            objRow("norekeningnew") = ""
            objRow("girooidnew") = 0
            objRow("bankoidnew") = 0
        End If

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
            I_u2.Text = "New Detail"
        End If
        If ddlStatusGiro.SelectedValue = "Change" Then
            ClearDetail() : ClearDetailNew()
        Else
            ClearDetail()
        End If
        Session("dtlTbl") = objTable
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        seq.Text = objTable.Rows.Count + 1
        tbldtl.SelectedIndex = -1
    End Sub

    Public Sub ClearDetailNew()
        girononew.Text = "" : duedatenew.Text = ""
        girooidnew.Text = "" : norekeningnew.Text = ""
    End Sub

    Public Sub ClearDetail()
        GiroNo.Text = "" : note.Text = ""
        RekNo.Text = "" : seq.Text = 1
        I_u2.Text = "New Detail" : amount.Text = ""
        girooid.Text = ""
    End Sub

    Protected Sub imbClearDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub tbldtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        I_u2.Text = "update"
        GiroNo.Text = tbldtl.SelectedDataKey("GiroNo")
        bank.SelectedValue = tbldtl.SelectedDataKey("bankoid")
        RekNo.Text = tbldtl.SelectedDataKey("RekNo")
        DueDate.Text = tbldtl.SelectedDataKey("DueDate")
        note.Text = tbldtl.SelectedDataKey("Note")
        seq.Text = tbldtl.SelectedDataKey("Seq")
        amount.Text = ToMaskEdit(tbldtl.SelectedDataKey("amount"), 4)
        girooid.Text = tbldtl.SelectedDataKey("girores1")
        ddlStatusGiro.SelectedValue = tbldtl.SelectedDataKey("girostatus")
        ddlStatusGiro_SelectedIndexChanged(Nothing, Nothing)
        If ddlStatusGiro.SelectedValue = "Change" Then
            girooidnew.Text = tbldtl.SelectedDataKey("girooidnew")
            girononew.Text = tbldtl.SelectedDataKey("girononew")
            duedatenew.Text = tbldtl.SelectedDataKey("giroduedatenew")
            ddlbanknew.SelectedValue = tbldtl.SelectedDataKey("bankoidnew")
            norekeningnew.Text = tbldtl.SelectedDataKey("norekeningnew")
        End If
    End Sub

    Protected Sub imbSearchListOf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchListOf.Click
        Dim sError As String = ""
        If cbPeriod.Checked Then
            If IsDate(toDate(FilterPeriod1.Text)) And IsDate(toDate(FilterPeriod2.Text)) Then
                If CDate(toDate(FilterPeriod2.Text)) < CDate(toDate(FilterPeriod1.Text)) Then
                    sError &= "Date end must be larger or equals than start date ! <br />"
                End If
            Else
                sError &= "Invalid Filter Date !!<br />"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, CompnyName & " - WARNING !!", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        BindData(" and " & ddlFilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'")
    End Sub

    Protected Sub imbViewAllListOf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAllListOf.Click
        FilterPeriod1.Text = Format(GetServerTime(), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbPeriod.Checked = True : DDLStatus.SelectedValue = "ALL"
        ddlBranch.SelectedValue = "ALL" : ddlPayStatus.SelectedValue = "ALL"
        txtFilter.Text = "" : ddlFilter.SelectedIndex = 0
        cbPeriod.Checked = False
        BindData("")
    End Sub

    Protected Sub gvListOf_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListOf.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
        End If
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Session("oid") = Nothing
        Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim sMsg As String = ""
        If custoid.Text.Trim = "" Then
            sMsg &= "- Tolong pilih customer !!<BR>"
        End If
        If IsDate(toDate(transDate.Text)) = False Then
            sMsg &= "- Tanggal transaksi salah!!<BR>"
        Else
            If CheckYearIsValid(toDate(transDate.Text)) Then
                sMsg &= "Tahun Tanggal tidak dapat kurang dari 2000 dan lebih dari 2072!<br />"
            End If
        End If
        If CDate(toDate(transDate.Text)) <= CDate(toDate(CutofDate.Text)) Then
            sMsg &= "- Tanggal Giro Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> "
        End If
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
        If ToDouble(amount.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(transDate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        'End If
        If note.Text.Length > 150 Then
            sMsg &= "- Maksimal karakter Catatan adalah 150 !!<BR>"
        End If

        'cek apa sudah ada Giro 
        If Session("dtlTbl") Is Nothing Then
            showMessage("Tidak ada Giro No!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            Dim objTableCek As DataTable = Session("dtlTbl")
            If objTableCek.Rows.Count = 0 Then
                showMessage("Tidak ada Giro No!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            Else
                For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                    'cek data synchronisasi dengan payment
                    sSql = "select isnull(girono,'') girono from ql_trngiroindtl where cmpcode='" & cmpcode & "' and girono='" & objTableCek.Rows(c1).Item("GiroNo") & "' and trngirooid <> " & ToDouble(oid.Text) & " and girostatus IN ('Cancel')"
                    If objTableCek.Rows(c1).Item("GiroNo") = GetStrData(sSql) Then
                        sMsg &= "- Giro No (" & objTableCek.Rows(c1).Item("GiroNo") & ") Sudah ada dalam inputan, cek untuk melihat giro dengan status in process !!, Silahkan pilih No Giro lain !! <BR>"
                        Exit For
                    End If
                Next

            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            status.Text = "In Process"
            Exit Sub
        End If

        If status.Text = "POST" Then
            generateno()
        End If
        Dim oid_dtl As Int64 = GenerateID("ql_trngiroindtl", cmpcode)
        If Session("oid") = Nothing Or Session("oid") = "" Then
            oid.Text = GenerateID("ql_trngiroin", cmpcode)
        End If
        ' insert table master
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "insert into ql_trngiroin (cmpcode,trngirooid,Trans_No,tgl,custoid,updtime,upduser,status,branch_code) values " & _
                           "('" & cmpcode & "'," & oid.Text & ",'" & trnNo.Text & "','" & CDate(toDate(transDate.Text)) & "','" & custoid.Text & "','" & UpdTime.Text & "', '" & UpdUser.Text & "', '" & status.Text & "','" & ddlCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & oid.Text & " where tablename ='ql_trngiroin' and cmpcode = '" & cmpcode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("dtlTbl") Is Nothing Then
                    Dim objTable As DataTable
                    objTable = Session("dtlTbl")

                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "insert into ql_trngiroindtl (cmpcode,trngirodtloid,trngirooid,seq,girono,giroduedate,bankoid,NoRekening, Note, amount,branch_code, girores1, girostatus, girooidnew, girononew, giroduedatenew, bankoidnew, norekeningnew) values " & _
                               "('" & cmpcode & "'," & oid_dtl & "," & oid.Text & ",'" & Tchar(objTable.Rows(c1).Item("seq")) & "','" & Tchar(objTable.Rows(c1).Item("girono")) & "', '" & CDate(toDate(objTable.Rows(c1).Item("DueDate"))) & "','" & objTable.Rows(c1).Item("bankoid") & "','" & Tchar(objTable.Rows(c1).Item("RekNo")) & "','" & Tchar(objTable.Rows(c1).Item("Note")) & "', " & ToDouble(objTable.Rows(c1).Item("amount")) & ", '" & ddlCabang.SelectedValue & "', '" & objTable.Rows(c1).Item("girores1") & "', '" & objTable.Rows(c1).Item("girostatus") & "', " & objTable.Rows(c1).Item("girooidnew") & ", '" & objTable.Rows(c1).Item("girononew") & "', '" & IIf(objTable.Rows(c1).Item("girostatus") = "Change", CDate(toDate(objTable.Rows(c1).Item("giroduedatenew"))), "01/01/1900") & "', '" & objTable.Rows(c1).Item("bankoidnew") & "', '" & objTable.Rows(c1).Item("norekeningnew") & "') "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        'sSql = "UPDATE QL_GIRODTL SET status = '" & objTable.Rows(c1).Item("girostatus") & "' where girodtloid = '" & objTable.Rows(c1).Item("girores1") & "'"
                        oid_dtl += 1
                    Next
                    sSql = "update QL_mstoid set lastoid=" & oid_dtl - 1 & " where tablename = 'ql_trngiroindtl' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Else
                'update tabel master    
                sSql = "Update ql_trngiroin set tgl='" & CDate(toDate(transDate.Text)) & "',custoid='" & custoid.Text & "',updtime='" & UpdTime.Text & "',upduser= '" & UpdUser.Text & "',status='" & status.Text & "', branch_code = '" & ddlCabang.SelectedValue & "', Trans_No = '" & trnNo.Text & "' Where trngirooid=" & oid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("dtlTbl") Is Nothing Then
                    ' Delete detail lama
                    sSql = "Delete from ql_trngiroindtl where trngirooid = " & oid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'insert tabel detail
                    Dim objTable As DataTable
                    objTable = Session("dtlTbl")
                    Dim sDate As Date = "01/01/1900"
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "insert into ql_trngiroindtl (cmpcode,trngirodtloid,trngirooid,seq,girono,giroduedate,bankoid,NoRekening,Note,amount,branch_code, girores1, girostatus, girooidnew, girononew, giroduedatenew, bankoidnew, norekeningnew) values " & _
                               "('" & cmpcode & "'," & oid_dtl & "," & oid.Text & ",'" & Tchar(objTable.Rows(c1).Item("seq")) & "','" & Tchar(objTable.Rows(c1).Item("girono")) & "', '" & CDate(toDate(objTable.Rows(c1).Item("DueDate"))) & "','" & objTable.Rows(c1).Item("bankoid") & "','" & Tchar(objTable.Rows(c1).Item("RekNo")) & "','" & Tchar(objTable.Rows(c1).Item("Note")) & "', " & ToDouble(objTable.Rows(c1).Item("amount")) & ",'" & ddlCabang.SelectedValue & "', '" & objTable.Rows(c1).Item("girores1") & "', '" & objTable.Rows(c1).Item("girostatus") & "', " & objTable.Rows(c1).Item("girooidnew") & ", '" & objTable.Rows(c1).Item("girononew") & "', '" & CDate(toDate(objTable.Rows(c1).Item("giroduedatenew"))) & "', '" & objTable.Rows(c1).Item("bankoidnew") & "', '" & objTable.Rows(c1).Item("norekeningnew") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        'sSql = "UPDATE QL_GIRODTL SET status = '" & objTable.Rows(c1).Item("girostatus") & "' where girodtloid = '" & objTable.Rows(c1).Item("girores1") & "'"
                        'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        oid_dtl += 1
                    Next
                    sSql = "update QL_mstoid set lastoid=" & oid_dtl - 1 & " where tablename = 'ql_trngiroindtl' and cmpcode = '" & cmpcode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                End If
            End If
            If status.Text = "POST" Then
                Dim objTable As DataTable
                objTable = Session("dtlTbl")
                For c1 As Int16 = 0 To objTable.Rows.Count - 1
                    If objTable.Rows(c1).Item("girostatus") <> "Change" Then
                        sSql = "UPDATE QL_GIRODTL SET status = '" & objTable.Rows(c1).Item("girostatus") & "' where girodtloid = '" & objTable.Rows(c1).Item("girores1") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_GIRODTL SET status = 'Reject' where girodtloid = '" & objTable.Rows(c1).Item("girores1") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_GIRODTL SET status = '" & objTable.Rows(c1).Item("girostatus") & "' where girodtloid = '" & objTable.Rows(c1).Item("girooidnew") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                Next
            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close() : status.Text = ""
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try


        If status.Text = "POST" Then
            showMessage("Data telah diposting !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub

    Protected Sub tbldtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(12).Text = ToMaskEdit(e.Row.Cells(12).Text, 4)
        End If
    End Sub

    Protected Sub tbldtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("dtlTbl")
        objTable.Rows.RemoveAt(e.RowIndex)
        'resequence Detial 
        For c1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(c1)
            dr.BeginEdit()
            dr("seq") = c1 + 1
            dr.EndEdit()
        Next
        Session("dtlTbl") = objTable
        tbldtl.DataSource = Session("dtlTbl")
        tbldtl.DataBind()
        seq.Text = objTable.Rows.Count + 1
    End Sub

    Protected Sub ibPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibPosting.Click
        status.Text = "POST"
        imbSave_Click(sender, e)
    End Sub

    Protected Sub imbDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbDelete.Click
        If oid.Text = "" Then
            showMessage("Tolong isi no transaksi !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        Dim xTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xCmd.Connection = conn
        xTrans = conn.BeginTransaction()
        xCmd.Transaction = xTrans

        Try
            sSql = "delete from ql_trngiroindtl where trngirooid = " & oid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from ql_trngiroin where trngirooid = " & oid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            xTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            xTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
        Session("oid") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvCust.SelectedIndex = -1
        PanelCust.Visible = True
        ButtonCust.Visible = True
        MPECust.Show()
        binddataCust("")
    End Sub

    Protected Sub imbFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelCust.Visible = True
        ButtonCust.Visible = True
        MPECust.Show()
        binddataCust(" and " & DDLFilterCust.SelectedValue & " like '%" & Tchar(txtFilterCust.Text) & "%'")
    End Sub

    Protected Sub imbAllSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilterCust.Text = ""
        DDLFilterCust.SelectedIndex = 0
        PanelCust.Visible = True
        ButtonCust.Visible = True
        MPECust.Show()
        binddataCust("")

    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custoid.Text = gvCust.SelectedDataKey("custoid")
        custname.Text = gvCust.SelectedDataKey("custname")
        PanelCust.Visible = False
        ButtonCust.Visible = False
        MPECust.Hide()
    End Sub

    Protected Sub amount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsNumeric(amount.Text) Then
            amount.Text = ToMaskEdit(amount.Text, 4)
        Else
            amount.Text = 0
        End If

    End Sub

    Protected Sub btnClearcust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearcust.Click
        custname.Text = ""
        custoid.Text = ""
        ClearDetail()
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCust.PageIndexChanging
        gvCust.PageIndex = e.NewPageIndex
        PanelCust.Visible = True
        ButtonCust.Visible = True
        MPECust.Show()
        binddataCust("")
    End Sub

    Protected Sub Close_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelCust.Visible = False
        ButtonCust.Visible = False
        MPECust.Hide()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvListOf_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListOf.PageIndexChanging
        gvListOf.PageIndex = e.NewPageIndex
        gvListOf.DataSource = Session("tbldata")
        gvListOf.DataBind()
    End Sub

    Protected Sub ddlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'InitCashbank(payflag.SelectedValue)
        generateno()
    End Sub

    Protected Sub btnSearchGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'FilterDDLListBGK.SelectedIndex = -1 : FilterTextListBGK.Text = "" : Session("TblListBGK") = Nothing : Session("TblListBGKView") = Nothing : gvListBGK.DataSource = Nothing : gvListBGK.DataBind()
        If custname.Text = "" Then
            showMessage("Pilih Customer terlebih dahulu sebelum memilih giro", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindListBGK(" and g.girono like '%" & GiroNo.Text & "%'")
    End Sub

    Protected Sub btnClearGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub gvListBGK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlStatusGiro.SelectedValue = "Change" Then
            If GiroNo.Text <> "" Then
                girooidnew.Text = gvListBGK.SelectedDataKey("girooid")
                girononew.Text = gvListBGK.SelectedDataKey("girono")
                ddlbanknew.SelectedValue = gvListBGK.SelectedDataKey("bankoid")
                duedatenew.Text = gvListBGK.SelectedDataKey("duedate")
                norekeningnew.Text = gvListBGK.SelectedDataKey("rekno")
                cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, False)
            Else
                girooid.Text = gvListBGK.SelectedDataKey("girooid")
                GiroNo.Text = gvListBGK.SelectedDataKey("girono")
                bank.SelectedValue = gvListBGK.SelectedDataKey("bankoid")
                DueDate.Text = gvListBGK.SelectedDataKey("duedate")
                amount.Text = ToMaskEdit(gvListBGK.SelectedDataKey("amount"), 4)
                RekNo.Text = gvListBGK.SelectedDataKey("rekno")
                note.Text = gvListBGK.SelectedDataKey("note")
                cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, False)
            End If
        Else
            girooid.Text = gvListBGK.SelectedDataKey("girooid")
            GiroNo.Text = gvListBGK.SelectedDataKey("girono")
            bank.SelectedValue = gvListBGK.SelectedDataKey("bankoid")
            DueDate.Text = gvListBGK.SelectedDataKey("duedate")
            amount.Text = ToMaskEdit(gvListBGK.SelectedDataKey("amount"), 4)
            RekNo.Text = gvListBGK.SelectedDataKey("rekno")
            note.Text = gvListBGK.SelectedDataKey("note")
            cProc.SetModalPopUpExtender(btnHideListBGK, pnlListBGK, mpeListBGK, False)
        End If
    End Sub

    Protected Sub gvListBGK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListBGK.PageIndex = e.NewPageIndex
        mpeListBGK.Show()
        BindListBGK("")
    End Sub

    Protected Sub btnFindListBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindListBGK(" and " & FilterDDLListBGK.SelectedValue & " like '%" & Tchar(FilterTextListBGK.Text) & "%'")
        'mpeListBGK.Show()
    End Sub

    Protected Sub btnAllListBGK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterTextListBGK.Text = ""
        BindListBGK("")
        'mpeListBGK.Show()
    End Sub

    Protected Sub gvListBGK_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub
#End Region

    Protected Sub lbCloseListBGK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListBGK.Click
        btnHideListBGK.Visible = False
        pnlListBGK.Visible = False
        mpeListBGK.Hide()
    End Sub

    Protected Sub ddlStatusGiro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatusGiro.SelectedIndexChanged
        If ddlStatusGiro.SelectedValue = "Change" Then
            TD1.Visible = True : TD2.Visible = True
            TD3.Visible = True : TD4.Visible = True
            TD5.Visible = True : TD6.Visible = True
            TD7.Visible = True : TD8.Visible = True
            TD9.Visible = True : TD10.Visible = True
            TD11.Visible = True : TD12.Visible = True
        Else
            TD1.Visible = False : TD2.Visible = False
            TD3.Visible = False : TD4.Visible = False
            TD5.Visible = False : TD6.Visible = False
            TD7.Visible = False : TD8.Visible = False
            TD9.Visible = False : TD10.Visible = False
            TD11.Visible = False : TD12.Visible = False
        End If
    End Sub

    Protected Sub btnsearchgironew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If GiroNo.Text = "" Then
            showMessage("Pilih No Giro Lama terlebih dahulu sebelum memilih Giro Baru", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindGiroNew(" and g.girono like '%" & girononew.Text & "%'")
    End Sub

    Protected Sub btncleargironew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btncleargironew.Click
        ClearDetailNew()
    End Sub
End Class
