<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
CodeFile="mstEkspedisi.aspx.vb" Inherits="mstekspedisi" Title="Master Expedisi" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
<%--
  <script type="text/javascript">
    function s()
    {
    try {
        var t = document.getElementById("<%=GVmstgen.ClientID%>");
        var t2 = t.cloneNode(true)
        for(i = t2.rows.length -1;i > 0;i--)
        t2.deleteRow(i)
        t.deleteRow(0)
        divTblData.appendChild(t2)
        }
    catch(errorInfo) {}
    }
    window.onload = s
  </script>--%>
  <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center" style="height: 34px">
                            <asp:Label ID="HDRBARANG" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Maroon" Text=".: Data Ekspedisi"></asp:Label></th>
                    </tr>
                </table>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%" Font-Size="Small">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="Label" style="font-size: 11px; font-weight: normal; width: 55px;">
                                            Filter :&nbsp;</td>
                                        <td align="left" colspan="4">
                                            &nbsp;<asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText" Width="96px" Font-Size="Small">
                                                <asp:ListItem Value="gendesc">Nama</asp:ListItem>
                                                <asp:ListItem Value="genother1">Alamat</asp:ListItem>
                                            </asp:DropDownList>
                                        <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="235px" CssClass="inpText" Font-Size="Small"></asp:TextBox>
                                            &nbsp;<asp:ImageButton
                                            ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" Height="23px" />&nbsp;<asp:ImageButton
                                            ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" Height="23px" />&nbsp;<asp:ImageButton ID="ibexcel" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/toexcel.png"
                                                OnClick="ibexcel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5" style="height: 8px">
                                        <asp:GridView ID="GVmstgen" runat="server" CellPadding="4" AutoGenerateColumns="False" Width="100%" CssClass="normalFont" AllowPaging="True" Font-Size="Large" ForeColor="#333333" GridLines="None">
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Right" Font-Bold="True" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="genoid" DataNavigateUrlFormatString="mstEkspedisi.aspx?oid={0}"
                                                    DataTextField="gendesc" HeaderText="Nama Ekspedisi" SortExpression="gendesc">
                                                    <ItemStyle Font-Size="Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="Small" HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="gencode" HeaderText="Kode">
                                                    <HeaderStyle Font-Size="Small" CssClass="gvhdr" ForeColor="Black" />
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="genother1" HeaderText="Alamat" SortExpression="genother1">
                                                    <ItemStyle Font-Size="Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="Small" HorizontalAlign="Left" ForeColor="Black" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="genother3" HeaderText="Contact/Telp/Fax">
                                                    <HeaderStyle Font-Size="Small" CssClass="gvhdr" ForeColor="Black" />
                                                    <ItemStyle Font-Size="Small" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Tidak Ketemu !!"></asp:Label>
                                            </EmptyDataTemplate>
                                            <PagerSettings PageButtonCount="15" />
                                        </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                                </asp:Panel>
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" />
                            <strong><span style="font-size: 9pt">Daftar Ekspedisi :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:TextBox id="genoid" runat="server" Width="25px" CssClass="inpText" MaxLength="10" Visible="False" __designer:wfdid="w354" size="20" Enabled="False"></asp:TextBox></TD><TD align=left><asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" Visible="False" __designer:wfdid="w355"></asp:Label>&nbsp; <asp:DropDownList id="gengroup" runat="server" Width="94px" CssClass="inpText" Font-Size="Small" Visible="False" __designer:wfdid="w356" AutoPostBack="True"><asp:ListItem>EKSPEDISI</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left>Kode</TD><TD align=left><asp:TextBox id="gencode" runat="server" Width="84px" CssClass="inpTextDisabled" Font-Size="Small" MaxLength="30" __designer:wfdid="w357" size="20" Enabled="False" OnTextChanged="gencode_TextChanged"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left>Nama Ekspedisi&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w358"></asp:Label></TD><TD align=left><asp:TextBox id="gendesc" runat="server" Width="273px" CssClass="inpText" Font-Size="Small" MaxLength="50" __designer:wfdid="w359" size="20"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left>Alamat</TD><TD align=left><asp:TextBox id="genother1" runat="server" Width="335px" CssClass="inpText" Font-Size="Small" MaxLength="50" __designer:wfdid="w360" size="20"></asp:TextBox></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left>Kota</TD><TD align=left><asp:DropDownList id="genother2" runat="server" Width="203px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w361" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="FONT-WEIGHT: normal; FONT-SIZE: 11px" class="Label" align=left>Contact/Telp/Fax</TD><TD align=left><asp:TextBox id="genother3" runat="server" Width="336px" Height="32px" CssClass="inpText" Font-Size="Small" MaxLength="80" __designer:wfdid="w362" size="20" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD align=left colSpan=2>Update terakhir oleh&nbsp;&nbsp;<asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w363"></asp:Label>&nbsp;pada tanggal&nbsp;&nbsp;<asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w364"></asp:Label></TD></TR><TR><TD align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" Height="20px" __designer:wfdid="w365"></asp:ImageButton><asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" Height="21px" __designer:wfdid="w366"></asp:ImageButton><asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Height="22px" __designer:wfdid="w367"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" height="16" /> <span style="font-size: 9pt">
                                <strong>
                                Form Ekspedisi</strong> <strong>:.</strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
<asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
<contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" DropShadow="True" TargetControlID="ButtonExtendervalidasi" PopupControlID="Panelvalidasi" BackgroundCssClass="modalBackground" PopupDragHandleControlID="label15" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="ButtonExtendervalidasi" runat="server" Visible="False" CausesValidation="False"></asp:Button> 
</contenttemplate>
</asp:UpdatePanel>
</asp:Content>
