Imports System.Data

Partial Class MasterUser
    Inherits System.Web.UI.MasterPage
    Dim dv As dataview
    'Protected Sub linkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles linkLogin.Click
    '    Session.Clear()
    '    Response.Redirect("~/other/logout.aspx")
    'End Sub
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")

    Function CreateMenuItem(ByVal text As String, ByVal url As String, ByVal toolTip As String) As MenuItem
        ' Create a new MenuItem object.
        Dim menuItem As New MenuItem()
        ' Set the properties of the MenuItem object using
        ' the specified parameters.
        menuItem.Text = text
        menuItem.NavigateUrl = url
        menuItem.ToolTip = toolTip
        Return menuItem
    End Function

    Function CreateParentItem(ByVal text As String, ByVal sNothing As String, ByVal toolTip As String) As MenuItem
        ' Create a new MenuItem object.
        Dim menuItem As New MenuItem()
        ' Set the properties of the MenuItem object using the specified parameters.
        menuItem.Text = text : menuItem.Selectable = False
        menuItem.ToolTip = toolTip
        Return menuItem
    End Function

    Sub CreateSeparator(ByVal parentItem As MenuItem) ' Have ImageUrlBug
        ' Create a new MenuItem object.
        Dim sepItem As New MenuItem() : sepItem.Selectable = False
        sepItem.SeparatorImageUrl = Server.MapPath("~/images/separator.gif")
        parentItem.ChildItems.Add(sepItem)
    End Sub

    Sub CreateSeparator(ByVal parentItem As MenuItem, ByVal cSep As Char, ByVal iCount As Integer)
        ' Create a new MenuItem object.
        Dim sepItem As New MenuItem() : sepItem.Selectable = False
        Dim sSepText As String = ""
        For C1 As Integer = 1 To iCount
            sSepText &= cSep
        Next
        sepItem.Text = sSepText
        'sepItem.SeparatorImageUrl = Server.MapPath("~/Images/separator.png")
        parentItem.ChildItems.Add(sepItem)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblUser.Text = "User Active : " & Session("UserID")
        lblCbang.Text = " || " & Session("branch")
        lblToday.Text = Now.ToLongDateString
        lblCompny.Text = CompnyName.ToUpper
        Page.Title = CompnyName.ToUpper
        If Not IsPostBack Then
            Dim homeMenuItem As MenuItem
            homeMenuItem = CreateMenuItem("HOME", "~/other/menu.aspx?awal=true", "HOME")
            Dim Master As MenuItem
            Master = CreateParentItem("MASTER", "", "MASTER")
            Dim Purchasing As MenuItem
            Purchasing = CreateParentItem("SERVICE", "", "SERVICE")
            Dim Marketing As MenuItem
            Marketing = CreateParentItem("MARKETING", "", "MARKETING")
            Dim Production As MenuItem
            Production = CreateParentItem("PURCHASING", "", "PURCHASING")
            Dim Accounting As MenuItem
            Accounting = CreateParentItem("ACCOUNTING", "", "ACCOUNTING")
            Dim Inventory As MenuItem
            Inventory = CreateParentItem("INVENTORY", "", "INVENTORY")
            Dim Transaction As MenuItem
            Transaction = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim Report As MenuItem
            Report = CreateParentItem("REPORT", "", "REPORT")
            Dim Help As MenuItem
            Help = CreateMenuItem("HELP", "~/Help/-.pdf", "HELP")
            Help.Target = "_blank"
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='MASTER'"
      
            Dim UserProfileChild As MenuItem
            'Dim userProf As MenuItem

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROFILE'" ' PROFILE
            For C1 As Integer = 0 To dv.Count - 1
                UserProfileChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & _
                    dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(UserProfileChild) : Exit For
            Next
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'ROLE'" ' ROLE
            For C1 As Integer = 0 To dv.Count - 1
                UserProfileChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & _
                    dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(UserProfileChild) : Exit For
            Next
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'USER ROLE'" ' USER ROEL
            For C1 As Integer = 0 To dv.Count - 1
                UserProfileChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & _
                    dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(UserProfileChild) : Exit For
            Next

            Dim setupChild As MenuItem
           
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'GENERAL'" ' GENERAL
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next
            ' MASTER APPROVAL
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'APPROVAL'" ' EKSPEDISI
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'RATE STANDARD'" ' RATE
            For C1 As Integer = 0 To dv.Count - 1
                UserProfileChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & _
                    dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(UserProfileChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CURRENCY'" ' CURRENCY
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'DIMENSI'" ' FIXED ASSET
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next
            ' MENU ITEMS
            ' MASTER BARANG + PRICE
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER KATALOG'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            'Master(BARANG + PRICE + HPP)
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PRICE KATALOG'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            'HARGA EXPEDISI
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'HARGA EXPEDISI'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            ' MASTER UPDATE PIC ITEM
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPDATE PIC ITEM'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER EXPEDISI'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SETTING COA PERSEDIAAN'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'BOTTOM PRICE'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPLOAD ADJUSMENT'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER CABANG'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            ' MASTER BARANG + PRICE
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'BARANG + DIMENSI'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER USER ID'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER PRICE CABANG'" ' BOM
            For C1 As Integer = 0 To dv.Count - 1
                Dim mach As MenuItem
                mach = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper) : Master.ChildItems.Add(mach)
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'STATUS KATALOG'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER JABATAN'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next 

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER GUDANG'" ' OTHER SETUP
            For C1 As Integer = 0 To dv.Count - 1
                setupChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, _
                    "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", _
                    dv(C1).Item(1).ToString.Trim.ToUpper)
                Master.ChildItems.Add(setupChild) : Exit For
            Next

            '==================================================================================
            ' Accounting
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='ACCOUNTING'"

            Dim acctgoid As MenuItem = CreateParentItem("MASTER", "", "MASTER")
            Dim accChild As MenuItem

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS BALANCE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'INTERFACE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'COA BALANCE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MONTHLY CLOSING'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next


            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CHART OF ACCOUNT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next


            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'BUKA POSTING AR PAYMENT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'NO FAKTUR PAJAK'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'ASSIGN NO FAKTUR'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgoid.ChildItems.Add(accChild) : Next
        
            AddingMenu(Accounting, acctgoid)

            Dim acctg As MenuItem = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim accChild1 As MenuItem


            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL HUTANG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL PIUTANG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AP BALANCE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AR BALANCE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/P PAYMENT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/R PAYMENT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DOWN PAYMENT A/P'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DOWN PAYMENT A/R'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next


            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CREDIT NOTE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DEBIT NOTE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'CASH/BANK RECEIPT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'EXPENSE ACCOUNTING'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MUTATION ACCOUNTING'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME ='GIRO'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MANIFESTED GIRO IN (A/R)'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MANIFESTED GIRO OUT (A/P)'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS PURCHASE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'FIXED ASSETS TRANSACTION'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'SALDO AWAL VOUCHER'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'REALISASI VOUCHER'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PAYMENT AR SERVIS'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'MEMORIAL JURNAL'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'A/R KOREKSI'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AP RETUR'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim &  "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'DP AR RETUR'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PREPAID TRANSACTION'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PREPAID DISPOSE%='"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'NOTA PIUTANG GANTUNG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            dv.RowFilter = "FORMTYPE='ACCOUNTING' AND FORMNAME = 'PELUNASAN PIUTANG GANTUNG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChild1 = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctg.ChildItems.Add(accChild1) : Next

            AddingMenu(Accounting, acctg)

            Dim acctgrpt As MenuItem = CreateParentItem("REPORT", "", "REPORT")
            Dim accChildrpt As MenuItem
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT DP AR'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT DP AP'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'GENERAL JOURNAL REPORT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT GENERAL LEDGER'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT BANK'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT CASH'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN HUTANG%='"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PIUTANG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'Report A/P Payment Status'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'Report A/R Payment Status'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT LABA RUGI'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'MANIFEST GIRO IN REPORT'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'MANIFEST GIRO OUT STATUS'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'INCOMING GIRO REALIZATION'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'OUTGOING GIRO REALIZATION'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN REALISASI VOUCHER'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REPORT TRIAL BALANCE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN LABA RUGI PER ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next


            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK ADMIN'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PIUTANG GANTUNG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN MATERIAL USAGE ON PRICE'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN ANALISA BIAYA'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN NOTA PIUTANG GANTUNG'"
            For C1 As Integer = 0 To dv.Count - 1
                accChildrpt = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                acctgrpt.ChildItems.Add(accChildrpt) : Next

            AddingMenu(Accounting, acctgrpt)
            '===========================================================================================================

            'SERVICE
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='TRANSACTION'"
            ' Transaction
            Dim TrnTransPurchasing As MenuItem = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim TrnTransPurchChild As MenuItem
 
            Dim mstsrv As MenuItem = CreateParentItem("MASTER", "", "MASTER")
            Dim mstsrvchild As MenuItem
            AddingMenu(Purchasing, mstsrv)

            ' PR
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PENERIMAAN SERVICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TW SERVICE CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TC SERVICE CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SERVICE SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'KEMBALI SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME='TC SUPPLIER INTERNAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME ='SERVICE FINAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SERVICE FINAL INTERNAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next


            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'INVOICE SERVICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK HISTORY PENERIMAAN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransPurchasing.ChildItems.Add(TrnTransPurchChild) : Next


            AddingMenu(Purchasing, TrnTransPurchasing)

            'Report
            Dim TrnRptPurchasing As MenuItem = CreateParentItem("REPORT", "", "REPORT")
            Dim TrnRptPurchChild As MenuItem
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PENERIMAAN BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STATUS SERVICE SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STATUS TW SERVICE INTERNAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SERVICE FINAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN INVOICE SERVICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'SERVICE SUPPLIER INTERNAL STATUS'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SERVICE FINAL INTERNAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PENDING'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptPurchChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptPurchasing.ChildItems.Add(TrnRptPurchChild) : Next

            AddingMenu(Purchasing, TrnRptPurchasing)
            '================================================================================= 
            'MARKETING
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='TRANSACTION'"

            Dim mstmkt As MenuItem = CreateParentItem("MASTER", "", "MASTER")
            Dim mstmktchild As MenuItem

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER CABANG'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'MASTER CUSTOMER PUSAT'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'EKSPEDISI'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER GROUP'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROGRAM POINT CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'CUSTOMER APPROVAL'"
            For C1 As Integer = 0 To dv.Count - 1
                mstmktchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstmkt.ChildItems.Add(mstmktchild) : Next

            AddingMenu(Marketing, mstmkt)

            ' Marketing
            Dim TrnTransMarketing As MenuItem = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim TrnTransMarkChild As MenuItem
       
            ' SO
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            ' DO
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'DELIVERY ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            ' SALES INVOICE
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES INVOICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            ' SALES RETURN
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SALES RETURN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SO CLOSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            'SO Cancel 
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SO CANCELED'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            'SURAT JALAN
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SURAT JALAN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            'NOTA EKSPEDISI
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'NOTA EKSPEDISI'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            'SI RECORD
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'SI RECORD'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            'CEK PIUTANG PER NOTA
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK PIUTANG PER NOTA'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TANDA TERIMA SEMENTARA'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'NOTA PENJUALAN ROMBENG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransMarkChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransMarketing.ChildItems.Add(TrnTransMarkChild) : Next

            AddingMenu(Marketing, TrnTransMarketing)

            'Report
            Dim TrnRptMarketing As MenuItem = CreateParentItem("REPORT", "", "REPORT")
            Dim TrnRptMarChild As MenuItem
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES DELIVERY ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SALES INVOICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SO CLOSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN RETUR PENJUALAN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PENJUALAN BERSIH'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            'LAPORAN SURAT JALAN
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN SURAT JALAN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN NOTA EXPEDISI'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PROGRAM POINT CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN HISTORY PRICE JUAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PROMO CUSTOMER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN CUSTOMER APPROVAL'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN INFO CETAK SI'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptMarChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptMarketing.ChildItems.Add(TrnRptMarChild) : Next

            AddingMenu(Marketing, TrnRptMarketing)
            '==================================================================================
            'PURCHASING
            'Master

            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='MASTER'"

            Dim mstPch As MenuItem = CreateParentItem("MASTER", "", "MASTER")
            Dim mstPchchild As MenuItem
            'SUPPLIER ADMIN
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SUPPLIER ADMIN'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next
            'SUPPLIER
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next

            'SUPPLIER
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROMO'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'TARGET ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next

            'SUPPLIER
            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'PROMO SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'OPEN CABANG PDO'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next 

            dv.RowFilter = "FORMTYPE='MASTER' AND FORMNAME = 'UPDATE TANGGAL PROMO PI'"
            For C1 As Integer = 0 To dv.Count - 1
                mstPchchild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                mstPch.ChildItems.Add(mstPchchild) : Next

            AddingMenu(Production, mstPch)

            ' Transaction
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='TRANSACTION'"
            Dim TrnTransProduction As MenuItem = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim TrnTransProdChild As MenuItem
            ' BPM
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE DO'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE INVOICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PURCHASE RETURN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next
 
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PO CLOSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CEK HUTANG PER NOTA'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'FORECAST PO'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransProduction.ChildItems.Add(TrnTransProdChild) : Next

            AddingMenu(Production, TrnTransProduction)
            'Report
            Dim TrnRptProductiong As MenuItem = CreateParentItem("REPORT", "", "REPORT")
            Dim TrnRptProdChild As MenuItem
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE DELIVERY ORDER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE INVOICE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PURCHASE RETURN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PO CLOSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REALISASI PEMBELIAN'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PO STATUS'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN VOUCHER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'REKAP PROMO SUPPLIER'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAP. PEMBELIAN AMP'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptProdChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptProductiong.ChildItems.Add(TrnRptProdChild) : Next

            AddingMenu(Production, TrnRptProductiong)
            '================================================================================== 
            'INVENTORY
            dv = Session("Role").defaultview
            dv.RowFilter = "FORMTYPE='TRANSACTION'"
            ' Marketing
            Dim TrnTransInventory As MenuItem = CreateParentItem("TRANSACTION", "", "TRANSACTION")
            Dim TrnTransInvChild As MenuItem
            ' STOCK OPNAME
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'INITIAL STOCK'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' Stock adjustment
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'STOCK ADJUSTMENT'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next
            ' TRANSFER WAREHOUSE
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFER WAREHOUSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' TRANSFORM ITEM
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFORM ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'CLOSING STOCK'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' TRANSFER CONFIRM
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TRANSFER CONFIRM'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' matusage
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'MATERIAL USAGE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' PINJAM BARANG
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PINJAM BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            ' PENGEMBALIAN BARANG
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PENGEMBALIAN BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'PERMINTAAN BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next
            dv.RowFilter = "FORMTYPE='TRANSACTION' AND FORMNAME = 'TW E-COMMERCE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnTransInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnTransInventory.ChildItems.Add(TrnTransInvChild) : Next

            AddingMenu(Inventory, TrnTransInventory)

            '-------------------------
            'Report
            Dim TrnRptInventory As MenuItem = CreateParentItem("REPORT", "", "REPORT")
            Dim TrnRptInvChild As MenuItem
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN STOCK ADJUSTMENT'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next
            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TRANSFER WAREHOUSE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TRANSFORMASI ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN MATERIAL USAGE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TW STATUS'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PINJAM BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN FAST MOVING'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN DIMENSI ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN HISTORY HPP ITEM'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN DEADSTOCK'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN PERMINTAAN BARANG'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            dv.RowFilter = "FORMTYPE='REPORT' AND FORMNAME = 'LAPORAN TW E-COMMERCE'"
            For C1 As Integer = 0 To dv.Count - 1
                TrnRptInvChild = CreateMenuItem(dv(C1).Item(1).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & _
                    "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnRptInventory.ChildItems.Add(TrnRptInvChild) : Next

            AddingMenu(Inventory, TrnRptInventory)
            '============================================================================ 
            Menu1.Items.Add(homeMenuItem)
            AddingMenu(Menu1, Master)
            AddingMenu(Menu1, Purchasing)
            AddingMenu(Menu1, Marketing)
            AddingMenu(Menu1, Production)
            AddingMenu(Menu1, Inventory)
            AddingMenu(Menu1, Accounting)
            AddingMenu(Menu1, Transaction)
            AddingMenu(Menu1, Report)
            Menu1.Items.Add(Help)
        End If
    End Sub

    Private Sub AddingMenu(ByVal parent As Menu, ByVal child As MenuItem)
        If child.ChildItems.Count > 0 Then
            parent.Items.Add(child)
        End If
    End Sub

    Private Sub AddingMenu(ByVal parent As MenuItem, ByVal child As MenuItem)
        If child.ChildItems.Count > 0 Then
            parent.ChildItems.Add(child)
        End If
    End Sub

    Protected Sub Menu2_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu2.MenuItemClick

    End Sub
End Class

