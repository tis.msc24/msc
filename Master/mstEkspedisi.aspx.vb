'Prgmr:ifan | UpdtBy:4n7JuK | LastUpdt:09.04.29
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class mstekspedisi
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
#End Region

#Region "Function"

#End Region

#Region "Procedure"
   

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub


    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        If Session("FilterDDL") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        Else
            sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        End If

        sSql = "SELECT cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,  createuser, upduser,updtime FROM QL_mstgen " & sWhere & IIf(sWhere <> "", " and ", " where ") & " upper(cmpcode) LIKE '%" & sCompCode.ToUpper & "%' and gengroup='EKSPEDISI' ORDER BY   gencode, gendesc"
         
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
    End Sub


    Public Sub FillTextBox(ByVal vGenoid As String)
        Try
            sSql = "SELECT cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,upduser,updtime,genother3 FROM QL_mstgen where cmpcode='" & CompnyCode & "' and genoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                genoid.Text = Trim(xreader.GetValue(1).ToString)
                gencode.Text = Trim(xreader.GetValue(2).ToString)
                gendesc.Text = Trim(xreader.GetValue(3).ToString)
                 
                
                 
                 genother1.Text = Trim(xreader.GetValue(5).ToString)
                genother2.SelectedValue = Trim(xreader.GetValue(6).ToString)
                
                genother3.Text = Trim(xreader("genother3").ToString)
                Upduser.Text = Trim(xreader.GetValue(7).ToString)
                Updtime.Text = Format(xreader.GetValue(8), "dd/MM/yyyy HH:mm:ss")
                btnDelete.Enabled = True


            End While

        Catch ex As Exception
            showMessage(ex.Message, "ERROR")
        Finally
            xreader.Close()
            conn.Close()
        End Try

    End Sub

    Public Sub InitAllDDL()
        sSql = "select lastoid,tablename from QL_mstoid where tablegroup='GENERAL'  "
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        
         
        sSql = "select distinct genoid,gendesc from QL_mstgen where cmpcode='" & CompnyCode & "' and gengroup='CITY' ORDER BY GENDESC "
        FillDDL(genother2, sSql)

        If gengroup.Items.Count = 0 Then
            'RegisterStartupScript("Informasi", "<script>alert('Please create/fill master TABLE!')</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PERINGATAN_PESAN", String.Format("<script>alert('{0}')</script>", "Please create/fill master TABLE!"))
            Exit Sub
        End If
    End Sub

    Protected Sub generateKode(ByVal nama As String)
        Dim namaItem, jumlahNol, c, kodeItem As String
        jumlahNol = ""
        Dim a, b, d As Integer
        namaItem = Left(nama, 1).ToUpper
        c = 0
        Try
            c = ToDouble(GetStrData("SELECT isnull(max(abs(replace(gencode, '" & namaItem & "', ''))),0) FROM ql_mstgen WHERE left(genCODE,1) = '" & namaItem & "'   and rtrim(gencode)<>'' and gengroup='EKSPEDISI'"))
        Catch ex As Exception
        End Try
        d = CInt(c) + 1

        kodeItem = GenNumberString(namaItem, "", d, 4)
        gencode.Text = kodeItem
    End Sub

    Public Sub GenerateGenID()
        genoid.Text = GenerateID("QL_mstgen", CompnyCode)
    End Sub

    Sub clearItem()
        gencode.Text = ""
        gendesc.Text = ""
        gengroup.SelectedIndex = 0
        genother1.Text = ""
        genother2.Text = ""
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Response.Redirect("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstekspedisi.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk menghapus data ini ?');")
        Page.Title = CompnyName & " - Data Ekspedisi"

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update"
            TabContainer1.ActiveTabIndex = 1
        Else
            i_u.Text = "New"
        End If

        If Not IsPostBack Then
            BindData(CompnyCode)
            InitAllDDL()
             If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                If gencode.Text.Trim = "" Then
                    generateKode(gendesc.Text.Trim)
                End If
            Else
                gencode.Text = ""
                GenerateGenID()
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
            End If
           
        End If
    End Sub

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
         Session("FilterText") = ""
        Session("FilterDDL") = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        GVmstgen.PageIndex = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If gencode.Text.Trim = "" Then
            generateKode(gendesc.Text.Trim)
        End If

        If gendesc.Text.Trim = "" Then
            showMessage("Isikan Nama Ekspedisi terlebih dahulu !!", "PERINGATAN")
            Exit Sub

        End If
        If Tchar(genother3.Text).Length > 80 Then
            showMessage("Maximal 80 karakter untuk Contact/Telp/Fax !!", "PERINGATAN")
            Exit Sub
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck As String = "SELECT COUNT(-1) FROM ql_mstgen WHERE gengroup = '" & gengroup.Text & _
            "' AND gendesc = '" & Tchar(gendesc.Text.ToUpper) & "'"
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck &= " AND genoid <> " & genoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck) > 0 Then
            showMessage("Nama Ekspedisi telah digunakan !!", "PERINGATAN")
            Exit Sub
        End If
        If Session("oid") = Nothing Or Session("oid") = "" Then
            genoid.Text = GenerateID("QL_mstgen", CompnyCode)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            'UNTUK MENGISI GENOTHER TEXT PADA SAAT DDL AKTIF

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_mstgen(cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,createuser,upduser,updtime,genother3) " & _
                "VALUES ('" & Tchar(CompnyCode) & "'," & genoid.Text & ",'" & Tchar(gencode.Text.ToUpper) & "','" & Tchar(gendesc.Text.ToUpper) & "','" & Tchar(gengroup.Text.ToUpper) & "','" & Tchar(genother1.Text.ToUpper) & "'," & genother2.SelectedValue & ",'" & Session("UserID") & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(genother3.Text.ToUpper) & "')"
            Else
                sSql = "UPDATE QL_mstgen SET  gencode='" & gencode.Text & "', genDesc='" & Tchar(gendesc.Text.ToUpper) & "',    genother1='" & Tchar(genother1.Text.ToUpper) & "', genother2= " & genother2.SelectedValue & " , upduser='" & Session("UserID") & "', updtime=current_timestamp,genother3='" & Tchar(genother3.Text.ToUpper) & "'  WHERE cmpcode like '%" & CompnyCode & "%' and genoid=" & genoid.Text
            End If
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "update  QL_mstoid set lastoid=" & genoid.Text & " where tablename like '%QL_mstgen%' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, "") : Exit Sub
        End Try
        Session("oid") = Nothing
        'Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah tersimpan !", "INFORMASI")
        Response.Redirect("~\Master\mstekspedisi.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click

        Dim kolom() As String = {"ekspedisioid"}

        Dim tabel() As String = {"ql_trnordermst"}

        If CheckDataExists(genoid.Text, kolom, tabel) Then
            showMessage("Data tidak dapat dihapus karena digunakan oleh form lain !!", "PERINGATAN")
            Exit Sub
        ElseIf genoid.Text = "" Then
            showMessage("Pilih data ekspedisi !!", CompnyName & " - ERROR")
            Exit Sub
        End If

        If DeleteData("QL_mstgen", "genoid", genoid.Text, CompnyCode) = True Then

        Else
            Exit Sub
        End If
        'Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah terhapus !", "INFORMASI")
        Response.Redirect("~\Master\mstekspedisi.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstekspedisi.aspx?awal=true")
    End Sub

    
    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstekspedisi.aspx?awal=true")
        End If
    End Sub
#End Region

    Protected Sub gencode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub SqlDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs)

    End Sub

    Protected Sub ddlgabungan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click

        Response.Clear()
        Response.AddHeader("content-disposition", "inline;filename=MasterEkspedisi.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        If Session("FilterDDL") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        Else
            sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        End If

       
        sSql = "SELECT    gencode Kode, gendesc Nama, genother1 Alamat,(select gendesc from ql_mstgen g where g.genoid=ql_mstgen.genother2) Kota, genother3 [Contact/Telp/Fax]  FROM QL_mstgen " & sWhere & IIf(sWhere <> "", " and ", " where ") & " upper(cmpcode) = '" & CompnyCode & "' and gengroup='EKSPEDISI' ORDER BY gengroup, gendesc"


        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()

    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function


End Class