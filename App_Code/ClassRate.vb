Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Public Class ClassRate
    Private iRateDailyOid As Integer
    Private iRateMonthlyOid As Integer
    Private dRateDailyIDRValue As Double
    Private dRateDailyUSDValue As Double
    Private dRateMonthlyIDRValue As Double
    Private dRateMonthlyUSDValue As Double
    Private sRateDailyLastError As String
    Private sRateMonthlyLastError As String
    Private iCurrOid As Integer
    Private sCurrCode As String

    Public Sub New()
        iRateDailyOid = 0 : iRateMonthlyOid = 0
        dRateDailyIDRValue = 0 : dRateMonthlyIDRValue = 0
        dRateDailyUSDValue = 0 : dRateMonthlyUSDValue = 0
        sRateDailyLastError = "" : sRateMonthlyLastError = ""
        iCurrOid = 0 : sCurrCode = ""
    End Sub

    Public Sub New(ByVal iCurr As Integer, ByVal ToDate As String)
        SetRateValue(iCurr, ToDate)
    End Sub

    Public Sub New(ByVal sCurr As String, ByVal ToDate As String)
        SetRateValue(sCurr, ToDate)
    End Sub

    Public Function GetRateDailyOid() As Integer
        Return iRateDailyOid
    End Function

    Public Function GetRateMonthlyOid() As Integer
        Return iRateMonthlyOid
    End Function

    Public Function GetRateDailyIDRValue() As Double
        Return dRateDailyIDRValue
    End Function

    Public Function GetRateMonthlyIDRValue() As Double
        Return dRateMonthlyIDRValue
    End Function

    Public Function GetRateDailyUSDValue() As Double
        Return dRateDailyUSDValue
    End Function

    Public Function GetRateMonthlyUSDValue() As Double
        Return dRateMonthlyUSDValue
    End Function

    Public Function GetRateDailyLastError() As String
        Return sRateDailyLastError
    End Function

    Public Function GetRateMonthlyLastError() As String
        Return sRateMonthlyLastError
    End Function

    Public Function GetCurrOid() As Integer
        Return iCurrOid
    End Function

    Public Function GetCurrCode() As String
        Return sCurrCode
    End Function

    Public Overloads Sub SetRateValue(ByVal iCurr As Integer, ByVal ToDate As String)
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As SqlCommand
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        iCurrOid = iCurr
        sSql = "SELECT currencycode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "'  AND currencyoid=" & iCurrOid
        xCmd = New SqlCommand(sSql, conn)
        sCurrCode = CStr(xCmd.ExecuteScalar().ToString)

        '' Get Daily Rate Data
        'sSql = "SELECT TOP 1 rateoid, rateres1 AS rateidrvalue, rateres2 AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND currencyoid=" & iCurrOid & " AND '" & CDate(ToDate) & " 00:00:00'>=ratedate AND '" & ToDate & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
        'xCmd = New SqlCommand(sSql, conn)
        'Dim xRdrDaily As SqlDataReader = xCmd.ExecuteReader
        'If xRdrDaily.HasRows Then
        '    While xRdrDaily.Read
        '        iRateDailyOid = xRdrDaily.GetInt32(0)
        '        dRateDailyIDRValue = CDbl(xRdrDaily.GetString(1))
        '        dRateDailyUSDValue = CDbl(xRdrDaily.GetString(2))
        '    End While
        'Else
        '    sRateDailyLastError = "Please define some Rate Daily data for Currency : " & sCurrCode & " and Date : " & ToDate & " first before continue using this form!"
        '    iRateDailyOid = 0
        '    dRateDailyIDRValue = 0
        '    dRateDailyUSDValue = 0
        'End If
        'xRdrDaily.Close()

        ' Get Monthly Rate Data
        sSql = "SELECT TOP 1 rate2oid, rate2res1 AS rate2idrvalue, rate2res2 AS rate2usdvalue FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND currencyoid=" & iCurrOid & " AND rate2month=" & Month(CDate(ToDate)) & " AND rate2year=" & Year(CDate(ToDate)) & " AND activeflag='ACTIVE' ORDER BY rate2oid DESC"
        xCmd = New SqlCommand(sSql, conn)
        Dim xRdrMonthly As SqlDataReader = xCmd.ExecuteReader
        If xRdrMonthly.HasRows Then
            While xRdrMonthly.Read
                iRateMonthlyOid = xRdrMonthly.GetInt32(0)
                dRateMonthlyIDRValue = CDbl(xRdrMonthly.GetString(1))
                dRateMonthlyUSDValue = CDbl(xRdrMonthly.GetString(2))
            End While
        Else
            'sRateMonthlyLastError = "Please define some Rate Monthly data for Currency : " & sCurrCode & " and Period : " & Format(CDate(ToDate), "MMMM") & " " & Year(CDate(ToDate)) & " before continue using this form!"
            sRateMonthlyLastError = "Rate Currency Belum Di isi !"
            iRateMonthlyOid = 0
            dRateMonthlyIDRValue = 0
            dRateMonthlyUSDValue = 0
        End If
        xRdrMonthly.Close()
        conn.Close()
    End Sub

    Public Overloads Sub SetRateValue(ByVal sCurr As String, ByVal ToDate As String)
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As SqlCommand
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sCurrCode = sCurr
        sSql = "SELECT curroid FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND currcode='" & sCurrCode & "'"
        xCmd = New SqlCommand(sSql, conn)
        iCurrOid = CInt(xCmd.ExecuteScalar().ToString)

        ' Get Daily Rate Data
        sSql = "SELECT TOP 1 rateoid, rateres1 AS rateidrvalue, rateres2 AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iCurrOid & " AND '" & ToDate & " 00:00:00'>=ratedate AND '" & ToDate & " 00:00:00'<=ratetodate AND activeflag='ACTIVE' ORDER BY ratedate DESC, rateoid DESC"
        xCmd = New SqlCommand(sSql, conn)
        Dim xRdrDaily As SqlDataReader = xCmd.ExecuteReader
        If xRdrDaily.HasRows Then
            While xRdrDaily.Read
                iRateDailyOid = xRdrDaily.GetInt32(0)
                dRateDailyIDRValue = CDbl(xRdrDaily.GetString(1))
                dRateDailyUSDValue = CDbl(xRdrDaily.GetString(2))
            End While
        Else
            sRateDailyLastError = "Please define some Rate Daily data for Currency : " & sCurrCode & " and Date : " & ToDate & " first before continue using this form!"
            iRateDailyOid = 0
            dRateDailyIDRValue = 0
            dRateDailyUSDValue = 0
        End If
        xRdrDaily.Close()

        ' Get Monthly Rate Data
        sSql = "SELECT TOP 1 rate2oid, rate2res1 AS rate2idrvalue, rate2res2 AS rate2usdvalue FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iCurrOid & " AND rate2month=" & Month(CDate(ToDate)) & " AND rate2year=" & Year(CDate(ToDate)) & " AND activeflag='ACTIVE' ORDER BY rate2oid DESC"
        xCmd = New SqlCommand(sSql, conn)
        Dim xRdrMonthly As SqlDataReader = xCmd.ExecuteReader
        If xRdrMonthly.HasRows Then
            While xRdrMonthly.Read
                iRateMonthlyOid = xRdrMonthly.GetInt32(0)
                dRateMonthlyIDRValue = CDbl(xRdrMonthly.GetString(1))
                dRateMonthlyUSDValue = CDbl(xRdrMonthly.GetString(2))
            End While
        Else
            sRateMonthlyLastError = "Please define some Rate Monthly data for Currency : " & sCurrCode & " and Period : " & Format(CDate(ToDate), "MMMM") & " " & Year(CDate(ToDate)) & " before continue using this form!"
            iRateMonthlyOid = 0
            dRateMonthlyIDRValue = 0
            dRateMonthlyUSDValue = 0
        End If
        xRdrMonthly.Close()
        conn.Close()
    End Sub

    Public Overloads Sub SetRateValue(ByVal iRateOid As Integer, ByVal iRate2Oid As Integer)
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("QL_CONN"))
        Dim xCmd As SqlCommand
        Dim sSql As String = ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        ' Get Daily Rate Data
        sSql = "SELECT rateoid, rateres1 AS rateidrvalue, rateres2 AS rateusdvalue, ratedate FROM QL_mstrate WHERE cmpcode='" & CompnyCode & "' AND rateoid=" & iRateOid & ""
        xCmd = New SqlCommand(sSql, conn)
        Dim xRdrDaily As SqlDataReader = xCmd.ExecuteReader
        If xRdrDaily.HasRows Then
            While xRdrDaily.Read
                iRateDailyOid = xRdrDaily.GetInt32(0)
                dRateDailyIDRValue = CDbl(xRdrDaily.GetString(1))
                dRateDailyUSDValue = CDbl(xRdrDaily.GetString(2))
            End While
        End If
        xRdrDaily.Close()

        ' Get Monthly Rate Data
        sSql = "SELECT rate2oid, rate2res1 AS rate2idrvalue, rate2res2 AS rate2usdvalue FROM QL_mstrate2 WHERE cmpcode='" & CompnyCode & "' AND rate2oid=" & iRate2Oid & ""
        xCmd = New SqlCommand(sSql, conn)
        Dim xRdrMonthly As SqlDataReader = xCmd.ExecuteReader
        If xRdrMonthly.HasRows Then
            While xRdrMonthly.Read
                iRateMonthlyOid = xRdrMonthly.GetInt32(0)
                dRateMonthlyIDRValue = CDbl(xRdrMonthly.GetString(1))
                dRateMonthlyUSDValue = CDbl(xRdrMonthly.GetString(2))
            End While
        End If
        xRdrMonthly.Close()
        conn.Close()
    End Sub
End Class
