Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_rptSalesItem
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim Report As New ReportDocument
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Dim kon As New Koneksi
#End Region

#Region "Functions"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Public Function CheckPeriod() As Boolean
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Tanggal Terakhir tidak boleh lebih kecil dari Tanggal Awal."
                Return False
            End If
        Catch
            lblkonfirmasi.Text = "Format tanggal salah."
            Return False
        End Try
        Return True
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = kon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub UpdateCheckedGV()
        If Not Session("QL_mstitem") Is Nothing Then
            Dim dtab As DataTable = Session("QL_mstitem")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To GVCust.Rows.Count - 1
                    cb = GVCust.Rows(i).FindControl("chkSelect")
                    dView.RowFilter = "itemoid = " & cb.ToolTip
                    drView = dView.Item(0)
                    drView.BeginEdit()
                    If cb.Checked = True Then
                        drView("selected") = 1
                    Else
                        drView("selected") = 0
                    End If
                    drView.EndEdit()
                    dView.RowFilter = ""
                Next
                dtab.AcceptChanges()
            End If
            Session("QL_mstitem") = dtab
        End If
    End Sub
#End Region

#Region "Procedures"
    Public Sub initddl()
        Dim Amp As String = GetStrData("Select ISNULL(pn.PERSONSTATUS,'') from QL_MSTPROF pr INNER JOIN QL_MSTPERSON pn ON pn.PERSONNAME=pr.USERNAME INNER JOIN ql_mstgen g On genoid=PERSONSTATUS Where pn.PERSONOID IN (Select personoid From QL_mstitem i Where i.personoid=pn.PERSONOID) AND USERID='" & Session("UserID").ToString.ToUpper & "'")

        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup = 'Cabang'"
        If Amp = "963" Then
            FillDDL(DDLcabang, sSql)
            DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLcabang.SelectedValue = "ALL"
            DDLcabang.SelectedIndex = DDLcabang.Items.Count - 1
        Else
            If Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(DDLcabang, sSql)
                DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLcabang.SelectedValue = "ALL"
                DDLcabang.SelectedIndex = DDLcabang.Items.Count - 1
            ElseIf Session("UserLevel") = 2 Or Session("UserLevel") = 4 Then
                sSql &= " AND Gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            ElseIf Session("UserLevel") = 0 Then
                sSql &= " AND Gencode='" & Session("branch_id") & "'"
                FillDDL(DDLcabang, sSql)
            End If
        End If
       
    End Sub

    Private Sub SalesDDL()
        Dim Amp As String = GetStrData("Select ISNULL(pn.PERSONSTATUS,'') from QL_MSTPROF pr INNER JOIN QL_MSTPERSON pn ON pn.PERSONNAME=pr.USERNAME INNER JOIN ql_mstgen g On genoid=PERSONSTATUS Where pn.PERSONOID IN (Select personoid From QL_mstitem i Where i.personoid=pn.PERSONOID) AND USERID='" & Session("UserID").ToString.ToUpper & "'")

        If rblType.SelectedValue = "sales" Then
            sSql = "Select PERSONOID,PERSONNAME FROM QL_MSTPERSON ps INNER JOIN QL_MSTPROF pr ON pr.USERNAME=ps.PERSONNAME Where STATUS='AKTIF' AND PERSONOID<>0 "

            If Amp = "963" Then
                sSql &= " AND PERSONSTATUS IN (987,986)" : FillDDL(pic, sSql)
                pic.Items.Add(New ListItem("ALL", "ALL"))
                pic.SelectedIndex = pic.Items.Count - 1
            Else

                If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                    sSql &= " And pr.BRANCH_CODE = '" & DDLcabang.SelectedValue & "' AND USERID='" & Session("UserID").ToString.ToUpper & "' AND PERSONSTATUS =987" : FillDDL(pic, sSql)
                ElseIf Session("UserLevel") = 2 Then
                    sSql &= " And pr.BRANCH_CODE = '" & DDLcabang.SelectedValue & "' AND PERSONSTATUS IN (987,986)"
                    FillDDL(pic, sSql)
                ElseIf Session("UserLevel") = 3 Or Session("UserLevel") = 1 Then
                    If DDLcabang.SelectedValue <> "ALL" Then
                        sSql &= " And pr.BRANCH_CODE = '" & DDLcabang.SelectedValue & "' AND PERSONSTATUS IN (987,986)"
                    End If
                    sSql &= " AND PERSONSTATUS IN (987,986)"
                    FillDDL(pic, sSql)
                    pic.Items.Add(New ListItem("ALL", "ALL"))
                    pic.SelectedIndex = pic.Items.Count - 1
                End If
            End If

        Else

            sSql = "Select PERSONOID,PERSONNAME from QL_MSTPROF pr INNER JOIN QL_MSTPERSON pn ON pn.PERSONNAME=pr.USERNAME Where pn.PERSONOID IN (Select personoid From QL_mstitem i Where i.personoid=pn.PERSONOID) " & sSql & ""

            If Amp = "963" Then
                If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                    sSql &= " AND pr.userid='" & Session("UserID").ToString.ToUpper & "'"
                    FillDDL(pic, sSql)
                ElseIf Session("UserLevel") = 2 Then
                    sSql &= " AND pn.PERSONSTATUS=963"
                    FillDDL(pic, sSql)
                    pic.Items.Add(New ListItem("ALL", "ALL"))
                    pic.SelectedIndex = pic.Items.Count - 1
                ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                    sSql &= " AND pn.PERSONSTATUS=963"
                    FillDDL(pic, sSql)
                    pic.Items.Add(New ListItem("ALL", "ALL"))
                    pic.SelectedIndex = pic.Items.Count - 1
                End If
            Else
                sSql &= " AND pn.PERSONSTATUS=963"
                FillDDL(pic, sSql)
                pic.Items.Add(New ListItem("ALL", "ALL"))
                pic.SelectedIndex = pic.Items.Count - 1
            End If
        End If

    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub ClearView()
        crvReportForm.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        If Not CheckPeriod() Then
            Exit Sub
        End If
        Try
            Dim dtSupp As DataTable = Session("QL_mstitem")
            Dim iTemoid As String = ""

            If GVCust.Visible = True Then
                If Not (Session("QL_mstitem") Is Nothing) Then
                    If dtSupp.Rows.Count > 0 Then
                        Dim dvSupp As DataView = dtSupp.DefaultView
                        dvSupp.RowFilter = "selected='1'"
                        If dvSupp.Count < 1 Then
                            showMessage("- Maaf, Filter Katalog belum anda pilih, Silahkan Klik icon luv kemudian pilih Katalog..<BR>", 2)
                            Exit Sub
                        End If
                        dvSupp.RowFilter = ""
                    Else
                        showMessage("- Maaf, Filter Katalog belum anda pilih, Silahkan Klik icon luv kemudian pilih Katalog..<BR>", 2)
                        Exit Sub
                    End If
                Else
                    showMessage("- Maaf, Filter Katalog belum anda pilih, Silahkan Klik icon luv kemudian pilih Katalog..<BR>", 2)
                    Exit Sub
                End If
            End If

            lblkonfirmasi.Text = "" : Session("diprint") = "False"
            Dim namaPDF As String = "" : Dim sWhereDetail As String = ""
            Dim sWhere As String = "" : Dim sTatus As String = ""

            If GVCust.Visible = True Then
                Dim vDt As DataView = dtSupp.DefaultView
                vDt.RowFilter = "selected='1'"

                For R1 As Integer = 0 To vDt.Count - 1
                    iTemoid &= vDt(R1)("itemoid").ToString & ","
                Next

                If iTemoid <> "" Then
                    sWhere &= " AND i.itemoid IN (" & Left(iTemoid, iTemoid.Length - 1) & ")"
                End If
            End If
           
            If rblType.SelectedValue = "pic" Then
                sWhere &= " AND jm.trnjualdate BETWEEN '" & CDate(toDate(dateAwal.Text)) & " 00:00' AND '" & CDate(toDate(dateAkhir.Text)) & " 23:59'"
                'sWhere &= IIf(oid.Text.Trim = "", "", " AND i.itemoid=" & oid.Text)
                If ddlStatus.SelectedValue <> "ALL" Then
                    sWhere &= "AND m.trnjualstatus LIKE '%" & ddlStatus.SelectedValue & "%'"
                End If
                sWhere &= IIf(DDLcabang.SelectedValue = "ALL", "", " AND jd.branch_code ='" & DDLcabang.SelectedValue & "'")
                sWhere &= IIf(pic.SelectedValue = "ALL", "", " AND i.personoid='" & pic.SelectedValue & "'")
                sWhere &= IIf(ddlproduct.SelectedValue = "ALL", "", " AND i.statusitem='" & ddlproduct.SelectedValue & "'")
                Report = New ReportDocument
                Report.Load(Server.MapPath("~\Report\crPICItem.rpt"))
                namaPDF = "SALESITEM_"

                Dim crConnInfo As New ConnectionInfo
                With crConnInfo
                    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                    .IntegratedSecurity = True
                End With
                setDBLogonForReport(crConnInfo, Report)
                Report.PrintOptions.PaperSize = PaperSize.PaperA4
                Report.SetParameterValue("sWhere", sWhere)
                Report.SetParameterValue("aWal", CDate(toDate(dateAwal.Text)))
                Report.SetParameterValue("aKhir", CDate(toDate(dateAkhir.Text)))
            Else
                sWhere &= " AND jm.trnjualdate BETWEEN '" & CDate(toDate(dateAwal.Text)) & " 00:00' AND '" & CDate(toDate(dateAkhir.Text)) & " 23:59'"
                'sWhere &= IIf(oid.Text.Trim = "", "", " AND i.itemoid=" & oid.Text)

                'If iTemoid <> "" Then
                '    sWhere &= " AND i.itemoid IN (" & Left(iTemoid, iTemoid.Length - 1) & ")"
                'End If

                'If iTemoid <> "" Then sWhere &= " AND custoid IN (" & Left(iTemoid, iTemoid.Length - 1) & ")"
                If ddlStatus.SelectedValue <> "ALL" Then
                    sWhere &= "AND jm.trnjualstatus LIKE '%" & ddlStatus.SelectedValue & "%'"
                End If
                sWhere &= IIf(DDLcabang.SelectedValue = "ALL", "", " AND jm.branch_code='" & DDLcabang.SelectedValue & "'")
                sWhere &= IIf(pic.SelectedValue = "ALL", "", " AND jm.spgoid='" & pic.SelectedValue & "'")
                sWhere &= IIf(ddlproduct.SelectedValue = "ALL", "", " AND i.statusitem='" & ddlproduct.SelectedValue & "'")
                Report = New ReportDocument
                Report.Load(Server.MapPath("~\Report\crSalesItem.rpt"))
                namaPDF = "SALESITEM_"

                Dim crConnInfo As New ConnectionInfo
                With crConnInfo
                    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                    .IntegratedSecurity = True
                End With
                setDBLogonForReport(crConnInfo, Report)
                Report.PrintOptions.PaperSize = PaperSize.PaperA4
                Report.SetParameterValue("swhere", sWhere)
                Report.SetParameterValue("aWal", CDate(toDate(dateAwal.Text)))
                Report.SetParameterValue("aKhir", CDate(toDate(dateAkhir.Text)))
            End If

            If tipe = "View" Then
                crvReportForm.ReportSource = Report
                crvReportForm.SeparatePages = True
            Else
                Session("diprint") = "True"
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()

                Try
                    If tipe = "pdf" Then
                        Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                    ElseIf tipe = "excel" Then
                        Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                    End If
                Catch ex As Exception
                    Report.Close() : Report.Dispose()
                End Try
                Response.Redirect("~\ReportForm\rptSalesItem.aspx?awal=true")
            End If

        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message.ToString()
        End Try
    End Sub

    Public Sub bindDataListCustomer()
        sSql = "SELECT 0 selected,s.itemoid,s.itemcode,s.itemdesc FROM QL_mstitem s WHERE s.itemoid IN(SELECT pom.itemoid FROM QL_trnjualdtl pom) AND (s.itemdesc LIKE '%" & Tchar(custname.Text) & "%' OR s.itemcode LIKE '%" & Tchar(custname.Text) & "%')"
        Dim dtSupp As DataTable = kon.ambiltabel(sSql, "QL_mstitem")
        GVCust.DataSource = dtSupp : GVCust.DataBind()
        Session("QL_mstitem") = dtSupp : GVCust.Visible = True
        GVCust.SelectedIndex = -1
        'FillGV(GVCust, sSql, "QL_mstitem")
        'GVCust.Visible = True
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If kon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptSalesItem.aspx") ' di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Sales Penjualan Bersih Item Per Sales"

        If kacabCek("ReportForm/rptSalesItem.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            btnpdf.Visible = True : btnXls.Visible = True
        Else
            If Session("branch_id") <> "10" Then
                btnpdf.Visible = False : btnXls.Visible = False
            Else
                btnpdf.Visible = True : btnXls.Visible = True
            End If
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            initddl() : SalesDDL()
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            rblType_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub DDLcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcabang.SelectedIndexChanged
        SalesDDL()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Report.Close()
        Report.Dispose()
    End Sub

    Protected Sub btnSearchNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNota.Click
        ClearView()
        bindDataListCustomer()
    End Sub

    Protected Sub GVCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVCust.PageIndexChanging
        UpdateCheckedGV()
        GVCust.PageIndex = e.NewPageIndex
        Dim dtSupp As DataTable = Session("QL_mstitem")
        GVCust.DataSource = dtSupp
        GVCust.DataBind()
        'GVCust.PageIndex = e.NewPageIndex
        'bindDataListCustomer()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EraseNota.Click
        ClearView()
        custname.Text = "" : oid.Text = ""
        GVCust.Visible = False
    End Sub

    'Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
    '    custname.Text = GVCust.SelectedDataKey.Item("itemdesc")
    '    oid.Text = GVCust.SelectedDataKey.Item("itemoid")
    '    cProc.DisposeGridView(GVCust)
    '    GVCust.Visible = False
    'End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        UpdateCheckedGV()
        showPrint("View")
    End Sub

    Protected Sub btnPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnpdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub btnXls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnXls.Click
        showPrint("excel")
    End Sub

    Protected Sub btnclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclear.Click
        Response.Redirect("rptSalesItem.aspx?awal=true")
    End Sub

    Protected Sub crvReportForm_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvReportForm.Navigate
        showPrint("View")
    End Sub

    Protected Sub crvReportForm_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs) Handles crvReportForm.Search
        showPrint("View")
    End Sub

    Protected Sub crvReportForm_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs) Handles crvReportForm.ViewZoom
        showPrint("View")
    End Sub
#End Region

    Protected Sub rblType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rblType.SelectedValue = "sales" Then
            sPic.Text = "Sales"
        Else
            sPic.Text = "PIC"
        End If
        SalesDDL()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub
End Class
