Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions


Partial Class rptstock
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Procedure"

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub initddl()

        FillDDL(person, "select personoid, personname from ql_mstperson where status = 'Aktif' AND cmpcode = '" & CompnyCode & "' AND PERSONPOST = (SELECT genoid FROM QL_mstgen WHERE gengroup='JOBPOSITION' AND gendesc = 'SALES PERSON')")
        person.Items.Add(New ListItem("ALL", "ALL"))
        person.SelectedValue = "ALL"

        sSql = "select distinct periodacctg,periodacctg from ql_crdmtr c order by c.periodacctg desc"
        FillDDL(period, sSql)

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' ORDER BY a.gendesc"
        FillDDL(ddlLocation, sSql)
        ddlLocation.Items.Add(New ListItem("SEMUA LOKASI", "ALL LOCATION"))
        ddlLocation.SelectedValue = "ALL LOCATION"

        '---------------------------------------------------------------------------------------
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP' AND cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("SEMUA GRUP", "ALL GRUP"))
        FilterDDLGrup.SelectedValue = "ALL GRUP"

        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP' AND cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("SEMUA SUBGRUP", "ALL SUB GRUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP"

    End Sub

    Public Sub showPrint(ByVal showtype As String)
        Dim sWhere As String = "  "
        lblkonfirmasi.Text = ""

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If

        Try
            Dim namaPDF As String = ""
            Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
            Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  itemoid=" & itemoid.Text, "  ")
            Dim swheregrup As String = IIf(FilterDDLGrup.SelectedValue = "ALL GRUP", "  ", "  and  itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            Dim swheresubgrup As String = IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP", "  ", "  and  itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")

            Dim swhereawal2 As String = "   "
            If type.SelectedValue = "Detail" Then
                swhereawal2 = " Where cmpcode = '" & CompnyCode & "' "
            End If


            vReport = New ReportDocument
            If type.SelectedValue = "Summary" Then
                'vReport.Load(Server.MapPath("~\Report\rptStockSum.rpt"))

                'vReport.SetParameterValue("swherecommand", swherecommand & " " & swhereloc & " " & swhereitem & " " & swheregrup & " " & swheresubgrup & " ")
                'namaPDF = "StockSum_"
            Else
                vReport.Load(Server.MapPath("~\Report\rptHistoryItem.rpt"))
                'vReport.SetParameterValue("filterqty", 0)
                vReport.SetParameterValue("sWhere", swhereawal2 & " " & swhereloc & " " & swhereitem & " " & swheregrup & " " & swheresubgrup & " ")
                vReport.SetParameterValue("periode1", CDate(toDate(dateAwal.Text)))
                vReport.SetParameterValue("periode2", CDate(toDate(dateAkhir.Text)))
                vReport.SetParameterValue("tperiode1", ((dateAwal.Text)))
                vReport.SetParameterValue("tperiode2", ((dateAkhir.Text)))

                namaPDF = "HistoryPriceList_"
            End If


            'vReport.SetParameterValue("swhereawal1", swhereawal1 & " " & swhereloc & " " & swhereitem & " " & swheregrup & " " & swheresubgrup & " ")


            vReport.PrintOptions.PaperSize = PaperSize.PaperA4
            'Dim paperMargin As CrystalDecisions.Shared.PageMargins
            '' 1 centimeter = 567 twips
            '' 1 inch = 1440 twips
            'paperMargin.leftMargin = 1 * 567
            'paperMargin.rightMargin = 1 * 567
            'paperMargin.topMargin = 1 * 567
            'paperMargin.bottomMargin = 1 * 567
            'vReport.PrintOptions.ApplyPageMargins(paperMargin)

            'vReport.SetParameterValue("CompanyName", System.Configuration.ConfigurationManager.AppSettings("CompanyName"))

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'crvMutasiStock.DisplayGroupTree = False
            'crvMutasiStock.ReportSource = vReport



            If showtype = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf showtype = "view" Then
                Session("diprint") = "True"
                crv.DisplayGroupTree = False
                crv.ReportSource = vReport
            End If

        Catch ex As Exception

            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))

        End Try
    End Sub

    Public Sub showPrintExcel(ByVal name As String)
        Dim sWhere As String = ""
        lblkonfirmasi.Text = ""
        'Session("diprint") = "False"

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Response.Clear()

        Dim sSql As String = ""


        Response.AddHeader("content-disposition", "inline;filename=stock.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
        Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ")
        Dim swheregrup As String = IIf(FilterDDLGrup.SelectedValue = "ALL GRUP", "  ", "  and  i.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
        Dim swheresubgrup As String = IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP", "  ", "  and  i.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")

        Dim swhereawal2 As String = "   "
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " c.updtime >='" & Format(CDate(toDate(dateAwal.Text)), "MM") & "/01/" & Format(CDate(toDate(dateAwal.Text)), "yyyy") & " 0:0:0' AND c.updtime <'" & toDate(dateAwal.Text) & " 0:0:0' "
        Else 'summary
            swhereawal2 = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'  "
        End If
        Dim swherecommand As String = ""
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " where c.updtime BETWEEN '" & toDate(dateAwal.Text) & " 0:0:0' AND '" & toDate(dateAkhir.Text) & " 23:59:59'  "
        Else 'summary
            swherecommand = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'   "
        End If


        If type.SelectedValue = "Summary" Then
            If ddlfilterqty.SelectedValue <> "IGNORE" Then
                Select Case ddlfilterqty.SelectedValue
                    Case "<"
                        swherecommand = swherecommand & " and (saldoakhir_1 <   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 <   " & ToDouble(filterqty.Text) & " or saldoakhir_3 <   " & ToDouble(filterqty.Text) & " )"
                    Case "<="
                        swherecommand = swherecommand & " and (saldoakhir_1 <=   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 <=   " & ToDouble(filterqty.Text) & " or saldoakhir_3 <=   " & ToDouble(filterqty.Text) & " )"
                    Case ">"
                        swherecommand = swherecommand & " and (saldoakhir_1 >   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 >   " & ToDouble(filterqty.Text) & " or saldoakhir_3 >   " & ToDouble(filterqty.Text) & " )"
                    Case ">="
                        swherecommand = swherecommand & " and (saldoakhir_1 >=   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 >=   " & ToDouble(filterqty.Text) & " or saldoakhir_3 >=   " & ToDouble(filterqty.Text) & " )"
                End Select
            End If
        Else
            'vReport.RecordSelectionFormula = " {@saldoakhir}<=50 "
        End If


        sSql = " select  g.gendesc lokasi, p.personname SPG,i.itemdesc Nama_barang, g2.gendesc satuan1,saldoawal_1,QTYIN_1,QTYOUT_1,qtyAdjIn_1, " & _
               "qtyadjout_1,saldoakhir_1,g3.gendesc satuan2,saldoawal_2,QTYIN_2,QTYOUT_2,qtyadjin_2,qtyadjout_2, saldoakhir_2," & _
               "g4.gendesc satuan3,saldoawal_3, QTYIN_3,QTYOUT_3,qtyadjin_3, qtyadjout_3, saldoakhir_3,c.updtime " & _
               "from ql_crdmtr c inner join ql_mstitem i on i.itemoid=c.refoid  and c.refname='ql_mstitem' " & _
"inner join ql_mstperson p on i.personoid=p.personoid  " & _
"inner join ql_mstgen g on g.genoid=mtrlocoid  " & _
               "inner join ql_mstgen g2 on g2.genoid=satuan1  " & _
               "inner join ql_mstgen g3 on g3.genoid=satuan2 " & _
               "inner join ql_mstgen g4 on g4.genoid=satuan3  " & IIf(person.SelectedValue <> "ALL", " and  i.personoid=" & person.SelectedValue, "  ") & swherecommand & " " & swhereloc & " " & swhereitem & " " & swheregrup & " " & swheresubgrup & " " & _
               " ORDER BY g.gendesc,i.itemcode "



        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        'Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()

    End Sub

    Public Sub showPrintExcelKonversi(ByVal name As String)
        Dim sWhere As String = ""
        lblkonfirmasi.Text = ""
        'Session("diprint") = "False"

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        Response.Clear()

        Dim sSql As String = ""


        Response.AddHeader("content-disposition", "inline;filename=stock_Konversi.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        Dim swhereloc As String = IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and mtrlocoid=" & ddlLocation.SelectedValue & " ")
        Dim swhereitem As String = IIf(ToDouble(itemoid.Text) > 0, "  and  i.itemoid=" & itemoid.Text, "  ")
        Dim swheregrup As String = IIf(FilterDDLGrup.SelectedValue = "ALL GRUP", "  ", "  and  i.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
        Dim swheresubgrup As String = IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP", "  ", "  and  i.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")

        Dim swhereawal2 As String = "   "
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " c.updtime >='" & Format(CDate(toDate(dateAwal.Text)), "MM") & "/01/" & Format(CDate(toDate(dateAwal.Text)), "yyyy") & " 0:0:0' AND c.updtime <'" & toDate(dateAwal.Text) & " 0:0:0' "
        Else 'summary
            swhereawal2 = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'  "
        End If
        Dim swherecommand As String = ""
        If type.SelectedValue = "Detail" Then
            swhereawal2 = " where c.updtime BETWEEN '" & toDate(dateAwal.Text) & " 0:0:0' AND '" & toDate(dateAkhir.Text) & " 23:59:59'  "
        Else 'summary
            swherecommand = " WHERE c.periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(dateAwal.Text))) & "'   "
        End If

        If type.SelectedValue = "Summary" Then
            If ddlfilterqty.SelectedValue <> "IGNORE" Then
                Select Case ddlfilterqty.SelectedValue
                    Case "<"
                        swherecommand = swherecommand & " and (saldoakhir_1 <   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 <   " & ToDouble(filterqty.Text) & " or saldoakhir_3 <   " & ToDouble(filterqty.Text) & " )"
                    Case "<="
                        swherecommand = swherecommand & " and (saldoakhir_1 <=   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 <=   " & ToDouble(filterqty.Text) & " or saldoakhir_3 <=   " & ToDouble(filterqty.Text) & " )"
                    Case ">"
                        swherecommand = swherecommand & " and (saldoakhir_1 >   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 >   " & ToDouble(filterqty.Text) & " or saldoakhir_3 >   " & ToDouble(filterqty.Text) & " )"
                    Case ">="
                        swherecommand = swherecommand & " and (saldoakhir_1 >=   " & ToDouble(filterqty.Text) & "  or saldoakhir_2 >=   " & ToDouble(filterqty.Text) & " or saldoakhir_3 >=   " & ToDouble(filterqty.Text) & " )"
                End Select
            End If
        Else
            'vReport.RecordSelectionFormula = " {@saldoakhir}<=50 "
        End If


        sSql = " select  g.gendesc lokasi,  p.personname SPG, i.itemdesc Nama_barang,    cast(saldoAkhir_1 as int)  QTY1, g2.gendesc satuan1,    cast(saldoAkhir_2 as int)+((saldoakhir_1-cast(saldoAkhir_1 as int))*i.konversi1_2) QTY2 ,   g3.gendesc satuan2,        cast(saldoakhir_3 as int)+((saldoakhir_2-cast(saldoAkhir_2 as int))*i.konversi2_3)  QTY3,    g4.gendesc satuan3    from ql_crdmtr c inner join ql_mstitem i on i.itemoid=c.refoid  and c.refname='ql_mstitem'   inner join ql_mstperson p on i.personoid=p.personoid  inner join ql_mstgen g on g.genoid=mtrlocoid  inner join ql_mstgen g2 on g2.genoid=satuan1        inner join ql_mstgen g3 on g3.genoid=satuan2  inner join ql_mstgen g4 on g4.genoid=satuan3  " & IIf(person.SelectedValue <> "ALL", " and  i.personoid=" & person.SelectedValue, "  ") & swherecommand & " " & swhereloc & " " & swhereitem & " " & swheregrup & " " & swheresubgrup & " " & _
               " ORDER BY g.gendesc,i.itemcode "



        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        'Response.Write(ConvertDtToTDF(dt))
        conn.Close()

        Response.End()

    End Sub

    Public Sub BindDataListItem()
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc,ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2,ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1,g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, konversi2_3 from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 and itemflag='AKTIF' inner join ql_mstgen g2 on g2.genoid=satuan2 inner join ql_mstgen g3 on g3.genoid=satuan3 WHERE ql_mstitem.cmpcode = '" & CompnyCode & "' AND (ql_mstitem.itemdesc like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.merk like '%" & Tchar(itemname.Text) & "%')"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "listofitem")
        gvItem.DataSource = dtab
        gvItem.DataBind()
        Session("listofitem") = dtab
        gvItem.Visible = True
    End Sub
#End Region

#Region "Event"
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        If Not Session("listofitem") Is Nothing Then
            gvItem.DataSource = Session("listofitem")
        Else
            gvItem.DataSource = Nothing
        End If
        gvItem.DataBind()
        gvItem.PageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crv.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptHistoryPricelist.aspx")
        End If

        Page.Title = CompnyName & " - Laporan History PriceList Item"
        If kacabCek("ReportForm/rptHistoryPricelist.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            ibPDF.Visible = True : ibexcel.Visible = True
        Else
            If Session("branch_id") <> "01" Then
                ibPDF.Visible = False : ibexcel.Visible = False
            Else
                ibPDF.Visible = True : ibexcel.Visible = True

            End If
        End If
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("view")
            End If
        Else
            initddl()
            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = "01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")
            type_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If vReport Is Nothing = False Then
            If vReport.IsLoaded = True Then
                vReport.Dispose()
                vReport.Close()
            End If
        End If
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles type.SelectedIndexChanged
        Session("diprint") = "False"
        If type.SelectedValue = "Summary" Then
            lblfilterqty.Visible = True
            filterqty.Visible = True
            ddlfilterqty.Visible = True
            tdperiod2.Visible = False
            tdPeriod1.Visible = False

            Dim lastPeriodene As String = GetStrData("select top 1 c.periodacctg from QL_crdmtr c where c.closingdate='01/01/1900' order by c.periodacctg")
            dateAwal.Text = "01/" & lastPeriodene.Substring(5).Trim & "/" & lastPeriodene.Substring(0, 4).Trim
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")

        Else ' detail
            lblfilterqty.Visible = False
            filterqty.Visible = False
            ddlfilterqty.Visible = False
            tdperiod2.Visible = True
            tdPeriod1.Visible = True
        End If
    End Sub

    Protected Sub ibkonversi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles export_konversi.Click
        showPrintExcelKonversi("ListofStock")
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        BindDataListItem()
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptHistoryPricelist.aspx?awal=true")
    End Sub

    Protected Sub filterqty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterqty.TextChanged
        filterqty.Text = ToMaskEdit(ToDouble(filterqty.Text), 3)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click

        showPrint("view")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        'showPrintExcel("ListofStock")

        showPrint("excel")
    End Sub

    Protected Sub ibPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibPDF.Click
        showPrint("pdf")
    End Sub
#End Region

End Class
