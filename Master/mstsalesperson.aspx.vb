'Programmer: ^Valz_niK^ | 20.03.2011
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_mstsalesperson
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public PersonImageUrl As String = "~/Images/Photo/"
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Dim CFun As New ClassFunction
    Dim rptReport As New ReportDocument
    Dim folderReport As String = "~/Report/"
    Private mImageFile As Image
    Private mImageFilePath As String
    Dim oMatches As MatchCollection
    Dim tempStatusFlag As String = ""
#End Region

#Region "Procedure"

    Public Sub bindDataPerson(ByVal sqlPlus As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        sSql = "SELECT count(-1) FROM QL_HRIS_JPT.dbo.QL_mstperson where cmpcode='" & CompnyCode & "' "
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar <= 0 Then
            showMessage("Can't Connect to Data Person in Database QL_HRIS_JPT !!", CompnyName & " - WARNING")
            Exit Sub
        End If
        conn.Close()

        sSql = "select personoid ,nip ,personname, cardno noktp, case personstatus when 'Karyawan Tetap' then 'Permanent' else 'Contract' end  [status],personsex ,(select gendesc from ql_hris_jpt.dbo.QL_mstgen where genoid = religionoid ) religion , 0 as amtcontract ,maritalstatus ,curraddr alamattinggal, (select gendesc from ql_hris_jpt.dbo.QL_mstgen where genoid = curraddrcityoid ) citytinggaloid, origaddr alamatasal, (select gendesc from QL_HRIS_JPT.dbo.QL_mstgen where genoid = origaddrcityoid ) asalcityoid,'' pendidikanterakhir,'' asalsekolah, personnote catatanlainlain, joindate tglmasuk,cityofbirth tempatlahir,dateofbirth tgllahir,personphone1  phone1,personphone2 phone2,'' personpicture,'' email from ql_hris_jpt.dbo.QL_mstperson  " & _
                "WHERE cmpcode='" & CompnyCode & "' and  personname not in (select personname from QL_JPT.dbo.QL_MSTPERSON ) " & sqlPlus & "" '& _
        '"AND personpost=" & _
        '"(SELECT genoid FROM QL_mstgen WHERE gengroup = 'JOBPOSITION' AND gendesc = 'Sales') " & sqlPlus

        FillGV(gvPIC, sSql, "QL_mstperson")
    End Sub

    Sub ClearDetail()
        companyname.Text = ""
        divisi.Text = ""
        startperiod.Text = Format(Now, "dd/MM/yyyy")
        endperiod.Text = Format(Now, "dd/MM/yyyy")
        historyseq.Text = "1"
        initdtlcountry() : initdtlprovince() : initdtlcity()
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            historyseq.Text = objTable.Rows.Count + 1
        End If
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstperson.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

#End Region

    Private Sub UpdateCheckedPost()
        If Session("person") IsNot Nothing Then
            Dim dt As DataTable = Session("person")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVmstperson.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVmstperson.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(3).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "personoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("person") = dt
        End If
    End Sub

    Sub UpdateDetail()
        If Not Session("Sales_Area") Is Nothing Then
            Dim iError As Int16 = 0
            Dim objTable As DataTable
            objTable = Session("Sales_Area")
            Try
                For c1 As Integer = 0 To objTable.Rows.Count - 1
                    iError = c1

                    Dim row As System.Web.UI.WebControls.GridViewRow = gvWil.Rows(c1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc2 As System.Web.UI.ControlCollection = row.Cells(1).Controls
                        For Each myControl As System.Web.UI.Control In cc2
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked Then
                                    Session("ITEMAVAILABLE") = "1"
                                Else
                                    Session("ITEMAVAILABLE") = "0"
                                End If
                            End If
                        Next

                        'update to session datatable
                        Dim dr As DataRow = objTable.Rows(c1)
                        dr.BeginEdit()
                        dr(2) = Session("ITEMAVAILABLE")
                        dr.EndEdit()

                    End If
                Next

                Session("PriceDtl") = objTable
                gvWil.DataSource = objTable
                gvWil.DataBind()

            Catch ex As Exception
                showMessage(ex.Message, 1)
            End Try
        End If
    End Sub

    Public Sub BindSalesArea(ByVal iOid As String)
        '        sSql = "select genoid,gendesc salesarea,'false' b from ql_mstgen where gengroup='AREA'"

        sSql = "SELECT genoid,gendesc salesarea, isnull((select available from QL_mstSales_Area Where " & _
               "cmpcode=g.cmpcode and salesoid= " & iOid & " and areaoid=g.genoid),'0') itemavailable " & _
               "FROM QL_mstGen g WHERE gengroup='AREA' " & _
               "and g.cmpcode='" & CompnyCode & "'"
        Dim tbDtl As DataTable = cKon.ambiltabel(sSql, "QL_mstGen")
        If tbDtl.Rows.Count > 0 Then

            'Dim objTable2 As DataTable
            'objTable2 = Session("Sales_Area")
            'objTable2.Rows.Clear() : Dim objRow As DataRow

            'For C1 As Integer = 0 To tbDtl.Rows.Count - 1
            '    objRow = objTable2.NewRow()
            '    objRow("salesarea") = tbDtl.Rows(C1)("salesarea").ToString
            '    objRow("itemavailable") = tbDtl.Rows(C1)("itemavailable").ToString
            '    objTable2.Rows.Add(objRow)
            'Next

            'Session("Sales_Area") = objTable2
            'gvWil.DataSource = objTable2
            'gvWil.DataBind()

            Dim mySqlDA As New SqlDataAdapter(sSql, conn)
            Dim objDs As New DataSet
            mySqlDA.Fill(objDs, "data")
            Session("Sales_Area") = objDs.Tables("data")
            gvWil.DataSource = objDs.Tables("data")
            gvWil.DataBind()
        Else
            Session("Sales_Area") = Nothing
            gvWil.DataSource = Nothing : gvWil.DataBind()
            showMessage("Open Failed !!" & CompnyName & " - ERROR", 1) : Exit Sub
        End If
        UpdateDetail()


        'Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        'Dim objDs As New DataSet
        'mySqlDA.Fill(objDs, "data")
        'Session("Sales_Area") = objDs.Tables("data")
        'gvWil.DataSource = objDs.Tables("data")
        'gvWil.DataBind()

        'FillGV(gvWil, sSql, "ql_salesperson")
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        Image1.ImageUrl = "~/Images/warn.png"
        btnErrOK.ImageUrl = "~/Images/ok.png"
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Private Sub showImage(ByVal imageUrl As String)
        'Image1.Visible = False
        lblMessage.Visible = False
        lblCaption2.Text = "Image Full View"
        Image4.Width = 450
        Image4.Height = 400

        Image4.Visible = True
        Image4.ImageUrl = imageUrl
        CProc.SetModalPopUpExtender(beMsgBox, PanelMsgBox, mpeMsgbox, True)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere, dWhere, oidtemp, sts As String
        sWhere = "" : oidtemp = "" : dWhere = "" : sts = ""

        FilterText.Text = Session("FilterText_mstperson")
        FilterDDL.SelectedIndex = Session("FilterDDL_mstperson")
        If StsDDL.SelectedValue.ToUpper <> "ALL" Then
            sts = "AND p.status='" & StsDDL.SelectedValue & "'"
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sWhere = " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "

        If chkArea.Checked Then
            sWhere &= (IIf(sWhere = "", " WHERE ", " AND ") & " salesarea='" & FilterArea.SelectedValue & "'")
        End If

        If chkPosition.Checked Then
            dWhere &= (IIf(dWhere = "", " AND ", " AND ") & " personstatus='" & FilterPosition.SelectedValue & "'")
        End If

        'disimpan ke dalam session untuk keperluan print
        Session("sWhere_mstperson") = sWhere
        sSql = "SELECT 0 selected,p.personoid, p.personnip, p.personname, p.status FROM QL_mstperson p where p.cmpcode = '" & sCompCode & "' AND p.personoid<>0 AND p." & FilterDDL.SelectedValue.ToString & " LIKE '%" & Tchar(FilterText.Text) & "%' " & dWhere & "" & sts & " ORDER BY p.personoid"
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "person")
        GVmstperson.DataSource = dtab
        GVmstperson.DataBind()
        Session("person") = dtab

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstperson.DataSource = objDs.Tables("data")
        GVmstperson.DataBind()
    End Sub

    Public Sub InitAllDDL()
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='CITY' order by gendesc"
        FillDDL(citytinggaloid, sSql)
        FillDDL(asalcityoid, sSql)
        FillDDL(tempatlahir, sSql)
        FillDDL(sekolahcityoid, sSql)
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='AREA' order by gendesc"
        FillDDL(salesarea, sSql)
        FillDDL(FilterArea, sSql)
        sSql = "select genoid,gendesc from ql_mstgen where gengroup ='JOBPOSITION' order by gendesc"
        FillDDL(position, sSql)
        FillDDL(FilterPosition, sSql)
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='PROVINCE' order by gendesc"
        FillDDL(crtprovince, sSql)
        FillDDL(asalprovince, sSql)
        initdtlcountry() : initdtlprovince() : initdtlcity() : initregion()
    End Sub

    Sub initregion()
        'sSql = "select regionoid,regionname from QL_mstregion_M where cmpcode like '%" & CompnyCode & "%' order by regionname"
        'FillDDL(regionoid, sSql)
    End Sub

    Sub initdtlprovince()
        sSql = "select genoid,gendesc from QL_mstgen where gengroup = 'PROVINCE' and (genother1 = '" & ddlDtlCountry.SelectedValue & "' OR genother1 = '')and cmpcode like '%" & CompnyCode & "%' order by gendesc"
        FillDDL(ddlDtlProvince, sSql)
    End Sub

    Sub initdtlcity()
        sSql = "select genoid,gendesc from QL_mstgen where gengroup = 'CITY' and (genother2 = '" & ddlDtlProvince.SelectedValue & "' or genother2 = '') and (genother1 ='" & ddlDtlCountry.SelectedValue & "' or genother1 ='') and cmpcode like '%" & CompnyCode & "%' order by gendesc"
        FillDDL(ddlDtlCity, sSql)
    End Sub

    Sub initdtlcountry()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='COUNTRY' order by gendesc"
        FillDDL(ddlDtlCountry, sSql)
    End Sub

    Sub initsalesarea()
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='AREA' order by gendesc"
        FillDDL(salesarea, sSql)
    End Sub

    Sub initposition()
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='JOBPOSITION' order by gendesc"
        FillDDL(position, sSql)
    End Sub

    Public Sub FillTextBox(ByVal vPersonoid As String)

        'sSql = "SELECT cmpcode, salesoid, salescode, salesname, salesarea, salessex, maritalstatus, convert(char(10),tglmasuk,103) tglmasuk, divisi, noktp, tempatlahir, convert(char(10),tgllahir,103) tgllahir, alamattinggal, citytinggaloid, alamatasal, " & _
        '"asalcityoid, phone1, phone2, emergencyphone, pendidikanterakhir, asalsekolah, sekolahcityoid, ijazah1, ijazah2, ijazah3, catatanlainlain, status, email, religion, " & _
        '"amtcontract, salespicture, createuser, upduser, updtime, regionoid, personoid, flag FROM QL_salesperson where cmpcode='" & CompnyCode & "' and salesoid = '" & vPersonoid & "'"

        sSql = "SELECT " & _
               "CMPCODE" & _
               ",PERSONOID" & _
               ",PERSONNIP" & _
               ",PERSONIDNO" & _
               ",PERSONNAME" & _
               ",PERSONGENDER" & _
               ",PERSONMRTSTATUS" & _
               ",PERSONRELIGION" & _
               ",PERSONBIRTHPLACE" & _
               ",PERSONBIRTHDATE" & _
               ",PERSONCRTADDRESS" & _
               ",PERSONCRTCITY" & _
               ",PERSONCRTPROVINCE" & _
               ",PERSONCRTPOSTCODE" & _
               ",PERSONORIADDRESS" & _
               ",PERSONORICITY" & _
               ",PERSONORIPROVINCE" & _
               ",PERSONORIPOSTCODE" & _
               ",PERSONPHONE1" & _
               ",PERSONPHONE2" & _
               ",PERSONJOINDATE" & _
               ",PERSONPOST" & _
               ",PERSONDEPARTMENT" & _
               ",PERSONLASTEDU" & _
               ",PERSONLASTSCHOOL" & _
               ",NOTE" & _
               ",PERSONPHOTO" & _
               ",PERSONEMAIL" & _
               ",PERSONSTATUS" & _
               ",CREATEUSER" & _
               ",CREATETIME" & _
               ",UPDUSER" & _
               ",UPDTIME" & _
               ",STATUS" & _
               ",PERSONLASTEDUCITY" & _
               ",POINT " & _
               ",STATUSKARYAWAN " & _
               "FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' AND personoid = '" & vPersonoid & "'"

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader

        While xreader.Read
            salesoid.Text = Trim(xreader("PERSONOID").ToString)
            salescode.Text = Trim(xreader("PERSONNIP").ToString)
            noktp.Text = Trim(xreader("PERSONIDNO").ToString)
            salesname.Text = Trim(xreader("PERSONNAME").ToString)
            salessex.SelectedValue = xreader("PERSONGENDER")
            maritalstatus.SelectedValue = xreader("PERSONMRTSTATUS")
            religion.SelectedValue = xreader("PERSONRELIGION")
            'tempatlahir.SelectedValue = tempatlahir.Items.FindByText(xreader("PERSONBIRTHPLACE").ToString).Value
            tgllahir.Text = Format(xreader("PERSONBIRTHDATE"), "dd/MM/yyyy")
            alamattinggal.Text = Trim(xreader("PERSONCRTADDRESS").ToString)
            citytinggaloid.SelectedValue = xreader("PERSONCRTCITY")
            crtprovince.SelectedValue = xreader("PERSONCRTPROVINCE")
            alamatasal.Text = Trim(xreader("PERSONORIADDRESS").ToString)
            asalcityoid.SelectedValue = xreader("PERSONORICITY")
            asalprovince.SelectedValue = xreader("PERSONORIPROVINCE")
            phone1.Text = Trim(xreader("PERSONPHONE1").ToString)
            phone2.Text = Trim(xreader("PERSONPHONE2").ToString)
            tglmasuk.Text = Format(xreader("PERSONJOINDATE"), "dd/MM/yyyy")
            position.SelectedValue = xreader("PERSONSTATUS")
            pendidikanterakhir.Text = Trim(xreader("PERSONLASTEDU").ToString)
            asalsekolah.Text = Trim(xreader("PERSONLASTSCHOOL").ToString)
            sekolahcityoid.SelectedValue = xreader("PERSONLASTEDUCITY")
            catatanlainlain.Text = Trim(xreader("NOTE").ToString)
            prspicture.ImageUrl = Nothing
            If Trim(xreader("PERSONPHOTO").ToString) <> "" Then
                prspicture.ImageUrl = Trim(xreader("PERSONPHOTO").ToString)
                prspicture.Visible = True
                lkbFullView.Visible = True
            End If
            lblpersonpicture.Text = Trim(xreader("PERSONPHOTO").ToString)
            email.Text = Trim(xreader("PERSONEMAIL").ToString)

            lblstatus.Text = xreader("STATUSKARYAWAN")
            If lblstatus.Text = "Contract" Then
                status.SelectedValue = "Contract"
            Else
                status.SelectedValue = "Permanent"
            End If
            Upduser.Text = Trim(xreader("upduser").ToString)
            Updtime.Text = Format(CDate(xreader("updtime").ToString), "dd/MM/yyyy HH:mm:ss")
            ddlStatus.SelectedValue = xreader("STATUS").ToString
        End While

        xreader.Close()
        conn.Close()

        'Session("TblDtl") = Nothing
        'sSql = "SELECT historyseq,salesoid,compname,position,country countryoid,province provinceoid,city cityoid, g1.gendesc country, g2.gendesc province, g3.gendesc city, convert(char(10),startperiod,103) startperiod, convert(char(10),endperiod, 103) endperiod FROM QL_salespersondtl inner join Ql_mstgen g1 on g1.genoid =  country inner join Ql_mstgen g2 on g2.genoid =  province inner join Ql_mstgen g3 on g3.genoid =  city where salesoid = " & vPersonoid & " order by historyseq"

        'Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sSql, conn)
        'Dim objTable As New DataTable
        'mySqlDAdtl.Fill(objTable)
        'Session("TblDtl") = objTable
        'If objTable.Rows.Count > 0 Then
        '    tbldtl.Visible = True
        '    tbldtl.DataSource = objTable
        '    tbldtl.DataBind()
        '    historyseq.Text = objTable.Rows.Count + 1
        'End If
    End Sub

    Public Sub GeneratePersonID()
        salesoid.Text = GenerateID("QL_mstperson", CompnyCode)
        salescode.Text = salesoid.Text
    End Sub

    Sub clearItem()
        Upduser.Text = Session("UserID")
        Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
    End Sub

    'Private Sub GenerateNo()
    '    Dim retval As String = ""
    '    Dim sPrefik As String = "S"
    '    'Dim sArea As String = "SELECT SUBSTRING(gencode,1,1) from QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND genoid='" & salesarea.SelectedValue & "' "
    '    Dim sArea As String = salesarea.SelectedItem.Text.Substring(0, 1)
    '    Dim salesPrefix As String = sPrefik & sArea
    '    sSql = "select salescode from QL_salesperson where salescode like '%" & salesPrefix & "%' order by salescode desc "
    '    Dim x As Object = cKon.ambilscalar(sSql)
    '    If x Is Nothing Then
    '        ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
    '        retVal = UCase(salesPrefix) & "001"
    '    Else
    '        If x = "" Then
    '            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
    '            retVal = UCase(salesPrefix) & "001"
    '        Else
    '            ' kode supplier seperti yg diminta ada, tinggal generate angka
    '            Dim angka As Integer = CInt(x.ToString.Substring(3, 3))
    '            angka += 1
    '            retVal = UCase(salesPrefix) & tambahNol(angka)
    '        End If
    '    End If
    '    salescode.Text = retval
    'End Sub

    '#End Region

#Region "Function"
    Private Function generateSalesCode(ByVal salesarea As String) As String
        Dim retVal As String = ""
        Dim sPrefix As String = ""
        If salesarea = "SALES PERSON" Then
            sPrefix = "SL"
        Else
            sPrefix = "AC"
        End If

        'Dim sArea As String = salesarea.Substring(0, 1)
        'Dim nama As String = salesname.substring(0, 1)
        Dim salesPrefix As String = sPrefix
        '& sArea
        'Dim salesPrefix As String = salesname.Substring(0, 3) ' 3 karakter nama supplier
        'SELECT ISNULL(MAX(CAST(RIGHT(trnbelino," & DefaultFormatCounter & ") AS INT)),0) FROM QL_trnbelimst WHERE trnbelino LIKE '" & trnbelino & "%'
        sSql = "select ISNULL(MAX(CAST(REPLACE(personnip,LEFT(personnip, 2),'') AS INT)),0) from QL_mstperson where personnip like '" & salesPrefix & "%'"
        Dim x As Integer = cKon.ambilscalar(sSql)
        If x = Nothing Or x = 0 Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = salesPrefix & "1"
        Else
            ' kode supplier seperti yg diminta ada, tinggal generate angka
            x += 1
            retVal = UCase(salesPrefix) & Format(x, "")
        End If

        Return retVal
    End Function

    Private Function tambahNol(ByVal angka As Integer) As String
        Dim retVal As String = ""
        For i As Integer = 0 To 2 - angka.ToString.Length
            retVal &= "0"
        Next
        retVal &= angka

        Return retVal
    End Function

    Private Function setTabelDetail() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("historyseq", Type.GetType("System.Int32"))
        dt.Columns.Add("salesoid", Type.GetType("System.Int32"))
        dt.Columns.Add("compname", Type.GetType("System.String"))
        dt.Columns.Add("position", Type.GetType("System.String"))
        dt.Columns.Add("countryoid", Type.GetType("System.Int32"))
        dt.Columns.Add("provinceoid", Type.GetType("System.Int32"))
        dt.Columns.Add("cityoid", Type.GetType("System.Int32"))
        dt.Columns.Add("country", Type.GetType("System.String"))
        dt.Columns.Add("province", Type.GetType("System.String"))
        dt.Columns.Add("city", Type.GetType("System.String"))
        dt.Columns.Add("startperiod", Type.GetType("System.String"))
        dt.Columns.Add("endperiod", Type.GetType("System.String"))
        Return dt
    End Function

    Public Function returnAvailable(ByVal sAvailable As String) As Boolean
        If sAvailable = "1" Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 15
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            'Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session.Remove("oid_mstperson")
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstsalesperson.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid_mstperson") = Request.QueryString("oid")
        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
        Page.Title = CompnyName & " - Master Sales Person"

        If Not Page.IsPostBack Then
            MultiView1.ActiveViewIndex = 0
            'BindSalesArea()
            BindData(CompnyCode)
            InitAllDDL()
            'generateSalesCode(salesarea.SelectedItem.Text.Trim)
            'salescode.Text = generateSalesCode(salesarea.SelectedItem.Text.Trim)
            'Dim sPrefix As String = "S"
            'Dim area As String = salesarea.SelectedItem.Text.Substring(0, 1)
            'Dim salesPrefix As String = sPrefix & area
            'MsgBox(salesPrefix)
            startperiod.Text = Format(Now, "dd/MM/yyyy")
            endperiod.Text = Format(Now, "dd/MM/yyyy")
            tglmasuk.Text = Format(Now, "dd/MM/yyyy")
            tgllahir.Text = Format(Now, "dd/MM/yyyy")
            If Session("oid_mstperson") IsNot Nothing And Session("oid_mstperson") <> "" Then
                BtnDelete.Visible = True
                PanelUpdate.Visible = True
                FillTextBox(Session("oid_mstperson"))
                'FillTextBox2()
                TabContainer1.ActiveTabIndex = 1
                'BindSalesArea(Session("oid_mstperson"))
            Else
                GeneratePersonID()
                'GenerateNo()
                BtnDelete.Visible = False
                PanelUpdate.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0
                'BindSalesArea("0")
            End If
        End If

        'Dim dtlTbl As DataTable : dtlTbl = Session("TblDtl")
        'tbldtl.DataSource = dtlTbl : tbldtl.DataBind() : tbldtl.Visible = True
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText_mstperson") = FilterText.Text
        Session("FilterDDL_mstperson") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session.Remove("FilterText_mstperson")
        Session.Remove("FilterDDL_mstperson")
        FilterText.Visible = True ': chkArea.Checked = False : chkPosition.Checked = False
        'FilterArea.SelectedIndex = 0
        'FilterPosition.SelectedIndex = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstsalesperson.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        If salesoid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill Person ID"))
            Exit Sub
        End If

        'cek relationship database to table
        Dim sColomnName() As String = {"personoid", "salesoid"}
        Dim sTable() As String = {"ql_mstitem", "ql_trnordermst"}

        If CheckDataExists(salesoid.Text, sColomnName, sTable) = True Then
            showMessage("You cannot delete this data, cause this data used by another table!!!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim imageUrl As String = GetStrData("select PERSONPHOTO from QL_mstperson where personoid=" & salesoid.Text)

        If DeleteData("QL_mstperson", "personoid", salesoid.Text, CompnyCode) = True Then
            If File.Exists(Server.MapPath(imageUrl)) Then
                File.Delete(Server.MapPath(imageUrl))
            End If
            GeneratePersonID()
            clearItem()
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data has been deleted!"))
        Else
            Exit Sub
        End If
        Session.Remove("oid_mstperson")
        Session.Remove("uploadImage_mstperson")
        Response.Redirect("~\Master\mstsalesperson.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        'If Session("oid_mstperson") = Nothing Or Session("oid_mstperson") = "" Then
        '    sSql = "SELECT COUNT(-1) FROM QL_salesperson WHERE cmpcode='" & CompnyCode & "' AND salesoid='" & salesoid.Text & "' "
        '    If cKon.ambilscalar(sSql) > 0 Then
        '        'GenerateNo()
        '    End If
        'End If

        Dim errMessage As String = ""
        'Dim salespersonmstoid As Int32 = GenerateID("QL_salesperson", CompnyCode)
        'Dim salespersondtloid As Int32 = GenerateID("QL_salespersondtl", CompnyCode)

        'personoid
        If personoid.Text = "" Then
            personoid.Text = "0"
        End If

        'generateHistorySeq()
        'If salesoid.Text = "" Then
        '    errMessage &= "Please Fill Person ID!<BR>"
        'End If
        If salescode.Text.Trim = "" Then
            errMessage &= "Sales Code tidak boleh kosong!<BR>"
        End If
        If salesname.Text.Trim = "" Then
            errMessage &= "Sales Name tidak boleh kosong!<BR>"
        End If
        If alamattinggal.Text.Trim = "" Then
            errMessage &= "Current Address tidak boleh kosong!<BR>"
        End If
        If phone1.Text.Trim = "" Then
            errMessage &= "Phone 1 tidak boleh kosong!<BR>"
        End If
        If Tchar(catatanlainlain.Text.Length) > 50 Then
            errMessage &= "Maksimum karakter untuk Note adalah 50!<BR>"
        End If

        Dim ConvertedDate1 As Date
        'Dim st1 As Boolean
        'Try
        '    ConvertedDate1 = CDate(tglmasuk.Text.Substring(3, 2) + "-" + tglmasuk.Text.Remove(2) + "-" + tglmasuk.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
        '    st1 = True
        'Catch ex As Exception
        '    st1 = False
        'End Try
        'If Not st1 Then
        '    errMessage &= "Invalid Join Date!<BR>"
        'End If

        If Not DateTime.TryParseExact(tglmasuk.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, ConvertedDate1) Then
            errMessage &= "Join Date tidak valid!<BR>"
        End If

        If ConvertedDate1.Year < 1900 Then
            errMessage &= "Tahun untuk Join Date tidak boleh kurang dari 1900!<br />"
        End If

        'If CheckYearIsValid(toDate(tglmasuk.Text)) Then
        '    showMessage("Tahun untuk Join Date tidak boleh kurang dari 2000 dan tidak boleh lebih dari 2072 <br />", CompnyName & " - WARNING")
        '    Exit Sub
        'End If


        Dim ConvertedDate2 As Date
        'Dim st2 As Boolean
        'Try
        '    ConvertedDate2 = CDate(tgllahir.Text.Substring(3, 2) + "-" + tgllahir.Text.Remove(2) + "-" + tgllahir.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
        '    st2 = True
        'Catch ex As Exception
        '    st2 = False
        'End Try
        'If Not st2 Then
        '    errMessage &= "Invalid Date of Birth!<BR>"
        'End If

        If Not DateTime.TryParseExact(tgllahir.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, ConvertedDate2) Then
            errMessage &= "Date of Birth tidak valid!<BR>"
        End If

        If ConvertedDate2.Year < 1900 Then
            errMessage &= "Tahun untuk Date of Birth tidak boleh kurang dari 1900!<br />"
        End If

        'Dim ConvertedDate3 As Date
        'Dim st3 As Boolean
        'Try
        '    ConvertedDate3 = CDate(startperiod.Text.Substring(3, 2) + "-" + startperiod.Text.Remove(2) + "-" + startperiod.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
        '    st3 = True
        'Catch ex As Exception
        '    st3 = False
        'End Try
        'If Not st3 Then
        '    errMessage &= "Invalid Start Period!<BR>"
        'End If

        'If Not DateTime.TryParseExact(startperiod.Text.Trim, "dd/MM/yyyy HH:mm", Nothing, Nothing, ConvertedDate3) Then
        '    errMessage &= "Start Period tidak valid!<BR>"
        'End If

        'Dim ConvertedDate4 As Date
        'Dim st4 As Boolean
        'Try
        '    ConvertedDate4 = CDate(endperiod.Text.Substring(3, 2) + "-" + endperiod.Text.Remove(2) + "-" + endperiod.Text.Remove(0, 6)).ToString("MM/dd/yyyy")
        '    st4 = True
        'Catch ex As Exception
        '    st4 = False
        'End Try
        'If Not st4 Then
        '    errMessage &= "Invalid End Period!<BR>"
        'End If

        'If Not DateTime.TryParseExact(endperiod.Text.Trim, "dd/MM/yyyy HH:mm", Nothing, Nothing, ConvertedDate4) Then
        '    errMessage &= "End Period tidak valid!<BR>"
        'End If

        'cek nip [QL_mstperson] yang kembar (nip tidak boleh kembar)
        Dim sSqlCheck As String = "SELECT COUNT(-1) FROM QL_mstperson WHERE PERSONNIP = '" & Tchar(salescode.Text) & "'"
        If Session("oid_mstperson") IsNot Nothing And Session("oid_mstperson") <> "" Then
            sSqlCheck &= " AND personoid <> " & salesoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck) > 0 Then
            errMessage &= "Sales Code telah digunakan!<BR>"
        End If

        If email.Text <> "" Then
            oMatches = Regex.Matches(email.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                errMessage &= "Email tidak valid, Contoh : mail@sample.com !!<BR>"
            End If
        End If

        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            If amtcontract.Text = "" Then
                amtcontract.Text = 0
            End If

            'mode insert
            If Session("oid_mstperson") Is Nothing Or Session("oid_mstperson") = "" Then
                GeneratePersonID()

                Dim saveImageUrl As String = ""
                If Session("uploadImage_mstperson") IsNot Nothing Then
                    saveImageUrl = PersonImageUrl & "sls" & salesoid.Text & "." & Session("uploadImage_mstperson")

                    If File.Exists(Server.MapPath(saveImageUrl)) Then
                        File.Delete(Server.MapPath(saveImageUrl))
                    End If
                    File.Copy(Server.MapPath(PersonImageUrl & "temp." & Session("uploadImage_mstperson")), Server.MapPath(saveImageUrl))
                Else
                    saveImageUrl = lblpersonpicture.Text
                End If

                If position.SelectedItem.Text = "SALES PERSON" Then
                    salescode.Text = generateSalesCode("SALES PERSON")
                Else
                    salescode.Text = generateSalesCode("ACCTG")
                End If

                sSql = "INSERT into QL_mstperson(" & _
                "CMPCODE" & _
                ",PERSONOID" & _
                ",PERSONNIP" & _
                ",PERSONIDNO" & _
                ",PERSONNAME" & _
                ",PERSONGENDER" & _
                ",PERSONMRTSTATUS" & _
                ",PERSONRELIGION" & _
                ",PERSONBIRTHPLACE" & _
                ",PERSONBIRTHDATE" & _
                ",PERSONCRTADDRESS" & _
                ",PERSONCRTCITY" & _
                ",PERSONCRTPROVINCE" & _
                ",PERSONCRTPOSTCODE" & _
                ",PERSONORIADDRESS" & _
                ",PERSONORICITY" & _
                ",PERSONORIPROVINCE" & _
                ",PERSONORIPOSTCODE" & _
                ",PERSONPHONE1" & _
                ",PERSONPHONE2" & _
                ",PERSONJOINDATE" & _
                ",PERSONPOST" & _
                ",PERSONDEPARTMENT" & _
                ",PERSONLASTEDU" & _
                ",PERSONLASTSCHOOL" & _
                ",NOTE" & _
                ",PERSONPHOTO" & _
                ",PERSONEMAIL" & _
                ",PERSONSTATUS" & _
                ",CREATEUSER" & _
                ",CREATETIME" & _
                ",UPDUSER" & _
                ",UPDTIME" & _
                ",STATUS" & _
                ",STATUSKARYAWAN" & _
                ",PERSONLASTEDUCITY" & _
                ",POINT) " & _
                "VALUES (" & _
                "'" & CompnyCode & "'," & _
                "" & salesoid.Text & "," & _
                "'" & Tchar(salescode.Text) & "'," & _
                "'" & Tchar(noktp.Text) & "'," & _
                "'" & Tchar(salesname.Text) & "'," & _
                "'" & Tchar(salessex.SelectedValue.ToString) & "'," & _
                "'" & Tchar(maritalstatus.SelectedValue.ToString) & "'," & _
                "'" & Tchar(religion.SelectedValue.ToString) & "'," & _
                "'" & Tchar(tempatlahir.SelectedItem.ToString) & "'," & _
                "'" & ConvertedDate2 & "'," & _
                "'" & Tchar(alamattinggal.Text) & "'," & _
                "" & citytinggaloid.SelectedValue & "," & _
                "" & crtprovince.SelectedValue & "," & _
                "''," & _
                "'" & Tchar(alamatasal.Text) & "'," & _
                "" & asalcityoid.SelectedValue & "," & _
                "" & asalprovince.SelectedValue & "," & _
                "''," & _
                "'" & Tchar(phone1.Text) & "'," & _
                "'" & Tchar(phone2.Text) & "'," & _
                "'" & ConvertedDate1 & "'," & _
                "" & position.SelectedValue & "," & _
                "0," & _
                "'" & Tchar(pendidikanterakhir.Text) & "'," & _
                "'" & Tchar(asalsekolah.Text) & "'," & _
                "'" & Tchar(catatanlainlain.Text) & "'," & _
                "'" & saveImageUrl & "'," & _
                "'" & Tchar(email.Text) & "'," & _
                "'" & Tchar(position.SelectedValue.ToString) & "'," & _
                "'" & Session("UserID") & "'," & _
                "current_timestamp, " & _
                "'" & Session("UserID") & "'," & _
                "current_timestamp, " & _
                "'" & ddlStatus.SelectedValue.ToString & "'," & _
                "'" & status.SelectedValue.ToString & "'," & _
                " " & sekolahcityoid.SelectedValue & "," & _
                "0)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not (Session("Sales_Area") Is Nothing) Then
                    Dim objTable As DataTable : objTable = Session("Sales_Area")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstSales_Area(cmpcode,salesoid,areaoid,available) " & _
                               "VALUES ('" & CompnyCode & "'," & salesoid.Text & ", " & _
                               "" & objTable.Rows(C1)("genoid").ToString & ", " & _
                               "'" & Tchar(objTable.Rows(C1)("itemavailable").ToString) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                End If

            Else 
                Dim saveImageUrl As String = ""
                If Session("uploadImage_mstperson") IsNot Nothing Then
                    saveImageUrl = PersonImageUrl & "sls" & salesoid.Text & "." & Session("uploadImage_mstperson")

                    If File.Exists(Server.MapPath(saveImageUrl)) Then
                        File.Delete(Server.MapPath(saveImageUrl))
                    End If
                    File.Copy(Server.MapPath(PersonImageUrl & "temp." & Session("uploadImage_mstperson")), Server.MapPath(saveImageUrl))
                Else
                    saveImageUrl = lblpersonpicture.Text
                End If

                sSql = "UPDATE QL_mstperson SET " & _
                "PERSONNIP = '" & Tchar(salescode.Text) & "'" & _
                ",PERSONIDNO = '" & Tchar(noktp.Text) & "'" & _
                ",PERSONNAME = '" & Tchar(salesname.Text) & "'" & _
                ",PERSONGENDER = '" & Tchar(salessex.SelectedValue.ToString) & "'" & _
                ",PERSONMRTSTATUS = '" & Tchar(maritalstatus.SelectedValue.ToString) & "'" & _
                ",PERSONRELIGION = '" & Tchar(religion.SelectedValue.ToString) & "'" & _
                ",PERSONBIRTHPLACE = '" & Tchar(tempatlahir.SelectedItem.ToString) & "'" & _
                ",PERSONBIRTHDATE = '" & ConvertedDate2 & "'" & _
                ",PERSONCRTADDRESS = '" & Tchar(alamattinggal.Text) & "'" & _
                ",PERSONCRTCITY = " & citytinggaloid.SelectedValue & "" & _
                ",PERSONCRTPROVINCE = " & crtprovince.SelectedValue & "" & _
                ",PERSONCRTPOSTCODE = ''" & _
                ",PERSONORIADDRESS = '" & Tchar(alamatasal.Text) & "'" & _
                ",PERSONORICITY = " & asalcityoid.SelectedValue & "" & _
                ",PERSONORIPROVINCE = " & asalprovince.SelectedValue & "" & _
                ",PERSONORIPOSTCODE = ''" & _
                ",PERSONPHONE1 = '" & Tchar(phone1.Text) & "'" & _
                ",PERSONPHONE2 = '" & Tchar(phone2.Text) & "'" & _
                ",PERSONJOINDATE = '" & ConvertedDate1 & "'" & _
                ",PERSONPOST = " & position.SelectedValue & "" & _
                ",PERSONDEPARTMENT = 0" & _
                ",PERSONLASTEDU = '" & Tchar(pendidikanterakhir.Text) & "'" & _
                ",PERSONLASTSCHOOL = '" & Tchar(asalsekolah.Text) & "'" & _
                ",NOTE = '" & Tchar(catatanlainlain.Text) & "'" & _
                ",PERSONPHOTO = '" & saveImageUrl & "'" & _
                ",PERSONEMAIL = '" & Tchar(email.Text) & "'" & _
                ",PERSONSTATUS = '" & position.SelectedValue & "'" & _
                ",UPDUSER = '" & Session("UserID") & "'" & _
                ",UPDTIME = current_timestamp" & _
                ",STATUS = '" & ddlStatus.SelectedValue.ToString & "'" & _
                ",PERSONLASTEDUCITY = " & sekolahcityoid.SelectedValue & "" & _
                ",POINT = 0" & _
                ", STATUSKARYAWAN='" & status.SelectedValue & "'" & _
                " WHERE cmpcode = '" & CompnyCode & "' and personoid = " & Session("oid_mstperson")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstprof set statusprof = '" & IIf(ddlStatus.SelectedValue = "AKTIF", "Active", "Inactive") & "' WHERE personnoid = " & salesoid.Text & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "delete QL_mstSales_Area where cmpcode = '" & CompnyCode & "' and salesoid=" & Session("oid_mstperson")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not (Session("Sales_Area") Is Nothing) Then
                    Dim objTable As DataTable : objTable = Session("Sales_Area")
                    For C1 As Integer = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_mstSales_Area(cmpcode,salesoid,areaoid,available) " & _
                               "VALUES ('" & CompnyCode & "'," & Session("oid_mstperson") & ", " & _
                               "" & objTable.Rows(C1)("genoid").ToString & ", " & _
                               "'" & Tchar(objTable.Rows(C1)("itemavailable").ToString) & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                End If

            End If

            'jika insert data baru, update mstoid
            If Session("oid_mstperson") Is Nothing Or Session("oid_mstperson") = "" Then
                'generateHistorySeq()
                sSql = "update QL_mstoid set lastoid=" & salesoid.Text & " where tablename = 'QL_mstperson' and cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update ql_mstoid set lastoid=" & salesoid.Text & " where tablename = 'QL_mstsales_area' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - WARNING")
            Exit Sub
        End Try
        'generateHistorySeq()
        Session.Remove("oid_mstperson")
        Session.Remove("uploadImage_mstperson")
        Response.Redirect("~\Master\mstsalesperson.aspx?awal=true")
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        Try
            Dim imgext, imgname As String
            Dim allow As Boolean = False

            imgname = matpictureloc.FileName
            imgext = imgname.Substring(imgname.LastIndexOf(".") + 1)
            If imgname = "" Then
                showMessage("Please choose picture first !!", CompnyName & " - WARNING")
                Exit Sub
            Else
                Dim arr() As String = {"JPG", "BMP"}
                Dim i As Integer = 0
                For Each ci As String In imgname.Split(".")
                    For cii As Integer = 0 To arr.Length - 1
                        If arr(cii) = ci.ToUpper() Then
                            allow = True
                        End If
                    Next
                Next
            End If
            If (allow = True) Then
                Session("uploadImage_mstperson") = imgext
                matpictureloc.SaveAs(Server.MapPath(PersonImageUrl & "temp." & Session("uploadImage_mstperson")))
                prspicture.Visible = True
                lkbFullView.Visible = True
                prspicture.ImageUrl = PersonImageUrl & "temp." & Session("uploadImage_mstperson")
            Else
                showMessage("Picture Extention is not valid format please choose picture with format jpg or bmp", CompnyName & " - WARNING ")
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING")
        End Try

    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
        'PanelValidasi.Width = AutoSizeMode.GrowAndShrink
        'PanelValidasi.Height = AutoSizeMode.GrowAndShrink
    End Sub

    Protected Sub lkbFullView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbFullView.Click
        If Session("uploadImage_mstperson") IsNot Nothing Then
            showImage(PersonImageUrl & "temp." & Session("uploadImage_mstperson"))
        Else
            showImage(lblpersonpicture.Text)
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("", "Data_Sales_Person", ExportFormatType.PortableDocFormat)
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)

        rptReport.Load(Server.MapPath("~/report/reptMstPerson.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim swhere As String = " where " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        rptReport.SetParameterValue("sWhere", swhere)


        CProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))


        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
        'Response.Redirect(Page.AppRelativeVirtualPath.ToString)
    End Sub

    Protected Sub CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateDetail()
    End Sub

    Protected Sub lbv12_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbv21_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub ddlDtlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDtlProvince.SelectedIndexChanged
        initdtlcity()
    End Sub

    Protected Sub ddlDtlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDtlCountry.SelectedIndexChanged
        initdtlprovince()
        initdtlcity()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If (IsDate(toDate(startperiod.Text))) And (IsDate(toDate(endperiod.Text))) Then
            If CDate(toDate(endperiod.Text)) < CDate(toDate(startperiod.Text)) Then
                showMessage("Date end must be larger or equals than start date !!", CompnyName & " - WARNING")
                Exit Sub
            End If
        Else
            showMessage("Invalid Time Period Date !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        If CheckYearIsValid(toDate(startperiod.Text)) Then
            showMessage("Start Period can't Less than 2000 and can't More than 2072 <br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        If CheckYearIsValid(toDate(endperiod.Text)) Then
            showMessage("End Period can't Less than 2000 and can't More than 2072 <br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        If companyname.Text = "" Then
            showMessage("Please Fill Company Name !!", CompnyName & " - WARNING")
            Exit Sub
        End If
        If divisi.Text = "" Then
            showMessage("Please Fill Position !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = setTabelDetail() : Session("TblDtl") = dtlTable
        End If

        Dim objTable As New DataTable : objTable = Session("TblDtl")

        'Cek apa sudah ada item yang sama dalam Tabel Detail
        If I_u2.Text = "New Detail" Then
            Dim dv As DataView = objTable.DefaultView
            'dv.RowFilter = " historyseq=" & historyseq.Text
            '& " and itemoid=" & itemoid.Text
            'If dv.Count > 0 Then
            '    showMessage("This Data has been added before, please check!", CompnyName & " - WARNING")
            '    dv.RowFilter = "" : Exit Sub
            'End If
            dv.RowFilter = ""
            'Else
            '   dv.RowFilter = " matoid=" & matoid.Text & "AND trnpodtlseq<>" & trnpodtlseq.Text
            '& " and itemoid=" & itemoid.Text & " " & _
        End If



        'insert/update to list data
        Dim objRow As DataRow
        If I_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            objRow("historyseq") = objTable.Rows.Count + 1
        Else
            objRow = objTable.Rows(historyseq.Text - 1)
            objRow.BeginEdit()
        End If

        objRow("salesoid") = salesoid.Text
        objRow("compname") = companyname.Text
        objRow("position") = divisi.Text
        'objRow("position") = position.SelectedValue
        objRow("countryoid") = ddlDtlCountry.SelectedValue
        objRow("provinceoid") = ddlDtlProvince.SelectedValue
        objRow("cityoid") = ddlDtlCity.SelectedValue
        objRow("country") = ddlDtlCountry.SelectedItem.Text
        objRow("province") = ddlDtlProvince.SelectedItem.Text
        objRow("city") = ddlDtlCity.SelectedItem.Text
        objRow("startperiod") = startperiod.Text
        objRow("endperiod") = endperiod.Text

        If I_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If


        Session("TblDtl") = objTable
        tbldtl.Visible = True
        tbldtl.DataSource = Nothing
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
        historyseq.Text = objTable.Rows.Count + 1
        ClearDetail()
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDetail()
    End Sub

    Protected Sub tblDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbldtl.SelectedIndexChanged
        historyseq.Text = tbldtl.SelectedDataKey("historyseq")
        'If Session("TblDtl") Is Nothing = False Then

        I_u2.Text = "Update Detail"
        Dim objTable As DataTable
        objTable = Session("TblDtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "historyseq=" & historyseq.Text

        salesoid.Text = tbldtl.SelectedDataKey("salesoid")
        companyname.Text = tbldtl.SelectedDataKey("compname")
        divisi.Text = tbldtl.SelectedDataKey("position")
        ddlDtlCountry.SelectedValue = tbldtl.SelectedDataKey("countryoid")
        ddlDtlProvince.SelectedValue = tbldtl.SelectedDataKey("provinceoid")
        ddlDtlCity.SelectedValue = tbldtl.SelectedDataKey("cityoid")
        startperiod.Text = tbldtl.SelectedDataKey("startperiod")
        endperiod.Text = tbldtl.SelectedDataKey("endperiod")

        'MultiView1.ActiveViewIndex = 1

        'End If

    End Sub

    Protected Sub tblDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles tbldtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))

        'resequence po detail ID + sequenceDtl + hitung total Amount
        'Dim dAmtbeli As Double = 0
        'For C1 As Int16 = 0 To objTable.Rows.Count - 1
        '    Dim dr As DataRow = objTable.Rows(C1)
        '    dr.BeginEdit()
        '    dr(2) = C1 + 1
        '    dr.EndEdit()
        '    dAmtbeli += ToDouble(objTable.Rows(C1).Item(8)) 'amtnettoheader
        'Next

        'amtbeli.Text = ToMaskEdit(dAmtbeli, 2)
        'ReAmount()

        Session("TblDtl") = objTable
        tbldtl.Visible = True
        historyseq.Text = objTable.Rows.Count + 1
        ClearDetail()
        tbldtl.DataSource = objTable
        tbldtl.DataBind()
    End Sub

    'Protected Sub imbSearchPIC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    PanelPIC.Visible = True : ButtonPIC.Visible = True
    '    MPEPIC.Show() : bindDataPIC("")
    'End Sub

    'Protected Sub imbErasePIC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbErasePIC.Click
    '    username.Text = ""
    '    userid.Text = ""
    'End Sub

    Protected Sub salesname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub salesarea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        salescode.Text = generateSalesCode(salesarea.SelectedItem.Text.Trim)
    End Sub

    Protected Sub FilterDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If FilterDDL.SelectedValue = "divisi" Then
        '    DDLFilter.Visible = True : FilterText.Visible = False
        '    sSql = "select genoid, gendesc from QL_mstgen where gengroup='POSITION' order by gendesc"
        '    FillDDL(DDLFilter, sSql)
        'ElseIf FilterDDL.SelectedValue = "salesarea" Then
        '    DDLFilter.Visible = True : FilterText.Visible = False
        '    sSql = "select genoid, gendesc from QL_mstgen where gengroup='AREA' order by gendesc"
        '    FillDDL(DDLFilter, sSql)
        'Else
        '    DDLFilter.Visible = False : FilterText.Visible = True
        'End If
    End Sub

    Protected Sub btnGetPrs_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPIC.Visible = True
        'ButtonPIC.Visible = True
        MPEPIC.Show() : bindDataPerson("")
    End Sub

    Protected Sub imbFindPIC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPIC.Click
        Dim ssql As String = " AND " & DDLFilterPIC.SelectedValue & " LIKE '%" & Tchar(txtFilterPIC.Text) & "%' "
        bindDataPerson(ssql)
        PanelPIC.Visible = True : MPEPIC.Show()
        'ButtonPIC.Visible = True
    End Sub

    Protected Sub imbAllPIC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAllPIC.Click
        Me.DDLFilterPIC.SelectedIndex = 0
        txtFilterPIC.Text = "" : bindDataPerson("")
        PanelPIC.Visible = True : MPEPIC.Show()
        'ButtonPIC.Visible = True 
    End Sub

    Public Sub CheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("person")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("status").ToString) <> "taktif" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstperson.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("person")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("status").ToString) <> "taktif" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstperson.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvPIC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'cek dulu
        'If CheckDataExists(gvPIC.SelectedDataKey("personname").ToString.Trim, "personname", "QL_mstperson") = False Then
        '    showMessage(" " & gvPIC.SelectedDataKey("personname").ToString.Trim & " Have been added In Sales Person !!", CompnyName & " - WARNING")
        '    PanelPIC.Visible = False : ButtonPIC.Visible = False : MPEPIC.Hide()
        '    Exit Sub
        'End If

        '------------------------------------------------------------------------------------------------------
        If CheckDataExists(gvPIC.SelectedDataKey("citytinggaloid").ToString.Trim, "gendesc", "ql_mstgen") = False Then
            showMessage("Can't Select this Data, City " & gvPIC.SelectedDataKey("citytinggaloid").ToString.Trim & " In General not found !!", CompnyName & " - WARNING")
            PanelPIC.Visible = False : MPEPIC.Hide()
            'ButtonPIC.Visible = False
            Exit Sub
        End If
        If CheckDataExists(gvPIC.SelectedDataKey("asalcityoid").ToString.Trim, "gendesc", "ql_mstgen") = False Then
            showMessage("Can't Select this Data, City " & gvPIC.SelectedDataKey("asalcityoid").ToString.Trim & " In General not found !!", CompnyName & " - WARNING")
            PanelPIC.Visible = False : MPEPIC.Hide()
            'ButtonPIC.Visible = False
            Exit Sub
        End If
        If CheckDataExists(gvPIC.SelectedDataKey("tempatlahir").ToString.Trim, "gendesc", "ql_mstgen") = False Then
            showMessage("Can't Select this Data, City " & gvPIC.SelectedDataKey("tempatlahir").ToString.Trim & " In General not found !!", CompnyName & " - WARNING")
            PanelPIC.Visible = False : MPEPIC.Hide()
            'ButtonPIC.Visible = False 
            Exit Sub
        End If

        'personoid.Text = gvPIC.SelectedDataKey("personoid").ToString.Trim
        'salescode.Text = gvPIC.SelectedDataKey("nip").ToString.Trim
        salesname.Text = gvPIC.SelectedDataKey("personname").ToString.Trim
        noktp.Text = gvPIC.SelectedDataKey("noktp").ToString.Trim
        status.SelectedValue = gvPIC.SelectedDataKey("status").ToString.Trim
        salessex.SelectedValue = gvPIC.SelectedDataKey("personsex").ToString.Trim
        religion.SelectedValue = gvPIC.SelectedDataKey("religion").ToString.Trim
        amtcontract.Text = gvPIC.SelectedDataKey("amtcontract").ToString.Trim
        maritalstatus.SelectedValue = gvPIC.SelectedDataKey("maritalstatus").ToString.Trim
        alamattinggal.Text = gvPIC.SelectedDataKey("alamattinggal").ToString.Trim
        '----------------------------------------------------------------------
        
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='CITY' and gendesc = '" & gvPIC.SelectedDataKey("citytinggaloid").ToString.Trim & "' order by gendesc "
        
        FillDDL(citytinggaloid, sSql)
        'citytinggaloid.SelectedValue = gvPIC.SelectedDataKey("citytinggaloid").ToString.Trim
        '----------------------------------------------------------------------
        alamatasal.Text = gvPIC.SelectedDataKey("alamatasal").ToString.Trim
        '----------------------------------------------------------------------

       
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='CITY' and gendesc = '" & gvPIC.SelectedDataKey("asalcityoid").ToString.Trim & "' order by gendesc "
        FillDDL(asalcityoid, sSql)
        'asalcityoid.SelectedValue = gvPIC.SelectedDataKey("asalcityoid").ToString.Trim
        '----------------------------------------------------------------------
        pendidikanterakhir.Text = gvPIC.SelectedDataKey("pendidikanterakhir").ToString.Trim
        asalsekolah.Text = gvPIC.SelectedDataKey("asalsekolah").ToString.Trim
        'sekolahcityoid.SelectedValue = gvPIC.SelectedDataKey("sekolahcityoid").ToString.Trim
        catatanlainlain.Text = gvPIC.SelectedDataKey("catatanlainlain").ToString.Trim
        tglmasuk.Text = toDate(gvPIC.SelectedDataKey("tglmasuk").ToString.Trim)
        '---------------------------------------------------------------------
       
        sSql = "select genoid, gendesc from QL_mstgen where gengroup='CITY' and gendesc = '" & gvPIC.SelectedDataKey("tempatlahir").ToString.Trim & "' order by gendesc "
        FillDDL(tempatlahir, sSql)
        'tempatlahir.SelectedValue = gvPIC.SelectedDataKey("tempatlahir").ToString.Trim
        '---------------------------------------------------------------------
        
        tgllahir.Text = toDate(gvPIC.SelectedDataKey("tgllahir").ToString.Trim)
        phone1.Text = gvPIC.SelectedDataKey("phone1").ToString.Trim
        phone2.Text = gvPIC.SelectedDataKey("phone2").ToString.Trim
        'emergencyphone.Text = gvPIC.SelectedDataKey("emergencyphone").ToString.Trim
        email.Text = gvPIC.SelectedDataKey("email").ToString.Trim
        'ijazah1.Text = gvPIC.SelectedDataKey("ijazah1").ToString.Trim
        'ijazah2.Text = gvPIC.SelectedDataKey("ijazah2").ToString.Trim
        'ijazah3.Text = gvPIC.SelectedDataKey("ijazah3").ToString.Trim
        lblpersonpicture.Text = gvPIC.SelectedDataKey("personpicture").ToString.Trim
        If Trim(gvPIC.SelectedDataKey("personpicture").ToString) <> "" Then
            prspicture.ImageUrl = gvPIC.SelectedDataKey("personpicture").ToString.Trim
            prspicture.Visible = True
            lkbFullView.Visible = True
        End If
        PanelPIC.Visible = False : MPEPIC.Hide()
        'ButtonPIC.Visible = False

        'Session("TblDtl") = Nothing
        'sSql = "SELECT historyseq,personoid salesoid,compname,position,country countryoid,province provinceoid,city cityoid, g1.gendesc country, g2.gendesc province, g3.gendesc city, convert(char(10),startperiod,103) startperiod, convert(char(10),endperiod, 103) endperiod FROM QL_mstpersonhist inner join Ql_mstgen g1 on g1.genoid =  country inner join Ql_mstgen g2 on g2.genoid =  province inner join Ql_mstgen g3 on g3.genoid =  city where personoid = " & personoid.Text & " order by historyseq"

        'Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sSql, conn)
        'Dim objTable As New DataTable
        'mySqlDAdtl.Fill(objTable)
        'Session("TblDtl") = objTable
        'If objTable.Rows.Count > 0 Then
        '    tbldtl.Visible = True
        '    tbldtl.DataSource = objTable
        '    tbldtl.DataBind()
        '    historyseq.Text = objTable.Rows.Count + 1
        'End If
    End Sub

    Protected Sub lblClosePIC_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'bindDataPIC("")
        PanelPIC.Visible = False : MPEPIC.Hide()
        'ButtonPIC.Visible = False
    End Sub

    Protected Sub status_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles status.TextChanged
        If status.SelectedValue = "Tetap" Then
            amtcontract.Text = "0"
            amtcontract.CssClass = "inpTextDisabled"
            amtcontract.Enabled = False
        Else
            amtcontract.CssClass = "inpText"
            amtcontract.Enabled = True

        End If
    End Sub

    Protected Sub GVmstperson_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstperson.PageIndexChanging
        GVmstperson.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMsgBoxOK.Click
        CProc.SetModalPopUpExtender(beMsgBox, PanelMsgBox, mpeMsgbox, False)
    End Sub

    Protected Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub BtnPostSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sErrorku As String = ""
        If Not Session("person") Is Nothing Then
            Dim dt As DataTable = Session("person")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView
                If dv.Count = 0 Then
                    sErrorku &= "Please select person first!"
                End If
                dv.RowFilter = ""
                If sErrorku <> "" Then
                    showMessage(sErrorku, CompnyName & " - WARNING")
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = "selected=1"
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                Dim objTrans As SqlClient.SqlTransaction
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                For i As Integer = 0 To dv.Count - 1
                    Dim dg As String = " personoid=" & dv(i)("personoid")
                    If dv.Count > 0 Then
                        sSql = "update QL_MSTPERSON set STATUS='IN AKTIF' where PERSONOID=" & dv(i)("personoid") & " AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Next

                dv.RowFilter = "" : Session("person") = dt
                objTrans.Commit() : conn.Close()
            End If
        End If

        Response.Redirect("mstsalesperson.aspx?type=awal=true")
    End Sub
#End Region
End Class
