Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_MstSupp
    Inherits System.Web.UI.Page

#Region "Variable"
    Private oRegex As Regex : Dim oMatches As MatchCollection
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim sfax1 As String
    Dim sfax2 As String
    Dim sQuery As String = ""
    Dim sValue As String
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then 'Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then 'Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption : Validasi.Text = message
        PanelValidasi.Visible = True : btnExtenderValidasi.Visible = True : mpeValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal message As String)
        Validasi.Text = message
        PanelValidasi.Visible = True
        btnExtenderValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        sSql = "SELECT DISTINCT s.CMPCODE,s.SUPPOID,s.SUPPCODE,s.SUPPNAME,s.SUPPTYPE,s.SUPPACCTGOID,s.SUPPFLAG,s.SUPPGROUPOID,s.SUPPADDR,s.SUPPCITYOID,s.SUPPPROVOID,s.SUPPCOUNTRYOID,s.SUPPPHONE1,s.SUPPPHONE2,s.SUPPPHONE3,s.SUPPFAX1,s.SUPPFAX2,s.SUPPEMAIL,s.SUPPWEBSITE,s.SUPPNPWP,s.SUPPPOSTCODE,s.UPDUSER,s.UPDTIME ,s.notes ,s.contactperson1 ,s.contactperson2 ,kota.gendesc kota,s.oldsuppcode,pr.gendesc FROM QL_MSTSUPP s inner join QL_mstgen kota on s.suppcityoid = kota.genoid Inner Join QL_mstgen pr ON pr.genoid=s.prefixsupp AND pr.gengroup='PREFIXSUPP' Where s.cmpcode like '%" & CompnyCode & "%'" & sWhere & " and s.SUPPTYPE in ('GROSIR') And SUPPFLAG ='" & Statusddl.SelectedValue.Trim & "' order by s.suppoid desc"
        Dim xTableItem1 As DataTable = ckoneksi.ambiltabel(sSql, "GVSupp")
        GVSupp.DataSource = xTableItem1
        GVSupp.DataBind()
        GVSupp.SelectedIndex = -1
    End Sub

    Sub FilterGV(ByVal filterName As String, ByVal filterAddr As String, ByVal filterStatus As String, ByVal filterCode As String, ByVal filterUser As String)
        With SDSDataView
            .SelectParameters("SuppCode").DefaultValue = "%" & Tchar(filterStatus) & "%"
            .SelectParameters("gendesc").DefaultValue = "%" & Tchar(filterCode) & "%"
            .SelectParameters("SuppName").DefaultValue = "%" & Tchar(filterName) & "%"
            .SelectParameters("SuppFlag").DefaultValue = "%%"
            .SelectParameters("SuppAddr").DefaultValue = "%" & Tchar(filterAddr) & "%"
            .SelectParameters("Suppphone1").DefaultValue = "%" & Tchar(filterAddr) & "%"
            .SelectParameters("Suppfax1").DefaultValue = "%" & Tchar(filterAddr) & "%"
            .SelectParameters("Upduser").DefaultValue = "%" & filterUser & "%"
            .SelectParameters("CmpCode").DefaultValue = CompnyCode
        End With
        GVSupp.DataBind()
    End Sub

    Public Sub GenerateSuppID()
        sSql = "select (lastoid+1) from QL_mstoid where tablename like '%QL_mstsupp%' and cmpcode like '%" & CompnyCode & "%' "
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        suppoid.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Sub initcountry()
        'Country
        sQuery = "select genoid,gendesc from QL_mstgen where (gengroup IN ('COUNTRY')) and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddlCountry, sQuery)
    End Sub

    Sub initprovince()
        'Province
        sQuery = "select genoid,gendesc,gengroup from QL_mstgen where gengroup IN('PROVINCE') and genother1 = '" & ddlCountry.SelectedValue & "' and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddlProvince, sQuery)
    End Sub

    Sub initcity()
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            sQuery = "select genoid,gendesc,gengroup from QL_mstgen where (gengroup IN ('CITY')) and genother2 = '" & ddlProvince.SelectedValue & "' and genother1 = '" & ddlCountry.SelectedValue & "' and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCity, sQuery)
            ddlCity.Items.Add(New ListItem("", ""))
            ddlCity.SelectedValue = ""
        Else
            sQuery = "select genoid,gendesc,gengroup from QL_mstgen where (gengroup IN ('CITY')) and genother2 = '" & ddlProvince.SelectedValue & "' and genother1 = '" & ddlCountry.SelectedValue & "' and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCity, sQuery)
        End If
    End Sub

    Sub initcountryphonecode(ByVal coid As Int32)
        'Country Phone Code
        sQuery = "select isnull(genother3,''),genoid,gengroup from QL_mstgen where (gengroup IN ('COUNTRY')) and cmpcode like '%" & CompnyCode & "%' and gengroup='COUNTRY' and genoid='" & coid & "' "
        xCmd.CommandText = sQuery
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        phoneintcode1.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Sub initcityphonecode(ByVal ctoid As Int32)
        'City Phone Code
        sQuery = "select genother3,genoid,gengroup from QL_mstgen where (gengroup IN('CITY')) and genoid = '" & ctoid & "' and cmpcode like '%" & CompnyCode & "%'"
        xCmd.CommandText = sQuery
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        phonelocalcode1.Text = xCmd.ExecuteScalar.ToString
        conn.Close()
    End Sub

    Sub cekcitylist()
        If ddlCity.Items.Count = 0 Then
            phonelocalcode1.Text = ""
            Exit Sub
        Else
            Try
                initcityphonecode(ddlCity.SelectedValue)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub filltextbox(ByVal xid As String) 
        Dim xdt As New DataTable
        Dim xquery = "select cmpcode,suppoid,suppcode,suppname,supptype,suppacctgoid,suppflag,suppgroupoid,suppaddr,suppcityoid,suppprovoid,suppcountryoid,suppphone1,suppphone2,suppphone3,suppfax1,suppfax2,suppemail,suppwebsite,suppnpwp,supppostcode,notes,contactperson1,contactperson2,phonecontactperson1,phonecontactperson2,createuser,UpdUser,UpdTime,prefixsupp,prefixcp1,prefixcp2,paymenttermoid,coa_retur,createtime from QL_mstsupp where suppoid = '" & xid & "'"
        xdt = ckoneksi.ambiltabel(xquery, "QL_mstsupp")
        Dim xRow() As DataRow
        xRow = xdt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        TabContainer1.ActiveTabIndex = 1

        Dim sUpName As String = ""
        Dim CtcPerson As String = ""

        If xRow.Length > 0 Then
            hfSuppId.Value = xRow(0)("SUPPOID").ToString.Trim
            txtId.Text = xRow(0)("SUPPCODE").ToString.Trim
            txtNPWP.Text = xRow(0)("SUPPNPWP").ToString.Trim
            conper1.Text = xRow(0)("CONTACTPERSON1").ToString.Trim
            prefixsupp.SelectedValue = xRow(0)("PREFIXSUPP").ToString.Trim
            txtNama.Text = xRow(0)("SUPPNAME").ToString '.Trim.Replace("," & prefixsupp.SelectedItem.Text, "")
            ddltitle.SelectedValue = xRow(0)("PREFIXCP1").ToString.Trim
            ddltitle2.SelectedValue = xRow(0)("PREFIXCP2").ToString.Trim
            ddlStatus.SelectedValue = xRow(0)("SUPPFLAG").ToString.Trim
            ddlType.SelectedValue = xRow(0)("SUPPTYPE").ToString.Trim
            ddlGroup.SelectedValue = xRow(0)("SUPPGROUPOID").ToString.Trim
            txtAddress.Text = xRow(0)("SUPPADDR").ToString.Trim
            ddlCountry.SelectedValue = xRow(0)("SUPPCOUNTRYOID").ToString.Trim
            ddlProvince.Items.Clear() : initprovince()
            ddlProvince.SelectedValue = xRow(0)("SUPPPROVOID").ToString.Trim
            ddlCity.Items.Clear() : initcity()
            ddlCity.SelectedValue = xRow(0)("SUPPCITYOID").ToString.Trim
            txtPostCode.Text = xRow(0)("SUPPPOSTCODE").ToString.Trim

            suppemail.Text = xRow(0)("SUPPEMAIL").ToString.Trim
            suppweb.Text = xRow(0)("SUPPWEBSITE").ToString.Trim
            suppnote.Text = xRow(0)("NOTES").ToString.Trim

            phoneconper1.Text = xRow(0)("PHONECONTACTPERSON1").ToString
            phonecontactperson2.Text = xRow(0)("PHONECONTACTPERSON2").ToString
            conper2.Text = xRow(0)("CONTACTPERSON2").ToString
            phone1.Text = xRow(0)("SUPPPHONE1").ToString
            phone2.Text = xRow(0)("SUPPPHONE2").ToString
            phone3.Text = xRow(0)("SUPPPHONE3").ToString
            suppfax1.Text = xRow(0)("SUPPFAX1").ToString
            suppfax2.Text = xRow(0)("SUPPFAX2").ToString
            supppaymenttermdefaultoid.SelectedValue = xRow(0)("paymenttermoid").ToString
            apaccount.SelectedValue = xRow(0)("suppacctgoid").ToString
            returaccount.SelectedValue = xRow(0)("coa_retur").ToString
            CreateTime.Text = Format(xRow(0)("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
            UpdTime.Text = Format(xRow(0)("updtime"), "dd/MM/yyyy HH:mm:ss.fff")
            CreateUser.Text = xRow(0)("createuser").ToString
            UpdUser.Text = xRow(0)("upduser").ToString

            Dim dtShipping As New DataTable
            Dim dtCP As New DataTable
            Dim dtBank As New DataTable
            Dim dtTemp As New DataTable
        End If
        initcountryphonecode(ddlCountry.SelectedValue)
        cekcitylist()
    End Sub

    Private Sub initAllDDL()
        'Type()
        sQuery = "select genoid,gendesc from QL_mstgen where (gengroup IN ('PAYTYPE')) and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(supppaymenttermdefaultoid, sQuery)
        'Group
        sQuery = "select genoid, gendesc from ql_mstgen where (gengroup IN ('SUPPGROUP')) and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddlGroup, sQuery)

        FillDDL(suppcreditlimitcurroid, sQuery)
        sSql = "select genoid,gendesc,gengroup from QL_mstgen where gengroup ='PREFIXSUPP' and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(prefixsupp, sSql)

        sSql = "select genoid,gendesc,gengroup from QL_mstgen where gengroup IN('PREFIXPERSON') and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddltitle, sSql)

        sSql = "select genoid,gendesc,gengroup from QL_mstgen where gengroup IN('PREFIXPERSON') and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddltitle2, sSql)
        initcountry() : initprovince()
        initcity() : initcountryphonecode(ddlCountry.SelectedValue)

        cekcitylist()
        Dim VAR_AP As String = GetVarInterface("VAR_AP", CompnyCode)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_AP & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(apaccount, sSql)

        Dim VAR_RETBELI As String = GetVarInterface("VAR_RETBELI", CompnyCode)
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_RETBELI & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        FillDDL(returaccount, sSql)
    End Sub

    Sub removeList(ByVal sessionname As String, ByVal gvName As GridView, ByVal btnName As ImageButton, ByVal checkCol As Integer)
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session(sessionname)
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For i As Integer = objRow.Length() - 1 To 0 Step -1
                    If ckoneksi.getCheckBoxValue(i, checkCol, gvName) = True Then
                        objTable.Rows.Remove(objRow(i))
                    End If
                Next
                Session(sessionname) = objTable
                gvName.DataSource = Session(sessionname)
                gvName.DataBind()
            End If
            If Session(sessionname).rows.count > 0 Then
                btnName.Visible = True
            Else
                btnName.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message)
        End Try
    End Sub

    Sub addCPList(ByVal btnhiddenName As Button, ByVal modalpopupName As String, ByVal sessionName As String)
        If Session(sessionName) Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("TblDtlCP")
            dtlTable.Columns.Add("CMPCODE", Type.GetType("System.String"))
            dtlTable.Columns.Add("CPOID", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("OID", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("TYPE", Type.GetType("System.String"))
            dtlTable.Columns.Add("CPNAME", Type.GetType("System.String"))
            dtlTable.Columns.Add("CPPHONE", Type.GetType("System.String"))
            dtlTable.Columns.Add("CPEMAIL", Type.GetType("System.String"))
            dtlTable.Columns.Add("UPDUSER", Type.GetType("System.String"))
            dtlTable.Columns.Add("UPDTIME", Type.GetType("System.DateTime"))
            dtlDS.Tables.Add(dtlTable)
            Session(sessionName) = dtlTable
        End If
        Dim objTable As DataTable
        objTable = Session(sessionName)
        Dim objRow As DataRow
        If I_U_CP.Text = "New Detail" Then
            objRow = objTable.NewRow()
        Else 'upddate
            objRow = objTable.Rows(gvCP.SelectedIndex)
            objRow.BeginEdit()
        End If
        objRow("CMPCODE") = CompnyCode
        objRow("CPOID") = GenerateID("QL_ContactPerson", CompnyCode)
        objRow("OID") = hfSuppId.Value
        objRow("TYPE") = "Supplier"
        objRow("CPNAME") = txtCPName.Text
        objRow("CPPHONE") = txtCPMobile.Text
        objRow("CPEMAIL") = txtCPEmail.Text
        objRow("upduser") = Session("UserID")
        objRow("updtime") = FormatDateTime(Now(), DateFormat.GeneralDate)
        If I_U_CP.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else 'update
            objRow.EndEdit()
        End If
        Session(sessionName) = objTable
        If Session(sessionName).rows.count > 0 Then
            imbRemoveCP.Visible = True
        Else
            imbRemoveCP.Visible = False
        End If
        dv = objTable.DefaultView
        dv.RowFilter = ""
        txtCPName.Text = ""
        txtCPMobile.Text = ""
        txtCPEmail.Text = ""
        gvCP.DataSource = dv
        gvCP.DataBind()
        imbRemoveCP.Visible = True
        I_U_CP.Text = "New Detail"
        Panel3.Visible = False
        btnHiddenCP.Visible = False
        mpe2.Hide()
    End Sub

    Sub AddBanklist(ByVal btnhiddenName As Button, ByVal modalpopupName As String, ByVal sessionName As String)
        If Session(sessionName) Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("TblDtlBank")
            dtlTable.Columns.Add("CMPCODE", Type.GetType("System.String"))
            dtlTable.Columns.Add("BANKOID", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("OID", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("TYPE", Type.GetType("System.String"))
            dtlTable.Columns.Add("CURRENCY", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("BANKNAME", Type.GetType("System.String"))
            dtlTable.Columns.Add("BANKADDRESS", Type.GetType("System.String"))
            dtlTable.Columns.Add("BANKACCNAME", Type.GetType("System.String"))
            dtlTable.Columns.Add("BANKACCNO", Type.GetType("System.String"))
            dtlTable.Columns.Add("SWIFTCODE", Type.GetType("System.String"))
        End If
    End Sub

    Private Sub fillFaxPhoneInterField(ByVal faxphonestring As String, ByVal intercode As TextBox)
        Dim sTemp() As String = faxphonestring.Split("-")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                intercode.Text = sTemp(0)
            ElseIf i = 1 Then
                'localcode.Text = sTemp(1)
            ElseIf i = 2 Then
                'phonefaxcode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillFaxPhoneLocalField(ByVal faxphonestring As String, ByVal localcode As TextBox)
        Dim sTemp() As String = faxphonestring.Split("-")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                'intercode.Text = sTemp(0)
            ElseIf i = 1 Then
                localcode.Text = sTemp(1)
            ElseIf i = 2 Then
                'phonefaxcode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillFaxPhoneCodeField(ByVal faxphonestring As String, ByVal phonefaxcode As TextBox)
        Dim sTemp() As String = faxphonestring.Split("-")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                'intercode.Text = sTemp(0)
            ElseIf i = 1 Then
                'localcode.Text = sTemp(1)
            ElseIf i = 2 Then
                phonefaxcode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillFaxField(ByVal faxString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal faxCode As TextBox)
        Dim sTemp() As String = faxString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                faxCode.Text = sTemp(2)
            End If
        Next
    End Sub
#End Region

#Region "Function"
    Private Function generateCustCode(ByVal custname As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = custname.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select suppcode from QL_mstsupp where suppcode like '" & custPrefix & "%' order by suppcode desc "
        Dim x As Object = cKoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tambahNol(angka)
            End If
        End If
        Return retVal
    End Function

    Function tambahNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function
#End Region

#Region "Events"

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\other\login.aspx")
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("MstSupp.aspx")
        End If

        Me.Title = CompnyName & " - Data Supplier"
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data??');")
        If Request.QueryString("oid") = Request.QueryString("oid") Then
            Session("oid") = Request.QueryString("oid")
        End If

        If Not IsPostBack Then
            BindData("") : initAllDDL()

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                CreateTime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                i_u.Text = "New"
                TabContainer1.ActiveTabIndex = 0
                hfSuppId.Value = GenerateID("QL_mstsupp", CompnyCode)
                btnDelete.Visible = False
                UpdTime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                lblUpd.Text = "Last Update"
            Else
                filltextbox(Session("oid"))
                'btnDelete.Visible = True
                i_u.Text = "Update"
                If lblsupp.Text = "Terserah" Then
                    'Response.Redirect("~\Master\MstSupp.aspx?awal=true")
                    lblUpd.Text = "Last Update"
                Else
                    lblUpd.Text = "Last Update"
                End If
                'lblsupp.Text = ""
                'UpdUser.Text = Session("UserID") ': 
            End If
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        sWhere &= " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        BindData(sWhere)
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        FilterText.Text = "" : FilterDDL.SelectedIndex = 0 
        BindData("")
    End Sub

    Protected Sub lbAddShipping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddShipping.Click
        Dim msg As String = ""
        If txtShippingAddr.Text = "" Then
            msg &= "- Isi Alamat !!<br> "
        End If
    End Sub

    Protected Sub GVSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVSupp.PageIndexChanging
        GVSupp.PageIndex = e.NewPageIndex
        Dim sWhere As String = ""
        sWhere &= " AND " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
        BindData(sWhere)
    End Sub

    Protected Sub lbNewShipping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbNewShipping.Click
        btnHiddenShipping.Visible = True
        Panel1.Visible = True : mpe1.Show()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        btnHiddenCP.Visible = True
        Panel3.Visible = True : mpe2.Show()
    End Sub
   
    Protected Sub lbCCShipping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCCShipping.Click
        Panel1.Visible = False : btnHiddenShipping.Visible = False : mpe1.Hide()
        txtShippingAddr.Text = "" : txtShippingPostCode.Text = ""
        ddlShippingCity.SelectedIndex = 0 : ddlShippingProv.SelectedIndex = 0
        ddlShippingCountry.SelectedIndex = 0
    End Sub

    Protected Sub lbCCCP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCCCP.Click
        Panel3.Visible = False : btnHiddenCP.Visible = False : mpe2.Hide()
        txtCPEmail.Text = "" : txtCPMobile.Text = "" : txtCPName.Text = ""
    End Sub

    Protected Sub newDataBAcc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Panel3.Visible = True
        btnHiddenBAcc.Visible = True
        mpe3.Show()
    End Sub

    Protected Sub lbAddBAcc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddBAcc.Click
        Dim msg As String = ""
        If txtBankName.Text = "" Then
            msg &= "- Please fill Bank Name !!<br>"
        End If
        If txtAccName.Text = "" Then
            msg &= "- Please fill Account Name !!<br>"
        End If
        If txtAccNo.Text = "" Then
            msg &= "- Please fill Account No !!<br>"
        End If
        If msg = "" Then

        End If
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvince.SelectedIndexChanged        
        ddlCity.Items.Clear() : initcity()
        cekcitylist()
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        ddlProvince.Items.Clear() : ddlCity.Items.Clear()
        initprovince() : initcity()
        initcountryphonecode(ddlCountry.SelectedItem.Value)
        cekcitylist()
    End Sub

    Protected Sub imbRemoveShipping_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemoveShipping.Click
        removeList("TabelShipping", gvShippingAddr, imbRemoveShipping, 5)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If txtId.Text.Trim = "" Then
            sMsg &= "Isi Code Supplier !<br>"
        End If

        If txtAddress.Text = "" Then
            sMsg &= "Isi Alamat Supplier !<br>"
        End If

        If txtNama.Text.Trim = "" Then
            sMsg &= "Isi Nama Supplier !<br>"
        End If

        If phone1.Text = "" Then
            sMsg &= "Isi Telepon 1 Supplier !<br>"
        End If

        If conper1.Text = "" Then
            sMsg &= "Isi CP 1 Supplier !<br>"
        End If

        If ddlCity.Text = "" Then
            sMsg &= "Isi Nama Kota !<br>"
        End If

        If ddlCountry.Text = "" Then
            sMsg &= "Isi Nama Negara !<br>"
        End If

        If ddlProvince.Text = "" Then
            sMsg &= "Isi Nama Propinsi !<br>"
        End If

        If Tchar(suppnote.Text).Length > 250 Then
            sMsg &= "Maksimal catatan 250 karakter !<br>"
        End If

        If suppemail.Text <> "" Then
            oMatches = Regex.Matches(suppemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sMsg &= "Alamat email tidak Valid !! Ex: mail@sample.com !<br>"
            End If
        End If

        If suppweb.Text <> "" Then
            oMatches = Regex.Matches(suppweb.Text, "([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?")
            If oMatches.Count <= 0 Then
                sMsg &= "Alamat website tidak Valid !! Ex: www.sample.com !<br>"
            End If
        End If
     
        Dim titlename As String = ""
        If prefixsupp.SelectedItem.Text.ToLower = "other" Then
            titlename = ""
        Else
            titlename = "," & prefixsupp.SelectedItem.Text
        End If

        If i_u.Text.ToLower = "new" Then
            Dim xdt1 As New DataTable
            Dim xquery1 = "select cmpcode,suppoid,suppcode,suppname,supptype from QL_mstsupp where cmpcode='" & _
                CompnyCode & "' AND suppname= '" & Tchar(txtNama.Text) & "' and supptype = '" & ddlType.SelectedValue & "'"
            xdt1 = ckoneksi.ambiltabel(xquery1, "QL_mstsupp")
            Dim xRow1() As DataRow
            xRow1 = xdt1.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If xRow1.Length > 0 Then
                sMsg &= "Nama Supplier ada yang Sama pada Type " & ddlType.SelectedValue & " ,Tolong Simpan dengan Nama Lain !!<br />"
            End If
        Else
            Dim xdt1 As New DataTable
            Dim xquery1 = "select cmpcode,suppoid,suppcode,suppname,supptype from QL_mstsupp where cmpcode='" & _
                CompnyCode & "' AND suppname= '" & Tchar(txtNama.Text) & "' and supptype = '" & ddlType.SelectedValue & "' and suppoid <> " & Session("oid")
            xdt1 = ckoneksi.ambiltabel(xquery1, "QL_mstsupp")
            Dim xRow1() As DataRow
            xRow1 = xdt1.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If xRow1.Length > 0 Then
                sMsg &= "Nama Supplier ada yang Sama pada Type " & ddlType.SelectedValue & " ,Tolong Simpan dengan Nama Lain !!<br />"
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_mstsupp WHERE createtime='" & CDate(toDate(CreateTime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                hfSuppId.Value = GenerateID("QL_mstsupp", CompnyCode)
                txtId.Text = generateCustCode(Tchar(txtNama.Text))

                sfax1 = Tchar(faxintcode1.Text) & "." & Tchar(faxlocalcode1.Text) & "." & Tchar(suppfax1.Text)
                sfax2 = Tchar(faxintcode2.Text) & "." & Tchar(faxlocalcode2.Text) & "." & Tchar(suppfax2.Text)

                sSql = "insert into QL_mstsupp (cmpcode,suppoid,suppcode,suppname,supptype,suppacctgoid,suppflag,suppaddr,suppcityoid,suppprovoid,suppcountryoid,suppphone1,suppphone2,suppphone3,suppfax1,suppfax2,suppemail,suppwebsite,suppnpwp,contactperson1,contactperson2,phonecontactperson1,phonecontactperson2,supppostcode,notes,createuser,upduser,updtime,prefixsupp,prefixcp1,prefixcp2,paymenttermoid,coa_retur,createtime) values " & _
                " ('" & CompnyCode & "', " & hfSuppId.Value & ",'" & Tchar(txtId.Text) & "','" & Tchar(txtNama.Text) & "','" & ddlType.SelectedValue & "', '" & apaccount.SelectedValue & "','" & ddlStatus.SelectedValue & "', '" & Tchar(txtAddress.Text) & "', " & ddlCity.SelectedValue & "," & ddlProvince.SelectedValue & ", " & ddlCountry.SelectedValue & ", '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & suppfax1.Text & "','" & suppfax2.Text & "','" & Tchar(suppemail.Text) & "','" & Tchar(suppweb.Text) & "','" & Tchar(txtNPWP.Text.Trim) & "','" & Tchar(conper1.Text) & "','" & Tchar(conper2.Text) & "','" & phoneconper1.Text & "','" & Tchar(phonecontactperson2.Text) & "','" & Tchar(txtPostCode.Text) & "','" & Tchar(suppnote.Text) & "','" & Session("UserID") & "','" & Session("UserID") & "',current_timestamp,'" & prefixsupp.SelectedValue & "','" & ddltitle.SelectedValue & "','" & ddltitle2.SelectedValue & "'," & supppaymenttermdefaultoid.SelectedValue & ", '" & returaccount.SelectedValue & "','" & CDate(toDate(CreateTime.Text)) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & hfSuppId.Value & " where tablename = 'QL_MSTSUPP' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sfax1 = Tchar(faxintcode1.Text) & "." & Tchar(faxlocalcode1.Text) & "." & Tchar(suppfax1.Text)
                sfax2 = Tchar(faxintcode2.Text) & "." & Tchar(faxlocalcode2.Text) & "." & Tchar(suppfax2.Text)
                sSql = "update QL_mstsupp set prefixsupp = '" & prefixsupp.SelectedValue & "', suppcode = '" & Tchar(txtId.Text) & "', suppname = '" & Tchar(txtNama.Text) & "', supptype='" & ddlType.SelectedValue & "',suppacctgoid='" & apaccount.SelectedValue & "', suppflag='" & ddlStatus.SelectedValue & "', suppaddr='" & Tchar(txtAddress.Text) & "', suppcityoid=" & ddlCity.SelectedValue & ", suppprovoid=" & ddlProvince.SelectedValue & ", suppcountryoid=" & ddlCountry.SelectedValue & ", suppphone1 = '" & phone1.Text & "', suppphone2='" & phone2.Text & "', suppphone3='" & phone3.Text & "', suppfax1='" & suppfax1.Text & "', suppfax2 = '" & suppfax2.Text & "', suppemail='" & Tchar(suppemail.Text) & "', suppwebsite='" & Tchar(suppweb.Text) & "', suppnpwp = '" & Tchar(txtNPWP.Text) & "', contactperson1='" & Tchar(conper1.Text) & "', contactperson2 = '" & Tchar(conper2.Text) & "', phonecontactperson1='" & phoneconper1.Text & "',phonecontactperson2='" & phonecontactperson2.Text & "', supppostcode='" & Tchar(txtPostCode.Text) & "', notes = '" & Tchar(suppnote.Text) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp,prefixcp1='" & ddltitle.SelectedValue & "', prefixcp2='" & ddltitle2.SelectedValue & "', paymenttermoid= '" & supppaymenttermdefaultoid.SelectedValue & "', coa_retur=0 Where cmpcode='" & CompnyCode & "' and suppoid=" & hfSuppId.Value & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & "<BR>" & sSql)
            Exit Sub
        End Try

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data tersimpan"))
        Response.Redirect("~\Master\MstSupp.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~/Master/MstSupp.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try
            sSql = "delete QL_mstsupp where cmpcode like '%" & CompnyCode & "' and suppoid = " & Session("oid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close() : GenerateSuppID()

        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString) : Exit Sub
        End Try
        Response.Redirect("~\Master\MstSupp.aspx?awal=true")
    End Sub

    Protected Sub btnErrorOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOk.Click
        PanelValidasi.Visible = False
        btnExtenderValidasi.Visible = False
        mpeValidasi.Hide()
        Validasi.Text = ""
        If lblsupp.Text = "Terserah" Then
            Response.Redirect("~\Master\MstSupp.aspx?awal=true")
        End If
        If lblState.Text = "SA" Then
            lblState.Text = "" : mpe1.Show()
        ElseIf lblState.Text = "CP" Then
            lblState.Text = "" : mpe2.Show()
        ElseIf lblState.Text = "BA" Then
            lblState.Text = "" : mpeValidasi.Show()
        End If
    End Sub

    Protected Sub imbRemoveCP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemoveCP.Click
        removeList("TabelCP", gvCP, imbRemoveCP, 3)
    End Sub

    Protected Sub imbRemoveBAcc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRemoveBAcc.Click
        removeList("TabelBank", gvBAcc, imbRemoveBAcc, 6)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        cekcitylist()

    End Sub

    Protected Sub txtNama_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNama.TextChanged
        If Session("oid") <> "" And Session("oid") <> Nothing Then
        Else
            'HANYA NEW YANG GENERATE CODE
            If txtNama.Text.Trim <> "" Then
                If txtNama.Text.Length < 1 Then
                    showMessage("Nama Supplier Minimal 1 Character", CompnyName & " - WARNING", 2)
                    txtNama.Text = "" : txtId.Text = ""
                    Exit Sub
                Else
                    txtId.Text = generateCustCode(Tchar(txtNama.Text))
                End If
            Else
                txtId.Text = ""
            End If
        End If
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rpt_mstSupplier.rpt"))
        Dim sWhere As String = ""
        Dim sFilter As String = ""
        sWhere = " where A.cmpcode LIKE '%" & CompnyCode & "%' and A.suppflag = '" & ddlStatus.SelectedValue & "' "
        If FilterText.Text.Trim <> "" Then
            If FilterDDL.SelectedItem.Text.ToUpper = "Code" Or FilterDDL.SelectedItem.Text.ToUpper = "Nama" Or FilterDDL.SelectedItem.Text.ToUpper = "Group" Or FilterDDL.SelectedItem.Text.ToUpper = "Kota" Then
                sWhere &= "  and  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere &= "  and  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        End If
        rptReport.SetParameterValue("sWhere", sWhere)
        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = ""

        If FilterText.Text.Trim <> "" Then
            If FilterDDL.SelectedItem.Text.ToUpper = "Code" Or FilterDDL.SelectedItem.Text.ToUpper = "Nama" Or FilterDDL.SelectedItem.Text.ToUpper = "Group" Then
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        End If
        Response.Clear()
        If printType = "SUPPLIER" Then
            Response.AddHeader("content-disposition", "inline;filename=mstsupp.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"
            sSql = "Select A.suppoid [No. Id],A.suppcode [Kode],A.suppname [Supp. Name],A.suppaddr [Alamat],B.gendesc [Kota],A.suppphone1 [Telp],A.supptype [Type],A.suppflag [Status],A.contactperson1 [Contact Person1],A.contactperson2 [Contact Person2],A.notes [Catatan],ter.gendesc Termin From QL_mstsupp A inner join QL_mstgen B on B.genoid = A.suppcityoid Inner Join QL_mstgen ter ON ter.genoid=a.paymenttermoid AND ter.gengroup='PAYTYPE' " & sWhere & " Order by A.suppname"
        End If

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd : da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0) : Response.Write(ConvertDtToTDF(dt))
        conn.Close() : Response.End()
        mpePrint.Show()

    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        printType.Text = "SUPPLIER" : imbPrintPDF.Visible = True
        suppNoForReport.Text = "" : suppIDForReport.Text = ""
        Session("NoSupp") = "" : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "Supplier_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, suppIDForReport.Text, suppNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppIDForReport.Text = "" : suppNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub
End Class
