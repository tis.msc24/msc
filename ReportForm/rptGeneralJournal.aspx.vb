'Prgmr:Bhie Nduuutt | LastUpdt:21.05.2013
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassFunction
Partial Class ReportForm_rptGeneralJournal
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public compnyname As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim vReport As New ReportDocument
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
#End Region

#Region "Functions"
    Private Function ValidateInput() As String ' Return empty string if Success
        Dim sReturn As String = ""

        Dim st1, st2 As Boolean
        Try
            Dim dt As Date = CDate(toDate(date1.Text)) : st1 = True
        Catch ex As Exception
            sReturn &= "- Incorrect format Periode date 1 !!<BR>" : st1 = False
        End Try
        Try
            Dim dt As Date = CDate(toDate(date2.Text)) : st2 = True
        Catch ex As Exception
            sReturn &= "- Incorrect format periode date 2 !!<BR>" : st2 = False
        End Try
        If st1 And st2 Then
            If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
                sReturn &= "- Periode date 2 must >= date 1 !!<BR>" : st2 = False
            End If
        End If

        If rblAccount.SelectedValue = "Beberapa" Then
            If Session("account") Is Nothing Then
                sReturn &= "- No Account selected.<BR>"
            Else
                Dim dtab As DataTable = Session("account")
                If dtab.Select("selected='1'", "").Length < 1 Then
                    sReturn &= "- No Account selected.<BR>"
                End If
            End If
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Sub initddl()
        Dim sSql As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLCabang, sSql)
        DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "10" Then
            DDLCabang.SelectedValue = Session("branch_id")
        Else
            DDLCabang.SelectedIndex = DDLCabang.Items.Count - 1
        End If
    End Sub

    Private Sub UpdateCheckedGV()
        If Not Session("account") Is Nothing Then
            Dim dtab As DataTable = Session("account")

            If dtab.Rows.Count > 0 Then
                Dim cb As System.Web.UI.WebControls.CheckBox
                Dim lbl As System.Web.UI.WebControls.Label
                Dim acctgoid As Integer
                Dim dView As DataView = dtab.DefaultView
                Dim drView As DataRowView

                For i As Integer = 0 To gvAccounting.Rows.Count - 1
                    cb = gvAccounting.Rows(i).FindControl("cbAccount")
                    If cb.Checked = True Then
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 1
                        drView.EndEdit()
                        dView.RowFilter = ""
                    Else
                        lbl = gvAccounting.Rows(i).FindControl("lblacctgoid")
                        acctgoid = Integer.Parse(lbl.Text)
                        dView.RowFilter = "id = " & acctgoid
                        drView = dView.Item(0)
                        drView.BeginEdit()
                        drView("selected") = 0
                        drView.EndEdit()
                        dView.RowFilter = ""
                    End If
                Next
                dtab.AcceptChanges()
            End If

            Session("account") = dtab
        End If
    End Sub

    Private Function ProcessReport(ByVal tipe As String) As String ' Return Error/Warning message if any
        Dim sMsg As String = "" : crv.ReportSource = Nothing
        Session("showReport") = "false"
        Try
            Dim sWhere As String = ""
            If IsDate(toDate(date1.Text)) = False Or IsDate(toDate(date2.Text)) = False Then
                showMessage("Format periode salah !!", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            End If
            If CDate(toDate(date1.Text.Trim)) > CDate(toDate(date2.Text.Trim)) Then
                showMessage("Periode 2 tidak boleh lebih besar dari periode 1!!", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            End If

            If CheckYearIsValid(toDate(date1.Text)) Then
                showMessage("Tahun periode awal tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            End If

            If CheckYearIsValid(toDate(date2.Text)) Then
                showMessage("Tahun periode akhir tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            End If

            If Session("branch_id") = "10" Then
                sWhere = IIf(DDLCabang.SelectedValue = "ALL", " and noref like '" & tipene.SelectedValue & "%'", "and m.branch_code='" & DDLCabang.SelectedValue & "' /*and noref like '" & tipene.SelectedValue & "%'*/")
            Else
                sWhere = "and m.branch_code='" & Session("branch_id") & "'"
            End If 

            Dim strcabang As String = ""

            For C1 As Integer = 0 To cbType.Items.Count - 1
                If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                    strcabang &= cbType.Items(C1).Value & "or "
                End If
            Next

            If strcabang <> "" Then
                strcabang = Left(strcabang, strcabang.Length - 3)
            End If

            sWhere &= IIf(strcabang = "", " and noref like '%" & Tchar(typeGJ.Text) & "%'", "and ( " & strcabang & ") and noref like '%" & Tchar(typeGJ.Text) & "%'")
 
            Dim sAcctgOid As String = ""
            If rblAccount.SelectedValue = "Beberapa" Then
                Dim dtAcctg As DataTable : dtAcctg = Session("account")
                Dim dvAcctg As DataView = dtAcctg.DefaultView
                dvAcctg.RowFilter = "selected=1"

                For R1 As Integer = 0 To dvAcctg.Count - 1
                    sAcctgOid &= dvAcctg(R1)("id").ToString & ","
                Next
            End If

            If rblAccount.SelectedValue = "Beberapa" Then
                sWhere &= " AND d.acctgoid IN (" & Left(sAcctgOid, sAcctgOid.Length - 1) & ")"
            End If
            Dim sDate1 As Date = Format(CDate(toDate(date1.Text.Trim)), "yyyy-MM-dd")
            Dim sDate2 As Date = Format(CDate(toDate(date2.Text.Trim)), "yyyy-MM-dd")
            Dim sTartDate As Date = Format(CDate(toDate(date1.Text.Trim)))
            Dim eNdDate As Date = Format(CDate(toDate(date2.Text.Trim)))
            vReport = New ReportDocument
            If tipe = "exl" Then
                vReport.Load(Server.MapPath("~\Report\GeneralJournalExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\GeneralJournal.rpt"))
            End If
            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("cmpcode", CompnyCode)
            vReport.SetParameterValue("startDate", Format(CDate(toDate(date1.Text.Trim)), "yyyy-MM-dd"))
            vReport.SetParameterValue("endDate", Format(CDate(toDate(date2.Text.Trim)), "yyyy-MM-dd"))
            vReport.SetParameterValue("swhere", sWhere)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", "")) 
            Session("showReport") = "true"
        Catch ex As Exception
            showMessage(ex.ToString, compnyname & " - ERROR", 1, "modalMsgBox")
        End Try
        Return sMsg
    End Function

    Private Sub showPrint(ByVal type As String)
        Try
            Dim sWhere As String = ""
            If IsDate(toDate(date1.Text)) = False Or IsDate(toDate(date2.Text)) = False Then
                showMessage("Format periode salah !!", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If CDate(toDate(date1.Text.Trim)) > CDate(toDate(date2.Text.Trim)) Then
                showMessage("Periode 2 tidak boleh lebih besar dari periode 1!!", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If CheckYearIsValid(toDate(date1.Text)) Then
                showMessage("Tahun periode awal tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If CheckYearIsValid(toDate(date2.Text)) Then
                showMessage("Tahun periode akhir tidak boleh kurang dari 2000 dan lebih besar dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If Session("branch_id") = "10" Then
                sWhere = IIf(DDLCabang.SelectedValue = "ALL", " and noref like '" & tipene.SelectedValue & "%'", "and m.branch_code='" & DDLCabang.SelectedValue & "' and noref like '" & tipene.SelectedValue & "%'")
            Else
                sWhere = "and m.branch_code='" & Session("branch_id") & "' and noref like '" & tipene.SelectedValue & "%'"
            End If

            If ddlType.SelectedValue <> "JURNAL UMUM" Then
                If typeGJ.Text <> "" Then
                    sWhere &= "and noref like '%" & Tchar(typeGJ.Text) & "%' and (noref like '%bbk%' or noref like '%bbm%' or noref like '%bkk%' or noref like '%bkm%' or noref like '%bgm%' or noref like '%bgk%' or noref like '%bdk%' or noref like '%bdm%' or noref like '%bst%' or noref like '%rgm%' or noref like '%rgk%') and m.glnote not like '%A/R%' and m.glnote not like '%A/P %' and m.glnote not like '%SALES INVOICE%' and m.glnote not like '%Transfer Gudang%' and m.glnote not like '%PURCHASE INVOICE%' and m.glnote not like '%AS%' and m.glnote not like '%MU%' and m.glnote not like '%AS%' and m.glnote not like '%Sales Return%' and m.glnote not like '%KEMBALI%' and m.glnote not like '%PINJAM%' and m.glnote not like '%NT%' and m.glnote not like '%EXP%' and m.glnote not like '%COA Initial Balance%'"
                Else
                    sWhere &= "and (noref like '%bbk%' or noref like '%bbm%' or noref like '%bkk%' or noref like '%bkm%' or noref like '%bgm%' or noref like '%bgk%' or noref like '%bdk%' or noref like '%bdm%' or noref like '%bst%' or noref like '%rgm%' or noref like '%rgk%') and m.glnote not like '%A/R%' and m.glnote not like '%A/P %' and m.glnote not like '%SALES INVOICE%' and m.glnote not like '%Transfer Gudang%' and m.glnote not like '%PURCHASE INVOICE%' and m.glnote not like '%AS%' and m.glnote not like '%MU%' and m.glnote not like '%AS%' and m.glnote not like '%Sales Return No%' and m.glnote not like '%KEMBALI%' and m.glnote not like '%PINJAM%' and m.glnote not like '%NT%' and m.glnote not like '%EXP%' and m.glnote not like '%COA Initial Balance%'"
                End If
            End If

            vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\GeneralJournal.rpt"))
            vReport.SetParameterValue("cmpcode", CompnyCode)
            vReport.SetParameterValue("startDate", Format(CDate(toDate(date1.Text.Trim)), "yyyy-MM-dd"))
            vReport.SetParameterValue("endDate", Format(CDate(toDate(date2.Text.Trim)), "yyyy-MM-dd"))
            vReport.SetParameterValue("swhere", sWhere)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            Session("showReport") = "true"

            If type = "" Then
                crv.ReportSource = vReport
                crv.DisplayGroupTree = False
            ElseIf type = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "General_Journal_" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptGeneralJournal.aspx?awal=true")
            Else
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "General_Journal_" & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptGeneralJournal.aspx?awal=true")
            End If

        Catch ex As Exception
            showMessage(ex.ToString, compnyname & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
    End Sub

    Private Sub BindAccounting(ByVal filter As String)
        Try
            Dim dtab As New DataTable
            sSql = "SELECT 0 as selected, acctgoid as id,acctgcode as code,acctgdesc as name FROM QL_mstacctg WHERE acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null) AND (acctgcode LIKE '%" & Tchar(filter.Trim) & "%' OR acctgdesc LIKE '%" & Tchar(filter.Trim) & "%') ORDER BY acctgcode "
            dtab = cKon.ambiltabel(sSql, "QL_mstacctg")
            gvAccounting.DataSource = dtab
            gvAccounting.DataBind()
            Session("account") = dtab
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, compnyname & " - WARNING", 1, "modalMsgBoxWarn")
            conn.Close() : Exit Sub
        End Try
    End Sub

    Private Sub BindType(ByVal filter As String)
        Try
            Dim dtab As New DataTable
            sSql = "SELECT 0 as selected, acctgoid as id,acctgcode as code,acctgdesc as name FROM QL_mstacctg WHERE acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null ) " & _
                "AND (acctgcode LIKE '%" & Tchar(Filter.Trim) & "%' OR acctgdesc LIKE '%" & Tchar(Filter.Trim) & "%') ORDER BY acctgcode "
            dtab = cKon.ambiltabel(sSql, "QL_mstacctg")
            gvAccounting.DataSource = dtab
            gvAccounting.DataBind()
            Session("account") = dtab
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, compnyname & " - WARNING", 1, "modalMsgBoxWarn")
            conn.Close() : Exit Sub
        End Try
    End Sub

    Public Sub showPrintExcel(ByVal name As String)
        Dim sWhere As String = ""
        Session("diprint") = "False"

        If IsDate(toDate(date1.Text)) = False Or IsDate(toDate(date2.Text)) = False Then
            showMessage("Format periode salah !!", compnyname & " -  WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If CDate(toDate(date1.Text.Trim)) > CDate(toDate(date2.Text.Trim)) Then
            showMessage("Tanggal Terakhir tidak boleh lebih kecil dari Tanggal Awal!!", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If CheckYearIsValid(toDate(date1.Text)) Then
            showMessage("Tahun periode awal tidak boleh kurang dari 2000 dan lebih dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If CheckYearIsValid(toDate(date2.Text)) Then
            showMessage("Tahun periode akhir tidak boleh kurang dari 2000 dan lebih dari 2072 ", compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Response.Clear()
        Dim sSql As String = ""
        Response.AddHeader("content-disposition", "inline;filename=Rpt_GeneralJournal.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        sSql = "select convert(char(10),m.gldate,103) Tgl, noref, '''' + a.acctgcode COA, a.acctgdesc Deskripsi, debet = case d.gldbcr when 'D' then d.glamt else 0 end, credit = case d.gldbcr when 'C' then d.glamt else 0 end , m.glnote Note from QL_trnglmst m left outer join Ql_trngldtl d on d.glmstoid = m.glmstoid and d.cmpcode = m.cmpcode left outer join Ql_mstacctg a on a.acctgoid = d.acctgoid where d.cmpcode = '" & CompnyCode & "' and convert(char(10),m.gldate,121) >= '" & Format(CDate(toDate(date1.Text.Trim)), "yyyy-MM-dd") & "' and m.glflag like 'post%'  and convert(char(10),m.gldate,121) <='" & Format(CDate(toDate(date2.Text.Trim)), "yyyy-MM-dd") & "'    and noref like '" & tipene.SelectedValue & "%' order by m.gldate ASC, m.glmstoid ASC , d.gldbcr DESC"

        Dim dt As DataTable = CreateDataTableFromSQL(sSql)
        Response.Write(ConvertDtToTDF(dt))
        Response.End()
    End Sub
#End Region

#Region "Event"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            '   Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("rptGeneralJournal.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = compnyname & " - General Journal Report"
        If Not Page.IsPostBack Then
            If Session("branch_id") = "10" Then
                DDLCabang.Visible = True
            Else
                DDLCabang.Enabled = False
                lblcbg.Enabled = False
                Label3.Enabled = False
            End If
            Session("showReport") = "false"
            date1.Text = Format(GetServerTime(), "01/MM/yyyy")
            date2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            initddl()
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("view")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn") 
            Else
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "GLReport_" & Format(toDate(date1.Text), "yyMMdd") & "_" & Format(toDate(date2.Text), "yyMMdd"))
                Catch ex As Exception
                    vReport.Close() : vReport.Dispose()
                End Try
            End If
        End If
    End Sub

    Protected Sub imbClearRpt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptGeneralJournal.aspx?awal=true")
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("exl")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Else
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                Try
                    vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "GLReport_" & _
                    Format(toDate(date1.Text), "yyMMdd") & "_" & Format(toDate(date2.Text), "yyMMdd"))
                Catch ex As Exception
                    report.Close() : report.Dispose()
                End Try
            End If
        End If
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
    End Sub

    Protected Sub btnshowprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("view")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Else
                crv.ReportSource = vReport
                Session("showReport") = "true"
            End If
        End If
    End Sub

    Protected Sub crv_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crv.Navigate
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("view")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Else
                crv.ReportSource = vReport
                Session("showReport") = "true"
            End If
        End If 
    End Sub

    Protected Sub rblAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rblAccount.SelectedValue = "All" Then
            gvAccounting.Visible = False
            gvAccounting.DataSource = Nothing
            gvAccounting.DataBind()
            acctgdesc.Visible = False
            btnsearchacctg.Visible = False : btnDelete.Visible = False
            acctgdesc.Text = "" : acctgoid.Text = ""
        Else
            acctgdesc.Visible = True
            btnsearchacctg.Visible = True : btnDelete.Visible = True
        End If
    End Sub

    Protected Sub btnsearchacctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sDate As String = ""
        BindAccounting(acctgdesc.Text.Trim)
        gvAccounting.Visible = True
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        acctgdesc.Text = "" : acctgoid.Text = ""
        gvAccounting.Visible = False
        gvAccounting.DataSource = Nothing
        gvAccounting.DataBind()
        Session("account") = Nothing
    End Sub

    Protected Sub gvAccounting_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        UpdateCheckedGV()
        Dim dtab As DataTable = Session("account")
        gvAccounting.PageIndex = e.NewPageIndex
        gvAccounting.DataSource = dtab
        gvAccounting.DataBind()
        Session("account") = dtab
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If ddlType.SelectedValue = "JURNAL UMUM" Then
        '    TD1.Visible = False
        'Else
        '    TD1.Visible = True
        'End If
    End Sub

    Protected Sub crv_Search(ByVal source As Object, ByVal e As CrystalDecisions.Web.SearchEventArgs)
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("view")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Else
                crv.ReportSource = vReport
                Session("showReport") = "true"
            End If
        End If 
    End Sub

    Protected Sub crv_ViewZoom(ByVal source As Object, ByVal e As CrystalDecisions.Web.ZoomEventArgs)
        UpdateCheckedGV()
        Dim sMsg As String = ValidateInput()
        If sMsg <> "" Then
            showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        Else
            sMsg = "" : sMsg = ProcessReport("View")
            If sMsg <> "" Then
                showMessage(sMsg, compnyname & " - WARNING", 2, "modalMsgBoxWarn")
            Else
                crv.ReportSource = vReport
                Session("showReport") = "true"
            End If
        End If 
    End Sub

    Protected Sub cbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strcabang As Int32 = 0

        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Selected = True Then
                strcabang += 1
            End If
        Next

        If strcabang > 1 Then
            typeGJ.CssClass = "inpTextDisabled"
            typeGJ.Enabled = False
        Else
            typeGJ.CssClass = "inpText"
            typeGJ.Enabled = True
        End If
    End Sub

    Protected Sub imbClearNoTrans_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearNoTrans.Click
        typeGJ.Text = ""
    End Sub

    Protected Sub cbCheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCheckAll.CheckedChanged
        If cbCheckAll.Checked = True Then
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = True
            Next
            typeGJ.Enabled = True : typeGJ.CssClass = "inpTextDisabled"
        Else
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = False
            Next
            typeGJ.Enabled = False : typeGJ.CssClass = "inpText"
        End If
    End Sub
#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        vReport.Close()
        vReport.Dispose()
    End Sub
End Class
