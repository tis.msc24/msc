<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Master_Customer_Create.aspx.vb" Inherits="Master_Master_Customer" Title="MSC - Master Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
        <table width="100%">
            <tr>
                <td colspan="3" style="background-color: silver" align="left" char="header">
                    <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Data Customer Admin"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" colspan="3" style="background-color: transparent">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                <HeaderTemplate>
                    <strong><span style="color: background">
                        <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                        List of Customer</span></strong> <strong>:.</strong>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD colSpan=5><asp:DropDownList id="fddlCabang" runat="server" Width="208px" CssClass="inpText" __designer:wfdid="w26"></asp:DropDownList></TD></TR><TR><TD><asp:Label id="Label2" runat="server" Text="Filter" __designer:wfdid="w27"></asp:Label></TD><TD><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w28"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="ddlFilter" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w29"><asp:ListItem Value="cust.custcode">Code</asp:ListItem>
<asp:ListItem Value="cust.custname">Nama</asp:ListItem>
<asp:ListItem Value="cust.custaddr">Alamat</asp:ListItem>
<asp:ListItem Value="phone1">Telepon</asp:ListItem>
<asp:ListItem Value="custcreditlimitrupiah">Credit Limit</asp:ListItem>
<asp:ListItem Value="custcreditlimitusagerupiah">Credit Usage</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="166px" CssClass="inpText" __designer:wfdid="w30"></asp:TextBox>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w31"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w32"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" __designer:wfdid="w33" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbcust" onclick="imbcust_Click" runat="server" ImageUrl="~/Images/printexport.gif" __designer:wfdid="w34"></asp:ImageButton></TD></TR><TR><TD><asp:CheckBox id="cbCreditTempo" runat="server" Width="127px" Text="Credit Temporary" __designer:wfdid="w35"></asp:CheckBox></TD><TD>:</TD><TD colSpan=5><asp:DropDownList id="ddlCreditTempo" runat="server" Width="40px" CssClass="inpText" __designer:wfdid="w36"><asp:ListItem>&gt; 0</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:CheckBox id="chkCreatedByBranch" runat="server" Text="Customer yg dibuat Cabang" __designer:wfdid="w37"></asp:CheckBox> <asp:CheckBox id="cbCreditAwal" runat="server" Text="CL Awal Hari ini" __designer:wfdid="w38"></asp:CheckBox></TD></TR><TR><TD><asp:CheckBox id="cbfOverDue" runat="server" Text="Over Due" __designer:wfdid="w39"></asp:CheckBox></TD><TD>:</TD><TD colSpan=5><asp:DropDownList id="ddlOverDue" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w40"><asp:ListItem Value="Y">Allow Over Due</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 78px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w41"></asp:CheckBox></TD><TD><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w42"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="DDLStatus" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w43">
                                                <asp:ListItem>Active</asp:ListItem>
                                                <asp:ListItem>Inactive</asp:ListItem>
                                                <asp:ListItem>Suspended</asp:ListItem>
                                            </asp:DropDownList>&nbsp;</TD></TR><TR><TD style="WIDTH: 78px"><asp:CheckBox id="cbTop" runat="server" Text="TOP" __designer:wfdid="w44"></asp:CheckBox></TD><TD>:</TD><TD colSpan=5><asp:DropDownList id="fddlTOP" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w45"></asp:DropDownList></TD></TR><TR><TD colSpan=57><asp:GridView style="max-width: 950px" id="GVmstcust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w23" GridLines="None" AllowPaging="True" OnRowDataBound="GVmstcust_RowDataBound" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custoid" DataNavigateUrlFormatString="~\master\Master_Customer_Create.aspx?idPage={0}" DataTextField="custcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="False" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="140px"></HeaderStyle>

<ItemStyle Wrap="False" Width="160px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="400px"></HeaderStyle>

<ItemStyle Width="400px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="city" HeaderText="Kota" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone" HeaderText="Telepon">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditlimit" HeaderText="Credit Limit">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditusage" HeaderText="Credit Usage">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credittempo" HeaderText="Credit Tempo">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditover" HeaderText="Over Due">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroup" HeaderText="Group" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="termin" HeaderText="TOP">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custflag" HeaderText="Status">
<HeaderStyle Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notes" HeaderText="Catatan" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Update Over Due" Visible="False"><ItemTemplate>
<asp:ImageButton style="HEIGHT: 23px" id="btnProcess" onclick="btnProcess_Click" runat="server" ImageUrl="~/Images/process.png" __designer:wfdid="w5" ToolTip='<%# eval("custoid") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Data Not Found" BorderColor="Blue"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>
</ContentTemplate>

                        <Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                <HeaderTemplate>
                    <strong><span style="color: background">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle" />
                        Form Customer</span></strong> <strong>:.</strong>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="HEIGHT: 4px" colSpan=3><TABLE width="100%"><TBODY><TR><TD colSpan=6><asp:Label id="Label8" runat="server" Font-Size="10pt" Font-Bold="True" Text="Informasi" Font-Underline="True" __designer:wfdid="w4"></asp:Label><asp:Label id="lblAdd" runat="server" __designer:wfdid="w123" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Code <asp:Label id="Label5" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w124"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custcode" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w125" MaxLength="10" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px; WHITE-SPACE: nowrap">Customer Type</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlcustgroup" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w126">
                                                                        <asp:ListItem>GROSIR</asp:ListItem>
                                                                        <asp:ListItem>PROJECT</asp:ListItem>
                                                                    </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Customer Group</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlcustgroup1" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w127" OnSelectedIndexChanged="ddlcustgroup1_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px; WHITE-SPACE: nowrap">Tgl Gabung</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="TglGabung" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox>&nbsp;<asp:ImageButton id="ImgBtnTglGabung" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w2"></asp:ImageButton>&nbsp;<asp:Label id="Label15" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama <asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w128"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD colSpan=1><asp:DropDownList id="precustname" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w129"></asp:DropDownList> <asp:TextBox id="custname" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w130" MaxLength="100" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:Label id="Label12" runat="server" ForeColor="Red" Text="min. 1 karakter" __designer:wfdid="w131"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px; COLOR: #000099">Provinsi <asp:Label id="Label30" runat="server" CssClass="Important" Text="*" __designer:wfdid="w132"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custprovoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w133" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Cabang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="branchID" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w134" OnSelectedIndexChanged="branchID_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><asp:Label id="KodeOutlate" runat="server" __designer:wfdid="w17" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px">Kota <asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w136"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custcityoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w137" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Alamat</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custaddr" runat="server" Width="280px" CssClass="inpText" __designer:wfdid="w138" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custflag" runat="server" Width="103px" CssClass="inpText" __designer:wfdid="w139">
                                                                        <asp:ListItem>Active</asp:ListItem>
                                                                        <asp:ListItem>Inactive</asp:ListItem>
                                                                        <asp:ListItem>Suspended</asp:ListItem>
                                                                    </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama Pemilik&nbsp;<asp:Label id="Label26" runat="server" CssClass="Important" Text="*" __designer:wfdid="w140"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlconttitle" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w141"></asp:DropDownList>&nbsp;<asp:TextBox id="contactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w142" MaxLength="125"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px">Fax</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custfax1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w143" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD64" runat="server" Visible="true">E-mail</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD67" runat="server" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD62" colSpan=1 runat="server" Visible="true"><asp:TextBox id="custemail" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w144" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px">Website</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custwebsite" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w145" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD63" runat="server" Visible="true">Telepon&nbsp;Rumah <asp:Label id="Label24" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w146"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD65" runat="server" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD66" colSpan=1 runat="server" Visible="true"><asp:TextBox id="phone1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w147" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px">NPWP</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custnpwp" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w148" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Telepon Kantor <asp:Label id="Label27" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w149"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phone2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w150" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px"><asp:Label id="Label25" runat="server" Text="TOP" __designer:wfdid="w151"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="dd_timeofpayment" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w152" AutoPostBack="True"></asp:DropDownList>&nbsp;Day</TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Telepon HP / WA <asp:Label id="Label28" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w153"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phone3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w154" MaxLength="25"></asp:TextBox>&nbsp;<asp:DropDownList id="spgOid" runat="server" Width="112px" CssClass="inpText" __designer:wfdid="w155" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD25" runat="server" Visible="true">Average Late A/R</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD6" runat="server" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD21" runat="server" Visible="true"><asp:TextBox id="averagelate" runat="server" Width="76px" CssClass="inpTextDisabled" __designer:wfdid="w1">0</asp:TextBox>&nbsp;Day</TD></TR><TR><TD style="VERTICAL-ALIGN: top">Catatan</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="notes" runat="server" Width="213px" Height="45px" CssClass="inpText" __designer:wfdid="w157" MaxLength="500" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="maks. 500 karakter" __designer:wfdid="w158"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px; COLOR: #000099" id="TD50" runat="server" Visible="false">Age Sales Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px; COLOR: #000099" id="TD49" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top; COLOR: #000099" id="TD35" runat="server" Visible="false"><asp:TextBox id="txtage" runat="server" Width="76px" CssClass="inpTextDisabled" __designer:wfdid="w156"></asp:TextBox>&nbsp;Day</TD></TR><TR style="COLOR: #000099"><TD style="VERTICAL-ALIGN: top" id="TD78" runat="server" Visible="false">Pin BB</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD75" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD77" colSpan=1 runat="server" Visible="false"><asp:TextBox id="pin_bb" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w159" MaxLength="12"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD82" runat="server" Visible="false"><asp:Label id="Label19" runat="server" Width="98px" Text="Nomor Paspor" __designer:wfdid="w160"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD85" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD80" runat="server" Visible="false"><asp:TextBox id="nopaspor" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w161" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD74" runat="server" Visible="false">Last Sales Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD76" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD79" colSpan=1 runat="server" Visible="false"><asp:TextBox id="lastsalesdate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w162"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD83" runat="server" Visible="false"><asp:Label id="Label20" runat="server" Width="85px" Text="Tanggal Lahir" __designer:wfdid="w163"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD81" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD84" runat="server" Visible="false"><asp:TextBox id="tgllahir" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w164"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtgllahir" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w165"></asp:ImageButton> <asp:Label id="Label11" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w166"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="Td18" Visible="true">Credit Limit Rupiah</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="Td20" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="Td24" colSpan=1 Visible="true"><asp:TextBox id="custcreditlimitrupiah" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w167" MaxLength="10" Enabled="False" OnTextChanged="custcreditlimitrupiah_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="Td40" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="Td46" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="Td86" Visible="false"></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="Td87" Visible="true"><asp:Label id="Label10" runat="server" Width="153px" Text="Credit Limit Usage Rupiah" __designer:wfdid="w168"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="Td88" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="Td89" colSpan=1 Visible="true"><asp:TextBox id="custcreditlimitusagerupiah" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w169" Enabled="False" AutoPostBack="True" OnTextChanged="custcreditlimitusagerupiah_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="Td90" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="Td91" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="Td92" Visible="false"></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Credit Limit Sisa Rupiah</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitrupiahsisa" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w170" Enabled="False" AutoPostBack="True"></asp:TextBox> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px; WHITE-SPACE: nowrap">Credit Limit&nbsp;Awal</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitrupiahawal" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w171" MaxLength="15" AutoPostBack="True" OnTextChanged="custcreditlimitrupiahawal_TextChanged"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label14" runat="server" Width="168px" Text="Credit Limit Usage Over Due" __designer:wfdid="w273"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitusageoverdue" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w172" Enabled="False" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:Button id="btnInfoNota" runat="server" CssClass="btn green" Text="Info Nota" __designer:wfdid="w173" Visible="False"></asp:Button></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px"><asp:Label id="Label13" runat="server" Width="142px" Text="Credit Limit Temporary" __designer:wfdid="w174"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitrupiahtempo" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w175" MaxLength="15" AutoPostBack="True">0.00</asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama Bank dan Rekening</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="bankrekening" runat="server" Width="281px" CssClass="inpText" __designer:wfdid="w176" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="Td93" colSpan=3 Visible="false"><asp:CheckBox id="cbOverdue" runat="server" Width="136px" Text="Allow Over Due AR" __designer:wfdid="w177"></asp:CheckBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label21" runat="server" Text="Retur Account" __designer:wfdid="w178"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="returaccount" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w179"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="Td61" colSpan=1 runat="server" Visible="false"><asp:Label id="Label18" runat="server" Width="114px" Text="Nama untuk Paspor" __designer:wfdid="w180"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD33" colSpan=1 runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD31" colSpan=1 runat="server" Visible="false"><asp:TextBox id="namapaspor" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w181" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap"><asp:Label id="Label22" runat="server" Width="75px" Text="A/R Account" __designer:wfdid="w182"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="araccount" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w183"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD73" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD69" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD68" runat="server" Visible="false"><asp:DropDownList id="ddlconttitle2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w184"></asp:DropDownList> <asp:TextBox id="contactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w185" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="phonecontactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w186" Visible="False" MaxLength="50"></asp:TextBox><asp:TextBox id="phonecontactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w187" Visible="False" MaxLength="25"></asp:TextBox><asp:TextBox id="phonecontactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w188" Visible="False" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD72" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD71" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD70" rowSpan=1 runat="server" Visible="false"><asp:DropDownList id="ddlconttitle3" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w189"></asp:DropDownList> <asp:TextBox id="contactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w190" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 200px; BACKGROUND-COLOR: beige"><asp:GridView id="gvCustdtl" runat="server" Width="99%" ForeColor="#333333" __designer:wfdid="w191" Visible="False" AutoGenerateColumns="False" CellPadding="4" OnRowDataBound="gvCustdtl_RowDataBound" AllowPaging="True" GridLines="None" PageSize="100">
<PagerSettings Visible="False"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="createtime" HeaderText="Createtime">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcreditlimit" HeaderText="Credit Limit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcreditlimitawal" HeaderText="Credit Limit Awal">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcreditlimittempo" HeaderText="Credit Limit Tempo">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custdtlflag" HeaderText="Flag">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="updtime" HeaderText="Updtime">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><asp:Label id="lblLast" runat="server" Text="Last Update On" __designer:wfdid="w192"></asp:Label><asp:Label id="lblCreate" runat="server" Text="Created On" __designer:wfdid="w193"></asp:Label> <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w194"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w195"></asp:Label> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w196"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False" __designer:wfdid="w197"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w198"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1407374883553325" __designer:wfdid="w270" AssociatedUpdatePanelID="UpdatePanel3"><ProgressTemplate __designer:dtid="1407374883553326">
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w272"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="MEEnpwp" runat="server" __designer:wfdid="w199" TargetControlID="custnpwp" MaskType="Number" Mask="99.999.999.9-999.999" AcceptNegative="Right"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte1" runat="server" __designer:wfdid="w200" TargetControlID="phone1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte2" runat="server" __designer:wfdid="w201" TargetControlID="phonecontactperson2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" __designer:wfdid="w202" TargetControlID="phonecontactperson3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte6" runat="server" __designer:wfdid="w203" TargetControlID="phone2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte8" runat="server" __designer:wfdid="w204" TargetControlID="phonecontactperson1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte9" runat="server" __designer:wfdid="w205" TargetControlID="phone3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="CETgllahir" runat="server" __designer:wfdid="w210" TargetControlID="tgllahir" PopupButtonID="ibtgllahir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CETglGabung" runat="server" __designer:wfdid="w4" TargetControlID="TglGabung" PopupButtonID="ImgBtnTglGabung" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEETgllahir" runat="server" __designer:wfdid="w211" TargetControlID="tgllahir" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MEETglGabung" runat="server" __designer:wfdid="w5" TargetControlID="TglGabung" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="pcountry" runat="server" __designer:wfdid="w208" TargetControlID="cpcountryphonecode" ValidChars="0123456789+"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fCustFax2" runat="server" __designer:wfdid="w207" TargetControlID="CustFax2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteCustFax1" runat="server" __designer:wfdid="w206" TargetControlID="custfax1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEcustpostcode" runat="server" __designer:wfdid="w209" TargetControlID="custpostcode" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w212" TargetControlID="custcreditlimitrupiah" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbcustcreditlimitrupiahawal" runat="server" __designer:wfdid="w213" TargetControlID="custcreditlimitrupiahawal" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftbcustcreditlimitrupiahtempo" runat="server" __designer:wfdid="w214" TargetControlID="custcreditlimitrupiahtempo" ValidChars="0123456789,."></ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="I_U" runat="server" ForeColor="Red" Text="NEW" __designer:wfdid="w215" Visible="False"></asp:Label><asp:TextBox id="custoid" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w216" Visible="False"></asp:TextBox><asp:TextBox id="custdtloid" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w217" Visible="False"></asp:TextBox><asp:TextBox id="custcreditlimitdiff" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w218" Visible="False"></asp:TextBox><asp:TextBox id="custcreditlimitawaldiff" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w219" Visible="False"></asp:TextBox><asp:TextBox id="custdtlstatus" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w220" Visible="False"></asp:TextBox><asp:TextBox id="custdtlstatusoverdue" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w221" Visible="False"></asp:TextBox><asp:CheckBox id="cbOverduediff" runat="server" __designer:wfdid="w222" Visible="False"></asp:CheckBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD38" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD37" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD16" runat="server" Visible="false"><asp:DropDownList id="custpaytermdefaultoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w223" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD42" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD30" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD23" runat="server" Visible="false"><asp:DropDownList id="custdefaultcurroid" runat="server" CssClass="inpText" __designer:wfdid="w224" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD22" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD14" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD15" runat="server" Visible="false"><asp:DropDownList id="custacctgoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w225" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD5" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD41" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD2" runat="server" Visible="false"><asp:TextBox id="custcreditlimit" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w226" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD48" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD44" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD28" runat="server" Visible="false"><asp:DropDownList id="custcreditlimitcurroid" runat="server" CssClass="inpText" __designer:wfdid="w227" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD7" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD39" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD29" rowSpan=1 runat="server" Visible="false"><asp:TextBox id="custcreditlimitusage" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w228" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD34" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD3" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD47" runat="server" Visible="false"><asp:DropDownList id="custcountryoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w229" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD36" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD45" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD32" rowSpan=1 runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Def. Limit Currency" __designer:wfdid="w230" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD17" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD43" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD12" runat="server" Visible="false"><asp:TextBox id="CustGroup" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w231" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD1" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD9" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD26" rowSpan=1 runat="server" Visible="false"><asp:DropDownList id="statusar" runat="server" CssClass="inpText" __designer:wfdid="w232" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD11" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD8" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD4" runat="server" Visible="false"><asp:TextBox id="cpcountryphonecode" runat="server" Width="31px" CssClass="inpTextDisabled" __designer:wfdid="w233" Visible="False" MaxLength="6" Enabled="False" AutoPostBack="True"></asp:TextBox><asp:TextBox id="custpostcode" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w234" Visible="False" MaxLength="10"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 58px" id="TD27" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD13" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD19" runat="server" Visible="false"><asp:TextBox id="CustFax2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w235" Visible="False" MaxLength="25"></asp:TextBox></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer></td>
            </tr>
        </table>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False" EnableTheming="True"><TABLE width=400><TBODY><TR><TD style="HEIGHT: 25px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 47px" align=left><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD align=left colSpan=2><asp:Label id="validasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="WIDTH: 47px"></TD><TD colSpan=2><asp:Label id="lblstate" runat="server"></asp:Label></TD></TR><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEError" runat="server" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblValidasi" PopupControlID="PanelErrMsg" Drag="True" DropShadow="True" TargetControlID="btnExtender"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel>
    <asp:UpdatePanel id="UpdPanelPrint" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Customer"></asp:Label></TD></TR><TR><TD id="TD10" align=left colSpan=2 runat="server" Visible="false"><asp:Label id="printType" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" align=left></TD><TD style="HEIGHT: 20px" align=left></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" TargetControlID="btnHidePrint" Drag="True" PopupControlID="pnlPrint" PopupDragHandleControlID="lblPrint" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">Catatan Transaksi Nota</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListNya" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" EnableModelValidation="True" GridLines="None" AllowPaging="True" OnRowDataBound="gvListNya_RowDataBound" CellPadding="4" AutoGenerateColumns="False" PageSize="5" DataKeyNames="trnjualno,trnjualdate,payduedate,trnamtjualnetto" OnPageIndexChanging="gvListNya_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnjualno" HeaderText="No. Transasksi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Tgl Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamtjualnetto" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3>&nbsp;&nbsp; <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="lbCloseListMat"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
        <asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" SelectCommand="SELECT cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, custfax1, phone3, custfax2, custemail, custwebsite, upduser, updtime, custnpwp,pin_bb FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" InsertCommand="INSERT INTO QL_mstcust(cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, upduser, updtime, phonecontactperson3, phonecontactperson2, phonecontactperson1, contactperson1, contactperson2, contactperson3, custnpwp,pin_bb) VALUES (@cmpcode, @custoid, @custcode, @custname, @custflag, @custaddr, @custcityoid, @custprovoid, @custcountryoid, @custpostcode, @phone1, @phone2, @phone3, @custfax1, @custfax2, @custemail, @custwebsite, @upduser, @updtime, @phonecontactperson3, @phonecontactperson2, @phonecontactperson1, @contactperson1, @contactperson2, @contactperson3, @custnpwp,@pin_bb)" UpdateCommand="UPDATE QL_mstcust SET custcode = @custcode, custname = @custname, custflag = @custflag, custaddr = @custaddr, custcityoid = @custcityoid, custprovoid = @custprovoid, custcountryoid = @custcountryoid, custpostcode = @custpostcode, phone1 = @phone1, phone2 = @phone2, phone3 = @phone3, custfax1 = @custfax1, custfax2 = @custfax2, custemail = @custemail, custwebsite = @custwebsite, upduser = @upduser, updtime = @updtime, custnpwp = @custnpwp, contactperson1 = @contactperson1, contactperson2 = @contactperson2, contactperson3 = @contactperson3, phonecontactperson1 = @phonecontactperson1, phonecontactperson2 = @phonecontactperson2, phonecontactperson3 = @phonecontactperson3,pin_bb=@pin_bb WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
            <SelectParameters>
                <asp:Parameter Name="cmpcode" />
                <asp:Parameter Name="custoid" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="cmpcode" />
                <asp:Parameter Name="custoid" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="custcode" />
                <asp:Parameter Name="custname" />
                <asp:Parameter Name="custflag" />
                <asp:Parameter Name="custaddr" />
                <asp:Parameter Name="custcityoid" />
                <asp:Parameter Name="custprovoid" />
                <asp:Parameter Name="custcountryoid" />
                <asp:Parameter Name="custpostcode" />
                <asp:Parameter Name="phone1" />
                <asp:Parameter Name="phone2" />
                <asp:Parameter Name="phone3" />
                <asp:Parameter Name="custfax1" />
                <asp:Parameter Name="custfax2" />
                <asp:Parameter Name="custemail" />
                <asp:Parameter Name="custwebsite" />
                <asp:Parameter Name="upduser" />
                <asp:Parameter Name="updtime" />
                <asp:Parameter Name="custnpwp" />
                <asp:Parameter Name="contactperson1" />
                <asp:Parameter Name="contactperson2" />
                <asp:Parameter Name="contactperson3" />
                <asp:Parameter Name="phonecontactperson1" />
                <asp:Parameter Name="phonecontactperson2" />
                <asp:Parameter Name="phonecontactperson3" />
                <asp:Parameter Name="cmpcode" />
                <asp:Parameter Name="custoid" />
                <asp:Parameter Name="pin_bb" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="cmpcode" />
                <asp:Parameter Name="custoid" />
                <asp:Parameter Name="custcode" />
                <asp:Parameter Name="custname" />
                <asp:Parameter Name="custflag" />
                <asp:Parameter Name="custaddr" />
                <asp:Parameter Name="custcityoid" />
                <asp:Parameter Name="custprovoid" />
                <asp:Parameter Name="custcountryoid" />
                <asp:Parameter Name="custpostcode" />
                <asp:Parameter Name="phone1" />
                <asp:Parameter Name="phone2" />
                <asp:Parameter Name="phone3" />
                <asp:Parameter Name="custfax1" />
                <asp:Parameter Name="custfax2" />
                <asp:Parameter Name="custemail" />
                <asp:Parameter Name="custwebsite" />
                <asp:Parameter Name="upduser" />
                <asp:Parameter Name="updtime" />
                <asp:Parameter Name="phonecontactperson3" />
                <asp:Parameter Name="phonecontactperson2" />
                <asp:Parameter Name="phonecontactperson1" />
                <asp:Parameter Name="contactperson1" />
                <asp:Parameter Name="contactperson2" />
                <asp:Parameter Name="contactperson3" />
                <asp:Parameter Name="custnpwp" />
                <asp:Parameter Name="pin_bb" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT cmpcode, custoid, custcode, custname, custaddr, custcityoid, custprovoid, custflag, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, upduser, updtime FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
            <SelectParameters>
                <asp:Parameter Name="cmpcode" />
                <asp:Parameter Name="custoid" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSOID" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, tablename, lastoid, tablegroup FROM QL_mstoid WHERE (tablename = @tablename)" UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)">
            <SelectParameters>
                <asp:Parameter DefaultValue="QL_mstcust" Name="tablename" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="lastoid" />
                <asp:Parameter Name="tablename" />
                <asp:Parameter Name="cmpcode" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSBank" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSCurrency" runat="server"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSCity" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
            <SelectParameters>
                <asp:Parameter DefaultValue="City" Name="gengroup" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSProvince" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
            <SelectParameters>
                <asp:Parameter DefaultValue="Province" Name="gengroup" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSCountry" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
            <SelectParameters>
                <asp:Parameter DefaultValue="Country" Name="gengroup" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSContactPerson" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" InsertCommand="INSERT INTO QL_CONTACTPERSON(CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDTIME, UPDUSER) VALUES (@CMPCODE, @CPOID, @OID, @TYPE, @CPNAME, @CPPHONE, @CPEMAIL, @UPDTIME, @UPDUSER)" SelectCommand="SELECT CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDUSER, UPDTIME FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" UpdateCommand="UPDATE QL_CONTACTPERSON SET OID = @OID, TYPE = @TYPE, CPNAME = @CPNAME, CPPHONE = @CPPHONE, CPEMAIL = @CPEMAIL, UPDUSER = @UPDUSER, UPDTIME = @UPDTIME WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)">
            <SelectParameters>
                <asp:Parameter Name="CMPCODE" />
                <asp:Parameter Name="CPOID" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="CMPCODE" />
                <asp:Parameter Name="CPOID" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="OID" />
                <asp:Parameter Name="TYPE" />
                <asp:Parameter Name="CPNAME" />
                <asp:Parameter Name="CPPHONE" />
                <asp:Parameter Name="CPEMAIL" />
                <asp:Parameter Name="UPDUSER" />
                <asp:Parameter Name="UPDTIME" />
                <asp:Parameter Name="CMPCODE" />
                <asp:Parameter Name="CPOID" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="CMPCODE" />
                <asp:Parameter Name="CPOID" />
                <asp:Parameter Name="OID" />
                <asp:Parameter Name="TYPE" />
                <asp:Parameter Name="CPNAME" />
                <asp:Parameter Name="CPPHONE" />
                <asp:Parameter Name="CPEMAIL" />
                <asp:Parameter Name="UPDTIME" />
                <asp:Parameter Name="UPDUSER" />
            </InsertParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSShipping" runat="server"></asp:SqlDataSource>
</asp:Content>

