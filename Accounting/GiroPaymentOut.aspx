<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="GiroPaymentOut.aspx.vb" Inherits="Accounting_GiroPaymentOut" Title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Maroon" Text=".: Pencairan Giro Keluar"></asp:Label></th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%" TabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: Pencairan Giro Keluar</span></strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel3" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 980px"><TBODY><TR><TD style="WIDTH: 169px" align=left>Periode</TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label21" runat="server" Font-Size="X-Small" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 169px" align=left>Status</TD><TD align=left colSpan=4><asp:DropDownList id="FilterStatus" runat="server" Width="75px" CssClass="inpText">
<asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Text="POST" Value="POST"></asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 169px" align=left>No Bukti(Cash/Bank)</TD><TD align=left colSpan=4><asp:TextBox id="nobukti" runat="server" Width="118px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 169px" align=left>No Akun/ Perkiraan (Bank) </TD><TD align=left colSpan=4><asp:DropDownList id="coafilter" runat="server" Width="278px" CssClass="inpText"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 169px" align=left><ajaxToolkit:CalendarExtender id="ce4" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnPeriode1" TargetControlID="txtperiode1"></ajaxToolkit:CalendarExtender></TD><TD align=left colSpan=4><ajaxToolkit:MaskedEditExtender id="meece4" runat="server" TargetControlID="txtPeriode1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnPeriode2" TargetControlID="txtperiode2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meece5" runat="server" TargetControlID="txtPeriode2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVmst" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w34" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="GiroPaymentMstOid" GridLines="None" AllowPaging="True" EmptyDataText="No data in database.">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="GiroPaymentMstOid" DataNavigateUrlFormatString="GiroPaymentout.aspx?oid={0}" DataTextField="cashbankno" HeaderText="No Cash/Bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="ExecutionDate" HeaderText="Tgl">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="perkiraan" HeaderText="Bank-Perkiraan(COA)">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gironote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroAmt" HeaderText="Jumlah">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroPaymentStatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
                                                                        <%--                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick='<%# ConfirmPrint(Container.DataItem("cashbankno")) %>'>Print</asp:LinkButton>
                                                                        --%><asp:ImageButton ID="imbPrintFormList" OnClick="imbPrintFormList_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" ToolTip='<%# Eval("cashbankno") %>' CommandArgument='<%# Eval("GiroPaymentMstOid") %>'></asp:ImageButton>
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label>
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Grand Total : Rp. " Visible="False"></asp:Label>&nbsp; <asp:Label id="lblgrandtotal" runat="server" Font-Bold="True" Visible="False"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="GVmst"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><asp:Label id="Label2" runat="server" Font-Bold="True" ForeColor="Black" Text="Informasi"></asp:Label> <BR /><TABLE style="WIDTH: 900px"><TBODY><TR><TD align=left colSpan=2><ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="paymentdate" PopupButtonID="btnPaydate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meece8" runat="server" TargetControlID="payduedate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=2><%--<ajaxToolkit:MaskedEditExtender ID="meeAmt" runat="server" TargetControlID="payamt" MaskType="Number" Mask="999,999,999,999.99" ClearMaskOnLostFocus="true" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender>--%><asp:HiddenField id="HiddenField1" runat="server"></asp:HiddenField> </TD><TD align=left colSpan=2><asp:HiddenField id="HiddenField2" runat="server"></asp:HiddenField> <ajaxToolkit:MaskedEditExtender id="meece3" runat="server" TargetControlID="paymentdate" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Tanggal</TD><TD align=left><asp:TextBox id="paymentdate" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="btnPaydate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>&nbsp;<asp:Label id="CutofDate" runat="server" __designer:wfdid="w6" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblCashBank" runat="server" Text="Bank"></asp:Label>&nbsp;Akun</TD><TD style="FONT-SIZE: x-small" align=left colSpan=3><asp:DropDownList id="cashbankacctgoid" runat="server" Width="366px" CssClass="inpText"></asp:DropDownList> </TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Total </TD><TD align=left><asp:TextBox id="totalreceipt" runat="server" Width="102px" CssClass="inpTextDisabled" ReadOnly="True">0.0000</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Catatan</TD><TD align=left colSpan=3><asp:TextBox id="cashbanknote" runat="server" Width="363px" Height="31px" CssClass="inpText" MaxLength="200" TextMode="MultiLine"></asp:TextBox>&nbsp;</TD></TR><TR><TD id="TD3" align=left runat="server" visible="false">Rate<asp:Label id="lblPOST" runat="server" Visible="False"></asp:Label></TD><TD id="TD5" align=left runat="server" visible="false"><asp:TextBox id="cashbankcurrate" runat="server" Width="24px" CssClass="inpText" Visible="False">1</asp:TextBox> <asp:DropDownList id="cashbankcurroid" runat="server" Width="41px" CssClass="inpText" OnSelectedIndexChanged="cashbankcurroid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD id="TD4" align=left runat="server" visible="false">Tipe Giro</TD><TD style="WIDTH: 239px" id="TD6" align=left runat="server" visible="false"><asp:DropDownList id="ddlGiroType" runat="server" Width="50px" CssClass="inpTextDisabled" OnSelectedIndexChanged="payflag_SelectedIndexChanged" Enabled="False">
                                                                <asp:ListItem>OUT</asp:ListItem>
                                                                <asp:ListItem Value="IN">IN</asp:ListItem>
                                                            </asp:DropDownList></TD><TD style="WIDTH: 72px" id="TD1" align=left runat="server" visible="false"><asp:Label id="I_U" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Data" Visible="False"></asp:Label></TD><TD id="TD2" align=left runat="server" visible="false"><asp:Label id="cashbankno" runat="server" Width="27px" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 15px" id="TD7" align=left runat="server" visible="false"></TD><TD style="HEIGHT: 15px" id="TD12" align=left runat="server" visible="false"><asp:Label id="bankacctgoid" runat="server" Visible="False"></asp:Label> <asp:Label id="suppoid" runat="server" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" id="TD11" align=left runat="server" visible="false"><asp:DropDownList id="payflag" runat="server" Width="27px" CssClass="inpText" Visible="False" OnSelectedIndexChanged="payflag_SelectedIndexChanged" Enabled="False">
                                                                <asp:ListItem>NONCASH</asp:ListItem>
                                                                <asp:ListItem Value="CASH">CASH</asp:ListItem>
                                                            </asp:DropDownList></TD><TD style="WIDTH: 239px; HEIGHT: 15px" id="TD8" align=left runat="server" visible="false"><asp:Label id="cashbankoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 72px; HEIGHT: 15px" id="TD9" align=left runat="server" visible="false"><asp:Label id="i_u2" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" id="TD10" align=left runat="server" visible="true"><asp:Label id="giroacctgoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: x-small; HEIGHT: 24px; TEXT-DECORATION: underline" align=left colSpan=2><asp:Label id="Label3" runat="server" Font-Bold="True" ForeColor="Black" Text="Detail"></asp:Label> <asp:Label id="GiroPaymentOid" runat="server" Width="50px" Visible="False"></asp:Label></TD><TD style="HEIGHT: 24px" align=left>&nbsp;</TD><TD style="WIDTH: 239px; HEIGHT: 24px" align=left><asp:Label id="payseq" runat="server" Visible="False"></asp:Label> <asp:Label id="paymentoid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 72px; HEIGHT: 24px" align=left><asp:Label id="GiroType" runat="server"></asp:Label></TD><TD style="HEIGHT: 24px" align=left><asp:Label id="paymentref_table" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>No.Giro <asp:Label id="Label12" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left><asp:TextBox id="payrefno" runat="server" Width="100px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="imbInvoice" onclick="ImageButton8_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbClearGiro" onclick="imbClearInv_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblDueDate" runat="server" Width="84px" Font-Size="X-Small" Text="Jatuh Tempo"></asp:Label></TD><TD style="WIDTH: 239px" align=left><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox>&nbsp; <asp:Label id="lblNotice" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" Visible="False"></asp:Label> </TD><TD style="FONT-SIZE: x-small; WIDTH: 72px" align=left><asp:Label id="Label5" runat="server" Width="84px" Font-Size="X-Small" Text="No.Dokumen"></asp:Label></TD><TD align=left><asp:TextBox id="document" runat="server" Width="149px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Note Detail</TD><TD align=left><asp:TextBox id="paynote" runat="server" Width="300px" Height="31px" CssClass="inpText" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Supplier /<BR />Cust.(R.DP.AR)</TD><TD style="WIDTH: 239px" align=left><asp:TextBox id="custname" runat="server" Width="187px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small; WIDTH: 72px" align=left>Total</TD><TD align=left><asp:TextBox id="payamt" runat="server" Width="150px" CssClass="inpTextDisabled" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left colSpan=3><asp:TextBox id="bank" runat="server" Width="203px" CssClass="inpTextDisabled" Visible="False" Enabled="False" Wrap="False"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:ImageButton id="imbAddReceipt" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton id="imbClearReceipt" onclick="imbClearReceipt_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=6><FIELDSET style="WIDTH: 950px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div2"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 91%"><TABLE id="dtltb" class="gvhdr" width="100%" runat="server" Visible="false"><TBODY><TR><TD style="WIDTH: 49px"></TD><TD style="WIDTH: 50px">No</TD><TD style="WIDTH: 121px">No. Giro</TD><TD style="WIDTH: 98px" align=left>No. Dokumen</TD><TD style="WIDTH: 152px" align=center>Customer</TD><TD style="WIDTH: 140px" align=center>Jatuh Tempo</TD><TD style="WIDTH: 114px" align=right>Total</TD><TD align=center colSpan=2>Catatan</TD></TR></TBODY></TABLE><asp:GridView id="gvReceiptDtl" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" GridLines="None" DataKeyNames="paymentoid,payrefno,custname,payamt,paynote,payduedate,payseq,payacctgoid,paymentref_table,document,custoid,bank" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w19" ShowHeader="False" OnRowDataBound="gvReceiptDtl_RowDataBound" OnSelectedIndexChanged="gvReceiptDtl_SelectedIndexChanged" OnRowDeleting="gvReceiptDtl_RowDeleting">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payseq" HeaderText="No ">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payrefno" HeaderText="No.Giro">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Coa Desc" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="document" HeaderText="No.Dokumen">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier/Cust.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Giro">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="paynote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Medium" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" CssClass="Important" Text="Tidak ada detail giro !!" __designer:wfdid="w7"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=6><SPAN style="FONT-SIZE: 10pt; COLOR: #585858">Last update on </SPAN><asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w20"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w21"></asp:Label></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w28"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:wfdid="w29"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w30"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w31"></asp:ImageButton> <asp:ImageButton id="imbPrintNota" onclick="imbPrintNota_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w32" Visible="False"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w33" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="imbPrintNota"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                :: Form Pencairan Giro Keluar</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" TargetControlID="beMsgBox" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelGiro" runat="server" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 900px"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblGiroData" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun Giro"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" align=left>Filter&nbsp;:<asp:DropDownList id="ddlgirofilter" runat="server" Width="94px" CssClass="inpText">
                                <asp:ListItem Value="payrefno">Giro No.</asp:ListItem>
                                <asp:ListItem Value="custname">Supplier</asp:ListItem>
                                <asp:ListItem Value="isnull(Bank,'')">Bank</asp:ListItem>
                                <asp:ListItem>Document</asp:ListItem>
                            </asp:DropDownList> <asp:TextBox id="tbGiro" runat="server" Width="222px" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="btnFindInvoice" onclick="btnFindInvoice_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 164px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV> <TABLE class="gvhdr" width="98%"><TBODY><TR><TD style="WIDTH: 53px"></TD><TD style="WIDTH: 146px">No. Giro</TD><TD style="WIDTH: 134px">Customer</TD><TD style="WIDTH: 156px">No. Dokumen</TD><TD style="WIDTH: 142px" align=left>Jatuh Tempo</TD><TD style="WIDTH: 131px" align=right>Total Giro</TD></TR></TBODY></TABLE> <DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 84%"><asp:GridView style="Z-INDEX: 100; LEFT: -1px; TOP: 128px; BACKGROUND-COLOR: transparent" id="gvGiro" runat="server" Width="100%" ForeColor="#333333" OnSelectedIndexChanged="gvGiro_SelectedIndexChanged" OnRowDataBound="gvGiro_RowDataBound" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankoid,payrefno,custname,payduedate,payamt,payacctgoid,paymentref_table,suppoid,document,bankacctgoid,bank" GridLines="None" AllowSorting="True" EmptyDataRowStyle-ForeColor="Red" ShowHeader="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="payrefno" HeaderText="No.Giro" SortExpression="payrefno">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="110px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier/Cust(R.DP)">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="document" HeaderText="No.Dokumen">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Jatuh Tempo" SortExpression="payduedate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payamt" HeaderText="Total Giro" SortExpression="payamt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payacctgoid" HeaderText="payacctgoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Account Giro Payment data !!"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV> </FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSales" onclick="CloseSales_Click" runat="server" CausesValidation="False" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenGiro" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderGiro" runat="server" TargetControlID="btnHiddenGiro" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelGiro" PopupDragHandleControlID="lblGiroData"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlPosting2" runat="server" Width="760px" Visible="False" CssClass="modalBox" BorderStyle="Solid" BorderWidth="2px">
                <table style="WIDTH: 560px">
                    <tbody>
                        <tr>
                            <td style="HEIGHT: 25px" align="center" colspan="3">
                                <asp:Label ID="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gvakun" runat="server" Width="750px" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="acctgcode" HeaderText="Kode">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="acctgdesc" HeaderText="Akun">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="glnote" HeaderText="Catatan">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="debet" HeaderText="Debet">
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="kredit" HeaderText="Kredit">
                                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lkbCancel2" runat="server" Font-Bold="True">[ CLOSE ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnHidePosting2" runat="server" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

