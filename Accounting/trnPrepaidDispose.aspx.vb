Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnPrePaidDispose
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompnyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Function"
    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("TblDtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("fixoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtlseq", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiod", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiodvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepaccum", Type.GetType("System.Double"))
        dtlTable.Columns.Add("depcostacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("accdepacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixpostdate", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(PrePaidDate.Text, "dd/MM/yyyy", sErr) Then
            ShowMessage("Pre-Paid Date 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(txtPeriode1.Text, "dd/MM/yyyy", sErr) Then
            ShowMessage("Period 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(txtPeriode2.Text, "dd/MM/yyyy", sErr) Then
            ShowMessage("Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            ShowMessage("Period 1 Can't > Period 2", 2)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Prosedure"

    Private Sub showMessage(ByVal message As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = iType : Validasi.Text = message
        panelMsg.Visible = True : btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub ReAmountAccum()
    End Sub

    Private Sub ReAmountDep()
    End Sub

    Public Sub binddata()
        IsValidPeriod()
        sSql = "SELECT pd.cmpcode,pd.branch_code,ol.gendesc outlet,pd.prepaiddisposeoid,pd.prepaiddisposeno,pm.prepaidmstcode,pm.prepaidmstdesc,pd.prepaiddisposedate,pd.prepaiddisposestatus FROM QL_trnprepaiddispose pd INNER JOIN QL_trnprepaidmst pm ON pm.prepaidmstoid=pd.prepaiddisposerefoid AND pd.branch_code=pm.branch_code INNER JOIN ql_mstgen ol ON ol.gencode=pd.branch_code AND ol.gengroup='CABANG'"

        If cbPeriode.Checked Then
            sSql &= "AND pd.prepaiddisposedate BETWEEN '" & CDate(toDate(txtPeriode1.Text)) & "' AND '" & CDate(toDate(txtPeriode2.Text)) & "' "
            If cbDesc.Checked Then
                sSql &= "AND pd.prepaiddisposeno LIKE '%" & Tchar(txtFilter.Text) & "%' "
                If cbBlmPosting.Checked Then
                    sSql &= "AND pd.prepaiddisposestatus <> 'POST' "
                End If
            ElseIf cbBlmPosting.Checked Then
                sSql &= "AND pd.prepaiddisposestatus <> 'POST' "
            End If
        ElseIf cbDesc.Checked Then
            sSql &= "AND pd.prepaiddisposeno LIKE '%" & Tchar(txtFilter.Text) & "%' "
            If cbBlmPosting.Checked Then
                sSql &= "AND pd.prepaiddisposestatus <> 'POST' "
            End If
        ElseIf cbBlmPosting.Checked Then
            sSql &= "AND pd.prepaiddisposestatus <> 'POST' "
        End If

        If CabangNya.SelectedValue <> "ALL" Then
            sSql &= " AND pd.branch_code='" & CabangNya.SelectedValue & "'"
        End If
        sSql &= "ORDER BY pd.prepaiddisposeno desc"

        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnprepaiddispose")
        Session("tbldata") = objTable
        GVPrePaid.DataSource = objTable
        GVPrePaid.DataBind()
    End Sub

    Public Sub BindDataMst()
        sSql &= "SELECT pm.cmpcode,ol.gendesc as outletname,acm.acctgoid mstacctg,(acm.acctgcode +' - '+acm.acctgdesc ) AS prepaidmstacctgoid,ac.acctgoid payacctg,(ac.acctgcode +' - '+ac.acctgdesc) AS prepaidmstpayacctgoid,pm.prepaidmstoid, pm.prepaidmstcode,pm.prepaidmstdesc,CONVERT(CHAR(10),pm.prepaidmstdate,103) prepaidmstdate,pm.prepaidmstamount PrepaidAmt,pm.prepaidmsttotalamount as BookValue,pm.prepaidmstvalue Accumulation,pm.prepaidmstlengthvalue,pm.prepaidmstaccumamount FROM QL_trnprepaidmst pm INNER JOIN QL_mstgen ol ON ol.gencode=pm.branch_code AND ol.gengroup='CABANG' INNER JOIN QL_mstacctg acm ON acm.acctgoid=pm.prepaidmstacctgoid INNER JOIN QL_mstacctg ac ON ac.acctgoid=pm.prepaidmstpayacctgoid WHERE pm.branch_code='" & DDLCabang.SelectedValue & "' AND pm.prepaidmststatus='POST' AND (pm.prepaidmstcode LIKE '%" & Tchar(PrepaidCode.Text.Trim) & "%' OR pm.prepaidmstdesc LIKE '%" & Tchar(PrepaidCode.Text.Trim) & "%') AND pm.prepaidmststatus NOT IN ('DISPOSED')"
        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnPrePaidmst")
        Session("tblMst") = objTable
        GvPrePaidmst.DataSource = objTable
        GvPrePaidmst.DataBind()
    End Sub

    Public Sub BindPrePaidDtl()
        Dim sMonth As Integer = Format(GetServerTime(), "MM")
        Dim sYear As Integer = Format(GetServerTime(), "yyyy")
        sSql = "SELECT cmpcode,prepaidmstoid,CONVERT(char(10),prepaiddtldate,103) as prepaiddtldate,prepaiddtlamount,MONTH(prepaiddtldate) as sMonth,prepaiddtlaccumamt,prepaiddtlaccum,prepaiddtlstatus FROM QL_trnprepaiddtl WHERE branch_code='" & DDLCabang.SelectedValue & "' AND prepaidmstoid=" & PrepaidGVoid.Text & " AND MONTH(prepaiddtldate) >= " & sMonth & " AND YEAR(prepaiddtldate) >= " & sYear & ""
        FillGV(GVPrePaiddtl, sSql, "QL_trnprepaiddtl")
        Dim DtlTbl As DataTable = GetDataTable(sSql, "QL_trnprepaiddtl")
        Session("DtlTbl") = DtlTbl
        GVPrePaiddtl.DataBind()
        GVPrePaiddtl.Visible = True
    End Sub

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DDLCabang, sSql)
            Else
                FillDDL(DDLCabang, sSql)
                DDLCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLCabang, sSql)
            DDLCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                CabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            CabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub initAllDDL()
        FillDDLAcctg(prepaidmstacctgoid, "VAR_AMORTY", DDLCabang.SelectedValue)
        FillDDLAcctg(prepaidmstpayacctgoid, "VAR_ACCUMULATION_AMORTYTATION", DDLCabang.SelectedValue)
    End Sub

    Private Sub fillTextBox(ByVal sCmpcode As String, ByVal vjurnaloid As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT pd.cmpcode,pd.branch_code,ol.gendesc outlet,pd.prepaiddisposeoid,pd.prepaiddisposeno,pm.prepaidmstcode,pd.prepaiddisposerefoid,pm.prepaidmstdesc,pd.prepaiddisposedate,pd.prepaiddisposestatus,pm.prepaidmstamount,pm.prepaidmsttotalamount,pm.prepaidmstlengthvalue,pd.crtuser,pd.crttime,pd.upduser,pd.updtime,pm.prepaidmstacctgoid mstacctg,pm.prepaidmstpayacctgoid payacctg FROM QL_trnprepaiddispose pd INNER JOIN QL_trnprepaidmst pm ON pm.prepaidmstoid=pd.prepaiddisposerefoid AND pd.cmpcode=pm.cmpcode INNER JOIN QL_mstgen ol ON ol.gencode=pd.branch_code AND ol.gengroup='CABANG' WHERE pd.prepaiddisposeoid=" & vjurnaloid & " AND pd.branch_code='" & sCmpcode & "'"

        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If objRow.Length > 0 Then

            DDLCabang.SelectedValue = Trim(objRow(0)("branch_code").ToString)
            PrePaidmstoid.Text = Trim(objRow(0)("prepaiddisposeoid").ToString)
            PrePaidDisposeNo.Text = Trim(objRow(0)("prepaiddisposeno").ToString)
            PrePaidDate.Text = Format(objRow(0)("prepaiddisposedate"), "dd/MM/yyyy")
            PrepaidGVoid.Text = Trim(objRow(0)("prepaiddisposerefoid").ToString)
            BindPrePaidDtl()
            PrepaidCode.Text = Trim(objRow(0)("prepaidmstcode").ToString)
            PrePaiddesc.Text = Trim(objRow(0)("prepaidmstdesc").ToString)
            prepaidmstacctgoid.SelectedValue = Trim(objRow(0)("mstacctg").ToString)
            prepaidmstpayacctgoid.SelectedValue = Trim(objRow(0)("payacctg").ToString)
            BookValue.Text = ToMaskEdit(Trim(objRow(0)("prepaidmsttotalamount").ToString), 0)
            PrePaidAmt.Text = ToMaskEdit(Trim(objRow(0)("prepaidmstamount").ToString), 0)
            Dim depp As Integer = CInt(Trim(objRow(0)("prepaidmstlengthvalue").ToString))
            sSql = "SELECT COUNT(*) from QL_trnprepaiddtl WHERE cmpcode='" & Session("CompnyCode") & "' AND prepaidmstoid=" & PrepaidGVoid.Text & " AND prepaiddtlstatus='POST'"
            PrePaiddepmonth.Text = depp - GetStrData(sSql)
            fixdepval.Text = ToMaskEdit(PrePaidAmt.Text - BookValue.Text, 0)
            lblUser.Text = Trim(objRow(0)("crtuser").ToString)
            lblTime.Text = Trim(objRow(0)("crttime").ToString)
            upduser.Text = Trim(objRow(0)("upduser").ToString)
            updtime.Text = Trim(objRow(0)("updtime").ToString)

            If Trim(objRow(0)("prepaiddisposestatus").ToString.ToUpper) <> "POST" Then
                btnSave.Visible = True
                btnDelete.Visible = True
                btnPosting.Visible = True
            Else
                btnSave.Visible = False
                btnDelete.Visible = False
                btnPosting.Visible = False
                PrePaiddepmonth.Enabled = False
            End If
        End If
        mySqlConn.Close()

        DDLCabang.Enabled = False
        DDLCabang.CssClass = "inpTextDisabled"
    End Sub

    Private Sub GenerateMstOid()
        PrePaidmstoid.Text = GenerateID("QL_trnprepaiddispose", DDLCabang.SelectedValue)
    End Sub

    Private Sub GenerateDtlOid()
        Session("dtloid") = GenerateID("QL_trnprepaiddtl", DDLCabang.SelectedValue)
    End Sub

    Private Sub GeneratePPDno()
        ' Cashbank No
        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & DDLCabang.SelectedValue & "' AND gengroup='CABANG'")
        If lblPOST.Text = "POST" Then
            Dim iCurID As Integer = 0
            Session("sNo") = "PPD/" & Cabang & "/" & Format(GetServerTime(), "mm/dd/yyyy") & "/"
            sSql = "SELECT isnull(MAX(replace(prepaiddisposeno,'" & Session("sNo") & "','')),0) prepaiddisposeno FROM QL_trnprepaiddispose WHERE prepaiddisposeno LIKE '" & Session("sNo") & "%' AND cmpcode='" & DDLCabang.SelectedValue & "'"
            xCmd.CommandText = sSql
            If Not IsDBNull(xCmd.ExecuteScalar) Then
                iCurID = xCmd.ExecuteScalar + 1
            Else
                iCurID = 1
            End If
            Session("sNo") = GenNumberString(Session("sNo"), "", iCurID, 4)
        Else
            Session("sNo") = Cabang & "/" & PrePaidmstoid.Text
        End If
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("sCmpcode") = CompnyName
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnPrePaidDispose.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Pre-Paid Dispose"
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        If Not IsPostBack Then
            fDDLBranch() : InitDDLBranch() : initAllDDL()
            binddata()
            Dim CutOffDate As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
                Exit Sub
            Else
                CutOffDate = toDate(GetStrData(sSql))
                Session("CutOffDate") = toDate(GetStrData(sSql))
            End If
            'ShowMessage(CUTOFFDATE.AddDays(-1) , 2)
            CutOffDatePaid.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
            txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            lblViewInfo.Visible = True
            PrepaidGVoid.Text = 0
            'payflag_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(Session("sCmpcode"), Session("oid"))
                lblPOST.Text = "IN PROCESS"
                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy")
                PrePaidDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                BindPrePaidDtl()
                TabContainer1.ActiveTabIndex = 1
            Else
                GenerateMstOid() : GeneratePPDno()
                BtnDelete.Visible = False
                PrePaidDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                'PrePaidDueDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                PrePaidDisposeNo.Text = Session("sNo")
                PrePaidAmt.Text = "0.00"
                BookValue.Text = "0.00"
                NilaiJual.Text = "0.00"
                fixdepval.Text = "0.00"
                DiffValue.Text = "0.00"
                upduser.Text = "-"
                updtime.Text = "-"
                lblUser.Text = Session("UserID")
                lblTime.Text = GetServerTime()
                lblPOST.Text = "IN PROCESS"
                TabContainer1.ActiveTabIndex = 0
            End If

        End If
    End Sub

    Protected Sub GVPrePaid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnPrePaidDispose.aspx?cmpcode=" & GVPrePaid.SelectedDataKey("branch_code").ToString & "&oid=" & GVPrePaid.SelectedDataKey("prepaiddisposeoid").ToString & "")
    End Sub

    Protected Sub GVPrePaid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVPrePaid.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub GVPrePaid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVPrePaid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub GVPrePaiddtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVPrePaiddtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 2)
            e.Row.Cells(2).Text = ToMaskEdit(e.Row.Cells(2).Text, 2)
        End If
    End Sub

    Protected Sub GVPrePaiddtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objTable As DataTable
        objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "prepaiddtldate='" & GVPrePaiddtl.SelectedDataKey("prepaiddtldate").ToString & "'"
        dv.RowFilter = "prepaiddtlstatus = 'IN PROCESS'"
        dv.RowFilter = ""
    End Sub

    Protected Sub GVPrePaiddtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVPrePaiddtl.PageIndex = e.NewPageIndex
        GVPrePaiddtl.DataSource = Session("tbldtl")
        GVPrePaiddtl.DataBind()
    End Sub

    Protected Sub GVDtlMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
        End If
    End Sub

    Protected Sub GvPrePaidmst_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PrepaidGVoid.Text = GvPrePaidmst.SelectedDataKey("prepaidmstoid").ToString().Trim
        PrepaidCode.Text = GvPrePaidmst.SelectedDataKey("prepaidmstcode").ToString().Trim
        PrePaiddesc.Text = GvPrePaidmst.SelectedDataKey("prepaidmstdesc").ToString().Trim
        AkunPrePaidDate.Text = GvPrePaidmst.SelectedDataKey("prepaidmstdate").ToString().Trim
        fixdepval.Text = GvPrePaidmst.SelectedDataKey("Accumulation").ToString().Trim
        BookValue.Text = NewMaskEdit(GvPrePaidmst.SelectedDataKey("BookValue").ToString().Trim)
        PrePaidAmt.Text = NewMaskEdit(GvPrePaidmst.SelectedDataKey("PrepaidAmt").ToString().Trim)
        prepaidmstacctgoid.SelectedValue = GvPrePaidmst.SelectedDataKey("mstacctg").ToString().Trim
        prepaidmstpayacctgoid.SelectedValue = GvPrePaidmst.SelectedDataKey("payacctg").ToString().Trim
        Dim dep As String = GvPrePaidmst.SelectedDataKey("prepaidmstlengthvalue").ToString().Trim
        sSql = "SELECT COUNT(*) from QL_trnprepaiddtl WHERE branch_code='" & DDLCabang.SelectedValue & "' AND prepaidmstoid=" & PrepaidGVoid.Text & " AND prepaiddtlstatus='POST'"
        PrePaiddepmonth.Text = dep - GetStrData(sSql)
        fixdepval.Text = ToMaskEdit(PrePaidAmt.Text - BookValue.Text, 0)
        BindPrePaidDtl()
        GvPrePaidmst.Visible = False

    End Sub

    Protected Sub GvPrePaidmst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GvPrePaidmst.PageIndex = e.NewPageIndex
        BindDataMst()
    End Sub

    Protected Sub PrePaiddepmonth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrePaiddepmonth.TextChanged
        ReAmountDep()
    End Sub

    Protected Sub DDLoutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateMstOid()
        GenerateDtlOid()
    End Sub  

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnPrePaidDispose.aspx?awal=true")
    End Sub

    Protected Sub btnHapus1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'FAPMst.Text = ""
        'FAPmstoid.Text = ""
        'PrePaidGroup.SelectedValue = Nothing
        'PrePaidGroup.CssClass = "inpText"
        'PrePaidGroup.Enabled = True
        'PrePaiddepmonth.Text = "-1"
        'GvPrePaidmst.Visible = False
        btnHapus2_Click(sender, e)
    End Sub

    Protected Sub btnCari2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataMst()
        GvPrePaidmst.Visible = True
        GVPrePaiddtl.Visible = False
    End Sub

    Protected Sub btnHapus2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrepaidGVoid.Text = 0
        PrepaidCode.Text = ""
        PrePaiddesc.Text = ""
        PrePaidDate.Text = Format(GetServerTime(), "dd/MM/yyyy")
        PrePaidAmt.Text = "0.00"
        BookValue.Text = "0.00"
        fixdepval.Text = "0.00"
        NilaiJual.Text = "0.00"
        DiffValue.Text = "0.00"
        initAllDDL()
        GvPrePaidmst.Visible = False
        GVPrePaiddtl.Visible = False
    End Sub

#End Region

    Sub setcontrolCash(ByVal status As Boolean)
        Label13.Visible = status
        btnDueDate.Visible = status
        Label19.Visible = status
        Label27.Visible = status
        PrePaidRefno.Visible = status
    End Sub

    Protected Sub NilaiJual_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        NilaiJual.Text = ToMaskEdit(NilaiJual.Text, 0)
        If NilaiJual.Text <> "" Or NilaiJual.Text <> "0.00" Then
            DiffValue.Text = ToMaskEdit(NilaiJual.Text - PrePaidAmt.Text, 0)
        End If
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False : panelMsg.Visible = False
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSave.Click
        Dim sMsg As String = ""
        Dim sMonth As Integer = Format(GetServerTime(), "MM")
        Dim sYear As Integer = Format(GetServerTime(), "yyyy")
        Dim sDate As String = Format(GetServerTime(), "dd/MM/yyyy")

        If IsValidPeriod() Then
            If CDate(toDate(PrePaidDate.Text)) <= CDate(toDate(CutOffDatePaid.Text)) Then
                sMsg &= "- Tanggal tidak boleh kurang dari CutOffDate tanggal '" & CutOffDatePaid.Text & "' !<BR>"
            Else
                'If Not CheckClosingAcctgStatus(CDate(toDate(PrePaidDate.Text)), DDLCabang.SelectedValue) Then
                '    sMsg &= "- Periode Accounting untuk tanggal yang dipilih tidak tersedia atau sudah Closed !!<BR>"
                'End If
            End If
        End If

        'If Not IsValidDate(PrePaidDate.Text, "dd/MM/yyyy") Then
        '    sMsg &= "- Pre-Paid Date is invalid..<BR>"
        'End If

        Dim objdtl As DataTable = Session("DtlTbl")
        If objdtl Is Nothing Then
            sMsg &= "- Akun Prep-Paid Belum Dipilih..!!<BR>"
        ElseIf PrepaidCode.Text.Trim = "" Or PrepaidGVoid.Text = "" Then
            sMsg &= "- Akun Prep-Paid Belum Dipilih..!!<BR>"
        End If

        If Not Session("DtlTbl") Is Nothing Then
            Dim objTblDetail As DataTable
            Dim objRowDetail() As DataRow
            objTblDetail = Session("DtlTbl")
            objRowDetail = objTblDetail.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowDetail.Length = 0 Then
                sMsg &= "- Tidak ada detail transaksi..!!<BR>"
            End If
        Else
            sMsg &= "- Tidak ada data detail transaksi..!!<BR>"
        End If

        sSql = "SELECT COUNT(-1) FROM QL_trnprepaiddispose WHERE prepaiddisposerefoid = '" & PrepaidGVoid.Text & "' AND cmpcode='" & DDLCabang.SelectedValue & "' AND prepaiddisposestatus NOT IN ('DELETE') "
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            sSql &= " AND prepaiddisposeoid <> '" & Session("oid") & "' "
        End If
        Dim sCheckrefoid As Integer = GetStrData(sSql)
        If sCheckrefoid > 0 Then
            sMsg &= "- No. Dispose Sudah pernah di input..!!<BR>"
        End If
        'End If

        sSql = "SELECT ISNULL(COUNT(prepaiddtlstatus),0) AS ppd FROM QL_trnprepaiddtl WHERE cmpcode='" & Session("CompnyCode") & "' AND prepaidmstoid=" & PrepaidGVoid.Text & " AND prepaiddtlstatus='POST' AND month(prepaiddtldate) = " & sMonth & " AND year(prepaiddtldate) = " & sYear & ""

        Dim periodPost As String = GetStrData(sSql)
        If periodPost <> 0 Then
            sMsg &= "- Periode sekarang sudah di posting, silahkan Pilih transaksi lain !!<BR>"
        End If

        sSql = "SELECT ISNULL(COUNT(Month(prepaiddtldate)),0) ppd FROM QL_trnprepaiddtl WHERE cmpcode='" & Session("ComnpyCode") & "' AND prepaiddtldate < " & sMonth & " AND prepaidmstoid=" & PrepaidGVoid.Text & " AND prepaiddtlstatus='IN PROCESS' "

        Dim periodIP As String = GetStrData(sSql)
        If periodIP > 0 Then
            sMsg &= "- Periode sebelumnya belum di posting, silahkan Posting Dulu!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "IN PROCESS"
            Exit Sub
        End If

        ' Generate oid
        Dim MstOid As String = GenerateID("QL_trnprepaiddispose", "MSC")
        Dim vGlMst As String = GenerateID("QL_trnglmst", "MSC")
        Dim vGlDtl As String = GenerateID("QL_trngldtl", "MSC")

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        If lblPOST.Text = "POST" Then
            GeneratePPDno()
        End If
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnprepaiddispose (cmpcode,branch_code,prepaiddisposeoid,prepaiddisposeno,prepaiddisposedate,prepaiddisposerefoid,prepaiddisposenote,prepaiddisposestatus,prepaiddisposeres1,prepaiddisposeres2,crtuser,crttime,upduser,updtime,syncflag,synctime) VALUES ('MSC','" & DDLCabang.SelectedValue & "', " & MstOid & ",'" & Tchar(PrePaidDisposeNo.Text) & "', '" & CDate(toDate(PrePaidDate.Text)) & "', " & PrepaidGVoid.Text & ",'PrePaid - DISPOSAL(" & PrepaidCode.Text & ")-" & Session("sNo") & "','" & lblPOST.Text & "','','','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'','1/1/1900')"

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & MstOid & " WHERE tablename='QL_trnprepaiddispose' AND cmpcode='" & DDLCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                ' Update
                sSql = "UPDATE QL_trnprepaiddispose SET prepaiddisposeno = '" & Tchar(PrePaidDisposeNo.Text) & "', prepaiddisposedate = '" & CDate(toDate(PrePaidDate.Text)) & "', prepaiddisposerefoid=" & PrepaidGVoid.Text & ", prepaiddisposenote = 'PrePaid - DISPOSAL(" & PrepaidCode.Text & ")-" & Session("sNo") & "', prepaiddisposestatus = '" & lblPOST.Text & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP, syncflag = '' WHERE prepaiddisposeoid=" & PrePaidmstoid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If lblPOST.Text = "POST" Then
                'GeneratePPDno()
                sSql = "UPDATE QL_trnprepaiddispose SET prepaiddisposeno = '" & Session("sNo") & "', prepaiddisposestatus = '" & lblPOST.Text & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP, syncflag = '' WHERE prepaiddisposeoid=" & PrePaidmstoid.Text & " AND cmpcode='" & DDLCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnprepaidmst SET prepaidmststatus='DISPOSED', syncflag = '' WHERE cmpcode='" & DDLCabang.SelectedValue & "' AND prepaidmstoid='" & PrepaidGVoid.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_trnprepaiddtl SET prepaiddtlstatus='CLOSED' WHERE cmpcode='" & DDLCabang.SelectedValue & "' AND prepaidmstoid='" & PrepaidGVoid.Text & "' AND prepaiddtlstatus='IN PROCESS'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ''INSERT QL_trnglmst
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,type) VALUES ('" & DDLCabang.SelectedValue & "'," & vGlMst & ",'" & CDate(toDate(PrePaidDate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(PrePaidDate.Text))) & "','PrePaid-DISPOSAL(" & PrepaidCode.Text & ")-" & Session("sNo") & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'PPDISPOSAL')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ''insert data GL DTL D
                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,glother2,branch_code) VALUES ('" & DDLCabang.SelectedValue & "'," & vGlDtl & ",1," & vGlMst & "," & prepaidmstacctgoid.SelectedValue & ",'D'," & ToDouble(BookValue.Text) & ",'" & Session("sNo") & "','PrePaid-DISPOSAL(" & PrepaidCode.Text & ")-" & Session("sNo") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & MstOid & "','" & DDLCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                vGlDtl += 1

                ''insert data GL DTL C
                sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,upduser,updtime,glother2,branch_code) VALUES ('" & DDLCabang.SelectedValue & "'," & vGlDtl & ",2," & vGlMst & "," & prepaidmstpayacctgoid.SelectedValue & ",'C'," & ToDouble(BookValue.Text) & ",'" & Session("sNo") & "','PrePaid-DISPOSAL(" & PrepaidCode.Text & ")-" & Session("sNo") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & MstOid & "','" & DDLCabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                vGlDtl += 1

                sSql = "UPDATE QL_mstoid SET lastoid=" & vGlMst & " WHERE tablename='QL_trnglmst' AND cmpcode='" & DDLCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & vGlDtl & " WHERE tablename='QL_trngldtl' AND cmpcode='" & DDLCabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            lblPOST.Text = "IN PROCESS"
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Response.Redirect("trnPrePaidDispose.aspx")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            ' delete mst
            sSql = "UPDATE QL_trnprepaiddispose SET prepaiddisposestatus= 'DELETE', syncflag= '' WHERE prepaiddisposeoid=" & PrePaidmstoid.Text & " AND cmpcode='" & DDLCabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback() : objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnPrePaidDispose.aspx")
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        BtnSave.Enabled = True
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnPrePaidDispose.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPosting.Click
        lblPOST.Text = "POST" : BtnSave_Click(sender, e)
    End Sub

    Protected Sub BtnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnFind.Click
        Session("tbldata") = Nothing
        GVPrePaid.PageIndex = 0
        Session("SearchPrePaid") = sql_temp
        lblViewInfo.Visible = False
        binddata()
    End Sub

    Protected Sub BtnViewALL_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewALL.Click
        txtFilter.Text = "" : cbPeriode.Checked = False
        cbDesc.Checked = False : cbBlmPosting.Checked = False
        txtPeriode1.Text = Format(GetServerTime(), "01/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        lblViewInfo.Visible = False
        binddata()
    End Sub
End Class
