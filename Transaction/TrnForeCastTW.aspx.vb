Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnForeCastTW
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate((FilterPeriod1.Text), "MM/dd/yyyy", "") Then
            showMessage("Format tanggal Period 1 salah..!!" & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate((FilterPeriod2.Text), "MM/dd/yyyy", "") Then
            showMessage("Format tanggal Period 2 salah..!!" & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Period 2 harus lebih besar dari Period 1..!!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsExistsMatData() As Boolean
        Dim sError As String = ""
        sSql = "SELECT COUNT(*) FROM QL_mstmatraw WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If ToDouble(GetStrData(sSql)) = 0 Then
            sError &= "There is no Raw Material data. Please input material data first or contact your administrator..!!"
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If

        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If itemoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If trnrequestitemdtlseq.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(trnrequestitemdtlseq.Text) <= 0 Then
                sError &= "- QUANTITY field must be more than 0!<BR>"
            End If
        End If
        Dim sErr As String = ""
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = "" : Dim sErr As String = ""
        If trnrequestitemnote.Text.Length > 100 Then
            sError &= "- Header Note maximal 100 character..!!<BR>"
        End If

        If Session("TblDtl") Is Nothing Then
            sError &= "- Maaf, Detail tidak ada..!!<BR>"
        End If

        If Not Session("TblDtl") Is Nothing Then
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Maaf, Detail tidak ada..!!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            trnrequestitemstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("trnrequestitemdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("trnrequestitemdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView(0)("ReqQty") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView(0)("trnrequestitemdtlnote") = GetTextBoxValue(row.Cells(8).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsFolderExists(ByVal sFolder As String) As Boolean
        Dim fso = CreateObject("Scripting.FileSystemObject")
        If (fso.FolderExists(sFolder)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub AddNewFolder(ByVal sFolder As String)
        Dim fso, f, fc, nf
        fso = CreateObject("Scripting.FileSystemObject")
        f = fso.GetFolder(Server.MapPath("~/"))
        fc = f.SubFolders
        nf = fc.Add(sFolder)
    End Sub

    Private Overloads Function CheckMaterial(ByRef sOid As String, ByVal sCode As String) As Boolean
        sSql = "SELECT TOP 1 itemoid FROM ql_mstitem m WHERE cmpcode='" & CompnyCode & "' AND itemcode='" & sCode & "' AND m.activeflag='ACTIVE'"
        sOid = GetStrData(sSql)
        If sOid = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Overloads Function CheckUnit(ByRef sOid As String, ByVal sCode As String) As Boolean
        sSql = "SELECT TOP 1 genoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='MATERIAL UNIT' AND gendesc='" & sCode & "' AND activeflag='ACTIVE'"
        sOid = GetStrData(sSql)
        If sOid = "" Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Procedures"
    Public Sub GenerateBulan(ByVal bln1 As String, ByVal note As String)
        If bln1 = "1" Then
            If note = "satu" Then
                lblbln1.Text = "Januari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Januari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Januari"
            End If
        ElseIf bln1 = "2" Then
            If note = "satu" Then
                lblbln1.Text = "Februari"
            ElseIf note = "dua" Then
                lblbln2.Text = "Februari"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Februari"
            End If
        ElseIf bln1 = "3" Then
            If note = "satu" Then
                lblbln1.Text = "Maret"
            ElseIf note = "dua" Then
                lblbln2.Text = "Maret"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Maret"
            End If
        ElseIf bln1 = "4" Then
            If note = "satu" Then
                lblbln1.Text = "April"
            ElseIf note = "dua" Then
                lblbln2.Text = "April"
            ElseIf note = "tiga" Then
                lblbln3.Text = "April"
            End If
        ElseIf bln1 = "5" Then
            If note = "satu" Then
                lblbln1.Text = "Mei"
            ElseIf note = "dua" Then
                lblbln2.Text = "Mei"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Mei"
            End If
        ElseIf bln1 = "6" Then
            If note = "satu" Then
                lblbln1.Text = "Juni"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juni"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juni"
            End If
        ElseIf bln1 = "7" Then
            If note = "satu" Then
                lblbln1.Text = "Juli"
            ElseIf note = "dua" Then
                lblbln2.Text = "Juli"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Juli"
            End If
        ElseIf bln1 = "8" Then
            If note = "satu" Then
                lblbln1.Text = "Agustus"
            ElseIf note = "dua" Then
                lblbln2.Text = "Agustus"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Agustus"
            End If
        ElseIf bln1 = "9" Then
            If note = "satu" Then
                lblbln1.Text = "September"
            ElseIf note = "dua" Then
                lblbln2.Text = "September"
            ElseIf note = "tiga" Then
                lblbln3.Text = "September"
            End If
        ElseIf bln1 = "10" Then
            If note = "satu" Then
                lblbln1.Text = "Oktober"
            ElseIf note = "dua" Then
                lblbln2.Text = "Oktober"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Oktober"
            End If
        ElseIf bln1 = "11" Then
            If note = "satu" Then
                lblbln1.Text = "November"
            ElseIf note = "dua" Then
                lblbln2.Text = "November"
            ElseIf note = "tiga" Then
                lblbln3.Text = "November"
            End If
        ElseIf bln1 = "12" Then
            If note = "satu" Then
                lblbln1.Text = "Desember"
            ElseIf note = "dua" Then
                lblbln2.Text = "Desember"
            ElseIf note = "tiga" Then
                lblbln3.Text = "Desember"
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckPRStatus()
        'Dim nDays As Integer = 7
        'sSql = "SELECT COUNT(*) FROM QL_prrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND prrawmststatus='In Process'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If

        'If ToDouble(GetStrData(sSql)) > 0 Then
        '    lkbPRInProcess.Visible = True
        '    lkbPRInProcess.Text = "You have " & GetStrData(sSql) & " In Process PR Raw Material that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        'End If

        'sSql = "SELECT COUNT(*) FROM QL_prrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND prrawmststatus='In Approval'"
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        'End If

        'If ToDouble(GetStrData(sSql)) > 0 Then
        '    lkbPRInApproval.Visible = True
        '    lkbPRInApproval.Text = "You have " & GetStrData(sSql) & " In Approval PR Raw Material that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        'End If
    End Sub

    Private Sub InitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DdlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(DdlCabang, sSql)
            Else
                FillDDL(DdlCabang, sSql)
                DdlCabang.SelectedValue = "10"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DdlCabang, sSql)
            DdlCabang.SelectedValue = "10"
        End If
    End Sub

    Private Sub InitCbKirim()
        sSql = "SELECT gencode,gendesc from ql_mstgen Where gengroup='CABANG' AND gencode<>'" & DdlCabang.SelectedValue & "'"
        FillDDL(DDLCbKirim, sSql)
    End Sub

    Private Sub fInitCbKirim()
        sSql = "SELECT gencode,gendesc from ql_mstgen Where gengroup='CABANG'"
        If fCbangDDL.SelectedValue <> "ALL" Then
            sSql &= "AND gencode<>'" & fCbangDDL.SelectedValue & "'"
        End If
        FillDDL(DDLToCabang, sSql)
        DDLToCabang.Items.Add(New ListItem("ALL", "ALL"))
        DDLToCabang.SelectedValue = "ALL"
    End Sub

    Private Sub fInitCabang()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCbangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCbangDDL, sSql)
            Else
                FillDDL(fCbangDDL, sSql)
                fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
                fCbangDDL.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fCbangDDL, sSql)
            fCbangDDL.Items.Add(New ListItem("ALL", "ALL"))
            fCbangDDL.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT pbm.cmpcode,pbm.trnrequestitemoid,branchcode,cb.gendesc,ct.gendesc CbKirim,trnrequestitemno,Convert(Varchar(20),trnrequestitemdate,103) trnrequestitemdate,trnrequestitemstatus FROM QL_trnrequestitem pbm INNER JOIN QL_mstgen cb ON cb.gencode=pbm.branchcode AND cb.gengroup='CABANG' INNER JOIN QL_mstgen ct ON ct.gencode=pbm.tobranch AND ct.gengroup='CABANG' " & sSqlPlus & ""
        If fCbangDDL.SelectedValue <> "ALL" Then
            sSql &= " AND branchcode='" & fCbangDDL.SelectedValue & "'"
        End If
        If DDLToCabang.SelectedValue <> "ALL" Then
            sSql &= "AND tobranch='" & DDLToCabang.SelectedValue & "'"
        End If

        If cbStatus.Checked = True Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSql &= " AND trnrequestitemstatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        sSql &= " ORDER BY pbm.trnrequestitemoid Desc"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnrequestitem")
        GvTrn.DataSource = dt : GvTrn.DataBind()
        GvTrn.Visible = True
    End Sub

    Private Sub BindMatData()
        Dim Year1 As String = Year(DateAdd(DateInterval.Month, -1, GetServerTime()))
        Dim year2 As String = Year(DateAdd(DateInterval.Month, -2, GetServerTime()))
        Dim year3 As String = Year(DateAdd(DateInterval.Month, -3, GetServerTime()))

        Dim bln1 As String = Month(GetServerTime()) - 1
        Dim bln2 As String = Month(GetServerTime()) - 2
        Dim bln3 As String = Month(GetServerTime()) - 3

        If bln1 <= 0 Then
            bln1 = bln1 + 12
        End If
        If bln2 <= 0 Then
            bln2 = bln2 + 12
        End If
        If bln3 <= 0 Then
            bln3 = bln3 + 12
        End If

        GenerateBulan(bln1, "satu")
        GenerateBulan(bln2, "dua")
        GenerateBulan(bln3, "tiga")

        gvListMat.Columns(5).HeaderText = lblbln1.Text
        gvListMat.Columns(4).HeaderText = lblbln2.Text
        gvListMat.Columns(3).HeaderText = lblbln3.Text

        sSql = "SELECT *,(bln1+bln2+bln3)/3 TotalNya,(bln1+bln2+bln3*3)/3 AvgNya FROM (SELECT 'False' AS checkvalue, itemoid, itemcode, itemdesc, ISNULL(SUM(con.qtyIn),0.00) QtyIn, ISNULL(SUM(qtyOut),0.00) QtyOut, ISNULL(SUM(con.qtyIn),0.00)-ISNULL(SUM(qtyOut),0.00) SaldoAkhir,0.00 ReqQty,statusitem jenis,'' trnrequestitemdtlnote," & trnrequestitemdtlseq.Text & " trnrequestitemdtlseq,Isnull((SELECT SUM(jd.trnjualdtlqty) FROM QL_trnjualdtl jd Inner Join QL_trnjualmst j On jd.trnjualmstoid=j.trnjualmstoid WHERE (jd.itemoid = i.itemoid And Month(j.trnjualdate)=" & bln1 & ") AND YEAR(j.trnjualdate) = " & Year1 & " AND j.trnjualstatus='POST' AND j.orderno NOT IN (Select so.orderno from QL_trnordermst so Where so.typeSO='Konsinyasi' AND so.ordermstoid=j.ordermstoid AND so.branch_code=jd.branch_code) AND j.branch_code='" & DdlCabang.SelectedValue & "'),0.00) - Isnull((Select SUM(jrd.trnjualdtlqty) FROM QL_trnjualreturmst jrm INNER JOIN QL_trnjualreturdtl jrd ON jrm.trnjualreturmstoid=jrd.trnjualreturmstoid AND jrm.branch_code=jrd.branch_code Where jrd.itemoid=i.itemoid AND MONTH(jrm.trnjualdate)=" & bln1 & " AND YEAR(jrm.trnjualdate)=" & Year1 & " AND jrm.trnjualstatus IN ('APPROVED','POST') AND jrm.typeSO<>'Konsinyasi' AND jrm.branch_code='" & DdlCabang.SelectedValue & "'),0.00) as 'bln1'" & _
        ",Isnull((SELECT SUM(jd.trnjualdtlqty) FROM QL_trnjualdtl jd Inner Join QL_trnjualmst j On jd.trnjualmstoid=j.trnjualmstoid WHERE (jd.itemoid = i.itemoid And Month(j.trnjualdate)=" & bln2 & ") AND YEAR(j.trnjualdate) = " & year2 & " AND j.trnjualstatus='POST' AND j.orderno NOT IN (Select so.orderno from QL_trnordermst so Where so.typeSO='Konsinyasi' AND so.ordermstoid=j.ordermstoid AND so.branch_code=jd.branch_code) AND j.branch_code='" & DdlCabang.SelectedValue & "'),0.00) - Isnull((Select SUM(jrd.trnjualdtlqty) FROM QL_trnjualreturmst jrm INNER JOIN QL_trnjualreturdtl jrd ON jrm.trnjualreturmstoid=jrd.trnjualreturmstoid AND jrm.branch_code=jrd.branch_code Where jrd.itemoid=i.itemoid AND MONTH(jrm.trnjualdate)=" & bln2 & " AND YEAR(jrm.trnjualdate)=" & Year1 & " AND jrm.trnjualstatus IN ('APPROVED','POST') AND jrm.typeSO<>'Konsinyasi' AND jrm.branch_code='" & DdlCabang.SelectedValue & "'),0.00) as 'bln2'" & _
        ",Isnull((SELECT SUM(jd.trnjualdtlqty) FROM QL_trnjualdtl jd Inner Join QL_trnjualmst j On jd.trnjualmstoid=j.trnjualmstoid WHERE (jd.itemoid = i.itemoid And Month(j.trnjualdate)=" & bln3 & ") AND YEAR(j.trnjualdate) = " & year3 & " AND j.trnjualstatus='POST' AND j.orderno NOT IN (Select so.orderno from QL_trnordermst so Where so.typeSO='Konsinyasi' AND so.ordermstoid=j.ordermstoid AND so.branch_code=jd.branch_code) AND j.branch_code='" & DdlCabang.SelectedValue & "'),0.00) - Isnull((Select SUM(jrd.trnjualdtlqty) FROM QL_trnjualreturmst jrm INNER JOIN QL_trnjualreturdtl jrd ON jrm.trnjualreturmstoid=jrd.trnjualreturmstoid AND jrm.branch_code=jrd.branch_code Where jrd.itemoid=i.itemoid AND MONTH(jrm.trnjualdate)=" & bln3 & " AND YEAR(jrm.trnjualdate)=" & Year1 & " AND jrm.trnjualstatus IN ('APPROVED','POST') AND jrm.typeSO<>'Konsinyasi' AND jrm.branch_code='" & DdlCabang.SelectedValue & "'),0.00) as 'bln3',0 ordermstoid,'' orderno, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end type " & _
        "FROM QL_mstitem i LEFT JOIN QL_conmtr con ON con.refoid=i.itemoid AND con.mtrlocoid IN (Select genoid from QL_mstgen WHERE gengroup='LOCATION' AND genother5 IN ('','UMUM')) AND con.branch_code='" & DdlCabang.SelectedValue & "' AND periodacctg='" & PeriodAcctg.Text & "' GROUP BY itemoid,itemdesc,con.branch_code,con.periodacctg,i.itemcode,statusitem,stockflag) Item Where itemoid IN (Select itemoid from QL_mstitem Where itemflag='AKTIF') ORDER BY itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
        gvListMat.Visible = True
    End Sub

    Private Sub BindDataSO()
        Dim sWhere As String = ""
        If itemoid.Text <> "0" Then
            sWhere = " AND itemoid=" & itemoid.Text & ""
        Else
            showMessage("Maaf, Pilih katalog dulu, klik select katalog dari data detail yang sudah di add to list", 2)
            Exit Sub
        End If
        sSql = "Select som.ordermstoid, orderno, trncustname, CONVERT(VARCHAR(20),trnorderdate,103) trnorderdate From ql_trnordermst som WHERE (orderno LIKE '%" & Tchar(orderno.Text) & "%' OR trncustname LIKE '%" & Tchar(orderno.Text) & "%') AND som.branch_code='" & DdlCabang.SelectedValue & "' AND som.trnorderstatus='APPROVED' AND som.ordermstoid IN (SELECT od.trnordermstoid FROM QL_trnorderdtl od WHERE od.branch_code=som.branch_code AND od.trnordermstoid=som.ordermstoid " & sWhere & " AND ISNULL(trnorderdtlstatus,'') <>'Completed')"
        Dim dtSO As DataTable = cKon.ambiltabel(sSql, "ql_trnordermst")
        GvSO.DataSource = dtSO : GvSO.DataBind()
        GvSO.SelectedIndex = -1 : GvSO.Visible = True
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_trnrequestitemdtl")
        dtlTable.Columns.Add("trnrequestitemdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("orderno", Type.GetType("System.String"))
        dtlTable.Columns.Add("ReqQty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("SaldoAkhir", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bln1", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bln2", Type.GetType("System.Double"))
        dtlTable.Columns.Add("bln3", Type.GetType("System.Double"))
        dtlTable.Columns.Add("acceptqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("ordermstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnrequestitemdtlnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        trnrequestitemdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
            trnrequestitemdtlseq.Text = objTable.Rows.Count + 1
        Else
            EnableHeader(True)
        End If
        i_u2.Text = "New Detail"
        itemdesc.Text = "" : itemoid.Text = "0"
        itemcode.Text = "" : orderno.Text = ""
        trnrequestitemdtlnote.Text = "" : trnrequestitemdtlseq.Text = 1
        trnrequestitemdtlqty.Text = "0.00"
        gvPRDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
        GvSO.Visible = False
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DdlCabang.Enabled = bVal : DdlCabang.CssClass = sCss
        orderno.Enabled = bVal : btnSearchSO.Visible = True
        orderno.CssClass = sCss
    End Sub

    Private Sub FillTextBox(ByVal sOid As String, ByVal sComp As String)
        sSql = "SELECT trnrequestitemoid, trnrequestitemno, branchcode, tobranch, trnrequestitemdate, trnrequestitemnote, trnrequestitemstatus, crtuser, crttime, upduser, updtime FROM QL_trnrequestitem prm WHERE prm.trnrequestitemoid=" & sOid & " AND prm.cmpcode='" & sComp & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            trnrequestitemoid.Text = Trim(xreader("trnrequestitemoid").ToString)
            PeriodAcctg.Text = GetDateToPeriodAcctg(GetServerTime())
            DdlCabang.SelectedValue = xreader("branchcode").ToString
            DDLCbKirim.SelectedValue = xreader("tobranch").ToString
            InitCabang()
            If DdlCabang.Items.Count > 0 Then
                DdlCabang.SelectedValue = xreader("branchcode").ToString
            End If

            InitCbKirim()
            If DDLCbKirim.Items.Count > 0 Then
                DDLCbKirim.SelectedValue = xreader("tobranch").ToString
            End If

            trnrequestitemdate.Text = Format(xreader("trnrequestitemdate"), "dd/MM/yyyy")
            trnrequestitemno.Text = Trim(xreader("trnrequestitemno").ToString)
            trnrequestitemnote.Text = Trim(xreader("trnrequestitemnote").ToString)
            trnrequestitemstatus.Text = Trim(xreader("trnrequestitemstatus").ToString)
            createuser.Text = Trim(xreader("crtuser").ToString)
            createtime.Text = Trim(xreader("crttime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
        End While
        xreader.Close()
        conn.Close()

        Dim bln1 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 1
        Dim bln2 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 2
        Dim bln3 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 3

        If bln1 <= 0 Then
            bln1 = bln1 + 12
        End If
        If bln2 <= 0 Then
            bln2 = bln2 + 12
        End If
        If bln3 <= 0 Then
            bln3 = bln3 + 12
        End If

        GenerateBulan(bln1, "satu")
        GenerateBulan(bln2, "dua")
        GenerateBulan(bln3, "tiga")

        gvPRDtl.Columns(6).HeaderText = lblbln1.Text
        gvPRDtl.Columns(5).HeaderText = lblbln2.Text
        gvPRDtl.Columns(4).HeaderText = lblbln3.Text

        sSql = "SELECT trnrequestitemdtloid, i.itemoid, itemcode, itemdesc, trnrequestitemdtlseq, trnrequestitemdtlqty ReqQty, totalsalesmonth1 bln1, totalsalesmonth2 bln2, totalsalesmonth3 bln3, saldoakhir, ordermstoid, trnrequestitemdtlnote, ISNULL((SELECT orderno FROM QL_trnordermst om WHERE om.ordermstoid=pd.ordermstoid AND om.branch_code='" & DdlCabang.SelectedValue & "'),'') orderno,pd.acceptqty FROM QL_trnrequestitemdtl pd INNER JOIN QL_mstitem i ON i.itemoid=pd.itemoid WHERE pd.cmpcode='" & sComp & "' AND trnrequestitemoid=" & sOid & " ORDER BY pd.trnrequestitemdtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_trnrequestitemdtl")
        Session("TblDtl") = dtTbl
        gvPRDtl.DataSource = dtTbl
        gvPRDtl.DataBind()
        If trnrequestitemstatus.Text = "In Process" Or trnrequestitemstatus.Text = "Revised" Then
            btnPrint.Visible = False
        Else
            btnSave.Visible = False
            btnDelete.Visible = False
            btnSendApproval.Visible = False
            btnAddToList.Visible = False
            gvPRDtl.Columns(0).Visible = False 
            If trnrequestitemstatus.Text = "Approved" Or trnrequestitemstatus.Text = "Closed" Or trnrequestitemstatus.Text = "Cancel" Then
                lblTrnNo.Text = "No. Request"
                trnrequestitemoid.Visible = False
                trnrequestitemno.Visible = True
            End If
            If trnrequestitemstatus.Text = "Cancel" Then
                btnPrint.Visible = False
            End If
        End If
        ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try 
            sSql = "SELECT prm.trnrequestitemoid,prm.trnrequestitemdate,prm.trnrequestitemno,prm.branchcode,cb.gendesc ,pd.itemoid,pd.itemcode,pd.itemdesc,pd.Seq,bln1,bln2,bln3,ReqQty,prm.trnrequestitemnote,pd.itemoid,acceptqty,ct.gendesc CbKirim FROM QL_trnrequestitem prm INNER JOIN QL_mstgen cb ON cb.gencode=prm.branchcode AND cb.gengroup='CABANG' INNER JOIN QL_mstgen ct ON ct.gencode=prm.tobranch AND ct.gengroup='CABANG' INNER JOIN (SELECT trnrequestitemdtloid,pd.trnrequestitemoid,i.itemoid,itemcode,itemdesc,trnrequestitemdtlseq Seq,trnrequestitemdtlqty ReqQty,totalsalesmonth1 bln1,totalsalesmonth2 bln2,totalsalesmonth3 bln3,saldoakhir,ordermstoid,trnrequestitemdtlnote Note,'' orderno,pd.acceptqty FROM QL_trnrequestitemdtl pd INNER JOIN QL_mstitem i ON i.itemoid=pd.itemoid ) pd ON pd.trnrequestitemoid=prm.trnrequestitemoid WHERE prm.trnrequestitemoid=" & sOid & " Order by Seq"
            report.Load(Server.MapPath(folderReport & "rptNotaPB.rpt"))

            Dim bln1 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 1
            Dim bln2 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 2
            Dim bln3 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 3

            If bln1 <= 0 Then
                bln1 = bln1 + 12
            End If
            If bln2 <= 0 Then
                bln2 = bln2 + 12
            End If
            If bln3 <= 0 Then
                bln3 = bln3 + 12
            End If

            GenerateBulan(bln1, "satu")
            GenerateBulan(bln2, "dua")
            GenerateBulan(bln3, "tiga")

            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_requestitem")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)

            report.SetParameterValue("bln1", lblbln1.Text)
            report.SetParameterValue("bln2", lblbln2.Text)
            report.SetParameterValue("bln3", lblbln3.Text)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Permintaan_Barang")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
        Response.Redirect("~\Transaction\trnForeCastTW.aspx?awal=true")
    End Sub

    Private Sub SaveRecords(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByVal status As Integer)
        If IsInputValid() Then 

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            trnrequestitemdtloid.Text = GenerateID("QL_trnrequestitemdtl", CompnyCode)
            Dim iAppOid As Integer = GenerateID("QL_APPROVAL", CompnyCode)
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_trnrequestitem WHERE trnrequestitemoid=" & trnrequestitemoid.Text) Then
                    trnrequestitemoid.Text = GenerateID("QL_trnrequestitem", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnrequestitem", "trnrequestitemoid", trnrequestitemoid.Text, "trnrequestitemstatus", updtime.Text, "Approved")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    trnrequestitemstatus.Text = "In Process"
                    Exit Sub
                End If
            End If

            If trnrequestitemstatus.Text = "Revised" Then
                trnrequestitemstatus.Text = "In Process"
            End If

            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO [QL_trnrequestitem] ([cmpcode],[trnrequestitemoid],[branchcode],[trnrequestitemno],[trnrequestitemdate],[trnrequestitemnote],[trnrequestitemstatus],[crtuser],[crttime],[upduser],[updtime],[approvaluser],[approvaltime],tobranch)" & _
                    " VALUES ('" & CompnyCode & "'," & trnrequestitemoid.Text & ",'" & DdlCabang.SelectedValue & "','" & trnrequestitemno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(trnrequestitemnote.Text) & "','" & trnrequestitemstatus.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'','1/1/1900','" & DDLCbKirim.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & trnrequestitemoid.Text & " WHERE tablename='QL_trnrequestitem' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE [QL_trnrequestitem] SET [branchcode] = '" & DdlCabang.SelectedValue & "',[trnrequestitemno] = '" & trnrequestitemno.Text & "',[trnrequestitemdate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),[trnrequestitemnote] = '" & Tchar(trnrequestitemnote.Text) & "',[trnrequestitemstatus] = '" & trnrequestitemstatus.Text & "',[upduser] = '" & Session("UserID") & "',[updtime] = CURRENT_TIMESTAMP,tobranch='" & DDLCbKirim.SelectedValue & "' WHERE trnrequestitemoid = " & trnrequestitemoid.Text & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM [QL_trnrequestitemdtl] WHERE trnrequestitemoid=" & trnrequestitemoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO [QL_trnrequestitemdtl] ([cmpcode],[trnrequestitemdtloid],[trnrequestitemoid],[trnrequestitemdtlseq],[itemoid],[trnrequestitemdtlqty],[totalsalesmonth1],[totalsalesmonth2],[totalsalesmonth3],[saldoakhir],[ordermstoid],[trnrequestitemdtlnote],[trnrequestitemdtlres1],[trnrequestitemdtlres2],[trnrequestitemdtlres3],[upduser],[updtime],acceptqty)" & _
                        " VALUES ('" & CompnyCode & "'," & (C1 + CInt(trnrequestitemdtloid.Text)) & "," & trnrequestitemoid.Text & "," & objTable.Rows(C1).Item("trnrequestitemdtlseq") & "," & objTable.Rows(C1).Item("itemoid") & "," & objTable.Rows(C1).Item("ReqQty") & "," & objTable.Rows(C1).Item("bln1") & "," & objTable.Rows(C1).Item("bln2") & "," & objTable.Rows(C1).Item("bln3") & "," & objTable.Rows(C1).Item("SaldoAkhir") & "," & objTable.Rows(C1).Item("ordermstoid") & ",'" & Tchar(objTable.Rows(C1).Item("trnrequestitemdtlnote").ToString) & "','','','','" & Session("UserID") & "',CURRENT_TIMESTAMP," & objTable.Rows(C1).Item("ReqQty") & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(trnrequestitemdtloid.Text)) & " WHERE tablename='QL_trnrequestitemdtl' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If trnrequestitemstatus.Text = "In Approval" Then
                    Dim dt As DataTable = Session("AppPerson")
                    For C1 As Integer = 0 To dt.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL (cmpcode,approvaloid,branch_code,requestcode,requestuser,requestdate,statusrequest,tablename,oid,event,approvalcode, approvaluser,approvaldate,approvaltype,approvallevel,approvalstatus,aprovalres1) VALUES" & _
                              " ('" & CompnyCode & "'," & iAppOid + C1 & ", '" & DdlCabang.SelectedValue & "','" & "PB" & trnrequestitemoid.Text & "_" & iAppOid + C1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNREQUESTITEM', '" & trnrequestitemoid.Text & "','In Approval','0', '" & dt.Rows(C1).Item("approvaluser") & "','1/1/1900','" & dt.Rows(C1).Item("approvaltype") & "','1', '" & dt.Rows(C1).Item("approvalstatus") & "','" & dt.Rows(C1).Item("aprovalres1").ToString & "')"

                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next

                    sSql = "UPDATE QL_mstOid SET lastoid=" & iAppOid + dt.Rows.Count - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_APPROVAL'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '-- welsa 24/11/2016 
                    status = 1
                End If

                objTrans.Commit() : conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString & "<br/>" & sSql, 1)
                        trnrequestitemstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString & "<br/>" & sSql, 1)
                    trnrequestitemstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & "<br/>" & sSql, 1)
                conn.Close()
                trnrequestitemstatus.Text = "In Process"
                Exit Sub
            End Try
            Response.Redirect("~\Transaction\trnForeCastTW.aspx?awal=true")
        End If
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            If Request.QueryString("back") <> "" Then
                Response.Redirect("~\Transaction\trnForeCastTW.aspx?back=true")
            Else
                Response.Redirect("~\Transaction\trnForeCastTW.aspx")
            End If '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        'If checkPagePermission("~\Transaction\trnForeCastTW.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - PR Raw Material"
        Session("oid") = Request.QueryString("oid")
        Session("comp") = Request.QueryString("comp")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnSendApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for approval?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            fInitCabang() : InitCabang() : CheckPRStatus() : fInitCbKirim()
            DdlCabang_SelectedIndexChanged(Nothing, Nothing)
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                trnrequestitemoid.Text = Session("oid")
                FillTextBox(Session("oid"), Session("comp"))
                TabContainer1.ActiveTabIndex = 1
            Else
                trnrequestitemoid.Text = GenerateID("QL_trnrequestitem", CompnyCode)
                trnrequestitemno.Text = GenerateID("QL_trnrequestitem", CompnyCode)
                trnrequestitemdate.Text = Format(GetServerTime(), "01/MM/yyyy")
                trnrequestitemstatus.Text = "In Process"
                PeriodAcctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnPrint.Visible = False
                TabContainer1.ActiveTabIndex = 0
                If Request.QueryString("back") <> "" Then
                    TabContainer1.ActiveTabIndex = 1
                End If
            End If
        End If

        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvPRDtl.DataSource = dt
            gvPRDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If

        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub 

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        sSql = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSql &= " AND pbm.trnrequestitemdate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND pbm.trnrequestitemdate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        BindTrnData(sSql)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = "" : FilterDDL.SelectedIndex = 0
        FilterText.Text = "" : cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = False : FilterDDLStatus.SelectedIndex = -1
        fInitCabang()
        BindTrnData("")
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        PrintReport("")
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        PrintReport(gvTRN.SelectedDataKey.Item("trnrequestitemoid").ToString)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        BindTrnData("")
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind() : tbData.Text = "5"
        gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub FilterDDLListMat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterDDLListMat.SelectedIndexChanged
        'If FilterDDLListMat.SelectedIndex <> 0 Then
        '    FilterTextListMat.TextMode = TextBoxMode.SingleLine
        'Else
        '    FilterTextListMat.TextMode = TextBoxMode.MultiLine
        'End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If

        Dim sPlus As String
        If FilterDDLListMat.SelectedIndex <> 0 Then
            sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
        Else
            If FilterTextListMat.Text <> "" Then
                Dim sText() As String = FilterTextListMat.Text.Split(";")
                For C1 As Integer = 0 To sText.Length - 1
                    If sText(C1) <> "" Then
                        sPlus &= FilterDDLListMat.SelectedValue & " LIKE '" & Tchar(sText(C1)) & "' OR "
                    End If
                Next
                If sPlus <> "" Then
                    sPlus = Left(sPlus, sPlus.Length - 4)
                Else
                    sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
                End If
            Else
                sPlus = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%'"
            End If
        End If

        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("prqty") = 0
                    objView(0)("prdtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("prqty") = 0
                    dtTbl.Rows(C1)("prdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then 
                    dtView.RowFilter = "checkvalue='True' AND ReqQty<=0.00"
                    If dtView.Count > 0 Then
                        For C1 As Integer = 0 To dtView.Count - 1
                            Session("WarningListMat") = "- Maaf, Jika quantity " & dtView(C1).Item("itemdesc") & ", belum di input quantity, guys..!!"
                        Next
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""

                    dtView.RowFilter = "checkvalue='True' AND ReqQty>TotalNya AND trnrequestitemdtlnote=''"
                    If dtView.Count > 0 Then
                        For C1 As Integer = 0 To dtView.Count - 1
                            Session("WarningListMat") = "- Maaf, Jika quantity " & dtView(C1).Item("itemdesc") & " melebihi (" & ToMaskEdit(dtView(C1).Item("bln1"), 3) & "+" & ToMaskEdit(dtView(C1).Item("bln2"), 3) & "+" & ToMaskEdit(dtView(C1).Item("bln3"), 3) & ")/3=" & ToMaskEdit(dtView(C1).Item("TotalNya"), 3) & ", input keterangan, guys..!!"
                        Next
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If
                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"

                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If

                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "itemoid=" & dtView(C1)("itemoid") & ""
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("ReqQty") = dtView(C1)("ReqQty")
                            dv(0)("acceptqty") = dtView(C1)("ReqQty").ToString
                            dv(0)("trnrequestitemdtlnote") = dtView(C1)("trnrequestitemdtlnote")
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("trnrequestitemdtlseq") = counter
                            objRow("itemoid") = dtView(C1)("itemoid")
                            objRow("ReqQty") = ToDouble(dtView(C1)("ReqQty"))
                            objRow("trnrequestitemdtlnote") = dtView(C1)("trnrequestitemdtlnote")
                            objRow("itemcode") = dtView(C1)("itemcode").ToString
                            objRow("itemdesc") = dtView(C1)("itemdesc").ToString
                            objRow("SaldoAkhir") = dtView(C1)("SaldoAkhir")
                            objRow("bln1") = dtView(C1)("bln1").ToString
                            objRow("bln2") = dtView(C1)("bln2").ToString
                            objRow("bln3") = dtView(C1)("bln3").ToString
                            objRow("acceptqty") = dtView(C1)("ReqQty").ToString
                            objRow("ordermstoid") = dtView(C1)("ordermstoid").ToString
                            objRow("orderno") = dtView(C1)("orderno").ToString
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""

                    Dim bln1 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 1
                    Dim bln2 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 2
                    Dim bln3 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 3

                    If bln1 <= 0 Then
                        bln1 = bln1 + 12
                    End If
                    If bln2 <= 0 Then
                        bln2 = bln2 + 12
                    End If
                    If bln3 <= 0 Then
                        bln3 = bln3 + 12
                    End If

                    GenerateBulan(bln1, "satu")
                    GenerateBulan(bln2, "dua")
                    GenerateBulan(bln3, "tiga")

                    gvPRDtl.Columns(5).HeaderText = lblbln1.Text
                    gvPRDtl.Columns(4).HeaderText = lblbln2.Text
                    gvPRDtl.Columns(3).HeaderText = lblbln3.Text

                    Session("TblDtl") = objTable
                    gvPRDtl.DataSource = objTable
                    gvPRDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
            btnSearchSO.Visible = False
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            If itemoid.Text = 0 Then
                Session("WarningListMat") = "Maaf, pilih katalog dulu..!!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & itemoid.Text & ""
                If dv.Count > 0 Then
                    showMessage("- Maaf, Katalog sudah di input..!!", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
            Else
                dv.RowFilter = "itemoid=" & itemoid.Text & " AND trnrequestitemdtlseq <>" & trnrequestitemdtlseq.Text & ""
                If dv.Count > 0 Then
                    showMessage("- Maaf, Katalog sudah di input..!!", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("trnrequestitemdtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(trnrequestitemdtlseq.Text - 1)
                objRow.BeginEdit()
            End If

            objRow("ReqQty") = ToDouble(trnrequestitemdtlqty.Text)
            objRow("trnrequestitemdtlnote") = ToDouble(trnrequestitemdtlnote.Text)
            If ordermstoid.Text <> 0 Then
                sSql = "SELECT ordermstoid,orderno FROM QL_trnordermst WHERE ordermstoid=" & ordermstoid.Text & " AND branch_code='" & DdlCabang.SelectedValue & "'"
                Dim dto As DataTable = cKon.ambiltabel(sSql, "Ordermst")

                objRow("ordermstoid") = Integer.Parse(dto.Rows(0)("ordermstoid"))
                objRow("orderno") = dto.Rows(0)("orderno").ToString
            Else
                objRow("ordermstoid") = 0
                objRow("orderno") = ""
            End If
           
            objRow("trnrequestitemdtlnote") = trnrequestitemdtlnote.Text
            objRow("acceptqty") = ToDouble(trnrequestitemdtlqty.Text)
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If

            Dim bln1 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 1
            Dim bln2 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 2
            Dim bln3 As String = Month(CDate(toDate(trnrequestitemdate.Text))) - 3

            If bln1 <= 0 Then
                bln1 = bln1 + 12
            End If
            If bln2 <= 0 Then
                bln2 = bln2 + 12
            End If
            If bln3 <= 0 Then
                bln3 = bln3 + 12
            End If

            GenerateBulan(bln1, "satu")
            GenerateBulan(bln2, "dua")
            GenerateBulan(bln3, "tiga")

            gvPRDtl.Columns(5).HeaderText = lblbln1.Text
            gvPRDtl.Columns(4).HeaderText = lblbln2.Text
            gvPRDtl.Columns(3).HeaderText = lblbln3.Text

            Session("TblDtl") = objTable
            gvPRDtl.DataSource = objTable
            gvPRDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvPRDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPRDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 3)
            e.Row.Cells(8).Text = ToMaskEdit(e.Row.Cells(8).Text, 3)
        End If
    End Sub

    Protected Sub gvPRDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPRDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("trnrequestitemdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvPRDtl.DataSource = objTable
        gvPRDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvPRDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPRDtl.SelectedIndexChanged
        Try
            trnrequestitemdtlseq.Text = gvPRDtl.SelectedDataKey.Item("trnrequestitemdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "trnrequestitemdtlseq=" & trnrequestitemdtlseq.Text
                trnrequestitemdtlqty.Text = ToMaskEdit(dv.Item(0).Item("ReqQty"), 3)
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemcode.Text = dv.Item(0).Item("itemcode").ToString
                itemdesc.Text = dv.Item(0).Item("itemdesc").ToString
                trnrequestitemdtlnote.Text = dv.Item(0).Item("trnrequestitemdtlnote").ToString
                ordermstoid.Text = dv.Item(0).Item("ordermstoid").ToString
                orderno.Text = dv.Item(0).Item("orderno").ToString
                dv.RowFilter = ""
            End If
            btnSearchMat.Visible = False
            orderno.Enabled = True : btnSearchSO.Visible = True
            orderno.CssClass = "inpText"
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        SaveRecords(sender, e, 0)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnForeCastTW.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If trnrequestitemoid.Text.Trim = "" Then
            showMessage("- Maaf, isi data detail..!!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnrequestitem", "trnrequestitemoid", trnrequestitemoid.Text, "trnrequestitemstatus", updtime.Text, "Approved")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                trnrequestitemstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        'Dim sCode As String = ""
        'sSql = "SELECT cmpcode FROM QL_mstdept WHERE DdlCabang=" & DdlCabang.SelectedValue & " AND DdlCabang=" & DdlCabang.SelectedValue
        'If Not IsDBNull(GetStrData(sSql)) Then
        '    sCode = GetStrData(sSql)
        'End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trnrequestitemdtl WHERE cmpcode='" & CompnyCode & "' AND trnrequestitemoid=" & trnrequestitemoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnrequestitem WHERE cmpcode='" & CompnyCode & "' AND trnrequestitemoid=" & trnrequestitemoid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnForeCastTW.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport(trnrequestitemoid.Text)
    End Sub

    Protected Sub btnSendApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSendApproval.Click
        If DdlCabang.SelectedValue <> "" And DdlCabang.SelectedValue <> "" Then
            sSql = "SELECT tablename,approvaltype,approvallevel,approvaluser,approvalstatus,aprovalres1 From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnrequestitem' And branch_code LIKE '%" & DdlCabang.SelectedValue & "%' Order by approvallevel"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_approvalperson")
            If dt.Rows.Count = 0 Then
                showMessage("There is no user can't approve this transaction. Please define approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
            Else
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "approvalstatus='FINAL7'"
                If dv.Count = 0 Then
                    showMessage("Final approval user is not defined for this transaction. Please define final approval user in <STRONG>APPROVAL USER</STRONG> form.", 2)
                    dv.RowFilter = ""
                Else
                    dv.RowFilter = ""
                    Session("AppPerson") = dt
                    trnrequestitemstatus.Text = "In Approval"
                    btnSave_Click(Nothing, Nothing)
                End If
            End If
        Else
            showMessage("Please select Business Unit & Department!", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSearchSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSO.Click
        BindDataSO()
    End Sub

    Protected Sub GvSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvSO.PageIndexChanging
        GvSO.PageIndex = e.NewPageIndex
        BindDataSO()
    End Sub

    Protected Sub GvSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvSO.SelectedIndexChanged
        ordermstoid.Text = GvSO.SelectedDataKey.Item("ordermstoid").ToString
        orderno.Text = GvSO.SelectedDataKey.Item("orderno").ToString
        GvSO.Visible = False
    End Sub 

    Protected Sub DdlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlCabang.SelectedIndexChanged
        InitCbKirim()
    End Sub
#End Region
End Class