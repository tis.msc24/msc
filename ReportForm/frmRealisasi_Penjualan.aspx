<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmRealisasi_Penjualan.aspx.vb" Inherits="ReportForm_frmRealisasi_Penjualan" Title="Untitled Page" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" style="text-align: left; height: 23px;" valign="center">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Report Realisasi Penjualan :."></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center" colspan="3">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE><TBODY><TR><TD align=left>Report Type</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlType" runat="server" Width="128px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w794"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem Value="Detail">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlcabang" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w795"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:DropDownList id="ddlOption1" runat="server" Width="96px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w796"><asp:ListItem Value="pomst.trnorderdate">SO Date</asp:ListItem>
<asp:ListItem Value="sjmst.trnsjjualdate">SDO Date</asp:ListItem>
<asp:ListItem Value="blmst.trnjualdate">SI Date</asp:ListItem>
<asp:ListItem Value="retmst.trnjualdate">Return Date</asp:ListItem>
</asp:DropDownList></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="podate1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w797"></asp:TextBox> <asp:ImageButton id="ibPOdate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w798"></asp:ImageButton> <asp:Label id="Label2" runat="server" Text="to" __designer:wfdid="w799"></asp:Label> <asp:TextBox id="podate2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w800"></asp:TextBox> <asp:ImageButton id="ibPODate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w801"></asp:ImageButton> <asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w802"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w803" TargetControlID="podate1" PopupButtonID="ibPOdate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender4" runat="server" __designer:wfdid="w804" TargetControlID="podate2" PopupButtonID="ibPODate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w805" TargetControlID="podate1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w806" TargetControlID="podate2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD id="TD5" align=right runat="server" Visible="true"></TD><TD id="TD6" align=left runat="server" Visible="true"></TD><TD id="TD4" align=left runat="server" Visible="true"><asp:DropDownList id="ddlTanggal" runat="server" CssClass="inpText" __designer:wfdid="w807"><asp:ListItem>NOT</asp:ListItem>
<asp:ListItem>OR</asp:ListItem>
<asp:ListItem>AND</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:DropDownList id="filterdate2" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w808"><asp:ListItem Value="retmst.trnjualdate">Return Date</asp:ListItem>
<asp:ListItem Value="blmst.trnjualdate">SI Date</asp:ListItem>
<asp:ListItem Value="sjmst.trnsjjualdate">SDO Date</asp:ListItem>
<asp:ListItem Value="pomst.trnorderdate">SO Date</asp:ListItem>
</asp:DropDownList></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="POdate_2_1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w809"></asp:TextBox> <asp:ImageButton id="cal_2_1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w810"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" Text="to" __designer:wfdid="w811"></asp:Label>&nbsp;<asp:TextBox id="POdate_2_2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w812"></asp:TextBox>&nbsp;<asp:ImageButton id="cal_2_2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w813"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w814"></asp:Label> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w815" TargetControlID="POdate_2_1" PopupButtonID="cal_2_1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w816" TargetControlID="POdate_2_2" PopupButtonID="cal_2_2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w817" TargetControlID="POdate_2_1" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w818" TargetControlID="POdate_2_2" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="WIDTH: 110px" align=left>Customer</TD><TD style="TEXT-ALIGN: left" align=left>:</TD><TD align=left><asp:TextBox id="custName" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w819"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w820"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w821"></asp:ImageButton><asp:Label id="custoid" runat="server" __designer:wfdid="w822" Visible="False"></asp:Label></TD></TR><TR><TD id="TDSupplier" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w748" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custoid,custcode,custname" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="lblstatusdataCust" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left><asp:DropDownList id="ddlTax" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w824" Visible="False"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="Yes">Yes</asp:ListItem>
<asp:ListItem>No</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Group</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLGrup" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w825"></asp:DropDownList></TD></TR><TR><TD align=left>Sub Group</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w826"></asp:DropDownList></TD></TR><TR><TD align=left>Barang</TD><TD align=left>:</TD><TD align=left colSpan=1><asp:TextBox id="material" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w827"></asp:TextBox> <asp:ImageButton id="ibSearchMat" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w828"></asp:ImageButton> <asp:ImageButton id="ibClearMat" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w829"></asp:ImageButton> <asp:Label id="matoid" runat="server" __designer:wfdid="w830" Visible="False"></asp:Label></TD></TR><TR><TD id="tdMaterial" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvItem" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w756" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3,merk" PageSize="8" UseAccessibleHeader="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left>SO No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="orderno" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w832"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w833"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w834"></asp:ImageButton><asp:Label id="ordermstoid" runat="server" __designer:wfdid="w835" Visible="False"></asp:Label></TD></TR><TR><TD id="tdPO" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSO" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w761" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ordermstoid,orderno,trnorderdate" PageSize="8" UseAccessibleHeader="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnorderdate" HeaderText="SO Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left>SDO No.</TD><TD align=left>:</TD><TD align=left><asp:TextBox id="trnsjjualno" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w837"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchLPB" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w838"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelLPB" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w839"></asp:ImageButton><asp:Label id="trnsjjualmstoid" runat="server" __designer:wfdid="w840" Visible="False"></asp:Label></TD></TR><TR><TD id="tdLPB" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSDO" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w766" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjjualmstoid,trnsjjualno,trnsjjualdate" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjjualno" HeaderText="SDO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjjualdate" HeaderText="SDO Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 110px" id="TD1" align=left runat="server" visible="false">Sales Return No</TD><TD id="TD2" align=left runat="server" visible="false">:</TD><TD id="TD3" align=left runat="server" visible="false"><asp:TextBox id="trnjualreturno" runat="server" Width="187px" CssClass="inpText" __designer:wfdid="w842"></asp:TextBox>&nbsp;<asp:ImageButton id="ibSearchPurchaseReturn" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w843"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibDelPurchaseReturn" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w844"></asp:ImageButton><asp:Label id="trnjualreturmstoid" runat="server" __designer:wfdid="w845" Visible="False"></asp:Label></TD></TR><TR><TD id="tdPurchaseReturn" align=center colSpan=3 runat="server" visible="false"><asp:GridView id="gvSalesReturn" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w771" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnjualreturmstoid,trnjualreturno,trnjualdate" PageSize="8" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualreturno" HeaderText="Retur No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Retur Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label14" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="RD1" align=left runat="server" visible="false">Merk</TD><TD id="RD2" align=left runat="server" visible="false">:</TD><TD id="RD3" align=left runat="server" visible="false"><asp:TextBox id="Merk" runat="server" Width="199px" CssClass="inpText" __designer:wfdid="w847"></asp:TextBox></TD></TR><TR><TD align=left>SO Status</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="postatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w848">
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem>In Process</asp:ListItem>
                                                <asp:ListItem>In Approval</asp:ListItem>
                                                <asp:ListItem>Approved</asp:ListItem>
                                                <asp:ListItem>Closed</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD align=left>SDO Status</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlSDOStatus" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w849">
                                                <asp:ListItem>ALL</asp:ListItem>
                                                <asp:ListItem>In Process</asp:ListItem>
                                                <asp:ListItem>In Approval</asp:ListItem>
                                                <asp:ListItem>Approved</asp:ListItem>
                                                <asp:ListItem>Closed</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD align=left>Sales</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="SalesPerson" runat="server" Width="149px" CssClass="inpText" __designer:wfdid="w850"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w851"></asp:ImageButton> <asp:ImageButton id="ToPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w852"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbExport" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w853"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w854"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w855"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w856"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv_penjualan" runat="server" __designer:wfdid="w857" AutoDataBind="true"></CR:CrystalReportViewer> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ToPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbExport"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upMsgbox" runat="server"><contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" BackgroundCssClass="modalBackground" Drag="True" DropShadow="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
          </asp:UpdatePanel></td>
        </tr>
    </table>
  
</asp:Content>
