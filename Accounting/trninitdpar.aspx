<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trninitdpar.aspx.vb" Inherits="Accounting_trninitdpar" title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text=".: DP AR Balance" CssClass="Title"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: List of DP AR Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" __designer:wfdid="w84" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD align=left>Filter&nbsp; </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="139px" CssClass="inpText" __designer:wfdid="w85"><asp:ListItem Value="trndparno">DP AR Balance No.</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="payreftype">Tipe Pembayaran</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w86"></asp:TextBox></TD></TR><TR><TD align=left>Periode </TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w87"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w88"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w89"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w90"></asp:ImageButton>&nbsp;<ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w91" Format="dd/MM/yyyy" PopupButtonID="imageButton1" TargetControlID="dateAwal"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w92" Format="dd/MM/yyyy" PopupButtonID="imageButton2" TargetControlID="dateAkhir"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w93" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w94" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="statuse" runat="server" Width="119px" CssClass="inpText" __designer:wfdid="w95"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Button id="btnSearch" runat="server" Width="75px" CssClass="orange" Font-Bold="True" Text="FIND" Visible="False" __designer:wfdid="w96"></asp:Button>&nbsp;<asp:Button id="btnAll" runat="server" Width="75px" CssClass="gray" Font-Bold="True" Text="VIEW ALL" Visible="False" __designer:wfdid="w97"></asp:Button><BR /></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w98" AlternateText="Find"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w99" AlternateText="View All"></asp:ImageButton> </TD></TR><TR><TD align=left colSpan=6><asp:GridView id="gvMst" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w78" GridLines="None" EmptyDataText="No data in database." AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="trndparoid,cmpcode" AllowSorting="True">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" __designer:wfdid="w2" ToolTip="<%# GetIDCB() %>"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:HyperLinkField DataNavigateUrlFields="trndparoid,cmpcode" DataNavigateUrlFormatString="trninitdpar.aspx?oid={0}" DataTextField="trndparno" HeaderText="DP AR No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trndpardate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="giroNo" HeaderText="Tipe Pembayaran">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trndparstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data not found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD5" align=left colSpan=6 runat="server" Visible="false"><asp:Button id="btnCheckAll" runat="server" CssClass="green" Font-Bold="True" Text="SELECT ALL" __designer:wfdid="w101"></asp:Button> <asp:Button id="btnUncheckAll" runat="server" CssClass="red" Font-Bold="True" Text="SELECT NONE" __designer:wfdid="w102"></asp:Button> <asp:Button id="btnPosting" runat="server" CssClass="orange" Font-Bold="True" Text="POST SELECTED" Visible="False" __designer:wfdid="w103"></asp:Button></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 527px"><TBODY><TR><TD id="TD2" align=left runat="server" Visible="true">Cabang</TD><TD align=left runat="server" Visible="true">:</TD><TD id="TD4" align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="cabangDDL" runat="server" CssClass="inpText" __designer:wfdid="w100" OnSelectedIndexChanged="cabangDDL_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><ajaxToolkit:CalendarExtender id="ceDPARDate" runat="server" __designer:wfdid="w101" Format="dd/MM/yyyy" PopupButtonID="imbDPARDate" TargetControlID="trndpardate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDPARDate" runat="server" __designer:wfdid="w102" TargetControlID="trndpardate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="cepayduedate" runat="server" __designer:wfdid="w103" Format="dd/MM/yyyy" PopupButtonID="imbDueDate" TargetControlID="payduedate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meepayduedate" runat="server" __designer:wfdid="w104" TargetControlID="payduedate" CultureName="id-ID" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left>Date</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndpardate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w105" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDPARDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px" Visible="False" __designer:wfdid="w106"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w107"></asp:Label> <asp:Label id="CutofDate" runat="server" Visible="False" __designer:wfdid="w108"></asp:Label></TD></TR><TR><TD align=left>DP No<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w109"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparno" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w110" MaxLength="20"></asp:TextBox> <asp:Label id="trndparoid" runat="server" Visible="False" __designer:wfdid="w111"></asp:Label></TD></TR><TR><TD align=left>Customer<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w112"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="custname" runat="server" Width="233px" CssClass="inpText" __designer:wfdid="w113" MaxLength="50"></asp:TextBox> <asp:Label id="custoid" runat="server" Visible="False" __designer:wfdid="w114"></asp:Label> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w115"></asp:ImageButton> <asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w116"></asp:ImageButton></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView id="gvCust" runat="server" Width="550px" ForeColor="#333333" Visible="False" __designer:wfdid="w72" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" PageSize="8" GridLines="None" EmptyDataText="No data found" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="custoid,custname">
<RowStyle BackColor="#FFFBD6" CssClass="gvrow" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Cust. Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="True" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Cust. Name">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="350px"></HeaderStyle>

<ItemStyle Width="350px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Account DP</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="trndparacctgoid" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w118"></asp:DropDownList></TD></TR><TR><TD id="TD10" align=left runat="server" Visible="false">Type Bayar</TD><TD id="TD9" align=left runat="server" Visible="false">:</TD><TD id="TD8" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="payreftype" runat="server" Width="132px" CssClass="inpText" __designer:wfdid="w119" AutoPostBack="True"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>NONCASH</asp:ListItem>
<asp:ListItem>GIRO</asp:ListItem>
</asp:DropDownList></TD></TR><TR id="tr1" runat="server"><TD id="TD3" align=left runat="server" Visible="false"><asp:Label id="Label4" runat="server" CssClass="Important" Text="*" __designer:wfdid="w120"></asp:Label></TD><TD id="TD7" align=left runat="server" Visible="false">:</TD><TD id="TD1" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="payduedate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w121"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w122"></asp:ImageButton> <asp:Label id="lblDate" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w123"></asp:Label></TD></TR><TR id="tr2" runat="server"><TD style="HEIGHT: 16px" align=left>Ref No.&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w124"></asp:Label></TD><TD style="HEIGHT: 16px" align=left>:</TD><TD style="HEIGHT: 16px" align=left colSpan=3><asp:TextBox id="payrefno" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w125" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left>Amount<asp:Label id="Label8" runat="server" CssClass="Important" Text="*" __designer:wfdid="w126"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparamt" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w127" AutoPostBack="True" MaxLength="20"></asp:TextBox>&nbsp;<ajaxToolkit:FilteredTextBoxExtender id="FtedpARamt" runat="server" __designer:wfdid="w128" TargetControlID="trndparamt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left>Note</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparnote" runat="server" Width="375px" Height="33px" CssClass="inpText" __designer:wfdid="w129" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparstatus" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w130" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD align=left colSpan=5 runat="server" Visible="true"><asp:Label id="update" runat="server" Font-Bold="False" __designer:wfdid="w131"></asp:Label> <asp:Label id="create" runat="server" Font-Bold="False" __designer:wfdid="w132"></asp:Label></TD></TR><TR><TD align=left colSpan=5 runat="server" Visible="true"><asp:ImageButton id="btnSave1" onclick="ImageButton3_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w133" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel1" onclick="btnCancel1_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w134" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete1" onclick="btnDelete1_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w135" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnposting1" onclick="btnposting1_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w136"></asp:ImageButton></TD></TR><TR><TD id="TD6" align=left colSpan=5 runat="server" Visible="false"><asp:Button id="btnSave" runat="server" Width="75px" CssClass="green" Font-Bold="True" Text="SAVE" Visible="False" __designer:wfdid="w137"></asp:Button><asp:Button id="btnCancel" runat="server" Width="75px" CssClass="gray" Font-Bold="True" Text="CANCEL" Visible="False" __designer:wfdid="w138"></asp:Button><asp:Button id="btnPosting2" runat="server" Width="75px" CssClass="orange" Font-Bold="True" Text="POSTING" Visible="False" __designer:wfdid="w139"></asp:Button><asp:Button id="btnDelete" runat="server" Width="75px" CssClass="red" Font-Bold="True" Text="DELETE" Visible="False" __designer:wfdid="w140"></asp:Button>&nbsp;&nbsp; </TD></TR><TR><TD align=left colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w141"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: Form DP AR Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>
                          
</asp:Content>