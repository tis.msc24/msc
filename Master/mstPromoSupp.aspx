<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstPromoSupp.aspx.vb" Inherits="Master_PromoSupp" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Master Promo Supplier"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="background-color: transparent">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <div style="width: 100%; text-align: left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" class="Label" style="width: 56px">
                                                Filter</td>
<td align="left" class="Label">
:</td>
<td align="left" colspan="4">
                                                <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                                    <asp:ListItem Value="promoname">Nama Promo</asp:ListItem>
                                                    <asp:ListItem Value="promocode">Kode Promo</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="160px" CssClass="inpText"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="height: 10px; width: 56px;">
                                                Status</td>
<td align="left" style="height: 10px">:</td>
                                            <td align="left" colspan="4">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="inpText">
                                                    <asp:ListItem>ALL</asp:ListItem>
                                                    <asp:ListItem>In Procces</asp:ListItem>
                                                    <asp:ListItem>In Approval</asp:ListItem>
                                                    <asp:ListItem>Approved</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" />
             <asp:ImageButton ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" />
                                                <asp:ImageButton ID="imbExportXLS" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/toexcel.png" /></td></tr>
<tr><td align="left" colspan="6">
                             <fieldset id="field1" style="height: 206px; width: 99%; text-align: left; border-top-style: none;
                                    border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                    <div id='divTblData'>
                                    </div>
                                    <div style="overflow-y: scroll; height: 92%; width: 96%; background-color: beige;">
                                        <asp:GridView ID="GVMst" runat="server" CellPadding="4" AutoGenerateColumns="False"
                                            Width="100%" ForeColor="#333333" GridLines="None" Height="55px">
                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" CssClass="gvhdr" />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="promoid" DataNavigateUrlFormatString="mstPromoSupp.aspx?oid={0}" DataTextField="promeventcode" HeaderText="Kode">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" CssClass="gvhdr" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="promeventname" HeaderText="Promo">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" CssClass="gvhdr" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promstartdate" HeaderText="Periode1">
                                                    <ItemStyle Font-Size="X-Small" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" Width="100px" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="promfinishdate" HeaderText="Periode2">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Right" Width="100px" CssClass="gvhdr" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="typenya" HeaderText="Type Promo">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="statuspromo" HeaderText="Status">
                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" Wrap="False" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField Visible="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbPrintFromList" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/print.gif"
                                                            ToolTip='<%# Eval("promoid") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="gvhdr" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
</td>
</tr>
                                    </table>
                                </asp:Panel>
&nbsp;
                            </div>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List Promo Supplier :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=9><asp:Label id="Label48" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Red" Text="Master Promo" __designer:wfdid="w90" Font-Underline="True"></asp:Label> <asp:Label id="lblamp" runat="server" __designer:wfdid="w91" Visible="False"></asp:Label> <asp:Label id="PromOid" runat="server" __designer:wfdid="w92" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left><asp:Label id="Label17" runat="server" Width="77px" Text="Type Promo" __designer:wfdid="w93"></asp:Label></TD><TD class="Label" align=left><asp:DropDownList id="TypeDDL" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w94" AutoPostBack="True"><asp:ListItem Value="POINT">TARGET POINT</asp:ListItem>
<asp:ListItem Value="AMOUNT">TARGET AMOUNT</asp:ListItem>
<asp:ListItem Value="QTYBARANG">TARGET QTY PER BARANG</asp:ListItem>
<asp:ListItem Value="QTYTOTAL">TARGET QTY TOTAL</asp:ListItem>
<asp:ListItem Value="ASPEND">TARGET ASPEND</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left colSpan=3></TD><TD class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left><asp:Label id="Label6" runat="server" Width="84px" Text="Kode Promo" __designer:wfdid="w95"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="CodePro" runat="server" CssClass="inpTextDisabled" MaxLength="100" __designer:wfdid="w96" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left colSpan=3>Nama Promo&nbsp;<asp:Label id="Label55" runat="server" CssClass="Important" Text="*" __designer:wfdid="w97"></asp:Label></TD><TD class="Label" align=left colSpan=4><asp:TextBox id="promeventname" runat="server" Width="200px" CssClass="inpText" MaxLength="50" __designer:wfdid="w98"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label1" runat="server" Width="59px" Text="Start Date" __designer:wfdid="w99"></asp:Label>&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w100"></asp:Label></TD><TD class="Label" align=left Visible="false"><asp:TextBox id="promstartdate" runat="server" Width="75px" CssClass="inpText" MaxLength="6" __designer:wfdid="w101"></asp:TextBox>&nbsp;<asp:ImageButton id="ibstartdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w102">
                                                        </asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w103"></asp:Label></TD><TD class="Label" align=left colSpan=3 Visible="false"><asp:Label id="Label3" runat="server" Width="52px" Text="End Date" __designer:wfdid="w104"></asp:Label> <asp:Label id="Label57" runat="server" CssClass="Important" Text="*" __designer:wfdid="w105"></asp:Label></TD><TD class="Label" align=left colSpan=4><asp:TextBox id="promfinishdate" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w106"></asp:TextBox> <asp:ImageButton id="ibfinishdate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w107">
                                                        </asp:ImageButton> <asp:Label id="Label7" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w108"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" id="Td2" class="Label" align=left Visible="false">Supplier&nbsp;<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w109"></asp:Label></TD><TD id="Td4" class="Label" align=left Visible="false"><asp:TextBox id="SuppName" runat="server" Width="149px" CssClass="inpText" MaxLength="100" __designer:wfdid="w110"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w111"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSupp" onclick="btnClearSupp_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w112"></asp:ImageButton><asp:Button id="AddListSupp" runat="server" CssClass="btn green" Font-Bold="True" Text="Add To List" __designer:wfdid="w113"></asp:Button></TD><TD class="Label" align=left colSpan=3 Visible="false">Status</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="sTatus" runat="server" CssClass="inpTextDisabled" MaxLength="50" __designer:wfdid="w114" Enabled="False">IN PROCESS</asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left><asp:Label id="Label10" runat="server" Width="88px" Text="Jenis Promo" __designer:wfdid="w115" Visible="False"></asp:Label> <asp:Label id="SuppOid" runat="server" CssClass="Important" __designer:wfdid="w116" Visible="False"></asp:Label> <asp:Label id="SuppSeq" runat="server" CssClass="Important" __designer:wfdid="w117" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=8 runat="server"><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvSupplier" runat="server" Width="100%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w118" Visible="False" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="CbSupp" runat="server" Text='<%# eval("suppcode") %>' __designer:wfdid="w4" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("suppoid") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supp" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No supplier data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="GVdtlSupp" runat="server" Width="100%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w119" Visible="False" PageSize="8" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="suppoid" HeaderText="suppoid" Visible="False"></asp:BoundField>
<asp:BoundField HeaderText="seq" Visible="False"></asp:BoundField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supp">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No supplier data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left>Keterangan</TD><TD class="Label" align=left colSpan=8><asp:TextBox id="note" runat="server" Width="336px" CssClass="inpText" MaxLength="150" __designer:wfdid="w120"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=9><asp:Label id="Label8" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Red" Text="Detail Barang" __designer:wfdid="w121" Font-Underline="True"></asp:Label> <asp:Label id="I_u2" runat="server" Width="65px" ForeColor="Red" Text="New Detail" __designer:wfdid="w122" Visible="False"></asp:Label> <asp:Label id="seq" runat="server" __designer:wfdid="w123" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" id="Td5" class="Label" align=left Visible="false"><asp:Label id="LblItem" runat="server" Width="64px" Text="Item" __designer:wfdid="w124"></asp:Label></TD><TD id="Td9" class="Label" align=left colSpan=8 Visible="false"><asp:TextBox id="ItemNya" runat="server" Width="250px" CssClass="inpText" MaxLength="100" __designer:wfdid="w125"></asp:TextBox> <asp:ImageButton id="sItemNya" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w126"></asp:ImageButton>&nbsp;<asp:ImageButton id="eItemNya" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w127"></asp:ImageButton> <asp:Label id="ItemOidNya" runat="server" CssClass="Important" Text="0" __designer:wfdid="w128" Visible="False"></asp:Label><asp:ImageButton id="BtnAddList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w129" AlternateText="Add to List"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 90px" id="Td15" class="Label" align=left runat="server" Visible="true"><asp:Label id="createtime" runat="server" __designer:wfdid="w130" Visible="False"></asp:Label></TD><TD class="Label" align=left colSpan=8 Visible="false"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w131" Visible="False" PageSize="8" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField HeaderText="ALL"><HeaderTemplate>
&nbsp;<asp:Button id="BtnSelectAll" onclick="BtnSelectAll_Click" runat="server" CssClass="btn green" Text="Select All" __designer:wfdid="w3"></asp:Button> 
</HeaderTemplate>
<ItemTemplate>
&nbsp;<asp:CheckBox id="cbItem" runat="server" __designer:wfdid="w1" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Nilai Point"><ItemTemplate>
<asp:TextBox id="tbQty" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("Point") %>' __designer:wfdid="w2" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyLM" runat="server" __designer:wfdid="w3" TargetControlID="tbQty" ValidChars="1234567890.,">
                                                </ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Target Qty"><ItemTemplate>
<asp:TextBox id="QtyTarget" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("TargetQty") %>' __designer:wfdid="w2" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQtyTarget" runat="server" __designer:wfdid="w3" TargetControlID="QtyTarget" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="priceitem" HeaderText="Pricelist">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Nilai Aspend"><ItemTemplate>
<asp:TextBox id="nAspend" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("aspend") %>' __designer:wfdid="w1" MaxLength="15"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEnAspend" runat="server" __designer:wfdid="w2" ValidChars="1234567890.," TargetControlID="nAspend"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="TypeBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left colSpan=9 runat="server" Visible="true"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 150px; BACKGROUND-COLOR: beige"><asp:GridView id="gvPromoDtl" runat="server" Width="99%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w132" EmptyDataText="No Data" OnSelectedIndexChanged="gvPromoDtl_SelectedIndexChanged" OnRowDeleting="gvPromoDtl_RowDeleting" DataKeyNames="seq,itemdesc,Point,TargetQty,priceitem,aspend">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item. Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Point" HeaderText="Nilai Point">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="targetqty" HeaderText="Target Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aspend" HeaderText="Nilai Aspend">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="priceitem" HeaderText="Pricelist">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" Font-Bold="True" ForeColor="Red" Width="2px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="2px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="promdtloid" HeaderText="promdtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=left colSpan=9 Visible="false"><asp:Label id="Label14" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Red" Text="Detail Reward" __designer:wfdid="w133" Font-Underline="True"></asp:Label> <asp:Label id="I_u2r" runat="server" Width="65px" ForeColor="Red" Text="New Detail" __designer:wfdid="w134" Visible="False"></asp:Label> <asp:Label id="rSeq" runat="server" __designer:wfdid="w135" Visible="False"></asp:Label> <asp:Label id="ItemRewardOid" runat="server" CssClass="Important" Text="0" __designer:wfdid="w136" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label13" runat="server" Width="79px" Text="Type Reward" __designer:wfdid="w137"></asp:Label></TD><TD class="Label" align=left colSpan=8 Visible="false"><asp:DropDownList id="DDLTypeReward" runat="server" CssClass="inpText" __designer:wfdid="w138" AutoPostBack="True"><asp:ListItem Value="BARANG">BARANG</asp:ListItem>
<asp:ListItem Value="NON BARANG">NON BARANG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label12" runat="server" Width="79px" Text="Item Reward" __designer:wfdid="w139"></asp:Label></TD><TD class="Label" align=left colSpan=8 Visible="true"><asp:TextBox id="ItemReward" runat="server" Width="250px" CssClass="inpText" MaxLength="100" __designer:wfdid="w140"></asp:TextBox>&nbsp;<asp:ImageButton id="sRewardBtn" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w141"></asp:ImageButton>&nbsp;<asp:ImageButton id="eRewardBtn" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w142"></asp:ImageButton> </TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"></TD><TD class="Label" align=left colSpan=8 Visible="false"><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GvItemReward" runat="server" Width="100%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w143" Visible="False" PageSize="8" AllowPaging="True" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="itemcode,itemdesc" OnPageIndexChanging="GvItemReward_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:Button id="BtnAllReward" onclick="BtnAllReward_Click" runat="server" CssClass="btn green" Text="Select All" __designer:wfdid="w6"></asp:Button> 
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbLM" runat="server" __designer:wfdid="w4" ToolTip='<%# eval("itemoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Target Point"><ItemTemplate>
<asp:TextBox id="tbQtyR" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("TargetPoint") %>' __designer:wfdid="w8" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeQty" runat="server" __designer:wfdid="w9" TargetControlID="tbQtyR" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Target Amt"><ItemTemplate>
<asp:TextBox id="tbAmt" runat="server" CssClass="inpText" Text='<%# eval("TargetAmt") %>' __designer:wfdid="w14" MaxLength="12"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftbeAmt" runat="server" __designer:wfdid="w15" TargetControlID="tbAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Qty Reward"><ItemTemplate>
<asp:TextBox id="dAmt" runat="server" Width="75px" CssClass="inpText" Text='<%# Eval("rQty") %>' __designer:wfdid="w12" MaxLength="10" ToolTip='<%# Eval("rQty") %>'></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="FTEdAmt" runat="server" __designer:wfdid="w13" TargetControlID="dAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Target Qty"><ItemTemplate>
<asp:TextBox id="tbQtyTar" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("TargetQty") %>' __designer:wfdid="w5" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftQtyTar" runat="server" __designer:wfdid="w6" TargetControlID="tbQtyTar" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Reward1 (%)"><ItemTemplate>
<asp:TextBox id="tbQtyPersen" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("PersenReward") %>' __designer:wfdid="w18" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftPersenReward" runat="server" __designer:wfdid="w56" TargetControlID="tbQtyPersen" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Reward2 (%)"><ItemTemplate>
<asp:TextBox id="tbQtyPersen2" runat="server" Width="50px" CssClass="inpText" Text='<%# eval("PersenReward2") %>' __designer:wfdid="w20" MaxLength="6"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="ftPersenReward2" runat="server" __designer:wfdid="w53" TargetControlID="tbQtyPersen2" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="TypeBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label9" runat="server" Width="79px" Text="Target Point" __designer:wfdid="w144"></asp:Label></TD><TD class="Label" align=left Visible="true"><asp:TextBox id="TargetPoint" runat="server" Width="75px" CssClass="inpText" MaxLength="15" __designer:wfdid="w145">0.00</asp:TextBox></TD><TD class="Label" align=left Visible="true"><asp:Label id="Label21" runat="server" Width="86px" Text="Reward1 (%)" __designer:wfdid="w146"></asp:Label></TD><TD class="Label" align=left Visible="true"><asp:TextBox id="PersenReward" runat="server" Width="75px" CssClass="inpText" MaxLength="20" __designer:wfdid="w147">0.00</asp:TextBox></TD><TD class="Label" align=left colSpan=4 Visible="false"><asp:Label id="Label16" runat="server" Width="79px" Text="Amt. Reward" __designer:wfdid="w148"></asp:Label></TD><TD class="Label" align=left colSpan=1 Visible="false"><asp:TextBox id="rAmount" runat="server" Width="75px" CssClass="inpText" MaxLength="20" __designer:wfdid="w149">0.00</asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label18" runat="server" Width="79px" Text="Target Qty" __designer:wfdid="w150"></asp:Label></TD><TD class="Label" align=left Visible="true"><asp:TextBox id="TargetQty" runat="server" Width="75px" CssClass="inpText" MaxLength="15" __designer:wfdid="w151">0.00</asp:TextBox></TD><TD class="Label" align=left Visible="true"><asp:Label id="Label22" runat="server" Width="86px" Text="Reward2 (%)" __designer:wfdid="w152"></asp:Label></TD><TD class="Label" align=left Visible="true"><asp:TextBox id="PersenReward2" runat="server" Width="75px" CssClass="inpText" MaxLength="20" __designer:wfdid="w153">0.00</asp:TextBox></TD><TD class="Label" align=left colSpan=4 Visible="false"><asp:Label id="Label15" runat="server" Width="79px" Text="Qty Reward" __designer:wfdid="w154"></asp:Label></TD><TD class="Label" align=left colSpan=1 Visible="false"><asp:TextBox id="rQty" runat="server" Width="75px" CssClass="inpText" MaxLength="20" __designer:wfdid="w155">0.00</asp:TextBox></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left Visible="false"><asp:Label id="Label11" runat="server" Width="79px" Text="Target Amt" __designer:wfdid="w156"></asp:Label></TD><TD class="Label" align=left Visible="true"><asp:TextBox id="TargetAmt" runat="server" Width="75px" CssClass="inpText" MaxLength="20" __designer:wfdid="w157">0.00</asp:TextBox></TD><TD class="Label" align=left Visible="true"></TD><TD class="Label" align=left Visible="true"></TD><TD class="Label" align=left colSpan=4 Visible="false"></TD><TD class="Label" align=left colSpan=1 Visible="false"><asp:ImageButton id="ImgAddReward" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w158" AlternateText="Add to List"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=9><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: beige"><asp:GridView id="GvRewardDtl" runat="server" Width="99%" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w159" EmptyDataText="No Data" DataKeyNames="itemdesc,TargetPoint,TargetAmt,rQty,rAmount,TypeNya,seq,itemoid,targetpoint,targetamt,targetqty,persenreward,persenreward2">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item. Reward">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TargetPoint" HeaderText="Target Point">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TargetAmt" HeaderText="Target Amt">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rQty" HeaderText="Qty Reward">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="rAmount" HeaderText="Amt. Reward">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="targetqty" HeaderText="Target Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="persenreward" HeaderText="Reward1(%)">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="persenreward2" HeaderText="Reward2(%)">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="TypeNya" HeaderText="Type Reward">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" Font-Bold="True" ForeColor="Red" Width="2px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="2px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="RewardOid" HeaderText="RewardOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="itemoid" HeaderText="itemoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="lblmsg3" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data found !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD class="Label" align=left colSpan=9>Last Update On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w160"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w161"></asp:Label> </TD></TR><TR><TD class="Label" align=left colSpan=9><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w162" CommandName="Update"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w163" CommandName="Cancel"></asp:ImageButton> <asp:ImageButton id="BtnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w164" CommandName="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbApproval" runat="server" ImageUrl="~/Images/sendapproval.png" ImageAlign="AbsBottom" __designer:dtid="1688849860263971" __designer:wfdid="w165" Visible="False"></asp:ImageButton> <asp:Button id="BtnSaveAs" runat="server" CssClass="btn green" Text="Save As" __designer:wfdid="w166" Visible="False"></asp:Button></TD></TR><TR><TD class="Label" align=center colSpan=9><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w167" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w168"></asp:Image><BR />LOADING....</SPAN><BR /></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="FTEPoint" runat="server" __designer:wfdid="w169" TargetControlID="TargetPoint" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEAmt" runat="server" __designer:wfdid="w170" TargetControlID="TargetAmt" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FTErAmt" runat="server" __designer:wfdid="w171" TargetControlID="rAmount" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTErQty" runat="server" __designer:wfdid="w172" TargetControlID="rQty" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="ceFinishDate" runat="server" __designer:wfdid="w173" TargetControlID="promfinishdate" PopupButtonID="ibfinishdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEEfDate" runat="server" __designer:wfdid="w174" TargetControlID="promfinishdate" Enabled="true" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MEEsDate" runat="server" __designer:wfdid="w175" TargetControlID="promstartdate" Enabled="true" CultureName="id-ID" UserDateFormat="DayMonthYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceStartDate" runat="server" __designer:wfdid="w176" TargetControlID="promstartdate" PopupButtonID="ibstartdate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Promo Supplier :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w145"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w146"></asp:Label></TD></TR><TR><TD vAlign=top><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px" __designer:wfdid="w147"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" CssClass="Important" __designer:wfdid="w148"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="BtnOk" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w149"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" __designer:wfdid="w150" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelValidasi" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w151">
            </asp:Button><asp:Label id="sValidasi" runat="server" __designer:dtid="281474976710732" Visible="False" __designer:wfdid="w1"></asp:Label>
</ContentTemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>
