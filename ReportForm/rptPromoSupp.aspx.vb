Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class RptPromoSupp
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMat") = dt
        End If

        If Not Session("TblListMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblListMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVItemList.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVItemList.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListMatView") = dt
        End If
    End Sub

    Private Sub UpdCheckedPro()
        If Session("TblListPro") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListPro")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVPromo.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVPromo.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "promooid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListPro") = dt
        End If

        If Not Session("TblListProView") Is Nothing Then
            Dim dt As DataTable = Session("TblListProView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To GVPromo.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = GVPromo.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next

                    dv.RowFilter = "promooid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListProView") = dt
        End If
    End Sub

    Private Sub UpdCheckedSupp()
        If Session("TblListSupp") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListSupp")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvSupp.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvSupp.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "suppoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListSupp") = dt
        End If

        If Not Session("TblListSuppView") Is Nothing Then
            Dim dt As DataTable = Session("TblListSuppView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvSupp.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvSupp.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next

                    dv.RowFilter = "suppoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListSuppView") = dt
        End If
    End Sub

#End Region

#Region "Procedure"
    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = ""
        Dim namaPDF As String = "PromoSupplier_" & DDLPromoType.SelectedItem.Text
        Dim sWhere As String = "" : Dim itemoid As String = ""

        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            lblkonfirmasi.Text = "Period report is invalid!"
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            lblkonfirmasi.Text = "Period 2 must be more than Period 1!"
        End If
        Dim crConnInfo As New ConnectionInfo
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With

        Try
            vReport = New ReportDocument

            If TypeDDL.SelectedValue = "SUMMARY" Then
                If DDLPromoType.SelectedValue = "QTYBARANG" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppPerBarangExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppPerBarang.rpt"))
                    End If
                ElseIf DDLPromoType.SelectedValue = "POINT" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppPointExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppPoint.rpt"))
                    End If
                ElseIf DDLPromoType.SelectedValue = "ASPEND" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppAspendExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppAspend.rpt"))
                    End If
                ElseIf DDLPromoType.SelectedValue = "AMOUNT" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppAmtExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppAmt.rpt"))
                    End If
                ElseIf DDLPromoType.SelectedValue = "QTYTOTAL" Then
                    If tipe = "excel" Then
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppTotQtyExl.rpt"))
                    Else
                        vReport.Load(Server.MapPath("~\Report\rptPromSuppTotQty.rpt"))
                    End If
                End If
            End If

            If DDLPromoType.SelectedValue = "AMOUNT" Then
                sWhere = " Where bd.cmpcode='" & CompnyCode & "' AND bd.trnbelimstoid IN (Select DISTINCT bm.trnbelimstoid From QL_trnbelimst bm Where bm.trnbelimstoid=bd.trnbelimstoid AND bm.promodate>=Isnull((Select promodate1 from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') AND bm.promodate<=Isnull((Select promodate2 from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'')) AND Isnull((Select promocode from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') <> ''"
            Else
                sWhere = " Where bd.cmpcode='MSC' AND bd.trnbelimstoid IN (Select bm.trnbelimstoid From QL_trnbelimst bm Where bm.trnbelimstoid=bd.trnbelimstoid AND bm.promodate>=pm.promodate1 AND bm.promodate<=pm.promodate2)"
            End If

            If GVItemList.Visible = True Then
                UpdateCheckedMat()
                Dim dBar As DataTable = Session("TblListMatView")
                Dim MisV As DataView = dBar.DefaultView
                MisV.RowFilter = "checkvalue='True'"
                If MisV.Count > 0 Then
                    For L1 As Int32 = 0 To MisV.Count - 1
                        itemoid &= MisV(L1)("itemoid").ToString & ","
                    Next
                    sWhere &= " AND bd.itemoid IN (" & Left(itemoid.Trim, itemoid.Trim.Length - 1) & ")"
                Else
                    lblkonfirmasi.Text = "- Maaf, Silahkan pilih Katalog..!!<br />"
                End If
            End If

            If GVPromo.Visible = True Then
                UpdCheckedPro()
                Dim dBaw As DataTable = Session("TblListProView")
                Dim MisP As DataView = dBaw.DefaultView
                MisP.RowFilter = "checkvalue='True'"
                If MisP.Count > 0 Then
                    For L1 As Int32 = 0 To MisP.Count - 1
                        Oid.Text &= MisP(L1)("promooid").ToString & ","
                    Next
                    sWhere &= " AND pm.promooid IN (" & Left(Oid.Text.Trim, Oid.Text.Trim.Length - 1) & ")"
                Else
                    lblkonfirmasi.Text = "- Maaf, Silahkan pilih Promo..!!<br />"
                End If
            End If

            If gvSupp.Visible = True Then
                UpdCheckedSupp()
                Dim dBa As DataTable = Session("TblListSuppView")
                Dim Mis As DataView = dBa.DefaultView
                Mis.RowFilter = "checkvalue='True'"
                If Mis.Count > 0 Then
                    For L1 As Int32 = 0 To Mis.Count - 1
                        suppoid.Text &= Mis(L1)("suppoid").ToString & ","
                    Next
                    sWhere &= " AND mst.trnsuppoid IN (" & Left(suppoid.Text.Trim, suppoid.Text.Trim.Length - 1) & ")"
                Else
                    lblkonfirmasi.Text = "- Maaf, Silahkan pilih Supplier..!!<br />"
                End If
            End If

            If lblkonfirmasi.Text <> "" Then
                showMessage(lblkonfirmasi.Text, 2)
                Exit Sub
            End If

            If DDLPromoType.SelectedValue <> "ALL" Then 'inpTextDisabled
                If DDLPromoType.SelectedValue <> "AMOUNT" Then
                    sWhere &= " AND typenya='" & DDLPromoType.SelectedValue & "'"
                End If
            End If

            If TypeDDL.SelectedValue = "SUMMARY" Then
                If DDLPromoType.SelectedValue = "QTYBARANG" Then
                    sSql = "Select msc,su.branch_code,ISnull(su.promooid,0) promooid,promocode,kodebarang,namabarang,'" & DDLPromoType.SelectedItem.Text & "' typenya,su.itemoid,promoname,promodate1,promodate2,SUM(trnbelidtlqty) trnbelidtlqty,SUM(NettPI) NettPI,SUM(DPP) DPP,SUM(NettPIRett) NettPIRett,SUM(AmtNettRet) AmtNettRet,SUM(AmtNettPI) AmtNettPI,SUM(QTyRet) QtyRet,SUM(NettQty) NettQty,su.priceitem,su.targetqty,/*Isnull(Case When SUM(NettQty)>=su.targetqty then rw.itemdesc Else '' End,'')*/rw.itemdesc AS ItemReward,/*Isnull(Case When SUM(NettQty)>=su.targetqty then rw.qtyreward Else 0.00 End,0.00)*/rw.qtyreward AS QtyReward,/*Isnull(Case When SUM(NettQty)>=su.targetqty then rw.persenreward Else 0.00 End,0.00)*/rw.persenreward AS PersenReward,/*(Isnull(Case When SUM(NettQty)>=su.targetqty then rw.persenreward Else 0.00 End,0.00)/100)*SUM(AmtNettPI)*/CASE WHEN rw.persenreward<>0 THEN ((rw.persenreward/100)*SUM(AmtNettPI)) ELSE rw.amtreward END AS AmtReward,su.statuspromo,'' ItemRewardReal,0.0 QtyRewardReal,0.0 PersenRewardReal,0.0 AmtRewardReal,rw.rewarddtloid From (" & _
                    "Select 'MULTI SARANA COMPUTER' msc,promooid,mst.branch_code,i.itemcode as kodebarang,i.itemdesc as namabarang,pm.typenya,bd.itemoid,pm.point,pm.promocode,pm.promoname,pm.promodate1,pm.promodate2,bd.trnbelidtlqty,((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3)) NettPI,Isnull(QtyRet,0.00) QtyRet,ISnull(NettRet,0.00) AmtNettRet,((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-(Isnull(NettRet,0.00)))/1.1) DPP,(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-(Isnull(NettRet,0.00))) NettPIRett,((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-(Isnull(NettRet,0.00)))/1.1) AmtNettPI,bd.trnbelidtlqty-Isnull(QtyRet,0.00) NettQty,(bd.amtdiscidr+bd.amtdisc3) AmtDisc,pm.targetqty,pm.aspend,priceitem,pm.statuspromo From ql_trnbelidtl bd Inner Join QL_trnbelimst mst On mst.trnbelimstoid=bd.trnbelimstoid INNER JOIN ql_mstitem i On bd.itemoid = i.itemoid LEFT JOIN (Select rm.trnbelireturno,rm.trnsuppoid,rm.trnbelino,rd.itemoid,rd.trnbelireturdtlqty QtyRet,(rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-(rd.amtdisc+rd.amtdisc3) NettRet,rm.trnbelimstoid,rd.trnbelidtloid From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid) rmd ON rmd.itemoid=i.itemoid AND rmd.trnsuppoid=mst.trnsuppoid AND mst.trnbelimstoid=rmd.trnbelimstoid AND rmd.trnbelidtloid=bd.trnbelidtloid Inner Join (Select pm.promooid,pm.promocode,pm.promoname,pd.itemoid,pd.point,pm.promodate1,pm.promodate2,spd.suppoid,pm.typenya,pd.targetqty,pd.aspend,pd.priceitem,pm.statuspromo " & _
                    "From QL_mstpromosuppdtl pd Inner Join QL_mstpromosupp pm ON pm.promooid=pd.promooid Inner join QL_mstsuppdtl spd ON spd.promooid=pm.promooid Where pm.statuspromo IN ('APPROVED','CLOSED')) pm ON pm.itemoid=bd.itemoid AND pm.suppoid=mst.trnsuppoid AND typenya IN ('QTYBARANG') " & sWhere & ") Su Left Join (Select promooid,itemdesc,qtyreward,persenreward,rw.rewarddtloid,rw.amtreward From QL_mstrewarddtl rw) rw ON rw.promooid=su.promooid Group by msc,su.promooid,promocode,kodebarang,namabarang,typenya,su.itemoid,promocode,promoname,promodate1,su.point,su.branch_code,su.targetqty,promodate2,su.aspend,su.priceitem,rw.itemdesc,rw.qtyreward,rw.persenreward,rw.rewarddtloid,rw.amtreward,statuspromo Order by promooid,rewarddtloid,kodebarang"
                ElseIf DDLPromoType.SelectedValue = "POINT" Then
                    sSql = "Select pm.promooid,pm.promocode,'TARGET POINT' typenya,pm.promoname,pm.promodate1,pm.promodate2,pm.statuspromo,SUM(pm.point) Point,SUM(bd.trnbelidtlqty) trnbelidtlqty,SUM((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3)) AmtBeliNetto,SUM(ISnull(NettRet,0.00)) AmtNettRet,SUM((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00))/1.1) AmtNettPI,pm.targetpoint,Case When SUM(pm.point*(bd.trnbelidtlqty-Isnull(QtyRet,0.00)))>=pm.targetpoint Then pm.itemdesc Else '-' End ItemReward,Case When SUM(pm.point*(bd.trnbelidtlqty-Isnull(QtyRet,0.00)))>=pm.targetpoint Then pm.qtyreward Else 0.00 End QtyReward,SUM(pm.point*(bd.trnbelidtlqty-Isnull(QtyRet,0.00))) TotalPoint,SUM((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00))/1.1) DPP,SUM(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-Isnull(NettRet,0.00)) AmtPiRett,SUM(Isnull(QtyRet,0.00)) QTyRet,SUM(bd.trnbelidtlqty-Isnull(QtyRet,0.00)) NettQty From ql_trnbelidtl bd INNER JOIN ql_trnbelimst mst On mst.trnbelimstoid = bd.trnbelimstoid LEFT JOIN (Select rm.trnbelireturno,rm.trnsuppoid,rm.trnbelino,(rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-(rd.amtdisc+rd.amtdisc3) NettRet,rm.trnbelimstoid,rd.trnbelidtloid,ISNULL(rd.trnbelireturdtlqty,0.00) QtyRet From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid) rmd ON rmd.trnsuppoid=mst.trnsuppoid AND mst.trnbelimstoid=rmd.trnbelimstoid AND bd.trnbelidtloid=rmd.trnbelidtloid INNER JOIN (Select pm.promooid,pm.promocode,pm.promoname,pd.itemoid,pd.point ,pm.promodate1,pm.promodate2,spd.suppoid,pm.typenya,pd.targetqty,pm.statuspromo,targetpoint,rd.itemdesc,rd.qtyreward from QL_mstpromosuppdtl pd Inner Join QL_mstpromosupp pm ON pm.promooid=pd.promooid Inner join QL_mstsuppdtl spd ON spd.promooid=pm.promooid INNER JOIN QL_mstrewarddtl rd ON rd.promooid=pm.promooid Where pm.statuspromo IN ('APPROVED','CLOSED')) pm ON pm.itemoid=bd.itemoid AND pm.suppoid=mst.trnsuppoid " & sWhere & " Group BY pm.promooid,pm.promocode,pm.promoname,pm.promodate1,pm.promodate2,pm.typenya,pm.statuspromo,targetpoint,pm.itemdesc,pm.qtyreward"
                ElseIf DDLPromoType.SelectedValue = "ASPEND" Then
                    sSql = "Select msc,su.branch_code,su.promooid,promocode,kodebarang,namabarang,'" & DDLPromoType.SelectedItem.Text & "' typenya,su.itemoid,su.statuspromo,promoname,promodate1,promodate2,SUM(amtbelinetto) amtbelinetto,SUM(AmtNettRet) AmtNettRet,SUM(AmtNettPI) AmtNettPI,su.aspend,su.priceitem,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 rd.persenreward From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 rd.persenreward From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else 0.00 End persenreward,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 rd.persenreward2 From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 rd.persenreward2 From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else 0.00 End persenreward2,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 (rd.persenreward2/100)*SUM(AmtNettPI) From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 (rd.persenreward/100)*SUM(AmtNettPI) From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else 0.00 End RewardNya,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else 0.00 End targetamt ,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 rd.itemdesc From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 rd.itemdesc From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else '' End ItemReward,Case When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)>=SUM(AmtNettPI) Then (Select Top 1 rd.qtyreward From QL_mstrewarddtl rd Where rd.targetamt>=SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) When (Select Top 1 rd.targetamt From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc)<SUM(AmtNettPI) then (Select Top 1 rd.qtyreward From QL_mstrewarddtl rd Where rd.targetamt<SUM(AmtNettPI) AND rd.promooid=su.promooid Order by targetamt Desc) Else 0.00 End QtyReward,Isnull(SUM(QTyRet),0.00) QTyRet,SUM(trnbelidtlqty) QtyPI,SUM(trnbelidtlqty)-Isnull(SUM(QTyRet),0.00) NettQty,Sum(DPP) DPP,SUM(TotalAmt) TotalAmt From (" & _
                    "Select 'MULTI SARANA COMPUTER' msc,promooid,mst.branch_code,i.itemcode as kodebarang ,i.itemdesc as namabarang,pm.typenya,bd.itemoid,pm.point,pm.promocode,pm.promoname,pm.promodate1,pm.promodate2,bd.trnbelidtlqty,(pm.aspend*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3) AmtBeliNetto,isnull((Select sum(amount) From QL_CreditNote Where refoid=bd.trnbelimstoid and reftype='HUTANG'),0) CreditNote,isnull((Select sum(amount) From QL_DebitNote Where refoid=bd.trnbelimstoid and reftype='HUTANG'),0) DebitNote,Isnull(QtyRet,0.00) QTyRet,ISnull(NettRet,0.00) AmtNettRet,((((pm.aspend*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00))/1.1) AmtNettPI,bd.trnbelidtlqty-Isnull(QtyRet,0.00) NettQty,(bd.amtdiscidr+bd.amtdisc3) AmtDisc,pm.targetqty,pm.aspend,priceitem,pm.statuspromo,((((pm.aspend*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00))/1.1) DPP,(((pm.aspend*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00)) TotalAmt From ql_trnbelidtl bd Inner Join QL_trnbelimst mst On mst.trnbelimstoid=bd.trnbelimstoid INNER JOIN ql_mstitem i On bd.itemoid = i.itemoid LEFT JOIN (Select rm.trnbelireturno,rm.trnsuppoid,rm.trnbelino,rd.itemoid,rd.trnbelireturdtlqty QtyRet,(rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-(rd.amtdisc+rd.amtdisc3) NettRet,rm.trnbelimstoid,rd.trnbelidtloid From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid) rmd ON rmd.itemoid=i.itemoid AND rmd.trnsuppoid=mst.trnsuppoid AND mst.trnbelimstoid=rmd.trnbelimstoid And rmd.trnbelidtloid=bd.trnbelidtloid INNER Join (Select pm.promooid,pm.promocode,pm.promoname,pd.itemoid,pd.point,pm.promodate1,pm.promodate2,spd.suppoid,pm.typenya,pd.targetqty,pd.aspend,pd.priceitem,pm.statuspromo from QL_mstpromosuppdtl pd Inner Join QL_mstpromosupp pm ON pm.promooid=pd.promooid Inner join QL_mstsuppdtl spd ON spd.promooid=pm.promooid Where pm.statuspromo IN ('APPROVED','CLOSED')) pm ON pm.itemoid=bd.itemoid AND pm.suppoid=mst.trnsuppoid AND typenya IN ('ASPEND') " & sWhere & _
                    " ) Su Group by msc,su.promooid,promocode,kodebarang,su.itemoid,promocode,promoname,promodate1,su.point,su.branch_code,su.targetqty,promodate2,su.aspend,su.priceitem,namabarang,typenya,su.statuspromo Order By kodebarang"
                ElseIf DDLPromoType.SelectedValue = "AMOUNT" Then
                    sSql = "Select pm.promooid,'MULTI SARANA COMPUTER' msc,mst.trnsuppoid,(Select suppname from QL_mstsupp sup Where suppoid=mst.trnsuppoid) suppname,'TARGET AMOUNT' typepromo,SUM((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3)) AmtBeliNetto,SUM((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00)))/1.1 AmtNettPI,Sum(bd.amtdiscidr+bd.amtdisc3) AmtDisc,rd.targetamt,(rd.persenreward/100)*(SUM((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00)))/1.1) AmtReward,SUM((((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISNULL(NettRet,0.00)))/1.1 DPP,ISnull(Sum(NettRet),0.00) NettRet,Sum(QtyRet) QtyRet,SUM(Isnull(bd.trnbelidtlqty,0.00)-ISNULL(QtyRet,0.00)) NettQty,SUM(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-ISnull(NettRet,0.00)) AmtNettRet,SUM(bd.trnbelidtlqty) QtyPI,rd.qtyreward,rd.persenreward, (rd.persenreward/100) amtPersen, 0.0 AS amtRT,Isnull((Select promocode from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') promocode,Isnull((Select promodate1 from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') promodate1,Isnull((Select promodate2 from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') promodate2,Isnull((Select statuspromo from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') statuspromo,Isnull((Select typenya from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') typenya,Isnull((Select promoname from QL_mstpromosupp pms Where pms.promooid=pm.promooid AND typenya='AMOUNT'),'') promoname,rd.itemdesc,'-' itemdesct From ql_trnbelidtl bd Inner Join QL_trnbelimst mst On mst.trnbelimstoid=bd.trnbelimstoid Inner join QL_mstsuppdtl pm ON pm.suppoid=mst.trnsuppoid Inner JOin QL_mstpromosuppdtl pd ON pd.itemoid=bd.itemoid AND pm.promooid=pd.promooid INNER JOIN QL_mstrewarddtl rd ON rd.promooid=pd.promooid LEFT JOIN (Select rm.trnsuppoid,rd.itemoid,rm.trnbelimstoid,Isnull((rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-(rd.amtdisc+rd.amtdisc3),0.00) NettRet,rd.trnbelidtloid,Isnull(rd.trnbelireturdtlqty,0.00) QtyRet From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid) rmd ON rmd.itemoid=bd.itemoid AND rmd.trnsuppoid=mst.trnsuppoid AND mst.trnbelimstoid=rmd.trnbelimstoid And rmd.trnbelidtloid=bd.trnbelidtloid " & sWhere & " Group BY pm.promooid,mst.trnsuppoid ,rd.targetamt,rd.persenreward,rd.qtyreward,rd.itemdesc"
                ElseIf DDLPromoType.SelectedValue = "QTYTOTAL" Then
                    sSql = "Select msc,su.branch_code,promocode,su.suppoid,su.suppname,'" & DDLPromoType.SelectedItem.Text & "' typenya,promoname,promodate1,promodate2,SUM(amtbelinetto) amtbelinetto,SUM(CreditNote) CreditNote,SUM(DebitNote) DebitNote,SUM(AmtNettRet) AmtNettRet,SUM(AmtNettPI) AmtNettPI,SUM(AmtDisc) AmtDisc,SUM(NettQty) NettQty,Case When SUM(NettQty)>=(Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where rd.targetqty<=SUM(NettQty) AND rd.promooid=su.promooid Order by targetqty DESC) Then (Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty)) AND rd.promooid=su.promooid Order by targetqty DESC) Else (Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty) And rd.promooid = su.promooid) Order by targetqty ASC) End targetqty,Case When SUM(NettQty)>=(Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where rd.targetamt<=SUM(NettQty) AND rd.promooid=su.promooid Order by targetqty DESC) Then (Select Top 1 rd.persenreward From QL_mstrewarddtl rd Where(rd.targetamt <= SUM(NettQty)) AND rd.promooid=su.promooid Order by targetqty DESC) Else (Select Top 1 rd.persenreward From QL_mstrewarddtl rd Where(rd.targetamt <= SUM(NettQty) And rd.promooid = su.promooid) Order by targetqty ASC) End persenreward,Case When SUM(NettQty)>=(Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where rd.targetqty<=SUM(NettQty) AND rd.promooid=su.promooid Order by targetqty DESC) Then ISNULL((Select Top 1 rd.persenreward/100 From QL_mstrewarddtl rd Where rd.targetqty<=SUM(NettQty) AND rd.promooid=su.promooid Order by targetqty DESC),0.00)*SUM(AmtNettPI) Else Isnull((Select Top 1 rd.persenreward/100 From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty) And rd.promooid = su.promooid) Order by targetqty ASC),0.00)*SUM(AmtNettPI) End AmtReward,Case When SUM(NettQty)>=(Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where rd.targetqty<=SUM(NettQty) AND rd.promooid=su.promooid Order by Targetqty DESC) Then ISNULL((Select Top 1 rd.itemdesc From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty)) AND rd.promooid=su.promooid Order by targetqty DESC),'') Else Isnull((Select Top 1 rd.itemdesc From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty) And rd.promooid = su.promooid) Order by targetqty ASC),'') End ItemReward,Case When SUM(NettQty)>=(Select Top 1 rd.targetqty From QL_mstrewarddtl rd Where rd.targetqty<=SUM(NettQty) AND rd.promooid=su.promooid Order by targetqty DESC) Then ISNULL((Select Top 1 rd.qtyreward From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty)) AND rd.promooid=su.promooid Order by targetqty DESC),0.00) Else Isnull((Select Top 1 rd.qtyreward From QL_mstrewarddtl rd Where(rd.targetqty <= SUM(NettQty) And rd.promooid = su.promooid) Order by targetqty ASC),0.00) End QtyReward,statuspromo,su.promooid,SUM(QtyRet) QtyRet,SUM(trnbelidtlqty) QtyBeli,SUM(DPP) DPP,Sum(AmtPIRet) AmtPIRet From (" & _
                    "Select 'MULTI SARANA COMPUTER' msc,promooid,mst.branch_code,pm.typenya,pm.promocode,pm.promoname,pm.promodate1,pm.promodate2,((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3)) AmtBeliNetto,Isnull(QtyRet,0.00) QtyRet,Isnull(NettRet,0.00) AmtNettRet,(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-(Isnull(NettRet,0.00)))/1.1DPP,(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-Isnull(NettRet,0.00)) AmtPIRet,(((bd.trnbelidtlprice*bd.trnbelidtlqty)-(bd.amtdiscidr+bd.amtdisc3))-(Isnull(NettRet,0.00)))/1.1 AmtNettPI,isnull((Select sum(amount) From QL_CreditNote Where refoid=bd.trnbelimstoid and reftype='HUTANG'),0) CreditNote,isnull((Select sum(amount) From QL_DebitNote Where refoid=bd.trnbelimstoid and reftype='HUTANG'),0) DebitNote,(bd.amtdiscidr+bd.amtdisc3) AmtDisc,mst.trnsuppoid suppoid,(Select suppname from QL_mstsupp sup Where suppoid=mst.trnsuppoid) suppname,pm.statuspromo,bd.trnbelidtlqty,bd.trnbelidtlqty-Isnull(QtyRet,0.00) NettQty From ql_trnbelidtl bd Inner Join QL_trnbelimst mst On mst.trnbelimstoid=bd.trnbelimstoid INNER JOIN ql_mstitem i On bd.itemoid = i.itemoid LEFT JOIN (Select rm.trnbelireturno,rm.trnsuppoid,rm.trnbelino,rd.trnbelidtloid,rd.itemoid,(rd.trnbelireturdtlqty*rd.trnbelireturdtlprice)-(rd.amtdisc+rd.amtdisc3) NettRet,rm.trnbelimstoid,rd.trnbelireturdtlqty QtyRet From QL_trnbeliReturdtl rd INNER JOIN QL_trnbeliReturmst rm ON rm.trnbeliReturmstoid=rd.trnbeliReturmstoid) rmd ON rmd.itemoid=i.itemoid AND rmd.trnsuppoid=mst.trnsuppoid AND mst.trnbelimstoid=rmd.trnbelimstoid And rmd.trnbelidtloid=bd.trnbelidtloid Inner Join (Select pm.promooid,pm.promocode,pm.promoname,pd.itemoid,pm.promodate1,pm.promodate2,spd.suppoid,pm.typenya,pd.targetqty,pm.statuspromo From QL_mstpromosuppdtl pd Inner Join QL_mstpromosupp pm ON pm.promooid=pd.promooid Inner join QL_mstsuppdtl spd ON spd.promooid=pm.promooid Where pm.statuspromo IN ('APPROVED','CLOSED') AND typenya='QTYTOTAL') pm ON pm.itemoid=bd.itemoid AND pm.suppoid=mst.trnsuppoid " & sWhere & ") Su Group by msc,su.promooid,promocode,typenya,promocode,promoname,promodate1,su.branch_code,promodate2,su.suppoid,su.suppname,statuspromo,su.promooid Order by suppname"
                End If
            End If

            If TypeDDL.SelectedValue = "SUMMARY" Then
                Dim dtTbl As DataTable = ckon.ambiltabel(sSql, "QL_mstpromsupp")
                If DDLPromoType.SelectedValue = "AMOUNT" Then
                    For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                        If ToDouble(dtTbl.Compute("SUM(DPP)", "promooid=" & dtTbl.Rows(C1).Item("promooid") & "")) >= ToDouble(dtTbl.Rows(C1).Item("targetamt")) Then
                            dtTbl.Rows(C1).Item("amtRT") = ToDouble(dtTbl.Compute("SUM(DPP)", "promooid=" & dtTbl.Rows(C1).Item("promooid") & "")) * ToDouble(dtTbl.Rows(C1).Item("amtPersen"))
                            dtTbl.Rows(C1).Item("itemdesct") = dtTbl.Rows(C1).Item("itemdesc")
                        End If
                    Next
                    dtTbl.AcceptChanges()
                ElseIf DDLPromoType.SelectedValue = "QTYBARANG" Then
                    Dim iCurrentPromoOid As Integer = 0
                    Dim iCurrentRewardOid As Integer = 0
                    Dim sItemReward As String = ""
                    Dim dQtyReward As Double = 0
                    Dim dPersenReward As Double = 0
                    Dim dAmtReward As Double = 0
                    ' Copy Table Aslinya utk Proses Checking pencapaian tiap barang, per promo per reward
                    Dim dtProcess As DataTable = dtTbl.Copy
                    For R1 As Integer = 0 To dtProcess.Rows.Count - 1
                        If (iCurrentPromoOid <> dtProcess.Rows(R1)("promooid").ToString Or iCurrentRewardOid <> ToDouble(dtProcess.Rows(R1)("rewarddtloid").ToString)) Then
                            ' Cek Tiap Promo Per Reward Jika ada barang yang tidak mencapai target Qty
                            iCurrentPromoOid = dtProcess.Rows(R1)("promooid").ToString
                            iCurrentRewardOid = ToDouble(dtProcess.Rows(R1)("rewarddtloid").ToString)
                            Dim dvCount As DataView = dtProcess.DefaultView
                            dvCount.RowFilter = "promooid=" & iCurrentPromoOid & " AND (trnbelidtlqty-QTyRet)<targetqty AND rewarddtloid=" & iCurrentRewardOid
                            If dvCount.Count = 0 Then
                                sItemReward = dtProcess.Rows(R1)("ItemReward").ToString
                                dQtyReward = ToDouble(dtProcess.Rows(R1)("QtyReward").ToString)
                                dPersenReward = ToDouble(dtProcess.Rows(R1)("PersenReward").ToString)
                                dAmtReward = ToDouble(dtProcess.Rows(R1)("AmtReward").ToString)
                            End If
                            dvCount.RowFilter = ""

                            ' Update Realisasi Reward
                            If sItemReward <> "" Then
                                Dim dvUpdate As DataView = dtTbl.DefaultView
                                dvUpdate.RowFilter = "promooid=" & iCurrentPromoOid & " AND rewarddtloid=" & iCurrentRewardOid
                                If dvUpdate.Count > 0 Then
                                    For R2 As Integer = 0 To dvUpdate.Count - 1
                                        dvUpdate(R2)("ItemRewardReal") = sItemReward
                                        dvUpdate(R2)("QtyRewardReal") = dQtyReward
                                        dvUpdate(R2)("PersenRewardReal") = dPersenReward
                                        dvUpdate(R2)("AmtRewardReal") = dAmtReward
                                    Next
                                End If
                                dvUpdate.RowFilter = ""
                                dtTbl.AcceptChanges()
                            End If
                        End If
                    Next
                End If
                Dim dvTbl As DataView = dtTbl.DefaultView
                vReport.SetDataSource(dvTbl.ToTable)
            Else
                vReport = New ReportDocument
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptPromoSuppDtlExcl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptPromoSuppDtl.rpt"))
                End If
            End If

            If TypeDDL.SelectedValue = "SUMMARY" Then
                vReport.SetParameterValue("sWhere", DDLPromoType.SelectedItem.Text)
            Else
                vReport.SetParameterValue("sWhere", sWhere)
            End If

            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            setDBLogonForReport(crConnInfo, vReport)
            If tipe = "" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "pdf" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Promo_Supplier" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Promo_Supplier" & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Public Sub BindSupp(ByVal sWhere As String)

        sSql = "SELECT distinct 'False' AS checkvalue,suppoid, suppcode, suppname, supptype,0 promoid FROM ql_mstsupp su WHERE (suppname LIKE '%" & Tchar(suppname.Text) & "%' OR suppcode LIKE '%" & Tchar(suppname.Text) & "%') AND suppoid IN (Select suppoid from QL_mstsuppdtl sp Where su.suppoid=sp.suppoid " & sWhere & ")"
        Dim dts As DataTable = ckon.ambiltabel(sSql, "ql_mstsupp")
        Session("TblListSupp") = dts
        Session("TblListSuppView") = Session("TblListSupp")
        gvSupp.DataSource = Session("TblListSuppView")
        gvSupp.DataBind() : gvSupp.SelectedIndex = -1
        gvSupp.Visible = True
    End Sub

    Public Sub BindPromo(ByVal sFilter As String)
        Try
            Dim dat1 As Date = CDate(toDate(dateAwal.Text))
            Dim dat2 As Date = CDate(toDate(dateAkhir.Text))
        Catch ex As Exception
            showMessage("- Maaf, Periode tidak valid..!", 2)
            Exit Sub
        End Try

        If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
            showMessage("- Maaf, Periode 2 tidak boleh lebid dari periode 1 ..!", 2)
            Exit Sub
        End If

        sSql = "Select 'False' AS checkvalue,promooid,promocode,promoname,typenya,statuspromo From QL_mstpromosupp pr Where statuspromo IN ('APPROVED','CLOSED') AND typenya='" & DDLPromoType.SelectedValue & "' " & sFilter & ""
        Dim dtp As DataTable = ckon.ambiltabel(sSql, "QL_mstPromoSupp")
        If dtp.Rows.Count > 0 Then
            Session("TblListPro") = dtp
            Session("TblListProView") = Session("TblListPro")
            GVPromo.DataSource = Session("TblListProView")
            GVPromo.DataBind() : GVPromo.SelectedIndex = -1
            GVPromo.Visible = True
        End If
    End Sub

    Private Sub BindItemNya(ByVal iFilter As String)
        sSql = "Select distinct 'False' AS checkvalue,i.itemoid,i.itemcode,i.itemdesc,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End TypeBarang,i.pricelist,0 promooid From ql_mstitem i Where itemflag='AKTIF' AND i.itemoid IN (Select pd.itemoid from QL_mstpromosuppdtl pd Where pd.itemoid=i.itemoid " & iFilter & ") And (itemcode like '%" & Tchar(ItemNya.Text) & "%' or itemdesc like '%" & Tchar(ItemNya.Text) & "%')"
        Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstmat")
        If dt.Rows.Count > 0 Then
            Session("TblListMat") = dt
            Session("TblListMatView") = Session("TblListMat")
            GVItemList.DataSource = Session("TblListMatView")
            GVItemList.DataBind() : GVItemList.SelectedIndex = -1
            GVItemList.Visible = True
        End If
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\ReportForm\rptPromoSupp.aspx")
        End If
        Page.Title = CompnyName & " - Rekap Promo Supplier"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        Session("promooid") = Request.QueryString("promooid")
        Session("type") = Request.QueryString("type")

        If Not IsPostBack Then
            dateAwal.Text = Format(GetServerTime(), "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")

            If Session("promooid") IsNot Nothing And Session("promooid") <> "" Then
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    Oid.Text = Session("promooid")
                    DDLPromoType.SelectedValue = Session("type")
                    Dim sqlPlus As String = ""
                    sqlPlus &= " AND promooid = " & Session("promooid") & ""
                    BindPromo(sqlPlus)

                    sSql = "UPDATE QL_mstpromosupp set promores1 = 1 where promooid = " & Session("promooid") & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    objTrans.Commit() : xCmd.Connection.Close()
                Catch ex As Exception
                    objTrans.Rollback() : xCmd.Connection.Close()
                    showMessage(ex.Message, 2) : Exit Sub
                End Try
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        showPrint("")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptPromoSupp.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Session("diprint") = "False"
        showPrint("excel")
    End Sub

    Protected Sub btnEraseSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppname.Text = "" : suppoid.Text = ""
        gvSupp.Visible = False : Session("TblListSuppView") = Nothing
    End Sub

    Protected Sub gvSupp_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        suppname.Text = gvSupp.SelectedDataKey.Item("suppname")
        suppoid.Text = gvSupp.SelectedDataKey.Item("suppoid")
        gvSupp.Visible = False
    End Sub

    Protected Sub gvSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupp.PageIndexChanging
        UpdCheckedSupp()
        gvSupp.PageIndex = e.NewPageIndex
        gvSupp.DataSource = Session("TblListSuppView")
        gvSupp.DataBind()
        'BindSupp()
    End Sub

    Protected Sub GVPromo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVPromo.PageIndexChanging
        UpdCheckedPro()
        GVPromo.PageIndex = e.NewPageIndex
        GVPromo.DataSource = Session("TblListProView")
        GVPromo.DataBind()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PromoNya.Text = "" : Oid.Text = ""
        GVPromo.Visible = False
    End Sub

    Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPdf.Click
        showPrint("pdf")
    End Sub

    Protected Sub GVPromo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVPromo.SelectedIndexChanged
        Oid.Text = GVPromo.SelectedDataKey("promooid")
        PromoNya.Text = GVPromo.SelectedDataKey("promoname")
        GVPromo.Visible = False
    End Sub

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showPrint("")
    End Sub

    Protected Sub sItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles sItemNya.Click
        Dim sWhere As String = ""
        If GVPromo.Visible = True Then
            UpdCheckedPro()
            Dim dBar As DataTable = Session("TblListSuppView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                For L1 As Int32 = 0 To MisV.Count - 1
                    Oid.Text &= MisV(L1)("suppoid").ToString & ","
                Next
                sWhere &= " AND suppoid IN (" & Left(Oid.Text, Oid.Text.Length - 1) & ")"
            End If
        End If

        If GVItemList.Visible = True Then
            UpdateCheckedMat()
            Dim dBar As DataTable = Session("TblListMatView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "(itemcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or itemdesc like '%" & Tchar(ItemNya.Text.Trim) & "%')"
            If MisV.Count > 0 Then
                Session("DataItem") = MisV.ToTable
                GVItemList.DataSource = Session("DataItem")
                GVItemList.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindItemNya(sWhere)
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        UpdateCheckedMat()
        GVItemList.PageIndex = e.NewPageIndex
        GVItemList.DataSource = Session("TblListMatView")
        GVItemList.DataBind()
    End Sub

    Protected Sub BtnViewList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewList.Click
        If GVItemList.Visible = True Then
            UpdateCheckedMat()
            Dim dBar As DataTable = Session("TblListMatView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                Session("DataItem") = MisV.ToTable
                GVItemList.DataSource = Session("DataItem")
                GVItemList.DataBind() : MisV.RowFilter = ""
            End If
        End If
    End Sub

    Protected Sub eItemNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles eItemNya.Click
        ItemNya.Text = "" : Session("TblListMatView") = Nothing
        GVItemList.Visible = False
    End Sub

    Protected Sub btnSearchNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchNota.Click
        If GVPromo.Visible = True Then
            UpdCheckedPro()
            Dim dBar As DataTable = Session("TblListProView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "(promocode like '%" & Tchar(PromoNya.Text.Trim) & "%' or promoname like '%" & Tchar(PromoNya.Text.Trim) & "%')"
            If MisV.Count > 0 Then
                Session("DataProm") = MisV.ToTable
                GVPromo.DataSource = Session("DataProm")
                GVPromo.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindPromo("AND (promocode LIKE '%" & Tchar(PromoNya.Text) & "%' OR promoname LIKE '%" & Tchar(PromoNya.Text) & "%')")
        End If
    End Sub

    Protected Sub BtnViewListProm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewListProm.Click
        If GVPromo.Visible = True Then
            UpdCheckedPro()
            Dim dBar As DataTable = Session("TblListProView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                Session("DataProm") = MisV.ToTable
                GVPromo.DataSource = Session("DataProm")
                GVPromo.DataBind() : MisV.RowFilter = ""
            End If
        End If
    End Sub

    Protected Sub BtnViewListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewListSupp.Click
        If gvSupp.Visible = True Then
            UpdCheckedSupp()
            Dim dBar As DataTable = Session("TblListSuppView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                Session("DataSupp") = MisV.ToTable
                gvSupp.DataSource = Session("DataSupp")
                gvSupp.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindSupp("")
        End If
    End Sub

    Protected Sub btnSearchSupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupplier.Click
        Dim sWhere As String = ""
        If GVPromo.Visible = True Then
            UpdCheckedPro()
            Dim dBar As DataTable = Session("TblListProView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "checkvalue='True'"
            If MisV.Count > 0 Then
                For L1 As Int32 = 0 To MisV.Count - 1
                    Oid.Text &= MisV(L1)("promooid").ToString & ","
                Next
                sWhere &= " AND promooid IN (" & Left(Oid.Text, Oid.Text.Length - 1) & ")"
            End If
        End If

        If gvSupp.Visible = True Then
            UpdCheckedSupp()
            Dim dBar As DataTable = Session("TblListSuppView")
            Dim MisV As DataView = dBar.DefaultView
            MisV.RowFilter = "(suppcode like '%" & Tchar(ItemNya.Text.Trim) & "%' or suppname like '%" & Tchar(ItemNya.Text.Trim) & "%')"
            If MisV.Count > 0 Then
                Session("DataSupp") = MisV.ToTable
                gvSupp.DataSource = Session("DataSupp")
                gvSupp.DataBind() : MisV.RowFilter = ""
            End If
        Else
            BindSupp(sWhere)
        End If
    End Sub

    Protected Sub cbProm_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gvSupp.Visible = False : Session("TblListSuppView") = Nothing
    End Sub

    Protected Sub cbSupp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TblListMatView") = Nothing : GVItemList.Visible = False
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub
#End Region
End Class
