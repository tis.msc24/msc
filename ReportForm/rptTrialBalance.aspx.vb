Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class reportForm_rptTrialBalance
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    'Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure

#End Region

#Region "Functions"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub DDLcbg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'CrystalReportViewer1.Visible = False
    End Sub

    Protected Sub btnViewPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowReport("crv")
    End Sub

    Protected Sub btnExportToPdf_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowReport("pdf")
    End Sub

    Protected Sub ExportToXls_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowReport("xls")
    End Sub

    Protected Sub ibClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\reportForm\rptTrialBalance.aspx?awal=true")
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(tbPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid", 2)
            Return False
        End If
        If Not IsValidDate(tbPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(toDate(tbPeriod1.Text)) > CDate(toDate(tbPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Procedures"

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub InitAllDDL()
        Dim sSql As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLcbg, sSql)
        DDLcbg.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "01" Then
            DDLcbg.SelectedValue = Session("branch_id")
        Else
            DDLcbg.SelectedIndex = DDLcbg.Items.Count - 1
        End If
    End Sub

    Private Sub ShowReport(ByVal sType As String)
        Try
            Dim cmpCode As String = ""
            Dim sDate As String = ""
            Dim periodText As String = ""
            Dim sPeriod As String = GetDateToPeriodAcctg(toDate(tbPeriod1.Text))
            Dim period1 As String = GetDateToPeriod(toDate(tbPeriod1.Text))
            Dim period2 As String = GetDateToPeriod(toDate(tbPeriod2.Text))

            If IsValidPeriod() Then
                sDate &= " AND gm.gldate BETWEEN '" & (toDate(tbPeriod1.Text)) & " 00:00:00' AND '" & (toDate(tbPeriod2.Text)) & " 23:59:59'"
                If Session("branch_id") = "01" Then
                    sDate &= IIf(DDLcbg.SelectedValue = "ALL", " ", " and gm.branch_code= '" & DDLcbg.SelectedValue & "'")
                Else
                    sDate &= "and gm.branch_code='" & Session("branch_id") & "'"
                End If
            Else
                Exit Sub
            End If

            Report = New ReportDocument
            Report.Load(Server.MapPath("~\Report\rptTB.rpt"))
            Report.SetParameterValue("cmpCode", CompnyCode)
            Report.SetParameterValue("sDate", sDate)
            Report.SetParameterValue("outlet", DDLcbg.SelectedItem.Text)
            Report.SetParameterValue("period1", period1)
            Report.SetParameterValue("period2", period2)
            Report.SetParameterValue("sPeriodAcctg", sPeriod)
            Report.SetParameterValue("sDatePeriod", Format((toDate(tbPeriod1.Text)), "dd/MM/yyyy") & " - " & Format((toDate(tbPeriod2.Text)), "dd/MM/yyyy"))
            Report.SetParameterValue("reportName", Report.FileName.Substring(Report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Report.PrintOptions.PaperSize = PaperSize.PaperA4
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, Report)
            If sType = "crv" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = Report
            ElseIf sType = "xls" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Trial_Balance_Report_" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptTrialBalance.aspx?awal=true")
            ElseIf sType = "pdf" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Trial_Balance_Report_" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close()
                Report.Dispose()
                Response.Redirect("~\ReportForm\rptTrialBalance.aspx?awal=true")
            Else
            End If
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            showMessage(ex.ToString, 1)
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\reportForm\rptTrialBalance.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Report Trial Balance"
        Session("oid") = Request.QueryString("oid")

        If Not Page.IsPostBack Then
            InitAllDDL()
            tbPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
            tbPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            DDLcbg_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
            End If
        Else
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not Report Is Nothing Then
                If Report.IsLoaded Then
                    Report.Dispose()
                    Report.Close()
                End If
            End If
        Catch ex As Exception
            Report.Dispose()
            Report.Close()
        End Try
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        ShowReport("crv")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\reportform\rptTrialBalance.aspx?awal=true")
            End If
        End If
    End Sub
#End Region
End Class
