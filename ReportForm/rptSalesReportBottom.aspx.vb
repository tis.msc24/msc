Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSalesReportBottom
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))

#End Region

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Sub bindNotaJual()
        Dim ssql As String
        ssql = "SELECT a.trnjualmstoid,a.trnjualno,convert(char(10),a.trnjualdate,103) trnjualdate from QL_trnjualmst a  where a.trnjualno like '%" & invoiceno.Text & "%'  and a.orderno like " & type_so.SelectedValue & "  and a.orderno like '%" & Tchar(sono.Text) & "%'    and  trnjualdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"
        'ssql = "SELECT a.ordermstoid,a.trnorderno,convert(char(10),a.trnjualdate,103) trnorderdate from QL_trnordermst a  where a.orderno like '%" & invoiceno.Text & "%'  and a.orderno like " & type_so.SelectedValue & "  and a.orderno like '%" & Tchar(sono.Text) & "%'    and  trnjualdate between '" & CDate(toDate(range1.Text)) & "' and '" & CDate(toDate(range2.Text)) & "'"

        FillGV(gvListCust, ssql, "ql_mstcust")
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Label2.Text = ""
            CrystalReportViewer1.Visible = True
            Dim sWhere As String = ""
            Dim namaPDF As String = ""

            Try
                If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                    Label2.Text = "Period 2 must be more than Period 1 !"
                    Exit Sub
                End If
            Catch ex As Exception
                Label2.Text = "Please check Period value"
                Exit Sub
            End Try


            'If ddltype.SelectedValue = "Summary" Then
            '    vReport.Load(Server.MapPath("~\Report\SalesReport_Sum.rpt"))
            '    sWhere = " where mst.trnorderdate between '" & toDate(range1.Text) & "' and '" & toDate(range2.Text) & "' and mst.trnordertype='GROSIR' " & IIf(trnjualmstoid.Text <> "", " and mst.ordermstoid = " & trnjualmstoid.Text, " ") & IIf(custoid.Text.Trim = "", " ", " and  mst.trncustname = '" & custname.Text & "' ") & IIf(sales.SelectedValue = "ALL", " ", " and mst.salesoid = " & sales.SelectedValue) & "  and mst.orderno like " & type_so.SelectedValue & "   " & IIf(ToDouble(sooid.Text) > 0, " and mst.orderno = '" & Tchar(sono.Text) & "' ", " ") & paytype.SelectedValue
            '    namaPDF = "Sales_Report_(Sum)_"

            'Else
            vReport.Load(Server.MapPath("~\Report\SalesReport_DtlBottom.rpt"))
            sWhere = " where mst.trnorderdate between '" & toDate(range1.Text) & "' and '" & toDate(range2.Text) & "' and mst.trnordertype in ('GROSIR','PROJECK') " & IIf(trnjualmstoid.Text <> "", " and mst.ordermstoid = " & trnjualmstoid.Text, " ") & IIf(typeM.SelectedValue = "ALL GROUP", " ", " and mat.itemgroupoid = " & typeM.SelectedValue) & IIf(mattype.SelectedValue = "ALL SUB GROUP", " ", " and mat.itemsubgroupoid = " & mattype.SelectedValue) & IIf(itemoid.Text.Trim = "", " ", " and mat.itemoid = '" & itemoid.Text & "'") & IIf(custoid.Text.Trim = "", " ", " and mst.trncustname = '" & custname.Text & "'") & IIf(sales.SelectedValue = "ALL", " ", " and mst.salesoid = " & sales.SelectedValue) & "  and mst.orderno like " & type_so.SelectedValue & "  " & IIf(ToDouble(sooid.Text) > 0, " and mst.orderno = '" & Tchar(sono.Text) & "'", " ") & paytype.SelectedValue
            namaPDF = "Sales_Report_UnderBottomPrice_"
            'End If
            'Label2.Text = sWhere
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            'CrystalReportViewer1.DisplayGroupTree = False

            'vReport.PrintOptions.PaperSize = PaperSize.PaperLegal
            'Dim paperMargin As CrystalDecisions.Shared.PageMargins
            '' 1 centimeter = 567 twips
            '' 1 inch = 1440 twips
            'paperMargin.leftMargin = 1 * 567
            'paperMargin.rightMargin = 1 * 567
            'paperMargin.topMargin = 1 * 567
            'paperMargin.bottomMargin = 1 * 567
            'vReport.PrintOptions.ApplyPageMargins(paperMargin)


            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiod", toDate(range1.Text))
            vReport.SetParameterValue("endperiod", toDate(range2.Text))
            vReport.SetParameterValue("titledef", "Under Bottom Price")
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            'CrystalReportViewer1.ReportSource = vReport
            'Session("showReport") = "True"

            'Response.Buffer = False
            'Response.ClearHeaders()
            'Response.ClearContent()
            'vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
            'vReport.Close() : vReport.Dispose()

            'Response.Redirect("~\ReportForm\rptSalesReportBottom.aspx?awal=true")

            If tipe = "view" Then
                CrystalReportViewer1.DisplayGroupTree = False
                'CRVPenerimaanBarang.SeparatePages = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "excel" Then
                'vReport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                Response.Buffer = False

                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesReportBottom.aspx?awal=true")
            Else
                'vReport.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                Response.Buffer = False

                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesReportBottom.aspx?awal=true")
            End If
        Catch ex As Exception
            'vReport.Close() : vReport.Dispose()
            ' lblmsgerrsave.Text = ex.Message
            Label2.Text = ex.Message
        End Try
    End Sub

    Sub bindpo_spb()
        Dim filtereorder As String = ""
        filtereorder = " "

        Dim ssql As String
        ssql = "SELECT mst.ordermstoid,mst.orderno,convert(char(10),mst.trnorderdate,103) trnorderdate, mst.trncustname , mst.trncustoid from QL_trnordermst mst where mst.trnordertype in ('GROSIR','PROJECK') and mst.flagbottomprice = 'T' and mst.orderno like '%" & Tchar(sono.Text.Trim) & "%' and mst.orderno like " & type_so.SelectedValue & "  and trnorderdate between '" & toDate(range1.Text) & "'  and '" & toDate(range2.Text) & "' order by  ordermstoid desc "
        FillGV(gvListpo, ssql, "QL_mstsupp")
    End Sub

    Protected Sub showPrintExcel(ByVal name As String)
        Dim sWhere As String = ""

        Try
            If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                Label2.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            Label2.Text = "Please check Period value"
        End Try
        Response.Clear()

        Try
            Dim sSql As String = ""

            If ddltype.SelectedValue = "Summary" Then
                Response.AddHeader("content-disposition", "inline;filename=sales_order_summary.xls")
                Response.Charset = ""
                'set the response mime type for excel
                Response.ContentType = "application/vnd.ms-excel"
                sWhere = " where mst.trnorderdate between '" & toDate(range1.Text) & "' and '" & toDate(range2.Text) & "' " & IIf(trnjualmstoid.Text <> "", " and mst.ordermstoid = " & trnjualmstoid.Text, " ") & _
                IIf(custoid.Text.Trim = "", " ", " and mst.trncustname = '" & custname.Text & "'") & _
                IIf(sales.SelectedValue = "ALL", " ", " and mst.salesoid = " & sales.SelectedValue) & "  and mst.orderno like " & type_so.SelectedValue & "  " & IIf(ToDouble(sooid.Text) > 0, " and mst.orderno = '" & Tchar(sono.Text) & "'", " ") & paytype.SelectedValue

                'sSql = "select isnull(p.personname,'') sales , convert(char(10),om.trnorderdate,101) 'Tgl SO/SJ',om.orderno 'No SO/SJ',  mst.trncustname Nama_Customer, mst.trnjualno NoNota, convert(char(10),mst.trnjualdate,101) Tgl_Nota, mst.periodacctg, mst.trnamtjual AmtSalesBruto,  mst.amtdischdr+mst.amtdischdr2+mst.amtdiscdtl TotDiscount,mst.trnamttax 'PPN', mst.trnamtjualnetto AmtSalesNetto, mst.trnjualstatus StatusPenjualan  from QL_trnjualmst mst   left JOIN QL_trnordermst om on om.cmpcode=mst.cmpcode and om.orderno=mst.orderno   left outer join ql_mstcust c on c.custoid = mst.trncustoid  left outer join ql_mstperson p on p.personoid=mst.salesoid  " & sWhere & "   order by   mst.trncustname, mst.trnjualdate,mst.trnjualno "
                sSql = "select isnull(p.personname,'') sales , convert(char(10),mst.trnorderdate,101) 'Tgl SO/SJ',mst.orderno 'No SO/SJ', mst.trncustname Nama_Customer, mst.amtjualnetto AmtSales from QL_trnordermst mst inner join ql_mstcust c on c.custoid = mst.trncustoid inner join ql_mstperson p on p.personoid=mst.salesoid " & sWhere & "  order by mst.trncustname, mst.trnorderdate, mst.orderno "

            Else 'detail
                Response.AddHeader("content-disposition", "inline;filename=sales_order_dtl.xls")
                Response.Charset = ""
                'set the response mime type for excel
                Response.ContentType = "application/vnd.ms-excel"
                sWhere = " where mst.trnorderdate between '" & toDate(range1.Text) & "' and '" & toDate(range2.Text) & "' " & IIf(trnjualmstoid.Text <> "", " and mst.ordermstoid = " & trnjualmstoid.Text, " ") & IIf(typeM.SelectedValue = "ALL GROUP", " ", " and mat.itemgroupoid = " & typeM.SelectedValue) & IIf(mattype.SelectedValue = "ALL SUB GROUP", " ", " and mat.itemsubgroupoid = " & mattype.SelectedValue) & IIf(itemoid.Text.Trim = "", " ", " and mat.itemoid = '" & itemoid.Text & "'") & IIf(custoid.Text.Trim = "", " ", " and mst.trncustname = '" & custname.Text & "' ") & IIf(sales.SelectedValue = "ALL", " ", " and mst.salesoid = " & sales.SelectedValue) & "  and mst.orderno like " & type_so.SelectedValue & "   " & IIf(ToDouble(sooid.Text) > 0, " and mst.orderno = '" & Tchar(sono.Text) & "'", " ") & paytype.SelectedValue

                'sSql = "SELECT isnull(p.personname,'') sales , convert(char(10),om.trnorderdate,101)'Tgl SO/SJ',om.orderno 'No SO/SJ',  mst.trncustname Nama_Customer, mst.trnjualno NoNota,convert(char(10),mst.trnjualdate,101) Tgl_Nota, mst.periodacctg, mst.trnamtjual AmtSalesBruto,  mst.amtdischdr+mst.amtdiscdtl TotDiscount, mst.trnamtjualnetto AmtSalesNetto, mst.trnjualstatus Status, trnjualdtlseq 'No Urut', mat.itemcode kode, mat.itemdesc barang, gen.gencode AS unit, dtl.trnjualdtlqty Qty, dtl.trnjualdtlprice Price,  dtl.amtjualdisc DiscDtl, dtl.amtjualnetto AmtNettoDtl    FROM QL_trnjualdtl dtl  INNER JOIN QL_trnjualmst mst ON mst.cmpcode = dtl.cmpcode AND mst.trnjualmstoid = dtl.trnjualmstoid  INNER JOIN QL_mstitem mat ON mat.cmpcode = dtl.cmpcode AND mat.itemoid = dtl.itemoid  left JOIN QL_trnordermst om on om.cmpcode=mst.cmpcode and om.orderno=mst.orderno left outer JOIN QL_mstcust cust ON cust.custoid = mst.trncustoid  INNER JOIN QL_mstgen gen ON gen.genoid = dtl.trnjualdtlunitoid  left outer join ql_mstperson p on p.personoid=mst.salesoid   " & sWhere & " order by mst.trncustname, mst.trnjualdate,mst.trnjualno,trnjualdtlseq"

                sSql = "SELECT isnull(p.personname,'') sales , convert(char(10),mst.trnorderdate,101)'Tgl SO/SJ',mst.orderno 'No SO/SJ',  mst.trncustname Nama_Customer, mst.amtjualnetto AmtSalesNetto, dtl.trnorderdtlseq 'No Urut', mat.itemcode kode, mat.itemdesc barang, gen.gencode AS unit, dtl.trnorderdtlqty Qty, dtl.trnorderprice Price, dtl.trnamountdtl AmtNettoDtl FROM QL_trnorderdtl dtl  INNER JOIN QL_trnordermst mst ON mst.cmpcode = dtl.cmpcode AND mst.ordermstoid = dtl.trnordermstoid INNER JOIN QL_mstitem mat ON mat.cmpcode = dtl.cmpcode AND mat.itemoid = dtl.itemoid inner JOIN QL_mstcust cust ON cust.custoid = mst.trncustoid  INNER JOIN QL_mstgen gen ON gen.genoid = dtl.trnorderdtlunitoid inner join ql_mstperson p on p.personoid=mst.salesoid " & sWhere & " order by mst.trncustname, mst.trnorderdate, mst.orderno,trnorderdtlseq "
                Dim dtTmp1 As DataTable = CreateDataTableFromSQL(sSql)

                sSql = "select '' 'Tgl SO/SJ', '' 'No SO/SJ', '' 'Sales', '' 'Customer', '' 'Total Jual', '' 'No Urut', '' 'Kode', '' 'Barang', '' 'Satuan', '' 'Harga', '' 'Qty', '' 'Sub Total'"
                Dim dtTampung As DataTable = CreateDataTableFromSQL(sSql)

                If dtTampung.Rows.Count > 0 Then
                    dtTampung.Rows.RemoveAt(0)
                End If

                'Dim nomere As Integer = 1
                For i As Integer = 0 To dtTmp1.Rows.Count - 1
                    Dim dr As DataRow = dtTampung.NewRow
                    ' <<- data header
                    If i = 0 Then
                        dr("Tgl SO/SJ") = dtTmp1.Rows(i)("Tgl SO/SJ")
                        dr("No SO/SJ") = dtTmp1.Rows(i)("No SO/SJ")
                        dr("Sales") = dtTmp1.Rows(i)("sales")
                        dr("Customer") = dtTmp1.Rows(i)("Nama_Customer")
                        dr("Total Jual") = dtTmp1.Rows(i)("AmtSalesNetto")
                    ElseIf dtTmp1.Rows(i)("No SO/SJ") = dtTmp1.Rows(i - 1)("No SO/SJ") Then
                        dr("Tgl SO/SJ") = ""
                        dr("No SO/SJ") = ""
                        dr("Sales") = ""
                        dr("Customer") = ""
                        dr("Total Jual") = ""
                    Else
                        dr("Tgl SO/SJ") = dtTmp1.Rows(i)("Tgl SO/SJ")
                        dr("No SO/SJ") = dtTmp1.Rows(i)("No SO/SJ")
                        dr("Sales") = dtTmp1.Rows(i)("sales")
                        dr("Customer") = dtTmp1.Rows(i)("Nama_Customer")
                        dr("Total Jual") = dtTmp1.Rows(i)("AmtSalesNetto")
                    End If
                    ' data header ->>

                    ' <<- data detail
                    dr("No Urut") = dtTmp1.Rows(i)("No Urut")
                    dr("Kode") = dtTmp1.Rows(i)("kode")
                    dr("Barang") = dtTmp1.Rows(i)("barang")
                    dr("Satuan") = dtTmp1.Rows(i)("unit")
                    dr("Harga") = dtTmp1.Rows(i)("Price")
                    dr("Qty") = dtTmp1.Rows(i)("Qty")
                    dr("Sub Total") = dtTmp1.Rows(i)("AmtNettoDtl")
                    ' data detail ->>

                    dtTampung.Rows.Add(dr)
                Next
                Response.Write(ConvertDtToTDF(dtTampung))
                Response.End()
                Exit Sub
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
            da.SelectCommand = xcmd
            da.Fill(ds)
            Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
            dt = ds.Tables(0)
            Response.Write(ConvertDtToTDF(dt))
            conn.Close()

            Response.End()

        Catch ex As Exception
            Label2.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            'Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Session.Clear()  ' -->>  clear all session 
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            '            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("rptSalesReportBottom.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If
        Page.Title = CompnyName & " - Sales Report"
        'If Session("showReport") = "True" Then
        '    showPrint("view")
        'End If
        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            ddltype_SelectedIndexChanged(sender, e)
            range1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy") : range2.Text = Format(Now, "dd/MM/yyyy")
            FillDDL(typeM, "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' order by gendesc")
            typeM.Items.Add("ALL GROUP")
            typeM.SelectedValue = "ALL GROUP"
            FillDDL(mattype, "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' order by gendesc")
            mattype.Items.Add("ALL SUB GROUP")
            mattype.SelectedValue = "ALL SUB GROUP"

            FillDDL(sales, "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personpost in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC = 'SALES PERSON')  and STATUS='AKTIF' ")
            sales.Items.Add("ALL")
            sales.SelectedIndex = sales.Items.Count - 1
        End If
        If Session("showReport") = "True" Then
            showPrint("view")
        End If
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        bindNotaJual()
        gvListCust.DataBind()
        panelDso.Visible = True
        btnHideDSO.Visible = True
        ModalPopupExtenderdso.Show()
    End Sub

    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("pdf")
        'Session("showReport") = "True"
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Session("showReport") = "False" : vReport.Close() : Label2.Text = ""
        Response.Redirect("rptSalesReportBottom.aspx?awal=true")
    End Sub

    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        'showPrintExcel("ListofSalesReport")
        showPrint("excel")
        'Session("showReport") = True
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trnjualmstoid.Text = ""
        invoiceno.Text = ""
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ImageButton3_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindNotaJual()
        panelDso.Visible = True
        btnHideDSO.Visible = True
        ModalPopupExtenderdso.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        invoiceno.Text = gvListCust.SelectedDataKey(1).ToString
        trnjualmstoid.Text = gvListCust.SelectedDataKey(0).ToString
        panelDso.Visible = False
        btnHideDSO.Visible = False
        ModalPopupExtenderdso.Hide()
    End Sub

    Public Sub BindDataListItem()
        Dim ssql As String = ""
        ssql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc ,ql_mstitem.merk ,ql_mstitem.itempriceunit1, ql_mstitem.itemoid, satuan1," & _
 "g.gendesc unit , satuan2, konversi1_2, g2.gendesc  unit2  from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
 "and itemflag='AKTIF'  inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
               "where (ql_mstitem.itemdesc like '%" & Tchar(itemdesc.Text) & "%' OR ql_mstitem.itemcode LIKE '%" & Tchar(itemdesc.Text.Trim) & "%' OR ql_mstitem.merk LIKE '%" & Tchar(itemdesc.Text.Trim) & "%') " & IIf(typeM.SelectedValue = "ALL GROUP", " ", " and ql_mstitem.itemgroupoid = " & typeM.SelectedValue) & IIf(mattype.SelectedValue = "ALL SUB GROUP", " ", " and ql_mstitem.itemsubgroupoid = " & mattype.SelectedValue)

        FillGV(gvListForecast, ssql, "ql_mstitem")
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
        gvListForecast.Visible = True
        'PanelForecast.Visible = True
        'btnHideForecast.Visible = True
        'ModalPopupExtenderForecast.Show()
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemdesc.Text = ""
        itemoid.Text = ""

        gvListForecast.Visible = False
        CProc.DisposeGridView(gvListForecast)
    End Sub

    Protected Sub gvListForecast_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        itemdesc.Text = gvListForecast.SelectedDataKey.Item(1)
        itemoid.Text = gvListForecast.SelectedDataKey.Item("itemoid")
        gvListForecast.Visible = False
        'btnHideForecast.Visible = False
        'PanelForecast.Visible = False
        'ModalPopupExtenderForecast.Hide()
        CProc.DisposeGridView(gvListForecast)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

    Protected Sub btnCloseForecast_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHideForecast.Visible = False
        PanelForecast.Visible = False
        ModalPopupExtenderForecast.Hide()
        CProc.DisposeGridView(gvListForecast)
    End Sub

    Sub FilterGVCust(ByVal filterName As String, ByVal filterCode As String, ByVal cmpcode As String)
        Dim ssql As String = ""
        ssql = "SELECT c.custoid as Id,c.custcode as code,c.custname as Name,c.custaddr as Address from QL_mstcust c WHERE c.custflag='Active' AND (c.custname LIKE '%" & filterName & "%' OR c.custcode LIKE '%" & filterCode & "%') and c.custgroup='Grosir' ORDER BY c.custoid DESC "

        FillGV(gvCustomer, ssql, "QL_mstcust")
        'With SDSCust
        '    .SelectParameters("Name").DefaultValue = "%" & filterName & "%"
        '    .SelectParameters("code").DefaultValue = "%" & filterCode & "%"
        '    .SelectParameters("CmpCode").DefaultValue = CompnyCode
        'End With
        'gvCustomer.DataBind()
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custname.Text = "" : custoid.Text = ""
        gvCustomer.Visible = False
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCustomer.SelectedDataKey.Item(1)
        custoid.Text = gvCustomer.SelectedDataKey.Item(0)
        gvCustomer.Visible = False
        'CProc.SetModalPopUpExtender(btnhiddenCust, Panel1, ModalPopupExtender1, False)
        CProc.DisposeGridView(gvCustomer)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub

    Protected Sub LBCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CProc.SetModalPopUpExtender(btnhiddenCust, Panel1, ModalPopupExtender1, False)
        CProc.DisposeGridView(gvCustomer)
    End Sub

    Protected Sub ibtnCustID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If DDLCustID.SelectedValue = "Name" Then
            FilterGVCust(txtFindCustID.Text, "", "")
        Else
            FilterGVCust("", txtFindCustID.Text, "")
        End If
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub imbAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterGVCust("", "", CompnyCode)
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub ddltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltype.SelectedIndexChanged
        If ddltype.SelectedValue = "Summary" Then
            typeM.Visible = False
            mattype.Visible = False
            lbltype.Visible = False
            lbltypeM.Visible = False
            Label3.Visible = False
            itemdesc.Visible = False
            btnSearchItem.Visible = False
            btnEraseItem.Visible = False
        Else
            typeM.Visible = True
            mattype.Visible = True
            lbltype.Visible = True
            lbltypeM.Visible = True
            Label3.Visible = True
            itemdesc.Visible = True
            btnSearchItem.Visible = True
            btnEraseItem.Visible = True
        End If
    End Sub

    Protected Sub closeDSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles closeDSO.Click
        panelDso.Visible = False
        btnHideDSO.Visible = False
        ModalPopupExtenderdso.Hide()
    End Sub

    Protected Sub ImageButtonpo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cancelpo.Click
        sooid.Text = ""
        sono.Text = ""

        custname.CssClass = "inpText"
        custname.Enabled = True
        btnSearch.Enabled = True
        imbClearCust.Enabled = True

        gvListpo.Visible = False

    End Sub

    Protected Sub closeDSOpo_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles closeDSOpo.Click
        panelDsopo.Visible = False
        btnHideDSOpo.Visible = False
        ModalPopupExtenderdsopo.Hide()
    End Sub

    Protected Sub gvListpo_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListpo.SelectedIndexChanged
        sono.Text = gvListpo.SelectedDataKey(1).ToString
        sooid.Text = gvListpo.SelectedDataKey(0).ToString
        custoid.Text = gvListpo.SelectedDataKey("trncustoid")
        custname.Text = gvListpo.SelectedDataKey("trncustname")
        custname.CssClass = "inpTextDisabled"
        custname.Enabled = False
        btnSearch.Enabled = False
        imbClearCust.Enabled = False
        gvListpo.Visible = False
        'panelDsopo.Visible = False
        'btnHideDSOpo.Visible = False
        'ModalPopupExtenderdsopo.Hide()
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Protected Sub gvListpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListpo.PageIndexChanging
        gvListpo.PageIndex = e.NewPageIndex
        bindpo_spb()

        gvListpo.Visible = True
        'panelDsopo.Visible = True
        'btnHideDSOpo.Visible = True
        'ModalPopupExtenderdsopo.Show()
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        gvCustomer.PageIndex = e.NewPageIndex
        FilterGVCust((custname.Text.Trim), (custname.Text.Trim), CompnyCode)
        gvCustomer.Visible = True
        'CProc.SetModalPopUpExtender(btnhiddenCust, Panel1, ModalPopupExtender1, True)
    End Sub

    Protected Sub gvListForecast_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListForecast.PageIndexChanging
        gvListForecast.PageIndex = e.NewPageIndex
        BindDataListItem()
        gvListForecast.Visible = True
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("view")
        Session("showReport") = "True"
    End Sub

    Protected Sub btnSearch_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        FilterGVCust((custname.Text.Trim), (custname.Text.Trim), CompnyCode)
        gvCustomer.Visible = True
    End Sub

    Protected Sub posearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles posearch.Click
        Label2.Text = ""
        Try
            If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                Label2.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            Label2.Text = "Please check Period value"
            Exit Sub
        End Try

        bindpo_spb()

        gvListpo.Visible = True

    End Sub

End Class
