<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="Master_Customer.aspx.vb" Inherits="Master_Master_Customer" Title="MSC- Master Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
                <table width="100%">
                    <tr>
                        <td colspan="3" style="background-color: silver" align="left" char="header">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large" ForeColor="Navy" Text=".: Data Customer"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3" style="background-color: transparent">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong><span style="color: background">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                List of Customer</span></strong> <strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD><asp:Label id="Label32" runat="server" Text="Cabang" __designer:wfdid="w19"></asp:Label></TD><TD><asp:Label id="Label33" runat="server" Text=":" __designer:wfdid="w20"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="CabangFilter" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w21"></asp:DropDownList></TD></TR><TR><TD><asp:Label id="Label2" runat="server" Text="Filter" __designer:wfdid="w22"></asp:Label></TD><TD><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w23"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="ddlFilter" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w24">
                                                        <asp:ListItem Value="custcode">Code</asp:ListItem>
                                                        <asp:ListItem Value="custname">Nama</asp:ListItem>
                                                        <asp:ListItem Value="custaddr">Alamat</asp:ListItem>
                                                        <asp:ListItem Value="cbg.gendesc">Cabang</asp:ListItem>
                                                        <asp:ListItem Value="phone1" Enabled="False">Telepon</asp:ListItem>
                                                        <asp:ListItem Value="custcreditlimitrupiah" Enabled="False">Credit Limit</asp:ListItem>
                                                        <asp:ListItem Value="custcreditlimitusagerupiah" Enabled="False">Credit Usage</asp:ListItem>
                                                    </asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="166px" CssClass="inpText" __designer:wfdid="w25"></asp:TextBox>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton> </TD></TR><TR><TD><asp:CheckBox id="cbOverDue" runat="server" Text="Over Due" __designer:wfdid="w1"></asp:CheckBox></TD><TD>:</TD><TD colSpan=5><asp:DropDownList id="ddlOverDue" runat="server" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem>&gt; 0</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 78px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w29"></asp:CheckBox></TD><TD><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w30"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="DDLStatus" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w31">
                                                        <asp:ListItem>Active</asp:ListItem>
                                                        <asp:ListItem>Inactive</asp:ListItem>
                                                        <asp:ListItem>Suspended</asp:ListItem>
                                                    </asp:DropDownList></TD></TR><TR><TD colSpan=65><asp:GridView style="max-width: 950px" id="GVmstcust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w32" OnRowDataBound="GVmstcust_RowDataBound" EnableModelValidation="True" AllowPaging="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custoid" DataNavigateUrlFormatString="~\master\Master_Customer.aspx?idPage={0}" DataTextField="custcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="False" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="140px"></HeaderStyle>

<ItemStyle Wrap="False" Width="160px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="400px"></HeaderStyle>

<ItemStyle Width="400px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="city" HeaderText="Kota" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone" HeaderText="Telepon">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditlimit" HeaderText="Credit Limit">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditusage" HeaderText="Credit Usage">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custcreditlimitusagaeoverdue" HeaderText="Over Due">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroup" HeaderText="Group" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="termin" HeaderText="TOP">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custflag" HeaderText="Status">
<HeaderStyle Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notes" HeaderText="Catatan" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Data Not Found" BorderColor="Blue"></asp:Label>
                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>
</ContentTemplate>

                                <Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong><span style="color: background">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle" />
                                Form Customer</span></strong> <strong>:.</strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=6><asp:Label id="Label8" runat="server" Font-Bold="True" Text="Informasi" __designer:wfdid="w445"></asp:Label> <asp:Label id="I_U" runat="server" ForeColor="Red" Text="NEW" __designer:wfdid="w560" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Code <asp:Label id="Label5" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w446"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custcode" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w447" Enabled="False" MaxLength="10"></asp:TextBox> <asp:Label id="KodeOutlate" runat="server" __designer:wfdid="w448" Visible="False"></asp:Label> <asp:Label id="lblAdd" runat="server" __designer:wfdid="w449" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px; WHITE-SPACE: nowrap"><asp:Label id="Label35" runat="server" Width="104px" Text="Customer Type" __designer:wfdid="w450"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlcustgroup" runat="server" Width="205px" CssClass="inpText" __designer:wfdid="w451" AutoPostBack="True"><asp:ListItem>GROSIR</asp:ListItem>
<asp:ListItem>PROJECT</asp:ListItem>
<asp:ListItem>INTERNAL</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:Label id="CustOid" runat="server" __designer:wfdid="w559" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px; WHITE-SPACE: nowrap" id="TD87" runat="server" Visible="false">Customer Group</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD88" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD86" runat="server" Visible="false"><asp:TextBox id="CustNameGroup" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w452"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindCG" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w453"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCG" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w454"></asp:ImageButton> <asp:Label id="custgroupoid" runat="server" __designer:wfdid="w455" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD colSpan=1></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px; WHITE-SPACE: nowrap"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"><asp:GridView id="gvCustGroup" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w456" Visible="False" EmptyDataText="No data in database." DataKeyNames="custgroupoid,custgroupname,LimitNya,UsageNya" PageSize="8" AllowPaging="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custgroupoid" HeaderText="CustGroupOid" ReadOnly="True" Visible="False">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupname" HeaderText="Group Name">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="LimitNya" HeaderText="Credit Limit">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="UsageNya" HeaderText="Cr. Usage">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                    <asp:Label ID="lblstatusdataCust" runat="server" Text="No Customer Data !" CssClass="Important"></asp:Label>
                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama <asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w457"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="precustname" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w458"></asp:DropDownList> <asp:TextBox id="custname" runat="server" Width="221px" CssClass="inpText" __designer:wfdid="w459" MaxLength="100" AutoPostBack="True"></asp:TextBox><BR /><asp:Label id="Label12" runat="server" ForeColor="Red" Text="min. 1 karakter" __designer:wfdid="w460"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Tgl Gabung</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="TglGabung" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w555"></asp:TextBox>&nbsp;<asp:ImageButton id="ibTglGabung" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w556"></asp:ImageButton>&nbsp;<asp:Label id="Label10" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w557"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Cabang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="branchID" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w463" AutoPostBack="True" OnSelectedIndexChanged="branchID_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp; </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Provinsi<asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w461"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custprovoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w462" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Alamat <asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w466"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custaddr" runat="server" Width="296px" CssClass="inpText" __designer:wfdid="w467" MaxLength="100" AutoPostBack="True" OnTextChanged="custaddr_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Kota <asp:Label id="Label26" runat="server" CssClass="Important" Text="*" __designer:wfdid="w554"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custcityoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w465" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama Pemilik&nbsp;<asp:Label id="Label27" runat="server" CssClass="Important" Text="*" __designer:wfdid="w470"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlconttitle" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w471"></asp:DropDownList> <asp:TextBox id="contactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w472" MaxLength="125" AutoPostBack="True" OnTextChanged="contactperson1_TextChanged"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px"><asp:Label id="Label25" runat="server" Text="TOP" __designer:wfdid="w468"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="dd_timeofpayment" runat="server" Width="80px" CssClass="inpTextDisabled" __designer:wfdid="w469"></asp:DropDownList>Hari</TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD72" Visible="true">E-mail</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD70" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD68" colSpan=1 Visible="true"><asp:TextBox id="custemail" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w474" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custflag" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w473" Enabled="False"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Inactive</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD73" Visible="true">Telepon&nbsp;Rumah <asp:Label id="Label24" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w476"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD71" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD69" colSpan=1 Visible="true"><asp:TextBox id="phone1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w477" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Fax</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custfax1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w475" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Telepon Kantor <asp:Label id="Label30" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w479"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phone2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w480" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">Website</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custwebsite" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w478" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Telepon HP&nbsp;/ WA <asp:Label id="Label31" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w482"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phone3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w483" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px">NPWP</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custnpwp" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w481" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Catatan<BR /></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="notes" runat="server" Width="250px" Height="45px" CssClass="inpText" __designer:wfdid="w484" MaxLength="500" AutoPostBack="True" OnTextChanged="notes_TextChanged" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="maks. 100 karakter" __designer:wfdid="w485"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD67" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD66" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD65" Visible="false"></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD80" runat="server" Visible="false">Last Sales Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD85" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD84" colSpan=1 runat="server" Visible="false"><asp:TextBox id="lastsalesdate" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w486"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD82" runat="server" Visible="false">Pin BB</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD81" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD83" colSpan=1 runat="server" Visible="false"><asp:TextBox id="pin_bb" runat="server" CssClass="inpText" __designer:wfdid="w487" MaxLength="12"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD58" runat="server" Visible="false"><asp:Label id="Label18" runat="server" Width="125px" Text="Nama untuk Paspor" __designer:wfdid="w488"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD57" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD56" runat="server" Visible="false"><asp:TextBox id="namapaspor" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w489" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD64" Visible="false">Credit Limit Rupiah</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD62" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD61" colSpan=1 Visible="false"><asp:TextBox id="custcreditlimitrupiah" runat="server" Width="104px" CssClass="inpTextDisabled" __designer:wfdid="w490" Enabled="False" MaxLength="10"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD50" runat="server" Visible="false"><asp:Label id="Label19" runat="server" Text="Nomor Paspor" __designer:wfdid="w491"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD53" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD52" runat="server" Visible="false"><asp:TextBox id="nopaspor" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w492" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD60" Visible="false">Credit Limit Usage Rupiah</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD59" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD63" colSpan=1 Visible="false"><asp:TextBox id="custcreditlimitusagerupiah" runat="server" Width="104px" CssClass="inpTextDisabled" __designer:wfdid="w493" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD51" runat="server" Visible="false"><asp:Label id="Label20" runat="server" Text="Tanggal Lahir" __designer:wfdid="w494"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD55" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD54" runat="server" Visible="false"><asp:TextBox id="tgllahir" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w495"></asp:TextBox>&nbsp;<asp:ImageButton id="ibtgllahir" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w496"></asp:ImageButton> <asp:Label id="Label11" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w497"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label34" runat="server" Width="177px" Text="Credit Limit Usage Over Due" __designer:wfdid="w498"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitusageoverdue" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w499" Enabled="False" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:Button id="btnInfoNota" runat="server" CssClass="btn green" Text="Info Nota" __designer:wfdid="w500" Visible="False"></asp:Button></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px; WHITE-SPACE: nowrap" id="TD78" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD76" runat="server" Visible="false">:</TD><TD style="VERTICAL-ALIGN: top" id="TD74" runat="server" Visible="false"><asp:DropDownList id="ddlconttitle2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w501"></asp:DropDownList><asp:TextBox id="contactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w502" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama Bank dan Rekening</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="bankrekening" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w503" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD75" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD79" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD77" runat="server" Visible="false"><asp:DropDownList id="ddlconttitle3" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w504"></asp:DropDownList><asp:TextBox id="contactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w505" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label21" runat="server" Text="Retur Account" __designer:wfdid="w506"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="returaccount" runat="server" Width="304px" CssClass="inpText" __designer:wfdid="w507"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap"><asp:Label id="Label22" runat="server" Text="A/R Account" __designer:wfdid="w508"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="araccount" runat="server" Width="304px" CssClass="inpText" __designer:wfdid="w509"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><asp:Label id="lblLast" runat="server" Text="Last Update On" __designer:wfdid="w510"></asp:Label><asp:Label id="lblCreate" runat="server" Text="Created On" __designer:wfdid="w511"></asp:Label> <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w512"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w513"></asp:Label> </TD></TR><TR><TD colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w514"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w515"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w516" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w517" AssociatedUpdatePanelID="UpdatePanel3"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w518"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><asp:DropDownList id="spgOid" runat="server" Width="112px" CssClass="inpText" __designer:wfdid="w519" Visible="False"></asp:DropDownList><asp:TextBox id="phonecontactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w520" Visible="False" MaxLength="50"></asp:TextBox><asp:TextBox id="phonecontactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w521" Visible="False" MaxLength="25"></asp:TextBox><asp:TextBox id="phonecontactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w522" Visible="False" MaxLength="25"></asp:TextBox><asp:TextBox id="txtage" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w523" Visible="False" Wrap="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=6><ajaxToolkit:CalendarExtender id="CETgllahir" runat="server" __designer:wfdid="w561" TargetControlID="tgllahir" Format="dd/MM/yyyy" PopupButtonID="ibtgllahir"></ajaxToolkit:CalendarExtender><ajaxToolkit:FilteredTextBoxExtender id="fte1" runat="server" __designer:wfdid="w525" TargetControlID="phone1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte2" runat="server" __designer:wfdid="w526" TargetControlID="phonecontactperson2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" __designer:wfdid="w527" TargetControlID="phonecontactperson3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte6" runat="server" __designer:wfdid="w528" TargetControlID="phone2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte8" runat="server" __designer:wfdid="w529" TargetControlID="phonecontactperson1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte9" runat="server" __designer:wfdid="w530" TargetControlID="phone3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:MaskedEditExtender id="MEEnpwp" runat="server" __designer:wfdid="w524" TargetControlID="custnpwp" AcceptNegative="Right" Mask="99.999.999.9-999.999" MaskType="Number"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CETglGabung" runat="server" __designer:wfdid="w562" TargetControlID="TglGabung" Format="dd/MM/yyyy" PopupButtonID="ibTglGabung"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEETgllahir" runat="server" __designer:wfdid="w536" TargetControlID="tgllahir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="pcountry" runat="server" __designer:wfdid="w533" TargetControlID="cpcountryphonecode" ValidChars="0123456789+"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fCustFax2" runat="server" __designer:wfdid="w532" TargetControlID="CustFax2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="MEETglGabung" runat="server" __designer:wfdid="w563" TargetControlID="TglGabung" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteCustFax1" runat="server" __designer:wfdid="w531" TargetControlID="custfax1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEcustpostcode" runat="server" __designer:wfdid="w534" TargetControlID="custpostcode" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender><%-- <ajaxToolkit:MaskedEditExtender ID="MEECreditLimitRupiah" runat="server" TargetControlID="custcreditlimitrupiah" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender>
                                                                    <ajaxToolkit:MaskedEditExtender ID="MEECreditLimitUsageRupiah" runat="server" TargetControlID="custcreditlimitusagerupiah" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender>--%> <ajaxToolkit:FilteredTextBoxExtender id="FTECrLimitRupiah" runat="server" __designer:wfdid="w537" TargetControlID="custcreditlimitrupiah" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD40" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD41" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD35" runat="server" Visible="false"><asp:DropDownList id="custpaytermdefaultoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w540" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD20" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD39" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD14" runat="server" Visible="false"><asp:DropDownList id="custdefaultcurroid" runat="server" CssClass="inpText" __designer:wfdid="w541" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD5" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD34" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD26" runat="server" Visible="false"><asp:DropDownList id="custacctgoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w542" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD32" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD44" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD7" runat="server" Visible="false"><asp:TextBox id="custcreditlimit" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w543" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD10" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD47" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD19" runat="server" Visible="false"><asp:DropDownList id="custcreditlimitcurroid" runat="server" CssClass="inpText" __designer:wfdid="w544" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD48" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD43" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD22" rowSpan=1 runat="server" Visible="false"><asp:TextBox id="custcreditlimitusage" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w545" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD38" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD29" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD2" runat="server" Visible="false"><asp:DropDownList id="custcountryoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w546" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD33" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD25" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD37" rowSpan=1 runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Def. Limit Currency" __designer:wfdid="w547" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD27" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD8" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD4" runat="server" Visible="false"><asp:TextBox id="CustGroup" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w548" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD18" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD12" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD49" rowSpan=1 runat="server" Visible="false"><asp:DropDownList id="statusar" runat="server" CssClass="inpText" __designer:wfdid="w549" Visible="False"></asp:DropDownList> <asp:Label id="lbl_lasttrans" runat="server" __designer:wfdid="w550"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD23" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD13" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD15" runat="server" Visible="false"><asp:TextBox id="cpcountryphonecode" runat="server" Width="31px" CssClass="inpTextDisabled" __designer:wfdid="w551" Visible="False" Enabled="False" MaxLength="6" AutoPostBack="True"></asp:TextBox><asp:TextBox id="custpostcode" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w552" Visible="False" MaxLength="10"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 115px" id="TD42" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD21" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD28" runat="server" Visible="false"><asp:TextBox id="CustFax2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w553" Visible="False" MaxLength="25"></asp:TextBox></TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
                    </tr>
                </table>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False" EnableTheming="True">
                <table width="400">
                    <tbody>
                        <tr>
                            <td style="HEIGHT: 25px; BACKGROUND-COLOR: red" align="left" colspan="3">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="WIDTH: 47px" align="left">
                                <asp:Image ID="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td align="left" colspan="2">
                                <asp:Label ID="validasi" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="WIDTH: 47px"></td>
                            <td colspan="2">
                                <asp:Label ID="lblstate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 25px" align="center" colspan="3">
                                <asp:ImageButton ID="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MPEError" runat="server" TargetControlID="btnExtender" DropShadow="True" Drag="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblValidasi" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnExtender" runat="server" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upListMat" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">Catatan Transaksi Nota</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListNya" runat="server" Width="99%" CssClass="MyTabStyle" ForeColor="#333333" DataKeyNames="trnjualno,trnjualdate,payduedate,trnamtjualnetto" PageSize="5" AllowPaging="True" GridLines="None" AutoGenerateColumns="False" CellPadding="4" OnPageIndexChanging="gvListNya_PageIndexChanging" OnRowDataBound="gvListNya_RowDataBound" EnableModelValidation="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnjualno" HeaderText="No. Transasksi">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payduedate" HeaderText="Tgl Jatuh Tempo">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamtjualnetto" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3>&nbsp;&nbsp; <asp:LinkButton id="lbCloseListMat" onclick="lbCloseListMat_Click" runat="server">[ Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="lbCloseListMat"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdPanelPrint" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 12px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" align=left></TD><TD style="HEIGHT: 20px" align=left></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" TargetControlID="btnHidePrint" Drag="True" PopupControlID="pnlPrint" PopupDragHandleControlID="lblPrint" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
                <asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" SelectCommand="SELECT cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, custfax1, phone3, custfax2, custemail, custwebsite, upduser, updtime, custnpwp,pin_bb FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" InsertCommand="INSERT INTO QL_mstcust(cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, upduser, updtime, phonecontactperson3, phonecontactperson2, phonecontactperson1, contactperson1, contactperson2, contactperson3, custnpwp,pin_bb) VALUES (@cmpcode, @custoid, @custcode, @custname, @custflag, @custaddr, @custcityoid, @custprovoid, @custcountryoid, @custpostcode, @phone1, @phone2, @phone3, @custfax1, @custfax2, @custemail, @custwebsite, @upduser, @updtime, @phonecontactperson3, @phonecontactperson2, @phonecontactperson1, @contactperson1, @contactperson2, @contactperson3, @custnpwp,@pin_bb)" UpdateCommand="UPDATE QL_mstcust SET custcode = @custcode, custname = @custname, custflag = @custflag, custaddr = @custaddr, custcityoid = @custcityoid, custprovoid = @custprovoid, custcountryoid = @custcountryoid, custpostcode = @custpostcode, phone1 = @phone1, phone2 = @phone2, phone3 = @phone3, custfax1 = @custfax1, custfax2 = @custfax2, custemail = @custemail, custwebsite = @custwebsite, upduser = @upduser, updtime = @updtime, custnpwp = @custnpwp, contactperson1 = @contactperson1, contactperson2 = @contactperson2, contactperson3 = @contactperson3, phonecontactperson1 = @phonecontactperson1, phonecontactperson2 = @phonecontactperson2, phonecontactperson3 = @phonecontactperson3,pin_bb=@pin_bb WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
                    <SelectParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="custcode" />
                        <asp:Parameter Name="custname" />
                        <asp:Parameter Name="custflag" />
                        <asp:Parameter Name="custaddr" />
                        <asp:Parameter Name="custcityoid" />
                        <asp:Parameter Name="custprovoid" />
                        <asp:Parameter Name="custcountryoid" />
                        <asp:Parameter Name="custpostcode" />
                        <asp:Parameter Name="phone1" />
                        <asp:Parameter Name="phone2" />
                        <asp:Parameter Name="phone3" />
                        <asp:Parameter Name="custfax1" />
                        <asp:Parameter Name="custfax2" />
                        <asp:Parameter Name="custemail" />
                        <asp:Parameter Name="custwebsite" />
                        <asp:Parameter Name="upduser" />
                        <asp:Parameter Name="updtime" />
                        <asp:Parameter Name="custnpwp" />
                        <asp:Parameter Name="contactperson1" />
                        <asp:Parameter Name="contactperson2" />
                        <asp:Parameter Name="contactperson3" />
                        <asp:Parameter Name="phonecontactperson1" />
                        <asp:Parameter Name="phonecontactperson2" />
                        <asp:Parameter Name="phonecontactperson3" />
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                        <asp:Parameter Name="pin_bb" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                        <asp:Parameter Name="custcode" />
                        <asp:Parameter Name="custname" />
                        <asp:Parameter Name="custflag" />
                        <asp:Parameter Name="custaddr" />
                        <asp:Parameter Name="custcityoid" />
                        <asp:Parameter Name="custprovoid" />
                        <asp:Parameter Name="custcountryoid" />
                        <asp:Parameter Name="custpostcode" />
                        <asp:Parameter Name="phone1" />
                        <asp:Parameter Name="phone2" />
                        <asp:Parameter Name="phone3" />
                        <asp:Parameter Name="custfax1" />
                        <asp:Parameter Name="custfax2" />
                        <asp:Parameter Name="custemail" />
                        <asp:Parameter Name="custwebsite" />
                        <asp:Parameter Name="upduser" />
                        <asp:Parameter Name="updtime" />
                        <asp:Parameter Name="phonecontactperson3" />
                        <asp:Parameter Name="phonecontactperson2" />
                        <asp:Parameter Name="phonecontactperson1" />
                        <asp:Parameter Name="contactperson1" />
                        <asp:Parameter Name="contactperson2" />
                        <asp:Parameter Name="contactperson3" />
                        <asp:Parameter Name="custnpwp" />
                        <asp:Parameter Name="pin_bb" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT cmpcode, custoid, custcode, custname, custaddr, custcityoid, custprovoid, custflag, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, upduser, updtime FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
                    <SelectParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSOID" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, tablename, lastoid, tablegroup FROM QL_mstoid WHERE (tablename = @tablename)" UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="QL_mstcust" Name="tablename" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="lastoid" />
                        <asp:Parameter Name="tablename" />
                        <asp:Parameter Name="cmpcode" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSBank" runat="server"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCurrency" runat="server"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCity" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="City" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSProvince" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Province" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCountry" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Country" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSContactPerson" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" InsertCommand="INSERT INTO QL_CONTACTPERSON(CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDTIME, UPDUSER) VALUES (@CMPCODE, @CPOID, @OID, @TYPE, @CPNAME, @CPPHONE, @CPEMAIL, @UPDTIME, @UPDUSER)" SelectCommand="SELECT CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDUSER, UPDTIME FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" UpdateCommand="UPDATE QL_CONTACTPERSON SET OID = @OID, TYPE = @TYPE, CPNAME = @CPNAME, CPPHONE = @CPPHONE, CPEMAIL = @CPEMAIL, UPDUSER = @UPDUSER, UPDTIME = @UPDTIME WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)">
                    <SelectParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="OID" />
                        <asp:Parameter Name="TYPE" />
                        <asp:Parameter Name="CPNAME" />
                        <asp:Parameter Name="CPPHONE" />
                        <asp:Parameter Name="CPEMAIL" />
                        <asp:Parameter Name="UPDUSER" />
                        <asp:Parameter Name="UPDTIME" />
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                        <asp:Parameter Name="OID" />
                        <asp:Parameter Name="TYPE" />
                        <asp:Parameter Name="CPNAME" />
                        <asp:Parameter Name="CPPHONE" />
                        <asp:Parameter Name="CPEMAIL" />
                        <asp:Parameter Name="UPDTIME" />
                        <asp:Parameter Name="UPDUSER" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSShipping" runat="server"></asp:SqlDataSource>
</asp:Content>
