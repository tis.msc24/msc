'Prgmr:Bhie Nduuutt | LastUpdt:16.05.2013
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunctionAccounting
Partial Class Accounting_trnConAP
    Inherits System.Web.UI.Page
#Region "Variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public compnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cfunction As New ClassFunction
#End Region

#Region "Prosedure"

    Private Sub ReAmount()
        dpp.Text = ToMaskEdit(ToDouble(dpp.Text), 4)
        amttax.Text = ToMaskEdit(ToDouble(dpp.Text) * (ToDouble(trntaxpct.Text) / 100), 4)
        amtbeli.Text = ToMaskEdit(ToDouble(dpp.Text) + ToDouble(amttax.Text), 4)
        amtbayar.Text = ToMaskEdit(ToDouble(amtbayar.Text), 4)
        totalAP.Text = ToMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtbayar.Text), 4)
    End Sub

    Private Sub initDDL()
        Dim sSql As String
        If Session("branch_id") = "01" Then
            dd_branch.Enabled = True
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and  gencode = '" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
        End If
        ' Init Payment Term
        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE gengroup LIKE 'PAYTYPE' AND gendesc NOT LIKE 'Cash%' order by gendesc"
        FillDDL(trnpaytype, sSql)
        ' Init Currency
        sSql = "select currencyoid, currencycode  + '-' + currencydesc from QL_mstcurr where cmpcode='" & cmpcode & "'"
        FillDDL(curroid, sSql)
        FillRate(curroid.SelectedValue)
    End Sub

    Private Sub FillRate(ByVal iCurrOid As Integer)

        currate.Text = ToMaskEdit(ToDouble(cKon.ambilscalar("select top 1 rate2idrvalue from QL_mstrate2 where currencyoid=" & curroid.SelectedValue & " order by rate2date desc ")), 4)

    End Sub

    Public Sub binddata(ByVal filterCompCode As String, ByVal filterName As String, ByVal filterNo As String)
        Dim sqlSelect As String
        Dim branch_code As String = ""
   
        sqlSelect = "SELECT trnbelimstoid, trnbelino, trnbelidate, trnsuppoid, suppname, payduedate, trnbelistatus,amttrans,sum(AMTBAYAR)amtbayar,trnbelinote From ( SELECT B.trnbelimstoid,B.trnbelino,B.trnbelidate,B.trnsuppoid,S.suppname,GETDATE() payduedate,Case B.trnbelistatus When '' Then 'IN PROCESS' Else B.trnbelistatus End trnbelistatus,B.amtbelinetto amttrans,ISNULL(b.accumpayment,0) AMTBAYAR,B.trnbelinote FROM ql_trnbelimst B INNER JOIN QL_mstsupp S ON B.trnsuppoid = S.suppoid LEFT JOIN ql_conap C ON B.trnbelimstoid = C.refoid AND C.reftype='ql_trnbelimst' and c.conapoid < 0 LEFT JOIN ql_mstacctg a on a.acctgoid=c.acctgoid LEFT OUTER JOIN QL_trnpayap P ON B.trnbelimstoid = P.payrefoid AND P.payreftype = 'ql_trnbelimst' LEFT OUTER JOIN ql_conap C1 ON C1.reftype='QL_trnpayap' AND B.trnbelimstoid = C1.refoid AND P.paymentoid = C1.payrefoid and p.paymentoid < 0 and c1.conapoid < 0 WHERE B.cmpcode = '" & filterCompCode & "' AND S.suppname LIKE '%" & Tchar(filterName) & "%' AND B.trnbelino LIKE '%" & Tchar(filterNo) & "%' AND B.trnbelimstoid < 0 " & branch_code & ")dt group by trnbelimstoid, trnbelino, trnbelidate, trnsuppoid, suppname, payduedate, trnbelistatus, amttrans, trnbelinote ORDER BY trnbelimstoid asc"

        FillGV(gvTrnConap, sqlSelect, "QL_TRNBELIMST")
    End Sub

    Public Sub fillTextBox(ByVal vID As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        sSql = "SELECT B.trnbelimstoid,B.trnsuppoid,Case B.trnbelistatus When '' Then 'IN PROCESS' Else B.trnbelistatus End trnbelistatus,(Select suppacctgoid FROM QL_mstsupp sp Where sp.suppoid=b.trnsuppoid) acctgoid,B.trnbelidate,B.trnbelino,B.trnpaytype,B.amtbeli,B.trntaxpct,B.trnamttax,B.amtbelinetto,B.trnbelinote,C.conapoid,getdate() payduedate,ISNULL(P.paymentoid,0) paymentoid,ISNULL(C1.payrefno,'') payrefno,ISNULL(B.accumpayment,0.00) amtbayar,B.upduser,B.updtime,ISNULL(C.paymentdate,'01/01/1900') paymentdate,ISNULL(C1.acctgoid,0) paymentacctgoid,ISNULL(B.currencyoid,2) AS currencyoid,ISNULL(B.currencyrate,1) AS currencyrate FROM ql_trnbelimst B LEFT JOIN ql_conap C ON B.trnbelimstoid = C.refoid AND C.reftype='ql_trnbelimst' LEFT OUTER JOIN QL_trnpayap P ON B.trnbelimstoid = P.payrefoid AND P.payreftype='ql_trnbelimst' LEFT OUTER JOIN ql_conap C1 ON C1.reftype='QL_trnpayap' AND B.trnbelimstoid = C1.refoid AND P.paymentoid = C1.payrefoid WHERE B.trnbelimstoid=" & vID & " AND B.cmpcode='" & cmpcode & "' ORDER BY B.trnbelimstoid DESC"
        Dim mySqlDA As New SqlClient.SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length > 0 Then
            trnbelimstoid.Text = Trim(objRow(0)("trnbelimstoid").ToString)
            suppoid.Text = Trim(objRow(0)("trnsuppoid").ToString)
            lblPosting.Text = Trim(objRow(0)("trnbelistatus").ToString)
            acctgoid.Text = Trim(objRow(0)("acctgoid").ToString)
            invoicedate.Text = Format(CDate(Trim(objRow(0)("trnbelidate").ToString)), "dd/MM/yyyy")
            trnbelino.Text = (objRow(0)("trnbelino").ToString)
            curroid.SelectedValue = objRow(0)("currencyoid").ToString
            currate.Text = ToMaskEdit(ToDouble(objRow(0)("currencyrate").ToString), 4)

            trnpaytype.SelectedValue = Trim(objRow(0)("trnpaytype").ToString)
            dpp.Text = ToMaskEdit(Trim(objRow(0)("amtbeli").ToString), 4)
            trntaxpct.Text = ToMaskEdit(Trim(objRow(0)("trntaxpct").ToString), 4)
            amttax.Text = ToMaskEdit(Trim(objRow(0)("trnamttax").ToString), 4)
            amtbeli.Text = ToMaskEdit(Trim(objRow(0)("amtbelinetto").ToString), 4)
            trnapnote.Text = Trim(objRow(0)("trnbelinote").ToString)
            conapoid.Text = Trim(objRow(0)("conapoid").ToString)
            payduedate.Text = Format(CDate(Trim(objRow(0)("payduedate").ToString)), "dd/MM/yyyy")
            paymentoid.Text = Trim(objRow(0)("paymentoid").ToString)
            'cashbankoid.Text = Trim(objRow(0)("cashbankoid").ToString)
            paymentdate.Text = Format(CDate(Trim(objRow(0)("paymentdate").ToString)), "dd/MM/yyyy")
            paymentacctgoid.Text = Trim(objRow(0)("paymentacctgoid").ToString)
            payrefno.Text = Trim(objRow(0)("payrefno").ToString)
            amtbayar.Text = ToMaskEdit(Trim(objRow(0)("amtbayar").ToString), 4)
            ReAmount()
            UpdUser.Text = Trim(objRow(0)("upduser").ToString)
            UpdTime.Text = Trim(objRow(0)("updtime").ToString)
            ' Other data
            acctgCode.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = " & acctgoid.Text)
            payacctg.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = '" & paymentacctgoid.Text & "'")
            suppcode.Text = cKon.ambilscalar("SELECT suppname FROM ql_mstsupp WHERE suppoid = " & suppoid.Text)
        End If
        mySqlConn.Close()
        If lblPosting.Text = "POST" Then
            imbposting.Visible = False : btnSave.Visible = False : btnDelete.Visible = False
        Else
            imbposting.Visible = True : btnSave.Visible = True : btnDelete.Visible = True
        End If
    End Sub

    Private Sub generateID()
        If I_U.Text = "NEW DATA" Then
            sSql = "SELECT ISNULL(MIN(trnbelimstoid),0) FROM ql_trnbelimst WHERE trnbelimstoid < 0"
            Session("vIDTrnBeli") = cKon.ambilscalar(sSql)
            trnbelimstoid.Text = Session("vIDTrnBeli") - 1
        End If

        sSql = "SELECT ISNULL(MIN(paymentoid),0) FROM ql_trnpayap WHERE paymentoid < 0"
        Session("vIDPayap") = cKon.ambilscalar(sSql)
        paymentoid.Text = Session("vIDPayap") - 1

        sSql = "SELECT ISNULL(MIN(conapoid),0) FROM ql_conap WHERE conapoid < 0"
        Session("vIDConap") = cKon.ambilscalar(sSql)
        conapoid.Text = Session("vIDConap") - 1
    End Sub

    Private Sub generateGLID()
        sSql = "SELECT ISNULL(MIN(glmstoid),0) FROM ql_trnglmst WHERE glmstoid < 0"
        Session("vIDGLMst") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(gldtloid),0) FROM ql_trngldtl WHERE gldtloid < 0"
        Session("vIDGLDtl") = cKon.ambilscalar(sSql)
        trnglmstoid.Text = Session("vIDGLMst") - 1
        trngldtloid.Text = Session("vIDGLDtl") - 1
    End Sub

    Sub bindDataAccounting(ByVal filterBranch As String, ByVal filterCode As String, ByVal filterDesc As String)
        Dim dt As New DataTable
        If lblTypeCOA.Text = "PaymentAccount" Then
            Dim varCash As String = GetVarInterface("VAR_CASH", cmpcode)
            Dim varBank As String = GetVarInterface("VAR_BANK", cmpcode)

            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE (acctgcode LIKE '" & varCash & "%' OR acctgcode LIKE '" & varBank & "%') AND acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE " & _
                "a.acctggrp3 IS NOT NULL AND a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        ElseIf lblTypeCOA.Text = "PurchAccount" Then
            Dim varPuch As String = GetVarInterface("VAR_PURCHASING", cmpcode)
            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varPuch & "%' AND " & _
                "acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND " & _
                "a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        Else
            Dim varAP As String = GetVarInterface("VAR_AP", cmpcode)
            dt = cKon.ambiltabel("SELECT acctgoid,acctgcode,acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & varAP & "%' AND " & _
                "acctgoid NOT IN (SELECT DISTINCT a.acctggrp3 FROM QL_mstacctg a WHERE a.acctggrp3 IS NOT NULL AND " & _
                "a.cmpcode=ql_mstacctg.cmpcode) AND cmpcode = '" & filterBranch & "' AND acctgcode LIKE '%" & _
                filterCode & "%' AND acctgdesc LIKE '%" & filterDesc & "%' order by acctgoid", "ql_mstacctg")
        End If
        gvAcc.DataSource = dt
        gvAcc.DataBind()
    End Sub

    Public Sub bindDataSupplier(ByVal filterBranch As String, ByVal filterCode As String, ByVal filterName As String)
        Dim dt As New DataTable
        If ddlSup.SelectedValue = "Code" Then
            sSql = "select  suppoid ,suppcode , suppname , 0 coa_hutang , '' coa  ,SUBSTRING(suppcode,1,2) FROM QL_mstsupp s  WHERE s.cmpcode = 'MSC'  and suppcode like '%" & filterName & "%' ORDER BY suppname "
        Else
            sSql = "select  suppoid ,suppcode , suppname , 0 coa_hutang , '' coa  ,SUBSTRING(suppcode,1,2) FROM QL_mstsupp s  WHERE s.cmpcode = 'MSC'  and suppname like '%" & filterName & "%' ORDER BY suppname "
        End If
        dt = cKon.ambiltabel(sSql, "TblMstSupp")
        gvSupplier.DataSource = dt
        gvSupplier.DataBind()
    End Sub

    Private Sub msg(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub
#End Region

#Region "Function"
    Private Function validatingData() As String
        Dim sMsg As String = ""
        If suppoid.Text = "" Then
            sMsg &= "- Supplier harus diisi !!<BR>"
        End If
        If trnbelino.Text = "" Then
            sMsg &= "-  No Nota harus diisi !!<BR>"
        End If
        If acctgCode.Text = "" Then
            sMsg &= "- COA Hutang harus diisi !!<BR>"
        End If
 
        Dim st1 As Boolean = False : Dim st2 As Boolean = False : Dim st3 As Boolean = False
        Try
            Dim duedate As Date : duedate = toDate(invoicedate.Text) : st1 = True
        Catch ex As Exception
            sMsg &= "- Format Tgl Invoice salah !!<BR>"
        End Try
        Try
            Dim duedate As Date : duedate = toDate(payduedate.Text) : st2 = True
        Catch ex As Exception
            sMsg &= "- Format Tgl Jatuh tempo salah !!<BR>"
        End Try
        Try
            If ToDouble(paymentacctgoid.Text) > 0 Or ToDouble(amtbayar.Text) > 0 Then
                Dim duedate As Date : duedate = toDate(paymentdate.Text) : st3 = True
            End If

        Catch ex As Exception
            sMsg &= "- Format Tgl Pembayaran salah !!<BR>"
        End Try
        If st1 And st2 And st3 Then
            If DateDiff(DateInterval.Day, CDate(toDate(invoicedate.Text)), CDate(toDate(payduedate.Text))) < 0 Then
                sMsg &= "- Tgl Jth tempo harus >= Tgl invoice !!<BR>"
            End If
            If DateDiff(DateInterval.Day, CDate(toDate(invoicedate.Text)), CDate(toDate(paymentdate.Text))) < 0 Then
                sMsg &= "- Tgl Pembayaran harus >= tgl Invoice   !!<BR>"
            End If
        End If
        If amtbeli.Text = "" Or ToDouble(amtbeli.Text) <= 0 Then
            sMsg &= "- Total Invoice harus >= 0 !!<BR>"
        End If
        If ToDouble(amtbayar.Text) >= ToDouble(amtbeli.Text) Then
            sMsg &= "- Total bayar harus < Total Invoice  !!<BR>"
        End If
        If trnapnote.Text.Trim.Length > 200 Then
            sMsg &= "- Masimal catatan pada note hanya 200 karakter !!<BR>"
        End If
        Return sMsg
    End Function
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("trnConAP.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        imbposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        Page.Title = "A/P Initial Balance"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            msg("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", compnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================

        If Not Page.IsPostBack Then
            initDDL()
            binddata(cmpcode, "", "")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                btnDelete.Visible = True
                fillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                I_U.Text = "UPDATE "
            Else
                generateID() : I_U.Text = "NEW DATA"
                invoicedate.Text = Format(CutOffDate.AddDays(-1), "dd/MM/yyyy")
                invoicedate_TextChanged(sender, e)
                paymentdate.Text = Format(GetServerTime, "dd/MM/yyyy")
                btnDelete.Visible = False
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        generateGLID()
        If lblPosting.Text = "POST" Then
            btnshowCOA.Visible = False
        Else
            btnshowCOA.Visible = False
        End If
    End Sub

    Protected Sub btnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCariSupp.Click
        bindDataSupplier(cmpcode, "", "")
        btnHiddenSupp.Visible = True : PanelSupp.Visible = True
        gvSupplier.Visible = True : ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnFindSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindSupp.Click
        bindDataSupplier(cmpcode, "", Tchar(filterSupp.Text))
        gvSupplier.Visible = True : btnHiddenSupp.Visible = True
        PanelSupp.Visible = True : ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListSupp.Click
        filterSupp.Text = "" : bindDataSupplier(cmpcode, "", "")
        btnHiddenSupp.Visible = True : PanelSupp.Visible = True
        gvSupplier.Visible = True : ModalPopupExtender1.Show()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid.Text = gvSupplier.SelectedDataKey(0).ToString()
        suppcode.Text = gvSupplier.SelectedDataKey(2).ToString()
        lblTypeCOA.Text = "APAccount"
        acctgoid.Text = gvSupplier.SelectedDataKey("coa_hutang").ToString().Trim
        acctgCode.Text = gvSupplier.SelectedDataKey("coa").ToString().Trim

        sSql = "SELECT distinct  acctgoid,acctgcode + '-' + acctgdesc as desk FROM QL_mstacctg W inner join QL_mstinterface i on W.acctgcode like i.interfacevalue + '%' And i.interfacevar like 'VAR_AP' AND  acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=W.cmpcode) AND W.cmpcode='" & cmpcode & "' ORDER BY acctgoid  "
        Dim otable As DataTable = CreateDataTableFromSQL(sSql)
        If otable.Rows.Count > 0 Then
            acctgoid.Text = otable.Rows(0).Item("acctgoid")
            acctgCode.Text = otable.Rows(0).Item("desk")
        End If
        If ToDouble(acctgoid.Text) = 0 Then
            btnSearcAcctg.Visible = True
        Else
            btnSearcAcctg.Visible = False
        End If
        btnHiddenSupp.Visible = False
        PanelSupp.Visible = False
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub ClosePopUP_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenSupp.Visible = False
        PanelSupp.Visible = False
        ModalPopupExtender1.Hide()
    End Sub

    Protected Sub btnSearcAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearcAcctg.Click
        lblTypeCOA.Text = "APAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub btnFindAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ddlAcctg.SelectedValue = "Code" Then
            bindDataAccounting(cmpcode, Tchar(filterAcctg.Text), "")
        ElseIf ddlAcctg.SelectedValue = "Name" Then
            bindDataAccounting(cmpcode, "", Tchar(filterAcctg.Text))
        End If
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub btnListAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        filterAcctg.Text = ""
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub gvAcc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAcc.SelectedIndexChanged
        If lblTypeCOA.Text = "PaymentAccount" Then
            paymentacctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            payacctg.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        ElseIf lblTypeCOA.Text = "PurchAccount" Then
            purchAcctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            purchAcctg.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        Else
            acctgoid.Text = gvAcc.SelectedDataKey(0).ToString().Trim
            acctgCode.Text = gvAcc.SelectedDataKey(1).ToString().Trim & "-" & gvAcc.SelectedDataKey(2).ToString().Trim
        End If
        btnHiddenAcctg.Visible = False
        PanelAcctg.Visible = False
        ModalPopupExtender2.Hide()
    End Sub

    Protected Sub CloseAcctg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHiddenAcctg.Visible = False
        PanelAcctg.Visible = False
        ModalPopupExtender2.Hide()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sValue As String = validatingData()
        If sValue <> "" Then
            lblPosting.Text = ""
            msg(sValue, compnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        If CDate(toDate(invoicedate.Text)) > CDate(toDate(CutofDate.Text)) Then
            msg("- Tanggal invoice Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> ", compnyName & "- WARNING", 2, "modalMsgBox")
        End If

        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
        If ToDouble(amtbayar.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "Select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(invoicedate.Text))) < GetStrData(sSql) Then
                msg("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", compnyName & "- WARNING", 2, "modalMsgBox")
                Exit Sub
            End If
        End If
        'End If

        '=======================================================================================
        Dim rateidr As Decimal = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")
        Dim rateusd As Decimal = GetStrData("select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")

        generateID()
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn : xCmd.Transaction = objTrans

        'sSql = "Select COUNT(*) From QL_trnbelimst Where trnsuppoid=" & suppoid.Text & " AND branch_code='" & Session("branch_id") & "' AND trnbelimstoid < 0 AND trnbelistatus ='POST'"
        'xCmd.CommandText = sSql
        'If xCmd.ExecuteScalar() > 0 Then
        '    msg("Customer " & suppcode.Text & " Sudah ada saldo awal..!!", compnyName & " - Warning", 2, "modalMsgBoxWarn")
        '    Exit Sub
        'End If

        Try
            ' ql_trnbelimst
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "DELETE QL_trnbelimst WHERE cmpcode='" & _
                    cmpcode & "' And trnbelimstoid=" & trnbelimstoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trnbelimst (cmpcode ,trnbelimstoid,branch_code ,periodacctg ,trnbelitype ,trnbelino ,trnbelidate ,trnbelipono ,trnbeliref ,trnsuppoid ,trnpaytype ,trnbelidisc,trnbelidiscidr,trnbelidiscusd ,trnbelidisctype ,trnbelinote ,trnbelistatus ,createuser ,upduser ,updtime ,amtdischdr,amtdischdridr,amtdischdrusd ,amtbeli,amtbeliidr,amtbeliusd ,amtbelinetto,amtbelinettoidr,amtbelinettousd ,accumpayment,accumpaymentidr,accumpaymentusd ,lastpaydate ,amtdiscdtl,amtdiscdtlidr,amtdiscdtlusd ,refnotaRevisi ,PIC ,trntaxpct,trnamttax ,trnamttaxidr ,trnamttaxusd ,trnbelidisctype2 ,trnbelidisc2,trnbelidisc2idr,trnbelidisc2usd ,amtdischdr2 ,amtdischdr2idr ,amtdischdr2usd ,trnbelidisctype3 ,trnbelidisc3,amtdischdr3 ,amtretur,amtreturidr,amtreturusd ,postacctg ,user_postacctg ,updtime_postacctg ,currencyrate ,currencyoid,amtbelinettoacctg,amtbelinettoacctgidr,amtbelinettoacctgusd) VALUES" & _
                " ('" & cmpcode & "'," & trnbelimstoid.Text & ",'" & Session("branch_id") & "','" & GetDateToPeriodAcctg3(toDate(invoicedate.Text)) & "','SALDO AWAL','" & Tchar(trnbelino.Text) & "','" & toDate(invoicedate.Text) & "','',''," & suppoid.Text & ",0,0,0,0,'AMT','" & Tchar(trnapnote.Text) & "','" & lblPosting.Text & "','" & Session("USERID") & "','" & Session("USERID") & "',CURRENT_TIMESTAMP,0,0,0," & CDec(dpp.Text) & "," & CDec(dpp.Text) * rateidr & "," & CDec(dpp.Text) * rateusd & "," & CDec(amtbeli.Text) & "," & CDec(amtbeli.Text) * rateidr & "," & CDec(amtbeli.Text) * rateusd & "," & CDec(amtbayar.Text) & "," & CDec(amtbayar.Text) * rateidr & "," & CDec(amtbayar.Text) * rateusd & ",'" & toDate(paymentdate.Text) & "',0,0,0,'',0," & CDec(trntaxpct.Text) & "," & CDec(amttax.Text) & "," & CDec(amttax.Text) * rateidr & "," & CDec(amttax.Text) * rateusd & ",'AMT',0,0,0,0,0,0,'AMT',0,0,0,0,0,'', '" & Session("USERID") & "',CURRENT_TIMESTAMP," & CDec(currate.Text) & ", " & curroid.SelectedValue & "," & CDec(amtbeli.Text) & "," & CDec(amtbeli.Text) * rateidr & "," & CDec(amtbeli.Text) * rateusd & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE QL_trnbelimst SET periodacctg = '" & GetDateToPeriodAcctg3(toDate(invoicedate.Text)) & "',trnbelino='" & Tchar(trnbelino.Text) & "',trnbelidate = '" & toDate(invoicedate.Text) & "',trnsuppoid = " & suppoid.Text & ",trnbelinote = '" & Tchar(trnapnote.Text) & "',trnbelistatus='" & lblPosting.Text & "',createuser = '" & Session("USERID") & "',upduser = '" & Session("USERID") & "',updtime = CURRENT_TIMESTAMP,amtbeli = " & CDec(dpp.Text) & ",amtbeliidr = " & CDec(dpp.Text) * rateidr & ",amtbeliusd = " & CDec(dpp.Text) * rateusd & ",amtbelinetto = " & CDec(amtbeli.Text) & ",amtbelinettoidr = " & CDec(amtbayar.Text) * rateidr & ",amtbelinettousd = " & CDec(amtbayar.Text) * rateusd & ",accumpayment = " & CDec(amtbayar.Text) & ",accumpaymentidr = " & CDec(amtbayar.Text) * rateidr & ",accumpaymentusd = " & CDec(amtbayar.Text) * rateusd & ",lastpaydate = '" & toDate(paymentdate.Text) & "',trntaxpct = " & CDec(trntaxpct.Text) & ",trnamttax = " & CDec(amttax.Text) & ",trnamttaxidr = " & CDec(amttax.Text) * rateidr & ",trnamttaxusd = " & CDec(amttax.Text) * rateusd & ",user_postacctg = '" & Session("USERID") & "',updtime_postacctg = CURRENT_TIMESTAMP,currencyrate = " & CDec(currate.Text) & ",currencyoid = " & curroid.SelectedValue & ",amtbelinettoacctg = " & CDec(amtbeli.Text) & ",amtbelinettoacctgidr = " & CDec(amtbeli.Text) * rateidr & ",amtbelinettoacctgusd = " & CDec(amtbeli.Text) * rateusd & _
                " WHERE trnbelimstoid=" & trnbelimstoid.Text & " AND branch_code='" & Session("branch_id") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            
            Dim asjdjs As Decimal = CDec(trntaxpct.Text)
            If lblPosting.Text = "POST" Then
                'CONAP
                sSql = "INSERT INTO QL_conap (cmpcode,conapoid, branch_code,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amttransidr,amttransusd,amtbayar,amtbayaridr,amtbayarusd,trnapnote,trnapres1,upduser,updtime,returoid) VALUES " & _
                " ('" & cmpcode & "'," & Trim(conapoid.Text) & ",'" & Session("branch_id") & "','QL_trnbelimst'," & trnbelimstoid.Text & ",0," & suppoid.Text & "," & Trim(acctgoid.Text) & ",'" & lblPosting.Text & "','AP','" & toDate(invoicedate.Text) & "','" & GetDateToPeriodAcctg3(toDate(invoicedate.Text)) & "',0,'" & toDate(paymentdate.Text) & "','',0,'" & toDate(payduedate.Text) & "'," & ToDouble(amtbeli.Text) & "," & CDec(amtbeli.Text) * rateidr & "," & CDec(amtbeli.Text) * rateusd & ",0,0,0,'" & Tchar(trnapnote.Text) & "','','" & Trim(Session("UserID")) & "',CURRENT_TIMESTAMP,0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If ToDouble(amtbayar.Text) > 0 Then
                    'PAYAP
                    sSql = "INSERT INTO QL_trnpayap (cmpcode,paymentoid,cashbankoid,suppoid,payreftype,payrefoid,payflag,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd,payrefflag,paystatus,upduser,updtime,trndpapoid,DPAmt,returoid, branch_code)" & _
                    " VALUES ('" & cmpcode & "'," & Trim(paymentoid.Text) & ",0," & suppoid.Text & ",'QL_trnbelimst'," & trnbelimstoid.Text & ",'','" & acctgoid.Text & "','" & Tchar(payrefno.Text) & "',0,'01/01/1900','" & Tchar(trnapnote.Text) & "'," & ToDouble(amtbayar.Text) & ",'" & CInt(amtbayar.Text) * rateidr & "','" & CInt(amtbayar.Text) * rateusd & "','','" & lblPosting.Text & "','" & Trim(Session("UserID")) & "',CURRENT_TIMESTAMP,0,0,0,'" & Session("branch_id") & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                End If
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            lblPosting.Text = ""
            objTrans.Rollback() : conn.Close()
            msg(ex.ToString & "<BR>" & sSql, compnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        If lblPosting.Text = "POST" Then
            msg("Data telah diposting !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            msg("Data telah disimpan !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            ' ql_trnbelimst
            sSql = "DELETE ql_trnbelimst WHERE cmpcode='" & cmpcode & "' and trnbelimstoid=" & trnbelimstoid.Text
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            ' ql_conap - Invoice
            'strSQL = "DELETE QL_conap WHERE cmpcode='" & cmpcode & "' and conapoid=" & conapoid.Text
            'objCmd.CommandText = strSQL
            'objCmd.ExecuteNonQuery()

            '' ql_trncashbankmst
            'strSQL = "DELETE ql_trncashbankmst WHERE cmpcode='" & cmpcode & "' and cashbankoid=" & cashbankoid.Text
            'objCmd.CommandText = strSQL
            'objCmd.ExecuteNonQuery()

            ' ql_trnpayap
            'strSQL = "DELETE ql_trnpayap WHERE cmpcode='" & cmpcode & "' and paymentoid=" & paymentoid.Text
            'objCmd.CommandText = strSQL
            'objCmd.ExecuteNonQuery()

            '' ql_conap - Payment
            'strSQL = "DELETE QL_conap WHERE cmpcode='" & cmpcode & "' and conapoid=" & conapoid.Text - 1
            'objCmd.CommandText = strSQL
            'objCmd.ExecuteNonQuery()

            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            msg(ex.Message, compnyName & " - ERROR", 1, "modalMsgBox") : Exit Sub
        End Try
        msg("Data telah dihapus !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        'Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub btnFindConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindConap.Click
        If ddlFilter.SelectedValue = "name" Then
            binddata(cmpcode, FilterConap.Text, "")
        ElseIf ddlFilter.SelectedValue = "no" Then
            binddata(cmpcode, "", FilterConap.Text)
        End If
    End Sub

    Protected Sub btnViewAllConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddata(cmpcode, "", "")
        FilterConap.Text = ""
    End Sub

    Protected Sub gvTrnConap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnconap.aspx?oid=" & gvTrnConap.SelectedDataKey(0).ToString().Trim)
    End Sub

    Protected Sub gvTrnConap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterConap.Text = ""
    End Sub

    Protected Sub trntaxpct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub amtbeli_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub imbPayAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblTypeCOA.Text = "PaymentAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub imbposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbposting.Click
        ' POST to GL
        lblPosting.Text = "POST" : postdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
        btnSave_Click(sender, e)
    End Sub

    Protected Sub imbPurch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblTypeCOA.Text = "PurchAccount"
        bindDataAccounting(cmpcode, "", "")
        btnHiddenAcctg.Visible = True
        PanelAcctg.Visible = True
        gvAcc.Visible = True
        ModalPopupExtender2.Show()
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        FillRate(curroid.SelectedValue)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA("CONAP=" & Tchar(trnbelino.Text), cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 4)

        End If
    End Sub

    Protected Sub gvTrnConap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTrnConap.PageIndexChanging
        gvTrnConap.PageIndex = e.NewPageIndex
        If ddlFilter.SelectedValue = "name" Then
            binddata(cmpcode, FilterConap.Text, "")
        ElseIf ddlFilter.SelectedValue = "no" Then
            binddata(cmpcode, "", FilterConap.Text)
        End If
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub trnpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "select genother1 from ql_mstgen where genoid=" & trnpaytype.SelectedValue
        payduedate.Text = Format(CDate(toDate(invoicedate.Text)).AddDays(ToDouble(GetStrData(sSql))), "dd/MM/yyyy")
    End Sub

    Protected Sub invoicedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles invoicedate.TextChanged
        Try
            sSql = "select genother1 from ql_mstgen where genoid=" & trnpaytype.SelectedValue
            payduedate.Text = Format(CDate(toDate(invoicedate.Text)).AddDays(ToDouble(GetStrData(sSql))), "dd/MM/yyyy")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnConAP.aspx?awal=true")
        End If
    End Sub
#End Region
End Class
