<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="TrnWh.aspx.vb" Inherits="trnWh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: TW E-COMMERCE"></asp:Label>
            </th>
        </tr>
        <tr>
            <th align="left" style="background-color: transparent" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of TW E-Commerce :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="fCbangDDL" runat="server" CssClass="inpText">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:DropDownList id="DDLFilter" runat="server" Width="100px" CssClass="inpText">
    <asp:ListItem Value="trntwmstoid">Draft</asp:ListItem>
    <asp:ListItem Value="trntwmstno">No. TW</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="83px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> - <asp:TextBox id="FilterPeriod2" runat="server" Width="83px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label> </TD></TR><TR><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD align=left colSpan=2><asp:DropDownList id="ddlstatus" runat="server" Width="100px" CssClass="inpText">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem>In Process</asp:ListItem>
    <asp:ListItem Value="POST">Post</asp:ListItem>
                                                                        </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 108px; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" align=left colSpan=2><asp:ImageButton style="MARGIN-RIGHT: 0px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 0px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton><ajaxToolkit:CalendarExtender id="CE1" runat="server" TargetControlID="FilterPeriod1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CE2" runat="server" TargetControlID="FilterPeriod2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; HEIGHT: 10px" class="Label" align=left colSpan=4><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None" PageSize="8" AllowPaging="True">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333"  />
                                                        <Columns>
                                                            <asp:HyperLinkField DataNavigateUrlFields="trntwmstoid" 
                                                                DataNavigateUrlFormatString="trnWh.aspx?oid={0}" 
                                                                DataTextField="trntwmstno" HeaderText="Transfer No">
                                                            <ControlStyle ForeColor="Red"  />
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False"  />
                                                            <ItemStyle ForeColor="Black" HorizontalAlign="Left"  />
                                                            </asp:HyperLinkField>
                                                            <asp:BoundField DataField="trntwmstdate" 
                                                                HeaderText="Tanggal" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" 
                                                                Wrap="False"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="gendesc" HeaderText="Cabang" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" 
                                                                Wrap="False"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="asal" HeaderText="Gd. Asal">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="tujuan" HeaderText="Gd. Tujuan">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="trntwmststatus" HeaderText="Status" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"  />
                                                            <ItemStyle HorizontalAlign="Left" Wrap="False"  />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="trntwmstnote" HeaderText="Keterangan" >
                                                            <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"  />
                                                            <ItemStyle HorizontalAlign="Left"  />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    &nbsp;<asp:LinkButton ID="lbprint" runat="server" OnClick="lbprint_Click" 
                                                                        ToolTip='<%# eval("trntwmstoid") %>'>Print</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvhdr" ForeColor="Black"  />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                        <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" 
                                                            Font-Bold="True"  />
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"  />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"  />
                                                        <AlternatingRowStyle BackColor="White"  />
                                                    </asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form TW E-Commerce :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Width="115px" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Data Header :" Font-Underline="True"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:DropDownList id="DDLCabang" runat="server" CssClass="inpText" AutoPostBack="True">
                                                    </asp:DropDownList> <asp:Label id="Trntwmstoid" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>No. Transfer</TD><TD class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="129px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" Enabled="False" MaxLength="20"></asp:TextBox> <asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" Visible="False"></asp:Label></TD><TD class="Label" align=left>Tanggal</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" Enabled="False" ValidationGroup="MKE"></asp:TextBox> <asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left>Gd. Asal</TD><TD class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:DropDownList id="GdAsal" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left>Gd. Tujuan</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="GdTujuan" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Keterangan</TD><TD class="Label" align=left>:</TD><TD style="WIDTH: 331px" class="Label" align=left><asp:TextBox id="note" runat="server" Width="251px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="trnstatus" runat="server" CssClass="inpTextDisabled" Enabled="False" MaxLength="20">In Process</asp:TextBox> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Label id="Label18" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Data Detail :" Font-Underline="True"></asp:Label> <asp:Label id="labelseq" runat="server" Text="0" Visible="False"></asp:Label> <asp:Label id="stockflag" runat="server"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Katalog <asp:Label id="Label6" runat="server" CssClass="label" ForeColor="Red">*</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="itemdesc" runat="server" Width="282px" CssClass="inpText" Font-Bold="False" ForeColor="Black"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px"></asp:ImageButton><asp:Label id="itemoid" runat="server" Visible="False"></asp:Label><asp:Label id="itemCode" runat="server" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" Visible="False"></asp:Label> <asp:Label id="dtlseq" runat="server" Visible="False">1</asp:Label></TD></TR><TR><TD class="Label" align=left>Qty<asp:Label id="Label7" runat="server" CssClass="label" ForeColor="Red">*</asp:Label></TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="53px" CssClass="inpText" AutoPostBack="True" MaxLength="10" ValidationGroup="MKE">0.00</asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="'<='" Visible="False" w153=""></asp:Label> <asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red" Visible="False">0.00</asp:Label> </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left>Keterangan</TD><TD style="VERTICAL-ALIGN: top" class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="dtlseq" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="dtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="40px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ReqQty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dtlnote" HeaderText="Note">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Width="50px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="Data Detail Belum ada..!!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label> <asp:Label id="createtime" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                        <ProgressTemplate>
                                                            <div id="Div1" class="progressBackgroundFilter">
                                                            </div>
                                                            <div id="Div3" class="processMessage">
                                                                <span style="font-weight: bold; font-size: 10pt; color: purple">
                                                                    <asp:Image ID="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif"  /><br  />
Please Wait .....</span><br  />
                                                            </div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" ValidChars="0123456789.," TargetControlID="qty"></ajaxToolkit:FilteredTextBoxExtender> &nbsp;&nbsp;</TD></TR></TBODY></TABLE><asp:UpdatePanel id="upListMat" runat="server"><ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" vAlign=top align=center colSpan=3>Filter : &nbsp;&nbsp;<asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" AutoPostBack="True">
                                                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                                                        <asp:ListItem Value="JenisNya">Jenis Katalog</asp:ListItem>
                                                                                        <asp:ListItem Value="statusitem">Type Katalog</asp:ListItem>
                                                                                    </asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat">
                                                                    <ProgressTemplate>
                                                                        <table style="width: 200px">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="font-weight: bold; font-size: 10pt; color: purple">
                                                                                        Load Data...</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <asp:Image ID="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" /></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=left colSpan=3>Show <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText"></asp:TextBox> Data Per Page <asp:LinkButton id="lbShowData" runat="server">>> View</asp:LinkButton></TD></TR><TR><TD class="Label" align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 894px; HEIGHT: 13%; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowPaging="True">
                                                                        <PagerSettings FirstPageText="First" LastPageText="Last" />
                                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SaldoAkhir" HeaderText="Stok">
                                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Qty">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="tbQty" runat="server" CssClass="inpText" MaxLength="12" Text='<%# eval("ReqQty") %>'
                                                                                        Width="50px"></asp:TextBox>
                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeQtyLM" runat="server" TargetControlID="tbQty"
                                                                                        ValidChars="1234567890.,">
                                                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Right" Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Note">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="tbNote" runat="server" CssClass="inpText" MaxLength="100" Text='<%# eval("dtlnote") %>'
                                                                                        Width="200px"></asp:TextBox>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="statusitem" HeaderText="Type">
                                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" Wrap="False" />
                                                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                                                            HorizontalAlign="Right" />
                                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                    </asp:GridView> </DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server" CssClass="green" Font-Bold="True" Font-Underline="False"> Add To List </asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server" CssClass="red" Font-Bold="True" Font-Underline="False"> Cancel & Close </asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True" BackgroundCssClass="modalBackground">
                                                </ajaxToolkit:ModalPopupExtender> <asp:HiddenField id="hfStatus" runat="server"></asp:HiddenField> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer><asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="background-color: #cc0000; text-align: left">
                                            <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 10px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                Width="24px" /></td>
                                        <td class="Label" style="text-align: left">
                                            <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 10px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                            &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                            Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                            TargetControlID="bePopUpMsg">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                </th>
        </tr>
    </table>
    
</asp:Content>