<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptsuratjalan.aspx.vb" Inherits="ReportForm_rptsuratjalan" title="Untitled Page" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=":: Laporan Surat Jalan"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 3px">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 971px">
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD colSpan=3></TD></TR><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText" __designer:wfdid="w16"></asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList style="MARGIN-RIGHT: 10px" id="ddlfilter" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w205"><asp:ListItem Value="b.suppname">Supplier</asp:ListItem>
<asp:ListItem Value="a.sjlperson">PIC</asp:ListItem>
<asp:ListItem Value="c.barcode">Barcode</asp:ListItem>
</asp:DropDownList><asp:TextBox id="tbfilter" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w206"></asp:TextBox></TD></TR><TR><TD>Periode</TD><TD>:</TD><TD><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w207"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w208"></asp:ImageButton> &nbsp;to&nbsp; <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w209"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w210"></asp:ImageButton> &nbsp;&nbsp; &nbsp;<SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></TD></TR><TR style="COLOR: #000099"><TD><asp:CheckBox id="cbstatus" runat="server" Text="Status" __designer:wfdid="w84"></asp:CheckBox></TD><TD>:</TD><TD><asp:DropDownList id="ddlfilterstatus" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Send</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 40px; TEXT-ALIGN: center" colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w212" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodstart"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w213" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodend"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w214" TargetControlID="tbperiodstart" PopupButtonID="ibperiodstart" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w215" TargetControlID="tbperiodend" PopupButtonID="ibperiodend" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w7" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loading_animate.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibreport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w216"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibtopdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w217" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibtoexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w218" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w219"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=3></TD></TR><TR><TD colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="crv" runat="server" __designer:wfdid="w222" HasGotoPageButton="False" DisplayGroupTree="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibreport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibtopdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibtoexcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel></td>
        </tr>
    </table>
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderColor="Transparent" BorderStyle="None"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

