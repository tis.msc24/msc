<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptStatusSIKonsi.aspx.vb" Inherits="rptStatusSIKonsi" title="MSC - Laporan Status SI Konsinyasi" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Status SI Konsinyasi"></asp:Label>
            </th>
        </tr>

        <tr>
            <th align="center" class="header" style="width: 100%; background-color: transparent"
                valign="center">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="3">
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                 <contenttemplate>
<TABLE><TBODY><TR><TD align=left runat="server" Visible="true"><asp:Label id="lblCabang" runat="server" Text="Cabang"></asp:Label> </TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="dCabangNya" runat="server" CssClass="inpText"></asp:DropDownList> </TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode"></asp:Label> </TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="ImgDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;-&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="ImgDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; <asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label> </TD></TR><TR><TD id="td3" align=left runat="server" Visible="true"><asp:Label id="Label2" runat="server" Text="Customer"></asp:Label> <asp:Label id="TxtCustoid" runat="server" Text="0" Visible="false"></asp:Label> </TD><TD id="td4" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="Custcode" runat="server" Width="130px" Height="40px" CssClass="inpTextDisabled" Enabled="false" TextMode="MultiLine"></asp:TextBox>&nbsp; <asp:ImageButton id="BtnCust" runat="server" Width="30px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="30px"></asp:ImageButton> <asp:ImageButton id="BtnEraseCust" runat="server" Width="30px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="30px"></asp:ImageButton> </TD></TR><TR><TD id="td1" align=left runat="server" Visible="true"><asp:Label id="Label1" runat="server" Text="No. SI"></asp:Label> <asp:Label id="TxtJualMstoid" runat="server" Text="0" Visible="false"></asp:Label> </TD><TD id="td2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="TextNoSI" runat="server" Width="130px" Height="40px" CssClass="inpTextDisabled" Enabled="false" TextMode="MultiLine"></asp:TextBox>&nbsp; <asp:ImageButton id="ImbBtnSI" runat="server" Width="30px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="30px"></asp:ImageButton> <asp:ImageButton id="imbEraseSO" runat="server" Width="30px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="30px"></asp:ImageButton> </TD></TR><TR><TD id="td5" align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Text="No. SR"></asp:Label> <asp:Label id="TxtRetMstoid" runat="server" Text="0" Visible="false"></asp:Label> </TD><TD id="td6" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="TxtNoSR" runat="server" Width="130px" Height="40px" CssClass="inpTextDisabled" Enabled="false" TextMode="MultiLine"></asp:TextBox>&nbsp; <asp:ImageButton id="BtnSR" runat="server" Width="30px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="30px"></asp:ImageButton> <asp:ImageButton id="BtnEraseSR" runat="server" Width="30px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="30px"></asp:ImageButton> </TD></TR><TR><TD id="td7" align=left runat="server" Visible="true"><asp:Label id="Label7" runat="server" Text="Katalog"></asp:Label> <asp:Label id="TxtItemOid" runat="server" Text="0" Visible="false"></asp:Label> </TD><TD id="td8" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="TxtKatalog" runat="server" Width="130px" Height="40px" CssClass="inpTextDisabled" Enabled="false" TextMode="MultiLine"></asp:TextBox>&nbsp; <asp:ImageButton id="BtnItem" runat="server" Width="30px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="30px"></asp:ImageButton> <asp:ImageButton id="BtnEraseItem" runat="server" Width="30px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="30px"></asp:ImageButton> </TD></TR><TR><TD id="TD10" align=left runat="server" Visible="false">Status</TD><TD id="TD9" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="statusDelivery" runat="server" Width="100px" CssClass="inpText">
                                                        <asp:ListItem>ALL</asp:ListItem>
                                                        <asp:ListItem>COMPLETE</asp:ListItem>
                                                        <asp:ListItem>IN COMPLETE</asp:ListItem>
                                                    </asp:DropDownList> </TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                        <ProgressTemplate>
                                                        <DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV>
                                                        <DIV id="processMessage" class="processMessage">
                                                            <SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">
                                                                <asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image>
                                                                <BR />Please Wait .....!
                                                            </SPAN>
                                                            <BR />
                                                        </DIV>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateAwal" PopupButtonID="ImgDate1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="dateAkhir" PopupButtonID="ImgDate2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrvSiKonsiNyasi" runat="server" Width="350px" Height="50px" HasToggleGroupTreeButton="False" HasSearchButton="False" HasRefreshButton="True" HasPrintButton="False" HasExportButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True">
                                    </CR:CrystalReportViewer> 
</contenttemplate>
                                <triggers>
                                    <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
                                </triggers>
                            </asp:UpdatePanel>

                            <!--Start Pop Up Data Customer -->    
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelCust" runat="server" Width="800px" CssClass="modalBox" Visible="False">
                                <TABLE style="WIDTH: 100%">
                                    <TBODY>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                <asp:Label id="lblListCust" runat="server" Font-Size="Large" Font-Bold="True" Text="List of Customer"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                   <asp:Panel id="Panel2" runat="server" Width="100%" DefaultButton="BtnFindCust">
                                                    <asp:Label id="Label5" runat="server" Font-Size="8pt" Text="Filter :"/> 
                                                    <asp:DropDownList id="DDLCustFilter" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt">
                                                        <asp:ListItem Value="custname">Customer</asp:ListItem>
                                                        <asp:ListItem Value="custcode">Kode</asp:ListItem>
                                                    </asp:DropDownList> 
                                                    <asp:TextBox id="TxtFilterCust" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"/>
                                                    <asp:ImageButton id="BtnFindCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="BtnFindALLCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"/>                                                     
                                                </asp:Panel> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                                                <asp:ImageButton id="BtnCheckAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"/>&nbsp;
                                                <asp:ImageButton id="BtnCheckNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" />&nbsp;
                                                <asp:ImageButton id="BtnViewCheckCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="false"/> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3> 
                                            <asp:GridView id="GVCust" runat="server" Width="795px" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="custoid,custcode" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbCust" runat="server" Checked='<%#Eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>'/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center"/>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="custoid" HeaderText="Draft No" Visible="false"/>
                            
                                                        <asp:BoundField DataField="custcode" HeaderText="Kode">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="custname" HeaderText="Customer">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>          
                                                        
                                                         <asp:BoundField DataField="custaddr" HeaderText="Alamat">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                        </asp:BoundField>                                               
                                                   </Columns>

                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                    <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"/><PagerSettings Visible="true" />
                                                    <EmptyDataTemplate>&nbsp;</EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="White"/>
                                                </asp:GridView>                                  
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD align="center" colSpan="3">
                                                <asp:LinkButton id="LblAddToListCust" runat="server">[ Add To List ]</asp:LinkButton> - 
                                                <asp:LinkButton id="LblCloseCust" runat="server">[ Cancel & Close ]</asp:LinkButton>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                            </asp:Panel> 
                                <ajaxToolkit:ModalPopupExtender id="mpeCUst" runat="server" TargetControlID="BtnHiddenCust" PopupDragHandleControlID="lblListcUst" PopupControlID="PanelCust" BackgroundCssClass="modalBackground">
                                 </ajaxToolkit:ModalPopupExtender> 
                                <asp:Button id="BtnHiddenCust" runat="server" Visible="False"></asp:Button> 
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--End Pop Up Data Customer -->

                            <!--Start Pop Up Data SI -->    
                            <asp:UpdatePanel ID="upListSO" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelListNota" runat="server" Width="800px" CssClass="modalBox" Visible="False">
                                <TABLE style="WIDTH: 100%">
                                    <TBODY>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                <asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List of Nota SI"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                   <asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO">
                                                    <asp:Label id="Label24" runat="server" Font-Size="8pt" Text="Filter :"/> 
                                                    <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt">
                                                        <asp:ListItem Value="trnjualno">No. SI</asp:ListItem>
                                                        <asp:ListItem Value="trnjualmstoid">Draft SI</asp:ListItem>
                                                        </asp:DropDownList> 
                                                    <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"/>
                                                    <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"/>                                                     
                                                </asp:Panel> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                                                <asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"/>&nbsp;
                                                <asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" />&nbsp;
                                                <asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="false"/> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3> 
                                                <asp:GridView id="GVNota" runat="server" Width="790px" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbnota" runat="server" Checked='<%#Eval("checkvalue") %>' ToolTip='<%# eval("trnjualmstoid") %>'/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center"/>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="trnjualmstoid" HeaderText="Draft No">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="trnjualno" HeaderText="No. SI">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>          
                                                        
                                                         <asp:BoundField DataField="custname" HeaderText="Customer">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" Wrap="false"></ItemStyle>
                                                        </asp:BoundField>                                               
                                                   </Columns>

                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                    <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"/>
                                                    <PagerSettings Visible="true" />
                                                    <EmptyDataTemplate>&nbsp;</EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="White"/>
                                                </asp:GridView> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD align="center" colSpan="3">
                                                <asp:LinkButton id="lkbAddListNota" runat="server">[ Add To List ]</asp:LinkButton> - 
                                                <asp:LinkButton id="lkbCloseListNota" runat="server">[ Cancel & Close ]</asp:LinkButton>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                            </asp:Panel> 
                                <ajaxToolkit:ModalPopupExtender id="MpeListNota" runat="server" TargetControlID="BtnHiddenListNota" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListNota" BackgroundCssClass="modalBackground">
                                 </ajaxToolkit:ModalPopupExtender> 
                                <asp:Button id="BtnHiddenListNota" runat="server" Visible="False"></asp:Button> 
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--End Pop Up Data SI -->

                            <!--Start Pop Up Data Retur -->    
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelSR" runat="server" Width="600px" CssClass="modalBox" Visible="False">
                                <TABLE style="WIDTH: 100%">
                                    <TBODY>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                <asp:Label id="LabelSR" runat="server" Font-Size="Large" Font-Bold="True" Text="List of Nota SR"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                   <asp:Panel id="Panel3" runat="server" Width="100%" DefaultButton="BtnFindSR">
                                                    <asp:Label id="Label9" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> 
                                                    <asp:DropDownList id="DDLFilterSR" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt">
                                                        <asp:ListItem Value="trnjualreturno">No. SR</asp:ListItem>
                                                        <asp:ListItem Value="trnjualreturmstoid">Draft SR</asp:ListItem>
                                                        </asp:DropDownList> 
                                                    <asp:TextBox id="TxtFilterSR" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"/>
                                                    <asp:ImageButton id="BtnFindSR" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="BtnViewallSR" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"/>                                                     
                                                </asp:Panel> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                                                <asp:ImageButton id="BtnCheckAllSR" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"/>&nbsp;
                                                <asp:ImageButton id="BtnCheckNoneSR" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" />&nbsp;
                                                <asp:ImageButton id="BtnViewCheck" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="false"/> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                            <div style="overflow-y: scroll; width: 100%; height: 250px; background-color: beige">
                                            <asp:GridView id="gvRet" runat="server" Width="98%" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="trnjualreturmstoid,trnjualreturno" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbSr" runat="server" Checked='<%#Eval("checkvalue") %>' ToolTip='<%# eval("trnjualreturmstoid") %>'/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center"/>
                                                        </asp:TemplateField>

                                                        <asp:BoundField DataField="trnjualreturmstoid" HeaderText="Draft No">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="trnjualreturno" HeaderText="No. SR">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>          
                                                        
                                                         <asp:BoundField DataField="custname" HeaderText="Customer">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" Wrap="false"></ItemStyle>
                                                        </asp:BoundField>                                               
                                                   </Columns>

                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                    <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"/><PagerSettings Visible="false" />
                                                    <EmptyDataTemplate>&nbsp;</EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="White"/>
                                                </asp:GridView>
                                            </div>                                                
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD align="center" colSpan="3">
                                                <asp:LinkButton id="LkbAddToListSR" runat="server">[ Add To List ]</asp:LinkButton> - 
                                                <asp:LinkButton id="LkbCloseSR" runat="server">[ Cancel & Close ]</asp:LinkButton>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                            </asp:Panel> 
                                <ajaxToolkit:ModalPopupExtender id="mpeSR" runat="server" TargetControlID="BtnHiddenListSR" PopupDragHandleControlID="LabelSR" PopupControlID="PanelSR" BackgroundCssClass="modalBackground">
                                 </ajaxToolkit:ModalPopupExtender> 
                                <asp:Button id="BtnHiddenListSR" runat="server" Visible="False"></asp:Button> 
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--End Pop Up Data Retur -->
                                                        
                            <!--Start Pop Up Data Katalog -->    
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelItem" runat="server" Width="600px" CssClass="modalBox" Visible="False">
                                <TABLE style="WIDTH: 100%">
                                    <TBODY>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                <asp:Label id="LblItem" runat="server" Font-Size="Large" Font-Bold="True" Text="List of Katalog"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                               <asp:Panel id="Panel4" runat="server" Width="100%" DefaultButton="BtnFindItem">
                                                <asp:Label id="Label10" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> 
                                                <asp:DropDownList id="DDLFilterItem" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt">
                                                    <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                    <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                    </asp:DropDownList> 
                                                <asp:TextBox id="TxtFilterItem" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"/>
                                                <asp:ImageButton id="BtnFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"/> 
                                                <asp:ImageButton id="BtnViewAllItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"/>                                                     
                                                </asp:Panel> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                                                <asp:ImageButton id="BtnCheckAllItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"/>&nbsp;
                                                <asp:ImageButton id="BtnCheckNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" />&nbsp;
                                                <asp:ImageButton id="ImageButton5" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle" Visible="false"/> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                            <div style="overflow-y: scroll; width: 100%; height: 250px; background-color: beige">
                                            <asp:GridView id="gvItem" runat="server" Width="98%" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="itemoid,Itemcode" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbItem" runat="server" Checked='<%#Eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>'/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center"/>
                                                        </asp:TemplateField> 

                                                        <asp:BoundField DataField="Itemcode" HeaderText="Kode">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Itemdesc" HeaderText="Katalog">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="Unit" HeaderText="Unit">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>                                               
                                                   </Columns>

                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                    <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"/><PagerSettings Visible="false" />
                                                    <EmptyDataTemplate>&nbsp;</EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="White"/>
                                                </asp:GridView>
                                            </div> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD align="center" colSpan="3">
                                                <asp:LinkButton id="LkbAddToListItem" runat="server">[ Add To List ]</asp:LinkButton> - 
                                                <asp:LinkButton id="LkbCloseItem" runat="server">[ Cancel & Close ]</asp:LinkButton>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                            </asp:Panel> 
                                <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="BtnHiddenListItem" PopupDragHandleControlID="LblItem" PopupControlID="PanelItem" BackgroundCssClass="modalBackground">
                                 </ajaxToolkit:ModalPopupExtender> 
                                <asp:Button id="BtnHiddenListItem" runat="server" Visible="False"></asp:Button> 
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--End Pop Up Data Katalog -->

                            <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                                        <table>
                                            <tr>
                                                <td colspan="2" style="background-color: #cc0000; text-align: left">
                                                    <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                        Width="24px" />
                                                </td>
                                                <td class="Label" style="text-align: left">
                                                    <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px; text-align: center">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center">
                                                    &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                                        Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                                        TargetControlID="bePopUpMsg">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <span style="display: none">
                                        <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
                                    </span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </th>
        </tr> 
    </table>     
</asp:Content>