<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnTCService.aspx.vb" Inherits="trnTCService" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: TC SERVICE CUSTOMER" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
               <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                 <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Transfer Confirm :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left>Transfer No </TD><TD class="Label" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="FilterText" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w176" MaxLength="30"></asp:TextBox></TD></TR><TR><TD align=left Visible="true"><asp:DropDownList id="DDLfCabang" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w193"><asp:ListItem Value="ToBranch">Penerima</asp:ListItem>
<asp:ListItem Value="FromBranch">Pengirim</asp:ListItem>
</asp:DropDownList></TD><TD id="TD11" align=left Visible="true">:</TD><TD id="TD8" align=left colSpan=4 Visible="true"><asp:DropDownList id="drCabang" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w177"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left><asp:CheckBox id="CbTanggal" runat="server" Text="Tanggal" __designer:wfdid="w178"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap" align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="tgl1" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w179"></asp:TextBox>&nbsp;<asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w180"></asp:ImageButton>&nbsp;- <asp:TextBox id="tgl2" runat="server" Width="83px" CssClass="inpText" __designer:wfdid="w181"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w182"></asp:ImageButton>&nbsp;<asp:Label id="Label3" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w183"></asp:Label>&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD align=left>Status</TD><TD align=left>: </TD><TD align=left colSpan=4><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" __designer:wfdid="w184"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w185">
                                                            </asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 20px" id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w186">
                                                            </asp:ImageButton></TD></TR><TR><TD align=left colSpan=6><asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w187" DefaultButton="btnSearch"><asp:GridView id="gvMaster" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w188" PageSize="8" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="intransferoid" DataNavigateUrlFormatString="trntcservice.aspx?oid={0}" DataTextField="intransferno" HeaderText="Transfer No">
<ControlStyle ForeColor="Red"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" ForeColor="Red"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Username">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FromBranch" HeaderText="From Branch">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ToBranch" HeaderText="To Branch">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<asp:Label id="labeloid" runat="server" Text='<%# Eval("InTransferOid") %>' __designer:wfdid="w3"></asp:Label> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField><ItemTemplate>
&nbsp;<asp:LinkButton id="lbprint" onclick="lbprint_Click" runat="server" __designer:wfdid="w13" ToolTip='<%# eval("InTransferOid") %>'>Print</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" Text="Data Not Found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w189" TargetControlID="tgl1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w190" TargetControlID="tgl2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w191" TargetControlID="tgl1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w192" TargetControlID="tgl2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="gvMaster"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left>Cabang</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="TujuanCb" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w271"></asp:DropDownList> <asp:Label id="OidTC" runat="server" Text="OidTC" __designer:wfdid="w272" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:DropDownList id="FromBranch" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w273" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Transfer No.</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="transferno" runat="server" Width="150px" CssClass="inpTextDisabled" Font-Bold="True" ForeColor="Black" __designer:wfdid="w274" Enabled="False" MaxLength="20"></asp:TextBox>&nbsp;<asp:Label id="i_u" runat="server" ForeColor="Red" Text="new" __designer:wfdid="w275" Visible="False"></asp:Label> </TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:TextBox id="trnstatus" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w276" Visible="False" MaxLength="20">In Process</asp:TextBox></TD></TR><TR><TD class="Label" align=left>Date</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transferdate" runat="server" Width="70px" CssClass="inpTextDisabled" Font-Bold="False" ForeColor="Black" __designer:wfdid="w277" Enabled="False" ValidationGroup="MKE"></asp:TextBox> <asp:ImageButton id="imbTwDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w278"></asp:ImageButton>&nbsp;<asp:Label id="Label17" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w279"></asp:Label>&nbsp; </TD><TD class="Label" align=left>To Branch</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="ToBranch" runat="server" Width="250px" CssClass="inpTextDisabled" AutoPostBack="True" __designer:wfdid="w280" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="labelnotr" runat="server" Width="93px" Text="No. TW Service" __designer:wfdid="w281"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="noref" runat="server" Width="152px" CssClass="inpText" __designer:wfdid="w282" MaxLength="200"></asp:TextBox>&nbsp;<asp:ImageButton id="ibposearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w283"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibpoerase" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w284"></asp:ImageButton> <asp:Label id="pooid" runat="server" __designer:wfdid="w285" Visible="False"></asp:Label> <asp:Label id="FromLoc" runat="server" __designer:wfdid="w286" Visible="False"></asp:Label> </TD><TD class="Label" align=left><asp:Label id="ToLoc" runat="server" Width="69px" __designer:wfdid="w287">To Location</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="tolocation" runat="server" Width="250px" CssClass="inpTextDisabled" AutoPostBack="True" __designer:wfdid="w288" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceTwDate" runat="server" __designer:wfdid="w289" PopupButtonID="imbTwDate" Format="dd/MM/yyyy" TargetControlID="transferdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="TwDate" runat="server" __designer:wfdid="w290" TargetControlID="transferdate" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVpo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w291" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="ToMtrlocOid,toMtrBranch,fromMtrBranch,FromMtrlocoid,trfmtrnote" PageSize="8" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnTrfToReturNo" HeaderText="Transfer No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trntrfdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Transfer Date">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="note" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w292" MaxLength="200"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="labelfromloc" runat="server" Width="86px" Text="From Location" __designer:wfdid="w293" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="fromlocation" runat="server" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w294" Visible="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label18" runat="server" Font-Bold="True" Text="Item Detail :" __designer:wfdid="w295" Font-Underline="True"></asp:Label><asp:Label id="I_u2" runat="server" CssClass="Important" Text="new" __designer:wfdid="w296" Visible="False"></asp:Label> <asp:RadioButtonList id="rblflag" runat="server" AutoPostBack="True" __designer:wfdid="w297" Visible="False" RepeatDirection="Horizontal"><asp:ListItem Selected="True" Value="Purchase">Purchase Retur</asp:ListItem>
<asp:ListItem Value="Supplier">Ambil Barang di WH Supplier</asp:ListItem>
</asp:RadioButtonList> <ajaxToolkit:FilteredTextBoxExtender id="ftbeqty" runat="server" __designer:wfdid="w312" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender> <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w135" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Item" __designer:wfdid="w305"></asp:Label> <asp:Label id="Label26" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w306"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="item" runat="server" Width="354px" CssClass="inpText" Font-Bold="False" ForeColor="Black" __designer:wfdid="w307"></asp:TextBox>&nbsp;<asp:ImageButton id="ibitemsearch" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="16px" __designer:wfdid="w308"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibitemdel" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="16px" __designer:wfdid="w309"></asp:ImageButton> <asp:Label id="labelitemoid" runat="server" __designer:wfdid="w310" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 8px" class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView style="Z-INDEX: 100; LEFT: -8px; POSITION: static; TOP: -3px; BACKGROUND-COLOR: transparent" id="GVItemList" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w1" Visible="False" EmptyDataRowStyle-ForeColor="Red" DataKeyNames="itemoid,sisaretur,unitoid,unit,itemdesc,trfwhserviceoid,trfwhservicedtloid,merk,reqdtloid,reqoid" PageSize="8" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Desc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisa" DataFormatString="{0:#,##0.00}" HeaderText="Stock Gudang" Visible="False">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit3" HeaderText="Unit Gudang" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sisaretur" DataFormatString="{0:#,##0.00}" HeaderText="Sisa Confirm">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trfwhserviceoid" HeaderText="trfmtrmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqoid" HeaderText="reqoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="reqdtloid" HeaderText="reqdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label19" runat="server" CssClass="Important" Text="Data Not Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="labeltempsisaretur" runat="server" __designer:wfdid="w318" Visible="False"></asp:Label><asp:Label id="labelsatuan" runat="server" __designer:wfdid="w322" Visible="False"></asp:Label> <asp:Label id="trfmtrmstoid" runat="server" __designer:wfdid="w323" Visible="False"></asp:Label>&nbsp;<asp:Label id="trfwhservicedtloid" runat="server" __designer:wfdid="w324" Visible="False"></asp:Label>&nbsp; </TD></TR><TR><TD class="Label" align=left>Merk</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="merk" runat="server" Width="251px" CssClass="inpTextDisabled" __designer:wfdid="w325" Enabled="False" MaxLength="200"></asp:TextBox> <asp:Label id="ReqDtlOid" runat="server" __designer:wfdid="w1" Visible="False"></asp:Label> <asp:Label id="ReqOid" runat="server" __designer:wfdid="w2" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left>Qty <asp:Label id="Label28" runat="server" Width="1px" Font-Size="Medium" Font-Names="Arial,Helvetica,sans-serif" ForeColor="Red" Text="*" __designer:wfdid="w326"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox style="TEXT-ALIGN: justify" id="qty" runat="server" Width="80px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w327" MaxLength="10" ValidationGroup="MKE"></asp:TextBox>&nbsp; <asp:Label id="Label30" runat="server" CssClass="label" Text="Max :" __designer:wfdid="w328"></asp:Label>&nbsp;&nbsp;<asp:Label id="labelmaxqty" runat="server" CssClass="label" ForeColor="Red" __designer:wfdid="w329">0.00</asp:Label>&nbsp;<asp:DropDownList id="unit" runat="server" Width="95px" CssClass="inpText" AutoPostBack="True" __designer:wfdid="w330"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="notedtl" runat="server" Width="310px" CssClass="inpText" __designer:wfdid="w331" MaxLength="200"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="labelseq" runat="server" __designer:wfdid="w332" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w333"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w334"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" class="Label" align=left colSpan=6><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 184px; BACKGROUND-COLOR: beige"><asp:GridView id="GVItemDetail" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w2" DataKeyNames="itemoid,satuan,sisaretur,trfwhserviceoid,trfwhservicedtloid,trfqty" GridLines="None" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelete" runat="server" OnClick="lbdelete_Click"><img src="../Images/del.jpeg" style="border-style: none;" /></asp:LinkButton>
</ItemTemplate>

<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trfwhserviceoid" HeaderText="trfmtrmstoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trfqty" HeaderText="trfqty" Visible="False"></asp:BoundField>
<asp:BoundField DataField="trfwhservicedtloid" HeaderText="trfwhservicedtloid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="ReqDtlOid" HeaderText="ReqDtlOid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="ReqOid" HeaderText="ReqOid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" Font-Size="Small" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="Blue" BorderWidth="1px" BorderStyle="Solid" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD class="Label" align=left colSpan=6>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w336"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w337"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w338"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w339"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w340" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w341" Visible="False"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="1688849860263976" __designer:wfdid="w342" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="1688849860263977">
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div3" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w343"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Transfer Confirm Service :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg"  runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender>
<span style="display:none"><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button></span>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

