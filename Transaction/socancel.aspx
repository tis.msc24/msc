﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="socancel.aspx.vb" Inherits="Transaction_socancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">


    <table class="tabelhias" width="100%">
        <tr>
            <th class="header">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text=".: SO Cancellation"></asp:Label></th>
        </tr>
    </table>

    <div style="text-align: left;">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
            Font-Size="9pt">
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                <ContentTemplate>
                    <asp:UpdatePanel ID="updPanel1" runat="server">
                        <ContentTemplate>
<TABLE id="TABLE1" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD style="WIDTH: 137px">Cabang</TD><TD><asp:DropDownList id="fddlCabang" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w315"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 137px">Filter</TD><TD><asp:DropDownList id="ddlFilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w316" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" AutoPostBack="True">
                                                <asp:ListItem Value="orderno">SO No</asp:ListItem>
                                            </asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w317" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 137px"><asp:CheckBox id="CBperiod" runat="server" Text="Periode" __designer:wfdid="w331" Checked="True"></asp:CheckBox></TD><TD><asp:TextBox id="tglAwal" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w318"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w319"></asp:ImageButton> to&nbsp; <asp:TextBox id="tglAkhir" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w320"></asp:TextBox>&nbsp; <asp:ImageButton id="btnTglAkhir" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w321"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w322"></asp:Label></TD></TR><TR><TD style="WIDTH: 137px"></TD><TD vAlign=bottom height=30><asp:ImageButton id="BtnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w323"></asp:ImageButton> <asp:ImageButton id="BtnViewALL" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w324"></asp:ImageButton></TD></TR><TR><TD colSpan=2><asp:GridView id="gvCancelBPM" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w330" OnPageIndexChanging="gvCancelBPM_PageIndexChanging" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderdatecancel" HeaderText="Date Canceled">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderusercancel" HeaderText="User Canceled">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ordernotecancel" HeaderText="Note Canceled">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="orderstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                        <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <ajaxToolkit:CalendarExtender id="ceAwal" runat="server" TargetControlID="tglAwal" PopupButtonID="btnTglAwal" Format="dd/MM/yyyy" __designer:wfdid="w326">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ceAkhir" runat="server" TargetControlID="tglAkhir" PopupButtonID="btnTglAkhir" Format="dd/MM/yyyy" __designer:wfdid="w327">
                                            </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="tglAwal" __designer:wfdid="w328" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="tglAkhir" __designer:wfdid="w329" UserDateFormat="DayMonthYear" CultureName="id-ID" MaskType="Date" Mask="99/99/9999">
                                            </ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <img align="absMiddle" alt="" src="../Images/corner.gif" />
                    <strong><span style="font-size: 9pt">:: List of Cancellation SO </span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 2px; HEIGHT: 15px">Cabang</TD><TD style="HEIGHT: 15px" colSpan=2><asp:DropDownList id="ddlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w281"></asp:DropDownList></TD><TD style="HEIGHT: 15px" colSpan=1><asp:Label id="ordermstoid" runat="server" __designer:wfdid="w282" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" colSpan=1></TD></TR><TR><TD style="WIDTH: 2px">SO&nbsp;No.</TD><TD colSpan=2><asp:TextBox id="txtorderNo" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w283"></asp:TextBox>&nbsp;<asp:ImageButton id="imbSearchBPM" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w284"></asp:ImageButton>&nbsp;<asp:ImageButton style="HEIGHT: 14px" id="imbEraseBPM" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w285"></asp:ImageButton>&nbsp; </TD><TD colSpan=1>Customer</TD><TD colSpan=1><asp:TextBox id="custname" runat="server" Width="220px" CssClass="inpTextDisabled" __designer:wfdid="w286" Enabled="False"></asp:TextBox> </TD></TR><TR><TD style="WIDTH: 2px; HEIGHT: 15px"><asp:Label id="Label3" runat="server" Text="Note" __designer:wfdid="w287"></asp:Label><asp:Label style="COLOR: #cc0000" id="Label6" runat="server" Text="*" __designer:wfdid="w288"></asp:Label></TD><TD colSpan=2><asp:TextBox id="txtorderNote" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w289" MaxLength="200"></asp:TextBox><asp:Label id="custoid" runat="server" __designer:wfdid="w290" Visible="False"></asp:Label></TD><TD colSpan=1><asp:Label id="lbl_creditlimit" runat="server" Text="Sisa Credit Limit" __designer:wfdid="w291" Visible="False"></asp:Label> </TD><TD style="HEIGHT: 15px" colSpan=1><asp:TextBox id="netto" runat="server" Width="131px" CssClass="inpTextDisabled" __designer:wfdid="w292" Visible="False" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 2px"></TD><TD colSpan=4><asp:GridView id="gvBPM2" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w293" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" Font-Underline="False" DataKeyNames="orderoid,orderno,custoid,custname" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="orderno" HeaderText="SO No.">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="deliverydate" HeaderText="Delivery Date">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD><asp:Label id="lbl_item" runat="server" Width="66px" __designer:wfdid="w294" Visible="False">Item List</asp:Label></TD><TD colSpan=4><asp:GridView id="gv_item" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w277" GridLines="None" EmptyDataText="No data in database." CellPadding="4" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" Font-Underline="False" DataKeyNames="itemcode,qty,unit">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>

                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="WIDTH: 2px"></TD><TD colSpan=4><asp:Button id="btnCloseBPM" onclick="btnCloseBPM_Click" runat="server" CssClass="btn red" Text="Cancellation SO" __designer:wfdid="w296"></asp:Button> <asp:Button id="btncancel" runat="server" CssClass="btn gray" Text="Cancel" __designer:wfdid="w297"></asp:Button></TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
                <HeaderTemplate>
                    <strong><span style="font-size: 9pt">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" />
                        :: Form Cancellation SO </span></strong>
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer><br />
        <asp:UpdatePanel ID="updPanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelValidasi" runat="server" CssClass="modalMsgBox" __designer:wfdid="w120" Visible="False">
                    <table>
                        <tbody>
                            <tr>
                                <td style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colspan="2">
                                    <asp:Label ID="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header" __designer:wfdid="w121"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w122"></asp:Image></td>
                                <td style="TEXT-ALIGN: left" class="Label">
                                    <asp:Label ID="isiPesan" runat="server" ForeColor="Red" __designer:wfdid="w123"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="HEIGHT: 3px; TEXT-ALIGN: center" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="TEXT-ALIGN: center" colspan="2">
                                    <asp:Button ID="btnErrOK" OnClick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " __designer:wfdid="w124" UseSubmitBehavior="False"></asp:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
                &nbsp;
                <ajaxToolkit:ModalPopupExtender ID="mpeMsg" runat="server" TargetControlID="PanelValidasi" __designer:wfdid="w125" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender>
                <asp:Button ID="btnExtenderValidasi" runat="server" __designer:wfdid="w126" Visible="False"></asp:Button>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

