<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTranformItem.aspx.vb" Inherits="ReportForm_rptTItem"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" width="100%">
        <tr>
            <td colspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan Transform Item Price" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY><TR><TD id="TD5" align=left runat="server">Cabang</TD><TD align=right runat="server">:</TD><TD id="TD6" align=left runat="server"><asp:DropDownList id="DDLbranch" runat="server" CssClass="inpText" __designer:wfdid="w1" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left>Periode</TD><TD align=right>:</TD><TD align=left><asp:TextBox id="range1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w1"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w2"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD align=left>Type Tramsform</TD><TD align=right>:</TD><TD align=left><asp:DropDownList id="TypeTransDll" runat="server" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>Create</asp:ListItem>
<asp:ListItem>Release</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Transform No</TD><TD align=right>:</TD><TD align=left><asp:TextBox id="txtTransNo" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w11"></asp:TextBox>&nbsp;<asp:ImageButton id="SearchTransNo" onclick="SearchTransNo_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w12"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseTransNo" onclick="EraseTransNo_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w13"></asp:ImageButton>&nbsp;<asp:Label id="lblTransOid" runat="server" __designer:wfdid="w22" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=right></TD><TD align=left><asp:GridView id="TransItemNo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w10" OnPageIndexChanging="TransItemNo_PageIndexChanging" OnSelectedIndexChanged="TransItemNo_SelectedIndexChanged" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="transformoid,transformno,Transdate" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="transformno" HeaderText="Transform No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Transdate" HeaderText="Transform Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transformtype" HeaderText="Type">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Gudang Bertambah</TD><TD align=right>:</TD><TD align=left><asp:DropDownList id="ddllocawal" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w2" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left>Gudang Berkurang</TD><TD align=right>:</TD><TD align=left><asp:DropDownList id="ddllocakhir" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w3">
    </asp:DropDownList> </TD></TR><TR><TD align=left>Item Name</TD><TD align=right>:</TD><TD align=left><asp:TextBox id="barang" runat="server" Width="169px" CssClass="inpText" __designer:wfdid="w19"></asp:TextBox> <asp:ImageButton id="barangsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w20"></asp:ImageButton> &nbsp;<asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w21"></asp:ImageButton><asp:Label id="barangoid" runat="server" __designer:wfdid="w22" Visible="False"></asp:Label> </TD></TR><TR><TD align=left></TD><TD align=right></TD><TD align=left><asp:GridView id="gvMat" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w10" OnPageIndexChanging="gvMat_PageIndexChanging" OnSelectedIndexChanged="gvMat_SelectedIndexChanged" PageSize="8" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="matcode,matlongdesc" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="matcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Description">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" ForeColor="Red" Text="No Data Found" __designer:wfdid="w17"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD id="TD4" align=left runat="server" Visible="false">Merk</TD><TD align=right runat="server" Visible="false">:</TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="merk" runat="server" Width="169px" CssClass="inpTextDisabled" __designer:wfdid="w24" Enabled="False"></asp:TextBox> </TD></TR><TR><TD id="TD2" align=left runat="server" Visible="false">Tipe Transformasi</TD><TD align=right runat="server" Visible="false">:</TD><TD id="TD1" align=left runat="server" Visible="false"><asp:DropDownList id="ddltype" runat="server" Width="110px" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem>Create</asp:ListItem>
<asp:ListItem>Release</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=3 runat="server"><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w40">
            <progresstemplate>
                <strong><span style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait ....</span></strong>
                <BR />
                <asp:Image ID="Image4" runat="server" __designer:wfdid="w41" ImageAlign="AbsBottom" ImageUrl="~/Images/loadingbar.gif" />
            </progresstemplate>
        </asp:UpdateProgress></TD></TR><TR><TD align=center colSpan=3><ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w201" TargetControlID="range1" PopupButtonID="ImageButton1" Format="dd/MM/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'."></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w202" TargetControlID="range2" PopupButtonID="ImageButton2" Format="dd/MM/yyyy" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'."></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w203" TargetControlID="range1" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w204" TargetControlID="range2" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." MaskType="Date" Mask="99/99/9999" ErrorTooltipEnabled="True" CultureName="id-ID">
        </ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w42"></asp:Label> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w1" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

