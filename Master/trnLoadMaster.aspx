﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnLoadMaster.aspx.vb" Inherits="trnLoadMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">

    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Upload Data SO Offline" CssClass="Title" ForeColor="Navy" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
        </tr>
        <tr>
            <th align="left" colspan="2" style="background-color: transparent" valign="center">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 90px" class="Label" align=left>Cabang</TD><TD class="Label" align=left><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w86" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 90px" class="Label" align=left><asp:Label id="Label2" runat="server" Width="80px" Text="SO Header" __designer:wfdid="w87"></asp:Label></TD><TD class="Label" align=left><asp:FileUpload id="FileSOHeader" runat="server" CssClass="inpText" __designer:wfdid="w88"></asp:FileUpload>&nbsp;&nbsp;</TD></TR><TR><TD class="Label" align=left colSpan=2><asp:Button id="BtnUpload" runat="server" CssClass="btn green" Font-Bold="True" ForeColor="White" Text="Upload Data SO" __designer:wfdid="w91"></asp:Button>&nbsp;<asp:Button id="BtnBatal" runat="server" CssClass="btn gray" Font-Bold="True" ForeColor="White" Text="Cancel" __designer:wfdid="w92"></asp:Button></TD></TR><TR style="COLOR: #000099"><TD class="Label" align=center colSpan=2><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w93" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w94"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE>&nbsp; 
</contenttemplate>
                                                    <triggers>
<asp:PostBackTrigger ControlID="BtnUpload"></asp:PostBackTrigger>
</triggers>
                                                </asp:UpdatePanel></th>
        </tr>
    </table>
    <%-- <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>--%>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="upPopUpMsg" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" TargetControlID="bePopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

