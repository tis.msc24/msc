'Prgmr : zipi ; Update : zipi on 15.03.13
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class OpenPO
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Private cKon As New Koneksi
    Private cProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub BindData(ByVal fCompCode As String)
        Try
            Dim sWhere As String = ""
            If FilterText.Text <> "" Then
                If ddlFilter.SelectedValue = "trnbelipono" Then
                    sWhere &= "AND (pom.trnbelipono LIKE '%" & Tchar(FilterText.Text) & "%') "
                ElseIf ddlFilter.SelectedValue = "suppname" Then
                    sWhere &= "AND (sup.suppname LIKE '%" & Tchar(FilterText.Text) & "%') "
                End If
            End If

            sSql = "Select openoid,pomstoid,pom.trnbelipodate,pom.trnbelipono,suppname,op.crttime,op.statusnya From QL_mstopenpo op INNER JOIN QL_pomst pom ON pom.trnbelimstoid=op.pomstoid INNER JOIN QL_mstsupp sup ON sup.suppoid=pom.trnsuppoid " & fCompCode & " " & sWhere & ""
            Session("QL_mstopenpo") = cKon.ambiltabel(sSql, "QL_mstopenpo")
            tbldata.DataSource = Session("QL_mstopenpo")
            tbldata.DataBind() : tbldata.SelectedIndex = -1

        Catch ex As Exception
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub InitCBL()
        sSql = "SELECT gencode,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' and gengroup='CABANG' ORDER BY gencode ASC"
        cbCabang.Items.Clear() 
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbCabang.Items.Add(xreader.GetValue(1).ToString)
            cbCabang.Items(cbCabang.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Private Sub FillTextBox(ByVal OidOp As Integer)
        Try
            sSql = "Select openoid,pomstoid,pom.trnbelipodate,pom.trnbelipono,suppname,op.statusnya,op.tocabang,op.upduser,op.updtime From QL_mstopenpo op INNER JOIN QL_pomst pom ON pom.trnbelimstoid=op.pomstoid INNER JOIN QL_mstsupp sup ON sup.suppoid=pom.trnsuppoid Where op.openoid=" & OidOp & ""
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                iOid.Text = xreader("pomstoid")
                Nopo.Text = Trim(xreader("trnbelipono"))

                Dim sValue() As String = xreader("tocabang").ToString.Split(",")
                If sValue.Length > 0 Then
                    If cbCabang.Items.Count > 0 Then
                        For C1 As Integer = 0 To sValue.Length - 1
                            Dim li As ListItem = cbCabang.Items.FindByValue(sValue(C1).Trim)
                            Dim i As Integer = cbCabang.Items.IndexOf(li)
                            If i >= 0 Then
                                cbCabang.Items(cbCabang.Items.IndexOf(li)).Selected = True
                            End If
                        Next
                    End If
                End If
                StatusNya.Text = Trim(xreader("statusnya"))
                Upduser.Text = Trim(xreader("upduser"))
                Updtime.Text = Format(CDate(Trim(xreader("updtime").ToString)), "MM/dd/yyyy HH:mm:ss")
                If StatusNya.Text = "POST" Then
                    BtnDel.Visible = False : BtnPost.Visible = False : btnsavemstr.Visible = False
                Else
                    BtnDel.Visible = True : BtnPost.Visible = True
                End If
            End While
            conn.Close()
        Catch ex As Exception
            conn.Close()
            showMessage("Maaf, Program Error Silahkan hubungi staff programmer..!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Public Sub binddataSupp(ByVal sqlPlus As String)
        Try
            sSql = "SELECT po.trnbelimstoid pomstoid, po.trnbelipono pono, po.trnbelipodate podate, po.trnsuppoid suppoid, s.suppname, ISNULL((Select gendesc FROM QL_mstgen tc Where tc.gencode=po.tocabang AND tc.gengroup='CABANG'),'') dCabang, ISNULL(po.tocabang,'') toCabang From ql_pomst po inner join ql_mstsupp s on po.trnsuppoid=s.suppoid and po.cmpcode=s.cmpcode and po.trnbelistatus='Approved' Where po.trnbelimstoid in (Select dt.trnbelimstoid from (SELECT D.trnbelimstoid, D.trnbelidtlqty - ISNULL((select sum(s.qty) from QL_trnsjbelidtl s where s.trnbelidtloid=d.trnbelidtloid),0) QTY FROM QL_podtl D) DT WHERE DT.QTY>0) AND po.TRNBELItype = 'GROSIR' AND (po.trnbelipono LIKE '%" & Tchar(Nopo.Text) & "%' OR s.suppname LIKE '%" & Tchar(Nopo.Text) & "%') And po.trnbelimstoid NOT IN (Select pomstoid from QL_mstopenpo) ORDER BY po.trnbelipono desc"
            Session("QL_mstsupp") = cKon.ambiltabel(sSql, "QL_mstsupp")
            gvSupplier.DataSource = Session("QL_mstsupp")
            gvSupplier.DataBind()
            gvSupplier.SelectedIndex = -1
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\master\frmOpenPO.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Open Security PO "
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

        Session("oid") = Request.QueryString("oid")

        BtnPost.Attributes.Add("OnClick", "javascript:return confirm('Data akan diposting..!!');")

        If Not IsPostBack Then
            FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
            BindData("Where pom.cmpcode='MSC'") : InitCBL()

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        Dim objTable As New DataTable : objTable = Session("TblDtl")
        'fillTot() ': ReAmount()
    End Sub

    Protected Sub TblData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles tbldata.PageIndexChanging
        tbldata.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sWhere As String = ""
        If chkPeriod.Checked Then
            Try
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 > dat2 Then
                    showMessage("Start Date must be less than End Date!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Invalid date format!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End Try
        End If
        sWhere = "Where pom.cmpcode='MSC'"

        If chkPeriod.Checked Then
            If FilterPeriod1.Text = "" And FilterPeriod2.Text = "" Then
                showMessage("Please fill periode!!", CompnyName & "  - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            Else
                sWhere &= "AND pom.trnbelipodate BETWEEN '" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND '" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            End If
        End If

        Session("FilterText") = Tchar(FilterText.Text)
        Session("FilterDDL") = ddlFilter.SelectedIndex
        BindData(sWhere)
    End Sub

    Protected Sub btnsavemstr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavemstr.Click
        If IOID.Text.Trim = "" Then
            showMessage("Maaf, Pilih nomer PO dulu..!!", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim toCabang As String = ""
        For C1 As Integer = 0 To cbCabang.Items.Count - 1
            If cbCabang.Items(C1).Value <> "All" And cbCabang.Items(C1).Selected = True Then
                toCabang &= cbCabang.Items(C1).Value & ", "
            End If
        Next

        If toCabang <> "" Then
            toCabang = Left(toCabang, toCabang.Length - 2)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                ' insert table master
                Session("openoid") = GenerateID("QL_mstopenpo", CompnyCode)
                sSql = "INSERT INTO [QL_mstopenpo]([cmpcode],[openoid],[pomstoid],[tocabang],[crtuser],[crttime],[upduser],[updtime],statusnya) VALUES " & _
                "('MSC'," & Session("openoid") & "," & iOid.Text & ",'" & toCabang & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & StatusNya.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & Session("openoid") & " Where tablename = 'QL_mstopenpo' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE [QL_MSC].[dbo].[QL_mstopenpo]" & _
                " SET [pomstoid] = " & iOid.Text & ",[tocabang] = '" & toCabang & "',[upduser] = '" & Session("UserID") & "',[updtime] = CURRENT_TIMESTAMP,[statusnya] = '" & StatusNya.Text & "'" & _
                " Where openoid=" & Session("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : StatusNya.Text = "In Process"
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            xCmd.Connection.Close() : Exit Sub
        End Try
        Response.Redirect("~\master\frmOpenPO.aspx?awal=true")
    End Sub

    Protected Sub btnCancelMstr1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelMstr.Click
        Session("oid") = Nothing
        Session("TblDtl") = Nothing
        Response.Redirect("~\master\frmOpenPO.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub tbldata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles tbldata.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy") 
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sWhere As String = ""
        ddlFilter.SelectedIndex = 0 : FilterText.Text = ""
        chkStatus.Checked = False

        FilterPeriod1.Text = Format(GetServerTime, "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        sWhere = "Where pom.cmpcode='MSC'"

        Session("FilterText") = Nothing
        Session("FilterDDL") = Nothing
        BindData(sWhere)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvSupplier.Visible = True
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.Visible = True
        binddataSupp("")
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        'ButtonSuplier.Visible = False : PanelSuplier.Visible = False : ModalPopupExtender1.Hide()
        IOID.Text = gvSupplier.SelectedDataKey("pomstoid")
        Nopo.Text = gvSupplier.SelectedDataKey("pono").ToString().Trim
         cProc.DisposeGridView(sender)
        gvSupplier.Visible = False 
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lblMessage.Text = "Data telah diClose !" Then
            Response.Redirect("~\master\frmOpenPO.aspx?awal=true")
        Else
            PanelMsgBox.Visible = False
            beMsgBox.Visible = False
            mpeMsgbox.Hide() 
        End If
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        iOid.Text = "" : Nopo.Text = ""
        osPO.Text = ""  
    End Sub
#End Region

    Protected Sub BtnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnPost.Click
        StatusNya.Text = "POST" : btnsavemstr_Click(sender, e)
    End Sub

    Protected Sub BtnDel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDel.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Then
                showMessage("Maaf, Apakah and sudah pernah input data sebelumnya?!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            Else
                sSql = "Delete QL_mstopenpo Where openoid=" & Session("oid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : StatusNya.Text = "In Process"
            showMessage("Maaf, Program Error Silahkan hubungi staf programmer..!!", CompnyName & "  - ERROR", 1, "modalMsgBox")
            xCmd.Connection.Close() : Exit Sub
        End Try
        Response.Redirect("~\master\frmOpenPO.aspx?awal=true")
    End Sub
End Class

