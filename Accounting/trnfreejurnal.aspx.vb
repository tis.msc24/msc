'Created By Muji On 30 September 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnfreejurnal
    Inherits System.Web.UI.Page

#Region "Variable"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim sql1 As String = ""
    Dim cProc As New ClassProcedure
    Private objConnExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(glDate.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Pre-Paid Date 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(periode1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(periode2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(toDate(periode1.Text)) > CDate(toDate(periode2.Text)) Then
            showMessage("Period 1 Can't > Period 2", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub InitDDL()
        Dim sSql As String = "Select gencode,gendesc from ql_mstgen Where gengroup = 'cabang' order by gencode asc"
        FillDDL(outlet, sSql)
        If Session("branch_id") <> "01" Then
            outlet.SelectedValue = Session("branch_id")
            outlet.Enabled = False
            outlet.CssClass = "inpTextDisabled"
        End If

        ' Fill DDL Account Group
        sSql = "SELECT DISTINCT acctggrp1 a, acctggrp1 b FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' ORDER BY a"
        If FillDDL(acctggroup, sSql) Then
            InitDDLCOA()
        End If
    End Sub

    Private Sub InitDDLCOA()
        ' Fill DDL Account
        sSql = "SELECT a.acctgoid, ('[' + a.acctgcode + '] ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.acctggrp1='" & acctggroup.SelectedValue & "' AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
        FillDDL(acctgoid, sSql)
    End Sub

    Public Sub filltextbox(ByVal sCmpcode As String, ByVal vjurnaloid As String)
        Dim xdt As New DataTable
        xdt = GetDataTable("SELECT 0 AS selected, ms.cmpcode,(select gendesc from ql_mstgen where gencode=ms.branch_code and gengroup='CABANG') outlet,ms.type, ms.glmstoid,ms.gldate,ms.periodacctg,ms.glnote,ms.glflag,ms.createuser,ms.createtime,ms.upduser,ms.updtime,(SELECT TOP 1 d.noref FROM QL_trngldtl d WHERE d.cmpcode=ms.cmpcode AND d.glmstoid=ms.glmstoid) AS noref, ms.postdate FROM QL_trnglmst ms INNER JOIN QL_MSTPROF p ON p.BRANCH_CODE=ms.branch_code AND p.USERID='" & Session("UserID") & "' WHERE ms.type='JURNAL' AND (SELECT TOP 1 d.noref FROM QL_trngldtl d WHERE ms.glmstoid='" & vjurnaloid & "' AND d.glmstoid=ms.glmstoid) LIKE '%" & FilterText.Text & "%' AND ms.glflag <> 'DELETE'", "dsGlMst")
        Dim xRow() As DataRow
        xRow = xdt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Try
            If xRow.Length > 0 Then
                ' -- set data header G/L
                txtGLID.Text = xRow(0)("glmstoid")
                glDate.Text = Format(xRow(0)("gldate"), "dd/MM/yyyy")
                'txtPeriod.Text = Trim(xRow(0)("periodacctg").ToString)
                txtNote.Text = Trim(xRow(0)("glnote").ToString)
                txtStatus.Text = Trim(xRow(0)("glflag").ToString)
                outlet.SelectedValue = Trim(xRow(0)("cmpcode").ToString)

                If txtStatus.Text = "POST" Then
                    DelBtn.Visible = False
                    SaveBtn.Visible = False : PostBtn.Visible = False
                    'txtPostDate.Text = Trim(xRow(0)("postdate").ToString)
                Else
                    PostBtn.Visible = True : DelBtn.Visible = True
                    'btnSave.Visible = True : txtPostDate.Text = "1/1/1900"
                End If
                noref.Text = Trim(xRow(0)("noref").ToString)
                created.Text = " Create By <B>" & xRow(0)("createuser").ToString & "</B> On <B>" & xRow(0)("createtime").ToString & "</B> "
                update.Text = " Last Updated By <B>" & xRow(0)("upduser").ToString & "</B> On <B>" & xRow(0)("updtime").ToString & "</B> "
                sSql = "SELECT glseq, acctggrp1 AS acctggroup, gld.acctgoid, acctgcode, acctgdesc, gldbcr, (CASE gldbcr WHEN 'D' THEN glamt ELSE 0.0 END) AS glamtdb, (CASE gldbcr WHEN 'D' THEN 0.0 ELSE glamt END) AS glamtcr, glnote FROM QL_trngldtl gld INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE glmstoid='" & vjurnaloid & "' ORDER BY glseq"

                xdt = GetDataTable(sSql, "dsGlDtl")

                Session("TblDtl") = xdt
                gvbaru.DataSource = xdt
                glseq.Text = xdt.Rows.Count + 1
                gvbaru.DataBind()

                ' -- set data debet credit nya
                Session("ssDb") = GetScalar("Select isnull(sum(glamt),0) from QL_trngldtl WHERE glmstoid = '" & vjurnaloid & "' and gldbcr = 'D' AND cmpcode='" & sCmpcode & "'")
                Session("ssCr") = GetScalar("Select isnull(sum(glamt),0) from QL_trngldtl WHERE glmstoid = '" & vjurnaloid & "' and gldbcr = 'C' AND cmpcode='" & sCmpcode & "'")
                txtDebet.Text = ToMaskEdit(ToDouble(Session("ssDb")), 1)
                txtCredit.Text = ToMaskEdit(ToDouble(Session("ssCr")), 1)
                'txtAllBalance.Text = ToMaskEdit(Session("ssDb") - Session("ssCr"), 2)
                TabContainer1.ActiveTabIndex = 1
            End If
        Catch ex As Exception
            objConn.Close() : showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Public Sub BindData()
        'CheckPeriod()
        Try
            Dim tgle As Date = toDate(periode1.Text)
            tgle = toDate(periode1.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(toDate(periode1.Text)) > CDate(toDate(periode2.Text)) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If
        sSql = "SELECT 0 AS selected, ms.cmpcode,(select gendesc from QL_mstgen where gencode=ms.branch_code and gengroup = 'CABANG') outlet,ms.type, ms.glmstoid,ms.gldate,ms.periodacctg,ms.glnote,ms.glflag,(SELECT TOP 1 d.noref FROM QL_trngldtl d WHERE d.cmpcode=ms.cmpcode AND d.glmstoid=ms.glmstoid) AS noref FROM QL_trnglmst ms INNER JOIN QL_MSTPROF p ON p.BRANCH_CODE=ms.branch_code AND p.USERID ='" & Session("UserID") & "' WHERE ms.type='JURNAL' AND ms.glflag NOT IN ('DELETE') AND ms.cmpcode='" & CompnyCode & "'"

        If cbPeriode.Checked Then
            sSql &= "AND ms.gldate BETWEEN '" & CDate(toDate(periode1.Text)) & "' AND '" & CDate(toDate(periode2.Text)) & "' AND glflag NOT IN ('DELETE') "
        End If
        If cbStatus.Checked Then
            sSql &= IIf(FilterDDLStatus.SelectedValue = "ALL", "", " AND ms.glflag = '" & FilterDDLStatus.SelectedValue & "'")
        End If
        sSql &= "AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%' "
        sSql &= "AND ms.glmstoid>0 ORDER BY ms.gldate DESC"

        Dim objTable As DataTable = GetDataTable(sSql, "QL_trnglmst")
        Session("tbldata") = objTable
        GvJurnal.DataSource = objTable
        GvJurnal.DataBind()
        UnabledCheckBox()
    End Sub

    Private Sub generateJurnalmstoid()
        'If (Session("oid") = Nothing Or Session("oid") = "") And txtStatus.Text <> "POST" Then
        '    txtGLID.Text = GenerateID("QL_trnglmst", CompnyCode)
        '    noref.Text = outlet.SelectedValue & "-" & txtGLID.Text
        'Else
        'If txtStatus.Text = "POST" Then
        'Session("vNoCashBank") = sCBType & "/" & Format(CDate(toDate(cashbankdate.Text)), "yy/MM/dd") & "/"
        'sSql = "SELECT MAX(ABS(replace(cashbankno,'" & Session("vNoCashBank") & "',''))) cashbankno FROM QL_trncashbankmst WHERE cashbankno LIKE '" & Session("vNoCashBank") & "%'"
        'xCmd.CommandText = sSql

        'Session("vNoCashBank") = GenNumberString(Session("vNoCashBank"), "", iCurID, DefCounter)
        Dim cabang As String = GetStrData("select substring (gendesc,1,3) from ql_mstgen Where gencode='" & outlet.SelectedValue & "' AND gengroup='CABANG'")
        Dim sNo As String = "MEMO/" & cabang & "/" & Format(CDate(toDate(glDate.Text)), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(noref,'" & sNo & "',''))),0)+1 AS IDNEW FROM ql_trngldtl WHERE cmpcode='" & CompnyCode & "' AND noref LIKE '" & sNo & "%'"
        noref.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
        'End If
        'End If
    End Sub

    Private Sub genereteJurnaldtloid()
        Session("dtloid") = GenerateID("QL_trngldtl", CompnyCode)
        gldtloid.Text = Session("dtloid")
    End Sub

    Public Sub UnabledCheckBox()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("tbldata")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (GvJurnal.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("glflag").ToString.ToUpper) = "POST" Or Trim(objRow(C1)("glflag").ToString) = "DELETE" Then

                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = GvJurnal.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Sub checkbox()
        If cbPeriode.Checked = True Then
            periode1.ReadOnly = False : periode2.ReadOnly = False
            btnperiode1.Enabled = True : btnperiode2.Enabled = True
        End If
    End Sub

    Sub Cleardtl()
        gldtloid.Text = CInt(Session("dtloid")) + gvbaru.Rows.Count + 1
        gvbaru.SelectedIndex = -1
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        glseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.Sort = "gldbcr DESC, glseq"
                dt = dv.ToTable
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("glseq") = C1 + 1
                Next
                dt.AcceptChanges()
                glseq.Text = dt.Rows.Count + 1
                Session("TblDtl") = dt
                gvbaru.DataSource = Session("TblDtl")
                gvbaru.DataBind()
            End If
        End If
        acctggroup.SelectedIndex = -1
        acctggroup_SelectedIndexChanged(Nothing, Nothing)
        gldbcr.SelectedIndex = -1
        glamt.Text = ""
        glnote.Text = ""
        gvbaru.SelectedIndex = -1
        CountTotalAmt()
    End Sub

    Private Sub CountTotalAmt()
        Dim dDb As Double = 0
        Dim dCr As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dDb += ToDouble(dt.Rows(C1)("glamtdb").ToString)
                dCr += ToDouble(dt.Rows(C1)("glamtcr").ToString)
            Next
        End If
        txtDebet.Text = ToMaskEdit(dDb, 1)
        txtCredit.Text = ToMaskEdit(dCr, 1)
    End Sub

#End Region

#Region "Function"
    Public Function getDebetValue() As String
        If Eval("gldbcr").ToString.Trim.ToUpper = "D" Then
            Return ToMaskEdit(Eval("glamt"), 2)
        Else
            Return ToMaskEdit(0, 2)
        End If
    End Function

    Public Function getCreditValue() As String
        If Eval("gldbcr").ToString.Trim.ToUpper = "C" Then
            Return ToMaskEdit(Eval("glamt"), 2)
        Else
            Return ToMaskEdit(0, 2)
        End If
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GvJurnal.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Private Function getNewDT(ByVal xdt As DataTable) As DataTable
        Dim dt As New DataTable : dt = setTblDtl()
        For i As Integer = 0 To xdt.Rows.Count - 1
            Dim xrow As DataRow
            xrow = dt.NewRow
            xrow("acctgoid") = xdt.Rows(i).Item("acctgoid")
            xrow("acctgcode") = xdt.Rows(i).Item("acctgcode")
            xrow("acctgdesc") = xdt.Rows(i).Item("acctgdesc")
            xrow("gldbcr") = xdt.Rows(i).Item("gldbcr")
            xrow("glamt") = xdt.Rows(i).Item("glamt")
            xrow("glnote") = xdt.Rows(i).Item("glnote")
            xrow("cmpcode") = xdt.Rows(i).Item("cmpcode")
            xrow("gldtloid") = xdt.Rows(i).Item("gldtloid")
            xrow("glmstoid") = xdt.Rows(i).Item("glmstoid")
            xrow("glseq") = xdt.Rows(i).Item("glseq")
            xrow("noref") = xdt.Rows(i).Item("noref")
            xrow("upduser") = Session("UserID")
            xrow("updtime") = GetServerTime
            dt.Rows.Add(xrow)
        Next
        Return dt
    End Function

    Private Function setTblDtl() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dt.Columns.Add("acctgcode", Type.GetType("System.String"))
        dt.Columns.Add("acctgdesc", Type.GetType("System.String"))
        dt.Columns.Add("gldbcr", Type.GetType("System.String"))
        dt.Columns.Add("glamt", Type.GetType("System.Double"))
        dt.Columns.Add("glnote", Type.GetType("System.String"))
        dt.Columns.Add("cmpcode", Type.GetType("System.String"))
        dt.Columns.Add("gldtloid", Type.GetType("System.Int32"))
        dt.Columns.Add("glmstoid", Type.GetType("System.Int32"))
        dt.Columns.Add("upduser", Type.GetType("System.String"))
        dt.Columns.Add("updtime", Type.GetType("System.DateTime"))
        dt.Columns.Add("glseq", Type.GetType("System.Int32"))
        dt.Columns.Add("noref", Type.GetType("System.String"))
        Return dt
    End Function

    Public Function CheckPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(periode1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. ", 2)
            Return False
        End If
        If Not IsValidDate(periode2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. ", 2)
            Return False
        End If
        If CDate(toDate(periode1.Text)) > CDate(toDate(periode2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select COA field!<BR>"
        End If
        If glamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(glamt.Text) <= 0 Then
                sError &= "- AMOUNT must be more than 0!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\trnFreejurnal.aspx")
        End If
        Session("sCmpcode") = Request.QueryString("cmpcode")
        Session("oid") = Request.QueryString("oid")
        DelBtn.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        PostBtn.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to post this data?');")
        postedx.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to post all selected Memorial Jurnal?');")
        Page.Title = CompnyName & " - Memorial Journal - "
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        If Not Page.IsPostBack Then
            InitDDL()
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(toDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = toDate(GetStrData(sSql))
            End If
            CutOffDateJurnal.Text = Format(CUTOFFDATE.AddDays(-1), "dd/MM/yyyy")
            periode1.Text = Format(GetServerTime, "01/MM/yyyy")
            periode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            lblViewInfo.Visible = True

            If Session("oid") <> Nothing Or Session("oid") <> "" Then
                'btnSave.Text = "Edit"
                filltextbox(Session("sCmpcode"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                outlet.Enabled = False
                outlet.CssClass = "inpTextDisabled"
                'BindData()
            Else
                glDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                generateJurnalmstoid()
                genereteJurnaldtloid()
                txtStatus.Text = "IN PROCESS"
                DelBtn.Visible = False
                created.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = ""

                'txtPostDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                'txtPeriod.Text = GetDateToPeriodAcctg(GetServerTime().Date)
                glseq.Text = 1
                TabContainer1.ActiveTabIndex = 0
            End If

            If Not Session("TblDtl") Is Nothing Then
                Dim dt As New DataTable
                dt = Session("TblDtl")
                gvbaru.DataSource = dt
                gvbaru.DataBind()
            End If

            Dim dt1 As New DataTable : dt1 = Session("TblDtl")
            'gvVoucherList.DataSource = dt1
            'gvVoucherList.DataBind()
        End If
    End Sub

    Protected Sub acctggroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctggroup.SelectedIndexChanged
        InitDDLCOA()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sError As String = ""
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select COA field!<BR>"
        End If
        If glamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(glamt.Text) <= 0 Then
                sError &= "- AMOUNT must be more than 0!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Exit Sub
        End If
        If Session("TblDtl") Is Nothing Then
            Dim dtlTable As DataTable = New DataTable("TblDetailMemo")
            dtlTable.Columns.Add("glseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("acctggroup", Type.GetType("System.String"))
            dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("gldbcr", Type.GetType("System.String"))
            dtlTable.Columns.Add("glamtdb", Type.GetType("System.Double"))
            dtlTable.Columns.Add("glamtcr", Type.GetType("System.Double"))
            dtlTable.Columns.Add("glnote", Type.GetType("System.String"))
            Session("TblDtl") = dtlTable
        End If
        Dim objTable As DataTable = Session("TblDtl")
        'Dim dv As DataView = objTable.DefaultView
        'If i_u2.Text = "New Detail" Then
        '    dv.RowFilter = "acctgoid=" & acctgoid.SelectedValue
        'Else
        '    dv.RowFilter = "acctgoid=" & acctgoid.SelectedValue & " AND glseq<>" & glseq.Text
        'End If
        'If dv.Count > 0 Then
        '    dv.RowFilter = ""
        '    showMessage("This Data has been added before, please check!", 2)
        '    Exit Sub
        'End If
        'dv.RowFilter = ""
        Dim objRow As DataRow
        If i_u2.Text = "New Detail" Then
            objRow = objTable.NewRow()
            glseq.Text = objTable.Rows.Count + 1
            objRow("glseq") = glseq.Text
        Else
            objRow = objTable.Rows(glseq.Text - 1)
            objRow.BeginEdit()
        End If
        objRow("acctggroup") = acctggroup.SelectedValue
        objRow("acctgoid") = acctgoid.SelectedValue
        Dim sData() As String = acctgoid.SelectedItem.Text.Split("]")
        objRow("acctgcode") = sData(0).Replace("[", "").Trim
        objRow("acctgdesc") = sData(1).Trim
        objRow("gldbcr") = gldbcr.SelectedValue
        If gldbcr.SelectedValue = "D" Then
            objRow("glamtdb") = ToDouble(glamt.Text)
            objRow("glamtcr") = 0
        Else
            objRow("glamtdb") = 0
            objRow("glamtcr") = ToDouble(glamt.Text)
        End If
        objRow("glnote") = glnote.Text
        If i_u2.Text = "New Detail" Then
            objTable.Rows.Add(objRow)
        Else
            objRow.EndEdit()
        End If
        Session("TblDtl") = objTable
        gvbaru.DataSource = Session("TblDtl")
        gvbaru.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvbaru_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 1)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 1)
        End If
    End Sub

    Protected Sub gvbaru_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvbaru.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("glseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvbaru.DataSource = objTable
        gvbaru.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvbaru_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvbaru.SelectedIndexChanged
        Try
            glseq.Text = gvbaru.SelectedDataKey.Item("glseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "glseq=" & glseq.Text
                acctggroup.SelectedValue = dv(0).Item("acctggroup").ToString
                acctggroup_SelectedIndexChanged(Nothing, Nothing)
                acctgoid.SelectedValue = dv(0).Item("acctgoid").ToString
                gldbcr.SelectedValue = dv(0).Item("gldbcr").ToString
                If gldbcr.SelectedValue = "D" Then
                    glamt.Text = ToMaskEdit(ToDouble(dv(0).Item("glamtdb").ToString), 1)
                Else
                    glamt.Text = ToMaskEdit(ToDouble(dv(0).Item("glamtcr").ToString), 1)
                End If
                glnote.Text = dv(0).Item("glnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub GvJurnal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvJurnal.PageIndexChanging
        GvJurnal.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub GvJurnal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvJurnal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub GvJurnal_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnfreejurnal.aspx?cmpcode=" & GvJurnal.SelectedDataKey("cmpcode").ToString & "&oid=" & GvJurnal.SelectedDataKey("glmstoid").ToString & "")
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkDetil_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub outlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generateJurnalmstoid()
    End Sub

    Protected Sub glDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dateStat1 As Boolean
        Try
            Dim dt1 As Date = CDate(toDate(glDate.Text))
            dateStat1 = True
            generateJurnalmstoid()
        Catch ex As Exception
            dateStat1 = False
            showMessage("Format Tanggal Salah", 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub periode2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(CDate(toDate(periode2.Text))) = False Or IsDate(CDate(toDate(periode2.Text))) = False Then
            showMessage("Tanggal salah !!", 2)
            Exit Sub
        End If
    End Sub
#End Region

    Protected Sub cbPeriode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        periode1.Enabled = cbPeriode.Checked : periode2.Enabled = cbPeriode.Checked
        btnperiode1.Visible = cbPeriode.Checked : btnperiode2.Visible = cbPeriode.Checked
        If cbPeriode.Checked Then
            periode1.CssClass = "inpText" : periode2.CssClass = "inpText"
        Else
            periode1.CssClass = "inpTextDisabled" : periode2.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub TextBox3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtEd As DataTable = Session("TblDtl")
        Dim ed As DataRow = dtEd.Rows(sender.ToolTip - 1)
        ed.BeginEdit()
        ed("gldbcr") = "D"
        ed("glamt") = ToDouble(sender.Text)
        ed.EndEdit()
        Session("TblDtl") = dtEd
        ' gvVoucherList.DataSource = dtEd
        '  gvVoucherList.DataBind()
    End Sub

    Protected Sub TextBox4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtEd As DataTable = Session("TblDtl")
        Dim ed As DataRow = dtEd.Rows(sender.ToolTip - 1)
        ed.BeginEdit()
        ed("gldbcr") = "C"
        ed("glamt") = ToDouble(sender.Text)
        ed.EndEdit()
        Session("TblDtl") = dtEd
        ' gvVoucherList.DataSource = dtEd : gvVoucherList.DataBind()
    End Sub


  

#Region "Multi POSTing"

    Public Sub CheckAll()

        Dim objTabel As DataTable
        Dim objRow() As DataRow
        objTabel = Session("tbldata")
        If objTabel Is Nothing Then
            showMessage("- No Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        objRow = objTabel.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("glflag").ToString.ToUpper) <> "POST" Then
                    objRow(C1)("selected") = 1
                    objTabel.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTabel
        GvJurnal.DataSource = objTabel
        GvJurnal.DataBind()
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        If objTable Is Nothing Then
            showMessage("- No Found Data Click Button Find Or View All..!!<br>", 2)
            Exit Sub
        End If
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For C1 As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(C1)("glflag").ToString.ToUpper) <> "POST" Then
                    objRow(C1)("selected") = 0
                    objTable.AcceptChanges()
                End If
            Next
        End If
        Session("tbldata") = objTable
        GvJurnal.DataSource = objTable
        GvJurnal.DataBind()
    End Sub

    Private Sub UpdateCheckedPOST()
        Dim dv As DataView = Session("tbldata").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To GvJurnal.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = GvJurnal.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "cmpcode='" & a(0) & "' AND glmstoid=" & a(1)
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Public Function GetEnabled() As Boolean
        Return Eval("glflag").ToString.ToUpper = "IN PROCESS"
    End Function

    Public Function GetCheckedOid() As String
        Return Eval("cmpcode") & "," & Eval("glmstoid")
    End Function

    Private Sub filltextboxSELECTED(ByVal sCmpcode As String, ByVal selectedoid As Integer)
        If Not selectedoid = Nothing Or selectedoid <> 0 Then
            Dim sMsg As String = ""
            Dim vpayid As Integer = selectedoid
            Session("oid") = vpayid.ToString

            Dim xdt As New DataTable
            xdt = GetDataTable("SELECT 0 AS selected,ms.cmpcode, ms.glmstoid,ol.gencode outlet,ms.gldate,ms.periodacctg,ms.glnote,ms.glflag,ms.postdate,ms.upduser,ms.createuser,ms.createtime,ms.updtime,ms.type,(SELECT TOP 1 noref FROM QL_trngldtl dtl WHERE dtl.cmpcode = ms.cmpcode AND dtl.glmstoid = ms.glmstoid) noref FROM QL_trnglmst ms INNER JOIN QL_mstgen ol ON ol.gencode=ms.branch_code INNER JOIN QL_MSTPROF p ON p.BRANCH_CODE=ol.gencode AND p.USERID='admin' WHERE ms.cmpcode='msc' AND ms.glmstoid ='9736'", "dsGlMst")

            Dim xRow() As DataRow
            xRow = xdt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Try
                If xRow.Length > 0 Then
                    ' -- set data header G/L
                    txtGLID.Text = xRow(0)("glmstoid")
                    glDate.Text = Format(xRow(0)("gldate"), "dd/MM/yyyy")
                    '   txtPeriod.Text = Trim(xRow(0)("periodacctg").ToString)
                    txtNote.Text = Trim(xRow(0)("glnote").ToString)
                    txtStatus.Text = Trim(xRow(0)("glflag").ToString)
                    outlet.SelectedValue = Trim(xRow(0)("outlet").ToString)
                    If Trim(xRow(0)("glflag").ToString) = "POST" Then
                        DelBtn.Visible = False
                        SaveBtn.Visible = False : PostBtn.Visible = False
                        '      txtPostDate.Text = Trim(xRow(0)("postdate").ToString)
                    ElseIf Trim(xRow(0)("glflag").ToString) <> "POST" Then
                        PostBtn.Visible = True
                        DelBtn.Visible = True
                        SaveBtn.Visible = True
                        '        txtPostDate.Text = "CURRENT_TIMESTAMP"
                    End If
                    noref.Text = Trim(xRow(0)("noref").ToString)
                    txtStatus.Text = Trim(xRow(0)("glflag").ToString)
                    created.Text = "Created By <B>" & xRow(0)("createuser").ToString & "</B> On <B>" & xRow(0)("createtime").ToString & "</B> "
                    update.Text = "; Last Updated By <B>" & xRow(0)("upduser").ToString & "</B> On <B>" & xRow(0)("updtime").ToString & "</B> "


                    sSql = "SELECT 0 seq,d.acctgoid, d.glmstoid, m.acctgcode, m.acctgdesc, d.gldbcr,d.glamt, d.glnote, d.cmpcode,d.gldtloid,d.glmstoid,d.upduser,d.upduser,d.updtime,d.glseq,d.noref FROM QL_trngldtl d INNER JOIN QL_mstacctg m ON d.acctgoid = m.acctgoid AND d.upduser='" & Session("UserID") & "' WHERE d.cmpcode='" & sCmpcode & "' AND  d.glmstoid = '" & Session("oid") & "' ORDER BY glseq"

                    xdt = GetDataTable(sSql, "dsGlDtl")
                    Session("TblDtl") = xdt
                    gvbaru.DataSource = xdt
                    glseq.Text = xdt.Rows.Count + 1
                    gvbaru.DataBind()
                    ' -- set data debet credit nya
                    Session("ssDb") = GetScalar("SELECT ISNULL(SUM(glamt),0) FROM QL_trngldtl WHERE glmstoid = '" & vpayid & "' AND gldbcr = 'D' AND cmpcode='" & sCmpcode & "'")
                    Session("ssCr") = GetScalar("SELECT ISNULL(SUM(glamt),0) FROM QL_trngldtl WHERE glmstoid = '" & vpayid & "' AND gldbcr = 'C' AND cmpcode='" & sCmpcode & "'")
                    txtDebet.Text = ToMaskEdit(ToDouble(Session("ssDb")), 2)
                    txtCredit.Text = ToMaskEdit(ToDouble(Session("ssCr")), 2)
                    '  txtAllBalance.Text = ToMaskEdit(Session("ssDb") - Session("ssCr"), 2)
                    TabContainer1.ActiveTabIndex = 1
                    If txtStatus.Text = "POST" Then
                        PostBtn.Visible = False
                        '    btnSave.Visible = False : btnAddToList.Visible = False
                    Else
                        PostBtn.Visible = True
                        '  btnSave.Visible = True : btnAddToList.Visible = True
                    End If
                End If
            Catch ex As Exception
                objConn.Close()
                showMessage(ex.ToString, 1)
                Exit Sub
            End Try
        Else
            showMessage("Memorial Jurnal is invalid !", 2)
            Exit Sub
        End If
        TabContainer1.ActiveTabIndex = 0
        PostBtn_Click(Nothing, Nothing)
        TabContainer1.ActiveTabIndex = 0
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnVoucher.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub FindBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Cleardtl() 'gvMstAcctg.DataBind()
        'gvMstAcctg.Visible = True : closePopup.Visible = True
    End Sub

    Protected Sub ViewBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim dtMstAcctg As New DataTable
        dtMstAcctg = GetDataTable("Select acctgoid,acctgcode,acctgdesc From QL_mstacctg", "TblMstAcctg")
        ' gvMstAcctg.DataSource = dtMstAcctg
        ' gvMstAcctg.DataBind()
        '  gvMstAcctg.Visible = True
        '  closePopup.Visible = True
    End Sub

    Protected Sub SaveBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""

        If IsValidPeriod() Then
            If CDate(toDate(glDate.Text)) <= CDate(toDate(CutOffDateJurnal.Text)) Then
                sMsg &= "- Tanggal Memorial Jurnal tidak boleh kurang dari CutOffDate tanggal '" & CutOffDateJurnal.Text & "' !<BR>"
            End If
        End If

        If noref.Text.Trim = "" Then
            sMsg &= "- No. Reference masih Kosong !!<BR>"
        End If

        If DelBtn.Visible = False Then
            sSql = "SELECT COUNT(-1) FROM QL_trngldtl WHERE noref = '" & Tchar(noref.Text) & "' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not (Session("oid") Is Nothing Or Session("oid") = "") Then
                sSql &= " AND gldtloid <> '" & (Session("oid")) & "'"
            End If
            Dim hitungrefno As Integer = Integer.Parse(GetStrData(sSql))
            If hitungrefno > 0 Then
                sMsg &= "- No Referensi Sudah pernah di input..!!<BR>"
            End If
        End If

        If txtDebet.Text <> "" And txtCredit.Text <> "" Then
            If (ToDouble(txtDebet.Text) - (ToDouble(txtCredit.Text)) <> 0) Or _
            (ToDouble(txtDebet.Text) = 0 And ToDouble(txtCredit.Text) = 0) Then
                sMsg &= "- Debet & Credit must be equal and greater than 0 !<BR>"
            End If
        End If
        If txtDebet.Text <> "" And txtCredit.Text <> "" Then
            If (ClassFunction.ToDouble(txtDebet.Text) - (ClassFunction.ToDouble(txtCredit.Text)) <> 0) Or _
            (ClassFunction.ToDouble(txtDebet.Text) = 0 And ClassFunction.ToDouble(txtCredit.Text) = 0) Then
                sMsg &= "Debet & Credit must be equal,<BR>and greater than 0!!"
            End If
        End If

        'cek apa sudah ada G/L detail
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- Please create G/L detail !<BR>"
        Else
            Dim objTableCek As DataTable
            Dim objRowCek() As DataRow
            objTableCek = Session("TblDtl")
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                sMsg &= "- Please create G/L detail !<BR>"
            End If
        End If

        'Check last status of data
        If txtStatus.Text = "DELETE" Then
            sSql = "SELECT glflag FROM QL_trnglmst WHERE cmpcode='" & CompnyCode & "' AND glmstoid = " & Session("oid") & " AND glflag='DELETE'"
            Dim sStatus As String = GetStrData(sSql)
            sMsg &= "- Memorial Jurnal with ID '" & Session("oid") & "' has been DELETED before !<br />"
            'End Select
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            txtStatus.Text = "IN PROCESS"
            Exit Sub
        End If

        generateJurnalmstoid()
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans

        Session("glmstoid") = GenerateID("QL_trnglmst", CompnyCode)
        Session("gldtloid") = GenerateID("QL_trngldtl", CompnyCode)

        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                Dim vMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
                sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,branch_code,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,createuser,createtime,type) VALUES ('" & CompnyCode & "'," & vMst & ",'" & outlet.SelectedValue & "','" & CDate(toDate(glDate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(glDate.Text))) & "','" & Tchar(txtNote.Text) & "','" & Tchar(txtStatus.Text) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'JURNAL')"

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    Dim vDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
                    Dim i As Integer
                    Dim objTable As DataTable : Dim objRow() As DataRow

                    objTable = Session("TblDtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim temp As Integer = Session("gldtloid")
                    For i = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr, noref,glnote,glother1,glother2,glflag,glpostdate,upduser,updtime,glamt) VALUES ('" & CompnyCode & "'," & vDtl & ",'" & outlet.SelectedValue & "'," & (i + 1) & "," & vMst & "," & (objRow(i)("acctgoid")).ToString & ",'" & (objRow(i)("gldbcr")).ToString & "','" & Tchar(noref.Text) & "','" & Tchar(txtNote.Text) & "','','','" & Tchar(txtStatus.Text) & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP"

                        If objTable.Rows(i)("gldbcr").ToString = "D" Then
                            sSql &= ", " & ToDouble(objTable.Rows(i)("glamtdb").ToString) & ")"
                        Else
                            sSql &= ", " & ToDouble(objTable.Rows(i)("glamtcr").ToString) & ")"
                        End If
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        vDtl = vDtl + 1
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vMst & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnglmst'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & (vDtl - 1) & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trngldtl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Else ' update

                sSql = "UPDATE QL_trnglmst SET gldate='" & toDate(glDate.Text) & "',periodacctg='" & GetDateToPeriodAcctg(toDate(glDate.Text)) & "', glnote ='" & Tchar(txtNote.Text) & "',glflag='" & txtStatus.Text & "', postdate=CURRENT_TIMESTAMP, upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE glmstoid='" & Session("oid") & "' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Delete from QL_trngldtl WHERE glmstoid='" & Session("oid") & "' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("TblDtl") Is Nothing Then
                    'insert tabel detail
                    Dim vDtl As Integer = GenerateID("QL_trngldtl", CompnyCode)
                    Dim vMst As Integer = GenerateID("QL_trnglmst", CompnyCode)
                    Dim vDtloid As Integer = vDtl + 1

                    Dim i As Integer
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("TblDtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    Dim temp As Integer = Session("gldtloid")
                    For i = 0 To objRow.Length - 1
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr, noref,glnote,glother1,glother2,glflag,glpostdate,upduser,updtime,glamt) VALUES ('" & CompnyCode & "'," & vDtloid & ",'" & outlet.SelectedValue & "'," & (i + 1) & "," & Session("oid") & "," & (objRow(i)("acctgoid")).ToString & ",'" & (objRow(i)("gldbcr")).ToString & "', '" & Tchar(noref.Text) & "','" & Tchar(txtNote.Text) & "','','','" & Tchar(txtStatus.Text) & "',CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP"
                        If objTable.Rows(i)("gldbcr").ToString = "D" Then
                            sSql &= ", " & ToDouble(objTable.Rows(i)("glamtdb").ToString) & ")"
                        Else
                            sSql &= ", " & ToDouble(objTable.Rows(i)("glamtcr").ToString) & ")"
                        End If
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        vDtloid = vDtloid + 1
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & vDtloid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trngldtl'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            objTrans.Commit()
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try

        Session("dtlTable") = Nothing : Session("JurnalID") = Nothing
        Session("MemoID") = Nothing : Session("oid") = Nothing
        Session("ssDb") = Nothing : Session("ssCr") = Nothing

        If txtStatus.Text = "POST" Then
            showMessage("Semua Data yang dipilih Telah di Posting <BR>", 3)
            Response.Redirect("TrnFreeJurnal.aspx?awal=true")
            'Session("QLMsgBox")
        Else
            Session("oid") = Nothing
            Session("dtlTable") = Nothing
            Response.Redirect("TrnFreeJurnal.aspx?awal=true")
        End If
    End Sub

    Protected Sub CancelBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("TblDtl") = Nothing : Session("mstoid") = Nothing
        Session("ssDb") = Nothing : Session("ssCr") = Nothing
        Response.Redirect("~/accounting/trnfreejurnal.aspx?awal=true")
    End Sub

    Protected Sub DelBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ' Check last status of data
        sSql = "SELECT glflag FROM QL_trnglmst WHERE cmpcode='" & CompnyCode & "' AND glmstoid='" & txtGLID.Text & "'"
        Dim sStatus As String = GetStrData(sSql)

        Select Case sStatus
            Case "POST"
                showMessage("Thisl data has been POSTED before !", 2)
                Exit Sub
            Case "IN PROCESS"
                ' do nothing
            Case Else
                showMessage("This data has beed DELETED before !", 2)
                Exit Sub
        End Select

        Dim strSQL As String = ""
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            strSQL = ("DELETE QL_TrnGlDtl WHERE cmpcode='" & CompnyCode & "' AND GLMstOid = '" & txtGLID.Text & "'")
            xCmd.CommandText = strSQL : xCmd.ExecuteNonQuery()

            strSQL = ("UPDATE QL_TrnGlMst SET glflag='DELETE' WHERE cmpcode='" & CompnyCode & "' AND GLMstOid = '" & txtGLID.Text & "'")
            xCmd.CommandText = strSQL : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("TblDtl") = Nothing
        Session("oid") = Nothing
        Session("ssDb") = Nothing
        Session("ssCr") = Nothing
        Response.Redirect("~/accounting/trnfreejurnal.aspx?awal=true")
    End Sub

    Protected Sub PostBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'txtPostDate.Text = GetServerTime()
        txtStatus.Text = "POST"
        'Session("MstrGLpostdate") = Trim(txtPostDate.Text)
        Session("MstrGLflag") = Trim(txtStatus.Text)
        SaveBtn_Click(sender, e)
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Cleardtl()
        Dim dt2 As New DataTable : dt2 = Session("tbldtl")
        glseq.Text = dt2.Rows.Count + 1
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblViewInfo.Visible = False
        BindData()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cbPeriode.Checked = False
        cbPeriode_CheckedChanged(Nothing, Nothing)
        FilterText.Text = ""
        lblViewInfo.Visible = False
        BindData()
    End Sub

    Protected Sub postedx_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sErrorku As String = ""
        If Not Session("tbldata") Is Nothing Then
            Dim dt As DataTable = Session("tbldata")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPOST()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select Noref first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND glflag='Post'"
                If dv.Count > 0 Then
                    sErrorku &= "Memo Jurnal have been POSTED before!"
                End If
                dv.RowFilter = ""
                If sErrorku <> "" Then
                    showMessage(sErrorku, 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                Dim parsingOID As Integer = 0
                Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("tbldata")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True
                dvAwal.AllowNew = True
                dv.RowFilter = "selected=1 AND glflag='In Process'"
                'poststatx.Text = "SPECIAL"
                For C1 As Integer = 0 To dv.Count - 1
                    parsingOID = dv(C1)("glmstoid").ToString
                    sCmpcode = dv(C1)("cmpcode").ToString
                    filltextboxSELECTED(sCmpcode, parsingOID)
                    If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                        sErrorku &= "Can't POST Noref " & dv(C1)("noref").ToString & " with reason:<BR>" & Session("errmsg")
                    End If
                Next

            End If
        End If
    End Sub

    Protected Sub postedall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub postednotallx_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub
#End Region

    
End Class
