<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstCustGroup.aspx.vb" Inherits="MstCustGroup" Title="MSC- Master Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table width="100%">
        <tr>
            <td align="left">
                <asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" SelectCommand="SELECT cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, custfax1, phone3, custfax2, custemail, custwebsite, upduser, updtime, custnpwp,pin_bb FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)" InsertCommand="INSERT INTO QL_mstcust(cmpcode, custoid, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, upduser, updtime, phonecontactperson3, phonecontactperson2, phonecontactperson1, contactperson1, contactperson2, contactperson3, custnpwp,pin_bb) VALUES (@cmpcode, @custoid, @custcode, @custname, @custflag, @custaddr, @custcityoid, @custprovoid, @custcountryoid, @custpostcode, @phone1, @phone2, @phone3, @custfax1, @custfax2, @custemail, @custwebsite, @upduser, @updtime, @phonecontactperson3, @phonecontactperson2, @phonecontactperson1, @contactperson1, @contactperson2, @contactperson3, @custnpwp,@pin_bb)" UpdateCommand="UPDATE QL_mstcust SET custcode = @custcode, custname = @custname, custflag = @custflag, custaddr = @custaddr, custcityoid = @custcityoid, custprovoid = @custprovoid, custcountryoid = @custcountryoid, custpostcode = @custpostcode, phone1 = @phone1, phone2 = @phone2, phone3 = @phone3, custfax1 = @custfax1, custfax2 = @custfax2, custemail = @custemail, custwebsite = @custwebsite, upduser = @upduser, updtime = @updtime, custnpwp = @custnpwp, contactperson1 = @contactperson1, contactperson2 = @contactperson2, contactperson3 = @contactperson3, phonecontactperson1 = @phonecontactperson1, phonecontactperson2 = @phonecontactperson2, phonecontactperson3 = @phonecontactperson3,pin_bb=@pin_bb WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
                    <SelectParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="custcode" />
                        <asp:Parameter Name="custname" />
                        <asp:Parameter Name="custflag" />
                        <asp:Parameter Name="custaddr" />
                        <asp:Parameter Name="custcityoid" />
                        <asp:Parameter Name="custprovoid" />
                        <asp:Parameter Name="custcountryoid" />
                        <asp:Parameter Name="custpostcode" />
                        <asp:Parameter Name="phone1" />
                        <asp:Parameter Name="phone2" />
                        <asp:Parameter Name="phone3" />
                        <asp:Parameter Name="custfax1" />
                        <asp:Parameter Name="custfax2" />
                        <asp:Parameter Name="custemail" />
                        <asp:Parameter Name="custwebsite" />
                        <asp:Parameter Name="upduser" />
                        <asp:Parameter Name="updtime" />
                        <asp:Parameter Name="custnpwp" />
                        <asp:Parameter Name="contactperson1" />
                        <asp:Parameter Name="contactperson2" />
                        <asp:Parameter Name="contactperson3" />
                        <asp:Parameter Name="phonecontactperson1" />
                        <asp:Parameter Name="phonecontactperson2" />
                        <asp:Parameter Name="phonecontactperson3" />
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                        <asp:Parameter Name="pin_bb" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                        <asp:Parameter Name="custcode" />
                        <asp:Parameter Name="custname" />
                        <asp:Parameter Name="custflag" />
                        <asp:Parameter Name="custaddr" />
                        <asp:Parameter Name="custcityoid" />
                        <asp:Parameter Name="custprovoid" />
                        <asp:Parameter Name="custcountryoid" />
                        <asp:Parameter Name="custpostcode" />
                        <asp:Parameter Name="phone1" />
                        <asp:Parameter Name="phone2" />
                        <asp:Parameter Name="phone3" />
                        <asp:Parameter Name="custfax1" />
                        <asp:Parameter Name="custfax2" />
                        <asp:Parameter Name="custemail" />
                        <asp:Parameter Name="custwebsite" />
                        <asp:Parameter Name="upduser" />
                        <asp:Parameter Name="updtime" />
                        <asp:Parameter Name="phonecontactperson3" />
                        <asp:Parameter Name="phonecontactperson2" />
                        <asp:Parameter Name="phonecontactperson1" />
                        <asp:Parameter Name="contactperson1" />
                        <asp:Parameter Name="contactperson2" />
                        <asp:Parameter Name="contactperson3" />
                        <asp:Parameter Name="custnpwp" />
                        <asp:Parameter Name="pin_bb" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT DISTINCT cmpcode, custoid, custcode, custname, custaddr, custcityoid, custprovoid, custflag, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, upduser, updtime FROM QL_mstcust WHERE (cmpcode = @cmpcode) AND (custoid = @custoid)">
                    <SelectParameters>
                        <asp:Parameter Name="cmpcode" />
                        <asp:Parameter Name="custoid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSOID" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, tablename, lastoid, tablegroup FROM QL_mstoid WHERE (tablename = @tablename)" UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="QL_mstcust" Name="tablename" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="lastoid" />
                        <asp:Parameter Name="tablename" />
                        <asp:Parameter Name="cmpcode" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSBank" runat="server"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCurrency" runat="server"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCity" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="City" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSProvince" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Province" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSCountry" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Country" Name="gengroup" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSContactPerson" runat="server" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>" DeleteCommand="DELETE FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" InsertCommand="INSERT INTO QL_CONTACTPERSON(CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDTIME, UPDUSER) VALUES (@CMPCODE, @CPOID, @OID, @TYPE, @CPNAME, @CPPHONE, @CPEMAIL, @UPDTIME, @UPDUSER)" SelectCommand="SELECT CMPCODE, CPOID, OID, TYPE, CPNAME, CPPHONE, CPEMAIL, UPDUSER, UPDTIME FROM QL_CONTACTPERSON WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)" UpdateCommand="UPDATE QL_CONTACTPERSON SET OID = @OID, TYPE = @TYPE, CPNAME = @CPNAME, CPPHONE = @CPPHONE, CPEMAIL = @CPEMAIL, UPDUSER = @UPDUSER, UPDTIME = @UPDTIME WHERE (CMPCODE = @CMPCODE) AND (CPOID = @CPOID)">
                    <SelectParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="OID" />
                        <asp:Parameter Name="TYPE" />
                        <asp:Parameter Name="CPNAME" />
                        <asp:Parameter Name="CPPHONE" />
                        <asp:Parameter Name="CPEMAIL" />
                        <asp:Parameter Name="UPDUSER" />
                        <asp:Parameter Name="UPDTIME" />
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="CMPCODE" />
                        <asp:Parameter Name="CPOID" />
                        <asp:Parameter Name="OID" />
                        <asp:Parameter Name="TYPE" />
                        <asp:Parameter Name="CPNAME" />
                        <asp:Parameter Name="CPPHONE" />
                        <asp:Parameter Name="CPEMAIL" />
                        <asp:Parameter Name="UPDTIME" />
                        <asp:Parameter Name="UPDUSER" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSShipping" runat="server"></asp:SqlDataSource>
                <table width="100%">
                    <tr>
                        <td colspan="3" style="background-color: silver" align="left" char="header">
                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                                ForeColor="Navy" Text=".: Customer Group"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3" style="background-color: transparent">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <strong><span style="color: background">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                :: List of Customer</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD><asp:Label id="Label2" runat="server" Text="Filter" __designer:wfdid="w93"></asp:Label></TD><TD><asp:Label id="Label3" runat="server" Text=":" __designer:wfdid="w94"></asp:Label></TD><TD colSpan=5><asp:DropDownList id="ddlFilter" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w95"><asp:ListItem Value="custgroupcode">Code</asp:ListItem>
<asp:ListItem Value="custgroupname">Nama</asp:ListItem>
<asp:ListItem Value="custgroupaddr">Alamat</asp:ListItem>
<asp:ListItem Value="cabang">Cabang</asp:ListItem>
<asp:ListItem Value="phone1">Telepon</asp:ListItem>
<asp:ListItem Value="custgroupcreditlimitrupiah">Credit Limit</asp:ListItem>
<asp:ListItem Value="custgroupcreditlimitusagerupiah">Credit Usage</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="166px" CssClass="inpText" __designer:wfdid="w96"></asp:TextBox>&nbsp;<asp:ImageButton style="HEIGHT: 23px" id="btnFind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w99"></asp:ImageButton> </TD></TR><TR><TD style="WIDTH: 78px; HEIGHT: 16px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w100"></asp:CheckBox></TD><TD style="HEIGHT: 16px"><asp:Label id="Label9" runat="server" Text=":" __designer:wfdid="w101"></asp:Label></TD><TD style="HEIGHT: 16px" colSpan=5><asp:DropDownList id="DDLStatus" runat="server" Width="108px" CssClass="inpText" __designer:wfdid="w102">
                                                        <asp:ListItem>Active</asp:ListItem>
                                                        <asp:ListItem>Inactive</asp:ListItem>
                                                        <asp:ListItem>Suspended</asp:ListItem>
                                                    </asp:DropDownList></TD></TR><TR><TD colSpan=65><asp:GridView style="max-width: 950px" id="GVmstcust" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w103" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="custgroupoid" DataNavigateUrlFormatString="~\master\mstCustGroup.aspx?idPage={0}" DataTextField="custgroupcode" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle Wrap="False" Width="100px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="custgroupname" HeaderText="Nama">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="140px"></HeaderStyle>

<ItemStyle Width="160px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupaddr" HeaderText="Alamat">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="city" HeaderText="Kota" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cabang" HeaderText="Cabang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="120px"></HeaderStyle>

<ItemStyle Width="120px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="phone" HeaderText="Telepon">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditlimit" HeaderText="Credit Limit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditusage" HeaderText="Credit Usage">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="creditover" HeaderText="Credit Over">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custgroupflag" HeaderText="Status">
<HeaderStyle Wrap="True" CssClass="gvhdr" ForeColor="Black" Width="80px"></HeaderStyle>

<ItemStyle Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="notes" HeaderText="Catatan" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="flagcustomer" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Data Not Found" BorderColor="Blue"></asp:Label>
                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE>
</ContentTemplate>

                                <Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <strong><span style="color: background">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/corner.gif" ImageAlign="AbsMiddle" />
                                :: Form Customer</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
<TABLE width=1005><TBODY><TR><TD style="HEIGHT: 4px" colSpan=3><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w4" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w5"><TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top; HEIGHT: 18px" colSpan=7><asp:Label id="Label8" runat="server" Font-Bold="True" Text="Informasi" __designer:wfdid="w6"></asp:Label>&nbsp;<asp:LinkButton id="lbconsignee" runat="server" __designer:wfdid="w7" Visible="False">Consignee</asp:LinkButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="custgroupoid" runat="server" Text="custgroupoid" __designer:wfdid="w8" Visible="False"></asp:Label>&nbsp;</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="lblAdd" runat="server" __designer:wfdid="w9" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Code <asp:Label id="Label5" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w10"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custcode" runat="server" Width="130px" CssClass="inpTextDisabled" Font-Size="X-Small" __designer:wfdid="w11" MaxLength="10" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap"><asp:CheckBox id="cbCustomer" runat="server" Text="Single Customer" __designer:wfdid="w12"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Nama <asp:Label id="Label6" runat="server" CssClass="Important" Text="*" __designer:wfdid="w13"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="precustname" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w14"></asp:DropDownList> <asp:TextBox id="custname" runat="server" Width="221px" CssClass="inpText" __designer:wfdid="w15" MaxLength="100" AutoPostBack="True"></asp:TextBox><BR /><asp:Label id="Label12" runat="server" ForeColor="Red" Text="min. 1 karakter" __designer:wfdid="w16"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Provinsi</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custprovoid" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w17" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD52" runat="server" Visible="true">Cabang</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD50" runat="server" Visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="TD51" colSpan=1 runat="server" Visible="true"><asp:DropDownList id="branchID" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w18" AutoPostBack="True" OnSelectedIndexChanged="branchID_SelectedIndexChanged"></asp:DropDownList> &nbsp;<asp:Label id="KodeOutlate" runat="server" Text="Label" __designer:wfdid="w19" Visible="False"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Alamat</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custaddr" runat="server" Width="296px" CssClass="inpText" __designer:wfdid="w20" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Kota</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custcityoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w21" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Contact Person 1</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlconttitle" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w22"></asp:DropDownList> <asp:TextBox id="contactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w23" MaxLength="125"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Status</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="custflag" runat="server" Width="103px" CssClass="inpText" __designer:wfdid="w24">
                                                                                <asp:ListItem>Active</asp:ListItem>
                                                                                <asp:ListItem>Inactive</asp:ListItem>
                                                                                <asp:ListItem>Suspended</asp:ListItem>
                                                                            </asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Contact Person 2</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlconttitle2" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w25"></asp:DropDownList> <asp:TextBox id="contactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w26" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Telepon 1 <asp:Label id="Label24" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w27"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="phone1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w28" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Contact Person 3</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:DropDownList id="ddlconttitle3" runat="server" Width="70px" CssClass="inpText" __designer:wfdid="w29"></asp:DropDownList> <asp:TextBox id="contactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w30" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Telepon 2</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="phone2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w31" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">E-mail</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="custemail" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w32" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Telepon 3</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="phone3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w33" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Telepon Contact Person 1</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phonecontactperson1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w34" MaxLength="50"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Fax</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custfax1" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w35" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Telepon Contact Person 2</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phonecontactperson2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w36" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Website</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custwebsite" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w37" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Telepon Contact Person 3</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="phonecontactperson3" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w38" MaxLength="25"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">NPWP</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custnpwp" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w39" MaxLength="25"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Catatan<BR /></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="notes" runat="server" Width="213px" Height="45px" CssClass="inpText" __designer:wfdid="w40" MaxLength="100" TextMode="MultiLine"></asp:TextBox> <asp:Label id="Label4" runat="server" ForeColor="Red" Text="maks. 100 karakter" __designer:wfdid="w41"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="spgOid" runat="server" Width="112px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w42" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Last Sales Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="lastsalesdate" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w43"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top">Age Sales Date</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="txtage" runat="server" Width="72px" CssClass="inpTextDisabled" __designer:wfdid="w44"></asp:TextBox>&nbsp;Day</TD></TR><TR><TD style="VERTICAL-ALIGN: top">Pin BB</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top" colSpan=1><asp:TextBox id="pin_bb" runat="server" CssClass="inpText" __designer:wfdid="w45" MaxLength="12"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label25" runat="server" Text="TOP" __designer:wfdid="w46"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="dd_timeofpayment" runat="server" Width="80px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w47" AutoPostBack="True"></asp:DropDownList>&nbsp;Hari</TD></TR><TR><TD style="VERTICAL-ALIGN: top">Credit Limit Rupiah <asp:Label id="Label26" runat="server" Width="12px" CssClass="Important" Text="*" __designer:wfdid="w48"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitrupiah" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w49" MaxLength="10" AutoPostBack="True"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap"><asp:Label id="Label18" runat="server" Text="Nama untuk Paspor" __designer:wfdid="w50"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="namapaspor" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w51" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top">Credit Limit Usage Rupiah</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="custcreditlimitusagerupiah" runat="server" Width="104px" CssClass="inpTextDisabled" __designer:wfdid="w52" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label19" runat="server" Text="Nomor Paspor" __designer:wfdid="w53"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="nopaspor" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w54" MaxLength="25"></asp:TextBox></TD></TR><TR><TD><asp:Label id="Label28" runat="server" Text="Sisa Credit Limit Rupiah" __designer:wfdid="w55"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="sisacreditlimitrupiah" runat="server" Width="104px" CssClass="inpTextDisabled" __designer:wfdid="w56" MaxLength="10" Enabled="False"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label20" runat="server" Text="Tanggal Lahir" __designer:wfdid="w57"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="tgllahir" runat="server" Width="72px" CssClass="inpText" __designer:wfdid="w58"></asp:TextBox><asp:ImageButton id="ibtgllahir" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w59"></asp:ImageButton><asp:Label id="Label11" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)" __designer:wfdid="w60"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Nama Bank dan Rekening</TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="bankrekening" runat="server" Width="300px" CssClass="inpText" __designer:wfdid="w61" MaxLength="100"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"><asp:Label id="Label22" runat="server" Text="A/R Account" __designer:wfdid="w62"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="araccount" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w63"></asp:DropDownList></TD></TR><TR><TD><asp:Label id="Label21" runat="server" Text="Retur Account" __designer:wfdid="w64"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px">:</TD><TD><asp:DropDownList id="returaccount" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w65"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px"></TD><TD style="VERTICAL-ALIGN: top"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px"></TD><TD style="VERTICAL-ALIGN: top" rowSpan=1></TD></TR><TR><TD colSpan=7><asp:Label id="lblLast" runat="server" Text="Last Update On" __designer:wfdid="w1"></asp:Label>&nbsp;<asp:Label id="lblCreate" runat="server" Text="Created On" __designer:wfdid="w2"></asp:Label> <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w3"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w4"></asp:Label> </TD></TR><TR><TD colSpan=7><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w5" Visible="False"></asp:ImageButton> <asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False" __designer:wfdid="w6"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 15px" id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" __designer:wfdid="w7" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=7><ajaxToolkit:MaskedEditExtender id="MEEnpwp" runat="server" __designer:wfdid="w66" TargetControlID="custnpwp" AcceptNegative="Right" Mask="99.999.999.9-999.999" MaskType="Number"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte1" runat="server" __designer:wfdid="w67" TargetControlID="phone1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte2" runat="server" __designer:wfdid="w68" TargetControlID="phonecontactperson2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" __designer:wfdid="w69" TargetControlID="phonecontactperson3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte6" runat="server" __designer:wfdid="w70" TargetControlID="phone2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte8" runat="server" __designer:wfdid="w71" TargetControlID="phonecontactperson1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fte9" runat="server" __designer:wfdid="w72" TargetControlID="phone3" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteCustFax1" runat="server" __designer:wfdid="w73" TargetControlID="custfax1" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fCustFax2" runat="server" __designer:wfdid="w74" TargetControlID="CustFax2" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="pcountry" runat="server" __designer:wfdid="w75" TargetControlID="cpcountryphonecode" ValidChars="0123456789+"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEcustpostcode" runat="server" __designer:wfdid="w76" TargetControlID="custpostcode" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="CETgllahir" runat="server" __designer:wfdid="w77" TargetControlID="tgllahir" Format="dd/MM/yyyy" PopupButtonID="ibtgllahir"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEETgllahir" runat="server" __designer:wfdid="w78" TargetControlID="tgllahir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEcustcreditlimitrupiah" runat="server" __designer:wfdid="w79" TargetControlID="custcreditlimitrupiah" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender><%-- <ajaxToolkit:MaskedEditExtender ID="MEECreditLimitRupiah" runat="server" TargetControlID="custcreditlimitrupiah" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender>
                                                                    <ajaxToolkit:MaskedEditExtender ID="MEECreditLimitUsageRupiah" runat="server" TargetControlID="custcreditlimitusagerupiah" Mask="999,999,999.99" MaskType="Number" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender>--%><asp:Label id="I_U" runat="server" ForeColor="Red" Text="NEW" __designer:wfdid="w80" Visible="False"></asp:Label> <asp:TextBox id="custoid" runat="server" Width="21px" CssClass="inpText" __designer:wfdid="w81" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD40" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD41" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD35" runat="server" Visible="false"><asp:DropDownList id="custpaytermdefaultoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w82" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD30" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD20" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD39" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD14" runat="server" Visible="false"><asp:DropDownList id="custdefaultcurroid" runat="server" CssClass="inpText" __designer:wfdid="w83" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD5" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD34" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD26" runat="server" Visible="false"><asp:DropDownList id="custacctgoid" runat="server" Width="140px" CssClass="inpText" __designer:wfdid="w84" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD16" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD32" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD44" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD7" runat="server" Visible="false"><asp:TextBox id="custcreditlimit" runat="server" Width="96px" CssClass="inpText" __designer:wfdid="w85" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD10" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD47" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD19" runat="server" Visible="false"><asp:DropDownList id="custcreditlimitcurroid" runat="server" CssClass="inpText" __designer:wfdid="w86" Visible="False"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD45" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD48" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD43" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD22" rowSpan=1 runat="server" Visible="false"><asp:TextBox id="custcreditlimitusage" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w87" Visible="False"></asp:TextBox></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD38" runat="server" Visible="false"><asp:Label id="Label10" runat="server" Text="Negara" __designer:wfdid="w88" Visible="False"></asp:Label></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD29" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD2" runat="server" Visible="false"><asp:DropDownList id="custcountryoid" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w89" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD3" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD33" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD25" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD37" rowSpan=1 runat="server" Visible="false"><asp:Label id="Label7" runat="server" Text="Def. Limit Currency" __designer:wfdid="w90" Visible="False"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD27" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD8" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD4" runat="server" Visible="false"><asp:TextBox id="CustGroup" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w91" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD1" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD18" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD12" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD49" rowSpan=1 runat="server" Visible="false"><asp:DropDownList id="statusar" runat="server" CssClass="inpText" __designer:wfdid="w92" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD23" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD13" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD15" runat="server" Visible="false"><asp:TextBox id="cpcountryphonecode" runat="server" Width="31px" CssClass="inpTextDisabled" __designer:wfdid="w93" Visible="False" MaxLength="6" Enabled="False" AutoPostBack="True"></asp:TextBox><asp:TextBox id="custpostcode" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w94" Visible="False" MaxLength="10"></asp:TextBox></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD46" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD42" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD21" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD28" runat="server" Visible="false"></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="TD36" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD31" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD17" runat="server" Visible="false"><asp:Label id="lbl_lasttrans" runat="server" __designer:wfdid="w95"></asp:Label> </TD><TD style="VERTICAL-ALIGN: top; WIDTH: 20px" id="TD9" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD24" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top; WIDTH: 5px" id="TD11" runat="server" Visible="false"></TD><TD style="VERTICAL-ALIGN: top" id="TD6" runat="server" Visible="false"><asp:TextBox id="CustFax2" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w96" Visible="False" MaxLength="25"></asp:TextBox></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server" __designer:wfdid="w97"><TABLE width="100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:LinkButton id="lbinformasi" runat="server" __designer:wfdid="w98">Informasi</asp:LinkButton> | <asp:Label id="Label13" runat="server" Font-Bold="True" Text="Consignee" __designer:wfdid="w99"></asp:Label></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 800px; HEIGHT: 10px"><asp:GridView id="GVConsignee" runat="server" Width="98%" ForeColor="#333333" __designer:wfdid="w100" DataKeyNames="consoid, custoid, conscityoid, consprovoid, consphone, consfax, consnpwp, conscontactperson, consphonecontactpersonchar, consnote, prefixcp, conpostcode" PageSize="5" CellPadding="4" AutoGenerateColumns="False">
                                                                                    <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Code">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="conscode" OnClick="conscode_Click" runat="server" Text='<%# Eval("conscode") %>'></asp:LinkButton>
                                                                                            </ItemTemplate>

                                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="consname" HeaderText="Nama">
                                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>

                                                                                            <ItemStyle Width="20%"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="consaddress" HeaderText="Alamat">
                                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>

                                                                                            <ItemStyle Width="40%"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="gendesc" HeaderText="Kota">
                                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>

                                                                                            <ItemStyle Width="20%"></ItemStyle>
                                                                                        </asp:BoundField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="cbcons" runat="server"></asp:CheckBox>
                                                                                            </ItemTemplate>

                                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>

                                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                                                                                    <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
                                                                                    <EmptyDataTemplate>
                                                                                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Data Not Found" BorderColor="Blue"></asp:Label>
                                                                                    </EmptyDataTemplate>

                                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                                                                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>

                                                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                                </asp:GridView> </DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top; TEXT-ALIGN: right" colSpan=3><asp:ImageButton id="ibremoveconsignee" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="Top" __designer:wfdid="w101"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:LinkButton id="lbnewconsignee" runat="server" Font-Bold="True" __designer:wfdid="w102">[New Consignee]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:View> </asp:MultiView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False" EnableTheming="True">
                <table width="400">
                    <tbody>
                        <tr>
                            <td style="HEIGHT: 25px; BACKGROUND-COLOR: red" align="left" colspan="3">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="WIDTH: 47px" align="left">
                                <asp:Image ID="imIcon" runat="server" ImageUrl="~/Images/error.jpg"></asp:Image></td>
                            <td align="left" colspan="2">
                                <asp:Label ID="validasi" runat="server" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="WIDTH: 47px"></td>
                            <td colspan="2">
                                <asp:Label ID="lblstate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="HEIGHT: 25px" align="center" colspan="3">
                                <asp:ImageButton ID="btnErrOK" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MPEError" runat="server" TargetControlID="btnExtender" DropShadow="True" Drag="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblValidasi" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnExtender" runat="server" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
        <ContentTemplate>
            <asp:UpdatePanel ID="UpdPanelPrint" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid">
                        <table style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="HEIGHT: 18px; TEXT-ALIGN: center" align="left" colspan="2">
                                        <asp:Label ID="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Customer"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="HEIGHT: 12px; TEXT-ALIGN: center" align="left" colspan="2">
                                        <asp:Label ID="printType" runat="server" CssClass="Important"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="HEIGHT: 10px; TEXT-ALIGN: center" align="left" colspan="2">
                                        <asp:Label ID="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label ID="orderNoForReport" runat="server" CssClass="Important"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="HEIGHT: 23px; TEXT-ALIGN: center" align="left" colspan="2">
                                        <asp:ImageButton ID="imbPrintPDF" OnClick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton ID="imbPrintExcel" OnClick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton ID="imbCancelPrint" OnClick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></td>
                                </tr>
                                <tr>
                                    <td style="TEXT-ALIGN: center" align="left" colspan="2">
                                        <asp:Label ID="lblError" runat="server" CssClass="Important"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="HEIGHT: 20px" align="left"></td>
                                    <td style="HEIGHT: 20px" align="left"></td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mpePrint" runat="server" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPrint" PopupControlID="pnlPrint" Drag="True" TargetControlID="btnHidePrint"></ajaxToolkit:ModalPopupExtender>
                    <asp:Button ID="btnHidePrint" runat="server" Visible="False"></asp:Button>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
                    <asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <asp:Panel ID="PanelConsignee" runat="server" CssClass="modalBox" EnableTheming="True" Visible="False">
                <table>
                    <tbody>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px" align="left" colspan="1"></td>
                            <td style="VERTICAL-ALIGN: middle; HEIGHT: 25px; TEXT-ALIGN: center" align="left" colspan="4">
                                <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Consignee"></asp:Label></td>
                            <td style="WIDTH: 30px" align="left" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px" align="left" colspan="1"></td>
                            <td style="VERTICAL-ALIGN: top" align="left" colspan="4">
                                <asp:Label ID="lblconsstate" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="consigneeoid" runat="server" Text="0" Visible="False"></asp:Label></td>
                            <td style="WIDTH: 30px" align="left" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px" align="left"></td>
                            <td style="VERTICAL-ALIGN: top" align="left">Code
                        <asp:Label ID="Label50" runat="server" Width="12px" CssClass="Important" Text="*" Visible="False"></asp:Label></td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px" align="left">:</td>
                            <td align="left" colspan="2">
                                <asp:TextBox ID="conscode" runat="server" CssClass="inpTextDisabled" Enabled="False" MaxLength="10"></asp:TextBox></td>
                            <td style="WIDTH: 30px" align="left" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Nama
                        <asp:Label ID="Label15" runat="server" Width="12px" CssClass="Important" Text="*"></asp:Label></td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consname" runat="server" Width="200px" CssClass="inpText" MaxLength="100"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Alamat
                        <asp:Label ID="Label16" runat="server" Width="12px" CssClass="Important" Text="*"></asp:Label></td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consaddress" runat="server" Width="200px" Height="48px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox><br />
                                <asp:Label ID="Label40" runat="server" ForeColor="Red" Text="maks. 100 karakter"></asp:Label></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Provinsi</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:DropDownList ID="consprovince" runat="server" Width="130px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Kota</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:DropDownList ID="conscity" runat="server" Width="130px" CssClass="inpText"></asp:DropDownList></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Kode Pos</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="conspostcode" runat="server" CssClass="inpText" MaxLength="10"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Telepon</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consphone" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Fax</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consfax" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">NPWP</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consnpwp" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Contact Person</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:DropDownList ID="conscpprefix" runat="server" Width="72px" CssClass="inpText"></asp:DropDownList>&nbsp;<asp:TextBox ID="conscpname" runat="server" Width="200px" CssClass="inpText" MaxLength="25"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top; WHITE-SPACE: nowrap">Telepon Contact Person</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="conscpphone" runat="server" Width="125px" CssClass="inpText" MaxLength="25"></asp:TextBox></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top">Catatan</td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px">:</td>
                            <td colspan="2">
                                <asp:TextBox ID="consnote" runat="server" Width="200px" Height="48px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox><br />
                                <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="maks. 50 karakter"></asp:Label></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px"></td>
                            <td style="VERTICAL-ALIGN: top"></td>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 8px"></td>
                            <td colspan="2"></td>
                            <td style="WIDTH: 30px" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px" align="center" colspan="1"></td>
                            <td style="VERTICAL-ALIGN: middle; HEIGHT: 25px; TEXT-ALIGN: center" align="center" colspan="4">
                                <asp:LinkButton Style="MARGIN-RIGHT: 20px" ID="lbaddconsignee" runat="server" Font-Bold="True">[ Add To List ]</asp:LinkButton><asp:LinkButton ID="lbcancelconsignee" runat="server" Font-Bold="True">[ Cancel & Close ]</asp:LinkButton></td>
                            <td style="WIDTH: 30px" align="center" colspan="1"></td>
                        </tr>
                        <tr>
                            <td style="VERTICAL-ALIGN: top; WIDTH: 30px" align="left" colspan="1"></td>
                            <td style="VERTICAL-ALIGN: top" align="left" colspan="4">
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftecons1" runat="server" TargetControlID="conspostcode" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftecons2" runat="server" TargetControlID="consphone" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftecons3" runat="server" TargetControlID="consfax" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftecons4" runat="server" TargetControlID="consnpwp" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftecons5" runat="server" TargetControlID="conscpphone" ValidChars="0123456789"></ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                            <td style="WIDTH: 30px" align="left" colspan="1"></td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MPEConsignee" runat="server" BackgroundCssClass="modalBackground" PopupControlID="PanelConsignee" Drag="True" DropShadow="True" TargetControlID="btnConsignee"></ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="btnConsignee" runat="server" Visible="False"></asp:Button>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
