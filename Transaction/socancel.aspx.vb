﻿Imports System.Data
Imports System.Data.SqlClient
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Partial Class Transaction_socancel
    Inherits System.Web.UI.Page
    Dim sSql As String = ""
    Dim CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cmd As New SqlCommand("", conn)
    Dim cKon As New Koneksi
    Dim cFungsi As New ClassFunction
    Dim cProc As New ClassProcedure

    Private Sub InitDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlCabang, sSql)
            Else
                FillDDL(ddlCabang, sSql)
                'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
                'CabangNya.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlCabang, sSql)
            'CabangNya.Items.Add(New ListItem("ALL", "ALL"))
            'CabangNya.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitfDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fddlCabang, sSql)
            Else
                FillDDL(fddlCabang, sSql)
                fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                fddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fddlCabang, sSql)
            fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            fddlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub showMessage(ByVal isi As String, ByVal judul As String)
        isiPesan.Text = isi
        captionPesan.Text = judul
        'btnExtenderValidasi.Visible = True
        'PanelValidasi.Visible = True
        'mpeMsg.Show()
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchSDO")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchSDO") = sqlSearch
            'If Session("branch_code") = "" Then
            '    Response.Redirect("~/Other/login.aspx")
            'End If
            Response.Redirect("~/Transaction/socancel.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")
        Page.Title = CompnyName & " - Sales Order Cancel"

        If Not Page.IsPostBack Then
            InitDDLBranch() : InitfDDLBranch()
            tglAwal.Text = Format(GetServerTime().AddDays(-1), "dd/MM/yyyy")
            tglAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            ddlFilter.SelectedIndex = 1
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub 

    Private Sub BindDataCancel()
        Try
            Dim dat1 As Date = CDate(toDate(tglAwal.Text))
            Dim dat2 As Date = CDate(toDate(tglAkhir.Text))
        Catch ex As Exception
            showMessage("Invalid filter date!!", CompnyName & " - WARNING") : Exit Sub
        End Try
        If CDate(toDate(tglAwal.Text)) > CDate(toDate(tglAkhir.Text)) Then
            showMessage("Start date must be less or equal than End date!!", CompnyName & " - WARNING") : Exit Sub
        End If
        sSql = "select m.ordermstoid orderoid, m.orderno, m.trncustoid custoid, c.custname, m.delivdate deliverydate, convert(varchar(10),m.trnorderdate,101) orderdatecancel, m.canceluser orderusercancel, m.cancelnote ordernotecancel, m.trnorderstatus orderstatus, m.periodacctg From ql_trnordermst m inner join ql_mstcust c on m.trncustoid=c.custoid and m.branch_code = c.branch_code Where m.cmpcode = '" & CompnyCode & "' AND m.orderno like '%" & TcharNoTrim(txtFilter.Text) & "%' AND m.trnorderstatus='Canceled' AND NOT EXISTS (select ordermstoid from QL_trnjualmst jm WHere jm.branch_code=m.branch_code AND jm.ordermstoid=m.ordermstoid)"
        If CBperiod.Checked = True Then
            sSql &= " AND convert(char(10), m.canceltime,101) between '" & Format(CDate(toDate(tglAwal.Text)), "MM/dd/yyyy") & "' AND '" & Format(CDate(toDate(tglAkhir.Text)), "MM/dd/yyyy") & "'"
        End If

        If fddlCabang.SelectedValue <> "ALL" Then
            sSql &= " and m.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If

        Dim dtTarget As DataTable = cKon.ambiltabel(sSql, "ql_trnordermst")
        gvCancelBPM.DataSource = dtTarget : gvCancelBPM.DataBind()
        Session("ql_trnordermst") = dtTarget
        gvCancelBPM.Visible = True : gvCancelBPM.SelectedIndex = -1
    End Sub

    Private Sub bindDataSO()
        sSql = "SELECT so.ordermstoid orderoid, so.orderno, so.trnorderdate, so.trncustoid custoid, s.custname, so.delivdate deliverydate From ql_trnordermst so, ql_mstcust s where so.trncustoid=s.custoid and so.cmpcode=s.cmpcode and so.trnorderstatus='Approved' and so.orderno like '%%' and so.ordermstoid in (select trnordermstoid from QL_trnorderdtl where(trnorderdtlstatus in ('Completed','In Process'))) ORDER BY so.trnorderdate DESC, so.orderno DESC "
        FillGV(gvBPM2, sSql, "ql_trnordermst")
        gvBPM2.Visible = True
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'btnExtenderValidasi.Visible = False
        'PanelValidasi.Visible = False
        'mpeMsg.Hide()
        Dim x As Integer = 123
        cProc.SetModalPopUpExtender(btnExtenderValidasi, PanelValidasi, mpeMsg, False)
        'isiPesan.Text = "tes"
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFilter.SelectedValue <> "spmno" Then
            tglAwal.Enabled = True
            tglAwal.CssClass = "inpText"
            tglAkhir.Enabled = True
            tglAkhir.CssClass = "inpText"
        Else
            tglAwal.Enabled = False
            tglAwal.CssClass = "inpTextDisabled"
            tglAkhir.Enabled = False
            tglAkhir.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub imbSearchBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchBPM.Click
        sSql = "Select m.ordermstoid orderoid, m.orderno, m.trncustoid custoid, c.custname, m.delivdate deliverydate, convert(varchar(10),m.trnorderdate,101) orderdatecancel, m.canceluser orderusercancel, m.cancelnote ordernotecancel, m.trnorderstatus orderstatus, m.periodacctg From ql_trnordermst m inner join ql_mstcust c on m.trncustoid=c.custoid and m.branch_code = c.branch_code Where m.cmpcode = '" & CompnyCode & "' And m.branch_code = '" & ddlCabang.SelectedValue & "' and m.orderno like '%" & Tchar(txtorderNo.Text.Trim) & "%' and m.trnorderstatus in ('Approved') and m.ordermstoid in (select trnordermstoid from QL_trnorderdtl Where (trnorderdtlstatus in ('','In Process')) OR trnorderdtlstatus NOT IN ('Completed')) and NOT EXISTS (SELECT ordermstoid FROM QL_trnjualmst jm WHERE jm.ordermstoid = m.ordermstoid AND jm.branch_code=m.branch_code)"
        Dim dtab As DataTable = cKon.ambiltabel(sSql, "order")
        gvBPM2.DataSource = dtab
        gvBPM2.DataBind()
    End Sub

    Protected Sub imbEraseBPM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseBPM.Click
        txtorderNo.Text = "" : ordermstoid.Text = ""
        custname.Text = "" : netto.Text = ""
        gvBPM2.Visible = False
        gvBPM2.SelectedIndex = -1
        gv_item.Visible = False
        lbl_item.Visible = False
    End Sub

    Protected Sub gvBPM2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBPM2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            'e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub gvBPM2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBPM2.SelectedIndexChanged
        ordermstoid.Text = gvBPM2.SelectedDataKey.Item("orderoid")
        txtorderNo.Text = gvBPM2.SelectedDataKey.Item("orderno")
        custoid.Text = gvBPM2.SelectedDataKey.Item("custoid")
        custname.Text = gvBPM2.SelectedDataKey.Item("custname")
        netto.Text = ToMaskEdit(GetStrData("select (sum((trnorderdtlqty-isnull(orderdelivqty,0))*(trnorderprice-((trnamountbruto-trnamountdtl)/trnorderdtlqty)))) + ((sum((trnorderdtlqty-isnull(orderdelivqty,0))*(trnorderprice-((trnamountbruto-trnamountdtl)/trnorderdtlqty))))*trntaxpct/100) From ql_trnorderdtl d inner join ql_trnordermst m on d.trnordermstoid=m.ordermstoid AND d.branch_code=m.branch_code Where m.ordermstoid=" & ordermstoid.Text & " AND m.branch_code='" & ddlCabang.SelectedValue & "' group by trntaxpct"), 3)
        gvBPM2.Visible = False
        gvBPM2.SelectedIndex = -1
        lbl_item.Visible = True
        bindItem()
    End Sub

    Protected Sub gvBPM2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBPM2.PageIndexChanging
        gvBPM2.PageIndex = e.NewPageIndex
        gvBPM2.Visible = True
        gvBPM2.DataSource = Session("ql_trnordermst")
        gvBPM2.DataBind()
    End Sub

    Protected Sub btnCloseBPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseBPM.Click
        If ordermstoid.Text.Trim = "" Or txtorderNo.Text = "" Then
            showMessage("Silahkan pilih SO !<br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        If txtorderNote.Text.Trim = "" Then
            showMessage("Silahkan Isi Note Cancellation dahulu !<br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        sSql = "select count(*) From ql_trnsjjualmst Where trnsjjualstatus in ('In Process','In Approval') and orderno = '" & txtorderNo.Text & "' and branch_code = '" & ddlCabang.SelectedValue & "'"
        Dim delivprocess As Decimal = ToDouble(GetStrData(sSql))
        If delivprocess > 0 Then
            sSql = "select trnsjjualno From ql_trnsjjualmst Where trnsjjualstatus in ('In Process','In Approval') and orderno = '" & txtorderNo.Text & "' and branch_code = '" & ddlCabang.SelectedValue & "'"
            Dim NO_DO As String = GetStrData(sSql)
            showMessage("SO No " & txtorderNo.Text & " Sudah dibuat SDO/SDO Project dalam status ('In Process','In Approval')..!!<br/> Silahkan process terlebih dahulu SDO/SDO No " & NO_DO & " !<br />", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        cmd.Transaction = objTrans

        Try
            sSql = "Update QL_trnordermst set trnorderstatus='Canceled', canceltime=CURRENT_TIMESTAMP, canceluser='" & Session("UserID") & "', cancelnote='" & Tchar(txtorderNote.Text) & "' Where ordermstoid=" & ordermstoid.Text & " AND branch_code = '" & ddlCabang.SelectedValue & "'"
            cmd.CommandText = sSql : cmd.ExecuteNonQuery()

            'update creditlimitusage dari customer tersebut
            sSql = "update ql_mstcust set custcreditlimitusagerupiah=abs(custcreditlimitusagerupiah)-" & ToDouble(netto.Text) & " Where custoid=" & custoid.Text & " AND branch_code = '" & ddlCabang.SelectedValue & "'"
            cmd.CommandText = sSql : cmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            cmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR")
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\socancel.aspx?awal=true")
    End Sub

    Protected Sub gvCancelBPM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCancelBPM.PageIndex = e.NewPageIndex
        BindDataCancel()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("~\Transaction\socancel.aspx")
    End Sub

    Protected Sub gvCancelBPM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCancelBPM.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy HH:mm:ss")
            e.Row.Cells(1).Text = Format(CDate(e.Row.Cells(1).Text), "dd/MM/yyyy")
        End If
    End Sub

    Private Sub bindItem()
        sSql = " select a.itemoid,item.itemcode,item.itemdesc,(trnorderdtlqty-orderdelivqty) qty, (select gendesc from QL_mstgen where gengroup='itemunit' and genoid = a.trnorderdtlunitoid) unit From QL_trnorderdtl a inner join QL_trnordermst b on a.trnordermstoid = b.ordermstoid AND a.branch_code = b.branch_code inner join QL_MSTITEM item on item.itemoid = a.itemoid Where b.orderno='" & txtorderNo.Text & "' and a.branch_code='" & ddlCabang.SelectedValue & "'"
        FillGV(gv_item, sSql, "ql_trnordermstdtl")
        gv_item.Visible = True
        Label6.Visible = True
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearch.Click
        BindDataCancel()
    End Sub

    Protected Sub BtnViewALL_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewALL.Click
        CBperiod.Checked = False
        BindDataCancel()
    End Sub
End Class
