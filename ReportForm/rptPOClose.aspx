<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPOClose.aspx.vb" Inherits="ReportForm_rptPOClose"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<table align="left" border="0" style="width: 976px; height: 168px">
        <tr>
            <td colspan="3" rowspan="3" align="center">                
                <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelJudul" runat="server" CssClass="Title" Font-Bold="True" ForeColor="Maroon"
                                Text=".: Laporan PO Close" Font-Names="Verdana" Font-Size="21px"></asp:Label></th>
                    </tr>
                </table>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD align=center colSpan=3 rowSpan=1><TABLE><TBODY><TR><TD align=left>Type</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddltype" runat="server" CssClass="inpText" __designer:wfdid="w225" OnSelectedIndexChanged="ddltype_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left>Tanggal PO</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="range1" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w279"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w280"></asp:ImageButton>&nbsp;to <asp:TextBox id="range2" runat="server" Width="50px" CssClass="inpText" __designer:wfdid="w281"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w282"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w283"></asp:Label></TD></TR><TR><TD align=left>Supplier</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="suppname" runat="server" Width="170px" CssClass="inpText" __designer:wfdid="w286"></asp:TextBox> <asp:ImageButton id="searchsupp" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w287"></asp:ImageButton>&nbsp;<asp:ImageButton id="erasesupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w288"></asp:ImageButton>&nbsp;<asp:Label id="suppoid" runat="server" __designer:wfdid="w289" Visible="False"></asp:Label> </TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVSupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w290" Visible="False" DataKeyNames="suppoid,suppcode,suppname" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Kode Supplier">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama Supplier">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label15" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>No. Po</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="pono" runat="server" Width="170px" CssClass="inpText" __designer:wfdid="w257"></asp:TextBox>&nbsp;<asp:ImageButton id="searchpo" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle" __designer:wfdid="w258"></asp:ImageButton>&nbsp;<asp:ImageButton id="erasepo" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w259"></asp:ImageButton>&nbsp;<asp:Label id="pooid" runat="server" __designer:wfdid="w260" Visible="False"></asp:Label></TD></TR><TR><TD align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 0px; POSITION: relative; TOP: 2px; BACKGROUND-COLOR: transparent" id="GVPo" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w284" Visible="False" DataKeyNames="trnbelimstoid,trnbelipono,trnbelipodate,suppoid,suppname" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" EmptyDataText="No data in database." PageSize="14" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="XX-Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelipono" HeaderText="Nomor PO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal PO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="sPic" runat="server" Width="38px" Text="PIC" __designer:wfdid="w53" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label4" runat="server" Text=":" __designer:wfdid="w53" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="pic" runat="server" CssClass="inpText" __designer:wfdid="w52" Visible="False"><asp:ListItem>ALL</asp:ListItem>
</asp:DropDownList></TD></TR><TR runat="server" visible="false"><TD align=left>Barang</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="barang" runat="server" Width="170px" CssClass="inpText" __designer:wfdid="w236"></asp:TextBox> <asp:ImageButton id="barangsearch" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px" __designer:wfdid="w237"></asp:ImageButton> <asp:ImageButton id="barangerase" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w238"></asp:ImageButton> <asp:Label id="barangoid" runat="server" __designer:wfdid="w239" Visible="False"></asp:Label></TD></TR><TR id="trbarang" runat="server" visible="false"><TD align=left></TD><TD align=left></TD><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 97px; TOP: 33px; BACKGROUND-COLOR: transparent" id="GVBarang" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w240" Visible="False" DataKeyNames="itemoid,itemcode,itemdesc,Merk" AutoGenerateColumns="False" CellPadding="4" EmptyDataRowStyle-ForeColor="Red" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="XX-Small" ForeColor="Red" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle Wrap="False"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label150" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR id="trmerk" runat="server" visible="false"><TD align=left>Merk&nbsp;:</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="merk" runat="server" Width="169px" CssClass="inpTextDisabled" __designer:wfdid="w241" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnshowprint" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w242"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w243"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w244"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 5px" id="ibClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w245"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w246"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait ....<BR /></SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w247"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:CalendarExtender id="CLE1" runat="server" __designer:wfdid="w248" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range1" PopupButtonID="ImageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CLE2" runat="server" __designer:wfdid="w249" __designer:errorcontrol="'dd/MM/yyyy' could not be set on property 'Format'." TargetControlID="range2" PopupButtonID="ImageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MKE1" runat="server" __designer:wfdid="w250" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range1" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MKE2" runat="server" __designer:wfdid="w251" __designer:errorcontrol="'id-ID' could not be set on property 'CultureName'." TargetControlID="range2" Mask="99/99/9999" ErrorTooltipEnabled="True" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w252"></asp:Label></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="CrystalReportViewer1" runat="server" __designer:wfdid="w285" DisplayGroupTree="False" HasZoomFactorList="False" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasGotoPageButton="False" AutoDataBind="true"></CR:CrystalReportViewer>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnshowprint"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                <contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w270" Visible="False"><TABLE width=250><TBODY><TR><TD style="HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w271"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" __designer:wfdid="w272"></asp:ImageButton></TD><TD colSpan=2><asp:Label id="lblValidasi" runat="server" ForeColor="Red" __designer:wfdid="w277"></asp:Label></TD></TR><TR><TD colSpan=3>trrytrytrt</TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnErrOK" runat="server" ImageUrl="~/Images/ok.png" __designer:wfdid="w278"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" __designer:wfdid="w275" TargetControlID="btnExtender" Drag="True" DropShadow="True" PopupControlID="PanelErrMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" __designer:wfdid="w276" Visible="False"></asp:Button> 
</contenttemplate>
            </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>

