Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Koneksi
    Public xkoneksi As New SqlConnection()
    Public xadapter As New SqlDataAdapter
    Public xcom As New SqlCommand
    Public xset As New Data.DataSet
    Public xreader As SqlDataReader
    Public dv As DataView

    Public Sub New()
        Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        xkoneksi.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("QL_SIP_Conn")
    End Sub

    Public Function ambiltabel(ByVal query As String, ByVal nama As String) As DataTable
        xset.Clear()
        xset = New Data.DataSet
        xadapter = New SqlDataAdapter(query, xkoneksi)
        xadapter.Fill(xset, nama)
        Return xset.Tables(nama)
    End Function
    Public Function ambiltabel2(ByVal query As String, ByVal nama As String) As DataTable
        'xset.Clear()
        'xset = New Data.DataSet
        xadapter = New SqlDataAdapter(query, xkoneksi)
        xadapter.Fill(xset, nama)
        Return xset.Tables(nama)
    End Function

    Public Function ambildataset(ByVal query As String, ByVal nama As String) As DataSet
        xset.Clear()
        xset = New DataSet
        xadapter = New SqlDataAdapter(query, xkoneksi)
        xadapter.Fill(xset, nama)
        Return xset
    End Function

    Public Function ambilscalar(ByVal query As String) As Object
        xcom = New SqlCommand(query, xkoneksi)
        If xkoneksi.State = ConnectionState.Closed Then
            xkoneksi.Open()
        End If
        Dim xint As Integer
        Return xcom.ExecuteScalar

        xkoneksi.Close()
    End Function

    Public Function ambilreader(ByVal query As String) As SqlDataReader
        xcom = New SqlCommand(query, xkoneksi)
        If xkoneksi.State = ConnectionState.Closed Then
            xkoneksi.Open()
        End If
        xreader = xcom.ExecuteReader
        Return xreader
        xreader.Close()
        xkoneksi.Close()
    End Function

    Public Sub DDL(ByVal query As String)

        xcom = New SqlCommand(query, xkoneksi)
        If xkoneksi.State = ConnectionState.Closed Then
            xkoneksi.Open()
        End If
        Dim xint As Integer = xcom.ExecuteNonQuery
        xkoneksi.Close()
    End Sub

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer, ByVal gvName As GridView) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvName.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    sReturn = cbcheck
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Sub InsertHistoryLog(ByVal sdsHistory As SqlDataSource, ByVal sdsOid As SqlDataSource, ByVal cmpcode As String, ByVal HISTORYOID As String, ByVal USERID As String, ByVal ACTIVITY As String, ByVal TABLENAME As String, ByVal OID As String, ByVal ACTIVITYDATE As Date, ByVal NOTE As String, ByVal lastoid As String, ByVal historytable As String)
        With sdsHistory
            .InsertParameters("cmpcode").DefaultValue = cmpcode
            .InsertParameters("HISTORYOID").DefaultValue = HISTORYOID
            .InsertParameters("USERID").DefaultValue = USERID
            .InsertParameters("ACTIVITY").DefaultValue = ACTIVITY
            .InsertParameters("TABLENAME").DefaultValue = TABLENAME
            .InsertParameters("OID").DefaultValue = OID
            .InsertParameters("ACTIVITYDATE").DefaultValue = ACTIVITYDATE
            .InsertParameters("NOTE").DefaultValue = NOTE
        End With
        sdsHistory.Insert()
        With sdsOid
            .UpdateParameters("lastoid").DefaultValue = lastoid
            .UpdateParameters("tablename").DefaultValue = historytable
            .UpdateParameters("cmpcode").DefaultValue = cmpcode
        End With
        sdsOid.Update()

    End Sub

    Function checkSpecialPermission(ByVal PagePath As String, ByVal dtAccess As DataTable) As Boolean
        Dim sTemp As String
        Dim sFill() As String
        Dim sepp As Char = "/"
        sFill = PagePath.Split(sepp)
        sTemp = sFill(sFill.Length - 1)
        dv = dtAccess.DefaultView 'Session("SpecialAccess").defaultview
        dv.RowFilter = "FORMADDRESS LIKE '%" & sTemp & "%'"
        If dv.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class

