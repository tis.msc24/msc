Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class AdjHpp
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Private cRate As New ClassRate
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function GetTransID() As String
        Return (Eval("itemoid"))
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("NewHpp") = ToDouble(GetTextBoxValue(row.Cells(4).Controls)) 
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("NewHpp") = ToDouble(GetTextBoxValue(row.Cells(4).Controls)) 
                                        dtView2(0)("keterangan") = GetTextBoxValue(row.Cells(5).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("NewHpp") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(5).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CreateTblDetail() '
        Dim dtlTable As DataTable = New DataTable("QL_trnmstitemdtl")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("Hpp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("NewHpp", Type.GetType("System.Double")) 
        dtlTable.Columns.Add("keterangan", Type.GetType("System.String"))
        dtlTable.Columns.Add("SaldoAkhir", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub BindData()
        sSql = "Select * From (Select i.cmpcode, i.itemoid, i.itemcode, i.itemdesc, 'BUAH' satuan3, i.merk, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, HPP From ql_mstitem i ) dt WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
        If JenisDDL.SelectedValue <> "ALL" Then
            sSql &= " AND stockflag='" & JenisDDL.SelectedValue & "'"
        End If 
        sSql &= " Order BY itemcode Desc"
        Session("TblMst") = cKon.ambiltabel(sSql, "ql_mst")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitJenisKatalog()
        sSql = "Select DISTINCT stockflag,JenisNya From (Select stockflag,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From ql_mstitem i WHERE itemflag='AKTIF') it"
        FillDDL(ddlType, sSql)
        ddlType.Items.Add(New ListItem("ALL", "ALL"))
        ddlType.SelectedValue = "ALL"
    End Sub

    Private Sub InitFilterJenis()
        sSql = "Select DISTINCT stockflag, JenisNya From (Select stockflag,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From ql_mstitem i WHERE itemflag='AKTIF') it"
        FillDDL(JenisDDL, sSql)
        JenisDDL.Items.Add(New ListItem("ALL", "ALL"))
        JenisDDL.SelectedValue = "ALL"
    End Sub

    Private Sub BindMaterial()
        sSql = "SELECT *, (HPP*Isnull(StokAkhir,0.00))+(HPP*Isnull(StokAkhir,0.00))/ (ISNULL(StokAkhir,0.00)+0) RumusHpp FROM (Select 'False' AS CheckValue, i.itemoid, i.itemcode, i.itemdesc, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, i.HPP, Case Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) When 0.00 Then 1 Else Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) END StokAkhir, 0.00 NewHpp, '' keterangan, Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) SaldoAkhir From ql_mstitem i) cok WHERE " & FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%' AND HPP<=0 Order BY itemcode"
        Dim dtMat As DataTable = cKon.ambiltabel(sSql, "ql_mstitem")
        If dtMat.Rows.Count > 0 Then
            Session("TblMat") = dtMat
            Session("TblMatView") = dtMat
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            showMessage("Katalog data can't be found!", 2)
        End If
    End Sub

    Private Sub FillTextBox(ByVal sNo As String)
        Try
            sSql = "SELECT *, (HPP*Isnull(StokAkhir,0.00))+(HPP*Isnull(StokAkhir,0.00))/ (ISNULL(StokAkhir,0.00)+0) RumusHpp FROM (Select ROW_NUMBER() OVER (ORDER BY itemoid ASC) AS seq, i.itemoid, i.itemcode, i.itemdesc, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, i.HPP, Case Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) When 0.00 Then 1 Else Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) END StokAkhir, 0.00 NewHpp, '' keterangan, Isnull((SELECT SUM(con.qtyin)-SUM(qtyOut) FROM QL_conmtr con Where con.refoid=i.itemoid AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'),0.00) SaldoAkhir From ql_mstitem i) cok WHERE cok.itemoid=" & sNo
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("TblDtl") = dt
            gvFindCrd.DataSource = Session("TblDtl")
            gvFindCrd.DataBind()
            gvFindCrd.Visible = True
            imbSave.Visible = False : BtnAddItem.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub PrintReport()
        Try
            Dim sWhere As String = "WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
            If JenisDDL.SelectedValue <> "ALL" Then
                sWhere &= " AND stockflag='" & JenisDDL.SelectedValue & "'"
            End If
            sWhere &= " Order BY itemdesc"
            report.Load(Server.MapPath(folderReport & "RptPriceCabang.rpt"))
            report.SetParameterValue("sWhere", sWhere)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "RptPriceCabang" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            Catch ex As Exception
                report.Close()
                report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Master\AdjHpp.aspx") 
        End If
        Page.Title = CompnyName & " - Master Price Cabang"
        upduser.Text = Session("UserID")
        updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
        Session("oid") = Request.QueryString("oid") 

        If Not Page.IsPostBack Then
            InitJenisKatalog() : InitFilterJenis()
            If (Session("oid") <> Nothing And Session("oid") <> "") Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub gvFindCrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFindCrd.PageIndexChanging
        UpdateCheckedMat()
        gvFindCrd.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("TblCrd")
        gvFindCrd.DataSource = dtDtl : gvFindCrd.DataBind()
    End Sub

    Protected Sub gvFindCrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFindCrd.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub lkbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
        'Dim dvDel As DataView = dtDtl.DefaultView
        'dvDel.RowFilter = "seq=" & sender.ToolTip
        'If dvDel.Count > 0 Then
        '    dvDel(0).Delete()
        'End If
        'dvDel.RowFilter = ""
        'dtDtl.AcceptChanges()

        'For C1 As Integer = 0 To dtDtl.Rows.Count - 1
        '    dtDtl.Rows.Item(C1)("seq") = C1 + 1
        'Next

        'Session("TblDtl") = dtDtl 
    End Sub 

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        'UpdateCheckedMat()
        Dim sMsg As String = "", objTable As DataTable = Session("TblDtl"), sParam As String = "", SaldoAkhir As Double
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No detail data.<BR>"
        Else
            If objTable.Rows.Count < 1 Then
                sMsg &= "- No detail data.<BR>"
            End If
        End If 

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                sSql = "Select ISNULL(SUM(qtyIn),0.00)-ISNULL(SUM(qtyout),0.00) from QL_conmtr Where refoid=" & Integer.Parse(objTable.Rows(C1)("itemoid")) & " AND periodacctg='" & GetPeriodAcctg(GetServerTime()) & "'"
                xCmd.CommandText = sSql : SaldoAkhir = xCmd.ExecuteScalar
                If ToDouble(SaldoAkhir) <= 0 Then
                    SaldoAkhir = 1
                End If
                sSql = "Update ql_mstitem set HPP=((" & objTable.Rows(C1)("HPP") & " * " & SaldoAkhir & ")+(" & objTable.Rows(C1)("NewHpp") & " * " & SaldoAkhir & "))/" & SaldoAkhir & " Where itemoid=" & Integer.Parse(objTable.Rows(C1)("itemoid"))
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next
            objTrans.Commit() : conn.Close() 
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1) : Exit Sub
        End Try
        Response.Redirect("~\Master\AdjHpp.aspx?awal=true")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("~\Master\AdjHpp.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        'If Session("TblDtl") Is Nothing Then
        '    showMessage("Please define detail data first!", 2, "")
        '    Exit Sub
        'End If
        'If UpdateCheckedMat() Then
        '    ShowReport()
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        JenisDDL.SelectedValue = "ALL"
        FilterTextMst.Text = ""
        BindData()
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        'UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dv As DataView = Session("TblMst").DefaultView
            If dv.Count < 1 Then
                dv.RowFilter = ""
                showMessage("Please Select data to Print first.", 2)
            Else
                dv.RowFilter = ""
                PrintReport()
            End If
        Else
            showMessage("Please Select data to Print first.", 2)
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        'UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
        End If
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        'UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt As DataTable = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub lkbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("CommandName") = sender.CommandName
        Response.Redirect("~\Master\AdjHpp.aspx?oid=" & sender.ToolTip & "&cashbankstatus=" & Session("CommandName") & "")
    End Sub

    Protected Sub BtnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddItem.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterial()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        Try
            If Not Session("TblMat") Is Nothing Then
                If UpdateCheckedMat() Then
                    Dim dtTbl As DataTable = Session("TblMat")
                    Dim dtView As DataView = dtTbl.DefaultView
                    dtView.RowFilter = "CheckValue='True'"
                    Dim iCheck As Integer = dtView.Count
                    If iCheck > 0 Then
                        If Session("TblDtl") Is Nothing Then
                            CreateTblDetail()
                        End If
                        Dim objTable As DataTable = Session("TblDtl")
                        Dim dv As DataView = objTable.DefaultView
                        Dim objRow As DataRow
                        Dim counter As Integer = objTable.Rows.Count
                        Dim dDiscAmt As Double = 0
                        For C1 As Integer = 0 To dtView.Count - 1
                            dv.RowFilter = "itemoid=" & dtView(C1)("itemoid")
                            If dv.Count > 0 Then
                                dv.AllowEdit = True
                                dv(0)("itemoid") = Integer.Parse(dtView(C1)("itemoid"))
                                dv(0)("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                                dv(0)("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                                dv(0)("Hpp") = ToMaskEdit(dtView(C1)("Hpp"), 3)
                                dv(0)("NewHpp") = ToMaskEdit(dtView(C1)("NewHpp"), 3)
                                dv(0)("keterangan") = Tchar(dtView(C1)("keterangan"))
                                dv(0)("SaldoAkhir") = ToMaskEdit(dtView(C1)("SaldoAkhir"), 3)
                            Else
                                counter += 1
                                objRow = objTable.NewRow()
                                objRow("seq") = counter
                                objRow("itemoid") = Integer.Parse(dtView(C1)("itemoid")) 
                                objRow("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                                objRow("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                                objRow("hpp") = Tchar(dtView(C1)("hpp").ToString)
                                objRow("NewHpp") = ToMaskEdit(dtView(C1)("NewHpp"), 3)
                                objRow("keterangan") = Tchar(dtView(C1)("keterangan"))
                                objRow("SaldoAkhir") = ToMaskEdit(dtView(C1)("SaldoAkhir"), 3)
                                objTable.Rows.Add(objRow)
                            End If
                    dv.RowFilter = ""
                        Next
                        dtView.RowFilter = ""
                        Session("TblDtl") = objTable
                        gvFindCrd.DataSource = objTable
                        gvFindCrd.DataBind()
                        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                        'ClearDetail()
                    Else
                        Session("WarningListMat") = "Please select material to add to list!"
                        showMessage(Session("WarningListMat"), 2)
                        Exit Sub
                    End If
                End If
            Else
                Session("WarningListMat") = "Please show some material data first!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try

    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show() 
    End Sub
#End Region

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub pExpedisi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If UpdateCheckedMat2() Then
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
End Class