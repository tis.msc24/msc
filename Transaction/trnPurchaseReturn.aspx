<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPurchaseReturn.aspx.vb" Inherits="Transaction_trnReturPI" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Purchase Return" Width="222px"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td style="height: 3px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px; vertical-align: text-top;">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; List Of Purchase Return</span>
                            <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<TABLE style="WIDTH: 961px"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="DdlCabang" runat="server" Width="123px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="123px" CssClass="inpText"><asp:ListItem Value="a.trnbelireturno">No. Return</asp:ListItem>
<asp:ListItem Value="d.suppname">Supplier</asp:ListItem>
<asp:ListItem Value="a.refnotaretur">No. Ref</asp:ListItem>
</asp:DropDownList><SPAN style="COLOR: #ff0000">&nbsp;<asp:TextBox id="tbfilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox>&nbsp;</SPAN></TD></TR><TR><TD><asp:CheckBox id="CbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD>:</TD><TD style="VERTICAL-ALIGN: top; HEIGHT: 22px"><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;to <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top"></asp:ImageButton>&nbsp;<SPAN style="FONT-SIZE: 11px; COLOR: red"><SPAN style="COLOR: red">(dd/MM/yyyy)</SPAN></SPAN></TD></TR><TR><TD>Status</TD><TD>:</TD><TD><asp:DropDownList id="ddlfilterstatus" runat="server" Width="123px" CssClass="inpText">
                                                        <asp:ListItem Selected="True" Value="All">All</asp:ListItem>
                                                        <asp:ListItem>In Process</asp:ListItem>
                                                        <asp:ListItem>In Approval</asp:ListItem>
                                                        <asp:ListItem>Approved</asp:ListItem>
                                                    </asp:DropDownList>&nbsp;<asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="tbperiodstart" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="tbperiodend" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender10" runat="server" TargetControlID="tbperiodstart" PopupButtonID="ibperiodstart" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="tbperiodend" PopupButtonID="ibperiodend" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> </TD></TR><TR><TD colSpan=3><asp:GridView id="GVTrn" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="trnbeliReturmstoid,branch_code" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelireturno" HeaderText="No. Return">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Date">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="refnotaretur" HeaderText="No. Ref">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelino" HeaderText="No. Invoice">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
<ItemStyle Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbelinetto" DataFormatString="{0:#,##0.00}" HeaderText="Total Retur">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
                                                                    <asp:LinkButton ID="lbprint" runat="server" ToolTip='<%# String.Format("{0},{1}",Eval("trnbeliReturmstoid"),Eval("trnbelireturno")) %>' Font-Underline="True" OnClick="lbprint_Click">Print</asp:LinkButton>
                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                            <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!"></asp:Label>
                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="GVTrn"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span style="font-weight: bold; font-size: 12px">
                                <img align="absMiddle" alt="" src="../Images/corner.gif" /> Form of Purchase Return</span> <strong><span style="font-size: 9pt">:.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=3><TABLE width="100%"><TBODY><TR><TD id="TD5" colSpan=9 Visible="false"><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information"></asp:Label> <asp:Label id="masterstate" runat="server" Visible="False">new</asp:Label> </TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD colSpan=9><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="returndate" PopupButtonID="btnreturndate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender30" runat="server" TargetControlID="returndate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="discheader" MaskType="Number" Mask="999,999,999.99" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Cabang</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:DropDownList id="CabangNya" runat="server" Width="150px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 107px"><asp:Label id="paytype" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD colSpan=4></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">No. Return</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="noreturn" runat="server" Width="150px" CssClass="inpTextDisabled" MaxLength="50" Enabled="False"></asp:TextBox> <asp:Label id="ReturOid" runat="server" Visible="False" __designer:wfdid="w1"></asp:Label></TD><TD style="WIDTH: 107px">Return Date</TD><TD>:</TD><TD colSpan=4><asp:TextBox id="returndate" runat="server" Width="70px" CssClass="inpTextDisabled" MaxLength="10" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnreturndate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label8" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt" id="trtr" runat="server" visible="false"><TD style="WIDTH: 131px">No. TR <asp:Label id="Label13" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD style="FONT-SIZE: 8pt; COLOR: #000099">:</TD><TD style="FONT-SIZE: 8pt; WIDTH: 240px"><asp:TextBox id="notr" runat="server" Width="125px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton style="HEIGHT: 16px" id="btnsearchtr" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnerasetr" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton><asp:Label id="trnopo" runat="server" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 107px">TR&nbsp;Date</TD><TD style="FONT-SIZE: 8pt">:</TD><TD style="FONT-SIZE: 8pt" colSpan=4><asp:TextBox id="trdate" runat="server" Width="70px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False"></asp:TextBox> <asp:Label id="Label6" runat="server" ForeColor="Red" Text="(dd/mm/yyyy)"></asp:Label>&nbsp;&nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Supplier</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="suppname" runat="server" Width="150px" CssClass="inpText" MaxLength="40"></asp:TextBox> <asp:ImageButton style="HEIGHT: 16px" id="btnsuppliersearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnerasesupplier" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; </TD><TD style="WIDTH: 107px">Type Retur</TD><TD>:</TD><TD colSpan=4><asp:DropDownList id="TypeNya" runat="server" CssClass="inpText" AutoPostBack="True"><asp:ListItem Value="NORMAL">Potong Invoice</asp:ListItem>
<asp:ListItem Value="JADI DP">Jadikan DP</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px"><asp:Label id="suppoid" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD colSpan=7><asp:GridView id="GVTr" runat="server" Width="101%" ForeColor="#333333" Visible="False" DataKeyNames="suppoid,suppcode,suppname,suppaddr" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code ">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<ControlStyle Width="100px"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="temptotalamount" runat="server" Text="0" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">No. Invoice</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="invoiceno" runat="server" Width="150px" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:ImageButton style="WIDTH: 16px" id="btnInvoicesearch" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseInvoice" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;&nbsp; </TD><TD style="WIDTH: 107px" id="Td9" runat="server" visible="true">No. Ref</TD><TD id="Td10" runat="server" visible="true">:</TD><TD id="Td11" colSpan=4 runat="server" visible="true"><asp:TextBox id="noref" runat="server" Width="125px" CssClass="inpText" MaxLength="20"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt" id="cariIv" runat="server" visible="false"><TD style="WIDTH: 131px"><asp:Label id="troid" runat="server"></asp:Label><asp:Label id="trnosj" runat="server" Visible="False"></asp:Label> <asp:Label id="LbIDSJ" runat="server" Visible="False"></asp:Label> <asp:Label id="trnodp" runat="server" Visible="False"></asp:Label> <asp:Label id="mCabang" runat="server" Visible="False"></asp:Label><asp:Label id="qtyPo" runat="server" Visible="False"></asp:Label></TD><TD>&nbsp;</TD><TD colSpan=7><asp:GridView id="GVInv" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="trnbelimstoid,trnbelino,trnsjbelino,trnbelipono,trnpaytype,trntaxpct,trnsjbelioid,qtyitem,branch_code,AmtSisa,ToBranch" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True" AllowPaging="True" PageSize="8" OnPageIndexChanging="GVInv_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelino" HeaderText="Invoice No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelino" HeaderText="SJ No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No.">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="branch_code" HeaderText="Cabang" Visible="False"></asp:BoundField>
<asp:BoundField DataField="ToBranch" HeaderText="ToBranch" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="StatusPI" HeaderText="Status PI">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="AmtSisa" HeaderText="AmtSisa" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label15" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="tempdisc" runat="server" Text="0" Visible="False"></asp:Label><asp:Label id="taxpct" runat="server" Visible="False">0</asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Total Return</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="grossreturn" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px">Total Disc. Detail </TD><TD>:</TD><TD colSpan=4><asp:TextBox id="discamount" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox>&nbsp;<asp:Label id="AmtSisa" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Disc. Header</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:DropDownList id="discheadertype" runat="server" Width="50px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False">
                                                                                <asp:ListItem Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="discheader" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="15" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px">Total Voucher</TD><TD>:</TD><TD colSpan=4><asp:TextBox id="voucher" runat="server" Width="125px" CssClass="inpTextDisabled" Enabled="False">0.00</asp:TextBox>&nbsp;&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Total Disc. Header</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="totaldischeader" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px">Status</TD><TD>:</TD><TD colSpan=4><asp:TextBox id="status" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">In Process</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px" id="TD13" runat="server" Visible="false"><asp:Label id="Label5" runat="server" Width="73px">Tax Amount</asp:Label></TD><TD id="TD12" runat="server" Visible="false"></TD><TD style="WIDTH: 240px" id="TD4" runat="server" Visible="false"><asp:TextBox id="taxamount" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px"></TD><TD></TD><TD colSpan=4></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Biaya Expedisi</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="ekspedisi" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;/ Unit</TD><TD style="WIDTH: 107px">Total Amount</TD><TD>:</TD><TD colSpan=4><asp:TextBox id="totalamount" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px; HEIGHT: 21px">Note</TD><TD style="HEIGHT: 21px">:</TD><TD style="HEIGHT: 21px" colSpan=7><asp:TextBox id="note" runat="server" Width="500px" CssClass="inpText" MaxLength="200"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD id="Td24" colSpan=9 Visible="false"><asp:LinkButton id="lbviewinfo" runat="server" CssClass="submenu" Font-Size="Small" Visible="False">Information</asp:LinkButton><asp:Label id="Label3" runat="server" ForeColor="#585858" Text="|" Visible="False"></asp:Label><asp:Label id="Label4" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Location</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:DropDownList id="fromlocation" runat="server" CssClass="inpText"></asp:DropDownList> <asp:Label id="trnbelidtloid" runat="server" Visible="False"></asp:Label> <asp:Label id="unitseq" runat="server" Visible="False"></asp:Label> <asp:Label id="detailstate" runat="server" Visible="False">new</asp:Label></TD><TD style="WIDTH: 107px"><asp:Label id="labelseq" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD>&nbsp;</TD><TD></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt" id="trtwr" runat="server" visible="false"><TD style="WIDTH: 131px">Retur Qty</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="gudangreturqty" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> </TD><TD style="WIDTH: 107px"><asp:Label id="Label1" runat="server" Width="105px" Text="PO Price Per Unit" Visible="False"></asp:Label></TD><TD></TD><TD><asp:TextBox id="poprice" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD><asp:Label id="ekspedisidtl" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD><asp:Label id="itemLoc" runat="server" Visible="False"></asp:Label> <asp:Label id="satuan1" runat="server" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Item <asp:Label id="Label14" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="itemname" runat="server" Width="150px" CssClass="inpText" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchitem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btneraseitem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;</TD><TD style="WIDTH: 107px" id="Td25" Visible="false">Quantity</TD><TD id="Td26" Visible="false">:</TD><TD id="Td27" Visible="false"><asp:TextBox id="qty" runat="server" Width="125px" CssClass="inpText" AutoPostBack="True" MaxLength="15">0.00</asp:TextBox>&nbsp;<asp:Label id="unit" runat="server" Visible="False"></asp:Label></TD><TD id="TD17" runat="server" Visible="false">Price Per Unit</TD><TD id="TD14" runat="server" Visible="false"></TD><TD id="TD16" runat="server" Visible="false"><asp:TextBox id="priceperunit" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="15" Enabled="False">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px"><asp:Label id="itemcode" runat="server" Visible="False"></asp:Label> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD><TD></TD><TD colSpan=4><asp:GridView id="GVItem" runat="server" Width="100%" ForeColor="#333333" Visible="False" DataKeyNames="refoid,satuan1,poprice,trnsjbelidtlseq,disc1type,disc1,disc2type,disc2,trnbelidtloid,has_SN,mtrlocoid,ekspedisi,unit,itemcode,qty,itemdesc" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Bold="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qtyrtr" DataFormatString="{0:#,##0.00}" HeaderText="Qty Beli">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SaldoAkhir" DataFormatString="{0:#,##0.00}" HeaderText="Stok Akhir">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD id="Td1" runat="server" Visible="false"></TD><TD id="Td2" runat="server" Visible="false"></TD><TD id="Td3" runat="server" Visible="false"></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Disc. 1</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:DropDownList id="disc1type" runat="server" Width="75px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False">
                                                                                <asp:ListItem Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="disc1" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="15" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px">Disc. Amount 1</TD><TD>:</TD><TD><asp:TextBox id="discamount1" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt" id="trdisc2" runat="server" visible="true"><TD style="WIDTH: 131px">Voucher</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:DropDownList id="disc2type" runat="server" Width="75px" Height="16px" CssClass="inpTextDisabled" AutoPostBack="True" Enabled="False">
                                                                                <asp:ListItem Value="VCR">Voucher</asp:ListItem>
                                                                                <asp:ListItem Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList>&nbsp;<asp:TextBox id="disc2" runat="server" Width="125px" CssClass="inpTextDisabled" AutoPostBack="True" MaxLength="15" Enabled="False">0.00</asp:TextBox></TD><TD style="WIDTH: 107px">Voucher Amount </TD><TD>:</TD><TD><asp:TextBox id="discamount2" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD><TD id="Td6" runat="server" Visible="false">Total Disc + Vcr</TD><TD id="Td7" runat="server" Visible="false">:</TD><TD id="Td8" runat="server" Visible="false"><asp:TextBox id="totaldisc" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px"><asp:Label id="Label16" runat="server" Width="80px" Text="Total Amount"></asp:Label></TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="totalamountdtl" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> </TD><TD style="WIDTH: 107px">Net Amount</TD><TD>:</TD><TD><asp:TextBox id="netamount" runat="server" Width="125px" CssClass="inpTextDisabled" MaxLength="20" Enabled="False">0.00</asp:TextBox> </TD><TD><asp:Label id="Label11" runat="server" Width="91px" Text="Disc. Amount 3" Visible="False"></asp:Label></TD><TD><asp:Label id="Label12" runat="server" Text=":" Visible="False"></asp:Label></TD><TD><asp:TextBox id="discamount3" runat="server" Width="125px" CssClass="inpTextDisabled" Visible="False" MaxLength="20" Enabled="False">0.00</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px">Note</TD><TD>:</TD><TD style="WIDTH: 240px"><asp:TextBox id="notedtl" runat="server" Width="240px" CssClass="inpText" MaxLength="200"></asp:TextBox> </TD><TD id="Td21" align=right Visible="false"><asp:ImageButton style="HEIGHT: 23px" id="btnaddtolist" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsBottom"></asp:ImageButton></TD><TD id="Td22" runat="server" Visible="false">:</TD><TD id="Td23" align=left Visible="false"><asp:ImageButton id="btnclear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsBottom" Visible="false"></asp:ImageButton></TD><TD></TD><TD>&nbsp;</TD><TD><asp:TextBox id="texthasSN" runat="server" Width="29px" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:TextBox> </TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 131px"><asp:Label id="Label9" runat="server" Text="Disc. 3" Visible="False"></asp:Label></TD><TD><asp:Label id="Label10" runat="server" Text=":" Visible="False"></asp:Label></TD><TD style="WIDTH: 240px"><asp:DropDownList id="disc3type" runat="server" Width="50px" CssClass="inpText" Visible="False" AutoPostBack="True">
                                                                                <asp:ListItem Value="AMT">AMT</asp:ListItem>
                                                                                <asp:ListItem Value="PCT">PCT</asp:ListItem>
                                                                            </asp:DropDownList> <asp:TextBox id="disc3" runat="server" Width="125px" CssClass="inpText" Visible="False" AutoPostBack="True" MaxLength="15">0.00</asp:TextBox></TD><TD align=right runat="server" Visible="false"><asp:Label id="Label111" runat="server" Text="Disc. Amount 3" Visible="False"></asp:Label></TD><TD runat="server" Visible="false"></TD><TD align=left runat="server" Visible="false"></TD><TD></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=9><asp:GridView id="GVDtl" runat="server" Width="100%" DataKeyNames="itemoid,disc1type,disc2type,disc3type,disc1,disc2,disc3,disc1amt,disc2amt,disc3amt,totalamt,totaldisc,has_sn,netamt,unitoid,unitseq,trnbelidtloid,itemloc" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="45px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" Width="45px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No ">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemname" HeaderText="Item">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" DataFormatString="{0:#,##0.00}" HeaderText="Qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle Wrap="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="price" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada detail !!"></asp:Label>
                                                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px; HEIGHT: 23px" id="ibsave" runat="server" ImageUrl="~/Images/Save.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancel" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapproval" runat="server" ImageUrl="~/Images/sendapproval.png" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="btnPrint" runat="server" ImageUrl="~/Images/print.png" CausesValidation="False" Visible="False"></asp:ImageButton> <asp:Label id="UpdTime" runat="server" Visible="False" __designer:wfdid="w2"></asp:Label> <asp:Label id="createtime" runat="server" Visible="False" __designer:wfdid="w2"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:FilteredTextBoxExtender id="FTEQty" runat="server" TargetControlID="qty" ValidChars="0123456789.,"></ajaxToolkit:FilteredTextBoxExtender></TD></TR></TBODY></TABLE><asp:UpdatePanel id="uppopupmsg" runat="server"><ContentTemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" TargetControlID="bepopupmsg" Drag="True" PopupDragHandleControlID="lblcaption" BackgroundCssClass="modalBackground" PopupControlID="pnlpopupmsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanelSN" runat="server"><ContentTemplate>
<asp:Panel id="pnlInvnSN" runat="server" Width="600px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="LabelDescSn" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Enter SN of Item"></asp:Label> </TD></TR><TR><TD><asp:Label id="NewSN" runat="server" ForeColor="Red" Text="New SN"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextItem" runat="server" Width="195px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp;<asp:Label id="KodeItem" runat="server" Visible="False"></asp:Label> &nbsp;<asp:Label id="SNseq" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Qty Item&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextQty" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> &nbsp; <asp:TextBox id="TextSatuan" runat="server" Width="69px" CssClass="inpTextDisabled" ReadOnly="True"></asp:TextBox> </TD></TR><TR><TD style="TEXT-ALIGN: left" align=left>Scan Serial Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <asp:TextBox id="TextSN" onkeypress="return EnterEvent(event)" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> &nbsp; <asp:ImageButton style="HEIGHT: 23px" id="EnterSN" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY><CAPTION><P style="TEXT-ALIGN: center">&nbsp;</P></CAPTION><TBODY><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 176px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV style="TEXT-ALIGN: center" id="Div3"><STRONG><asp:Label id="LabelPesanSn" runat="server" Font-Size="Larger" Font-Bold="True" ForeColor="Red"></asp:Label> </STRONG></DIV><DIV></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 82%"><asp:GridView id="GVSN" runat="server" Width="549px" Height="34px" CssClass="MyTabStyle" ForeColor="#333333" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EnableModelValidation="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="itemoid" HeaderText="Itemoid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="SN" HeaderText="Serial Number">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Width="450px"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> </TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="lkbCloseSN" runat="server" Font-Size="X-Small">[ Close ]</asp:LinkButton> &nbsp;&nbsp; <asp:LinkButton id="lkbPilihItem" runat="server" Font-Size="X-Small">[ OK ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupSN" runat="server" TargetControlID="btnHideSN" Drag="True" PopupDragHandleControlID="lblCaptInvtry" BackgroundCssClass="modalBackground" PopupControlID="UpdatePanelSN"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideSN" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="TextSN"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
        
        
    </table>
    
</asp:Content>

