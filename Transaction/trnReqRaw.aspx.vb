Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_RawMaterialRequest
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim rpt As ReportDocument
    'Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 tidak valid/benar. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 tidak valid/benar. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Tanggal pada Period 2 harus melebihi dari tanggal pada Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Mohon untuk memilih MATERIAL field!<BR>"
        End If
        If reqqty.Text = "" Then
            sError &= "- Mohon untuk mengisi WEIGHT field!<BR>"
        Else
            If ToDouble(reqqtybiji.Text) <= 0 Then
                sError &= "- WEIGHT field harus lebih dari 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("reqrawqty", "QL_trnreqrawdtl", ToDouble(reqqty.Text), sErrReply) Then
                    sError &= "- WEIGHT field MAX WEIGHT (" & sErrReply & ") sudah disimpan di database!<BR>"
                End If
            End If
        End If
        If reqqtybiji.Text = "" Then
            sError &= "- Please fill QTY field!<BR>"
        Else
            If ToDouble(reqqtybiji.Text) <= 0 Then
                sError &= "- QTY field must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("reqrawqtybiji", "QL_trnreqrawdtl", ToDouble(reqqty.Text), sErrReply) Then
                    sError &= "- QTY field harus kurang dari MAX QTY (" & sErrReply & ") yang disimpan di database!<BR>"
                Else
                    If ToDouble(reqqtybiji.Text) > ToDouble(reqmaxqtybiji.Text) Then
                        If reftype.Text = "Detail 3" Then
                            Dim dTol As Double = (GetQtyKIK(refoid.Text) * GetTolerance()) / 100
                            If ToDouble(reqqtybiji.Text) > ToDouble(reqmaxqtybiji.Text) + dTol Then
                                sError &= "- QTY field harus kurang dari MAX QTY + TOLERANCE (" & ToMaskEdit(dTol, 4) & ")!<BR>"
                            End If
                        Else
                            sError &= "- QTY field harus kurang dari MAX QTY!<BR>"
                        End If
                    Else
                        'If Not IsQtyRounded(ToDouble(reqqty.Text), ToDouble(reqlimitqty.Text)) Then
                        '    sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                        'End If
                    End If
                End If
            End If
        End If
        If requnitoid.SelectedValue = "" Then
            sError &= "- Mohon untuk memilih UNIT field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Mohon untuk memilih BUSINESS UNIT field!<BR>"
        End If
        If deptoid.SelectedValue = "" Then
            sError &= "- Mohon untuk memilih DEPARTMENT field!<BR>"
        End If
        If reqdate.Text = "" Then
            sError &= "- Mohon untuk mengisi REQUEST DATE field!<BR>"
            sErr = "None"
        Else
            If Not IsValidDate(reqdate.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- REQUEST DATE tidak valid! " & sErr & "<BR>"
            End If
        End If
        If reqmststatus.Text <> "Post" Then
            If seqprocess.Text = "" Then
                sError &= "- Mohon untuk memilih Process! <BR>"
            Else
                If seqprocess.Text = 1 Then
                    Dim cek_seq As Integer = 0
                    cek_seq = GetStrData("select count (-1) from ql_trnreqrawmst where womstoid = " & womstoid.Text & " and reqrawflag = 'NONPROCESS'")
                    If cek_seq > 0 Then
                        'If Session("oid") = Nothing Or Session("oid") = "" Then
                        '    sError &= "- Mohon untuk memilih Process Selain Sequence 1!<BR>"
                        'End If
                    End If
                Else
                    Dim cek_seq1 As Integer = 0
                    cek_seq1 = GetStrData("select count (-1) from ql_trnreqrawmst where womstoid = " & womstoid.Text & " and reqrawflag = 'NONPROCESS'")
                    If cek_seq1 < 1 Then
                        sError &= "- Mohon untuk memilih Process dengan Sequence 1 Dahulu!<BR>"
                    End If
                End If
            End If
            'cek apakah dalam tabel ini sudah ada data yang flagnya NONPROCESS
           
        Else
            ''
        End If
      
        If reqwhoid.SelectedValue = "" Then
            sError &= "- Mohon untuk memilih WAREHOUSE field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Mohon untuk mengisi Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Mohon untuk mengisi Detail Data!<BR>"
            Else
                If DDLBusUnit.SelectedValue <> "" Then
                    Dim dvTbl As DataView = dtTbl.DefaultView
                    dvTbl.RowFilter = "reftype='Detail 3'"
                    If dvTbl.Count > 0 Then
                        For C1 As Integer = 0 To dvTbl.Count - 1
                            Dim dQty As Double = ToDouble(dvTbl(C1)("reqmaxqtyfix").ToString)
                            Dim dTol As Double = (GetQtyKIK(dvTbl(C1)("refoid").ToString) * GetTolerance()) / 100
                            dvTbl(C1)("reqmaxqty") = dQty - ToDouble(dtTbl.Compute("SUM(reqqty)", "reftype='Detail 3' AND refoid=" & dvTbl(C1)("refoid") & " AND matoid<>" & dvTbl(C1)("matoid") & " AND reqdtlseq<" & dvTbl(C1)("reqdtlseq")).ToString)
                            If ToDouble(dvTbl(C1)("reqqty").ToString) > ToDouble(dvTbl(C1)("reqmaxqty").ToString) + dTol Then
                                sError &= "- MAX QTY untuk detail data no. " & dvTbl(C1)("reqdtlseq") & " telah di update karena sudah digunakan oleh detail data yang lain. Mohon untuk mengecek bahwa setiap QTY harus kurang dari MAX QTY + TOLERANCE (" & ToMaskEdit(dTol, 4) & ")!<BR>"
                            End If
                        Next
                    End If
                    dvTbl.RowFilter = ""
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            reqmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTolerance() As Double
        GetTolerance = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='REQUEST KIK TOLERANCE (%)' ORDER BY updtime DESC"))
    End Function

    Private Function GetQtyKIK(ByVal sOid As String) As Double
        GetQtyKIK = ToDouble(GetStrData("SELECT wodtl4qty FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid=" & sOid & ""))
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckRequestStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnreqrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND reqrawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnReqRaw.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbReqInProcess.Visible = True
            lkbReqInProcess.Text = "Anda memiliki " & GetStrData(sSql) & " In Process Raw Material Request data yang tidak diproses lebih dari " & nDays.ToString & " hari. Klik pada link ini untuk melihatnya !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT 'MSC', 'MULTI SARANA COMPUTER' "
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='ITEMUNIT' OR gengroup='ITEMUNIT')"
        FillDDL(requnitoid, sSql)
        FillDDL(DDLunit1, sSql)

        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='ITEMUNIT' OR gengroup='ITEMUNIT') order by gendesc asc "
        FillDDL(requnitbijioid, sSql)
        ' Fill DDL Warehouse

    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'Murni Gold Prima' divname, reqm.reqrawmstoid, reqm.reqrawno, CONVERT(VARCHAR(10), reqm.reqrawdate, 103) reqrawdate, wom.wono, d.gendesc deptname, reqm.reqrawmststatus, reqm.reqrawmstnote, g.gendesc AS reqrawwh, 'False' AS checkvalue FROM QL_trnreqrawmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid AND wom.branch_code=reqm.branch_code INNER JOIN ql_mstgen d ON d.cmpcode=reqm.cmpcode AND d.genoid=reqm.deptoid INNER JOIN QL_mstgen g ON g.genoid=reqm.reqrawwhoid WHERE reqm.cmpcode='" & CompnyCode & "'"

        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, reqm.reqrawdate) DESC, reqm.reqrawmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnreqrawmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "reqrawmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("reqrawmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath("~/Report/rptReqRaw.rpt"))
            Dim sWhere As String = ""
            sWhere &= "WHERE reqm.cmpcode='" & CompnyCode & "'"
            If sOid = "" Then
                sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND reqm.reqrawdate >='" & FilterPeriod1.Text & " 00:00:00' AND reqm.reqrawdate <='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " And reqm.reqrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnReqRaw.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND reqm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND reqm.reqrawmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 username FROM QL_mstprof WHERE userid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RequestItemPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnReqRaw.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        Dim sType As String = ""
        If ReqTypeDDL.SelectedValue = "NEW" Then
            sType = "AND ISNULL(wod3.wodtl3reqflag,'')='' AND wom.womstres1 <> 'Complete' AND wom.womstres1 <>'RETURN'"
        Else
            sType = "AND wod3.wodtl3reqflag='Complete' AND wom.womstres1='RETURN'"
        End If
        sSql = "SELECT DISTINCT wom.womstoid,wom.branch_code,wom.woflag,wom.tobranch_code AS ToBranch,wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate FROM QL_trnwomst wom INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wom.cmpcode AND wod3.womstoid=wom.womstoid WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Approved' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' " & sType & " AND wom.woflag='RK' AND wom.tobranch_code='" & Session("branch_id") & "' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

    Private Sub InitDDLDept(ByVal ToBranch As String, ByVal BranchCode As String)
        ' Fill DDL Department
        Dim sAdd As String = ""
        If i_u.Text = "Update Data" Then
            sAdd = " OR genoid IN (SELECT deptoid FROM QL_trnreqrawmst WHERE reqrawmstoid=" & reqmstoid.Text & " AND womstoid=" & womstoid.Text & ")"
        End If

        FillDDL(deptoid, "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND (genoid IN (SELECT deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wod2.cmpcode AND wod3.wodtl2oid=wod2.wodtl2oid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.womstoid=" & womstoid.Text & " AND wodtl3reqflag='' )" & sAdd & ") ORDER BY genoid")

        FillDDL(reqwhoid, "SELECT genoid,ISNULL(gendesc+' - '+(SELECT DISTINCT gendesc FROM QL_mstgen gd WHERE loc.genother1=gd.genoid AND gd.gengroup='WAREHOUSE'),'-') gendesc FROM QL_mstgen loc WHERE gengroup='LOCATION' AND genother2=(SELECT DISTINCT genoid FROM QL_mstgen br WHERE br.genoid=CAST(loc.genother2 AS int) /*AND br.gencode NOT IN ('" & Session("branch_id") & "')*/ AND br.gengroup='CABANG' AND gencode='" & BranchCode & "')")

        FillDDL(ToMtrlocOid, "SELECT genoid,ISNULL(gendesc+' - '+(SELECT DISTINCT gendesc FROM QL_mstgen gd WHERE loc.genother1=gd.genoid AND gd.gengroup='WAREHOUSE'),'-') AS gendesc FROM QL_mstgen loc WHERE gengroup='LOCATION' AND genother2=(SELECT DISTINCT genoid FROM QL_mstgen br WHERE br.genoid=CAST(loc.genother2 AS int) AND br.gencode='" & ToBranch & "' AND br.gengroup='CABANG')")
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        reqdtlseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                reqdtlseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        matoid.Text = "" : matcode.Text = "" : matcodedtl1.Text = ""
        matlongdesc.Text = "" : refoid.Text = "" : reftype.Text = ""
        reqqty.Text = "" : reqqtybiji.Text = ""
        reqmaxqtybiji.Text = "" : requnitoid.SelectedIndex = -1
        reqdtlnote.Text = "" : ReqMaxWeight.Text = "" : gvReqDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        'deptoid.Enabled = bVal : deptoid.CssClass = sCss
        btnSearchKIK.Visible = bVal : btnClearKIK.Visible = bVal
    End Sub

    Private Sub BindMaterial()
        Dim join As String = ""
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(toDate(reqdate.Text))).Trim
        If woflag.Text = "GB " Then
            join = " wod1.wodtl1oidgb = wod3.wodtl1oidgb "
        Else
            join = "wod1.wodtl1oid = wod3.wodtl1oid"
        End If
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND reqd.reqrawmstoid <> " & Session("oid")
        End If
        If ReqTypeDDL.SelectedValue = "NEW" Then
            sSql = "SELECT distinct 'False' AS checkvalue, 'Detail 5' AS reftype,case when wod3.wodtl3reftype='' then 'RAKITAN' else wod3.wodtl3reftype end wodtl3reftype,0.0 AS tolerance,wod3.wodtl3qtybiji AS wodtl3qty, wod3.wodtl3oid AS refoid, m.itemcode_old AS matoldcode, m.itemoid AS matoid,wod2.wodtl2oid, m.itemcode AS matcode,m.itemdesc AS matlongdesc,0 AS kadaremas,m.itemoid,m.itemdesc,ISNULL(wodtl3qtybiji,0.00) AS reqqty,ISNULL(wodtl3qtybiji,0.00) AS reqmaxqty,ISNULL(wodtl3qtybiji,0.00)-ISNULL((SELECT ISNULL(SUM(reqrawqty),0.00) FROM QL_trnreqrawdtl rd INNER JOIN QL_trnreqrawmst rm ON rd.reqrawmstoid=rm.reqrawmstoid AND rm.branch_code=rd.branch_code AND rm.cmpcode=rm.cmpcode AND rd.itemoid=wod3.itemoid AND rd.frommtrlocoid=" & reqwhoid.SelectedValue & " AND rm.branch_code='" & BranchCode.Text & "'),0.00) AS reqqtybiji,ISNULL(wodtl3qtybiji,0.00)-ISNULL((SELECT ISNULL(SUM(reqrawqty),0.00) FROM QL_trnreqrawdtl rd INNER JOIN QL_trnreqrawmst rm ON rd.reqrawmstoid=rm.reqrawmstoid AND rm.branch_code=rd.branch_code AND rm.cmpcode=rm.cmpcode AND rd.itemoid=wod3.itemoid AND rd.frommtrlocoid=" & reqwhoid.SelectedValue & " AND rm.branch_code='" & BranchCode.Text & "'),0.00) AS reqmaxqtybiji, wod3.wodtl3unitoid AS matunitoid,'' AS matunit,'' AS reqdtlnote, 0.0001 AS reqlimitqty,0.0 AS reqweight,0 AS reqmaxweight,0 typeemas, wod3.wodtl3unitoid,m.itemdesc AS itemshortdesc,ISNULL((SELECT ISNULL(crd.saldoAkhir,0.00) FROM QL_crdmtr crd WHERE crd.refoid=wod3.itemoid AND crd.branch_code='" & BranchCode.Text & "' AND crd.mtrlocoid=" & reqwhoid.SelectedValue & " AND periodacctg='" & sPeriod & "'),0.00) AS StockAkhir" & _
                    " FROM QL_trnwodtl3 wod3" & _
                    " INNER JOIN QL_mstitem m ON m.itemoid =wod3.wodtl3refoid" & _
                    " INNER JOIN QL_trnwodtl1 wod1 ON wod1.wodtl1oid = wod3.wodtl1oid" & _
                    " INNER JOIN QL_trnwodtl2 wod2 on wod2.wodtl2oid = wod3.wodtl2oid" & _
                    " WHERE wod3.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.deptoid=" & deptoid.SelectedValue & " AND wod3.womstoid=" & womstoid.Text & ""
        Else
            sSql = "SELECT 'False' AS checkvalue, 'Detail 5' AS reftype,'' AS wodtl3reftype, 0.00 AS tolerance,0 AS wodtl3qty,ISNULL(m.itemoid,0.00) AS refoid,ISNULL(m.itemcode_old,m.itemcode) AS matoldcode,m.itemoid AS matoid,0.00 AS wodtl2oid,m.itemcode AS matcode,m.itemdesc AS matlongdesc,0 AS kadaremas,m.itemoid,m.itemdesc,ISNULL((SELECT ISNULL(wodtl3qtybiji,0.00) FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwomst wom ON wod3.womstoid=wom.womstoid AND wod3.cmpcode=wom.cmpcode AND wom.branch_code='" & BranchCode.Text & "' AND m.itemoid=wod3.itemoid AND wom.womstoid=" & womstoid.Text & "),0.00) AS reqqty,ISNULL((SELECT ISNULL(wodtl3qtybiji,0.00) FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwomst wom ON wod3.womstoid=wom.womstoid AND wod3.cmpcode=wom.cmpcode AND wom.branch_code='" & BranchCode.Text & "' AND m.itemoid=wod3.itemoid),0.00) AS reqmaxqty,ISNULL((SELECT ISNULL(SUM(reqrawqty),0.00) FROM QL_trnreqrawdtl rd INNER JOIN QL_trnreqrawmst rm ON rd.reqrawmstoid=rm.reqrawmstoid AND rm.branch_code=rd.branch_code AND rm.cmpcode=rm.cmpcode AND rd.itemoid=m.itemoid AND rd.frommtrlocoid=" & reqwhoid.SelectedValue & " AND rm.branch_code='" & BranchCode.Text & "' AND rm.womstoid=" & womstoid.Text & "),0.00) AS reqqtybiji,ISNULL((SELECT ISNULL(wodtl3qtybiji,0.00) FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwomst wom ON wod3.womstoid=wom.womstoid AND wod3.cmpcode=wom.cmpcode AND wom.branch_code='" & BranchCode.Text & "' AND m.itemoid=wod3.itemoid),0.00) - ISNULL((SELECT ISNULL(SUM(reqrawqty),0.00) FROM QL_trnreqrawdtl rd INNER JOIN QL_trnreqrawmst rm ON rd.reqrawmstoid=rm.reqrawmstoid AND rm.branch_code=rd.branch_code AND rm.cmpcode=rm.cmpcode AND rd.itemoid=m.itemoid AND rd.frommtrlocoid=" & reqwhoid.SelectedValue & " AND rm.branch_code='" & BranchCode.Text & "' AND rm.womstoid=" & womstoid.Text & "),0.00) AS reqmaxqtybiji,0 AS matunitoid,'' AS matunit,'' AS reqdtlnote,0.0001 AS reqlimitqty,0.0 AS reqweight,0 AS reqmaxweight,0 typeemas,0.00 AS wodtl3unitoid,m.itemdesc AS itemshortdesc,(SELECT SUM(crd.saldoAkhir) From QL_crdmtr crd WHERE m.itemoid=crd.refoid AND crd.mtrlocoid=" & reqwhoid.SelectedValue & " AND crd.branch_code='" & BranchCode.Text & "' AND crd.periodacctg='" & sPeriod & "') AS StockAkhir FROM ql_mstitem m WHERE m.itemoid IN (SELECT DISTINCT Refoid From QL_crdmtr crd WHERE m.itemoid=crd.refoid AND crd.mtrlocoid=" & reqwhoid.SelectedValue & " AND crd.branch_code='" & BranchCode.Text & "' AND crd.periodacctg='" & sPeriod & "') Order By m.itemoid"
        End If

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmatraw")
        dt.DefaultView.Sort = "reftype, matcode"
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("reqqty") = ToDouble(dt.Rows(C1)("reqqty").ToString)
            dt.Rows(C1)("reqqtybiji") = ToDouble(dt.Rows(C1)("reqqtybiji").ToString)
        Next
        dt.AcceptChanges()
        Session("TblListMat") = dt
        Session("TblListMatView") = dt
        gvListMat2.DataSource = Session("TblListMatView")
        gvListMat2.DataBind()
    End Sub

    Private Sub UpdateCheckedMat()
        Dim dv As DataView = Session("TblListMat").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid & " and itemoid = " & Session("TblListMat").DefaultView(C1)("itemoid").ToString
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("reqqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(6).Controls
                            dv(0)("reqqtybiji") = GetTextValue(cc2)
                            cc2 = row.Cells(8).Controls
                            dv(0)("reqdtlnote") = GetTextValue(cc2)
                            'cc2 = row.Cells(7).Controls
                            'dv(0)("reqweight") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub UpdateCheckedMatView()
        Dim dv As DataView = Session("TblListMatView").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        dv.RowFilter = "matoid=" & sOid
                        If cbcheck = True Then
                            dv(0)("checkvalue") = "True"
                            Dim cc2 As System.Web.UI.ControlCollection = row.Cells(3).Controls
                            dv(0)("reqqty") = ToDouble(GetTextValue(cc2))
                            cc2 = row.Cells(6).Controls
                            dv(0)("reqdtlnote") = GetTextValue(cc2)
                            cc2 = row.Cells(7).Controls
                            dv(0)("reqweight") = GetTextValue(cc2)
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TabelReqRawDetail")
        dtlTable.Columns.Add("reqdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("wodtl2oid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqqtybiji", Type.GetType("System.Double"))
        dtlTable.Columns.Add("requnitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("requnit", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqdtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("reqlimitqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxqtybiji", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxqtyfix", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqweight", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqmaxweight", Type.GetType("System.Double"))
        dtlTable.Columns.Add("typeemas", Type.GetType("System.Double"))
        dtlTable.Columns.Add("kadaremas", Type.GetType("System.Double"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemshortdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("wodtl3reftype", Type.GetType("System.String"))
        dtlTable.Columns.Add("tolerance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("wodtl3qty", Type.GetType("System.Double"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT DISTINCT reqm.cmpcode, reqm.reqrawmstoid, reqm.periodacctg, Convert(VARCHAR(10),reqm.reqrawdate,103) AS reqrawdate, reqm.reqrawno,reqm.seqprocess,reqm.reqrawflag,reqm.wodtl2oid, reqm.womstoid, wom.wodate, wom.wono, reqm.deptoid, reqm.reqrawmstnote, reqm.reqrawmststatus, reqm.createuser, reqm.createtime, reqm.upduser, reqm.updtime, reqm.reqrawwhoid,reqm.mtrlocoid,reqm.tomtrlocoid,reqm.branch_code,reqm.tobranch_code,reqrawmstres1 FROM QL_trnreqrawmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid AND wom.branch_code=reqm.branch_code INNER JOIN QL_trnwodtl2 wod2 ON wod2.cmpcode=reqm.cmpcode AND wod2.deptoid=reqm.deptoid WHERE reqm.reqrawmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                reqmstoid.Text = Trim(xreader("reqrawmstoid"))
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                reqdate.Text = Format(CDate(toDate(xreader("reqrawdate"))), "dd/MM/yyyy")
                reqno.Text = Trim(xreader("reqrawno").ToString)
                womstoid.Text = Trim(xreader("womstoid"))
                reqwhoid.SelectedValue = Trim(xreader("reqrawwhoid"))
                BranchCode.Text = Trim(xreader("branch_code").ToString)
                ToBranch.Text = Trim(xreader("tobranch_code").ToString)
                wono.Text = Trim(xreader("wono").ToString)
                seqprocess.Text = Trim(xreader("seqprocess").ToString)
                InitDDLDept(ToBranch.Text, BranchCode.Text)
                deptoid.SelectedValue = Trim(xreader("deptoid").ToString)
                flagrequest.Text = Trim(xreader("reqrawflag").ToString)
                reqmstnote.Text = Trim(xreader("reqrawmstnote").ToString)
                reqmststatus.Text = Trim(xreader("reqrawmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                woflag.Text = GetStrData("select woflag from ql_trnwomst where womstoid = '" & Trim(xreader("womstoid").ToString) & "'")
                ReqTypeDDL.SelectedValue = Trim(xreader("reqrawmstres1").ToString)
                ToMtrlocOid.SelectedValue = Trim(xreader("tomtrlocoid")).ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
        End Try
        If reqmststatus.Text = "Post" Or reqmststatus.Text = "Closed" Or reqmststatus.Text = "Cancel" Then
            btnSave.Visible = False : btnDelete.Visible = False
            btnPost.Visible = False : btnAddToList.Visible = False
            gvReqDtl.Columns(0).Visible = False
            gvReqDtl.Columns(gvReqDtl.Columns.Count - 1).Visible = False
            lblsprno.Text = "Request No." : reqmstoid.Visible = False
            reqno.Visible = True
        Else
            btnPost.Visible = True
        End If
        sSql = "SELECT reqd.reqrawdtloid,reqd.reqrawdtlseq AS reqdtlseq,reqd.wodtl2oid,reqd.itemoid,m.itemdesc AS itemshortdesc,0 AS kadaremas,0 typeemas, reqd.itemoid AS matoid,m.itemcode AS matcode, m.itemcode_old  AS matoldcode, m.ITEMDESC AS matlongdesc, reqd.reqrawqty AS reqqty,ISNULL(wod3.wodtl3qtybiji,0.00) AS wodtl3qty,ISNULL(wod3.tolerance,0.00) AS tolerance,reqd.reqrawqty AS reqqtybiji, reqd.reqrawunitoid AS requnitoid, g.gendesc AS requnit, reqd.reqrawdtlnote AS reqdtlnote, reqd.refoid, reqd.reftype,reqd.reqrawdtltype wodtl3reftype, 0.0001 AS reqlimitqty,(ISNULL(wod3.wodtl3qty,0.00) - ISNULL((SELECT SUM(x.reqrawqty) FROM QL_trnreqrawdtl x INNER JOIN QL_trnreqrawmst xx ON xx.cmpcode=x.cmpcode AND xx.reqrawmstoid=x.reqrawmstoid WHERE x.cmpcode=reqd.cmpcode AND x.reftype='Detail 5' AND x.refoid=reqd.refoid AND xx.reqrawmststatus <> 'Cancel' AND x.reqrawmstoid<>reqd.reqrawmstoid), 0)) AS reqmaxqty,(ISNULL(wod3.wodtl3qtybiji,0.00) - ISNULL((SELECT SUM(x.reqrawqty) FROM QL_trnreqrawdtl x INNER JOIN QL_trnreqrawmst xx ON xx.cmpcode=x.cmpcode AND xx.reqrawmstoid=x.reqrawmstoid WHERE x.cmpcode=reqd.cmpcode AND x.reftype='Detail 5' AND x.refoid=reqd.refoid AND xx.reqrawmststatus<>'Cancel' AND x.reqrawmstoid <> reqd.reqrawmstoid), 0)) AS reqmaxqtybiji, 0.0 AS reqmaxqtyfix ,0 AS reqweight, reqd.reqrawqty AS reqmaxweight,0 AS typeemas,ISNULL(wod3.wodtl3unitoid,0.00) AS wodtl3unitoid" & _
        " FROM QL_trnreqrawdtl reqd " & _
        " INNER JOIN QL_MSTITEM m ON m.itemoid=reqd.itemoid " & _
        " LEFT JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=reqd.cmpcode AND wod3.wodtl3oid=reqd.refoid " & _
        " INNER JOIN QL_mstgen g ON g.genoid=reqd.reqrawunitoid " & _
        " WHERE reqd.reqrawmstoid=" & sOid & " AND reqd.reftype='Detail 5' /*And reqd.reqrawdtltype <> 'WIP'*/ "

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnreqrawdtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("reqmaxqtyfix") = ToDouble(dt.Rows(C1)("reqmaxqty").ToString)
        Next
        Dim dv As DataView = dt.DefaultView
        dv.Sort = "reqdtlseq"
        dv.RowFilter = "reftype='Detail 3'"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                Dim dQty As Double = ToDouble(dv(C1)("reqmaxqtyfix").ToString)
                dv(C1)("reqmaxqty") = dQty - ToDouble(dt.Compute("SUM(reqqty)", "reftype='Detail 3' AND refoid=" & dv(C1)("refoid") & " AND matoid<>" & dv(C1)("matoid") & " AND reqdtlseq<" & dv(C1)("reqdtlseq")).ToString)
            Next
        End If
        dv.RowFilter = ""
        Session("TblDtl") = dv.ToTable
        gvReqDtl.DataSource = Session("TblDtl")
        gvReqDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnReqRaw.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Raw Material Request"

        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda yakin untuk MENGHAPUS data ini?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda yakin untuk POST data ini?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckRequestStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                reqmstoid.Text = GenerateID("QL_TRNREQRAWMST", CompnyCode)
                reqdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                reqmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvReqDtl.DataSource = dt
            gvReqDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ErrMat") Is Nothing And Session("ErrMat") <> "" Then
            Session("ErrMat") = Nothing
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnReqRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbReqInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReqInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, reqm.updtime, GETDATE()) > " & nDays & " AND reqrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnReqRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND reqm.reqrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.reqrawdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND reqm.reqrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnReqRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnReqRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearKIK_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Mohon untuk memilih Business Unit dahulu!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        womstoid.Text = "" : wono.Text = "" : deptoid.Items.Clear()
        deptoid_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListKIK.SelectedIndexChanged
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If
        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid")
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        BranchCode.Text = gvListKIK.SelectedDataKey.Item("branch_code").ToString
        ToBranch.Text = gvListKIK.SelectedDataKey.Item("ToBranch").ToString
        InitDDLDept(ToBranch.Text, BranchCode.Text) : deptoid_SelectedIndexChanged(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        If deptoid.SelectedValue <> "" Then
            'wodtl2oid.Text = GetStrData("SELECT wodtl2oid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text)

            seqprocess.Text = GetStrData("SELECT wodtl2procseq FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text)
            If seqprocess.Text = "1" Then
                flagrequest.Text = "NONPROCESS"
            Else
                flagrequest.Text = "PROCESS"
            End If
        End If
        ClearDetail()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If wono.Text = "" Then
            showMessage("Mohon untuk memilih No. SPK dahulu!", 2)
            Exit Sub
        End If
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
        Session("TblListMat") = Nothing : Session("TblListMatView") = Nothing
        gvListMat2.DataSource = Session("TblListMatView") : gvListMat2.DataBind()
        BindMaterial()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat2.Text) & "%'"
                Session("TblListMatView") = dv.ToTable
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
                dv.RowFilter = ""
            End If
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
                Session("TblListMatView") = dt
                gvListMat2.DataSource = Session("TblListMatView")
                gvListMat2.DataBind()
            End If
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If Not Session("TblListMat") Is Nothing Then
            UpdateCheckedMat()
            UpdateCheckedMatView()
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblListMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(3).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next
            cc = e.Row.Cells(7).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    'If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                    '    CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    'Else
                    CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    'End If
                End If
            Next

            Dim str As String = e.Row.Cells(1).Text
            e.Row.Cells(1).Text = Mid(str, 1, 7) & vbCrLf & Mid(str, 8, 10) & vbCrLf & Mid(str, 18, 5)
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToList.Click
        If Not Session("TblListMat") Is Nothing Then
            Dim dt As DataTable = Session("TblListMat")
            If dt.Rows.Count > 0 Then
                UpdateCheckedMat()
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count = 0 Then
                    Session("ErrMat") = "Mohon untuk memilih Material data!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""

                dv.RowFilter = "checkvalue='True' AND reqqtybiji=0"
                If dv.Count > 0 Then
                    Session("ErrMat") = "Request Qty untuk setiap Material yang dipilih harus lebih dari 0!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""

                dv.RowFilter = "checkvalue='True' AND reqqtybiji > StockAkhir "
                If dv.Count > 0 Then
                    Session("ErrMat") = "Stock Akhir tidak memenuhi..!!"
                    showMessage(Session("ErrMat"), 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
                dv.RowFilter = ""

                If ReqTypeDDL.SelectedValue = "NEW" Then
                    dv.RowFilter = "checkvalue='True' AND reqqtybiji > reqmaxqtybiji"
                    If dv.Count > 0 Then
                        Session("ErrMat") = "Request Qty untuk setiap Material yang dipilih harus kurang dari atau sama dengan Max Qty!"
                        showMessage(Session("ErrMat"), 2)
                        dv.RowFilter = ""
                        Exit Sub
                    End If
                    dv.RowFilter = ""
                End If

                If Session("TblDtl") Is Nothing Then
                    CreateTblDetail()
                End If
                Dim dtMat As DataTable = Session("TblDtl")
                Dim dvMat As DataView = dtMat.DefaultView
                Dim iSeq As Integer = dvMat.Count + 1
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    dvMat.RowFilter = "reftype='" & dv(C1)("reftype").ToString & "' AND matoid=" & dv(C1)("matoid").ToString & " And Itemoid = '" & dv(C1)("itemoid") & "'"
                    If dvMat.Count > 0 Then
                        dvMat(0)("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                        dvMat(0)("reqdtlnote") = dv(C1)("reqdtlnote").ToString
                        dvMat(0)("reqmaxqty") = ToDouble(dvMat(0)("reqmaxqty").ToString)
                        dvMat(0)("reqmaxqtyfix") = ToDouble(dvMat(0)("reqmaxqty").ToString)
                        dvMat(0)("reqqtybiji") = ToDouble(dvMat(0)("reqqtybiji").ToString)
                        dvMat(0)("reqmaxqtybiji") = ToDouble(dvMat(0)("reqmaxqtybiji").ToString)
                    Else
                        Dim rv As DataRowView = dvMat.AddNew()
                        rv.BeginEdit()
                        rv("reqdtlseq") = iSeq
                        rv("wodtl2oid") = dv(C1)("wodtl2oid")
                        rv("matoid") = dv(C1)("matoid")
                        rv("matcode") = dv(C1)("matcode").ToString
                        rv("matlongdesc") = dv(C1)("matlongdesc").ToString
                        rv("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                        rv("reqqtybiji") = ToDouble(dv(C1)("reqqtybiji").ToString)
                        rv("requnitoid") = dv(C1)("matunitoid")
                        rv("requnit") = dv(C1)("matunit").ToString
                        rv("reqdtlnote") = dv(C1)("reqdtlnote").ToString
                        rv("refoid") = dv(C1)("refoid")
                        rv("reftype") = dv(C1)("reftype").ToString
                        rv("itemoid") = dv(C1)("itemoid")
                        rv("itemshortdesc") = dv(C1)("itemshortdesc").ToString
                        rv("kadaremas") = dv(C1)("kadaremas").ToString
                        rv("typeemas") = dv(C1)("typeemas").ToString
                        rv("wodtl3reftype") = dv(C1)("wodtl3reftype").ToString
                        rv("reqlimitqty") = ToDouble(dv(C1)("reqlimitqty").ToString)
                        rv("reqmaxqty") = ToDouble(dv(C1)("reqmaxqty").ToString)
                        rv("reqmaxqtybiji") = ToDouble(dv(C1)("reqmaxqtybiji").ToString)
                        rv("reqmaxqtyfix") = ToDouble(dv(C1)("reqmaxqty").ToString)
                        rv("reqweight") = ToDouble(dv(C1)("reqweight").ToString)
                        rv("reqmaxweight") = ToDouble(dv(C1)("reqmaxweight").ToString)
                        rv("typeemas") = ToDouble(dv(C1)("typeemas").ToString)
                        rv("tolerance") = ToDouble(dv(C1)("tolerance"))
                        rv("wodtl3qty") = ToDouble(dv(C1)("wodtl3qty"))
                        rv.EndEdit()
                        iSeq += 1
                    End If
                    dvMat.RowFilter = ""
                Next
                dv.RowFilter = ""
                Session("TblDtl") = dvMat.ToTable
                gvReqDtl.DataSource = Session("TblDtl")
                gvReqDtl.DataBind()
                ClearDetail()
                deptoid.Enabled = False
                deptoid.CssClass = "inpTextDisabled"
            End If
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat2.Click
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "matoid=" & matoid.Text & " AND reftype='" & reftype.Text & "' AND itemoid = '" & reqfgoid.Text & "'"
            Else
                dv.RowFilter = "matoid=" & matoid.Text & " AND reftype='" & reftype.Text & "' AND itemoid = '" & reqfgoid.Text & "' AND reqdtlseq<>" & reqdtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah ditambahkan sebelumnya, Mohon dicek!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                reqdtlseq.Text = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(reqdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("reqdtlseq") = reqdtlseq.Text
            objRow("wodtl2oid") = wodtl2oid.Text
            objRow("matoid") = matoid.Text
            objRow("matcode") = matcodedtl1.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("reqqty") = ToDouble(reqqty.Text)
            objRow("reqqtybiji") = ToDouble(reqqtybiji.Text)
            objRow("requnitoid") = requnitoid.SelectedValue
            objRow("requnit") = requnitoid.SelectedItem.Text
            objRow("reqdtlnote") = reqdtlnote.Text.Trim
            objRow("refoid") = refoid.Text
            objRow("reftype") = reftype.Text
            objRow("reqmaxqtybiji") = ToDouble(reqmaxqtybiji.Text)
            objRow("tolerance") = ToDouble(tolerance.Text)
            objRow("itemoid") = reqfgoid.Text
            objRow("itemshortdesc") = reqfgdesc.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvReqDtl.DataSource = Session("TblDtl")
            gvReqDtl.DataBind()
            ClearDetail()
            deptoid.Enabled = False
            deptoid.CssClass = "inpTextDisabled"
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
        End If
    End Sub

    Protected Sub gvReqDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReqDtl.SelectedIndexChanged
        Try
            reqdtlseq.Text = gvReqDtl.SelectedDataKey.Item("reqdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "reqdtlseq=" & reqdtlseq.Text
                wodtl2oid.Text = dv.Item(0).Item("wodtl2oid").ToString
                matoid.Text = dv.Item(0).Item("matoid").ToString
                matcodedtl1.Text = dv.Item(0).Item("matcode").ToString
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString
                reqqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqty").ToString), 3)
                reqqtybiji.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqtybiji").ToString), 3)
                'requnitoid.SelectedValue = dv.Item(0).Item("requnitoid").ToString
                reqdtlnote.Text = dv.Item(0).Item("reqdtlnote").ToString
                refoid.Text = dv.Item(0).Item("refoid").ToString
                reftype.Text = dv.Item(0).Item("reftype").ToString
                reqmaxqtybiji.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqmaxqtybiji").ToString), 3)
                ReqMaxWeight.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqmaxweight").ToString), 3)
                reqfgoid.Text = dv.Item(0).Item("itemoid").ToString
                reqfgdesc.Text = dv.Item(0).Item("itemshortdesc").ToString
                tolerance.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("tolerance")), 3)
                wodtl3qty.Text = ToMaskEdit(ToDouble(dv.Item(0)("wodtl3qty")), 3)
                dv.RowFilter = ""
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvReqDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReqDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("reqdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvReqDtl.DataSource = objTable
        gvReqDtl.DataBind()
        ClearDetail()
        deptoid.Enabled = True
        deptoid.CssClass = "inpText"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim CrdMtroid As Integer = 0 : Dim ConMtroid As Integer = 0
        periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(reqdate.Text))).Trim

        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnreqrawmst WHERE reqrawmstoid=" & reqmstoid.Text
                If CheckDataExists(sSql) Then
                    reqmstoid.Text = GenerateID("QL_TRNREQRAWMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqrawmst", "reqrawmstoid", reqmstoid.Text, "reqrawmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            '--------------------------------------------------------
            '===================Generated ID Tabel===================
            '--------------------------------------------------------
            reqdtloid.Text = GenerateID("QL_TRNREQRAWDTL", CompnyCode)
            CrdMtroid = GenerateID("QL_crdmtr", CompnyCode)
            ConMtroid = GenerateID("QL_conmtr", CompnyCode)
            '--------------------------------------------------------
            '==================Query Cek Qty Spk=====================
            '--------------------------------------------------------
            sSql = "SELECT ISNULL(SUM(wodtl3qtybiji),0.00)-(SELECT ISNULL(SUM(reqrawqty),0.00) FROM QL_trnreqrawdtl rd INNER JOIN QL_trnreqrawmst rm ON rm.reqrawmstoid=rd.reqrawmstoid WHERE rm.womstoid=wod3.womstoid) FROM QL_trnwodtl3 wod3 INNER JOIN QL_trnwomst wom ON wom.womstoid=wod3.womstoid WHERE wod3.womstoid=" & womstoid.Text & " AND wom.cmpcode='" & CompnyCode & "' AND wom.branch_code='" & BranchCode.Text & "' GROUP BY wom.womstoid,branch_code,wom.cmpcode,wod3.womstoid"
            Dim sCok As Double = GetStrData(sSql)
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            If reqmststatus.Text = "Post" Then
                Dim trnno As String = "REQ/" & Format(CDate(toDate(reqdate.Text)), "yy") & "/" & Format(CDate(toDate(reqdate.Text)), "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(reqrawno,4) AS INT)), 0) FROM QL_trnreqrawmst WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND reqrawno LIKE '" & trnno & "%'"
                xCmd.CommandText = sSql
                trnno = trnno & Format(xCmd.ExecuteScalar + 1, "0000")
                reqno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    reqno.Text = reqmstoid.ToString
                End If
            End If

            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnreqrawmst (cmpcode,branch_code,tobranch_code, reqrawmstoid, periodacctg, reqrawdate, reqrawno, womstoid,seqprocess, deptoid, reqrawflag,wodtl2oid, reqrawwhoid, reqrawmstnote, reqrawmststatus, createuser, createtime, upduser, updtime,mtrlocoid,tomtrlocoid,reqrawmstres1) VALUES ('" & DDLBusUnit.SelectedValue & "','" & BranchCode.Text & "','" & ToBranch.Text & "', " & reqmstoid.Text & ", '" & periodacctg.Text & "', '" & CDate(toDate(reqdate.Text)) & "', '" & reqno.Text & "', " & womstoid.Text & "," & seqprocess.Text & ", " & deptoid.SelectedValue & ",'" & flagrequest.Text & "',0, " & reqwhoid.SelectedValue & ", '" & Tchar(reqmstnote.Text.Trim) & "', '" & reqmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & reqwhoid.SelectedValue & "," & ToMtrlocOid.SelectedValue & ",'" & ReqTypeDDL.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & reqmstoid.Text & " WHERE tablename='QL_TRNREQRAWMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnreqrawmst SET periodacctg='" & periodacctg.Text & "', reqrawdate='" & CDate(toDate(reqdate.Text)) & "', reqrawno='" & reqno.Text & "', womstoid=" & womstoid.Text & ",seqprocess=" & seqprocess.Text & ", deptoid=" & deptoid.SelectedValue & ",reqrawflag='" & flagrequest.Text & "', wodtl2oid=0, reqrawwhoid=" & reqwhoid.SelectedValue & ", reqrawmstnote='" & Tchar(reqmstnote.Text.Trim) & "', reqrawmststatus='" & reqmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, tomtrlocoid=" & ToMtrlocOid.SelectedValue & ",reqrawmstres1='" & ReqTypeDDL.SelectedValue & "' WHERE reqrawmstoid=" & reqmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid in (" & GetStrData("SELECT wodtl2oid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT refoid FROM QL_trnreqrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmstoid=" & reqmstoid.Text & " AND reftype='Detail 5')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM QL_trnreqrawdtl WHERE reqrawmstoid=" & reqmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnreqrawdtl (cmpcode, reqrawdtloid, reqrawmstoid, reqrawdtlseq, reftype,wodtl2oid, refoid, reqrawqty,reqrawunitoid, reqrawflag,reqrawdtlnote, reqrawdtlstatus, upduser, updtime,reqrawdtltype,itemoid,frommtrlocoid,tomtrlocoid,branch_code,tobranch_code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(reqdtloid.Text)) & ", " & reqmstoid.Text & ", " & c1 + 1 & ", '" & objTable.Rows(c1).Item("reftype").ToString & "'," & objTable.Rows(c1).Item("wodtl2oid") & ", " & objTable.Rows(c1).Item("refoid") & "," & ToDouble(objTable.Rows(c1).Item("reqqtybiji").ToString) & ", " & objTable.Rows(c1).Item("requnitoid") & ",'" & flagrequest.Text & "','" & Tchar(objTable.Rows(c1).Item("reqdtlnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & objTable.Rows(c1).Item("wodtl3reftype").ToString & "','" & objTable.Rows(c1).Item("itemoid").ToString & "'," & reqwhoid.SelectedValue & "," & ToMtrlocOid.SelectedValue & ",'" & BranchCode.Text & "','" & ToBranch.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        'Dim tolerance As Double = ToDouble(objTable.Rows(c1).Item("tolerance").ToString) * ToDouble(objTable.Rows(c1).Item("wodtl3qty").ToString) / 100
                        '======================================
                        '--------Jika Qty Spk Terpenuhi--------
                        '======================================
                        'If ToDouble(objTable.Rows(c1).Item("reqmaxqtybiji").ToString) = 0 Then
                        If ToDouble(objTable.Rows(c1).Item("reqqtybiji").ToString) >= ToDouble(objTable.Rows(c1).Item("reqmaxqtybiji").ToString) Then
                            If objTable.Rows(c1).Item("reftype").ToString.ToUpper = "DETAIL 5" Then
                                '===============================================
                                '---Update QL_trnwodtl3 Jika Qty SPK Complete---
                                '===============================================
                                sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid=" & objTable.Rows(c1).Item("refoid")
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                                '==============================================
                                '---Update Ql_trnwomst Jika Qty SPK Complete---
                                '==============================================
                                If sCok = 0 Or ReqTypeDDL.SelectedValue = "RETURN" Then
                                    If reqmststatus.Text = "Post" Then
                                        sSql = "UPDATE QL_trnwomst set womstres1='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ""
                                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                    End If
                                End If
                            End If
                            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='Complete' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid in (" & objTable.Rows(c1).Item("wodtl2oid") & ") AND (SELECT COUNT(*) FROM QL_trnwodtl3 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid in (" & objTable.Rows(c1).Item("wodtl2oid") & ") AND wodtl3reqflag='') /*+ (SELECT COUNT(*) FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid in (" & objTable.Rows(c1).Item("wodtl2oid") & ") AND wodtl4reqflag='')*/=0"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                        If reqmststatus.Text = "Post" Then
                            '==========================
                            '-----Cek Hpp Terakhir-----
                            '==========================
                            Dim lasthpp As Double = 0
                            sSql = "SELECT HPP FROM ql_mstitem WHERE itemoid=" & objTable.Rows(c1).Item("itemoid") & ""
                            xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                            '-------------------------------------
                            '=====Update crdmtr for item out======
                            '-------------------------------------
                            sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", saldoakhir = saldoakhir - " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", LastTransType = 'MATREQ', lastTrans = '" & CDate(toDate(reqdate.Text)) & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND mtrlocoid = " & ToDouble(reqwhoid.SelectedValue) & " AND refoid = " & objTable.Rows(c1).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & periodacctg.Text & "' And branch_Code = '" & BranchCode.Text & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                CrdMtroid = CrdMtroid + 1
                                '--------------------------------
                                'Insert crdmtr if no record found
                                '--------------------------------
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CrdMtroid & ", '" & periodacctg.Text & "', " & objTable.Rows(c1).Item("itemoid") & ", 'QL_MSTITEM'," & reqwhoid.SelectedValue & ", '" & BranchCode.Text & "', 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", 0, 0, 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", 0, 'MATREQ', '" & CDate(toDate(reqdate.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900','')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If
                            '------------------------------------
                            '=====Insert conmtr for item out=====
                            '------------------------------------
                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & ConMtroid & ", 'MATREQ', '" & CDate(toDate(reqdate.Text)) & "', '" & periodacctg.Text & "', '" & reqno.Text & "', " & objTable.Rows(c1).Item("reqrawdtloid") & ", 'QL_trnreqrawdtl', " & objTable.Rows(c1).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(c1).Item("requnitoid") & ", " & reqwhoid.SelectedValue & ", 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) * ToDouble(lasthpp) & ", '" & Tchar(objTable.Rows(c1).Item("reqdtlnote")) & "', " & ToDouble(lasthpp) & ",'" & BranchCode.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                            '--------------------------
                            'Update crdmtr for item in 
                            '--------------------------
                            sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ",branch_code='" & ToBranch.Text & "', saldoakhir = saldoakhir + " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", LastTransType = 'MATREQ', lastTrans = '" & CDate(toDate(reqdate.Text)) & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND mtrlocoid =" & ToMtrlocOid.SelectedValue & " AND refoid = " & objTable.Rows(c1).Item("itemoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & periodacctg.Text & "' AND branch_code = '" & ToBranch.Text & "' "
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                '--------------------------------
                                'Insert crdmtr if no record found
                                '--------------------------------
                                CrdMtroid = CrdMtroid + 1
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CrdMtroid & ", '" & periodacctg.Text & "', " & objTable.Rows(c1).Item("itemoid") & ", 'QL_MSTITEM'," & ToMtrlocOid.SelectedValue & " , '" & ToBranch.Text & "', " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", 0, 'MATREQ', '" & CDate(toDate(reqdate.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If

                            '=======================================================
                            '-----------------Insert Conmtr Qty In-----------------
                            '=======================================================
                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid,branch_code,qtyIn, qtyOut, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & DDLBusUnit.SelectedValue & "', " & ConMtroid & ", 'REQIN', '" & CDate(toDate(reqdate.Text)) & "', '" & periodacctg.Text & "', '" & reqno.Text & "', " & objTable.Rows(c1).Item("reqrawdtloid") & ", 'QL_trnreqrawdtl', " & objTable.Rows(c1).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(c1).Item("requnitoid") & ", " & ToMtrlocOid.SelectedValue & ",'" & ToBranch.Text & "'," & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) & ", 0, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(objTable.Rows(c1).Item("reqqtybiji")) * ToDouble(lasthpp) & ", '" & Tchar(objTable.Rows(c1).Item("reqdtlnote")) & "', " & ToDouble(lasthpp) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1

                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(reqdtloid.Text)) & " WHERE tablename='QL_TRNREQRAWDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '========================
                    'Update lastoid QL_crdmtr
                    '========================
                    sSql = "UPDATE QL_mstoid SET lastoid =" & CrdMtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '========================
                    'Update lastoid QL_conmtr
                    '========================
                    sSql = "UPDATE QL_mstoid SET lastoid =" & ConMtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        reqmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                reqmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. telah diregenerasi karena digunakan oleh data lain. Draft No. yang baru adalah " & reqmstoid.Text & ".<BR>"
            End If
            If reqmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data telah diposting dengan Request No. = " & reqno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnReqRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnReqRaw.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If reqmstoid.Text = "" Then
            showMessage("Mohon untuk memilih Raw Material Request data dahulu!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqrawmst", "reqrawmstoid", reqmstoid.Text, "reqrawmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                reqmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trnwodtl2 SET wodtl2reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl2oid in (" & GetStrData("SELECT wodtl2oid FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text) & ")"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "UPDATE QL_trnwodtl3 SET wodtl3reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl3oid IN (SELECT refoid FROM QL_trnreqrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmstoid=" & reqmstoid.Text & " AND reftype='Detail 5')"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            'sSql = "UPDATE QL_trnwodtl4 SET wodtl4reqflag='' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid IN (SELECT refoid FROM QL_trnreqrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmstoid=" & reqmstoid.Text & " AND reftype='Detail 3')"
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_trnreqrawdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmstoid=" & reqmstoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreqrawmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND reqrawmstoid=" & reqmstoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnReqRaw.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        reqmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub ReqTypeDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReqTypeDDL.SelectedIndexChanged
        wono.Text = ""
        'reqwhoid.SelectedValue = 0
        'deptoid.SelectedItem.Text = "" : ToMtrlocOid.SelectedItem.Text = ""
    End Sub
#End Region
End Class