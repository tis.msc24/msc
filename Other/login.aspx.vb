'Prgmr:Henry | LastUpdt:23.09.08
Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class login
    Inherits System.Web.UI.Page
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim cKoneksi As New Koneksi
    Dim sSql As String = ""
    Dim cfunction As New ClassFunction
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Dim xCmd As New SqlCommand("", conn)
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Clear()
        If Trim(txtInputuserid.Text) <> "" Then
            txtInputPassword.Focus()
        Else
            txtInputuserid.Focus()
        End If
    End Sub

    Protected Sub imbLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbLogin.Click
        Dim vuserid As String
        Dim vpassword As String
        vuserid = txtInputuserid.Text
        vpassword = txtInputPassword.Text
        Session("Role") = Nothing
        If (vuserid <> "" And vpassword <> "") Then
            Dim myDS As New DataSet()
            Dim sqlSelect As String
            sqlSelect = "select a.cmpcode,a.userid,a.username,a.profpass,a.STATUSPROF,a.APPROVALLIMIT,a.upduser,a.updtime,a.branch_code,b.gendesc, b.genoid from QL_mstprof a inner join ql_mstgen b on a.cmpcode = b.cmpcode and a.branch_code = b.gencode where userid='" & Tchar(vuserid) & "' and profpass='" & Tchar(vpassword) & "' and a.cmpcode='" & CompnyCode & "'"
            Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            mySqlDA.Fill(myDS)
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = myDS.Tables(0)
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length > 0 Then
                If objRow(0).Item("STATUSPROF") = "Active" Then
                    Session("UserID") = Tchar(txtInputuserid.Text)
                    Session("ApprovalLimit") = objRow(0).Item("APPROVALLIMIT")
                    Session("branch") = objRow(0).Item("gendesc")
                    Session("branch_id") = objRow(0).Item("branch_code")
          
                    sqlSelect = "SELECT DISTINCT [QL_ROLEDTL].[FORMTYPE], [QL_ROLEDTL].[FORMNAME], [QL_ROLEDTL].[FORMADDRESS] FROM [QL_ROLEDTL] INNER JOIN [QL_ROLE] ON [QL_ROLE].[ROLEOID]=[QL_ROLEDTL].[ROLEOID] INNER JOIN [QL_USERROLE] ON [QL_ROLE].[ROLEOID]=[QL_USERROLE].[ROLEOID] WHERE (([QL_ROLEDTL].[CMPCODE] = '" & CompnyCode & "') AND ([QL_ROLE].[CMPCODE] = '" & CompnyCode & "') AND ([QL_USERROLE].[CMPCODE] = '" & CompnyCode & "') AND ([QL_USERROLE].[USERPROF] = '" & Session("UserID") & "'))"
                    Session("Role") = cKoneksi.ambiltabel(sqlSelect, "Role")
                    sqlSelect = "SELECT DISTINCT [QL_ROLEDTL].[FORMADDRESS], [QL_USERROLE].[SPECIAL] FROM [QL_ROLEDTL] INNER JOIN [QL_ROLE] ON [QL_ROLE].[ROLEOID]=[QL_ROLEDTL].[ROLEOID] INNER JOIN [QL_USERROLE] ON [QL_ROLE].[ROLEOID]=[QL_USERROLE].[ROLEOID] WHERE (([QL_ROLEDTL].[CMPCODE] = '" & CompnyCode & "') AND ([QL_ROLE].[CMPCODE] = '" & CompnyCode & "') AND ([QL_USERROLE].[CMPCODE] = '" & CompnyCode & "') AND [QL_USERROLE].[SPECIAL]='Yes' and  ([QL_USERROLE].[USERPROF] = '" & Session("UserID") & "'))"
                    Session("SpecialAccess") = cKoneksi.ambiltabel(sqlSelect, "SpecialAccess")

                    Dim objTrans As SqlClient.SqlTransaction
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    objTrans = conn.BeginTransaction()
                    xCmd.Transaction = objTrans
                    Try
                        '============================ CUSTOMER =======================================
                        ' Update CL Tempo menjadi NOL ketika melebihi 24 jam Konversi ke detik
                        sSql = "Update ql_mstcust set custcreditlimitrupiahtempo = 0,custcreditlimit=custcreditlimitrupiahawal,custcreditlimitrupiah=custcreditlimitrupiahawal, updtime = CURRENT_TIMESTAMP where custoid in (select custoid from ql_mstcustdtl where custdtlstatus = 'Open' and custdtlflag = 'Tempo' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_mstcust set custoverduestatus = 'N', updtime = CURRENT_TIMESTAMP where custoid in (select custoid from ql_mstcustdtl where custdtlstatus = 'Enabled' and custdtlflag = 'Over Due' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400 /*(select (genother1*3600) from ql_mstgen where gengroup = 'CREDITLIMITTYPE' and gendesc = 'Over Due')*/)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_mstcustdtl set custdtlstatus = 'Expired', updtime = CURRENT_TIMESTAMP where custdtloid in (select custdtloid from ql_mstcustdtl where custdtlstatus = 'Open' and custdtlflag = 'Tempo' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_mstcustdtl set custdtlstatus = 'Expired', updtime = CURRENT_TIMESTAMP where custdtloid in (select custdtloid from ql_mstcustdtl where custdtlstatus = 'Enabled' and custdtlflag = 'Over Due' and DATEDIFF(S, createtime, CURRENT_TIMESTAMP) > 86400 /*(select (genother1*3600) from ql_mstgen where gengroup = 'CREDITLIMITTYPE' and gendesc = 'Over Due')*/)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        '=============================== SALES ORDER ===================================
                        sSql = "select count(-1) from ql_mstgen where gengroup = 'SOCLOSETYPE'"
                        xCmd.CommandText = sSql : Dim countsotype As Integer = xCmd.ExecuteScalar
                        If countsotype > 1 Then
                            conn.Close() : showMessage("Data untuk set hari pada SOCLOSETYPE Lebih Dari 1, Hapus salah satu untuk set AUTO CLOSE SO", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                        End If
                        sSql = "select isnull(genother1,0) from ql_mstgen where gengroup = 'SOCLOSETYPE'"
                        xCmd.CommandText = sSql : Dim sotype As Integer = xCmd.ExecuteScalar
                        If sotype = 0 Then
                            conn.Close() : showMessage("Master General untuk type SOCLOSETYPE belum di set, silahkan hubungi admin!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                        End If
                        Dim conDayToSecond As Integer = 86400
                        sSql = "select DATEDIFF(S,o.createtime,CURRENT_TIMESTAMP) AS Day,o.createtime,o.ordermstoid,isnull(sjm.trnsjjualmstoid,0) AS trnsjjualmstoid, o.trnorderstatus,ISNULL(sjm.trnsjjualstatus,'') AS trnsjjualstatus,o.branch_code from QL_trnordermst o LEFT JOIN ql_trnsjjualmst sjm on sjm.orderno = o.orderno and sjm.branch_code = o.branch_code AND sjm.trnsjjualstatus NOT IN ('Approved','Rejected', 'Closed') where o.trnorderstatus NOT IN ('Canceled', 'Closed', 'Rejected','POST', 'Closed Manual') and DATEDIFF(S,o.createtime,CURRENT_TIMESTAMP) >= " & sotype * conDayToSecond & ""
                        Session("mstso") = cKoneksi.ambiltabel(sSql, "ql_mstso")
                        Dim objTables As DataTable : objTables = Session("mstso")
                        Dim dvso As DataView = objTables.DefaultView
                        If objTable.Rows.Count > 0 Then
                            '-------
                            dvso.RowFilter = "trnorderstatus = 'In Approval'"
                            For C1 As Int16 = 0 To dvso.Count - 1
                                sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sSql = "Update ql_approval set STATUSREQUEST = 'End', Event = 'Closed' where oid = " & dvso(C1)("ordermstoid") & " AND branch_code = '" & dvso(C1)("branch_code") & "' AND TABLENAME = 'QL_TRNORDERMST'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            dvso.RowFilter = ""
                            '--------
                            dvso.RowFilter = "isnull(trnsjjualstatus,'') = '' AND trnorderstatus <> 'In Approval'"
                            For C1 As Int16 = 0 To dvso.Count - 1
                                sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            dvso.RowFilter = ""
                            '---------
                            dvso.RowFilter = "trnsjjualstatus = 'In Approval'"
                            For C1 As Int16 = 0 To dvso.Count - 1
                                sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sSql = "Update ql_trnsjjualmst set trnsjjualstatus = 'Closed', upduser = 'Auto Closed' where trnsjjualmstoid = " & dvso(C1)("trnsjjualmstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sSql = "Update ql_approval set STATUSREQUEST = 'End', Event = 'Closed' where oid = " & dvso(C1)("trnsjjualmstoid") & " AND branch_code = '" & dvso(C1)("branch_code") & "' AND TABLENAME = 'QL_TRNSJJUALMST'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            dvso.RowFilter = ""

                            dvso.RowFilter = "trnsjjualstatus  <> 'In Approval' and trnorderstatus <> 'In Approval' and trnsjjualstatus <> '' "
                            '---------
                            For C1 As Int16 = 0 To dvso.Count - 1
                                sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sSql = "Update ql_trnsjjualmst set trnsjjualstatus = 'Closed', upduser = 'Auto Closed' where trnsjjualmstoid = " & dvso(C1)("trnsjjualmstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            dvso.RowFilter = ""
                            '---------
                            dvso.RowFilter = "trnsjjualstatus = 'Approved' and trnorderstatus <> 'In Approval' "
                            For C1 As Int16 = 0 To dvso.Count - 1
                                sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            Next
                            dvso.RowFilter = ""
                        End If
                        objTrans.Commit() : xCmd.Connection.Close()
                    Catch ex As Exception
                        objTrans.Rollback() : xCmd.Connection.Close()
                        showMessage(ex.Message, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                    End Try
                    Response.Redirect("Menu.aspx?page=true")
                ElseIf objRow(0).Item("STATUSPROF") = "blocked" Then
                    lblMessage.Text = "<br>Your user ID is blocked, please contact your administrator"
                    txtInputuserid.Text = ""
                    txtInputPassword.Text = ""
                    txtInputuserid.Focus()
                ElseIf objRow(0).Item("STATUSPROF") = "idle" Then
                    lblMessage.Text = "<br>Your user ID is idle, please contact your administrator"
                    txtInputuserid.Text = ""
                    txtInputPassword.Text = ""
                    txtInputuserid.Focus()
                End If
            Else
                lblMessage.Text = "<br>User ID and password do not match!"
                txtInputuserid.Text = ""
                txtInputPassword.Text = ""
                txtInputuserid.Focus()
            End If
        End If

    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessages.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMsgBoxOK.Click
        Response.Redirect("~\Other\login.aspx")
    End Sub
End Class