Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstPriceCabang
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Private cRate As New ClassRate
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Public Function GetTransID() As String
        Return (Eval("itemoid"))
    End Function 

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    'Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
    '    Dim sReturn As String = ""
    '    For Each myControl As System.Web.UI.Control In cc
    '        If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
    '            sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
    '        End If
    '    Next
    '    Return sReturn
    'End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("priceexpedisi") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("pricenormal") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("PriceMinim") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("pricekhusus") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("pricesilver") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                    dtView(0)("pricegold") = ToDouble(GetTextBoxValue(row.Cells(9).Controls))
                                    dtView(0)("priceplatinum") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("addcost") = ToDouble(GetTextBoxValue(row.Cells(11).Controls))
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(12).Controls)
                                    dtView(0)("active_flag") = GetDDLValue(row.Cells(13).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("priceexpedisi") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("pricenormal") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                        dtView2(0)("PriceMinim") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                        dtView2(0)("pricekhusus") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                        dtView2(0)("pricesilver") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                        dtView2(0)("pricegold") = ToDouble(GetTextBoxValue(row.Cells(9).Controls))
                                        dtView2(0)("priceplatinum") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                        dtView2(0)("addcost") = ToDouble(GetTextBoxValue(row.Cells(11).Controls))
                                        dtView2(0)("keterangan") = GetTextBoxValue(row.Cells(12).Controls)
                                        dtView2(0)("active_flag") = GetDDLValue(row.Cells(13).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("priceexpedisi") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("pricenormal") = ToDouble(GetTextBoxValue(row.Cells(5).Controls))
                                    dtView(0)("PriceMinim") = ToDouble(GetTextBoxValue(row.Cells(6).Controls))
                                    dtView(0)("pricekhusus") = ToDouble(GetTextBoxValue(row.Cells(7).Controls))
                                    dtView(0)("pricesilver") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                    dtView(0)("pricegold") = ToDouble(GetTextBoxValue(row.Cells(8).Controls))
                                    dtView(0)("priceplatinum") = ToDouble(GetTextBoxValue(row.Cells(10).Controls))
                                    dtView(0)("addcost") = ToDouble(GetTextBoxValue(row.Cells(11).Controls))
                                    dtView(0)("keterangan") = GetTextBoxValue(row.Cells(12).Controls)
                                    dtView(0)("active_flag") = GetDDLValue(row.Cells(13).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
     
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CreateTblDetail() '
        Dim dtlTable As DataTable = New DataTable("QL_trnmstitemdtl")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mstexpoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("mstexpdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("branchCode", Type.GetType("System.String"))
        dtlTable.Columns.Add("pricenormal", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pricekhusus", Type.GetType("System.Double"))
        dtlTable.Columns.Add("PriceMinim", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pricenota", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pricegold", Type.GetType("System.Double"))
        dtlTable.Columns.Add("priceplatinum", Type.GetType("System.Double"))
        dtlTable.Columns.Add("pricesilver", Type.GetType("System.Double"))
        dtlTable.Columns.Add("priceexpedisi", Type.GetType("System.Double"))
        dtlTable.Columns.Add("addcost", Type.GetType("System.Double"))
        dtlTable.Columns.Add("hpp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("keterangan", Type.GetType("System.String"))
        dtlTable.Columns.Add("createdate", Type.GetType("System.String"))
        dtlTable.Columns.Add("createtime", Type.GetType("System.String"))
        dtlTable.Columns.Add("active_flag", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(DDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                If Session("branch_id") = "01" Then
                    FillDDL(DDLCabang, sSql)
                    DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                    DDLCabang.SelectedValue = "ALL"
                Else
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(DDLCabang, sSql)
                End If
            Else
                FillDDL(DDLCabang, sSql)
                DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                DDLCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(DDLCabang, sSql)
            DDLCabang.Items.Add(New ListItem("ALL", "ALL"))
            DDLCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitCabangDDL()
        sSql = "select gencode, gendesc from QL_mstgen where gengroup = 'CABANG' ORDER BY gencode"
        FillDDL(ddlBranch, sSql)
    End Sub

    Private Sub BindData()
        sSql = "Select * From (Select gencode branch_code, gendesc cabang, i.cmpcode, i.itemoid, i.itemcode, i.itemdesc, 'BUAH' satuan3, i.merk, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, p.pricenormal, p.pricekhusus, p.pricenota, p.pricegold, p.priceplatinum, p.pricesilver, p.priceminim, p.active_flag, p.createtime From ql_mstitem i Inner Join QL_mstpricedtl p ON p.itemoid=i.itemoid INNER JOIN QL_mstgen b ON b.gencode=p.branchcode AND b.gengroup='CABANG' WHERE itemflag='AKTIF' AND p.active_flag='ACTIVE') dt WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
        If JenisDDL.SelectedValue <> "ALL" Then
            sSql &= " AND stockflag='" & JenisDDL.SelectedValue & "'"
        End If

        If DDLCabang.SelectedValue <> "ALL" Then
            sSql &= " AND branch_code='" & DDLCabang.SelectedValue & "'"
        End If
        sSql &= " Order BY branch_code ASC, createtime Desc"
        Session("TblMst") = cKon.ambiltabel(sSql, "ql_mst")
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub InitJenisKatalog()
        sSql = "Select DISTINCT stockflag,JenisNya From (Select stockflag,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From ql_mstitem i WHERE itemflag='AKTIF') it"
        FillDDL(ddlType, sSql)
        ddlType.Items.Add(New ListItem("ALL", "ALL"))
        ddlType.SelectedValue = "ALL"
    End Sub

    Private Sub InitFilterJenis()
        sSql = "Select DISTINCT stockflag, JenisNya From (Select stockflag,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From ql_mstitem i WHERE itemflag='AKTIF') it"
        FillDDL(JenisDDL, sSql)
        JenisDDL.Items.Add(New ListItem("ALL", "ALL"))
        JenisDDL.SelectedValue = "ALL"
    End Sub

    Private Sub BindMaterial()
        Try
            sSql = "Select 'False' AS CheckValue, * From (Select i.itemoid, i.itemcode, i.itemdesc, 'BUAH' satuan3, i.merk, i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya, pricelist pricenormal, itempriceunit2 pricekhusus, itempriceunit1 pricenota, i.bottompricegrosir PriceMinim, pricegold, priceplatinum, pricesilver pricesilver, '' keterangan, i.HPP, '' active_flag, ISNULL((Select TOP 1 addcost from QL_mstpricedtl p Where p.itemoid=i.itemoid AND p.branchcode='" & ddlBranch.SelectedValue & "' Order By createtime DESC),0.00) addcost, ISNULL((Select top 1 ed.priceexpedisi from QL_mstpriceexpdtl ed Where ed.itemoid=i.itemoid AND ed.branch_code='" & ddlBranch.SelectedValue & "' Order By crttime DESC), 0.00) priceexpedisi, ISNULL((Select top 1 ed.branch_code from QL_mstpriceexpdtl ed Where ed.itemoid=i.itemoid AND ed.branch_code='" & ddlBranch.SelectedValue & "' Order By crttime DESC), '') branch_code, ISNULL((Select top 1 ed.mstexpdtloid from QL_mstpriceexpdtl ed Where ed.itemoid=i.itemoid AND ed.branch_code='" & ddlBranch.SelectedValue & "' Order By crttime DESC), 0 ) mstexpdtloid, ISNULL((Select top 1 ed.mstexpoid from QL_mstpriceexpdtl ed Where ed.itemoid=i.itemoid AND ed.branch_code='" & ddlBranch.SelectedValue & "' Order By crttime DESC), 0) mstexpoid From ql_mstitem i WHERE i.itemflag='Aktif') dt Where branch_code='" & ddlBranch.SelectedValue & "' Order BY itemdesc"
            Dim dtMat As DataTable = cKon.ambiltabel(sSql, "ql_mstitem")
            If dtMat.Rows.Count > 0 Then
                Session("TblMat") = dtMat
                Session("TblMatView") = dtMat
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else
                showMessage("Katalog data can't be found!", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub FillTextBox(ByVal sNo As String)
        Try
            ddlBranch.SelectedValue = Session("CommandName")
            sSql = "SELECT ROW_NUMBER() OVER (ORDER BY c.gencode ASC) AS seq, c.genoid, c.gencode branchCode, i.itemcode, i.itemdesc, c.gendesc Cabang, pricenormal, p.priceminim, p.pricekhusus, p.pricenota, p.pricegold, p.priceplatinum, p.pricesilver, notedtl Keterangan, Convert(Varchar(20), p.createtime, 103) createdate, Convert(Varchar(20), p.createtime, 108) createtime, i.hpp, p.priceexpedisi, p.mstexpoid, p.mstexpdtloid, p.active_flag, p.itemdtloid, p.itemoid, p.addcost FROM QL_mstitem i INNER JOIN QL_mstpricedtl p ON p.itemoid=i.itemoid INNER JOIN QL_mstgen c ON c.gencode=p.branchcode AND c.gengroup='CABANG' WHERE i.itemoid=" & sNo & " AND p.branchcode='" & Session("CommandName") & "' Order By p.createtime DESC"
            Dim dt As DataTable = cKon.ambiltabel2(sSql, "QL_mstitem")
            Session("TblDtl") = dt
            gvFindCrd.DataSource = Session("TblDtl")
            gvFindCrd.DataBind()
            gvFindCrd.Columns(1).Visible = True
            gvFindCrd.Columns(2).Visible = True
            gvFindCrd.Visible = True
            imbSave.Visible = False : BtnAddItem.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub PrintReport()
        Try
            Dim sWhere As String = "WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLMst.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextMst.Text) & "%'"
            If JenisDDL.SelectedValue <> "ALL" Then
                sWhere &= " AND stockflag='" & JenisDDL.SelectedValue & "'"
            End If

            If DDLCabang.SelectedValue <> "ALL" Then
                sWhere &= " AND branch_code='" & DDLCabang.SelectedValue & "'"
            End If
            sWhere &= " Order BY itemdesc"
            report.Load(Server.MapPath(folderReport & "RptPriceCabang.rpt"))
            report.SetParameterValue("sWhere", sWhere)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "RptPriceCabang" & Format(GetServerTime(), "yyyyMMddHHmmss"))
            Catch ex As Exception
                report.Close()
                report.Dispose()
            End Try
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub 

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Master\MstPriceCabang.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        upduser.Text = Session("UserID")
        updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
 
        Page.Title = CompnyName & " - Master Price Cabang"
        Session("oid") = Request.QueryString("oid")

        imbPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data for APPROVAL?');")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            fDDLBranch() : InitCabangDDL() : InitJenisKatalog() : InitFilterJenis()
            If (Session("oid") <> Nothing And Session("oid") <> "") Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
                TabContainer1.ActiveTabIndex = 0
            End If 
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub 

    Protected Sub gvFindCrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFindCrd.PageIndexChanging
        UpdateCheckedMat()
        gvFindCrd.PageIndex = e.NewPageIndex
        Dim dtDtl As DataTable = Session("TblCrd")
        gvFindCrd.DataSource = dtDtl : gvFindCrd.DataBind()
    End Sub

    Protected Sub gvFindCrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFindCrd.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
            e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 3)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
            e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 3)
            e.Row.Cells(14).Text = ToMaskEdit(ToDouble(e.Row.Cells(14).Text), 3)
        End If
    End Sub 

    Protected Sub lkbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim dtDtl As DataTable : dtDtl = Session("TblDtl")
        'Dim dvDel As DataView = dtDtl.DefaultView
        'dvDel.RowFilter = "seq=" & sender.ToolTip
        'If dvDel.Count > 0 Then
        '    dvDel(0).Delete()
        'End If
        'dvDel.RowFilter = ""
        'dtDtl.AcceptChanges()

        'For C1 As Integer = 0 To dtDtl.Rows.Count - 1
        '    dtDtl.Rows.Item(C1)("seq") = C1 + 1
        'Next

        'Session("TblDtl") = dtDtl 
    End Sub

    Protected Sub imbPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPosting.Click 
        imbSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click

        Dim sMsg As String = "", objTable As DataTable = Session("TblDtl"), sParam As String = ""
        If Session("TblDtl") Is Nothing Then
            sMsg &= "- No detail data.<BR>"
        Else
            If objTable.Rows.Count < 1 Then
                sMsg &= "- No detail data.<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        Dim itemdtloid As Integer = GenerateID("QL_mstpricedtl", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                For C1 As Integer = 0 To objTable.Rows.Count - 1
                    sSql = "Update QL_mstpricedtl set active_flag='IN AKTIVE' Where itemoid=" & Integer.Parse(objTable.Rows(C1)("itemoid")) & " AND branchcode='" & objTable.Rows(C1)("branchCode").ToString & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO QL_mstpricedtl (cmpcode, itemdtloid, itemoid, branchCode, pricenormal, pricekhusus, pricenota, pricegold, priceplatinum, pricesilver, notedtl, dNormal, dKhusus, dNota, dPlatinum, dGold, dSilver, sNormal, sKhusus, sNota, sPlatinum, sGold, sSilver, createuser, createtime, [mstexpoid], [mstexpdtloid], [hpp], [priceexpedisi], [active_flag], upduser, updtime, PriceMinim, addcost) VALUES ('" & CompnyCode & "', " & itemdtloid & ", " & Integer.Parse(objTable.Rows(C1)("itemoid")) & ", '" & objTable.Rows(C1)("branchCode").ToString & "', '" & ToDouble(objTable.Rows(C1)("pricenormal")) & "', '" & ToDouble(objTable.Rows(C1)("pricekhusus")) & "', " & ToDouble(objTable.Rows(C1)("pricenota")) & ", " & ToDouble(objTable.Rows(C1)("pricegold")) & ", " & ToDouble(objTable.Rows(C1)("priceplatinum")) & ", " & ToDouble(objTable.Rows(C1)("pricesilver")) & ", '" & Tchar(objTable.Rows(C1)("Keterangan")) & "', " & ToDouble(objTable.Rows(C1)("pricenormal")) & ", " & ToDouble(objTable.Rows(C1)("pricekhusus")) & ", " & ToDouble(objTable.Rows(C1)("pricenota")) & ", " & ToDouble(objTable.Rows(C1)("priceplatinum")) & ", " & ToDouble(objTable.Rows(C1)("pricegold")) & ", " & ToDouble(objTable.Rows(C1)("pricesilver")) & ", " & ToDouble(objTable.Rows(C1)("pricenormal")) & ", " & ToDouble(objTable.Rows(C1)("pricekhusus")) & ", " & ToDouble(objTable.Rows(C1)("pricenota")) & ", " & ToDouble(objTable.Rows(C1)("priceplatinum")) & ", " & ToDouble(objTable.Rows(C1)("pricegold")) & ", " & ToDouble(objTable.Rows(C1)("pricesilver").ToString) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Integer.Parse(objTable.Rows(C1)("mstexpoid")) & "', '" & Integer.Parse(objTable.Rows(C1)("mstexpdtloid")) & "', " & ToDouble(objTable.Rows(C1)("hpp")) & ", " & ToDouble(objTable.Rows(C1)("priceexpedisi")) & ", 'ACTIVE', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("PriceMinim")) & ", " & ToDouble(objTable.Rows(C1)("addcost")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    itemdtloid += 1
                Next
            End If
            sSql = "UPDATE ql_mstoid set lastoid = " & itemdtloid - 1 & " Where tablename = 'QL_mstpricedtl' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    imbSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.ToString & sSql, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.ToString & sSql, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, 1) : Exit Sub
        End Try
        Response.Redirect("~\Master\MstPriceCabang.aspx?awal=true")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("~\Master\MstPriceCabang.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        'If Session("TblDtl") Is Nothing Then
        '    showMessage("Please define detail data first!", 2, "")
        '    Exit Sub
        'End If
        'If UpdateCheckedMat() Then
        '    ShowReport()
        'End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click 
        BindData()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        JenisDDL.SelectedValue = "ALL"
        FilterTextMst.Text = ""
        BindData()
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        'UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dv As DataView = Session("TblMst").DefaultView 
            If dv.Count < 1 Then
                dv.RowFilter = ""
                showMessage("Please Select data to Print first.", 2)
            Else
                dv.RowFilter = ""
                PrintReport()
            End If
        Else
            showMessage("Please Select data to Print first.", 2)
        End If
    End Sub

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        'UpdateCheckedValue()
        gvList.PageIndex = e.NewPageIndex
        gvList.DataSource = Session("TblMst")
        gvList.DataBind()
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub gvList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvList.Sorting
        'UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt As DataTable = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvList.DataSource = Session("TblMst")
            gvList.DataBind()
        End If
    End Sub

    Protected Sub lkbSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("CommandName") = sender.CommandName
        Response.Redirect("~\Master\MstPriceCabang.aspx?oid=" & sender.ToolTip & "&cashbankstatus=" & Session("CommandName") & "")
    End Sub 

    Protected Sub BtnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddItem.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        BindMaterial()
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'" 
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lbAddListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddListMat.Click
        Try
            If Not Session("TblMat") Is Nothing Then
                If UpdateCheckedMat() Then
                    Dim dtTbl As DataTable = Session("TblMat")
                    Dim dtView As DataView = dtTbl.DefaultView
                    dtView.RowFilter = "CheckValue='True'"
                    Dim iCheck As Integer = dtView.Count
                    If iCheck > 0 Then
                        If Session("TblDtl") Is Nothing Then
                            CreateTblDetail()
                        End If
                        Dim objTable As DataTable = Session("TblDtl")
                        Dim dv As DataView = objTable.DefaultView
                        Dim objRow As DataRow
                        Dim counter As Integer = objTable.Rows.Count
                        Dim dDiscAmt As Double = 0
                        For C1 As Integer = 0 To dtView.Count - 1
                            dv.RowFilter = "itemoid=" & dtView(C1)("itemoid")
                            If dv.Count > 0 Then
                                dv.AllowEdit = True
                                dv(0)("branchCode") = ddlBranch.SelectedValue
                                dv(0)("itemoid") = Integer.Parse(dtView(C1)("itemoid"))
                                dv(0)("mstexpoid") = Integer.Parse(dtView(C1)("mstexpoid"))
                                dv(0)("mstexpdtloid") = Integer.Parse(dtView(C1)("mstexpdtloid"))
                                dv(0)("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                                dv(0)("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                                If ToDouble(dtView(C1)("pricekhusus")) = 0 Then
                                    dv(0)("pricekhusus") = ToMaskEdit(dtView(C1)("pricekhusus"), 3)
                                Else
                                    dv(0)("pricekhusus") = ToMaskEdit(dtView(C1)("pricekhusus") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricenormal")) = 0.0 Then
                                    dv(0)("pricenormal") = ToMaskEdit(dtView(C1)("pricenormal"), 3)
                                Else
                                    dv(0)("pricenormal") = ToMaskEdit(dtView(C1)("pricenormal") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricenota")) = 0.0 Then
                                    dv(0)("pricenota") = ToMaskEdit(dtView(C1)("pricenota"), 3)
                                Else
                                    dv(0)("pricenota") = ToMaskEdit(dtView(C1)("pricenota") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricegold")) = 0.0 Then
                                    dv(0)("pricegold") = ToMaskEdit(dtView(C1)("pricegold"), 3)
                                Else
                                    dv(0)("pricegold") = ToMaskEdit(dtView(C1)("pricegold") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("priceplatinum")) = 0.0 Then
                                    dv(0)("priceplatinum") = ToMaskEdit(dtView(C1)("priceplatinum"), 3)
                                Else
                                    dv(0)("priceplatinum") = ToMaskEdit(dtView(C1)("priceplatinum") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricesilver")) = 0.0 Then
                                    dv(0)("pricesilver") = ToMaskEdit(dtView(C1)("pricesilver"), 3)
                                Else
                                    dv(0)("pricesilver") = ToMaskEdit(dtView(C1)("pricesilver") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("PriceMinim")) = 0.0 Then
                                    dv(0)("PriceMinim") = ToMaskEdit(dtView(C1)("PriceMinim"), 3)
                                Else
                                    dv(0)("PriceMinim") = ToMaskEdit(dtView(C1)("PriceMinim") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If
  
                                dv(0)("priceexpedisi") = ToMaskEdit(dtView(C1)("priceexpedisi"), 3)
                                dv(0)("addcost") = ToMaskEdit(dtView(C1)("addcost"), 3)
                                dv(0)("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                                dv(0)("keterangan") = Tchar(dtView(C1)("keterangan"))
                                dv(0)("active_flag") = "ACTIVE"
                                dv(0)("createdate") = "1/1/1900"
                                dv(0)("createtime") = "1/1/1900"
                            Else
                                counter += 1
                                objRow = objTable.NewRow()
                                objRow("seq") = counter
                                objRow("branchCode") = ddlBranch.SelectedValue
                                objRow("itemoid") = Integer.Parse(dtView(C1)("itemoid"))
                                objRow("mstexpoid") = Integer.Parse(dtView(C1)("mstexpoid"))
                                objRow("mstexpdtloid") = Integer.Parse(dtView(C1)("mstexpdtloid"))
                                objRow("itemdesc") = Tchar(dtView(C1)("itemdesc").ToString)
                                objRow("itemcode") = Tchar(dtView(C1)("itemcode").ToString)
                                If ToDouble(dtView(C1)("pricenormal")) = 0.0 Then
                                    objRow("pricenormal") = ToMaskEdit(dtView(C1)("pricenormal"), 3)
                                Else
                                    objRow("pricenormal") = ToMaskEdit(dtView(C1)("pricenormal") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricekhusus")) = 0.0 Then
                                    objRow("pricekhusus") = ToMaskEdit(dtView(C1)("pricekhusus"), 3)
                                Else
                                    objRow("pricekhusus") = ToMaskEdit(dtView(C1)("pricekhusus") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricenota")) = 0.0 Then
                                    objRow("pricenota") = ToMaskEdit(dtView(C1)("pricenota"), 3)
                                Else
                                    objRow("pricenota") = ToMaskEdit(dtView(C1)("pricenota") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricegold")) = 0.0 Then
                                    objRow("pricegold") = ToMaskEdit(dtView(C1)("pricegold"), 3)
                                Else
                                    objRow("pricegold") = ToMaskEdit(dtView(C1)("pricegold") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("priceplatinum")) = 0.0 Then
                                    objRow("priceplatinum") = ToMaskEdit(dtView(C1)("priceplatinum"), 3)
                                Else
                                    objRow("priceplatinum") = ToMaskEdit(dtView(C1)("priceplatinum") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("pricesilver")) = 0.0 Then
                                    objRow("pricesilver") = ToMaskEdit(dtView(C1)("pricesilver"), 3)
                                Else
                                    objRow("pricesilver") = ToMaskEdit(dtView(C1)("pricesilver") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                If ToDouble(dtView(C1)("PriceMinim")) = 0.0 Then
                                    objRow("PriceMinim") = ToMaskEdit(dtView(C1)("PriceMinim"), 3)
                                Else
                                    objRow("PriceMinim") = ToMaskEdit(dtView(C1)("PriceMinim") + dtView(C1)("priceexpedisi") + dtView(C1)("addcost"), 3)
                                End If

                                objRow("priceexpedisi") = ToMaskEdit(dtView(C1)("priceexpedisi"), 3)
                                objRow("addcost") = ToMaskEdit(dtView(C1)("addcost"), 3)
                                objRow("hpp") = ToMaskEdit(dtView(C1)("hpp"), 3)
                                objRow("keterangan") = Tchar(dtView(C1)("keterangan"))
                                objRow("active_flag") = "ACTIVE"
                                objRow("createdate") = "1/1/1900"
                                objRow("createtime") = "1/1/1900"
                                objTable.Rows.Add(objRow)
                            End If
                            dv.RowFilter = ""
                        Next
                        dtView.RowFilter = ""
                        Session("TblDtl") = objTable
                        gvFindCrd.DataSource = objTable
                        gvFindCrd.DataBind()
                        gvFindCrd.Columns(1).Visible = False
                        gvFindCrd.Columns(2).Visible = False
                        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                        'ClearDetail()
                    Else
                        Session("WarningListMat") = "Please select material to add to list!"
                        showMessage(Session("WarningListMat"), 2)
                        Exit Sub
                    End If
                End If
            Else
                Session("WarningListMat") = "Please show some material data first!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
        'gvListMat.PageIndex = e.NewPageIndex
        'If UpdateCheckedMat() Then
        '    Dim dtTbl As DataTable = Session("TblMat")
        '    Dim dtView As DataView = dtTbl.DefaultView
        '    Dim sFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        '    dtView.RowFilter = sFilter
        '    If dtView.Count > 0 Then
        '        Session("TblMatView") = dtView.ToTable
        '        gvListMat.DataSource = Session("TblMatView")
        '        gvListMat.DataBind() : dtView.RowFilter = ""
        '        mpeListMat.Show()
        '    Else
        '        dtView.RowFilter = ""
        '        showMessage("Data Barang tidak ada..!!", 2)
        '    End If
        'Else
        '    mpeListMat.Show()
        'End If
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
             
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc1 As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc1
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc2 As System.Web.UI.ControlCollection = e.Row.Cells(6).Controls
            For Each myControl As System.Web.UI.Control In cc2
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc3 As System.Web.UI.ControlCollection = e.Row.Cells(7).Controls
            For Each myControl As System.Web.UI.Control In cc3
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc4 As System.Web.UI.ControlCollection = e.Row.Cells(8).Controls
            For Each myControl As System.Web.UI.Control In cc4
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc5 As System.Web.UI.ControlCollection = e.Row.Cells(9).Controls
            For Each myControl As System.Web.UI.Control In cc5
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc6 As System.Web.UI.ControlCollection = e.Row.Cells(10).Controls
            For Each myControl As System.Web.UI.Control In cc6
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

            Dim cc7 As System.Web.UI.ControlCollection = e.Row.Cells(11).Controls
            For Each myControl As System.Web.UI.Control In cc7
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = "0.00"
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next

        End If
    End Sub

    Protected Sub pExpedisi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If UpdateCheckedMat2() Then
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False) 
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If 
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub
#End Region
End Class