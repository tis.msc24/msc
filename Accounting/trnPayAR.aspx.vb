﻿Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunctionAccounting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnPayAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = 4
    Public DefCounterCashBankNo As String = "4"
    Public DefaultFormatCounter As Int16 = 4
    Dim conn As New SqlConnection(ConnStr)
    Dim conn2 As New SqlConnection(ConnStr)
    Dim objCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dv As DataView
    Dim dtStaticItem As DataTable
    Dim mySqlDA As New SqlDataAdapter(sSql, ConnStr)
    Dim objDs As New DataSet
    Dim cKoneksi As New Koneksi
    Dim dp, DPsc As Decimal
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim cProc As New ClassProcedure
    Public sqlTempSearch As String = ""
    Private cBulan() As Char = {"A", "B", "C", "", "D", "E", "F", "G", "H", "I", "J", "K", "L"}
    Dim dRate As Double
#End Region

#Region "Procedures"
    Private Sub ClearDtlSelisih()
        If otherAcctgoid.Items.Count > 1 Then
            otherAcctgoid.SelectedIndex = 0
        End If
        amtdtlselisih.Text = ""
        gvDtlSelisih.Columns(5).Visible = True
        gvDtlSelisih.SelectedIndex = -1
        dtlnoteselisih.Text = ""
        stateDtlSls.Text = "New Selisih"
    End Sub

    Private Sub FillDPAccount(ByVal iDPOid As Integer)
        Dim dparacctgoid As Integer
        sSql = "SELECT isnull(trndpaRacctgoid,0) acctgoid FROM QL_trndpaR WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        dparacctgoid = GetStrData(sSql)
        If dparacctgoid = 0 Then
            showMessage("Acctgoid kosong / nol pada DP", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        cashbankacctgoid.SelectedValue = CStr(cKoneksi.ambilscalar(sSql))

        sSql = "SELECT currencyoid FROM QL_trndpaR WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        dp_currency.Text = CStr(cKoneksi.ambilscalar(sSql))
        CurrencyOid.Enabled = True
        If dp_currency.Text = 2 Then
            CurrencyOid.SelectedValue = 2
            CurrencyOid.Enabled = False

        End If
    End Sub

    Private Sub FillDPBalance(ByVal iDPOid As Integer)
        sSql = "SELECT ISNULL(dp.trndpaRamt-dp.trndpaRacumamt,0) "
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(pap.payamt) FROM QL_trnpayaR pap " & _
                "WHERE pap.paybankoid=dp.trndpaRoid AND pap.cashbankoid='" & Session("oid") & "'),0) "
        End If
        sSql &= "FROM QL_trndpaR dp WHERE cmpcode='" & cmpcode & "' AND trndpaRoid='" & iDPOid & "'"
        dpbalance.Text = ToMaskEdit(ToDouble(CStr(cKoneksi.ambilscalar(sSql))), 3)

        payrefno.Text = RTrim(GetStrData("select trndpaRno from ql_trndpaR where trndpaRoid=" & iDPOid))
    End Sub

    Private Sub InitDPAR(ByVal iSuppOid As Integer)
        sSql = "SELECT dp.trndpaRoid,dp.trndpaRno/*+' ('+CONVERT(VARCHAR,dp.trndpaRdate,101)+')'*/ AS no FROM QL_trndpaR dp WHERE cmpcode='" & cmpcode & "' AND dp.trndpaRstatus='POST' and dp.flagdp NOT IN ('TITIPAN') AND dp.CUSToid=" & iSuppOid & " AND (ISNULL(dp.trndpaRamt-dp.trndpaRacumamt,0)"
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            sSql &= "+ISNULL((SELECT SUM(par.payamt) FROM QL_trnpayaR par " & _
                "WHERE par.paybankoid=dp.trndpaRoid AND par.cashbankoid='" & Session("oid") & "'),0)"
        End If
        sSql &= ")>0 ORDER BY dp.trndpaRdate,dp.trndpaRno "
        FillDDL(trndpapoid, sSql)
    End Sub

    Private Sub GenerateDefaultNo(ByVal sFlag As String, ByVal iAcctgOid As Integer, ByVal dDate As Date)
        ' BKK/10FKBE0001
        Dim sCBType As String
        Select Case sFlag
            Case "CASH" : sCBType = "BKM"
            Case "BANK" : sCBType = "BBM"
            Case "GIRO" : sCBType = "BGM"
            Case "CREDIT CARD" : sCBType = "BCM"
            Case "DP" : sCBType = "BDM"
            Case Else : sCBType = "BLM"
        End Select
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & ddlcabang.SelectedValue & "' and gengroup='CABANG'")
        Dim sNo As String = sCBType & "/" & cabang & "/" & Format(dDate, "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1  FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & ddlcabang.SelectedValue & "'"
        defcbno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
    End Sub

    Private Sub BinddataGiro()
        Dim rate As Double = InvoiceRate(CurrencyOid.SelectedValue)
        If ToDouble(trnsuppoid.Text) > 0 Then
            If CurrencyOid.SelectedValue = 2 Then
                sSql = "SELECT girodtloid, Trans_No, girono, m.gendesc Bank, bankoid, NoRekening, convert(char(10),d.GiroDueDate,103) GiroDueDate,isnull(amount,0.0) - isnull((select sum(cashbankamountidr) from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid and cb.pic = " & trnsuppoid.Text & " and cb.cashbankno NOT IN (select SUBSTRING(cashbanknote,20,LEN(cashbanknote)) from QL_trncashbankmst where SUBSTRING(cashbanknote,20,LEN(cashbanknote)) = (isnull((select cb.cashbankno from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid ),'')))),0.0) as amount from ql_giro g INNER JOIN QL_GIRODTL d on g.girooid=d.girooid INNER JOIN QL_mstgen m on m.genoid=d.bankoid AND m.gengroup='BANK NAME' where g.status='Post' and custoid= " & trnsuppoid.Text & " and d.status IN ('Change', 'Bank') and isnull((select sum(cashbankamountidr) from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid and cb.cashbankno NOT IN (select SUBSTRING(cashbanknote,20,LEN(cashbanknote)) from QL_trncashbankmst where SUBSTRING(cashbanknote,20,LEN(cashbanknote)) = (isnull((select cb.cashbankno from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid ),'')))),00) - d.amount <> 0 and " & DDLSuppIDX.SelectedValue & " like '%" & Tchar(txtFindSuppIDX.Text) & "%' "
            Else
                sSql = "SELECT girodtloid, Trans_No, girono, m.gendesc Bank, bankoid, NoRekening, convert(char(10),d.GiroDueDate,103) GiroDueDate,isnull(amount,0.0) - isnull((select sum(cashbankamountidr) from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid and cb.pic = " & trnsuppoid.Text & " and cb.cashbankno NOT IN (select SUBSTRING(cashbanknote,20,LEN(cashbanknote)) from QL_trncashbankmst where SUBSTRING(cashbanknote,20,LEN(cashbanknote)) = (isnull((select cb.cashbankno from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid ),'')))),0.0) as amount from ql_giro g INNER JOIN QL_GIRODTL d on g.girooid=d.girooid INNER JOIN QL_mstgen m on m.genoid=d.bankoid AND m.gengroup='BANK NAME' where g.status='Post' and custoid= " & trnsuppoid.Text & " and d.status IN ('Change', 'Bank') and isnull((select sum(cashbankamountidr) from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid and cb.cashbankno NOT IN (select SUBSTRING(cashbanknote,20,LEN(cashbanknote)) from QL_trncashbankmst where SUBSTRING(cashbanknote,20,LEN(cashbanknote)) = (isnull((select cb.cashbankno from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid ),'')))),00) - d.amount <> 0 and " & DDLSuppIDX.SelectedValue & " like '%" & Tchar(txtFindSuppIDX.Text) & "%' "
            End If

            Dim tbldt As DataTable = CreateDataTableFromSQL(sSql)
            Session("tblgiro") = tbldt
            gvSupplierX.DataSource = tbldt
            gvSupplierX.DataBind()
            If tbldt.Rows.Count > 0 Then
                hiddenbtn2sX.Visible = True : Panel1X.Visible = True
                ModalPopupExtender3sX.Show()
            Else
                showMessage("Tidak ada data giro", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            End If
        Else
            showMessage("Customer belum di pilih", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
        End If
    End Sub

    Private Sub SetOtherAccount(ByVal bState As Boolean)
        otherAcctgoid.Enabled = bState
        lkbAddDtlSlisih.Visible = bState
        lkbClearDtlSlisih.Visible = bState
        If bState Then
            otherAcctgoid.CssClass = "inpText"
        Else
            otherAcctgoid.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub Fill_payflag()
        divGiro.Visible = False : lbldpno.Visible = False
        trndpapoid.Visible = False : lbldpbalance.Visible = False
        dpbalance.Visible = False : Session("payflag") = payflag.SelectedValue
        If payflag.SelectedValue = "CASH" Then
            lblPayType.Text = "Cash" : lblpayrefno.Visible = False : payrefno.Visible = False
            lblpayduedate.Visible = False : payduedate.Visible = False
            btnDueDate.Visible = False : lblnotice.Visible = False
            lblBankName.Visible = False : ddlBankName.Visible = False
            LblBank.Visible = False : Label4.Visible = False
            dd_bankgiro.Visible = False
        ElseIf payflag.SelectedValue = "BANK" Then
            lblpayrefno.Text = "No. Rekening"
            lblBankName.Visible = False : ddlBankName.Visible = False
            lblPayType.Text = "Bank" : lblpayrefno.Visible = True
            payrefno.Visible = True : lblpayduedate.Visible = True
            payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : LblBank.Visible = False
            Label4.Visible = False : dd_bankgiro.Visible = False
        ElseIf payflag.SelectedValue = "GIRO" Then
            divGiro.Visible = True : lblpayrefno.Text = "No. Giro"
            lblPayType.Text = "Giro" : lblpayrefno.Visible = True
            payrefno.Visible = True : lblpayduedate.Visible = True
            payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : lblBankName.Visible = False ' True ' Buat apa sih ??
            ddlBankName.Visible = False : dd_bankgiro.Visible = True ' True ' Buat apa sih ??
            creditsearch.Visible = True : CREDITCLEAR.Visible = True
            payrefno.Enabled = False : payrefno.CssClass = "inpTextDisabled"
            LblBank.Visible = True : Label4.Visible = True
        ElseIf payflag.SelectedValue = "CREDIT CARD" Then
            lblPayType.Text = "Credit Card" : lblpayrefno.Visible = True
            payrefno.Visible = True : lblpayduedate.Visible = True
            payduedate.Visible = True : btnDueDate.Visible = True
            lblnotice.Visible = True : lblBankName.Visible = False ' True ' Buat apa sih ??
            ddlBankName.Visible = False ' True ' Buat apa sih ??
            LblBank.Visible = False : Label4.Visible = False
            dd_bankgiro.Visible = False
        ElseIf payflag.SelectedValue = "DP" Then
            lblPayType.Text = "Down Payment" : lblpayrefno.Visible = False
            payrefno.Visible = False : lblpayduedate.Visible = False
            payduedate.Visible = False : btnDueDate.Visible = False
            lblnotice.Visible = False : lblBankName.Visible = False
            ddlBankName.Visible = False : lbldpno.Visible = True
            trndpapoid.Visible = True : lbldpbalance.Visible = True
            dpbalance.Visible = True : LblBank.Visible = False
            Label4.Visible = False : dd_bankgiro.Visible = False
            creditsearch.Visible = False : CREDITCLEAR.Visible = False
            If trnsuppoid.Text.Trim = "" Then
                showMessage("Pilih Supplier dahulu !!!", CompnyName & " - INFORMATION", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
        Dim err As String = ""
        initDDLcashbank(payflag.SelectedValue, err)

        'If err = "" Then
        '    GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, GetServerTime())
        'End If
        If payflag.SelectedValue = "GIRO" Then
            creditsearch.Visible = True : CREDITCLEAR.Visible = True
            If Session("oid") = "" Then
                payrefno.Text = "" : payrefno.Enabled = False
                payrefno.CssClass = "inpTextDisabled"
            Else
                creditsearch.Visible = False : CREDITCLEAR.Visible = False
            End If
            AmountGiro.Visible = True
        Else
            AmountGiro.Visible = False
            creditsearch.Visible = False : CREDITCLEAR.Visible = False
            code.Visible = False : code.Text = "" : payrefno.Text = ""
            payrefno.Enabled = True : payrefno.CssClass = "inpText"
        End If

        If Page.IsPostBack Then
            If payflag.SelectedValue = "DP" Then
                InitDPAR(trnsuppoid.Text)
                If trndpapoid.Items.Count > 0 Then
                    FillDPBalance(trndpapoid.SelectedValue)
                    FillDPAccount(trndpapoid.SelectedValue)
                Else
                    FillDPBalance(0)
                End If
            Else
                CurrencyOid.SelectedValue = "1"
                CurrencyOid.Enabled = True
            End If
        End If
        If lblPOST.Text = "POST" Then
            cashbankacctgoid.SelectedValue = Session("cashbankoid")
        End If
    End Sub

    Private Sub CalculateTotalPayment()
        amtpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 3)
        If amtpayment.Text = 0 Then
            amtpayment.Text = 1
        End If
        If ToDouble(amtpayment.Text) > ToDouble(APAmt.Text) Then
            chkOther.Enabled = False
            chkOther.Checked = True : DDLOtherType.SelectedValue = "+"
            SetOtherAccount(True)
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            otheramt.Text = ToMaskEdit(ToDouble(amtpayment.Text) - ToDouble(APAmt.Text), 3)
            amtdtlselisih.Text = otheramt.Text
        ElseIf ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
            chkOther.Enabled = True
            chkOther.Checked = False : DDLOtherType.SelectedValue = "-"
            DDLOtherType_SelectedIndexChanged(Nothing, Nothing)
            SetOtherAccount(True)
            otheramt.Text = ToMaskEdit(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text), 3)
            amtdtlselisih.Text = otheramt.Text
        Else
            chkOther.Checked = False
            chkOther.Enabled = False
            SetOtherAccount(False)
            otheramt.Text = ToMaskEdit(0, 3)
            amtdtlselisih.Text = otheramt.Text
        End If

        totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 3)
        CalculateSelisih()

    End Sub

    Private Sub CalculateSelisih()
        ' Calculate total Detail Selisih
        If Not Session("DtlSelisih") Is Nothing Then
            Dim objCek As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = Math.Abs(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString))
            If dTotalSelisih < 0 Then dTotalSelisih *= -1
            otheramt.Text = ToMaskEdit(dTotalSelisih, 3)
            If ToDouble(amtpayment.Text) < ToDouble(APAmt.Text) Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + dTotalSelisih, 3)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 3)
            End If
        End If
    End Sub

    Public Sub InitAllDDL()
        FillDDL(CurrencyOid, "select currencyoid,currencycode from QL_mstcurr where cmpcode='" & cmpcode & "' order by currencyoid")
        FillCurrencyRate(CurrencyOid.SelectedItem.Value)
    End Sub

    Private Sub BankDdl()
        FillDDL(dd_bankgiro, "Select distinct bankoid,isnull((Select gb.gendesc FROM QL_mstgen gb Where gb.genoid=gd.bankoid AND gengroup ='BANK NAME'),'') Bank From ql_girodtl gd")
    End Sub

    Private Sub FillCurrencyRate(ByVal iOid As Int32)
        sSql = "select top 1 D.curratestoIDRbeli from QL_mstcurrhist D INNER JOIN ql_mstcurr m on M.CMPCODE=D.cmpcode AND m.currencyoid=d.curroid and  m.cmpcode='" & cmpcode & "' and currencyoid=" & iOid & " order by d.currdate desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        currencyRate.Text = ToMaskEdit(objCmd.ExecuteScalar, 3)
        conn.Close()
    End Sub

    Private Sub CekPaymentCurrencyToInvoice()
        If CurrencyOid.SelectedValue = invCurrOid.Text Then
            If ToDouble(invCurrRate.Text) <> ToDouble(currencyRate.Text) Then
                Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
                currencyRate.Text = ToMaskEdit(ToDouble(invCurrRate.Text), 3)
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Public Sub UncheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub InitOtherAcctg(ByVal sType As String)
        If sType = "+" Then
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAR_DIFFERENCE_+1", ddlcabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_PAYAR_DIFFERENCE_+1 di master interface !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        ElseIf sType = "-" Then
            FillDDLAcctg(otherAcctgoid, "VAR_PAYAR_DIFFERENCE_-1", ddlcabang.SelectedValue)
            If otherAcctgoid.Items.Count = 0 Then
                showMessage("Silahkan seting VAR_PAYAR_DIFFERENCE_-1 di master interface !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        End If
    End Sub

    Public Sub initDDLcashbank(ByVal cashbank As String, ByVal err As String)
        If lblPOST.Text = "POST" Then
            btnSave.Visible = False
        Else
            btnSave.Visible = True
        End If
        Dim sVar As String = ""
        If Trim(cashbank) = "CASH" Then
            sVar = "VAR_CASH"
        ElseIf Trim(cashbank) = "BANK" Then
            sVar = "VAR_BANK"
        ElseIf Trim(cashbank) = "GIRO" Then
            sVar = "VAR_GIRO_PIUTANG"
        ElseIf Trim(cashbank) = "DP" Then
            sVar = "VAR_DPAR"
        End If
        'Dim sAdd As String = " AND a.acctgflag='A' "
        'If lblPOST.Text = "POST" Then
        '    sAdd = " AND a.acctgflag IN ('A','I') "
        'End If

        'sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sVar & "' AND interfaceres1='" & ddlcabang.SelectedValue & "'"
        'Dim sCode As String = GetStrData(sSql)
        'If sCode = "?" Then
        '    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE interfaceres1='" & ddlcabang.SelectedValue & "' AND interfacevar='" & sVar & "'"
        '    sCode = GetStrData(sSql)
        'End If
        'If sCode <> "" Then
        '    sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' " & sAdd & " AND ("
        '    Dim sSplitCode() As String = sCode.Split(",")
        '    For C1 As Integer = 0 To sSplitCode.Length - 1
        '        sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
        '        If C1 < sSplitCode.Length - 1 Then
        '            sSql &= " OR "
        '        End If
        '    Next
        '    sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) " & sAdd & " ORDER BY acctgdesc"
        'End If
        'FillDDL(cashbankacctgoid, sSql)
        FillDDLAcctg(cashbankacctgoid, sVar, ddlcabang.SelectedValue)
        If cashbankacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account " & sVar & " di master accounting!! " & sVar & " !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Err = "Isi/Buat account " & sVar & " di master accounting!! " & sVar & " !Isi/Buat account " & sVar & " di master accounting!! " & sVar & " !"
            Exit Sub
        Else
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, GetServerTime())
        End If
    End Sub

    Public Sub BindData(ByVal sWhere As String)
        '/*= isnull(STUFF((SELECT ',' + (select trnjualno from ql_trnjualmst where trnjualmstoid = jm.payrefoid and branch_code =jm.branch_code and c.trnartype = 'PAYAR' UNION ALL select trnbiayaeksno trnjualno from ql_trnbiayaeksmst where trnbiayaeksoid = jm.payrefoid and branch_code =jm.branch_code and c.trnartype = 'PAYAREXP') FROM QL_trnpayar jm INNER JOIN QL_conar c ON c.payrefoid = jm.paymentoid where jm.cashbankoid=cb.cashbankoid AND payflag <> 'OTHER' FOR XML PATH ('')), 1, 1, ''),'')*/ 
        Dim sqlSelect As String = "SELECT DISTINCT cb.cashbankoid, cb.branch_code, cashbankno, case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'BANK' when 'BCM' then 'CREDIT CARD' when 'BDM' then 'DP'  WHEN 'BGM' then 'GIRO' else 'LAIN-LAIN' end payflag, cashbankdate as date, (SELECT FLOOR(SUM(payamt)) FROM QL_trnpayar p WHERE p.cashbankoid=cb.cashbankoid) AS payamt, (SELECT FLOOR(SUM(payamt)) FROM QL_trnpayar p WHERE p.cashbankoid=cb.cashbankoid) * cb.cashbankcurrate AS payamtrp, cashbankstatus, cashbanknote, '' currencycode,cb.cashbankcurrate, s.custname suppliername, '' payrefno, '' bank, '' trnjualno FROM QL_trncashbankmst cb INNER JOIN ql_mstcust s on s.cmpcode=cb.cmpcode and s.custoid=cb.pic where cb.cmpcode='" & cmpcode & "' AND cb.cashbankgroup='AR' " & sWhere & " "
        If fCabang.SelectedValue <> "ALL" Then
            sqlSelect &= "And cb.branch_code = '" & fCabang.SelectedValue & "' "
        End If
        sqlSelect = sqlSelect & " ORDER BY " & orderby.SelectedValue
        sqlTempSearch = sqlSelect ' Untuk simpan history search

        Dim objTable As DataTable = cKoneksi.ambiltabel(sqlSelect, "ql_trncashbankmst")
        Session("tbldata") = objTable
        GVmstPAYAP.DataSource = objTable
        GVmstPAYAP.DataBind()
        calcTotalInGrid()
    End Sub

    Private Sub filterGVS(ByVal code As String, ByVal name As String, ByVal cmpcode As String, ByVal flag As String, ByVal branch_code As String)
        Dim statusGiro As String = ""
        If payflag.SelectedValue = "GIRO" Then
            statusGiro = "and custoid in (select custoid from QL_GIRO g where g.custoid = c.custoid )"
        End If
        sSql = "SELECT custOID AS ID,custCODE AS Code, custNAME AS Name FROM QL_MSTcust AS c WHERE (CMPCODE = '" & cmpcode & "') and (branch_code = '" & ddlcabang.SelectedValue & "') AND (" & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text) & "%') AND (" & DDLSuppID.SelectedValue & " LIKE '%" & Tchar(txtFindSuppID.Text) & "%') and custoid in ( select m.trncustoid from ql_trnjualmst m where m.trnamtjualnetto>(m.accumpayment+m.amtretur) and trnjualstatus in ('Approved','Post') Union All Select custoid from ql_trnbiayaeksmst be Where branch_code='" & ddlcabang.SelectedValue & "' AND be.amtekspedisi-Isnull((Select SUM(amtbayar) From QL_conar con Where be.custoid=con.custoid AND be.branch_code=con.branch_code AND be.trnbiayaeksoid=con.refoid AND con.payrefoid>0),0.0000)>0.0000 AND be.status='POST') " & statusGiro & " ORDER BY custCODE"

        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "QL_MSTcust")
        Session("MSTcust") = objTable
        gvSupplier.DataSource = objTable
        gvSupplier.DataBind()
    End Sub

    Public Sub CheckAll()
        Dim objTable As DataTable
        Dim objRow() As DataRow
        objTable = Session("tbldata")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = 0 To objRow.Length() - 1
                If Trim(objRow(i)("cashbankstatus").ToString) <> "POST" Then
                    Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(i)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                            End If
                        Next
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub generateCashBankID()
        cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        cashbankno.Text = GenerateID("QL_trncashbankmst", cmpcode)
    End Sub

    Public Sub FillTextBox(ByVal vpayid As String, ByVal branch As String)
        Try
            sSql = "SELECT DISTINCT cb.cashbankoid, cb.upduser, cb.updtime, case cashbanktype when 'BKM' then 'CASH' when 'BBM' then 'BANK' when 'BGM' then 'GIRO' when 'BDM' then 'DP' when 'BCM' then 'CREDIT CARD' end payflag, cashbankacctgoid, payrefno, paybankoid, ISNULL(payduedate,'1/1/1900')payduedate, cashbankstatus, custname suppname, p.custoid suppoid, cb.cashbankdate, cb.upduser, cb.updtime, cashbanknote, cashbankno, cb.cashbankcurroid,cb.cashbankcurrate, cb.giroref, bankoid, cb.cashbankrefno, cb.branch_code, cb.cashbankamountidr FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar p ON p.cashbankoid=cb.cashbankoid AND p.branch_code = cb.branch_code INNER JOIN QL_mstcust s ON s.custoid=p.custoid AND s.cmpcode=p.cmpcode WHERE cb.cmpcode='" & cmpcode & "' AND cb.cashbankoid='" & vpayid & "' AND cb.branch_code='" & branch & "'"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objCmd.CommandText = sSql
            xreader = objCmd.ExecuteReader
            Dim q As String = String.Empty
            If xreader.HasRows Then
                While xreader.Read
                    Fill_payflag()
                    cashbankoid.Text = Trim(xreader.GetValue(0))
                    cashbankno.Text = Trim(xreader("cashbankno"))
                    defcbno.Text = Trim(xreader("cashbankno"))
                    suppnames.Text = xreader("suppname").ToString
                    trnsuppoid.Text = xreader("suppoid").ToString
                    payflag.SelectedValue = Trim(xreader.GetValue(3))
                    HiddenField1.Value = payflag.SelectedValue
                    payduedate.Text = Format(xreader("payduedate"), "dd/MM/yyyy")
                    HiddenField2.Value = Trim(xreader.GetValue(4))
                    Session("PayRef") = Trim(xreader("payrefno").ToString)
                    ddlcabang.SelectedValue = Trim(xreader("branch_code").ToString)
                    ddlcabang.Enabled = False
                    If Session("currsess") = False Then
                        CurrencyOid.SelectedValue = xreader("cashbankcurroid").ToString
                    Else
                        CurrencyOid.SelectedValue = CurrencyOid.SelectedValue
                    End If
                    currencyRate.Text = ToMaskEdit(ToDouble(xreader("cashbankcurrate").ToString), 3)
                    cashbanknote.Text = Trim(xreader("cashbanknote"))
                    updUser.Text = xreader("upduser")
                    updTime.Text = xreader("updtime")
                    code.Text = Session("PayRef")
                    PaymentDate.Text = Format(CDate(Trim(xreader("cashbankdate").ToString)), "dd/MM/yyyy")

                    If payflag.SelectedValue = "BANK" Then
                        payrefno.Visible = True : lblpayrefno.Visible = True
                        lblpayrefno.Text = "No. Rekening"
                        Session("cashbankrefno") = Trim(xreader("cashbankrefno"))
                    Else
                        Session("payrefno") = Trim(xreader("cashbankrefno").ToString)
                    End If

                    lblPOST.Text = Trim(xreader.GetValue(8))
                    Session("cashbankoid") = xreader("cashbankacctgoid")
                    Session("giroOid") = xreader("giroref").ToString
                    initDDLcashbank(payflag.SelectedValue, "")
                    cashbankacctgoid.SelectedValue = Trim(xreader("cashbankacctgoid"))
                    Session("totalbayar") = ToDouble(xreader("cashbankamountidr"))
                End While
            End If
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString & "<BR><BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox")
        End Try
        'Fill_payflag()
        If payflag.SelectedValue = "GIRO" Then
            If Session("giroOid") = "" Then
                Session("giroOid") = 0
            End If
            sSql = "Select amount - isnull((select sum(cashbankamountidr) from QL_trncashbankmst cb where cb.girodtloid = d.girodtloid  ),0.0) from ql_girodtl d where girodtloid = " & Session("giroOid") & ""
            AmountGiro.Text = ToMaskEdit(ToDouble(GetStrData(sSql)), 3)
            AmountGiro.Visible = True
            sSql = "Select bankoid from ql_girodtl Where girodtloid = " & Session("giroOid") & ""
            dd_bankgiro.SelectedValue = GetStrData(sSql)
            dd_bankgiro.Visible = True : dd_bankgiro.Enabled = False
        End If
        If payflag.SelectedValue = "DP" Then
            payrefno.Text = Session("PayRef")
        ElseIf payflag.SelectedValue = "BANK" Then
            payrefno.Text = Session("cashbankrefno")
        Else
            payrefno.Text = Session("PayRef")
        End If
        If lblPOST.Text = "POST" Then
            cashbankacctgoid.SelectedValue = Session("cashbankoid")
        End If
        'defcbno.Text = cashbankno.Text
        trndpapoid.Items.Clear()
        trndpapoid.Items.Add(Session("PayRef"))
        sSql = "select trndparoid from ql_trndpar where trndparno='" & Session("PayRef") & "'"
        trndpapoid.Items.Add(Session("PayRef"))
        trndpapoid.Items(0).Value = GetStrData(sSql)

        sSql = "select currencyoid from  ql_trndpar where trndparno='" & Session("PayRef") & "'"
        dp_currency.Text = GetStrData(sSql)

        If dp_currency.Text = "2" Then
            CurrencyOid.Enabled = False
        Else
            CurrencyOid.Enabled = True
        End If
        If lblPOST.Text = "POST" Then
            If CurrencyOid.SelectedValue = "1" Then
                sSql = "select (dpamtidr-payamtidr) trndparamt from ql_trnpayar where cashbankoid=" & vpayid & ""
            Else
                sSql = "select (dpamtusd-payamtusd) trndparamt from ql_trnpayar where cashbankoid=" & vpayid & ""
            End If

        Else
            If CurrencyOid.SelectedValue = "1" Then
                sSql = "select (trndparamtidr-trndparacumamtidr) trndparamt from ql_trndpar where trndparno='" & payrefno.Text.Trim & "'"
            Else
                sSql = "select (trndparamtusd-trndparacumamtusd) trndparamt from ql_trndpar where trndparno='" & payrefno.Text.Trim & "'"
            End If
        End If
        If payflag.SelectedValue = "DP" Then
            dpbalance.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text), 3)
            dpbalance.Visible = True
            lbldpbalance.Visible = True
        End If
        sSql = "select * from (SELECT distinct pap.cmpcode,pap.branch_code ,trnjualmstoid,paymentoid,pap.payduedate, pap.trndparoid trndpapoid, pap.DPAmtidr DPAmt , pap.payrefno,pap.cashbankoid, cb.cashbankno,pap.payreftype,pap.payrefoid,pap.payacctgoid,paynote,payamtidr payamt , bm.trnjualno trnbelino,bm.trncustoid trnsuppoid,s.custname suppname, (select con.amttransidr from QL_conar con where con.refoid = ap.refoid and con.branch_code = ap.branch_code and con.custoid = ap.custoid and payrefoid = 0 and con.reftype = 'QL_trnjualmst') + ISNULL((SELECT SUM(db.amountidr) FROM QL_debitnote db where db.refoid=BM.TRNJUALMSTOID and db.reftype='PIUTANG' AND DB.cmpcode=AP.CMPCODE),0) amttrans, (select isnull(SUM(p2.payamtidr),0) From ql_trnpayar p2 INNER JOIN ql_conar cp on cp.payrefoid=p2.paymentoid and cp.reftype='QL_trnpayar' where p2.cmpcode=cb.cmpcode and p2.cashbankoid<>cb.cashbankoid and p2.paystatus='POST' and p2.payrefoid=pap.payrefoid AND p2.custoid=pap.custoid and p2.branch_code=pap.branch_code and p2.payreftype='ql_trnjualmst' and rtrim(p2.payflag)='' and p2.payrefno not like '%SR/%' ) + isnull(( SELECT  sum(ABS(ISNULL(pap.amount,0))) as kredit FROM QL_creditnote pap WHERE pap.refoid= bm.trnjualmstoid and pap.reftype='PIUTANG' ),0) amtpaid, bm.amtreturidr amtretur,(SELECT acc.acctgcode+'-'+acc.acctgdesc FROM ql_mstacctg acc WHERE pap.payacctgoid=acc.acctgoid) acctgdesc,bm.trntaxpct,'1' AS payseq,ISNULL(bm.currencyoid,2) as invCurrOid,ISNULL(bm.currencyrate,1) AS invCurrRate,(SELECT cr.currencycode FROM QL_mstcurr cr WHERE cr.cmpcode=bm.cmpcode AND cr.currencyoid=ISNULL(bm.currencyoid,2)) AS invCurrCode,(SELECT cr.currencycode+'-'+cr.currencydesc FROM QL_mstcurr cr WHERE cr.cmpcode=bm.cmpcode AND cr.currencyoid=ISNULL(bm.currencyoid,2)) AS invCurrDesc,'invPayment'=CASE WHEN cb.cashbankcurrate<ISNULL(bm.currencyrate,1) THEN pap.payamt/ISNULL(bm.currencyrate,1) WHEN cb.cashbankcurrate>ISNULL(bm.currencyrate,1) THEN pap.payamt*cb.cashbankcurrate ELSE pap.payamt END,pap.payflag AS flagdtl,pap.payres1, ap.trnartype AS paytype FROM ql_trnpayar pap INNER JOIN ql_conar ap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid  and pap.branch_code=ap.branch_code and ap.custoid = pap.custoid and ap.trnartype = 'PAYAR' INNER JOIN ql_trnjualmst bm ON bm.cmpcode=pap.cmpcode AND ap.refoid=bm.trnjualmstoid And pap.branch_code=bm.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=pap.cmpcode AND cb.cashbankoid=pap.cashbankoid AND cb.branch_code=pap.branch_code INNER JOIN ql_mstcust s ON bm.cmpcode=s.cmpcode AND bm.trncustoid=s.custoid " & _
        "UNION ALL " & _
"SELECT distinct e.cmpcode,e.branch_code ,trnbiayaeksoid,paymentoid,p.payduedate, p.trndparoid trndpapoid, p.DPAmtidr DPAmt , p.payrefno,p.cashbankoid, cb.cashbankno,p.payreftype,p.payrefoid,p.payacctgoid payacctgoid,paynote,payamtidr payamt , e.trnbiayaeksno trnbelino,e.custoid trnsuppoid,s.custname suppname, (select con.amttransidr from QL_conar con where con.refoid = ap.refoid and con.branch_code = ap.branch_code and con.custoid = ap.custoid and payrefoid = 0 and con.reftype = 'ql_trnbiayaeksmst') + ISNULL((SELECT SUM(db.amountidr) FROM QL_debitnote db where db.refoid=e.trnbiayaeksoid and db.reftype='PIUTANG' AND DB.cmpcode=AP.CMPCODE),0) amttrans , (select isnull(SUM(p2.payamtidr),0) From ql_trnpayar p2 INNER JOIN ql_conar cp on cp.payrefoid=p2.paymentoid and cp.reftype='QL_trnpayar' where p2.cmpcode=cb.cmpcode and p2.cashbankoid<>cb.cashbankoid and p2.paystatus='POST' and p2.payrefoid=p.payrefoid AND p2.custoid=p.custoid and p2.branch_code=p.branch_code and p2.payreftype='ql_trnjualmst' and rtrim(p2.payflag)='' and p2.payrefno not like '%SR/%' ) + isnull(( SELECT  sum(ABS(ISNULL(pap.amount,0))) as kredit FROM QL_creditnote pap WHERE pap.refoid= e.trnbiayaeksoid and pap.reftype='PIUTANG' ),0) amtpaid , 0.0 amtretur ,(SELECT acc.acctgcode+'-'+acc.acctgdesc FROM ql_mstacctg acc WHERE p.payacctgoid=acc.acctgoid) acctgdesc,0.0 trntaxpct,'1' AS payseq ,1 as invCurrOid,1 AS invCurrRate ,'IDR' AS invCurrCode ,(SELECT cr.currencycode+'-'+cr.currencydesc FROM QL_mstcurr cr WHERE cr.cmpcode=e.cmpcode AND cr.currencyoid=1) AS invCurrDesc,'invPayment'=CASE WHEN cb.cashbankcurrate<1 THEN p.payamt/1 WHEN cb.cashbankcurrate>1 THEN p.payamt*cb.cashbankcurrate ELSE p.payamt END,p.payflag AS flagdtl,p.payres1, ap.trnartype AS paytype FROM ql_trnpayar p INNER JOIN ql_conar ap ON p.cmpcode=ap.cmpcode AND p.paymentoid=ap.payrefoid  and p.branch_code=ap.branch_code and p.custoid = ap.custoid and ap.trnartype = 'PAYAREXP' INNER JOIN ql_trnbiayaeksmst e ON e.cmpcode=p.cmpcode AND ap.refoid=e.trnbiayaeksoid   and p.branch_code=ap.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=p.cmpcode AND cb.cashbankoid=p.cashbankoid AND cb.branch_code=p.branch_code INNER JOIN ql_mstcust s ON e.cmpcode=s.cmpcode AND e.custoid=s.custoid " & _
") tbl WHERE tbl.cashbankoid='" & vpayid & "' AND tbl.cmpcode='" & cmpcode & "' and tbl.branch_code = '" & ddlcabang.SelectedValue & "'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "dataCost")

        Dim dv As DataView = objDs.Tables("dataCost").DefaultView
        Session("dtlTable") = objDs.Tables("dataCost")
        conn.Close()
        Dim dtlTable As DataTable
        dtlTable = Session("dtlTable")
        If dtlTable.Rows.Count > 0 Then
            'find data acctg
            Dim VAR_DPAR As String = GetVarInterface("VAR_DPAR", ddlcabang.SelectedValue)
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & VAR_DPAR & "' AND interfaceres1='" & cmpcode & "'"
            Dim sCode As String = GetStrData(sSql)
            If sCode = "" Then
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & VAR_DPAR & "' AND interfaceres1='All'"
                sCode = GetStrData(sSql)
            End If
            If sCode <> "" Then
                sSql = "SELECT DISTINCT a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & cmpcode & "' AND ("
                Dim sSplitCode() As String = sCode.Split(",")
                For C1 As Integer = 0 To sSplitCode.Length - 1
                    sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                    If C1 < sSplitCode.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY a.acctgcode"
            End If
            'sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_DPAR & "%' AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
            Dim otableDP As DataTable = cKoneksi.ambiltabel(sSql, "acctgoid_dp")
            For C2 As Integer = 0 To dtlTable.Rows.Count - 1
                Dim ed As DataRow = dtlTable.Rows(C2)
                ed.BeginEdit()
                ed("payseq") += C2

                If otableDP.Rows.Count > 0 Then
                    For c1 As Int16 = 0 To otableDP.Rows.Count - 1
                        If otableDP.Rows(c1).Item("acctgoid") = ed("PAYacctgoid").ToString Then
                            ed("trndpapoid") = 1
                        End If
                    Next
                End If
                ed.EndEdit()
            Next
            Session("invCurrOid") = dtlTable.Rows(0).Item("invCurrOid")
            Session("paymentOid") = dtlTable.Rows(0).Item("trnjualmstoid").ToString
            Payseq.Text = dtlTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            GVDtlPayAP.Visible = True
            Session("tbldtl") = dtlTable
            GVDtlPayAP.DataSource = dtlTable
            GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
        Else
            showMessage("Tidak dapat membuka data detail pembayaran!!!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        dpbalance.Text = amtbelinettodtl.Text
        If lblPOST.Text = "POST" Then
            cashbankacctgoid.SelectedValue = Session("cashbankoid")
        End If
    End Sub

    Private Sub initCbg()
        Dim sCb As String = ""
        If Session("branch_id") <> "10" Then
            sCb = "And gencode='" & Session("branch_id") & "'"
        Else
            ddlcabang.Enabled = True : ddlcabang.CssClass = "inpText"
        End If
        sSql = "Select gencode, gendesc from QL_mstgen Where gengroup='CABANG'" & sCb & ""
        FillDDL(ddlcabang, sSql)

        sSql = "Select gencode, gendesc From QL_mstgen Where gengroup = 'CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fCabang, sSql)
            Else
                FillDDL(fCabang, sSql)
                fCabang.Items.Add(New ListItem("ALL", "ALL"))
                fCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(fCabang, sSql)
            fCabang.Items.Add(New ListItem("ALL", "ALL"))
            fCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub PrintReport(ByVal no As String, ByVal type As String)
        'untuk print
        If type = "BBM" Then
            report.Load(Server.MapPath(folderReport & "printBBM.rpt"))
        ElseIf type = "BKM" Then
            report.Load(Server.MapPath(folderReport & "printBKM.rpt"))
        ElseIf type = "BGM" Then
            report.Load(Server.MapPath(folderReport & "printBGM.rpt"))
        ElseIf type = "BDM" Then
            report.Load(Server.MapPath(folderReport & "printBDM.rpt"))
        End If

        report.SetParameterValue("cmpcode", cmpcode)
        report.SetParameterValue("no", no)
        report.SetParameterValue("companyname", CompnyName)

        Dim crConnInfo As New ConnectionInfo()
        With crConnInfo
            .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
            .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
            .IntegratedSecurity = True
        End With
        SetDBLogonForReport(crConnInfo, report)
        'report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no)
        report.Close() : report.Dispose() : Session("no") = Nothing
    End Sub

#End Region

#Region "Functions"
    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = GVmstPAYAP.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Public Function InvoiceRate(ByVal curroid As Integer) As Double
        Dim curRate As Double
        If Session("paymentOid") > 0 Then
            sSql = "select isnull((case when c.currencyoid=1 then rate2usdvalue else rate2idrvalue end),0) rateValue  from ql_trnjualmst a " & _
                   "INNER JOIN ql_trnordermst b on a.orderno = b.orderno left join ql_mstrate2 c on b.rate2oid = c.rate2oid " & _
                   "where a.trnjualmstoid=" & Session("paymentOid") & ""
            objCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            curRate = objCmd.ExecuteScalar
            Return curRate
            conn.Close()
        Else
            curRate = DailyRate()
            Return curRate
        End If
    End Function

    Public Function DailyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rate2idrvalue from ql_mstrate2 where currencyoid=2 and rate2month = MONTH(GETDATE()) and rate2year = year(getdate()) order by rate2oid desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = objCmd.ExecuteScalar
        Return curRate
        conn.Close()

    End Function

    Function WeeklyRate() As Single
        Dim curRate As Single
        sSql = "select top 1 rateidrvalue from ql_mstrate where currencyoid=2 and  convert(varchar,getdate(),101) between  convert(varchar,ratedate,101) and convert(varchar,ratetodate,101) order by rateoid desc"
        objCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        curRate = ToMaskEdit(objCmd.ExecuteScalar, 3)
        Return curRate
        conn.Close()

    End Function

    Function get_dpOid(ByVal trndparno As String, ByVal custoid As Integer) As Integer

        Dim dpoid As Integer

        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "select trdparoid from QL_trndpar where trndparno = '" & trndparno & "' and branch_code='" & ddlcabang.SelectedValue & "' and custoid=" & custoid & ""
        cmd.CommandType = CommandType.Text

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        While rdr.Read
            dpoid = rdr("trdparoid")
        End While
        Return dpoid
        conn2.Close()

    End Function

    Private Function SetTabelDetailSlisih() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("selisihseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("amtdtlselisih", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("dtlnoteselisih", Type.GetType("System.String"))
        Return nuDT
    End Function

    Private Function SetTabelDetail() As DataTable
        Dim nuDT As New DataTable
        nuDT.Columns.Add("paymentoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("cashbankoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payreftype", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("payacctgoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("paynote", Type.GetType("System.String"))
        nuDT.Columns.Add("payamt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("trnbelino", Type.GetType("System.String"))
        nuDT.Columns.Add("suppname", Type.GetType("System.String"))
        nuDT.Columns.Add("amttrans", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtpaid", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("amtretur", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("acctgdesc", Type.GetType("System.String"))
        nuDT.Columns.Add("trntaxpct", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("payseq", Type.GetType("System.Int32"))
        nuDT.Columns.Add("invCurrOid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("invCurrRate", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("invCurrCode", Type.GetType("System.String"))
        nuDT.Columns.Add("invCurrDesc", Type.GetType("System.String"))
        nuDT.Columns.Add("invPayment", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("payduedate", Type.GetType("System.String"))
        nuDT.Columns.Add("payrefno", Type.GetType("System.String"))
        nuDT.Columns.Add("trndpapoid", Type.GetType("System.Int32"))
        nuDT.Columns.Add("DPAmt", Type.GetType("System.Decimal"))
        nuDT.Columns.Add("flagdtl", Type.GetType("System.String"))
        nuDT.Columns.Add("payres1", Type.GetType("System.String"))
        nuDT.Columns.Add("paytype", Type.GetType("System.String"))
        Return nuDT
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Response.Redirect("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim sqlSearch As String = Session("SearchAP")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("SearchAP") = sqlSearch
            Response.Redirect(Page.AppRelativeVirtualPath) '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Pay AR"
        Session("oid") = Request.QueryString("oid")
        Session("branch") = Request.QueryString("branch_code")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk HAPUS data ini ?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Anda yakin untuk POST data ini ?');")
        I_U.Text = "New Data"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        '=============================================================
        If Not IsPostBack Then
            initDDLcashbank(payflag.SelectedValue, "")
            ddlcabang_SelectedIndexChanged(Nothing, Nothing)
            InitAllDDL() : initCbg() : BankDdl()
            payflag.SelectedIndex = 0
            divGiro.Visible = False
            'binddata("")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                I_U.Text = "Update Data"
                PaymentDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 1
                trndpapoid.Enabled = False : payflag.CssClass = "inpTextDisabled"
                btnSearchSupp.Visible = False : PaymentDate.Enabled = False
                PaymentDate.CssClass = "inpTextDisabled" : btnPayDate.Visible = False
                'Fill_payflag()
                FillTextBox(Session("oid"), Session("branch"))
                If lblPOST.Text = "POST" Then
                    btnPosting2.Visible = False
                    btnSave.Visible = False : btnDelete.Visible = False
                    ibtn.Visible = False : payflag.Enabled = False
                Else
                    payflag.CssClass = "inpTextDisabled"
                    payflag.Enabled = False : btnPosting2.Visible = True
                    btnSave.Visible = True : btnDelete.Visible = True : ibtn.Visible = True
                End If
            Else
                I_U.Text = "New Data"
                Session("mstoid") = GenerateID("QL_trnpayar", cmpcode)
                txtPaymentNo.Text = Session("mstoid")
                generateCashBankID()
                btnDelete.Visible = False : btnPrint.Visible = False : lblPOST.Text = ""
                Payseq.Text = 1 : Session("ItemLinePayment") = Payseq.Text
                PaymentDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                payduedate.Text = Format(GetServerTime, "dd/MM/yyyy")
                updUser.Text = Session("UserID") : updTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
                Fill_payflag() : payflag_SelectedIndexChanged(Nothing, Nothing)
                btnSearchSupp.Visible = True : trnbelimstoid.Visible = False
            End If
            txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            'binddata("")

            Dim objTable As DataTable : objTable = Session("tbldtl")
            GVDtlPayAP.DataSource = objTable : GVDtlPayAP.DataBind()
            calcTotalInGridDtl()
            Dim objTable2 As DataTable : objTable2 = Session("DtlSelisih")
            gvDtlSelisih.DataSource = objTable2 : gvDtlSelisih.DataBind()
        End If

        If lblPOST.Text = "POST" Then
            btnshowCOA.Visible = True : btnPrint.Visible = True
        Else
            btnPrint.Visible = False : btnshowCOA.Visible = False
        End If
        GVmstPAYAP.Columns(8).Visible = True
    End Sub

    Protected Sub initddlDP(ByVal suppoid As String)
        FillDDL(ddlDPNo, "SELECT trndparoid, trndparno FROM QL_trndpar WHERE cmpcode='" & cmpcode & "' AND custoid=" & suppoid & " AND trndparflag='OPEN'")
    End Sub

    Protected Sub gridCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If (e.CommandName = "ConfirmPrint") Then
            Response.Redirect("~/reportform/ql_printBK.aspx?paymentoid=" & Trim(e.CommandArgument.ToString()) & "&printtype=8")
        End If
    End Sub

    Protected Sub calcTotalInGrid()
        Dim objTable As DataTable : objTable = Session("tbldata")
        Dim gtotal As Double = 0
        For C1 As Integer = 0 To objTable.Rows.Count - 1
            gtotal += ToDouble(objTable.Rows(C1)("payamtrp").ToString)
        Next
        lblgrandtotal.Text = ToMaskEdit(gtotal, 3)
    End Sub

    Protected Sub bindDataPurchasing(ByVal cmpcode As String, ByVal noNotaBeli As String, ByVal noFaktur As String)
        If CurrencyOid.SelectedValue = 1 Then
            sSql = "SELECT * From ( " & _
"SELECT Distinct b.trnjualmstoid trnbelimstoid, b.trncustoid trnsuppoid, s.custname suppname, b.trnjualno trnbelino,case b.trnjualref when '-' then '' when '' then '' else b.trnjualref end trnjualref, convert(char(10),b.trnjualdate,103) trnbelidate, convert(char(10),dateadd(day,abs(g.genother1), trnjualdate),103) payduedate, dateadd(day,abs(g.genother1), trnjualdate)jt, b.trnamtjualnettoidr + ISNULL((SELECT pap.amount AS kredit FROM QL_debitnote pap WHERE pap.refoid= B.trnjualmstoid AND pap.reftype='AR' and pap.dnstatus = 'Approved'),0) amttrans, isnull((SELECT abs(sum(kredit)) kredit " & _
" FROM (" & _
"SELECT ISNULL(bm.trnjualno,'') AS notrans ,ISNULL(ap.amtbayaridr,0) AS kredit FROM QL_conar ap INNER JOIN QL_trnjualmst bm ON bm.cmpcode=ap.cmpcode AND bm.trnjualmstoid=ap.refoid INNER JOIN QL_trnpayar pap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid AND ap.trnartype = 'PAYAR' WHERE ap.custoid=b.trncustoid AND ap.trnarstatus='POST' AND (pap.payflag <> 'OTHER' /*OR (pap.payflag='OTHER' AND pap.payamt < 0)*/) AND ap.refoid=B.trnjualmstoid AND ap.payrefno NOT LIKE '%SR/%' AND bm.orderno NOT IN (select orderno from QL_trnordermst where typeSO = 'Konsinyasi')  " & _
            "/* UNION ALL " & _
"SELECT ISNULL(bm.trnjualno,'') AS notrans ,ISNULL(ap.amtbayaridr,0) AS kredit FROM QL_conar ap LEFT OUTER JOIN QL_trnjualmst bm ON bm.cmpcode=ap.cmpcode AND ap.reftype='QL_trnPAYAr' AND bm.trnjualmstoid=ap.refoid INNER JOIN QL_trnpayaR pap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid AND lower(pap.payreftype)='ql_trnjualmst' WHERE ap.custoid=b.trncustoid AND ap.reftype='QL_trnpayar' AND ap.trnarstatus='POST' AND pap.payflag='OTHER' AND pap.payamt < 0 AND ap.refoid=B.trnjualmstoid AND ap.payrefno NOT LIKE '%SR/%'*/" & _
           " UNION ALL " & _
"SELECT '' AS notrans,ABS(ISNULL(pap.amount,0)) AS kredit FROM QL_creditnote pap WHERE pap.refoid= B.trnjualmstoid AND pap.reftype='AR' and pap.cnstatus = 'Approved')as dt),0) amtpaid " & _
",b.amtreturidr amtretur" & _
", (SELECT TOP 1 AP.ACCTGOID FROM ql_conar ap Where b.cmpcode=ap.cmpcode AND ap.reftype='ql_trnjualmst' AND b.trnjualmstoid=ap.refoid) acctgoid " & _
",b.currencyoid currencyoid,b.currencyrate currencyrate,cr.currencycode ,cr.currencydesc,b.trntaxpct, b.trnjualnote, 'PAYAR' AS paytype From ql_trnjualmst b " & _
"INNER JOIN QL_mstcurr cr On cr.currencyoid=b.currencyoid " & _
"INNER JOIN ql_mstcust s On b.trncustoid=s.custoid AND b.cmpcode=s.cmpcode " & _
"INNER JOIN ql_mstgen g On g.genoid=(Case b.trnpaytype " & _
"When 0 then 34 else b.trnpaytype End) AND g.gengroup = 'PAYTYPE' " & _
"INNER JOIN ql_conar c On b.trnjualmstoid = c.refoid AND c.reftype='QL_TRNJUALMST' " & _
"INNER JOIN ql_mstacctg a On a.acctgoid=c.acctgoid " & _
"Where (b.trnamtjualnettoidr-b.amtreturidr) - accumpaymentidr > 0 AND trnjualstatus IN ('Approved','Post') AND b.cmpcode='" & cmpcode & "' AND b.trnjualno NOT LIKE '%R' AND b.trnjualno NOT IN (SELECT v.refnotarevisi From QL_trnjualmst v) AND b.trncustoid= " & trnsuppoid.Text & " and b.branch_code = '" & ddlcabang.SelectedValue & "' AND b.orderno NOT IN (select orderno from QL_trnordermst where typeSO = 'Konsinyasi')" & _
"UNION ALL " & _
"select e.trnbiayaeksoid trnbelimstoid, custoid trnsuppoid, (select c.custname from QL_mstcust c where c.custoid=e.custoid) suppname, trnbiayaeksno trnbelino, trnbiayaeksnote trnjualref,convert(char(10),e.trnbiayaeksdate,103) trnbelidate ,'' payduedate,'' jt, e.amtekspedisi + ISNULL((SELECT pap.amount AS kredit FROM QL_debitnote pap WHERE pap.refoid= e.trnbiayaeksoid AND pap.reftype='NT' and pap.dnstatus = 'Approved'),0) amttrans " & _
", isnull((SELECT abs(sum(kredit))kredit FROM ( " & _
 "SELECT ISNULL(bm.trnbiayaeksno,'') AS notrans,ISNULL(ap.amtbayaridr,0) AS kredit FROM QL_conar ap LEFT OUTER JOIN ql_trnbiayaeksmst bm ON bm.cmpcode=ap.cmpcode AND bm.trnbiayaeksoid=ap.refoid INNER JOIN QL_trnpayaR pap ON pap.cmpcode=ap.cmpcode AND pap.paymentoid=ap.payrefoid AND ap.trnartype ='PAYAREXP' WHERE ap.custoid= e.custoid AND ap.trnarstatus='POST' AND ap.refoid= e.trnbiayaeksoid AND ap.payrefno NOT LIKE '%SR/%' " & _
            " UNION ALL " & _
 "SELECT '' AS notrans,ABS(ISNULL(pap.amount,0)) AS kredit FROM QL_creditnote pap WHERE pap.refoid= e.trnbiayaeksoid AND pap.reftype='NT')as dt),0) amtpaid" & _
 ",0.0 amtretur, e.acctgoid acctgoid,'1' currencyoid,'1' currencyrate,'IDR' currencycode ,'Rupiah' currencydesc,0.0 trntaxpct, e.trnbiayaeksnote trnjualnote, 'PAYAREXP' AS paytype " & _
 "from ql_trnbiayaeksmst e WHERE status = 'POST' and e.trnbiayaeksno like '%%' and cmpcode = '" & cmpcode & "' AND e.custoid= " & trnsuppoid.Text & " and e.branch_code = '" & ddlcabang.SelectedValue & "' AND e.approvalstatus NOT IN ('APPROVED','IN APPROVAL')" & _
") dt Where trnbelino LIKE '%" & Tchar(noNotaBeli.Trim) & "%' /*and trnjualref like '%" & Tchar(noFaktur.Trim) & "%'*/ AND dt.amttrans-amtpaid-amtretur > 0 Order By dt.jt  "
        End If
        FillGV(gvPurchasing, sSql, "ql_trnbelimst")
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, True)
    End Sub

    Protected Sub ClearDtlAP(ByVal bState As Boolean)
        txtPaymentNo.Text = "" : trnbelino.Text = "" : trnbelimstoid.Text = ""
        suppname.Text = ""
        amttrans.Text = "0.00" : amtpaid.Text = "0.00" : APAmt.Text = "0.00"
        APAcc.Text = "" : acctgoid.Text = ""
        amtpayment.Text = "0.00" : totalpayment.Text = "0.00"
        trnTaxPct.Text = "0.00" : TaxAmount.Text = "0.00"
        lblTaxPct.Visible = False : trnTaxPct.Visible = False
        lblAmtTax.Visible = False : TaxAmount.Visible = False
        invCurrOid.Text = "" : invCurrRate.Text = ""
        invCurrCode.Text = "" : invCurrDesc.Text = ""
        amtretur.Text = "0.00"
        chkOther.Checked = False : chkOther.Enabled = True
        otherAcctgoid.Enabled = True : otherAcctgoid.CssClass = "inpTextDisabled"
        otheramt.Text = "" : lkbAddDtlSlisih.Visible = False : lkbClearDtlSlisih.Visible = False
        If bState Then
            ' Reset Tabel Detail Selisih
            Session("DtlSelisih") = Nothing
            gvDtlSelisih.DataSource = Nothing
            gvDtlSelisih.DataBind()
            ClearDtlSelisih()
            txtNote.Text = ""
            GVDtlPayAP.Columns(11).Visible = True
            GVDtlPayAP.SelectedIndex = -1
            I_U2.Text = "New Detail"
        End If
        paytype.Text = ""
    End Sub

    Protected Sub payflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payflag.SelectedIndexChanged
        Fill_payflag()
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPurchasing.Click
        If trnsuppoid.Text.Trim = "" Then
            showMessage("Pilih Customer terlebih dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        If payflag.SelectedValue = "GIRO" And payrefno.Text = "" Then
            showMessage("Pilih Nomor Giro terlebih dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        bindDataPurchasing(cmpcode, txtInputNotaBeli.Text, txtInputNoFaktur.Text)
    End Sub

    Protected Sub calcTotalInGridDtl()
        Dim gtotal As Double = 0
        If Not (Session("tbldtl") Is Nothing) Then
            Dim objTable As DataTable : objTable = Session("tbldtl")
            gtotal = ToDouble(objTable.Compute("SUM(payamt)", "").ToString)
        End If
        amtbelinettodtl.Text = ToMaskEdit(gtotal, 3)
        amtbelinettodtl4.Text = ToMaskEdit(gtotal, 3)
        NetPayment.Text = ToMaskEdit(ToDouble(amtbelinettodtl.Text) - ToDouble(TotalCost.Text), 3)
    End Sub

    Protected Sub ClosePurc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hiddenbtnpur.Visible = False : Panel2.Visible = False : ModalPopupExtender2.Hide()
        gvPurchasing.DataSource = Nothing : gvPurchasing.DataBind()
    End Sub

    Protected Sub GVDtlPayAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVDtlPayAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
        End If
    End Sub

    Protected Sub GVDtlPayAP_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GVDtlPayAP.RowDeleting
        If lblPOST.Text <> "POST" Then
            Dim idx As Integer = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")

            ' CEK apakah induk transaksi (utang) ato bukan
            Dim dvTemp As DataView = objTable.DefaultView
            dvTemp.RowFilter = "payseq=" & idx + 1
            ' Get Id Induk 
            Dim idInvoice As Integer = objTable.Rows(e.RowIndex).Item("payrefoid")
            Dim payreftype As String = objTable.Rows(e.RowIndex).Item("paytype")
            If dvTemp.Count > 0 Then
                If dvTemp(0)("flagdtl") = "OTHER" Then
                    showMessage("Pilih induk transaksi !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
            End If
            dvTemp.RowFilter = ""

            ' DELETE data bila ada
            For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
                If objTable.Rows(C1).Item("payrefoid") = idInvoice And objTable.Rows(C1).Item("paytype").ToString.ToUpper = payreftype.ToUpper Then
                    If payrefno.Text <> "" Then
                        AmountGiro.Text = AmountGiro.Text + objTable.Rows(C1).Item("payamt")
                    End If
                    Session("totalbayar") = Session("totalbayar") - objTable.Rows(C1).Item("payamt")
                    objTable.Rows.RemoveAt(C1)
                End If
            Next

            'resequence Detial 
            For C2 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C2)
                dr.BeginEdit() : dr("payseq") = C2 + 1 : dr.EndEdit()
            Next

            Session("tbldtl") = objTable
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Payseq.Text = objTable.Rows.Count + 1
            Session("ItemLinePayment") = Payseq.Text
            calcTotalInGridDtl()
            If GVDtlPayAP.Rows.Count = 0 Then
                'Enabled ketika addtolist
                payflag.CssClass = "inpText"
                payflag.Enabled = True
            End If
        Else
            e.Cancel = True 'Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Cannot be Deleted!!"))
        End If
    End Sub

    Protected Sub GVDtlPayAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDtlPayAP.SelectedIndexChanged
        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dvTemp As DataView = objTable.DefaultView
        Payseq.Text = GVDtlPayAP.SelectedDataKey.Item("payseq").ToString
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        If dvTemp.Count > 0 Then
            If dvTemp(0)("flagdtl") = "OTHER" Then
                showMessage("Pilih induk transaksi", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                dvTemp.RowFilter = "" : Exit Sub
            End If
        End If
        dvTemp.RowFilter = ""
        ' Get Table Detail Selisih
        gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind()
        otheramt.Text = "0.00"
        dvTemp.RowFilter = "payrefoid=" & trnbelimstoid.Text & " AND flagdtl='OTHER'"
        Dim dTotSelisih As Double = 0
        If dvTemp.Count > 0 Then
            Dim tbSelisih As DataTable = SetTabelDetailSlisih()
            For C4 As Integer = 0 To dvTemp.Count - 1
                Dim nuRow As DataRow : nuRow = tbSelisih.NewRow
                nuRow("selisihseq") = C4 + 1
                nuRow("acctgoid") = dvTemp(C4)("payacctgoid").ToString
                nuRow("acctgdesc") = dvTemp(C4)("acctgdesc").ToString
                nuRow("amtdtlselisih") = Math.Abs(ToDouble(dvTemp(C4)("payamt").ToString))
                nuRow("dtlnoteselisih") = dvTemp(C4)("paynote").ToString
                dTotSelisih += ToDouble(dvTemp(C4)("payamt").ToString)
                tbSelisih.Rows.Add(nuRow)
            Next

            chkOther.Checked = True
            ' Cek kurang bayar atau lebih bayar
            If dTotSelisih < 0 Then ' lebih bayar
                DDLOtherType.SelectedValue = "-"
                chkOther.Enabled = True
                SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
            Else ' kurang bayar
                DDLOtherType.SelectedValue = "+"
                chkOther.Enabled = False
                'chkOther_CheckedChanged(Nothing, Nothing)
                SetOtherAccount(chkOther.Checked)
                InitOtherAcctg(DDLOtherType.SelectedValue)
            End If
            otheramt.Text = ToMaskEdit(Math.Abs(ToDouble(dTotSelisih)), 3)

            Session("DtlSelisih") = tbSelisih
            gvDtlSelisih.DataSource = tbSelisih
            gvDtlSelisih.DataBind()
        End If
        dvTemp.RowFilter = ""
        ' Detail Payap
        dvTemp.RowFilter = "payseq=" & Payseq.Text
        paytype.Text = dvTemp(0)("paytype").ToString
        trnbelimstoid.Text = dvTemp(0)("payrefoid").ToString
        trnbelino.Text = dvTemp(0)("trnbelino").ToString
        suppname.Text = dvTemp(0)("suppname").ToString
        acctgoid.Text = dvTemp(0)("payacctgoid").ToString
        APAcc.Text = dvTemp(0)("acctgdesc").ToString
        invCurrOid.Text = dvTemp(0)("invCurrOid").ToString
        invCurrRate.Text = ToMaskEdit(ToDouble(dvTemp(0)("invCurrRate").ToString), 3)
        invCurrCode.Text = dvTemp(0)("invCurrCode").ToString
        invCurrDesc.Text = dvTemp(0)("invCurrDesc").ToString
        amttrans.Text = ToMaskEdit(ToDouble(dvTemp(0)("amttrans").ToString), 3)
        amtpaid.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtpaid").ToString), 3)
        amtretur.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtretur").ToString), 3)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 3)
        AmountGiro.Text = ToMaskEdit(ToDouble(AmountGiro.Text) + ToDouble(dvTemp(0)("payamt").ToString), 3)
        Session("totalbayar") -= ToDouble(dvTemp(0)("payamt").ToString)
        If chkOther.Checked Then
            If DDLOtherType.SelectedValue = "+" Then ' Lebih Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 3)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) + Math.Abs(dTotSelisih), 3)
            Else ' Kurang Bayar
                totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 3)
                amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString) - Math.Abs(dTotSelisih), 3)
            End If
        Else ' Tanpa Selisih
            totalpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 3)
            amtpayment.Text = ToMaskEdit(ToDouble(dvTemp(0)("payamt").ToString), 3)
        End If

        If amtpayment.Text = APAmt.Text Then
            chkOther.Enabled = False
        End If

        trnTaxPct.Text = ToMaskEdit(ToDouble(dvTemp(0)("trntaxpct").ToString), 3)
        txtNote.Text = dvTemp(0)("paynote").ToString
        ' payduedate.Text = CDate(dvTemp(0)("payduedate").ToString)
        payrefno.Text = dvTemp(0)("payrefno").ToString
        If CInt(dvTemp(0)("trndpapoid")) > 0 Then
            ddlDPNo.Visible = True : cbDP.Checked = True
            DPAmt.Visible = True : lblDPAmount.Visible = True
            ddlDPNo.SelectedValue = dvTemp(0)("trndpapoid").ToString
        Else
            ddlDPNo.Visible = False : cbDP.Checked = False
            DPAmt.Visible = False : lblDPAmount.Visible = False
        End If
        DPAmt.Text = ToMaskEdit(dvTemp(0)("DPAmt").ToString, 3)
        dvTemp.RowFilter = ""
        ' CalculateSelisih()
        GVDtlPayAP.Columns(11).Visible = False
        I_U2.Text = "Update Data"
        'MultiView1.ActiveViewIndex = 1
        CalculateTotalPayment()
        If dTotSelisih <> 0 Then ' lebih bayar
            chkOther.Checked = True
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        txtPeriode1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
        postinge.SelectedIndex = 0 : ddlFilter.SelectedIndex = 0
        orderby.SelectedIndex = 0 : FilterText.Text = ""
        cbPeriode.Checked = False
        BindData("")
    End Sub

    Protected Sub cashbankacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
    End Sub

    Protected Sub btnClearPurchase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True)
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvPurchasing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchasing.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 2)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
        End If
    End Sub

    Protected Sub currencyRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ToDouble(currencyRate.Text) <= 0 Then
            FillCurrencyRate(CurrencyOid.SelectedValue)
        End If
        ClearDtlAP(True) : Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing : GVDtlPayAP.DataBind()
    End Sub

    Protected Sub LBPost_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        btnPosting2_Click(sender, e)
    End Sub

    Protected Sub ibtnacctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub CBTax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initDDLcashbank(payflag.SelectedValue, "")
    End Sub

    Protected Sub GVmstPAYAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstPAYAP.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            If e.Row.Cells(7).Text.Trim = "&nbsp;" Then
                e.Row.Cells(7).Text = "In Process"
            End If

            'Dim HyperLink1 As HyperLink = CType(e.Row.Cells(0).FindControl("HyperLink1"), HyperLink)
            'Dim objGrid As GridView = CType(e.Row.Cells(5).FindControl("gvsubmst"), GridView)
            ''sSql = "select distinct b.trnjualno from ql_trnpayar p INNER JOIN ql_trnjualmst b on b.trnjualmstoid=p.payrefoid  where p.cashbankoid in (select cashbankoid from QL_trncashbankmst where cashbankno='" & HyperLink1.Text & "') "
            'sSql = "select bm.trnjualno FROM ql_trnpayar pap INNER JOIN ql_conar c ON c.cmpcode=pap.cmpcode AND c.payrefoid=pap.paymentoid  and c.branch_code=pap.branch_code and c.custoid = pap.custoid and c.trnartype = 'PAYAR' INNER JOIN ql_trnjualmst bm ON bm.cmpcode=pap.cmpcode AND c.refoid=bm.trnjualmstoid  and pap.branch_code=bm.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=pap.cmpcode AND cb.cashbankoid=pap.cashbankoid INNER JOIN ql_mstcust s ON bm.cmpcode=s.cmpcode AND bm.trncustoid=s.custoid where pap.cashbankoid in (select cashbankoid from QL_trncashbankmst where cashbankno='" & HyperLink1.Text & "') " & _
            '"UNION ALL " & _
            '"select e.trnbiayaeksno trnjualno FROM ql_trnpayar p INNER JOIN ql_conar ap ON p.cmpcode=ap.cmpcode AND p.paymentoid=ap.payrefoid  and p.branch_code=ap.branch_code and p.custoid = ap.custoid and ap.trnartype = 'PAYAREXP' INNER JOIN ql_trnbiayaeksmst e ON e.cmpcode=p.cmpcode AND ap.refoid=e.trnbiayaeksoid   and p.branch_code=ap.branch_code INNER JOIN ql_trncashbankmst cb ON cb.cmpcode=p.cmpcode AND cb.cashbankoid=p.cashbankoid INNER JOIN ql_mstcust s ON e.cmpcode=s.cmpcode AND e.custoid=s.custoid and p.cashbankoid in (select cashbankoid from QL_trncashbankmst where cashbankno='" & HyperLink1.Text & "')"
            'sSql = " SELECT '' trnjualno"
            'sSql = "select DISTINCT trnjualno = isnull(STUFF((SELECT ',' + (select trnjualno from ql_trnjualmst where trnjualmstoid = jm.payrefoid) FROM QL_trnpayar jm where jm.cashbankoid=p.cashbankoid AND payflag <> 'OTHER' FOR XML PATH ('')), 1, 1, ''),'') from QL_trnpayar p INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid = p.cashbankoid where cb.cashbankno = '" & HyperLink1.Text & "' " & _
            '"UNION ALL " & _
            '"select DISTINCT trnjualno = isnull(STUFF((SELECT ',' + (select trnbiayaeksno from QL_trnbiayaeksmst where trnbiayaeksoid = jm.payrefoid) FROM QL_trnpayar jm where jm.cashbankoid=p.cashbankoid AND payflag <> 'OTHER' FOR XML PATH ('')), 1, 1, ''),'') from QL_trnpayar p INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid = p.cashbankoid where cb.cashbankno = '" & HyperLink1.Text & "'"
            'Dim ods As New SqlDataAdapter(sSql, conn2)
            'Dim objTablee As New DataTable
            'ods.Fill(objTablee)
            'objGrid.DataSource = objTablee.DefaultView
            'objGrid.DataBind()
        End If
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UncheckAll()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False : beMsgBox.Visible = False : mpeMsgbox.Hide()
        If lblState.Text = "INV" Then
            lblState.Text = "" : ModalPopupExtender2.Show()
        End If
    End Sub

    Protected Sub imbFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataPurchasing(cmpcode, txtInputNotaBeli.Text, txtInputNoFaktur.Text)
    End Sub

    Protected Sub imbViewAllInv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAllInv.Click
        bindDataPurchasing(cmpcode, "", "")
        txtInputNotaBeli.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtlAP(True) : ClearDtlSelisih()
        Payseq.Text = Session("ItemLinePayment")
    End Sub

    Protected Sub lkbInfo1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbInfo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lkbDetail2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim strSQL As String
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            strSQL = "Delete From QL_conar Where cmpcode='" & cmpcode & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"

            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "Delete from QL_trnpayar where cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            strSQL = "DELETE FROM QL_trncashbankmst " & _
                "WHERE cmpcode='" & cmpcode & "' and cashbankoid = " & Trim(cashbankoid.Text)
            objCmd.CommandText = strSQL : objCmd.ExecuteNonQuery()

            Dim dRate2 As Double
            dRate2 = InvoiceRate(Session("invCurrOid"))
            Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
            If CurrencyOid.SelectedValue = 1 Then
                dparacumamt = ToDouble(amtbelinettodtl.Text)
                dparacumamtidr = ToDouble(amtbelinettodtl.Text)
                If Session("invCurrOid") = 1 Then
                    dparacumamtusd = ToDouble(amtbelinettodtl.Text) * dRate2
                Else

                    dparacumamtusd = ToDouble(amtbelinettodtl.Text) / dRate2
                End If
            Else
                dparacumamt = ToDouble(amtbelinettodtl.Text)
                dparacumamtusd = ToDouble(amtbelinettodtl.Text)
                If Session("invCurrOid") = 1 Then
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text) / dRate2
                Else
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text) * dRate2
                End If
            End If
            ' UPDATE QL_trndpap bila pake DP status CLose
            If payflag.SelectedValue = "DP" Then
                sSql = "UPDATE QL_trndpar SET trndparflag=CASE WHEN  (ISNULL(trndparacumamt,0) - " & ToDouble(amtbelinettodtl.Text) & ")>=ISNULL(trndparamt,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' AND trndparoid='" & trndpapoid.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.Message, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        Session("oid") = Nothing : Session("tbldtl") = Nothing
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting2.Click
        lblPOST.Text = "POST" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Try
            Dim sMsg As String = ""
            'If CDate(toDate(PaymentDate.Text)) >= CDate(toDate(CutofDate.Text)) Then
            '    sMsg &= "- Tanggal asset Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> "
            'End If
            dRate = InvoiceRate(Session("invCurrOid"))
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag='OPEN'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctgHJ(CDate(toDate(PaymentDate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            'End If
            '=================================================================================
            If cashbankacctgoid.SelectedValue = "" Then : sMsg &= "- Please Fill COA Cash/Bank/Giro !!<BR>" : End If
            Dim st2 As Boolean
            If ToDouble(trnsuppoid.Text) = 0 Then
                sMsg &= "- Silahkan pilih customer!!<BR>"
            End If
            If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                payduedate.Text = PaymentDate.Text
            ElseIf payduedate.Text.Trim = "" Then
                sMsg &= "- Silahkan isi Tanggal jatuh tempo bayar !!<BR>"
            End If

            If payflag.SelectedValue = "DP" Then
                If trndpapoid.Items.Count < 1 Then
                    sMsg &= "- Tidak ada DP di customer ini !!<BR>"
                Else
                    If ToDouble(amtbelinettodtl.Text) > ToDouble(dpbalance.Text) Then
                        sMsg &= "- Total bayar harus <= total DP !!<BR>"
                    End If
                End If
            End If

            'Cek Giro yang sisa
            If payflag.SelectedValue = "GIRO" Then
                If ToDouble(AmountGiro.Text) <> 0 Then
                    sMsg &= "- Nilai Giro harus sama dengan nilai piutang dan tidak boleh sisa !!<BR>"
                End If
            End If
            Try
                Dim dt As Date = CDate(toDate(PaymentDate.Text)) : st2 = True
            Catch ex As Exception
                sMsg &= "- Tanggal bayar salah !!<BR>" : st2 = False
            End Try
            If payduedate.Visible Then
                If CDate(toDate(payduedate.Text)) < CDate(toDate(PaymentDate.Text)) Then
                    sMsg &= "- Tanggal jatuh tempo harus >= Tanggal bayar !!<BR>"
                End If
            End If
            If ddlDPNo.Items.Count > 0 Then
                Dim tempDP As Double = ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue()))
                If ToDouble(DPAmt.Text) > tempDP Then
                    sMsg &= "- Jumlah DP tidak boleh > actual DP !!<BR>"
                End If
            End If

            ' cek apakah Payment Detail sudah ada atau belum
            If Session("tbldtl") Is Nothing Then
                sMsg &= "- Tidak ada detail bayar !!<BR>"
                GVDtlPayAP.DataSource = Session("tbldtl")
                GVDtlPayAP.DataBind()
            Else
                Dim objTableCek As DataTable : objTableCek = Session("tbldtl")
                If objTableCek.Rows.Count <= 0 Then
                    sMsg &= "- Tidak ada detail bayar !!<BR>"
                    GVDtlPayAP.DataSource = Session("tbldtl")
                    GVDtlPayAP.DataBind()
                Else             'cek tgl ivoice pd detail apakah ada yg kurang dari tgl bayar
                    If dRate = 0 Then
                        'showMessage("Rate Invoice tidak ditemukan", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                        'Exit Sub
                    End If

                    For c1 As Int16 = 0 To objTableCek.Rows.Count - 1
                        If objTableCek.Rows(c1).Item("flagdtl") = "" Then
                            sSql = "select Convert(Char(10),trnjualdate,102) from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid") & " And branch_code='" & ddlcabang.SelectedValue & "'"
                            If (conn.State = ConnectionState.Closed) Then
                                conn.Open()
                            End If
                            objCmd.CommandText = sSql
                            Dim tgle As Date = objCmd.ExecuteScalar
                            Dim PayDate As Date = CDate(toDate(PaymentDate.Text))
                            conn.Close()

                            If tgle > PayDate Then
                                sMsg &= "- Maaf, tanggal nota jual tidak boleh > tanggal pembayaran (inv.date=" & objTableCek.Rows(c1).Item("trnbelino") & " dan Tanggal bayar= " & Format(tgle, "dd/MM/yyyy") & ")!!<BR>"
                                Exit For
                            End If
                        End If

                        'cek data synchronisasi dengan payment
                        If objTableCek.Rows(c1).Item("paytype") = "PAYAR" Then
                            If CurrencyOid.SelectedValue = 1 Then
                                sSql = "select accumpaymentidr from ql_trnjualmst where cmpcode='" & cmpcode & "' And trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid") & " And branch_code='" & ddlcabang.SelectedValue & "'"
                                If ToDouble(GetStrData(sSql)) < 0 Then
                                    sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                                    Exit For
                                End If
                            Else
                                sSql = "select accumpaymentusd from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid") & " And branch_code='" & ddlcabang.SelectedValue & "'"
                                If ToDouble(GetStrData(sSql)) < 0 Then
                                    sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                                    Exit For
                                End If
                            End If

                            'cek data synchronisasi dengan amt retur payment
                            If CurrencyOid.SelectedValue = 1 Then
                                sSql = "select amtreturidr from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid") & " And branch_code='" & ddlcabang.SelectedValue & "'"
                                If objTableCek.Rows(c1).Item("amtretur") <> Math.Round(ToDouble(GetStrData(sSql)), 1) Then
                                    sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                                    Exit For
                                End If
                            Else
                                sSql = "select amtreturusd from ql_trnjualmst where cmpcode='" & cmpcode & "' and trnjualmstoid=" & objTableCek.Rows(c1).Item("payrefoid") & " And branch_code='" & ddlcabang.SelectedValue & "')"
                                If objTableCek.Rows(c1).Item("amtretur") <> Math.Round(ToDouble(GetStrData(sSql)), 1) Then
                                    sMsg &= "- Nota beli (" & objTableCek.Rows(c1).Item("trnbelino") & ") tidak valid !!, Silahkan refresh atau pilih ulang untuk nota jual ini !! <BR>"
                                    Exit For
                                End If
                            End If
                        End If

                        If Session("oid") = Nothing Or Session("oid") = "" Then
                            'Cek apakah ada invoice yg sudah dibuatkan payment tp blm diposting
                            If GetStrData("select count(-1) from QL_trnpayar where payrefoid=" & objTableCek.Rows(c1).Item("payrefoid") & " and upper(payreftype)='QL_TRNJUALMST' and upper(paystatus)<>'POST' And branch_code='" & ddlcabang.SelectedValue & "'") > 0 Then
                                sMsg &= "- Ada transaksi pembayaran/Retur lain untuk nota jual ini (" & objTableCek.Rows(c1).Item("trnbelino") & ") <BR> Silahkan tekan batal dan cek di transaksi pembayaran yang lain kemudian tekan posting untuk nota  (" & objTableCek.Rows(c1).Item("trnbelino") & ") terlebih dahulu !! <BR>"
                                Exit For
                            End If
                        Else
                            'Cek apakah ada invoice yg sudah dibuatkan payment tp blm diposting
                            If GetStrData("select count(-1) from QL_trnpayar where payrefoid=" & objTableCek.Rows(c1).Item("payrefoid") & " and upper(payreftype)='QL_TRNJUALMST' and cashbankoid<>" & Session("oid") & " and upper(paystatus)<>'POST' And branch_code='" & ddlcabang.SelectedValue & "'") > 0 Then
                                sMsg &= "- Ada transaksi pembayaran/Retur lain untuk nota jual ini (" & objTableCek.Rows(c1).Item("trnbelino") & ") <BR> Silahkan tekan batal dan cek di transaksi pembayaran yang lain kemudian tekan posting untuk nota  (" & objTableCek.Rows(c1).Item("trnbelino") & ") terlebih dahulu !! <BR>"
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If

            If defcbno.Text.Trim = "" Then
                sMsg &= " - Isi no pembayaran piutang dahulu !!<BR>"
            End If

            If cashbanknote.Text.Trim.Length > 200 Then
                sMsg &= "- Maksimal Note Header adalah 200 karakter !!<BR>"
            End If
            If sMsg <> "" Then
                lblPOST.Text = "In Process"
                showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End If
        Catch ex As Exception
            lblPOST.Text = "In Process"
            showMessage(ex.ToString, CompnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try

        'If Session("oid") = Nothing Or Session("oid") = "" Then
        GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))

        'End If
        If Session("oid") = Nothing Or Session("oid") = "" Then
            cashbankoid.Text = GenerateID("QL_trncashbankmst", cmpcode)
        Else
            cashbankoid.Text = Session("oid")
            cashbankno.Text = Session("oid")
        End If
        Session("vJurnalDtl") = GenerateID("ql_trngldtl", cmpcode)
        Session("vJurnalMst") = GenerateID("ql_trnglmst", cmpcode)
        Dim iDPapoid = GenerateID("ql_trndpar", cmpcode)
        Dim sNoDPAP As String = ""
        If payflag.SelectedValue = "CASH" Then
            sNoDPAP = "T"
        ElseIf payflag.SelectedValue = "BANK" Then
            sNoDPAP = "B"
        ElseIf payflag.SelectedValue = "GIRO" Then
            sNoDPAP = "G"
        ElseIf payflag.SelectedValue = "DP" Then
            sNoDPAP = "D"
        ElseIf payflag.SelectedValue = "CREDIT CARD" Then
            sNoDPAP = "C"
        End If

        sNoDPAP = "DP.AR." & sNoDPAP & "-" & Format(CDate(toDate(PaymentDate.Text.Trim)), "yy/MM/")
        sSql = "SELECT  isnull(max(abs(replace(trndparno,'" & sNoDPAP & "',''))),0)+1   AS IDNEW FROM ql_trndpar WHERE cmpcode='" & cmpcode & "' AND trndparno LIKE '" & sNoDPAP & "%'"
        Dim IcounterDPAPno As Int32 = GetStrData(sSql)

        Session("vDtlSeq") = 1
        Dim iDPOid As Integer = 0
        If payflag.SelectedValue = "DP" Then
            iDPOid = trndpapoid.SelectedValue
        End If
        currencyRate.Text = "1.00"
        Dim itrnbelimstoid As Int32 = 0

        Dim sCBType As String = ""
        Dim iCurID As Integer = 0 : Dim sCBCode As String = ""

        If payflag.SelectedValue = "CASH" Then
            sCBType = "BKM"
        ElseIf payflag.SelectedValue = "BANK" Then
            sCBType = "BBM"
        ElseIf payflag.SelectedValue = "GIRO" Then
            sCBType = "BGM"
        ElseIf payflag.SelectedValue = "CREDIT CARD" Then
            sCBType = "BCM"
        ElseIf payflag.SelectedValue = "DP" Then
            sCBType = "BDM"
            payrefno.Text = trndpapoid.SelectedItem.Text
        Else
            sCBType = "BLM"
        End If
        'Dim sCashBank As Integer = GenerateID("QL_trncashbankmst", cmpcode)
        'cashbankoid.Text = sCashBank
        Dim Cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & ddlcabang.SelectedValue & "' AND gengroup='CABANG'")
        If lblPOST.Text = "POST" Then
            Dim sNo As String = sCBType & "/" & Cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT isnull(max(abs(replace(cashbankno,'" & sNo & "',''))),0)+1 FROM QL_trncashbankmst WHERE cashbankno LIKE '" & sNo & "%' and branch_code='" & ddlcabang.SelectedValue & "'"
            cashbankno.Text = GenNumberString(sNo, "", cKoneksi.ambilscalar(sSql), 4)
        End If
        Dim sTemp As Integer = GenerateID("QL_trnpayar", cmpcode)
        Session("conaroid") = GenerateID("ql_conar", cmpcode)
        Dim cbnote As String = ""
        If payflag.SelectedValue = "GIRO" Then
            cbnote = "GIRO =>" & dd_bankgiro.SelectedItem.Text & " " & cashbanknote.Text
        Else
            cbnote = cashbanknote.Text
        End If
        If girooid.Text = "" Then
            girooid.Text = 0
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_trncashbankmst (cmpcode, cashbankoid, cashbankno, cashbankstatus,cashbanktype,cashbankgroup,cashbankacctgoid,cashbankdate,cashbanknote, createuser,createtime,upduser,updtime,cashbankcurroid,cashbankcurrate, pic, pic_refname, branch_code, giroref, cashbankrefno, bankoid, girodtloid, cashbankamount, cashbankamountidr) " & _
                   " VALUES ('" & cmpcode & "'," & cashbankoid.Text & ",'" & Tchar(cashbankno.Text) & "','" & lblPOST.Text & "','" & sCBType & "','AR'," & cashbankacctgoid.SelectedValue & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & Tchar(cbnote) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp, " & CurrencyOid.SelectedValue & "," & ToDouble(1) & ", " & trnsuppoid.Text & ", 'QL_MSTCUST','" & ddlcabang.SelectedValue & "'," & girooid.Text & ",'" & Tchar(payrefno.Text.Trim) & "','" & dd_bankgiro.SelectedValue & "'," & girooid.Text & ", " & Session("totalbayar") & ", " & Session("totalbayar") & ")"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                Dim dRate2 As Double : dRate2 = InvoiceRate(Session("invCurrOid"))
                Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
                If CurrencyOid.SelectedValue = 1 Then
                    dparacumamt = ToDouble(amtbelinettodtl.Text)
                    dparacumamtidr = ToDouble(amtbelinettodtl.Text)
                Else
                    dparacumamt = ToDouble(amtbelinettodtl.Text)
                    dparacumamtusd = ToDouble(amtbelinettodtl.Text)
                End If

                ' UPDATE QL_trndpap bila pake DP
                If iDPOid <> 0 And lblPOST.Text = "POST" Then
                    sSql = "UPDATE QL_trndpar SET trndparacumamt=ISNULL(trndparacumamt,0)+" & ToDouble(dparacumamtidr) & " ,trndparacumamtidr=ISNULL(trndparacumamtidr,0)+" & ToDouble(dparacumamtidr) & ",trndparacumamtusd=ISNULL(trndparacumamtusd,0)+" & ToDouble(dparacumamtidr) & ",trndparflag=CASE WHEN (ISNULL(trndparacumamtidr,0)+" & ToDouble(dparacumamtidr) & ")>=ISNULL(trndparamtidr,0) THEN 'CLOSE' ELSE 'OPEN' END WHERE cmpcode='" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "' AND trndparoid='" & iDPOid & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                'Update lastoid dari QL_mstoid table QL_trncashbankmst
                sSql = "update QL_mstoid set lastoid=" & cashbankoid.Text & " where tablename like 'QL_trncashbank%' and cmpcode like '%" & cmpcode & "%'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                '===========================================
                'Start save invoice detail(banyak detail saja)
                '============================================
                Dim totalPayment As Decimal = 0
                Dim totalPaymentIDR As Decimal = 0 : Dim totalPaymentUSD As Decimal = 0
                Dim vPayam, vPayamidr, vPayamusd As Double
                Dim vdpam, vdpamidr, vdpamusd As Double
                If Not Session("tbldtl") Is Nothing Then
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    For C1 As Int16 = 0 To objRow.Length - 1
                        If lblPOST.Text = "POST" Then
                            If itrnbelimstoid <> objRow(C1)("payrefoid") And objRow(C1)("payamt") > 0 Then
                                'update ke acumamt ql_trnbeli sby tanda pembayaran
                                sSql = "Update ql_trnjualmst set accumpayment=accumpayment+" & ToDouble(objRow(C1)("payamt")) & ", accumpaymentidr=accumpaymentidr+" & ToDouble(objRow(C1)("payamt")) & ",accumpaymentusd=accumpaymentusd+" & ToDouble(objRow(C1)("payamt")) & ",lastpaymentdate='" & CDate(toDate(PaymentDate.Text)) & "' Where cmpcode='" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "' and trnjualmstoid=" & objRow(C1)("payrefoid")
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                itrnbelimstoid = objRow(C1)("payrefoid")
                            ElseIf objRow(C1)("payamt") > 0 And objRow(C1)("trndpapoid") = 1 Then 'insert to trndp
                                sSql = "INSERT INTO QL_trndpar (cmpcode,trndparoid,trndparno,trndpardate,custoid,cashbankoid, trndparacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate, trndparamt,trndparamtidr,trndparamtusd,taxtype,taxoid,taxpct,taxamt,trndparnote,trndparflag,trndparacumamt,trndparstatus, createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                    "('" & cmpcode & "'," & iDPapoid & ",'" & GenNumberString(sNoDPAP, "", IcounterDPAPno, 3) & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & trnsuppoid.Text & "," & cashbankoid.Text & "," & objRow(C1)("payacctgoid") & ",'" & sCBType & "'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(cashbankno.Text) & "'," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & "," & ToDouble(objRow(C1)("payamt")) & "," & ToDouble(objRow(C1)("payamt")) & "," & ToDouble(objRow(C1)("payamt")) & ",'',0,0,0,'Kelebihan bayar " & Tchar(cashbankno.Text) & "','OPEN',0,'" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlcabang.SelectedValue & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPapoid & " WHERE tablename='QL_trndpar' AND cmpcode='" & cmpcode & "'"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                IcounterDPAPno += 1 : iDPapoid += 1
                            End If
                        End If
                        '--- total bayar ---
                        totalPayment += ToDouble(objRow(C1)("payamt"))
                    Next

                    Dim dtTable As DataTable = Session("tbldtl")
                    Dim dvRow As DataView = dtTable.DefaultView
                    Dim AmtNya As Double

                    'dvRow.RowFilter = "flagdtl <> 'OTHER'"
                    '======= Insert Nilai Bayar di Ql_conar =======
                    '============================================== 
                    For t1 As Integer = 0 To dvRow.Count - 1
                        If dvRow(t1)("payres1").ToString = "KURANG BAYAR" Then
                            AmtNya = ToDouble(dtTable.Compute("SUM(payamt)", "payrefoid =" & Integer.Parse(dvRow(t1)("payrefoid")) & ""))
                        Else
                            AmtNya = ToDouble(dvRow(t1)("payamt").ToString)
                        End If

                        If CurrencyOid.SelectedValue = 1 Then
                            vPayam = ToDouble(AmtNya)
                            vdpam = ToDouble(dvRow(t1)("DPAmt"))
                            vPayamidr = ToDouble(AmtNya)
                            vdpamidr = ToDouble(dvRow(t1)("DPAmt"))
                            If Session("invCurrOid") = 1 Then
                                vPayamusd = ToDouble(AmtNya) * dRate2
                                vdpamusd = ToDouble(dvRow(t1)("DPAmt")) * dRate2
                            Else
                                vPayamusd = ToDouble(AmtNya) / dRate2
                                vdpamusd = ToDouble(dvRow(t1)("DPAmt")) / dRate2
                            End If
                        Else
                            vPayam = ToDouble(AmtNya)
                            vdpam = ToDouble(dvRow(t1)("DPAmt"))
                            vPayamusd = ToDouble(AmtNya)
                            vdpamusd = ToDouble(dvRow(t1)("DPAmt"))
                            If Session("invCurrOid") = 1 Then
                                vPayamidr = vPayam / dRate2 : vdpamidr = vdpam / dRate2
                            Else
                                vPayamidr = vPayam * dRate2 : vdpamidr = vdpam * dRate2
                            End If
                        End If

                        sSql = "INSERT into QL_trnpayar(cmpcode,paymentoid,cashbankoid,custoid,payreftype, payrefoid,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd, paystatus,upduser,updtime,trndparoid,DPAmt,DPamtidr,DPAmtusd,payflag,branch_code,payres1)" & _
                        " VALUES ('" & cmpcode & "'," & sTemp & "," & cashbankoid.Text & "," & trnsuppoid.Text & ",'" & objRow(t1)("payreftype").ToString & "'," & objRow(t1)("payrefoid").ToString & "," & ToDouble(objRow(t1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & IIf(iDPOid > 0, iDPOid, cashbankacctgoid.SelectedValue()) & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(t1)("paynote").ToString) & "'," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp," & iDPOid & "," & ToDouble(objRow(t1)("DPAmt")) & "" & "," & ToDouble(objRow(t1)("DPAmt")) & "," & ToDouble(objRow(t1)("DPAmt")) & ",'" & objRow(t1)("flagdtl").ToString & "','" & ddlcabang.SelectedValue & "','" & Tchar(objRow(t1)("payres1").ToString) & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        sSql = "INSERT into QL_conar (cmpcode,conaroid,branch_code,reftype,refoid,payrefoid,custoid,amttrans,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnarstatus,trnarnote,trnartype) " & _
                       "VALUES ('" & cmpcode & "'," & Session("conaroid") & ",'" & ddlcabang.SelectedValue & "','QL_trnpayar'," & dvRow(t1)("payrefoid") & "," & sTemp & "," & trnsuppoid.Text & ",0," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(cashbankno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dvRow(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(cashbankno.Text) & "','" & dvRow(t1)("paytype").ToString & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaroid") += 1
                        sTemp += 1
                    Next 

                    ' Update lastoid conar
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conaroid") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conar'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    'Update lastoid dari QL_mstoid table QL_trnpayar
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp - 1 & " WHERE tablename='QL_trnpayar'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery() 
                End If
                '===========================================
                'End save invoice detail 1 saja
                '============================================

                If lblPOST.Text = "POST" Then
                    Dim custgroupoid As Integer = 0
                    sSql = "SELECT cg.custgroupoid ID FROM QL_MSTcust AS c inner join QL_mstcustgroup cg ON cg.custgroupoid = c.custgroupoid WHERE (c.CMPCODE = '" & cmpcode & "') /*and (c.branch_code = '" & ddlcabang.SelectedValue & "')*/ and c.custoid = " & trnsuppoid.Text & " ORDER BY custCODE"
                    objCmd.CommandText = sSql : custgroupoid = objCmd.ExecuteScalar

                    'update credit limit (potong creditlimitusagenya)
                    sSql = "update ql_mstcustgroup set custgroupcreditlimitusagerupiah=custgroupcreditlimitusagerupiah-" & ToDouble(totalPayment) & " Where custgroupoid=" & custgroupoid & " /*AND branch_code='" & ddlcabang.SelectedValue & "'*/"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    sSql = "update QL_mstcust set custcreditlimitusagerupiah=custcreditlimitusagerupiah-" & ToDouble(totalPayment) & " Where custoid=" & trnsuppoid.Text & " /*AND branch_code='" & ddlcabang.SelectedValue & "'*/"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    If Not Session("tbldtl") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("tbldtl")
                        '================================
                        'NORMAL POSTING
                        '       Piutang          100000
                        '   Kas/Bank        100000
                        '=================================
                        'KURANG BAYAR
                        '       Piutang          100000
                        '   Kas/Bank        80000
                        '   Retur           10000
                        '   Pot. Jual       10000
                        '=================================
                        'LEBIH BAYAR
                        '       Piutang         80000
                        '       Titipan         5000
                        '       BIAYA           5000
                        '       UM Jual         10000
                        '   Kas/Bank        100000
                        '=================================

                        '//////INSERT INTO TRN GL MST
                        sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,branch_code,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,type)" & _
                            "VALUES ('" & cmpcode & "'," & Session("vJurnalMst") & ",'" & ddlcabang.SelectedValue & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','A/R Payment|No=" & Tchar(cashbankno.Text) & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',current_timestamp,'AR PAYMENT')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        '//////INSERT INTO TRN GL DTL
                        Dim dvDtl As DataView = objTable.DefaultView
                        Dim glamt, glamtidr, glamtusd As Double
                        ' PIUTANG
                        dvDtl.RowFilter = "flagdtl<>'OTHER'"
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            glamt = dvDtl(c1)("payamt")
                            glamtidr = dvDtl(c1)("payamt")
                            glamtusd = dvDtl(c1)("payamt")

                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(cashbankno.Text) & "','A/R Payment | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""

                        ' LEBIH BAYAR
                        dvDtl.RowFilter = "flagdtl='OTHER' AND payamt>0"
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            glamt = dvDtl(c1)("payamt")
                            glamtidr = dvDtl(c1)("payamt")
                            glamtusd = dvDtl(c1)("payamt")

                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & "," & ToDouble(dvDtl(c1)("payamt")) & ",'" & Tchar(cashbankno.Text) & "','A/R Payment (Lebih Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""
                        Dim cglamt, cglamtidr, cglamtusd As Double
                        cglamt = ToDouble(amtbelinettodtl.Text)
                        cglamtidr = ToDouble(amtbelinettodtl.Text)
                        cglamtusd = ToDouble(amtbelinettodtl.Text)

                        ' CASH/BANK
                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime) " & _
                            "VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & cglamtidr & "," & cglamtidr & "," & cglamtidr & ",'" & Tchar(cashbankno.Text) & "','A/R Payment | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp)"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Dim kcglamt, kcglamtidr, kcglamtusd As Double

                        ' KURANG BAYAR
                        dvDtl.RowFilter = "flagdtl='OTHER' AND payamt < 0"
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            kcglamt = dvDtl(c1)("payamt") * -1
                            kcglamtidr = dvDtl(c1)("payamt") * -1
                            kcglamtusd = dvDtl(c1)("payamt") * -1

                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd, noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'D'," & kcglamtidr & "," & kcglamtidr & "," & kcglamtidr & ",'" & Tchar(cashbankno.Text) & "','A/R Payment (Kurang Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""
                        ' Update lastoid GLMST
                        sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        ' Update lastoid GLDTL
                        sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalDtl") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                End If
            Else
                '======
                'Update
                '======
                Dim bUpdPayType As Boolean = False
                If payflag.SelectedValue <> "GIRO" Then
                    girooid.Text = 0
                End If
                ' Karena No Bisa Manual, no bs diupdate terus
                bUpdPayType = True
                sSql = "UPDATE QL_trncashbankmst SET cashbankacctgoid=" & cashbankacctgoid.SelectedValue & ", cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), cashbanknote='" & Tchar(cashbanknote.Text) & "', upduser = '" & Session("UserID") & "',updtime=current_timestamp" & ",cashbankcurroid='" & CurrencyOid.SelectedValue & "',cashbankcurrate=" & ToDouble(currencyRate.Text) & ",cashbankstatus='" & lblPOST.Text & "', PIC=" & trnsuppoid.Text & ", pic_refname='QL_MSTCUST',cashbankrefno='" & Tchar(payrefno.Text.Trim) & "', cashbankamount = " & Session("totalbayar") & ", cashbankamountidr = " & Session("totalbayar") & ",cashbankno='" & Tchar(cashbankno.Text) & "', cashbanktype='" & sCBType & "' "

                If payflag.SelectedValue <> "GIRO" Then
                    sSql &= ",girodtloid = " & girooid.Text & ", giroref = " & girooid.Text & ""
                End If
                sSql &= " WHERE cmpcode = '" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "' and cashbankoid = " & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                ' Check Current DB if use DP then Reverse DP
                Dim iCurrentDP, iCurrentDPIDR, iCurrentDPUSD As Integer
                Dim dCurrentDPTotal, dCurrentDPTotalIDR, dCurrentDPTotalUSD As Double
                iCurrentDP = 0 : iCurrentDPIDR = 0 : iCurrentDPUSD = 0
                dCurrentDPTotal = 0 : dCurrentDPTotalIDR = 0 : dCurrentDPTotalUSD = 0

                sSql = "SELECT TOP 1 paybankoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : iCurrentDP = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamt) FROM QL_trnpayar par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotal = objCmd.ExecuteScalar()

                sSql = "SELECT TOP 1 paybankoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : iCurrentDP = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamtidr) FROM QL_trnpayar par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotalIDR = objCmd.ExecuteScalar()

                sSql = "SELECT TOP 1 paybankoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : iCurrentDP = objCmd.ExecuteScalar()

                sSql = "SELECT SUM(par.payamtusd) FROM QL_trnpayar par WHERE par.cmpcode='" & cmpcode & "' AND par.cashbankoid=" & Trim(cashbankoid.Text)
                objCmd.CommandText = sSql : dCurrentDPTotalUSD = objCmd.ExecuteScalar()
                ' UPDATE QL_trndpap bila pake DP
                If iDPOid <> 0 Then
                    If iCurrentDP <> 0 And lblPOST.Text = "POST" Then
                        sSql = "UPDATE QL_trndpar SET trndparacumamt=ISNULL(trndparacumamt,0)+" & dCurrentDPTotalIDR & ",trndparacumamtidr=ISNULL(trndparacumamtidr,0)+" & dCurrentDPTotalIDR & ",trndparacumamtusd=ISNULL(trndparacumamtusd,0)+" & dCurrentDPTotalIDR & ",trndparflag='OPEN'  WHERE cmpcode='" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "' AND trndparoid='" & iDPOid & "'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                    'tello dparacumamtidr
                    Dim dRate2 As Double : dRate2 = InvoiceRate(Session("invCurrOid"))
                    Dim dparacumamt, dparacumamtidr, dparacumamtusd As Double
                    If CurrencyOid.SelectedValue = 1 Then
                        dparacumamt = ToDouble(amtbelinettodtl.Text)
                        dparacumamtidr = ToDouble(amtbelinettodtl.Text) 
                    Else
                        dparacumamt = ToDouble(amtbelinettodtl.Text)
                        dparacumamtusd = ToDouble(amtbelinettodtl.Text) 
                    End If
                    If lblPOST.Text = "POST" Then
                        sSql = "UPDATE QL_trndpar SET trndparflag=CASE WHEN (ISNULL(trndparacumamt,0)+" & ToDouble(amtbelinettodtl.Text) & ")>=ISNULL(trndparamt,0) THEN 'CLOSE' ELSE 'OPEN' END  WHERE cmpcode='" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "' AND trndparoid='" & iDPOid & "'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                End If
                Session("conaroid") = GenerateID("ql_conar", cmpcode)
                Dim totalPayment As Decimal = 0
                If Not Session("tbldtl") Is Nothing Then
                    sSql = "Delete from QL_conar where cmpcode='" & cmpcode & "' and branch_code = '" & ddlcabang.SelectedValue & "' AND payrefoid IN (SELECT paymentoid FROM QL_trnpayar WHERE cmpcode='" & cmpcode & "' AND cashbankoid=" & cashbankoid.Text & ")"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    sSql = "Delete from QL_trnpayar where cmpcode='" & cmpcode & "' and branch_code = '" & ddlcabang.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    '===========================================
                    'Start save invoice detail(banyak detail saja)
                    '============================================
                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("tbldtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                    Dim vpayamt, vpayamtidr, vpayamtusd As Double
                    Dim dRate2 As Double
                    dRate2 = InvoiceRate(Session("invCurrOid"))
                    Dim dpamt, dpamtidr, dpamtusd As Double
                    For C1 As Integer = 0 To objRow.Length - 1

                        'update ke acumamt ql_trnbeli sby tanda pembayaran 
                        If lblPOST.Text = "POST" Then
                            If CurrencyOid.SelectedValue = 1 Then
                                vpayamt = ToDouble(objRow(C1)("payamt"))
                                vpayamtidr = ToDouble(objRow(C1)("payamt")) 
                            Else
                                vpayamt = ToDouble(objRow(C1)("payamt"))
                                vpayamtusd = ToDouble(objRow(C1)("payamt")) 
                            End If

                            If itrnbelimstoid <> objRow(C1)("payrefoid") And objRow(C1)("payamt") > 0 Then
                                sSql = "update ql_trnjualmst set accumpayment=accumpayment+" & ToDouble(objRow(C1)("payamt")) & " ,accumpaymentidr=accumpaymentidr+" & ToDouble(objRow(C1)("payamt")) & ",accumpaymentusd=accumpaymentusd+ " & ToDouble(objRow(C1)("payamt")) & ", lastpaymentdate='" & CDate(toDate(PaymentDate.Text)) & "' Where cmpcode='" & cmpcode & "'  and branch_code = '" & ddlcabang.SelectedValue & "' and trnjualmstoid=" & objRow(C1)("payrefoid")
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                itrnbelimstoid = objRow(C1)("payrefoid")
                            ElseIf objRow(C1)("payamt") > 0 And objRow(C1)("trndpapoid") = 1 Then
                                sSql = "INSERT INTO QL_trndpar (cmpcode,trndparoid,trndparno,trndpardate,custoid,cashbankoid, trndparacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate, trndparamt,trndparamtidr,trndparamtusd,taxtype,taxoid,taxpct,taxamt,trndparnote,trndparflag,trndparacumamt,trndparstatus, createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                    "('" & cmpcode & "'," & iDPapoid & ",'" & GenNumberString(sNoDPAP, "", IcounterDPAPno, 3) & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime))," & trnsuppoid.Text & "," & cashbankacctgoid.SelectedValue & "," & objRow(C1)("payacctgoid") & ",'" & sCBType & "'," & cashbankacctgoid.SelectedValue & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(cashbankno.Text) & "'," & CurrencyOid.SelectedValue & "," & ToDouble(currencyRate.Text) & "," & ToDouble(objRow(C1)("payamt")) & "," & ToDouble(objRow(C1)("payamt")) & "," & ToDouble(objRow(C1)("payamt")) & ",'',0,0,0,'Kelebihan bayar " & Tchar(cashbankno.Text) & "','OPEN',0,'" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ddlcabang.SelectedValue & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPapoid & " WHERE tablename='QL_trndpar' AND cmpcode='" & cmpcode & "' and branch_code = '" & ddlcabang.SelectedValue & "'"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                                IcounterDPAPno += 1 : iDPapoid += 1
                            End If
                        End If

                        totalPayment += ToDouble(objRow(C1)("payamt"))
                    Next

                    '======= Insert Nilai Bayar di Ql_conar =======
                    '==============================================
                    Dim dtTable As DataTable = Session("tbldtl")
                    Dim dvRow As DataView = dtTable.DefaultView
                    Dim AmtNya As Double
                    'dvRow.RowFilter = "flagdtl <> 'OTHER'"
                    For t1 As Integer = 0 To dvRow.Count - 1
                        If dvRow(t1)("payres1").ToString = "KURANG BAYAR" Then
                            AmtNya = ToDouble(dtTable.Compute("SUM(payamt)", "payrefoid =" & dvRow(t1)("payrefoid") & ""))
                        Else
                            AmtNya = ToDouble(dvRow(t1)("payamt"))
                        End If

                        If CurrencyOid.SelectedValue = 1 Then
                            vpayamt = ToDouble(AmtNya)
                            vpayamtidr = vpayamt
                            dpamt = ToDouble(dvRow(t1)("DPAmt"))
                            dpamtidr = ToDouble(dvRow(t1)("DPAmt"))
                        Else
                            vpayamt = ToDouble(AmtNya)
                            vpayamtusd = vpayamt
                            dpamt = ToDouble(dvRow(t1)("DPAmt").ToString)
                            dpamtusd = ToDouble(dvRow(t1)("DPAmt").ToString)
                        End If

                        If CurrencyOid.SelectedValue = 1 Then
                            vpayamt = ToDouble(objRow(t1)("payamt"))
                            vpayamtidr = vpayamt
                            dpamt = ToDouble(objRow(t1)("DPAmt"))
                            dpamtidr = ToDouble(objRow(t1)("DPAmt"))
                        Else
                            vpayamt = ToDouble(objRow(t1)("payamt"))
                            vpayamtusd = vpayamt
                            dpamt = ToDouble(objRow(t1)("DPAmt"))
                            dpamtusd = ToDouble(objRow(t1)("DPAmt"))
                        End If

                        sSql = "INSERT into QL_trnpayar(cmpcode,paymentoid,cashbankoid,custoid,payreftype, payrefoid,payacctgoid,payrefno,paybankoid,payduedate,paynote,payamt,payamtidr,payamtusd, paystatus,upduser,updtime,trndparoid,DPAmt,DPamtidr,DPAmtusd,payflag,branch_code,payres1)" & _
                        " VALUES ('" & cmpcode & "'," & sTemp & "," & cashbankoid.Text & "," & trnsuppoid.Text & ",'" & objRow(t1)("payreftype").ToString & "'," & objRow(t1)("payrefoid").ToString & "," & ToDouble(objRow(t1)("payacctgoid").ToString) & ",'" & Tchar(payrefno.Text.Trim) & "'," & IIf(iDPOid > 0, iDPOid, cashbankacctgoid.SelectedValue()) & ",'" & CDate(toDate(payduedate.Text)) & "','" & Tchar(objRow(t1)("paynote").ToString) & "'," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & "," & ToDouble(objRow(t1)("payamt")) & ",'" & lblPOST.Text & "','" & Session("UserID") & "',current_timestamp," & iDPOid & "," & ToDouble(objRow(t1)("DPAmt")) & "" & "," & ToDouble(objRow(t1)("DPAmt")) & "," & ToDouble(objRow(t1)("DPAmt")) & ",'" & objRow(t1)("flagdtl").ToString & "','" & ddlcabang.SelectedValue & "','" & Tchar(objRow(t1)("payres1").ToString) & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        sSql = "INSERT into QL_conar (cmpcode,conaroid,branch_code,reftype,refoid,payrefoid,custoid,amttrans,amtbayar,amtbayaridr,amtbayarusd,paymentdate,periodacctg,upduser,updtime,payrefno,acctgoid,paymentacctgoid,trnarstatus,trnarnote,trnartype) " & _
                       "VALUES ('" & cmpcode & "'," & Session("conaroid") & ",'" & ddlcabang.SelectedValue & "','QL_trnpayar'," & dvRow(t1)("payrefoid") & "," & sTemp & "," & trnsuppoid.Text & ",0," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & "," & IIf(objRow(t1)("flagdtl") = "OTHER" And objRow(t1)("payamt") < 0, ToDouble(dvRow(t1)("payamt")) * -1, ToDouble(AmtNya)) & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','" & Session("UserID") & "',current_timestamp,'" & Tchar(cashbankno.Text) & "'," & cashbankacctgoid.SelectedValue & "," & dvRow(t1)("payacctgoid") & ",'" & lblPOST.Text & "','" & Tchar(cashbankno.Text) & "','" & dvRow(t1)("paytype").ToString & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("conaroid") += 1
                        sTemp += 1
                    Next
                    dvRow.RowFilter = ""


                    ' Update lastoid conar
                    sSql = "UPDATE ql_mstoid SET lastoid=" & Session("conaroid") - 1 & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_conar'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    'Update lastoid dari QL_mstoid table QL_trnpayar
                    sSql = "UPDATE QL_mstoid SET lastoid=" & sTemp - 1 & " WHERE tablename='QL_trnpayar'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                Else
                    showMessage("Silahkan pilih nota jual", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If
                '===========================================
                'End save invoice Banyk detail  saja
                '============================================

                If lblPOST.Text = "POST" Then
                    'update credit limit (potong creditlimitusagenya)
                    'sSql = "update ql_mstcust set custcreditlimitusagerupiah=custcreditlimitusagerupiah-" & ToDouble(totalPayment) & " Where custoid=" & trnsuppoid.Text & ""
                    'objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    If Not Session("tbldtl") Is Nothing Then
                        Dim objTable As DataTable : objTable = Session("tbldtl")
                        '================================
                        'NORMAL POSTING
                        '       Piutang          100000
                        '   Kas/Bank        100000
                        '=================================
                        'KURANG BAYAR
                        '       Piutang          100000
                        '   Kas/Bank        80000
                        '   Retur           10000
                        '   Pot. Jual       10000
                        '=================================
                        'LEBIH BAYAR
                        '       Piutang         80000
                        '       Titipan         10000
                        '       UM Jual         10000
                        '   Kas/Bank        100000
                        '=================================
                        '//////INSERT INTO TRNGLMST
                        sSql = "INSERT into QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,upduser,updtime,branch_code,type)" & _
                            "VALUES ('" & cmpcode & "'," & Session("vJurnalMst") & ",(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & GetDateToPeriodAcctg3(CDate(toDate(PaymentDate.Text))) & "','A/R Payment|No=" & Tchar(cashbankno.Text) & "','" & lblPOST.Text & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "','AR PAYMENT')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        '//////INSERT INTO TRNGLDTL
                        Dim dvDtl As DataView = objTable.DefaultView
                        Dim dRate2 As Double : dRate2 = InvoiceRate(Session("invCurrOid"))
                        ' PiUTANG
                        dvDtl.RowFilter = "flagdtl<>'OTHER'"
                        Dim vcashbankglamt, vcashbankglamtidr, vcashbankglamtusd As Double
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            If CurrencyOid.SelectedValue = 1 Then
                                vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                                vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) * dRate2
                                Else
                                    vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) / dRate2
                                End If
                            Else
                                vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                                vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) / dRate2
                                Else
                                    vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) * dRate2
                                End If
                            End If

                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & vcashbankglamt & "," & vcashbankglamtidr & "," & vcashbankglamtusd & ",'" & Tchar(cashbankno.Text) & "','A/R Payment | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""

                        ' LEBIH BAYAR
                        dvDtl.RowFilter = "flagdtl='OTHER' AND payamt>0"
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            If CurrencyOid.SelectedValue = 1 Then
                                vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                                vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) * dRate2
                                Else
                                    vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt")) / dRate2
                                End If
                            Else
                                vcashbankglamt = ToDouble(dvDtl(c1)("payamt"))
                                vcashbankglamtusd = ToDouble(dvDtl(c1)("payamt"))
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) / dRate2
                                Else
                                    vcashbankglamtidr = ToDouble(dvDtl(c1)("payamt")) * dRate2
                                End If
                            End If
                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'C'," & vcashbankglamt & "," & vcashbankglamtidr & "," & vcashbankglamtusd & ",'" & Tchar(cashbankno.Text) & "','A/R Payment (Lebih Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""
                        ' CASH/BANK
                        Dim vcashbankglamt2, vcashbankglamtidr2, vcashbankglamtusd2 As Double
                        If CurrencyOid.SelectedValue = 1 Then
                            vcashbankglamt2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                            vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text)) * dRate2
                            Else
                                vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text)) / dRate2
                            End If
                        Else
                            vcashbankglamtusd2 = ToDouble(ToDouble(amtbelinettodtl.Text))
                            If Session("invCurrOid") = 1 Then
                                vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text)) / dRate2
                            Else
                                vcashbankglamtidr2 = ToDouble(ToDouble(amtbelinettodtl.Text)) * dRate2
                            End If
                        End If

                        sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) " & _
                            "VALUES ('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & cashbankacctgoid.SelectedValue & ",'D'," & vcashbankglamt2 & "," & vcashbankglamtidr2 & "," & vcashbankglamtusd2 & ",'" & Tchar(cashbankno.Text) & "','A/R Payment | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1

                        ' KURANG BAYAR
                        dvDtl.RowFilter = "flagdtl='OTHER' AND payamt<0"
                        Dim vcashbankglamt3, vcashbankglamtidr3, vcashbankglamtusd3 As Double
                        For c1 As Integer = 0 To dvDtl.Count - 1
                            If CurrencyOid.SelectedValue = 1 Then
                                vcashbankglamt3 = ToDouble(dvDtl(c1)("payamt")) * -1
                                vcashbankglamtidr3 = ToDouble(dvDtl(c1)("payamt")) * -1
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtusd3 = ToDouble(dvDtl(c1)("payamt")) * -1 * dRate2
                                Else
                                    vcashbankglamtusd3 = ToDouble(dvDtl(c1)("payamt")) * -1 / dRate2
                                End If
                            Else
                                vcashbankglamt3 = ToDouble(dvDtl(c1)("payamt")) * -1
                                vcashbankglamtusd3 = ToDouble(dvDtl(c1)("payamt")) * -1
                                If Session("invCurrOid") = 1 Then
                                    vcashbankglamtidr3 = ToDouble(dvDtl(c1)("payamt")) * -1 / dRate2
                                Else
                                    vcashbankglamtidr3 = ToDouble(dvDtl(c1)("payamt")) * -1 * dRate2
                                End If
                            End If

                            sSql = "INSERT into QL_trngldtl (cmpcode,gldtloid,branch_code,glseq,glmstoid,acctgoid,gldbcr,glamt,glamtidr,glamtusd,noref,glnote,glother1,glother2,glpostdate,upduser,updtime,glflag) VALUES " & _
                                "('" & cmpcode & "'," & Session("vJurnalDtl") & ",'" & ddlcabang.SelectedValue & "'," & Session("vDtlSeq") & "," & Session("vJurnalMst") & "," & dvDtl(c1)("payacctgoid") & ",'D'," & vcashbankglamt3 & "," & vcashbankglamtidr3 & "," & vcashbankglamtusd3 & ",'" & Tchar(cashbankno.Text) & "','A/R Payment (Kurang Bayar) | Cust. " & Tchar(suppnames.Text) & " | No. Cashbank. " & Tchar(cashbankno.Text) & " | Note. " & Tchar(cashbanknote.Text) & " | No. Nota. " & Tchar(dvDtl(c1)("trnbelino")) & "','','" & cashbankoid.Text & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & lblPOST.Text & "')"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Session("vJurnalDtl") += 1 : Session("vDtlSeq") += 1
                        Next
                        dvDtl.RowFilter = ""
                        ' Update lastoid GLMST
                        sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalMst") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trnglmst'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                        ' Update lastoid GLDTL
                        sSql = "UPDATE ql_mstoid SET lastoid=" & Session("vJurnalDtl") & " WHERE cmpcode='" & cmpcode & "' AND tablename='ql_trngldtl'"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    End If
                End If
            End If

            Dim dRate As Double
            dRate = InvoiceRate(Session("invCurrOid"))
            Dim iCbdtloid As Int64 = ClassFunctionAccounting.GenerateID("QL_CASHBANKGL", cmpcode)
            If lblPOST.Text = "POST" Then
                Dim objtablexx As DataTable
                Dim objrowxx() As DataRow
                objtablexx = Session("tbldtl")
                objrowxx = objtablexx.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                Dim vcashbankglamt, vcashbankglamtidr, vcashbankglamtusd As Double
                For C1 As Int16 = 0 To objrowxx.Length - 1
                    'INSERT TO CASHBANK GL DULU
                    If CurrencyOid.SelectedValue = 1 Then
                        vcashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                        vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString)
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) * dRate
                        Else
                            vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString) / dRate
                        End If
                    Else
                        vcashbankglamt = ToDouble(objrowxx(C1)("payamt").ToString)
                        vcashbankglamtusd = ToDouble(objrowxx(C1)("payamt").ToString)
                        If Session("invCurrOid") = 1 Then
                            vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) / dRate
                        Else
                            vcashbankglamtidr = ToDouble(objrowxx(C1)("payamt").ToString) * dRate
                        End If
                    End If

                    sSql = "INSERT INTO QL_cashbankgl (cmpcode,cashbankgloid,cashbankoid,acctgoid,cashbankglamt,cashbankglamtidr,cashbankglamtusd,cashbankglnote,cashbankglstatus,cashbankglres1,duedate,refno,createuser,createtime,upduser,updtime,branch_code) VALUES " & _
                                         "('" & cmpcode & "'," & iCbdtloid & "," & cashbankoid.Text & "," & ToDouble(objrowxx(C1)("payacctgoid").ToString) & "," & vcashbankglamt & "," & vcashbankglamtidr & "," & vcashbankglamtusd & ",'A/R Payment|No=" & Tchar(objrowxx(C1)("TRNBELINO").ToString) & " ~ " & Tchar(objrowxx(C1)("PAYNOTE").ToString) & "','" & lblPOST.Text & "','','" & CDate(toDate(PaymentDate.Text)) & "','" & Tchar(payrefno.Text.Trim) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp,'" & ddlcabang.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    iCbdtloid += 1
                Next
                sSql = "update  QL_mstoid set lastoid=" & iCbdtloid & " where tablename like 'QL_cashbankGL' and cmpcode like '%" & cmpcode & "%'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            End If

            'recovery credit limit
            sSql = "Update c set custcreditlimitusagerupiah= isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit   from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid  ),0),custcreditlimitusage = isnull(( select sum(trnamtjualnetto - (accumpayment + amtretur )) climit   from QL_trnjualmst where trnjualstatus = 'POST' and trnamtjualnetto - (accumpayment + amtretur ) > 0 and custoid = trncustoid group by trncustoid  ),0)  from QL_mstcust c "
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "update g set trnid = (select trnid from QL_trncashbankmst where cashbankoid = g.cashbankoid and branch_code = g.branch_code) from QL_cashbankgl g inner join QL_trncashbankmst cb on cb.branch_code=g.branch_code AND cb.cashbankoid=g.cashbankoid AND g.trnid=0"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close() : lblPOST.Text = ""
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBox") : objTrans.Dispose() : Exit Sub
        End Try
        Session("tbldtl") = Nothing : Session("oid") = Nothing
        If lblPOST.Text = "POST" Then
            showMessage("Data telah diposting !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        End If
    End Sub

    Protected Sub amtpayment_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtpayment.TextChanged
        CalculateTotalPayment()
    End Sub

    Protected Sub SqlDataSource1_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs)

    End Sub

    Protected Sub CurrencyOid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CurrencyOid.SelectedIndexChanged
        FillCurrencyRate(CurrencyOid.SelectedValue)
        If Session("oid") = "" Or Session("oid") <= 0 Then
            ClearDtlAP(True) : Session("tbldtl") = Nothing
            GVDtlPayAP.DataSource = Nothing
            If payflag.SelectedValue = "GIRO" Then
                payrefno.Text = ""
                AmountGiro.Text = 0
            End If
        Else
            Session("currsess") = True

            FillTextBox(Session("oid"), Session("branch"))
        End If
        If lblPOST.Text = "POST" Then
            btnSave.Visible = False
        End If

    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.SetActiveView(View1)
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'MultiView1.SetActiveView(View2)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & Tchar(cashbankno.Text) & "'"

        PrintReport(cashbankno.Text, cKoneksi.ambilscalar(sSql))

    End Sub

    Protected Sub GVmstPAYAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmstPAYAP.SelectedIndexChanged
        Response.Redirect("~\ACCOUNTING\trnPayAR.aspx?branch_code=" & GVmstPAYAP.SelectedDataKey("branch_code").ToString & "&oid=" & GVmstPAYAP.SelectedDataKey("cashbankoid").ToString & "")
    End Sub

    Protected Sub imbLastSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sQuery As String = Session("SearchAP")
        If sQuery <> "" Then
            Dim objTable As DataTable = cKoneksi.ambiltabel(Session("SearchAP"), "ql_trncashbankmst")
            Session("tbldata") = objTable
            GVmstPAYAP.DataSource = objTable
            GVmstPAYAP.DataBind()
            calcTotalInGrid()
        End If
    End Sub

    Protected Sub btnClearSupp_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub txtAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub DP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblDPAmount.Visible = cbDP.Checked
        ddlDPNo.Visible = cbDP.Checked
        DPAmt.Visible = cbDP.Checked
        If cbDP.Checked Then
            initddlDP(cKoneksi.ambilscalar("SELECT trncustoid FROM QL_trnjualmst WHERE cmpcode='" & cmpcode & "' AND trnbelimstoid=" & trnbelimstoid.Text))
            If ddlDPNo.Items.Count > 0 Then
                DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue())), 3)
            Else
                DPAmt.Text = ToMaskEdit(0, 3)
            End If

        End If
    End Sub

    Protected Sub ddlDPNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDPNo.SelectedIndexChanged
        DPAmt.Text = ToMaskEdit(ToDouble(cKoneksi.ambilscalar("SELECT trndparamt FROM QL_trndpar WHERE trndparoid=" & ddlDPNo.SelectedValue())), 3)
    End Sub

    ' UPDATE MULTI ACCOUNT on DEBET/CREDIT ==> SELEISH PEMBAYARAN

    Protected Sub chkOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetOtherAccount(chkOther.Checked)
        InitOtherAcctg(DDLOtherType.SelectedValue)

        If DDLOtherType.SelectedValue = "-" Then
            If chkOther.Checked Then
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text) + ToDouble(otheramt.Text), 3)
            Else
                totalpayment.Text = ToMaskEdit(ToDouble(amtpayment.Text), 3)
            End If
        End If
        'If DDLOtherType.SelectedValue = "-" Then
        '    If chkOther.Checked Then
        '        otheramt.ReadOnly = False : otheramt.CssClass = "inpText"
        '    Else
        '        otheramt.ReadOnly = True : otheramt.CssClass = "inpTextDisabled"
        '    End If
        'End If
    End Sub

    Protected Sub DDLOtherType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitOtherAcctg(DDLOtherType.SelectedValue)
    End Sub

    Protected Sub lkbAddDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sMsg As String = ""
        If otherAcctgoid.Items.Count < 1 Then
            sMsg &= "- tidak ada COA untuk beda bayar !!<BR>"
        End If
        If ToDouble(amtdtlselisih.Text) <= 0 Then
            sMsg &= "- Total beda bayar harus > 0 !!<BR>"
        End If
        If dtlnoteselisih.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal Note is 200 Karakter !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Session("DtlSelisih") Is Nothing Then
            Dim nu As DataTable = SetTabelDetailSlisih()
            Session("DtlSelisih") = nu
        End If

        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim oRow As DataRow
        If stateDtlSls.Text = "New Selisih" Then
            oRow = objTable.NewRow
            oRow("selisihseq") = objTable.Rows.Count + 1
        Else
            oRow = objTable.Rows(gvDtlSelisih.SelectedIndex)
            oRow.BeginEdit()
        End If

        oRow("acctgoid") = otherAcctgoid.SelectedValue
        oRow("acctgdesc") = otherAcctgoid.SelectedItem.Text
        oRow("amtdtlselisih") = ToDouble(amtdtlselisih.Text)
        oRow("dtlnoteselisih") = dtlnoteselisih.Text

        If stateDtlSls.Text = "New Selisih" Then
            objTable.Rows.Add(oRow)
        Else
            oRow.EndEdit()
        End If
        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        ClearDtlSelisih()
        CalculateSelisih()
    End Sub

    Protected Sub lkbClearDtlSlisih_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ClearDtlSelisih()
    End Sub

    Protected Sub gvDtlSelisih_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtlSelisih.SelectedIndexChanged
        Dim objTable As DataTable : objTable = Session("DtlSelisih")
        Dim dvTemp As DataView = objTable.DefaultView
        dvTemp.RowFilter = "selisihseq=" & gvDtlSelisih.SelectedDataKey("selisihseq").ToString
        otherAcctgoid.SelectedValue = dvTemp(0)("acctgoid").ToString
        amtdtlselisih.Text = ToMaskEdit(ToDouble(dvTemp(0)("amtdtlselisih").ToString), 3)
        dtlnoteselisih.Text = dvTemp(0)("dtlnoteselisih").ToString
        dvTemp.RowFilter = ""
        gvDtlSelisih.Columns(5).Visible = False
        stateDtlSls.Text = "Update"
    End Sub

    Protected Sub gvDtlSelisih_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtlSelisih.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub

    Protected Sub gvDtlSelisih_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtlSelisih.RowDeleting
        Dim idx As Integer = e.RowIndex
        Dim objTable As DataTable
        objTable = Session("DtlSelisih")
        objTable.Rows.RemoveAt(idx)

        'resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("selisihseq") = C2 + 1
        Next

        Session("DtlSelisih") = objTable
        gvDtlSelisih.DataSource = Session("DtlSelisih")
        gvDtlSelisih.DataBind()
        CalculateSelisih()
    End Sub

    Protected Sub PaymentDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(toDate(PaymentDate.Text)) Then
            GenerateDefaultNo(payflag.SelectedValue, cashbankacctgoid.SelectedValue, CDate(toDate(PaymentDate.Text)))
        Else
            showMessage("Format tanggal salah !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            PaymentDate.Text = GetServerTime() : Exit Sub
        End If
    End Sub

    Protected Sub amtdtlselisih_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles amtdtlselisih.TextChanged, DPAmt.TextChanged
        sender.text = ToMaskEdit(ToDouble(sender.text), 3)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        filterGVS("", "", cmpcode, "MANUFACTURE", ddlcabang.SelectedValue)
        hiddenbtn2.Visible = True
        Panel1.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        trnsuppoid.Text = "" : suppname.Text = ""
        ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind() : calcTotalInGridDtl()
        'payflag.SelectedIndex = 0 : payflag_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub ClearDtlAP()
        Session("tbldtl") = Nothing
        GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind()
        Session("DtlSelisih") = Nothing
        gvDtlSelisih.DataSource = Nothing
        gvDtlSelisih.DataBind()

        ClearDtlAP(True)
    End Sub

    Protected Sub ibtnSuppID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSuppID.Click
        ModalPopupExtender1.Show()
        filterGVS(txtFindSuppID.Text, "", cmpcode, "MANUFACTURE", Session("branch_id"))
    End Sub

    Protected Sub imbViewAlls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewAlls.Click
        txtFindSuppID.Text = "" : filterGVS("", "", cmpcode, "MANUFACTURE", Session("branch_id"))
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub CloseSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseSupp.Click
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged

        ClearDtlAP()
        Session("tbldtl") = Nothing : GVDtlPayAP.DataSource = Nothing
        GVDtlPayAP.DataBind() : calcTotalInGridDtl()
        'payflag.SelectedIndex = 0 : payflag_SelectedIndexChanged(Nothing, Nothing)
        trnsuppoid.Text = gvSupplier.SelectedDataKey.Item("ID")
        suppnames.Text = gvSupplier.SelectedDataKey.Item("Name").ToString
        cProc.SetModalPopUpExtender(hiddenbtn2, Panel1, ModalPopupExtender1, False)
        cProc.DisposeGridView(gvSupplier)
        Fill_payflag()
        'payflag.SelectedValue = Session("payflag")
    End Sub

    Protected Sub trndpapoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trndpapoid.SelectedIndexChanged
        If trndpapoid.Items.Count > 0 Then
            FillDPBalance(trndpapoid.SelectedValue)
            FillDPAccount(trndpapoid.SelectedValue)
        Else
            FillDPBalance(0)
        End If
    End Sub

    Protected Sub CREDITCLEAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CREDITCLEAR.Click
        payrefno.Text = ""
        code.Text = ""
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCancel2.Click
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnshowCOA.Click
        showTableCOA(cashbankno.Text, cmpcode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 3)
        End If
    End Sub

    Protected Sub GVmstPAYAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVmstPAYAP.PageIndex = e.NewPageIndex
        GVmstPAYAP.DataSource = Session("tbldata")
        GVmstPAYAP.DataBind()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnPayAR.aspx?awal=true")
        End If
    End Sub

    Protected Sub ibtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtn.Click
        Dim sMsg As String = ""
        If trnbelimstoid.Text = "" Then : sMsg &= "- Pilih Nota Jual !!<BR>" : End If
        If ToDouble(amtpayment.Text) <= 0 Then : sMsg &= "- Total Payment must be >0 !!<BR>" : End If
        'If payduedate.Text.Trim = "" And payflag.SelectedValue = "CASH" Then
        '    payduedate.Text = "01/01/1900"
        'End If
        'If Session("invCurrOid") = "" Then
        '    Session("invCurrOid") = invCurrOid.Text
        'End If
        'If Session("paymentOid") = "" Then
        '    Session("paymentOid") = trnbelimstoid.Text
        'End If
        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = PaymentDate.Text
        End If
        If payflag.SelectedValue = "GIRO" And ToDouble(amtpayment.Text) > ToDouble(AmountGiro.Text) Then
            sMsg &= "Nilai pembayaran tidak boleh lebih besar dari nilai giro !"
        End If

        If chkOther.Checked Then
            If Session("DtlSelisih") Is Nothing Then
                sMsg &= "- Tidak ada detail beda bayar !!<BR>"
            Else
                Dim objCek As DataTable = Session("DtlSelisih")
                If objCek.Rows.Count < 1 Then
                    sMsg &= "- Tidak ada detail beda bayar !!<BR>"
                Else
                    Dim dTotalSelisih As Double = ToMaskEdit(ToDouble(objCek.Compute("SUM(amtdtlselisih)", "").ToString), 3)
                    If dTotalSelisih > ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 3)) Then
                        sMsg &= "- Maksimum total detail di selisih bayar =" & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 3) & " !!<BR>"
                    End If
                    If DDLOtherType.SelectedValue = "+" Then
                        If dTotalSelisih <> ToDouble(ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 3)) Then
                            sMsg &= "- Total detail di selisih bayar harus = " & ToMaskEdit(Math.Abs(ToDouble(APAmt.Text) - ToDouble(amtpayment.Text)), 3) & " !!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If txtNote.Text.Trim.Length > 200 Then
            sMsg &= "- Maksimal Note detail adalah 200 karakter !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        'Disabled ketika addtolist
        payflag.CssClass = "inpTextDisabled"
        payflag.Enabled = False

        'GET ACCTGOID DARI ALL DP
        Dim VAR_DPAR As String = cKoneksi.ambilscalar("SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='VAR_DPAR'")
        sSql = "SELECT acctgoid,acctgcode+'-'+acctgdesc FROM QL_mstacctg WHERE acctgcode LIKE '" & VAR_DPAR & "%'  AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode )"
        Session("acctgoid_dp") = cKoneksi.ambiltabel(sSql, "acctgoid_dp")


        If (Session("oid") = Nothing Or Session("oid") = "") And Session("tbldtl") Is Nothing Then
            Dim dtlTable As DataTable = SetTabelDetail()
            Session("tbldtl") = dtlTable
        End If

        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        If I_U2.Text = "New Detail" Then
            dv.RowFilter = "trnbelino='" & Trim(trnbelino.Text) & "' AND flagdtl<>'OTHER'"
        Else
            dv.RowFilter = "trnbelino='" & Trim(trnbelino.Text) & "' AND flagdtl<>'OTHER' AND payseq<>" & Payseq.Text
        End If
        If dv.Count > 0 Then
            showMessage("Data sudah ditambahkan sebelumnya !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            dv.RowFilter = ""
            GVDtlPayAP.DataSource = Session("tbldtl")
            GVDtlPayAP.DataBind()
            Exit Sub
        End If
        dv.RowFilter = ""

        ' DELETE data bila ada
        For C1 As Integer = objTable.Rows.Count - 1 To 0 Step -1
            If objTable.Rows(C1).Item("trnbelino") = trnbelino.Text Then
                objTable.Rows.RemoveAt(C1)
            End If
        Next

        Dim oRow As DataRow
        oRow = objTable.NewRow
        oRow("payseq") = objTable.Rows.Count + 1
        oRow("paymentoid") = 0
        oRow("cashbankoid") = 0
        oRow("payreftype") = "ql_trnjualmst"
        oRow("payrefoid") = trnbelimstoid.Text
        oRow("payacctgoid") = acctgoid.Text
        oRow("paynote") = IIf(txtNote.Text = "", trnbelino.Text & " ~ ", "") & "" & txtNote.Text

        If chkOther.Checked = True Then
            If DDLOtherType.SelectedValue = "+" Then
                oRow("payamt") = ToDouble(APAmt.Text)
                oRow("payres1") = "LEBIH BAYAR"
            Else
                oRow("payamt") = ToDouble(totalpayment.Text)
                oRow("payres1") = "KURANG BAYAR"
            End If
        Else
            oRow("payamt") = ToDouble(amtpayment.Text)
            oRow("payres1") = "NORMAL"
        End If

        oRow("paytype") = paytype.Text
        oRow("trnbelino") = trnbelino.Text
        oRow("suppname") = suppname.Text
        oRow("amttrans") = ToDouble(amttrans.Text)
        oRow("amtpaid") = ToDouble(amtpaid.Text)
        oRow("amtretur") = ToDouble(amtretur.Text)
        oRow("acctgdesc") = APAcc.Text
        oRow("trntaxpct") = ToDouble(trnTaxPct.Text)
        oRow("invCurrOid") = invCurrOid.Text
        oRow("invCurrRate") = ToDouble(invCurrRate.Text)
        oRow("invCurrCode") = invCurrCode.Text
        oRow("invCurrDesc") = invCurrDesc.Text
        oRow("invPayment") = ToDouble(amtpayment.Text)
        If Session("totalbayar") Is Nothing Or Session("totalbayar") = "0" Then
            Session("totalbayar") = ToDouble(amtpayment.Text)
            AmountGiro.Text = ToMaskEdit(ToDouble(AmountGiro.Text), 3) - ToDouble(amtpayment.Text)
        Else
            Session("totalbayar") += ToDouble(amtpayment.Text)
            AmountGiro.Text = ToMaskEdit(ToDouble(AmountGiro.Text), 3) - ToDouble(amtpayment.Text)
        End If


        If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
            payduedate.Text = Format(Now, "dd/MM/yyyy")
        End If
        oRow("payduedate") = "01/01/1900" ''toDate(payduedate.Text)
        oRow("payrefno") = payrefno.Text
        oRow("flagdtl") = ""

        oRow("trndpapoid") = 0
        If payflag.SelectedValue = "DP" Then
            oRow("DPAmt") = ToDouble(dpbalance.Text)
        Else
            oRow("DPAmt") = 0
        End If
        objTable.Rows.Add(oRow)

        ' Bila ada selisih
        If chkOther.Checked Then
            Dim tbSls As DataTable = Session("DtlSelisih")
            Dim dTotalSelisih As Double = ToDouble(tbSls.Compute("SUM(amtdtlselisih)", "").ToString)
            For C2 As Integer = 0 To tbSls.Rows.Count - 1
                Dim PlusRow As DataRow
                PlusRow = objTable.NewRow
                PlusRow("payseq") = objTable.Rows.Count + 1
                PlusRow("paymentoid") = 0
                PlusRow("cashbankoid") = 0
                PlusRow("payreftype") = "ql_trnjualmst"
                PlusRow("payrefoid") = trnbelimstoid.Text
                PlusRow("payacctgoid") = tbSls.Rows(C2)("acctgoid").ToString
                PlusRow("paynote") = trnbelino.Text & " ~ " & tbSls.Rows(C2)("dtlnoteselisih").ToString
                If DDLOtherType.SelectedValue = "+" Then
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString)
                    PlusRow("invPayment") = ToDouble(amtpayment.Text) - dTotalSelisih
                    PlusRow("payres1") = "LEBIH BAYAR"
                Else
                    PlusRow("invPayment") = ToDouble(amtpayment.Text) + dTotalSelisih
                    PlusRow("payamt") = ToDouble(tbSls.Rows(C2)("amtdtlselisih").ToString) * -1
                    PlusRow("payres1") = "KURANG BAYAR"
                End If
                PlusRow("paytype") = paytype.Text
                PlusRow("trnbelino") = trnbelino.Text
                PlusRow("suppname") = suppname.Text
                PlusRow("amttrans") = ToDouble(amttrans.Text)
                PlusRow("amtpaid") = ToDouble(amtpaid.Text)
                PlusRow("amtretur") = ToDouble(amtretur.Text)
                PlusRow("acctgdesc") = tbSls.Rows(C2)("acctgdesc").ToString
                PlusRow("trntaxpct") = ToDouble(trnTaxPct.Text)
                PlusRow("invCurrOid") = invCurrOid.Text
                PlusRow("invCurrRate") = ToDouble(invCurrRate.Text)
                PlusRow("invCurrCode") = invCurrCode.Text
                PlusRow("invCurrDesc") = invCurrDesc.Text

                If (payflag.SelectedValue = "CASH" Or payflag.SelectedValue = "DP") Then
                    payduedate.Text = Format(Now, "dd/MM/yyyy")
                End If
                PlusRow("payduedate") = toDate(payduedate.Text)
                PlusRow("payrefno") = payrefno.Text
                PlusRow("flagdtl") = "OTHER"

                PlusRow("trndpapoid") = 0
                Dim otableDP As DataTable = Session("acctgoid_dp")
                If otableDP.Rows.Count > 0 Then
                    For c1 As Int16 = 0 To otableDP.Rows.Count - 1
                        If otableDP.Rows(c1).Item("acctgoid") = tbSls.Rows(C2)("acctgoid").ToString Then
                            PlusRow("trndpapoid") = 1
                        End If
                    Next
                End If
                PlusRow("DPAmt") = 0

                objTable.Rows.Add(PlusRow)
            Next
        End If

        ''resequence Detial 
        For C2 As Int16 = 0 To objTable.Rows.Count - 1
            objTable.Rows(C2)("payseq") = C2 + 1
        Next
        Session("invCurrOid") = invCurrOid.Text
        Session("paymentOid") = trnbelimstoid.Text
        'showMessage(Session("paymentOid") & "=>" & Session("invCurrOid"), CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        Session("tbldtl") = objTable

        GVDtlPayAP.DataSource = Session("tbldtl")
        GVDtlPayAP.DataBind()
        ClearDtlAP(True)
        calcTotalInGridDtl()
        GVDtlPayAP.SelectedIndex = -1
    End Sub

    Protected Sub creditsearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles creditsearch.Click
        BinddataGiro()
    End Sub

    Protected Sub gvSupplierX_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplierX.PageIndexChanging
        gvSupplierX.PageIndex = e.NewPageIndex
        gvSupplierX.DataSource = Session("tblgiro")
        gvSupplierX.DataBind()
        hiddenbtn2sX.Visible = True : Panel1X.Visible = True
        ModalPopupExtender3sX.Show()
    End Sub

    Protected Sub gvSupplierX_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvSupplierX.SelectedIndexChanged
        girooid.Text = gvSupplierX.SelectedDataKey("girodtloid").ToString()
        payrefno.Text = gvSupplierX.SelectedDataKey("girono") & " / " & gvSupplierX.SelectedDataKey("NoRekening")
        payduedate.Text = gvSupplierX.SelectedDataKey("GiroDueDate")
        dd_bankgiro.SelectedValue = gvSupplierX.SelectedDataKey("bankoid")
        AmountGiro.Text = ToMaskEdit(gvSupplierX.SelectedDataKey("amount"), 3)
        hiddenbtn2sX.Visible = False : dd_bankgiro.Enabled = False
        dd_bankgiro.Visible = True : Panel1X.Visible = False
        ModalPopupExtender3sX.Hide()
    End Sub

    Protected Sub ibtnSuppIDX_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ibtnSuppIDX.Click
        BinddataGiro()
    End Sub

    Protected Sub imbViewAllsX_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imbViewAllsX.Click
        BinddataGiro()
    End Sub

    Protected Sub CloseSuppX_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseSuppX.Click
        hiddenbtn2sX.Visible = False
        Panel1X.Visible = False
        ModalPopupExtender3sX.Hide()
    End Sub

    Protected Sub gvSupplierX_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvSupplierX.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
        End If
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvSupplier.PageIndex = e.NewPageIndex
        filterGVS("", "", cmpcode, "MANUFACTURE", ddlcabang.SelectedValue)
        hiddenbtn2.Visible = True
        Panel1.Visible = True
        ModalPopupExtender1.Show()
    End Sub

    Protected Sub ddlcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Fill_payflag()
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchasing.SelectedIndexChanged
        chkOther.Checked = False
        trnbelimstoid.Text = gvPurchasing.SelectedDataKey(0).ToString()
        trnbelino.Text = gvPurchasing.SelectedDataKey(1).ToString()
        suppname.Text = gvPurchasing.SelectedDataKey("suppname").ToString()
        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 3)
        ' New Version Get NT Account (Piutang Lain/Piutang Exp) berdasarkan tipe NT
        If gvPurchasing.SelectedDataKey("paytype").ToString.ToUpper = "PAYAREXP" Then
            Dim sVarNT As String = "" : Dim sVarNTCode As String = ""
            sSql = "SELECT typenota FROM ql_trnbiayaeksmst WHERE trnbiayaeksoid=" & trnbelimstoid.Text
            If GetStrData(sSql).ToUpper = "NON EXPEDISI" Then
                sVarNT = "VAR_PIUTANG_LAIN"
            ElseIf GetStrData(sSql).ToUpper = "EXPEDISI" Then
                sVarNT = "VAR_PIUTANG_EXP"
            Else
                showMessage("COA utk Nota No " & gvPurchasing.SelectedDataKey(1).ToString() & " tidak terbaca.", CompnyName & " - Warning", 2, "modalMsgBoxWarn")
                cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, False)
                cProc.DisposeGridView(gvPurchasing)
                Exit Sub
            End If
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" & sVarNT & "' AND interfaceres1='" & ddlcabang.SelectedValue & "'"
            sVarNTCode = GetStrData(sSql)
            sSql = "Select acctgoid from ql_mstacctg Where acctgcode='" & sVarNTCode & "'"
            acctgoid.Text = GetStrData(sSql)
        Else
            acctgoid.Text = gvPurchasing.SelectedDataKey("acctgoid").ToString()
        End If
        APAcc.Text = cKoneksi.ambilscalar("SELECT acctgcode+'-'+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")

        'Dim iconaroid As Int64 = ToDouble(GetStrData("select top 1 conaroid from QL_conar where refoid=" & trnbelimstoid.Text & " and reftype='QL_trnjualmst'  order by conaroid asc"))
        'If ToDouble(acctgoid.Text) = 0 Then
        '    'insert to conar & get acctgoid piutang from mst interface
        '    Dim Var_AR_x As String = GetVarInterface("VAR_AR", cmpcode)
        '    acctgoid.Text = GetStrData("select acctgoid  from ql_mstacctg where acctgcode ='" & Var_AR_x & "'")

        '    'If iconaroid > 0 Then 'update acctgnya aja deh
        '    '    sSql = "update QL_conar set acctgoid=" & acctgoid.Text & " Where cmpcode='" & cmpcode & "' and conaroid=" & iconaroid
        '    '    cKoneksi.ambilscalar(sSql)
        '    'Else
        '    '    iconaroid = GenerateID("QL_conar", cmpcode)
        '    '    sSql = "INSERT INTO QL_conar(cmpcode,conaroid,branch_code,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate, periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnarnote,trnarres1,upduser,updtime) " & _
        '    '              "VALUES ('" & cmpcode & "'," & iconaroid & ",'" & ddlcabang.SelectedValue & "','ql_trnjualmst'," & trnbelimstoid.Text & ",0," & trnsuppoid.Text & ",'" & acctgoid.Text & "','POST','AR','" & CDate(toDate(gvPurchasing.SelectedDataKey("trnbelidate"))) & "','" & GetDateToPeriodAcctg3(CDate(toDate(gvPurchasing.SelectedDataKey("trnbelidate")))) & "',0,'1/1/1900','',0,'" & CDate(toDate(gvPurchasing.SelectedDataKey("payduedate"))) & "'," & ToDouble(amttrans.Text) & ",0,'" & Tchar(cashbankno.text) & "','','" & Session("UserID") & "', CURRENT_TIMESTAMP)"
        '    '    cKoneksi.ambilscalar(sSql)

        '    '    ' update lastoid ql_conar
        '    '    sSql = "update QL_mstoid set lastoid=" & iconaroid & " where tablename ='ql_conar' and cmpcode = '" & cmpcode & "' and branch_code='" & ddlcabang.SelectedValue & "'"
        '    '    cKoneksi.ambilscalar(sSql)
        '    'End If


        '    APAcc.Text = cKoneksi.ambilscalar("SELECT '('+acctgcode+') '+acctgdesc FROM ql_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgoid='" & acctgoid.Text & "'")
        'End If

        invCurrOid.Text = gvPurchasing.SelectedDataKey("currencyoid").ToString()
        invCurrRate.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("currencyrate").ToString()), 3)
        invCurrCode.Text = gvPurchasing.SelectedDataKey("currencycode").ToString()
        invCurrDesc.Text = gvPurchasing.SelectedDataKey("currencycode").ToString() & "-" & gvPurchasing.SelectedDataKey("currencydesc").ToString()
        CekPaymentCurrencyToInvoice()

        amttrans.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amttrans").ToString), 3)
        amtpaid.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtpaid").ToString), 3)
        amtretur.Text = ToMaskEdit(ToDouble(gvPurchasing.SelectedDataKey("amtretur").ToString), 3)
        APAmt.Text = ToMaskEdit(ToDouble(amttrans.Text) - (ToDouble(amtpaid.Text) + ToDouble(amtretur.Text)), 3)
        If payflag.SelectedValue = "DP" Then
            amtpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 3)
        Else
            amtpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 3)
        End If
        paytype.Text = gvPurchasing.SelectedDataKey("paytype").ToString
        totalpayment.Text = ToMaskEdit(ToDouble(APAmt.Text), 3)
        Dim exp As String = cKoneksi.ambilscalar("SELECT count(-1) FROM ql_trnbiayaeksmst WHERE cmpcode='" & cmpcode & "' AND trnbiayaeksoid=" & trnbelimstoid.Text)
        If exp = 0 Then
            initddlDP(cKoneksi.ambilscalar("SELECT trncustoid FROM QL_trnjualmst WHERE cmpcode='" & cmpcode & "' AND trnjualmstoid=" & trnbelimstoid.Text))
        End If
        txtInputNotaBeli.Text = ""
        CalculateTotalPayment()
        cProc.SetModalPopUpExtender(hiddenbtnpur, Panel2, ModalPopupExtender2, False)
        cProc.DisposeGridView(gvPurchasing)
    End Sub

    Protected Sub btnPrintNya_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cashbankno As String = sender.Tooltip
        sSql = "SELECT count(-1) FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & cashbankno.ToString & "' and cashbankstatus in ('POST')"
        If cKoneksi.ambilscalar(sSql) <= 0 Then
            showMessage("Data harus di posting terlebih dahulu sebelum di cetak !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        sSql = "SELECT cashbanktype FROM QL_trncashbankmst WHERE cmpcode='" & cmpcode & "' AND cashbankno='" & cashbankno.ToString & "'"
        PrintReport(cashbankno, cKoneksi.ambilscalar(sSql))

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sWhere As String = "", status As String = "", sMsg As String = ""
        Dim st1, st2 As Boolean

        If IsDate(toDate(txtPeriode1.Text)) Then : st1 = True
        Else : st1 = False : sMsg &= "- Format Periode  salah!!<BR>" : End If
        If IsDate(toDate(txtPeriode2.Text)) Then : st2 = True
        Else : st2 = False : sMsg &= "- Format Periode  salah!!<BR>" : End If

        If st1 And st2 Then
            If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
                sMsg &= "- Last periode must be >= first periode  !!<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If postinge.SelectedValue <> "ALL" Then
            sWhere &= " AND cashbankstatus = '" & postinge.SelectedValue.ToUpper & "'"
        End If

        If cbPeriode.Checked = True Then
            sWhere &= " AND cashbankdate>='" & CDate(toDate(txtPeriode1.Text)) & "' And cashbankdate<='" & CDate(toDate(txtPeriode2.Text)) & "'"
        End If

        If FilterText.Text <> "" Then
            If ddlFilter.SelectedValue = "b.trnjualno" Then
                sWhere &= " AND cb.cashbankoid in (select pay.cashbankoid from QL_trnjualmst b INNER JOIN QL_trnpayar pay ON pay.payrefoid = b.trnjualmstoid and pay.branch_code = b.branch_code where b.trnjualno like '%" & Tchar(FilterText.Text.Trim) & "%' UNION ALL select pay.cashbankoid from ql_trnbiayaeksmst b INNER JOIN QL_trnpayar pay ON pay.payrefoid = b.trnbiayaeksoid and pay.branch_code = b.branch_code where b.trnbiayaeksno like '%" & Tchar(FilterText.Text.Trim) & "%')"
            Else
                sWhere &= " AND " & ddlFilter.SelectedValue & " LIKE '%" & FilterText.Text.Trim & "%'"
            End If
        End If
        If postinge.SelectedValue <> "ALL" Then
            sWhere &= " AND cashbankstatus = '" & postinge.SelectedValue & "'"
        End If
        GVmstPAYAP.PageIndex = 0
        BindData(sWhere)
    End Sub
#End Region
    
End Class
