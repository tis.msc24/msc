Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptTSupplier
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#Region "Procedure"
    Public Sub initddl()
        sSql = "select  periodacctg,periodacctg from ql_pomst"
        FillDDL(period, sSql)

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'RETUR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' UNION ALL SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'SUPPLIER' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'"
        FillDDL(ddlLocation, sSql)
        ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
        ddlLocation.SelectedValue = "ALL LOCATION"

        sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'"
        FillDDL(ddlLocation1, sSql)
        ddlLocation1.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
        ddlLocation1.SelectedValue = "ALL LOCATION"

        '---------------------------------------------------------------------------------------
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GRUP", "ALL GRUP"))
        FilterDDLGrup.SelectedValue = "ALL GRUP"
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GRUP", "ALL SUB GRUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GRUP"
    End Sub

    Public Sub showPrintReport()
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If


        Try
            Dim swhere As String = ""
            vReport = New ReportDocument

            vReport.Load(Server.MapPath("~\Report\rptTSupplier.rpt"))
            swhere = " where a.trntrfdate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and a.trnTrfFromReturoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, "  and  c.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_from=" & ddlLocation.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_to=" & ddlLocation1.SelectedValue & " ") & " and a.trnTrfFromReturNo like '%" & Tchar(nota.Text) & "%' and c.itemdesc like '%" & Tchar(itemname.Text) & "%'" & IIf(flag.SelectedValue.ToString = "All", "", " and a.flag = '" & flag.SelectedValue.ToString & "'")

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("swhere", swhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            crvMutasiStock.DisplayGroupTree = False
            crvMutasiStock.ReportSource = vReport
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Public Sub showPrint()
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If


        Try
            Dim swhere As String = ""
            vReport = New ReportDocument

            vReport.Load(Server.MapPath("~\Report\rptTSupplier.rpt"))
            swhere = " where a.trntrfdate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and a.trnTrfFromReturoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, "  and  c.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_from=" & ddlLocation.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_to=" & ddlLocation1.SelectedValue & " ") & " and a.trnTrfFromReturNo like '%" & Tchar(nota.Text) & "%' and c.itemdesc like '%" & Tchar(itemname.Text) & "%'" & IIf(flag.SelectedValue.ToString = "All", "", " and a.flag = '" & flag.SelectedValue.ToString & "'")

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("swhere", swhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()

            vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TS_" & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptTSupplier.aspx?awal=true")
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal msg As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        lblCaption.Text = caption
        lblValidasi.Text = msg
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEErrMsg.Show()
    End Sub

    Public Sub showPrintExcel()
        lblkonfirmasi.Text = ""
        Session("diprint") = "False"

        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If


        Try
            Dim swhere As String = ""
            vReport = New ReportDocument

            vReport.Load(Server.MapPath("~\Report\rptTSupplier.rpt"))
            swhere = " where a.trntrfdate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "'" & IIf(oid.Text.Trim = "", " ", " and a.trnTrfFromReturoid = '" & oid.Text & "' ") & IIf(ToDouble(itemoid.Text) > 0, "  and  c.itemoid=" & itemoid.Text, "  ") & IIf(ddlLocation.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_from=" & ddlLocation.SelectedValue & " ") & IIf(ddlLocation1.SelectedValue = "ALL LOCATION", "  ", " and b.mtrlocoid_to=" & ddlLocation1.SelectedValue & " ") & " and a.trnTrfFromReturNo like '%" & Tchar(nota.Text) & "%' and c.itemdesc like '%" & Tchar(itemname.Text) & "%'" & IIf(flag.SelectedValue.ToString = "All", "", " and a.flag = '" & flag.SelectedValue.ToString & "'")

            cProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            vReport.SetParameterValue("swhere", swhere)
            vReport.SetParameterValue("periode1", dateAwal.Text)
            vReport.SetParameterValue("periode2", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Session("diprint") = "True"

            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()

            vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "TS_" & Format(GetServerTime, "dd_MM_yy"))
            vReport.Close() : vReport.Dispose()
            Response.Redirect("~\ReportForm\rptTSupplier.aspx?awal=true")

        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Exit Sub
        End Try
    End Sub

    Public Sub BindDataListItem()
        sSql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc, " & _
        "ql_mstitem.itempriceunit1,ql_mstitem.itempriceunit2," & _
        "ql_mstitem.itempriceunit3,ql_mstitem.itemoid, g.gendesc satuan1," & _
        "g2.gendesc satuan2, g3.gendesc satuan3,konversi1_2, " & _
        "konversi2_3 from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
        "and itemflag='AKTIF' inner join ql_mstgen g2 on g2.genoid=satuan2  " & _
        "inner join ql_mstgen g3 on g3.genoid=satuan3  " & _
        "where itemdesc like '%" & Tchar(itemname.Text) & "%' or ql_mstitem.itemcode like '%" & Tchar(itemname.Text) & "%'"
        FillGV(gvItem, sSql, "ql_mstitem")
        gvItem.Visible = True
    End Sub

    Public Sub bindDataListPO()
        If dateAwal.Text.Trim <> "" And dateAkhir.Text.Trim <> "" Then
            Try
                Dim dDate1 As Date = CDate(toDate(dateAwal.Text))
                Dim dDate2 As Date = CDate(toDate(dateAkhir.Text))
                If dDate1 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 1 is invalid!" : Exit Sub
                End If
                If dDate2 < CDate("01/01/1900") Then
                    lblkonfirmasi.Text = "Period 2 is invalid!" : Exit Sub
                End If
                If dDate1 > dDate2 Then
                    lblkonfirmasi.Text = "Period 2 must be more than Period 1!" : Exit Sub
                End If
            Catch ex As Exception
                lblkonfirmasi.Text = "Period report is invalid!" : Exit Sub
            End Try
        Else
            lblkonfirmasi.Text = "Please fill period report first!" : Exit Sub
        End If


        sSql = "select top 200 trntrffromreturoid trntrftoreturoid, trntrffromreturno trntrftoreturno from ql_trntrffromreturmst where trntrffromreturno like '%" & Tchar(nota.Text) & "%' and trntrfdate between '" & CDate(toDate(dateAwal.Text)) & "' and '" & CDate(toDate(dateAkhir.Text)) & "' order by trntrffromreturno desc"
        FillGV(GVNota, sSql, "ql_trntrfmtrmst")
        GVNota.Visible = True
    End Sub
#End Region

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindDataListItem()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        itemname.Text = ""
        itemoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch") = branch
            Session("branch_id") = branch_id
            Response.Redirect("~\ReportForm\rptTSupplier.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Transfer Gudang Supplier"
        If ckon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            'Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrintReport()
            End If
        Else
            initddl()
            dateAwal.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
            dateAkhir.Text = Format(Now, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Session("diprint") = "True"
        showPrint()
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptTSupplier.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'showPrintExcel("ListofStock")
        Session("diprint") = "False"
        showPrintExcel()
    End Sub

    Protected Sub btnSearchNota_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        bindDataListPO()
    End Sub

    Protected Sub gvNota_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVNota.PageIndexChanging
        GVNota.PageIndex = e.NewPageIndex
        bindDataListPO()
    End Sub

    Protected Sub EraseNota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        nota.Text = ""
        oid.Text = ""
        GVNota.Visible = False
    End Sub

    Protected Sub GVNota_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        crvMutasiStock.ReportSource = Nothing
        Session("diprint") = "False"
        nota.Text = GVNota.SelectedDataKey.Item("trntrftoreturno")
        oid.Text = GVNota.SelectedDataKey.Item("trntrftoreturoid")
        GVNota.Visible = False
    End Sub

    Protected Sub flag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles flag.SelectedIndexChanged
        If flag.SelectedValue = "All" Then
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'RETUR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "' UNION ALL SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'SUPPLIER' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        ElseIf flag.SelectedValue = "Purchase" Then
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'RETUR' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        ElseIf flag.SelectedValue = "Supplier" Then
            sSql = "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'SUPPLIER' AND a.gengroup = 'LOCATION' AND b.gengroup = 'WAREHOUSE' AND a.cmpcode='" & CompnyCode & "'"
            FillDDL(ddlLocation, sSql)
            ddlLocation.Items.Add(New ListItem("ALL LOCATION", "ALL LOCATION"))
            ddlLocation.SelectedValue = "ALL LOCATION"
        End If
    End Sub

    Protected Sub btnreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreport.Click
        Session("diprint") = "True"
        showPrintReport()
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
    End Sub
End Class
