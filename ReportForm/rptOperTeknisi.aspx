<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptOperTeknisi.aspx.vb" Inherits="ReportForm_rptOperTeknisi" title="MSC" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 984px; height: 40px">
        <tr>
            <td align="left" style="height: 45px; background-color: silver">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Maroon"
                    Text=":: Laporan Oper Teknisi"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE style="WIDTH: 616px; HEIGHT: 32px"><TBODY><TR><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DDLcabang" runat="server" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD></TR><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem Value="reqcode">No. Tanda Terima</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="barcode">Barcode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" __designer:wfdid="w2"></asp:TextBox>&nbsp;</TD></TR><TR><TD style="HEIGHT: 17px">Periode</TD><TD style="HEIGHT: 17px">:</TD><TD><asp:TextBox id="tgl1" runat="server" CssClass="inpText" __designer:wfdid="w3"></asp:TextBox>&nbsp;<asp:ImageButton id="cal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton> to <asp:TextBox id="tgl2" runat="server" CssClass="inpText" __designer:wfdid="w5"></asp:TextBox>&nbsp;<asp:ImageButton id="cal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w6"></asp:ImageButton> <asp:Label id="Label2" runat="server" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w7"></asp:Label></TD></TR><TR><TD style="HEIGHT: 22px"><asp:CheckBox id="cbStatus" runat="server" Text="Status" __designer:wfdid="w8"></asp:CheckBox></TD><TD>:</TD><TD style="HEIGHT: 22px"><asp:DropDownList id="DDLStatus" runat="server" CssClass="inpText" __designer:wfdid="w9"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>Process</asp:ListItem>
<asp:ListItem>Switch</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><asp:ImageButton id="btnView" onclick="btnView_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w11" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w12" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 22px"></TD><TD></TD><TD style="HEIGHT: 22px"></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><BR /><CR:CrystalReportViewer id="crvOper" runat="server" __designer:wfdid="w14" HasViewList="False" HasToggleGroupTreeButton="False" HasSearchButton="False" HasPrintButton="False" HasGotoPageButton="False" HasExportButton="False" HasDrillUpButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer></TD></TR><TR><TD style="HEIGHT: 22px" colSpan=3><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w15" TargetControlID="tgl1" PopupButtonID="cal1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w16" TargetControlID="tgl2" PopupButtonID="cal2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w17" TargetControlID="tgl1" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w18" TargetControlID="tgl2" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE>
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnView"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnCancel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel><br />
                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                    <contenttemplate>
<asp:Panel id="panelMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE style="WIDTH: 99%; HEIGHT: 109%"><TR><TD style="HEIGHT: 18px; BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD align=center><asp:ImageButton id="imIcon" runat="server" ImageUrl="~/Images/warn.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:ImageButton id="btnErrorOK" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" TargetControlID="btnMsg" PopupDragHandleControlID="lblCaption" PopupControlID="panelMsg" DropShadow="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>&nbsp;&nbsp; <asp:Button id="btnMsg" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

