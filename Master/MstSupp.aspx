<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="MstSupp.aspx.vb" Inherits="Master_MstSupp" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}

// ]]>
</script>

    <table style="height: 56px" width="100%" id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" class="header" style="background-color: silver;" valign="top">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Data Supplier" Width="247px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" valign="top">
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <HeaderTemplate>
                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                ::
                <strong><span style="font-size: 9pt"> List Of Supplier&nbsp;</span></strong>
            </HeaderTemplate>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            Filter :</td>
                        <td colspan="5">
                            <asp:DropDownList ID="FilterDDL" runat="server" CssClass="inpText">
                                <asp:ListItem Value="s.suppname">Nama</asp:ListItem>
                                <asp:ListItem Value="s.suppcode">Code</asp:ListItem>
                                <asp:ListItem Value="s.suppaddr">Alamat</asp:ListItem>
                                <asp:ListItem Value="Gendesc">Kota</asp:ListItem>
                                <asp:ListItem Value="suppphone1" Enabled="False">Telepon</asp:ListItem>
                                <asp:ListItem Value="suppType">Type</asp:ListItem>
                                <asp:ListItem Value="contactperson1" Enabled="False">CP 1</asp:ListItem>
                                <asp:ListItem Value="contactperson2" Enabled="False">CP 2</asp:ListItem>
                                <asp:ListItem Value="notes" Enabled="False">Catatan</asp:ListItem>
                                <asp:ListItem Value="oldsuppcode" Enabled="False">Old Code</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="FilterText" runat="server" CssClass="inpText" Width="152px"></asp:TextBox>
                            <asp:ImageButton ID="btnFind" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                            <asp:ImageButton ID="btnView" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />
                            <asp:ImageButton ID="imbPrint" runat="server" ImageAlign="AbsMiddle"
                                ImageUrl="~/Images/print.png" /></td>
                    </tr>
                    <tr>
                        <td>
                            Status :</td>
                        <td colspan="5">
                            <asp:DropDownList ID="Statusddl" runat="server" CssClass="inpText" Height="20px">
                                <asp:ListItem>Active</asp:ListItem>
                                <asp:ListItem Value="Inactive">In Active</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                <div style="width: 984px; height: 224px">
                    <asp:GridView ID="GVSupp" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" Width="100%" CellPadding="4" Font-Overline="False" Font-Strikeout="False" ForeColor="#333333" HorizontalAlign="Left" PageSize="8" EnableModelValidation="True" GridLines="None">
                        <Columns>
                            <asp:HyperLinkField DataNavigateUrlFields="suppoid" DataNavigateUrlFormatString="~/Master/MstSupp.aspx?oid={0}"
                                DataTextField="suppcode" HeaderText="Code" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:HyperLinkField>
                            <asp:BoundField DataField="oldsuppcode" HeaderText="Old Code" Visible="False">
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="suppname" HeaderText="Nama" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="suppaddr" HeaderText="Alamat" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="kota" HeaderText="Kota" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SuppPhone1" HeaderText="Telepon" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="supptype" HeaderText="Type" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="contactperson1" HeaderText="CP 1" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="contactperson2" HeaderText="CP 2" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SUPPFLAG" HeaderText="Status">
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="notes" HeaderText="Catatan" >
                                <HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                        </Columns>
                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                        <FooterStyle BackColor="#990000" BorderColor="#8080FF" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" Font-Bold="True" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        <EmptyDataTemplate>
                            <asp:Label ID="Label30" runat="server" ForeColor="Red" Text="Data Not Found"></asp:Label>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                            <asp:Label ID="lblsupp" runat="server"></asp:Label></td>
                    </tr>
                </table>
                
  
            </ContentTemplate>
            

        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                        <asp:UpdatePanel id="UpdatePanel1" runat="server">
                            <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD><asp:Label id="i_u" runat="server" ForeColor="Red" Visible="False" __designer:wfdid="w2"></asp:Label></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR><TR><TD style="HEIGHT: 22px">Code <asp:Label id="Label2" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w7"></asp:Label></TD><TD style="HEIGHT: 22px"><asp:TextBox id="txtId" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w8" Enabled="False" MaxLength="10"></asp:TextBox> <asp:TextBox id="suppoid" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w9" MaxLength="20">suppoid</asp:TextBox></TD><TD style="WIDTH: 72px; HEIGHT: 22px">Nama <asp:Label id="Label3" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w10"></asp:Label>&nbsp;</TD><TD style="HEIGHT: 22px"><asp:DropDownList id="prefixsupp" runat="server" CssClass="inpText" __designer:wfdid="w11"></asp:DropDownList>&nbsp;<asp:TextBox id="txtNama" runat="server" Width="171px" CssClass="inpText" __designer:wfdid="w12" MaxLength="100" AutoPostBack="True"></asp:TextBox></TD><TD style="HEIGHT: 22px"><asp:Label id="Label5" runat="server" Text="Type" __designer:wfdid="w13"></asp:Label></TD><TD style="HEIGHT: 22px"><asp:DropDownList id="ddlType" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w14" Enabled="False"><asp:ListItem>GROSIR</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD>NPWP</TD><TD><asp:TextBox id="txtNPWP" runat="server" CssClass="inpText" __designer:wfdid="w15" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 72px">Alamat <asp:Label id="Label36" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w16"></asp:Label></TD><TD><asp:TextBox id="txtAddress" runat="server" CssClass="inpText" __designer:wfdid="w17" MaxLength="100"></asp:TextBox></TD><TD>Kode Pos</TD><TD><asp:TextBox id="txtPostCode" runat="server" CssClass="inpText" __designer:wfdid="w18" MaxLength="10"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 21px">Propinsi</TD><TD style="HEIGHT: 21px"><asp:DropDownList id="ddlProvince" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w19" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 72px; HEIGHT: 21px">Kota <asp:Label id="Label34" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w20"></asp:Label></TD><TD style="HEIGHT: 21px"><asp:DropDownList id="ddlCity" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w21" AutoPostBack="True"></asp:DropDownList></TD><TD style="HEIGHT: 21px">Negara</TD><TD style="HEIGHT: 21px"><asp:DropDownList id="ddlCountry" runat="server" CssClass="inpText" __designer:wfdid="w22" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD>Telepon&nbsp;1 <asp:Label id="Label35" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w23"></asp:Label></TD><TD><asp:TextBox id="phone1" runat="server" CssClass="inpText" __designer:wfdid="w24" MaxLength="15"></asp:TextBox>&nbsp;<asp:TextBox id="phoneintcode1" runat="server" Width="24px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w25" AutoPostBack="True" ReadOnly="True"></asp:TextBox><asp:TextBox id="phonelocalcode1" runat="server" Width="24px" CssClass="inpTextDisabled" Visible="False" __designer:wfdid="w26" AutoPostBack="True" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 72px">Telepon&nbsp;2</TD><TD><asp:TextBox id="phone2" runat="server" CssClass="inpText" __designer:wfdid="w27" MaxLength="15"></asp:TextBox>&nbsp;<asp:TextBox id="phoneintcode2" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w28" MaxLength="5"></asp:TextBox><asp:TextBox id="phonelocalcode2" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w29" MaxLength="5"></asp:TextBox></TD><TD>Telepon&nbsp;3</TD><TD><asp:TextBox id="phone3" runat="server" CssClass="inpText" __designer:wfdid="w30" MaxLength="15"></asp:TextBox> <asp:TextBox id="phoneintcode3" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w31" MaxLength="5"></asp:TextBox> <asp:TextBox id="phonelocal3" runat="server" Width="16px" CssClass="inpText" Visible="False" __designer:wfdid="w32" MaxLength="5"></asp:TextBox></TD></TR><TR><TD>Fax 1</TD><TD><asp:TextBox id="suppfax1" runat="server" CssClass="inpText" __designer:wfdid="w33" MaxLength="15"></asp:TextBox> <asp:TextBox id="faxintcode1" runat="server" Width="32px" CssClass="inpText" Visible="False" __designer:wfdid="w34" MaxLength="5"></asp:TextBox> <asp:TextBox id="faxlocalcode1" runat="server" Width="32px" CssClass="inpText" Visible="False" __designer:wfdid="w35" MaxLength="5"></asp:TextBox></TD><TD style="WIDTH: 72px">Fax 2</TD><TD><asp:TextBox id="suppfax2" runat="server" Width="112px" CssClass="inpText" __designer:wfdid="w36" MaxLength="15"></asp:TextBox> <asp:TextBox id="faxintcode2" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w37" MaxLength="5"></asp:TextBox> <asp:TextBox id="faxlocalcode2" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w38" MaxLength="5"></asp:TextBox></TD><TD>Email</TD><TD><asp:TextBox id="suppemail" runat="server" CssClass="inpText" __designer:wfdid="w39" MaxLength="100"></asp:TextBox></TD></TR><TR><TD>Contact Person 1 <asp:Label id="Label37" runat="server" Width="8px" CssClass="Important" ForeColor="Red" Text="*" __designer:wfdid="w40"></asp:Label></TD><TD><asp:DropDownList id="ddltitle" runat="server" CssClass="inpText" __designer:wfdid="w41"></asp:DropDownList>&nbsp;<asp:TextBox id="conper1" runat="server" CssClass="inpText" __designer:wfdid="w42" MaxLength="25"></asp:TextBox></TD><TD style="WIDTH: 85px">Telepon&nbsp;Contact Person 1</TD><TD><asp:TextBox id="phoneconper1" runat="server" CssClass="inpText" __designer:wfdid="w43" MaxLength="15"></asp:TextBox>&nbsp;<asp:DropDownList id="suppdefaultcurroid" runat="server" Width="32px" CssClass="inpText" Visible="False" __designer:wfdid="w44"></asp:DropDownList></TD><TD>Website</TD><TD><asp:TextBox id="suppweb" runat="server" CssClass="inpText" __designer:wfdid="w45" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 12px">Contact Person 2</TD><TD><asp:DropDownList id="ddltitle2" runat="server" CssClass="inpText" __designer:wfdid="w46"></asp:DropDownList>&nbsp;<asp:TextBox id="conper2" runat="server" CssClass="inpText" __designer:wfdid="w47" MaxLength="25"></asp:TextBox>&nbsp;<asp:DropDownList id="suppcreditlimitcurroid" runat="server" Width="32px" CssClass="inpText" Visible="False" __designer:wfdid="w48"></asp:DropDownList></TD><TD style="WIDTH: 85px; HEIGHT: 12px">Telepon&nbsp;Contact&nbsp; Person 2</TD><TD style="HEIGHT: 12px"><asp:TextBox id="phonecontactperson2" runat="server" CssClass="inpText" __designer:wfdid="w49" MaxLength="15"></asp:TextBox> <asp:TextBox id="suppcreditlimit" runat="server" Width="24px" CssClass="inpText" Visible="False" __designer:wfdid="w50" MaxLength="20"></asp:TextBox></TD><TD style="HEIGHT: 12px"><asp:Label id="Label4" runat="server" Text="Payment Term" __designer:wfdid="w51"></asp:Label></TD><TD style="HEIGHT: 12px"><asp:DropDownList id="supppaymenttermdefaultoid" runat="server" CssClass="inpText" __designer:wfdid="w52"></asp:DropDownList></TD></TR><TR><TD>Status</TD><TD><asp:DropDownList id="ddlStatus" runat="server" Width="104px" CssClass="inpText" __designer:wfdid="w53"><asp:ListItem>Active</asp:ListItem>
<asp:ListItem>Inactive</asp:ListItem>
<asp:ListItem>Suspended</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 85px">Catatan</TD><TD><asp:TextBox id="suppnote" runat="server" Width="248px" Height="48px" CssClass="inpText" __designer:wfdid="w54" MaxLength="50" TextMode="MultiLine"></asp:TextBox></TD><TD><asp:Label id="Label31" runat="server" ForeColor="Red" Text="Maks. 40 karakter" __designer:wfdid="w55"></asp:Label></TD><TD></TD></TR><TR><TD style="HEIGHT: 12px"><asp:Label id="Label32" runat="server" Width="124px" Text="A/P Account" Visible="False" __designer:wfdid="w20"></asp:Label></TD><TD><asp:DropDownList id="apaccount" runat="server" Width="200px" CssClass="inpText" Visible="False" __designer:wfdid="w57"></asp:DropDownList></TD><TD style="WIDTH: 133px; HEIGHT: 12px"><asp:Label id="Label33" runat="server" Width="153px" Text="Retur Account" Visible="False" __designer:wfdid="w20"></asp:Label></TD><TD><asp:DropDownList id="returaccount" runat="server" Width="200px" CssClass="inpText" Visible="False" __designer:wfdid="w59"></asp:DropDownList></TD><TD style="HEIGHT: 12px"><asp:DropDownList id="suppacctgoid" runat="server" Width="120px" CssClass="inpText" Visible="False" __designer:wfdid="w84"></asp:DropDownList></TD><TD style="HEIGHT: 12px"><asp:DropDownList id="ddlGroup" runat="server" Width="168px" CssClass="inpText" Visible="False" __designer:wfdid="w85"></asp:DropDownList></TD></TR><TR><TD colSpan=6>Create On&nbsp;<asp:Label id="CreateTime" runat="server" Font-Bold="True" __designer:dtid="281474976710807" __designer:wfdid="w101"></asp:Label>&nbsp;by&nbsp; <asp:Label id="CreateUser" runat="server" Font-Bold="True" __designer:dtid="281474976710807" __designer:wfdid="w96"></asp:Label></TD></TR><TR><TD colSpan=6><asp:Label id="lblUpd" runat="server" __designer:dtid="281474976710806" __designer:wfdid="w95"></asp:Label> On <asp:Label id="UpdTime" runat="server" Font-Bold="True" __designer:dtid="281474976710807" __designer:wfdid="w96"></asp:Label> &nbsp;by&nbsp; <asp:Label id="UpdUser" runat="server" Font-Bold="True" __designer:dtid="281474976710808" __designer:wfdid="w97"></asp:Label></TD></TR><TR><TD colSpan=6><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710811" __designer:wfdid="w98"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710812" __designer:wfdid="w99"></asp:ImageButton> &nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:dtid="281474976710813" Visible="False" __designer:wfdid="w100"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=6><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w103" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w104"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD colSpan=6><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="txtNPWP" __designer:wfdid="w80" MaskType="Number" Mask="99.999.999.9-999.999" AcceptNegative="Right"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="fteConPer1" runat="server" TargetControlID="phoneconper1" __designer:wfdid="w81" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteIntCode2" runat="server" TargetControlID="phoneintcode2" __designer:wfdid="w3" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteLocal3" runat="server" TargetControlID="phonelocal3" __designer:wfdid="w5" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftePhone2" runat="server" TargetControlID="phone2" __designer:wfdid="w6" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteLocalCode2" runat="server" TargetControlID="phonelocalcode2" __designer:wfdid="w4" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteConPer2" runat="server" TargetControlID="phonecontactperson2" __designer:wfdid="w82" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="fteSuppPostCode" runat="server" TargetControlID="txtPostCode" __designer:wfdid="w83" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftePhone1" runat="server" TargetControlID="phone1" __designer:wfdid="w64" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftesf1" runat="server" TargetControlID="suppfax1" __designer:wfdid="w65" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FTEsf2" runat="server" TargetControlID="suppfax2" __designer:wfdid="w66" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="mee" runat="server" TargetControlID="suppcreditlimit" __designer:wfdid="w67" MaskType="Number" Mask="999,999,999,999.99"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteFaxInt1" runat="server" TargetControlID="faxintcode1" __designer:wfdid="w70" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteFaxLocal1" runat="server" TargetControlID="faxlocalcode1" __designer:wfdid="w71" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteSuppFax1" runat="server" TargetControlID="suppfax1" __designer:wfdid="w72" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteFaxInt2" runat="server" TargetControlID="faxintcode2" __designer:wfdid="w73" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftePhoneInt3" runat="server" TargetControlID="phoneintcode3" __designer:wfdid="w79" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteFaxLocal2" runat="server" TargetControlID="faxlocalcode2" __designer:wfdid="w68" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftePhoneLocCod1" runat="server" TargetControlID="phonelocalcode1" __designer:wfdid="w74" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="ftePhone3" runat="server" TargetControlID="phone3" __designer:wfdid="w69" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteSuppFax2" runat="server" TargetControlID="suppfax2" __designer:wfdid="w75" ValidChars="1234567890"></ajaxToolkit:FilteredTextBoxExtender> <asp:HiddenField id="hfSuppId" runat="server" __designer:wfdid="w76"></asp:HiddenField><asp:SqlDataSource id="SqlAcctgOid" runat="server" __designer:dtid="281474976710664" __designer:wfdid="w86" SelectCommand="SELECT [acctgoid], [acctgcode], [acctgdesc] FROM [QL_mstacctg]" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>"></asp:SqlDataSource><asp:SqlDataSource id="SqlSuppType" runat="server" __designer:dtid="281474976710665" __designer:wfdid="w87" SelectCommand="SELECT genoid, gendesc FROM QL_mstgen WHERE (gengroup = 'SUPPTYPE')" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                </asp:SqlDataSource><asp:SqlDataSource id="SqlGroup" runat="server" __designer:dtid="281474976710666" __designer:wfdid="w88" SelectCommand="SELECT DISTINCT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                    <SelectParameters __designer:dtid="281474976710667">
                        <asp:Parameter __designer:dtid="281474976710668" DefaultValue="SUPPGROUP" Name="gengroup"  />
                    </SelectParameters>
                </asp:SqlDataSource><asp:SqlDataSource id="SqlCity" runat="server" __designer:dtid="281474976710669" __designer:wfdid="w89" SelectCommand="SELECT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, upduser, updtime FROM QL_mstgen WHERE (gengroup = @gengroup)" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                    <SelectParameters __designer:dtid="281474976710670">
                        <asp:Parameter __designer:dtid="281474976710671" DefaultValue="City" Name="gengroup"  />
                    </SelectParameters>
                </asp:SqlDataSource><asp:SqlDataSource id="SqlProvince" runat="server" __designer:dtid="281474976710672" __designer:wfdid="w90" SelectCommand="SELECT DISTINCT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2, upduser, updtime FROM QL_mstgen WHERE (gengroup = @gengroup)" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                    <SelectParameters __designer:dtid="281474976710673">
                        <asp:Parameter __designer:dtid="281474976710674" DefaultValue="Province" Name="gengroup"  />
                    </SelectParameters>
                </asp:SqlDataSource><asp:SqlDataSource id="SqlCurrency" runat="server" __designer:dtid="281474976710675" __designer:wfdid="w91" SelectCommand="SELECT DISTINCT cmpcode, currencyoid, currencycode, currencydesc FROM QL_mstcurr" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                </asp:SqlDataSource><asp:SqlDataSource id="SqlCountry" runat="server" __designer:dtid="281474976710676" __designer:wfdid="w92" SelectCommand="SELECT DISTINCT cmpcode, genoid, gencode, gendesc, gengroup, genother1, genother2 FROM QL_mstgen WHERE (gengroup = @gengroup)" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                    <SelectParameters __designer:dtid="281474976710677">
                        <asp:Parameter __designer:dtid="281474976710678" DefaultValue="Country" Name="gengroup"  />
                    </SelectParameters>
                </asp:SqlDataSource><asp:SqlDataSource id="SqlSupp" runat="server" __designer:dtid="281474976710679" __designer:wfdid="w93" SelectCommand="SELECT cmpcode, suppoid, suppcode, suppname, supptype, suppacctgoid, suppflag, suppgroupoid, suppaddr, suppcityoid, suppprovoid, suppcountryoid, suppphone1, suppphone2, suppphone3, suppfax1, suppfax2, suppemail, suppwebsite, suppnpwp, supppostcode, upduser, updtime FROM QL_mstsupp" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                </asp:SqlDataSource><asp:SqlDataSource id="SDSDataView" runat="server" __designer:dtid="281474976710680" __designer:wfdid="w94" SelectCommand="SELECT DISTINCT QL_mstsupp.cmpcode, QL_mstsupp.suppoid, QL_mstsupp.suppcode, QL_mstsupp.suppname, QL_mstsupp.supptype, QL_mstsupp.suppacctgoid, QL_mstsupp.suppflag, QL_mstsupp.suppgroupoid, QL_mstsupp.suppaddr, QL_mstsupp.suppcityoid, QL_mstsupp.suppprovoid, QL_mstsupp.suppcountryoid, QL_mstsupp.suppphone1, QL_mstsupp.suppphone2, QL_mstsupp.suppphone3, QL_mstsupp.suppfax1, QL_mstsupp.suppfax2, QL_mstsupp.suppemail, QL_mstsupp.suppwebsite, QL_mstsupp.suppnpwp, QL_mstsupp.supppostcode, QL_mstsupp.upduser, QL_mstsupp.updtime, QL_mstgen.gendesc AS type FROM QL_mstsupp  WHERE (QL_mstsupp.cmpcode = @CMPCODE) AND (QL_mstsupp.suppcode LIKE @SUPPCODE) AND (QL_mstsupp.suppaddr LIKE @SUPPADDR) AND (QL_mstsupp.suppflag LIKE @SUPPFLAG) AND (QL_mstsupp.suppname LIKE @SUPPNAME) AND (QL_mstsupp.suppphone1 LIKE @SUPPPHONE1) AND (QL_mstsupp.suppfax1 LIKE @SUPPFAX1) AND (QL_mstsupp.upduser LIKE @Upduser)  ORDER BY QL_mstsupp.suppcode" ConnectionString="<%$ ConnectionStrings:QL_JPTConnectionString %>">
                    <SelectParameters __designer:dtid="281474976710681">
                        <asp:Parameter __designer:dtid="281474976710682" Name="CMPCODE"  />
                        <asp:Parameter __designer:dtid="281474976710683" Name="SUPPCODE"  />
                        <asp:Parameter __designer:dtid="281474976710684" Name="SUPPADDR"  />
                        <asp:Parameter __designer:dtid="281474976710685" Name="SUPPFLAG"  />
                        <asp:Parameter __designer:dtid="281474976710686" Name="SUPPNAME"  />
                        <asp:Parameter __designer:dtid="281474976710687" Name="SUPPPHONE1"  />
                        <asp:Parameter __designer:dtid="281474976710688" Name="SUPPFAX1"  />
                        <asp:Parameter __designer:dtid="281474976710689" Name="Upduser"  />
                     </SelectParameters>
                </asp:SqlDataSource></TD></TR></TBODY></TABLE>
</contenttemplate>
                        </asp:UpdatePanel>
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="viewName" runat="server">
                        </asp:View>
                    <asp:View ID="viewShippingAddr" runat="server">
                        <asp:Label ID="lblInfo" runat="server" CssClass="submenu" Font-Size="Small" Text="Information"></asp:Label><asp:Label
                            ID="Label6" runat="server" ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label
                                ID="Label7" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Black"
                                Text="Shipping Address" Width="133px"></asp:Label><asp:Label ID="Label8" runat="server"
                                    ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label ID="lblCP"
                                        runat="server" CssClass="submenu" Font-Size="Small" Text="Contact Person"></asp:Label><asp:Label
                                            ID="Label10" runat="server" ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label
                                                ID="lblBank" runat="server" CssClass="submenu" Font-Size="Small" Text="Bank Account"></asp:Label><asp:UpdatePanel
                                                    id="UpdatePanel2" runat="server"><contenttemplate>
<TABLE style="WIDTH: 448px; HEIGHT: 112px"><TBODY><TR><TD style="WIDTH: 217px"></TD><TD></TD></TR><TR><TD style="HEIGHT: 191px" colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 440px; HEIGHT: 176px"><asp:GridView id="gvShippingAddr" runat="server" Width="424px" Height="136px" ForeColor="#333333" CellPadding="4" AutoGenerateColumns="False" __designer:wfdid="w81" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="ADDRESS" HeaderText="Address">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="CityName" HeaderText="City">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ProvName" HeaderText="Province">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="CountryName" HeaderText="Country">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ADDRPOSTCODE" HeaderText="Post Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField>
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="WIDTH: 217px"><asp:LinkButton id="lbNewShipping" runat="server" __designer:wfdid="w82">[New Shipping Address]</asp:LinkButton></TD><TD align=right>&nbsp;&nbsp; <asp:Label id="I_U_Shipping" runat="server" Text="New Detail" __designer:wfdid="w83"></asp:Label>&nbsp;<asp:ImageButton id="imbRemoveShipping" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE>
</contenttemplate>
                                                </asp:UpdatePanel>
                        <br />
                        <asp:UpdatePanel id="UpdatePanel3" runat="server">
                            <contenttemplate>
<asp:Panel id="Panel1" runat="server" CssClass="modalBox" Visible="False" __designer:wfdid="w86"><TABLE style="WIDTH: 192px; HEIGHT: 56px"><TBODY><TR><TD align=center colSpan=2><asp:Label id="Label12" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Shipping Address" __designer:wfdid="w87"></asp:Label></TD></TR><TR><TD style="WIDTH: 120px">Address <asp:Label id="Label13" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w88"></asp:Label></TD><TD style="WIDTH: 235px"><asp:TextBox id="txtShippingAddr" runat="server" Width="168px" CssClass="inpText" __designer:wfdid="w89" MaxLength="255"></asp:TextBox> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</TD></TR><TR><TD style="WIDTH: 120px">City</TD><TD style="WIDTH: 235px"><asp:DropDownList id="ddlShippingCity" runat="server" Width="168px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w90" DataTextField="gendesc" DataValueField="genoid" DataSourceID="SqlCity"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 120px">Province</TD><TD style="WIDTH: 235px"><asp:DropDownList id="ddlShippingProv" runat="server" Width="168px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w91" DataTextField="gendesc" DataValueField="genoid" DataSourceID="SqlProvince"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 120px">Country</TD><TD style="WIDTH: 235px"><asp:DropDownList id="ddlShippingCountry" runat="server" Width="168px" CssClass="inpText" Font-Size="X-Small" __designer:wfdid="w92" DataTextField="gendesc" DataValueField="genoid" DataSourceID="SqlCountry"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 120px">Post Code</TD><TD style="WIDTH: 235px"><asp:TextBox id="txtShippingPostCode" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w93" MaxLength="10"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="fteShippingPostCode" runat="server" __designer:wfdid="w94" TargetControlID="txtShippingPostCode"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=2><asp:LinkButton id="lbAddShipping" runat="server" __designer:wfdid="w95">[Add To List]</asp:LinkButton>&nbsp;<asp:LinkButton id="lbCCShipping" runat="server" __designer:wfdid="w96">[Cancel & Close]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpe1" runat="server" __designer:wfdid="w97" TargetControlID="btnHiddenShipping" PopupDragHandleControlID="Label12" PopupControlID="Panel1" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHiddenShipping" runat="server" Visible="False" __designer:wfdid="w98"></asp:Button> 
</contenttemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="viewCP" runat="server">
                        <asp:Label ID="Label9" runat="server" CssClass="submenu" Font-Size="Small" Text="Information"></asp:Label><asp:Label
                            ID="Label11" runat="server" ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label
                                ID="Label14" runat="server" CssClass="submenu" Font-Size="Small" ForeColor="Navy"
                                Text="Shipping Address" Width="133px"></asp:Label><asp:Label ID="Label15" runat="server"
                                    ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label ID="Label16"
                                        runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Black" Text="Contact Person"></asp:Label><asp:Label
                                            ID="Label17" runat="server" ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label
                                                ID="Label18" runat="server" CssClass="submenu" Font-Size="Small" Text="Bank Account"></asp:Label><br />
                        <asp:UpdatePanel id="UpdatePanel4" runat="server">
                            <contenttemplate>
<TABLE style="WIDTH: 280px; HEIGHT: 40px"><TBODY><TR><TD style="HEIGHT: 15px"></TD><TD style="WIDTH: 102px; HEIGHT: 15px"></TD></TR><TR><TD style="HEIGHT: 13px" colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 100px; HEIGHT: 100px"><asp:GridView id="gvCP" runat="server" Width="138%" Height="64px" ForeColor="#333333" __designer:wfdid="w108" CellPadding="4" BorderStyle="Solid" BorderWidth="1px" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:BoundField DataField="CPNAME" HeaderText="Contact Person"></asp:BoundField>
<asp:BoundField DataField="CPPHONE" HeaderText="Phone"></asp:BoundField>
<asp:BoundField DataField="CPEMAIL" HeaderText="Email"></asp:BoundField>
<asp:TemplateField>
<HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></HeaderStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="HEIGHT: 41px"><asp:LinkButton id="LinkButton1" runat="server" __designer:wfdid="w109">[ New Contact Person ]</asp:LinkButton></TD><TD style="WIDTH: 102px; HEIGHT: 41px" align=right>&nbsp;<asp:Label id="I_U_CP" runat="server" Text="New Detail" __designer:wfdid="w110"></asp:Label>&nbsp;<asp:ImageButton id="imbRemoveCP" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w111"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel id="UpdatePanel5" runat="server">
                            <contenttemplate>
<asp:Panel id="Panel2" runat="server" CssClass="modalBox" Visible="False" __designer:wfdid="w113"><TABLE style="WIDTH: 312px; HEIGHT: 56px"><TBODY><TR><TD style="HEIGHT: 15px" align=center colSpan=2><asp:Label id="labelCP" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Contact Person" __designer:wfdid="w114"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD>Contact Person <asp:Label id="Label20" runat="server" CssClass="Important" Text="*" __designer:wfdid="w115"></asp:Label></TD><TD><asp:TextBox id="txtCPName" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w116"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Mobile</TD><TD><asp:TextBox id="txtCPMobile" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w117"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD>Email</TD><TD><asp:TextBox id="txtCPEmail" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w118"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=2><ajaxToolkit:FilteredTextBoxExtender id="fteCP" runat="server" __designer:wfdid="w119" TargetControlID="txtCPMobile"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=center colSpan=2><asp:LinkButton id="lbAddCP" runat="server" __designer:wfdid="w120">[Add To List]</asp:LinkButton>&nbsp;<asp:LinkButton id="lbCCCP" runat="server" __designer:wfdid="w121">[Cancel & Close]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpe2" runat="server" __designer:wfdid="w122" TargetControlID="btnHiddenCP" PopupDragHandleControlID="labelCP" PopupControlID="Panel2" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><BR /></asp:Panel> <asp:Button id="btnHiddenCP" runat="server" Visible="False" __designer:wfdid="w123"></asp:Button> 
</contenttemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="viewBankAccount" runat="server">
                        <asp:Label ID="Label19" runat="server" CssClass="submenu" Font-Size="Small" Text="Information"></asp:Label><asp:Label
                            ID="Label21" runat="server" ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label><asp:Label
                                ID="Label22" runat="server" CssClass="submenu" Font-Size="Small" ForeColor="Navy"
                                Text="Shipping Address" Width="133px"></asp:Label><asp:Label ID="Label23" runat="server"
                                    ForeColor="#404040" Height="16px" Text="|" Width="1px"></asp:Label>
                        <asp:Label ID="Label24" runat="server" CssClass="submenu" Font-Size="Small" Text="Contact Person"></asp:Label>
                        <asp:Label ID="Label25" runat="server" ForeColor="#404040" Height="16px" Text="|"
                            Width="1px"></asp:Label><asp:Label ID="Label26" runat="server" Font-Bold="True" Font-Size="Small"
                                ForeColor="Black" Text="Bank Account"></asp:Label><br />
                        <asp:UpdatePanel id="UpdatePanel6" runat="server">
                            <contenttemplate>
<TABLE style="WIDTH: 344px; HEIGHT: 72px"><TBODY><TR><TD style="HEIGHT: 14px"></TD><TD style="HEIGHT: 14px"></TD></TR><TR><TD colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 408px; HEIGHT: 152px"><asp:GridView id="gvBAcc" runat="server" Width="480px" Height="144px" __designer:wfdid="w133" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="CurrencyName" HeaderText="Currency"></asp:BoundField>
<asp:BoundField DataField="BANKNAME" HeaderText="Bank Name"></asp:BoundField>
<asp:BoundField DataField="BankAddress" HeaderText="Bank Address"></asp:BoundField>
<asp:BoundField DataField="BankAccName" HeaderText="Account Name"></asp:BoundField>
<asp:BoundField DataField="BankAccNo" HeaderText="Account No"></asp:BoundField>
<asp:BoundField DataField="SWIFTCODE" HeaderText="Swift Code"></asp:BoundField>
<asp:TemplateField></asp:TemplateField>
</Columns>
</asp:GridView></DIV></TD></TR><TR><TD><asp:LinkButton id="newDataBAcc" onclick="newDataBAcc_Click" runat="server" __designer:wfdid="w134">[ New Bank Account ]</asp:LinkButton></TD><TD align=right><asp:Label id="I_U_BAcc" runat="server" Text="New Detail" __designer:wfdid="w135"></asp:Label>&nbsp;<asp:ImageButton id="imbRemoveBAcc" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w136"></asp:ImageButton></TD></TR></TBODY></TABLE>
</contenttemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel id="UpdatePanel7" runat="server">
                            <contenttemplate>
<asp:Panel id="Panel3" runat="server" Width="360px" Height="160px" CssClass="modalBox" Visible="False" __designer:wfdid="w138"><TABLE style="WIDTH: 336px; HEIGHT: 72px"><TBODY><TR><TD align=center colSpan=4><asp:Label id="labelBAcc" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Navy" Text="Bank Account" __designer:wfdid="w139" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 81px">Bank Name <asp:Label id="Label27" runat="server" CssClass="Important" Text="*" __designer:wfdid="w140"></asp:Label></TD><TD><asp:TextBox id="txtBankName" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w141" MaxLength="50"></asp:TextBox></TD><TD style="WIDTH: 58px">Currency</TD><TD style="WIDTH: 23px"><asp:DropDownList id="ddlCurrency" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w142" DataTextField="currencycode" DataValueField="currencyoid" DataSourceID="SqlCurrency"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 81px; HEIGHT: 28px">Bank Address</TD><TD style="HEIGHT: 28px"><asp:TextBox id="txtBankAddr" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w143" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 58px; HEIGHT: 28px">Swift Code</TD><TD style="WIDTH: 23px; HEIGHT: 28px"><asp:TextBox id="txtSwift" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w144" MaxLength="50"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="fteSwift" runat="server" __designer:wfdid="w145" TargetControlID="txtSwift"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="WIDTH: 81px">Account Name <asp:Label id="Label28" runat="server" CssClass="Important" Text="*" __designer:wfdid="w146"></asp:Label></TD><TD><asp:TextBox id="txtAccName" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w147" MaxLength="100"></asp:TextBox></TD><TD style="WIDTH: 58px">Account No <asp:Label id="Label29" runat="server" CssClass="Important" Text="*" __designer:wfdid="w148"></asp:Label></TD><TD style="WIDTH: 23px"><asp:TextBox id="txtAccNo" runat="server" Width="160px" CssClass="inpText" __designer:wfdid="w149" MaxLength="30"></asp:TextBox><BR /><ajaxToolkit:FilteredTextBoxExtender id="fteAccNo" runat="server" __designer:wfdid="w150" TargetControlID="txtAccNo"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 15px" align=center colSpan=4><asp:LinkButton id="lbAddBAcc" runat="server" Font-Bold="False" __designer:wfdid="w151">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="lbCloseBAcc" runat="server" Font-Bold="False" __designer:wfdid="w152">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpe3" runat="server" __designer:wfdid="w153" TargetControlID="btnHiddenBAcc" PopupDragHandleControlID="labelBAcc" PopupControlID="Panel3" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHiddenBAcc" runat="server" Visible="False" __designer:wfdid="w154"></asp:Button> 
</contenttemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                </asp:MultiView><br />
            </ContentTemplate>
            <HeaderTemplate>
                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                .:
                <span style="font-size: 9pt"><strong> Form Supplier</strong></span>&nbsp;
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
        </table>
    <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="WIDTH: 0px; HEIGHT: 0px; BACKGROUND-COLOR: red" align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="WIDTH: 39px" align=center><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle"></asp:Image></TD><TD style="WIDTH: 180px" align=left><asp:Label id="Validasi" runat="server" CssClass="Important" Font-Bold="True"></asp:Label></TD></TR><TR><TD colSpan=2><asp:Label id="lblState" runat="server" Font-Bold="False" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 38px" align=center colSpan=2><asp:ImageButton id="btnErrorOk" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="Top"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeValidasi" runat="server" TargetControlID="btnExtenderValidasi" PopupDragHandleControlID="lblCaption" PopupControlID="PanelValidasi" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnExtenderValidasi" runat="server" Visible="False"></asp:Button><BR />
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel9" runat="server">
        <contenttemplate>
<asp:UpdatePanel id="UpdPanelPrint" runat="server"><ContentTemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Supplier"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="suppIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="suppNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 20px" align=left></TD><TD style="HEIGHT: 20px" align=left></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" BackgroundCssClass="modalBackground" PopupControlID="pnlPrint" PopupDragHandleControlID="lblPrint" TargetControlID="btnHidePrint" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>

