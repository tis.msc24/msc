<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
CodeFile="mstcurr.aspx.vb" Inherits="mstcurr" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
 <script type="text/javascript">
//    function s()
//    {
//    try {
//        var t = document.getElementById("<%=GVmst.ClientID%>");
//        var t2 = t.cloneNode(true)
//        for(i = t2.rows.length -1;i > 0;i--)
//        t2.deleteRow(i)
//        t.deleteRow(0)
//        divTblData.appendChild(t2)
//        }
//    catch(errorInfo) {}
//    
//    }
//    window.onload = s
  </script>
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        width="100%" class="tabelhias">
        <tr style="font-size: 8pt; color: #000099">
            <th class="header" colspan="3" valign="middle">
                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Data Currency"></asp:Label><br />
                            <asp:SqlDataSource ID="SDSData" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                DeleteCommand="DELETE FROM [QL_mstcurr] WHERE [cmpcode] = @cmpcode AND [currencyoid] = @currencyoid"
                                InsertCommand="INSERT INTO [QL_mstcurr] ([cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime]) VALUES (@cmpcode, @currencyoid, @currencycode, @currencydesc, @upduser, @updtime)"
                                SelectCommand="SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencyoid] = @currencyoid))"
                                UpdateCommand="UPDATE [QL_mstcurr] SET [currencycode] = @currencycode, [currencydesc] = @currencydesc, [upduser] = @upduser, [updtime] = @updtime WHERE [cmpcode] = @cmpcode AND [currencyoid] = @currencyoid" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currencyoid" Type="Int32" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currencyoid" Type="Int32" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="currencycode" Type="String" />
                                    <asp:Parameter Name="currencydesc" Type="String" />
                                    <asp:Parameter Name="upduser" Type="String" />
                                    <asp:Parameter Name="updtime" Type="DateTime" />
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currencyoid" Type="Int32" />
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currencyoid" Type="Int32" />
                                    <asp:Parameter Name="currencycode" Type="String" />
                                    <asp:Parameter Name="currencydesc" Type="String" />
                                    <asp:Parameter Name="upduser" Type="String" />
                                    <asp:Parameter Name="updtime" Type="DateTime" />
                                </InsertParameters>
                            </asp:SqlDataSource>

                            <asp:SqlDataSource ID="SDSDataView" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                SelectCommand="SELECT DISTINCT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencydesc] LIKE @currencydesc) AND ([currencycode] LIKE @currencycode) AND ([upduser] LIKE @upduser)) ORDER BY [currencyoid] DESC" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter Name="cmpcode" Type="String" />
                                    <asp:Parameter Name="currencydesc" Type="String" />
                                    <asp:Parameter Name="currencycode" Type="String" />
                                    <asp:Parameter Name="upduser" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SDSOid" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>"
                                SelectCommand="SELECT [cmpcode], [tablename], [lastoid], [tablegroup] FROM [QL_mstoid] WHERE (([tablename] = @tablename) AND ([cmpcode] = @cmpcode))"
                                UpdateCommand="UPDATE QL_mstoid SET lastoid = @lastoid WHERE (tablename = @tablename) AND (cmpcode = @cmpcode)" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="QL_MstPayment" Name="tablename" Type="String" />
                                    <asp:Parameter DefaultValue="" Name="cmpcode" Type="String" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="lastoid" />
                                    <asp:Parameter Name="tablename" />
                                    <asp:Parameter Name="cmpcode" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" SelectCommand="SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencycode] = @currencycode))">
                    <SelectParameters>
                        <asp:Parameter Name="currencycode" Type="String" />
                        <asp:Parameter Name="cmpcode" Type="String" />
                    </SelectParameters>
                
                </asp:SqlDataSource><asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" SelectCommand="SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencydesc] = @currencydesc))">
                    <SelectParameters>
                        <asp:Parameter Name="currencydesc" Type="String" />
                        <asp:Parameter Name="cmpcode" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource><asp:SqlDataSource ID="SqlDataSource1upd" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" SelectCommand="SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencycode] = @currencycode) AND ([currencyoid] <> @oid))">
                    <SelectParameters>
                        <asp:Parameter Name="currencycode" Type="String" />
                        <asp:Parameter Name="cmpcode" Type="String" />
                        <asp:Parameter Name="oid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2upd" runat="server" ConnectionString="<%$ ConnectionStrings:QL_SIP_ConnectionString %>" ProviderName="<%$ ConnectionStrings:QL_SIP_ConnectionString.ProviderName %>" SelectCommand="SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE (([cmpcode] = @cmpcode) AND ([currencydesc] = @currencydesc) AND ([currencyoid] <> @oid))">
                    <SelectParameters>
                        <asp:Parameter Name="currencydesc" Type="String" />
                        <asp:Parameter Name="cmpcode" Type="String" />
                        <asp:Parameter Name="oid" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </th>
        </tr>
    </table>
    <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" style="height: 15px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Height="350px" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                            <table>
                                <tr>
                                    <td class="Label">
                                        Filter :</td>
                                    <td>
                                        <asp:DropDownList ID="FilterDDL" runat="server" Width="96px" CssClass="inpText">
                                            <asp:ListItem Value="Code">Code</asp:ListItem>
                                            <asp:ListItem Value="Description">Description</asp:ListItem>
                                        </asp:DropDownList>
                                        </td>
                                    <td>
                                        <asp:TextBox ID="FilterText" runat="server" MaxLength="30" Width="150px" CssClass="inpText" Height="16px"></asp:TextBox></td>
                                    <td>
                                        <asp:ImageButton
                                            ID="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" CausesValidation="False" /></td>
                                    <td>
                                        <asp:ImageButton
                                            ID="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" CausesValidation="False" /></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px">
                                        </td>
                                    <td style="height: 10px">
                                    </td>
                                    <td style="height: 10px">
                                    </td>
                                    <td style="height: 10px">
                                    </td>
                                    <td style="height: 10px">
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                                         <fieldset  id="field1" style="height: 275px; width: 99%; text-align: left; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"  > 
    <div id='divTblData'>
    </div>
    <div style="height: 100%; width: 100%;">
                                        <asp:GridView ID="GVmst" runat="server" CellPadding="4" AutoGenerateColumns="False" BackColor="White"
                                            BorderColor="#DEDFDE" BorderWidth="1px" Width="98%" BorderStyle="Solid" DataSourceID="SDSDataView" AllowSorting="True" AllowPaging="True">
                                            <FooterStyle BackColor="#F25407" />
                                            <SelectedRowStyle BackColor="#F25407" ForeColor="White" Font-Bold="True" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Right" />
                                            <HeaderStyle BackColor="#124E81" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:HyperLinkField DataNavigateUrlFields="currencyoid" DataNavigateUrlFormatString="~/master/mstcurr.aspx?oid={0}"
                                                    DataTextField="currencycode" HeaderText="Code" SortExpression="currencycode" >
                                                    <ItemStyle Width="15%" Font-Size="X-Small" />
                                                    <HeaderStyle ForeColor="Navy" Width="15%" Font-Size="X-Small" />
                                                </asp:HyperLinkField>
                                                <asp:BoundField DataField="currencydesc" HeaderText="Description" SortExpression="currencydesc">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="85%" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Left" Width="85%" ForeColor="Navy" />
                                                </asp:BoundField>
                                            </Columns>
                                            <RowStyle BackColor="#F7F7DE" />
                                            <EmptyDataTemplate>
                                        <asp:Label ID="lblmsg" runat="server" Text="No data found !" CssClass="Important"></asp:Label>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        
                                        
                                        
                                        </div></fieldset>
                        </ContentTemplate>
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/corner.gif" />
                            <span style="font-size: 9pt"><strong>
                            <span>::</span> <span>
                                List Currency</span></strong></span>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate><img align="absMiddle" alt="" src="../Images/corner.gif" id="Img2" onclick="return IMG1_onclick()" />
                            <span style="font-size: 9pt"><strong>
                            :: Form Currency</strong></span>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td align="center" style="text-align: left;">
                                        <asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<asp:FormView id="FrmViewCurr" runat="server" Width="456px" DefaultMode="Insert" DataKeyNames="cmpcode,currencyoid" OnPageIndexChanging="FrmViewCurr_PageIndexChanging" __designer:wfdid="w13" DataSourceID="SDSData"><EditItemTemplate>
<TABLE id="Table1" cellSpacing=2 cellPadding=4 width=475 align=center border=0><TBODY><TR style="COLOR: #000099"><TD style="WIDTH: 30%; TEXT-ALIGN: left" class="Label"><SPAN style="FONT-SIZE: x-small">Currency Code <asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w1"></asp:Label></SPAN></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:TextBox id="currencycode" runat="server" Width="90px" CssClass="inpText" Font-Size="X-Small" Text='<%# Bind("currencycode", "{0}") %>' __designer:wfdid="w2" MaxLength="10" size="20"></asp:TextBox>&nbsp;<asp:LinkButton id="LinkButton1" onclick="LinkButton1_Click" runat="server" __designer:wfdid="w3">[ Show History ]</asp:LinkButton> <asp:TextBox id="oid" runat="server" Width="50px" ForeColor="Navy" Text='<%# Bind("currencyoid", "{0}") %>' __designer:wfdid="w4" Visible="False" MaxLength="10" size="20" BorderWidth="0px" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 30%; TEXT-ALIGN: left" class="Label"><SPAN style="FONT-SIZE: x-small">Description <asp:Label id="Label4" runat="server" CssClass="Important" Text="*" __designer:wfdid="w5"></asp:Label></SPAN></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:TextBox id="currencydesc" runat="server" Width="236px" CssClass="inpText" Font-Size="X-Small" Text='<%# Bind("currencydesc", "{0}") %>' __designer:wfdid="w6" MaxLength="49" size="20"></asp:TextBox></TD></TR><TR><TD style="COLOR: #585858; TEXT-ALIGN: left" colSpan=2><SPAN style="FONT-SIZE: x-small; COLOR: #585858">Last Update On</SPAN> <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w7"></asp:Label> <SPAN style="FONT-SIZE: x-small; COLOR: dimgray">by</SPAN> <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w9" CommandName="Update"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w10" CommandName="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w11" CommandName="Delete" onClientClick="return confirm('Are you sure to delete this data?')"></asp:ImageButton></TD></TR></TBODY></TABLE>
</EditItemTemplate>
<InsertItemTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server" __designer:wfdid="w7"><ContentTemplate>
<TABLE id="Table2" cellSpacing=2 cellPadding=4 width=475 align=center border=0><TBODY><TR style="COLOR: #000099"><TD style="WIDTH: 30%; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Currency Code <asp:Label id="lbl11" runat="server" CssClass="Important" __designer:wfdid="w8">*</asp:Label></SPAN></TD><TD style="TEXT-ALIGN: left"><asp:TextBox id="currencycode" runat="server" Width="90px" CssClass="inpTextDisabled" Font-Size="X-Small" Text='<%# Bind("currencycode", "{0}") %>' __designer:wfdid="w9" MaxLength="10" size="20" ReadOnly="True"></asp:TextBox><asp:TextBox id="oid" runat="server" Width="50px" CssClass="inpText" Text='<%# Bind("currencyoid", "{0}") %>' __designer:wfdid="w10" Visible="False" MaxLength="10" size="20" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 30%; TEXT-ALIGN: left"><SPAN style="FONT-SIZE: x-small">Description <asp:Label id="Label2" runat="server" CssClass="Important" __designer:wfdid="w11">*</asp:Label></SPAN></TD><TD style="TEXT-ALIGN: left"><asp:TextBox id="currencydesc" runat="server" Width="232px" CssClass="inpText" Font-Size="X-Small" Text='<%# Bind("currencydesc", "{0}") %>' __designer:wfdid="w12" MaxLength="49" size="20"></asp:TextBox></TD></TR><TR><TD colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w13" CommandName="Insert" OnClick="btnSave_Click"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" CausesValidation="False" __designer:wfdid="w14" CommandName="Cancel"></asp:ImageButton></TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel> 
</InsertItemTemplate>
</asp:FormView> 
</contenttemplate>
                                        </asp:UpdatePanel>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
                                <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" __designer:wfdid="w15" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCapt" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" Text="PT. SEKAWAN INTIPRATAMA - Warning" __designer:wfdid="w16"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px"></TD><TD style="HEIGHT: 10px" class="Label"></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w17"></asp:Image></TD><TD class="Label"><asp:Label id="Validasi" runat="server" ForeColor="Red" __designer:wfdid="w18"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px"></TD><TD style="HEIGHT: 10px" class="Label"></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click1" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w19"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" __designer:wfdid="w20" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" __designer:wfdid="w21" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelValidasi" PopupDragHandleControlID="lblCapt" TargetControlID="ButtonExtendervalidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
</asp:Content>
