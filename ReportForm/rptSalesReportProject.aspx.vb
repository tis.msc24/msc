Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction

Partial Class ReportForm_rptSalesReport_Project
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))

#End Region

#Region "Procedure"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Sub showPrint(ByVal tipe As String)
        Try
            Label2.Text = ""
            CrystalReportViewer1.Visible = True
            Dim sWhere As String = ""
            Dim namaPDF As String = ""

            Try
                If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                    Label2.Text = "Period 2 must be more than Period 1 !"
                    Exit Sub
                End If
            Catch ex As Exception
                Label2.Text = "Please check Period value"
                Exit Sub
            End Try

            If rbSO.Checked = True Then
                If ddltype.SelectedValue = "Summary" Then
                    vReport.Load(Server.MapPath("~\Report\SalesReport_Sum.rpt"))
                    namaPDF = "Sales_Project_Report_(Sum)_"

                Else
                    vReport.Load(Server.MapPath("~\Report\SalesReport_Dtl.rpt"))
                    namaPDF = "Sales_Project_Report_(Dtl)_"
                End If
            Else
                vReport.Load(Server.MapPath("~\Report\rptStatus_SO_DO.rpt"))
                namaPDF = "Status_SO_DO_Project_"
            End If


            sWhere = " where mst.trnordertype = 'PROJECK' and convert(char(10),mst.trnorderdate,121) between '" & Format(CDate(toDate(range1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(range2.Text)), "yyyy-MM-dd") & "'"
            sWhere &= IIf(ToDouble(itemoid.Text) > 0, " and mat.itemoid = '" & itemoid.Text & "' ", " ")
            sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "  ", "  and  mat.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            sWhere &= IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "  ", "  and  mat.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")
            sWhere &= IIf(sooid.Text.Trim = "", " ", " and mst.ordermstoid = " & sooid.Text & " ")
            sWhere &= IIf(sales.SelectedValue = "ALL", " ", " and mst.salesoid = " & sales.SelectedValue & " ")
            sWhere &= IIf(custoid.Text.Trim = "", " ", " and  mst.trncustoid = " & custoid.Text & " ")
            sWhere &= IIf(ddlStatus.SelectedItem.Text = "All", " ", " and  mst.trnorderstatus = " & ddlStatus.SelectedValue & " ")

            If taxable.SelectedValue <> "ALL" Then
                If taxable.SelectedValue = "YES" Then
                    sWhere &= " and mst.trntaxpct > 0"
                Else
                    sWhere &= " and mst.trntaxpct = 0"
                End If
            End If
            If statusDelivery.SelectedValue <> "ALL" Then
                If statusDelivery.SelectedValue = "COMPLETE" Then
                    sWhere &= " and upper(dtl.trnorderdtlstatus) = 'COMPLETED'"
                Else
                    sWhere &= " and upper(dtl.trnorderdtlstatus) <> 'COMPLETED'"
                End If
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)

            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("startperiod", toDate(range1.Text))
            vReport.SetParameterValue("endperiod", toDate(range2.Text))
            If rbSO.Checked = True Then
                vReport.SetParameterValue("titledef", "Project ")
            End If
            vReport.SetParameterValue("filterSO", IIf(sooid.Text.Trim = "", "ALL", sono.Text))
            vReport.SetParameterValue("filterGroup", IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "ALL GROUP", FilterDDLGrup.SelectedItem.Text))
            vReport.SetParameterValue("filterSubGroup", IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "ALL SUB GROUP", FilterDDLSubGrup.SelectedItem.Text))
            vReport.SetParameterValue("filterItem", IIf(itemoid.Text.Trim = "", "ALL", itemdesc.Text))
            vReport.SetParameterValue("filterCustomer", IIf(custoid.Text.Trim = "", "ALL", custname.Text))
            vReport.SetParameterValue("filterTaxable", IIf(taxable.SelectedValue = "ALL", "ALL", taxable.SelectedValue))
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If tipe = "view" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "excel" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesreportProject.aspx?awal=true")
            Else
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptSalesreportProject.aspx?awal=true")
            End If
        Catch ex As Exception
            'vReport.Close() : vReport.Dispose()
            ' lblmsgerrsave.Text = ex.Message
            Label2.Text = ex.Message
        End Try
    End Sub
    Sub bindpo_spb()
        Dim filtereorder As String = ""
        filtereorder = " "

        Dim ssql As String
        ssql = "SELECT mst.ordermstoid,mst.orderno,convert(char(10),mst.trnorderdate,103) trnorderdate, mst.trncustname , mst.trncustoid from QL_trnordermst mst where mst.trnordertype = 'PROJECK' and mst.orderno like '%" & Tchar(sono.Text.Trim) & "%' and mst.orderno like " & type_so.SelectedValue & "  and trnorderdate between '" & toDate(range1.Text) & "'  and '" & toDate(range2.Text) & "' order by orderno desc "
        FillGV(gvListpo, ssql, "QL_mstsupp")
    End Sub
#End Region


#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        Dim cP As New ClassProcedure
        cP.CheckRegionalSetting()

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            'Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Session.Clear()  ' -->>  clear all session 
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            '            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("rptSalesreportProject.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang 
        End If
        Page.Title = CompnyName & " - Sales Project Report"
        'If Session("showReport") = "True" Then
        '    showPrint("view")
        'End If
        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            ddltype_SelectedIndexChanged(sender, e)
            range1.Text = Format(Now.AddDays(-1), "dd/MM/yyyy") : range2.Text = Format(Now, "dd/MM/yyyy")
            FillDDL(FilterDDLGrup, "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' order by gendesc")
            FilterDDLGrup.Items.Add("ALL GROUP")
            FilterDDLGrup.SelectedValue = "ALL GROUP"
            FillDDL(FilterDDLSubGrup, "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' order by gendesc")
            FilterDDLSubGrup.Items.Add("ALL SUB GROUP")
            FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"

            FillDDL(sales, "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personpost in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC = 'SALES PERSON')  and STATUS='AKTIF' ")
            sales.Items.Add("ALL")
            sales.SelectedIndex = sales.Items.Count - 1
        End If
        If Session("showReport") = "True" Then
            showPrint("view")
        End If
    End Sub
    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("pdf")
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub
    Protected Sub ibClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClear.Click
        Session("showReport") = "False" : vReport.Close() : Label2.Text = ""
        Response.Redirect("rptSalesreportProject.aspx?awal=true")
    End Sub
    Protected Sub ibExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        showPrint("excel")
    End Sub
    Public Sub BindDataListItem()
        Dim ssql As String = ""
        ssql = "select ql_mstitem.itemcode, ql_mstitem.itemdesc ,ql_mstitem.merk ,ql_mstitem.itempriceunit1, ql_mstitem.itemoid, satuan1," & _
 "g.gendesc unit , satuan2, konversi1_2, g2.gendesc  unit2  from ql_mstitem inner join ql_mstgen g on g.genoid=satuan1 " & _
 "and itemflag='AKTIF'  inner join ql_mstgen g2 on g2.genoid=satuan2 where (ql_mstitem.itemdesc like '%" & Tchar(itemdesc.Text) & "%' OR ql_mstitem.itemcode LIKE '%" & Tchar(itemdesc.Text.Trim) & "%' OR ql_mstitem.merk LIKE '%" & Tchar(itemdesc.Text.Trim) & "%') " & IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", " ", " and ql_mstitem.itemgroupoid = " & FilterDDLGrup.SelectedValue) & IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", " ", " and ql_mstitem.itemsubgroupoid = " & FilterDDLSubGrup.SelectedValue) & " order by ql_mstitem.itemdesc"

        FillGV(gvListForecast, ssql, "ql_mstitem")
    End Sub
    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
        gvListForecast.Visible = True
    End Sub
    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        itemdesc.Text = ""
        itemoid.Text = ""

        gvListForecast.Visible = False
        CProc.DisposeGridView(gvListForecast)
    End Sub
    Protected Sub gvListForecast_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        itemdesc.Text = gvListForecast.SelectedDataKey.Item(1)
        itemoid.Text = gvListForecast.SelectedDataKey.Item("itemoid")
        gvListForecast.Visible = False
        CProc.DisposeGridView(gvListForecast)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Sub FilterGVCust(ByVal filterName As String, ByVal filterCode As String, ByVal cmpcode As String)
        Dim ssql As String = ""
        ssql = "SELECT c.custoid as Id,c.custcode as code,c.custname as Name,c.custaddr as Address from QL_mstcust c WHERE c.custflag='Active' AND (c.custname LIKE '%" & filterName & "%' OR c.custcode LIKE '%" & filterCode & "%') and c.custgroup='PROJECK' ORDER BY c.custname "

        FillGV(gvCustomer, ssql, "QL_mstcust")
    End Sub
    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custname.Text = "" : custoid.Text = ""
        gvCustomer.Visible = False
    End Sub
    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        custname.Text = gvCustomer.SelectedDataKey.Item(1)
        custoid.Text = gvCustomer.SelectedDataKey.Item(0)
        gvCustomer.Visible = False
        'CProc.SetModalPopUpExtender(btnhiddenCust, Panel1, ModalPopupExtender1, False)
        CProc.DisposeGridView(gvCustomer)
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Protected Sub ddltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltype.SelectedIndexChanged
        'If ddltype.SelectedValue = "Summary" Then
        '    FilterDDLGrup.Visible = True
        '    FilterDDLSubGrup.Visible = True
        '    Label3.Visible = False
        '    itemdesc.Visible = False
        '    btnSearchItem.Visible = False
        '    btnEraseItem.Visible = False
        'Else
        '    FilterDDLGrup.Visible = True
        '    FilterDDLSubGrup.Visible = True
        '    Label3.Visible = True
        '    itemdesc.Visible = True
        '    btnSearchItem.Visible = True
        '    btnEraseItem.Visible = True
        'End If
    End Sub
    Protected Sub ImageButtonpo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cancelpo.Click
        sooid.Text = ""
        sono.Text = ""
        tdSO.Visible = False
        custname.CssClass = "inpText"
        custname.Enabled = True
        btnSearch.Enabled = True
        imbClearCust.Enabled = True

        gvListpo.Visible = False

    End Sub
    Protected Sub gvListpo_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListpo.SelectedIndexChanged
        sono.Text = gvListpo.SelectedDataKey(1).ToString
        sooid.Text = gvListpo.SelectedDataKey(0).ToString
        custoid.Text = gvListpo.SelectedDataKey("trncustoid")
        custname.Text = gvListpo.SelectedDataKey("trncustname")
        custname.CssClass = "inpTextDisabled"
        custname.Enabled = False
        btnSearch.Enabled = False
        imbClearCust.Enabled = False
        gvListpo.Visible = False
        tdSO.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
        Session("showReport") = "False"
    End Sub
    Protected Sub gvListpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListpo.PageIndexChanging
        gvListpo.PageIndex = e.NewPageIndex
        bindpo_spb()

        gvListpo.Visible = True
        'panelDsopo.Visible = True
        'btnHideDSOpo.Visible = True
        'ModalPopupExtenderdsopo.Show()
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        gvCustomer.PageIndex = e.NewPageIndex
        FilterGVCust((custname.Text.Trim), (custname.Text.Trim), CompnyCode)
        gvCustomer.Visible = True
        'CProc.SetModalPopUpExtender(btnhiddenCust, Panel1, ModalPopupExtender1, True)
    End Sub

    Protected Sub gvListForecast_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListForecast.PageIndexChanging
        gvListForecast.PageIndex = e.NewPageIndex
        BindDataListItem()
        gvListForecast.Visible = True
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        showPrint("view")
        Session("showReport") = "True"
    End Sub

    Protected Sub btnSearch_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        FilterGVCust((custname.Text.Trim), (custname.Text.Trim), CompnyCode)
        gvCustomer.Visible = True
    End Sub

    Protected Sub posearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles posearch.Click
        Label2.Text = ""
        Try
            If CDate(toDate(range1.Text.Trim)) > CDate(toDate(range2.Text.Trim)) Then
                Label2.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            Label2.Text = "Please check Period value"
            Exit Sub
        End Try

        bindpo_spb()
        tdSO.Visible = True
        gvListpo.Visible = True

    End Sub
    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\ReportForm\frmRealisasi_Pembelian.aspx?awal=true")
        End If
    End Sub
    Protected Sub rbPO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSO.CheckedChanged, rbSODO.CheckedChanged
        If rbSO.Checked = True Then
            TD2.Visible = True
            TD3.Visible = True
        Else
            TD2.Visible = False
            TD3.Visible = False
        End If
        Session("diprint") = "False"
    End Sub
#End Region

End Class
