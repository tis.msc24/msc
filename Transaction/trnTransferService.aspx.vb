Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnTransferService
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function IsInputValid() As Boolean
        Dim errmsg As String = ""

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If

        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong..!"
        End If

        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail Item tidak boleh kosong !"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            Return False
        End If

        Return True
    End Function
#End Region

#Region "Procedure"

    Private Function GetInterfaceValue(ByVal sInterfaceVar As String) As String
        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & cmpcode & "' AND interfacevar='" & sInterfaceVar & "'"
        Return GetStrData(sSql)
    End Function

    Private Function GetAccountOid(ByVal sFilterCode As String) As Integer
        sSql = "SELECT acctgoid FROM QL_mstacctg WHERE cmpcode='" & cmpcode & "' AND acctgcode LIKE '" & sFilterCode & "' AND acctgflag='A' " & _
            "AND acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=ql_mstacctg.cmpcode) ORDER BY acctgcode"
        Return ToDouble(GetStrData(sSql))
    End Function

    Private Sub FillTextbox()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim transdate As New Date
        Dim fromlocationoid As Integer = 0 : Dim tolocationoid As Integer = 0

        sSql = "Select trfwhserviceoid,trfwhserviceNo,trfwhservicedate, trfwhservicenote,createtime, createuser, updtime,upduser, trfwhservicerefno, trfwhservicestatus, frombranch, tobranch, frommtrlocoid, tomtrlocoid from ql_trfwhservicemst s WHERE s.cmpcode = '" & cmpcode & "' AND s.trfwhserviceoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader

        If xreader.HasRows Then
            While xreader.Read
                transferno.Text = xreader("trfwhserviceNo")
                transdate = xreader("trfwhservicedate")
                transferdate.Text = Format(xreader("trfwhservicedate"), "dd/MM/yyyy")
                FromBranch.SelectedValue = xreader("fromBranch")
                ToBranch.SelectedValue = xreader("tobranch") 
                InitDDLLocation()
                InitDDLLocationTo()
                fromlocation.SelectedValue = xreader("frommtrlocoid")
                tolocation.SelectedValue = xreader("tomtrlocoid")
                note.Text = xreader("trfwhserviceNote")
                trnstatus.Text = xreader("trfwhservicestatus").ToString.Trim
                upduser.Text = xreader("upduser")
                updtime.Text = xreader("updtime")
                noref.Text = xreader("trfwhservicerefno")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                createtime.Text = Format(xreader("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
            xreader.Close() : conn.Close()
        End If
        xreader.Close() : conn.Close()

        sSql = "Select trfwhservicedtlseq seq,r.reqcode,r.reqoid,twd.reqdtloid,(Select itemdesc From ql_mstitem i WHERE i.itemoid = twd.itemoid) itemdesc,trfwhservicedtlnote reqdtljob,trfwhserviceqty reqqty, twd.itemoid, ISNULL((SELECT rd.reqqty from QL_TRNREQUESTDTL rd where rd.reqdtloid = twd.reqdtloid),0.00) qty from ql_trfwhservicedtl twd INNER JOIN QL_TRNREQUEST r on r.reqoid = twd.reqoid WHERE twd.cmpcode = '" & cmpcode & "' and trfwhserviceoid = " & Integer.Parse(Session("oid")) & " Order by trfwhservicedtlseq"
        Dim dtabs As DataTable = ckon.ambiltabel(sSql, "detailts")
        GVItemDetail.DataSource = dtabs
        GVItemDetail.DataBind()
        Session("itemdetail") = dtabs

        If trnstatus.Text = "In Process" Then
            btnSave.Visible = True : btnDelete.Visible = True
            BtnCancel.Visible = True : btnPosting.Visible = True
            FromBranch.Enabled = False : fromlocation.Enabled = False
            FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
        ElseIf trnstatus.Text = "In Approval" Then
            btnSave.Visible = False : btnDelete.Visible = False
            BtnCancel.Visible = True : btnPosting.Visible = False
            FromBranch.Enabled = False : fromlocation.Enabled = False
            FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
            ToBranch.Enabled = False : tolocation.Enabled = False
            ToBranch.CssClass = "inpTextDisabled" : tolocation.CssClass = "inpTextDisabled"
        ElseIf trnstatus.Text = "Approved" Then
            btnSave.Visible = False
            FromBranch.Enabled = False : fromlocation.Enabled = False
            FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
            ToBranch.Enabled = False : tolocation.Enabled = False
            ToBranch.CssClass = "inpTextDisabled" : tolocation.CssClass = "inpTextDisabled"
        Else
            btnSave.Visible = False
            FromBranch.Enabled = False : fromlocation.Enabled = False
            FromBranch.CssClass = "inpTextDisabled" : fromlocation.CssClass = "inpTextDisabled"
            ToBranch.Enabled = False : tolocation.Enabled = False
            ToBranch.CssClass = "inpTextDisabled" : tolocation.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub BindData()

        sSql = "Select trfwhserviceoid,trfwhserviceNo,trfwhservicedate, (SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=s.fromBranch AND gengroup='CABANG') fromBranch,(SELECT gendesc FROM QL_mstgen tb WHERE tb.gencode=s.toBranch AND gengroup='CABANG') toBranch, trfwhservicestatus from ql_trfwhservicemst s WHERE s.cmpcode = '" & cmpcode & "' And Trfwhservicetype='INTERNAL'"

        If FilterText.Text.Trim <> "" Then
            sSql &= " AND s.trfwhserviceno LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
        End If

        If drCabang.SelectedValue <> "ALL" Then
            If DdlFCabang.SelectedValue = "FromBranch" Then
                sSql &= " And s.fromBranch='" & drCabang.SelectedValue & "'"
            Else
                sSql &= " And s.toBranch='" & drCabang.SelectedValue & "'"
            End If
        End If

        If ddlStatus.SelectedValue.ToUpper <> "ALL" Then
            sSql &= " AND trfwhservicestatus='" & ddlStatus.SelectedValue.ToUpper & "'"
        End If

        If CbTanggal.Checked = True Then
            sSql &= " AND CONVERT(varchar(10), s.trfwhservicedate, 103) BETWEEN '" & tgl1.Text & "' AND '" & tgl2.Text & "'"
        End If
        sSql &= "ORDER BY trfwhserviceoid Desc"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "ts")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("ts") = dtab
    End Sub

    Private Sub BindItem(ByVal state As Integer)
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        sSql = "select i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob, r.reqqty, r.reqdtloid,Isnull((Select SUM(trd.trfwhserviceqty) from ql_trfwhservicedtl trd Where trd.reqdtloid=r.reqdtloid AND trd.reqoid=r.reqmstoid AND r.itemoid=trd.itemoid),0.00) QtyTw ,r.reqqty-Isnull((Select SUM(trd.trfwhserviceqty) from ql_trfwhservicedtl trd Where trd.reqdtloid=r.reqdtloid AND trd.reqoid=r.reqmstoid AND r.itemoid=trd.itemoid),0.00) QtySisa From ql_mstitem i inner join QL_TRNREQUESTDTL r on r.itemoid = i.itemoid where r.reqmstoid = " & reqoid.Text & " AND r.reqdtloid NOT IN (select reqdtloid from QL_TRNFINALSPART fn Where r.reqdtloid=fn.reqdtloid) AND (r.reqqty-Isnull((Select SUM(trd.trfwhserviceqty) from ql_trfwhservicedtl trd Where trd.reqdtloid=r.reqdtloid AND trd.reqoid=r.reqmstoid AND r.itemoid=trd.itemoid),0.00))<>0 Union ALL Select i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob, fs.itemqty reqqty, r.reqdtloid,0.00 QtyTw,0.00 QtySisa From ql_mstitem i Inner Join QL_TRNFINALSPART fs ON fs.itemreqoid=i.itemoid Inner Join QL_TRNREQUESTDTL r ON r.reqdtloid=fs.reqdtloid Where trnfinaloid IN (Select fm.FINOID from QL_TRNFINAL fm Where fm.FINOID=fs.trnfinaloid AND fm.FINSTATUS='POST') And r.reqmstoid =" & reqoid.Text & ""
 
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable : GVItemList.DataBind()
        GVItemList.SelectedIndex = -1 : GVItemList.Visible = True
        Session("itemlistwhretur") = objTable
    End Sub

    Private Sub BindItems(ByVal sWhere As String)
        Try
            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            sSql = "Select COUNT(conrefoid) from QL_conmtr WHere conrefoid IN (Select reqdtloid from QL_TRNREQUESTDTL WHere reqmstoid=" & Integer.Parse(reqoid.Text) & ")"

            If ToDouble(GetScalar(sSql)) > 0 Then
                sSql = "Select rq.reqmstoid, rq.reqdtloid, i.itemoid, i.itemcode, i.itemdesc, rq.reqdtljob, rq.reqqty, SUM(con.qtyIn)-SUM(qtyOut) OsQty, rq.snno From QL_TRNREQUESTDTL rq Inner Join (SELECT con.branch_code, con.conrefoid, con.mtrlocoid, refoid, qtyIn, qtyOut FROM QL_conmtr con) con ON con.conrefoid=rq.reqdtloid Inner Join QL_mstitem i ON i.itemoid=con.refoid Where rq.reqmstoid=" & Integer.Parse(reqoid.Text) & " AND con.mtrlocoid=" & fromlocation.SelectedValue & " AND con.branch_code='" & FromBranch.SelectedValue & "' Group By rq.reqmstoid, rq.reqdtloid, i.itemoid, i.itemcode, i.itemdesc, rq.reqdtljob, rq.reqqty, rq.snno Having(SUM(con.qtyIn) - SUM(qtyOut) > 0.0)"
            Else
                sSql = "'Select *,Isnull(ReqQty,0.00)-Isnull(QtyTW,0.00) OsQty from (/*Query TTS=TWS (Penerimaan langsung Dikirim ke Cabang Lain)*/ " & _
                "Select r.reqmstoid,r.reqdtloid ,i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob,r.reqqty,Isnull((Select SUM(trfwhserviceqty) From ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm ON twd.trfwhserviceoid=twm.Trfwhserviceoid Where(twm.FromBranch=r.branch_code) AND r.reqdtloid=twd.reqdtloid AND twm.FromMtrLocOid=" & fromlocation.SelectedValue & " AND twd.reqoid=r.reqmstoid And twm.Trfwhservicestatus<>'Rejected'),0.00) QtyTW,(Select reqservicecat from QL_TRNREQUEST rq Where rq.reqoid=r.reqmstoid) reqservicecat From QL_TRNREQUESTDTL r Inner Join QL_mstitem i ON i.itemoid=r.itemoid Where r.branch_code='" & FromBranch.SelectedValue & "' UNION ALL " & _
                "/*Query TTS(Cabang A)=>TWS (Cabang A Ke B)=>TCS (Cabang B) => TWS(Cabang B Ke A)*/" & _
                "Select r.reqmstoid,r.reqdtloid ,i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob,Isnull((Select SUM(tcd.trntcservicedtlqty) From ql_trntcservicedtl tcd Where r.reqmstoid=tcd.reqoid AND r.reqdtloid=tcd.reqdtloid AND tcd.tomtrlocoid=" & fromlocation.SelectedValue & "),0.00) ReqQty,Isnull((Select SUM(trfwhserviceqty) From ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm ON twd.trfwhserviceoid=twm.Trfwhserviceoid Where twd.reqoid=r.reqmstoid AND r.reqdtloid=twd.reqdtloid AND twm.Trfwhservicetype='INTERNAL' AND twm.FromBranch=r.branch_code AND twm.FromMtrLocOid=" & fromlocation.SelectedValue & " And twm.Trfwhservicestatus<>'Rejected' AND twm.FromBranch='" & FromBranch.SelectedValue & "'),0.00) QtyTW,(Select reqservicecat From QL_TRNREQUEST rq Where rq.reqoid=r.reqmstoid) reqservicecat From QL_TRNREQUESTDTL r Inner Join QL_mstitem i ON i.itemoid=r.itemoid Where r.cmpcode='" & cmpcode & "' AND reqmstoid NOT IN (Select MSTREQOID from QL_TRNFINAL fm) UNION ALL" & _
                "/*Query TTS(Cabang A)=>TWS (Cabang A Ke B)=>TCS (Cabang B)=>Service Final (Cabang B)*/" & _
                " Select fs.reqoid reqmstoid,fs.reqdtloid ,Case (Select fm.typetts from QL_TRNFINAL fm Where fm.FINOID=fs.trnfinaloid AND fm.FINSTATUS='POST' AND fm.BRANCH_CODE='" & FromBranch.SelectedValue & "') When 'REPLACE' Then (Case When fs.itempartoid=fs.itemreqoid Then fs.itemreqoid Else fs.itempartoid End) Else fs.itemreqoid End itemoid,i.itemcode, i.itemdesc,(Select r.reqdtljob from QL_TRNREQUESTDTL r Where r.reqdtloid=fs.reqdtloid) reqdtljob,Case (Select fm.typetts from QL_TRNFINAL fm Where fm.FINOID=fs.trnfinaloid AND fm.FINSTATUS='POST' AND fm.BRANCH_CODE='" & FromBranch.SelectedValue & "') When 'REPLACE' Then (Case When fs.itempartoid=fs.itemreqoid Then fs.itemqty Else fs.itempartqty End) Else fs.itemqty End reqqty,Isnull((Select SUM(trfwhserviceqty) From ql_trfwhservicedtl twd Inner Join ql_trfwhservicemst twm ON twd.trfwhserviceoid=twm.Trfwhserviceoid Where twd.reqoid=fs.reqoid AND fs.reqdtloid=twd.reqdtloid AND twm.Trfwhservicetype='INTERNAL' AND twm.FromMtrLocOid=" & fromlocation.SelectedValue & " And twm.Trfwhservicestatus<>'Rejected'),0.00) QtyTW,'' reqservicecat From ql_mstitem i Inner Join QL_TRNFINALSPART fs ON Case (Select fm.typetts from QL_TRNFINAL fm Where fm.FINOID=fs.trnfinaloid AND fm.FINSTATUS='POST' AND fm.BRANCH_CODE='" & FromBranch.SelectedValue & "') When 'REPLACE' Then (Case When fs.itempartoid=fs.itemreqoid Then fs.itemreqoid Else fs.itempartoid End) Else fs.itemreqoid End=i.itemoid Where trnfinaloid IN (Select fm.FINOID from QL_TRNFINAL fm Where fm.FINOID=fs.trnfinaloid AND fm.FINSTATUS='POST' AND fm.BRANCH_CODE='" & FromBranch.SelectedValue & "' AND fm.locoidtitipan=" & fromlocation.SelectedValue & ") UNION ALL" & _
                "/*Query TTS(Cabang A)=>TWS (Cabang A Ke B)=>TCS (Cabang B)=>Servis Supp (Cabang B)=>Kembali Supp(Cabang B)*/" & _
                "Select r.reqmstoid,r.reqdtloid ,i.itemoid, i.itemcode, i.itemdesc, r.reqdtljob,Isnull((Select SUM(tcd.trntcserviceqty) From QL_trntcsuppdtl tcd Where tcd.reqoid=r.reqmstoid AND tcd.branch_code='" & FromBranch.SelectedValue & "' AND tcd.mtrlocoid=" & fromlocation.SelectedValue & "),0.00) ReqQty,Isnull((Select SUM(trntcserviceqty) From QL_trntcsuppdtl tcd Inner Join QL_trntcsuppmst tcm ON tcd.trntcserviceoid=tcm.trntcserviceoid Where tcd.reqoid=r.reqmstoid AND r.reqmstoid=tcd.reqoid AND tcd.trfwhserviceoid IN (Select twm.trfwhserviceoid From ql_trfwhservicemst twm Where twm.Trfwhservicetype='EXTERNAL' And twm.Trfwhservicestatus='POST') AND tcm.MtrLocOId=" & fromlocation.SelectedValue & "),0.00) QtyTW,(Select reqservicecat From QL_TRNREQUEST rq Where rq.reqoid=r.reqmstoid) reqservicecat From QL_TRNREQUESTDTL r Inner Join ql_mstitem i ON i.itemoid=r.itemoid Where r.cmpcode='" & cmpcode & "') Rq Where Isnull(ReqQty-QtyTW,0.00)>0.00 AND reqmstoid NOT IN (Select inv.MSTREQOID From QL_TRNINVOICE inv Where Rq.reqmstoid=inv.MSTREQOID) AND reqservicecat NOT IN ('M','Y') AND reqmstoid=" & reqoid.Text & " " & sWhere & " Order By reqmstoid Desc'"
            End If

            Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_itemlists")
            GVItemList.DataSource = objTable : GVItemList.DataBind()
            GVItemList.SelectedIndex = -1 : GVItemList.Visible = True
            Session("itemlistwhreturs") = objTable
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindPenerimaan(ByVal sWhere As String)
        Try
            sSql = "Select * from ( Select rq.reqoid, rq.reqcode, cu.custoid, reqservicecat, cu.custname, con.mtrlocoid, Convert(varchar(10),rq.reqdate, 103) reqdate, SUM(rqd.reqqty) ReqQty, SUM(con.qtyIn)-SUM(con.qtyOut) QtyTw, con.branch_code From QL_TRNREQUEST rq Inner Join QL_TRNREQUESTDTL rqd ON rqd.reqmstoid=rq.reqoid Inner Join QL_mstcust cu ON cu.custoid=rq.reqcustoid Inner Join (SELECT con.branch_code, con.mtrlocoid, qtyIn, qtyOut, conrefoid FROM QL_conmtr con ) con ON con.conrefoid=reqdtloid Where rq.reqstatus IN ('POST','APPROVED') AND rq.reqservicecat IN ('N') Group BY rq.reqoid, rq.reqcode, cu.custoid, reqservicecat, cu.custname, con.mtrlocoid, rq.reqdate, con.branch_code Having(Isnull(SUM(qtyIn), 0.00) - Isnull(Sum(qtyOut), 0.00) > 0.00)) req Where branch_code='" & FromBranch.SelectedValue & "' AND mtrlocoid=" & fromlocation.SelectedValue & " " & sWhere & " Order by req.reqcode"
            Dim objTable As DataTable = ckon.ambiltabel(sSql, "penerimaan")
            gvPenerimaan.DataSource = objTable : gvPenerimaan.DataBind()
            gvPenerimaan.SelectedIndex = -1 : gvPenerimaan.Visible = True
            Session("listpenerimaan") = objTable
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitCabang()
        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
        FillDDL(ToBranch, sSql)

        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FromBranch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FromBranch, sSql)
            Else
                FillDDL(FromBranch, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FromBranch, sSql)
        End If
    End Sub

    Private Sub fInitCabang()
        If DdlFCabang.SelectedValue = "FromBranch" Then
            sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(drCabang, sSql)
            ElseIf Session("UserLevel") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(drCabang, sSql)
                Else
                    FillDDL(drCabang, sSql)
                    drCabang.Items.Add(New ListItem("ALL", "ALL"))
                    drCabang.SelectedValue = "ALL"
                End If
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                FillDDL(drCabang, sSql)
                drCabang.Items.Add(New ListItem("ALL", "ALL"))
                drCabang.SelectedValue = "ALL"
            End If

        Else
            sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='cabang'"
            FillDDL(drCabang, sSql)
            drCabang.Items.Add(New ListItem("ALL", "ALL"))
            drCabang.SelectedValue = "ALL"
        End If
    End Sub

    Protected Sub FromBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsPostBack Then
            InitDDLLocation()
        End If
    End Sub

    Private Sub InitDDLLocation()
        'From Locotion
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & FromBranch.SelectedValue & "' and gengroup = 'cabang') AND a.genother6 IN ('TITIPAN')"
        FillDDL(fromlocation, sSql)

        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If
        'fromlocation.SelectedIndex = 0
    End Sub

    Private Sub InitDDLLocationTo()
        'To Locotion
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & ToBranch.SelectedValue & "' and gengroup = 'cabang') and a.genoid <> '" & fromlocation.SelectedValue & "' AND a.genother6 IN ('TITIPAN')"
        FillDDL(tolocation, sSql)
    End Sub

    Protected Sub ToBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLLocationTo()
        If tolocation.Items.Count = 0 Then
            showMessage("- Maaf, Lokasi gudang tujuan masih kosong..!!", 3)
            Exit Sub
        End If
    End Sub

    Protected Sub imbSeachPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSeachPenerimaan.Click
        If tolocation.Items.Count = 0 Then
            showMessage("- Maaf, Lokasi gudang tujuan masih kosong..!!", 3)
            Exit Sub
        End If
        panelPenerimaan.Visible = True : btnHidePenerimaan.Visible = True
        mpePenerimaan.Show() : BindPenerimaan("")
    End Sub

    Protected Sub gvPenerimaan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        btnHidePenerimaan.Visible = False : panelPenerimaan.Visible = False
        mpePenerimaan.Show()
        reqoid.Text = gvPenerimaan.SelectedDataKey(0).ToString
        txtPenerimaan.Text = gvPenerimaan.SelectedDataKey("reqcode")
        CProc.DisposeGridView(sender)

        panelItem.Visible = True : btnHideItem.Visible = True
        mpeItem.Show() : BindItems("")
    End Sub

    Protected Sub imbAddToListPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If txtPenerimaan.Text.Trim = "" Or penerimaanoid.Text = "" Then
            showMessage("No. Penerimaan tidak boleh kosong!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("penerimaandetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqcode", Type.GetType("System.String"))
                dtab.Columns.Add("custoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("custname", Type.GetType("System.String"))
                dtab.Columns.Add("reqdate", Type.GetType("System.String"))

                Session("penerimaandetail") = dtab
                sequence.Text = "1"
            Else
                dtab = Session("penerimaandetail")
                sequence.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("penerimaandetail")
        End If

        Dim objTable As DataTable
        objTable = Session("penerimaandetail")
        Dim dv As DataView = objTable.DefaultView

        If I_u2.Text = "new" Then
            dv.RowFilter = "reqoid = " & Integer.Parse(penerimaanoid.Text) & " "
        Else
            dv.RowFilter = "reqoid = " & Integer.Parse(penerimaanoid.Text) & " AND seq <> " & Integer.Parse(sequence.Text) & ""
        End If
        If dv.Count > 0 Then
            dv.RowFilter = ""
            showMessage("No. Penerimaan tidak bisa ditambahkan ke dalam list ! No. Penerimaan ini telah ada di dalam list !", 2)
            Exit Sub
        End If

        dv.RowFilter = ""

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(sequence.Text)
            drow("reqoid") = Integer.Parse(penerimaanoid.Text)
            drow("reqcode") = txtPenerimaan.Text

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(sequence.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()
            drow("reqoid") = Integer.Parse(penerimaanoid.Text)
            drow("reqcode") = txtPenerimaan.Text

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        'gvPenerimaanDetail.DataSource = dtab
        'gvPenerimaanDetail.DataBind()
        'Session("penerimaandetail") = dtab

        'sequence.Text = (gvPenerimaanDetail.Rows.Count + 1).ToString
        I_u2.Text = "new" : txtPenerimaan.Text = ""

        'gvPenerimaanDetail.SelectedIndex = -1
        'gvPenerimaanDetail.Columns(8).Visible = True

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If

        GVItemList.Visible = True
        BindItem(2)
    End Sub

    Protected Sub imbClearPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtPenerimaan.Text = "" : gvPenerimaan.Visible = False
    End Sub

    Protected Sub imbClearPenerimaanDetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClearPenerimaanDetail.Click
        I_u2.Text = "new" : txtPenerimaan.Text = ""
        txtPenerimaan.Text = ""


        If Session("penerimaandetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("penerimaandetail")
            sequence.Text = objTable.Rows.Count + 1
        Else
            sequence.Text = 1
        End If

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If

        'gvPenerimaanDetail.SelectedIndex = -1
        'gvPenerimaanDetail.Columns(8).Visible = True
    End Sub

    Protected Sub lbldelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("penerimaandetail") Is Nothing Then
            dtab = Session("penerimaandetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        'gvPenerimaanDetail.DataSource = dtab
        'gvPenerimaanDetail.DataBind()
        'Session("penerimaandetail") = dtab
        'If Session("itemdetail") Is True Then
        lbldeleteitem_Click(Nothing, Nothing)
        'End If


    End Sub

    Protected Sub lbldeleteitem_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton1 As System.Web.UI.WebControls.LinkButton
        Dim gvr1 As GridViewRow

        lbutton1 = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr1 = TryCast(lbutton1.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("reqoid = " & Integer.Parse(gvr1.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        'gvPenerimaanDetail.DataSource = dtab
        'gvPenerimaanDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objTable As DataTable = Session("itemdetail")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("itemdetail") = objTable
        GVItemDetail.DataSource = objTable
        GVItemDetail.DataBind()

        If GVItemDetail.Rows.Count = 0 Then
            FromBranch.Enabled = True : FromBranch.CssClass = "inpText"
            fromlocation.Enabled = True : fromlocation.CssClass = "inpText"
        End If
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("~\Transaction\trnTransferService.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send Approval this data?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Service"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            'DdlFCabang_SelectedIndexChanged(Nothing, Nothing)
            fInitCabang() : BindData()
            transferno.Text = GenerateID("ql_trfwhservicemst", cmpcode)
            transferdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitCabang() : InitDDLLocation()
            InitDDLLocationTo()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime()
            upduser.Text = "-" : updtime.Text = "-"

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                labelseq.Text = "1" : sequence.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        CbTanggal.Checked = False
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        'drCabang.SelectedValue = "ALL"
        'ddlStatus.SelectedValue = "ALL"
        FilterText.Text = "" : BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If
        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If
        BindData()
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            If reqoid.Text = "" Then
                showMessage("Pilih No. Penerimaan terlebih dahulu!", 2)
            Else
                panelItem.Visible = True : btnHideItem.Visible = True
                mpeItem.Show() : BindItems("")
            End If

            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = ""
        notedtl.Text = "" : qty.Text = ""
        reqdtloid.Text = "" : reqoid.Text = ""
        txtPenerimaan.Text = "" : labelsatuan1.Text = ""

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        'If rblflag.SelectedValue.ToString = "Purchase" Then
        GVItemList.PageIndex = e.NewPageIndex
        '    BindItem(1)
        'Else
        '    GVItemList.PageIndex = e.NewPageIndex
        '    BindItem(2)
        'End If
        BindItem(0)
        GVItemList.Visible = True
    End Sub

    Protected Sub GVItemList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemList.SelectedIndexChanged
        btnHideItem.Visible = False : panelItem.Visible = False
        mpeItem.Show()
        item.Text = GVItemList.SelectedDataKey("itemdesc").ToString
        labelitemoid.Text = Integer.Parse(GVItemList.SelectedDataKey("itemoid"))
        notedtl.Text = GVItemList.SelectedDataKey("reqdtljob").ToString
        qty.Text = ToMaskEdit(GVItemList.SelectedDataKey("OsQty"), 4)
        reqqty.Text = ToMaskEdit(GVItemList.SelectedDataKey("OsQty"), 4)
        reqdtloid.Text = Integer.Parse(GVItemList.SelectedDataKey("reqdtloid"))

        If Session("itemdetail") IsNot Nothing Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnTransferService.aspx?awal=true")
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        Dim dMsg As String = ""
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            dMsg &= "- Maaf, Item tidak boleh kosong..!!<br>"
        End If

        If qty.Text = "" Then : qty.Text = 0
            If qty.Text = 0 Then
                dMsg &= "- Maaf, Qty tidak boleh nol..!!<br> "
            End If
        End If

        If ToDouble(qty.Text) > ToDouble(reqqty.Text) Then
            dMsg &= "- Maaf, Qty transfer tidak boleh melebihi Qty Penerimaan barang..!!<br>" 
        End If

        Dim dtab As DataTable
        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("reqqty", Type.GetType("System.Double"))
                dtab.Columns.Add("reqdtljob", Type.GetType("System.String"))
                dtab.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
                dtab.Columns.Add("reqcode", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        Dim objTable As DataTable
        objTable = Session("itemdetail")
        Dim dv As DataView = objTable.DefaultView

        If I_u2.Text = "new" Then
            dv.RowFilter = "reqoid = " & Integer.Parse(reqoid.Text) & " AND reqdtloid=" & Integer.Parse(reqdtloid.Text) & ""
        Else
            dv.RowFilter = "reqoid = " & Integer.Parse(reqoid.Text) & " AND seq <> " & Integer.Parse(labelseq.Text) & " AND reqdtloid=" & Integer.Parse(reqdtloid.Text) & ""
        End If

        If dv.Count > 0 Then
            dv.RowFilter = ""
            dMsg &= "- Maaf, Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list..!!<br>" 
        End If
        dv.RowFilter = ""

        If dMsg <> "" Then
            showMessage(dMsg, 2)
            Exit Sub
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow
            drow("seq") = Integer.Parse(labelseq.Text)
            drow("reqoid") = Integer.Parse(reqoid.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("reqqty") = ToDouble(qty.Text)
            drow("qty") = ToDouble(reqqty.Text)
            drow("reqdtljob") = notedtl.Text
            drow("reqdtloid") = reqdtloid.Text
            drow("reqcode") = txtPenerimaan.Text
            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()
            drow("reqoid") = Integer.Parse(reqoid.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("reqqty") = ToDouble(qty.Text)
            drow("qty") = ToDouble(reqqty.Text)
            drow("reqdtljob") = notedtl.Text
            drow("reqdtloid") = reqdtloid.Text
            drow("reqcode") = txtPenerimaan.Text
            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        I_u2.Text = "new"
        notedtl.Text = "" : labelitemoid.Text = ""
        item.Text = "" : qty.Text = ""
        reqoid.Text = "" : reqdtloid.Text = ""
        txtPenerimaan.Text = "" : reqqty.Text = ""

        FromBranch.Enabled = False : FromBranch.CssClass = "inpTextDisabled"
        fromlocation.Enabled = False : fromlocation.CssClass = "inpTextDisabled"

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(8).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = ""
        item.Text = ""
        notedtl.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(8).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)
        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If
        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()
        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemDetail.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then 
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text
        reqoid.Text = GVItemDetail.SelectedDataKey("reqoid")
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.SelectedDataKey("itemdesc")
        qty.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("reqqty")), 1)
        reqqty.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("qty")), 1)
        txtPenerimaan.Text = GVItemDetail.SelectedDataKey("reqcode")
        reqdtloid.Text = GVItemDetail.SelectedDataKey("reqdtloid")
        notedtl.Text = GVItemDetail.SelectedDataKey("reqdtljob").Replace("&nbsp;", "")

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(8).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim
            Dim lMsg As String = ""
            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'" 
            If GetScalar(sSql) = 0 Then
                lMsg &= "- Maaf, Tanggal ini tidak dalam periode Open Stock..!!<br>"
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "SELECT ISNULL(SUM(qtyin)-SUM(qtyout),0.00) from QL_conmtr con Where con.refoid = " & dtab.Rows(j).Item("itemoid") & " And con.PeriodAcctg ='" & period & "' And con.mtrlocoid = '" & fromlocation.SelectedValue & "' And con.branch_code = '" & FromBranch.SelectedValue & "'"
                    saldoakhire = GetScalar(sSql)
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        lMsg &= "- Maaf, Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena Quantity sudah 0.00..!!<br> - Atau sudah pernah di TW Service internal, Silahkan cek pada laporan stok pada gudang " & fromlocation.SelectedItem.Text & "..!!<br>"
                    End If
                Next
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                If ToDouble(GetStrData("SELECT COUNT(*) FROM ql_trfwhservicemst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                    lMsg &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!<BR />"
                End If
            Else

                sSql = "SELECT trfwhservicestatus FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                Dim srest As String = GetStrData(sSql)
                If srest Is Nothing Or srest = "" Then
                    lMsg &= "- Maaf, Data transfer service sudah tersimpan !<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
                Else
                    If srest = "In Approval" Then
                        lMsg &= "- Maaf, Data twwarehouse tidak dapat diubah !<br />Periksa bila data telah dalam status 'In Approval'..!!<br>"
                    ElseIf srest = "Approved" Then
                        lMsg &= "- Maaf, Data tw tidak dapat diubah !<br />Periksa bila data telah dalam status 'Approved'..!!<br>"
                    End If
                End If

            End If

            If lMsg <> "" Then 
                showMessage(lMsg, 2)
                Exit Sub
            End If

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim objTrans As SqlClient.SqlTransaction
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                'Generate crdmtr ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

                Dim trfmtrmstoid As Integer = 0
                If i_u.Text = "new" Then
                    'Generate transfer master ID
                    sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trfwhservicemst' AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : trfmtrmstoid = xCmd.ExecuteScalar + 1
                End If
               
                'Generate transfer detail1 ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'ql_trfwhservicedtl' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1

                If trnstatus.Text = "In Approval" Then
                    sSql = "select lastoid from ql_mstoid where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : Dim approvaloid As Integer = xCmd.ExecuteScalar

                    sSql = "select tablename, approvaltype, approvallevel, approvaluser, approvalstatus from QL_approvalstructure where cmpcode = '" & cmpcode & "' and tablename = 'QL_trfwhservicemst' And branch_Code like '%" & ToBranch.SelectedValue & "%' order by approvallevel"

                    Dim dtab2 As DataTable = ckon.ambiltabel(sSql, "QL_approvalstructure")
                    If dtab2.Rows.Count > 0 Then
                        For i As Integer = 0 To dtab2.Rows.Count - 1
                            approvaloid = approvaloid + 1
                            sSql = "insert into ql_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) VALUES" & _
                            " ('" & cmpcode & "', " & approvaloid & ", '" & "PENERIMAAN BARANG" & Session("oid") & "_" & approvaloid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'ql_trfwhservicemst', '" & Session("oid") & "', 'In Approval', '0', '" & dtab2.Rows(i).Item("approvaluser") & "', '1/1/1900', '" & dtab2.Rows(i).Item("approvaltype") & "', '1', '" & dtab2.Rows(i).Item("approvalstatus") & "','" & ToBranch.SelectedValue & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        Next
                        sSql = "update ql_mstoid set lastoid = " & approvaloid & " where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        objTrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        showMessage("Tidak ada approval hierarki pada transfer service cabang '" & ToBranch.SelectedItem.ToString & "', silahkan kontak admin terlebih dahulu!", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    End If
                End If

                'Insert new record
                If i_u.Text = "new" Then
                    'Insert to master
                    sSql = "INSERT INTO ql_trfwhservicemst (cmpcode, Trfwhserviceoid, TrfwhserviceNo, Trfwhservicedate, Trfwhservicenote, createtime, createuser, updtime, upduser, finalappovaldatetime, finalapprovaluser, finalapprovalcode, canceluser, canceltime, cancelnote, Trfwhserviceres1, Trfwhserviceres2,Trfwhservicestatus,Trfwhservicerefno,Frombranch, ToBranch, FromMtrLocOid, ToMtrLocOid,Trfwhservicetype) VALUES " & _
                    "('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(note.Text.Trim) & "', '" & CDate(toDate(createtime.Text)) & "', '" & createuser.Text & "', CURRENT_TIMESTAMP,  '" & createuser.Text & "', CURRENT_TIMESTAMP, '', '', '', '','','','', '" & trnstatus.Text & "','" & Tchar(noref.Text) & "','" & FromBranch.SelectedValue & "','" & ToBranch.SelectedValue & "','" & fromlocation.SelectedValue & "', '" & tolocation.SelectedValue & "', 'INTERNAL')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    'Update lastoid QL_trntrfmtrmst
                    sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'ql_trfwhservicemst' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                Else

                    'Update master record
                    sSql = "UPDATE ql_trfwhservicemst SET TrfwhserviceNo = '" & transferno.Text & "', TrfwhserviceNote = '" & Tchar(note.Text.Trim) & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', finalappovaldatetime = CURRENT_TIMESTAMP, finalapprovaluser = '" & Session("UserID") & "', Trfwhservicestatus = '" & trnstatus.Text & "', frombranch = '" & FromBranch.SelectedValue & "' , tobranch = '" & ToBranch.SelectedValue & "', Frommtrlocoid = '" & fromlocation.SelectedValue & "', tomtrlocoid = '" & tolocation.SelectedValue & "', trfwhservicerefno = '" & Tchar(noref.Text) & "' WHERE Trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnrequestdtl SET reqdtlres1='' WHERE cmpcode='" & cmpcode & "' AND reqdtloid IN (SELECT reqdtloid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_trnrequest SET reqres1='' WHERE cmpcode='" & cmpcode & "' AND reqoid IN (SELECT reqoid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    'Delete detail
                    Dim smbrg As DataTable = Session("penerimaandetail")
                    sSql = "DELETE FROM ql_trfwhservicedtl WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                'Insert to detail
                If Not Session("itemdetail") Is Nothing Then
                    Dim objTable As DataTable = Session("itemdetail")
                    For i As Integer = 0 To objTable.Rows.Count - 1
                        If i_u.Text = "edit" Then
                            trfmtrmstoid = Integer.Parse(Session("oid"))
                        End If

                        sSql = " INSERT INTO ql_trfwhservicedtl (cmpcode, trfwhserviceoid, trfwhservicedtloid, trfwhservicedtlseq, reqoid, reqdtloid, itemoid, trfwhserviceqty, trfwhservicedtlnote, trfwhservicedtlres1) " & _
                        "VALUES ('" & cmpcode & "', " & trfmtrmstoid & "," & trfmtrdtloid & ", " & objTable.Rows(i).Item("seq") & ", " & objTable.Rows(i).Item("reqoid") & ", " & objTable.Rows(i).Item("reqdtloid") & ", " & objTable.Rows(i).Item("itemoid") & ", " & objTable.Rows(i).Item("reqqty") & ", '" & Tchar(objTable.Rows(i).Item("reqdtljob")) & "','')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If ToDouble(objTable.Rows(i).Item("reqqty")) >= ToDouble(objTable.Rows(i).Item("qty")) Then

                            sSql = "UPDATE QL_trnrequestdtl SET reqdtlres1 ='Complete', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & cmpcode & "' AND reqdtloid=" & objTable.Rows(i).Item("reqdtloid").ToString
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = "UPDATE QL_trnrequest SET reqres1='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & cmpcode & "' AND reqoid=" & objTable.Rows(i).Item("reqoid").ToString & " AND (SELECT COUNT(*) FROM QL_trnrequestdtl WHERE cmpcode='" & cmpcode & "' AND reqdtlres1='' AND reqmstoid=" & objTable.Rows(i).Item("reqoid").ToString & " AND reqdtloid <>" & objTable.Rows(i).Item("reqdtloid").ToString & ")=0"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                        trfmtrdtloid += 1
                    Next
                    'Update lastoid ql_InTransferDtl
                    sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & " WHERE tablename = 'ql_trfwhservicedtl' and cmpcode = '" & cmpcode & "'" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                End If
                objTrans.Commit() : conn.Close()

            Catch ex As Exception
                trnstatus.Text = "In Process"
                objTrans.Rollback() : conn.Close()
                showMessage(ex.ToString & "<br />" & sSql, 1)
                Exit Sub
            End Try
            Response.Redirect("trnTransferService.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "UPDATE QL_TRNREQUESTDTL SET reqdtlres1 ='' WHERE cmpcode='" & cmpcode & "' AND reqdtloid IN (SELECT reqdtloid FROM QL_trfwhservicemst WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid =" & Integer.Parse(Session("oid")) & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_TRNREQUEST SET reqres1='' WHERE cmpcode='" & cmpcode & "' AND reqoid IN (SELECT reqoid FROM QL_trfwhservicedtl WHERE cmpcode='" & cmpcode & "' AND trfwhserviceoid=" & Integer.Parse(Session("oid")) & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "SELECT trfwhservicestatus FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer Service tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "In Approval" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer Service tidak dapat dihapus !<br />Periksa bila data telah diposting oleh user lain", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trfwhservicemst WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trfwhservicedtl WHERE trfwhserviceoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()


            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnTransferService.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "In Approval"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        InitDDLLocationTo()
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        'penerimaan
        reqoid.Text = "" : sequence.Text = ""

        'item
        I_u2.Text = "new" : labelitemoid.Text = ""
        item.Text = "":notedtl.Text = ""
        labelsatuan1.Text = "" : labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        If gvPenerimaan.Visible = True Then
            gvPenerimaan.DataSource = Nothing
            gvPenerimaan.DataBind()
            gvPenerimaan.Visible = False
        End If
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            'Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            'Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim twOid As Integer = sender.ToolTip
            Dim sWhere As String = " WHERE twm.Trfwhserviceoid = " & Integer.Parse(twOid) & ""
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "PrintOutTWInt.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", Integer.Parse(twOid))
            'report.SetParameterValue("userID", Session("UserID"))
            'report.PrintOptions.PaperSize = PaperSize.PaperA5

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TransferServicePrintOut_" & twOid & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnTransferService.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        gvMaster.PageIndex = e.NewPageIndex
        BindData()
    End Sub

    Private Sub bindDataPO()
        sSql = "select a.transferno trnTrfToReturNo, a.trfmtrdate AS trntrfdate,trfmtrnote,ToMtrlocOid,fromMtrBranch,toMtrBranch ,(select sum(qty_to) From QL_trntrfmtrdtl Where cmpcode = a.cmpcode and trfmtrmstoid = a.trfmtrmstoid) totalqty, ((select isnull(sum(i.qty_to), 0) From ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) From ql_trfwhservicemst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid Where h.trnTrfToReturNo = a.transferno)) totaluse,a.FromMtrlocoid From QL_trntrfmtrmst a Where a.cmpcode = '" & cmpcode & "' And a.status = 'Approved' And toMtrBranch = '" & Session("branch_id") & "' And ((select sum(qty_to) From QL_trntrfmtrdtl Where cmpcode = a.cmpcode and trfmtrmstoid = a.trfmtrmstoid)-((select isnull(sum(i.qty_to), 0) from ql_trnTrfFromReturmst h inner join ql_trnTrfFromReturDtl i on h.cmpcode = i.cmpcode and h.trnTrfFromReturoid = i.trnTrfFromReturoid Where h.trnTrfToReturNo = a.transferno)+(select isnull(sum(i.qty_to), 0) From ql_trfwhservicemst h inner join ql_InTransferDtl i on h.cmpcode = i.cmpcode and h.InTransferoid = i.InTransferoid where h.trnTrfToReturNo = a.transferno))) > 0 And a.transferno like '%" & Tchar(noref.Text) & "%'"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "polist")
        GVpo.DataSource = dtab
        GVpo.DataBind()
        Session("polistwhsupplier") = dtab
    End Sub

    Protected Sub GVpo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVpo.PageIndexChanging
        If Not Session("polistwhsupplier") Is Nothing Then
            GVpo.DataSource = Session("polistwhsupplier")
            GVpo.PageIndex = e.NewPageIndex
            GVpo.DataBind()
        End If
    End Sub

    Protected Sub GVpo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVpo.SelectedIndexChanged
        noref.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        pooid.Text = GVpo.Rows(GVpo.SelectedIndex).Cells(1).Text
        tolocation.SelectedValue = GVpo.SelectedDataKey("ToMtrlocOid")
        ToBranch.SelectedValue = GVpo.SelectedDataKey("toMtrBranch")
        FromBranch.SelectedValue = GVpo.SelectedDataKey("fromMtrBranch")
        FromLoc.Text = GVpo.SelectedDataKey("FromMtrlocoid")
        GVpo.Visible = False : GVpo.DataSource = Nothing
        GVpo.DataBind()
        Session("polistwhsupplier") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()
        Session("itemdetail") = Nothing
    End Sub

    Protected Sub imbFindPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindPenerimaan.Click
        panelPenerimaan.Visible = True
        btnHidePenerimaan.Visible = True
        mpePenerimaan.Show()
        Dim sWhere As String = " AND " & DDLFilterPenerimaan.SelectedValue & " LIKE '%" & Tchar(txtFilterPenerimaan.Text) & "%'"
        BindPenerimaan(sWhere) 
    End Sub

    Protected Sub imbViewPenerimaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewPenerimaan.Click
        panelPenerimaan.Visible = True
        btnHidePenerimaan.Visible = True
        mpePenerimaan.Show() : BindPenerimaan("")
        txtFilterPenerimaan.Text = "" : DDLFilterPenerimaan.SelectedIndex = 0
    End Sub

    Protected Sub lkbClosePenerimaan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbClosePenerimaan.Click
        panelPenerimaan.Visible = False : btnHidePenerimaan.Visible = False
        CProc.DisposeGridView(gvPenerimaan)
    End Sub

    Protected Sub imbFindItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindItem.Click
        panelItem.Visible = True
        btnHideItem.Visible = True
        mpeItem.Show()

        Dim sWhere As String = " AND " & DDLFilterItem.SelectedValue & " LIKE '%" & Tchar(txtFilterItem.Text) & "%'"
        BindItems(sWhere)
    End Sub

    Protected Sub imbViewItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbViewItem.Click
        panelItem.Visible = True
        btnHideItem.Visible = True
        mpeItem.Show() : BindItems("")
        txtFilterItem.Text = "" : DDLFilterItem.SelectedIndex = 0
    End Sub

    Protected Sub gvMaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaster.SelectedIndexChanged
        Response.Redirect("trnTransferService.aspx?branch_code=" & gvMaster.SelectedDataKey("branch_code").ToString & "&oid=" & gvMaster.SelectedDataKey("trfwhserviceoid") & "")
    End Sub

    Protected Sub DdlFCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlFCabang.SelectedIndexChanged
        fInitCabang()
    End Sub

    Protected Sub gvPenerimaan_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPenerimaan.PageIndexChanging
        panelPenerimaan.Visible = True
        btnHidePenerimaan.Visible = True
        gvPenerimaan.PageIndex = e.NewPageIndex
        mpePenerimaan.Show()
        Dim sWhere As String = " AND " & DDLFilterPenerimaan.SelectedValue & " LIKE '%" & Tchar(txtFilterPenerimaan.Text) & "%'"
        BindPenerimaan(sWhere)
    End Sub

    Protected Sub lkbCloseItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseItem.Click
        btnHideItem.Visible = False
        panelItem.Visible = False : btnHideItem.Visible = False
        mpeItem.Show()
    End Sub
#End Region
End Class
