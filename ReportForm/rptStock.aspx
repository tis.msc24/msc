<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptStock.aspx.vb" Inherits="rptStock" Title="MSC - Laporan Mutasi Stock" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Laporan Stok" Width="182px"></asp:Label></th>
        </tr>
<tr>
<th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE id="tb"><TBODY><TR><TD id="TD28" align=left runat="server"><asp:Label id="Label2" runat="server" Width="102px" Text="Tipe Laporan "></asp:Label></TD><TD id="td29" align=center runat="server">:</TD><TD id="tdtipe3" align=left colSpan=3 runat="server"><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" AutoPostBack="True"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList> <asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" Visible="False"></asp:DropDownList>&nbsp;</TD></TR><TR><TD id="Td1" align=left runat="server"><asp:Label id="Label12" runat="server" Text="Periode " Visible="False"></asp:Label></TD><TD id="Td2" align=center runat="server"></TD><TD id="TD44" align=left colSpan=3 runat="server"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" AutoPostBack="True" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" Visible="False">(dd/MM/yyyy)</asp:Label></TD></TR><TR id="trtgl" runat="server" visible="false"><TD id="tdpr1" align=left runat="server"><asp:Label id="Label6" runat="server" Text="Periode "></asp:Label></TD><TD id="tdpr2" align=left runat="server"><asp:Label id="Label10" runat="server" Text=":"></asp:Label></TD><TD id="tdpr3" align=left colSpan=3 runat="server"><asp:DropDownList id="DDLMonth" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList>&nbsp;<asp:DropDownList id="DDLYear" runat="server" CssClass="inpTextDisabled" Enabled="False">
                            </asp:DropDownList>&nbsp;<asp:Label id="Label11" runat="server" Text="-"></asp:Label></TD></TR><TR><TD id="Td3" align=left runat="server" visible="true"><asp:Label id="Label9" runat="server" Text="Cabang"></asp:Label></TD><TD id="Td4" align=left runat="server" visible="true">:</TD><TD id="Td5" align=left runat="server" visible="true"><asp:DropDownList id="dd_branch" runat="server" Width="200px" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD id="tdperiod3" align=left runat="server" visible="true"><asp:Label id="Label1" runat="server" Text="Lokasi "></asp:Label></TD><TD id="Td6" align=left runat="server" visible="true">:</TD><TD id="tdperiod4" align=left runat="server" visible="true"><asp:DropDownList id="ddlLocation" runat="server" Width="250px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD id="TD31" align=left runat="server"><asp:CheckBox id="PIC" runat="server" Text="Filter PIC"></asp:CheckBox></TD><TD id="TD32" align=left runat="server" Visible="true">:</TD><TD id="TD30" align=left runat="server"><asp:DropDownList id="ddlPIC" runat="server" Width="150px" CssClass="inpText"></asp:DropDownList> <asp:Label id="itemoid" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server">Jenis Barang</TD><TD align=left runat="server" Visible="true">:</TD><TD align=left runat="server"><asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Item / Barang "></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="ItemName" runat="server" Width="255px" CssClass="inpTextDisabled" Enabled="False" Rows="2" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton
        ID="btnSearchItem" runat="server" Height="26px" ImageAlign="Top" ImageUrl="~/Images/search.gif"
        Width="25px" />&nbsp;<asp:ImageButton ID="imbEraseMat" runat="server" ImageAlign="Top"
            ImageUrl="~/Images/erase.bmp" Width="25px" /></TD></TR><TR><TD id="Td10" align=left colSpan=5 runat="server" visible="true"><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" HasZoomFactorList="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False" HasViewList="False" HasSearchButton="False" HasExportButton="False" DisplayGroupTree="False" HasPrintButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CEdateAwal" runat="server" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEdateAkhir" runat="server" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> 
</ContentTemplate>
        <Triggers>
<asp:PostBackTrigger ControlID="ibPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
</Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upListMat" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlListMat" runat="server" CssClass="modalBox" Visible="False" Width="900px">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Label ID="lblTitleListMat" runat="server" Font-Bold="True" Font-Size="Large"
                                    Font-Underline="False" Text="List Katalog"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:Panel ID="pnlFindListMat" runat="server" DefaultButton="btnFindListMat" Width="100%">
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Filter :
                                                    <asp:DropDownList ID="FilterDDLListMat" runat="server" CssClass="inpText" Width="100px">
                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                        <asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="FilterTextListMat" runat="server" CssClass="inpText" Width="200px"></asp:TextBox>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Jenis :
                                                    <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    Group :
                                                    <asp:DropDownList ID="DDLItemGroup" runat="server" CssClass="inpText" Width="150px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    Status : <asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList>
                                    <asp:ImageButton ID="btnFindListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                    <asp:ImageButton ID="btnAllListMat" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:ImageButton ID="btnALLSelectItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                                    ID="btnSelectNoneItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                                        ID="btnViewCheckedItem" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <asp:UpdateProgress ID="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat">
                                    <ProgressTemplate>
                                        <table style="width: 200px">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 10pt; color: purple">
                                                    Load Data, Please Wait ...</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Image ID="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" /></td>
                                            </tr>
                                        </table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label" colspan="3">
                                Show
                                <asp:TextBox ID="tbData" runat="server" CssClass="inpText" Width="25px"></asp:TextBox>
                                Data Per Page
                                <asp:LinkButton ID="lbShowData" runat="server">>> View</asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td align="center" class="Label" colspan="3">
                                <div style="overflow-y: scroll; width: 100%; height: 247px; background-color: beige">
                                    <asp:GridView ID="gvListMat" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CellPadding="4" DataKeyNames="itemcode" ForeColor="#333333" GridLines="None"
                                        PageSize="5" Width="99%">
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                <HeaderStyle CssClass="gvhdr" Font-Strikeout="False" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="merk" HeaderText="Merk">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="satuan3" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemflag" HeaderText="Status">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                            HorizontalAlign="Right" />
                                        <EmptyDataTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="Klik tombol View All atau Find untuk tampilkan data"></asp:Label>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView>
                                </div>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <asp:LinkButton ID="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton>
                                -
                                <asp:LinkButton ID="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                        </tr>
                    </tbody>
                </table>
                <ajaxToolkit:ModalPopupExtender ID="mpeListMat" runat="server" BackgroundCssClass="modalBackground"
                    Drag="True" PopupControlID="pnlListMat" PopupDragHandleControlID="lblTitleListMat"
                    TargetControlID="btnHideListMat">
                </ajaxToolkit:ModalPopupExtender>
            </asp:Panel>
            <asp:Button ID="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
</tr>
    </table>
</asp:Content>

