Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstCustGroup
    Inherits System.Web.UI.Page

#Region "Variable"
    Private Regex As Regex
    Dim oMatches As MatchCollection
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Dim xCmd As New SqlCommand("", conn)
    Dim xReader As SqlDataReader
    Dim sSql As String
    Dim sFax1 As String
    Dim sFax2 As String
    Dim sValue As String
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal message As String, ByVal caption As String, ByVal iType As Integer)
        If iType = 1 Then '-> error
            imIcon.ImageUrl = "~/Images/error.jpg"
        ElseIf iType = 2 Then '-> warning
            imIcon.ImageUrl = "~/Images/warn.png"
        ElseIf iType = 3 Then '-> information
            imIcon.ImageUrl = "~/Images/information.png"
        Else
            imIcon.ImageUrl = "~/Images/error.jpg"
        End If

        validasi.Text = message
        lblCaption.Text = caption
        PanelErrMsg.Visible = True
        btnExtender.Visible = True
        MPEError.Show()
    End Sub

    Private Sub fillFaxField(ByVal faxString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal faxCode As TextBox)
        Dim sTemp() As String = faxString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                faxCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Private Sub fillPhoneField(ByVal phoneString As String, ByVal interCode As TextBox, ByVal localCode As TextBox, ByVal phoneCode As TextBox)
        Dim sTemp() As String = phoneString.Split(".")
        For i As Integer = 0 To sTemp.Length - 1
            If i = 0 Then
                interCode.Text = sTemp(0)
            ElseIf i = 1 Then
                localCode.Text = sTemp(1)
            ElseIf i = 2 Then
                phoneCode.Text = sTemp(2)
            End If
        Next
    End Sub

    Sub initProvince(ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%'   ORDER BY gendesc"
        FillDDL(custprovoid, sSql)
        FillDDL(consprovince, sSql)

        sSql = "SELECT genoid FROM QL_mstgen WHERE gengroup IN ('PROVINCE') AND (genother1='" & sCountry & "') AND cmpcode like '%" & CompnyCode & "%' and gendesc like 'BALI'  ORDER BY gendesc"
        custprovoid.SelectedValue = sSql
        consprovince.SelectedValue = sSql
    End Sub

    Sub initCity(ByVal sProv As String, ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('CITY') AND (genother2='" & sProv & "' ) AND (genother1='" & sCountry & "') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(custcityoid, sSql)
    End Sub

    Sub initConsCity(ByVal sProv As String, ByVal sCountry As String)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE gengroup IN ('CITY') AND (genother2='" & sProv & "' ) AND (genother1='" & sCountry & "') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(conscity, sSql)
    End Sub

    Sub initCountry()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)
    End Sub

    Sub initPrefix()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXCOMPANY') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(precustname, sSql)
    End Sub

    Sub initConTitle()
        sSql = "SELECT genoid, gendesc, gengroup FROM QL_mstgen WHERE gengroup IN ('PREFIXPERSON') AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(ddlconttitle, sSql)
        FillDDL(ddlconttitle2, sSql)
        FillDDL(ddlconttitle3, sSql)
        FillDDL(conscpprefix, sSql)
    End Sub

    Sub initCurr()
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custdefaultcurroid, sSql)
        FillDDL(custcreditlimitcurroid, sSql)
    End Sub

    Sub checkCityList()
        Try
            If custcityoid.Items.Count = 0 Then
                'cpcityphonecode.Text = "" 
                Exit Sub
            Else
                Try
                    initCityPhoneCode(custcityoid.SelectedValue)
                Catch ex As Exception
                    showMessage(ex.ToString, CompnyName & " - Error", 1)
                End Try
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - Error", 1)
        End Try
    End Sub

    Sub initCityPhoneCode(ByVal ctoid As Char)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, '') FROM QL_mstgen WHERE (gengroup IN ('CITY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'cpcityphonecode.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Sub initCountryPhoneCode(ByVal ctoid As Int32)
        'City Phone Code
        sSql = "SELECT ISNULL(genother3, ''), genoid, gengroup FROM QL_mstgen WHERE (gengroup IN ('COUNTRY')) AND genoid ='" & ctoid & "' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        cpcountryphonecode.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Sub Outlate()
        sSql = "SELECT gencode from QL_mstgen where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'"
        KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
    End Sub

    Sub initCOA()
        FillDDLAcctg(araccount, "VAR_AR", branchID.SelectedValue)
        FillDDLAcctg(returaccount, "VAR_RETJUAL", branchID.SelectedValue)
    End Sub

    Sub initDDL()
        initPrefix()
        initConTitle()
        initCurr()

        Dim swhere As String = ""
        swhere = " and gendesc like 'INDONESIA%'  "
        sSql = "SELECT genoid, gendesc gengroup FROM QL_mstgen WHERE gengroup IN ('COUNTRY') AND cmpcode LIKE '%" & CompnyCode & "%' " & swhere & " ORDER BY gendesc"
        FillDDL(custcountryoid, sSql)
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personstatus in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC like '%SALES%') "
        FillDDL(spgOid, sSql)
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE (gengroup IN ('PAYTYPE')) AND cmpcode LIKE '%" & CompnyCode & "%'"
        FillDDL(custpaytermdefaultoid, sSql)

        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' and  gendesc='cash' AND cmpcode LIKE '%" & CompnyCode & "%'"
        Else
            sSql = "select genoid, gendesc from QL_mstgen where gengroup like 'PAYTYPE' AND cmpcode LIKE '%" & CompnyCode & "%'"
        End If
        FillDDL(dd_timeofpayment, sSql)
        initCountryPhoneCode(custcountryoid.SelectedValue)
        checkCityList()
        sSql = "SELECT gencode from QL_mstgen where gengroup = 'CABANG' and genoid = '" & branchID.SelectedValue & "'"
        KodeOutlate.Text = cKoneksi.ambilscalar(sSql)
        
    End Sub

    Public Sub generateCustID()
        sSql = "SELECT (lastoid+1) FROM QL_mstoid WHERE tablename LIKE '%QL_mstcust%' AND cmpcode LIKE '%" & CompnyCode & "%'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        custoid.Text = xCmd.ExecuteScalar
        conn.Close()
    End Sub

    Public Sub clearItems()
        custcode.Text = ""
        custname.Text = ""
        custaddr.Text = ""
        custpostcode.Text = ""
        'phoneintcode1.Text = "" : phoneintcode2.Text = ""
        'phoneintcode3.Text = "" : phoneintcode4.Text = ""
        'phonelocalcode1.Text = "" : phonelocalcode2.Text = ""
        'phonelocalcode3.Text = "" : phonelocalcode4.Text = ""
        phone1.Text = "" : phone2.Text = "" : phone3.Text = ""
        custfax1.Text = "" : CustFax2.Text = ""
        custemail.Text = "" : custwebsite.Text = ""
        notes.Text = ""
        custnpwp.Text = ""
        UpdUser.Text = Session("UserId")
        UpdTime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
        precustname.SelectedIndex = 0
        custcityoid.SelectedIndex = 0
        custprovoid.SelectedIndex = 0
        custcountryoid.SelectedIndex = 0
        custflag.SelectedIndex = 0
        custpaytermdefaultoid.SelectedIndex = 0
        custdefaultcurroid.SelectedIndex = 0
        custcreditlimitcurroid.SelectedIndex = 0
        custcreditlimit.Text = ""
    End Sub

    Private Sub bindCust(ByVal sWhere As String)
        sSql = "select custgroupoid, Case custgroupcode when '' then  CAST(custgroupoid as varchar(10)) else custgroupcode End custgroupcode , custgroupname, custgroupaddr, g.gendesc as city, g1.gendesc cabang, ISNULL(phone1,0) phone,custgroupflag, notes, custgroupcreditlimitrupiah creditlimit, custgroupcreditlimitusagerupiah creditusage, case when custgroupcreditlimitusagerupiah>custgroupcreditlimitrupiah then (custgroupcreditlimitusagerupiah-custgroupcreditlimitrupiah) * -1 else 0 end creditover,pin_bb, flagcustomer from QL_mstcustgroup cg inner join QL_mstgen g on g.genoid = cg.custgroupcityoid left join QL_mstgen g1 on g1.gencode = cg.branch_code and g1.gengroup = 'CABANG' WHERE cg.cmpcode LIKE '" & CompnyCode & "' " & sWhere & " ORDER BY cg.custgroupname "
        Dim xTableItem1 As DataTable = cKoneksi.ambiltabel(sSql, "customer")
        GVmstcust.Visible = True
        GVmstcust.DataSource = xTableItem1
        GVmstcust.DataBind()
        GVmstcust.SelectedIndex = -1
    End Sub

    Sub fillTextBox(ByVal idPage As Integer)
        sSql = "SELECT custgroupoid, Case custgroupcode when '' then  CAST(custgroupoid as varchar(10)) Else custgroupcode End custgroupcode,timeofpayment ,custgroupname, custgroupflag, custgroupaddr, custgroupcityoid, custgroupprovoid, custgroupcountryoid, custgrouppostcode, phone1, phone2, phone3,custgroupfax1, custgroupemail, custgroupwebsite, custgroupnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, iSNULL(custgroupcreditlimitrupiah,0.0000) custgroupcreditlimitrupiah, isnull(custgroupcreditlimitusagerupiah,0.0000) custgroupcreditlimitusagerupiah,notes, createuser, upduser, updtime, prefixcmp, prefixcp1, prefixcp2, prefixcp3, custbank, custgrouppassportname, custgrouppassportno, custgroupbirthdate,salesoid,custgroupacctgoid,coa_retur,pin_bb, flagcustomer,branch_code FROM QL_mstcustgroup WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & idPage & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = "" : Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read
                fDDLBranch()
                branchID.SelectedValue = xReader("branch_code").ToString
                custgroupoid.Text = xReader("custgroupoid")
                dd_timeofpayment.SelectedValue = xReader("timeofpayment").ToString
                Session("creditlimit") = ToMaskEdit(xReader("custgroupcreditlimitrupiah"), 4)
                custcode.Text = xReader("custgroupcode").ToString.Trim
                custflag.Text = xReader("custgroupflag").ToString.Trim
                custaddr.Text = xReader("custgroupaddr").ToString.Trim
                custcountryoid.SelectedValue = xReader("custgroupcountryoid")
                custpostcode.Text = xReader("custgrouppostcode").ToString.Trim
                phone1.Text = xReader("phone1").ToString
                phone2.Text = xReader("phone2").ToString
                phone3.Text = xReader("phone3").ToString
                custfax1.Text = xReader("custgroupfax1").ToString
                lastsalesdate.Text = GetStrData("select top 1 trnjualdate from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & custoid.Text & "' order by trnjualdate desc")
                If lastsalesdate.Text = "?" Then
                    lastsalesdate.Text = "-"
                End If
                txtage.Text = GetStrData("select top 1 DATEDIFF(d, trnjualdate , GETDATE()) [umur sales]  from QL_trnjualmst where trnjualstatus = 'POST' and trncustoid='" & custoid.Text & "' order by trnjualdate desc")
                If txtage.Text = "?" Then
                    txtage.Text = "-"
                End If
                custemail.Text = xReader("custgroupemail").ToString.Trim
                custwebsite.Text = xReader("custgroupwebsite").ToString.Trim
                custnpwp.Text = xReader("custgroupnpwp").ToString.Trim
                contactperson1.Text = xReader("contactperson1").ToString.Trim
                contactperson2.Text = xReader("contactperson2").ToString.Trim
                contactperson3.Text = xReader("contactperson3").ToString.Trim
                'statusar.SelectedValue = xReader("statusar").ToString.Trim
                phonecontactperson1.Text = xReader("phonecontactperson1").ToString.Trim
                phonecontactperson2.Text = xReader("phonecontactperson2").ToString.Trim
                phonecontactperson3.Text = xReader("phonecontactperson3").ToString.Trim
                notes.Text = Trim(xReader("notes")).ToString
                UpdUser.Text = xReader("upduser").ToString.Trim
                UpdTime.Text = Format(xReader("updtime"), "dd/MM/yyyy HH:mm:ss")
                initPrefix()
                precustname.SelectedValue = xReader("PREFIXCMP").ToString

                custname.Text = xReader("custgroupname").ToString.Trim.Replace("," & precustname.SelectedItem.Text, "")

                custprovoid.Items.Clear()
                initProvince(custcountryoid.SelectedValue)
                custprovoid.SelectedValue = xReader("custgroupprovoid")
                custcityoid.Items.Clear()
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
                initConsCity(consprovince.SelectedValue, custcountryoid.SelectedValue)
                custcityoid.SelectedValue = xReader("custgroupcityoid")

                custcreditlimitrupiah.Text = ToMaskEdit(ToDouble(xReader("custgroupcreditlimitrupiah")), 4)
                custcreditlimitusagerupiah.Text = ToMaskEdit(ToDouble(xReader("custgroupcreditlimitusagerupiah")), 4)

                sisacreditlimitrupiah.Text = ToMaskEdit(ToDouble(xReader("custgroupcreditlimitrupiah")) - ToDouble(xReader("custgroupcreditlimitusagerupiah")), 4)

                ddlconttitle.SelectedValue = xReader("prefixcp1").ToString
                ddlconttitle2.SelectedValue = xReader("prefixcp2").ToString
                ddlconttitle3.SelectedValue = xReader("prefixcp3").ToString
                namapaspor.Text = xReader("custgrouppassportname").ToString
                nopaspor.Text = xReader("custgrouppassportno").ToString
                bankrekening.Text = xReader("custbank").ToString
                spgOid.SelectedValue = xReader("salesoid").ToString

                If xReader("custgroupbirthdate") <> "1/1/1900" Then
                    tgllahir.Text = Format(xReader("custgroupbirthdate"), "dd/MM/yyyy")
                End If

                If dd_timeofpayment.SelectedValue = 34 Then
                    custcreditlimitrupiah.Enabled = True
                    'custcreditlimitrupiah.CssClass = "inpTextDisabled"
                Else
                    custcreditlimitrupiah.Enabled = True
                End If
                initCOA()
                araccount.SelectedValue = xReader("custgroupacctgoid").ToString
                returaccount.SelectedValue = xReader("coa_retur").ToString
                pin_bb.Text = xReader("pin_bb").ToString
                cbCustomer.Checked = xReader("flagcustomer").ToString
            End While
        End If

        BindConsigneeData(idPage)
    End Sub

    Sub CreditLimit(ByVal idPage As Integer)
        'sSql = "SELECT c.cmpcode, c.custoid, c.custcode, c.custname, c.custflag, c.custaddr, c.custcityoid, c.custprovoid, c.custcountryoid, c.custpostcode, c.phone1, c.phone2, c.phone3, c.custfax1, c.custfax2, c.custemail, c.custwebsite, c.custnpwp, c.contactperson1, c.contactperson2, c.contactperson3, c.statusar, c.phonecontactperson1, c.phonecontactperson2, c.phonecontactperson3, c.notes, c.upduser, c.updtime, c.prefixcmp, c.prefixcp1, c.prefixcp2, c.prefixcp3, (select m1.genother3 from ql_mstgen m1 where c.custcountryoid=m1.genoid) as cdphonecountry, (select m2.genother3 from ql_mstgen m2 where c.custcityoid=m2.genoid) as cdphonecity, custgroup FROM QL_mstcust c WHERE c.cmpcode='" & CompnyCode & "' AND c.custoid=" & idPage & ""

        sSql = "SELECT timeofpayment FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & idPage & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xReader = xCmd.ExecuteReader
        Dim supname As String = ""
        Dim contname As String = ""
        If xReader.HasRows Then
            While xReader.Read

                dd_timeofpayment.SelectedValue = xReader("timeofpayment")

            End While
        End If

    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(branchID, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(branchID, sSql)
            Else
                FillDDL(branchID, sSql) 
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(branchID, sSql) 
        End If
    End Sub
#End Region

#Region "Function"
    Private Function generateCustCode(ByVal custname As String) As String
        Dim retVal As String = ""
        Dim custPrefix As String = custname.Substring(0, 1) ' 1 karakter nama CUSTOMER
        sSql = "select custgroupcode from QL_mstcustgroup where custgroupcode like '" & custPrefix & "%' order by custgroupcode desc "
        Dim x As Object = cKoneksi.ambilscalar(sSql)
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(custPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(custPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(custPrefix) & tambahNol(angka)
            End If
        End If
        Return retVal
    End Function


    Function tambahNol(ByVal iAngka As Integer)
        sValue = "0000"
        If iAngka >= 10 Then : sValue = "000" : End If
        If iAngka >= 100 Then : sValue = "00" : End If
        If iAngka >= 1000 Then : sValue = "0" : End If
        iAngka.ToString()
        If iAngka > 10000 Then : sValue = iAngka : Return sValue
        Else : sValue &= iAngka : Return sValue
        End If
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sMsg As String = ""
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            sMsg &= "- Session login anda habis !<br>"
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Session("consignee") = Nothing
            Dim userId As String = Session("UserID")
            Dim xSetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xSetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId
            Session("SpecialAccess") = xSetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xSetRole
            Session("tblSA") = Nothing
            Session("tblCP") = Nothing
            Session("tblBA") = Nothing
            TabContainer1.ActiveTabIndex = 0
            Response.Redirect("mstCustGroup.aspx")
        End If

        Page.Title = CompnyName & " - Data Customer Admin"
        Session("idPage") = Request.QueryString("idPage")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Apakah Anda yakin ingin menghapus data ini ?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not IsPostBack Then
            fDDLBranch() : initDDL()
            'Disable Credit Limit when cash

            bindCust("")
            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                initCOA()
                initProvince(custcountryoid.SelectedValue)
                initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
                initConsCity(consprovince.SelectedValue, custcountryoid.SelectedValue)
                initCountryPhoneCode(custcountryoid.SelectedValue)
                initCityPhoneCode(custcityoid.SelectedValue)
                I_U.Text = "New"
                UpdTime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
                UpdUser.Text = Session("UserID")
                lblLast.Visible = False
                TabContainer1.ActiveTabIndex = 0
                btnDelete.Visible = False
                btnSave.Visible = True
                BindConsigneeData(0)
            Else
                initCountry()
                I_U.Text = "UPDATE"
                lblCreate.Visible = False
                With SDSData
                    .SelectParameters("custoid").DefaultValue = Session.Item("idPage")
                    .SelectParameters("cmpcode").DefaultValue = CompnyCode
                End With
                fillTextBox(Session("idPage"))
                TabContainer1.ActiveTabIndex = 1
                btnDelete.Visible = False
                btnSave.Visible = True
                dd_timeofpayment.Enabled = False
                BindConsigneeData(Integer.Parse(Session("idPage")))
            End If
        End If
    End Sub

    Protected Sub GVmstcust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstcust.PageIndexChanging
        GVmstcust.PageIndex = e.NewPageIndex
        bindCust("")
    End Sub

    Protected Sub custcountryoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcountryoid.SelectedIndexChanged
        initProvince(custcountryoid.SelectedValue)
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        initCountryPhoneCode(custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub custprovoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custprovoid.SelectedIndexChanged
        initCity(custprovoid.SelectedValue, custcountryoid.SelectedValue)
        If custcityoid.Items.Count <> 0 Then
            initCityPhoneCode(custcityoid.SelectedValue)
        Else
            'cpcityphonecode.Text = ""
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        Dim sWhere As String = ""
        sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
        If cbStatus.Checked = True Then
            sWhere &= " AND custgroupflag='" & DDLStatus.SelectedValue & "'"
        End If
        bindCust(sWhere)
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        txtFilter.Text = ""
        cbStatus.Checked = False
        ddlFilter.SelectedIndex = 0
        DDLStatus.SelectedIndex = 0
        bindCust("")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If Not Session("consignee") Is Nothing Then
            Session("consignee") = Nothing
        End If
        I_U.Text = "NEW"
        btnDelete.Visible = False
        TabContainer1.ActiveTabIndex = 0
        Response.Redirect("~\master\mstCustGroup.aspx?awal=true")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If custcode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode Customer !<br>"
        End If
        If precustname.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih Prefix Customer !<br>"
        End If
        If custname.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Nama Customer !<br>"
        End If
        If custname.Text.Length > 100 Then
            sMsg &= "Nama Customer Max. 100 karakter !<br>"
        End If
        'If custaddr.Text = "" Then
        '    sMsg &= "- Silahkan isi Alamat Customer !<br>"
        'End If
        If ddlconttitle.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 1 !<br>"
        End If
        If ddlconttitle2.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 2 !<br>"
        End If
        If ddlconttitle3.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan isi Prefix Contact Person 3 !<br>"
        End If
        If custcountryoid.SelectedValue = "" Then
            sMsg &= "- Silahkan pilih nama Negara !<br>"
        End If
        If custprovoid.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Kota dan Provinsi untuk Negara " & custcountryoid.SelectedItem.Text & "!<br>"
        End If
        If custprovoid.SelectedItem.Text.ToLower = "none" Then
            sMsg &= "- Silahkan pilih nama Provinsi !<br>"
        End If
        If custcityoid.Items.Count = 0 Then
            sMsg &= "- Silahkan buat Kota untuk Provinsi " & custprovoid.SelectedItem.Text & " !<br>"
        End If
        If phone1.Text = "" Then
            sMsg &= "- Silahkan isi Telepon 1 !<br>"
        End If
        If custemail.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custemail.Text, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat email tidak valid, Ex : mail@sample.com !!<BR>"
            End If
        End If
        If custwebsite.Text <> "" Then
            oMatches = System.Text.RegularExpressions.Regex.Matches(custwebsite.Text, "\w\w\w\.([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?")
            If oMatches.Count <= 0 Then
                sMsg &= "- Alamat website tidak valid, Ex : www.sample.com !!<BR>"
            End If
        End If
        If notes.Text.Trim.Length > 100 Then
            sMsg &= "- Catatan tidak boleh lebih dari 100 karakter !<br>"
        End If
        If custcreditlimitrupiah.Text = "" Then
            sMsg &= "- Credit limit rupiah tidak boleh kosong !<br>"
        End If
        Dim datebirth As New Date
        If tgllahir.Text.Trim <> "" Then
            If Date.TryParseExact(tgllahir.Text, "dd/MM/yyyy", Nothing, Nothing, datebirth) = False Then
                sMsg &= "- Tanggal lahir customer tidak valid !<br>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - Warning", 2)
            Exit Sub
        End If

        Dim tittlename As String = ""
        If precustname.SelectedItem.Text.ToLower = "other" Then
            tittlename = ""
        Else
            tittlename = "," & precustname.SelectedItem.Text
        End If

        'cek kembar customer name
        If I_U.Text.ToLower = "new" Then
            Dim xdt1 As New DataTable
            Dim xquery1 = "select cmpcode,custoid,custcode,custname,custgroup from ql_mstcust where cmpcode='" & _
                CompnyCode & "' AND custname= '" & Tchar(custname.Text) & "" & tittlename & "' and custgroup = '" & custgroupoid.Text & "'"
            xdt1 = cKoneksi.ambiltabel(xquery1, "ql_mstitem")
            Dim xRow1() As DataRow
            xRow1 = xdt1.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If xRow1.Length > 0 Then
                showMessage("Nama Customer ada yang Sama pada Type " & custgroupoid.Text & " ,Tolong Simpan dengan Nama Lain !!", CompnyName & "- Warning", 2)
                Exit Sub
            End If
        Else
            Dim xdt1 As New DataTable
            Dim xquery1 = "select cmpcode,custoid,custcode,custname,custgroup from ql_mstcust where cmpcode='" & _
                CompnyCode & "' AND custname= '" & Tchar(custname.Text) & "" & tittlename & "' and custgroup = '" & custgroupoid.Text & "' and custoid <> " & Session("idPage")
            xdt1 = cKoneksi.ambiltabel(xquery1, "ql_mstitem")
            Dim xRow1() As DataRow
            xRow1 = xdt1.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If xRow1.Length > 0 Then
                showMessage("Nama Customer ada yang Sama pada Type " & custgroupoid.Text & " ,Tolong Simpan dengan Nama Lain !!", CompnyName & "- Warning", 2)
                Exit Sub
            End If
        End If
        Dim cekCust As Integer = 0
        If Session("idPage") Is Nothing Or Session("idPage") = "" Then
            custgroupoid.Text = GenerateID("QL_MSTcustgroup", CompnyCode)
            custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
        Else
            'cek apakah sudah ada customer belum
            If GetStrData("select count(-1) from ql_mstcust where custgroupoid=" & custgroupoid.Text & "") > 0 Then
                cekCust = 1
            Else
                cekCust = 0
                custoid.Text = GenerateID("QL_MSTCUST", CompnyCode)
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        'Dim OidTemp As Integer = ClassFunction.GenerateID("QL_shippingaddress", CompnyCode)

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try 

            If Session("idPage") Is Nothing Or Session("idPage") = "" Then
                sSql = "INSERT INTO QL_mstcustgroup (cmpcode, custgroupoid,branch_code,timeofpayment, custgroupcode, custgroupname, custgroupflag, custgroupaddr, custgroupcityoid, custgroupprovoid, custgroupcountryoid, custgrouppostcode, phone1, phone2, phone3, custgroupfax1, custgroupfax2, custgroupemail, custgroupwebsite, custgroupnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, statusAR, custgrouppaytermdefaultoid, custgroupcurrdefaultoid, custgroupcreditlimitcurroid, custgroupcreditlimit, custgroupcreditlimitusage, custgroupcreditlimitrupiah, custgroupcreditlimitusagerupiah, custgroupacctgoid, notes, resfield1, resfield2, createuser, upduser, updtime, cdphonecountry, cdphonecity, prefixcmp, prefixcp1, prefixcp2, prefixcp3, custbank, custgrouppassportname, custgrouppassportno, custgroupbirthdate,salesoid,coa_retur,pin_bb,flagcustomer) VALUES ('" & CompnyCode & "', " & Tchar(custgroupoid.Text) & ",'" & branchID.SelectedValue & "' ," & dd_timeofpayment.SelectedValue & ",'" & Tchar(custcode.Text) & "', '" & Tchar(custname.Text) & "" & tittlename & "', '" & custflag.SelectedValue & "', '" & Tchar(custaddr.Text) & "', " & custcityoid.SelectedValue & ", " & custprovoid.SelectedValue & ", " & custcountryoid.SelectedValue & ", '" & custpostcode.Text & "', '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & custfax1.Text & "', '', '" & Tchar(custemail.Text) & "', '" & Tchar(custwebsite.Text) & "', '" & Tchar(custnpwp.Text) & "', '" & Tchar(contactperson1.Text) & "', '" & Tchar(contactperson2.Text) & "', '" & Tchar(contactperson3.Text) & "', '" & phonecontactperson1.Text & "', '" & phonecontactperson2.Text & "', '" & phonecontactperson3.Text & "', '" & statusar.SelectedValue & "', 0, 0, 0, 0, 0, " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & araccount.SelectedValue & ", '" & Tchar(notes.Text) & "', '', '', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', " & precustname.SelectedValue & ", " & ddlconttitle.SelectedValue & ", " & ddlconttitle2.SelectedValue & ", " & ddlconttitle3.SelectedValue & ",'" & Tchar(bankrekening.Text) & "', '" & Tchar(namapaspor.Text) & "', '" & Tchar(nopaspor.Text) & "', '" & datebirth & "'," & spgOid.SelectedValue & "," & returaccount.SelectedValue & ",'" & pin_bb.Text & "','" & IIf(cbCustomer.Checked = True, "1", "0") & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & custgroupoid.Text & " WHERE tablename='QL_MSTcustgroup' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If cbCustomer.Checked = True Then
                    'Insertkan juga ke master customer
                    sSql = "INSERT INTO QL_mstcust (cmpcode, custoid, custgroupoid, branch_code,timeofpayment, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, statusAR, custpaytermdefaultoid, custcurrdefaultoid, custcreditlimitcurroid, custcreditlimit, custcreditlimitusage, custcreditlimitrupiah, custcreditlimitusagerupiah, custacctgoid, notes, resfield1, resfield2, createuser, upduser, updtime, cdphonecountry, cdphonecity, prefixcmp, prefixcp1, prefixcp2, prefixcp3, CustGroup, custbank, custpassportname, custpassportno, custbirthdate,salesoid,coa_retur,pin_bb) VALUES ('" & CompnyCode & "', " & Tchar(custoid.Text) & "," & custgroupoid.Text & ",'" & branchID.SelectedValue & "' ," & dd_timeofpayment.SelectedValue & ",'" & Tchar(custcode.Text) & "', '" & Tchar(custname.Text) & "" & tittlename & "', '" & custflag.SelectedValue & "', '" & Tchar(custaddr.Text) & "', " & custcityoid.SelectedValue & ", " & custprovoid.SelectedValue & ", " & custcountryoid.SelectedValue & ", '" & custpostcode.Text & "', '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & custfax1.Text & "', '', '" & Tchar(custemail.Text) & "', '" & Tchar(custwebsite.Text) & "', '" & Tchar(custnpwp.Text) & "', '" & Tchar(contactperson1.Text) & "', '" & Tchar(contactperson2.Text) & "', '" & Tchar(contactperson3.Text) & "', '" & phonecontactperson1.Text & "', '" & phonecontactperson2.Text & "', '" & phonecontactperson3.Text & "', '" & statusar.SelectedValue & "', 0, 0, 0, 0, 0, " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & araccount.SelectedValue & ", '" & Tchar(notes.Text) & "', '', '', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', " & precustname.SelectedValue & ", " & ddlconttitle.SelectedValue & ", " & ddlconttitle2.SelectedValue & ", " & ddlconttitle3.SelectedValue & ", '" & custgroupoid.Text & "', '" & Tchar(bankrekening.Text) & "', '" & Tchar(namapaspor.Text) & "', '" & Tchar(nopaspor.Text) & "', '" & datebirth & "'," & spgOid.SelectedValue & "," & returaccount.SelectedValue & ",'" & pin_bb.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & custoid.Text & " WHERE tablename='QL_MSTCUST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
            Else
                sSql = "update QL_mstcustgroup set timeofpayment= " & dd_timeofpayment.SelectedValue & ", custgroupcode = '" & Tchar(custcode.Text) & "', custgroupname = '" & Tchar(custname.Text) & "" & tittlename & "', custgroupflag = '" & custflag.SelectedValue & "', custgroupaddr= '" & Tchar(custaddr.Text) & "', custgroupcityoid = " & custcityoid.SelectedValue & ", custgroupprovoid = " & custprovoid.SelectedValue & ", custgroupcountryoid = " & custcountryoid.SelectedValue & ", custgrouppostcode = '" & custpostcode.Text & "', phone1 = '" & phone1.Text & "', phone2 = '" & phone2.Text & "', phone3 = '" & phone3.Text & "', custgroupfax1 = '" & custfax1.Text & "', custgroupfax2 = '', custgroupemail = '" & Tchar(custemail.Text) & "', custgroupwebsite = '" & Tchar(custwebsite.Text) & "', custgroupnpwp = '" & Tchar(custnpwp.Text) & "', contactperson1 = '" & Tchar(contactperson1.Text) & "', contactperson2 = '" & Tchar(contactperson2.Text) & "', contactperson3 =  '" & Tchar(contactperson3.Text) & "', phonecontactperson1 = '" & phonecontactperson1.Text & "', phonecontactperson2 = '" & phonecontactperson2.Text & "', phonecontactperson3 = '" & phonecontactperson3.Text & "', statusAR = '" & statusar.SelectedValue & "', custgrouppaytermdefaultoid = 0, custgroupcurrdefaultoid = 0, custgroupcreditlimitcurroid = 0, custgroupcreditlimit = 0, custgroupcreditlimitusage = 0, custgroupcreditlimitrupiah = " & ToDouble(custcreditlimitrupiah.Text) & ", custgroupcreditlimitusagerupiah = " & ToDouble(custcreditlimitusage.Text) & ", custgroupacctgoid = " & araccount.SelectedValue & ", notes = '" & Tchar(notes.Text) & "', resfield1 = '', resfield2 = '', createuser = '" & Session("UserID") & "', upduser = '" & Session("UserID") & "', updtime = current_timestamp, cdphonecountry = '', cdphonecity = '', prefixcmp = " & precustname.SelectedValue & ", prefixcp1 = " & ddlconttitle.SelectedValue & ", prefixcp2 = " & ddlconttitle2.SelectedValue & ", prefixcp3 = " & ddlconttitle3.SelectedValue & ", custbank = '" & Tchar(bankrekening.Text) & "', custgrouppassportname = '" & Tchar(namapaspor.Text) & "', custgrouppassportno = '" & Tchar(nopaspor.Text) & "', custgroupbirthdate = '" & datebirth & "',salesoid = " & spgOid.SelectedValue & ",coa_retur =" & returaccount.SelectedValue & ",pin_bb = '" & pin_bb.Text & "',flagcustomer = '" & IIf(cbCustomer.Checked = True, "1", "0") & "' where cmpcode = '" & CompnyCode & "' and custgroupoid = " & custgroupoid.Text & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                If cbCustomer.Checked = True Then
                    If cekCust = 0 Then
                        sSql = "INSERT INTO QL_mstcust (cmpcode, custoid, custgroupoid, branch_code,timeofpayment, custcode, custname, custflag, custaddr, custcityoid, custprovoid, custcountryoid, custpostcode, phone1, phone2, phone3, custfax1, custfax2, custemail, custwebsite, custnpwp, contactperson1, contactperson2, contactperson3, phonecontactperson1, phonecontactperson2, phonecontactperson3, statusAR, custpaytermdefaultoid, custcurrdefaultoid, custcreditlimitcurroid, custcreditlimit, custcreditlimitusage, custcreditlimitrupiah, custcreditlimitusagerupiah, custacctgoid, notes, resfield1, resfield2, createuser, upduser, updtime, cdphonecountry, cdphonecity, prefixcmp, prefixcp1, prefixcp2, prefixcp3, CustGroup, custbank, custpassportname, custpassportno, custbirthdate,salesoid,coa_retur,pin_bb) VALUES ('" & CompnyCode & "', " & Tchar(custoid.Text) & "," & custgroupoid.Text & ",'" & branchID.SelectedValue & "' ," & dd_timeofpayment.SelectedValue & ",'" & Tchar(custcode.Text) & "', '" & Tchar(custname.Text) & "" & tittlename & "', '" & custflag.SelectedValue & "', '" & Tchar(custaddr.Text) & "', " & custcityoid.SelectedValue & ", " & custprovoid.SelectedValue & ", " & custcountryoid.SelectedValue & ", '" & custpostcode.Text & "', '" & phone1.Text & "', '" & phone2.Text & "', '" & phone3.Text & "', '" & custfax1.Text & "', '', '" & Tchar(custemail.Text) & "', '" & Tchar(custwebsite.Text) & "', '" & Tchar(custnpwp.Text) & "', '" & Tchar(contactperson1.Text) & "', '" & Tchar(contactperson2.Text) & "', '" & Tchar(contactperson3.Text) & "', '" & phonecontactperson1.Text & "', '" & phonecontactperson2.Text & "', '" & phonecontactperson3.Text & "', '" & statusar.SelectedValue & "', 0, 0, 0, 0, 0, " & ToDouble(custcreditlimitrupiah.Text) & ", " & ToDouble(custcreditlimitusage.Text) & ", " & araccount.SelectedValue & ", '" & Tchar(notes.Text) & "', '', '', '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', " & precustname.SelectedValue & ", " & ddlconttitle.SelectedValue & ", " & ddlconttitle2.SelectedValue & ", " & ddlconttitle3.SelectedValue & ", '" & custgroupoid.Text & "', '" & Tchar(bankrekening.Text) & "', '" & Tchar(namapaspor.Text) & "', '" & Tchar(nopaspor.Text) & "', '" & datebirth & "'," & spgOid.SelectedValue & "," & returaccount.SelectedValue & ",'" & pin_bb.Text & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & custoid.Text & " WHERE tablename='QL_MSTCUST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        'updatekan ke customernya
                        sSql = "update QL_mstcust set custgroupoid = " & custgroupoid.Text & ", timeofpayment= " & dd_timeofpayment.SelectedValue & ", custcode = '" & Tchar(custcode.Text) & "', custname = '" & Tchar(custname.Text) & "" & tittlename & "', custflag = '" & custflag.SelectedValue & "', custaddr= '" & Tchar(custaddr.Text) & "', custcityoid = " & custcityoid.SelectedValue & ", custprovoid = " & custprovoid.SelectedValue & ", custcountryoid = " & custcountryoid.SelectedValue & ", custpostcode = '" & custpostcode.Text & "', phone1 = '" & phone1.Text & "', phone2 = '" & phone2.Text & "', phone3 = '" & phone3.Text & "', custfax1 = '" & custfax1.Text & "', custfax2 = '', custemail = '" & Tchar(custemail.Text) & "', custwebsite = '" & Tchar(custwebsite.Text) & "', custnpwp = '" & Tchar(custnpwp.Text) & "', contactperson1 = '" & Tchar(contactperson1.Text) & "', contactperson2 = '" & Tchar(contactperson2.Text) & "', contactperson3 =  '" & Tchar(contactperson3.Text) & "', phonecontactperson1 = '" & phonecontactperson1.Text & "', phonecontactperson2 = '" & phonecontactperson2.Text & "', phonecontactperson3 = '" & phonecontactperson3.Text & "', statusAR = '" & statusar.SelectedValue & "', custpaytermdefaultoid = 0, custcurrdefaultoid = 0, custcreditlimitcurroid = 0, custcreditlimit = 0, custcreditlimitusage = 0, custcreditlimitrupiah = " & ToDouble(custcreditlimitrupiah.Text) & ", custcreditlimitusagerupiah = " & ToDouble(custcreditlimitusage.Text) & ", custacctgoid = " & araccount.SelectedValue & ", notes = '" & Tchar(notes.Text) & "', resfield1 = '', resfield2 = '', createuser = '" & Session("UserID") & "', upduser = '" & Session("UserID") & "', updtime = current_timestamp, cdphonecountry = '', cdphonecity = '', prefixcmp = " & precustname.SelectedValue & ", prefixcp1 = " & ddlconttitle.SelectedValue & ", prefixcp2 = " & ddlconttitle2.SelectedValue & ", prefixcp3 = " & ddlconttitle3.SelectedValue & ", CustGroup = '" & custgroupoid.Text & "', custbank = '" & Tchar(bankrekening.Text) & "', custpassportname = '" & Tchar(namapaspor.Text) & "', custpassportno = '" & Tchar(nopaspor.Text) & "', custbirthdate = '" & datebirth & "',salesoid = " & spgOid.SelectedValue & ",coa_retur =" & returaccount.SelectedValue & ",pin_bb = '" & pin_bb.Text & "' Where cmpcode = '" & CompnyCode & "' and custoid = '" & custoid.Text & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If
            End If
            objTrans.Commit()
            conn.Close()
            'generateCustID() 
            'clearItems()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Session("idPage") = Nothing
        Session("consignee") = Nothing
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data tersimpan"))
        Response.Redirect("~\master\mstCustGroup.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sColomnName() As String = {"REQCUSTOID", "trncustoid", "trncustoid"}
        Dim sTable() As String = {"QL_TRNREQUEST", "QL_trnordermst", "QL_trnjualmst"}
        If CheckDataExists(custoid.Text, sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain !", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstcust WHERE cmpcode = '" & CompnyCode & "' AND custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "DELETE QL_mstcustcons WHERE cmpcode = '" & CompnyCode & "' AND custoid=" & Session("idPage") & " "
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
            'generateCustID()
            'clearItems()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - Error", 1)
            Exit Sub
        End Try
        Response.Redirect("~\master\mstCustGroup.aspx?awal=true")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
        btnExtender.Visible = False
        PanelErrMsg.Visible = False
        If Not Session("consigneeerror") Is Nothing Then
            If Session("consigneeerror") = "yes" Then
                PanelConsignee.Visible = True
                btnConsignee.Visible = True
                MPEConsignee.Show()
                Session("consigneeerror") = "no"
            End If
        End If
    End Sub

    Protected Sub custcityoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcityoid.SelectedIndexChanged
        initCityPhoneCode(custcityoid.SelectedValue)
    End Sub

    Protected Sub custname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custname.TextChanged
        'If Session("idPage") <> "" And Session("idPage") <> Nothing Then
        'Else
        'HANYA NEW YANG GENERATE CODE
        If custname.Text.Trim <> "" Then
            If custname.Text.Length < 1 Then
                showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                custname.Text = "" : custcode.Text = ""
                Exit Sub
            Else
                custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
            End If
        Else
            custcode.Text = ""
        End If
        'End If
    End Sub

    Protected Sub DropBrunch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles branchID.TextChanged
        Outlate()
        If custname.Text <> "" Then
            If Session("idPage") <> "" And Session("idPage") <> Nothing Then
            Else
                'HANYA NEW YANG GENERATE CODE
                If custname.Text.Trim <> "" Then
                    If custname.Text.Length < 1 Then
                        showMessage("Nama Customer Minimal 1 Character", CompnyName & " - WARNING", 2)
                        custname.Text = ""
                        custcode.Text = ""
                        Exit Sub
                    Else
                        custcode.Text = KodeOutlate.Text & generateCustCode(Tchar(custname.Text))
                    End If
                Else
                    custcode.Text = ""
                End If
            End If
        End If
    End Sub

    Protected Sub lbinformasi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbinformasi.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbconsignee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbconsignee.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub lbnewconsignee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbnewconsignee.Click
        PanelConsignee.Visible = True : btnConsignee.Visible = True
        MPEConsignee.Show()
        lblconsstate.Text = "new" : consigneeoid.Text = 0
    End Sub

    Protected Sub lbcancelconsignee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbcancelconsignee.Click
        ClearConsignee()
        PanelConsignee.Visible = False : btnConsignee.Visible = False
        MPEConsignee.Hide()
    End Sub

    Private Sub BindConsigneeData(ByVal custoid As Integer)
        sSql = "SELECT consoid, custoid, conscode, consname, consaddress, conscityoid, gendesc, consprovoid, consphone, consfax, consnpwp, conscontactperson, consphonecontactpersonchar, consnote, prefixcp, conpostcode FROM QL_mstcustcons a INNER JOIN QL_mstgen b ON b.genoid = a.conscityoid AND b.cmpcode = a.cmpcode WHERE a.cmpcode = '" & CompnyCode & "' AND custoid = " & custoid & " ORDER BY consoid"

        Dim dtab As DataTable = cKoneksi.ambiltabel(sSql, "consignee")
        GVConsignee.DataSource = dtab
        GVConsignee.DataBind()
        GVConsignee.SelectedIndex = -1
        Session("consignee") = dtab
    End Sub

    Private Sub ClearConsignee()
        conscode.Text = ""
        consname.Text = "" : consaddress.Text = "" : consprovince.SelectedIndex = 0
        conscity.SelectedIndex = 0 : conspostcode.Text = "" : consphone.Text = ""
        consfax.Text = "" : consnpwp.Text = "" : conscpprefix.SelectedIndex = 0
        conscpname.Text = "" : conscpphone.Text = "" : consnote.Text = ""
    End Sub

    Protected Sub lbaddconsignee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbaddconsignee.Click
        Dim ErrMsg As String = ""
        If consname.Text.Trim = "" Then
            ErrMsg &= " - Nama consignee tidak boleh kosong!<br />"
        End If
        If consaddress.Text.Trim = "" Then
            ErrMsg &= " - Alamat consignee tidak boleh kosong!<br />"
        Else
            If consaddress.Text.Trim.Length > 100 Then
                ErrMsg &= " - Alamat consignee tidak boleh lebih dari 100 karakter!<br />"
            End If
        End If
        If consnote.Text.Trim.Length > 50 Then
            ErrMsg &= " - Catatan consignee tidak boleh lebih dari 50 karakter!<br />"
        End If

        If ErrMsg <> "" Then
            Session("consigneeerror") = "yes" : PanelConsignee.Visible = False
            btnConsignee.Visible = False : MPEConsignee.Hide()
            showMessage(ErrMsg, CompnyName & " - WARNING", 2)
            Exit Sub
        End If

        If lblconsstate.Text = "new" Then
            If Session("consignee") Is Nothing Then
                CreateConsigneeTable()
            End If
        End If

        Dim dtab As DataTable = Session("consignee")

        'Pengecekan data kembar
        Dim dview As DataView = dtab.DefaultView
        If lblconsstate.Text = "new" Then
            dview.RowFilter = "consname = '" & consname.Text.Trim & "' AND consaddress = '" & consaddress.Text.Trim & "' AND gendesc = '" & conscity.SelectedItem.ToString & "'"
        Else
            dview.RowFilter = "conscode <> '" & conscode.Text & "' AND consname = '" & consname.Text.Trim & "' AND consaddress = '" & consaddress.Text.Trim & "' AND gendesc = '" & conscity.SelectedItem.ToString & "'"
        End If
        If dview.Count > 0 Then
            Session("consigneeerror") = "yes" : PanelConsignee.Visible = False
            btnConsignee.Visible = False : MPEConsignee.Hide()

            showMessage("Consignee dengan nama, alamat, dan kota yang sama telah terdaftar sebelumnya !", CompnyName & " - WARNING", 2)
            dview.RowFilter = ""
            Exit Sub
        End If

        dview.RowFilter = ""

        'Pencatatan data baru
        If lblconsstate.Text = "new" Then
            Dim drow As DataRow
            drow = dtab.NewRow

            drow("consoid") = consigneeoid.Text
            drow("custoid") = 0

            Dim no As Integer
            Dim lb As System.Web.UI.WebControls.LinkButton
            If GVConsignee.Rows.Count > 0 Then
                lb = GVConsignee.Rows(GVConsignee.Rows.Count - 1).FindControl("conscode")
                If Integer.TryParse(lb.Text, no) = False Then
                    no = GVConsignee.Rows.Count + 1
                End If
            Else
                no = 0
            End If

            drow("conscode") = (no + 1).ToString
            drow("consname") = consname.Text.Trim
            drow("consaddress") = consaddress.Text.Trim
            drow("consprovoid") = consprovince.SelectedValue
            drow("conscityoid") = conscity.SelectedValue
            drow("gendesc") = conscity.SelectedItem.ToString
            drow("conpostcode") = conspostcode.Text.Trim
            drow("consphone") = consphone.Text.Trim
            drow("consfax") = consfax.Text.Trim
            drow("consnpwp") = consnpwp.Text.Trim
            drow("prefixcp") = conscpprefix.SelectedValue
            drow("conscontactperson") = conscpname.Text.Trim
            drow("consphonecontactpersonchar") = conscpphone.Text.Trim
            drow("consnote") = consnote.Text.Trim

            dtab.Rows.Add(drow)
        Else
            dview.RowFilter = "conscode = '" & conscode.Text & "'"
            Dim dviewrow As DataRowView = dview.Item(0)
            dviewrow.BeginEdit()

            dviewrow("consoid") = consigneeoid.Text
            dviewrow("custoid") = 0
            dviewrow("conscode") = conscode.Text
            dviewrow("consname") = consname.Text.Trim
            dviewrow("consaddress") = consaddress.Text.Trim
            dviewrow("consprovoid") = consprovince.SelectedValue
            dviewrow("conscityoid") = conscity.SelectedValue
            dviewrow("gendesc") = conscity.SelectedItem.ToString
            dviewrow("conpostcode") = conspostcode.Text.Trim
            dviewrow("consphone") = consphone.Text.Trim
            dviewrow("consfax") = consfax.Text.Trim
            dviewrow("consnpwp") = consnpwp.Text.Trim
            dviewrow("prefixcp") = conscpprefix.SelectedValue
            dviewrow("conscontactperson") = conscpname.Text.Trim
            dviewrow("consphonecontactpersonchar") = conscpphone.Text.Trim
            dviewrow("consnote") = consnote.Text.Trim

            dviewrow.EndEdit()
            dview.RowFilter = ""
        End If

        dtab.AcceptChanges()
        Session("consignee") = dtab : GVConsignee.DataSource = dtab
        GVConsignee.DataBind() : ClearConsignee()
        PanelConsignee.Visible = False : btnConsignee.Visible = False
        MPEConsignee.Hide() : ibremoveconsignee.Visible = True
    End Sub

    Private Function GenerateConsigneeCode(ByVal consname As String) As String
        Dim retVal As String = ""
        Dim consPrefix As String = consname.Substring(0, 1)
        sSql = "select conscode from QL_mstcustcons where conscode like '" & consPrefix & "%' order by conscode desc "
        xCmd.CommandText = sSql
        Dim x As Object = xCmd.ExecuteScalar()
        If x Is Nothing Then
            ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
            retVal = UCase(consPrefix) & "00001"
        Else
            If x = "" Then
                ' kode supplier seperti yg diminta tidak ada sama sekali, generate kode baru
                retVal = UCase(consPrefix) & "00001"
            Else
                ' kode supplier seperti yg diminta ada, tinggal generate angka
                Dim angka As Integer = CInt(x.ToString.Substring(1, 5))
                angka += 1
                retVal = UCase(consPrefix) & tambahNol(angka)
            End If
        End If
        Return retVal
    End Function

    Protected Sub consname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles consname.TextChanged
        'If consname.Text.Trim <> "" Then
        '    If consname.Text.Length < 1 Then
        '        showMessage("Nama Consignee Minimal 1 Karakter", CompnyName & " - WARNING", 2)
        '        consname.Text = ""
        '        conscode.Text = ""
        '        Exit Sub
        '    Else
        '        conscode.Text = GenerateConsigneeCode(Tchar(consname.Text))
        '    End If
        'Else
        '    showMessage("Nama Consignee Minimal 1 Karakter", CompnyName & "WARNING", 2)
        '    conscode.Text = ""
        'End If

        'MPEConsignee.Show()
    End Sub

    Private Sub CreateConsigneeTable()
        Dim dtab As New DataTable
        dtab.Columns.Add("consoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("custoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("conscode", Type.GetType("System.String"))
        dtab.Columns.Add("consname", Type.GetType("System.String"))
        dtab.Columns.Add("consaddress", Type.GetType("System.String"))
        dtab.Columns.Add("consprovoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("conscityoid", Type.GetType("System.Int32"))
        dtab.Columns.Add("gendesc", Type.GetType("System.String"))
        dtab.Columns.Add("conpostcode", Type.GetType("System.String"))
        dtab.Columns.Add("consphone", Type.GetType("System.String"))
        dtab.Columns.Add("consfax", Type.GetType("System.String"))
        dtab.Columns.Add("consnpwp", Type.GetType("System.String"))
        dtab.Columns.Add("prefixcp", Type.GetType("System.Int32"))
        dtab.Columns.Add("conscontactperson", Type.GetType("System.String"))
        dtab.Columns.Add("consphonecontactpersonchar", Type.GetType("System.String"))
        dtab.Columns.Add("consnote", Type.GetType("System.String"))
        Session("consignee") = dtab
    End Sub

    Protected Sub consprovince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles consprovince.SelectedIndexChanged
        initConsCity(consprovince.SelectedValue, custcountryoid.SelectedValue)
        MPEConsignee.Show()
    End Sub

    Protected Sub conscode_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblconsstate.Text = "edit"

        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvrow As System.Web.UI.WebControls.GridViewRow
        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvrow = TryCast(lbutton.NamingContainer, System.Web.UI.WebControls.GridViewRow)

        consigneeoid.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consoid").ToString
        conscode.Text = lbutton.Text
        consname.Text = gvrow.Cells(1).Text
        consaddress.Text = gvrow.Cells(2).Text
        'gvCustomer.SelectedDataKey("custoid").ToString
        consprovince.SelectedValue = GVConsignee.DataKeys(gvrow.RowIndex).Values("consprovoid").ToString
        consprovince_SelectedIndexChanged(sender, e)
        conscity.SelectedValue = GVConsignee.DataKeys(gvrow.RowIndex).Values("conscityoid").ToString
        conspostcode.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("conpostcode")
        consphone.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consphone")
        consfax.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consfax")
        consnpwp.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consnpwp")
        conscpprefix.SelectedValue = GVConsignee.DataKeys(gvrow.RowIndex).Values("prefixcp")
        conscpname.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("conscontactperson")
        conscpphone.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consphonecontactpersonchar")
        consnote.Text = GVConsignee.DataKeys(gvrow.RowIndex).Values("consnote")
        PanelConsignee.Visible = True : btnConsignee.Visible = True
        MPEConsignee.Show()
    End Sub

    Protected Sub ibremoveconsignee_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibremoveconsignee.Click
        removeList("consignee", GVConsignee, ibremoveconsignee, 4)
    End Sub

    Sub removeList(ByVal sessionname As String, ByVal gvName As GridView, ByVal btnName As ImageButton, ByVal checkCol As Integer)
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session(sessionname)
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For i As Integer = objRow.Length() - 1 To 0 Step -1
                    If cKoneksi.getCheckBoxValue(i, checkCol, gvName) = True Then
                        objTable.Rows.Remove(objRow(i))
                    End If
                Next
                Session(sessionname) = objTable
                gvName.DataSource = Session(sessionname)
                gvName.DataBind()
            End If
            If Session(sessionname).rows.count > 0 Then
                btnName.Visible = True
            Else
                btnName.Visible = False
            End If
        Catch ex As Exception
            validasi.Text = "Choose the data before deleting"
            validasi.Visible = True
            showMessage(ex.ToString, CompnyName & " - WARNING", 2)
        End Try
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintReport("", "Customer_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/report/rpt_mstCustomer.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim sWhere As String = "" : Dim sFilter As String = ""

        sWhere = " where QL_mstcust.cmpcode LIKE '%" & CompnyCode & "%' and custflag = '" & DDLStatus.SelectedValue & "' "

        If txtFilter.Text.Trim <> "" Then
            If ddlFilter.SelectedItem.Text.ToUpper = "Code" Or ddlFilter.SelectedItem.Text.ToUpper = "Nama" Or ddlFilter.SelectedItem.Text.ToUpper = "Group" Then
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '" & Tchar(txtFilter.Text) & "' "
            Else
                sWhere &= "  and  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '%" & Tchar(txtFilter.Text) & "%' "
            End If
            'sFilter &= " " & ddlFilter.SelectedItem.Text & " = " & txtFilter.Text & ", "
        End If

        'If sFilter = "" Then
        '    sFilter = " ALL "
        'End If

        rptReport.SetParameterValue("sWhere", sWhere)
        'rptReport.SetParameterValue("sFilter", " Filter By : " & sFilter)

        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = ""

        If txtFilter.Text.Trim <> "" Then
            If ddlFilter.SelectedItem.Text.ToUpper = "Code" Or ddlFilter.SelectedItem.Text.ToUpper = "Nama" Or ddlFilter.SelectedItem.Text.ToUpper = "Group" Then
                sWhere = "  where  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '" & Tchar(txtFilter.Text) & "' "
            Else
                sWhere = "  where  upper(" & ddlFilter.Items(ddlFilter.SelectedIndex).Value & ") like '%" & Tchar(txtFilter.Text) & "%' "
            End If
        End If
        Response.Clear()
        If printType = "CUSTOMER" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"
            sSql = "select custoid,custcode,custname,custaddr,phone1,custgroup,custflag,notes from QL_mstcust " & sWhere & " order by custcode"
        End If
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet : Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.Excel)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "CUSTOMER" : imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoCust") = "" : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub dd_timeofpayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dd_timeofpayment.SelectedIndexChanged
        If dd_timeofpayment.SelectedValue = 34 Then
            custcreditlimitrupiah.Enabled = False
            custcreditlimitrupiah.Text = 0
            custcreditlimitrupiah.CssClass = "inpTextDisabled"
        Else
            custcreditlimitrupiah.Enabled = True
            custcreditlimitrupiah.CssClass = "inpText"
            custcreditlimitrupiah.Text = Session("creditlimit")
        End If
    End Sub
#End Region

    Protected Sub branchID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initCOA()
    End Sub

    Protected Sub custcreditlimitrupiah_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custcreditlimitrupiah.TextChanged
        custcreditlimitrupiah.Text = ToMaskEdit(custcreditlimitrupiah.Text, 4)
        sisacreditlimitrupiah.Text = ToMaskEdit(ToDouble(custcreditlimitrupiah.Text) - ToDouble(custcreditlimitusagerupiah.Text), 4)
    End Sub
End Class