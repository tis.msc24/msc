'Created By Vriska On 4 October 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstPengajuan
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Dim sMsg As String = ""
#End Region

#Region "Functions"

    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("trndparoid")
    End Function

#End Region

#Region "Procedures"
    Private Sub IndexChanged()
        If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
            clawalnew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = True : CLTempNew.Visible = True
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
            CLTempNew_TextChanged(Nothing, Nothing)
        ElseIf custapprovaltype.SelectedValue = "TERMIN" Then
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = True : TerminNew.Visible = True
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "REVISI DATA" Then
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = True : Lbladdrnew.Visible = True
            Lblcitynew.Visible = True : citynew.Visible = True
            BtnCariCity.Visible = True : BtnBersihCity.Visible = True
            Lblprovnew.Visible = True : provnew.Visible = True
            Lbltelprmhnew.Visible = True : telprmhnew.Visible = True
            Lbltelpktrnew.Visible = True : telpktrnew.Visible = True
            Lbltelpwanew.Visible = True : telpwanew.Visible = True
            Label8.Visible = True : custflagOld.Visible = True
            Label10.Visible = True : custflagNew.Visible = True
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "CL AWAL" Then
            Lblclawalnew.Visible = True : clawalnew.Visible = True
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            clawalnew_TextChanged(Nothing, Nothing)
            CLTempNew.Text = "0.00" : btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "OVERDUE" Then
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = True
        End If
    End Sub

    Private Sub clear()
        custoid.Text = "0" : custname.Text = ""
        clawalnew.Text = "0.00" : clusage.Text = "0.00"
        CLTempNew.Text = "0.00" : cltotal.Text = "0.00"
        CLAwalOld.Text = "0.00" : CLTempOld.Text = "0.00"
        CLTempOld.Text = "0.00" : clsisa.Text = "0.00"

        addrnew.Text = "" : citynew.Text = "" : CityOidNew.Text = "0"
        provnew.Text = "" : ProvOidNew.Text = "0" : telprmhnew.Text = ""
        telpktrnew.Text = "" : telpwanew.Text = ""

        addrold.Text = "" : telprmhold.Text = ""
        cityold.Text = "" : OidCityOld.Text = "0"
        ProvOld.Text = "" : OidProvOld.Text = "0"
        telpktrold.Text = "" : telpwaold.Text = ""
    End Sub

    Private Sub GeneratedCode()
        'If i_u.Text = "New" Then
        Dim reqcode As String = ""
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim code As String = ""
            sSql = "Select genother1 From ql_mstgen Where gengroup='CABANG' and gencode='" & cabangDDL.SelectedValue & "'"
            xCmd.CommandText = sSql : Dim sCabang As String = xCmd.ExecuteScalar()
            code = "CA/" & sCabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custapprovalno,4) AS INT)),0) FROM QL_mstcustapproval WHERE custapprovalno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            Session("reqcode") = code & sequence
            custapprovalno.Text = Session("reqcode")

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        'End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub DDLfCabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(cabangDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(cabangDDL, sSql)
            Else
                FillDDL(cabangDDL, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(cabangDDL, sSql)
        End If
    End Sub

    Private Sub DDLcabang()
        sSql = "Select gencode,gendesc from ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(FilterDDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(FilterDDLCabang, sSql)
            Else
                FillDDL(FilterDDLCabang, sSql)
                FilterDDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                FilterDDLCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(FilterDDLCabang, sSql)
            FilterDDLCabang.Items.Add(New ListItem("ALL", "ALL"))
            FilterDDLCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub DDLTermin()
        Try
            sSql = "select genoid, gendesc from QL_mstgen Where gengroup='PAYTYPE' AND cmpcode='" & CompnyCode & "'"
            FillDDL(TerminOld, sSql)
            FillDDL(TerminNew, sSql)
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindCust()
        Try
            sSql = "Select * from (SELECT custoverduestatus,custoid,custcode,custname,cb.gendesc,py.genoid TerminOid,py.gendesc Termin,cu.custaddr alamat,cu.custcityoid,ct.gendesc Kota,cu.custprovoid,pv.gendesc Provinsi,cu.phone1 TelpRmh,cu.phone2 TlpKantor,cu.phone3 TelpWa,Isnull(cu.custcreditlimitrupiah,0.00) CrLimit,Isnull(cu.custcreditlimitusagerupiah,0.00) CrUsage,Isnull(cu.custcreditlimitrupiahawal,0.00) CrAwal,Isnull(cu.custcreditlimitrupiahtempo,0.00) CrTempo,(SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate,(SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate,CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=cu.custoid AND j.trnjualstatus='POST' AND branch_code=cu.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE))<=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) CrOverDue,0.00 Total,lower(custflag) custflag,cu.notes Catatan,Convert(VarChar(20),cu.createtime,103) tglGabung FROM QL_mstcust cu Inner Join QL_mstgen ct ON ct.genoid=cu.custcityoid AND ct.gengroup='CITY' Inner Join QL_mstgen pv ON pv.genoid=cu.custprovoid AND pv.gengroup='PROVINCE' Inner Join ql_mstgen cb ON cb.gencode=cu.branch_code And cb.gengroup='CABANG' Inner Join ql_mstgen py ON py.genoid=cu.timeofpayment And py.gengroup='PAYTYPE' WHERE cu.cmpcode='" & CompnyCode & "' And cu.branch_code = '" & cabangDDL.SelectedValue & "' AND (custcode LIKE '%" & Tchar(custname.Text) & "%' OR custname LIKE '%" & Tchar(custname.Text) & "%')) ct "
            If custapprovaltype.SelectedValue = "OVERDUE" Then
                sSql &= "Where CrOverDue>0.00 AND custoverduestatus<>'Y'"
            End If

            sSql &= "ORDER BY custcode"
            FillGV(gvCust, sSql, "QL_mstcreditcard")
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindDataList()
        Try
            sSql = "SELECT TOP 100 cp.custapprovaloid, cb.gendesc Cabang, cu.custcode, cu.custname ,cp.custapprovalno, Convert(char(20), cp.custapprovaldate,103) custapprovaldate, cp.custapprovaltype, tr.genoid TerMinOid, tr.gendesc TerminNew, cp.addrnew, cp.telprmhnew, cp.telpktrnew, cp.telpwanew, cp.clawalnew, cp.cltempnew, cp.custapprovalnote, cp.custapprovalstatus, cp.flagstatus FROM QL_mstcustapproval cp INNER JOIN QL_mstgen cb On cb.gencode=cp.branch_code AND cb.gengroup='CABANG' INNER JOIN QL_mstgen tr ON tr.genoid=cp.terminnew AND tr.gengroup='PAYTYPE' Left Join QL_mstgen ct ON ct.genoid=cp.citynew AND tr.gengroup='CITY' Left Join QL_mstgen pro ON pro.genoid=cp.provnew AND tr.gengroup='CITY' Inner Join QL_mstcust cu ON cu.custoid=cp.custoid AND cp.branch_code=cu.branch_code Where cp.custapprovaldate Between '" & CDate(toDate(dateAwal.Text)) & " 0:0:0' AND '" & CDate(toDate(dateAkhir.Text)) & " 23:55:59' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterNya.Text) & "%'"
            If FilterDDLCabang.SelectedValue <> "ALL" Then
                sSql &= " AND cu.branch_code='" & FilterDDLCabang.SelectedValue & "'"
            End If

            If StatusNya.Text <> "ALL" Then
                sSql &= " AND cp.custapprovalstatus='" & StatusNya.SelectedValue & "'"
            End If

            sSql &= " Order By custapprovaloid Desc, custapprovaldate ASC"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_mstCustApp")
            Session("QL_mstCustApp") = objTable
            gvMst.DataSource = Session("QL_mstCustApp")
            gvMst.DataBind()

            sSql = "SELECT COUNT(*) From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcustapproval' AND approvaluser='" & Session("UserID") & "'"
            If GetScalar(sSql) > 0 Then
                gvMst.Columns(9).Visible = True
            Else
                gvMst.Columns(9).Visible = False
            End If
            gvMst.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub BindCity()
        Try
            sSql = "Select ct.genoid CityOId,ct.gencode KodeCity,ct.gendesc CityNya,pro.genoid ProvOid,pro.gendesc Provinsi From ql_mstgen ct Inner Join QL_mstgen pro ON pro.genoid=ct.genother2 AND pro.gengroup='PROVINCE' Where ct.gengroup='CITY' AND (ct.gencode LIKE '%" & TcharNoTrim(citynew.Text) & "%' OR ct.gendesc LIKE '%" & TcharNoTrim(citynew.Text) & "%' OR pro.gendesc LIKE '%" & TcharNoTrim(citynew.Text) & "%')"
            Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_mstcity")
            Session("QL_mstcity") = objTable
            GvCity.DataSource = Session("QL_mstcity")
            GvCity.DataBind()
            GvCity.Visible = True
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(dateAwal.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(dateAkhir.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub FillTextBox(ByVal iOid As Integer)
        Try
            sSql = "SELECT cp.*, cb.gendesc Cabang, cu.custcode, cu.custname, Isnull(cto.gendesc,'') OldCity, pr.gendesc ProvOldNya, Isnull(ct.gendesc,'') CityNya, Isnull(pro.gendesc,'') ProvNewNya, (SELECT ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) FROM ( select j.trnjualmstoid, j.trnamtjualnettoidr, j.accumpaymentidr, j.amtreturidr, j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=cu.custoid AND j.trnjualstatus='POST' AND branch_code=cu.branch_code) AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE))<=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr)) NewOverDue, custflagold, custflagnew, Convert(Varchar(20),cu.createtime,103) createtime, cu.notes FROM QL_mstcustapproval cp INNER JOIN QL_mstgen cb On cb.gencode=cp.branch_code AND cb.gengroup='CABANG' INNER JOIN QL_mstgen tr ON tr.genoid=cp.terminnew AND tr.gengroup='PAYTYPE' Left Join QL_mstgen ct ON ct.genoid=cp.citynew AND ct.gengroup='CITY' Inner Join QL_mstgen cto ON cto.genoid=cp.cityold AND cto.gengroup='CITY' Left Join QL_mstgen pro ON pro.genoid=cp.provnew AND pro.gengroup='PROVINCE' Inner Join QL_mstgen pr ON pr.genoid=cp.provold AND pr.gengroup='PROVINCE' Inner Join QL_mstcust cu ON cu.custoid=cp.custoid AND cp.branch_code=cu.branch_code Where cp.custapprovaloid=" & iOid
            Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_mstcustapproval")
            If dtMst.Rows.Count < 1 Then
                showMessage("Error Load Data", 2)
                Exit Sub
            Else
                cabangDDL.SelectedValue = dtMst.Rows(0)("branch_code").ToString
                custoid.Text = dtMst.Rows(0)("custoid").ToString
                CustCode.Text = dtMst.Rows(0)("custcode").ToString
                custapprovalno.Text = dtMst.Rows(0)("custapprovalno").ToString
                custapprovaldate.Text = Format(CDate(dtMst.Rows(0)("custapprovaldate")), "dd/MM/yyyy")
                custname.Text = dtMst.Rows(0)("custname").ToString
                custapprovaloid.Text = Integer.Parse(dtMst.Rows(0)("custapprovaloid"))
                custapprovaltype.SelectedValue = dtMst.Rows(0)("custapprovaltype").ToString
                DDLTermin()
                TerminOld.SelectedValue = dtMst.Rows(0)("terminold")
                TerminNew.SelectedValue = dtMst.Rows(0)("terminnew")
                addrold.Text = dtMst.Rows(0)("addrold").ToString
                cityold.Text = dtMst.Rows(0)("OldCity").ToString
                OidCityOld.Text = dtMst.Rows(0)("cityold")
                CLAwalOld.Text = ToMaskEdit(dtMst.Rows(0)("CLAwalOld"), 3)
                CLTempOld.Text = ToMaskEdit(dtMst.Rows(0)("CLTempOld"), 3)
                clusage.Text = ToMaskEdit(dtMst.Rows(0)("clusage"), 3)
                clsisa.Text = ToMaskEdit(dtMst.Rows(0)("clsisa"), 3)
                OidProvOld.Text = Integer.Parse(dtMst.Rows(0)("provold"))
                ProvOld.Text = dtMst.Rows(0)("ProvOldNya")
                telprmhold.Text = dtMst.Rows(0)("telprmhold").ToString
                telpktrold.Text = dtMst.Rows(0)("telpktrold").ToString
                telpwaold.Text = dtMst.Rows(0)("telpwaold").ToString
                addrnew.Text = dtMst.Rows(0)("addrnew").ToString
                CityOidNew.Text = dtMst.Rows(0)("citynew")
                citynew.Text = dtMst.Rows(0)("CityNya")

                ProvOidNew.Text = dtMst.Rows(0)("provnew")
                provnew.Text = dtMst.Rows(0)("ProvNewNya")
                telprmhnew.Text = dtMst.Rows(0)("telprmhnew")
                telpktrnew.Text = dtMst.Rows(0)("telpktrnew")
                telpwanew.Text = dtMst.Rows(0)("telpwanew")

                custflagOld.SelectedValue = dtMst.Rows(0)("custflagold")
                custflagNew.SelectedValue = dtMst.Rows(0)("custflagnew")
                clawalnew.Text = ToMaskEdit(dtMst.Rows(0)("clawalnew"), 3)
                CLTempNew.Text = ToMaskEdit(dtMst.Rows(0)("CLTempNew"), 3)
                cloverdue.Text = ToMaskEdit(dtMst.Rows(0)("NewOverDue"), 3)
                cltotal.Text = ToMaskEdit(dtMst.Rows(0)("cltotal"), 3)

                custapprovalstatus.Text = dtMst.Rows(0)("custapprovalstatus")
                custapprovalnote.Text = dtMst.Rows(0)("custapprovalnote")
                createtime.Text = Format(dtMst.Rows(0)("crttime"), "dd/MM/yyyy HH:mm:ss.fff")
                create.Text = "<br />Created By <B>" & dtMst.Rows(0)("crtuser").ToString & "</B> On <B>" & dtMst.Rows(0)("crttime").ToString & "</B> "
                update.Text = "Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "
                TglGabung.Text = dtMst.Rows(0)("createtime").ToString
                Catatan.Text = dtMst.Rows(0)("notes").ToString
            End If

            If custapprovalstatus.Text = "APPROVED" Then
                btnSave.Visible = False : btnDelete.Visible = False
                BtnSendApp.Visible = False : BtnApproved.Visible = False
                BtnRejected.Visible = False : custapprovaltype.Enabled = False
            ElseIf custapprovalstatus.Text = "IN APPROVAL" Then
                btnSave.Visible = False : btnDelete.Visible = False
                BtnSendApp.Visible = False : BtnApproved.Visible = False
                BtnRejected.Visible = False : custapprovaltype.Enabled = False
            ElseIf custapprovalstatus.Text = "IN PROCESS" Then
                btnSave.Visible = True : btnDelete.Visible = True
                BtnSendApp.Visible = True : BtnApproved.Visible = False
                BtnRejected.Visible = False
            ElseIf custapprovalstatus.Text = "REJECTED" Then
                btnSave.Visible = False : btnDelete.Visible = False
                BtnSendApp.Visible = False : BtnApproved.Visible = False
                BtnRejected.Visible = False : custapprovaltype.Enabled = False
            End If

            Dim QlTwS As String = GetStrData("SELECT branch_code From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcustapproval' And approvaluser='" & Session("UserID") & "' order by approvallevel")
            Dim sCodeTTs As String = ""
            Dim TTsCode() As String = QlTwS.Split(",")
            For C1 As Integer = 0 To TTsCode.Length - 1
                If TTsCode(C1) <> "" Then
                    sCodeTTs &= "'" & TTsCode(C1).Trim & "',"
                End If
            Next

            sSql = "Select Count(*) From QL_mstcustapproval tw inner join ql_approval b on tw.custapprovaloid = b.oid AND b.branch_code=tw.branch_code Where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("UserID") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.custapprovalstatus = 'in approval' and b.tablename = 'QL_mstcustapproval' and tw.branch_code IN (" & Left(sCodeTTs, sCodeTTs.Length - 1) & ")"

            If ToDouble(GetScalar(sSql)) > 0 Then
                If custapprovalstatus.Text = "IN PROCESS" Then
                    BtnApproved.Visible = False : BtnRejected.Visible = False
                ElseIf custapprovalstatus.Text = "APPROVED" Then
                    BtnApproved.Visible = False : BtnRejected.Visible = False
                    custapprovaltype.Enabled = False
                ElseIf custapprovalstatus.Text = "IN APPROVAL" Then
                    BtnApproved.Visible = True : BtnRejected.Visible = True
                    custapprovaltype.Enabled = False
                ElseIf custapprovalstatus.Text = "REJECTED" Then
                    BtnApproved.Visible = False : BtnRejected.Visible = False
                    custapprovaltype.Enabled = False
                End If
            End If
            IndexChanged()

            sSql = "Select b.approvaloid From QL_mstcustapproval tw inner join ql_approval b on tw.custapprovaloid = b.oid AND b.branch_code=tw.branch_code Where tw.cmpcode = '" & CompnyCode & "' and b.approvaluser = '" & Session("UserID") & "' and b.event = 'in approval' and b.statusrequest = 'new' and tw.custapprovalstatus = 'in approval' and b.tablename = 'QL_mstcustapproval' and tw.branch_code IN (" & Left(sCodeTTs, sCodeTTs.Length - 1) & ")"
            Dim DtAp As DataTable = cKon.ambiltabel2(sSql, "ql_approval")
            If DtAp.Rows.Count > 0 Then
                For i As Integer = 0 To DtAp.Rows.Count - 1
                    sAppOid.Text = DtAp.Rows(i)("approvaloid")
                Next
            End If
            sSql = "SELECT CASE WHEN ISNULL(SUM(late),0.0) = 0 THEN 0 WHEN ISNULL(COUNT(totalnota),0.0) = 0 THEN 0 ELSE ISNULL(SUM(late),0.0) / ISNULL(COUNT(totalnota),0.0) END FROM (SELECT case when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) < payduedate then 0 when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) <> 0 then (case when DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) < 0 then 0 else DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) end) when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) > payduedate then DATEDIFF(d,payduedate, (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC)) else 0 end as late, trnjualmstoid totalnota FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=con.refoid and jm.branch_code = con.branch_code AND jm.trncustoid=con.custoid AND jm.branch_code=con.branch_code WHERE reftype ='ql_trnjualmst' AND con.trnartype IN ('AR','piutang') and jm.trncustoid = " & custoid.Text & " ) gg"
            averagelate.Text = GetStrData(sSql)
            If averagelate.Text = "?" Then
                averagelate.Text = 0.0
            End If
        Catch ex As Exception
            showMessage(ex.ToString & " <br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub

    Private Sub DataBindTrn()
        Try
            sSql = "SELECT trnjualno, CONVERT(varchar,trnjualdate,103) AS trnjualdate, CONVERT(varchar,payduedate,103) AS payduedate,ISNULL(SUM(trnamtjualnettoidr-(accumpaymentidr+amtreturidr)),0.0) trnamtjualnetto FROM ( select j.trnjualmstoid, j.trnjualno,j.trnamtjualnettoidr,j.accumpaymentidr,j.amtreturidr,j.trnjualdate, (SELECT CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE) FROM QL_conar ar WHERE ar.reftype='ql_trnjualmst' and ar.payrefoid=0 and ar.branch_code=j.branch_code and ar.trnartype IN ('AR','PIUTANG') and ar.refoid=j.trnjualmstoid) payduedate, CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AS today from QL_trnjualmst j WHERE j.trncustoid=" & custoid.Text & " AND j.trnjualstatus='POST') AS ar WHERE DATEADD(D,7,CAST(CONVERT(varchar(10),ar.payduedate,102) AS DATE)) <=CAST(CONVERT(varchar(10),GETDATE(),102) AS DATE) AND ar.trnamtjualnettoidr>(ar.accumpaymentidr+ar.amtreturidr) group by trnjualno,trnjualdate, payduedate"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")
            Session("TblListMat") = dt
            gvListNya.DataSource = Session("TblListMat")
            gvListNya.DataBind() : gvListNya.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString & " <br />" & sSql, 1)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Master\MstPengajuan.aspx")
        End If

        Page.Title = CompnyName & " - Customer Approval"
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan menghapus data ini ?');")
        'btnposting.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            dateAwal.Text = Format(GetServerTime.AddDays(-7), "dd/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
            custapprovaldate.Text = Format(GetServerTime(), "dd/MM/yyyy")
            DDLfCabang() : DDLTermin()
            DDLcabang() : BindDataList() 
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                TabContainer1.ActiveTabIndex = 0
                custapprovalno.Text = GenerateID("QL_mstcustapproval", CompnyCode)
                custapprovaloid.Text = GenerateID("QL_mstcustapproval", CompnyCode)
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = "" : custapprovalstatus.Text = "IN PROCESS" : btnDelete.Visible = False
                custapprovaltype_SelectedIndexChanged(Nothing, Nothing)
            End If

        End If
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = "0" : custname.Text = ""
        gvCust.Visible = False
        clawalnew.Text = "0.00"
        CLTempNew.Text = "0.00" : cltotal.Text = "0.00"
        CLAwalOld.Text = "0.00" : CLTempOld.Text = "0.00"
        CLTempOld.Text = "0.00" : clsisa.Text = "0.00"

        addrnew.Text = "" : citynew.Text = "" : CityOidNew.Text = "0"
        provnew.Text = "" : ProvOidNew.Text = "0" : telprmhnew.Text = ""
        telpktrnew.Text = "" : telpwanew.Text = ""

        addrold.Text = ""
        cityold.Text = "" : OidCityOld.Text = "0"
        ProvOld.Text = "" : OidProvOld.Text = "0"
        telprmhold.Text = ""
        telpktrold.Text = "" : telpwaold.Text = ""
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindDataList()
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        '    e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        'End If
    End Sub

    Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCust.PageIndexChanging
        gvCust.PageIndex = e.NewPageIndex
        BindCust() : gvCust.Visible = True
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\MstPengajuan.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearchCust.Click
        BindCust() : gvCust.Visible = True
    End Sub

    Protected Sub GvCity_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvCity.PageIndexChanging
        GvCity.PageIndex = e.NewPageIndex
        BindCity()
    End Sub

    Protected Sub GvCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvCity.SelectedIndexChanged
        Try
            CityOidNew.Text = GvCity.SelectedDataKey("CityOId").ToString
            citynew.Text = GvCity.SelectedDataKey("CityNya").ToString
            ProvOidNew.Text = GvCity.SelectedDataKey("ProvOid").ToString
            provnew.Text = GvCity.SelectedDataKey("Provinsi").ToString
            GvCity.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub BtnBersihCity_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnBersihCity.Click
        CityOidNew.Text = "0" : citynew.Text = ""
        ProvOidNew.Text = "0" : provnew.Text = ""
        GvCity.Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Try
            If custoid.Text = "0" Then
                sMsg &= "- Maaf, Tolong pilih customer dulu..!!<br />"
            End If

            If custapprovalnote.MaxLength.ToString > 250 Then
                sMsg &= "- Maaf, maximal 250 karakter ..!!<br />"
            End If

            If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
                If ToDouble(CLTempNew.Text) = 0.0 Then
                    sMsg &= "- Maaf, Tolong isi nominal Temporary..!!<br />"
                End If

                If ToDouble(CLTempNew.Text) = ToDouble(CLTempOld.Text) Then
                    sMsg &= "- Maaf,Nominal Temporary baru tidak boleh sama dengan nominal temporary Old..!!<br />"
                End If

            ElseIf custapprovaltype.SelectedValue = "TERMIN" Then
                If TerminNew.SelectedValue = TerminOld.SelectedValue Then
                    sMsg &= "- Maaf, Termin baru tidak boleh sama dengan termin old..!!<br />"
                End If

            ElseIf custapprovaltype.SelectedValue = "CL AWAL" Then
                If ToDouble(clawalnew.Text) = 0.0 Then
                    sMsg &= "- Maaf, Tolong isi nominal CL Awal New..!!<br />"
                End If

                If ToDouble(clawalnew.Text) = ToDouble(CLAwalOld.Text) Then
                    sMsg &= "- Maaf, Nominal CL awal New tidak boleh sama dengan nominal CL Awal Old..!!<br />"
                End If
            End If

            If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
                sSql = "SELECT c.createtime, c.custcreditlimit, c.custcreditlimitawal, c.custcreditlimittempo, c.custdtlstatus AS status, c.custdtlflag, c.updtime From ql_mstcustdtl c WHERE c.custoid=" & custoid.Text & " AND custdtlstatus='OPEN' Order by c.createtime DESC"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "ql_mstcustdtl")
                If ToDouble(dt.Rows.Count) > 0 Then
                    sMsg &= "Maaf, customer " & custname.Text & " masih ada pengajuan " & ToMaskEdit(dt.Rows(0).Item("custcreditlimittempo"), 3) & " dengan status " & dt.Rows(0).Item("status") & ", dan pengajuan hanya bisa dilakukan 1 x 24 jam" 
                End If
            End If

            If custapprovalstatus.Text = "IN APPROVAL" Then
                sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_mstcustapproval' And branch_code LIKE '%" & cabangDDL.SelectedValue & "%' order by approvallevel"
                Dim dtData2 As DataTable = cKon.ambiltabel(sSql, "QL_approvalstructure")
                If dtData2.Rows.Count > 0 Then
                    Session("TblApproval") = dtData2
                Else
                    sMsg &= "- Maaf, User untuk Cabang " & cabangDDL.SelectedItem.Text & " belum disetting, Silahkan hubungi admin..!!<br>"
                End If
            ElseIf custapprovalstatus.Text = "Rejected" Then
                custapprovalstatus.Text = "IN PROCESS"
            End If

            If checkApproval("QL_mstcustapproval", "QL_mstcustapproval", Session("oid"), "New", "FINAL", cabangDDL.SelectedValue) > 0 Then
                sMsg &= "- Maaf, data ini sudah send Approval..!!<br>"
                custapprovalstatus.Text = "IN APPROVAL"
            End If
 
            If Session("oid") = Nothing Or Session("oid") = "" Then
                If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_mstcustapproval WHERE crttime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                    sMsg &= "Maaf, Data sudah ter input, Klik tombol cancel dan mohon untuk cek data pada tab List form..!!<br>"
                End If
            Else
                ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
                sSql = "SELECT [custapprovalstatus] FROM QL_mstcustapproval WHERE custapprovaloid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & CompnyCode & "'"

                Dim srest As String = GetStrData(sSql)
                If srest Is Nothing Or srest = "" Then
                    sMsg &= "Maaf, Data tidak ditemukan..!<br />Periksa bila data telah dihapus oleh user lain..!!<br>"
                Else
                    If srest.ToLower = "post" Then
                        sMsg &= "Maaf, Status transaksi sudah terupdate!<br />Klik tombol Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!<br>"
                    End If
                End If
            End If

            If sMsg <> "" Then
                custapprovalstatus.Text = "IN PROCESS"
                showMessage(sMsg, 2)
                Exit Sub
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                custapprovaloid.Text = GenerateID("QL_mstcustapproval", CompnyCode)
            Else
                custapprovaloid.Text = Session("oid")
                custdtloid.Text = GenerateID("QL_mstcustdtl", CompnyCode)
            End If

            If custapprovalstatus.Text = "APPROVED" Then
                GeneratedCode()
            Else
                custapprovalno.Text = custapprovaloid.Text
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br />" & sSql, 1)
            custapprovalstatus.Text = "IN PROCESS" : Exit Sub
        End Try

        If custapprovaltype.SelectedValue = "CL AWAL" Then
            clawalnew.Text = ToDouble(clawalnew.Text)
            cltotal.Text = ToDouble(CLAwalOld.Text) + ToDouble(clawalnew.Text) + ToDouble(CLTempOld.Text)
        ElseIf custapprovaltype.SelectedValue = "CL TEMPORARY" Then
            CLTempNew.Text = ToDouble(CLTempNew.Text)
            cltotal.Text = ToDouble(CLTempNew.Text) + ToDouble(CLAwalOld.Text)
        End If

        Dim AppOid As Integer = GenerateID("QL_Approval", CompnyCode)

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO [QL_mstcustapproval] ([cmpcode], [custapprovaloid], [custapprovalno], [custapprovaldate], [custoid], [branch_code], [custapprovaltype], [custapprovalnote], [custapprovalstatus], [crtuser], [crttime], [upduser], [updtime], [clawalold], [clawalnew], [cltempold], [cltempnew], [terminold], [terminnew], [cltotal], [clusage], [clsisa], [cloverdue], [addrold], [cityold], [provold], [telprmhold], [telpktrold], [telpwaold], [addrnew], [citynew], [provnew], [telprmhnew], [telpktrnew], [telpwanew], [custflagold], [custflagnew]) VALUES" & _
                " ('" & CompnyCode & "', " & Integer.Parse(custapprovaloid.Text) & ", '" & Tchar(custapprovalno.Text) & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & Integer.Parse(custoid.Text) & ", '" & cabangDDL.SelectedValue & "', '" & custapprovaltype.SelectedValue & "', '" & Tchar(custapprovalnote.Text) & "', '" & custapprovalstatus.Text & "', '" & Session("UserID").ToString & "', '" & CDate(toDate(createtime.Text)) & "', '" & Session("UserID").ToString & "', CURRENT_TIMESTAMP, " & ToDouble(CLAwalOld.Text) & ", " & ToDouble(clawalnew.Text) & ", " & ToDouble(CLTempOld.Text) & ", " & ToDouble(CLTempNew.Text) & ", " & Integer.Parse(TerminOld.SelectedValue) & ", " & Integer.Parse(TerminNew.SelectedValue) & ", " & ToDouble(cltotal.Text) & ", " & ToDouble(clusage.Text) & ", " & ToDouble(clsisa.Text) & ", " & ToDouble(cloverdue.Text) & ", '" & Tchar(addrold.Text) & "', " & Integer.Parse(OidCityOld.Text) & ", " & Integer.Parse(OidProvOld.Text) & ", '" & Tchar(telprmhold.Text) & "', '" & Tchar(telpktrold.Text) & "', '" & Tchar(telpwaold.Text) & "', '" & Tchar(addrnew.Text) & "', " & Integer.Parse(CityOidNew.Text) & ", " & Integer.Parse(ProvOidNew.Text) & ", '" & Tchar(telprmhnew.Text) & "', '" & Tchar(telpktrnew.Text) & "', '" & Tchar(telpwanew.Text) & "', '" & custflagOld.SelectedValue & "', '" & custflagNew.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & Integer.Parse(custapprovaloid.Text) & " WHERE tablename='QL_mstcustapproval'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE [QL_mstcustapproval] SET[custapprovalno] = '" & Tchar(custapprovalno.Text) & "', [custapprovaldate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [custoid] = " & Integer.Parse(custoid.Text) & ", [custapprovalnote] ='" & Tchar(custapprovalnote.Text) & "', [custapprovalstatus] = '" & custapprovalstatus.Text & "', [upduser] = '" & Session("UserID") & "', [updtime]=Current_timestamp, [terminnew]=" & Integer.Parse(TerminNew.SelectedValue) & " , [cltotal]=" & ToDouble(cltotal.Text) & ", [clusage] = " & ToDouble(clusage.Text) & ", [clsisa] = " & ToDouble(clsisa.Text) & ",[addrnew] = '" & Tchar(addrnew.Text) & "', [citynew] = " & Integer.Parse(CityOidNew.Text) & ", [provnew] = " & Integer.Parse(ProvOidNew.Text) & ", [telprmhnew] = '" & Tchar(telprmhnew.Text) & "', [telpktrnew] = '" & Tchar(telpktrnew.Text) & "',[telpwanew] = '" & Tchar(telpwanew.Text) & "', custapprovaltype='" & custapprovaltype.SelectedValue & "', custflagold='" & custflagOld.SelectedValue & "', custflagnew='" & custflagNew.SelectedValue & "' WHERE custapprovaloid=" & custapprovaloid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If custapprovalstatus.Text = "IN APPROVAL" Then
                If Not Session("TblApproval") Is Nothing Then
                    Dim objTable As DataTable : objTable = Session("TblApproval")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, branch_code, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES" & _
                        " ('" & CompnyCode & "', " & AppOid & ", '" & cabangDDL.SelectedValue & "', '" & "CA" & AppOid & "_" & AppOid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_mstcustapproval', '" & custapprovaloid.Text & "', 'IN APPROVAL', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        AppOid += 1
                    Next
                    sSql = "UPDATE ql_mstoid set lastoid = " & AppOid - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If

            If custapprovalstatus.Text = "APPROVED" Then
                sSql = "UPDATE [QL_mstcustapproval] SET [custapprovalno] = '" & Tchar(custapprovalno.Text) & "', [custapprovaldate] = (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), [custoid] = " & Integer.Parse(custoid.Text) & ", [custapprovalnote] ='" & Tchar(custapprovalnote.Text) & "', [custapprovalstatus] = '" & custapprovalstatus.Text & "', [upduser] = '" & Session("UserID") & "', [updtime]=Current_timestamp, [terminnew] =" & Integer.Parse(TerminNew.SelectedValue) & ", [cltotal]=" & ToDouble(cltotal.Text) & ", [clusage] = " & ToDouble(clusage.Text) & ", [clsisa] = " & ToDouble(clsisa.Text) & ",[addrnew] = '" & Tchar(addrnew.Text) & "', [citynew] = " & Integer.Parse(CityOidNew.Text) & ", [provnew] = " & Integer.Parse(ProvOidNew.Text) & ", [telprmhnew] = '" & Tchar(telprmhnew.Text) & "', [telpktrnew] = '" & Tchar(telpktrnew.Text) & "', [telpwanew] = '" & Tchar(telpwanew.Text) & "' WHERE custapprovaloid=" & custapprovaloid.Text & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If custapprovaltype.SelectedValue = "CL AWAL" Then
                    '======= CL AWAL =======
                    sSql = "Update QL_mstcust set custcreditlimitrupiahawal=" & ToDouble(CLAwalOld.Text) + ToDouble(clawalnew.Text) & ", custcreditlimit=" & ToDouble(cltotal.Text) & ", custcreditlimitrupiah=" & ToDouble(cltotal.Text) & " Where [custoid] = " & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus, updtime) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(cltotal.Text) & ", " & ToDouble(CLTempOld.Text) & ", " & ToDouble(CLAwalOld.Text) + ToDouble(clawalnew.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Closed', 'Awal',(Select custoverduestatus from QL_mstcust Where custoid =" & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'), Current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ElseIf custapprovaltype.SelectedValue = "CL TEMPORARY" Then
                    sSql = "Update QL_mstcust set custcreditlimitrupiahtempo=" & ToDouble(CLTempNew.Text) & ", custcreditlimit=" & ToDouble(cltotal.Text) & ", custcreditlimitrupiah=" & ToDouble(cltotal.Text) & " Where [custoid] = " & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus, updtime) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(cltotal.Text) & ", " & ToDouble(CLTempNew.Text) & ", " & ToDouble(CLAwalOld.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Open', 'Tempo', (Select custoverduestatus from QL_mstcust Where custoid =" & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'), Current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ElseIf custapprovaltype.SelectedValue = "TERMIN" Then
                    sSql = "Update QL_mstcust set timeofpayment='" & TerminNew.SelectedValue & "' Where [custoid] = " & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ElseIf custapprovaltype.SelectedValue = "OVERDUE" Then
                    sSql = "Update QL_mstcust set custoverduestatus='Y' Where [custoid] = " & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT INTO QL_mstcustdtl (custdtloid, custoid, custcreditlimit, custcreditlimittempo, custcreditlimitawal, createuser, createtime, custdtlstatus, custdtlflag, custoverduestatus, updtime) VALUES (" & custdtloid.Text & ", " & custoid.Text & ", " & ToDouble(cltotal.Text) & ", " & ToDouble(CLTempOld.Text) & ", " & ToDouble(CLAwalOld.Text) & ", '" & Session("UserID") & "', current_timestamp, 'Enabled', 'Over Due', 'Y', Current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ElseIf custapprovaltype.SelectedValue = "REVISI DATA" Then

                    sSql = "Update QL_mstcust set custaddr='" & Tchar(addrnew.Text) & "', custcityoid=" & Integer.Parse(CityOidNew.Text) & ", phone1='" & Tchar(telprmhnew.Text) & "', phone2='" & Tchar(telpktrnew.Text) & "', phone3='" & Tchar(telpwanew.Text) & "', custflag='" & custflagNew.SelectedValue & "' Where [custoid] = " & Integer.Parse(custoid.Text) & " AND branch_code='" & cabangDDL.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                sSql = "update QL_mstoid set lastoid=" & custdtloid.Text & " Where tablename like 'QL_mstcustdtl' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                ' Update Approval
                sSql = "UPDATE QL_approval SET " & _
                        "approvaldate=CURRENT_TIMESTAMP, " & _
                        "approvalstatus='Approved', " & _
                        "event='Approved', " & _
                        "statusrequest='End' " & _
                        "WHERE cmpcode='" & CompnyCode & "' " & _
                        "and tablename='QL_mstcustapproval' " & _
                        "and oid=" & Integer.Parse(custapprovaloid.Text) & " " & _
                        "and approvaluser='" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'update event user yg lain 
                sSql = "UPDATE QL_approval SET " & _
                       "approvaldate=CURRENT_TIMESTAMP, " & _
                       "approvalstatus='Ignore', " & _
                       "event='Ignore', " & _
                       "statusrequest='End' " & _
                       "WHERE cmpcode='" & CompnyCode & "' " & _
                       "and tablename='QL_mstcustapproval' " & _
                       "and oid=" & Integer.Parse(custapprovaloid.Text) & " " & _
                       "and approvaluser<>'" & Session("UserId") & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If custapprovalstatus.Text = "REJECTED" Then
                sSql = "Update QL_mstcustapproval Set custapprovalstatus='REJECTED' Where custapprovaloid=" & Session("oid") & "" : xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ' Update Approval
                sSql = "UPDATE ql_approval SET event='Rejected', statusrequest='Rejected' WHERE cmpcode = '" & CompnyCode & "' AND approvaloid = '" & sAppOid.Text & "' And branch_code='" & cabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                ' Update Approval user yg lain 
                sSql = "update QL_approval set event='Ignore', statusrequest='End' WHERE cmpcode='" & CompnyCode & "' And tablename='QL_mstcustapproval' And oid=" & Session("oid") & " AND approvaloid <> '" & sAppOid.Text & "' And branch_code='" & cabangDDL.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : xCmd.Connection.Close()

        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            custapprovalstatus.Text = "IN PROCESS" : Exit Sub
        End Try

        If custapprovalstatus.Text = "APPROVED" Then
            Response.Redirect("~\other\Menu.aspx?page=true")
        ElseIf custapprovalstatus.Text = "REJECTED" Then
            Response.Redirect("~\Master\MstPengajuan.aspx?awal=true")
        Else
            Response.Redirect("~\Master\MstPengajuan.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\MstPengajuan.aspx")
    End Sub

    Protected Sub BtnSendApp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSendApp.Click
        custapprovalstatus.Text = "IN APPROVAL" : btnSave_Click(sender, e)
    End Sub

    Protected Sub BtnCariCity_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCariCity.Click
        If custoid.Text = "0" Then
            showMessage("- Maaf, Tolong pilih customer dulu..!!<br />", 2) : Exit Sub
        End If
        BindCity()
    End Sub

    Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCust.SelectedIndexChanged
        Try
            custoid.Text = gvCust.SelectedDataKey("custoid").ToString
            custname.Text = gvCust.SelectedDataKey("custname").ToString
            If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
                sSql = "SELECT c.createtime, c.custcreditlimit, c.custcreditlimitawal, c.custcreditlimittempo, c.custdtlstatus AS status, c.custdtlflag, c.updtime From ql_mstcustdtl c WHERE c.custoid=" & custoid.Text & " AND custdtlstatus='OPEN' Order by c.createtime DESC"
                Dim dt As DataTable = cKon.ambiltabel(sSql, "ql_mstcustdtl")
                If ToDouble(dt.Rows.Count) > 0 Then
                    Dim sMsg = "Maaf, customer " & custname.Text & " masih ada pengajuan " & ToMaskEdit(dt.Rows(0).Item("custcreditlimittempo"), 3) & " dengan status " & dt.Rows(0).Item("status") & ", dan pengajuan hanya bisa dilakukan 1 x 24 jam"
                    showMessage(sMsg, 2)
                    Exit Sub
                End If
            End If
            DDLTermin()
            TerminOld.SelectedValue = gvCust.SelectedDataKey("TerminOid").ToString
            TerminNew.SelectedValue = gvCust.SelectedDataKey("TerminOid").ToString

            addrold.Text = gvCust.SelectedDataKey("alamat").ToString
            addrnew.Text = gvCust.SelectedDataKey("alamat").ToString

            OidCityOld.Text = gvCust.SelectedDataKey("custcityoid").ToString
            CityOidNew.Text = gvCust.SelectedDataKey("custcityoid").ToString
            cityold.Text = gvCust.SelectedDataKey("Kota").ToString
            citynew.Text = gvCust.SelectedDataKey("Kota").ToString

            OidProvOld.Text = gvCust.SelectedDataKey("custprovoid").ToString
            ProvOld.Text = gvCust.SelectedDataKey("Provinsi").ToString
            ProvOidNew.Text = gvCust.SelectedDataKey("custprovoid").ToString
            provnew.Text = gvCust.SelectedDataKey("Provinsi").ToString

            telprmhold.Text = gvCust.SelectedDataKey("TelpRmh").ToString
            telprmhnew.Text = gvCust.SelectedDataKey("TelpRmh").ToString

            telpktrold.Text = gvCust.SelectedDataKey("TlpKantor").ToString
            telpktrnew.Text = gvCust.SelectedDataKey("TlpKantor").ToString

            telpwaold.Text = gvCust.SelectedDataKey("TelpWa").ToString
            telpwanew.Text = gvCust.SelectedDataKey("TelpWa").ToString

            CustCode.Text = gvCust.SelectedDataKey("custcode").ToString
            CLAwalOld.Text = ToMaskEdit(gvCust.SelectedDataKey("CrAwal"), 3)
            CLTempOld.Text = ToMaskEdit(gvCust.SelectedDataKey("CrTempo"), 3)
            clusage.Text = ToMaskEdit(gvCust.SelectedDataKey("CrUsage"), 3)
            cloverdue.Text = ToMaskEdit(gvCust.SelectedDataKey("CrOverDue"), 3)
            clsisa.Text = ToMaskEdit(gvCust.SelectedDataKey("CrLimit") - gvCust.SelectedDataKey("CrUsage"), 3)
            cltotal.Text = ToMaskEdit(gvCust.SelectedDataKey("CrAwal"), 3)
            custflagOld.SelectedValue = gvCust.SelectedDataKey("custflag").ToString
            If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
                cltotal.Text = ToMaskEdit(gvCust.SelectedDataKey("CrAwal"), 3)
            Else
                cltotal.Text = ToMaskEdit(gvCust.SelectedDataKey("CrAwal"), 3)
            End If

            Catatan.Text = gvCust.SelectedDataKey("Catatan").ToString
            TglGabung.Text = gvCust.SelectedDataKey("tglGabung").ToString
            gvCust.Visible = False

            sSql = "SELECT CASE WHEN ISNULL(SUM(late),0.0) = 0 THEN 0 WHEN ISNULL(COUNT(totalnota),0.0) = 0 THEN 0 ELSE ISNULL(SUM(late),0.0) / ISNULL(COUNT(totalnota),0.0) END FROM (SELECT case when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) < payduedate then 0 when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) <> 0 then (case when DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) < 0 then 0 else DATEDIFF(d,payduedate, CURRENT_TIMESTAMP) end) when con.amttrans - (select isnull(SUM(amtbayaridr),0) from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR')) = 0 AND (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC) > payduedate then DATEDIFF(d,payduedate, (select TOP 1 paymentdate from QL_conar where refoid = con.refoid and reftype IN ('QL_creditnote', 'QL_debitnote', 'QL_trnpayar', 'QL_trnjualmst') AND trnartype IN ('AR', 'CNAR', 'DNAR', 'PAYAR') ORDER BY updtime DESC)) else 0 end as late, trnjualmstoid totalnota FROM QL_conar con INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=con.refoid and jm.branch_code = con.branch_code AND jm.trncustoid=con.custoid AND jm.branch_code=con.branch_code WHERE reftype ='ql_trnjualmst' AND con.trnartype IN ('AR','piutang') and jm.trncustoid = " & custoid.Text & " ) gg"
            averagelate.Text = GetStrData(sSql)
            If averagelate.Text = "?" Then
                averagelate.Text = 0.0
            End If

        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub

    Protected Sub clawalnew_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles clawalnew.TextChanged
        clawalnew.Text = ToMaskEdit(ToDouble(clawalnew.Text), 3)
        cltotal.Text = ToMaskEdit(ToDouble(CLAwalOld.Text) + ToDouble(clawalnew.Text) + ToDouble(CLTempOld.Text), 3)
        clsisa.Text = ToMaskEdit(ToDouble(CLAwalOld.Text) + ToDouble(clawalnew.Text) + ToDouble(CLTempOld.Text) - ToDouble(clusage.Text), 3)
    End Sub

    Protected Sub custapprovaltype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles custapprovaltype.SelectedIndexChanged
        If custapprovaltype.SelectedValue = "CL TEMPORARY" Then
            clawalnew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = True : CLTempNew.Visible = True
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
            CLTempNew_TextChanged(Nothing, Nothing)
        ElseIf custapprovaltype.SelectedValue = "TERMIN" Then
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = True : TerminNew.Visible = True
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "REVISI DATA" Then
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = True : Lbladdrnew.Visible = True
            Lblcitynew.Visible = True : citynew.Visible = True
            BtnCariCity.Visible = True : BtnBersihCity.Visible = True
            Lblprovnew.Visible = True : provnew.Visible = True
            Lbltelprmhnew.Visible = True : telprmhnew.Visible = True
            Lbltelpktrnew.Visible = True : telpktrnew.Visible = True
            Lbltelpwanew.Visible = True : telpwanew.Visible = True
            Label8.Visible = True : custflagOld.Visible = True
            Label10.Visible = True : custflagNew.Visible = True
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "CL AWAL" Then
            Lblclawalnew.Visible = True : clawalnew.Visible = True
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            clawalnew_TextChanged(Nothing, Nothing)
            CLTempNew.Text = "0.00" : cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = False
        ElseIf custapprovaltype.SelectedValue = "OVERDUE" Then
            Lblclawalnew.Visible = False : clawalnew.Visible = False
            LblCLTempNew.Visible = False : CLTempNew.Visible = False
            LblTerminNew.Visible = False : TerminNew.Visible = False
            addrnew.Visible = False : Lbladdrnew.Visible = False
            Lblcitynew.Visible = False : citynew.Visible = False
            BtnCariCity.Visible = False : BtnBersihCity.Visible = False
            Lblprovnew.Visible = False : provnew.Visible = False
            Lbltelprmhnew.Visible = False : telprmhnew.Visible = False
            Lbltelpktrnew.Visible = False : telpktrnew.Visible = False
            Lbltelpwanew.Visible = False : telpwanew.Visible = False
            Label8.Visible = False : custflagOld.Visible = False
            Label10.Visible = False : custflagNew.Visible = False
            clawalnew.Text = "0.00" : CLTempNew.Text = "0.00"
            cltotal.Text = ToMaskEdit(CLAwalOld.Text, 3)
            btnInfoNota.Visible = True
        End If
      clear()
    End Sub

    Protected Sub CLTempNew_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CLTempNew.TextChanged
        CLTempNew.Text = ToMaskEdit(ToDouble(CLTempNew.Text), 3)
        cltotal.Text = ToMaskEdit(ToDouble(CLTempNew.Text) + ToDouble(CLAwalOld.Text), 3)
    End Sub

    Protected Sub BtnApproved_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnApproved.Click
        custapprovalstatus.Text = "APPROVED" : btnSave_Click(sender, e)
    End Sub
 
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                showMessage("- Maaf, Tidak ada data yg bisa di hapus", 2)
            End If

            sSql = "DELETE QL_mstcustapproval WHERE cmpcode = '" & CompnyCode & "' AND custapprovaloid=" & Integer.Parse(custapprovaloid.Text) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()

        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            custapprovalstatus.Text = "IN PROCESS" : Exit Sub
        End Try
        Response.Redirect("~\master\MstPengajuan.aspx?awal=true")
    End Sub
  
    Protected Sub cabangDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cabangDDL.SelectedIndexChanged
        clear()
    End Sub

    Protected Sub BtnFlagStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim OidApp As Integer = sender.ToolTip
        Dim FlagStatus As String = sender.Text
        Dim custapprovalstatus As String = sender.CommandName
        If FlagStatus.ToString = "OPEN" And custapprovalstatus = "APPROVED" Then
            FlagStatus = "CLOSED"
        Else
            FlagStatus = "OPEN"
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If custapprovalstatus = "APPROVED" Then
                sSql = "Update QL_mstcustapproval Set flagstatus='" & FlagStatus & "' Where custapprovaloid=" & OidApp & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                showMessage("- Maaf, Status data transaksi Harus APPROVED..!!", 2)
                Exit Sub
            End If
            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\MstPengajuan.aspx?awal=true")
    End Sub

    Protected Sub BtnRejected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRejected.Click
        custapprovalstatus.Text = "REJECTED" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        BindDataList()
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
        If Session("branch") = "10" Then
            FilterDDLCabang.SelectedValue = "ALL"
        End If
        StatusNya.Text = "ALL" : FilterNya.Text = ""
        dateAwal.Text = Format(GetServerTime.AddDays(-30), "dd/MM/yyyy")
        dateAkhir.Text = Format(GetServerTime(), "dd/MM/yyyy")
        BindDataList()
    End Sub

    Protected Sub btnInfoNota_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInfoNota.Click
        DataBindTrn() : mpeListMat.Show()
    End Sub

    Protected Sub gvListNya_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListNya.PageIndexChanging
        gvListNya.PageIndex = e.NewPageIndex
        DataBindTrn() : mpeListMat.Show()
    End Sub

    Protected Sub gvListNya_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListNya.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
        End If
    End Sub 

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
#End Region 
End Class
