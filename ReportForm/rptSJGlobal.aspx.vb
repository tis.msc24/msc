Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class ReportForm_frmpostat
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptSJGlobal.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Laporan SJ Global"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Not Page.IsPostBack Then
            'Session("showReport") = False
            ddlCabang()
            date1.Text = Format(GetServerTime(), "01/MM/yyyy")
            date2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            'SuppBind()
        End If
        'If Session("showReport") = True Then
        '    showPrint("")
        'End If
    End Sub

    Private Sub ddlCabang()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(BranchDDL, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(BranchDDL, sSql)
            Else
                FillDDL(BranchDDL, sSql)
                BranchDDL.Items.Add("ALL BRANCH")
                BranchDDL.SelectedValue = "ALL BRANCH"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(BranchDDL, sSql)
            BranchDDL.Items.Add("ALL BRANCH")
            BranchDDL.SelectedValue = "ALL BRANCH"
        End If
         
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Sub showPrint(ByVal ekstensi As String)
        Try
            Dim dat1 As Date = CDate(toDate(date1.Text))
            Dim dat2 As Date = CDate(toDate(date2.Text))
        Catch ex As Exception
            labelmsg.Text = "Period report is invalid!"
            Exit Sub
        End Try
        If CDate(toDate(date1.Text)) > CDate(toDate(date2.Text)) Then
            labelmsg.Text = "Period 2 must be more than Period 1!"
            Exit Sub
        End If
        CrystalReportViewer1.Visible = True
        Try
            Dim sWhere As String = "" : Dim d1 As String = "" : Dim d2 As String = ""
           
            d1 = CDate(toDate(date1.Text))
            d2 = CDate(toDate(date2.Text))
            sWhere &= "Where tglSj between '" & d1 & "' and '" & d2 & "'"
            If trnPono.Text <> "" Then
                sWhere &= "and sjm.globalno = '" & trnPono.Text & "'"
            End If

            If Itemdesc.Text <> "" Then
                sWhere &= "and sjd.item = '" & Itemdesc.Text & "'"
            End If

            If BranchDDL.SelectedValue <> "ALL BRANCH" Then
                sWhere &= "and sjm.branch_code='" & BranchDDL.SelectedValue & "'"
            End If

            vReport = New ReportDocument
            If ekstensi.ToUpper = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\rptSJExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptSJ.rpt"))
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)
            'CrystalReportViewer1.DisplayGroupTree = False
            vReport.SetParameterValue("d1", d1)
            vReport.SetParameterValue("d2", d2)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            vReport.SetParameterValue("sWhere", sWhere)
            If ekstensi = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                If ekstensi = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = vReport
                End If
            End If
        Catch ex As Exception
            labelmsg.Text = ex.ToString
        End Try
    End Sub

    Private Sub BindSJ()
        sSql = "select sjglobalmstoid, sjglobalno, convert(varchar(10),tglSj,103) tglSj from QL_trnSjGlobalmst where statusSjGlobal = 'POST' AND sjglobalno LIKE '%" & Tchar(trnPono.Text) & "%' Order By sjglobalmstoid Desc"
        FillGV(poNoGV, sSql, "QL_trnSjGlobalmst")
        poNoGV.Visible = True
    End Sub

    Private Sub Binditem()
        Dim sWhere As String = ""
        If trnPono.Text <> "" Then
            sWhere = "and sjm.sjglobalno = '" & trnPono.Text & "'"
        End If

        sSql = "select i.itemoid, i.itemcode, i.itemdesc from ql_mstitem i left join QL_trnSjGlobaldtl sjd on sjd.item = i.itemdesc inner join QL_trnSjGlobalmst sjm on sjm.sjglobalmstoid=sjd.sjglobalmstoid where i.itemdesc LIKE '%" & Tchar(Itemdesc.Text) & "%' AND i.itemcode LIKE '%" & Tchar(Itemdesc.Text) & "%'" & sWhere & ""
        FillGV(SjNoGV, sSql, "ql_mstitem")
        SjNoGV.Visible = True
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        'Session("showReport") = True
        showPrint("")
    End Sub

    Protected Sub dView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub detailstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetReport()
    End Sub

    Private Sub ResetReport()
        Session("showReport") = False
        CrystalReportViewer1.ReportSource = Nothing
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("EXCEL")
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("PDF")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptSJGlobal.aspx?awal=true")
    End Sub

    Protected Sub ImgPono_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgPono.Click
        BindSJ()
    End Sub

    Protected Sub poNoGV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        poNoGV.PageIndex = e.NewPageIndex
        BindSJ()
    End Sub

    Protected Sub poNoGV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        trnPono.Text = poNoGV.SelectedDataKey.Item("sjglobalno")
        SJoid.Text = poNoGV.SelectedDataKey.Item("sjglobalmstoid")
        poNoGV.Visible = False
        ResetReport()
    End Sub

    Protected Sub ImgErasePono_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trnPono.Text = "" : SJoid.Text = ""
        ResetReport()
        poNoGV.Visible = False
    End Sub

    Protected Sub imgSjno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Binditem()
    End Sub

    Protected Sub SjNoGV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Itemdesc.Text = SjNoGV.SelectedDataKey.Item("itemdesc")
        Itemoid.Text = SjNoGV.SelectedDataKey.Item("itemoid")
        SjNoGV.Visible = False
        ResetReport()
    End Sub

    Protected Sub EraseSjNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Itemdesc.Text = "" : Itemoid.Text = ""
        ResetReport()
        SjNoGV.Visible = False
    End Sub

    Protected Sub SjNoGV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        SjNoGV.PageIndex = e.NewPageIndex
        Binditem()
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        showPrint("")
    End Sub
End Class
