﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmGiroOutStatus.aspx.vb" Inherits="ReportForm_frmGiroOutStatus" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Manifest Giro Out Status Report" CssClass="Title" ForeColor="SaddleBrown" Width="262px"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="upReportForm" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="500px" DefaultButton="btnViewReport"><TABLE><TBODY><TR><TD class="Label" align=left><asp:Label id="lblStatus" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLStatus" runat="server" CssClass="inpText"><asp:ListItem>All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblBankAcc" runat="server" Text="Bank Account"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="DDLBankAccount" runat="server" Width="250px" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left rowSpan=2><asp:DropDownList id="DDLDate" runat="server" Width="105px" CssClass="inpText">
                                                <asp:ListItem Value="[Cash/Bank Date]">Cash/Bank Date</asp:ListItem>
                                            </asp:DropDownList></TD><TD class="Label" align=center rowSpan=2>:</TD></TR><TR><TD class="Label" align=left><asp:TextBox id="FilterPeriod1" runat="server" Width="80px" CssClass="inpText" ReadOnly="True" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" Width="80px" CssClass="inpText" ReadOnly="True" ToolTip="dd/MM/yyyy"></asp:TextBox> <asp:ImageButton id="CalPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblInfoDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD id="TD1" class="Label" align=left rowSpan=1 runat="server" visible="false">Type</TD><TD id="TD2" class="Label" align=center rowSpan=1 runat="server" visible="false">:</TD><TD id="TD3" class="Label" align=left runat="server" visible="false"><asp:RadioButton id="rbAll" runat="server" Text="ALL" Checked="True" GroupName="rb"></asp:RadioButton>&nbsp;<asp:RadioButton id="rbInternal" runat="server" Text="INTERNAL" GroupName="rb"></asp:RadioButton>&nbsp;<asp:RadioButton id="rbExternal" runat="server" Text="EXTERNAL" GroupName="rb"></asp:RadioButton></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:DropDownList id="DDLSupp" runat="server" Width="105px" CssClass="inpText">
                                                <asp:ListItem Value="[Supplier Code]">Supplier Code</asp:ListItem>
                                                <asp:ListItem Value="[Supplier]">Supplier Name</asp:ListItem>
                                            </asp:DropDownList></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="TextSupplier" runat="server" Width="250px" CssClass="inpText" Rows="2" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left rowSpan=1><asp:Label id="lblBGKNo" runat="server" Text="BGK No."></asp:Label></TD><TD class="Label" align=center rowSpan=1>:</TD><TD class="Label" align=left><asp:TextBox id="TextBGKNo" runat="server" Width="250px" CssClass="inpText" Rows="2" TextMode="MultiLine"></asp:TextBox> <asp:ImageButton id="btnSearchBGK" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="btnClearBGK" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD4" class="Label" align=left rowSpan=1 runat="server" Visible="false"><asp:Label id="lblSorting" runat="server" Text="Sorting"></asp:Label></TD><TD id="TD6" class="Label" align=center rowSpan=1 runat="server" Visible="false">:</TD><TD id="TD5" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="DDLSorting" runat="server" Width="105px" CssClass="inpText">
                                                <asp:ListItem Value="[Cash/Bank Date]">Cash/Bank Date</asp:ListItem>
                                                <asp:ListItem Value="[Giro No.]">Giro No.</asp:ListItem>
                                                <asp:ListItem Value="[Supplier]">Supplier</asp:ListItem>
                                                <asp:ListItem Value="[COA]">Bank Account</asp:ListItem>
                                                <asp:ListItem Value="[Amount]">Amount</asp:ListItem>
                                                <asp:ListItem Value="[Status]">Status</asp:ListItem>
                                                <asp:ListItem Value="[Create User]">Create User</asp:ListItem>
                                            </asp:DropDownList> <asp:DropDownList id="DDLOrderby" runat="server" Width="50px" CssClass="inpText">
                                                <asp:ListItem Value="ASC">ASC</asp:ListItem>
                                                <asp:ListItem Value="DESC">DESC</asp:ListItem>
                                            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="cePeriod1" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalPeriod1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod1" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cePeriod2" runat="server" Format="dd/MM/yyyy" PopupButtonID="CalPeriod2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meePeriod2" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD class="Label" align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upReportForm">
                                                <ProgressTemplate>
                                                    <span></span><span style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">
                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><br />
                                                        Please Wait .....</span><br />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress> </TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" AutoDataBind="True" HasViewList="False" HasCrystalLogo="False" HasPrintButton="False" HasExportButton="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False"></CR:CrystalReportViewer> 
</ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
                        <asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upListSupp" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" CssClass="modalBox" DefaultButton="btnFindListSupp" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" Width="100px" CssClass="inpText">
                                            <asp:ListItem Value="[Supplier Code]">Code</asp:ListItem>
                                            <asp:ListItem Value="[Supplier]">Name</asp:ListItem>
                                            <asp:ListItem Value="suppaddr">Address</asp:ListItem>
                                        </asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" Width="99%" ForeColor="#333333" GridLines="None" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
                                                        <asp:CheckBox ID="cbLMSupp" runat="server" ToolTip='<%# eval("suppoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="Supplier Code" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Strikeout="False" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Supplier" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListSupp" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" Drag="True" PopupDragHandleControlID="lblListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upListBGK" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlListBGK" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListBGK" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Outgoing Giro"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListBGK" runat="server" Width="100px" CssClass="inpText">
                                                <asp:ListItem Value="cashbankno">BGK No.</asp:ListItem>
                                                <asp:ListItem Value="cashbanknote">Note</asp:ListItem>
                                            </asp:DropDownList> <asp:TextBox id="FilterTextListBGK" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListBGK" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListBGK" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListBGK" runat="server" Width="99%" ForeColor="#333333" GridLines="None" PageSize="5" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
                                                            <asp:CheckBox ID="cbLMAP" runat="server" ToolTip='<%# eval("cashbankoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
                                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cashbankno" HeaderText="BGK No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankdate" HeaderText="BGK Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankgroup" HeaderText="Group">
<HeaderStyle CssClass="gvpopup" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbAddToListBGK" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lbCloseListBGK" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListBGK" runat="server" TargetControlID="btnHideListBGK" PopupDragHandleControlID="lblListBGK" PopupControlID="pnlListBGK" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListBGK" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                    <ContentTemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</ContentTemplate>
                </asp:UpdatePanel>
                &nbsp; &nbsp; &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">&nbsp;</td>
        </tr>
    </table>
</asp:Content>
