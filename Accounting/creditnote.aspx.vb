' DEBET NOTE :  -A/P : No Max Amount
' +A/R : Max Amount = A/R Balance
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunction

Partial Class creditnote
#Region "variabel"
    Inherits System.Web.UI.Page
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Dim Report As New ReportDocument
    Dim cRate As New ClassRate()
    Dim cKoneksi As New Koneksi
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function IsInputValid() As Boolean
        Dim sErr As String = ""
        Dim sError As String = ""

        If cust_supp_oid.Text = "" Then
            sError &= "- Please choose a Supplier/Customer !!<BR>"
        End If
        If refoid.Text = "" Then
            sError &= "- Please select invoice field !!<BR>"
        End If
        If ToDouble(amount.Text) <= 0 Then
            sError &= "- CN Amount must more than 0 !!<BR>"
        Else
            If reftype.SelectedValue = "AR" Then
                If ToDouble(amount.Text) > ToDouble(maxpayment.Text) Then
                    sError &= "- Amount must be equal or less than " & ToMaskEdit(ToDouble(maxpayment.Text), 4) & " !!<BR>"
                End If
            End If
        End If

        If TChar(note.Text).Length > 200 Then
            sError &= "- Maximum character for note is 200 !!<br>"
        End If
        If tgl.Text = "" Then
            sError &= "- Please fill Date !!<br>"
        Else
            If CDate(toDate(tgl.Text)) <= CDate(toDate(CutofDate.Text)) Then
                sError &= "- CN Date must equal or more than CutoffDate, CutoffDate Date Is (" & CutofDate.Text & ") !! <BR>"
            End If
            If Not IsValidDate(tgl.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- DATE is INVALID. " & sErr & " !!"
            Else
                If invoicedate.Text <> "" Then
                    If CDate(toDate(tgl.Text)) < CDate(toDate(invoicedate.Text)) Then
                        sError &= "- CN Date must equal or more than Invoice date,Invoice Date Is " & invoicedate.Text & "!!<br>"
                    End If
                End If
            End If
        End If

        If coadebet.Items.Count = 0 Then
            If reftype.SelectedValue = "AR" Then
                sError &= "- Please set  VAR_AR in master interface first!!<br>"
            Else
                sError &= "- Please set  VAR_AP in master interface first!!<br>"
            End If
        End If

        If ToDouble(amount.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "Select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "select top 1 periodacctg from QL_crdgl where glflag='close'"
            If GetPeriodAcctg(CDate(toDate(invoicedate.Text))) = GetStrData(sSql) Then
                sError &= "Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql)
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
            Exit Function
        End If
        Return True
    End Function 'OK

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(tgl1.Text, "dd/MM/yyyy", sErr) Then
            ShowMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(tgl2.Text, "dd/MM/yyyy", sErr) Then
            ShowMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(tgl1.Text)) > CDate(toDate(tgl2.Text)) Then
            ShowMessage("Period 2 must be more than Period 1 !", 2)
            'showMessage("Please select Down Payment A/R data first!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function CheckValidPeriod(ByVal sDate As String) As Boolean
        Dim sErr As String = ""
        'Dim offDate As String = ""
        'sSql = "SELECT TOP 1 ISNULL(genother2,'') FROM QL_mstgen WHERE genstatus='ACTIVE' AND gengroup='CUTOFFDATE' AND genother3='" & CompnyCode & "'"
        'If GetStrData(sSql) = "?" Then
        '    sErr &= "- Please SET CUTT OFF DATE For Outlet (#" & DDLOutlet.SelectedItem.Text & "#)"
        'Else
        '    offDate = Format(CDate(GetStrData(sSql)), "dd/MM/yyyy")
        '    If CDate(toDate(sDate)) < CDate(toDate(offDate)) Then
        '        sErr &= "- TRANSACTION DATE must more or equal than CUT OFF DATE (" & offDate & ")!"
        '    End If
        'End If
        If ToDouble(amount.Text) > 0 Then
            '=========================
            'CEK PERIODE AKTIF BULANAN
            '=========================
            sSql = "SELECT DISTINCT ISNULL(periodacctg,'') FROM QL_crdgl Where glflag <> 'CLOSE'"
            If GetStrData(sSql) <> "" Then
                sSql = "select distinct left(isnull(periodacctg,''),4)+'-'+substring(isnull(periodacctg,''),6,2) FROM QL_crdgl where glflag='OPEN'"
                If GetPeriodAcctg(CDate(toDate(tgl.Text))) < GetStrData(sSql) Then
                    showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
                    Exit Function
                End If
            End If
        End If
        'sSql = "Select Distinct isnull(periodacctg,'') FROM QL_crdgl where glflag='CLOSE'"
        'If GetStrData(sSql) <> "" Then
        '    If GetPeriodAcctg((toDate(invoicedate.Text))) < GetStrData(sSql) Then
        '        showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
        '        Exit Function
        '    End If
        'sSql = "select distinct right(isnull(periodacctg,''),7)+left(isnull(periodacctg,''),0) FROM QL_crdgl where glflag='OPEN'"
        'Dim bb As String = Format(CDate(toDate(sDate)), "yyyy-") & Format(CDate(toDate(sDate)), "MM")
        'Dim cc As String = bb
        'If cc < Integer.Parse(GetStrData(sSql)) Then
        '    showMessage("Periode creditnote bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & " !", 2)
        '    Exit Function
        'End If
        'End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            LblPosting.Text = "IN PROCESS"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedure"
    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\creditnote.aspx?awal=true")
            End If
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Sub generateNo()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & DDLcabang.SelectedValue & "' And gengroup='CABANG'")
        Dim sNo As String = "CN/" & cabang & "/" & Format(CDate(toDate(tgl.Text)), "yy/MM/dd") & "/"
        sSql = "SELECT isnull(max(abs(replace(no,'" & sNo & "',''))),0)+1  FROM ql_creditnote WHERE no LIKE '" & sNo & "%'"
        cnno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub 'OK

    Protected Sub bindDataPurchasing()
        sSql = "SELECT cmpcode, trnbelimsttaxpct, refoid, invno, trndate, trnbelimstnote, payduedate, amttrans, amtpaid, amttrans-amtpaid amtbalance, acctgoid, apaccount, trnbelimststatus, suppoid FROM (SELECT bm.cmpcode, bm.trnbelimstoid refoid, bm.trnbelino invno, bm.trnbelimstnote, con.amttrans, trnbelidate trndate, payduedate, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap WHERE ap.payrefoid<>0 AND ap.cmpcode=con.cmpcode AND ap.refoid=con.refoid), 0.0)) AS amtpaid, con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnbelimststatus, con.reftype, con.payrefoid, bm.suppoid, bm.trnbelimsttaxpct  FROM QL_trnbelimst bm INNER JOIN QL_conap con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnbelimstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.suppoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnbelimst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND ISNULL(dt.payrefoid, 0)=0 AND dt.trnbelimststatus='POST' AND amttrans-amtpaid > 0"
        Session("TblPurchasing") = GetDataTable(sSql, "QL_trnbelimst")
        gvPurchasing.DataSource = Session("TblPurchasing")
        gvPurchasing.DataBind()
    End Sub 'NOT OK

    Protected Sub bindDataSales()
        sSql = "SELECT cmpcode, trntaxpct, refoid, invno, trnjualnote, trndate, payduedate, amttrans, amtpaid, amttrans-amtpaid amtbalance, acctgoid, apaccount, trnjualstatus, trncustoid FROM (SELECT bm.cmpcode, bm.trnjualmstoid refoid, bm.trnjualno invno, bm.trnjualnote, con.amttrans, trnjualdate trndate, payduedate, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap WHERE ap.payrefoid<>0 AND ap.cmpcode=con.cmpcode AND ap.refoid=con.refoid and ap.branch_code=con.branch_code), 0.0)) AS amtpaid, con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnjualstatus, con.reftype, con.payrefoid, bm.trncustoid, bm.trntaxpct FROM QL_trnjualmst bm INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnjualmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code = '" & DDLcabang.SelectedValue & "') AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.trncustoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnjualmst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND ISNULL(dt.payrefoid, 0)=0 AND dt.trnjualstatus='POST' AND amttrans-amtpaid>0"
        Session("TblPurchasing") = GetDataTable(sSql, "QL_trnjualmst")
        gvPurchasing.DataSource = Session("TblPurchasing")
        gvPurchasing.DataBind()
    End Sub 'NOT OK

    Public Sub BindData(ByVal sqlPlus As String)
        Try
            If IsValidPeriod() Then
                sSql = "Select * From (" & _
                " SELECT d.cmpcode, d.branch_code, d.oid ,convert(char(10),d.tgl,103)tgl ,d.no ,d.refoid ,d.reftype ,d.cust_supp_oid, d.coa_credit, d.coa_debet, d.amount, d.note ,d.updtime ,d.upduser, (case when d.reftype = 'AR' then c.custname when d.reftype = 'NT' then c.custname when d.reftype = 'SRV' then c.custname else s.suppname end) name, (case when d.reftype = 'AR' then j.trnjualno when d.reftype = 'NT' then bs.trnbiayaeksno when d.reftype = 'SRV' then srv.invoiceno else b.trnbelino end) invoice, CASE WHEN d.reftype='AP' THEN (SELECT ap.amttrans FROM QL_conap ap WHERE ap.refoid=d.refoid AND ap.cmpcode=d.cmpcode AND ap.amttrans > 0 AND ap.reftype='QL_trnbelimst') WHEN d.reftype = 'AR' THEN (SELECT ar.amttrans FROM QL_conar ar WHERE ar.cmpcode=d.cmpcode AND ar.refoid=d.refoid AND ar.amttrans > 0 AND ar.reftype='QL_trnjualmst') WHEN d.reftype = 'SRV' THEN (SELECT ar.amttrans FROM QL_conar ar WHERE ar.cmpcode=d.cmpcode AND ar.refoid=d.refoid AND ar.amttrans > 0 AND ar.reftype='QL_TRNINVOICE') ELSE (SELECT ar.amttrans FROM QL_conar ar WHERE ar.branch_code=d.branch_code AND ar.refoid=d.refoid AND ar.amttrans > 0 AND ar.reftype='QL_trnbiayaeksmst') END AS invoiceAmt, d.cnstatus FROM QL_CreditNote d Inner Join ql_mstacctg a on a.acctgoid=d.coa_credit Left outer join QL_mstcust c on d.reftype IN ('AR','NT','SRV') and c.custoid=d.cust_supp_oid Left outer join QL_mstsupp s on d.reftype='AP' And s.suppoid=d.cust_supp_oid Left outer join QL_trnbelimst b on d.reftype='AP' and b.trnbelimstoid=d.refoid left outer join QL_trnjualmst j on d.reftype='AR' and j.trnjualmstoid=d.refoid left outer join ql_trnbiayaeksmst bs on d.reftype='NT' and bs.trnbiayaeksoid=d.refoid left outer join QL_TRNINVOICE srv on d.reftype='SRV' and srv.SOID=d.refoid Where d.cmpcode='" & CompnyCode & "' " & sqlPlus & ") dt Where " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ORDER BY tgl DESC, oid DESC"
                Session("TblMst") = GetDataTable(sSql, "QL_trndebetnote")
                GVmstgen.DataSource = Session("TblMst")
                GVmstgen.DataBind()
                lblViewInfo.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.ToString & " <br>" & sSql, 1)
            Exit Sub
        End Try
       
    End Sub 'NOT OK

    Public Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT * FROM (SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.suppoid, s.suppname, CASE WHEN dn.reftype='AP' THEN (SELECT bm.trnbelino FROM QL_trnbelimst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnbelimstoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.refoid=ap2.refoid),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.cnstatus,dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnapdate trndate FROM ql_creditnote dn INNER JOIN QL_mstsupp s ON dn.cust_supp_oid=s.suppoid AND dn.reftype='AP' INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_debet" & _
                    " UNION ALL " & _
                    "SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.custoid suppoid, s.custname suppname, CASE WHEN dn.reftype='AR' THEN (SELECT bm.trnjualno FROM QL_trnjualmst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnjualmstoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.cnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnardate trndate FROM ql_creditnote dn INNER JOIN QL_mstcust s ON dn.cust_supp_oid=s.custoid AND dn.reftype='AR' INNER JOIN QL_conar ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_credit" & _
           " UNION ALL " & _
 "SELECT dn.cmpcode,dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.custoid suppoid, s.custname suppname, CASE WHEN dn.reftype='NT' THEN (SELECT bm.trnbiayaeksno FROM ql_trnbiayaeksmst bm WHERE bm.cmpcode=dn.cmpcode AND bm.trnbiayaeksoid=dn.refoid) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.cnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnardate trndate FROM ql_creditnote dn INNER JOIN QL_mstcust s ON dn.cust_supp_oid=s.custoid AND dn.reftype='NT' INNER JOIN QL_conar ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 and trnartype = 'EXP' INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_credit " & _
 " UNION ALL " & _
 " SELECT dn.cmpcode, dn.branch_code, dn.oid, dn.no, dn.refoid, dn.reftype, dn.tgl, s.custoid suppoid, s.custname suppname, CASE WHEN dn.reftype='SRV' THEN (SELECT bm.invoiceno FROM QL_TRNINVOICE bm WHERE bm.cmpcode=dn.cmpcode AND bm.SOID=dn.refoid AND bm.BRANCH_CODE=dn.branch_code) END AS invoice, dn.amount, ap.amttrans, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)) AS amtbayar, dn.coa_debet, dn.coa_credit, dn.note, dn.cnstatus, dn.updtime, dn.upduser, a.acctgcode, a.acctgdesc, ap.trnardate trndate FROM ql_creditnote dn INNER JOIN QL_mstcust s ON dn.cust_supp_oid=s.custoid AND dn.reftype='SRV' INNER JOIN QL_conar ap ON ap.cmpcode=dn.cmpcode AND ap.refoid=dn.refoid AND ap.payrefoid=0 and trnartype = 'PIUTANGINV' INNER JOIN QL_mstacctg a ON a.acctgoid=dn.coa_credit) AS dt " & _
            "WHERE dt.cmpcode='" & CompnyCode & "' AND dt.oid=" & sOid
            Dim dtData As DataTable = GetDataTable(sSql, "QL_trndebetnote")
            If dtData.Rows.Count < 1 Then
                showMessage("Failed to load transaction data.", 2)
            Else
                DDLcabang.SelectedValue = dtData.Rows(0)("branch_code").ToString
                cabang.Text = dtData.Rows(0)("branch_code").ToString
                CompnyCode = dtData.Rows(0)("cmpcode").ToString
                cnno.Text = dtData.Rows(0)("no").ToString
                cnoid.Text = dtData.Rows(0)("oid").ToString
                tgl.Text = Format(dtData.Rows(0)("tgl"), "dd/MM/yyyy")
                cnno.Text = dtData.Rows(0)("no").ToString
                refoid.Text = dtData.Rows(0)("refoid").ToString
                reftype.SelectedValue = dtData.Rows(0)("reftype").ToString
                nama.Text = dtData.Rows(0)("suppname").ToString
                cust_supp_oid.Text = dtData.Rows(0)("suppoid").ToString
                'coacredit.SelectedValue = dtData.Rows(0)("coa_credit").ToString
                amount.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amount").ToString), 4)
                note.Text = dtData.Rows(0)("note").ToString
                invno.Text = dtData.Rows(0)("invoice").ToString
                create.Text = "Create By " & dtData.Rows(0)("upduser").ToString & " On " & dtData.Rows(0)("updtime").ToString
                update.Text = "; Update By " & dtData.Rows(0)("upduser").ToString & " On " & dtData.Rows(0)("updtime").ToString
                LblPosting.Text = dtData.Rows(0)("cnstatus").ToString
                btnDelete.Enabled = True
                maxpayment.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amttrans").ToString) - ToDouble(dtData.Rows(0)("amtbayar").ToString), 4)
                sSql = "SELECT acctgoid,+'('+acctgcode+') '+ acctgdesc FROM QL_mstacctg Where acctgoid = " & dtData.Rows(0)("coa_credit") & ""
                FillDDL(coacredit, sSql)
                coacredit.SelectedValue = dtData.Rows(0)("coa_credit").ToString
                InitCOADebet()
                coadebet.SelectedValue = dtData.Rows(0)("coa_debet").ToString
                invoicedate.Text = Format(CDate(dtData.Rows(0)("trndate").ToString), "dd/MM/yyyy")
            End If
            If LblPosting.Text.ToLower = "approved" Or LblPosting.Text.ToLower = "in approval" Then
                btnsave.Visible = False : btnpost.Visible = False
                btnDelete.Visible = False : draft.Visible = False
                cnno.Visible = True : btnshowCOA.Visible = True
            Else
                btnsave.Visible = True
                btnpost.Visible = True
                If Session("oid") <> Nothing And Session("oid") <> "" Then
                    btnDelete.Visible = True
                    If LblPosting.Text = "POST" Then
                        btnshowCOA.Visible = True
                    End If
                Else
                    btnDelete.Visible = False
                End If
                draft.Visible = True
                cnno.Visible = False
            End If
            DDLOutlet.Enabled = False : DDLOutlet.CssClass = "inpTextDisabled"
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

    Public Sub GenerateGenID()
        cnoid.Text = GenerateID("ql_creditnote", CompnyCode)
    End Sub 'OK

    Private Sub BindDataSupplier()
        sSql = "SELECT DISTINCT s.suppoid oid, s.suppcode code, s.suppname name FROM QL_mstsupp s INNER JOIN QL_trnbelimst bm ON bm.trnsuppoid=s.suppoid WHERE /*amtbelinetto - accumpayment > 0 AND*/ (s.suppcode LIKE '%" & Tchar(nama.Text) & "%' OR s.suppname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code = '" & DDLcabang.SelectedValue & "' ORDER BY s.suppname"
        Session("TblSupplier") = GetDataTable(sSql, "QL_mstsupp")
        gvSupplier.DataSource = Session("TblSupplier")
        gvSupplier.DataBind()
    End Sub 'OK

    Private Sub BinDataCustomer()
        Try
            If reftype.SelectedValue = "AR" Then
                sSql = "SELECT DISTINCT s.custoid oid, s.custcode code, s.custname name FROM QL_mstcust s INNER JOIN QL_trnjualmst bm ON bm.trncustoid=s.custoid WHERE trnamtjualnetto - accumpayment > 0  AND (s.custcode LIKE '%" & Tchar(nama.Text) & "%' OR s.custname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code='" & DDLcabang.SelectedValue & "' ORDER BY s.custname"
            ElseIf reftype.SelectedValue = "NT" Then
                sSql = "SELECT DISTINCT oid, code, name FROM ( SELECT s.custoid oid, s.custcode code, s.custname name, bm.amtekspedisi,ISNULL((select SUM(amtbayar) from QL_conar c where c.refoid = bm.trnbiayaeksoid and c.trnartype = 'PAYAREXP'),0) AS amtbayar FROM QL_mstcust s INNER JOIN ql_trnbiayaeksmst bm ON bm.custoid=s.custoid where (s.custcode LIKE '%" & Tchar(nama.Text) & "%' OR s.custname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code='" & DDLcabang.SelectedValue & "') cust where amtekspedisi - amtbayar > 0 ORDER BY cust.name"
            ElseIf reftype.SelectedValue = "SRV" Then
                sSql = "SELECT DISTINCT s.custoid oid, s.custcode code, s.custname name FROM QL_mstcust s INNER JOIN QL_TRNINVOICE bm ON bm.custoid=s.custoid WHERE amtnetto - ISNULL((Select SUM(amtbayaridr) From QL_conar con Where trnartype IN ('PAYARSVC','CNSRV','DNSRV') AND con.custoid=bm.custoid AND bm.BRANCH_CODE=con.branch_code AND SOID=con.refoid),0.00) > 0 AND (s.custcode LIKE '%" & Tchar(nama.Text) & "%' OR s.custname LIKE '%" & Tchar(nama.Text) & "%') AND bm.cmpcode='" & CompnyCode & "' and bm.branch_code='" & DDLcabang.SelectedValue & "' ORDER BY s.custname"
            End If
            Session("TblSupplier") = GetDataTable(sSql, "QL_mstsupp")
            gvSupplier.DataSource = Session("TblSupplier")
            gvSupplier.DataBind()
        Catch ex As Exception
            showMessage(ex.ToString & " <br>" & sSql, 1)
            Exit Sub
        End Try
      
    End Sub 'OK

    Private Sub InitCOACredit(ByVal iAcctgOid As Integer, ByVal sAcctgCodeDesc As String)
        coacredit.Items.Clear()
        coacredit.Items.Add(New ListItem(sAcctgCodeDesc, iAcctgOid))
    End Sub

    Private Sub InitCOADebet()
        coadebet.Items.Clear()
        'Dim sVarAP() As String = {"VAR_CREDIT_NOTE_AP"}
        'Dim sVarAR() As String = {"VAR_CREDIT_NOTE_AR"}
        If reftype.SelectedValue.ToUpper = "AP" Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_CREDITNOTE_HUTANG_DEBIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coadebet, "VAR_CREDITNOTE_HUTANG_DEBIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_AP in master interface first!", 2)
                Exit Sub
            End If
        ElseIf reftype.SelectedValue.ToUpper = "AR" Then
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_CREDITNOTE_PIUTANG_DEBIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coadebet, "VAR_CREDITNOTE_PIUTANG_DEBIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_AR in master interface first!", 2)
                Exit Sub
            End If
        Else
            sSql = "SELECT COUNT(*) FROM QL_mstinterface WHERE interfacevar='VAR_CREDITNOTE_NT_DEBIT'"
            If CheckDataExists(sSql) Then
                FillDDLAcctg(coadebet, "VAR_CREDITNOTE_NT_DEBIT", DDLcabang.SelectedValue)
            Else
                showMessage("Please set VAR_NT in master interface first!", 2)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub ResetAPARData()
        lblAPARNo.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Type", "A/R Type")
        lblsupp_cust.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "Supplier", "Customer")
        refoid.Text = 0 : invno.Text = "" : aparrate2oid.Text = ""
        curroid.SelectedIndex = 0 : aparrateoid.Text = ""
        cust_supp_oid.Text = "" : nama.Text = ""
        amount.Text = "0" : invoicedate.Text = ""
        cProc.DisposeGridView(gvPurchasing) : gvPurchasing.Visible = False
        cProc.DisposeGridView(gvSupplier) : gvSupplier.Visible = False
        coadebet.SelectedIndex = IIf(coadebet.Items.Count > 0, 0, -1)
        coacredit.SelectedIndex = IIf(coacredit.Items.Count > 0, 0, -1)
        maxpayment.Text = "0.00"
        maxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
        lblmaxpayment.Visible = (reftype.SelectedValue.ToUpper = "AP")
        invno.CssClass = "inpText" : invno.Enabled = True
        btnpost.Visible = True : btnshowCOA.Visible = False
    End Sub

    Private Sub PrintReport(ByVal sType As String)
        Dim sWhere As String = "" : Dim periode As String = ""
        Try
            If sType = "PDF" Then
                Report.Load(Server.MapPath(folderReport & "rptCust.rpt"))
            Else
                Report.Load(Server.MapPath(folderReport & "rptCreditNote.rpt"))
            End If
            If tgl1.Text > tgl2.Text Then
                showMessage("Tgl Awal tidak boleh lebih dari Tgl Akhir", CompnyName) : Exit Sub
            End If
            If tgl1.Text < tgl2.Text Then
                sWhere &= " AND cn.tgl >= '" & toDate(tgl1.Text) & " 0:0:0' and cn.tgl <= '" & toDate(tgl2.Text) & " 0:0:0'"
            End If
            If tipene.SelectedValue <> "ALL" Then
                sWhere &= " AND cn.reftype = '" & tipene.SelectedValue & "'"
            End If
            If DDLStatus.SelectedValue <> "ALL" Then
                sWhere &= " AND cnstatus = '" & DDLStatus.SelectedValue & "'"
            End If
            If DDLfiltercabang.SelectedValue.ToUpper <> "ALL" Then
                sWhere &= "And cn.branch_code='" & DDLfiltercabang.SelectedValue & "'"
            End If
            sWhere = " WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' " & sWhere & ""
            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, Report)
            Report.SetParameterValue("sWhere", sWhere)
            Report.SetParameterValue("periode", periode)
            'cProc.SetDBLogonForReport(rptReport)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                Report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "Credit_Note_" & Format(GetServerTime(), "dd_MM_yy"))
                Report.Close() : Report.Dispose()
            End If
            Report.Close() : Report.Dispose()
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("~\Accounting\creditnote.aspx")
        End If

        Me.btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to delete this data ?');")
        btnpost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure you want to send approval this data ?');")
        Page.Title = "Credit Note"
        Session("oid") = Request.QueryString("oid")
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update Data"
        Else
            i_u.Text = "New Data"
        End If
        If Not IsPostBack Then
            DDLOutlet_SelectedIndexChanged(Nothing, Nothing)
            tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
            tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitCOADebet() : BindData("")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                tgl.Text = Format(GetServerTime(), "dd/MM/yyyy")
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime.ToString & "</B>; "
                update.Text = "Update By - On -"
            End If
        End If

    End Sub 'OK

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        GVmstgen.DataSource = Session("TblMst")
        GVmstgen.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(4).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(NewMaskEdit(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub btnlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnlist.Click
        tipene.SelectedIndex = -1
        FilterDDL.SelectedIndex = -1
        DDLStatus.SelectedIndex = -1
        DDLfiltercabang.SelectedIndex = DDLcabang.Items.Count - 0
        FilterText.Text = ""
        tgl1.Text = Format(Now, "01/MM/yyyy")
        tgl2.Text = Format(Now, "dd/MM/yyyy")
        GVmstgen.PageIndex = 0
        BindData("")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GVmstgen.PageIndex = 0
        Dim sqlPlus As String = ""
        If DDLfiltercabang.SelectedValue <> "ALL" Then
            sqlPlus &= " AND d.branch_code = '" & DDLfiltercabang.SelectedValue & "'"
        End If
        If tipene.SelectedValue <> "ALL" Then
            sqlPlus &= " AND reftype='" & tipene.SelectedValue & "'"
        End If
        If DDLStatus.SelectedValue <> "ALL" Then
            sqlPlus &= "  AND cnstatus='" & DDLStatus.SelectedValue & "'"
        End If
        sqlPlus &= " And tgl between '" & toDate(tgl1.Text) & "' AND '" & toDate(tgl2.Text) & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        BindData(sqlPlus)
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        If IsInputValid() Then
            If CheckValidPeriod(tgl.Text) Then
                Dim cRate As New ClassRate()
                Dim iGldtloid As Int32 = ClassFunction.GenerateID("QL_TRNGLDTL", CompnyCode)
                Dim iGlmstoid As Int32 = ClassFunction.GenerateID("QL_TRNGLMST", CompnyCode)
                Dim iDtlseq As Int16 = 1
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    cnoid.Text = GenerateID("QL_CREDITNOTE", CompnyCode)
                End If

                sSql = "SELECT COUNT(*) FROM QL_creditnote WHERE cmpcode='" & CompnyCode & "' AND refoid=" & refoid.Text & " AND reftype='" & reftype.SelectedValue & "' AND cnstatus='IN PROCESS' " & IIf(Session("oid") = "", "", " AND oid<>" & cnoid.Text & "") & ""
                If CheckDataExists(sSql) Then
                    showMessage("- Ada transaksi CN lain untuk nota ini yang belum ter Posting, silakan ulangi kembali", 2)
                    Exit Sub
                End If
                
                If LblPosting.Text = "POST" Then
                    '    generateNo()
                    '    cRate.SetRateValue(CInt(curroid.SelectedValue), toDate(invoicedate.Text))
                    '    If cRate.GetRateDailyLastError <> "" Then
                    '        showMessage(cRate.GetRateDailyLastError, 2)
                    '        Exit Sub
                    '    End If
                    '    If cRate.GetRateMonthlyLastError <> "" Then
                    '        showMessage(cRate.GetRateMonthlyLastError, 2)
                    '        Exit Sub
                    '    End If
                    'Else
                    cnno.Text = cnoid.Text
                End If

                Dim idConap As Integer = GenerateID("QL_CONAP", CompnyCode)
                Dim idConaR As Integer = GenerateID("QL_CONAR", CompnyCode)

                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans

                Dim objTable As DataTable = Session("TblDtl")
                Try
                    If Session("oid") = Nothing Or Session("oid") = "" Then
                      
                        sSql = "INSERT INTO QL_creditnote (cmpcode, branch_code, oid, tgl, no, reftype, refoid, cust_supp_oid, coa_debet, coa_credit, amount, amountidr, amountusd, note, cnstatus, upduser, updtime, curroid, rate2oid) VALUES " & _
                        "('" & CompnyCode & "', '" & DDLcabang.SelectedValue & "', " & cnoid.Text & ", '" & CDate(toDate(tgl.Text)) & "', '" & cnno.Text & "', '" & reftype.SelectedValue & "', " & refoid.Text & ", " & cust_supp_oid.Text & ", " & coadebet.SelectedValue & ", " & coacredit.SelectedValue & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", " & ToDouble(amount.Text) & ", '" & Tchar(note.Text) & "', '" & LblPosting.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & curroid.SelectedValue & "', " & aparrate2oid.Text & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid=" & cnoid.Text & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_creditnote'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Else
                        sSql = "UPDATE QL_creditnote SET no='" & cnno.Text & "', tgl='" & CDate(toDate(tgl.Text)) & "', reftype='" & reftype.SelectedValue & "', refoid=" & refoid.Text & ", cust_supp_oid=" & cust_supp_oid.Text & ", coa_debet=" & coadebet.SelectedValue & ", coa_credit=" & coacredit.SelectedValue & ", amount=" & ToDouble(amount.Text) & ", amountidr=" & ToDouble(amount.Text) & ", amountusd=" & ToDouble(amount.Text) & ", note='" & Tchar(note.Text) & "' ,cnstatus='" & LblPosting.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND oid=" & cnoid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    End If

                    If LblPosting.Text = "In Approval" Then
                        sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='ql_creditnote' Order By approvallevel"
                        Dim dtData2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_approvalstructure")
                        If dtData2.Rows.Count > 0 Then
                            Session("TblApproval") = dtData2
                        Else
                            showMessage("Tidak Approval User untuk Cabang " & DDLcabang.SelectedItem.Text & ", Silahkan contact admin dahulu", 2)
                            Exit Sub
                        End If

                        Session("AppOid") = ClassFunction.GenerateID("QL_Approval", CompnyCode)
                        If LblPosting.Text = "In Approval" Then
                            If Not Session("TblApproval") Is Nothing Then
                                objTable = Session("TblApproval")
                                For c1 As Int16 = 0 To objTable.Rows.Count - 1
                                    sSql = "INSERT INTO QL_APPROVAL(cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) VALUES " & _
                                    "('" & CompnyCode & "', " & Session("AppOid") + c1 & ", 'CN" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'ql_creditnote', '" & cnoid.Text & "', 'In Approval', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "', '" & DDLcabang.SelectedValue & "')"
                                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                Next
                                sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If
                        End If
                    End If

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.ToString, 2)
                    LblPosting.Text = "IN PROCESS"
                    Exit Sub
                End Try

                If LblPosting.Text = "POST" Then
                    Session("SavedInfo") = "Data telah berhasil di posting dengan nomor. " & cnno.Text & " !"
                Else
                    Response.Redirect("creditnote.aspx?awal=true")
                End If
                If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                    showMessage(Session("SavedInfo"), 3)
                Else
                    Response.Redirect("~\Accounting\creditnote.aspx?awal=true")
                End If
            End If
        End If
    End Sub

    Protected Sub btnpost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        sSql = "select * from QL_mstgen where gengroup='TABLENAME' and gencode='ql_creditnote' and cmpcode IN ('" & CompnyCode & "')"
        Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_approval")
        Session("AppPerson") = dt
        LblPosting.Text = "In Approval"
        btnsave_Click(sender, e)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("creditnote.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            'If reftype.SelectedValue = "AR" Then
            '    sSql = "DELETE FROM QL_conar WHERE cmpcode='" & CompnyCode & "' AND reftype='ql_creditnote' AND payrefoid=" & cnoid.Text & " and branch_code='" & DDLcabang.SelectedValue & "'"
            'Else
            '    sSql = "DELETE FROM QL_conap WHERE cmpcode='" & CompnyCode & "' AND reftype='ql_creditnote' AND payrefoid=" & cnoid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
            'End If
            'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Delete ql_creditnote WHERE cmpcode='" & CompnyCode & "' AND oid=" & cnoid.Text & " and branch_code = '" & DDLcabang.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("~\accounting\creditnote.aspx?awal=true")
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch_SC.Click
        If reftype.SelectedValue = "AP" Then
            BindDataSupplier()
        Else
            BinDataCustomer()
        End If
        gvSupplier.Visible = True
    End Sub

    Protected Sub btnSearchPurchasing_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If cust_supp_oid.Text = "" Then
            showMessage("Please choose Supplier/Customer !!", 2)
            Exit Sub
        End If
        If reftype.SelectedValue = "AP" Then
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnbelinote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, trnbelistatus, trnsuppoid FROM ( SELECT bm.cmpcode,bm.currencyoid, bm.trnbelimstoid, refoid, bm.trnbelino invno, bm.trnbelinote, con.amttrans, trnbelidate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conap ar2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnaptype IN ('AP','PAYAP')),0.0)) AS amtpaid ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conap ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnaptype IN ('DNAP','CNAP')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnbelistatus, con.reftype, con.payrefoid, bm.trnsuppoid, bm.trntaxpct FROM QL_trnbelimst bm INNER JOIN QL_conap con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnbelimstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "'  AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.trnsuppoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnbelimst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.trnbelistatus='POST'"
        ElseIf reftype.SelectedValue = "AR" Then 
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnjualnote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, trnjualstatus, trncustoid FROM ( SELECT bm.cmpcode,bm.currencyoid, bm.trnjualmstoid, refoid, bm.trnjualno invno, bm.trnjualnote, con.amttrans, trnjualdate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnartype IN ('AR','PAYAR')),0.0)) AS amtpaid ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnartype IN ('DNAR','CNAR')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.trnjualstatus, con.reftype, con.payrefoid, bm.trncustoid, bm.trntaxpct FROM QL_trnjualmst bm  INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnjualmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "' AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.trncustoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trnjualmst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.trnjualstatus='POST' AND amttrans > amtpaid + amtdncn"
        ElseIf reftype.SelectedValue = "SRV" Then
            sSql = "SELECT cmpcode, currencyoid, trntaxpct, refoid, invno, '' trnbiayaeksnote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, SSTATUS, custoid FROM ( SELECT bm.cmpcode,1 AS currencyoid, bm.SOID trnbiayaeksoid, refoid, bm.invoiceno invno, '' trnbiayaeksnote, con.amttrans, SSTARTTIME trndate, payduedate, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnartype IN ('PIUTANGINV','PAYARSVC')),0.0)) AS amtpaid, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ar2.trnartype IN ('DNSRV','CNSRV')), 0.0)) AS amtdncn, con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount, bm.SSTATUS, con.reftype, con.payrefoid, bm.custoid, 0.00 AS trntaxpct FROM QL_TRNINVOICE bm INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.SOID INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "' AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.custoid=" & cust_supp_oid.Text & " AND dt.reftype='QL_trninvoice' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.SSTATUS='POST' AND amttrans > amtpaid + amtdncn"
        Else
            sSql = "SELECT cmpcode,currencyoid, trntaxpct, refoid, invno, trnbiayaeksnote, trndate, payduedate, amttrans, amtpaid + amtdncn AS amtpaid, amtdncn, amttrans-(amtpaid+amtdncn) AS amtbalance, acctgoid, apaccount, status, custoid FROM ( SELECT bm.cmpcode, 1 AS currencyoid, bm.trnbiayaeksoid, refoid, bm.trnbiayaeksno invno, bm.trnbiayaeksnote, con.amttrans, trnbiayaeksdate trndate, payduedate ,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.paymentoid=ar2.payrefoid  WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid AND ISNULL(pay.payres1,'')<>'Lebih Bayar' AND ar2.trnartype IN ('EXP','PAYAREXP')),0.0)) AS amtpaid, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND con.cmpcode=ar2.cmpcode AND con.refoid=ar2.refoid  AND ar2.trnartype IN ('DNNT','CNNT')),0.0)) AS amtdncn , con.acctgoid AS acctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,bm.status, con.reftype, con.payrefoid, bm.custoid, 0.00 AS trntaxpct FROM ql_trnbiayaeksmst bm INNER JOIN QL_conar con ON con.cmpcode=bm.cmpcode AND con.refoid=bm.trnbiayaeksoid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid where bm.branch_code='" & DDLcabang.SelectedValue & "' AND ISNULL(con.payrefoid, 0)=0 ) AS dt WHERE dt.cmpcode='" & CompnyCode & "' AND dt.custoid=" & cust_supp_oid.Text & " AND dt.reftype='ql_trnbiayaeksmst' AND dt.invno LIKE '%" & Tchar(invno.Text) & "%' AND dt.status='POST' AND amttrans > amtpaid + amtdncn"

        End If
        Dim dtAPAR As DataTable = cKoneksi.ambiltabel(sSql, "apar")
        Session("apar") = dtAPAR
        gvPurchasing.DataSource = dtAPAR
        gvPurchasing.DataBind()
        gvPurchasing.Visible = True
    End Sub

    Protected Sub gvPurchasing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPurchasing.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub gvPurchasing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchasing.SelectedIndexChanged
        Dim dtAPAR As DataTable = Session("apar")
        Dim dvSelectAPAR As DataView = dtAPAR.DefaultView
        Try
            refoid.Text = gvPurchasing.SelectedDataKey.Item("refoid").ToString
            invno.Text = gvPurchasing.SelectedDataKey.Item("invno").ToString
            invoicedate.Text = Format(CDate(gvPurchasing.SelectedDataKey.Item("trndate").ToString), "dd/MM/yyyy")
            InitCOACredit(gvPurchasing.SelectedDataKey.Item("acctgoid"), gvPurchasing.SelectedDataKey.Item("apaccount").ToString)
            lblmaxpayment.Visible = (reftype.SelectedValue.ToUpper = "AR" Or reftype.SelectedValue.ToUpper = "NT" Or reftype.SelectedValue.ToUpper = "SRV")
            maxpayment.Visible = (reftype.SelectedValue.ToUpper = "AR" Or reftype.SelectedValue.ToUpper = "NT" Or reftype.SelectedValue.ToUpper = "SRV")
            curroid.SelectedValue = dvSelectAPAR(0)("currencyoid").ToString
            cRate.SetRateValue(CInt(curroid.SelectedValue), gvPurchasing.SelectedDataKey.Item("trndate").ToString)
            aparrateoid.Text = cRate.GetRateDailyOid
            aparrate2oid.Text = cRate.GetRateMonthlyOid
            If maxpayment.Visible Then
                maxpayment.Text = ToMaskEdit(gvPurchasing.SelectedDataKey("amtbalance").ToString, 4)
            Else
                maxpayment.Text = "0.00"
            End If
            gvPurchasing.Visible = False
            invno.CssClass = "inpTextDisabled" : invno.Enabled = False
        Catch ex As Exception
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvPurchasing_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPurchasing.PageIndex = e.NewPageIndex
        gvPurchasing.DataSource = Session("apar")
        gvPurchasing.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.DataSource = Session("TblSupplier")
        gvSupplier.DataBind()
        gvSupplier.Visible = True
    End Sub

    Protected Sub gvsupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        cust_supp_oid.Text = gvSupplier.SelectedDataKey("oid").ToString()
        nama.Text = gvSupplier.SelectedDataKey("name").ToString()
        refoid.Text = 0 : invno.Text = ""
        gvSupplier.Visible = False
        btnSearchPurchasing_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showTableCOA(cnno.Text, cabang.Text, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reftype.SelectedIndexChanged
        ResetAPARData()
        InitCOADebet()
    End Sub

    Protected Sub amount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        amount.Text = ToMaskEdit(ToDouble(amount.Text), 4)
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub btnclearSC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cust_supp_oid.Text = "" : nama.Text = "" : invno.Text = ""
        gvSupplier.Visible = False
    End Sub

    Protected Sub btnClearinv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ResetAPARData()
    End Sub

    Protected Sub DDLOutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateGenID()
        draft.Text = cnoid.Text
        reftype_SelectedIndexChanged(Nothing, Nothing)
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        sSql = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang' order by gencode asc"
        FillDDL(DDLcabang, sSql)
        If Session("branch_id") <> "10" Then
            DDLcabang.SelectedValue = Session("branch_id")
            DDLcabang.Enabled = False
            DDLcabang.CssClass = "inpTextDisabled"
        End If
        Dim sSql1 As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLfiltercabang, sSql1)
        DDLfiltercabang.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "10" Then
            DDLfiltercabang.SelectedValue = Session("branch_id")
            DDLfiltercabang.Enabled = False
            DDLfiltercabang.CssClass = "inpTextDisabled"
        Else
            DDLfiltercabang.SelectedIndex = DDLcabang.Items.Count - 0
        End If
    End Sub

    Protected Sub tgl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'generateNo()
        Try
            Dim dat1 As Date = CDate(toDate(tgl.Text))
            'Dim dat2 As Date = CDate(FilterPeriod2.Text)
            If dat1 < CDate("01/01/1900") Then
                showMessage("Salah Format tanggal...!!", 2) : Exit Sub
            End If
        Catch ex As Exception
            showMessage("Salah Format tanggal...!!", 2) : Exit Sub
        End Try
    End Sub

    Protected Sub GVmstgen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("creditnote.aspx?cmpcode=" & GVmstgen.SelectedDataKey.Item("cmpcode").ToString & "&oid=" & GVmstgen.SelectedDataKey.Item("oid").ToString & "")
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub DDLcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ResetAPARData()
        InitCOADebet()
    End Sub

    Protected Sub btnPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintExcel.Click
        PrintReport("EXCEL")
    End Sub

#End Region

End Class