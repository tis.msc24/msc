'Create by Widi on 18 oktober 2013
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.Data
Imports ClassFunction
Partial Class ReportForm_frmRealisasi_Pembelian
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim CProc As New ClassProcedure
    Dim sSql As String
#End Region

#Region "Procedure"
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Sub initAllDDL()
        initGroup()
        initSubGroup()
    End Sub

    Public Sub initGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLGrup, sSql)
        FilterDDLGrup.Items.Add(New ListItem("ALL GROUP", "ALL GROUP"))
        FilterDDLGrup.SelectedValue = "ALL GROUP"
    End Sub

    Public Sub initSubGroup()
        sSql = "select genoid, gendesc from ql_mstgen where gengroup='ITEMSUBGROUP'  ORDER BY gendesc"
        FillDDL(FilterDDLSubGrup, sSql)
        FilterDDLSubGrup.Items.Add(New ListItem("ALL SUB GROUP", "ALL SUB GROUP"))
        FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP"
    End Sub

    Private Sub PICddl(ByVal OidItemCok As String)
        '    Dim OidMat As String = ""
        '    If matoid.Text <> "" Then
        '        OidMat &= " WHERE pd.itemoid =" & OidItemCok & ""
        '    End If
        '    sSql = "SELECT DISTINCT pic.PERSONOID,pic.PERSONNIP,pic.PERSONNAME FROM QL_MSTPERSON pic INNER JOIN QL_MSTITEM i ON pic.PERSONOID=i.personoid AND pic.personoid <> 0 WHERE i.itemoid IN (SELECT DISTINCT pd.itemoid FROM QL_podtl pd INNER JOIN QL_pomst pm ON pm.trnbelimstoid=pd.trnbelimstoid AND pm.branch_code=pd.branch_code AND pd.itemoid=i.itemoid INNER JOIN ql_trnsjbelidtl sjd ON sjd.trnbelidtloid=pd.trnbelidtloid AND sjd.branch_code=pm.branch_code AND sjd.refoid=i.itemoid INNER JOIN ql_trnsjbelimst sjm ON sjm.trnsjbelioid=sjd.trnsjbelioid AND sjd.branch_code=sjm.branch_code INNER JOIN QL_trnbelidtl bd ON bd.itemoid=i.itemoid AND sjm.trnsjbelino=bd.trnsjbelino AND sjm.branch_code='" & Session("branch_id") & "' LEFT JOIN QL_trnbeliReturmst rm ON rm.trnsjbelino=sjm.trnsjbelino AND sjm.branch_code=rm.branch_code LEFT JOIN QL_trnbeliReturdtl rd ON rd.itemoid=i.itemoid AND bd.trnbelidtloid=rd.trnbelidtloid AND rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.branch_code='" & Session("branch_id") & "' " & OidMat & ")"
        '    FillDDL(ddLPic, sSql)
    End Sub

    Sub bindDataSupplier()
        Dim sPic As String = ""
        If Session("branch_id") <> "10" Then
            sPic &= "AND po.upduser='" & Session("UserID") & "'"
        Else
            If Session("UserLevel") = 4 Or Session("UserLevel") = 2 Then
                sPic = "AND po.upduser='" & Session("UserID") & "'"
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                sPic &= ""
            End If
        End If

        sSql = "select suppoid,suppcode,suppname from ql_mstsupp where cmpcode='" & CompnyCode & "' and (suppcode like '%" & Tchar(suppName.Text) & "%' or suppname like '%" & Tchar(suppName.Text) & "%') AND suppoid IN (SELECT po.trnsuppoid FROM QL_pomst po WHERE po.trnsuppoid=suppoid /*AND po.branch_code='" & Session("branch_id") & "'*/ " & sPic & ")"
        FillGV(gvSupplier, sSql, "ql_mstsupp")
    End Sub

    Sub bindDataMaterial()
        Dim OidSup As String = "" : Dim sPic As String = ""

        If suppoid.Text <> "" Then
            OidSup &= " AND pm.trnsuppoid LIKE '%" & suppoid.Text & "%'"
        End If

        If Session("branch_id") <> "10" Then
            sPic &= "AND pm.upduser='" & Session("UserID") & "'"
        Else
            If Session("UserLevel") = 4 Or Session("UserLevel") = 2 Then
                sPic = "AND pm.upduser='" & Session("UserID") & "'"
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                sPic &= ""
            End If
        End If
        'If Session("UserID") = "alchu" Or Session("UserID") = "POPY" Or Session("UserID") = "admin" Then
        '    sPic &= ""
        'Else
        '    sPic &= "AND pm.upduser='" & Session("UserID") & "'"
        'End If
        sSql = "SELECT i.itemcode,i.itemoid,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,g.gendesc satuan1,g.gendesc AS satuan2,g.gendesc AS satuan3,i.konversi1_2,i.konversi2_3,i.Merk FROM QL_mstitem i INNER JOIN QL_mstgen g ON g.genoid=i.satuan1 AND g.gengroup IN ('ITEMUNIT') WHERE i.itemoid IN ( SELECT DISTINCT pd.itemoid FROM QL_podtl pd INNER JOIN QL_pomst pm ON pm.trnbelimstoid=pd.trnbelimstoid AND pm.branch_code=pd.branch_code AND pd.itemoid=i.itemoid INNER JOIN ql_trnsjbelidtl sjd ON sjd.trnbelidtloid=pd.trnbelidtloid /*AND sjd.frombranch=pm.branch_code*/ AND sjd.refoid=i.itemoid INNER JOIN ql_trnsjbelimst sjm ON sjm.trnsjbelioid=sjd.trnsjbelioid /*AND sjd.frombranch=sjm.branch_code*/ INNER JOIN QL_trnbelidtl bd ON bd.itemoid=i.itemoid AND sjm.trnsjbelino=bd.trnsjbelino /*AND sjm.frombranch=pm.branch_code*/LEFT JOIN QL_trnbeliReturmst rm ON rm.trnsjbelino=sjm.trnsjbelino /*AND sjm.frombranch=rm.branch_code*/ LEFT JOIN QL_trnbeliReturdtl rd ON rd.itemoid=i.itemoid AND bd.trnbelidtloid=rd.trnbelidtloid AND rm.trnbeliReturmstoid=rd.trnbeliReturmstoid AND rm.branch_code='" & Session("branch_id") & "' WHERE pm.cmpcode='" & CompnyCode & "'" & OidSup & " " & sPic & ") AND (itemdesc like '%" & Tchar(material.Text) & "%' or i.itemcode like '%" & Tchar(material.Text) & "%' or i.merk like '%" & Tchar(material.Text) & "%')"
        FillGV(gvItem, sSql, "ql_mstmat")
    End Sub

    Sub bindDataPO()
        Dim sPic As String = ""
        If Session("branch_id") <> "10" Then
            sPic &= "AND pom.upduser='" & Session("UserID") & "'"
        Else
            If Session("UserLevel") = 4 Or Session("UserLevel") = 2 Then
                sPic = "AND pom.upduser='" & Session("UserID") & "'"
            ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
                sPic &= ""
            End If
        End If

        sSql = "SELECT pom.trnbelimstoid, pom.trnbelipono pono, sup.suppoid, sup.suppname,pom.trnbelipodate podate FROM ql_pomst pom inner join ql_mstsupp sup on sup.suppoid=pom.trnsuppoid Where pom.trnbelitype = 'GROSIR' " & sPic & " And pom.trnbelistatus in ('Approved') and trnbelipono LIKE '%" & Tchar(pono.Text.Trim) & "%' AND sup.suppcode LIKE '%" & Tchar(suppName.Text) & "%' " & sPic & ""
        FillGV(gvPO, sSql, "ql_pomst")
        gvPO.Visible = True
    End Sub

    Sub bindDataLPB()
        sSql = "select sjmst.trnsjbelioid ,sjmst.trnsjbelino,sjmst.trnsjbelidate,RIGHT(trnsjbelino,4) from ql_trnsjbelimst sjmst inner join ql_trnsjbelidtl sjdtl on sjmst.trnsjbelioid=sjdtl.trnsjbelioid where sjmst.cmpcode='" & CompnyCode & "' and sjmst.trnsjbelino like '%" & Tchar(lpbNo.Text) & "%' AND sjmst.trnbelipono LIKE '%" & Tchar(pono.Text) & "%' order by RIGHT(trnsjbelino,4) desc,sjmst.trnsjbelidate Desc"
        FillGV(gvLPB, sSql, "ql_trnsjbelimst")
    End Sub

    Sub bindDataReturn()
        Dim sWhereTemp As String = ""
        sSql = "select trnbelireturmstoid trnretbelimstoid,trnbelireturno trnretbelino,trnbelidate trnretbelidate from QL_trnbeliReturmst where cmpcode='" & CompnyCode & "' and trnbelireturno like '%" & Tchar(purchaseReturn.Text) & "%' " & sWhereTemp & ""
        FillGV(gvPurchaseReturn, sSql, "QL_trnretbelimst")
    End Sub

    Sub showPrint(ByVal ekstensi As String)
        vReport = New ReportDocument
        If ddlType.SelectedValue = "Summary" Then
            vReport.Load(Server.MapPath("~\Report\rptRealisasi_Purchase_Summary.rpt"))
        Else
            If ekstensi = "EXCEL" Then
                vReport.Load(Server.MapPath("~\Report\rptRealisasiPODtlExl.rpt"))
            Else
                vReport.Load(Server.MapPath("~\Report\rptRealisasiPODtl.rpt"))
            End If
        End If

        Try
            Dim dat1 As Date = CDate(toDate(podate1.Text))
            Dim dat2 As Date = CDate(toDate(podate2.Text))
        Catch ex As Exception
            showMessage("Format tanggal periode salah", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End Try

        If CDate(toDate(podate1.Text)) > CDate(toDate(podate2.Text)) Then
            showMessage("Tanggal Awal tidak boleh lebih besar dari tanggal akhir !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        Try
            Dim sWhere As String = "" : Dim sWhereInv As String = ""
            Dim d1 As String = "" : Dim d2 As String = ""
            Dim sPic As String = ""
            sWhere = " and convert(char(10)," & ddlOption1.SelectedValue & ",121) between '" & Format(CDate(toDate(podate1.Text)), "yyyy-MM-dd") & "' and '" & Format(CDate(toDate(podate2.Text)), "yyyy-MM-dd") & "' " & sPic & ""

            sWhere &= IIf(FilterDDLGrup.SelectedValue = "ALL GROUP", "  ", "  and  m.itemgroupoid=" & FilterDDLGrup.SelectedValue & " ")
            sWhere &= IIf(FilterDDLSubGrup.SelectedValue = "ALL SUB GROUP", "  ", "  and  m.itemsubgroupoid=" & FilterDDLSubGrup.SelectedValue & " ")

            If suppoid.Text.Trim <> "" Then
                sWhere &= " and s.suppoid=" & suppoid.Text & ""
            End If
            If ddlTax.SelectedValue <> "ALL" Then
                If ddlTax.SelectedValue = "Yes" Then
                    sWhere &= " and pomst.trntaxpct>0"
                Else
                    sWhere &= " and pomst.trntaxpct=0"
                End If
            End If

            If matoid.Text.Trim <> "" Then
                sWhere &= " and m.itemoid=" & matoid.Text & ""
            End If

            If pomstoid.Text.Trim <> "" Then
                sWhere &= " and pomst.trnbelimstoid=" & pomstoid.Text & ""
            End If

            If lpboid.Text.Trim <> "" Then
                sWhere &= " and sjmst.trnsjbelioid=" & lpboid.Text & ""
            End If

            If postatus.SelectedItem.Text <> "ALL" Then
                sWhere &= " and pomst.trnbelistatus='" & postatus.SelectedValue & "'"
            End If

            If ddlType.SelectedValue = "Detail" Then
                If purchaseReturnOid.Text <> "" Then
                    sWhere &= " and retmst.trnbelireturmstoid=" & purchaseReturnOid.Text & ""
                End If
                sWhere &= " and m.merk like '%" & merk.Text & "%' "
                vReport.SetParameterValue("pretno", IIf(Tchar(purchaseReturn.Text) = "", "ALL", Tchar(purchaseReturn.Text)))
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With
            SetDBLogonForReport(crConnInfo, vReport)

            vReport.SetParameterValue("cmpcode", CompnyCode)
            vReport.SetParameterValue("sWhere", sWhere)
            vReport.SetParameterValue("material", IIf(Tchar(material.Text.Trim) = "", "ALL", material.Text))
            vReport.SetParameterValue("lpbno", IIf(Tchar(lpbNo.Text.Trim) = "", "ALL", lpbNo.Text))
            vReport.SetParameterValue("postatus", postatus.SelectedItem.Text)
            vReport.SetParameterValue("poDate1", podate1.Text)
            vReport.SetParameterValue("poDate2", podate2.Text)
            vReport.SetParameterValue("pono", IIf(Tchar(pono.Text.Trim) = "", "ALL", pono.Text))
            vReport.SetParameterValue("taxable", ddlTax.SelectedValue)
            vReport.SetParameterValue("filter1", ddlOption1.SelectedItem.Text)
            vReport.SetParameterValue("matcat", FilterDDLGrup.SelectedItem.Text)
            vReport.SetParameterValue("matsubcat", FilterDDLSubGrup.SelectedItem.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            If ekstensi = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            ElseIf ekstensi = "PDF" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                crv_pembelian.DisplayGroupTree = False
                crv_pembelian.ReportSource = vReport
            End If
        Catch ex As Exception
            ' showMessage(ex.ToString, CompnyName & " - Error")
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim cmpcode As String = Session("CompnyCode")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            Response.Redirect("frmRealisasi_Pembelian.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Page.Title = CompnyName & " - Report Realisasi Pembelian"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not Page.IsPostBack Then '
            initAllDDL()
            Session("showReport") = False
            podate1.Text = "01/" & Date.Now.Month & "/" & Date.Now.Year & ""
            podate2.Text = Format(Date.Now, "dd/MM/yyyy")
        End If

        If Session("showReport") = "True" Then
            showPrint("")
        End If
    End Sub

    Protected Sub ibSearchLPB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchLPB.Click
        If lpbNo.Text.Trim = "" Then
            'showMessage("Please fill LPB No first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        gvLPB.Visible = True
        bindDataLPB()
    End Sub

    Protected Sub ibSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchMat.Click
        gvItem.Visible = True
        bindDataMaterial()
    End Sub

    Protected Sub ibSearchPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchPO.Click
        bindDataPO()
    End Sub

    Protected Sub ibClearMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClearMat.Click
        material.Text = "" : matoid.Text = ""
        gvItem.Visible = False
    End Sub

    Protected Sub ibDelLPB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelLPB.Click
        lpbNo.Text = "" : lpboid.Text = ""
        gvLPB.Visible = False
    End Sub

    Protected Sub ibDelPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelPO.Click
        pono.Text = "" : pomstoid.Text = ""
        gvPO.Visible = False
    End Sub

    Protected Sub gvLPB_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLPB.PageIndexChanging
        gvLPB.PageIndex = e.NewPageIndex
        gvLPB.Visible = True
        bindDataLPB()
    End Sub

    Protected Sub gvPO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPO.PageIndexChanging
        gvPO.PageIndex = e.NewPageIndex
        gvPO.Visible = True
        bindDataPO()
    End Sub

    Protected Sub gvPO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPO.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPO.SelectedIndexChanged
        pomstoid.Text = gvPO.SelectedDataKey("trnbelimstoid").ToString
        pono.Text = gvPO.SelectedDataKey("pono").ToString
        gvPO.Visible = False
    End Sub

    Protected Sub gvLPB_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLPB.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvLPB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvLPB.SelectedIndexChanged
        lpboid.Text = gvLPB.SelectedDataKey("trnsjbelioid").ToString
        lpbNo.Text = gvLPB.SelectedDataKey("trnsjbelino").ToString
        gvLPB.Visible = False
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToPDF.Click
        showPrint("PDF")
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbExport.Click
        showPrint("EXCEL")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("frmRealisasi_Pembelian.aspx")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub ibSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchSupp.Click
        gvSupplier.Visible = True
        bindDataSupplier()
    End Sub

    Protected Sub ibDelSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelSupp.Click
        suppoid.Text = "" : suppName.Text = ""
        gvSupplier.Visible = False
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        gvSupplier.Visible = True
        bindDataSupplier()
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSupplier.SelectedIndexChanged
        suppoid.Text = gvSupplier.SelectedDataKey("suppoid").ToString
        suppName.Text = gvSupplier.SelectedDataKey("suppname").ToString
        gvSupplier.Visible = False
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Summary" Then
            TD1.Visible = False : TD2.Visible = False : TD3.Visible = False
            TDmerk1.Visible = False : TDmerk2.Visible = False : TDmerk3.Visible = False
        Else
            TD1.Visible = True : TD2.Visible = True : TD3.Visible = True
            TDmerk1.Visible = True : TDmerk2.Visible = True : TDmerk3.Visible = True
        End If
    End Sub

    Protected Sub ibSearchPurchaseReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibSearchPurchaseReturn.Click
        If purchaseReturn.Text.Trim = "" Then
            'showMessage("Please fill Purchase Return No first for filter !", CompnyName & " - Warning") : Exit Sub
        End If
        gvPurchaseReturn.Visible = True
        bindDataReturn()
    End Sub

    Protected Sub ibDelPurchaseReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibDelPurchaseReturn.Click
        purchaseReturn.Text = "" : purchaseReturnOid.Text = ""
        gvPurchaseReturn.Visible = False
    End Sub

    Protected Sub gvPurchaseReturn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPurchaseReturn.PageIndexChanging
        gvPurchaseReturn.PageIndex = e.NewPageIndex
        gvPurchaseReturn.Visible = True
        bindDataReturn()
    End Sub

    Protected Sub gvPurchaseReturn_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPurchaseReturn.SelectedIndexChanged
        purchaseReturn.Text = gvPurchaseReturn.SelectedDataKey("trnretbelino").ToString
        purchaseReturnOid.Text = gvPurchaseReturn.SelectedDataKey("trnretbelimstoid").ToString
        gvPurchaseReturn.Visible = False
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\ReportForm\frmRealisasi_Pembelian.aspx?awal=true")
        End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        gvItem.Visible = True
        bindDataMaterial()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        material.Text = gvItem.SelectedDataKey.Item("itemdesc")
        matoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.DataSource = Nothing
        gvItem.DataBind()
        gvItem.Visible = False
        'PICddl(matoid.Text)
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Session("showReport") = "True"
        showPrint("")
    End Sub
#End Region
End Class
