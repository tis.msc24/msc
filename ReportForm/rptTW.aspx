<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptTW.aspx.vb" Inherits="rptTW" Title="Toko Ali - Laporan Transfer Warehouse" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Transfer Warehouse"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: transparent" valign="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD style="VERTICAL-ALIGN: top" id="tdPeriod1" align=left runat="server" visible="true"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w37"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" id="Td1" align=left runat="server" visible="true">:</TD><TD style="VERTICAL-ALIGN: top" id="tdperiod2" align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dateAwal" runat="server" Width="62px" CssClass="inpText" __designer:wfdid="w38"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="62px" CssClass="inpText" __designer:wfdid="w40"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w41"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w42">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w43" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w44" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w45" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w46" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD align=left>Type TW</TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLTypeTW" runat="server" Width="163px" CssClass="inpText" __designer:wfdid="w47" AutoPostBack="True" Font-Italic="True"></asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD align=left>Branch Asal</TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLFromBranch" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w48" AutoPostBack="True"></asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD align=left><asp:Label id="lblAsal" runat="server" Width="81px" Text="Lokasi Asal" __designer:wfdid="w13"></asp:Label> </TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlLocation" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w50">
                                            </asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD align=left>Branch Tujuan </TD><TD align=right>:</TD><TD align=left colSpan=3><asp:DropDownList id="DDLToBranch" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w51" AutoPostBack="True"></asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD style="HEIGHT: 21px" align=left><asp:Label id="lblTujuan" runat="server" Width="97px" Text="Lokasi Tujuan" __designer:wfdid="w16"></asp:Label> </TD><TD style="HEIGHT: 21px" align=right>:</TD><TD style="HEIGHT: 21px" align=left colSpan=3><asp:DropDownList id="ddlLocation1" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w53">
                                            </asp:DropDownList> &nbsp;&nbsp;</TD></TR><TR><TD vAlign=top align=left>No&nbsp;Transfer</TD><TD vAlign=top align=right>:</TD><TD align=left colSpan=3><asp:TextBox id="nota" runat="server" Width="236px" CssClass="inpText" __designer:wfdid="w54"></asp:TextBox> <asp:ImageButton id="btnSearchNota" onclick="btnSearchNota_Click1" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w55"></asp:ImageButton>&nbsp;<asp:ImageButton id="EraseNota" onclick="EraseNota_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w56"></asp:ImageButton><asp:Label id="oid" runat="server" __designer:wfdid="w57" Visible="False"></asp:Label>&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD vAlign=top align=left></TD><TD vAlign=top align=right></TD><TD align=left colSpan=3><asp:GridView id="GVNota" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w58" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trfmtrmstoid,transferno" OnSelectedIndexChanged="GVNota_SelectedIndexChanged1" PageSize="5" UseAccessibleHeader="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Maroon" Width="50px" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="transferno" HeaderText="No Transfer">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trfmtrdate" HeaderText="Tanggal TW">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="status" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="VERTICAL-ALIGN: top" align=left><asp:Label id="Label1" runat="server" Width="85px" Text="Item/Barang" __designer:wfdid="w59"></asp:Label></TD><TD style="VERTICAL-ALIGN: top" align=right>:</TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="255px" CssClass="inpTextDisabled" __designer:dtid="562949953421371" __designer:wfdid="w7" TextMode="MultiLine" Rows="2" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w9"></asp:ImageButton>&nbsp;&nbsp;&nbsp;<asp:Label id="itemoid" runat="server" __designer:wfdid="w63" Visible="False"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 1px" align=left><asp:Label id="Label2" runat="server" Width="85px" Text="Status" __designer:wfdid="w65" Visible="False"></asp:Label></TD><TD style="HEIGHT: 1px" align=right><asp:Label id="Label3" runat="server" Width="5px" Text=":" __designer:wfdid="w66" Visible="False"></asp:Label></TD><TD style="HEIGHT: 1px" align=left colSpan=3><asp:DropDownList id="DDLStatus" runat="server" CssClass="inpText" __designer:wfdid="w67" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w68"></asp:ImageButton> <asp:ImageButton id="btnpdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w69"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w70"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w72" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w73"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w74"></asp:Label><asp:Label id="labelEx" runat="server" Width="98px" __designer:wfdid="w75"></asp:Label></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w36" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>&nbsp; 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnpdf"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<asp:Panel id="PanelErrMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE width=250><TBODY><TR><TD style="WIDTH: 294px; HEIGHT: 15px; BACKGROUND-COLOR: red" align=left colSpan=3><asp:Label id="lblCaptions" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR></TBODY></TABLE><TABLE width=250><TBODY><TR><TD style="WIDTH: 30px"><asp:ImageButton id="imIcons" runat="server" ImageUrl="~/Images/warn.png"></asp:ImageButton></TD><TD colSpan=2><asp:Label id="lblValidasi" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="WIDTH: 30px"></TD><TD colSpan=2><asp:ImageButton id="btnErrOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="MPEErrMsg" runat="server" TargetControlID="btnExtender" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="PanelErrMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtender" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="750px" CssClass="modalBox" Visible="False" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=4><asp:Label id="lblListMat" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Katalog</asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4>Filter : &nbsp;&nbsp; <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemdesc">Description</asp:ListItem>
<asp:ListItem Value="itemcode">Kode Katalog</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=center colSpan=4>Jenis Barang : &nbsp; <asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=4><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 325px"><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="itemcode,itemdesc,itemoid,satuan3" PageSize="100" UseAccessibleHeader="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# Eval("itemoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Merk" HeaderText="Merk">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=4><asp:LinkButton id="lbAddToListMat" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListMat" PopupControlID="pnlListMat" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
                </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

