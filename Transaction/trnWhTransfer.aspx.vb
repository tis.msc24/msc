Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnWhTransfer
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
    Public ItemcodeToSN As String
    Dim NoTWID As String
    Dim PrintDoc1 As New System.Drawing.Printing.PrintDocument
#End Region

#Region "Procedure"
    Dim dtab As DataTable
    Dim drow As DataRow
    Dim drowedit() As DataRow

    Private Sub FillTextbox()
        Dim tolocationoid As Integer = 0
        sSql = "SELECT transferno, trfmtrdate, trfmtrnote, noref, status,fromMtrBranch, FromMtrlocoid, toMtrBranch, ToMtrlocoid, updtime, upduser FROM QL_trntrfmtrmst WHERE cmpcode = '" & cmpcode & "' AND trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                InitAllDDL()
                Session("AprovelToTW") = xreader("toMtrBranch")
                If Session("branch_id") = xreader("fromMtrBranch") Then
                    fromBramch.Visible = True : fromlocationBranch.Visible = True
                    toBranch.Visible = True : tolocationBranch.Visible = True
                    fromBramch.SelectedValue = xreader("fromMtrBranch")

                    sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother6='UMUM' And a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & fromBramch.SelectedValue & "' and gengroup = 'cabang')"
                    FillDDL(fromlocationBranch, sSql)
                    fromlocationBranch.SelectedValue = xreader("FromMtrlocoid")
                    toBranch.SelectedValue = xreader("toMtrBranch")
                    tolocationBranch.SelectedValue = xreader("ToMtrlocOid")

                    Lb_FromBranch.Text = "" : Lb_FromLoc.Text = ""
                    Lb_ToBranch.Text = "" : Lb_ToLoc.Text = ""
                Else
                    fromBramch.Visible = False : fromlocationBranch.Visible = False
                    toBranch.Visible = False : tolocationBranch.Visible = False
                    sSql = "select gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & xreader("fromMtrBranch") & "' "
                    Lb_FromBranch.Text = ckon.ambilscalar(sSql)
                    sSql = "select (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother6='UMUM' and genoid =  '" & xreader("FromMtrlocoid") & "' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & xreader("fromMtrBranch") & "' and gengroup = 'cabang')"
                    Lb_FromLoc.Text = ckon.ambilscalar(sSql)
                    sSql = "select gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & xreader("toMtrBranch") & "' "
                    Lb_ToBranch.Text = ckon.ambilscalar(sSql)
                    sSql = "select (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother6='UMUM' and genoid =  '" & xreader("ToMtrlocOid") & "' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & xreader("toMtrBranch") & "' and gengroup = 'cabang')"
                    Lb_ToLoc.Text = ckon.ambilscalar(sSql)

                End If
                transferno.Text = xreader("transferno")
                transferdate.Text = Format(xreader("trfmtrdate"), "dd/MM/yyyy")
                fromBramch.SelectedValue = xreader("fromMtrBranch")
                InitDDLLocation()
                fromlocationBranch.SelectedValue = xreader("FromMtrlocoid")
                Session("BC") = xreader("fromMtrBranch")
                Session("ML") = xreader("FromMtrlocoid")
                note.Text = xreader("trfmtrnote")
                noref.Text = xreader("noref")

                toBranch.SelectedValue = xreader("toMtrBranch")
                'sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & toBranch.SelectedValue & "' and gengroup = 'cabang') and a.genoid <> '" & fromlocationBranch.SelectedValue & "'"
                'FillDDL(tolocationBranch, sSql)
                InitDDLCabang()
                tolocationBranch.SelectedValue = xreader("ToMtrlocOid")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()

        sSql = "SELECT a.seq, a.refoid AS itemoid, b.itemdesc, b.itemcode, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, d.gendesc AS unit2, d.gendesc AS unit3, a.trfdtlnote AS note,ISNULL(statusexp,'TIDAK') statusexp,ISNULL(a.typedimensi,0) typedimensi,Isnull(a.beratvolume,0.00) beratvolume,Isnull(a.beratbarang,0.00) beratbarang,ISNULL(a.jenisexp,'') jenisexp,Isnull(a.amtexpedisi,0.0) amtexpedisi,ISNULL(a.nettoexp,0.00) nettoexp,b.stockflag FROM QL_trntrfmtrdtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' WHERE a.cmpcode = '" & cmpcode & "' AND a.trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailtw")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        sSql = "SELECT status FROM QL_trntrfmtrmst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        Dim xStatus As String = ckon.ambilscalar(sSql)

        If xStatus = "Approved" Then
            sSql = "SELECT transferno FROM QL_trntrfmtrmst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            NoTWID = ckon.ambilscalar(sSql)
        Else
            NoTWID = Integer.Parse(Session("oid"))
        End If
        sSql = "SELECT row_number() OVER (ORDER BY sn.id_sn) AS 'SNseq', i.itemoid, sn.id_sn SN from QL_Mst_SN sn INNER JOIN QL_mstItem i ON sn.itemcode = i.itemcode WHERE trntrfoid ='" & NoTWID & "'"
        Session("TblSN") = ckon.ambiltabel2(sSql, "TbSN")

        If trnstatus.Text = "Rejected" Or trnstatus.Text = "In Approval" Or trnstatus.Text = "Approved" Then
            btnSave.Visible = False : btnDelete.Visible = False
            BtnCancel.Visible = True : btnPosting.Visible = False
            btnApproval.Visible = False
        Else
            sSql = "SELECT DISTINCT fromMtrBranch from QL_trntrfmtrmst WHERE fromMtrBranch = '" & Session("branch_id") & "'and trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            Dim xFrom As String = ckon.ambilscalar(sSql)

            'If xFrom = Session("branch_id") Then
            btnSave.Visible = True
            btnDelete.Visible = True : BtnCancel.Visible = True
            '    btnPosting.Visible = False : 
            btnApproval.Visible = True
            'Else
            '    btnSave.Visible = False
            '    btnDelete.Visible = False : BtnCancel.Visible = True
            '    btnPosting.Visible = False : btnApproval.Visible = False
            'End If
        End If

        For i As Integer = 0 To dtab.Rows.Count - 1
            If dtab.Rows(i).Item("statusexp") = "YA" Then
                GVItemDetail.Columns(9).Visible = True
                GVItemDetail.Columns(10).Visible = True
                GVItemDetail.Columns(11).Visible = True
                GVItemDetail.Columns(12).Visible = True
                GVItemDetail.Columns(13).Visible = True
            Else
                GVItemDetail.Columns(8).Visible = False
                GVItemDetail.Columns(9).Visible = False
                GVItemDetail.Columns(10).Visible = False
                GVItemDetail.Columns(11).Visible = False
                GVItemDetail.Columns(12).Visible = False
                GVItemDetail.Columns(13).Visible = False
            End If
        Next
        conn.Close()
        tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")
    End Sub

    Private Sub BindData(ByVal state As String)
        Dim date1, date2 As String : Dim sWhere As String = ""
        If state = 0 Then
            'InitAllDDL()
            tgl1.Text = toDate(tgl1.Text) : tgl2.Text = toDate(tgl2.Text)
            date1 = Format(GetServerTime, "dd/MM/yyyy")
            date2 = Format(GetServerTime, "dd/MM/yyyy")
            'dd_branchTO.SelectedValue = "SEMUA BRANCH"
            If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
                sWhere &= "and a.upduser = '" & Session("UserID") & "'"
            End If

            If DDLTypeTW.SelectedValue = "Send TW" Then
                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.fromMtrBranch = '" & dd_branch.SelectedValue & "'"
                End If
                If dd_branchTO.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.toMtrBranch = '" & dd_branchTO.SelectedValue & "'"
                End If
            Else
                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.fromMtrBranch = '" & dd_branchTO.SelectedValue & "'"
                End If
                If dd_branchTO.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.toMtrBranch = '" & dd_branch.SelectedValue & "'"
                End If
            End If

            sSql = "SELECT a.trfmtrmstoid, a.transferno, a.trfmtrdate, a.upduser,(select d.gendesc from QL_mstgen d where d.gencode = a.fromMtrBranch AND d.gengroup='CABANG') as 'FromBranch', (select d.gendesc from QL_mstgen d where d.gencode = a.toMtrBranch AND d.gengroup='CABANG') as 'ToBranch', a.status,CASE status When 'Revised' THEN 'Revisi :'+a.revisenote +' '+ a.trfmtrnote ELSE a.trfmtrnote END Note FROM QL_trntrfmtrmst a WHERE a.cmpcode = '" & cmpcode & "' " & sWhere & " ORDER BY case a.status when 'In Process' then 1 when 'In Approval' then 2 when 'Rejected' then 3 else 4 end , a.transferno desc "
        Else
            date1 = Format(CDate(toDate(tgl1.Text)), "MM/dd/yyyy")
            date2 = Format(CDate(toDate(tgl2.Text)), "MM/dd/yyyy") 
            If FilterText.Text.Trim <> "" Then
                sWhere &= " AND a.transferno LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
            End If

            If DDLTypeTW.SelectedValue = "Send TW" Then
                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.fromMtrBranch = '" & dd_branch.SelectedValue & "'"
                End If
                If dd_branchTO.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.toMtrBranch = '" & dd_branchTO.SelectedValue & "'"
                End If
            Else
                If dd_branch.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.fromMtrBranch = '" & dd_branchTO.SelectedValue & "'"
                End If
                If dd_branchTO.SelectedValue <> "SEMUA BRANCH" Then
                    sWhere &= " and a.toMtrBranch = '" & dd_branch.SelectedValue & "'"
                End If
            End If

            If ddl_status.SelectedValue <> "ALL" Then
                sWhere = "AND a.status='" & ddl_status.SelectedValue & "'"
            End If

            'If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
            '    sWhere &= "and a.upduser = '" & Session("UserID") & "'"
            'End If

            If DDLTypeTW.SelectedValue = "Send TW" Then
                sSql = "SELECT a.trfmtrmstoid, a.transferno, a.trfmtrdate, a.upduser, a.status,(SELECT DISTINCT d.gendesc FROM QL_mstgen d  Where d.gencode = a.fromMtrBranch AND d.gengroup='CABANG') as 'FROMBranch',(select DISTINCT f.gendesc FROM QL_mstgen f Where f.gencode = a.toMtrBranch AND f.gengroup='CABANG') as 'ToBranch' ,CASE status When 'Revised' THEN 'Revisi :'+a.revisenote +' '+ a.trfmtrnote ELSE a.trfmtrnote END Note FROM QL_trntrfmtrmst a WHERE a.cmpcode = '" & cmpcode & "' AND a.trfmtrdate BETWEEN '" & date1 & "' AND '" & date2 & "' " & sWhere & " ORDER BY case a.status when 'In Process' then 1 when 'In Approval' then 2 when 'Rejected' then 3 else 4 end , a.transferno desc "
            Else
                sSql = "SELECT * FROM (" & _
                " SELECT DISTINCT a.trfmtrmstoid, a.transferno, a.trfmtrdate, a.upduser,(select d.gendesc from QL_mstgen d inner join QL_mstgen e on d.cmpcode=e.cmpcode and d.genoid=e.genoid where d.gencode = a.fromMtrBranch AND d.gengroup='CABANG') as 'FromBranch', (select d.gendesc from QL_mstgen d inner join QL_mstgen e on d.cmpcode=e.cmpcode and d.genoid=e.genoid  where d.gencode = a.toMtrBranch AND d.gengroup='CABANG') as 'ToBranch' , a.status,CASE status When 'Revised' THEN 'Revisi :'+a.revisenote +' '+ a.trfmtrnote ELSE a.trfmtrnote END Note FROM QL_trntrfmtrmst a INNER JOIN QL_trntrfmtrdtl d ON d.trfmtrmstoid = a.trfmtrmstoid WHERE a.cmpcode = '" & cmpcode & "' AND Convert(CHAR(20),a.trfmtrdate,101) BETWEEN '" & date1 & "' AND '" & date2 & "' " & sWhere & ") terima ORDER BY case status when 'In Process' then 1 when 'In Approval' then 2 when 'Rejected' then 3 else 4 end , transferno desc "
            End If
        End If
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "tw")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("tw") = dtab
    End Sub

    Private Sub BindItem()
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        Dim TypeExp As String = "'0'"
        If CbExp.Checked = True Then
            TypeExp = "ISNULL((Select isnull(price,0.00) From mstexpedisibranch ex Where ex.itemoid=a.itemoid and expedisi_type='" & ddtype.SelectedValue & "'),0.00)"
        End If

        sSql = "Select * from (SELECT row_number() OVER(ORDER BY a.itemdesc) AS 'No', a.itemoid, a.itemcode, a.itemdesc, a.merk, ISNULL(SUM(r.qtyIn)-SUM(qtyOut),0.0000) sisa, a.satuan1,g.gendesc unit1, a.satuan1 satuan2, g.gendesc unit2, a.satuan1 satuan3, g.gendesc unit3, 1 konversi1_2,1 konversi2_3,dimensi_p,dimensi_l,dimensi_t,beratvolume,beratbarang, " & TypeExp & " ByExp,a.stockflag, Case a.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya From QL_conmtr r INNER JOIN QL_mstItem a ON a.itemoid = r.refoid INNER join QL_mstgen g on g.genoid = a.satuan1 and g.gengroup='ITEMUNIT' WHERE r.periodacctg ='" & GetServerTime.ToString("yyyy-MM") & "' AND r.branch_code = '" & fromBramch.SelectedValue & "' AND r.mtrlocoid = '" & fromlocationBranch.SelectedValue & "' and a.itemflag = 'Aktif' AND a.cmpcode ='" & cmpcode & "' /*AND a.stockflag<>'ASSET'*/ Group By a.itemdesc ,a.itemoid,a.itemcode,a.Merk,a.satuan1,g.gendesc,a.dimensi_l,a.dimensi_p,a.dimensi_t,a.beratbarang,a.beratvolume,a.stockflag,r.periodacctg,r.mtrlocoid,r.branch_code,r.refoid,a.stockflag) a Where (a.itemdesc LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.itemcode LIKE '%" & Tchar(item.Text.Trim) & "%' OR a.merk LIKE '%" & Tchar(item.Text.Trim) & "%' Or JenisNya LIKE '%" & Tchar(item.Text.Trim) & "%')"

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        Session("ItemPilih") = objTable
        GVItemList.DataSource = objTable
        GVItemList.DataBind()
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"

        If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fromBramch, sSql)
        ElseIf Session("LevelUser") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fromBramch, sSql)
            Else
                FillDDL(fromBramch, sSql)
            End If
        ElseIf Session("LevelUser") = 1 Or Session("LevelUser") = 3 Then
            FillDDL(fromBramch, sSql)
        End If
    End Sub

    Private Sub keCabang()
        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
        FillDDL(toBranch, sSql)
        If fromBramch.Items.Count = 0 Then
            showMessage("Please create/fill Data General in group Cabang!", 3)
        End If
    End Sub

    Private Sub InitDDLLocation()
        'From Locotion
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' AND a.genother6='UMUM' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & fromBramch.SelectedValue & "' and gengroup = 'cabang')"
        FillDDL(fromlocationBranch, sSql)
    End Sub

    Private Sub InitDDLCabang()
        'To Locotion
        sSql = "select a.genoid, (SELECT gendesc FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1) +' - '+a.gendesc gendesc from QL_mstgen a WHERE a.gengroup = 'Location' And a.genother6='UMUM' and a.genother2 = (SELECT genoid FROM QL_mstgen WHERE gencode = '" & toBranch.SelectedValue & "' and gengroup = 'cabang') and a.genoid <> '" & fromlocationBranch.SelectedValue & "'"
        FillDDL(tolocationBranch, sSql)
    End Sub

    Private Sub ddlCabang()

        If DDLTypeTW.SelectedValue = "Send TW" Then
            sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
            If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branch, sSql)
            ElseIf Session("LevelUser") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(dd_branch, sSql)
                Else
                    FillDDL(dd_branch, sSql)
                End If
            ElseIf Session("LevelUser") = 1 Or Session("LevelUser") = 3 Then
                FillDDL(dd_branch, sSql)
            End If
            Dim sSqli As String = "Select gencode,gendesc from ql_mstgen where gengroup='Cabang'"
            FillDDL(dd_branchTO, sSqli)
            dd_branchTO.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branchTO.SelectedValue = "SEMUA BRANCH"
        Else
            sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
            If Session("LevelUser") = 4 Or Session("LevelUser") = 0 Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(dd_branchTO, sSql)
            ElseIf Session("LevelUser") = 2 Then
                If Session("branch_id") <> "10" Then
                    sSql &= " AND gencode='" & Session("branch_id") & "'"
                    FillDDL(dd_branchTO, sSql)
                Else
                    FillDDL(dd_branchTO, sSql)
                End If
            ElseIf Session("LevelUser") = 1 Or Session("LevelUser") = 3 Then
                FillDDL(dd_branchTO, sSql)
            End If
            Dim sSqli As String = "Select gencode,gendesc from ql_mstgen where gengroup='Cabang'"
            FillDDL(dd_branch, sSqli)
            dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            dd_branch.SelectedValue = "SEMUA BRANCH"
        End If
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnWHTransfer.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")
        btnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to approval?')")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Transfer Warehouse"

        Session("LevelUser") = GetStrData("SELECT userlevel FROM QL_mstprof WHERE BRANCH_CODE='" & Session("branch_id") & "' And USERID='" & Session("UserID") & "'")

        If Not Page.IsPostBack Then
            ddlCabang() : BindData(0)
            transferno.Text = GenerateID("QL_TRNTRFMTRMST", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            InitAllDDL() : keCabang() : InitDDLCabang()
            cb1_CheckedChanged(Nothing, Nothing)
            cb2_CheckedChanged(Nothing, Nothing)
            InitDDLLocation()
            If Session("itemdetail") Is Nothing Then
                fromBramch.Enabled = True : fromBramch.CssClass = "inpText"
                fromlocationBranch.Enabled = True : fromlocationBranch.CssClass = "inpText"
                toBranch.Enabled = True : toBranch.CssClass = "inpText"
                tolocationBranch.Enabled = True : tolocationBranch.CssClass = "inpText"
            End If

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(GetServerTime(), "01/MM/yyyy")
                tgl2.Text = Format(GetServerTime(), "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0

                qty.Text = "0" : GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                trnstatus.Text = "In Process" : labelseq.Text = "1"
                labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        tgl1.Text = Format(GetServerTime, "01/MM/yyyy")
        tgl2.Text = Format(GetServerTime, "dd/MM/yyyy")
        BindData(0)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1 As String : Dim date2 As String
        date1 = Format((toDate(tgl1.Text)), "dd/MM/yyyy")
        date2 = Format((toDate(tgl2.Text)), "dd/MM/yyyy")

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If
        BindData(1)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            BindItem()
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = ""
        qty.Text = "0" : labelmaxqty.Text = "0"
        merk.Text = "" : notedtl.Text = ""

        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        Try
            GVItemList.DataSource = Session("ItemPilih")
            GVItemList.PageIndex = e.NewPageIndex
            GVItemList.DataBind()
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = IIf(IsDBNull(GVItemList.SelectedDataKey("itemdesc")), "", GVItemList.SelectedDataKey("itemdesc"))
        labelitemoid.Text = IIf(IsDBNull(GVItemList.SelectedDataKey("itemoid")), "", GVItemList.SelectedDataKey("itemoid"))
        LabelItemCode.Text = IIf(IsDBNull(GVItemList.SelectedDataKey("itemcode")), "", GVItemList.SelectedDataKey("itemcode"))
        merk.Text = IIf(IsDBNull(GVItemList.SelectedDataKey("merk")), "", GVItemList.SelectedDataKey("merk"))

        qty.Text = ToDouble(0)

        labelmaxqty.Text = GVItemList.SelectedDataKey("sisa")
        unit.Items.Clear()
        unit.Items.Add(GVItemList.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.SelectedDataKey("unit2").ToString
        stockflag.Text = GVItemList.SelectedDataKey("stockflag").ToString

        If CbExp.Checked = True Then
            AmtExpedisi.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("ByExp")), 3)
        Else
            AmtExpedisi.Text = ToMaskEdit(ToDouble(0), 3)
        End If

        If typeDimensi.Text = 1 Then
            beratBarang.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("beratvolume")), 3)
        Else
            beratBarang.Text = ToMaskEdit(ToDouble(GVItemList.SelectedDataKey("beratBarang")), 3)
        End If

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")
        ItemcodeToSN = GVItemList.SelectedDataKey("itemcode")

        I_u2.Text = "new"
        GVItemList.Visible = False
        GVItemList.DataSource = Nothing
        GVItemList.DataBind()
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnWHTransfer.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        qty.Text = ToDouble(qty.Text)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        CbExp.Enabled = False
        cb1.Enabled = False : cb2.Enabled = False
        If item.Text = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text) > Double.Parse(labelmaxqty.Text) Then
            showMessage("Qty tidak boleh lebih dari maksimum qty!", 2)
            Exit Sub
        End If

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("sisa", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                dtab.Columns.Add("statusexp", Type.GetType("System.String"))
                dtab.Columns.Add("typedimensi", Type.GetType("System.Int32"))
                dtab.Columns.Add("beratvolume", Type.GetType("System.Double"))
                dtab.Columns.Add("beratbarang", Type.GetType("System.Double"))
                dtab.Columns.Add("jenisexp", Type.GetType("System.String"))
                dtab.Columns.Add("amtexpedisi", Type.GetType("System.Double"))
                dtab.Columns.Add("nettoexp", Type.GetType("System.Double"))
                dtab.Columns.Add("stockflag", Type.GetType("System.String"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                Exit Sub
            End If
        End If

        If i_u.Text = "new" Then
            sSql = "SELECT DISTINCT isnull(i.has_sn,0) from QL_mstItem i INNER JOIN QL_mstItem_branch ib ON i.itemoid = ib.itemoid WHERE i.itemcode = '" & LabelItemCode.Text & "' and ib.branch_code = '" & fromBramch.SelectedValue & "'"
            Dim has_sn As String = ckon.ambilscalar(sSql)
            If has_sn = "1" Then
                TextSN.Text = ""
                LabelPesanSn.Text = ""
                If Session("TblSN") Is Nothing Then
                    pnlInvnSN.Visible = True : btnHideSN.Visible = True
                    TextItem.Text = item.Text : KodeItem.Text = labelitemoid.Text
                    MAXQTYSN.Text = qty.Text : GVSN.DataSource = Nothing
                    GVSN.DataBind() : ModalPopupSN.Show()
                    TextSN.Text = "" : LabelPesanSn.Text = ""
                    CProc.SetFocusToControl(Me.Page, TextSN)
                Else
                    Dim objTableSN As DataTable
                    objTableSN = Session("TblSN")
                    Dim dvjum As DataView = objTableSN.DefaultView
                    dvjum.RowFilter = "itemoid= '" & labelitemoid.Text & "'"

                    pnlInvnSN.Visible = True : btnHideSN.Visible = True
                    TextItem.Text = item.Text : KodeItem.Text = labelitemoid.Text
                    MAXQTYSN.Text = qty.Text : GVSN.DataSource = Nothing
                    GVSN.DataSource = dvjum : GVSN.DataSource = dvjum
                    GVSN.DataBind() : ModalPopupSN.Show()
                    TextSN.Text = "" : CProc.SetFocusToControl(Me.Page, TextSN)
                    NewSN.Text = "New SN"
                End If

            Else
                If I_u2.Text = "new" Then
                    drow = dtab.NewRow
                    drow("seq") = Integer.Parse(labelseq.Text)
                    drow("itemoid") = Integer.Parse(labelitemoid.Text)
                    drow("itemdesc") = item.Text.Trim
                    drow("itemcode") = LabelItemCode.Text
                    drow("merk") = merk.Text
                    drow("qty") = Double.Parse(qty.Text)
                    drow("satuan") = unit.SelectedValue
                    drow("unit") = unit.SelectedItem.Text
                    drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                    drow("unit1") = labelunit1.Text
                    drow("unit2") = labelunit2.Text
                    drow("unit3") = labelunit3.Text
                    drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                    drow("note") = notedtl.Text.Trim
                    drow("stockflag") = stockflag.Text
                    If CbExp.Checked = True Then 'Berat Volume
                        drow("statusexp") = "YA"
                        If cb2.Checked = True Then
                            drow("typedimensi") = typeDimensi.Text
                            drow("beratvolume") = ToDouble(beratBarang.Text)
                            drow("beratbarang") = ToDouble(0)
                            drow("nettoexp") = ToDouble(AmtExpedisi.Text) * (ToDouble(beratBarang.Text) / 1000000) * ToDouble(qty.Text)
                        ElseIf cb1.Checked = True Then 'Berat Barang
                            drow("typedimensi") = 2
                            drow("beratvolume") = ToDouble(0)
                            drow("beratbarang") = ToDouble(beratBarang.Text)
                            drow("nettoexp") = ToDouble(beratBarang.Text) * ToDouble(qty.Text) * ToDouble(AmtExpedisi.Text)
                        End If
                        drow("jenisexp") = ddtype.SelectedValue.ToUpper
                        drow("amtexpedisi") = ToDouble(AmtExpedisi.Text)
                        'nettoexp
                    Else
                        drow("statusexp") = "TIDAK"
                        drow("typedimensi") = ToDouble(0)
                        drow("beratvolume") = ToDouble(0)
                        drow("beratbarang") = ToDouble(0)
                        drow("jenisexp") = ""
                        drow("amtexpedisi") = ToDouble(0)
                        drow("nettoexp") = ToDouble(0)
                    End If

                    dtab.Rows.Add(drow) : dtab.AcceptChanges()
                    GVItemDetail.DataSource = dtab
                    GVItemDetail.DataBind()
                    Session("itemdetail") = dtab

                    labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
                    merk.Text = "" : qty.Text = "0"
                    labelmaxqty.Text = "0" : unit.Items.Clear()
                    notedtl.Text = "" : labelitemoid.Text = ""
                    item.Text = "" : I_u2.Text = "new"

                    labelsatuan1.Text = "" : labelsatuan2.Text = ""
                    labelsatuan3.Text = "" : labelunit1.Text = ""
                    labelunit2.Text = "" : labelunit3.Text = ""
                    labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
                    beratBarang.Text = 0 : AmtExpedisi.Text = 0
                    cb1.Checked = False : cb2.Checked = False
                    GVItemDetail.SelectedIndex = -1

                    If CbExp.Checked = True Then
                        'GVItemDetail.Columns(8).Visible = True
                        GVItemDetail.Columns(9).Visible = True
                        GVItemDetail.Columns(10).Visible = True
                        GVItemDetail.Columns(11).Visible = True
                        GVItemDetail.Columns(12).Visible = True
                        GVItemDetail.Columns(13).Visible = True
                    Else
                        GVItemDetail.Columns(8).Visible = False
                        GVItemDetail.Columns(9).Visible = False
                        GVItemDetail.Columns(10).Visible = False
                        GVItemDetail.Columns(11).Visible = False
                        GVItemDetail.Columns(12).Visible = False
                        GVItemDetail.Columns(13).Visible = False
                    End If

                    If Not Session("itemdetail") Is Nothing Then
                        fromBramch.Enabled = False : fromBramch.CssClass = "inpTextDisabled"
                        fromlocationBranch.Enabled = False
                        fromlocationBranch.CssClass = "inpTextDisabled"
                        toBranch.Enabled = False : toBranch.CssClass = "inpTextDisabled"
                        tolocationBranch.Enabled = False
                        tolocationBranch.CssClass = "inpTextDisabled"
                    End If
                    If GVItemList.Visible = True Then
                        GVItemList.DataSource = Nothing
                        GVItemList.DataBind()
                        GVItemList.Visible = False
                    End If

                Else
                    dtab = Session("itemdetail")
                    drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
                    drow = drowedit(0)
                    drowedit(0).BeginEdit()

                    drow("itemoid") = Integer.Parse(labelitemoid.Text)
                    drow("itemdesc") = item.Text.Trim
                    drow("merk") = merk.Text
                    drow("itemcode") = LabelItemCode.Text
                    drow("qty") = Double.Parse(qty.Text)
                    drow("satuan") = unit.SelectedValue
                    drow("unit") = unit.SelectedItem.Text

                    drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                    drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                    drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                    drow("unit1") = labelunit1.Text
                    drow("unit2") = labelunit2.Text
                    drow("unit3") = labelunit3.Text

                    drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                    drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                    drow("note") = notedtl.Text.Trim
                    drow("stockflag") = stockflag.Text
                    drowedit(0).EndEdit()
                    dtab.Select(Nothing, Nothing)
                    dtab.AcceptChanges()

                    GVItemDetail.DataSource = dtab
                    GVItemDetail.DataBind()
                    Session("itemdetail") = dtab

                    labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
                    merk.Text = "" : qty.Text = "0"
                    labelmaxqty.Text = "0" : unit.Items.Clear()
                    notedtl.Text = "" : labelitemoid.Text = ""
                    item.Text = "" : I_u2.Text = "new"
                    beratBarang.Text = 0 : AmtExpedisi.Text = 0

                    labelsatuan1.Text = "" : labelsatuan2.Text = ""
                    labelsatuan3.Text = "" : labelunit1.Text = ""
                    labelunit2.Text = "" : labelunit3.Text = ""
                    labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
                    GVItemDetail.SelectedIndex = -1

                    If CbExp.Checked = True Then
                        'GVItemDetail.Columns(8).Visible = True
                        GVItemDetail.Columns(9).Visible = True
                        GVItemDetail.Columns(10).Visible = True
                        GVItemDetail.Columns(11).Visible = True
                        GVItemDetail.Columns(12).Visible = True
                        GVItemDetail.Columns(13).Visible = True
                    Else
                        GVItemDetail.Columns(8).Visible = False
                        GVItemDetail.Columns(9).Visible = False
                        GVItemDetail.Columns(10).Visible = False
                        GVItemDetail.Columns(11).Visible = False
                        GVItemDetail.Columns(12).Visible = False
                        GVItemDetail.Columns(13).Visible = False
                    End If
                End If
            End If
        Else

            sSql = "SELECT DISTINCT isnull(i.has_sn,0) from QL_mstItem i INNER JOIN QL_mstItem_branch ib ON i.itemoid = ib.itemoid WHERE i.itemcode = '" & LabelItemCode.Text & "' and ib.branch_code = '" & fromBramch.SelectedValue & "'"
            Dim has_sn As String = ckon.ambilscalar(sSql)
            If has_sn = "1" Then
                pnlInvnSN.Visible = True : btnHideSN.Visible = True
                If Session("TblSN") Is Nothing Then
                    sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntrfoid = '" & transferno.Text & "' AND i.itemoid = '" & labelitemoid.Text & "' "
                    Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                    Dim objDs As New DataSet
                    mySqlDA.Fill(objDs, "dataSN")
                    GVSN.DataSource = Nothing
                    GVSN.DataSource = objDs.Tables("dataSN")
                    GVSN.DataBind()
                    Session("TblSN") = objDs.Tables("dataSN")
                Else
                    GVSN.DataSource = Nothing : GVSN.DataSource = Session("TblSN")
                    GVSN.DataBind()
                End If

                TextItem.Text = item.Text : KodeItem.Text = labelitemoid.Text
                MAXQTYSN.Text = qty.Text : ModalPopupSN.Show()
                TextSN.Text = "" : LabelPesanSn.Text = ""
                TextSN.Focus() : NewSN.Text = "New SN"
            Else
                If I_u2.Text = "new" Then
                    drow = dtab.NewRow()
                    drow("seq") = dtab.Rows.Count + 1
                Else
                    drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
                    drow = drowedit(0)
                    drowedit(0).BeginEdit()
                End If

                drow("itemoid") = Integer.Parse(labelitemoid.Text)
                drow("itemdesc") = item.Text.Trim
                drow("merk") = merk.Text
                drow("itemcode") = LabelItemCode.Text
                drow("qty") = Double.Parse(qty.Text)
                drow("satuan") = unit.SelectedValue
                drow("unit") = unit.SelectedItem.Text

                drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                drow("unit1") = labelunit1.Text
                drow("unit2") = labelunit2.Text
                drow("unit3") = labelunit3.Text

                drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                drow("note") = notedtl.Text.Trim
                drow("stockflag") = stockflag.Text
                If CbExp.Checked = True Then 'Berat Volume
                    drow("statusexp") = "YA"
                    If cb2.Checked = True Then
                        drow("typedimensi") = typeDimensi.Text
                        drow("beratvolume") = ToDouble(beratBarang.Text)
                        drow("beratbarang") = ToDouble(0)
                        drow("nettoexp") = ToDouble(AmtExpedisi.Text) * (ToDouble(beratBarang.Text) / 1000000) * ToDouble(qty.Text)
                    ElseIf cb1.Checked = True Then 'Berat Barang
                        drow("typedimensi") = 2
                        drow("beratvolume") = ToDouble(0)
                        drow("beratbarang") = ToDouble(beratBarang.Text)
                        drow("nettoexp") = ToDouble(beratBarang.Text) * ToDouble(qty.Text) * ToDouble(AmtExpedisi.Text)
                    End If
                    drow("jenisexp") = ddtype.SelectedValue.ToUpper
                    drow("amtexpedisi") = ToDouble(AmtExpedisi.Text)
                    'nettoexp
                Else
                    drow("statusexp") = "TIDAK"
                    drow("typedimensi") = ToDouble(0)
                    drow("beratvolume") = ToDouble(0)
                    drow("beratbarang") = ToDouble(0)
                    drow("jenisexp") = ""
                    drow("amtexpedisi") = ToDouble(0)
                    drow("nettoexp") = ToDouble(0)
                End If
                If I_u2.Text = "new" Then
                    dtab.Rows.Add(drow)
                Else
                    drowedit(0).EndEdit()
                End If

                dtab.Select(Nothing, Nothing)
                dtab.AcceptChanges()

                GVItemDetail.DataSource = dtab
                GVItemDetail.DataBind()
                Session("itemdetail") = dtab

                labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
                merk.Text = "" : qty.Text = "0"
                labelmaxqty.Text = "0" : unit.Items.Clear()
                notedtl.Text = "" : labelitemoid.Text = ""
                item.Text = "" : I_u2.Text = "new"

                labelsatuan1.Text = "" : labelsatuan2.Text = ""
                labelsatuan3.Text = "" : labelunit1.Text = ""
                labelunit2.Text = "" : labelunit3.Text = ""
                labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
                GVItemDetail.SelectedIndex = -1
                'GVItemDetail.Columns(10).Visible = True
            End If
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = ""
        qty.Text = "0" : item.Text = ""
        labelmaxqty.Text = "0"
        unit.Items.Clear()
        notedtl.Text = "" : merk.Text = ""

        CbExp.Enabled = True
        cb1.Enabled = True : cb2.Enabled = True

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemDetail.Visible = True Then
            GVItemDetail.DataSource = Nothing
            GVItemDetail.DataBind()
            GVItemDetail.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        'GVItemDetail.Columns(10).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable = Session("itemdetail")
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@

        If Not Session("TblSN") Is Nothing Then
            'Dim country As String = GVItemDetail.Rows(e.RowIndex).Cells(5).Text
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As Integer = Integer.Parse(gvr.Cells(2).Text)
            dvSN.RowFilter = "itemoid = " & itemid
            If Not dvSN.Count = 0 Then
                For kdsn As Integer = 0 To dvSN.Count - 1
                    objTableSN.Rows.RemoveAt(0)
                Next
            End If
            objTableSN.AcceptChanges()
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
        If Session("itemdetail") Is Nothing Then
            fromBramch.Enabled = True : fromBramch.CssClass = "inpText"
            fromlocationBranch.Enabled = True : fromlocationBranch.CssClass = "inpText"
            toBranch.Enabled = True : toBranch.CssClass = "inpText"
            tolocationBranch.Enabled = True : tolocationBranch.CssClass = "inpText"
        End If
    End Sub

    Protected Sub GVItemDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVItemDetail.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
                e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
                e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
                e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
                e.Row.Cells(13).Text = ToMaskEdit(ToDouble(e.Row.Cells(13).Text), 3)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Protected Sub GVItemDetail_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GVItemDetail.RowDeleting

        Dim iIndex As Int16 = e.RowIndex
        Dim dtab As DataTable = Session("itemdetail")
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If
        dtab.Rows.RemoveAt(iIndex)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()

        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@

        If Not Session("TblSN") Is Nothing Then
            Dim ITemCode As String = GVItemDetail.Rows(e.RowIndex).Cells(3).Text
            sSql = "SELECT itemoid from QL_mstItem WHERE itemcode = '" & ITemCode & "'"
            Dim XOidItem As Integer = ckon.ambilscalar(sSql)
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As Integer = XOidItem
            dvSN.RowFilter = "itemoid = " & itemid
            If Not dvSN.Count = 0 Then
                For kdsn As Integer = 0 To dvSN.Count - 1
                    objTableSN.Rows.RemoveAt(0)
                Next
            End If
            objTableSN.AcceptChanges()
        End If
        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
        If Session("itemdetail") Is Nothing Then
            fromBramch.Enabled = True : fromBramch.CssClass = "inpText"
            fromlocationBranch.Enabled = True : fromlocationBranch.CssClass = "inpText"
            toBranch.Enabled = True : toBranch.CssClass = "inpText"
            tolocationBranch.Enabled = True : tolocationBranch.CssClass = "inpText"
        End If
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        Dim dbn As DataTable = Session("itemdetail")
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        LabelItemCode.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text.Replace("&nbsp;", "")
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(8).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(5).Text.Replace("&nbsp;", "")

        unit.Items.Clear()
        unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")

        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            unit.Items(unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If

        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")
        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")
        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text
        stockflag.Text = GVItemDetail.SelectedDataKey("stockflag")
        ' Dim er As String = GVItemDetail.SelectedDataKey("jenisexp").ToString

        If GVItemDetail.SelectedDataKey("statusexp").ToString.ToUpper = "YA" Then
            CbExp.Checked = True : cb1.Visible = True
            cb2.Visible = True : beratBarang.Visible = True
            Label9.Visible = True : Label6.Visible = True
            Label11.Visible = True : Label12.Visible = True : ddtype.Visible = True

            ddtype.SelectedValue = GVItemDetail.SelectedDataKey("jenisexp").ToString
            AmtExpedisi.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("AmtExpedisi")), 3)
            typeDimensi.Text = GVItemDetail.SelectedDataKey("typeDimensi").ToString

            If typeDimensi.Text = 1 Then
                cb2.Checked = True : cb1.Checked = False : Label9.Text = "Berat Volume"
                beratBarang.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("beratvolume").ToString), 3)
            Else
                cb2.Checked = False : cb1.Checked = True : Label9.Text = "Berat Barang"
                beratBarang.Text = ToMaskEdit(ToDouble(GVItemDetail.SelectedDataKey("beratbarang").ToString), 3)
            End If
        Else
            beratBarang.Text = ToMaskEdit(ToDouble(0), 3)
            AmtExpedisi.Text = ToMaskEdit(ToDouble(0), 3) : typeDimensi.Text = 0
        End If

        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()

        Dim bc As String = "" : Dim Ml As String = ""
        If DDLTypeTW.SelectedValue = "Send TW" Then
            bc = fromBramch.SelectedValue : Ml = fromlocationBranch.SelectedValue
        Else
            bc = Session("BC") : Ml = Session("ML")
        End If

        sSql = "SELECT saldoakhir from QL_crdmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(labelitemoid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND cmpcode = '" & cmpcode & "' and branch_code = '" & bc & "' AND mtrlocoid = '" & Ml & "'"
        xCmd.CommandText = sSql
        Dim maxqty As Double = xCmd.ExecuteScalar
        Dim ExpSts As String = GVItemDetail.SelectedDataKey("statusexp").ToString.ToUpper


        'If Not maxqty = Nothing Then
        '    If GVItemDetail.SelectedDataKey("satuan") = GVItemDetail.SelectedDataKey("satuan1") Then
        '        maxqty = maxqty '/ GVItemDetail.SelectedDataKey("konversi2_3")
        '    End If
        '    labelmaxqty.Text = Format(maxqty, "#,##0.00")
        'Else
        labelmaxqty.Text = ToMaskEdit(ToDouble(maxqty), 3)
        'End If
        conn.Close()

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        'GVItemDetail.Columns(10).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""
        If trnstatus.Text = "Revised" Then
            trnstatus.Text = "In Process"
        End If

        If fromBramch.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If

        If toBranch.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong !"
        End If

        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong !"
        End If

        If fromlocationBranch.SelectedValue = "" Or tolocationBranch.SelectedValue = "" Then
            errmsg &= "Lokasi untuk cabang yang di pilih belum ada silahkan input terlebih dahulu"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(transdate).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                trnstatus.Text = "In Process"
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = " SELECT ISNULL(SUM(qtyin)-SUM(qtyOut),0.00) FROM QL_conmtr con Inner Join ql_mstitem i ON i.itemoid=con.refoid WHERE con.periodacctg = '" & period & "' AND con.branch_code = '" & fromBramch.SelectedValue & "' AND con.mtrlocoid = " & fromlocationBranch.SelectedValue & " AND refoid =" & dtab.Rows(j).Item("itemoid") & " AND con.cmpcode = '" & cmpcode & "' AND i.stockflag='" & dtab.Rows(j).Item("stockflag") & "' Group BY mtrlocoid,branch_code,refoid,periodacctg,i.stockflag"
                    xCmd.CommandText = sSql : saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Transfer barang tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir satuan std hanya tersedia 0.00..!!", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub 
                    End If
                Next
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
            Dim tempoid As Int32 = crdmtroid

            Dim trfmtrmstoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_trntrfmtrmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                trfmtrmstoid = xCmd.ExecuteScalar + 1
            End If

            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_trntrfmtrdtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql : Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1

            If trnstatus.Text = "In Approval" Then
                sSql = "select lastoid from ql_mstoid where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : Dim approvaloid As Integer = xCmd.ExecuteScalar

                sSql = "select tablename, approvaltype, approvallevel, approvaluser, approvalstatus from QL_approvalstructure where cmpcode = '" & cmpcode & "' And tablename = 'QL_trntrfmtrmst' and branch_Code like '%" & fromBramch.SelectedValue & "%' order by approvallevel"
                Dim dtab2 As DataTable = ckon.ambiltabel(sSql, "QL_approvalstructure")

                If dtab2.Rows.Count > 0 Then
                    For i As Integer = 0 To dtab2.Rows.Count - 1
                        approvaloid = approvaloid + 1
                        sSql = "insert into ql_approval (cmpcode, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus, branch_code) VALUES" & _
                        " ('" & cmpcode & "', " & approvaloid & ", '" & "TW" & Session("oid") & "_" & approvaloid & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 'New', 'QL_trntrfmtrmst', '" & Session("oid") & "', 'In Approval', '0', '" & dtab2.Rows(i).Item("approvaluser") & "', '1/1/1900', '" & dtab2.Rows(i).Item("approvaltype") & "', '1', '" & dtab2.Rows(i).Item("approvalstatus") & "', '" & fromBramch.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "update ql_mstoid set lastoid = " & approvaloid & " where tablename = 'ql_approval' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    objTrans.Rollback()
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                    showMessage("Maaf, User approval belum di setting, hubungi admin segera..!", 2)
                    trnstatus.Text = "In Process"
                    Exit Sub
                End If
            End If

            'Insert new record
            If i_u.Text = "new" Then
                transferno.Text = trfmtrmstoid.ToString
                'Insert to master
                sSql = "INSERT INTO QL_trntrfmtrmst (cmpcode, trfmtrmstoid, transferno, trfmtrdate, trfmtrnote, noref, status, fromMtrBranch, frommtrlocoid, toMtrBranch, tomtrlocoid, updtime, upduser, typeTransfer, revisenote) VALUES ('" & cmpcode & "', " & trfmtrmstoid & ", '" & transferno.Text & "', '" & transdate & "', '" & Tchar(note.Text.Trim) & "', '" & Tchar(noref.Text.Trim) & "', '" & trnstatus.Text & "', '" & fromBramch.SelectedValue & "', '" & fromlocationBranch.SelectedValue & "', '" & toBranch.SelectedValue & "', '" & tolocationBranch.SelectedValue & "', CURRENT_TIMESTAMP, '" & upduser.Text & "', 'TW','')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_trntrfmtrmst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'QL_trntrfmtrmst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "SELECT status FROM QL_trntrfmtrmst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                    Exit Sub
                Else
                    If srest = "In Approval" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer warehouse tidak dapat diubah !<br />Periksa bila data telah dalam status 'In Approval'!", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    ElseIf srest = "Approved" Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Data transfer warehouse tidak dapat diubah !<br />Periksa bila data telah dalam status 'Approved'", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    End If
                End If

                'Update master record
                sSql = "UPDATE QL_trntrfmtrmst SET transferno = '" & transferno.Text & "', trfmtrnote = '" & Tchar(note.Text.Trim) & "', fromMtrBranch = '" & fromBramch.SelectedValue & "', frommtrlocoid = " & fromlocationBranch.SelectedValue & ", toMtrBranch = '" & toBranch.SelectedValue & "', tomtrlocoid = " & tolocationBranch.SelectedValue & ", noref = '" & Tchar(noref.Text.Trim) & "', status = '" & trnstatus.Text & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "', revisenote = '' WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_trntrfmtrdtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            '@@@@@@@@@@@@@@@@@@@ update Serial Number '@@@@@@@@@@@@@@@@@@@
            'If Session("TblSN") Is Nothing Then
            '    sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid, s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntrfoid = '" & transferno.Text & "'"

            '    Session("TblSN") = ckon.ambiltabel(sSql, "TbSN")
            '    Dim objTableSN As DataTable
            '    objTableSN = Session("TblSN")

            '    For semula As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(semula).Item("itemoid") & "'"
            '        Dim itemcodesn As String = ckon.ambilscalar(sSql)

            '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = NULL, status_item = 'Post' where itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_Mst_SN set status_approval = 'No PROCESS', status_in_out = 'In',trntrfoid = NULL, tgltrf = NULL  where itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next

            '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
            '        Dim itemcodesn As String = ckon.ambilscalar(sSql)
            '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = CURRENT_TIMESTAMP, status_item = '" & trnstatus.Text & "' Where sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_Mst_SN set status_approval = '" & trnstatus.Text & "',trntrfoid = '" & transferno.Text & "', tgltrf = CURRENT_TIMESTAMP  where id_sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next
            'Else

            '    Dim objTableSN As DataTable
            '    objTableSN = Session("TblSN")
            '    For semula As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(semula).Item("itemoid") & "'"
            '        Dim itemcodesn As String = ckon.ambilscalar(sSql)

            '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = NULL, status_item = 'Post' Where itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_Mst_SN set status_approval = 'No PROCESS', status_in_out = 'In',trntrfoid = NULL, tgltrf = NULL  where itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next

            '    For SNID As Integer = 0 To objTableSN.Rows.Count - 1
            '        sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
            '        Dim itemcodesn As String = ckon.ambilscalar(sSql)

            '        sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = CURRENT_TIMESTAMP, status_item = '" & trnstatus.Text & "' Where sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            '        sSql = "update QL_Mst_SN set status_approval = '" & trnstatus.Text & "',trntrfoid = '" & transferno.Text & "', tgltrf = CURRENT_TIMESTAMP Where id_sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
            '        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            '    Next
            'End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0 : Dim qty_to As Double = 0.0

                For i As Integer = 0 To objTable.Rows.Count - 1
                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If
                    sSql = "INSERT INTO QL_trntrfmtrdtl (cmpcode, trfmtrdtloid, trfmtrmstoid, seq, refoid, refname, unitseq, qty, unitoid, trfdtlnote, unitseq_to, qty_to, unitoid_to, statusexp, typedimensi, beratvolume, beratbarang, jenisexp, amtexpedisi, nettoexp) VALUES " & _
                    "('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", '" & Tchar(objTable.Rows(i).Item("note")) & "', 3, " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ",'" & objTable.Rows(i).Item("statusexp") & "','" & objTable.Rows(i).Item("typedimensi") & "', " & ToDouble(objTable.Rows(i).Item("beratvolume")) & "," & ToDouble(objTable.Rows(i).Item("beratbarang")) & ",'" & objTable.Rows(i).Item("jenisexp") & "'," & ToDouble(objTable.Rows(i).Item("amtexpedisi")) & "," & ToDouble(objTable.Rows(i).Item("nettoexp")) & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    trfmtrdtloid += 1
                Next

                'Update lastoid QL_trntrfmtrdtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & "  WHERE tablename = 'QL_trntrfmtrdtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_crdmtr
                If tempoid <> crdmtroid Then
                    sSql = "UPDATE QL_mstoid SET lastoid =" & crdmtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & cmpcode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            objTrans.Commit()
            conn.Close()

        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            trnstatus.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("trnWHTransfer.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged
        If labelitemoid.Text <> "" Then
            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            conn.Open()
            sSql = "SELECT (a.saldoakhir) saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_crdmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(labelitemoid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & fromBramch.SelectedValue & " AND a.cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            Dim maxqty As Double = 0.0
            If xreader.HasRows Then
                While xreader.Read
                    If unit.SelectedValue = xreader("satuan2") Then
                        maxqty = xreader("saldoakhir") / xreader("konversi2_3")
                    ElseIf unit.SelectedValue = xreader("satuan1") Then
                        maxqty = xreader("saldoakhir") / xreader("konversi2_3")
                    End If
                End While
            End If
            labelmaxqty.Text = Format(maxqty, "#,##0.00")
            conn.Close()
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM QL_trntrfmtrmst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer warehouse tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "Approved" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Periksa bila data telah berstatus 'Approved'!", 2)
                    Exit Sub
                ElseIf srest = "In Approval" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Data sedang berada dalam status 'In Approval'!", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM ql_trntrfmtrmst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM ql_trntrfmtrdtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()


            If Session("TblSN") Is Nothing Then
                sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntrfoid = '" & transferno.Text & "'"
                Session("TblSN") = ckon.ambiltabel(sSql, "TbSN")
                Dim objTableSN As DataTable
                objTableSN = Session("TblSN")
                For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                    sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid").ToString.Trim & "'"
                    Dim itemcodesn As String = ckon.ambilscalar(sSql)
                    sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = NULL , status_in_out = 'In'  where sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update QL_Mst_SN set status_approval = 'No PROCESS  ',trntrfoid = NULL, tgltrf = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            Else
                Dim objTableSN As DataTable
                objTableSN = Session("TblSN")
                For SNID As Integer = 0 To objTableSN.Rows.Count - 1
                    sSql = "select itemcode from QL_mstItem WHERE itemoid = '" & objTableSN.Rows(SNID).Item("itemoid") & "'"
                    Dim itemcodesn As String = ckon.ambilscalar(sSql)
                    sSql = "update QL_mstitemDtl set createuser = '" & Session("UserID") & "', createtime = NULL, status_item = 'Post', status_in_out = 'In'  where sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update QL_Mst_SN set status_approval = 'No PROCESS  ',trntrfoid = NULL, tgltrf = NULL  where id_sn ='" & objTableSN.Rows(SNID).Item("SN").ToString.Trim & "' and itemcode = '" & itemcodesn.Trim & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If

            sSql = "delete from QL_approval where tablename = 'ql_trntrfmtrmst' and oid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnWHTransfer.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub toBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles toBranch.SelectedIndexChanged
        'InitDDLCabang()
        If fromlocationBranch.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        InitDDLCabang()
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim sWhere As String = " " & Integer.Parse(labeloid.Text) & " "
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "TWGprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("IDTW", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.PaperStatement
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            sSql = "SELECT transferno from QL_trntrfmtrmst WHERE trfmtrmstoid = '" & sWhere & "'"
            Dim NOTW As String = ckon.ambilscalar(sSql)
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & NOTW & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close()
            report.Dispose()
            Response.Redirect("trnWHTransfer.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try

    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("tw") Is Nothing Then
            Dim dtab As DataTable = Session("tw")
            gvMaster.DataSource = dtab
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApproval.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If fromlocationBranch.SelectedValue = "" Or tolocationBranch.SelectedValue = "" Then
            showMessage("Lokasi untuk cabang yang di pilih belum ada silahkan input terlebi dahulu", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        sSql = "SELECT count(*) from QL_approvalstructure WHERE tablename = 'QL_trntrfmtrmst' AND approvaltype = 'FINAL' AND Branch_code LIKE '%" & fromBramch.SelectedValue & "%'"
        Dim XApproval As Integer = ckon.ambilscalar(sSql)
        If XApproval = 0 Then
            showMessage("Approval User tidak ditemukan, silahkan hubungi administrator", 3)
            trnstatus.Text = "In Process"
            Exit Sub
        Else
            trnstatus.Text = "In Approval"
            btnSave_Click(sender, e)
        End If
    End Sub

    Protected Sub lkbCloseSN_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbCloseSN.Click
        Dim Objdtl As DataTable = Session("itemdetail")
        '@@@@@@@@@@@@@@@@ Hapus SN Berdasrkan Item @@@@@@@@@@@@@@@@@@@@@@@@@
        If Not Session("TblSN") Is Nothing Then
            Dim objTableSN As DataTable = Session("TblSN")
            Dim dvSN As DataView = objTableSN.DefaultView
            Dim itemid As String = labelitemoid.Text
            dvSN.RowFilter = "itemoid = " & itemid

            Dim dvdtl As DataView = Objdtl.DefaultView
            dvdtl.RowFilter = "itemoid = " & itemid
            If dvdtl.Count = 0 Then
                If Not dvSN.Count = 0 Then
                    For kdsn As Integer = 0 To dvSN.Count - 1
                        objTableSN.Rows.RemoveAt(0)
                    Next
                End If
                objTableSN.AcceptChanges()
            Else
                sSql = "SELECT row_number() OVER(ORDER BY s.id_sn) AS 'SNseq', (SELECT itemoid FROM QL_mstItem WHERE itemcode = s.itemcode) itemoid,  s.id_sn SN from QL_Mst_SN s INNER JOIN QL_mstItem i ON i.itemcode = s.itemcode WHERE s.trntrfoid = '" & transferno.Text & "'"
                Dim mySqlDA As New SqlDataAdapter(sSql, conn)
                Dim objDs As New DataSet
                mySqlDA.Fill(objDs, "dataSN")
                Session("TblSN") = objDs.Tables("dataSN")
            End If
        End If

        '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        item.Text = "" : labelitemoid.Text = ""
        qty.Text = "0" : labelmaxqty.Text = "0"
        merk.Text = "" : notedtl.Text = ""
        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(10).Visible = True
        If GVItemList.Visible = True Then
            GVItemList.Visible = False : GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub lkbPilihItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbPilihItem.Click
        Dim drow As DataRow
        Dim dtab As DataTable
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            If Not Session("TblSN") Is Nothing Then
                Dim objTableSN As DataTable
                objTableSN = Session("TblSN")
                Dim dv As DataView = objTableSN.DefaultView
                'Cek apa sudah ada item yang sama dalam Tabel Detail

                If NewSN.Text = "New SN" Then
                    dv.RowFilter = "itemoid= '" & KodeItem.Text & "'"
                Else
                    dv.RowFilter = "itemoid= '" & KodeItem.Text & "'"
                End If

                If dv.Count <> MAXQTYSN.Text Then
                    LabelPesanSn.Text = "MAX Qty SN " & MAXQTYSN.Text
                    dv.RowFilter = "" : ModalPopupSN.Show()
                    TextSN.Text = "" : CProc.SetFocusToControl(Me.Page, TextSN)
                    LabelPesanSn.Text = "Jumlah Serial Number Tidak Sesuai dengan Qty"
                    Exit Sub
                End If

                If Session("itemdetail") Is Nothing Then
                    dtab = New DataTable
                    dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                    dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                    dtab.Columns.Add("itemcode", Type.GetType("System.String"))
                    dtab.Columns.Add("merk", Type.GetType("System.String"))
                    dtab.Columns.Add("qty", Type.GetType("System.Double"))
                    dtab.Columns.Add("sisa", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                    dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                    dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                    dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                    dtab.Columns.Add("unit", Type.GetType("System.String"))
                    dtab.Columns.Add("unit1", Type.GetType("System.String"))
                    dtab.Columns.Add("unit2", Type.GetType("System.String"))
                    dtab.Columns.Add("unit3", Type.GetType("System.String"))
                    dtab.Columns.Add("note", Type.GetType("System.String"))
                    Session("itemdetail") = dtab
                    labelseq.Text = "1"
                Else
                    dtab = Session("itemdetail")
                    labelseq.Text = (dtab.Rows.Count + 1).ToString
                End If
                drow = dtab.NewRow
                drow("seq") = Integer.Parse(labelseq.Text)
                drow("itemoid") = Integer.Parse(labelitemoid.Text)
                drow("itemdesc") = item.Text.Trim
                drow("itemcode") = LabelItemCode.Text
                drow("merk") = merk.Text
                drow("qty") = Double.Parse(qty.Text)
                drow("satuan") = unit.SelectedValue
                drow("unit") = unit.SelectedItem.Text

                drow("satuan1") = Integer.Parse(labelsatuan1.Text)
                drow("satuan2") = Integer.Parse(labelsatuan2.Text)
                drow("satuan3") = Integer.Parse(labelsatuan3.Text)
                drow("unit1") = labelunit1.Text
                drow("unit2") = labelunit2.Text
                drow("unit3") = labelunit3.Text

                drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
                drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
                drow("note") = notedtl.Text.Trim

                dtab.Rows.Add(drow)
                dtab.AcceptChanges()
                GVItemDetail.DataSource = dtab
                GVItemDetail.DataBind()
                Session("itemdetail") = dtab

                labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
                merk.Text = "" : qty.Text = "0"
                labelmaxqty.Text = "0" : unit.Items.Clear()
                notedtl.Text = "" : labelitemoid.Text = ""
                item.Text = "" : I_u2.Text = "new"

                labelsatuan1.Text = "" : labelsatuan2.Text = ""
                labelsatuan3.Text = "" : labelunit1.Text = ""
                labelunit2.Text = "" : labelunit3.Text = ""
                labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
                GVItemDetail.SelectedIndex = -1
                GVItemDetail.Columns(10).Visible = True
            Else
                LabelPesanSn.Text = "Mohon Masukan Kode Serial Number"
                ModalPopupSN.Show() : TextSN.Text = ""
                CProc.SetFocusToControl(Me.Page, TextSN)
            End If
        Else
            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView
            'Cek apa sudah ada item yang sama dalam Tabel Detail
            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            Else
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "'"
            End If

            If dv.Count <> MAXQTYSN.Text Then
                LabelPesanSn.Text = "MAX Qty SN " & MAXQTYSN.Text
                dv.RowFilter = "" : ModalPopupSN.Show()
                TextSN.Text = "" : CProc.SetFocusToControl(Me.Page, TextSN)
                LabelPesanSn.Text = "Jumlah Serial Number Tidak Sesuai dengan Qty"
                Exit Sub
            End If

            dtab = Session("itemdetail")
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()

            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text.Trim
            drow("merk") = merk.Text
            drow("itemcode") = LabelItemCode.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = unit.SelectedValue
            drow("unit") = unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()

            GVItemDetail.DataSource = dtab
            GVItemDetail.DataBind()
            Session("itemdetail") = dtab

            labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
            merk.Text = "" : qty.Text = "0"
            labelmaxqty.Text = "0" : unit.Items.Clear()
            notedtl.Text = "" : labelitemoid.Text = ""
            item.Text = "" : I_u2.Text = "new"

            labelsatuan1.Text = "" : labelsatuan2.Text = ""
            labelsatuan3.Text = "" : labelunit1.Text = ""
            labelunit2.Text = "" : labelunit3.Text = ""
            labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
            GVItemDetail.SelectedIndex = -1
            GVItemDetail.Columns(10).Visible = True
        End If

    End Sub

    Private Function setTabelSN() As DataTable
        Dim dtSN As New DataTable
        dtSN.Columns.Add("SNseq", Type.GetType("System.Int32"))
        dtSN.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtSN.Columns.Add("SN", Type.GetType("System.String"))
        Return dtSN
    End Function

    Protected Sub GVSN_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GVSN.RowDeleting
        Dim iIndex As Int16 = e.RowIndex
        Dim objTableSN As DataTable = Session("TblSN")
        objTableSN.Rows.RemoveAt(iIndex)
        'resequence sjDtlsequence 
        For C1 As Int16 = 0 To objTableSN.Rows.Count - 1
            Dim dr As DataRow = objTableSN.Rows(C1)
            dr.BeginEdit()
            dr("SNseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblSN") = objTableSN
        GVSN.Visible = True : GVSN.DataSource = objTableSN
        GVSN.DataBind() : ModalPopupSN.Show()
        TextSN.Text = "" : CProc.SetFocusToControl(Me.Page, TextSN)
    End Sub

    Protected Sub EnterSN_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles EnterSN.Click
        TextSN.Focus() : LabelPesanSn.Text = ""
        sSql = "select COUNT(a.sn) FROM QL_mstitemDtl a WHERE a.last_trans_type not in( 'Jual') AND a.sn = '" & TextSN.Text.Trim & "' and a.itemoid = " & labelitemoid.Text.Trim & " AND branch_code = '" & fromBramch.SelectedValue & "' AND status_item <> 'In Process' AND locoid = " & fromlocationBranch.SelectedValue & " and status_in_out = 'In'"
        Dim SN As Object = ckon.ambilscalar(sSql)
        If Not SN = 0 Then
            If Session("TblSN") Is Nothing Then
                Dim dtlTableSN As DataTable = setTabelSN()
                Session("TblSN") = dtlTableSN
            End If
            Dim objTableSN As DataTable
            objTableSN = Session("TblSN")
            Dim dv As DataView = objTableSN.DefaultView
            If dv.Count = 0 Then
                NewSN.Text = "New SN"
            End If
            'Cek apa sudah ada item yang sama dalam Tabel Detail
            If NewSN.Text = "New SN" Then
                dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "'"
            Else
                If Not Session("TblSN") Is Nothing Then
                    dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "' AND SNseq <> '" & SNseq.Text & "'"
                Else
                    dv.RowFilter = "itemoid= '" & KodeItem.Text & "' and SN = '" & TextSN.Text & "'"
                End If
            End If
            If dv.Count > MAXQTYSN.Text Then
                LabelPesanSn.Text = "MAX Qty SN " & MAXQTYSN.Text
                dv.RowFilter = ""
                ModalPopupSN.Show()
                TextSN.Text = ""
                CProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If
            If dv.Count > 0 Then
                LabelPesanSn.Text = "Serial Number Sudah Entry"
                dv.RowFilter = ""
                ModalPopupSN.Show()
                TextSN.Text = ""
                CProc.SetFocusToControl(Me.Page, TextSN)
                Exit Sub
            End If
            dv.RowFilter = ""
            'insert/update to list data
            Dim objRow As DataRow
            If NewSN.Text = "New SN" Then
                objRow = objTableSN.NewRow()
                objRow("SNseq") = objTableSN.Rows.Count + 1
            Else
                Dim selrow As DataRow() = objTableSN.Select("SNseq=" & SNseq.Text)
                objRow = selrow(0)
                objRow.BeginEdit()
            End If
            objRow("itemoid") = KodeItem.Text
            objRow("SN") = TextSN.Text
            If NewSN.Text = "New SN" Then
                objTableSN.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblSN") = objTableSN
            GVSN.Visible = True : GVSN.DataSource = Nothing
            GVSN.DataSource = objTableSN
            GVSN.DataBind() : LabelPesanSn.Text = ""
            SNseq.Text = objTableSN.Rows.Count + 1 : GVSN.SelectedIndex = -1
            TextSN.Focus()
        Else
            LabelPesanSn.Text = "Serial Number Tidak Dikenali"
            CProc.SetFocusToControl(Me.Page, TextSN)
        End If

        CProc.SetFocusToControl(Me.Page, TextSN)
        ModalPopupSN.Show() : TextSN.Text = ""
    End Sub

    Protected Sub DDLTypeTW_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DDLTypeTW.SelectedIndexChanged
        'If Page.IsPostBack Then
            'If DDLTypeTW.SelectedValue = "Send TW" Then
            '    If Session("LevelUser") <> 1 Or Session("UserID").ToString.ToUpper <> "ADMIN" Or Session("branch_id") <> "10" Then
            '        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & Session("branch_id") & "'"
            '    Else
            '        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
            '    End If
            '    FillDDL(dd_branch, sSql)
            'Else
            '    sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' "
            '    FillDDL(dd_branch, sSql)
            '    dd_branch.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            '    dd_branch.SelectedValue = "SEMUA BRANCH"
            'End If

            'If DDLTypeTW.SelectedValue = "Send TW" Then
            '    sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
            '    FillDDL(dd_branchTO, sSql)
            '    dd_branchTO.Items.Add(New ListItem("SEMUA BRANCH", "SEMUA BRANCH"))
            '    dd_branchTO.SelectedValue = "SEMUA BRANCH"
            'Else
            '    If Session("LevelUser") <> 1 Or Session("UserID").ToString.ToUpper <> "ADMIN" Or Session("branch_id") <> "10" Then
            '        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang' and gencode = '" & Session("branch_id") & "'"
            '    Else
            '        sSql = "select gencode, gendesc from QL_mstgen WHERE gengroup = 'cabang'"
            '    End If
            '    FillDDL(dd_branchTO, sSql)
            'End If
        'End If
        If DDLTypeTW.SelectedValue = "Send TW" Then
            ToCab.Text = "To Branch"
            FromBranch.text = "From Branch"
        Else
            ToCab.Text = "From Branch"
            FromBranch.text = "To Branch"
        End If
        ddlCabang()
    End Sub

    Protected Sub fromlocationBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles fromlocationBranch.SelectedIndexChanged
        'If Page.IsPostBack Then
        '    InitDDLCabang()
        'End If
    End Sub

    Protected Sub cb2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb2.CheckedChanged
        If cb2.Checked = True Then
            'beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            'beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            cb1.Checked = False : beratBarang.Text = ToMaskEdit(0, 3)
            Label9.Text = "Berat Volume" : typeDimensi.Text = 1
        ElseIf cb2.Checked = False Then
            '    beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            '    beratVolume.Enabled = False : beratVolume.CssClass = "inpTextDisabled"
            'beratVolume.Text = ToMaskEdit(0, 3)
            cb1.Checked = True : Label9.Text = "Berat Barang"
            typeDimensi.Text = 2
        End If
    End Sub

    Protected Sub cb1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb1.CheckedChanged
        If cb1.Checked = True Then
            'beratBarang.Enabled = True : beratBarang.CssClass = "inpText"
            'beratVolume.Enabled = False ': beratVolume.CssClass = "inpTextDisabled"
            'beratVolume.Text = ToMaskEdit(0, 3)
            cb2.Checked = False : Label9.Text = "Berat Barang"
            typeDimensi.Text = 2
        ElseIf cb1.Checked = False Then
            '    beratVolume.Enabled = True : beratVolume.CssClass = "inpText"
            '    beratBarang.Enabled = False : beratBarang.CssClass = "inpTextDisabled"
            beratBarang.Text = ToMaskEdit(0, 3)
            cb2.Checked = True : Label9.Text = "Berat Volume"
            typeDimensi.Text = 1
        End If
    End Sub

    Protected Sub CbExp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbExp.CheckedChanged
         
        If CbExp.Checked = True Then
            beratBarang.Visible = True ': beratVolume.Visible = True
            cb1.Visible = True : cb2.Visible = True
            cb1_CheckedChanged(Nothing, Nothing)
            cb2_CheckedChanged(Nothing, Nothing)
            Label6.Visible = True ': Label4.Visible = True
            LblBiaya.Visible = True : Label7.Visible = True
            AmtExpedisi.Visible = True : Label9.Visible = True
            'Label8.Visible = True
            Label11.Visible = True : Label12.Visible = True
            ddtype.Visible = True
        Else
            beratBarang.Visible = False ': beratVolume.Visible = False
            cb1.Visible = False : cb2.Visible = False
            'centimeter.Visible = False
            Label6.Visible = False ': Label4.Visible = False
            LblBiaya.Visible = False : Label7.Visible = False
            AmtExpedisi.Visible = False : Label9.Visible = False
            'Label8.Visible = False
            Label11.Visible = False : Label12.Visible = False
            ddtype.Visible = False : typeDimensi.Text = 0
        End If
    End Sub

    Protected Sub fromBramch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromBramch.TextChanged
        If fromlocationBranch.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 2)
            Exit Sub
        End If

        Session("itemdetail") = Nothing
        GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new" : labelitemoid.Text = ""
        qty.Text = "0" : item.Text = ""
        labelmaxqty.Text = "0" : unit.Items.Clear()
        notedtl.Text = "" : merk.Text = ""
        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
        labelseq.Text = 1 : stockflag.Text = ""

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        InitDDLLocation()
    End Sub
#End Region
End Class
