<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="reportKartuHutang.aspx.vb" Inherits="ReportForm_reportKartuHutang" Title="" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Kartu Hutang"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-color: #ffffff" valign="center">
                                                    <asp:UpdatePanel ID="upReportForm" runat="server">
                                                        <ContentTemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" __designer:wfdid="w1" DefaultButton="btnSearchSupplier"><TABLE><TBODY><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Type" __designer:wfdid="w2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" __designer:wfdid="w3" AutoPostBack="True"><asp:ListItem>SUMMARY</asp:ListItem>
<asp:ListItem Value="DETAIL">KARTU HUTANG</asp:ListItem>
<asp:ListItem Value="DETAILNYA">DETAIL HUTANG</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="tdPeriod" align=left><asp:Label id="Label2" runat="server" Text="Period" __designer:wfdid="w4"></asp:Label></TD><TD id="tdPeriod2" class="Label" align=center><asp:Label id="Label8" runat="server" Text=":" __designer:wfdid="w5"></asp:Label></TD><TD id="tdPeriod3" align=left><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w6" Wrap="False"></asp:TextBox>&nbsp;<asp:ImageButton id="imgCal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="-" __designer:wfdid="w8"></asp:Label>&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w9" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imgCal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w11">(dd/mm/yyyy)</asp:Label></TD></TR><TR><TD align=left>Supplier<asp:Label id="custoid" runat="server" __designer:wfdid="w12" Visible="False"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" __designer:wfdid="w13" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Selected="True">ALL</asp:ListItem>
                                                                                                    <asp:ListItem>SELECT</asp:ListItem>
                                                                                                </asp:RadioButtonList></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="175px" CssClass="inpText" __designer:wfdid="w14" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w15" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w16" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Customer with 0 A/P amount" __designer:wfdid="w17" Visible="False" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left><asp:Panel id="pnlSupplier" runat="server" __designer:wfdid="w18"><asp:GridView id="gvSupplier" runat="server" Width="314px" ForeColor="#333333" __designer:wfdid="w19" Visible="False" PageSize="7" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><EditItemTemplate>
                                                                                                    <asp:CheckBox ID="CheckBox1" runat="server" __designer:wfdid="w3"></asp:CheckBox>
                                                                                                
</EditItemTemplate>
<ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkSelect" runat="server" ToolTip='<%# Eval("custoid") %>' Checked='<%# Eval("selected") %>' __designer:wfdid="w2"></asp:CheckBox>
                                                                                                
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label>
                                                                                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD id="TD7" align=left runat="server" Visible="false"><asp:Label id="Label1" runat="server" Text="Sort By" __designer:wfdid="w20"></asp:Label></TD><TD id="TD8" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD9" align=left runat="server" Visible="false"><asp:DropDownList id="DDLFilter" runat="server" CssClass="inpText" __designer:wfdid="w21" Visible="False"><asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="DDLFilterSum" runat="server" CssClass="inpText" __designer:wfdid="w22"><asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:DropDownList id="DDLFilter2" runat="server" CssClass="inpText" __designer:wfdid="w23"><asp:ListItem Value="Asc">Ascending</asp:ListItem>
<asp:ListItem Value="Desc">Descending</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD3" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="11px" Text="Currency" __designer:wfdid="w24"></asp:Label></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD2" align=left runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w25"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w26"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w27"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w28"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 120px" vAlign=top align=center colSpan=3><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w30" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w31"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CEdateAwal" runat="server" __designer:wfdid="w32" PopupButtonID="imgCal1" Format="dd/MM/yyyy" TargetControlID="dateAwal"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CEdateAkhir" runat="server" __designer:wfdid="w33" PopupButtonID="imgCal2" Format="dd/MM/yyyy" TargetControlID="dateAkhir"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w34" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w35" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> <CR:CrystalReportViewer id="crvReportForm" runat="server" __designer:dtid="1125899906842650" __designer:wfdid="w36" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasExportButton="False" HasPrintButton="False" HasCrystalLogo="False" HasViewList="False" AutoDataBind="True"></CR:CrystalReportViewer> <asp:UpdatePanel id="upPopUpMsg" runat="server" __designer:dtid="281474976710666" __designer:wfdid="w37"><ContentTemplate __designer:dtid="281474976710667">
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w38" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w39"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w40"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w43" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w44" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                                                        <Triggers>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</Triggers>
                                                    </asp:UpdatePanel>
                </th>
        </tr>
    </table>
</asp:Content>

