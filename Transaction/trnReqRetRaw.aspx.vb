Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_RawMaterialRequestReturn
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim rpt As ReportDocument
    'Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 tidak valid/benar. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 tidak valid/benar. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Tanggal pada Period 2 harus melebihi dari tanggal pada Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If matoid.Text = "" Then
            sError &= "- Mohon untuk memilih MATERIAL field!<BR>"
        End If
        If reqqtybiji.Text = "" Then
            sError &= "- Please fill Return QTY field!<BR>"
        Else
            If ToDouble(reqqtybiji.Text) <= 0 Then
                sError &= "- Return QTY field must be more than 0!<BR>"
                'Else
                '    Dim sErrReply As String = ""
                'If Not isLengthAccepted("reqrawqtybiji", "QL_trnreqrawdtl", ToDouble(reqqty.Text), sErrReply) Then
                '    sError &= "- QTY field harus kurang dari MAX QTY (" & sErrReply & ") yang disimpan di database!<BR>"
                'Else
                '    If ToDouble(reqqtybiji.Text) > ToDouble(reqmaxqtybiji.Text) Then
                '        If reftype.Text = "Detail 3" Then
                '            Dim dTol As Double = (GetQtyKIK(refoid.Text) * GetTolerance()) / 100
                '            If ToDouble(reqqtybiji.Text) > ToDouble(reqmaxqtybiji.Text) + dTol Then
                '                sError &= "- QTY field harus kurang dari MAX QTY + TOLERANCE (" & ToMaskEdit(dTol, 4) & ")!<BR>"
                '            End If
                '        Else
                '            sError &= "- QTY field harus kurang dari MAX QTY!<BR>"
                '        End If
                '    Else
                'If Not IsQtyRounded(ToDouble(reqqty.Text), ToDouble(reqlimitqty.Text)) Then
                '    sError &= "- QTY field must be rounded by ROUNDING QTY!<BR>"
                '        'End If
                '    End If
                'End If
            End If
            If ToDouble(reqqtybiji.Text) > ToDouble(reqmaxqtybiji.Text) Then
                sError &= "- Return QTY field harus kurang dari MAX QTY Return!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Mohon untuk memilih BUSINESS UNIT field!<BR>"
        End If

        If reqdate.Text = "" Then
            sError &= "- Mohon untuk mengisi RETURN DATE field!<BR>"
        Else
            If Not IsValidDate(reqdate.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- RETURN DATE tidak valid! " & sErr & "<BR>"
            End If
        End If

        If Session("TblDtl") Is Nothing Then
            sError &= "- Mohon untuk mengisi Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Mohon untuk mengisi Detail Data!<BR>"
            Else
                Dim dvTbl As DataView = dtTbl.DefaultView
                dvTbl.RowFilter = ""
                If dvTbl.Count > 0 Then
                    For C1 As Integer = 0 To dvTbl.Count - 1
                        If ToDouble(dvTbl(C1)("retqty").ToString) > ToDouble(dvTbl(C1)("reqqty").ToString) Then
                            sError &= "- Mohon untuk mengecek bahwa setiap RETURN QTY harus kurang dari MAX QTY!<BR>"
                        End If
                    Next
                End If
                dvTbl.RowFilter = ""
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            reqmststatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTolerance() As Double
        GetTolerance = ToDouble(GetStrData("SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='REQUEST KIK TOLERANCE (%)' ORDER BY updtime DESC"))
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetQtyKIK(ByVal sOid As String) As Double
        GetQtyKIK = ToDouble(GetStrData("SELECT wodtl4qty FROM QL_trnwodtl4 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND wodtl4oid=" & sOid & ""))
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "dtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("note") = GetTextBoxValue(row.Cells(7).Controls)
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "dtloid=" & cbOid
                                dtView2.RowFilter = "dtloid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                    dtView(0)("note") = GetTextBoxValue(row.Cells(7).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("retqty") = ToDouble(GetTextBoxValue(row.Cells(4).Controls))
                                        dtView2(0)("note") = GetTextBoxValue(row.Cells(7).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckRequestStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnreqrawmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND reqrawmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Transaction\trnReqRetRaw.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbReqInProcess.Visible = True
            lkbReqInProcess.Text = "Anda memiliki " & GetStrData(sSql) & " In Process Raw Material Request data yang tidak diproses lebih dari " & nDays.ToString & " hari. Klik pada link ini untuk melihatnya !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT 'MSC', 'MULTI SARANA COMPUTER' "
        FillDDL(DDLBusUnit, sSql)

        ' Fill DDL Unit
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND (gengroup='ITEMUNIT' OR gengroup='ITEMUNIT')"
        FillDDL(requnitoid, sSql) : FillDDL(requnitbijioid, sSql)
        FillDDL(DDLunit1, sSql) : FillDDL(DDLunitRet, sSql)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT '" & CompnyCode & "' divname, reqm.retrawmstoid, reqm.retrawmstoid reqrawmstoid, reqm.retrawno reqrawno, CONVERT(VARCHAR(10), reqm.retrawdate, 103) reqrawdate, wom.wono, reqm.retrawmststatus reqrawmststatus, reqm.retrawmstnote reqrawmstnote, 'False' AS checkvalue FROM QL_trnreqrawretmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid AND wom.branch_code=reqm.branch_code WHERE reqm.cmpcode='" & CompnyCode & "'"

        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, reqm.retrawdate) DESC, reqm.retrawmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnreqrawmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "retrawmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("reqrawmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath("~/Report/rptReqRawRet.rpt"))
            Dim sWhere As String = " WHERE"
            sWhere &= " reqm.cmpcode='" & CompnyCode & "'"
            If sOid = "" Then
                sWhere = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND reqm.retrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.retrawdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " And reqm.retrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Transaction\trnReqRetRaw.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND reqm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND reqm.retrawmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 username FROM QL_mstprof WHERE userid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "RequestReturnPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Transaction\trnReqRetRaw.aspx?awal=true")
    End Sub

    Private Sub BindListKIK()
        sSql = "SELECT DISTINCT wom.womstoid,wom.branch_code,wom.woflag,wom.tobranch_code AS ToBranch,wono, CONVERT(VARCHAR(10), wodate, 101) AS wodate FROM QL_trnwomst wom INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wom.cmpcode AND wod3.womstoid=wom.womstoid WHERE wom.cmpcode='" & DDLBusUnit.SelectedValue & "' AND womststatus='Approved' AND " & FilterDDLListKIK.SelectedValue & " LIKE '%" & Tchar(FilterTextListKIK.Text) & "%' AND wom.woflag='RK' AND wom.tobranch_code='" & Session("branch_id") & "' ORDER BY wono"
        FillGV(gvListKIK, sSql, "QL_trnwomst")
    End Sub

    Private Sub InitDDLDept()
        ' Fill DDL Department
        Dim sAdd As String = ""
        If i_u.Text = "Update Data" Then
            sAdd = " OR genoid IN (SELECT deptoid FROM QL_trnreqrawmst WHERE reqrawmstoid=" & reqmstoid.Text & " AND womstoid=" & womstoid.Text & ")"
        End If
        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND (genoid IN (SELECT deptoid FROM QL_trnwodtl2 wod2 INNER JOIN QL_trnwodtl3 wod3 ON wod3.cmpcode=wod2.cmpcode AND wod3.wodtl2oid=wod2.wodtl2oid WHERE wod2.cmpcode='" & DDLBusUnit.SelectedValue & "' AND wod2.womstoid=" & womstoid.Text & " AND wodtl3reqflag='' )" & sAdd & ") ORDER BY genoid"
        FillDDL(deptoid, sSql)
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        reqdtlseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                reqdtlseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        matoid.Text = "" : matcode.Text = "" : matcodedtl1.Text = ""
        matlongdesc.Text = "" : refoid.Text = "" : reftype.Text = ""
        reqqty.Text = "" : reqqtybiji.Text = ""
        reqmaxqtybiji.Text = "" : requnitoid.SelectedIndex = -1
        reqdtlnote.Text = "" : ReqMaxWeight.Text = "" : gvReqDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        'deptoid.Enabled = bVal : deptoid.CssClass = sCss
        btnSearchKIK.Visible = bVal : btnClearKIK.Visible = bVal
    End Sub

    Private Sub BindMaterial()
        sSql = "select * from( select distinct 'False' AS checkvalue, reqd.reqrawdtloid AS dtloid, reqd.reqrawmstoid AS mstoid, reqm.reqrawno AS reqno, 0 AS retqty, reqd.reqrawqty - ISNULL((SELECT SUM(rd.retrawqty) FROM QL_trnreqrawretdtl rd WHERE rd.cmpcode=reqd.cmpcode AND rd.reqdtloid=reqd.reqrawdtloid AND rd.reqmstoid=reqd.reqrawmstoid AND rd.branch_code=reqd.branch_code),0) AS reqqty, i.itemoid, i.itemcode, i.itemdesc, g.genoid AS unitoid, g.gendesc AS unitdesc, '' AS note, reqm.tomtrlocoid whoid, reqm.reqrawwhoid towhoid from QL_trnreqrawdtl reqd inner join QL_trnreqrawmst reqm ON reqd.reqrawmstoid=reqm.reqrawmstoid inner join QL_mstitem i ON i.itemoid=reqd.itemoid inner join QL_mstgen g ON g.genoid=i.satuan1 WHERE reqm.womstoid=" & womstoid.Text & " AND reqm.reqrawmststatus='Post') AS mat where mat.reqqty>0"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstmatraw")
        If dtTbl.Rows.Count > 0 Then
            Session("TblMat") = dtTbl
            Session("TblMatView") = dtTbl
            gvListMat2.DataSource = Session("TblMatView")
            gvListMat2.DataBind()
            cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
        Else
            showMessage("Material data can't be found!", 2)
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TabelReqRawDetail")
        dtlTable.Columns.Add("retdtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reqdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("reqmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("retwhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("rettowhoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reqqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("retunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("retunit", Type.GetType("System.String"))
        dtlTable.Columns.Add("retnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT DISTINCT reqm.cmpcode, reqm.retrawmstoid, reqm.periodacctg, Convert(VARCHAR(10),reqm.retrawdate,103) AS retrawdate, reqm.retrawno, reqm.retrawflag, reqm.womstoid, wom.wodate, wom.wono, reqm.retrawmstnote, reqm.retrawmststatus, reqm.createuser, reqm.createtime, reqm.upduser, reqm.updtime, reqm.mtrlocoid, reqm.tomtrlocoid, reqm.branch_code, reqm.tobranch_code FROM QL_trnreqrawretmst reqm INNER JOIN QL_trnwomst wom ON wom.cmpcode=reqm.cmpcode AND wom.womstoid=reqm.womstoid AND wom.branch_code=reqm.branch_code WHERE reqm.retrawmstoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                retmstoid.Text = Trim(xreader("retrawmstoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                reqdate.Text = Format(CDate(toDate(xreader("retrawdate"))), "dd/MM/yyyy")
                reqno.Text = Trim(xreader("retrawno").ToString)
                womstoid.Text = Trim(xreader("womstoid").ToString)
                BranchCode.Text = Trim(xreader("branch_code").ToString)
                ToBranch.Text = Trim(xreader("tobranch_code").ToString)
                wono.Text = Trim(xreader("wono").ToString)
                flagrequest.Text = Trim(xreader("retrawflag").ToString)
                reqmstnote.Text = Trim(xreader("retrawmstnote").ToString)
                reqmststatus.Text = Trim(xreader("retrawmststatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False
        End Try
        If reqmststatus.Text = "Post" Or reqmststatus.Text = "Closed" Or reqmststatus.Text = "Cancel" Then
            btnSave.Visible = False : btnDelete.Visible = False
            btnPost.Visible = False : btnAddToList.Visible = False
            gvReqDtl.Columns(0).Visible = False
            gvReqDtl.Columns(gvReqDtl.Columns.Count - 1).Visible = False
            lblsprno.Text = "Request Return No." : reqmstoid.Visible = False
            retmstoid.Visible = False : reqno.Visible = True
        Else
            retmstoid.Visible = True : btnPost.Visible = True
        End If
        sSql = "SELECT reqd.retrawdtlseq AS retdtlseq, reqd.reqdtloid AS reqdtloid, reqd.reqmstoid AS reqmstoid, reqm.retrawno AS retno, reqd.retrawqty AS retqty, ISNULL((SELECT rd.reqrawqty FROM QL_trnreqrawdtl rd WHERE rd.cmpcode=reqd.cmpcode AND rd.reqrawdtloid=reqd.reqdtloid),0) AS reqqty, i.itemoid AS matoid, i.itemcode AS matcode, i.itemdesc AS matlongdesc, g.genoid AS retunitoid, g.gendesc AS retunit, reqd.retrawdtlnote AS retnote, reqm.mtrlocoid retwhoid, reqm.tomtrlocoid rettowhoid from QL_trnreqrawretdtl reqd inner join QL_trnreqrawretmst reqm ON reqd.retrawmstoid=reqm.retrawmstoid inner join QL_mstitem i ON i.itemoid=reqd.itemoid inner join QL_mstgen g ON g.genoid=i.satuan1 WHERE reqm.retrawmstoid=" & sOid & ""

        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnreqrawdtl")
        Session("TblDtl") = dt
        gvReqDtl.DataSource = Session("TblDtl")
        gvReqDtl.DataBind()
        ClearDetail()
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnReqRetRaw.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Raw Material Request Return"

        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda yakin untuk MENGHAPUS data ini?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Apakah Anda yakin untuk POST data ini?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckRequestStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                retmstoid.Text = GenerateID("QL_TRNREQRAWRETMST", CompnyCode)
                reqdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                reqmststatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvReqDtl.DataSource = dt
            gvReqDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Transaction\trnReqRetRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbReqInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReqInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, reqm.updtime, GETDATE()) > " & nDays & " AND reqrawmststatus='In Process' "
        If checkPagePermission("~\Transaction\trnReqRetRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND reqm.retrawdate>='" & FilterPeriod1.Text & " 00:00:00' AND reqm.retrawdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND reqm.retrawmststatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Transaction\trnReqRetRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Transaction\trnReqRetRaw.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND reqm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        PrintReport()
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearKIK_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnSearchKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchKIK.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Mohon untuk memilih Business Unit dahulu!", 2)
            Exit Sub
        End If
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, True)
    End Sub

    Protected Sub btnClearKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearKIK.Click
        womstoid.Text = "" : wono.Text = ""
        deptoid.Items.Clear() : deptoid_SelectedIndexChanged(Nothing, Nothing)
        reqwhoid.Items.Clear() : ToMtrlocOid.Items.Clear()
    End Sub

    Protected Sub btnFindListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListKIK.Click
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub btnAllListKIK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListKIK.Click
        FilterDDLListKIK.SelectedIndex = -1 : FilterTextListKIK.Text = "" : gvListKIK.SelectedIndex = -1
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListKIK.PageIndexChanging
        gvListKIK.PageIndex = e.NewPageIndex
        BindListKIK()
        mpeListKIK.Show()
    End Sub

    Protected Sub gvListKIK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListKIK.SelectedIndexChanged
        If womstoid.Text <> gvListKIK.SelectedDataKey.Item("womstoid").ToString Then
            btnClearKIK_Click(Nothing, Nothing)
        End If

        womstoid.Text = gvListKIK.SelectedDataKey.Item("womstoid").ToString
        wono.Text = gvListKIK.SelectedDataKey.Item("wono").ToString
        BranchCode.Text = gvListKIK.SelectedDataKey.Item("branch_code").ToString
        ToBranch.Text = gvListKIK.SelectedDataKey.Item("ToBranch").ToString
        woflag.Text = gvListKIK.SelectedDataKey.Item("woflag").ToString
        DDLFG.Items.Add(GetStrData("select itemshortdesc from ql_trnwodtl3 where womstoid = " & gvListKIK.SelectedDataKey.Item("womstoid") & " "))
        DDLFG.Items.Item(DDLFG.Items.Count - 1).Value = GetStrData("select itemoid from ql_trnwodtl3 where womstoid = " & gvListKIK.SelectedDataKey.Item("womstoid") & " ")

        '' Fill DDL Warehouse
        'FillDDL(reqwhoid, "SELECT genoid,ISNULL(gendesc+' - '+(SELECT DISTINCT gendesc FROM QL_mstgen gd WHERE loc.genother1=gd.genoid AND gd.gengroup='WAREHOUSE'),'-') gendesc FROM QL_mstgen loc WHERE gengroup='LOCATION' AND genother2=(SELECT DISTINCT genoid FROM QL_mstgen br WHERE br.genoid=CAST(loc.genother2 AS int) AND br.gencode='" & gvListKIK.SelectedDataKey.Item("ToBranch").ToString & "' AND br.gengroup='CABANG')")

        '' Fill DDL Warehouse Tujuan
        'FillDDL(ToMtrlocOid, "SELECT genoid,ISNULL(gendesc+' - '+(SELECT DISTINCT gendesc FROM QL_mstgen gd WHERE loc.genother1=gd.genoid AND gd.gengroup='WAREHOUSE'),'-') gendesc FROM QL_mstgen loc WHERE gengroup='LOCATION' AND genother2=(SELECT DISTINCT genoid FROM QL_mstgen br WHERE br.genoid=CAST(loc.genother2 AS int) AND br.gencode NOT IN ('" & Session("branch_id") & "') AND br.gengroup='CABANG')")

        DDLFG_SelectedIndexChanged(Nothing, Nothing)
        InitDDLDept() : deptoid_SelectedIndexChanged(Nothing, Nothing)
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub lkbCloseListKIK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListKIK.Click
        cProc.SetModalPopUpExtender(btnHideListKIK, pnlListKIK, mpeListKIK, False)
    End Sub

    Protected Sub deptoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deptoid.SelectedIndexChanged
        If deptoid.SelectedValue <> "" Then
            seqprocess.Text = GetStrData("SELECT wodtl2procseq FROM QL_trnwodtl2 WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND deptoid=" & deptoid.SelectedValue & " AND womstoid=" & womstoid.Text)
            If seqprocess.Text = "1" Then
                flagrequest.Text = "NONPROCESS"
            Else
                flagrequest.Text = "PROCESS"
            End If
        End If
        ClearDetail()
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        If wono.Text.Trim = "" Then
            showMessage("Mohon untuk memilih No. SPK dahulu!", 2)
            Exit Sub
        End If
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat2.DataSource = Session("TblMatView") : gvListMat2.DataBind()
        BindMaterial()
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, True)
    End Sub

    Protected Sub btnFindListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat2.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat2.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat2.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat2.DataSource = Session("TblMatView")
                gvListMat2.DataBind()
                dtView.RowFilter = ""
                mpeListMat2.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat2.Show()
        End If
    End Sub

    Protected Sub btnAllListMat2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat2.Click
        FilterDDLListMat2.SelectedIndex = -1 : FilterTextListMat2.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMaterial()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Session("TblMatView") = Session("TblMat")
            gvListMat2.DataSource = Session("TblMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub cbHdrListMat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bVal As Boolean = sender.Checked
        For C1 As Integer = 0 To gvListMat2.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat2.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bVal
                    End If
                Next
            End If
        Next
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat2.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat2.PageIndex = e.NewPageIndex
            gvListMat2.DataSource = Session("TblMatView")
            gvListMat2.DataBind()
        End If
        mpeListMat2.Show()
    End Sub

    Protected Sub gvListMat2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(4).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    Else
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ToMaskEdit(ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text), 3)
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub lkbAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToList.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dt As DataTable = Session("TblMat")
                Dim dv As DataView = dt.DefaultView
                Dim erR As String = ""
                dv.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dv.Count
                If iCheck > 0 Then
                    dv.RowFilter = ""
                    dv.RowFilter = "checkvalue='True' AND retqty>0"
                    If dv.Count <> iCheck Then
                        erR = "Return qty for every checked material data must be more than 0!<BR>"
                    End If
                    For C1 As Integer = 0 To dv.Count - 1
                        dv.RowFilter = ""
                        dv.RowFilter = "checkvalue='True' AND retqty<=" & ToDouble(dv(C1)("reqqty").ToString) & ""
                        If dv.Count <> iCheck Then
                            erR = "Return Qty harus kurang dari atau sama dengan Max Qty!<BR>"
                        End If
                    Next
                    If erR <> "" Then
                        Session("WarningListMat") = erR
                        showMessage(Session("WarningListMat"), 2)
                        dv.RowFilter = ""
                        Exit Sub
                    End If

                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim dtMat As DataTable = Session("TblDtl")
                    Dim dvMat As DataView = dtMat.DefaultView
                    Dim rv As DataRow
                    Dim iSeq As Integer = dvMat.Count
                    For C1 As Integer = 0 To dv.Count - 1
                        dvMat.RowFilter = "reqdtloid='" & dv(C1)("dtloid").ToString & "'"
                        If dvMat.Count > 0 Then
                            dvMat.AllowEdit = True
                            dvMat(0)("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                            dvMat(0)("retqty") = ToDouble(dv(C1)("retqty").ToString)
                            dvMat(0)("retnote") = dv(C1)("note").ToString
                        Else
                            iSeq += 1
                            rv = dtMat.NewRow()
                            rv("retdtlseq") = iSeq
                            rv("reqdtloid") = dv(C1)("dtloid").ToString
                            rv("reqmstoid") = dv(C1)("mstoid").ToString
                            rv("matoid") = dv(C1)("itemoid").ToString
                            rv("matcode") = dv(C1)("itemcode").ToString
                            rv("matlongdesc") = dv(C1)("itemdesc").ToString
                            rv("retwhoid") = dv(C1)("whoid").ToString
                            rv("rettowhoid") = dv(C1)("towhoid").ToString
                            rv("reqqty") = ToDouble(dv(C1)("reqqty").ToString)
                            rv("retqty") = ToDouble(dv(C1)("retqty").ToString)
                            rv("retunitoid") = dv(C1)("unitoid")
                            rv("retunit") = dv(C1)("unitdesc").ToString
                            rv("retnote") = dv(C1)("note").ToString
                            dtMat.Rows.Add(rv)
                        End If
                        dvMat.RowFilter = ""
                    Next
                    dv.RowFilter = ""
                    Session("TblDtl") = dvMat.ToTable
                    gvReqDtl.DataSource = Session("TblDtl")
                    gvReqDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
                    ClearDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat2.Click
        cProc.SetModalPopUpExtender(btnHideListMat2, pnlListMat2, mpeListMat2, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "reqdtloid=" & reqdtloid.Text & ""
            Else
                dv.RowFilter = "reqdtloid=" & reqdtloid.Text & " AND retdtlseq<>" & reqdtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("Data ini sudah ditambahkan sebelumnya, Mohon dicek!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                reqdtlseq.Text = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(reqdtlseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("retdtlseq") = reqdtlseq.Text
            objRow("reqdtloid") = reqdtloid.Text
            objRow("reqmstoid") = reqmstoid.Text
            objRow("matoid") = matoid.Text
            objRow("matcode") = matcodedtl1.Text
            objRow("matlongdesc") = matlongdesc.Text
            objRow("retqty") = ToDouble(reqqtybiji.Text)
            objRow("reqqty") = ToDouble(reqmaxqtybiji.Text)
            objRow("retunitoid") = DDLunitRet.SelectedValue
            objRow("retunit") = DDLunitRet.SelectedItem.Text
            objRow("retnote") = reqdtlnote.Text.Trim
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvReqDtl.DataSource = Session("TblDtl")
            gvReqDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvReqDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReqDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
        End If
    End Sub

    Protected Sub gvReqDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvReqDtl.SelectedIndexChanged
        Try
            reqdtlseq.Text = gvReqDtl.SelectedDataKey.Item("retdtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "retdtlseq=" & reqdtlseq.Text
                reqdtloid.Text = dv.Item(0).Item("reqdtloid").ToString
                reqmstoid.Text = dv.Item(0).Item("reqmstoid").ToString
                matoid.Text = dv.Item(0).Item("matoid").ToString
                matcodedtl1.Text = dv.Item(0).Item("matcode").ToString
                matlongdesc.Text = dv.Item(0).Item("matlongdesc").ToString
                reqqtybiji.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("retqty").ToString), 3)
                reqmaxqtybiji.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("reqqty").ToString), 3)
                DDLunitRet.SelectedValue = dv.Item(0).Item("retunitoid").ToString
                DDLunit1.SelectedValue = dv.Item(0).Item("retunitoid").ToString
                reqdtlnote.Text = dv.Item(0).Item("retnote").ToString
                dv.RowFilter = ""
                btnSearchMat.Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvReqDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReqDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("retdtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvReqDtl.DataSource = objTable
        gvReqDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim CrdMtroid As Integer = 0 : Dim ConMtroid As Integer = 0
        periodacctg.Text = GetDateToPeriodAcctg(CDate(toDate(reqdate.Text))).Trim

        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnreqrawretmst WHERE retrawmstoid=" & retmstoid.Text
                If CheckDataExists(sSql) Then
                    retmstoid.Text = GenerateID("QL_TRNREQRAWRETMST", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqrawretmst", "retrawmstoid", retmstoid.Text, "retrawmststatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            '--------------------------------------------------------
            '===================Generated ID Tabel===================
            retdtloid.Text = GenerateID("QL_TRNREQRAWRETDTL", CompnyCode)
            CrdMtroid = GenerateID("QL_crdmtr", CompnyCode)
            ConMtroid = GenerateID("QL_conmtr", CompnyCode)
            '--------------------------------------------------------
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            If reqmststatus.Text = "Post" Then
                Dim trnno As String = "REQRET/" & Format(CDate(toDate(reqdate.Text)), "yy") & "/" & Format(CDate(toDate(reqdate.Text)), "MM") & "/"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(retrawno,4) AS INT)), 0) FROM QL_trnreqrawretmst WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND retrawno LIKE '" & trnno & "%'"
                xCmd.CommandText = sSql
                trnno = trnno & Format(xCmd.ExecuteScalar + 1, "0000")
                reqno.Text = trnno
            Else
                If i_u.Text = "new" Then
                    reqno.Text = retmstoid.ToString
                End If
            End If

            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnreqrawretmst (cmpcode, branch_code, tobranch_code, retrawmstoid, periodacctg, retrawdate, retrawno, womstoid, seqprocess, deptoid, retrawflag, wodtl2oid, retrawwhoid, retrawmstnote, retrawmststatus, createuser, createtime, upduser, updtime, mtrlocoid, tomtrlocoid) VALUES ('" & DDLBusUnit.SelectedValue & "','" & BranchCode.Text & "','" & ToBranch.Text & "', " & retmstoid.Text & ", '" & periodacctg.Text & "', '" & CDate(toDate(reqdate.Text)) & "', '" & reqno.Text & "', " & womstoid.Text & ", 0, 0,'" & flagrequest.Text & "',0, 0, '" & Tchar(reqmstnote.Text.Trim) & "', '" & reqmststatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & retmstoid.Text & " WHERE tablename='QL_TRNREQRAWRETMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnreqrawretmst SET periodacctg='" & periodacctg.Text & "', retrawdate='" & CDate(toDate(reqdate.Text)) & "', retrawno='" & reqno.Text & "', womstoid=" & womstoid.Text & ", retrawmstnote='" & Tchar(reqmstnote.Text.Trim) & "', retrawmststatus='" & reqmststatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE retrawmstoid=" & retmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM QL_trnreqrawretdtl WHERE retrawmstoid=" & retmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnreqrawretdtl (cmpcode, retrawdtloid, retrawmstoid, retrawdtlseq, reftype, wodtl2oid, refoid, retrawqty, retrawunitoid, retrawflag, retrawdtlnote, retrawdtlstatus, upduser, updtime,retrawdtltype, itemoid, frommtrlocoid, tomtrlocoid, branch_code, tobranch_code, reqmstoid, reqdtloid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(retdtloid.Text)) & ", " & retmstoid.Text & ", " & c1 + 1 & ", '', 0, " & objTable.Rows(c1).Item("matoid") & "," & ToDouble(objTable.Rows(c1).Item("retqty").ToString) & ", " & objTable.Rows(c1).Item("retunitoid") & ",'" & flagrequest.Text & "','" & Tchar(objTable.Rows(c1).Item("retnote")) & "', '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '','" & objTable.Rows(c1).Item("matoid").ToString & "'," & objTable.Rows(c1).Item("retwhoid") & "," & objTable.Rows(c1).Item("rettowhoid") & ",'" & BranchCode.Text & "','" & ToBranch.Text & "', " & objTable.Rows(c1).Item("reqmstoid") & ", " & objTable.Rows(c1).Item("reqdtloid") & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If reqmststatus.Text = "Post" Then
                            '==========================
                            '-----Cek Hpp Terakhir-----
                            '==========================
                            Dim lasthpp As Double = 0
                            sSql = "SELECT HPP FROM ql_mstitem WHERE itemoid=" & objTable.Rows(c1).Item("matoid") & ""
                            xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar
                            '-------------------------------------
                            '=====Update crdmtr for item out======
                            '-------------------------------------
                            sSql = "UPDATE QL_crdmtr SET qtyout = qtyout + " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", saldoakhir = saldoakhir - " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", LastTransType = 'REQRETOUT', lastTrans = '" & CDate(toDate(reqdate.Text)) & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND mtrlocoid = " & objTable.Rows(c1).Item("retwhoid") & " AND refoid = " & objTable.Rows(c1).Item("matoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & periodacctg.Text & "' And branch_Code = '" & BranchCode.Text & "'"
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                CrdMtroid = CrdMtroid + 1
                                '--------------------------------
                                'Insert crdmtr if no record found
                                '--------------------------------
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CrdMtroid & ", '" & periodacctg.Text & "', " & objTable.Rows(c1).Item("matoid") & ", 'QL_MSTITEM'," & objTable.Rows(c1).Item("retwhoid") & ", '" & BranchCode.Text & "', 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", 0, 0, 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", 0, 'REQRETOUT', '" & CDate(toDate(reqdate.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900','')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If
                            '-------------------------------------
                            '=====Insert conmtr for item out======
                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, upduser, updtime, typeMin, personoid, amount, note, HPP,Branch_Code) VALUES ('" & DDLBusUnit.SelectedValue & "', " & ConMtroid & ", 'REQRETOUT', '" & CDate(toDate(reqdate.Text)) & "', '" & periodacctg.Text & "', '" & reqno.Text & "', " & retmstoid.Text & ", 'QL_trnrerawretmst', " & objTable.Rows(c1).Item("matoid") & ", 'QL_MSTITEM', " & objTable.Rows(c1).Item("retunitoid") & ", " & objTable.Rows(c1).Item("retwhoid") & ", 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) * ToDouble(lasthpp) & ", '" & Tchar(objTable.Rows(c1).Item("retnote")) & "', " & ToDouble(lasthpp) & ",'" & BranchCode.Text & "')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1

                            '--------------------------
                            'Update crdmtr for item in 
                            '--------------------------
                            sSql = "UPDATE QL_crdmtr SET qtyin = qtyin + " & ToDouble(objTable.Rows(c1).Item("retqty")) & ",branch_code='" & ToBranch.Text & "', saldoakhir = saldoakhir + " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", LastTransType = 'REQRETIN', lastTrans = '" & CDate(toDate(reqdate.Text)) & "', upduser = '" & Session("UserID") & "', updtime = CURRENT_TIMESTAMP WHERE cmpcode = '" & DDLBusUnit.SelectedValue & "' AND mtrlocoid =" & objTable.Rows(c1).Item("rettowhoid") & " AND refoid = " & objTable.Rows(c1).Item("matoid") & " AND refname = 'QL_MSTITEM' AND periodacctg = '" & periodacctg.Text & "' AND branch_code = '" & ToBranch.Text & "' "
                            xCmd.CommandText = sSql
                            If xCmd.ExecuteNonQuery() = 0 Then
                                '--------------------------------
                                'Insert crdmtr if no record found
                                '--------------------------------
                                CrdMtroid = CrdMtroid + 1
                                sSql = "INSERT INTO QL_crdmtr (cmpcode, crdmatoid, periodacctg, refoid, refname, mtrlocoid, branch_code, qtyIn, qtyOut, qtyAdjIn, qtyAdjOut, saldoAwal, saldoAkhir, qtyBooking, LastTransType, lastTrans, upduser, updtime, createuser, createdate, closingdate, closeuser) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CrdMtroid & ", '" & periodacctg.Text & "', " & objTable.Rows(c1).Item("matoid") & ", 'QL_MSTITEM'," & objTable.Rows(c1).Item("rettowhoid") & " , '" & ToBranch.Text & "', " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", 0, 0, 0, 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) & ", 0, 'REQRETIN', '" & CDate(toDate(reqdate.Text)) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '1/1/1900', '')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If

                            '=======================================================
                            '-----------------Insert Conmtr Qty In-----------------
                            '=======================================================
                            sSql = "INSERT INTO QL_conmtr (cmpcode, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid,branch_code,qtyIn, qtyOut, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES ('" & DDLBusUnit.SelectedValue & "', " & ConMtroid & ", 'REQRETIN', '" & CDate(toDate(reqdate.Text)) & "', '" & periodacctg.Text & "', '" & reqno.Text & "', " & retmstoid.Text & ", 'QL_trnreqrawretmst', " & objTable.Rows(c1).Item("matoid") & ", 'QL_MSTITEM', " & objTable.Rows(c1).Item("retunitoid") & ", " & objTable.Rows(c1).Item("rettowhoid") & ",'" & ToBranch.Text & "'," & ToDouble(objTable.Rows(c1).Item("retqty")) & ", 0, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, " & ToDouble(objTable.Rows(c1).Item("retqty")) * ToDouble(lasthpp) & ", '" & Tchar(objTable.Rows(c1).Item("retnote")) & "', " & ToDouble(lasthpp) & ")"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            ConMtroid += 1
                        End If
                    Next

                    If reqmststatus.Text = "Post" Then
                        '========================
                        'Update Ql_trnwomst
                        '========================
                        sSql = "UPDATE QL_trnwomst set womstres1='RETURN' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND womstoid=" & womstoid.Text & ""
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                   
                    '========================
                    'Update lastoid QL_trnreqrawretdtl
                    '========================
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(retdtloid.Text)) & " WHERE tablename='QL_TRNREQRAWRETDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '========================
                    'Update lastoid QL_crdmtr
                    '========================
                    sSql = "UPDATE QL_mstoid SET lastoid =" & CrdMtroid - 1 & "  WHERE tablename = 'QL_crdmtr' and cmpcode = '" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    '========================
                    'Update lastoid QL_conmtr
                    '========================
                    sSql = "UPDATE QL_mstoid SET lastoid =" & ConMtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        reqmststatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    reqmststatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                conn.Close()
                reqmststatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. telah diregenerasi karena digunakan oleh data lain. Draft No. yang baru adalah " & reqmstoid.Text & ".<BR>"
            End If
            If reqmststatus.Text = "Post" Then
                Session("SavedInfo") &= "Data telah diposting dengan Request Return No. = " & reqno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Transaction\trnReqRetRaw.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnReqRetRaw.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If retmstoid.Text = "" Then
            showMessage("Mohon untuk memilih Request Return data dahulu!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnreqrawretmst", "retrawmstoid", retmstoid.Text, "retrawmststatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                reqmststatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trnreqrawretdtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retrawmstoid=" & retmstoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnreqrawretmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND retrawmstoid=" & retmstoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnReqRetRaw.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        reqmststatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub DDLFG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLFG.SelectedIndexChanged

    End Sub
#End Region

End Class