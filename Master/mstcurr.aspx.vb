'Prgmr:DW |UpdtBy:Els | LastUpdt:14.4.08
Imports System.Data
Imports System.Data.SqlClient

Partial Class mstcurr
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

    Public Sub showMessage(ByVal message As String, ByVal type As Integer)
        Dim scaption As String = CompnyName
        If type = 1 Then
            scaption &= " - ERROR"
            Image1.ImageUrl = "~/Images/error.jpg"
        ElseIf type = 2 Then
            scaption &= " - WARNING"
            Image1.ImageUrl = "~/Images/warn.png"
        End If
        Validasi.Text = message
        lblCapt.Text = scaption
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
    End Sub

    Protected Sub LBOid_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TabContainer1.ActiveTabIndex = 1
        Dim lnkEdit As LinkButton = sender
        Dim row As GridViewRow = lnkEdit.NamingContainer
        Session.Item("kode") = lnkEdit.Text 'sender.CommandArgument
        With SDSData
            .SelectParameters("currencyoid").DefaultValue = Session.Item("kode")
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
        End With
        FrmViewCurr.ChangeMode(FormViewMode.Edit)
        FrmViewCurr.DataBind()
    End Sub

    Sub FilterGV(ByVal filterCode As String, ByVal filterDesc As String, ByVal filteruser As String)
        With SDSDataView
            .SelectParameters("CurrencyCode").DefaultValue = "%" & filterCode & "%"
            .SelectParameters("Currencydesc").DefaultValue = "%" & filterDesc & "%"
            .SelectParameters("upduser").DefaultValue = "%%"
            .SelectParameters("CmpCode").DefaultValue = CompnyCode
        End With
        GVmst.DataBind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Select Case FilterDDL.SelectedValue
            Case "Code"
                If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
                    FilterGV(FilterText.Text, "", "")
                Else
                    FilterGV(FilterText.Text, "", Session("UserID"))
                End If
            Case "Description"
                If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
                    FilterGV("", FilterText.Text, "")
                Else
                    FilterGV("", FilterText.Text, Session("UserID"))
                End If
        End Select
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
            FilterGV("", "", "")
        Else
            FilterGV("", "", Session("UserID"))
        End If
        FilterDDL.SelectedIndex = 0 : FilterText.Text = ""
        GVmst.PageIndex = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If
        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstcurr.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid") = Request.QueryString("oid")
        'start_add32
        'CType(Me.FrmViewCurr.FindControl("btnDelete"), Button).Attributes.Add("onclick", "return confirm ('Are you sure delete this data');")
        'end_add32

        Page.Title = CompnyName & " - Master Currency"

        If Not IsPostBack Then
            If cKoneksi.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("SpecialAccess")) = True Then
                FilterGV("", "", "")
            Else
                FilterGV("", "", Session("UserID"))
            End If

            If Session("oid") Is Nothing Or Session("oid") = "" Then
                'start_add32
                CType(FrmViewCurr.FindControl("currencycode"), TextBox).ReadOnly = False
                CType(FrmViewCurr.FindControl("currencycode"), TextBox).CssClass = "inpText"
                'CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurr", CompnyCode)
                'end_add32
                FrmViewCurr.ChangeMode(FormViewMode.Insert)
                CType(FrmViewCurr.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurr", CompnyCode)
                TabContainer1.ActiveTabIndex = 0
            Else
                With SDSData
                    .SelectParameters("currencyoid").DefaultValue = Session.Item("oid")
                    .SelectParameters("cmpcode").DefaultValue = CompnyCode
                    '.SelectParameters("currencycode").DefaultValue = 21
                End With
                FrmViewCurr.ChangeMode(FormViewMode.Edit)
                FrmViewCurr.DataBind()
                TabContainer1.ActiveTabIndex = 1
                'start_add32
                'CType(FrmViewCurr.FindControl("currencycode"), TextBox).ReadOnly = True
                'CType(FrmViewCurr.FindControl("currencycode"), TextBox).CssClass = "inpTextDisabled"
                'end_add32

            End If
        End If
    End Sub

    Protected Sub FrmViewCurr_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewCommandEventArgs) Handles FrmViewCurr.ItemCommand
        If e.CommandName = "Cancel" Then
            Response.Redirect("~/Master/mstcurr.aspx?awal=true")
        End If
    End Sub

    Protected Sub FrmViewCurr_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeletedEventArgs) Handles FrmViewCurr.ItemDeleted
        GVmst.DataBind()
        CType(FrmViewCurr.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstcurr", CompnyCode)
        Response.Redirect("~/Master/mstcurr.aspx")
    End Sub

    Protected Sub FrmViewCurr_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewDeleteEventArgs) Handles FrmViewCurr.ItemDeleting
        Dim sColumnName() As String = {"CURROID", "custcreditlimitcurroid", "curroid", "currencyoid", "cashbankcurroid", "forccurroid", "currencyoid"}
        Dim sTableName() As String = {"ql_mstCurrhist", "ql_mstcust", "ql_pomst", "ql_trnbelimst", "QL_trncashbankmst", "ql_trnforcmst", "ql_trnjualmst"}
        If ClassFunction.CheckDataExists(CType(FrmViewCurr.FindControl("oid"), TextBox).Text, sColumnName, sTableName) = False Then
            With SDSData
                .DeleteParameters("CurrencyOid").DefaultValue = CType(FrmViewCurr.FindControl("oid"), TextBox).Text
                .DeleteParameters("cmpcode").DefaultValue = CompnyCode
            End With
        Else
            showMessage("Data tidak dapat dihapus!<br />Data ini telah digunakan pada transaksi lain!", 2)
            'Validasi.Text = "You cannot delete this data, cause this data used by another table!"
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            e.Cancel = True : Exit Sub
        End If
    End Sub

    Protected Sub FrmViewCurr_ItemInserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertedEventArgs) Handles FrmViewCurr.ItemInserted
        GVmst.DataBind()
        'FrmViewItem.ChangeMode(FormViewMode.Insert)
        CType(FrmViewCurr.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstcurr", CompnyCode)
        Response.Redirect("~/Master/mstcurr.aspx")
    End Sub

    Protected Sub FrmViewCurr_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewInsertEventArgs) Handles FrmViewCurr.ItemInserting
        If CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text.Trim = "" Or CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text.Trim = "" Then
            If CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text.Trim = "" Then
                ' CType(FrmViewCurr.FindControl("Validasi"), Label).Text = " Please Fill Currency Code Field ! " & vbCrLf
                Validasi.Text = "Kolom Currency Code tidak boleh kosong!<BR>"
            End If
            If CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text.Trim = "" Then
                '       CType(FrmViewCurr.FindControl("Validasi"), Label).Text = " Please Fill Description Field ! " & vbCrLf
                Validasi.Text &= "Kolom Description tidak boleh kosong!<BR>"
            End If
            showMessage(Validasi.Text, 2)
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            e.Cancel = True
            Exit Sub
        End If

        'asli
        CType(FrmViewCurr.FindControl("oid"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurr", CompnyCode)
        'start_add32
        'CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text = ClassFunction.GenerateID("QL_mstCurr", CompnyCode)
        'end_add32

        With SDSData
            .InsertParameters("CurrencyOid").DefaultValue = ClassFunction.GenerateID("QL_mstCurr", CompnyCode) 'id
            .InsertParameters("currencycode").DefaultValue = CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text
            .InsertParameters("currencydesc").DefaultValue = CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text
            .InsertParameters("cmpcode").DefaultValue = CompnyCode
            .InsertParameters("UpdUser").DefaultValue = Session("UserID")
            .InsertParameters("UpdTime").DefaultValue = Now
        End With

        'start_add32
        'Dim xdt1 As New DataTable
        'Dim xquery1 = "SELECT [cmpcode], [currencyoid], [currencycode], [currencydesc], [upduser], [updtime] FROM [QL_mstcurr] WHERE [CMPCODE]='" & _
        '    CompnyCode & "' AND [currencycode]='" & CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text & "'"
        'xdt1 = cKoneksi.ambiltabel(xquery1, "QL_mstCurr")
        'Dim xRow1() As DataRow
        'xRow1 = xdt1.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        'If xRow1.Length > 0 Then
        '    'sMsg &= "- Duplicate Customer Code. Please fill another Customer Code !!<BR>"
        '    'showMessage(sMsg & " PT. SEKAWAN INTIPRATAMA - WARNING")
        '    Validasi.Text = "Duplicate Code. Please fill another Code !"
        '    CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
        '    e.Cancel = True : Exit Sub
        '    Exit Sub
        'End If
        Dim sMsg As String = ""
        With SqlDataSource1
            .SelectParameters("currencycode").DefaultValue = CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
        End With
        Dim dvSql As DataView = DirectCast(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        For Each drvSql As DataRowView In dvSql
            sMsg &= "Currency Code telah digunakan! Silahkan mengisikan Currency Code yang lain!<br />"
            Exit For
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            'e.Cancel = True : Exit Sub
            'Exit Sub
        Next
        With SqlDataSource2
            .SelectParameters("currencydesc").DefaultValue = CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
        End With
        Dim dvSql2 As DataView = DirectCast(SqlDataSource2.Select(DataSourceSelectArguments.Empty), DataView)
        For Each drvSql2 As DataRowView In dvSql2
            sMsg &= "Description telah digunakan pada data lain! Silahkan mengisikan Description yang lain!<BR>"
            Exit For
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            'e.Cancel = True : Exit Sub
            'Exit Sub
        Next

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            'Validasi.Text = sMsg
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            e.Cancel = True : Exit Sub
            Exit Sub
        End If
        'end_add32

        SDSData.Insert()
        With SDSOid
            .UpdateParameters("lastoid").DefaultValue = ClassFunction.GenerateID("QL_mstCurr", CompnyCode) 'id
            .UpdateParameters("cmpcode").DefaultValue = CompnyCode
            .UpdateParameters("tablename").DefaultValue = "QL_MstCurr"
        End With
        SDSOid.Update()
    End Sub

    Protected Sub FrmViewCurr_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdatedEventArgs) Handles FrmViewCurr.ItemUpdated
        GVmst.DataBind()
        Response.Redirect("~/Master/mstcurr.aspx")
    End Sub

    Protected Sub FrmViewCurr_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewUpdateEventArgs) Handles FrmViewCurr.ItemUpdating
        If CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text.Trim = "" Or CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text.Trim = "" Then
            If CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text.Trim = "" Then
                Validasi.Text = "Kolom Currency Code tidak boleh kosong!<BR> "
            End If
            If CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text.Trim = "" Then
                Validasi.Text &= "Kolom Description tidak boleh kosong!<BR>"
            End If
            showMessage(Validasi.Text, 2)
            'CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
            e.Cancel = True : Exit Sub
        End If

        Dim sMsg As String = ""
        With SqlDataSource1upd
            .SelectParameters("currencycode").DefaultValue = CType(FrmViewCurr.FindControl("currencycode"), TextBox).Text
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
            .SelectParameters("oid").DefaultValue = CType(FrmViewCurr.FindControl("oid"), TextBox).Text
        End With
        Dim dvSql As DataView = DirectCast(SqlDataSource1upd.Select(DataSourceSelectArguments.Empty), DataView)
        For Each drvSql As DataRowView In dvSql
            sMsg &= "Currency Code talh digunakan sebelumnya! Silahkan mengisikan Currency Code yang lain!<BR>"
            Exit For
        Next
        With SqlDataSource2upd
            .SelectParameters("currencydesc").DefaultValue = CType(FrmViewCurr.FindControl("currencydesc"), TextBox).Text
            .SelectParameters("cmpcode").DefaultValue = CompnyCode
            .SelectParameters("oid").DefaultValue = CType(FrmViewCurr.FindControl("oid"), TextBox).Text
        End With
        Dim dvSql2 As DataView = DirectCast(SqlDataSource2upd.Select(DataSourceSelectArguments.Empty), DataView)
        For Each drvSql2 As DataRowView In dvSql2
            sMsg &= "Description telah digunakan oleh data lain! Silahkan mengisikan Description yang lain!<BR>"
            Exit For
        Next

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            e.Cancel = True : Exit Sub
            Exit Sub
        End If

        With SDSData
            .UpdateParameters("CurrencyOID").DefaultValue = CType(FrmViewCurr.FindControl("oid"), TextBox).Text
            .UpdateParameters("CmpCode").DefaultValue = CompnyCode
            .UpdateParameters("UpdUser").DefaultValue = Session("UserID")
            .UpdateParameters("Updtime").DefaultValue = Now
        End With
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/Master/mstcurr.aspx")
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/Master/mstcurrhist.aspx?oid=" & Session("oid") & " &awal=true")
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Protected Sub ImageButton1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Validasi.Text = ""
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()
    End Sub

    Protected Sub FrmViewCurr_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.FormViewPageEventArgs)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
End Class