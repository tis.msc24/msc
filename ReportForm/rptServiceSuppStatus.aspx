<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptServiceSuppStatus.aspx.vb" Inherits="rptSSStatus" title="MSC - Laporan Order Pembelian" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: LAPORAN STATUS SERVICE SUPPLIER"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" class="header" style="width: 100%; background-color: transparent"
                valign="center">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="3">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=left runat="server" Visible="true"><asp:Label id="lblCabang" runat="server" Text="Cabang"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="dCabangNya" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label1" runat="server" Text="Type TW"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlTypeTW" runat="server" CssClass="inpText"><asp:ListItem>Service Supplier</asp:ListItem>
<asp:ListItem>Kembali Supplier</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" visible="true"><asp:Label id="Label5" runat="server" Text="Type Service"></asp:Label></TD><TD align=left colSpan=3 runat="server" visible="true"><asp:DropDownList id="DDLGaransi" runat="server" CssClass="inpText">
                <asp:ListItem>ALL</asp:ListItem>
                <asp:ListItem>Service</asp:ListItem>
                <asp:ListItem>Garansi</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;-&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label></TD></TR><TR><TD align=left runat="server" visible="true">No. Service</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="sono" runat="server" Width="158px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true">No. Kembali</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="dono" runat="server" Width="158px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left runat="server" visible="true">Supplier</TD><TD align=left colSpan=3 runat="server" visible="true"><asp:TextBox id="custcode" runat="server" Width="158px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Width="150px" Text="No. Penerimaan(TTS)"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="shipmentno" runat="server" Width="158px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindShipment" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseShipment" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="27px" Text="Katalog"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemcode" runat="server" Width="178px" CssClass="inpText" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindMat" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseMat" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px"></asp:ImageButton></TD></TR><TR><TD align=left>Status</TD><TD align=left colSpan=3><asp:DropDownList id="statusDelivery" runat="server" Width="134px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>COMPLETE</asp:ListItem>
<asp:ListItem>IN COMPLETE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="dateAwal"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="dateAkhir"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateAwal" PopupButtonID="imageButton1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="dateAkhir" PopupButtonID="imageButton2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" HasToggleGroupTreeButton="False" HasSearchButton="False" HasRefreshButton="True" HasPrintButton="False" HasExportButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListSO" runat="server">
                                <ContentTemplate>
<asp:Panel id="PanelListSO" runat="server" Width="1000px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Service Supplier"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label241" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="soitemno">No. Service</asp:ListItem>
<asp:ListItem Value="soitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 25px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("soitemmstoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="soitemmstoid" HeaderText="Draft No">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Left"
                                                                        VerticalAlign="Middle" Wrap="False" />
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="soitemno" HeaderText="No. Service">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="sodate" HeaderText="Tanggal">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="suppname" HeaderText="Supplier">
                                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="cabang" HeaderText="Cabang">
                                                                    <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="soitemmststatus" HeaderText="Status">
                                                                    <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView> </TD></TR><TR><TD class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO" BackgroundCssClass="modalBackground">
                                    </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListDO" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="PanelListDO" runat="server" CssClass="modalBox" Visible="False" Width="900px">
                                        <table style="width: 100%">
                                            <tbody>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3">
                                                        <asp:Label ID="lblListDO" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Kembali Supplier"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3">
                                                        <asp:Panel ID="pnlFindListDO" runat="server" DefaultButton="btnFindListDO" Width="100%">
                                                            <asp:Label ID="Label24" runat="server" Text="Filter :"></asp:Label>
                                                            <asp:DropDownList ID="DDLFilterListDO" runat="server" CssClass="inpText" Width="100px">
                                                                <asp:ListItem Value="doitemno">No. Kembali</asp:ListItem>
                                                                <asp:ListItem Value="doitemmstoid">No. Draft</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtFilterListDO" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnFindListDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                                            <asp:ImageButton ID="btnViewAllListDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3">
                                                        <asp:ImageButton ID="btnSelectAllDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                                                            ID="btnSelectNoneDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                                                                ID="btnViewCheckedDO" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3">
                                                        <asp:GridView ID="gvListDO" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                            CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="8" Width="98%">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbLMDO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("doitemmstoid") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="doitemmstoid" HeaderText="No. Draft">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                                        VerticalAlign="Middle" Wrap="False" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="doitemno" HeaderText="No. Kembali">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="dodate" HeaderText="Tanggal">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="custname" HeaderText="Supplier" Visible="False">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="doitemmststatus" HeaderText="Status">
                                                                    <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                                HorizontalAlign="Right" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;
                                                            </EmptyDataTemplate>
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="Label" colspan="3" style="height: 5px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:LinkButton ID="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton>
                                                        -
                                                        <asp:LinkButton ID="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpeListDO" runat="server" BackgroundCssClass="modalBackground"
                                        PopupControlID="PanelListDO" PopupDragHandleControlID="lblListDO" TargetControlID="btnHiddenListDO">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="btnHiddenListDO" runat="server" Visible="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListCust" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlListCust" runat="server" CssClass="modalBox" Visible="False" Width="650px">
                                        <table width="100%">
                                            <tr>
                                                <td align="center" class="Label" colspan="3">
                                                    <asp:Label ID="lblListCust" runat="server" Font-Bold="True" Font-Size="Medium" Text="List of Supplier"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3" style="height: 5px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3" valign="top">
                                                    <asp:Panel ID="pnlFilterListCust" runat="server" DefaultButton="btnFindListCust"
                                                        Width="100%">
                                                        Filter :
                                                        <asp:DropDownList ID="DDLFilterListCust" runat="server" CssClass="inpText" Width="100px">
                                                            <asp:ListItem Value="custname">Supplier</asp:ListItem>
                                                            <asp:ListItem Value="Custcode">Kode</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="txtFilterListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton
                                                            ID="btnFindListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />&nbsp;<asp:ImageButton
                                                                ID="btnViewAllListCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3" valign="top">
                                                    <asp:ImageButton ID="btnSelectAllCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectall.png" />&nbsp;<asp:ImageButton
                                                        ID="btnSelectNoneCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectnone.png" />&nbsp;<asp:ImageButton
                                                            ID="btnViewCheckedCust" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewselected.png" /></td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3" valign="top">
                                                    <asp:GridView ID="gvListCust" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CellPadding="4" DataKeyNames="custoid,custcode,custname" ForeColor="#333333"
                                                        GridLines="None" PageSize="8" Width="98%">
                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="custcode" HeaderText="Kode">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custname" HeaderText="Supplier">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="gendesc" HeaderText="Cabang">
                                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="custaddr" HeaderText="Address">
                                                                <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"
                                                            HorizontalAlign="Right" />
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="Label" colspan="3" style="height: 5px" valign="top">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <asp:LinkButton ID="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton>
                                                    -
                                                    <asp:LinkButton ID="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpeListCust" runat="server" BackgroundCssClass="modalBackground"
                                        Drag="True" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust"
                                        TargetControlID="btnHideListCust">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="btnHideListCust" runat="server" Visible="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel><asp:UpdatePanel id="upListShipment" runat="server"><contenttemplate>
<asp:Panel id="PanelListShipment" runat="server" Width="600px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListShipment" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Penerimaan(TTS)"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListShipment" runat="server" Width="100%" DefaultButton="btnFindListShipment"><asp:Label id="Label67851" runat="server" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListShipment" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="shipmentitemno">No. TTS</asp:ListItem>
<asp:ListItem Value="shipmentitemmstoid">No. Draft</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListShipment" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListShipment" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListShipment" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllShipment" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneShipment" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedShipment" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListShipment" runat="server" Width="98%" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="shipmentitemmstoid,shipmentitemno,shipmentdate,shipmentitemmststatus,shipmentitemmstnote" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMShipment" runat="server" ToolTip='<%# eval("shipmentitemmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="shipmentitemmstoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Size="X-Small" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentitemno" HeaderText="No. TTS">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="shipmentitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListShipment" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListShipment" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListShipment" runat="server" TargetControlID="btnHiddenListShipment" PopupDragHandleControlID="lblListShipment" PopupControlID="PanelListShipment" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListShipment" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upListMat" runat="server">
                                <ContentTemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="itemlongdesc">Katalog</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3>Jenis Barang : <asp:DropDownList id="JenisBarangDDL" runat="server" CssClass="inpText" Font-Size="Small">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
                                                                            <asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
                                                                            <asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
                                                                            <asp:ListItem>ASSET</asp:ListItem>
                                                                        </asp:DropDownList></TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAll" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNone" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewChecked" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat">
                                                            <ProgressTemplate>
                                                                <table style="width: 200px">
                                                                    <tr>
                                                                        <td style="font-weight: bold; font-size: 10pt; color: purple">
                                                                            Load Data, Please Wait ...</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Image ID="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif" /></td>
                                                                    </tr>
                                                                </table>
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListMat" runat="server" Width="96%" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="itemcode" AutoGenerateColumns="False" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                        <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                                    
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemlongdesc" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisNya" HeaderText="Jenis">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                &nbsp;
                                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" BackgroundCssClass="modalBackground" Drag="True">
                                        </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2" style="background-color: #cc0000; text-align: left">
                                                        <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="height: 10px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                            Width="24px" /></td>
                                                    <td class="Label" style="text-align: left">
                                                        <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="height: 10px; text-align: center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: center">
                                                        &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                                        Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                                        TargetControlID="bePopUpMsg">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </th>
        </tr>
    </table>
</asp:Content>

