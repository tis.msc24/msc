'Prgmr : zipi : Last Update : 07.05.13
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class trnSlsIntOrder
#Region "Variabel"
    Inherits System.Web.UI.Page
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim dtStaticItem As DataTable
    Dim cRate As New ClassRate()
    Dim mySqlConn As New SqlConnection(ConnStr)
    Dim mysql As String
    Dim dv, dv2, dv3 As DataView
    Dim cKoneksi As New Koneksi
    Dim cProc As New ClassProcedure
    Dim hash As New ClassToken
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
#End Region

#Region "Function"
    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvItemOrdered.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    sReturn = cbcheck

                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

    Private Function checkApprovaldata(ByVal sTableName As String, ByVal sEvent As String, ByVal sOid As String, _
        ByVal sRequestUser As String, ByVal sStatus As String, ByVal sbranch As String) As DataTable
        Dim ssql As String = "SELECT CMPCODE,APPROVALOID,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME, OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval WHERE CmpCode='" & CompnyCode & "' and RequestUser LIKE '" & sRequestUser & "%' and STATUSREQUEST = '" & sStatus & "' AND TABLENAME='" & sTableName & "' and OID=" & sOid & " and EVENT='" & sEvent & "' and branch_code='" & sbranch & "' ORDER BY REQUESTDATE DESC"
        Return cKoneksi.ambiltabel(ssql, "QL_APPROVAL")
    End Function

    Public Function isiDS(ByVal squery As String, ByVal snamatabel As String) As DataTable
        Dim ssqlcust As String
        ssqlcust = squery
        Dim mySqlDA As New SqlDataAdapter(ssqlcust, ConnStr)
        Dim objDsRef As New DataSet
        mySqlDA.Fill(objDsRef, snamatabel)
        Return objDsRef.Tables(snamatabel)
    End Function

    Function Bulatkan(ByVal nValue As Double, ByVal nDigits As Integer) As Double
        Bulatkan = Int(nValue * (10 ^ nDigits) + 0.5) / (10 ^ nDigits)
    End Function

    Function insertDataDesign(ByVal dtTemp As DataTable) As DataTable
        If Session("TabelDtl3") Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("DesignColor")
            dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("orderoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("designcolor", Type.GetType("System.String"))
            dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
            dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
            dtlDS.Tables.Add(dtlTable)

            Session("TabelDtl3") = dtlTable
        End If
        Dim objTable As DataTable
        objTable = Session("TabelDtl3")
        Dim objRow As DataRow
        For C1 As Integer = 0 To dtTemp.Rows.Count - 1
            objRow = objTable.NewRow()
            objRow("cmpcode") = dtTemp.Rows(C1).Item(0)
            objRow("orderoid") = dtTemp.Rows(C1).Item(1)
            objRow("designcolor") = dtTemp.Rows(C1).Item(2)
            objRow("upduser") = Session("UserID") 'dtTemp.Rows(C1).Item(0)
            objRow("updtime") = FormatDateTime(GetServerTime(), DateFormat.GeneralDate) 'dtTemp.Rows(C1).Item(0)
            objTable.Rows.Add(objRow)
        Next
        Session("TabelDtl3") = objTable

        Return Session("TabelDtl3")
    End Function

    Function insertData(ByVal dtTemp As DataTable) As DataTable
        '        If Session("OrderedItemTemp") Is Nothing Then
        '            Dim dtlDS As DataSet = New DataSet
        '            Dim dtlTable As DataTable = New DataTable("TrnItemTemp")
        '0:          dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        '1:          dtlTable.Columns.Add("orderdtloid", Type.GetType("System.Int32"))
        '2:          dtlTable.Columns.Add("ordermstoid", Type.GetType("System.Int32"))
        '3:          dtlTable.Columns.Add("orderdtlseq", Type.GetType("System.Int32"))
        '4:          dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        '5:          dtlTable.Columns.Add("orderqty", Type.GetType("System.Decimal"))
        '6:          dtlTable.Columns.Add("itemunit", Type.GetType("System.Int32"))
        '7:          dtlTable.Columns.Add("unitseq", Type.GetType("System.Int32"))
        '8:          dtlTable.Columns.Add("itemnote", Type.GetType("System.String"))
        '9:          dtlTable.Columns.Add("disctype", Type.GetType("System.String"))
        '10:         dtlTable.Columns.Add("discamtpct", Type.GetType("System.Decimal"))
        '11:         dtlTable.Columns.Add("disc", Type.GetType("System.Decimal"))
        '12:         dtlTable.Columns.Add("orderprice", Type.GetType("System.Decimal"))
        '13:         dtlTable.Columns.Add("total", Type.GetType("System.Decimal"))
        '14:         dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        '15:         dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        '16:         dtlTable.Columns.Add("ItemLDesc", Type.GetType("System.String"))
        '17:         dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        '            dtlTable.Columns.Add("amtdtldisc1", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("amtdtldisc2", Type.GetType("System.Decimal"))
        '            'dtlTable.Columns.Add("deliverydate", Type.GetType("System.DateTime"))
        '18:         dtlTable.Columns.Add("Itemgroupcode", Type.GetType("System.String"))
        '19:         dtlTable.Columns.Add("merk", Type.GetType("System.String"))
        '            'dtlTable.Columns.Add("free", Type.GetType("System.String"))
        '20:         dtlTable.Columns.Add("totalalokasi", Type.GetType("System.Decimal"))
        '21:         dtlTable.Columns.Add("promooid", Type.GetType("System.Int32")) 'promooid
        '22:         dtlTable.Columns.Add("totKotor", Type.GetType("System.Decimal"))
        '23:         dtlTable.Columns.Add("orderpriceidr", Type.GetType("System.Decimal"))
        '24:         dtlTable.Columns.Add("orderpriceusd", Type.GetType("System.Decimal"))
        '25:         dtlTable.Columns.Add("currency", Type.GetType("System.Decimal"))
        '26:         dtlTable.Columns.Add("branch_code", Type.GetType("System.String"))
        '27:         dtlTable.Columns.Add("varprice", Type.GetType("System.Decimal"))
        '28:         dtlTable.Columns.Add("tempAmtUsd", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("jenisprice", Type.GetType("System.String"))
        '            dtlTable.Columns.Add("priceitem", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("pricenota", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("pricekhusus", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("pricesilver", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("priceplatinum", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("pricegold", Type.GetType("System.Decimal"))
        '            dtlTable.Columns.Add("MinimPrice", Type.GetType("System.Decimal"))

        '            dtlDS.Tables.Add(dtlTable)
        '            Session("OrderedItemTemp") = dtlTable


        '        End If
        '        Dim objTable As DataTable
        '        objTable = Session("OrderedItemTemp")
        '        Dim objRow As DataRow
        '        For C1 As Integer = 0 To dtTemp.Rows.Count - 1
        '            objRow = objTable.NewRow()
        '            objRow("cmpcode") = dtTemp.Rows(C1).Item("cmpcode")
        '            objRow("orderdtloid") = dtTemp.Rows(C1).Item("orderoid").ToString.Trim
        '            objRow("ordermstoid") = dtTemp.Rows(C1).Item("ordermstoid").ToString.Trim
        '            objRow("orderdtlseq") = C1 + 1
        '            objRow("itemoid") = dtTemp.Rows(C1).Item("itemoid").ToString.Trim
        '            objRow("orderqty") = ToDouble(dtTemp.Rows(C1).Item("orderqty"))
        '            objRow("itemunit") = dtTemp.Rows(C1).Item("itemunitoid").ToString
        '            'objRow("packingqty") = ToDouble(dtTemp.Rows(C1).Item("packingunit"))
        '            objRow("amtdtldisc1") = ToMaskEdit(dtTemp.Rows(C1).Item("amtdtldisc1").ToString, 2)
        '            objRow("amtdtldisc2") = ToMaskEdit(dtTemp.Rows(C1).Item("amtdtldisc2").ToString, 2)
        '            objRow("itemnote") = dtTemp.Rows(C1).Item("itemnote")
        '            objRow("disctype") = dtTemp.Rows(C1).Item("disctype")
        '            If currencyoid.SelectedValue = "1" Then
        '                objRow("orderprice") = ToDouble(dtTemp.Rows(C1).Item("orderprice"))
        '                objRow("varprice") = ToMaskEdit(dtTemp.Rows(C1).Item("varprice").ToString, 2)
        '                objRow("tempAmtUsd") = ToMaskEdit(dtTemp.Rows(C1).Item("tempAmtUsd").ToString, 2)
        '                objRow("discamtpct") = ToMaskEdit(dtTemp.Rows(C1).Item("discamtpct").ToString, 2)
        '                objRow("disc") = ToMaskEdit(dtTemp.Rows(C1).Item("disc").ToString, 2)
        '                objRow("total") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("totKotor")), 2)
        '                objRow("totKotor") = ToMaskEdit(dtTemp.Rows(C1).Item("totKotor"), 2)
        '            Else
        '                objRow("orderprice") = ToDouble(dtTemp.Rows(C1).Item("orderprice"))
        '                objRow("varprice") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("varprice").ToString), 2)
        '                objRow("tempAmtUsd") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("tempAmtUsd").ToString), 2)
        '                objRow("discamtpct") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("discamtpct").ToString), 2)
        '                objRow("disc") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("disc").ToString), 2)
        '                objRow("total") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("totKotor")), 2)
        '                objRow("totKotor") = ToMaskEdit(ToDouble(dtTemp.Rows(C1).Item("totKotor")), 2)
        '            End If
        '            objRow("upduser") = dtTemp.Rows(C1).Item("upduser")
        '            objRow("updtime") = dtTemp.Rows(C1).Item("updtime")
        '            'objRow("deliverydate") = dtTemp.Rows(C1).Item("deliverydate")
        '            objRow("ItemLDesc") = dtTemp.Rows(C1).Item("ItemLDesc").ToString
        '            objRow("unit") = dtTemp.Rows(C1).Item("unit").ToString
        '            objRow("unitseq") = dtTemp.Rows(C1).Item("unitseq").ToString
        '            objRow("Itemgroupcode") = dtTemp.Rows(C1).Item("Itemgroupcode").ToString
        '            objRow("merk") = dtTemp.Rows(C1).Item("merk").ToString
        '            'objRow("free") = dtTemp.Rows(C1).Item("free").ToString
        '            objRow("totalalokasi") = dtTemp.Rows(C1).Item("totalalokasi").ToString
        '            objRow("promooid") = dtTemp.Rows(C1).Item("promooid").ToString
        '            objRow("currency") = dtTemp.Rows(C1).Item("currencyoid").ToString
        '            objRow("branch_code") = dtTemp.Rows(C1).Item("branch_code").ToString
        '            objRow("jenisprice") = dtTemp.Rows(C1).Item("jenisprice").ToString
        '            objRow("priceitem") = dtTemp.Rows(C1).Item("priceitem").ToString
        '            objRow("pricenota") = ToDouble(dtTemp.Rows(C1).Item("pricenota"))
        '            objRow("pricekhusus") = ToDouble(dtTemp.Rows(C1).Item("pricekhusus"))
        '            objRow("pricesilver") = ToDouble(dtTemp.Rows(C1).Item("pricesilver"))
        '            objRow("priceplatinum") = ToDouble(dtTemp.Rows(C1).Item("priceplatinum"))
        '            objRow("pricegold") = ToDouble(dtTemp.Rows(C1).Item("pricegold"))
        '            objRow("MinimPrice") = ToDouble(dtTemp.Rows(C1).Item("MinimPrice"))
        '            objTable.Rows.Add(objRow)
        '        Next
        '        Session("OrderedItemTemp") = objTable
        '        Session("ItemLine") = dtTemp.Rows.Count + 1
        '        trnorderdtlseq.Text = Session("ItemLine")
        '        Return Session("OrderedItemTemp")
    End Function

    Function checkData() As DataTable
        Dim ssql As String = "select CMPCODE,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME,OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval where CmpCode='" & CompnyCode & "' and RequestUser='" & Session("UserID") & "' and STATUSREQUEST LIKE 'New%' AND TABLENAME='QL_trnordermst' and OID=" & oid.Text & " and EVENT='APPROVAL-TOKEN'"
        Return cKoneksi.ambiltabel(ssql, "APPROVAL")
    End Function

    Function checkDataWeb() As DataTable
        Dim ssql As String = "select CMPCODE,REQUESTCODE,REQUESTUSER,REQUESTDATE,STATUSREQUEST,TABLENAME,OID,EVENT,APPROVALCODE,APPROVALUSER,APPROVALDATE from QL_Approval where CmpCode='" & CompnyCode & "' and RequestUser='" & Session("UserID") & "' and STATUSREQUEST LIKE 'Send'AND TABLENAME='QL_trnordermst' and OID=" & oid.Text & " and EVENT='APPROVAL-WEB'"
        Return cKoneksi.ambiltabel(ssql, "APPROVAL")
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub SubFunction()
        If orderstatus.Text = "Approved" Or orderstatus.Text = "POST" Or orderstatus.Text = "Closed" Or orderstatus.Text = "Canceled" Or orderstatus.Text = "Closed Manual" Then
            btnSave.Visible = False : btnDelete.Visible = False
            imbAdd.Visible = False : BtnApproval.Visible = False
            taxable.Enabled = False : taxable.CssClass = "inpTextDisabled"
            orderflag.Enabled = True : orderflag.CssClass = "inpTextDisabled"
            DDLWH.Enabled = False : DDLWH.CssClass = "inpTextDisabled"
            currencyoid.Enabled = False : currencyoid.CssClass = "inpTextDisabled"
            btnPost.Visible = False
            sSql = "Select genother4 from QL_mstgen Where gengroup='CABANG' AND gencode='" & CabangNya.SelectedValue & "'"
            If GetStrData(sSql).ToUpper = "YA" Then
                btnPrint.Visible = True
            Else
                btnPrint.Visible = False
            End If
        ElseIf orderstatus.Text = "In Approval" Then
            btnSave.Visible = False : imbAdd.Visible = True
            btnDelete.Visible = False : BtnApproval.Visible = False
            btnPrint.Visible = False : taxable.Enabled = False
            taxable.CssClass = "inpTextDisabled" : orderflag.Enabled = True
            orderflag.CssClass = "inpTextDisabled" : btnPost.Visible = False
        ElseIf orderstatus.Text = "In Process" Then
            btnPrint.Visible = False
            taxable.Enabled = False : taxable.CssClass = "inpTextDisabled"
            orderflag.Enabled = True : orderflag.CssClass = "inpText"
            If typeSO.SelectedValue = "Kanvas" Then
                btnPost.Visible = True : BtnApproval.Visible = False
            Else
                btnPost.Visible = False : BtnApproval.Visible = True
            End If
        ElseIf orderstatus.Text = "Revised" Then
            BtnApproval.Visible = True : btnPrint.Visible = False
            taxable.Enabled = False : taxable.CssClass = "inpTextDisabled"
            orderflag.Enabled = True : orderflag.CssClass = "inpText"
            btnPost.Visible = False
        ElseIf orderstatus.Text = "Rejected" Then
            btnSave.Visible = False : btnDelete.Visible = False
            BtnApproval.Visible = False : imbAdd.Visible = True
            btnPrint.Visible = False : btnPost.Visible = False
            taxable.Enabled = False : taxable.CssClass = "inpTextDisabled"
            orderflag.Enabled = True : orderflag.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub CreateDtlTbl()
        If Session("TabelDtl") Is Nothing Then
            Dim dtlDS As DataSet = New DataSet
            Dim dtlTable As DataTable = New DataTable("TrnItem")
            dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("orderdtloid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("ordermstoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("orderdtlseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("orderqty", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("itemunit", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("unitseq", Type.GetType("System.Int32"))
            dtlTable.Columns.Add("itemnote", Type.GetType("System.String"))
            dtlTable.Columns.Add("disctype", Type.GetType("System.String"))
            dtlTable.Columns.Add("discamtpct", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("disc", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("orderprice", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("total", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
            dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
            dtlTable.Columns.Add("itemLdesc", Type.GetType("System.String"))
            dtlTable.Columns.Add("merk", Type.GetType("System.String"))
            dtlTable.Columns.Add("unit", Type.GetType("System.String"))
            dtlTable.Columns.Add("itemgroupcode", Type.GetType("System.String"))
            dtlTable.Columns.Add("totalalokasi", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("promooid", Type.GetType("System.Int32")) 'promooid
            dtlTable.Columns.Add("totKotor", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("orderpriceidr", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("currency", Type.GetType("System.String"))
            dtlTable.Columns.Add("orderpriceusd", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("branch_code", Type.GetType("System.String"))
            dtlTable.Columns.Add("varprice", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("tempAmtUsd", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("amtdtldisc1", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("amtdtldisc2", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("jenisprice", Type.GetType("System.String"))
            dtlTable.Columns.Add("priceitem", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("pricenota", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("pricekhusus", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("pricesilver", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("priceplatinum", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("pricegold", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("MinimPrice", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("priceexpedisi", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("hpp", Type.GetType("System.Decimal"))
            dtlTable.Columns.Add("itemdtloid", Type.GetType("System.Int32"))

            dtlDS.Tables.Add(dtlTable)
            Session("TabelDtl") = dtlTable
            Session("vSeqGLDtl") = 0
        End If
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub InitPromo()
        sSql = "SELECT promoid,promeventname FROM QL_mstpromo WHERE cmpcode='MSC' and promflag<>'INACTIVE' AND (CURRENT_TIMESTAMP BETWEEN promstartdate AND dateadd(day,1,promfinishdate)) and CONVERT(varchar, CURRENT_TIMESTAMP, 108)>=promstarttime and CONVERT(varchar,CURRENT_TIMESTAMP, 108)<=promfinishtime and dayflag like '%'+CAST(datepart(weekday,current_timestamp) AS VARCHAR)+'%' and branchFlag like '%" & CabangNya.SelectedValue & "%' UNION Select distinct 0 promoid,'NONE' promeventname FROM QL_mstGen"
        FillDDL(ddlPromo, sSql)
    End Sub

    Private Sub InitCabangNya()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='CABANG'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(CabangNya, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(CabangNya, sSql)
            Else
                FillDDL(CabangNya, sSql)
                'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                'ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(CabangNya, sSql)
            'ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            'ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub PrintOut()
        Dim currency As String = GetStrData("select currencyoid from ql_trnordermst where ordermstoid=" & GVmstOrder.SelectedDataKey.Item(0) & " and branch_code='" & Session("branch_id") & "'")
        Try
            Dim sWhere As String
            sWhere = "and m.branch_code='" & Session("branch_id") & "' AND m.ordermstoid =  " & GVmstOrder.SelectedDataKey.Item(0)
            If currency = "1" Then
                report.Load(Server.MapPath("~/report/rptNotaSO.rpt"))
            Else
                report.Load(Server.MapPath("~/report/rptNotaSOusd.rpt"))
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("namaPencetak", Session("userid"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Order" & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()

        Catch ex As Exception
            showMessage(ex.Message, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            report.Close() : report.Dispose()
        End Try
    End Sub

    Private Sub DDLBranch()
        sSql = "Select gencode,gendesc From ql_mstgen Where gengroup='cabang'"

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlFromcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlFromcabang, sSql)
            Else
                FillDDL(ddlFromcabang, sSql)
                ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
                ddlFromcabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= "" : FillDDL(ddlFromcabang, sSql)
            ddlFromcabang.Items.Add(New ListItem("ALL", "ALL"))
            ddlFromcabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindDataSO()
        Try
            sSql = "SELECT od.branch_code, (Select gendesc from ql_mstgen vg Where vg.gengroup='CABANG' AND vg.gencode=od.branch_code) Cabang, od.ordermstoid, od.orderno, od.trnorderdate, c.custname, case when od.trnorderstatus='Revised' then od.trnordernote + '<br>' + 'Revisi:' + '<span style=color:red;>' + od.revisenote + '</span>' else od.trnordernote end trnordernote, od.trnorderstatus, od.trntaxpct, CONVERT(VARCHAR(8), od.updtime, 108) JamBuat FROM QL_trnordermst od inner join ql_mstcust c on od.trncustoid=c.custoid INNER JOIN QL_MSTPERSON p on od.salesoid = p.PERSONOID WHERE (" & DDLSearch.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilter.Text) & "%')"
            If ddlFromcabang.SelectedValue <> "ALL" Then
                sSql &= " AND od.branch_code='" & ddlFromcabang.SelectedValue & "'"
            End If

            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= " AND od.createuser='" & Session("UserID") & "'"
            End If

            If DDLStatus.SelectedValue <> "ALL" Then
                sSql &= " AND od.trnorderstatus ='" & DDLStatus.SelectedValue & "'"
            End If

            If chkPeriod.Checked = True Then
                Dim dat1 As Date = CDate(toDate(FilterPeriod1.Text))
                Dim dat2 As Date = CDate(toDate(FilterPeriod2.Text))
                If dat1 < CDate("01/01/1900") Then
                    showMessage("Filter tanggal salah!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    Exit Sub
                End If

                If dat1 > dat2 Then
                    showMessage("Tanggal Awal harus lebih kecil dari Tanggal Akhir!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If

                sSql &= " AND (convert(char(10),od.trnorderdate,121) BETWEEN '" & Format(CDate(toDate(FilterPeriod1.Text)), "yyyy-MM-dd") & "' AND '" & Format(CDate(toDate(FilterPeriod2.Text)), "yyyy-MM-dd") & "')"
            End If

            sSql &= " Order by od.trnorderdate Desc"
            Dim dataSo As DataTable = cKoneksi.ambiltabel(sSql, "Ql_mstcust")
            GVmstOrder.DataSource = dataSo
            GVmstOrder.DataBind() : GVmstOrder.Visible = True
            sSql = ""
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub fillTotalQty()
        Dim dIsc1Tot As Double = 0 : Dim Disc2Tot As Double = 0
        Try
            totalqty.Text = ToDouble(OrderQty.Text)
            If DDLFree.SelectedValue = "Y" Then
                TxtTotKotor.Text = "0.00" 'Tot.Text = "0.00"
            Else
                dIsc1Tot = ToDouble(Discdtl1.Text * totalqty.Text)
                Disc2Tot = ToDouble(Discdtl2.Text * totalqty.Text)
                TxtTotKotor.Text = ToMaskEdit((ToDouble(OrderQty.Text.Trim) * ToDouble(OrderPrice.Text.Trim)) - (ToDouble(dIsc1Tot + Disc2Tot)), 3)
            End If

        Catch ex As Exception
            totalqty.Text = "0.00" : TxtTotKotor.Text = "0.00" ': Tot.Text = "0.00"
        End Try
    End Sub

    Private Sub ReAmount()
        totalorder.Text = ToMaskEdit(ToDouble(orderamount.Text), 3)
    End Sub

    Private Sub BindData(ByVal filterCompCode As String)
        sSql = "SELECT QL_trnordermst.orderoid,QL_trnordermst.orderno,QL_trnordermst.orderdate as Orderdate,QL_trnordermst.ordernote,QL_mstcust.custname,QL_trnordermst.orderstatus,QL_trnordermst.taxable FROM QL_trnordermst,QL_mstprof Where QL_mstprof.userid=QL_trnordermst.pic and QL_trnordermst.cmpcode LIKE '%" & CompnyCode & "%'"

        If Session("ddlFilterIndex") Is Nothing = False Then
            If DDLSearch.SelectedValue = "orderoid" Then
                sSql &= " AND UPPER(QL_trnordermst." & Session("ddlFilterIndex") & ") LIKE '%" & Tchar(txtFilter.Text.Trim) & "%' "
            Else
                sSql &= " AND UPPER(QL_mstcust." & Session("ddlFilterIndex") & ") LIKE '%" & Tchar(txtFilter.Text) & "%' "

            End If
        End If

        If filterCompCode <> "" Then
            sSql &= " ORDER BY QL_trnordermst.periodacctg desc,right(QL_trnordermst.orderno,4) desc,QL_trnordermst.orderdate desc"
        Else
            sSql &= " ORDER BY QL_trnordermst.periodacctg desc,right(QL_trnordermst.orderno,4) desc,QL_trnordermst.orderdate desc"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstOrder.DataSource = objDs.Tables("data")
        GVmstOrder.DataBind()
    End Sub

    Private Sub bindordereditem(ByVal sRefCode As Integer, ByVal sBranch As String)
        Try
            gvItemOrdered.Visible = True
            sSql = "SELECT f.cmpcode as cmpcode, f.branch_code, f.varprice, f.trnorderdtloid as orderdtloid, f.trnordermstoid AS ordermstoid, f.itemoid as itemoid, f.trnorderdtlseq orderdtlseq, f.trnorderdtlqty as orderqty, f.trnorderdtlnote as itemnote, f.trnorderprice as orderprice, f.trnorderprice as orderpriceidr, f.trnorderprice AS orderpriceusd, f.trnamountdtlidr total, f.tempAmtUsd, f.upduser as upduser, f.updtime as updtime, trnorderdtlunitoid itemunitoid, 'UNIT' as unit, f.unitseq, f.trnorderdtlunitoid As itemunit, i.merk, 'AMOUNT' disctype, 0.00 discamtpct, trndiskonpromo disc, promooid, f.trnamountdtlidr totKotor, i.itemdesc as ItemLDesc, f.jenisprice, g5.gendesc itemgroupcode, 0.00 AS totalalokasi, '1' currency, f.currencyoid, f.amtdtldisc1, f.amtdtldisc2, f.pricenormal priceitem, f.pricenota, f.pricekhusus, f.pricesilver, f.priceplatinum, f.pricegold, f.priceminim MinimPrice, f.priceexpedisi, f.hpp, f.itemdtloid FROM QL_trnorderdtl f INNER JOIN QL_mstitem i on f.itemoid=i.itemoid INNER JOIN ql_mstgen g5 on g5.genoid=i.itemgroupoid AND g5.gengroup='ITEMGROUP' WHERE f.trnordermstoid=" & sRefCode & " And f.branch_code ='" & sBranch & "' ORDER BY orderdtlseq"
            Session("TabelDtl") = cKoneksi.ambiltabel(sSql, "Ql_sodtl")
            gvItemOrdered.Visible = True
            gvItemOrdered.DataSource = Session("TabelDtl")
            gvItemOrdered.DataBind()
            TotOrder() : ReAmount()
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub ClearTextBox()
        custoid.Text = "" : CustName.Text = "" : deliveryaddr.Text = ""
        'custcity.Text = ""
        ordernote.Text = "" ' curratesUSD.Text = "0.00"
        refoid.Text = "" : orderstatus.Text = "In Process"
        'orderlastprint.Text = "" : refno.Text = ""
        currencyoid.SelectedIndex = 0
        Session("ItemLine") = 1
        Session("TabelOrderdesign") = Nothing
        Session("TabelDtl") = Nothing
        itemoid.Text = "" : itemlongdesc.Text = ""
    End Sub

    Private Sub BindDataCustomer(ByVal sQueryplus As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim mysql As String
        mysql = "SELECT c.custoid as Id,c.custname as Name,c.custaddr as Address,g.gendesc as City from QL_mstcust c, QL_mstgen g WHERE c.cmpcode LIKE '%" & CompnyCode & "%' " & sQueryplus & "AND c.custflag='ACTIVE' and g.genoid=c.custcityoid ORDER BY c.custoid DESC"

        Dim objTableRef As DataTable
        Dim objRowRef() As DataRow

        objTableRef = isiDS(mysql, "QL_mstcust")
        Session("Customer") = objTableRef
        objRowRef = Session("Customer").Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        gvCustomer.Visible = True
        If objRowRef.Length > 0 Then
            gvCustomer.DataSource = Session("Customer")
            gvCustomer.DataBind()
            gvCustomer.Visible = True
        Else
            gvCustomer.Visible = False
        End If
        mySqlConn.Close()
    End Sub

    Private Sub BindDataItem(ByVal sQueryplus As String)
        Try
            Dim pRice As String = "", GroupGudang As String = ""
            If jPrice.SelectedValue = "NORMAL" Then
                pRice = "ISNULL(id.priceNormal,a.pricelist)"
            ElseIf jPrice.SelectedValue = "NOTA" Then
                pRice = " ISNULL(id.pricenota,a.itempriceunit1)"
            ElseIf jPrice.SelectedValue = "SILVER" Then
                pRice = " ISNULL(id.pricesilver,a.pricesilver)"
            ElseIf jPrice.SelectedValue = "PLATINUM" Then
                pRice = " ISNULL(id.priceplatinum,a.priceplatinum)"
            ElseIf jPrice.SelectedValue = "GOLD" Then
                pRice = " ISNULL(id.pricegold,a.pricegold)"
            ElseIf jPrice.SelectedValue = "KHUSUS" Then
                pRice = "ISNULL(id.pricekhusus,a.itempriceunit2)"
            End If

            If typeSO.SelectedValue = "ecommers" Then
                GroupGudang = " AND glo.genother6='ECOMMERCE' AND glo.genother4<>'KANVAS'"
            ElseIf typeSO.Text = "Kanvas" Then
                GroupGudang = " AND glo.genother6='UMUM' AND glo.genother4='KANVAS'"
            Else
                GroupGudang = " AND glo.genother6='UMUM' AND glo.genother4<>'KANVAS'"
            End If

            sSql = "SELECT top 200 * FROM (Select DISTINCT a.itemoid, a.itemcode, ISNULL(id.hpp,a.HPP) HPP, a.itemdesc, a.itemflag, a.merk, a.updtime, isnull((select gendesc from QL_mstgen Where genoid = a.itemgroupoid ),'') itemgroupcode, " & pRice & " priceList, ISNULL((Select SUM(con.qtyIn)-SUM(qtyOut) From QL_conmtr con Where con.refoid=a.itemoid AND con.periodacctg='" & identifierno.Text & "' AND con.branch_code='" & CabangNya.SelectedValue & "' AND con.mtrlocoid IN (Select glo.genoid From QL_mstgen glo Where glo.genoid=con.mtrlocoid " & GroupGudang & " AND gengroup='LOCATION')),0.00) SALDOAKHIR, a.priceList price, 0.00 netto, ISNULL(id.pricenota, a.itempriceunit1) pricenota, ISNULL(id.priceNormal, a.pricelist) priceNormal, ISNULL(id.pricekhusus,a.itempriceunit2) pricekhusus, a.discountunit1 discunit1, a.discountunit2 discunit2, a.discountunit3 discunit3, a.pricelist PriceNya, '' typebarang, a.pricelist priceitem, '' promtype, 'NON' StatusBarang, 0.00 QtyTuku, 0 promoid, a.satuan1, a.itemgroupoid, 'BUAH' unit, 0.00 discnya, 2 NoMerNya, ISNULL(id.pricesilver, a.pricesilver) pricesilver, ISNULL(id.priceplatinum, a.priceplatinum) priceplatinum, ISNULL(id.pricegold,a.pricegold) pricegold, ISNULL(id.MinimPrice, a.bottompricegrosir) MinimPrice, ISNULL(id.priceexpedisi,0.00) priceexpedisi, ISNULL(id.itemdtloid,0) itemdtloid From ql_mstitem a Outer Apply (SELECT TOP 1 id.itemoid, id.branchcode, id.pricesilver, id.priceplatinum, id.pricegold, id.priceminim MinimPrice, id.priceexpedisi, id.pricenota, id.priceNormal, id.pricekhusus, id.hpp, id.itemdtloid FROM QL_mstpricedtl id Where id.branchcode='" & CabangNya.SelectedValue & "' And id.itemoid=a.itemoid Order BY createtime DESC) id Where (a.itemdesc LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' OR a.itemcode LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' or a.merk LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' OR itembarcode1 LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%') and a.stockflag IN ('T','V')"
            If ddlPromo.SelectedValue <> "0" Then
                sSql &= " UNION ALL Select DISTINCT a.itemoid, a.itemcode, ISNULL(id.hpp,a.HPP) HPP, a.itemdesc, a.itemflag, a.merk, a.updtime, isnull((select gendesc from QL_mstgen where genoid = a.itemgroupoid ),'') itemgroupcode, CASE pm.promtype WHEN 'BUNDLING' Then subsidi1 WHEN 'DISCAMT' Then subsidi1 ELSE 0.00 END priceList, ISNULL((Select Sum(qtyIn)-SUM(qtyout) From ql_conmtr con Where con.refoid=a.itemoid AND periodacctg='" & identifierno.Text & "' AND con.branch_code='" & CabangNya.SelectedValue & "' AND con.mtrlocoid IN ( Select glo.genoid From QL_mstgen glo Where glo.genoid=con.mtrlocoid " & GroupGudang & " AND gengroup='LOCATION')),0.00) SALDOAKHIR, a.priceList price, 0.00 netto, CASE pm.promtype WHEN 'BUNDLING' Then subsidi1 WHEN 'DISCAMT' Then subsidi1 ELSE 0.00 END pricenota, CASE pm.promtype WHEN 'BUNDLING' Then subsidi1 WHEN 'DISCAMT' Then subsidi1 ELSE 0.00 END priceNormal, ISNULL(id.pricekhusus,a.itempriceunit2) PriceKhusus, a.discountunit1 discunit1, a.discountunit2 discunit2, a.discountunit3 discunit3, Case typebarang When 'UTAMA' then (CASE pm.promtype WHEN 'BUNDLING' Then subsidi1 WHEN 'DISCAMT' Then subsidi1 ELSE subsidi1 END) else 0.00 End PriceNya, pd.typebarang, a.pricelist priceitem, pm.promtype, 'PROMO' StatusBarang, pd.qtyitemoid1 QtyTuku, pd.promoid, a.satuan1, a.itemgroupoid, 'BUAH' unit, CASE pm.promtype WHEN 'DISCAMT' Then pd.totalcashback Else pd.discnya END discnya, 1 NoMerNya, ISNULL(id.pricesilver,a.pricesilver) pricesilver, ISNULL(id.priceplatinum,a.priceplatinum) priceplatinum, ISNULL(id.pricegold,a.pricegold) pricegold, ISNULL(id.MinimPrice, a.bottompricegrosir) MinimPrice, ISNULL(id.priceexpedisi,0.00) priceexpedisi, ISNULL(id.itemdtloid,0) itemdtloid From ql_mstitem a INNER JOIN QL_mstpromodtl pd ON pd.itemoid=a.itemoid INNER JOIN QL_mstpromo pm ON pm.promoid=pd.promoid Outer Apply (SELECT TOP 1 id.itemoid, id.branchcode, id.pricesilver, id.priceplatinum, id.pricegold, id.priceminim MinimPrice, id.priceexpedisi, id.pricenota, id.priceNormal, id.pricekhusus, id.hpp, id.itemdtloid FROM QL_mstpricedtl id Where id.branchcode='" & CabangNya.SelectedValue & "' AND id.itemoid=a.itemoid Order BY createtime DESC) id Where (a.itemdesc LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' OR a.itemcode LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' OR a.merk LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%' OR itembarcode1 LIKE '%" & TcharNoTrim(itemlongdesc.Text.ToUpper) & "%') and a.stockflag IN ('T','V') AND pd.promoid='" & ddlPromo.SelectedValue & "' AND pd.typebarang='UTAMA'"
            End If

            sSql &= ") As so Where itemflag ='AKTIF' ORDER BY NoMerNya, itemdesc ASC"
            Session("QL_mstitem") = cKoneksi.ambiltabel(sSql, "QL_mstitem")
            gvItemSearch.DataSource = Session("QL_mstitem")
            gvItemSearch.DataBind() : gvItemSearch.SelectedIndex = -1
        Catch ex As Exception
            showMessage(ex.ToString & "</br />" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub BindItemBonus()
        sSql = "SELECT * FROM ( Select distinct a.itemoid,a.itemcode,a.itemdesc,a.merk,isnull((select gendesc from QL_mstgen where genoid = a.itemgroupoid ),'') itemgroupcode,a.itemflag,subsidi1 AS priceList,Case typebarang When 'UTAMA' then pd.subsidi1 else (CASE pm.promtype WHEN 'BUNDLING' Then subsidi1 ELSE 0.00 END) End PriceNya,pd.typebarang,pm.promtype,'PROMO' StatusBarang,pd.qtyitemoid1 QtyTuku,pd.promoid,a.satuan1,itemgroupoid,isnull((select gendesc from QL_mstgen where genoid = a.satuan1 AND gengroup='ITEMUNIT'),'') unit,pd.totalamt From ql_mstitem a LEFT join QL_mstItem_branch b on a.itemcode = b.itemcode AND b.branch_code='" & CabangNya.SelectedValue & "' INNER JOIN QL_mstpromodtl pd ON pd.itemoid=a.itemoid INNER JOIN QL_mstpromo pm ON pm.promoid=pd.promoid Where a.stockflag='T' and pm.promoid='" & ddlPromo.SelectedValue & "') As so Where itemflag ='AKTIF' ORDER BY itemoid ASC"
        Dim data As DataTable = cKoneksi.ambiltabel(sSql, "ItemBonus")
        Session("ItemBonus") = data
    End Sub

    Private Sub initUnit()
        Try
            Dim sSql As String
            sSql = "Select Distinct g.genoid,g.gendesc from ql_mstgen g inner join ql_mstITEM m on g.genoid=m.satuan1 or g.genoid = m.satuan1 or g.genoid = m.satuan1 and g.gengroup='ITEMUNIT' where m.itemoid = '" & itemoid.Text & "' and m.ITEMDESC LIKE '%" & Tchar(itemlongdesc.Text) & "%' group by g.genoid,g.gendesc"
            FillDDL(ddlUnit, sSql)
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub fillTextBox(ByVal sBranch As String, ByVal vjurnaloid As String)
        Try
            CabangNya.Enabled = False
            Dim mySqlConn As New SqlConnection(ConnStr)
            sSql = "SELECT o.branch_code, cg.custgroupoid, cg.custgroupname, o.cmpcode, o.branch_code to_branch, o.ordermstoid, o.typeso, o.bundling, o.timeofpaymentSO, o.rate2oid, o.currencyoid, o.orderno, o.trnorderdate, o.periodacctg, o.trnordertype, o.trnorderstatus, o.ordertaxtype, o.consigneeoid, o.trnordernote, o.trntaxpct, o.upduser, delivdate deliverydate, o.updtime, o.finalapprovalcode, o.finalapprovaluser, o.finalappovaldatetime, c.custname as username, o.trncustoid, o.trnorderref, spgoid, o.promooid, o.createtime, o.sobo, o.flagtax, o.typeexpedisi, amtdisc1, amtdisc2, flagCash, flagSR, ordernumber, endusername, noinvoiceonline, noawb, shippingaddr, toaddr From QL_trnordermst o inner join ql_mstcust c on c.custoid = o.trncustoid left join QL_mstcustgroup cg on c.custgroupoid = cg.custgroupoid WHERE o.ordermstoid=" & vjurnaloid & " AND o.branch_code = '" & sBranch & "'"

            Dim mySqlDA As New SqlClient.SqlDataAdapter(sSql, ConnStr)
            Dim objDs As New DataSet
            Dim objTable As DataTable
            Dim objRow() As DataRow
            mySqlDA.Fill(objDs)
            objTable = objDs.Tables(0)

            deliverydate.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length > 0 Then
                InitCabangNya()
                CabangNya.SelectedValue = objRow(0)("branch_code").ToString
                oid.Text = Trim(objRow(0)("ordermstoid").ToString)
                orderdate.Text = Format(CDate(objRow(0)("trnorderdate").ToString), "dd/MM/yyyy")
                deliverydate.Text = Format(CDate(objRow(0)("deliverydate").ToString), "dd/MM/yyyy")
                identifierno.Text = GetDateToPeriodAcctg(CDate(objRow(0)("trnorderdate").ToString))
                orderno.Text = Trim(objRow(0)("orderno").ToString)
                custoid.Text = Trim(objRow(0)("trncustoid").ToString)
                CustName.Text = Trim(objRow(0)("userName").ToString)
                orderflag.Text = Trim(objRow(0)("trnordertype").ToString)
                deliveryaddr.Text = GetStrData("select custaddr +','+isnull((select gendesc from QL_mstgen where genoid = custcityoid)+',','')+ isnull((select gendesc from QL_mstgen where genoid = custprovoid) + ' ,','') + isnull((select gendesc from QL_mstgen where genoid = custcountryoid),'') from ql_mstcust where custoid= '" & custoid.Text & "'")
                ordernote.Text = Trim(objRow(0)("trnordernote").ToString)
                orderstatus.Text = Trim(objRow(0)("trnorderstatus").ToString)

                ddlSales()
                spgOid.SelectedValue = Trim(objRow(0)("spgoid").ToString)
                typeSO.SelectedValue = Trim(objRow(0)("typeso").ToString)
                POref.Text = Trim(objRow(0)("trnorderref").ToString)
                DDLTop.SelectedValue = Trim(objRow(0)("timeofpaymentSO").ToString)
                CustNameGroup.Text = Trim(objRow(0)("custgroupname").ToString)
                custgroupoid.Text = Trim(objRow(0)("custgroupoid").ToString)
                currencyoid.SelectedValue = Trim(objRow(0)("currencyoid").ToString)
                taxable.SelectedValue = Trim(objRow(0)("ordertaxtype").ToString)
                ddlexpedisi.SelectedValue = (Trim(objRow(0)("typeExpedisi").ToString))
                DiscHdr1.Text = (objRow(0)("amtdisc1").ToString)
                DiscHdr2.Text = (objRow(0)("amtdisc2").ToString)
                createtime.Text = Format(objRow(0)("createtime"), "dd/MM/yyyy HH:mm:ss.fff")
                cbCash.Checked = IIf(objRow(0)("flagCash").ToString = "Y", True, False)
                cbSR.Checked = IIf(objRow(0)("flagSR").ToString = "Y", True, False)
                DDLWH.SelectedValue = sBranch
                NoOrder.Text = objRow(0)("ordernumber").ToString
                NamaEndUSer.Text = objRow(0)("endusername").ToString
                NoInvOnline.Text = objRow(0)("noinvoiceonline").ToString
                NoAwb.Text = objRow(0)("noawb").ToString
                ShipppingAddres.Text = objRow(0)("shippingaddr").ToString
                ToAddress.Text = objRow(0)("toaddr").ToString
                lblbundling.Text = Trim(objRow(0)("bundling").ToString)
                CurrOid.Text = 1 : rate2oid.Text = 1

                If taxable.SelectedValue = "INC" Then
                    txtVarPrice.Visible = True : lblVarPrice.Visible = True
                    lbltax.Text = 1 : chkTax.Checked = True
                Else
                    lbltax.Text = 0 : chkTax.Checked = False
                    txtVarPrice.Visible = False : lblVarPrice.Visible = False
                End If

                sSql = "SELECT promoid, promeventname + ' - ' + promtype promeventname FROM QL_mstpromo WHERE cmpcode='" & CompnyCode & "' and promflag<>'INACTIVE' AND ('" & Format(CDate(Trim(objRow(0)("createtime").ToString)), "yyyy-MM-dd HH:mm:ss") & "' BETWEEN promstartdate AND dateadd(day,1,promfinishdate)) and '" & Format(CDate(Trim(objRow(0)("createtime").ToString)), "HH:mm:ss") & "' >=promstarttime and '" & Format(CDate(Trim(objRow(0)("createtime").ToString)), "HH:mm:ss") & "' <=promfinishtime and dayflag LIKE '%'+CAST(datepart(weekday,'" & Format(CDate(Trim(objRow(0)("createtime").ToString)), "yyyy-MM-dd HH:mm:ss") & "') AS VARCHAR)+'%' UNION Select distinct 0 promoid,'NONE' promeventname from QL_mstGen "
                FillDDL(ddlPromo, sSql)
                ddlPromo.SelectedValue = Trim(objRow(0)("promooid").ToString)
                If ddlPromo.SelectedValue <> "0" Then
                    ddlPromo.Enabled = False : ddlPromo.CssClass = "inpTextDisabled"
                End If
                updUser.Text = Trim(objRow(0)("upduser").ToString)
                updTime.Text = Trim(objRow(0)("updtime").ToString)

                mySqlConn.Close()
                bindordereditem(vjurnaloid, sBranch)
                SubFunction() : ControlEcommers()
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "</br />" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub showsearchitem()
        gvItemSearch.Visible = True
    End Sub

    Private Sub hideSearchItem()
        gvItemSearch.Visible = False
    End Sub

    Public Sub InitAllDDL()

        mysql = "Select gencode,gendesc from ql_mstgen where gengroup like '%Cabang%' and cmpcode='" & CompnyCode & "'"
        FillDDL(DDLWH, mysql)

        mysql = "select currencyoid,currencycode+' - '+currencydesc AS currencycode from QL_mstcurr"
        FillDDL(currencyoid, mysql)

        mysql = "select genoid ,gendesc from ql_mstgen where cmpcode = '" & CompnyCode & "' and gengroup = 'PAYTYPE' "
        FillDDL(DDLTop, mysql)
    End Sub

    Private Sub ddlSales()
        Try
            sSql = "SELECT pr.PERSONOID, UPPER(PERSONNAME) PERSONNAME FROM QL_mstperson pr Inner Join QL_MSTPROF pf ON pf.personnoid=pr.PERSONOID Inner Join QL_mstgen jp ON jp.genoid=pr.personstatus AND jp.gengroup='JOBPOSITION' WHERE pr.CMPCODE = '" & CompnyCode & "' AND STATUSPROF='Active' AND STATUS='AKTIF' AND (genother2='YES' OR FLAGSALES='YES') AND pf.BRANCH_CODE='" & CabangNya.SelectedValue & "'"
            If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
                sSql &= "AND pf.USERID='" & Session("UserID") & "'"
            End If
            sSql &= " order by personname"
            FillDDL(spgOid, sSql)

            Dim OidPerson As String = GetStrData("Select PERSONOID from QL_mstperson Where PERSONOID IN (Select personnoid from QL_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & CabangNya.SelectedValue & "')")
            If OidPerson <> "?" Then
                spgOid.SelectedValue = Integer.Parse(OidPerson)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & "- WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Private Sub generateNoOrder()
        orderno.Text = GenerateIDbranch("QL_trnordermst", CabangNya.SelectedValue)
    End Sub

    Private Sub FilterGVCust(ByVal filterName As String, ByVal filtercode As String)
        gvCustomer.DataBind()
    End Sub

    Private Sub BindDataCustGroup(ByVal sQueryplus As String)
        sSql = "SELECT custgroupoid, custgroupcode,custgroupname, custgroupaddr,ISNULL(case custgroupcreditlimitrupiah when 0 then -1 else custgroupcreditlimitrupiah-isnull(abs(custgroupcreditlimitusagerupiah),0) end,0.0000) crlimit FROM ql_mstcustgroup WHERE ((cmpcode LIKE '%" & CompnyCode & "%') and custgroupflag = 'Active' AND (custgroupcode like '%" & Tchar(CustNameGroup.Text) & "%' or custgroupname like '%" & Tchar(CustNameGroup.Text) & "%')) /*AND branch_code='" & CabangNya.SelectedValue & "'*/ ORDER BY custgroupcode"
        FillGV(gvCustomerGroup, sSql, "ql_mstcustgroup")
    End Sub

    Private Sub BindDataCust(ByVal sQueryplus As String)
        Dim sqlPlus As String = ""
        If CustNameGroup.Text <> "" Then
            sqlPlus = "AND (cg.custgroupoid like '%" & custgroupoid.Text & "%')"
        End If

        sSql = "SELECT [CUSTOID],[CUSTCODE],[CUSTNAME],[CUSTFLAG],[CUSTADDR],custpaytermdefaultoid,c.salesoid,c.timeofpayment, custcurrdefaultoid,/*Case c.custgroupoid When 0 Then ISNULL(c.custcreditlimitrupiah-custcreditlimitusagerupiah,0.00) Else isnull((case cg.custgroupcreditlimitrupiah when 0 then -1 else cg.custgroupcreditlimitrupiah-abs(cg.custgroupcreditlimitusagerupiah) end),-1) End crlimit*/ISNULL(c.custcreditlimitrupiah-(Case When custcreditlimitusagerupiah<0 Then ABS(custcreditlimitusagerupiah) else custcreditlimitusagerupiah end) ,0.0000) crlimit FROM [QL_MSTCUST] c left join QL_mstcustgroup cg on cg.custgroupoid = c.custgroupoid WHERE ((c.[CMPCODE] LIKE '%" & CompnyCode & "%') and [CUSTFLAG] = 'Active' and c.branch_code = '" & CabangNya.SelectedValue & "' AND (custcode like '%" & Tchar(CustName.Text) & "%' or custname like '%" & Tchar(CustName.Text) & "%') " & sqlPlus & " " & sQueryplus & ") ORDER BY custcode"
        FillGV(gvCustomer, sSql, "QL_MSTCUST")
    End Sub

    Private Sub TotOrder()
        If Not Session("TabelDtl") Is Nothing Then
            Dim dtb As DataTable = Session("TabelDtl")
            If dtb.Rows.Count > 0 Then
                ddisc.Text = dtb.Compute("SUM(disc)", "")
                DiscHdr1.Text = dtb.Compute("SUM(amtdtldisc1)", "")
                DiscHdr2.Text = dtb.Compute("SUM(amtdtldisc2)", "")
                totKotor.Text = dtb.Compute("SUM(total)", "")
                totalorder.Text = ToMaskEdit(totKotor.Text, 3)
                orderamount.Text = ToMaskEdit(totKotor.Text, 3)
                granddisc.Text = ToMaskEdit(ToDouble(ddisc.Text) + ToDouble(DiscHdr1.Text) + ToDouble(DiscHdr2.Text), 3)
            End If
        End If
    End Sub

    Private Sub clearControlAddItem()
        I_U2.Text = "New Detail"
        itemoid.Text = "" : itemlongdesc.Text = "" : ItemGroup.Text = ""
        Merk.Text = "" : Konversike3.Text = ""
        TxtTotKotor.Text = "0.00" ': Tot.Text = "0.00"
        txtVarPrice.Text = "0.00" : OrderQty.Text = "0" : OrderPrice.Text = "0.00"
        totalqty.Text = "0.00" : ItemNote.Text = ""
        disctype.SelectedIndex = 0 : DiscPct.Text = "0.00"
        disc.Text = "0.00" : discount.Text = "(Amount)"
        DDLFree.SelectedIndex = 0 : gvItemOrdered.SelectedIndex = -1
        OrderPrice.CssClass = "inpText" : OrderPrice.Enabled = True
        ddlUnit.Items.Clear() : lbltempAmt.Text = ""
        Discdtl1.Text = "0" : Discdtl2.Text = "0"
    End Sub

    Private Sub showPrintExcel(ByVal name As String, ByVal oid As Integer)

        ' lblkonfirmasi.Text = ""
        Session("diprint") = "False"
        Response.Clear()
        Dim swhere As String = "" : Dim shaving As String = "" : Dim swhere2 As String = ""

        ' If dView.SelectedValue = "Master" Then
        sSql = "SELECT m.orderno,c.custname,cr.currencycode,m.ordercurrate,m.orderdate,m.orderflag,m.orderstatus,m.custoid,m.deliveryaddr,m.ordercurroid,m.taxable,m.orderamttax,m.tradecityoid,m.market,f.orderdtloid as orderoid,g.gendesc as paytype,f.ordermstoid as ordermstoid,f.itemoid as itemoid, f.orderqty as orderqty,f.itemnote as itemnote,f.orderprice as orderprice,(isnull((select orderprice from ql_trnorderdtl where orderdtloid=f.orderdtloid and free='N'),0)*f.orderqty)-f.disc as total, f.upduser as upduser,f.updtime as updtime, itemunitoid, g1.gendesc as unit ,f.unitoid as itemunit, ISNULL(deliverydate,getdate()) AS plandeliverydate, f.disctype, f.discamtpct, f.disc ,f.itemdesc as ItemLDesc,f.itemgroupcode,f.free  FROM QL_trnorderdtl f inner join QL_trnordermst m on m.orderoid = f.ordermstoid inner join QL_mstcust c on m.custoid=c.custoid inner join ql_mstgen g on g.genoid=m.paytermoid inner join QL_mstitem i on i.itemoid=f.itemoid inner join QL_mstgen g1 on g1.genoid=i.itemunitoid inner join QL_mstcurr cr on cr.currencyoid=m.ordercurroid WHERE f.cmpcode LIKE 'SIP' AND f.itemoid=i.itemoid and f.ordermstoid=" & oid

        Response.AddHeader("content-disposition", "inline;filename=StatusIO.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
    End Sub

    Private Sub CalculateTotalAlokasi(ByVal iItemOid As Integer)
        Dim tblAlloc As DataTable : tblAlloc = Session("TabelAllocDtl")
        Dim dTotalAlloc As Double = 0
        If Not tblAlloc Is Nothing Then
            dTotalAlloc = ToDouble(tblAlloc.Compute("SUM(allocationqty)", "itemoid=" & iItemOid).ToString)
        End If

    End Sub

    Private Sub CekQtyToAllocation()
        Dim tbDtl As DataTable : tbDtl = Session("TabelDtl")
        Dim bState As Boolean = False
        If Not tbDtl Is Nothing Then
            For C1 As Integer = 0 To tbDtl.Rows.Count - 1
                If ToDouble(tbDtl.Rows(C1)("totalalokasi").ToString) > ToDouble(tbDtl.Rows(C1)("orderqty").ToString) Then
                    bState = True : Exit For
                End If
            Next
        End If
    End Sub

    Private Sub ControlEcommers()
        If typeSO.SelectedValue = "ecommers" Then
            LblNoOrderOnLine.Visible = True : NoOrder.Visible = True
            LblNamaEndUSer.Visible = True : NamaEndUSer.Visible = True
            LblNoInvOnline.Visible = True : NoInvOnline.Visible = True
            LblNoAwb.Visible = True : NoAwb.Visible = True
            LblShippingAddr.Visible = True : ShipppingAddres.Visible = True
            LblToAddres.Visible = True : ToAddress.Visible = True
        Else
            LblNoOrderOnLine.Visible = False : NoOrder.Visible = False
            LblNamaEndUSer.Visible = False : NamaEndUSer.Visible = False
            LblNoInvOnline.Visible = False : NoInvOnline.Visible = False
            LblNoAwb.Visible = False : NoAwb.Visible = False
            LblShippingAddr.Visible = False : ShipppingAddres.Visible = False
            LblToAddres.Visible = False : ToAddress.Visible = False

            NoOrder.Text = "" : NamaEndUSer.Text = "" : NoInvOnline.Text = ""
            NoAwb.Text = "" : ShipppingAddres.Text = "" : ToAddress.Text = ""
        End If
    End Sub

    Private Sub cekpromo()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        'cek promo available range tanggal & hari2 promo berdasarkan CURRENT_TIMESTAMP
        sSql = "SELECT count(-1) FROM QL_mstpromo WHERE cmpcode='" & CompnyCode & "'  and promflag<>'INACTIVE' AND (CURRENT_TIMESTAMP BETWEEN promstartdate AND dateadd(day,1,promfinishdate)) and CONVERT(varchar, CURRENT_TIMESTAMP, 108)>=promstarttime and CONVERT(varchar,CURRENT_TIMESTAMP, 108)<=promfinishtime and dayflag like '%'+CAST(datepart(weekday,current_timestamp) AS VARCHAR)+'%' "
        xCmd.CommandText = sSql
        If xCmd.ExecuteScalar() > 0 Then
            'cek item2 yg d pilih yang ada di promo tersebut ada atau tidak khusus diskon
            sSql = "SELECT count(-1) From QL_mstpromo p inner join QL_mstpromodtl pd on p.promoid = pd.promoid where pd.itemoid = " & itemoid.Text & " and p.promtype in ('Disc % (Item Only)','Disc Quantity') and p.cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar() > 0 Then
                'cek promo diskon tipe apa
                sSql = "select p.promtype from QL_mstpromo p inner join QL_mstpromodtl pd on p.promoid = pd.promoid where pd.itemoid = " & itemoid.Text & " and p.promtype in ('Disc % (Item Only)','Disc Quantity') and p.cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                Dim tipe As String = xCmd.ExecuteScalar
                If tipe = "Disc % (Item Only)" Then 'diskon item only 
                    OrderPrice.Text = ToMaskEdit(OrderPrice.Text - (OrderPrice.Text * GetStrData("SELECT subsidi1 FROM ql_mstpromodtl d WHERE d.itemoid = '" & itemoid.Text & "'") / 100), 3)
                End If
            End If
        End If
        conn.Close()
    End Sub

    Private Sub editharganota()
        Dim cek As String = GetStrData("select editharga from ql_mstITEM m where m.itemoid = '" & itemoid.Text & "' ")
        If cek = "F" Then
            If currencyoid.SelectedValue = "1" Then
                OrderPrice.CssClass = "inpTextDisabled"
                OrderPrice.Enabled = False
            Else
                OrderPrice.CssClass = "inpText"
                OrderPrice.Enabled = True
            End If
        Else
            OrderPrice.CssClass = "inpText"
            OrderPrice.Enabled = True
        End If
    End Sub

    Private Sub ambilhargaitem()
        If taxable.SelectedValue = "INCLUDE" Then
            If currencyoid.SelectedValue = "1" Then
                OrderPrice.Text = ToMaskEdit(OrderPrice.Text / 1.1, 3)
                txtVarPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
            Else
                OrderPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
                txtVarPrice.Text = ToMaskEdit(txtVarPrice.Text, 3)
            End If
        Else
            If currencyoid.SelectedValue = "1" Then
                If typeSO.SelectedValue = "User" Then
                    OrderPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
                    txtVarPrice.Text = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
                Else
                    OrderPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
                    txtVarPrice.Text = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
                End If
            Else
                OrderPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
                txtVarPrice.Text = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
            End If

        End If
    End Sub

    Private Sub PrintReport(ByVal id As String, ByVal no As String, ByVal tax As String, ByVal report2 As String)
        'untuk print
        If tax = "YES" Then
            report.Load(Server.MapPath(folderReport & "rptiotax.rpt"))
        ElseIf tax = "NO" Then
            report.Load(Server.MapPath(folderReport & "rptio.rpt"))
        End If
        report.SetParameterValue("orderoidmst", id)

        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
            System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

        'report.PrintOptions.PrinterName = printerPOS
        report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize
        'report.PrintToPrinter(1, False, 0, 0)
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers

        Try
            If report2 = "pdf" Or report2 = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
            ElseIf report2 = "excel" Then
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, no & "_" & Format(GetServerTime(), "dd_MM_yy"))
            End If
            report.Close() : report.Dispose() : Session("no") = Nothing
            Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx?awal=true")
        Catch ex As Exception
            report.Close() : report.Dispose()
        End Try
    End Sub

    Private Sub diskonqty()
        Dim sSql1 As String = ""
        Try
            Dim pRice As String = "", oPrice As Double = 0
            If jPrice.SelectedValue = "NORMAL" Then
                pRice = "ISNULL(id.priceNormal,a.pricelist)"
            ElseIf jPrice.SelectedValue = "NOTA" Then
                pRice = " ISNULL(id.pricenota,a.itempriceunit1)"
            Else
                pRice = "ISNULL(id.pricekhusus,a.itempriceunit2)"
            End If

            sSql1 = "SELECT pm.promoid, subsidi1 Harga, branchFlag FROM QL_mstpromodtl pd INNER JOIN QL_mstpromo pm ON pm.promoid=pd.promoid WHERE typebarang='UTAMA' AND pm.promoid='" & ddlPromo.SelectedValue & "' AND itemoid=" & itemoid.Text & " AND branchFlag LIKE '%" & CabangNya.SelectedValue & "%'"
            Dim dtpro As DataTable = cKoneksi.ambiltabel(sSql1, "DataPromo")
            If dtpro.Rows.Count > 0 Then
                oPrice = ToMaskEdit(dtpro.Rows(0)("Harga").ToString, 3)
            Else
                oPrice = ToDouble(GetScalar("SELECT " & pRice & " Harga FROM ql_mstitem a LEFT JOIN (SELECT TOP 1 id.itemoid, id.branchcode, id.pricesilver, id.priceplatinum, id.pricegold, id.priceminim MinimPrice, id.priceexpedisi, id.pricenota, id.priceNormal, id.pricekhusus, id.hpp, id.itemdtloid FROM QL_mstpricedtl id Order BY createtime DESC) id ON id.itemoid=a.itemoid AND id.branchcode='" & CabangNya.SelectedValue & "' WHERE a.itemoid = " & itemoid.Text & ""))
            End If
            If PembagiTax.Text = "" Or PembagiTax.Text = "0" Then
                PembagiTax.Text = 1
            End If
            If currencyoid.SelectedValue = "1" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(oPrice) / ToDouble(PembagiTax.Text), 3)
                If taxable.SelectedValue = "INCLUDE" Then
                    txtVarPrice.Text = ToDouble(Math.Round(oPrice * 1.1, 1))
                Else
                    txtVarPrice.Text = ToDouble(Math.Round(oPrice * 1, 1))
                End If
            Else
                OrderPrice.Text = ToDouble(oPrice)
                If taxable.SelectedValue = "INCLUDE" Then
                    txtVarPrice.Text = ToMaskEdit(txtVarPrice.Text, 3)
                Else
                    txtVarPrice.Text = ToMaskEdit(txtVarPrice.Text, 3)
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString & sSql1, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try

    End Sub

    Private Sub changebonus()
        ambilhargaitem() : fillTotalQty()
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_id") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If
            Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx")
        End If
        Page.Title = CompnyName & " - Sales Order"
        Session("oid") = Request.QueryString("oid")
        Session("branch_code") = Request.QueryString("branch_code")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        BtnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Send this data for Approval?.');")
        'btnPrint1.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Print this data?');")
        I_U.Text = "New"
        Session("UserLevel") = GetStrData("SELECT USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        Session("Sales") = GetStrData("SELECT flagsales From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                sSql = "SELECT count(-1) from ql_mstgen where gengroup = 'SOCLOSETYPE'"
                xCmd.CommandText = sSql : Dim countsotype As Integer = xCmd.ExecuteScalar
                If countsotype >= 2 Then
                    conn.Close() : showMessage("Data untuk set hari pada SOCLOSETYPE Lebih Dari 1, Hapus salah satu untuk set AUTO CLOSE SO", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If
                sSql = "select isnull(genother1,0) from ql_mstgen where gengroup = 'SOCLOSETYPE'"
                xCmd.CommandText = sSql : Dim sotype As Integer = xCmd.ExecuteScalar
                If sotype = 0 Then
                    conn.Close() : showMessage("Master General untuk type SOCLOSETYPE belum di set, silahkan hubungi admin!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
                End If
                Dim conDayToSecond As Integer = 86400
                sSql = "select DATEDIFF(S,o.createtime,CURRENT_TIMESTAMP) AS Day, o.createtime, o.ordermstoid, isnull(sjm.trnsjjualmstoid,0) AS trnsjjualmstoid, o.trnorderstatus, ISNULL(sjm.trnsjjualstatus,'') AS trnsjjualstatus, o.branch_code from QL_trnordermst o LEFT JOIN ql_trnsjjualmst sjm on sjm.orderno = o.orderno and sjm.branch_code = o.branch_code AND sjm.trnsjjualstatus NOT IN ('Approved','Rejected', 'Closed') where o.trnorderstatus NOT IN ('Canceled', 'Closed', 'Rejected','POST', 'Closed Manual') and DATEDIFF(S,o.createtime,CURRENT_TIMESTAMP) >= " & sotype * conDayToSecond & ""
                Session("mstso") = cKoneksi.ambiltabel(sSql, "ql_mstso")
                Dim objTable As DataTable : objTable = Session("mstso")
                Dim dvso As DataView = objTable.DefaultView
                If objTable.Rows.Count > 0 Then
                    '-------
                    dvso.RowFilter = "trnorderstatus = 'In Approval'"
                    For C1 As Int16 = 0 To dvso.Count - 1
                        sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_approval set STATUSREQUEST = 'End', Event = 'Closed' where oid = " & dvso(C1)("ordermstoid") & " AND branch_code = '" & dvso(C1)("branch_code") & "' AND TABLENAME = 'QL_TRNORDERMST'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvso.RowFilter = ""
                    '--------
                    dvso.RowFilter = "isnull(trnsjjualstatus,'') = '' AND trnorderstatus <> 'In Approval'"
                    For C1 As Int16 = 0 To dvso.Count - 1
                        sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvso.RowFilter = ""
                    '---------
                    dvso.RowFilter = "trnsjjualstatus = 'In Approval'"
                    For C1 As Int16 = 0 To dvso.Count - 1
                        sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_trnsjjualmst set trnsjjualstatus = 'Closed', upduser = 'Auto Closed' where trnsjjualmstoid = " & dvso(C1)("trnsjjualmstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_approval set STATUSREQUEST = 'End', Event = 'Closed' where oid = " & dvso(C1)("trnsjjualmstoid") & " AND branch_code = '" & dvso(C1)("branch_code") & "' AND TABLENAME = 'QL_TRNSJJUALMST'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvso.RowFilter = ""
                    dvso.RowFilter = "trnsjjualstatus  <> 'In Approval' and trnorderstatus <> 'In Approval' and trnsjjualstatus<>''"
                    '---------
                    For C1 As Int16 = 0 To dvso.Count - 1
                        sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        sSql = "Update ql_trnsjjualmst set trnsjjualstatus = 'Closed', upduser = 'Auto Closed' where trnsjjualmstoid = " & dvso(C1)("trnsjjualmstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvso.RowFilter = ""
                    '---------
                    dvso.RowFilter = "trnsjjualstatus = 'Approved' and trnorderstatus <> 'In Approval' "
                    For C1 As Int16 = 0 To dvso.Count - 1
                        sSql = "Update ql_trnordermst set trnorderstatus = 'Closed', upduser = 'Auto Closed' where ordermstoid = " & dvso(C1)("ordermstoid") & " and branch_code = '" & dvso(C1)("branch_code") & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    dvso.RowFilter = ""
                End If
                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                showMessage(ex.ToString, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
            End Try

            Session("ddlFilterIndex") = Nothing
            orderflag.Enabled = True
            FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            DDLBranch() : BindDataSO()
            Dim salesperson As String = Session("UserID")
            lbltempAmt.Text = ToMaskEdit(ToDouble(0), 3)

            If chkTax.Checked = False Then
                lbltax.Text = 0
            Else
                lbltax.Text = 1
            End If

            InitCabangNya() : InitAllDDL() : ddlSales()
            CabangNya_SelectedIndexChanged(Nothing, Nothing)
            If currencyoid.SelectedValue = "1" Or currencyoid.SelectedValue = "" Then
                CurrOid.Text = 1
            Else
                CurrOid.Text = 2
            End If

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                'Cek apakah IO ini sudah dibuatkan SO atau belum, kalau sudah dibuatkan SO maka SO harus di hapus dulu
                Session("cekIO") = False : I_U.Text = "Update"
                fillTextBox(Session("branch_code"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblUpdate.Text = "Last Update By"
                lblOn.Text = "On" : trcopy.Visible = False
            Else
                I_U.Text = "New"
                createtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss.fff")
                lblVarPrice.Visible = False : txtVarPrice.Visible = False
                taxable.SelectedIndex = 0
                If taxable.SelectedValue = "INCLUDE" Then
                    PembagiTax.Text = "1.1"
                Else
                    PembagiTax.Text = "1.00"
                End If
                curr.Text = "Rupiah" : generateNoOrder()
                Session("ItemLine") = 1 : orderstatus.Text = "In Process"
                deliverydate.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
                trnorderdtlseq.Text = Session("ItemLine")
                orderdate.Text = Format(GetServerTime(), "dd/MM/yyyy") 
                updUser.Text = Session("UserID")
                updTime.Text = GetServerTime()
                btnDelete.Visible = False : btnPrint.Visible = False
                TabContainer1.ActiveTabIndex = 0
                identifierno.Text = GetDateToPeriodAcctg(GetServerTime())
                ddisc.Text = "0.00"
                orderflag.Enabled = True : lblUpdate.Text = "Create By"
                lblOn.Text = "On" : trcopy.Visible = True

                sSql = "SELECT promoid, promeventname FROM QL_mstpromo WHERE cmpcode='" & CompnyCode & "' and promflag<>'INACTIVE' AND (CURRENT_TIMESTAMP BETWEEN promstartdate AND dateadd(day,1,promfinishdate)) and CONVERT(varchar, CURRENT_TIMESTAMP, 108)>=promstarttime and CONVERT(varchar,CURRENT_TIMESTAMP, 108)<=promfinishtime and dayflag like '%'+CAST(datepart(weekday,current_timestamp) AS VARCHAR)+'%' and branchFlag like '%" & CabangNya.SelectedValue & "%' UNION SELECT distinct 0 promoid,'NONE' promeventname from QL_mstGen "
                FillDDL(ddlPromo, sSql)
            End If
            disctype_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub ibtnFindCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnFindCust.Click
        Dim sales As String = spgOid.SelectedValue
        If sales = "" Then
            showMessage("Hanya Sales bisa lanjutkan transaksi ini !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindDataCust("")
        gvCustomer.Visible = True
    End Sub

    Protected Sub gvCustomer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomer.PageIndexChanging
        gvCustomer.PageIndex = e.NewPageIndex
        gvCustomer.Visible = True
        BindDataCust("")
    End Sub

    Protected Sub gvCustomer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCustomer.SelectedIndexChanged
        custoid.Text = gvCustomer.SelectedDataKey("custoid").ToString
        CustName.Text = gvCustomer.SelectedDataKey("custname").ToString
        custcode.Text = gvCustomer.SelectedDataKey("custcode").ToString
        DDLTop.SelectedValue = gvCustomer.SelectedDataKey.Item("timeofpayment").ToString
        deliveryaddr.Text = GetStrData("SELECT custaddr +','+isnull((SELECT gendesc from QL_mstgen where genoid = custcityoid)+',','')+ isnull((SELECT gendesc from QL_mstgen where genoid = custprovoid) + ' ,','') + isnull((SELECT gendesc from QL_mstgen where genoid = custcountryoid),'') from ql_mstcust where custoid= '" & custoid.Text & "'")

        If custoid.Text <> "" Then
            If orderamount.Text = "0.00" Then
            ElseIf orderamount.Text <> "0.00" Then
                Dim totSoIo As Double = cKoneksi.ambilscalar("SELECT isnull(sum(dtl.trnorderprice*dtl.trnorderdtlqty),0) as price from QL_trnorderdtl dtl, QL_trnordermst mst where dtl.trnordermstoid=mst.ordermstoid and mst.trncustoid='" & custoid.Text & "' and trnordertype in ('GROSIR') and mst.cmpcode='" & CompnyCode & "'")
                Dim piutang As Double = cKoneksi.ambilscalar("SELECT isnull(sum(trnamtjualnetto),0) as jumlah from ql_trnjualmst where cmpcode='" & CompnyCode & "' and  trncustoid='" & custoid.Text & "'")
                Dim ordunlimit As Double = cKoneksi.ambilscalar("SELECT isnull(custcreditlimitrupiah,0) from QL_mstcust where cmpcode like '%" & CompnyCode & "%' and custflag = 'Active' and custoid='" & custoid.Text & "'")
                If ordunlimit <> 0 Then 'lolos unlimitied
                    Dim ordlimit As Double = cKoneksi.ambilscalar("SELECT isnull(custcreditlimitrupiah,0) - isnull(custcreditlimitusagerupiah,0) from QL_mstcust where cmpcode like '%" & CompnyCode & "%' and custflag = 'Active' and custoid='" & custoid.Text & "'")
                    Dim totlimit As Double = ToDouble(orderamount.Text)

                End If
            End If
            Dim totalbelumlunas As Double = 0.0

            If totalbelumlunas > 0 Then
                Dim telatplus As Integer = cKoneksi.ambilscalar("(SELECT isnull(genother1,30) from ql_mstgen where gengroup = 'OTOCUSTLATE')")

                'get data customer Invoice yg telat
                sSql = "SELECT c.custname ,c.custgroup ,m.trnjualno ,convert(varchar(10),m.trnjualdate,103) trnjualdate,convert(varchar(10),dateadd(d,(cast(g.genother1 as int)),m.trnjualdate),103) duedate,m.trnamtjualnetto,m.accumpayment,m.amtretur,(m.trnamtjualnetto -m.accumpayment - m.amtretur) totbelumlunas  from QL_trnjualmst m inner join QL_mstcust c on m.trncustoid = c.custoid inner join ql_mstgen g on g.genoid = m.trnpaytype and g.gengroup = 'paytype' where c.custoid = " & custoid.Text & " and (m.trnamtjualnetto -m.accumpayment - m.amtretur > 0 ) and datediff(d,dateadd(d, cast(g.genother1 as int) ,m.trnjualdate) ,CURRENT_TIMESTAMP ) > (SELECT isnull(genother1,30) from ql_mstgen where gengroup = 'OTOCUSTLATE')"

                'tampung di notif
                Dim objTablecek As DataTable
                Dim objRowcek() As DataRow
                objTablecek = cKoneksi.ambiltabel(sSql, "ql_trnsjjualdtl")
                objRowcek = objTablecek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
                Exit Sub
            End If
        End If
        gvCustomer.Visible = False : cProc.DisposeGridView(sender)
    End Sub

    Protected Sub gvItemSearch_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemSearch.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
        End If
    End Sub

    Protected Sub gvItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemSearch.SelectedIndexChanged
        Try
            itemoid.Text = gvItemSearch.SelectedDataKey("itemoid")
            itemlongdesc.Text = gvItemSearch.SelectedDataKey("itemdesc")
            Merk.Text = gvItemSearch.SelectedDataKey("merk")
            ItemGroup.Text = gvItemSearch.SelectedDataKey("itemgroupcode")
            initUnit()
            StatusBarang.Text = gvItemSearch.SelectedDataKey("StatusBarang")

            sSql = "SELECT promtype FROM QL_mstpromo WHERE cmpcode='" & CompnyCode & "' and promflag<>'INACTIVE' and branchFlag like '%" & CabangNya.SelectedValue & "%' AND promoid='" & ddlPromo.SelectedValue & "'"
            Dim PromoType As String = GetStrData(sSql)

            If PromoType = "Disc % (Item Only)" Then
                DiscPct.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("discnya")), 3)
            ElseIf PromoType = "DISCAMT" Then
                DiscPct.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("discnya")), 3)
            Else
                DiscPct.Text = ToDouble(0.0)
            End If

            'changebonus() : editharganota() : fillTotalQty()
            If jPrice.SelectedValue = "NORMAL" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("priceNormal")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit1"))
            ElseIf jPrice.SelectedValue = "NOTA" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricenota")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit2"))
            ElseIf jPrice.SelectedValue = "KHUSUS" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricekhusus")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit3"))
            ElseIf jPrice.SelectedValue = "SILVER" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricesilver")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit1"))
            ElseIf jPrice.SelectedValue = "PLATINUM" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("priceplatinum")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit1"))
            ElseIf jPrice.SelectedValue = "GOLD" Then
                OrderPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricegold")), 3)
                Discdtl2.Text = ToDouble(gvItemSearch.SelectedDataKey("discunit1"))
            End If

            pricenormal.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("priceNormal")), 3)
            pricenota.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricenota")), 3)
            pricekhusus.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricekhusus")), 3)
            pricesilver.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricesilver")), 3)
            priceplatinum.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("priceplatinum")), 3)
            pricegold.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("pricegold")), 3)
            MinimPrice.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("MinimPrice")), 3)
            priceexpedisi.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("priceexpedisi")), 3)
            hpp.Text = ToMaskEdit(ToDouble(gvItemSearch.SelectedDataKey("hpp")), 3)
            itemdtloid.Text = gvItemSearch.SelectedDataKey("itemdtloid").ToString
            Discdtl2.Enabled = False

            If taxable.SelectedValue = "INCLUDE" Or gvItemSearch.SelectedDataKey("StatusBarang") = "PROMO" Then
                OrderPrice.Enabled = False
                OrderPrice.CssClass = "inpTextDisabled"
            End If

            If currencyoid.SelectedValue = "1" Then
                If typeSO.SelectedValue = "User" Then
                    TxtTotKotor.Text = ToMaskEdit(OrderQty.Text * OrderPrice.Text, 3)
                    Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                Else
                    Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                End If
            Else
                TxtTotKotor.Text = ToMaskEdit(ToDouble(OrderQty.Text * OrderPrice.Text), 3)
                Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
            End If
            gvItemSearch.Visible = False
            cProc.DisposeGridView(sender)
        Catch ex As Exception
            showMessage(ex.ToString & sSql, CompnyName & "<br />" & sSql & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
     
    End Sub

    Protected Sub gvItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemSearch.PageIndexChanging
        gvItemSearch.PageIndex = e.NewPageIndex
        gvItemSearch.Visible = True : Dim sTempSql As String = ""
        BindDataItem(sTempSql) ': ModalPopupExtender4.Show()
    End Sub

    Protected Sub GVmstOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstOrder.PageIndexChanging
        GVmstOrder.PageIndex = e.NewPageIndex
        BindDataSO() : GVmstOrder.Visible = True
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        DDLSearch.SelectedIndex = 0 : txtFilter.Text = ""
        chkPeriod.Checked = False : chkStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime.AddDays(-1), "dd/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime, "dd/MM/yyyy")
        DDLStatus.SelectedIndex = 0
        Dim salesperson As String = Session("UserID")
        BindDataSO()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindDataSO()
    End Sub

    Protected Sub BtnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnApproval.Click
        orderstatus.Text = "In Approval" : btnSave_Click(sender, e)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("TabelDtl") = Nothing
        Session("Total") = Nothing
        Session("TabelOrderdesign") = Nothing
        Session("MstOid") = Nothing
        Session("DtlOid") = Nothing
        Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx?awal=true")
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custcode.Text = "" : custoid.Text = ""
        CustName.Text = "" : gvCustomer.Visible = False
        deliveryaddr.Text = ""
        cbCash.Checked = False
    End Sub 

    Protected Sub gvItemOrdered_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemOrdered.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            If currencyoid.SelectedValue = "1" Then
                e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
                e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
            Else
                e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
                e.Row.Cells(11).Text = ToMaskEdit(ToDouble(e.Row.Cells(11).Text), 3)
            End If
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), 3)
            e.Row.Cells(10).Text = ToMaskEdit(ToDouble(e.Row.Cells(10).Text), 3)
            e.Row.Cells(12).Text = ToMaskEdit(ToDouble(e.Row.Cells(12).Text), 3)
        End If
    End Sub

    Protected Sub GVmstOrder_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstOrder.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub gvItemOrdered_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItemOrdered.RowDeleting
        Try
            Dim idx As Integer = e.RowIndex
            Dim objTable As DataTable
            Dim objrow() As DataRow
            objTable = Session("TabelDtl")

            Dim dvTemp As DataView = objTable.DefaultView
            dvTemp.RowFilter = "orderdtlseq=" & idx + 1 & ""

            If dvTemp.Count > 0 Then
                If dvTemp(0)("jenisprice") = "BONUS" Then
                    showMessage("Maaf, Anda menghapus barang bonus, pilih jenis promo jika menghapus barang jenis bonus..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    dvTemp.RowFilter = ""
                    Exit Sub
                End If
            End If

            objrow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim iItemOid As Integer = objTable.Rows(e.RowIndex)("itemoid").ToString

            If dvTemp(0)("jenisprice") = "PROMO" Then
                dvTemp.RowFilter = ""
                dvTemp.RowFilter = "jenisprice NOT IN ('NORMAL','KHUSUS','NOTA')"
                If dvTemp.Count > 0 Then
                    For C1 As Integer = dvTemp.Count - 1 To 0 Step -1
                        dvTemp(C1).Row.Delete()
                    Next
                End If
                dvTemp.RowFilter = ""
            Else
                dvTemp.RowFilter = ""
                objTable.Rows.RemoveAt(e.RowIndex)
            End If

            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                Dim dr As DataRow = objTable.Rows(C1)
                dr.BeginEdit() : dr(3) = C1 + 1
                dr.EndEdit()
            Next

            If objTable.Rows.Count = 0 Then
                ddlPromo.Enabled = True : ddlPromo.CssClass = "inpText"
                currencyoid.Enabled = True : currencyoid.CssClass = "inpText"
            End If
            Session("TabelDtl") = objTable

            Session("ItemLine") = Session("ItemLine") - 1
            trnorderdtlseq.Text = Session("ItemLine")
            gvItemOrdered.DataSource = Session("TabelDtl")
            gvItemOrdered.DataBind()
            TotOrder() : ReAmount()

        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 1, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        clearControlAddItem()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close() : report.Dispose()
    End Sub

    Protected Sub imbAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataCust("")
    End Sub

    Protected Sub imbAllItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataItem("")
    End Sub

    Protected Sub packingqty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        fillTotalQty()
    End Sub

    Protected Sub taxable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub DDLLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generateNoOrder()
    End Sub

    Protected Sub orderflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generateNoOrder()
    End Sub

    Protected Sub gvCustomer_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCustomer.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(4).Text > 0 Then
                e.Row.Cells(4).Text = NewMaskEdit(e.Row.Cells(4).Text)
            ElseIf e.Row.Cells(4).Text = 0 Or e.Row.Cells(4).Text < 0 Then
                e.Row.Cells(4).Text = "Limited"
                e.Row.Cells(4).ForeColor = Drawing.Color.Red
            Else
                e.Row.Cells(4).Text = "Un-Limited"
                e.Row.Cells(4).ForeColor = Drawing.Color.Green
            End If

        End If
    End Sub 

    Protected Sub cbBonus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If itemoid.Text = "" Then
        Else
            changebonus()
        End If
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMsgBoxOK.Click
        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Or lblMessage.Text = "Data telah di Send for Approval !" Then
            Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx?awal=true")
        ElseIf lblMessage.Text = "INFONYA" And Session("sMsg") IsNot Nothing Then
            PanelMsgBox.Visible = False : beMsgBox.Visible = False
            mpeMsgbox.Hide() : Session("sMsg") = Nothing
            lblMessage.Visible = True
        Else
            PanelMsgBox.Visible = False
            beMsgBox.Visible = False
            mpeMsgbox.Hide()
        End If
    End Sub

    Protected Sub txtVarPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtVarPrice.Text = "" Then
            txtVarPrice.Text = "1.00"
        End If
        OrderPrice.Text = ToMaskEdit(ToDouble(txtVarPrice.Text / 1.1), 3)
        fillTotalQty()
    End Sub

    Protected Sub bundling_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If bundling.Checked = True Then
            lblbundling.Text = 1
        Else
            lblbundling.Text = 0
        End If
    End Sub

    Protected Sub ibtnFindCustGroup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sales As String = spgOid.SelectedValue
        If sales = "" Then
            showMessage("Hanya Sales bisa lanjutkan transaksi ini !", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindDataCustGroup("")
        gvCustomerGroup.Visible = True
    End Sub

    Protected Sub gvCustomerGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCustomerGroup.PageIndexChanging
        gvCustomerGroup.PageIndex = e.NewPageIndex
        gvCustomerGroup.Visible = True
        BindDataCustGroup("")
    End Sub

    Protected Sub gvCustomerGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCustomerGroup.SelectedIndexChanged
        custgroupoid.Text = gvCustomerGroup.SelectedDataKey("custgroupoid").ToString
        CustNameGroup.Text = gvCustomerGroup.SelectedDataKey("custgroupname").ToString
        gvCustomerGroup.Visible = False
    End Sub

    Protected Sub btnClearCustGroup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custgroupoid.Text = ""
        CustNameGroup.Text = "" : gvCustomerGroup.Visible = False
    End Sub

    Protected Sub gvCustomerGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(4).Text > 0 Then
                e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            ElseIf e.Row.Cells(4).Text = 0 Then
                e.Row.Cells(4).Text = "Limited"
                e.Row.Cells(4).ForeColor = Drawing.Color.Red
            Else
                e.Row.Cells(4).Text = "Un-Limited"
                e.Row.Cells(4).ForeColor = Drawing.Color.Green
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = "", flagbottompricedtl As String = "", bottompricegrosir As Double = 0, bottompricegrosirperqty As Integer = 0, flagbottomprice As String = "N"
        orderdate.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")

        If chkTax.Checked = True Then
            lbltax.Text = 1 : taxable.SelectedValue = "INC"
        Else
            lbltax.Text = 0 : taxable.SelectedValue = "NONTAX"
        End If

        If IsDate(toDate(orderdate.Text)) = False Then
            sMsg &= "Maaf, Tanggal Sales Order salah..!!<br>"
            orderstatus.Text = "In Process"
        Else
            If CDate(toDate(orderdate.Text)) < Format(Date.Now, "MM/dd/yyyy") Then
                sMsg &= "Maaf, Tanggal Sales Order tidak boleh backdate..!!<br> "
                orderstatus.Text = "In Process"
            End If

            If CDate(toDate(orderdate.Text)) > CDate(toDate("6/6/2079")) Then
                sMsg &= "Maaf, Tanggal Sales Order Date harus kurang dari 06/06/2079..!!<br>"
                orderstatus.Text = "In Process"
            End If
        End If

        If deliveryaddr.Text = "" Then
            sMsg &= "Maaf, Delivery Address Harus Di Isi..!!<br>"
            orderstatus.Text = "In Process"
        End If

        If IsDate(toDate(deliverydate.Text)) = False Then
            sMsg &= "Maaf, Tanggal Sales Order Delivery salah..!!"
            orderstatus.Text = "In Process"
        Else
            If CDate(toDate(deliverydate.Text)) < Format(Date.Now, "MM/dd/yyyy") Then
                sMsg &= "Maaf, Tanggal Sales Order Delivery tidak boleh backdate..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If CDate(toDate(deliverydate.Text)) > CDate(toDate("6/6/2079")) Then
                sMsg &= "Maaf, Tanggal Sales Order Delivery harus kurang dari 06/06/2079..!!<br>"
                orderstatus.Text = "In Process"
            End If
        End If

        If CDate(toDate(orderdate.Text)) > CDate(toDate(deliverydate.Text)) Then
            sMsg &= "Maaf, Tanggal Order harus lebih kecil dari Tanggal Delivery !!"
            orderstatus.Text = "In Process"
        End If

        If custoid.Text = "" Then
            sMsg &= "Maaf, Silahkan pilih Customer dulu..!!<br>"
            orderstatus.Text = "In Process"
        End If

        If ordernote.Text.Length > 300 Then
            sMsg &= "Maaf, Note maximum 300 character !!<br>"
            orderstatus.Text = "In Process"
        End If

        If deliveryaddr.Text.Length > 200 Then
            sMsg &= "Maaf, Delivery Address maximal 200 character !!<br>"
            orderstatus.Text = "In Process"
        End If

        If orderamount.Text = "0.00" Then
            sMsg &= "Maaf, Total nota Amount netto SO sekarang tidak boleh nol !!<br>"
            orderstatus.Text = "In Process"
        End If

        If typeSO.SelectedValue = "ecommers" Then
            If NoOrder.Text = "" Then
                sMsg &= "Maaf, Nomer Order Online masih kosong..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If NamaEndUSer.Text = "" Then
                sMsg &= "Maaf, Nama End user masih kosong..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If NoInvOnline.Text = "" Then
                sMsg &= "Maaf, Nomer Invoice Online masih kosong..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If NoAwb.Text = "" Then
                sMsg &= "Maaf, Nomer AWB masih kosong..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If ShipppingAddres.Text.Length > 500 Then
                sMsg &= "Maaf, Maximal Shipping Addres 500 character..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If ToAddress.Text.Length > 500 Then
                sMsg &= "Maaf, Maximal To Addres 500 character..!!<br>"
                orderstatus.Text = "In Process"
            End If
        End If

        'cek apa sudah ada material dari Order
        If Session("TabelDtl") Is Nothing Then
            sMsg &= "Maaf, Silahkan buat Order detail dulu..!!<br>"
            orderstatus.Text = "In Process"
        Else
            Dim objTableCek As DataTable = Session("TabelDtl")
            Dim objRowCek() As DataRow : dv = objTableCek.DefaultView
            'Cek apa sudah ada item yang sama dalam Tabel Detail
            objRowCek = objTableCek.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRowCek.Length = 0 Then
                showMessage("Silahkan membuat Order detail dulu..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                orderstatus.Text = "In Process"
                Exit Sub
            End If
            gvItemOrdered.Visible = True
        End If
        ' checking Current Last No to prevent redundancy
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If CheckDataExists(orderno.Text, "orderno", "QL_trnordermst") Then
                generateNoOrder()
            End If
        Else 'cek data ada atau sudah di hapuse
            If CheckDataExists(oid.Text, "ordermstoid", "QL_trnordermst") = False Then
                sMsg &= "Tidak bisa Update Data ini, Data telah dihapus..!!<br>"
            End If
        End If

        If checkApproval("QL_trnordermst", "In Approval", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
            sMsg &= "Maaf, Sales Order ini sudah send Approval..!!<br>"
            orderstatus.Text = "In Approval"
        End If
        '--------------------------------------------------------------------------------- 

        If orderstatus.Text = "In Approval" Then
            sSql = "SELECT tablename, approvaltype, approvallevel, approvaluser, approvalstatus From QL_approvalstructure WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnordermst' And branch_code LIKE '%" & CabangNya.SelectedValue & "%' order by approvallevel"
            Dim dtData2 As DataTable = cKoneksi.ambiltabel(sSql, "QL_approvalstructure")
            If dtData2.Rows.Count > 0 Then
                Session("TblApproval") = dtData2
            Else
                sMsg &= "Tidak ada User untuk Approved SO, Silahkan hubungi admin dahulu..!!<br>"
            End If
        ElseIf orderstatus.Text = "Rejected" Then
            orderstatus.Text = "In Process"
        End If

        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand

        Session("MstOid") = GenerateIDbranch("QL_trnordermst", CabangNya.SelectedValue)
        Session("DtlOid") = GenerateIDbranch("QL_trnorderdtl", CabangNya.SelectedValue)

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ToDouble(GetStrData("SELECT COUNT(*) FROM QL_trnordermst WHERE createtime='" & CDate(toDate(createtime.Text)) & "'")) > 0 Then
                sMsg &= "Maaf, Data sudah tersimpan,silahkan klik tombol cancel dan mohon untuk cek data pada list form..!!"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        '== OPEN CONNECTION ==
        objConn.Open()
        objCmd.Connection = objConn
        objTrans = objConn.BeginTransaction()
        objCmd.Transaction = objTrans

        If Session("oid") = Nothing Or Session("oid") = "" Then
            Try
                sSql = "INSERT into QL_trnordermst (cmpcode, ordermstoid, branch_code, orderno, trnorderdate, periodacctg, trnordertype, trnorderstatus, trnpaytype, trncustoid, consigneeoid, trncustname, trnordernote, trntaxpct, createuser, upduser, updtime, finalapprovalcode, finalapprovaluser, finalappovaldatetime, trnorderref, salesoid, spgoid, delivdate, amtjualnetto, amtjualnettoidr, amtjualnettousd, promooid, sobo, flagbottomprice, currencyoid, rateoid, rate2oid,timeofpaymentSO, to_branch, ordertaxtype, ordertaxamt, flagTax, typeExpedisi, typeso, bundling, amtdisc1, amtdisc2, createtime, flagCash, flagSR, ordernumber, endusername, noinvoiceonline, noawb, shippingaddr, toaddr)" & _
                    " VALUES ('" & CompnyCode & "', " & Session("MstOid") & ", '" & CabangNya.SelectedValue & "', '" & Session("MstOid") & "', '" & toDate(orderdate.Text) & "', '" & identifierno.Text & "', '" & orderflag.SelectedValue & "', '" & orderstatus.Text & "', " & DDLTop.SelectedValue & ", '" & custoid.Text & "', 0, '" & Tchar(CustName.Text) & "', '" & Tchar(ordernote.Text) & "', " & ToDouble(0) & ", '" & Session("UserID") & "', '" & Session("UserID") & "', current_timestamp, '', '', '1/1/1900', '" & Tchar(POref.Text) & "', " & spgOid.SelectedValue & ", " & spgOid.SelectedValue & ", '" & toDate(deliverydate.Text) & "', " & ToDouble(totalorder.Text) & ", " & ToDouble(totalorder.Text) & ", " & ToDouble(totalorder.Text) & ", " & ddlPromo.SelectedValue & ", '1', '" & flagbottomprice & "', '" & currencyoid.SelectedValue & "', '" & rateoid.Text & "', '" & rate2oid.Text & "', '" & DDLTop.SelectedValue & " ', '" & CabangNya.SelectedValue & "', '" & taxable.SelectedValue & "', " & ToDouble(0) & ", " & (lbltax.Text) & ", '" & ddlexpedisi.SelectedValue & "', '" & typeSO.SelectedValue.ToString & "', '" & lblbundling.Text & "', " & ToDouble(DiscHdr1.Text) & ", " & ToDouble(DiscHdr2.Text) & ", '" & CDate(toDate(createtime.Text)) & "', '" & IIf(cbCash.Checked, "Y", "N") & "', '" & IIf(cbSR.Checked, "Y", "N") & "', '" & Tchar(NoOrder.Text) & "', '" & Tchar(NamaEndUSer.Text) & "', '" & Tchar(NoInvOnline.Text) & "', '" & Tchar(NoAwb.Text) & "', '" & Tchar(ShipppingAddres.Text) & "', '" & Tchar(ToAddress.Text) & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                'Update lastoid dari QL_mstoid table QL_trnordermst
                sSql = "UPDATE QL_mstoid SET lastoid=" & Session("MstOid") & " WHERE tablename='QL_trnordermst' and branch_code = '" & CabangNya.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                If Not Session("TabelDtl") Is Nothing Then

                    Dim objTable As DataTable : Dim objRow() As DataRow
                    objTable = Session("TabelDtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    For c1 As Integer = 0 To objRow.Length - 1
                        sSql = "SELECT bottompricegrosir FROM ql_mstitem WHERE itemoid=" & objTable.Rows(c1)("itemoid") & ""
                        objCmd.CommandText = sSql
                        bottompricegrosir = objTable.Rows(c1)("orderqty") * objCmd.ExecuteScalar
                        bottompricegrosirperqty = objCmd.ExecuteScalar

                        If bottompricegrosir > (objTable.Rows(c1)("total")) Then
                            If ddlPromo.SelectedValue = 0 Then
                                flagbottompricedtl = "T"
                            Else
                                flagbottompricedtl = "N"
                            End If
                        End If

                        sSql = "INSERT into QL_trnorderdtl (cmpcode, branch_code, trnorderdtloid, trnordermstoid, itemoid, trnorderdtlseq, trnorderdtlqty, trnorderdtlunitoid, trnorderdtlnote, trnorderprice, trnorderpriceidr, trnorderpriceusd, upduser, updtime, unitseq, trnamountdtl, trnamountdtlidr, trnamountdtlusd, trndiskonpromo, trndiskonpromoidr, trndiskonpromousd, trnamountbruto, trnamountbrutoidr, trnamountbrutousd, promooid , flagbottompricedtl, bottomprice, currencyoid, amountbottomprice, varprice, to_branch, tempAmtUsd, amtdtldisc1, amtdtldisc2, jenisprice, pricenormal, pricenota, pricekhusus, pricesilver, pricegold, priceplatinum, priceminim, itemdtloid) " & _
                            "VALUES ('" & CompnyCode & "', '" & CabangNya.SelectedValue & "', " & Session("DtlOid") & ", " & Session("MstOid") & ", " & objRow(c1)("itemoid") & ", " & objRow(c1)("orderdtlseq") & ", " & ToDouble(objRow(c1)("orderqty")) & ", " & objRow(c1)("itemunit") & ", '" & Tchar(objRow(c1)("itemnote").ToString.Trim) & "', " & ToDouble(objRow(c1)("orderprice")) & ", " & ToDouble(objRow(c1)("orderprice")) & ", " & ToDouble(objRow(c1)("orderprice")) & ",'" & objRow(c1)("upduser").ToString & "', CURRENT_TIMESTAMP," & objRow(c1)("unitseq") & ", " & ToDouble(objRow(c1)("totKotor")) & "," & ToDouble(objRow(c1)("totKotor")) & ", " & ToDouble(objRow(c1)("totKotor")) & ", " & ToDouble(objRow(c1)("disc")) & "," & ToDouble(objRow(c1)("disc")) & ", " & ToDouble(objRow(c1)("disc")) & ", " & ToDouble(objRow(c1)("orderprice")) * ToDouble(objRow(c1)("orderqty")) & ", " & ToDouble(objRow(c1)("orderprice")) * ToDouble(objRow(c1)("orderqty")) & ", " & ToDouble(objRow(c1)("orderprice")) * ToDouble(objRow(c1)("orderqty")) & ", " & (objRow(c1)("promooid")) & ", '" & flagbottompricedtl & "', " & ToDouble(bottompricegrosirperqty) & ", " & objRow(c1)("currency") & "," & ToDouble(bottompricegrosir) & ", " & ToDouble(objRow(c1)("varprice")) & ", '" & CabangNya.SelectedValue & "'," & ToDouble(objRow(c1)("tempAmtUsd")) & ", " & ToDouble(objRow(c1)("amtdtldisc1")) & ", " & ToDouble(objRow(c1)("amtdtldisc2")) & ", '" & objRow(c1)("jenisprice") & "', " & ToDouble(objRow(c1)("priceitem")) & ", " & ToDouble(objRow(c1)("pricenota")) & ", " & ToDouble(objRow(c1)("pricekhusus")) & ", " & ToDouble(objRow(c1)("pricesilver")) & ", " & ToDouble(objRow(c1)("pricegold")) & ", " & ToDouble(objRow(c1)("priceplatinum")) & ", " & ToDouble(objRow(c1)("MinimPrice")) & ", " & objRow(c1)("itemdtloid") & ")"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("DtlOid") += 1
                    Next

                    'Update lastoid dari QL_mstoid table QL_trnorderdtl
                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("DtlOid") & " WHERE tablename='QL_trnorderdtl' and branch_code='" & CabangNya.SelectedValue & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : objCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : objCmd.Connection.Close()
                showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 2, "modalMsgBoxWarn")
                orderstatus.Text = "In Process" : Exit Sub
            End Try
        Else
            Try ' UPDATE
                If orderstatus.Text = "In Approval" Then
                    sSql = "Update ql_trnordermst set revisenote='' Where ordermstoid=" & Session("oid") & " And branch_code='" & CabangNya.SelectedValue & "' "
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                ElseIf orderstatus.Text = "Revised" Then
                    orderstatus.Text = "In Process"
                End If

                sSql = "UPDATE QL_trnordermst SET orderno='" & Session("oid") & "', ordertaxtype='" & taxable.SelectedValue & "', trnorderdate='" & toDate(orderdate.Text) & "', periodacctg='" & identifierno.Text & "', typeso='" & typeSO.SelectedValue.ToString & "', timeofpaymentSO='" & DDLTop.SelectedValue & "', trnordertype='" & orderflag.SelectedValue & "', trnorderstatus='" & orderstatus.Text & "', trnpaytype= " & DDLTop.SelectedValue & ", trncustoid='" & custoid.Text & "', trncustname = '" & Tchar(CustName.Text) & "', trnordernote='" & Tchar(ordernote.Text) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp, finalapprovalcode='', finalapprovaluser='', finalappovaldatetime='1/1/1900', trnorderref='" & Tchar(POref.Text) & "', delivdate = '" & toDate(deliverydate.Text) & "', salesoid = " & spgOid.SelectedValue & ", spgoid = " & spgOid.SelectedValue & " , amtjualnetto = " & ToDouble(totalorder.Text) & ", amtjualnettoidr = " & ToDouble(totalorder.Text) & ", amtjualnettousd = " & ToDouble(totalorder.Text) & ", promooid = " & ddlPromo.SelectedValue & " , sobo = 1, flagbottomprice = '" & flagbottomprice & "', to_branch = '" & CabangNya.SelectedValue & "', flagTax=" & lbltax.Text & ", typeExpedisi='" & ddlexpedisi.SelectedValue & "', bundling='" & lblbundling.Text & "', amtdisc1=" & ToDouble(DiscHdr1.Text) & ", amtdisc2=" & ToDouble(DiscHdr2.Text) & ", flagCash = '" & IIf(cbCash.Checked, "Y", "N") & "', flagSR = '" & IIf(cbSR.Checked, "Y", "N") & "', ordernumber='" & Tchar(NoOrder.Text) & "', endusername='" & Tchar(NamaEndUSer.Text) & "', noinvoiceonline='" & Tchar(NoInvOnline.Text) & "', noawb='" & Tchar(NoAwb.Text) & "', shippingaddr='" & Tchar(ShipppingAddres.Text) & "', toaddr ='" & Tchar(ToAddress.Text) & "', branch_code='" & CabangNya.SelectedValue & "' WHERE ordermstoid=" & Session("oid") & " And branch_code='" & CabangNya.SelectedValue & "' And cmpcode='" & CompnyCode & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                sSql = "Select top 1 ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnorderdtl' and branch_code='" & CabangNya.SelectedValue & "'"
                objCmd.CommandText = sSql : Session("DtlOid") = objCmd.ExecuteScalar + 1

                If Not Session("TabelDtl") Is Nothing Then
                    sSql = "Delete from QL_trnorderdtl where trnordermstoid='" & Session("oid") & "' and branch_code='" & CabangNya.SelectedValue & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

                    'insert tabel detail
                    Dim objTable As DataTable
                    Dim objRow() As DataRow
                    objTable = Session("TabelDtl")
                    objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                    For C1 As Integer = 0 To objRow.Length - 1
                        sSql = "Select bottompricegrosir from ql_mstitem Where itemoid =" & objTable.Rows(C1)("itemoid") & ""
                        objCmd.CommandText = sSql
                        bottompricegrosir = ToDouble(objTable.Rows(C1)("orderqty")) * objCmd.ExecuteScalar
                        bottompricegrosirperqty = objCmd.ExecuteScalar

                        If bottompricegrosir > (objTable.Rows(C1)("total")) Then
                            If ddlPromo.SelectedValue = 0 Then
                                flagbottompricedtl = "T"
                            Else
                                flagbottompricedtl = "N"
                            End If
                        End If

                        sSql = "INSERT into QL_trnorderdtl (cmpcode, branch_code, trnorderdtloid, trnordermstoid, itemoid, trnorderdtlseq, trnorderdtlqty, trnorderdtlunitoid, trnorderdtlnote, trnorderprice, trnorderpriceidr, trnorderpriceusd, upduser, updtime, unitseq, trnamountdtl, trnamountdtlidr, trnamountdtlusd, trndiskonpromo, trndiskonpromoidr, trndiskonpromousd, trnamountbruto, trnamountbrutoidr, trnamountbrutousd, promooid, flagbottompricedtl, bottomprice, currencyoid, amountbottomprice, varprice, to_branch, tempAmtUsd, amtdtldisc1, amtdtldisc2, jenisprice, pricenormal, pricenota, pricekhusus, pricesilver, pricegold, priceplatinum, priceminim, itemdtloid) " & _
                         " VALUES ('" & CompnyCode & "', '" & CabangNya.SelectedValue & "', " & Session("DtlOid") & ", " & Session("oid") & ", " & objRow(C1)("itemoid") & ", " & objRow(C1)("orderdtlseq") & ", " & ToDouble(objRow(C1)("orderqty")) & ", " & objRow(C1)("itemunit") & ", '" & Tchar(objRow(C1)("itemnote").ToString.Trim) & "', " & ToDouble(objRow(C1)("orderprice")) & ", " & ToDouble(objRow(C1)("orderprice")) & ", " & ToDouble(objRow(C1)("orderprice")) & ",'" & objRow(C1)("upduser").ToString & "', CURRENT_TIMESTAMP," & objRow(C1)("unitseq") & ", " & ToDouble(objRow(C1)("totKotor")) & "," & ToDouble(objRow(C1)("totKotor")) & ", " & ToDouble(objRow(C1)("totKotor")) & ", " & ToDouble(objRow(C1)("disc")) & "," & ToDouble(objRow(C1)("disc")) & ", " & ToDouble(objRow(C1)("disc")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & (objRow(C1)("promooid")) & ",'" & flagbottompricedtl & "', " & ToDouble(bottompricegrosirperqty) & ", " & objRow(C1)("currency") & "," & ToDouble(bottompricegrosir) & ", " & ToDouble(objRow(C1)("varprice")) & ", '" & CabangNya.SelectedValue & "'," & ToDouble(objRow(C1)("tempAmtUsd")) & ", " & ToDouble(objRow(C1)("amtdtldisc1")) & ", " & ToDouble(objRow(C1)("amtdtldisc2")) & ", '" & objRow(C1)("jenisprice") & "', " & ToDouble(objRow(C1)("priceitem")) & ", " & ToDouble(objRow(C1)("pricenota")) & ", " & ToDouble(objRow(C1)("pricekhusus")) & ", " & ToDouble(objRow(C1)("pricesilver")) & ", " & ToDouble(objRow(C1)("pricegold")) & "," & ToDouble(objRow(C1)("priceplatinum")) & ", " & ToDouble(objRow(C1)("MinimPrice")) & ", " & objRow(C1)("itemdtloid") & ")"
                        objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        Session("DtlOid") += 1
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & Session("DtlOid") & " WHERE tablename='QL_trnorderdtl' And branch_code = '" & CabangNya.SelectedValue & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                End If

                If orderstatus.Text = "In Approval" Then
                    Session("AppOid") = GenerateID("QL_Approval", CompnyCode)
                    If Session("oid") <> Nothing Or Session("oid") <> "" Then
                        If Not Session("TblApproval") Is Nothing Then
                            Dim objTable As DataTable : objTable = Session("TblApproval")
                            For c1 As Int16 = 0 To objTable.Rows.Count - 1
                                sSql = "INSERT INTO QL_APPROVAL (cmpcode, approvaloid, branch_code, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser, approvaldate, approvaltype, approvallevel, approvalstatus) VALUES" & _
                                " ('" & CompnyCode & "', " & Session("AppOid") + c1 & ", '" & CabangNya.SelectedValue & "', '" & "SO" & Session("oid") & "_" & Session("AppOid") + c1 & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'New','QL_TRNORDERMST', '" & Session("oid") & "', 'In Approval', '0', '" & objTable.Rows(c1).Item("approvaluser") & "', '1/1/1900', '" & objTable.Rows(c1).Item("approvaltype") & "', '1', '" & objTable.Rows(c1).Item("approvalstatus") & "')"
                                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                            Next

                            sSql = "UPDATE ql_mstoid set lastoid = " & Session("AppOid") + objTable.Rows.Count - 1 & " Where tablename = 'QL_Approval' AND cmpcode='" & CompnyCode & "'"
                            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                        End If
                    Else
                        showMessage("Simpan SO terlebih dahulu !!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                        objTrans.Rollback() : objCmd.Connection.Close()
                        Exit Sub
                    End If
                End If
                '===============================================================================
                objTrans.Commit() : objCmd.Connection.Close()

            Catch ex As Exception
                objTrans.Rollback() : objCmd.Connection.Close()
                showMessage(ex.ToString & sSql, CompnyName & " - ERROR", 2, "modalMsgBoxWarn")
                orderstatus.Text = "In Process" : Exit Sub
            End Try
        End If
        Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx?awal=true")
    End Sub

    Protected Sub OrderQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OrderQty.TextChanged
        Try
            If itemlongdesc.Text.Trim = "" Then
                showMessage("Silahkan pilih Barang dahulu!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If OrderQty.Text.Trim = "" Then
                OrderQty.Text = "0"
            Else
                If currencyoid.SelectedValue = "1" Then
                    If typeSO.SelectedValue = "User" Then
                        TxtTotKotor.Text = ToMaskEdit(OrderQty.Text * OrderPrice.Text, 3)
                        Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                    Else
                        diskonqty() : fillTotalQty()
                        Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                    End If
                Else
                    TxtTotKotor.Text = ToMaskEdit(OrderQty.Text * OrderPrice.Text, 3)
                    Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                End If
                OrderQty.Text = ToMaskEdit(ToDouble(OrderQty.Text), 3)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            orderstatus.Text = "In Process"
            Exit Sub
        End Try

    End Sub

    Protected Sub OrderPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OrderPrice.TextChanged
        Try
            If ToDouble(DiscPct.Text) > ToDouble(OrderPrice.Text) * ToDouble(OrderQty.Text) Then
                showMessage("Diskon Harus kurang dari sama dengan Harga!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                DiscPct.Text = "0.00"
                Exit Sub
            End If
            disc.Text = ToMaskEdit(ToDouble(OrderQty.Text) * ToDouble(DiscPct.Text), 3)

            If DDLFree.SelectedValue = "Y" Then
                TxtTotKotor.Text = "0.00"
            Else
                TxtTotKotor.Text = ToMaskEdit((ToDouble(OrderQty.Text.Trim) * ToDouble(OrderPrice.Text.Trim)), 3)
            End If
        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            orderstatus.Text = "In Process"
            Exit Sub
        End Try

    End Sub

    Protected Sub GVmstOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmstOrder.SelectedIndexChanged
        Response.Redirect("ft_trnSlsIntOrder.aspx?branch_code=" & GVmstOrder.SelectedDataKey("branch_code").ToString & "&oid=" & GVmstOrder.SelectedDataKey("ordermstoid") & "")
    End Sub

    Protected Sub DDLStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLStatus.SelectedIndexChanged
        chkStatus.Checked = True
    End Sub

    Protected Sub FilterPeriod1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterPeriod1.TextChanged
        chkPeriod.Checked = True
    End Sub

    Protected Sub CabangNya_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CabangNya.SelectedIndexChanged
        ddlSales() : InitAllDDL()
        DDLWH.SelectedValue = CabangNya.SelectedValue : InitPromo()
    End Sub

    Protected Sub cbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCash.CheckedChanged
        If custoid.Text = "" Then
            cbCash.Checked = False
            showMessage("Silahkan pilih customer terlebih dahulu!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            If cbCash.Checked Then
                'Type CASH
                DDLTop.SelectedValue = "34"
            Else
                'Kembalikan ke default based on mst cust
                DDLTop.SelectedValue = GetStrData("SELECT timeofpayment FROM QL_MSTCUST where custoid = " & custoid.Text & "")
            End If
        End If
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        Try
            Dim sWhere As String
            sWhere = "Where m.branch_code='" & CabangNya.SelectedValue & "' AND m.ordermstoid=" & Session("oid") & ""
            sSql = "Select flagprint From QL_trnordermst m " & sWhere & ""
            If GetScalar(sSql) = 0 Then
                report.Load(Server.MapPath("~/report/rptNotaSO.rpt"))
                report.SetParameterValue("sWhere", sWhere)

                Dim crConnInfo As New ConnectionInfo()
                With crConnInfo
                    .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                    .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                    .IntegratedSecurity = True
                End With
                SetDBLogonForReport(crConnInfo, report)
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "Sales_Order")
                report.Close() : report.Dispose()

                Dim objConn As New SqlClient.SqlConnection(ConnStr)
                Dim objTrans As SqlClient.SqlTransaction
                Dim objCmd As New SqlClient.SqlCommand

                objConn.Open()
                objCmd.Connection = objConn
                objTrans = objConn.BeginTransaction()
                objCmd.Transaction = objTrans

                sSql = "Update QL_trnordermst Set flagprint=1 Where ordermstoid=" & Session("oid") & " And branch_code='" & CabangNya.SelectedValue & "'"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                objTrans.Commit() : objCmd.Connection.Close()
            Else
                showMessage("Maaf, Nota SO " & orderno.Text & " sudah pernah di print..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
        Catch ex As Exception
            report.Close() : report.Dispose()
            showMessage(ex.ToString & "<br />" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Protected Sub typeSO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles typeSO.SelectedIndexChanged
        clearControlAddItem() : ControlEcommers()
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            SubFunction()
        End If
    End Sub

    Protected Sub ibtneraseitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtneraseitem.Click
        itemoid.Text = "" : jPrice.SelectedIndex = 0
        itemlongdesc.Text = "" : Merk.Text = ""
        OrderQty.Text = ToMaskEdit(0.0, 3)
        OrderPrice.Text = ToMaskEdit(0.0, 3)
        Discdtl1.Text = ToMaskEdit(0.0, 3)
        TxtTotKotor.Text = ToMaskEdit(0.0, 3)
        lblVarPrice.Text = ToMaskEdit(0.0, 3)
        txtVarPrice.Text = ToMaskEdit(0.0, 3)
        pricenormal.Text = ToMaskEdit(0.0, 3)
        pricenota.Text = ToMaskEdit(0.0, 3)
        pricekhusus.Text = ToMaskEdit(0.0, 3)
        pricesilver.Text = ToMaskEdit(0.0, 3)
        priceplatinum.Text = ToMaskEdit(0.0, 3)
        pricegold.Text = ToMaskEdit(0.0, 3)
        MinimPrice.Text = ToMaskEdit(0.0, 3)
        BottomPrice.Text = ToMaskEdit(0.0, 3)
        StatusBarang.Text = ""
        ItemGroup.Text = ""
    End Sub

    Protected Sub disctype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles disctype.SelectedIndexChanged
        If disctype.SelectedValue = "AMOUNT" Then
            discount.Text = "(Amount)"
            disc.Text = "0.00"
        ElseIf disctype.SelectedValue = "PERCENTAGE" Then
            discount.Text = "(%)"
            disc.Text = "0.00"
        End If
    End Sub

    Protected Sub ddlUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUnit.SelectedIndexChanged
        If lbltempAmt.Text = "" Then
            lbltempAmt.Text = 0
        End If

        If currencyoid.SelectedValue = "2" Then
            If lbltempAmt.Text = "0.00" Then
                If taxable.SelectedValue = "INCLUDE" Then
                    txtVarPrice.Text = ToMaskEdit(txtVarPrice.Text, 3)
                    lbltempAmt.Text = ToMaskEdit(txtVarPrice.Text, 3)
                    TxtTotKotor.Text = ToMaskEdit(ToDouble(OrderQty.Text * OrderPrice.Text), 3)
                    Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                Else
                    If typeSO.SelectedValue = "User" Then
                        OrderPrice.Text = ToMaskEdit(OrderPrice.Text, 3)
                        lbltempAmt.Text = ToMaskEdit(OrderPrice.Text, 3)
                        TxtTotKotor.Text = ToMaskEdit(ToDouble(OrderQty.Text * OrderPrice.Text), 3)
                        Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                    End If
                End If

            Else
                If taxable.SelectedValue = "INCLUDE" Then
                    txtVarPrice.Text = ToMaskEdit(lbltempAmt.Text, 3)
                    OrderPrice.Text = ToMaskEdit(ToDouble(txtVarPrice.Text / 1.1), 3)
                    TxtTotKotor.Text = ToMaskEdit(ToDouble(OrderQty.Text * OrderPrice.Text), 3)
                    Konversike3.Text = ToMaskEdit(OrderQty.Text, 3)
                Else
                    If typeSO.SelectedValue = "User" Then
                        OrderPrice.Text = ToMaskEdit(lbltempAmt.Text, 3)
                        TxtTotKotor.Text = ToMaskEdit(ToDouble(OrderQty.Text * OrderPrice.Text), 3)
                    End If
                End If
            End If
        Else
            If taxable.SelectedValue = "INCLUDE" Then
                lbltempAmt.Text = txtVarPrice.Text
            Else
                lbltempAmt.Text = OrderPrice.Text
            End If
            changebonus() : diskonqty() : fillTotalQty()
        End If

    End Sub

    Protected Sub gvItemOrdered_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemOrdered.SelectedIndexChanged
        If custoid.Text = "" Then
            showMessage("Silahkan pilih Customer terlebih dahulu!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        itemoid.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("itemoid")
        itemlongdesc.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("itemldesc")
        OrderQty.Text = ToMaskEdit(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("OrderQty"), 1)
        If taxable.SelectedValue = "INCLUDE" Then
            txtVarPrice.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("varprice")
        Else
            txtVarPrice.Text = ToMaskEdit(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("varprice"), 1)
        End If

        initUnit() : editharganota()

        Merk.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("merk")
        ddlUnit.SelectedValue = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("itemunit")
        lbltempAmt.Text = Session("tabeldtl").rows(gvItemOrdered.SelectedIndex).item("tempAmtUsd")
        ItemNote.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("ItemNote")
        ItemGroup.Text = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("Itemgroupcode")
        Session("vSeqGLDtl") = Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("orderdtlseq")
        trnorderdtlseq.Text = Session("vSeqGLDtl")

        I_U2.Text = "Update Detail"
        totalqty.Text = ToMaskEdit(ToDouble(OrderQty.Text), 3)
        OrderPrice.Text = ToMaskEdit(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("OrderPrice"), 2)
        TxtTotKotor.Text = ToMaskEdit(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("total"), 2)
        Discdtl1.Text = ToDouble(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("amtdtldisc1") / Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("OrderQty"))
        Discdtl2.Text = ToDouble(Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("amtdtldisc2") / Session("TabelDtl").rows(gvItemOrdered.SelectedIndex).item("OrderQty"))
        Konversike3.Text = OrderPrice.Text

        If taxable.SelectedValue = "INCLUDE" Then
            OrderPrice.Enabled = False : OrderPrice.CssClass = "inpTextDisabled"
            txtVarPrice.Visible = True : lblVarPrice.Visible = True
        End If
    End Sub

    Protected Sub DDLFree_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLFree.SelectedIndexChanged
        fillTotalQty()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        Dim dtTempWeb As DataTable = checkApprovaldata("QL_trnordermst", "In Approval", Session("oid"), "", "New", CabangNya.SelectedValue)

        'hapus tabel detail
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            If checkApproval("QL_TRNORDERMST", "In Approval", Session("oid"), "New", "FINAL", CabangNya.SelectedValue) > 0 Then
                showMessage("Sales Order ini sudah send for Approval", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            sSql = "Delete from QL_trnorderdtl Where trnordermstoid='" & Session("oid") & "' AND branch_code='" & CabangNya.SelectedValue & "'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            'hapus tabel master
            sSql = "Delete from QL_trnordermst where ordermstoid='" & Session("oid") & "' AND branch_code='" & CabangNya.SelectedValue & "'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            objTrans.Commit() : objCmd.Connection.Close()

        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            showMessage(ex.ToString & "</br />" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End Try
        showMessage("Data telah dihapus !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub ibtnsearchitem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearchitem.Click
        If custoid.Text = "" Then
            showMessage("Maaf, Pilih Customer dulu!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If
        BindDataItem("")
        gvItemSearch.Visible = True
    End Sub

    Protected Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAdd.Click
        Try
            Dim sMsg As String = "", totalcashback As Double, ProQty As Double, DiscPerQty As Double, rQty As Double = 0, unitseq As Int16 = 1, tempItemBonus As String
            If itemoid.Text = "" Then
                sMsg &= "- Maaf, Silahkan pilih item dulu!!<BR>"
            End If

            If ToDouble(OrderQty.Text) <= 0 Then
                sMsg &= "- Maaf, Quantity tidak boleh 0..!!<BR>"
            End If

            If OrderPrice.Text = "0.00" Then
                sMsg &= "- Maaf, Price per unit tidak boleh 0..!!<BR>"
            End If

            If OrderPrice.Text = "" Then
                sMsg &= "- Maaf, Price per unit tidak boleh Kosong..!!<BR>"
            End If

            If cbBonus.Checked = True Then
                If OrderPrice.Text > "0.00" Then
                    sMsg &= "- Price untuk item bonus = 0..!!<BR>"
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

            If jPrice.SelectedValue = "NORMAL" Then
                If ToDouble(OrderPrice.Text) < ToDouble(pricenormal.Text) Then
                    sMsg &= "- Price Katalog=" & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(pricenormal.Text, 3) & "..!!<BR>"
                End If
            ElseIf jPrice.SelectedValue = "NOTA" Then
                If ToDouble(OrderPrice.Text) < ToDouble(pricenota.Text) Then
                    sMsg &= "- Price Katalog " & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(pricenota.Text, 3) & "..!!<BR>"
                End If
            ElseIf jPrice.SelectedValue = "SILVER" Then
                If ToDouble(OrderPrice.Text) < ToDouble(pricesilver.Text) Then
                    sMsg &= "- Price Katalog=" & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(pricesilver.Text, 3) & "..!!<BR>"
                End If
            ElseIf jPrice.SelectedValue = "PLATINUM" Then
                If ToDouble(OrderPrice.Text) < ToDouble(priceplatinum.Text) Then
                    sMsg &= "- Price Katalog=" & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(priceplatinum.Text, 3) & "..!!<BR>"
                End If
            ElseIf jPrice.SelectedValue = "GOLD" Then
                If ToDouble(OrderPrice.Text) < ToDouble(pricegold.Text) Then
                    sMsg &= "- Price Katalog=" & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(pricegold.Text, 3) & "..!!<BR>"
                End If
            ElseIf jPrice.SelectedValue = "KHUSUS" Then
                If ToDouble(OrderPrice.Text) < ToDouble(pricekhusus.Text) Then
                    sMsg &= "- Price Katalog=" & ToMaskEdit(OrderPrice.Text, 3) & "< Price " & jPrice.SelectedValue.ToLower & "=" & ToMaskEdit(pricekhusus.Text, 3) & "..!!<BR>"
                End If
            End If

            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - INFORMATION", 2, "modalMsgBoxWarn")
                lblMessage.Text = "INFONYA" : LblsMsg.Text = sMsg
                lblMessage.Visible = False
            End If

            If cbBonus.Checked = True Then : tempItemBonus = "Yes" : Else : tempItemBonus = "No" : End If
            CreateDtlTbl() : Dim objTable As DataTable = Session("TabelDtl")
            dv = objTable.DefaultView
            If dv.Count > 0 Then : taxable.Enabled = False : End If

            'Cek apa sudah ada item yang sama dalam Tabel Detail
            If I_U2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & itemoid.Text
            Else
                dv.RowFilter = "itemoid=" & itemoid.Text & " AND orderdtlseq <> " & trnorderdtlseq.Text
            End If

            If dv.Count > 0 Then
                showMessage("Maaf, Data Barang ini sudah ditambahkan di list sebelumnya, Silahkan check...!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                dv.RowFilter = ""
                Exit Sub
            End If
            dv.RowFilter = ""

            Dim objRow As DataRow
            If I_U2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("orderdtlseq") = objTable.Rows.Count + 1
                Session("ItemLine") += 1
            Else 'update
                objRow = objTable.Rows(trnorderdtlseq.Text - 1)
                objRow.BeginEdit()
            End If

            objRow("cmpcode") = CompnyCode
            Dim test As String = disc.Text.Trim

            objRow("orderdtloid") = 0
            objRow("ordermstoid") = 0
            objRow("itemoid") = itemoid.Text
            objRow("orderqty") = ToDouble(OrderQty.Text.Trim)
            objRow("itemunit") = ddlUnit.SelectedValue
            objRow("unitseq") = unitseq
            objRow("itemgroupcode") = ItemGroup.Text
            objRow("unit") = ddlUnit.SelectedItem.Text
            objRow("itemnote") = ItemNote.Text
            objRow("disctype") = disctype.SelectedValue
            objRow("amtdtldisc1") = ToDouble(Discdtl1.Text) * ToDouble(OrderQty.Text)
            objRow("amtdtldisc2") = ToDouble(Discdtl2.Text) * ToDouble(OrderQty.Text)

            Dim PromoType As String = ""
            sSql = "Select COUNT(itemoid) from QL_mstpromodtl Where promoid='" & ddlPromo.SelectedValue & "' AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA' And qtyitemoid1<=" & ToDouble(OrderQty.Text) & ""
            'Dim ProCek As Integer = GetScalar(sSql)
            If GetScalar(sSql) > 0 Then
                sSql = "SELECT promtype FROM QL_mstpromo WHERE cmpcode='" & CompnyCode & "' AND promflag<>'INACTIVE' AND branchFlag like '%" & CabangNya.SelectedValue & "%' AND promoid='" & ddlPromo.SelectedValue & "'"
                PromoType = GetStrData(sSql)

                sSql = "Select qtyitemoid1, totalcashback FROM QL_mstpromodtl Where promoid='" & ddlPromo.SelectedValue & "' AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA'"
                Dim dt As DataTable = cKoneksi.ambiltabel(sSql, "QL_Promodtl")
                ProQty = dt.Rows(0)("qtyitemoid1").ToString
                totalcashback = dt.Rows(0)("totalcashback").ToString
                rQty = Math.Floor(ToDouble(OrderQty.Text / ProQty))

                If taxable.SelectedValue = "INCLUDE" Then
                    If PromoType = "Disc % (Item Only)" Then
                        DiscPerQty = ToDouble(OrderPrice.Text * (DiscPct.Text / 100))
                        If StatusBarang.Text <> "" Then
                            objRow("discamtpct") = ToMaskEdit(ToDouble(DiscPct.Text), 3)
                            objRow("disc") = ToDouble(DiscPerQty * (ProQty * rQty))
                        Else
                            objRow("discamtpct") = "0.0"
                            objRow("disc") = "0.0"
                        End If
                    ElseIf PromoType = "DISCAMT" Then
                        objRow("discamtpct") = ToMaskEdit(0.0, 3)
                        objRow("disc") = ToDouble(totalcashback) * ToDouble(rQty)
                    Else
                        objRow("discamtpct") = "0.0"
                        objRow("disc") = "0.0"
                    End If
                Else
                    If PromoType = "Disc % (Item Only)" Then
                        If StatusBarang.Text <> "" Then
                            DiscPerQty = ToDouble(OrderPrice.Text * (DiscPct.Text / 100))
                            objRow("discamtpct") = ToMaskEdit(ToDouble(DiscPct.Text), 3)
                            objRow("disc") = ToDouble(DiscPerQty * (ProQty * rQty))
                        Else
                            objRow("discamtpct") = "0.0"
                            objRow("disc") = "0.0"
                        End If
                    ElseIf PromoType = "DISCAMT" Then
                        objRow("discamtpct") = ToMaskEdit(0, 3)
                        objRow("disc") = ToDouble(totalcashback) * ToDouble(rQty)
                    Else
                        objRow("discamtpct") = ToMaskEdit(ToDouble(DiscPct.Text), 3)
                        objRow("disc") = ToDouble(disc.Text)
                    End If
                End If
            Else
                If taxable.SelectedValue = "INCLUDE" Then
                    objRow("discamtpct") = "0.0"
                    objRow("disc") = "0.0"
                Else
                    objRow("discamtpct") = ToMaskEdit(ToDouble(DiscPct.Text), 3)
                    objRow("disc") = ToDouble(disc.Text)
                End If
            End If

            objRow("orderprice") = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
            objRow("orderpriceidr") = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
            objRow("orderpriceusd") = ToMaskEdit(ToDouble(OrderPrice.Text), 3)
            objRow("tempAmtUsd") = ToMaskEdit(ToDouble(lbltempAmt.Text), 3)
            objRow("upduser") = Session("UserID")
            objRow("updtime") = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            objRow("ItemLDesc") = itemlongdesc.Text
            objRow("merk") = Merk.Text
            objRow("currency") = currencyoid.SelectedValue.Trim
            objRow("promooid") = ddlPromo.SelectedValue
            objRow("branch_code") = CabangNya.SelectedValue
            objRow("varprice") = ToDouble(txtVarPrice.Text)
            objRow("priceitem") = ToDouble(pricenormal.Text)
            objRow("pricenota") = ToDouble(pricenota.Text)
            objRow("pricekhusus") = ToDouble(pricekhusus.Text)
            objRow("pricesilver") = ToDouble(pricesilver.Text)
            objRow("priceplatinum") = ToDouble(priceplatinum.Text)
            objRow("pricegold") = ToDouble(pricegold.Text)
            objRow("MinimPrice") = ToDouble(MinimPrice.Text)
            objRow("hpp") = ToDouble(hpp.Text)
            objRow("priceexpedisi") = ToDouble(priceexpedisi.Text)
            objRow("itemdtloid") = itemdtloid.Text
            objRow("total") = ToDouble((objRow("orderpriceidr") * objRow("orderqty") - (objRow("amtdtldisc1") + objRow("amtdtldisc2"))) - objRow("disc"))
            objRow("totkotor") = ToDouble((objRow("orderpriceidr") * objRow("orderqty") - (objRow("amtdtldisc1") + objRow("amtdtldisc2"))) - objRow("disc"))

            If StatusBarang.Text = "PROMO" Then
                objRow("jenisprice") = "PROMO"
            Else
                objRow("jenisprice") = jPrice.SelectedValue
            End If

            If I_U2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else 'update
                objRow.EndEdit()
            End If

            sSql = "Select COUNT(itemoid) FROM QL_mstpromodtl Where promoid='" & ddlPromo.SelectedValue & "' AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA' And qtyitemoid1<=" & ToDouble(OrderQty.Text) & ""
            Dim cekPro As Integer = GetScalar(sSql)
            If cekPro > 0 Then
                sSql = "Select qtyitemoid1 from QL_mstpromodtl Where promoid='" & ddlPromo.SelectedValue & "' AND itemoid=" & itemoid.Text & " AND typebarang='UTAMA'" : Dim QtyUtama As Decimal = GetScalar(sSql)

                'Function Untuk Ambil barang bonus kemudian di masukkan di 
                BindItemBonus()
                '-----------------------
                Dim tblitem As DataTable : Dim TotBonus As Double = 0
                tblitem = Session("ItemBonus")
                Dim tbitv As DataView = tblitem.DefaultView
                tbitv.RowFilter = "promoid=" & ddlPromo.SelectedValue & " AND typebarang='BONUS'"

                If tbitv.Count > 0 Then
                    For a1 As Integer = 0 To tbitv.Count - 1
                        If I_U2.Text = "New Detail" Then
                            objRow = objTable.NewRow()
                            objRow("orderdtlseq") = objTable.Rows.Count + 1
                            Session("ItemLine") += 1
                        Else 'update
                            objRow = objTable.Rows(trnorderdtlseq.Text - 1)
                            objRow.BeginEdit()
                        End If
                        Dim QtyBonus As Decimal = Math.Floor(OrderQty.Text / QtyUtama)
                        Dim FreeQty As Decimal = Math.Floor(QtyBonus * tbitv.Item(a1)("QtyTuku"))
                        objRow("cmpcode") = CompnyCode
                        objRow("orderdtloid") = 0
                        objRow("ordermstoid") = 0
                        objRow("itemoid") = tbitv.Item(a1)("itemoid")
                        objRow("orderqty") = ToDouble(FreeQty)
                        objRow("itemunit") = tbitv.Item(a1)("satuan1").ToString
                        objRow("unitseq") = unitseq + 1
                        objRow("itemgroupcode") = tbitv.Item(a1)("itemgroupcode").ToString
                        objRow("unit") = tbitv.Item(a1)("unit").ToString
                        objRow("itemnote") = ""
                        objRow("disctype") = ToDouble(0)
                        objRow("amtdtldisc1") = ToDouble(0)
                        objRow("amtdtldisc2") = ToDouble(0)
                        objRow("discamtpct") = "0.0"

                        If PromoType = "BUNDLING" Then
                            objRow("disc") = ToMaskEdit(0, 3)
                            objRow("total") = ToMaskEdit(ToDouble(tbitv.Item(a1)("PriceNya")), 3)
                            objRow("totkotor") = ToMaskEdit(ToDouble(tbitv.Item(a1)("PriceNya") * objRow("orderqty")), 3)
                        Else
                            objRow("total") = ToMaskEdit(0, 3)
                            objRow("totkotor") = ToMaskEdit(0, 3)
                            objRow("disc") = ToMaskEdit(ToDouble(tbitv.Item(a1)("priceList")), 3)
                        End If

                        objRow("orderprice") = ToMaskEdit(ToDouble(tbitv.Item(a1)("priceList")), 3)
                        objRow("orderpriceidr") = ToMaskEdit(ToDouble(tbitv.Item(a1)("priceList")), 3)
                        objRow("orderpriceusd") = ToMaskEdit(ToDouble(0), 3)
                        objRow("tempAmtUsd") = ToMaskEdit(ToDouble(0), 3) ',pd.totalamt
                        objRow("upduser") = Session("UserID")
                        objRow("updtime") = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                        objRow("ItemLDesc") = tbitv(a1)("itemdesc").ToString
                        objRow("merk") = tbitv(a1)("merk").ToString
                        granddisc.Text = ToMaskEdit(ToDouble(0), 3)
                        objRow("currency") = currencyoid.SelectedValue.Trim
                        objRow("promooid") = ddlPromo.SelectedValue
                        objRow("branch_code") = CabangNya.SelectedValue
                        objRow("varprice") = ToDouble(0)
                        objRow("jenisprice") = "BONUS"
                        objRow("priceitem") = ToDouble(pricenormal.Text)
                        objRow("pricenota") = ToDouble(pricenota.Text)
                        objRow("pricekhusus") = ToDouble(pricekhusus.Text)
                        objRow("pricesilver") = ToDouble(pricesilver.Text)
                        objRow("priceplatinum") = ToDouble(priceplatinum.Text)
                        objRow("pricegold") = ToDouble(pricegold.Text)
                        objRow("MinimPrice") = ToDouble(MinimPrice.Text)
                        objRow("hpp") = ToDouble(hpp.Text)
                        objRow("priceexpedisi") = ToDouble(priceexpedisi.Text)
                        objRow("itemdtloid") = itemdtloid.Text
                        TotBonus += ToDouble(tbitv.Item(a1)("totalamt"))

                        If I_U2.Text = "New Detail" Then
                            objTable.Rows.Add(objRow)
                        Else 'update
                            objRow.EndEdit()
                        End If
                    Next
                    Session("TotBonus") = ToDouble(0)
                End If
                tbitv.RowFilter = ""
            End If

            taxable.Enabled = False : taxable.CssClass = "inpTextDisabled"
            ddlPromo.Enabled = False : ddlPromo.CssClass = "inpTextDisabled"
            Session("TabelDtl") = objTable
            TotOrder() : ReAmount()
            gvItemOrdered.DataSource = objTable
            gvItemOrdered.DataBind()

            clearControlAddItem()
            trnorderdtlseq.Text = objTable.Rows.Count + 1
            gvItemOrdered.Visible = True : I_U2.Text = "New Detail"

        Catch ex As Exception
            showMessage(ex.ToString, CompnyName & " - WARNING", 1, "modalMsgBoxWarn")
            Exit Sub
        End Try
    End Sub

    Protected Sub ddlPromo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPromo.SelectedIndexChanged
        BindDataItem("")
    End Sub

    Protected Sub Discdtl1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Discdtl1.TextChanged
        fillTotalQty()
    End Sub

    Protected Sub Discdtl2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Discdtl2.TextChanged
        fillTotalQty()
    End Sub

    Protected Sub jPrice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles jPrice.SelectedIndexChanged
        Session("QL_mstitem") = Nothing
        gvItemSearch.Visible = False
        clearControlAddItem()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        '--------------------- generate NO ---------------------
        Dim flag As String = "", sTax As String = "", iCurID As Integer = 0, orderlimitbottom As String, sMsg As String = "", flagbottompricedtl As String = "", bottompricegrosir As Double = 0, bottompricegrosirperqty As Integer = 0, DtlOid As Integer = 0, tax As String = "" 
        If chkTax.Checked = True Then
            lbltax.Text = 1 : taxable.SelectedValue = "INC"
            tax = "0"
        Else
            lbltax.Text = 0 : taxable.SelectedValue = "NONTAX"
            tax = "1"
        End If

        If IsDate(toDate(orderdate.Text)) = False Then
            sMsg &= "Maaf, Tanggal Sales Order salah..!!<br>"
            orderstatus.Text = "In Process"
        Else
            If CDate(toDate(orderdate.Text)) < Format(GetServerTime(), "MM/dd/yyyy") Then
                sMsg &= "Maaf, Tanggal Sales Order tidak boleh backdate..!!<br> "
                orderstatus.Text = "In Process"
            End If

            If CDate(toDate(orderdate.Text)) > CDate(toDate("6/6/2079")) Then
                sMsg &= "Maaf, Tanggal Sales Order Date harus kurang dari 06/06/2079..!!<br>"
                orderstatus.Text = "In Process"
            End If
        End If

        If deliveryaddr.Text = "" Then
            sMsg &= "Maaf, Delivery Address Harus Di Isi..!!<br>"
            orderstatus.Text = "In Process"
        End If

        If IsDate(toDate(deliverydate.Text)) = False Then
            sMsg &= "Maaf, Tanggal Sales Order Delivery salah..!!"
            orderstatus.Text = "In Process"
        Else
            If CDate(toDate(deliverydate.Text)) < Format(GetServerTime(), "MM/dd/yyyy") Then
                sMsg &= "Maaf, Tanggal Sales Order Delivery tidak boleh backdate..!!<br>"
                orderstatus.Text = "In Process"
            End If

            If CDate(toDate(deliverydate.Text)) > CDate(toDate("6/6/2079")) Then
                sMsg &= "Maaf, Tanggal Sales Order Delivery harus kurang dari 06/06/2079..!!<br>"
                orderstatus.Text = "In Process"
            End If
        End If

        If CDate(toDate(orderdate.Text)) > CDate(toDate(deliverydate.Text)) Then
            sMsg &= "Maaf, Tanggal Order harus lebih kecil dari Tanggal Delivery !!"
            orderstatus.Text = "In Process"
        End If

        If custoid.Text = "" Then
            sMsg &= "Maaf, Silahkan pilih Customer dulu..!!<br>"
            orderstatus.Text = "In Process"
        End If

        If ordernote.Text.Length > 300 Then
            sMsg &= "Maaf, Note maximum 300 character !!<br>"
            orderstatus.Text = "In Process"
        End If

        If deliveryaddr.Text.Length > 200 Then
            sMsg &= "Maaf, Delivery Address maximal 200 character !!<br>"
            orderstatus.Text = "In Process"
        End If

        If orderamount.Text = "0.00" Then
            sMsg &= "Maaf, Total nota Amount netto SO sekarang tidak boleh nol !!<br>"
            orderstatus.Text = "In Process"
        End If

        sSql = "Select ordermstoid, orderno, trnorderstatus From QL_trnordermst WHere trnorderstatus IN ('Canceled','Approved','Closed','Rejected','POST') AND branch_code='" & CabangNya.SelectedValue & "' AND ordermstoid=" & oid.Text
        Dim dta As DataTable = cKoneksi.ambiltabel(sSql, "dta")
        If ToDouble(dta.Rows.Count) > 0 Then
            For i As Integer = 0 To dta.Rows.Count - 1
                sMsg = "- Maaf, No. draft SO '" & dta.Rows(i).Item("ordermstoid") & "' sudah di '" & dta.Rows(i).Item("trnorderstatus") & "' dengan nomer '" & dta.Rows(i).Item("orderno") & "'..!!<br>"
            Next
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        '--------------------------------------------------------------------------
        '-------------------------------Warning Done-------------------------------
        orderlimitbottom = GetStrData("select flagbottomprice from ql_trnordermst Where ordermstoid = " & oid.Text & " And branch_code='" & CabangNya.SelectedValue & "'")

        Dim sCabang As String = GetStrData("Select genother1 from ql_mstgen where gencode='" & CabangNya.SelectedValue & "' AND gengroup='CABANG'")
        Dim sNo As String = "SO" & tax & "/" & sCabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(orderno, 4) AS INT)),0) AS IDNEW FROM ql_trnordermst WHERE branch_code='" & CabangNya.SelectedValue & "' AND orderno LIKE '" & sNo & "%'"
        iCurID = cKoneksi.ambilscalar(sSql) + 1
        orderno.Text = GenNumberString(sNo, "", iCurID, 4)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            ' Update SO
            sSql = "UPDATE QL_trnordermst SET orderno='" & orderno.Text & "', ordertaxtype='" & taxable.SelectedValue & "', trnorderdate='" & Format(GetServerTime(), "MM/dd/yyyy") & "', periodacctg='" & Format(GetServerTime(), "yyyy-MM") & "', typeso='" & typeSO.SelectedValue.ToString & "', timeofpaymentSO='" & DDLTop.SelectedValue & "', trnordertype='" & orderflag.SelectedValue & "', trnorderstatus='Approved', trnpaytype= " & DDLTop.SelectedValue & ", trncustoid='" & custoid.Text & "', trncustname = '" & Tchar(CustName.Text) & "', trnordernote='" & Tchar(ordernote.Text) & "', upduser='" & Session("UserID") & "', updtime=current_timestamp, trnorderref='" & Tchar(POref.Text) & "', delivdate = '" & toDate(deliverydate.Text) & "', salesoid = " & spgOid.SelectedValue & ", spgoid = " & spgOid.SelectedValue & ", amtjualnetto = " & ToDouble(totalorder.Text) & ", amtjualnettoidr = " & ToDouble(totalorder.Text) & ", amtjualnettousd = " & ToDouble(totalorder.Text) & ", promooid = " & ddlPromo.SelectedValue & ", flagbottomprice = 'N', to_branch = '" & CabangNya.SelectedValue & "', flagTax=" & lbltax.Text & ", typeExpedisi='" & ddlexpedisi.SelectedValue & "', bundling='" & lblbundling.Text & "', amtdisc1=" & ToDouble(DiscHdr1.Text) & ", amtdisc2=" & ToDouble(DiscHdr2.Text) & ", flagCash = '" & IIf(cbCash.Checked, "Y", "N") & "', flagSR = '" & IIf(cbSR.Checked, "Y", "N") & "', branch_code='" & CabangNya.SelectedValue & "' WHERE ordermstoid=" & oid.Text & " And branch_code='" & CabangNya.SelectedValue & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "Select ISNULL(lastoid,0) from QL_mstoid where tablename = 'QL_trnorderdtl' and branch_code='" & CabangNya.SelectedValue & "'"
            xCmd.CommandText = sSql : DtlOid = xCmd.ExecuteScalar + 1

            If Not Session("TabelDtl") Is Nothing Then
                sSql = "Delete from QL_trnorderdtl where trnordermstoid='" & oid.Text & "' and branch_code='" & CabangNya.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insert tabel detail
                Dim objTable As DataTable
                Dim objRow() As DataRow
                objTable = Session("TabelDtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For C1 As Integer = 0 To objRow.Length - 1
                    sSql = "Select bottompricegrosir from ql_mstitem Where itemoid=" & objTable.Rows(C1)("itemoid") & ""
                    xCmd.CommandText = sSql
                    bottompricegrosir = ToDouble(objTable.Rows(C1)("orderqty")) * xCmd.ExecuteScalar
                    bottompricegrosirperqty = xCmd.ExecuteScalar

                    If bottompricegrosir > (objTable.Rows(C1)("total")) Then
                        If ddlPromo.SelectedValue = 0 Then
                            flagbottompricedtl = "T"
                        Else
                            flagbottompricedtl = "N"
                        End If
                    End If

                    sSql = "INSERT into QL_trnorderdtl (cmpcode, branch_code, trnorderdtloid, trnordermstoid, itemoid, trnorderdtlseq, trnorderdtlqty, trnorderdtlunitoid, trnorderdtlnote, trnorderprice, trnorderpriceidr, trnorderpriceusd, upduser, updtime, unitseq, trnamountdtl, trnamountdtlidr, trnamountdtlusd, trndiskonpromo, trndiskonpromoidr, trndiskonpromousd, trnamountbruto, trnamountbrutoidr, trnamountbrutousd, promooid , flagbottompricedtl, bottomprice, currencyoid, amountbottomprice, varprice, to_branch, tempAmtUsd, amtdtldisc1, amtdtldisc2, jenisprice, pricenormal, pricenota, pricekhusus, pricesilver, pricegold, priceplatinum, priceminim, itemdtloid) " & _
                     " VALUES ('" & CompnyCode & "', '" & CabangNya.SelectedValue & "', " & DtlOid & ", " & oid.Text & ", " & objRow(C1)("itemoid") & ", " & objRow(C1)("orderdtlseq") & ", " & ToDouble(objRow(C1)("orderqty")) & ", " & objRow(C1)("itemunit") & ", '" & Tchar(objRow(C1)("itemnote").ToString.Trim) & "', " & ToDouble(objRow(C1)("orderprice")) & ", " & ToDouble(objRow(C1)("orderprice")) & ", " & ToDouble(objRow(C1)("orderprice")) & ",'" & objRow(C1)("upduser").ToString & "', CURRENT_TIMESTAMP," & objRow(C1)("unitseq") & ", " & ToDouble(objRow(C1)("totKotor")) & "," & ToDouble(objRow(C1)("totKotor")) & ", " & ToDouble(objRow(C1)("totKotor")) & ", " & ToDouble(objRow(C1)("disc")) & "," & ToDouble(objRow(C1)("disc")) & ", " & ToDouble(objRow(C1)("disc")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & ToDouble(objRow(C1)("orderprice")) * ToDouble(objRow(C1)("orderqty")) & ", " & (objRow(C1)("promooid")) & ",'" & flagbottompricedtl & "', " & ToDouble(bottompricegrosirperqty) & ", " & objRow(C1)("currency") & "," & ToDouble(bottompricegrosir) & ", " & ToDouble(objRow(C1)("varprice")) & ", '" & CabangNya.SelectedValue & "'," & ToDouble(objRow(C1)("tempAmtUsd")) & ", " & ToDouble(objRow(C1)("amtdtldisc1")) & ", " & ToDouble(objRow(C1)("amtdtldisc2")) & ", '" & objRow(C1)("jenisprice") & "', " & ToDouble(objRow(C1)("priceitem")) & ", " & ToDouble(objRow(C1)("pricenota")) & ", " & ToDouble(objRow(C1)("pricekhusus")) & ", " & ToDouble(objRow(C1)("pricesilver")) & ", " & ToDouble(objRow(C1)("pricegold")) & "," & ToDouble(objRow(C1)("priceplatinum")) & ", " & ToDouble(objRow(C1)("MinimPrice")) & ", " & objRow(C1)("itemdtloid") & ")"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    DtlOid += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & DtlOid & " WHERE tablename='QL_trnorderdtl' And branch_code = '" & CabangNya.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\ft_trnSlsIntOrder.aspx?awal=true")
    End Sub
#End Region
End Class
