
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunctionAccounting

Partial Class Accounting_trnMonthlyClosing
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim objCmd As New SqlCommand("", conn)
    Dim objReader As SqlDataReader
    Dim sSql As String = ""
    Dim cKoneksi As New Koneksi
    Dim cKoneksi2 As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function BindClosingData(ByVal iMonth As Integer, ByVal iYear As Integer) As DataTable
        Dim tempDate As New Date(iYear, iMonth, 1)
        '============ OLD VERSION 
        sSql = "SELECT branch_code,acctgoid,isnull(acctgcode,'0') acctgcode,acctgdesc,amtopen,SUM(debet) AS amtdebet,SUM(credit) AS amtcredit, amtopen+SUM(debet)-SUM(credit) AS amtbalance,seq FROM (" & _
            "SELECT crd.branch_code,a1.acctgoid,a1.acctgcode,+a1.acctgdesc,ISNULL(crd.amtopen,0) AS amtopen,'debet'=CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END,'credit'=CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END,0.00 AS amtbalance,0 AS seq FROM QL_mstacctg a1 INNER JOIN QL_crdgl crd ON crd.cmpcode=a1.cmpcode AND crd.acctgoid=a1.acctgoid AND crd.glgroup='MONTHLY' AND crd.periodacctg='" & GetDateToPeriodAcctg3(tempDate) & "' And crd.branch_code='" & DDLCabang.SelectedValue & "' LEFT JOIN QL_trngldtl gd ON a1.cmpcode=gd.cmpcode AND a1.acctgoid=gd.acctgoid and gd.branch_code='" & DDLCabang.SelectedValue & "'And gd.glmstoid > 0 LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' and gm.branch_code='" & DDLCabang.SelectedValue & "' WHERE a1.cmpcode='" & CompnyCode & "') AS crd GROUP BY acctgoid,acctgcode,acctgdesc,amtopen,seq,branch_code ORDER BY acctgcode "

        '============ NEW VERSION (Penggabungan ke induk utk COA 51,52,53 => Query utk 51,52,53 dibedakan dengan yg COA lain)
        'sSql = "SELECT acctgoid,isnull(acctgcode,'0') acctgcode,acctgdesc,amtopen,SUM(debet) AS amtdebet,SUM(credit) AS amtcredit," & _
        '    "amtopen+SUM(debet)-SUM(credit) AS amtbalance,seq FROM (" & _
        '    "SELECT a1.acctgoid,a1.acctgcode,a1.acctgdesc,ISNULL(crd.amtopen,0) AS amtopen, " & _
        '    "'debet'=CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END," & _
        '    "'credit'=CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END," & _
        '    "0.00 AS amtbalance,0 AS seq FROM QL_mstacctg a1 " & _
        '    "LEFT JOIN QL_crdgl crd ON crd.cmpcode=a1.cmpcode AND crd.acctgoid=a1.acctgoid AND crd.glgroup='MONTHLY' AND crd.periodacctg='" & GetDateToPeriodAcctg3(tempDate) & "' " & _
        '    "LEFT JOIN QL_trngldtl gd ON a1.cmpcode=gd.cmpcode AND a1.acctgoid=gd.acctgoid " & _
        '    "LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' AND gm.glmstoid>0 " & _
        '    "WHERE a1.cmpcode='" & CompnyCode & "' and a1.acctgarea='" & AcctgArea & "' AND a1.acctgcode IN (" & _
        '    "SELECT i.interfacevalue FROM QL_mstinterface i WHERE i.cmpcode=a1.cmpcode and i.acctgarea='" & AcctgArea & "' " & _
        '    "AND i.interfacevar IN ('VAR_SI_HPP_MANUF','VAR_SI_HPP_SLITTER','VAR_SI_HPP_TRADING')) " & _
        '    "UNION ALL " & _
        '    "SELECT a1.acctgoid,a1.acctgcode,a1.acctgdesc,ISNULL(crd.amtopen,0) AS amtopen," & _
        '    "'debet'=CASE WHEN gd.gldbcr='D' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END," & _
        '    "'credit'=CASE WHEN gd.gldbcr='C' AND MONTH(gm.gldate)=" & iMonth & " AND YEAR(gm.gldate)=" & iYear & " THEN gd.glamt ELSE 0 END," & _
        '    "0.00 AS amtbalance,0 AS seq FROM QL_mstacctg a1 " & _
        '    "LEFT JOIN QL_crdgl crd ON crd.cmpcode=a1.cmpcode AND crd.acctgoid=a1.acctgoid AND crd.glgroup='MONTHLY' AND crd.periodacctg='" & GetDateToPeriodAcctg3(tempDate) & "' " & _
        '    "LEFT JOIN QL_trngldtl gd ON a1.cmpcode=gd.cmpcode AND a1.acctgoid=gd.acctgoid " & _
        '    "LEFT JOIN QL_trnglmst gm ON gm.cmpcode=gd.cmpcode AND gm.glmstoid=gd.glmstoid AND gm.glflag='POST' AND gm.glmstoid>0 " & _
        '    "WHERE a1.cmpcode='" & CompnyCode & "' and a1.acctgarea='" & AcctgArea & "' AND LEFT(a1.acctgcode,2) NOT IN (SELECT i.interfacevalue FROM QL_mstinterface i WHERE i.cmpcode=a1.cmpcode " & _
        '    "AND i.interfacevar IN ('VAR_SI_HPP_MANUF','VAR_SI_HPP_SLITTER','VAR_SI_HPP_TRADING')) AND a1.acctgoid NOT IN " & _
        '    "(SELECT DISTINCT a.acctggrp2 FROM QL_mstacctg a WHERE a.acctggrp2 IS NOT NULL AND a.cmpcode=a1.cmpcode and a.acctgarea='" & AcctgArea & "') " & _
        '    ") AS crd GROUP BY acctgoid,acctgcode,acctgdesc,amtopen,seq ORDER BY acctgcode"
        Return cKoneksi.ambiltabel(sSql, "QL_mstacctg")
    End Function
#End Region

#Region "Procedures"
    Private Sub InitDDL()
        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth As New ListItem(MonthName(C1), C1)
            DDLMonth.Items.Add(liMonth)
        Next

        ' TAHUN
        For C1 As Integer = 2014 To GetServerTime.Year + 1
            Dim liYear As New ListItem(C1, C1)
            DDLYear.Items.Add(liYear)
        Next

        sSql = "select genCODE,gendesc from QL_mstgen where gengroup='CABANG' and cmpcode='" & CompnyCode & "'"
        If cKoneksi.ambilscalar(sSql) > 0 Then
            FillDDL(DDLCabang, sSql)
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssclass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Sub GetLastAndNextMonthlyClosing()
        sSql = "SELECT ISNULL(closingperiod,'') FROM QL_acctgclosinghistory WHERE cmpcode='" & CompnyCode & "' and branch_code='" & DDLCabang.SelectedValue & "' AND historyclosingoid=(SELECT MAX(historyclosingoid) FROM QL_acctgclosinghistory WHERE cmpcode='" & CompnyCode & "' " & "AND closinggroup='MONTHLY' AND closingstatus='CLOSE' And branch_code='" & DDLCabang.SelectedValue & "') "
        Dim history As String = cKoneksi.ambilscalar(sSql)

        If history <> "" Then
            lastcloseyear.Text = Left(history, 4)
            lastclosemonth.Text = Integer.Parse(Right(history, 2))
            lblLastClosing.Text = MonthName(lastclosemonth.Text) & " " & lastcloseyear.Text
            If lastclosemonth.Text = 12 Then
                DDLMonth.SelectedValue = 1 : DDLYear.SelectedValue = ToDouble(lastcloseyear.Text) + 1
                nextclosemonth.Text = 1 : nextcloseyear.Text = ToDouble(lastcloseyear.Text) + 1
            Else
                DDLMonth.SelectedValue = ToDouble(lastclosemonth.Text) + 1 : DDLYear.SelectedValue = ToDouble(lastcloseyear.Text)
                nextclosemonth.Text = ToDouble(lastclosemonth.Text) + 1 : nextcloseyear.Text = ToDouble(lastcloseyear.Text)
            End If
        Else
            lastcloseyear.Text = "" : lastclosemonth.Text = "" : lblLastClosing.Text = "-"
            'DDLMonth.SelectedValue = Month(GetServerTime) : DDLYear.SelectedValue = Year(GetServerTime)
            nextclosemonth.Text = "" : nextcloseyear.Text = ""
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        ' THROW TO NOT AUTHORIZED PAGE IF NO ROLE EXIST
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Response.Redirect("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("trnmonthlyclosing.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Monthly Closing" ' CHANGE FORM NAME HERE
        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are u sure want to DELETE this data?');")
         btnSave.Attributes.Add("OnClick", "javascript:return confirm('Are u sure want to SAVE this data?');")

        If Not Page.IsPostBack Then
            InitDDL()
            GetLastAndNextMonthlyClosing()
            upduser.Text = Session("UserID")
            updtime.Text = GetServerTime()
            ' IF HAVE TABLE DETAIL, RE-BIND ON POSTBACK
            Dim dtlTbl As DataTable : dtlTbl = Session("TblClose")
            gvDtl.DataSource = dtlTbl : gvDtl.DataBind()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnmonthlyclosing.aspx?awal=true")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnMonthlyClosing.aspx?awal=true")
        End If
    End Sub

    Protected Sub DDLCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GetLastAndNextMonthlyClosing()
    End Sub
#End Region

    Protected Sub imbFindLoan_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim msg As String = ""

        Dim dtlTbl As DataTable : dtlTbl = BindClosingData(DDLMonth.SelectedValue, DDLYear.SelectedValue)
        ' Re-sequence & Calculate Closing Data
        For C1 As Integer = 0 To dtlTbl.Rows.Count - 1
            Dim ed As DataRow = dtlTbl.Rows(C1)
            ed.BeginEdit()
            ed("seq") = C1 + 1
            ed.EndEdit()
        Next
        Session("TblClose") = dtlTbl

        ' ======= PENDAPATAN : Pendapatan Penjualan dan Pendapatan Non Operasional
        Dim VAR_SALES As String = GetVarInterface("VAR_CLOSING_SALES", CompnyCode)
        If VAR_SALES = "0" Or VAR_SALES Is Nothing Then
            If msg = "" Then
                msg = "U must setting intervace for : <BR>" & "- VAR_CLOSING_SALES"
            Else
                msg &= "<BR>- VAR_CLOSING_SALES"
            End If
        End If
        Dim VAR_SALES_OTHER As String = GetVarInterface("VAR_CLOSING_REVENUE", CompnyCode)
        If VAR_SALES = "0" Or VAR_SALES Is Nothing Then
            If msg = "" Then
                msg = "U must setting intervace for : <BR>" & "- VAR_CLOSING_REVENUE"
            Else
                msg &= "<BR>- VAR_CLOSING_REVENUE"
            End If
        End If

        ' ======= BIAYA : Biaya Pokok, Biaya Operasional, Biaya Non Operasional
        Dim VAR_HPP As String = GetVarInterface("VAR_CLOSING_COGS", CompnyCode)
        If VAR_HPP = "0" Then
            If msg = "" Then
                msg = "U must setting intervace for : <BR>" & "- VAR_CLOSING_COGS"
            Else
                msg &= "<BR>- VAR_CLOSING_COGS"
            End If
        End If
        Dim VAR_CLOSING_COST As String = GetVarInterface("VAR_CLOSING_SLS_ADM", CompnyCode)
        If VAR_CLOSING_COST = "0" Then
            If msg = "" Then
                msg = "U must setting intervace for : <BR>" & "- VAR_CLOSING_SLS_ADM"
            Else
                msg &= "<BR>- VAR_CLOSING_SLS_ADM"
            End If
        End If


        ' ======= LABARUGI : Laba Rugi Ditahan, Tahun Berjalan, dan Periode Bulan Berjalan
        Dim iLabaBerjalanOid As Integer = 0
        Dim iLabaTahunBerjalanOid As Integer = 0
        Dim iLabaDitahan As Integer = 0

        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_LR_MONTH' "
        Dim varLabaBerjalan As String = CStr(cKoneksi.ambilscalar(sSql))
        If varLabaBerjalan <> "" Then
            sSql = "SELECT a1.acctgoid FROM QL_mstacctg a1 WHERE a1.cmpcode='" & CompnyCode & "' AND a1.acctgcode LIKE '" & varLabaBerjalan & "%' AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode)  ORDER BY a1.acctgcode"
            iLabaBerjalanOid = cKoneksi.ambilscalar(sSql)
        End If

        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_LR_YEAR' "
        Dim varLabaTahunBerjalan As String = CStr(cKoneksi.ambilscalar(sSql))
        If varLabaTahunBerjalan <> "" Then
            sSql = "SELECT a1.acctgoid FROM QL_mstacctg a1 WHERE a1.cmpcode='" & CompnyCode & "' AND a1.acctgcode LIKE '" & varLabaTahunBerjalan & "%' AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode) ORDER BY a1.acctgcode"
            iLabaTahunBerjalanOid = cKoneksi.ambilscalar(sSql)
        End If

        sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_LR_ONHAND'"
        Dim varLabaDitahan As String = CStr(cKoneksi.ambilscalar(sSql))
        If varLabaDitahan <> "" Then
            sSql = "SELECT a1.acctgoid FROM QL_mstacctg a1 WHERE a1.cmpcode='" & CompnyCode & "' AND a1.acctgcode LIKE '" & varLabaDitahan & "%' AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode)  ORDER BY a1.acctgcode"
            iLabaDitahan = cKoneksi.ambilscalar(sSql)
        End If

        Dim objTable As DataTable : objTable = Session("TblClose")

        ' L/R Tahun Berjalan dan L/R Bulan Berjalan tahun sebelumnya, ditambahkan ke L/R Ditahan 
        ' pada Closing Januari tahun setelahnya.
        If DDLMonth.SelectedValue = 1 Then
            Dim sPeriodBefore As String = GetDateToPeriodAcctg3(New Date(DDLYear.SelectedValue - 1, 12, 1))
            sSql = "SELECT ISNULL(SUM(ISNULL(c.amtbalance,0)),0) FROM QL_crdgl c WHERE c.cmpcode='" & CompnyCode & "' AND c.periodacctg='" & sPeriodBefore & "' AND c.acctgoid IN (" & iLabaTahunBerjalanOid & "," & iLabaBerjalanOid & ") and c.branch_code='" & DDLCabang.SelectedValue & "'"
            Dim dTotalLabaRugiTahunLalu As Double = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
            If iLabaDitahan <> 0 Then
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "acctgoid=" & iLabaDitahan
                If dv.Count > 0 Then
                    Dim ed As DataRowView = dv(0)
                    ed.BeginEdit()
                    If dTotalLabaRugiTahunLalu >= 0 Then
                        ed("amtdebet") = dTotalLabaRugiTahunLalu
                    Else
                        ed("amtcredit") = (dTotalLabaRugiTahunLalu * -1)
                    End If

                    ed("amtbalance") = ToDouble(ed("amtopen")) + (ToDouble(ed("amtdebet")) - ToDouble(ed("amtcredit")))
                    ed.EndEdit()
                End If
                dv.RowFilter = ""
            End If
        Else
            ' Selain Januari, L/R Bulan sebelumnya diakumulasikan ke L/R Tahun Berjalan
            Dim sPeriodBefore As String = GetDateToPeriodAcctg3(New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue - 1, 1))
            sSql = "SELECT ISNULL(SUM(ISNULL(c.amtbalance,0)),0) FROM QL_crdgl c WHERE c.cmpcode='" & CompnyCode & "' AND c.periodacctg='" & sPeriodBefore & "' AND c.acctgoid IN (" & iLabaBerjalanOid & ") and c.branch_code='" & DDLCabang.SelectedValue & "'"
            Dim dTotalLabaRugiBulanLalu As Double = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
            If iLabaTahunBerjalanOid <> 0 Then
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "acctgoid=" & iLabaTahunBerjalanOid
                If dv.Count > 0 Then
                    Dim ed As DataRowView = dv(0)
                    ed.BeginEdit()
                    If dTotalLabaRugiBulanLalu >= 0 Then
                        ed("amtdebet") = dTotalLabaRugiBulanLalu
                    Else
                        ed("amtcredit") = (dTotalLabaRugiBulanLalu * -1)
                    End If

                    ed("amtbalance") = ToDouble(ed("amtopen")) + (ToDouble(ed("amtdebet")) - ToDouble(ed("amtcredit")))
                    ed.EndEdit()
                End If
                dv.RowFilter = ""
            End If
        End If
        objTable.AcceptChanges()

        ' CALCULATE LABA (RUGI) PERIODE BULAN BERJALAN
        Dim totPendapatan As Double = 0
        Dim totBiaya As Double = 0

        For C2 As Integer = 0 To objTable.Rows.Count - 1
            If (Left(objTable.Rows(C2)("acctgcode").ToString, VAR_SALES.Length) = VAR_SALES) Or _
            (Left(objTable.Rows(C2)("acctgcode").ToString, VAR_SALES_OTHER.Length) = VAR_SALES_OTHER) Then
                ' Summarize pendapatan
                totPendapatan += (ToDouble(objTable.Rows(C2)("amtdebet").ToString) - ToDouble(objTable.Rows(C2)("amtcredit").ToString))
            ElseIf (Left(objTable.Rows(C2)("acctgcode").ToString, VAR_HPP.Length) = VAR_HPP) Or _
            (Left(objTable.Rows(C2)("acctgcode").ToString, VAR_CLOSING_COST.Length) = VAR_CLOSING_COST) Then
                ' Summarize biaya
                totBiaya += (ToDouble(objTable.Rows(C2)("amtdebet").ToString) - ToDouble(objTable.Rows(C2)("amtcredit").ToString))
            Else
                ' DO NOTHING
            End If
        Next

        ' Membalik Pendapatan 
        totPendapatan *= -1

        ' Update Laba Rugi Berjalan ke QL_crdgl (pd Session)
        If iLabaBerjalanOid <> 0 Then
            Dim dv As DataView = objTable.DefaultView
            dv.RowFilter = "acctgoid=" & iLabaBerjalanOid
            If dv.Count > 0 Then
                Dim ed As DataRowView = dv(0)
                ed.BeginEdit()
                If totPendapatan > totBiaya Then
                    ed("amtcredit") = totPendapatan - totBiaya
                Else
                    ed("amtdebet") = totBiaya - totPendapatan
                End If
                '---- edit HJ , laba rugi bulan berjalan selalu refresh -----
                'ed("amtbalance") = ToDouble(ed("amtopen")) + (ToDouble(ed("amtdebet")) - ToDouble(ed("amtcredit"))) 
                ed("amtbalance") = (ToDouble(ed("amtdebet")) - ToDouble(ed("amtcredit")))
                ed.EndEdit()
            End If
            dv.RowFilter = ""
        End If
        objTable.AcceptChanges()

        gvDtl.DataSource = objTable : gvDtl.DataBind()
        Session("TblClose") = objTable
        ' ======================= SAMPE KENE DISEK =======================
        ' ===================CHECK UN-POST Transaction====================
        sSql = "DECLARE @bln INT " & _
            "DECLARE @thn INT " & _
            "DECLARE @cmpcode VARCHAR(10) " & _
            "DECLARE @awal DATETIME " & _
            "DECLARE @akhir DATETIME " & _
            "SET @bln=" & DDLMonth.SelectedValue & " " & _
            "SET @thn=" & DDLYear.SelectedValue & " " & _
            "SET @cmpcode='" & CompnyCode & "' " & _
            "SET @awal='" & New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, 1) & "' " & _
            "SET @akhir='" & New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)) & "' " & _
            "SELECT COUNT(-1) AS jml, 'A/P INITIAL BALANCE' AS tipe,1 AS seq " & _
            "FROM QL_trnbelimst WHERE cmpcode=@cmpcode AND trnbelimstoid<0 AND trnbelistatus NOT IN ('POST') " & _
            "AND MONTH(trnbelidate)=@bln AND YEAR(trnbelidate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/R INITIAL BALANCE' AS tipe,2 AS seq " & _
            "FROM QL_trnjualmst WHERE cmpcode=@cmpcode AND trnjualmstoid<0 AND trnjualstatus NOT IN ('POST') " & _
            "AND MONTH(trnjualdate)=@bln AND YEAR(trnjualdate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'COA INITIAL BALANCE' AS tipe,3 AS seq " & _
            "FROM QL_trnglmst a INNER JOIN QL_trngldtl b ON a.glmstoid=b.glmstoid INNER JOIN ql_mstacctg c on c.acctgoid=b.acctgoid " & _
            "WHERE a.cmpcode=@cmpcode AND a.glmstoid<0 AND a.glflag NOT IN ('POST') " & _
            "AND MONTH(a.gldate)=@bln AND YEAR(a.gldate)=@thn and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'PURCHASE INVOICE' AS tipe,5 AS seq " & _
            "FROM QL_trnbelimst WHERE cmpcode=@cmpcode AND trnbelimstoid > 0 AND trnbelistatus not in ('Approved','Rejected','POST') " & _
            "AND MONTH(trnbelidate)=@bln AND YEAR(trnbelidate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'SALES INVOICE' AS tipe,6 AS seq " & _
            "FROM QL_trnjualmst WHERE cmpcode=@cmpcode AND trnjualmstoid > 0 " & _
            "AND upper(trnjualstatus) not in ('APPROVED','REJECTED','POST') AND MONTH(trnjualdate)=@bln AND YEAR(trnjualdate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'PURCHASE RETURN' AS tipe,9 AS seq " & _
            "FROM QL_trnbeliReturmst WHERE cmpcode=@cmpcode AND trnbelistatus NOT IN ('Approved','Rejected','POST') " & _
            "AND MONTH(trnbelidate)=@bln AND YEAR(trnbelidate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'SALES RETURN' AS tipe,10 AS seq " & _
            "FROM QL_trnjualreturmst WHERE cmpcode=@cmpcode AND trnjualstatus NOT IN ('Approved','Rejected','POST') " & _
            "AND MONTH(trnjualdate)=@bln AND YEAR(trnjualdate)=@thn and branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/P PAYMENT' AS tipe,11 AS seq " & _
            "FROM QL_trncashbankmst a inner join ql_mstacctg b on a.cashbankacctgoid=b.acctgoid " & _
            "WHERE a.cmpcode=@cmpcode AND cashbankstatus<>'POST' " & _
            "AND MONTH(cashbankdate)=@bln AND YEAR(cashbankdate)=@thn AND cashbankgroup='AP' and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'A/R RECEIPT' AS tipe,12 AS seq " & _
            "FROM QL_trncashbankmst a inner join ql_mstacctg b on a.cashbankacctgoid=b.acctgoid  " & _
            "WHERE a.cmpcode=@cmpcode AND cashbankstatus<>'POST' " & _
            "AND MONTH(cashbankdate)=@bln AND YEAR(cashbankdate)=@thn AND cashbankgroup='AR' and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK EXPENSE' AS tipe,13 AS seq " & _
            "FROM QL_trncashbankmst a inner join ql_mstacctg b on a.cashbankacctgoid=b.acctgoid " & _
            "WHERE a.cmpcode=@cmpcode AND cashbankstatus<>'POST' " & _
            "AND MONTH(cashbankdate)=@bln AND YEAR(cashbankdate)=@thn AND cashbankgroup='COST' and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK RECEIPT' AS tipe,14 AS seq " & _
            "FROM QL_trncashbankmst a inner join ql_mstacctg b on a.cashbankacctgoid=b.acctgoid " & _
            "WHERE a.cmpcode=@cmpcode AND cashbankstatus<>'POST' " & _
            "AND MONTH(cashbankdate)=@bln AND YEAR(cashbankdate)=@thn AND cashbankgroup='RECEIPT' and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml, 'CASH/BANK MUTATION' AS tipe,15 AS seq " & _
            "FROM QL_trncashbankmst a inner join ql_mstacctg b on a.cashbankacctgoid=b.acctgoid " & _
            "WHERE a.cmpcode=@cmpcode AND cashbankstatus<>'POST' " & _
            "AND MONTH(cashbankdate)=@bln AND YEAR(cashbankdate)=@thn AND cashbankgroup='MUTATION' and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            "SELECT COUNT(-1) AS jml,'JURNAL VOUCHER' AS tipe,16 AS seq " & _
            "FROM QL_trnglmst a " & _
            "WHERE a.cmpcode=@cmpcode AND a.glflag<>'POST' " & _
            "AND MONTH(gldate)=@bln AND YEAR(gldate)=@thn and a.branch_code='" & DDLCabang.SelectedValue & "'" & _
            "UNION ALL " & _
            " SELECT COUNT(-1) AS jml,'FIX ASSETS' AS tipe,17 AS seq FROM QL_trnfixdtl a  WHERE   a.fixflag<>'POST' AND abs(LEFT(a.fixperiod,2))=abs(@bln) AND abs(right(a.fixperiod,4))=abs(@thn) and a.branch_code='" & DDLCabang.SelectedValue & "' "
        Dim dtCekPost As DataTable = cKoneksi2.ambiltabel(sSql, "CekPost")
        ' showMessage(sSql, 3)
        Dim iCounter As Integer = ToDouble(dtCekPost.Compute("SUM(jml)", "").ToString)
        lblCounter.Text = iCounter : pnlNeedPost.Visible = (iCounter > 0)
        btnSave.Visible = Not (iCounter > 0)
        gvPostList.DataSource = dtCekPost : gvPostList.DataBind()
    End Sub

    Protected Sub btnSave_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If lastcloseyear.Text <> "" And lastclosemonth.Text <> "" Then
            If (DDLMonth.SelectedValue <> CInt(nextclosemonth.Text)) Or (DDLYear.SelectedValue <> CInt(nextcloseyear.Text)) Then
                sMsg &= "- Tutup periode selanjutnya adalah " & MonthName(nextclosemonth.Text) & " " & nextcloseyear.Text & " !!<BR>"
            End If
            If GetServerTime() < New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)) Then
                sMsg &= "- Tutup bulan dapat di lakukan minimal di akhir bulan tutup bulan !!<BR>"
            End If
        Else
            If GetServerTime() < New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue)) Then
                sMsg &= "- Tutup bulan dapat di lakukan minimal di akhir bulan tutup bulan !!<BR>"
            End If
        End If

        If Session("TblClose") Is Nothing Then
            sMsg &= "- Tidak ada data detail tutup bulan !!<BR>"
        Else
            Dim dtlTbl As DataTable : dtlTbl = Session("TblClose")
            If dtlTbl.Rows.Count < 1 Then
                sMsg &= "- Tidak ada tutup bulan data detail !!<BR>"
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If

        ' ======= Generate Oid
        Session("vIDCH") = GenerateID("QL_acctgclosinghistory", CompnyCode)
        Session("vIDCRD") = GenerateID("QL_crdgl", CompnyCode)

        ' ======= PENDAPATAN : Pendapatan Penjualan dan Pendapatan Non Operasional
        Dim varPdptJual As String = GetVarInterface("VAR_CLOSING_SALES", CompnyCode)
        Dim varPdptJasa As String = GetVarInterface("VAR_CLOSING_REVENUE", CompnyCode)

        ' ======= BIAYA : Biaya Pokok, Biaya Operasional, Biaya Non Operasional
        Dim varHpp As String = GetVarInterface("VAR_CLOSING_COGS", CompnyCode)
        Dim varCostUmum As String = GetVarInterface("VAR_CLOSING_SLS_ADM", CompnyCode)


        ' ======= GET ID utk Laba Rugi Periode Bulan Berjalan
        Dim iLabaBerjalanOid As Integer = 0
        Dim varLabaBerjalan As String = GetVarInterface("VAR_LR_ONHAND", CompnyCode)
        If varLabaBerjalan <> "" Then
            sSql = "SELECT a1.acctgoid FROM QL_mstacctg a1 WHERE a1.cmpcode='" & CompnyCode & "' AND a1.acctgcode LIKE '" & varLabaBerjalan & "%' AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode) ORDER BY a1.acctgcode"
            iLabaBerjalanOid = cKoneksi.ambilscalar(sSql)
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        objCmd.Transaction = objTrans

        Try
            If varLabaBerjalan <> "" Then
                sSql = "SELECT a1.acctgoid FROM QL_mstacctg a1 WHERE a1.cmpcode='" & CompnyCode & "' AND a1.acctgflag='A' AND a1.acctgcode LIKE '" & varLabaBerjalan & "%' AND a1.acctgoid not in (select distinct a.acctggrp3 from QL_mstacctg a where a.acctggrp3 is not null and a.cmpcode=a1.cmpcode)  ORDER BY a1.acctgcode"
                objCmd.CommandText = sSql : iLabaBerjalanOid = objCmd.ExecuteScalar
            End If

            Dim first, last, nextfirst As Date
            first = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, 1)
            last = New Date(DDLYear.SelectedValue, DDLMonth.SelectedValue, Date.DaysInMonth(DDLYear.SelectedValue, DDLMonth.SelectedValue))
            Dim nextmonth, nextyear As Integer
            If DDLMonth.SelectedValue = 12 Then
                nextmonth = 1 : nextyear = DDLYear.SelectedValue + 1
            Else
                nextmonth = DDLMonth.SelectedValue + 1 : nextyear = DDLYear.SelectedValue
            End If
            nextfirst = New Date(nextyear, nextmonth, 1)

            ' Untuk laba (rugi) berjalan
            Dim totPendapatan As Double = 0
            Dim totBiaya As Double = 0

            Dim objTable As DataTable : objTable = Session("TblClose")

            ' QL_crdgl
            For C1 As Integer = 0 To objTable.Rows.Count - 1
                ' SUMMARIZE CLOSING MONTH
                sSql = "SELECT COUNT(-1) FROM QL_crdgl WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & objTable.Rows(C1)("acctgoid").ToString & " AND periodacctg='" & GetDateToPeriodAcctg3(last) & "' AND glgroup='MONTHLY' and branch_code='" & DDLCabang.SelectedValue & "'"
                objCmd.CommandText = sSql
                If objCmd.ExecuteScalar > 0 Then
                    ' UPDATE SALDO BULAN CLOSING
                    sSql = "UPDATE QL_crdgl SET amtdebit=" & ToDouble(objTable.Rows(C1)("amtdebet").ToString) & ",amtcredit=" & ToDouble(objTable.Rows(C1)("amtcredit").ToString) & ",amtbalance=" & ToDouble(objTable.Rows(C1)("amtbalance").ToString) & ",postdate=CURRENT_TIMESTAMP,upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP,glflag='CLOSE',gldesc='MONTHLY CLOSING BALANCE - " & GetDateToPeriodAcctg3(last) & "' WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & objTable.Rows(C1)("acctgoid").ToString & " AND periodacctg='" & GetDateToPeriodAcctg3(last) & "' and branch_code='" & DDLCabang.SelectedValue & "'"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Else
                    ' INSERT SALDO BULAN CLOSING
                    sSql = "INSERT INTO QL_crdgl (cmpcode,crdgloid,crdgldate,periodacctg,acctgoid,amtopen,amtdebit,amtcredit,amtbalance,postdate,glgroup,glflag,gldesc,createuser,upduser,updtime,branch_code) VALUES " & _
                        "('" & CompnyCode & "'," & Session("vIDCRD") & ",'" & first & "','" & GetDateToPeriodAcctg3(last) & "'," & objTable.Rows(C1)("acctgoid").ToString & "," & "" & ToDouble(objTable.Rows(C1)("amtopen").ToString) & "," & ToDouble(objTable.Rows(C1)("amtdebet").ToString) & "," & ToDouble(objTable.Rows(C1)("amtcredit").ToString) & "," & ToDouble(objTable.Rows(C1)("amtbalance").ToString) & ",CURRENT_TIMESTAMP," & "'MONTHLY','CLOSE','MONTHLY CLOSING BALANCE - " & GetDateToPeriodAcctg3(last) & "','" & Session("UserID") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DDLCabang.SelectedValue & "')"
                    objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                    Session("vIDCRD") += 1
                End If
                'Set AmtOpen NextMonth=0 bila periode closing adalah akhir tahun dan merupakan akun Pendapatan / Biaya
                Dim amtBalance As Double = ToDouble(objTable.Rows(C1)("amtbalance").ToString)
                If (Left(objTable.Rows(C1)("acctgcode").ToString, varPdptJual.Length) = varPdptJual) Or _
                (Left(objTable.Rows(C1)("acctgcode").ToString, varPdptJasa.Length) = varPdptJasa) Or _
                 (Left(objTable.Rows(C1)("acctgcode").ToString, varCostUmum.Length) = varCostUmum) Or _
                (Left(objTable.Rows(C1)("acctgcode").ToString, varHpp.Length) = varHpp) Or _
                 (Left(objTable.Rows(C1)("acctgcode").ToString, varLabaBerjalan.Length) = varLabaBerjalan) Then
                    '(Left(objTable.Rows(C1)("acctgcode").ToString, varCostOps.Length) = varCostOps) Or _
                    If DDLMonth.SelectedValue = 12 Then
                        amtBalance = 0
                    End If
                End If

                ' INSERT SALDO AWAL BULAN SELANJUTNYA
                sSql = "INSERT INTO QL_crdgl (cmpcode,crdgloid,crdgldate,periodacctg,acctgoid,amtopen,amtdebit,amtcredit,amtbalance,postdate,glgroup,glflag,gldesc,createuser,upduser,updtime,branch_code) VALUES " & _
                    "('" & CompnyCode & "'," & Session("vIDCRD") & ",'" & nextfirst & "','" & GetDateToPeriodAcctg3(nextfirst) & "'," & objTable.Rows(C1)("acctgoid").ToString & "," & "" & amtBalance & ",0,0," & amtBalance & ",CURRENT_TIMESTAMP,'MONTHLY','OPEN','MONTHLY CLOSING BALANCE - " & GetDateToPeriodAcctg3(nextfirst) & "','" & Session("UserID") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & DDLCabang.SelectedValue & "')"
                objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
                Session("vIDCRD") += 1
            Next

            sSql = "update QL_mstoid set lastoid=" & Session("vIDCRD") - 1 & " where cmpcode='" & CompnyCode & "' AND tablename = 'QL_crdgl'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            '--Simpan Data History Closing Bulanan'
            '-------------------------------------'
            sSql = "INSERT INTO QL_acctgclosinghistory (cmpcode,historyclosingoid,historyclosingdate,historyclosinguserid,historyclosingtime,historyopendate,historyopenuserid,historyopentime,closingperiod,closingstatus,closingnote,closinggroup,branch_code) VALUES " & _
                "('" & CompnyCode & "'," & Session("vIDCH") & ",'" & last & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & first & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & GetDateToPeriodAcctg3(last) & "','CLOSE','CLOSING BULANAN - " & GetDateToPeriodAcctg3(last) & "','MONTHLY','" & DDLCabang.SelectedValue & "')"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            sSql = "update QL_mstoid set lastoid=" & Session("vIDCH") & " where cmpcode='" & CompnyCode & "' AND tablename = 'QL_acctgclosinghistory'"
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()

            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBoxWarn")
            Exit Sub
        End Try
        showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub
End Class
