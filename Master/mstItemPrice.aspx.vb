Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class mstItemPrice
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private oRegex As Regex
    Dim oMatches As MatchCollection
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Private rptSupp As New ReportDocument
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Dim salesitem As String = ""
#End Region

#Region "Function"
    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Function getHargaBeli(ByVal itemOid As String) As String()
        Dim retVal(3) As String
        sSql = _
        "select m.trnbelidate 'tgl',m.trnbelimstoid, d.trnbelidtloid, m.trnbelino 'nonota',g.gendesc 'satasli', " & _
        "((d.amtbelinetto-( " & _
        "( " & _
        "(d.amtbelinetto/(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid)*100) " & _
        "/100*(m.amtdischdr+m.amtdischdr2+m.amtdischdr3)) " & _
        "))+(m.trnamttax*( " & _
        "d.amtbelinetto/ " & _
        "	(select sum(amtbelinetto) from ql_trnbelidtl where trnbelimstoid=d.trnbelimstoid) " & _
        "*100)/100))/d.trnbelidtlqty 'harga', " & _
        "i.satuan1,i.satuan2,i.satuan3,i.konversi1_2,i.konversi2_3,d.trnbelidtlunitoid, " & _
        "g2.gendesc 'SatBesar',g3.gendesc 'SatSedang',g4.gendesc 'SatKecil',s.suppname,i.itemdesc   " & _
        "from ql_trnbelimst m  " & _
        "inner join ql_trnbelidtl d on d.trnbelimstoid=m.trnbelimstoid  " & _
        "inner join ql_mstgen g on d.trnbelidtlunitoid=g.genoid  " & _
        "inner join ql_mstitem i on d.itemoid=i.itemoid  " & _
        "inner join ql_mstgen g2 on i.satuan1=g2.genoid  " & _
        "inner join ql_mstgen g3 on i.satuan2=g3.genoid  " & _
        "inner join ql_mstgen g4 on i.satuan3=g4.genoid  " & _
        "inner join ql_mstsupp s on m.trnsuppoid=s.suppoid  " & _
        "where i.itemoid='" & itemOid & "' and m.trnbelidate between '" & DateTime.Now.AddYears(-1).ToShortDateString & "' and '" & DateTime.Now.ToShortDateString & "'" & _
        "order by m.trnbelidate, m.trnbelimstoid, d.trnbelidtloid "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        If dtTmp.Rows.Count <= 0 Then
            ' belum ada pmebelian
            retVal(0) = "0"
            retVal(1) = "0"
            retVal(2) = "0"
        Else
            Dim targetRow As Integer = dtTmp.Rows.Count - 1

            ' ambil harga sat besar
            Dim hrgSatBesar As Double = 0
            If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi1_2")
            ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatBesar = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3") * dtTmp.Rows(targetRow)("konversi1_2")
            End If

            Dim hrgSatSedang As Double = 0
            ' cek dulu apakah sat 1,2,3 sama semua
            If dtTmp.Rows(targetRow)("satuan1") = dtTmp.Rows(targetRow)("satuan2") Then
                hrgSatSedang = 0
            Else
                ' ambil harga sat sedang
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatSedang = dtTmp.Rows(targetRow)("harga") * dtTmp.Rows(targetRow)("konversi2_3")
                End If
            End If

            Dim hrgSatKecil As Double = 0
            If dtTmp.Rows(targetRow)("satuan2") = dtTmp.Rows(targetRow)("satuan3") Then
                hrgSatKecil = 0
            Else
                ' ambil harga sat kecil
                If dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan1") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi1_2") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan2") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga") / dtTmp.Rows(targetRow)("konversi2_3")
                ElseIf dtTmp.Rows(targetRow)("trnbelidtlunitoid") = dtTmp.Rows(targetRow)("satuan3") Then
                    hrgSatKecil = dtTmp.Rows(targetRow)("harga")
                End If
            End If

            retVal(0) = hrgSatBesar
            retVal(1) = hrgSatSedang
            retVal(2) = hrgSatKecil
        End If

        Return retVal
    End Function

    Function isHargaUpdated(ByVal itemOid As String) As String
        Dim retVal As String = "PNA"
        Dim hargaHitung(3) As String
        hargaHitung = getHargaBeli(itemOid)

        ' cek apakah sebelumnya ada pembelian
        If hargaHitung(0) = "0" And hargaHitung(1) = "0" And hargaHitung(2) = "0" Then
            retVal = "PNA"
            GoTo selesai
        End If

        Dim hargaSekarang(3) As String
        sSql = "select itemoid, itemdesc, itempriceunit1, itempriceunit2, itempriceunit3, " & _
        "satuan1, satuan2, satuan3 " & _
        "from ql_mstitem where itemoid='" & itemOid & "' "
        Dim dtTmp As DataTable = CreateDataTableFromSQL(sSql)

        Dim satuan1 As String = dtTmp.Rows(0)("satuan1").ToString()
        Dim satuan2 As String = dtTmp.Rows(0)("satuan2").ToString()
        Dim satuan3 As String = dtTmp.Rows(0)("satuan3").ToString()

        'If dtTmp.Rows.Count > 0 Then
        hargaSekarang(0) = dtTmp.Rows(0)("itempriceunit1")
        hargaSekarang(1) = dtTmp.Rows(0)("itempriceunit2")
        hargaSekarang(2) = dtTmp.Rows(0)("itempriceunit3")


        'For i As Integer = 0 To 2
        Dim dHargaSkg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaSekarang(0), "0"))
        Dim dHargaHtg1 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(0)) = False, hargaHitung(0), "0"))
        If dHargaSkg1 <= dHargaHtg1 Then
            retVal = "FALSE"
            GoTo selesai
        Else
            retVal = "TRUE"
        End If
        'Next i

        If satuan1 = satuan2 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaSekarang(1), "0"))
            Dim dHargaHtg2 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(1)) = False, hargaHitung(1), "0"))
            If dHargaSkg2 <= dHargaHtg2 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If


        If satuan2 = satuan3 Then
            retVal = "TRUE"
        Else
            Dim dHargaSkg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaSekarang(2), "0"))
            Dim dHargaHtg3 As Double = Double.Parse(IIf(IsDBNull(hargaSekarang(2)) = False, hargaHitung(2), "0"))
            If dHargaSkg3 <= dHargaHtg3 Then
                retVal = "FALSE"
                GoTo selesai
            Else
                retVal = "TRUE"
            End If
        End If
selesai:
        Return retVal
    End Function

    Function getTanda(ByVal itemOid As String) As String
        Dim retVal As String = ""
        'If itemOid = "1043" Then
        '    Dim aaa As String = "lalalala"
        'End If
        Dim hasil As String = isHargaUpdated(itemOid)
        If hasil = "TRUE" Then
            retVal = "V"
        ElseIf hasil = "FALSE" Then
            retVal = "X"
        ElseIf hasil = "PNA" Then
            retVal = "O"
        End If

        Return retVal
    End Function

    Function tandaHarga(ByVal tanda As String) As String
        Dim retVal As String = ""
        If tanda = "X" Then
            retVal = "<span style='color:#990000;font-weight:bolder;'>X</span>"
        ElseIf tanda = "V" Then
            retVal = "<span style='color:#009966;font-weight:bolder;'>V</span>"
        Else
            retVal = "<span style='color:#ff6600;font-weight:bolder;'>O</span>"
        End If
        Return retVal
    End Function

    Function formatToIDR(ByVal money As String) As String
        'Dim ci As New System.Globalization.CultureInfo("id-ID")
        'Return money.ToString("c", ci)
        If money.IndexOf(".") > 0 Then
            Dim depan As String = money.Substring(0, money.IndexOf("."))
            depan = depan.Replace(",", ".")
            Dim belakang As String = money.Substring(money.IndexOf(".") + 1, money.Length - (depan.Length + 1))
            Return depan & "," & belakang
        Else
            Return money.Replace(",", ".")
        End If
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Public Function GetSessionCheckPriceNota() As Boolean
        If Session("CheckPriceNota") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetSessionCheckPriceNormal() As Boolean
        If Session("CheckPriceNormal") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetSessionCheckPriceminim() As Boolean
        If Session("CheckPriceMinim") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetSessionCheckPriceKhusus() As Boolean
        If Session("CheckPriceMinim") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetTextBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As String
        Dim sReturn As String = ""
        Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                End If
            Next
        End If
        Return sReturn
    End Function
#End Region

#Region "Procedure"
    Private Sub UpdateCheckedMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvItem.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    'TargetQty
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("PriceNota") = ToDouble(GetTextValue(row.Cells(3).Controls))
                            dv(0)("PriceNormal") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("minimprice") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("pricekhusus") = ToDouble(GetTextValue(row.Cells(6).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMat") = dt
        End If

        If Not Session("TblLMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvItem.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                            dv(0)("PriceNota") = ToDouble(GetTextValue(row.Cells(3).Controls))
                            dv(0)("PriceNormal") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("minimprice") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("pricekhusus") = ToDouble(GetTextValue(row.Cells(6).Controls))
                        Else
                            dv(0)("checkvalue") = "False"
                            dv(0)("PriceNota") = ToDouble(GetTextValue(row.Cells(3).Controls))
                            dv(0)("PriceNormal") = ToDouble(GetTextValue(row.Cells(4).Controls))
                            dv(0)("minimprice") = ToDouble(GetTextValue(row.Cells(5).Controls))
                            dv(0)("pricekhusus") = ToDouble(GetTextValue(row.Cells(6).Controls))
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMatView") = dt
        End If

        If Session("TblMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMat")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvItem.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(9).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    'TargetQty
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkprice") = "True"
                        Else
                            dv(0)("checkprice") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMat") = dt
        End If

        If Not Session("TblLMatView") Is Nothing Then
            Dim dt As DataTable = Session("TblMatView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvItem.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(9).Controls
                    Dim cbCheck As Boolean = False : Dim sOid As String = ""

                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "itemoid=" & sOid
                    If dv.Count > 0 Then
                        If cbCheck = True Then
                            dv(0)("checkprice") = "True"
                        Else
                            dv(0)("checkprice") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMatView") = dt
        End If
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "item_" & ID & ".jpg"
        Return fname
    End Function

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Try
            Dim sWhere As String = ""
            FilterText.Text = Session("FilterText")
            FilterDDL.SelectedIndex = Session("FilterDDL")

            If Session("FilterDDL") Is Nothing = False Then
                If Session("FilterText").ToString.Trim <> "" Then
                    If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                        sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "'"
                    Else
                        sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%'"
                    End If
                End If
            Else

                If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                    sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "'"
                Else
                    sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%'"
                End If

                If Session("UserId") <> "admin" Then
                    If salesitem = "" Then salesitem = 0
                    sWhere &= " and i.personoid=" & salesitem & ""
                End If

            End If

            If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
                SelectTop.Text = 100
            End If
            sSql = "SELECT top " & SelectTop.Text & " 1 as nomor,i.cmpcode, i.itemoid, i.itemcode, itemdesc,merk,g.gendesc as itemgroupoid, itemsafetystockunit1, i.createuser, i.Upduser, i.Updtime, itemflag, personname as personoid,ISNULL([itempriceunit1],0.00) bottompricegrosir,pricelist PriceNormal,ISNULL(bottompricegrosir,0.00) minimprice,ISNULL(itempriceunit2,0.00) pricekhusus,discunit1,discunit2,discunit3,itemsafetystockunit1 minimqtyCase ,case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang,i.pricesilver,i.priceplatinum,i.pricegold FROM QL_mstitem i inner join QL_mstgen g on i.itemgroupoid = g.genoid inner join QL_mstperson pr on i.personoid = pr.personoid Inner join ql_mstgen g1 on g1.genoid=i.satuan1 AND g1.gengroup='ITEMUNIT' " & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " i.cmpcode LIKE '%" & CompnyCode & "%' and itemflag = '" & ddlStatusView.SelectedValue & "' ORDER BY itemdesc"
            Dim dtNya As DataTable = cKoneksi.ambiltabel(sSql, "ItemNya")
            GVmstgen.DataSource = dtNya : GVmstgen.DataBind()
            Session("dtPublic") = GVmstgen.DataSource
        Catch ex As Exception
            showMessage(ex.ToString & "<br>" & sSql, "ERROR")
            Exit Sub
        End Try
    End Sub

    Public Sub BindDataCabang()
        ' this function will load ALL DATA at first time
        ' so call this function wisely.
        sSql = "select itm.itemoid,itm.itemdesc,itm.pricelist,cbg.branch_code,gen.gendesc branch_name, " & _
               "cbg.biayaexpedisi,(itm.pricelist+cbg.biayaexpedisi) pricelist_cbg,cbg.expedisi_type from ql_mstitem itm " & _
               "inner join ql_mstitem_branch cbg on itm.itemoid = cbg.itemoid " & _
               "inner join ql_mstgen gen on gen.gengroup='cabang' and gen.gencode = cbg.branch_code " & _
               "where itm.itemoid = '" & Session("oid") & "'"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")
        GV_Branch.DataSource = objDs.Tables("data")
        GV_Branch.DataBind()
        Session("dtPublic") = GV_Branch.DataSource
    End Sub

    Public Sub findAData()
        ' search data
        Dim dtTmp As DataTable = Session("dtPublic")
        Dim dv As DataView = dtTmp.DefaultView
        If FilterDDL.SelectedValue <> "infoharga" Then
            dv.RowFilter = FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        Else
            dv.RowFilter = "infoharga='" & ddlStatusHarga.SelectedValue & "' "
        End If

        Dim aa As Integer = 2
        GVmstgen.DataSource = dv.ToTable()
        GVmstgen.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As String)
        Try
            sSql = "SELECT mi.[cmpcode],mi.personoid,mi.[itemoid],mi.[itemcode],mi.[itemdesc],mi.[itemgroupoid],mi.[itemsubgroupoid],mi.itemtype,mi.merk,ISNULL(bottompricegrosir,0.00) minimprice,iSNULL(mi.pricelist,0.00) priceNormal,ISNULL([itempriceunit1],0.00) PriceNota,ISNULL(itempriceunit2,0.00) pricekhusus,mi.[satuan2],mi.payoid,mi.[itemsafetystockunit1],mi.[createuser],mi.[upduser],mi.[updtime], mi.[itemflag], mi.[acctgoid],mi.personoid, keterangan ,pricelist,discountunit1 discunit1,discountunit2 discunit2,discountunit3 discunit3,stockflag,statusitem,stockflag,mi.satuan1,itemsafetystockunit1,has_SN,case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang,pricesilver,priceplatinum,pricegold FROM [QL_mstitem] mi Where mi.cmpcode='" & CompnyCode & "' and mi.itemoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader 

            While xreader.Read
                itemoid.Text = Integer.Parse(xreader("itemoid"))
                itemdesc.Text = xreader("itemdesc")
                PriceList.Text = ToMaskEdit(xreader("priceNormal"), 3)
                PriceNota.Text = ToMaskEdit(xreader("PriceNota"), 3)
                bottompriceGrosir.Text = ToMaskEdit(xreader("minimprice"), 3)
                PriceKhusus.Text = ToMaskEdit(xreader("pricekhusus"), 3)
                Disc1.Text = ToMaskEdit(xreader("discunit1"), 3)
                Disc2.Text = ToMaskEdit(xreader("discunit2"), 3)
                Disc3.Text = ToMaskEdit(xreader("discunit3"), 3)
                MinimStock.Text = ToMaskEdit(xreader("itemsafetystockunit1"), 3)
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss")
                TypeBarang.Text = xreader("JenisBarang").ToString
                PriceSilver.Text = ToMaskEdit(xreader("pricesilver"), 3)
                PricePlatinum.Text = ToMaskEdit(xreader("priceplatinum"), 3)
                PriceGold.Text = ToMaskEdit(xreader("pricegold"), 3)
            End While
            xreader.Close()
            conn.Close()
            If Session("UserId") <> "admin" Then
                'spgOid.Enabled = False
            End If

        Catch ex As Exception
            showMessage(ex.ToString & sSql, "ERROR")
            Exit Sub
        End Try

    End Sub

    Public Sub GenerateGenID()
        itemoid.Text = GenerateID("QL_mstitem", CompnyCode)
    End Sub

    Sub clearItem()
        itemoid.Text = "" : itemdesc.Text = ""
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        Dim sWhere As String = "" : Dim sFilter As String = ""
        Try
            If printType.Text = "PDF" Then
                rptReport.Load(Server.MapPath("~/report/rptMstItem.rpt"))
                sWhere = " Where QL_mstitem.cmpcode LIKE '%" & CompnyCode & "%' and itemflag = '" & ddlStatusView.SelectedValue & "'"
                If FilterText.Text.Trim <> "" Then
                    If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                        sWhere &= " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
                    Else
                        sWhere &= " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
                    End If
                    sFilter &= " " & FilterDDL.SelectedItem.Text & " = " & FilterText.Text & ", "
                End If


                If sFilter = "" Then
                    sFilter = " ALL "
                End If
                rptReport.SetParameterValue("sFilter", " Filter By : " & sFilter)
            Else
                rptReport.Load(Server.MapPath("~/report/rptMstItemExl.rpt"))

                sWhere = " Where i.cmpcode ='" & CompnyCode & "' AND i.itemflag = '" & ddlStatusView.SelectedValue & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & TcharNoTrim(FilterText.Text) & "%'"
                rptReport.SetParameterValue("SelectTop", SelectTop.Text)
            End If

            rptReport.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            If printType.Text = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rptReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MASTER_ITEM")
                rptReport.Close() : rptReport.Dispose()
            ElseIf printType.Text = "EXCEL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rptReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "MASTER_ITEM")
                rptReport.Close() : rptReport.Dispose()
            End If

        Catch ex As Exception
            lblError.Text = ex.ToString & "<br />"
            Exit Sub
        End Try
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim sWhere As String = "" : Response.Clear()
        Response.AddHeader("content-disposition", "inline;filename=mstitemdtl.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        sSql = "SELECT top " & SelectTop.Text & " itemcode Kode, itemdesc [Nama Barang],ISNULL([itempriceunit1],0.00) Nota,pricelist [Price Normal],ISNULL(bottompricegrosir,0.00) [Minim Price],itemsafetystockunit1 [Minim Qty],ISNULL(itempriceunit2,0.00) [Price Khusus],discunit1 [Diskon 1],discunit2 [Diskon 2],discunit3 [Diskon 3],i.pricesilver [Price Silver],i.priceplatinum [Price Platinum],i.pricegold [Price Gold],case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang, i.createuser [Create User],i.crttime [Create Time], i.Upduser [Update User], i.Updtime [Update Time] FROM QL_mstitem i inner join QL_mstgen on i.itemgroupoid = QL_mstgen.genoid inner join QL_mstperson p on i.personoid = p.personoid Inner join ql_mstgen g1 on g1.genoid=i.satuan1 AND g1.gengroup='ITEMUNIT' WHERE i.cmpcode='" & CompnyCode & "' And itemflag = '" & ddlStatusView.SelectedValue & "' AND " & FilterDDL.SelectedValue & " Like '%" & TcharNoTrim(FilterText.Text) & "%' ORDER BY itemdesc"

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()
    End Sub

    Public Sub BindEditPrice()
        Try
            Dim sPeriod As String = ""
            sPeriod = GetPeriodAcctg(GetServerTime())
            sSql = "Select  * from (Select 'False' Checkvalue,i.itemcode, i.itemdesc,i.itemoid, i.merk,i.stockflag, Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' end JenisNya,ISNULL([itempriceunit1],0.00) pricenota,pricelist PriceNormal,ISNULL(bottompricegrosir,0.00) minimprice,ISNULL(itempriceunit2,0.00) pricekhusus, ISNULL((SELECT SUM(qtyin)-SUM(qtyOut) FROM QL_conmtr c where c.refoid = i.itemoid and mtrlocoid NOT IN (select genoid from QL_mstgen where genother5 IN ('TITIPAN','RUSAK') and genoid IN (-9)) AND periodacctg = '" & sPeriod & "'),0) AS laststock, 'False' AS checkprice From ql_mstitem i Where itemflag='AKTIF') dt"
            Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
            Session("TblMat") = dt
            Session("TblMatView") = Session("TblMat")
            gvItem.DataSource = Session("TblMatView")
            gvItem.DataBind() : gvItem.Visible = True
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try

    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("branch_id") = branch_id
            Session("branch") = branch
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_code") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If

            Response.Redirect("mstItemPrice.aspx?page=" & GVmstgen.PageIndex)
        End If

        Page.Title = CompnyName & " - Data Pricelist Item"
        Session("oid") = Request.QueryString("oid")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            ' i_u.Text = "Update"
        Else
            'i_u.Text = "New"
            TabPanel3.Enabled = False
        End If

        If Not IsPostBack Then
            GVmstgen.PageIndex = Session("page") 
            BindData(CompnyCode)
            BindDataCabang() 
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update" 
                'cb_sn.Text = "Type SN tidak dapat diupdate 2 kali"
            Else
                GenerateGenID() 
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                GVmstgen.PageIndex = ToDouble(Request.QueryString("page"))
                GVmstgen.DataBind()
                lblupd.Text = "Create"
            End If

        End If
    End Sub
    'Buat paging
    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
        Session("page") = GVmstgen.PageIndex
        'findAData()
    End Sub

    Protected Sub GV_Branch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Branch.PageIndexChanging
        GV_Branch.PageIndex = e.NewPageIndex
        BindDataCabang()
        Session("page") = GV_Branch.PageIndex
        TabContainer1.ActiveTabIndex = 2
        'findAData()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        ' FilterDDL.SelectedIndex = 0
        Session("FilterText") = "" : Session("FilterDDL") = 0
        Session("FilterText2") = "" : Session("FilterDDL2") = 0
        Session("FilterText3") = "" : Session("FilterDDL3") = 0 
        BindData(CompnyCode)
        'findAData()
    End Sub

    Private Sub BindItem()
        Dim sQl As String = ""
        If FilterItemDDL.SelectedValue <> "ALL" Then
            sQl = " AND " & FilterItemDDL.SelectedValue & " LIKE '%" & Tchar(itemdesc.Text) & "%'"
        End If
        sSql = "Select itemoid,itemcode,itemdesc,statusitem jenis,itemflag,HPP,p.PERSONNAME,i.Merk,case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang,ISNULL(bottompricegrosir,0.00) minimprice,iSNULL(pricelist,0.00) priceNormal,ISNULL([itempriceunit1],0.00) PriceNota,ISNULL(itempriceunit2,0.00) pricekhusus,itemsafetystockunit1,discunit1,discunit2,discunit3, i.upduser, i.updtime, 'Last Update' AS upd From ql_mstitem i LEft Join QL_MSTPERSON p ON p.PERSONOID=i.personoid Where itemflag='AKTIF' " & sQl & ""
        FillGV(gvMaterial, sSql, "QL_mstITEM")
        gvMaterial.Visible = True
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        GVmstgen.PageIndex = 0
        BindData(CompnyCode)
        'findAData()
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "SUMMARY"
        imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoItem") = ""
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub itempriceunit3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'itempriceunit3.Text = NewMaskEdit(ToDouble(itempriceunit3.Text))
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 3)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 3)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 3)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 3)
        End If
    End Sub

    Protected Sub btnprintlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "DETAIL" : imbPrintPDF.Visible = False
        orderNoForReport.Text = sender.ToolTip : orderIDForReport.Text = sender.CommandArgument()
        Session("NoItem") = sender.ToolTip : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = "" : printType.Text = "PDF"
        Try
            PrintReport("", "Barang_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstItemPrice.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()

        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstItemPrice.aspx?awal=true")
        End If
    End Sub

    Protected Sub FilterDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub bottompriceGrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bottompriceGrosir.Text = ToMaskEdit(ToDouble(bottompriceGrosir.Text), 3)
        If bottompriceGrosir.Text = "" Then
            bottompriceGrosir.Text = "0"
        End If
    End Sub

    Protected Sub bottompriceRetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'bottompriceRetail.Text = NewMaskEdit(ToDouble(bottompriceRetail.Text))
        'If bottompriceRetail.Text = "" Then
        '    bottompriceRetail.Text = "0"
        'End If
    End Sub

    Protected Sub PriceList_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PriceList.Text = ToMaskEdit(ToDouble(PriceList.Text), 4)
        If PriceList.Text = "" Then
            PriceList.Text = "0.0000"
        End If 
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ImageButton2.Click
        Response.Redirect("~\Master\mstItemPrice.aspx?awal=true")
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = "" : printType.Text = "EXCEL"
        Try
            PrintReport("", "Barang_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptSupp.Close() : rptSupp.Dispose()
    End Sub

    Protected Sub PriceNota_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PriceNota.TextChanged
        PriceNota.Text = ToMaskEdit(ToDouble(PriceNota.Text), 3)
        PriceList.Text = ToMaskEdit(ToDouble(PriceNota.Text), 3)
        If PriceNota.Text = "" Then
            PriceNota.Text = "0.0000"
        End If
    End Sub

    Protected Sub PriceKhusus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PriceKhusus.TextChanged
        PriceKhusus.Text = ToMaskEdit(ToDouble(PriceKhusus.Text), 3)
        If PriceList.Text = "" Then
            PriceKhusus.Text = "0.00"
        End If
    End Sub

    Protected Sub MinimStock_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MinimStock.TextChanged
        MinimStock.Text = ToMaskEdit(ToDouble(MinimStock.Text), 3)
        If MinimStock.Text = "" Then
            MinimStock.Text = "0"
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "UPDATE QL_MSTITEM SET bottompricegrosir=" & ToDouble(bottompriceGrosir.Text) & ", [pricelist]=" & ToDouble(PriceList.Text) & ", discountunit1=" & ToDouble(Disc1.Text) & ", discountunit2=" & ToDouble(Disc2.Text) & ", discountunit3=" & ToDouble(Disc3.Text) & ", [itempriceunit2]=" & ToDouble(PriceKhusus.Text) & ", [qty3_to2]=" & ToDouble(1) & ", [qty3_to1]=" & ToDouble(1) & ", [itemsafetystockunit1]=" & ToDouble(MinimStock.Text) & ", [upduser]='" & Session("UserID") & "', [updtime]=current_timestamp, itempriceunit1=" & ToDouble(PriceNota.Text) & ",pricesilver=" & ToDouble(PriceSilver.Text) & ",priceplatinum=" & ToDouble(PricePlatinum.Text) & ",pricegold=" & ToDouble(PriceGold.Text) & " WHERE cmpcode = '" & CompnyCode & "' and itemoid=" & itemoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                sSql = "UPDATE QL_MSTITEM SET bottompricegrosir=" & ToDouble(bottompriceGrosir.Text) & ", [pricelist]=" & ToDouble(PriceList.Text) & ", discountunit1=" & ToDouble(Disc1.Text) & ", discountunit2=" & ToDouble(Disc2.Text) & ", discountunit3=" & ToDouble(Disc3.Text) & ", [itempriceunit2]=" & ToDouble(PriceKhusus.Text) & ", [qty3_to2]=" & ToDouble(1) & ", [qty3_to1]=" & ToDouble(1) & ", [itemsafetystockunit1]=" & ToDouble(MinimStock.Text) & ", [upduser]='" & Session("UserID") & "', [updtime]=current_timestamp, itempriceunit1=" & ToDouble(PriceNota.Text) & ",pricesilver=" & ToDouble(PriceSilver.Text) & ",priceplatinum=" & ToDouble(PricePlatinum.Text) & ",pricegold=" & ToDouble(PriceGold.Text) & " WHERE cmpcode = '" & CompnyCode & "' And itemoid=" & itemoid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try 
        Response.Redirect("~\Master\mstItemPrice.aspx")
    End Sub

    Protected Sub Disc1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc1.TextChanged
        Disc1.Text = ToMaskEdit(ToDouble(Disc1.Text), 4)
        If Disc1.Text = "" Then
            Disc1.Text = "0"
        End If
        PriceList.Text = ToMaskEdit(ToDouble(PriceNota.Text) - ToDouble(Disc1.Text), 4)
    End Sub

    Protected Sub Disc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Disc2.TextChanged
        If ToDouble(Disc2.Text) <> 0 Then
            bottompriceGrosir.Text = ToMaskEdit(ToDouble(PriceList.Text - Disc2.Text), 4)
        Else
            bottompriceGrosir.Text = 0
        End If
    End Sub

    Protected Sub btnSearchmat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchmat.Click
        BindItem()
    End Sub

    Protected Sub btnClearB_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearB.Click
        itemdesc.Text = "" : itemoid.Text = ""
        gvMaterial.Visible = False
    End Sub

    Protected Sub gvMaterial_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaterial.PageIndexChanging
        gvMaterial.PageIndex = e.NewPageIndex
        BindItem()
    End Sub

    Protected Sub gvMaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaterial.SelectedIndexChanged
        itemoid.Text = Integer.Parse(gvMaterial.SelectedDataKey.Item("itemoid"))
        itemdesc.Text = gvMaterial.SelectedDataKey.Item("itemdesc").ToString
        TypeBarang.Text = gvMaterial.SelectedDataKey.Item("JenisBarang").ToString
        PriceNota.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("priceNota"), 4)
        PriceList.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("priceNormal"), 4)
        bottompriceGrosir.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("minimprice"), 4)
        PriceKhusus.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("pricekhusus"), 4)
        MinimStock.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("itemsafetystockunit1"), 4)
        Disc1.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("discunit1"), 4)
        Disc2.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("discunit2"), 4)
        Disc3.Text = ToMaskEdit(gvMaterial.SelectedDataKey.Item("discunit3"), 4)
        lblupd.Text = gvMaterial.SelectedDataKey.Item("upd").ToString
        Upduser.Text = gvMaterial.SelectedDataKey.Item("upduser").ToString
        Updtime.Text = gvMaterial.SelectedDataKey.Item("updtime").ToString
        gvMaterial.Visible = False
    End Sub

    Protected Sub BtnViewItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewItem.Click
        BindEditPrice()
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        dd_stock.SelectedIndex = -1
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
 
    Protected Sub BtnFindItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnFindItem.Click
        If Session("TblMat") Is Nothing Then
            BindEditPrice()
        ElseIf Not Session("TblMat") Is Nothing Then
            Dim dv As DataView = Session("TblMat").DefaultView
            Dim sSqli As String = ""

            If dd_stock.SelectedValue <> "ALL" Then
                sSqli &= "AND stockflag='" & dd_stock.SelectedValue & "'"
            End If

            dv.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' " & sSqli & ""
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                Session("TblMat") = Session("TblMatView")
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() : dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind() 
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub BtnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewAll.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        BindEditPrice()
        'If gvItem.Visible = True Then
        '    UpdateCheckedMat()
        '    Dim dBar As DataTable = Session("TblMatView")
        '    Dim MisV As DataView = dBar.DefaultView
        '    MisV.RowFilter = ""
        '    If MisV.Count > 0 Then
        '        Session("DataItem") = MisV.ToTable
        '        gvItem.DataSource = Session("DataItem")
        '        gvItem.DataBind() : MisV.RowFilter = ""
        '    End If
        'Else
        '    BindEditPrice()
        'End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Dim sSqli As String = ""
        UpdateCheckedMat()
        gvItem.PageIndex = e.NewPageIndex
        Dim dBar As DataTable = Session("TblMatView")
        Dim MisV As DataView = dBar.DefaultView
        If dd_stock.SelectedValue <> "ALL" Then
            sSqli &= "AND stockflag='" & dd_stock.SelectedValue & "'"
        End If
        MisV.RowFilter = FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' " & sSqli & ""
        If MisV.Count > 0 Then
            Session("DataItem") = MisV.ToTable
            gvItem.DataSource = Session("DataItem")
            gvItem.DataBind() : MisV.RowFilter = ""
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub BtnSelectALL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSelectALL.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else
            showMessage("- Data tidak ditemukan", "MSC - WARNING")
        End If
    End Sub

    Protected Sub BtnSaveItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveItem.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMatView")
        Dim dtv As DataView = dt.DefaultView
        dtv.RowFilter = "Checkvalue='true'"

        If dtv.Count < 0 Then
            showMessage("- Data tidak ditemukan", "MSC - WARNING")
            Exit Sub
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
            For c2 As Int32 = 0 To dtv.Count - 1
                If dtv.Item(c2)("checkprice") = True Then
                    sSql = "UPDATE QL_MSTITEM SET itempriceunit1=" & ToDouble(dtv.Item(c2)("pricenota")) & ", [pricelist]=" & ToDouble(dtv.Item(c2)("pricenota")) & ", [itempriceunit2]=" & ToDouble(dtv.Item(c2)("pricenota")) & ", [upduser]='" & Session("UserID") & "',bottompricegrosir=" & ToDouble(dtv.Item(c2)("pricenota")) & ", [updtime]=current_timestamp WHERE cmpcode = 'MSC' And itemoid=" & Integer.Parse(dtv.Item(c2)("itemoid"))
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_MSTITEM SET itempriceunit1=" & ToDouble(dtv.Item(c2)("pricenota")) & ", [pricelist]=" & ToDouble(dtv.Item(c2)("PriceNormal")) & ", [itempriceunit2]=" & ToDouble(dtv.Item(c2)("pricekhusus")) & ", [upduser]='" & Session("UserID") & "',bottompricegrosir=" & ToDouble(dtv.Item(c2)("minimprice")) & ", [updtime]=current_timestamp WHERE cmpcode = 'MSC' And itemoid=" & Integer.Parse(dtv.Item(c2)("itemoid"))
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Next
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try
        Response.Redirect("~\Master\mstItemPrice.aspx")
    End Sub

    Protected Sub BtnSelectNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False" 
                    dtTbl.Rows(C1)("checkvalue") = "False" 
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvItem.DataSource = Session("TblMatView")
                gvItem.DataBind()
            End If
            mpeListMat.Show()
        Else 
            showMessage("- Maaf data tidak ketemu..!!", "MSC - INFORMATION")
        End If
    End Sub

    Protected Sub CbPriceNota_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtab As DataTable = Session("TblMatView")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("PriceNota") = ToMaskEdit(ToDouble(GetTextBoxValue(0, 3)), 3)
                Next
                dtab.AcceptChanges()
            End If
            gvItem.DataSource = dtab
            gvItem.DataBind()
            Session("TblMatView") = dtab
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            If Session("CheckPriceNota") <> True Then
                Session("CheckPriceNota") = False
            Else
                Session("CheckPriceNota") = False
            End If
        End If
    End Sub

    Protected Sub CbPriceNormal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtab As DataTable = Session("TblMatView")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("PriceNormal") = ToMaskEdit(ToDouble(GetTextBoxValue(0, 4)), 3)
                Next
                dtab.AcceptChanges()
            End If
            gvItem.DataSource = dtab
            gvItem.DataBind()
            Session("TblMatView") = dtab
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            If Session("CheckPriceNormal") <> True Then
                Session("CheckPriceNormal") = False
            Else
                Session("CheckPriceNormal") = False
            End If
        End If
    End Sub

    Protected Sub CbPriceMinim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtab As DataTable = Session("TblMatView")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("minimprice") = ToMaskEdit(ToDouble(GetTextBoxValue(0, 5)), 3)
                Next
                dtab.AcceptChanges()
            End If
            gvItem.DataSource = dtab
            gvItem.DataBind()
            Session("TblMatView") = dtab
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            If Session("CheckPriceMinim") <> True Then
                Session("CheckPriceMinim") = False
            Else
                Session("CheckPriceMinim") = False
            End If
        End If
    End Sub

    Protected Sub CbPriceKhusus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtab As DataTable = Session("TblMatView")
            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("pricekhusus") = ToMaskEdit(ToDouble(GetTextBoxValue(0, 6)), 3)
                Next
                dtab.AcceptChanges()
            End If
            gvItem.DataSource = dtab
            gvItem.DataBind()
            Session("TblMatView") = dtab
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            If Session("CheckPriceKhusus") <> True Then
                Session("CheckPriceKhusus") = False
            Else
                Session("CheckPriceKhusus") = False
            End If
        End If
    End Sub

    Protected Sub PriceGold_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PriceGold.TextChanged
        PriceGold.Text = ToMaskEdit(ToDouble(PriceGold.Text), 3)
        If PriceGold.Text = "" Then
            PriceGold.Text = "0.00"
        End If
    End Sub

    Protected Sub PricePlatinum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PricePlatinum.TextChanged
        PricePlatinum.Text = ToMaskEdit(ToDouble(PricePlatinum.Text), 3)
        If PricePlatinum.Text = "" Then
            PricePlatinum.Text = "0.00"
        End If
    End Sub

    Protected Sub PriceSilver_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PriceSilver.TextChanged
        PriceSilver.Text = ToMaskEdit(ToDouble(PriceSilver.Text), 3)
        If PriceSilver.Text = "" Then
            PriceSilver.Text = "0.00"
        End If
    End Sub
#End Region
End Class