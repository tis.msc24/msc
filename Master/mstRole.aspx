﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="mstRole.aspx.vb" Inherits="MstRole" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Role" CssClass="Title" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">List of Role :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 8%" class="Label" align=left>Filter </TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="rolename">Role Name</asp:ListItem>
<asp:ListItem Value="description">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvMst" runat="server" Width="100%" ForeColor="#333333" AllowSorting="True" AllowPaging="True" PageSize="8" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="roleoid" DataNavigateUrlFormatString="~/Master/mstRole.aspx?oid={0}" DataTextField="rolename" HeaderText="Role Name" SortExpression="rolename">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black" Width="25%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="roledesc" HeaderText="Description" SortExpression="roledesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="There is no data !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <asp:Label id="lblViewInfo" runat="server" Width="238px" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label> </TD></TR></TBODY></TABLE></asp:Panel> &nbsp; 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 11%" class="Label" align=left><asp:Label id="I_U" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w42"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=right></TD><TD class="Label" align=left colSpan=2><asp:Label id="roleoid" runat="server" __designer:wfdid="w43" Visible="False"></asp:Label> <asp:Label id="cmpcode" runat="server" __designer:wfdid="w44" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblNmRole" runat="server" Text="Role Name" __designer:wfdid="w45"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="rolename" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w46" MaxLength="20"></asp:TextBox> </TD></TR><TR><TD class="Label" align=left><asp:Label id="lblDeskripsi" runat="server" Text="Description" __designer:wfdid="w47"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="roledesc" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w48" MaxLength="50"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="FormType" runat="server" Width="74px" Text="Form Type" __designer:wfdid="w47"></asp:Label></TD><TD class="Label" align=right>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="TxtFormType" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w2" MaxLength="50"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchForm" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="AbsBottom" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseFrom" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom" __designer:wfdid="w4"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=right></TD><TD class="Label" align=left colSpan=2><asp:Panel id="Panel2" runat="server" Width="100%" Height="200px" CssClass="inpText" __designer:wfdid="w10" Visible="False" ScrollBars="Vertical"><asp:GridView id="gvRole" runat="server" Width="100%" Height="1px" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w18" UseAccessibleHeader="False" AllowPaging="True" PageSize="100" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="Pilih"><ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" __designer:wfdid="w2" ToolTip='<%# Eval("genoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="FormType" HeaderText="Form Type">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="FormName" HeaderText="Form Name">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="FormAddress" HeaderText="Form Url">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="genoid" HeaderText="Genoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:Label id="Label2" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="Role Detail :" __designer:wfdid="w49"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="imbTambah" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbRemove" runat="server" ImageUrl="~/Images/remove.png" ImageAlign="AbsMiddle" __designer:wfdid="w51" Visible="False"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 216px" class="Label" align=left colSpan=4><asp:Panel id="pnlDtl" runat="server" Width="100%" Height="200px" CssClass="inpText" __designer:wfdid="w52" ScrollBars="Vertical"><asp:GridView id="gvDtl" runat="server" Width="98%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" __designer:wfdid="w14">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="formtype" HeaderText="Form Type">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="formname" HeaderText="Form Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="formaddress" HeaderText="Form URL">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbDelData" runat="server"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No Role Detail !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD id="TD1" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:Label id="Label3" runat="server" Font-Size="8pt" Font-Bold="True" ForeColor="Black" Text="User Role :" __designer:wfdid="w54" Visible="False"></asp:Label></TD></TR><TR><TD id="TD3" class="Label" align=left colSpan=4 runat="server" Visible="false"><asp:Panel id="PnlUserRole" runat="server" Width="100%" Height="200px" CssClass="inpText" __designer:wfdid="w55" Visible="False" ScrollBars="Vertical"><asp:GridView id="gvUserRole" runat="server" Width="98%" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None" Visible="False" __designer:wfdid="w56">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="profoid" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="profname" HeaderText="User Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No User Role Detail !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w57"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w58"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w59"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w60"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w61" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w62" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w63" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPrint2" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w64" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132008" __designer:wfdid="w79" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132009">
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w81"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;</DIV><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:UpdatePanel id="updpnlListForm" runat="server" __designer:wfdid="w67"><ContentTemplate>
<asp:Panel id="pnlListForm" runat="server" CssClass="modalBox" __designer:wfdid="w68" Visible="False"><TABLE style="WIDTH: 400px"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblListForm" runat="server" Font-Size="Medium" Font-Bold="True" Text="FORM LIST" __designer:wfdid="w69"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" align=left colSpan=2></TD></TR><TR><TD id="TD5" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD4" align=left runat="server" Visible="false"><asp:DropDownList id="listformmodule" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w84" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form Type</TD><TD align=left><asp:DropDownList id="listformtype" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w14" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form Name</TD><TD align=left><asp:DropDownList id="listformname" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w15" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Form URL</TD><TD align=left><asp:TextBox id="listformurl" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w16" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD class="Label" align=left></TD><TD style="HEIGHT: 5px" align=left><asp:Label id="lblListFormError" runat="server" CssClass="Important" __designer:wfdid="w74"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" class="Label" align=left colSpan=2><asp:LinkButton id="LBAddRole" runat="server" Width="86px" CausesValidation="False" Font-Bold="True" __designer:wfdid="w17" Visible="False" Font-Underline="True">[ Add To List ]</asp:LinkButton>&nbsp;<asp:LinkButton id="LBCancelRole" runat="server" CausesValidation="False" Font-Bold="True" __designer:wfdid="w76" Font-Underline="True">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnhiddenListForm" runat="server" CausesValidation="False" __designer:wfdid="w77" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeListForm" runat="server" __designer:wfdid="w78" PopupControlID="pnlListForm" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListForm" Drag="True" TargetControlID="btnhiddenListForm"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint2"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Form Role :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

