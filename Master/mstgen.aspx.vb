
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class Mstgen
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")

#End Region

#Region "Function"

#End Region

#Region "Procedure"
    Private Sub checkDDL(ByVal genoid As Integer)
        Try
            sSql = "Select a.genoid, ISNULL((SELECT ISNULL(gendesc,'') FROM QL_mstgen WHERE gengroup = 'WAREHOUSE' AND genoid = a.genother1),'') +' - '+a.gendesc gendesc From QL_mstgen a WHERE a.gengroup = 'Location' AND genoid <> -10  "
            If genoid <> 0 Then
                sSql &= " AND a.genother2 = " & genoid
            End If
            sSql &= " Order by genoid ASC"
            FillDDL(genother1, sSql)
        Catch ex As Exception
            showMessage(sSql, 3)
            Exit Sub
        End Try 
    End Sub

    Private Sub InitddlCabang()
        sSql = "Select gencode, gendesc from QL_mstgen Where gengroup='Cabang' And cmpcode='" & CompnyCode & "'"
        FillDDL(genother3, sSql)
    End Sub

    Private Sub InitCoa()
        If DDLType.SelectedValue = "T" Or DDLType.SelectedValue = "V" Then
            FillDDLAcctg(genother2, "VAR_GUDANG", genother3.SelectedValue, "")
        ElseIf DDLType.SelectedValue = "I" Then
            FillDDLAcctg(genother2, "VAR_INVENTORY", genother3.SelectedValue, "")
        Else
            FillDDLAcctg(genother2, "VAR_GUDANG_ASSET", genother3.SelectedValue, "")
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        If Session("FilterDDL") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = "WHERE upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "
            End If
        Else
            sWhere = " WHERE upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") like '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "
        End If 

        sSql = "SELECT gm.cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,gm.upduser,gm.updtime,(Select gd.gendesc From QL_mstgen gd Where gd.genoid=gm.genother1 And gd.gengroup='LOCATION') Gudang,ac.acctgcode+' - '+ac.acctgdesc AkunCoa,(Select gd.gendesc From QL_mstgen gd Where gd.gencode=gm.genother3 And gd.gengroup='CABANG') Cabang FROM QL_mstgen gm INNER JOIN QL_mstacctg ac On gm.genother2=ac.acctgoid AND gm.gengroup='COA' " & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & "upper(gm.cmpcode) LIKE '%" & sCompCode.ToUpper & "%' And gm.gengroup not in ('EKSPEDISI') AND gm.gengroup ='COA' AND genother4 IN ('T','I','ASSET','V') ORDER BY gengroup,gencode"

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
        GVmstgen.PageIndex = 0
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As Integer)
        sSql = "SELECT top 1 cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,upduser,updtime,genother4,genother5 FROM QL_mstgen where cmpcode='" & CompnyCode & "' and genoid = " & vGenoid & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        btnDelete.Enabled = False
        If xreader.HasRows Then
            While xreader.Read
                GenOid.Text = xreader("genoid").ToString
                gencode.Text = Trim(xreader("gencode").ToString)
                gendesc.Text = Trim(xreader("gendesc").ToString)
                gengroup.SelectedValue = xreader("gengroup")
                DDLType.SelectedValue = xreader("genother4").ToString
                checkDDL(0)
                If DDLType.SelectedValue = "T" Then
                    FillDDLAcctg(genother2, "VAR_GUDANG", genother3.SelectedValue, "")
                Else
                    FillDDLAcctg(genother2, "VAR_INVENTORY", genother3.SelectedValue, "")
                End If
                genother1.SelectedValue = xreader("genother1").ToString
                Session("oIDcOA") = xreader("genother2")
                'genother2.SelectedValue = xreader("genother2").ToString
                genother3.SelectedValue = xreader("genother3").ToString
                Dim jumRow As Integer = gengroup.Items.Count 
                Upduser.Text = Trim(xreader.GetValue(8).ToString)
                Updtime.Text = Trim(xreader.GetValue(9).ToString)
                btnDelete.Visible = True : btnDelete.Enabled = True
            End While

        Else
            gencode.Text = "sddfsfddfs"
        End If
        xreader.Close()
        conn.Close()
        sSql = "SELECT ACCTGOID,ACCTGDESC FROM QL_MSTACCTG wHERE ACCTGOID=" & Session("oIDcOA") & ""
        FillDDL(genother2, sSql)

    End Sub

    Public Sub InitAllDDL() 
        sSql = "Select lastoid,tablename From QL_mstoid Where tablegroup='GEN' And tablename='COA'"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        gengroup.Items.Clear()

        xCmd.CommandText = sSql : xreader = xCmd.ExecuteReader
        While xreader.Read
            gengroup.Items.Add(Trim(xreader.GetValue(1).ToString.Trim))
            gengroup.Items.Item(gengroup.Items.Count - 1).Value = Trim(xreader.GetValue(1).ToString.Trim)

            FilterGroup.Items.Add(Trim(xreader.GetValue(1).ToString.Trim))
            FilterGroup.Items.Item(FilterGroup.Items.Count - 1).Value = Trim(xreader.GetValue(1).ToString.Trim)
        End While
        xreader.Close() : conn.Close()
    End Sub

    Public Sub GenerateGenID()
        GenOid.Text = GenerateID("ql_mstgen", CompnyCode)
    End Sub

    Private Sub clearItem()
        gencode.Text = "" : gendesc.Text = "" : gengroup.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            btnSearch.Visible = True
            btnList.Visible = True
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            If Session("branch_id") = "" Then
                Response.Redirect("~/Other/login.aspx")
            End If
            Response.Redirect("mstgen.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")

        Page.Title = CompnyName & " - Data General"

        If Not IsPostBack Then
            BindData(CompnyCode)
            InitAllDDL() : InitddlCabang() : checkDDL(0) : InitCoa()
            btnSearch.Visible = True : btnList.Visible = True
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update" : btnDelete.Visible = True
                TabContainer1.ActiveTabIndex = 1
                FillTextBox(Session("oid"))
            Else
                i_u.Text = "New" : GenerateGenID()
                btnDelete.Enabled = False : btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
                genother3_SelectedIndexChanged(Nothing, Nothing)
                'gencode.Enabled = True : gencode.CssClass = "inpText"
            End If
        End If
    End Sub

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session("FilterText") = "" : Session("FilterDDL") = 0
        chkGroup.Checked = False : FilterGroup.SelectedIndex = 0
        BindData(CompnyCode)
        GVmstgen.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        BindData("MSC")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If gencode.Text.Trim = "" Then
            sMsg &= "- Tolong Isi Code Dahulu !!<BR>"
        End If
        If gendesc.Text.Trim = "" Then
            sMsg &= "- Tolong Isi Description Dahulu !!<BR>"
        End If
        If gendesc.Text.Trim.Length > 70 Then
            sMsg &= "- Maximum 70 characters untuk Description !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck As String = ""

        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck &= " AND genoid <> " & genoid.Text
        End If
        sSqlCheck = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & ClassFunction.Tchar(gendesc.Text) & "'"
        If i_u.Text = "New" Then
            If cKon.ambilscalar(sSqlCheck) > 0 Then
                showMessage("Description sudah dipakai di General Group yang lain", 3)
                Exit Sub
            End If
        End If

        'cek code msgen yang kembar
        If i_u.Text = "New" Then
            Dim sSqlCheckcode As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gencode = '" & Tchar(gencode.Text) & "' AND genother4='" & DDLType.SelectedValue & "'"
            If Session("oid") = Nothing Or Session("oid") = "" Then
            Else : sSqlCheckcode &= " AND genoid <> " & GenOid.Text
            End If
            If cKon.ambilscalar(sSqlCheckcode) > 0 Then
                showMessage("Code sudah dipakai di General Group yang sama", 3)
                Exit Sub
            Else
                Dim sSqlCheckdesc As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & ClassFunction.Tchar(gendesc.Text) & "'"
                If Session("oid") = Nothing Or Session("oid") = "" Then
                Else : sSqlCheckdesc &= " AND genoid <> " & GenOid.Text
                End If
                If cKon.ambilscalar(sSqlCheckdesc) > 0 Then
                    showMessage("Description sudah dipakai di General Group yang sama", 3)
                    Exit Sub
                End If
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            'UNTUK MENGISI GENOTHER TEXT PADA SAAT DDL AKTIF
            If Session("oid") = Nothing Or Session("oid") = "" Then
                GenerateGenID()
                'If gengroup.SelectedValue = "ITEMUNIT" Then
                sSql = "INSERT into QL_mstgen (cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,upduser,updtime,genother4,genother5) " & _
                      "VALUES ('" & CompnyCode & "'," & GenOid.Text & ",'" & Tchar(gencode.Text) & "','" & Tchar(gendesc.Text) & "','" & Tchar(gengroup.Text.Trim) & "','" & Tchar(genother1.SelectedValue) & "','" & Tchar(genother2.SelectedValue) & "','" & Tchar(genother3.SelectedValue) & "','" & Upduser.Text & "',current_timestamp,'" & DDLType.SelectedValue & "','')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Update QL_mstoid set lastoid=" & GenOid.Text & " WHERE tablename like '%QL_mstgen%' And cmpcode Like '%" & CompnyCode & "%'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else 'update data

                sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', genDesc='" & Tchar(gendesc.Text) & "', gengroup='" & Tchar(gengroup.Text.Trim) & "',  genother1='" & Tchar(genother1.SelectedValue) & "', genother2='" & Tchar(genother2.SelectedValue) & "', genother3='" & Tchar(genother3.SelectedValue) & "',genother4='" & Tchar(DDLType.SelectedValue.Trim) & "',upduser='" & Upduser.Text & "', updtime=current_timestamp WHERE cmpcode like '%" & CompnyCode & "%' and genoid=" & GenOid.Text
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(sSql, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Master\mstgen.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If genoid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill General ID"))
            Exit Sub
        End If

        Dim sColomnName() As String = {"custcityoid", "custprovoid", "custcountryoid", "prefixcmp", "prefixcp1", "prefixcp2", "prefixcp3", "itservtypeoid", "reqitemtype", "areaoid", "reqitembrand", "reqperson"}
        Dim sTable() As String = {"QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstitemserv", "QL_trnrequest", "Ql_mstregiondtl"}

        If CheckDataExists(GenOid.Text, sColomnName, sTable) = True Then
            showMessage("Tidak dapat dihapus karena digunakan di tabel lain !!", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE QL_mstgen WHERE cmpcode = '" & CompnyCode & "' AND genoid=" & GenOid.Text
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            'Close koneksi
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try 
        Response.Redirect("~\Master\mstgen.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstgen.aspx?awal=true")
    End Sub

    Protected Sub gengroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        genother3.Enabled = False
        checkDDL(0)
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

#End Region

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        checkDDL(0)
    End Sub

    Protected Sub genother3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles genother3.SelectedIndexChanged
        sSql = "Select genoid from QL_mstgen Where gengroup='Cabang' And gencode='" & genother3.SelectedValue & "'"
        Dim OidCabang As String = GetScalar(sSql)
        InitCoa()
        checkDDL(OidCabang)
    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim gDesc, jumlahNol, c, c2, kodegen As String
        jumlahNol = "" : Dim d As Integer = 0
        gDesc = Left(gendesc.Text, 1).ToUpper

        c = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0)) FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' AND gengroup='" & gengroup.SelectedItem.Text & "'"))
        c2 = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0)) FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' AND gengroup='" & gengroup.SelectedItem.Text & "'"))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If

        kodegen = GenNumberString(gDesc, "", d, 5)
        gencode.Text = kodegen
    End Sub

    Protected Sub gendesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gendesc.TextChanged
        generateItemId("")
    End Sub
End Class