<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnAssignFakturPajak.aspx.vb" Inherits="Transaction_trnAssignFakturPajak"
title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table class="tabelhias" width="100%">
<tr>
<th class="header">
    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
        ForeColor="Maroon" Text=".: Assign No Faktur"></asp:Label></th>
</tr>
</table>

<div style="text-align:left;">
    <br />
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Font-Bold="True"
        Font-Size="9pt">
        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
<asp:UpdatePanel ID="updPanel1" runat="server">
    <contenttemplate>
<TABLE id="TABLE1" onclick="return TABLE1_onclick()" width="100%"><TBODY><TR><TD colSpan=2><ajaxToolkit:CalendarExtender id="ceAwal" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnTglAwal" TargetControlID="tglAwal">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceAkhir" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnTglAkhir" TargetControlID="tglAkhir">
                </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="tglAwal" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear">
                </ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="tglAkhir" Mask="99/99/9999" MaskType="Date" CultureName="id-ID" UserDateFormat="DayMonthYear">
                </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="WIDTH: 137px">Filter</TD><TD><asp:DropDownList id="ddlFilter" runat="server" Width="102px" CssClass="inpText" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Selected="True">Barcode</asp:ListItem>
<asp:ListItem Value="reqcode">Penerimaan No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="136px" CssClass="inpText" MaxLength="30"></asp:TextBox>&nbsp;&nbsp;<asp:Button id="btnFind" onclick="btnFind_Click" runat="server" Width="51px" CssClass="btn orange" Text="Find" UseSubmitBehavior="False"></asp:Button>&nbsp;<asp:Button id="btnViewAll" onclick="btnViewAll_Click" runat="server" CssClass="btn gray" Text="View All" UseSubmitBehavior="False"></asp:Button></TD></TR><TR><TD style="WIDTH: 137px">Periode</TD><TD><asp:TextBox id="tglAwal" runat="server" Width="78px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTglAwal" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> to&nbsp; <asp:TextBox id="tglAkhir" runat="server" Width="78px" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="btnTglAkhir" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:Label id="Label1" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR><TR><TD style="WIDTH: 137px"></TD><TD vAlign=bottom height=30>&nbsp; </TD></TR><TR><TD colSpan=2 height=20></TD></TR><TR><TD colSpan=2><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvClosedBPM" runat="server" Width="90%" Font-Size="X-Small" ForeColor="#333333" OnSelectedIndexChanged="gvClosedBPM_SelectedIndexChanged" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" OnPageIndexChanging="gvClosedBPM_PageIndexChanging">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="spmno" HeaderText="No Penerimaan.">
<HeaderStyle CssClass="gvhdr" Width="130px"></HeaderStyle>

<ItemStyle Width="130px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmdateclosed" HeaderText="Tanggal Closed">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="machine" HeaderText="Barcode">
<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmuserclosed" HeaderText="User Closed">
<HeaderStyle CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle Width="60px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmnoteclosed" HeaderText="Note Closed">
<HeaderStyle CssClass="gvhdr" Width="170px"></HeaderStyle>

<ItemStyle Width="170px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="spmstatus" HeaderText="Status">
<HeaderStyle CssClass="gvhdr" Width="60px"></HeaderStyle>

<ItemStyle Width="60px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:GridView id="gvMR2" runat="server" Width="150px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="spkno" HeaderText="SPK">
<HeaderStyle Font-Size="X-Small"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" Width="100px"></HeaderStyle>

<ItemStyle Width="100px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                            <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                        
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></TD></TR><TR><TD style="HEIGHT: 15px" colSpan=2></TD></TR></TBODY></TABLE>
</contenttemplate>
</asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong><span style="font-size: 9pt">
                List of Assign No Faktur :.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<TABLE><TBODY>
    <tr>
        <td>
            <ajaxToolkit:CalendarExtender id="ceTAwal" runat="server" Enabled="True" Format="dd/MM/yyyy" PopupButtonID="btnTglAwal2" TargetControlID="tglAwal2"></ajaxToolkit:CalendarExtender> 
    <ajaxToolkit:MaskedEditExtender ID="meeTAwal" runat="server" Mask="99/99/9999" MaskType="Date"
        TargetControlID="tglAwal2">
    </ajaxToolkit:MaskedEditExtender>
        </td>
        <td colspan="2">
            <ajaxToolkit:CalendarExtender id="ceTAkhir" runat="server" Enabled="True" Format="dd/MM/yyyy" PopupButtonID="btnTglAkhir2" TargetControlID="tglAkhir2"></ajaxToolkit:CalendarExtender>
    <ajaxToolkit:MaskedEditExtender ID="meeTAkhir" runat="server" Mask="99/99/9999" MaskType="Date"
        TargetControlID="tglAkhir2">
    </ajaxToolkit:MaskedEditExtender>
        </td>
    </tr>
    <TR><TD><asp:Label id="Label4" runat="server" Text="Periode"></asp:Label></TD><TD colSpan=2><asp:TextBox id="tglAwal2" runat="server" Width="78px" CssClass="inpText"></asp:TextBox>
    <asp:ImageButton id="btnTglAwal2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>
    to
    <asp:TextBox id="tglAkhir2" runat="server" Width="78px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnTglAkhir2" runat="server" ImageUrl="~/images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label></TD></TR>
    <tr>
        <td>
            Nomor
            <asp:DropDownList ID="DDLNoSI" runat="server" CssClass="inpText">
                <asp:ListItem Value="SI0%">SI0</asp:ListItem>
                <asp:ListItem Value="SI1%">SI1</asp:ListItem>
                <asp:ListItem Value="%">ALL SI</asp:ListItem>
            </asp:DropDownList></td>
        <td colspan="2">
            <asp:TextBox id="txtFilter2" runat="server" Width="136px" CssClass="inpText" MaxLength="30"></asp:TextBox> 
            <asp:Button ID="btnFindSI" runat="server" CssClass="orange" Font-Bold="True" Text="Find"
                Width="75px" />
            <asp:Button ID="btnSelectAll" runat="server" CssClass="green" Font-Bold="True" Text="Select All"
                Width="125px" />
            <asp:Button ID="btnSelectNone" runat="server" CssClass="red" Font-Bold="True" Text="Select None"
                Width="125px" /></td>
    </tr>
    <tr>
        <td>
        </td>
        <td colspan="2" style="height: 15px">
            <asp:GridView id="gvSI" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="trnjualmstoid,trnjualno" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
    <asp:CheckBox ID="chkSelect" runat="server" Checked="<%# GetSelected() %>" ToolTip='<%# Eval("trnjualmstoid") %>' />
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" Width="50px"></HeaderStyle>

<ItemStyle Width="50px" HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnjualno" HeaderText="No SI">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Center"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center" />
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnamtjualnettoidr" HeaderText="Netto">
<HeaderStyle CssClass="gvhdr" HorizontalAlign="Right"></HeaderStyle>
    <ItemStyle HorizontalAlign="Right" />
</asp:BoundField>
<asp:CommandField ShowSelectButton="True" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle><PagerSettings Mode="NumericFirstLast" />
</asp:GridView> 
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblKodefaktur" runat="server" Text="Kode Faktur" Visible="False"></asp:Label>
            <asp:DropDownList ID="DDLKodeFaktur" runat="server" CssClass="inpText" Visible="False">
            </asp:DropDownList></td>
        <td colspan="2" style="height: 15px">
            <asp:Button ID="btnAddSI" runat="server" CssClass="orange" Font-Bold="True" Text="Add and Apply No Faktur"
                Visible="False" />
            <asp:Button ID="btnCancelSI" runat="server" CssClass="gray" Font-Bold="True" Text="Cancel"
                Visible="False" /></td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Underline="True" Text="List SI :"></asp:Label></td>
        <td colspan="2" style="height: 15px">
            <asp:Button ID="btnPostingFaktur" runat="server" CssClass="green" Font-Bold="True"
                Text="POST" />
            <asp:Button ID="btnCancel" runat="server" CssClass="gray" Font-Bold="True" Text="CANCEL" /></td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:GridView id="gvFaktur" runat="server" Width="100%" ForeColor="#333333" DataKeyNames="trnjualmstoid,trnjualno" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No data in database." GridLines="None" PageSize="8">
                <PagerSettings Mode="NumericFirstLast" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="trnjualno" HeaderText="No SI">
                        <HeaderStyle CssClass="gvhdr" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trnjualdate" HeaderText="Tanggal">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="custname" HeaderText="Customer">
                        <HeaderStyle CssClass="gvhdr" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trnamtjualnettoidr" HeaderText="Netto">
                        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="trnjualref" HeaderText="No Faktur">
                        <HeaderStyle CssClass="gvhdr" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    <asp:Label ID="Label5" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No data found !!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </td>
    </tr>
</TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                <strong><span style="font-size: 9pt">
                <img align="absMiddle" alt="" src="../Images/corner.gif" />
                Form Assign No Faktur :.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer><br />
    <br />
    <asp:UpdatePanel id="updPanel2" runat="server">
        <contenttemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="captionPesan" runat="server" Font-Size="Small" Font-Bold="True" Text="header"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px" colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="Image1" runat="server" ImageUrl="~/Images/warn.png"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="isiPesan" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 3px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:Button id="btnErrOK" onclick="btnErrOK_Click" runat="server" CssClass="btn red" Text=" OK " UseSubmitBehavior="False"></asp:Button> </TD></TR></TBODY></TABLE></asp:Panel>&nbsp; <ajaxToolkit:ModalPopupExtender id="mpeMsg" runat="server" TargetControlID="PanelValidasi" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panelvalidasi" PopupDragHandleControlID="Validasi"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnExtenderValidasi" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>

