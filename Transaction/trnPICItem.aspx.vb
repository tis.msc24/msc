﻿Imports System.IO
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports ClassFunction
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnPIC
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim kumpulanSN As String
    Dim has_sn As String
    '@@@@@@@@@@@ Konek Exel @@@@@@@@@
    Dim KodeITemExcel As String
    Dim JumItemExcel As Integer
    Dim adap As OleDbDataAdapter
    Dim SNExcel As String
    Dim cRate As New ClassRate
#End Region

#Region "Functions"

    Private Function IsFilterValid()
        'Dim sError As String = ""
        'If DDLBusUnit.SelectedValue = "" Then
        '    sError &= "- Please select GROUP field!<BR>"
        'End If
        'If DDLLocation.SelectedValue = "" Then
        '    sError &= "- Please select WAREHOUSE field!<BR>"
        'End If
        'If sError <> "" Then
        '    showMessage(sError, CompnyName & " - Warning", 2)
        '    Return False
        'End If
        Return True
    End Function

    Private Function GetTextValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("personoid") = GetDDLValue(row.Cells(4).Controls)
                                    dtView(0)("personname") = GetDDLText(row.Cells(4).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "matoid=" & cbOid
                                dtView2.RowFilter = "matoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("personoid") = GetDDLValue(row.Cells(4).Controls)
                                    dtView(0)("personname") = GetDDLText(row.Cells(4).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView(0)("personoid") = GetDDLValue(row.Cells(4).Controls)
                                        dtView(0)("personname") = GetDDLText(row.Cells(4).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            strCaption &= " - INFORMATION"
            lblCaption.ForeColor = Drawing.Color.White
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"

        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindMaterial()
        sSql = "Select * from (Select 'False' Checkvalue, itemoid matoid, itemcode matcode, UPPER(itemdesc) longdesc, Isnull(personoid,0) personoid, Isnull((select UPPER(personname) personname from QL_MSTPERSON Where PERSONOID = ql_mstitem.personoid),'-') personname From ql_mstitem Where itemflag='AKTIF') It Where " & FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text.Trim) & "%'"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstitem")
        Session("TblMat") = dt
        Session("TblMatView") = Session("TblMat")
        gvListMat.DataSource = Session("TblMatView")
        gvListMat.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TableDetail")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("personoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("personname", Type.GetType("System.String"))
        dtlTable.Columns.Add("longdesc", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal
        DDLBusUnit.CssClass = sCss
        DDLLocation.Enabled = bVal
        DDLLocation.CssClass = sCss
    End Sub

    Private Sub InitAllDDL()
        'Jenis Barang
        sSql = "SELECT personoid, UPPER(personname) personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' AND PERSONOID IN (Select personoid from ql_mstitem) AND STATUS='AKTIF'"
        FillDDL(DDLPic, sSql)
        DDLPic.Items.Add(New ListItem("NONE", "NONE"))
        DDLPic.SelectedIndex = DDLPic.Items.Count - 1
    End Sub

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            'dv = Session("Role").DefaultView
            'MsgBox(dv.Count)
            Response.Redirect("~\Transaction\trnPICItem.aspx")
        End If
        'If checkPagePermission("~\Transaction\trnPICItem.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If

        Page.Title = CompnyName & " - Update PIC Item"
        Session("oid") = Request.QueryString("oid")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
        Page.Title = CompnyName & " - Update PIC Item"
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
        If Not Page.IsPostBack Then
            InitAllDDL()
            'TabContainer1.ActiveTabIndex = 1

        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                mpeListMat.Show()
            End If
        End If
    End Sub

    Private Sub CreateTblDetailSN()
        Dim dtlTableSN As DataTable = New DataTable("TableDetailSN")
        dtlTableSN.Columns.Add("matoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("matno", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matcode", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matlongdesc", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matqty", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("Harga", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("matunit", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("unitoid", Type.GetType("System.Int32"))
        dtlTableSN.Columns.Add("checkvalue", Type.GetType("System.String"))
        dtlTableSN.Columns.Add("enableqty", Type.GetType("System.String"))
        Session("TblDtlSN") = dtlTableSN
    End Sub

    Private Sub BindMaterialSN()

        has_sn = "Yes"
        Session("Y") = has_sn
        Dim SaveLocation As String = FileExelInitStokSN.Text
        Dim ConnStrExe As String = "provider=Microsoft.jet.OLEDB.4.0; Data Source=" & SaveLocation & "; Extended Properties=Excel 8.0;"
        Dim connexc As New OleDbConnection(ConnStrExe)
        Dim commecel As OleDbCommand
        Dim TbSNAda As New DataTable
        Dim TbSNAda1 As New DataTable
        Dim TBCekSN As New DataTable
        Dim jumlahSnSama As Integer = 0
        Dim TampungSnSama As String
        Dim SNDami As Integer = 0
        connexc.Open()
        Try
            sSql = "select itemcode, sn from [SN_Uplaod$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            adap.Fill(TbSNAda)
            adap.Dispose()
        Catch ex As Exception
            showMessage("Data Serial Init Stock Tidak Sesuai ", CompnyName & " - Warning", 2)
            Exit Sub
        End Try

        '@@@@@@@@@@@@@ Cek Item Di Gudang Init @@@@@@@@@@@@@@@
        sSql = "select DISTINCT(itemcode) from [SN_Uplaod$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TbSNAda1)
        For itemDataSn As Integer = 0 To TbSNAda1.Rows.Count - 1
            sSql = "SELECT count(itemoid) FROM QL_mstItem where itemcode = '" & TbSNAda1.Rows(itemDataSn).Item("itemcode") & "'"
            Dim ItemID As Integer = cKon.ambilscalar(sSql)

            sSql = "SELECT count(*) FROM QL_initstock where initrefoid = '" & ItemID & "' and initbrach = '" & DDLBusUnit.SelectedValue & "' and initlocation = '" & DDLLocation.SelectedValue & "'"
            Dim ItemData As Integer = cKon.ambilscalar(sSql)
            If Not ItemData = 0 Then
                showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found!, <strong>check Data Excel</strong> ", CompnyName & " - INFORMATION", 3)
                Exit Sub
            End If
        Next


        '@@@@@@@@@@@@ Cek SN Per Item @@@@@@@@@@@@@@@@@@@@@@@
        sSql = "select Sn from [SN_Uplaod$] "
        adap = New OleDbDataAdapter(sSql, connexc)
        adap.Fill(TBCekSN)
        For SNup As Integer = 0 To TBCekSN.Rows.Count - 1
            sSql = "SELECT count(SN) FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
            Dim JumSn As Integer = cKon.ambilscalar(sSql)
            If JumSn = 1 Then
                sSql = "SELECT SN FROM QL_mstitemDtl WHERE sn = '" & TBCekSN.Rows(SNup).Item("Sn") & "'"
                TampungSnSama = cKon.ambilscalar(sSql)
                kumpulanSN = kumpulanSN & " " & TampungSnSama
                jumlahSnSama = jumlahSnSama + 1
                If jumlahSnSama < TBCekSN.Rows.Count - 1 Then
                    kumpulanSN &= ","
                    If (jumlahSnSama / 2) = 5 Then
                        kumpulanSN &= "<br/>"
                    End If
                End If
            End If
        Next

        If Not jumlahSnSama = 0 Then
            showMessage("<strong>Ada SN Sama Yang Di Upload </strong> <br/> <br> " & kumpulanSN & " ", CompnyName & " - INFORMATION", 3)
            Exit Sub
        End If

        For TamSN As Integer = 0 To TbSNAda1.Rows.Count - 1
            SNExcel &= "'" & TbSNAda1.Rows(TamSN).Item("itemcode") & "'"
            If TamSN < TbSNAda1.Rows.Count - 1 Then
                SNExcel &= ","
            End If
        Next

        UpdateCheckedMat()
        If Session("TblListMatView") Is Nothing Then
            sSql = "select i.itemoid  AS matoid, '' AS matno,i.itemcode matcode,  gen.gendesc +' '+ i.itemdesc +' '+  i.merk matlongdesc ,'' AS matqty, '' AS Harga, gen2.gendesc matunit, gen2.genoid unitoid , 'True' AS checkvalue,'True' AS enableqty   from QL_mstitem i INNER JOIN QL_mstgen gen ON gen.genoid= i.itemgroupoid INNER JOIN QL_mstgen gen2 on gen2.genoid = i.satuan3 where i.has_SN = 1 and i.itemoid NOT IN (SELECT initrefoid FROM QL_initstock init WHERE init.cmpcode='" & CompnyCode & "' AND init.initbrach='" & DDLBusUnit.SelectedValue & "' AND initlocation ='" & DDLLocation.SelectedValue & "') and i.itemcode in (" & SNExcel & ")"
            Dim dtSN As DataTable = cKon.ambiltabel(sSql, "QL_mstmat")

            If dtSN.Rows.Count > 0 Then
                Session("TblListMatView") = dtSN
            End If
        End If

        If Not Session("TblListMatView") Is Nothing Then

            Dim TabelItem As DataTable = Session("TblListMatView")
            Dim dv As DataView = TabelItem.DefaultView

            If Session("TblDtlSN") Is Nothing Then
                CreateTblDetailSN()
                Dim objTblSN As DataTable = Session("TblDtlSN")
                Dim objView As DataView = objTblSN.DefaultView
                For iSN As Integer = 0 To TabelItem.Rows.Count - 1
                    Dim rv As DataRowView = objView.AddNew()
                    rv.BeginEdit()

                    sSql = "select count(sn) from [SN_Uplaod$] where itemcode = '" & TabelItem.Rows(iSN).Item("matcode") & "'"
                    commecel = New OleDbCommand(sSql, connexc)
                    JumItemExcel = commecel.ExecuteScalar()

                    rv("matoid") = dv(iSN)("matoid").ToString
                    rv("matcode") = dv(iSN)("matcode").ToString

                    rv("matqty") = JumItemExcel
                    rv("Harga") = ""
                    rv("matunit") = dv(iSN)("matunit").ToString
                    rv("unitoid") = dv(iSN)("unitoid").ToString
                    rv("checkvalue") = dv(iSN)("enableqty").ToString
                    rv("enableqty") = dv(iSN)("enableqty").ToString
                    rv("matlongdesc") = dv(iSN)("matlongdesc").ToString
                    rv.EndEdit()
                Next
                objTblSN.AcceptChanges()
                Session("TblListMatView") = objTblSN
                Session("TblListMat") = objTblSN

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            Else

                gvListMat.DataSource = Session("TblListMatView")
                gvListMat.DataBind()
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)

            End If
            sSql = "select * from [SN_Uplaod$]"
            adap = New OleDbDataAdapter(sSql, connexc)
            Dim dtecelSN As New DataTable
            adap.Fill(dtecelSN)
            Session("TabelSN") = dtecelSN
        Else
            showMessage("Init Stock Sudah Ada Di Gudang or </br> Material data can't be found! ", CompnyName & " - INFORMATION", 3)
            Exit Sub
        End If

        connexc.Close()
    End Sub

    Protected Sub btnLookUpMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLookUpMat.Click
        If IsFilterValid() Then
            FilterDDLListMat.SelectedIndex = 0
            FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing : gvListMat.DataSource = Nothing : gvListMat.DataBind()
            BindMaterial()
        End If
    End Sub

    Protected Sub cbHdrLM_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        For C1 As Integer = 0 To gvListMat.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        'UpdateCheckedMat()
        gvListMat.PageIndex = e.NewPageIndex
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
                If DDLPic.SelectedValue <> "NONE" Then
                    sFilter &= " And personoid = " & DDLPic.SelectedValue & ""
                End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If 
    End Sub

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbAddToListMat.Click
        UpdateCheckedMat()
        If Session("TblMat") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMat")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.RowFilter = "checkvalue='True'"
                If dv.Count > 0 Then
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If
                    Dim objTbl As DataTable = Session("TblDtl")
                    Dim iSeq As Integer = objTbl.Rows.Count + 1
                    Dim objView As DataView = objTbl.DefaultView
                    For C1 As Integer = 0 To dv.Count - 1
                        objView.RowFilter = " matoid=" & dv(C1)("matoid") & " AND personoid = '" & dv(C1)("personoid") & "'"
                        If objView.Count > 0 Then
                            objView(0)("personoid") = ToDouble(dv(C1)("personoid").ToString)
                        Else
                            Dim rv As DataRowView = objView.AddNew()
                            rv.BeginEdit()
                            rv("seq") = iSeq
                            rv("matoid") = dv(C1)("matoid").ToString
                            rv("matcode") = dv(C1)("matcode").ToString
                            rv("longdesc") = dv(C1)("longdesc").ToString
                            rv("personoid") = dv(C1)("personoid").ToString
                            rv("personname") = dv(C1)("personname").ToString
                            rv.EndEdit()
                            iSeq += 1
                        End If
                        objView.RowFilter = ""
                    Next
                    objTbl.AcceptChanges()
                    Session("TblDtl") = objTbl
                    gvDtl.DataSource = Session("TblDtl")
                    gvDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
                End If
            Else
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Material data can't be found!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub lbSelectAllToList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbSelectAllToList.Click
        UpdateCheckedMat()
        If Session("TblListMat") IsNot Nothing Then
            Dim dtTbl As DataTable = Session("TblListMat")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblListMat")
                Dim objView As DataView = objTbl.DefaultView
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblListMat") = objTbl
                gvListMat.DataSource = Session("TblListMat")
                gvListMat.DataBind()
                lbAddToListMat_Click(Nothing, Nothing)
                btnHideListMat.Visible = False
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            Else
                Session("WarningListMat") = "Please show material data first!"
                showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
            End If
        Else
            Session("WarningListMat") = "Please show material data first!"
            showMessage(Session("WarningListMat"), CompnyName & " - Warning", 2)
        End If
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDtl.PageIndexChanging
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs) Handles gvDtl.RowCancelingEdit
        e.Cancel = True
        gvDtl.EditIndex = -1
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.Cells(5).Text = "Yes" Then
            '    e.Row.Cells(6).Enabled = False
            'Else
            '    e.Row.Cells(6).Enabled = True
            'End If
        End If
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        'Dim country As String = gvDtl.Rows(e.RowIndex).Cells(5).Text
        'If country = "Yes" Then
        '    showMessage("Data Menggunakan SN Tidak Bisa Di Edit", CompnyCode, 3)
        '    e.Cancel = True
        'Else
        e.Cancel = True
        gvDtl.EditIndex = -1
        Dim objTable As DataTable = Session("TblDtl")

        Dim objRow() As DataRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs) Handles gvDtl.RowEditing
        gvDtl.EditIndex = e.NewEditIndex
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
    End Sub

    Protected Sub gvDtl_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvDtl.RowUpdating

    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnFindListMat.Click
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            Dim sFilter As String = ""
            Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
            For C1 As Integer = 0 To sSplit.Length - 1
                sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(sSplit(C1)) & "%'"
                If DDLPic.SelectedValue <> "NONE" Then
                    sFilter &= " And personoid = " & DDLPic.SelectedValue & ""
                End If
                If C1 < sSplit.Length - 1 Then
                    sFilter &= " AND "
                End If
            Next
            dtView.RowFilter = sFilter
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind() : dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        If UpdateCheckedMat() Then
            BindMaterial()
            Session("TblMatView") = Session("TblMat")
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPost.Click
        If Session("TblDtl") Is Nothing Then
            showMessage("Please fill some PIC data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTable As DataTable = Session("TblDtl")
        If objTable.Rows.Count = 0 Then
            showMessage("Please fill some PIC data first!", CompnyName & " - Warning", 2)
            Exit Sub
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            For C1 As Int16 = 0 To objTable.Rows.Count - 1
                sSql = "UPDATE QL_MSTITEM set personoid = " & objTable.Rows(C1).Item("personoid") & " where itemoid = " & objTable.Rows(C1).Item("matoid") & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Next

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            showMessage(ex.Message, CompnyName & " - ERORR", 1)
            conn.Close()
            Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnPICItem.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Transaction\trnPICItem.aspx?awal=true")
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBox1.CheckedChanged
        If Page.IsPostBack Then
            If CheckBox1.Checked = True Then
                FileUpload1.Visible = True
                BtnUplaodFile.Visible = True
                TabContainer1.ActiveTabIndex = 1
            Else
                FileUpload1.Visible = False
                BtnUplaodFile.Visible = False
                FileExelInitStokSN.Text = ""
                TabContainer1.ActiveTabIndex = 1
            End If
            btnPost.Visible = True
            btnCancel.Visible = True
            btnLookUpMat.Visible = True
        End If

    End Sub

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "InitStok_" & ID & ".xls"
        Return fname
    End Function

    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".xls"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function

    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/FileExcel/")

        If FileUpload1.HasFile Then
            If checkTypeFile(FileUpload1.FileName) Then
                savePath &= fname
                Try
                    FileUpload1.PostedFile.SaveAs(savePath)
                    FileExelInitStokSN.Text = savePath
                    Session("LinkSaveFile") = FileExelInitStokSN.Text
                Catch ex As Exception
                End Try
            Else
                showMessage("Tidak dapat diupload. Format file harus .xls!", CompnyName & " - WARNING", 2)
            End If
        Else
            'showMessage("Tidak Ada File  .xls! Yang Di Pilih", CompnyName & " - WARNING", 2)
        End If
    End Sub

    Protected Sub BtnUplaodFile_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnUplaodFile.Click
        Dim fname As String = getFileName(Trim("File Exel Init Stok SN"))
        uploadFileGambar("insert", fname)
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbCloseListMat.Click
        btnHideListMat.Visible = False
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        btnPost.Visible = True
        btnCancel.Visible = True
        btnLookUpMat.Visible = True
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvDtl.SelectedIndexChanged
        If gvDtl.SelectedDataKey("SN").ToString().Trim = "Yes" Then
            showMessage("Data Tidak Bisa Di Edit Atau Delete", CompnyCode, 3)
        End If
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvListMat.RowDataBound 
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim personoid As DropDownList = e.Row.FindControl("personoid")
            sSql = "SELECT personoid, UPPER(personname) personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "' AND PERSONOID IN (Select personoid from ql_mstitem) AND STATUS='AKTIF'"
            FillDDL(personoid, sSql)
            personoid.SelectedValue = personoid.ToolTip
            Dim ods As New SqlDataAdapter(sSql, conn)
            Dim objTablee As New DataTable
            ods.Fill(objTablee)
        End If
    End Sub

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnPreview.Click
        Try
            Dim path As String = Server.MapPath("~/report/FileExel_SN.xls") 'get file object as FileInfo
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
            Response.AddHeader("Content-Disposition", "attachment; filename=FileExel_SN.xls")
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End() 'if file does not exist
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "True"
                    dtTbl.Rows(C1)("CheckValue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), "WARNING", 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "matoid=" & dtTbl.Rows(C1)("matoid")
                    objView(0)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                mpeListMat.Show()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), "WARNING", 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "CheckValue='True'"
            If dtView.Count > 0 Then
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("WarningListMat") = "No material data have been selected before!"
                showMessage(Session("WarningListMat"), "WARNING", 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

#End Region
End Class
