Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_DownPaymentAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cRate As New ClassRate
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "dd/MM/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(FilterPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = "" : Dim sErr As String = ""

        If dpardate.Text = "" Then
            sError &= "- Please fill DP DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(dpardate.Text, "dd/MM/yyyy", sErr) Then
                sError &= "- DP DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select Customer field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select DP ACCOUNT field!<BR>"
        End If
        If dparpayacctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If

        If DDLTypeDP.SelectedValue = "TITIPAN" And dparpayrefno.Text = "" Then
            sError &= "- Maaf, Kolom " & lblRefNo.Text & " Isi dengan Nomer transaksi bank pengirim!<BR>"
        End If

        If CDate(toDate(dpardate.Text)) <= CDate(toDate(CutofDate.Text)) Then
            sError &= "- Tanggal asset Tidak boleh <= CutoffDate (" & CutofDate.Text & ") !! <BR> "
        End If

        If dparpaytype.SelectedValue = "BGM" Then
            If dparpayrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If dparamt.Text = "" Then
            sError &= "- Please fill DP AMOUNT field!<BR>"
        Else
            If ToDouble(dparamt.Text) <= 0 Then
                sError &= "- DP AMOUNT must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("dparamt", "QL_trndpar", ToDouble(dparamt.Text), sErrReply) Then
                    sError &= "- DP AMOUNT must be less than MAX DP AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            dparstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub Abal2BindDP()
        sSql = "Select dp.trndparoid,dp.branch_code,dp.trndparno,dp.trndpardate,dp.trndparamtidr,cb.cashbankoid,cashbankno,trndparacctgoid,dp.cashbankacctgoid From QL_trndpar dp INNER JOIN QL_trncashbankmst cb ON dp.cashbankoid=cb.cashbankoid AND dp.branch_code=cb.branch_code And dp.flagdp='TITIPAN' AND trndparstatus='POST' AND dp.payrefno LIKE '%" & Tchar(dparpayrefno.Text) & "%' And dp.trndparoid NOT IN (sELECT oiddp FROM QL_trndpar wHERE flagdp='PENGAKUAN') and dp.branch_code = '" & DDLBusUnit.SelectedValue & "' "
        FillGV(gvTitipanDP, sSql, "QL_TitipanDP")
        gvTitipanDP.Visible = True
    End Sub

    Private Sub ReAmountDP()
        dparnett.Text = ToMaskEdit(ToDouble(dparamt.Text), 3)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trndpar WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Down Payment A/R data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        sSql = "SELECT currencyoid, currencycode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        Dim sCb As String = ""
        If Session("branch_id") <> "10" Then
            sCb = "And gencode='" & Session("branch_id") & "'"
        Else
            DDLBusUnit.Enabled = True : DDLBusUnit.CssClass = "inpText"
        End If
        sSql = "Select gencode,gendesc from QL_mstgen Where gengroup='CABANG'" & sCb & ""
        FillDDL(DDLBusUnit, sSql) : InitDDLAdd()
    End Sub

    Private Sub fDDLBranch()
        sSql = "Select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fddlCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fddlCabang, sSql)
            Else
                FillDDL(fddlCabang, sSql)
                fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
                fddlCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fddlCabang, sSql)
            fddlCabang.Items.Add(New ListItem("ALL", "ALL"))
            fddlCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(acctgoid, "VAR_DPAR", DDLBusUnit.SelectedValue)
    End Sub

    Private Sub InitDDLDPAccount()
        ' Fill DDL DP Account
        If DDLTypeDP.SelectedValue = "NORMAL" Then
            FillDDLAcctg(acctgoid, "VAR_DPAR", DDLBusUnit.SelectedValue)
        ElseIf DDLTypeDP.SelectedValue = "PENGAKUAN" Then
            FillDDLAcctg(acctgoid, "VAR_DPAR", DDLBusUnit.SelectedValue)
        Else
            FillDDLAcctg(acctgoid, "VAR_DPAR_TITIPAN", DDLBusUnit.SelectedValue)
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT trndparoid, trndparno, CONVERT(VARCHAR(10), trndpardate, 101) AS dpardate, custname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE payreftype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'BANK' ELSE 'GIRO' END) AS dparpaytype, trndparstatus, trndparnote, 'False' AS checkvalue FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.trndparacctgoid WHERE dp.cmpcode='" & CompnyCode & "' and dp.trndparoid >=0 and dp.trndparstatus <> 'DELETE' AND dp.payreftype <> 'RETUR' " & sSqlPlus & " ORDER BY CONVERT(DATETIME, dp.trndpardate) DESC, dp.trndparoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trndpar")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "trndparoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateCashBankNo()
        'If DDLBusUnit.SelectedValue <> "" Then
        Dim sErr As String = "" : Dim bCode As String = GetStrData("Select genother1 from QL_mstgen Where gengroup='CABANG' And cmpcode='" & CompnyCode & "' And gencode='" & DDLBusUnit.SelectedValue & "'")
        If dpardate.Text <> "" Then
            If IsValidDate(dpardate.Text, "dd/MM/yyyy", sErr) Then
                If dparpayacctgoid.SelectedValue <> "" Then
                    Dim sNo As String = dparpaytype.SelectedValue & "/" & bCode & "/" & Format(CDate(toDate(dpardate.Text)), "yy/MM/dd") & "/"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankno LIKE '%" & sNo & "%' AND cashbankacctgoid=" & dparpayacctgoid.SelectedValue
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindCustomerData()
        Dim hCust As String = "" : Dim refono As String = ""
        If DDLTypeDP.SelectedValue = "TITIPAN" Then
            hCust &= " AND custname = 'NONE' and branch_code = '" & DDLBusUnit.SelectedValue & "'"
        Else
            hCust &= " AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND branch_code='" & DDLBusUnit.SelectedValue & "'"
        End If

        'If DDLTypeDP.SelectedValue = "PENGAKUAN" Then
        '    sSql = "SELECT c.custoid,custcode,custname,custaddr,dp.payrefno FROM QL_mstcust c INNER JOIN QL_trndpar dp ON dp.custoid=c.custoid AND dp.flagdp='TITIPAN' AND dp.trndparstatus='POST' Where dp.branch_code='" & DDLBusUnit.SelectedValue & "' AND c.custoid =2437"
        '    gvListCust.Columns(4).Visible = True
        '    FillGV(gvListCust, sSql, "QL_mstcust")
        'Else
        sSql = "SELECT custoid,custcode,custname,custaddr,'' payrefno FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' " & hCust & " ORDER BY custcode, custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
        gvListCust.Columns(4).Visible = False
        'End If
      
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal : lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal : dparduedate.Visible = bVal
        imbDueDate.Visible = bVal : lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub EnableAddInfoGiro(ByVal bVal As Boolean)
        lblDTG.Visible = bVal
        lblWarnDTG.Visible = bVal : lblSeptDTG.Visible = bVal
        dpartakegiro.Visible = bVal : imbDTG.Visible = bVal
        lblInfoDTG.Visible = bVal : lblWarnRefNo.Visible = bVal
    End Sub

    Private Sub GeneratedNoDP()
        Dim cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & DDLBusUnit.SelectedValue & "' AND gengroup='CABANG'")
        Dim sNo As String = "DP/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(trndparno, 2) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" & CompnyCode & "' AND trndparno LIKE '%" & sNo & "%'"
        dparno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT dp.branch_code,dp.cmpcode, trndparoid,trndparno, trndpardate, dp.custoid, custname, dp.trndparacctgoid, dp.cashbankoid, cashbankno, payreftype, payrefno, payduedate, dp.currencyoid, trndparamt, trndparnote, trndparstatus, dp.createuser, dp.createtime, dp.upduser, dp.updtime,dp.cashbankacctgoid,oiddp,flagdp FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE trndparoid=" & sOid & " /*AND dp.branch_code='" & fddlCabang.SelectedValue & "'*/"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                CompnyCode = Trim(xreader("cmpcode").ToString)
                DDLBusUnit.SelectedValue = xreader("branch_code").ToString
                dparoid.Text = Trim(xreader("trndparoid").ToString)
                OidAbal2.Text = Trim(xreader("oiddp").ToString)
                dparno.Text = Trim(xreader("trndparno").ToString)
                dpardate.Text = Format(xreader("trndpardate"), "dd/MM/yyyy")
                custoid.Text = Trim(xreader("custoid").ToString)
                custname.Text = Trim(xreader("custname").ToString)
                DDLTypeDP.SelectedValue = Trim(xreader("flagdp").ToString)
                InitDDLDPAccount()
                acctgoid.SelectedValue = Trim(xreader("trndparacctgoid").ToString)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                dparpaytype.SelectedValue = Trim(xreader("payreftype").ToString)
                dparpaytype_SelectedIndexChanged(Nothing, Nothing)
                If DDLTypeDP.SelectedValue.ToUpper = "PENGAKUAN" Then
                    dparpayrefno.Enabled = False : DDLTypeDP.Enabled = False
                    dparpayacctgoid.Enabled = False : dparamt.Enabled = False
                ElseIf DDLTypeDP.SelectedValue.ToUpper = "TITIPAN" Then
                    DDLTypeDP.Enabled = False
                End If
                dparpayrefno.Text = Trim(xreader("payrefno").ToString)
                dparduedate.Text = Format(xreader("payduedate"), "dd/MM/yyyy")

                If dparduedate.Text = "01/01/1900" Then
                    dparduedate.Text = ""
                End If
                If DDLTypeDP.SelectedValue.ToUpper = "PENGAKUAN" Then
                    FillDDLAcctg(dparpayacctgoid, "VAR_DPAR_TITIPAN", DDLBusUnit.SelectedValue)
                    dparpayacctgoid.SelectedValue = Trim(xreader("trndparacctgoid").ToString)
                    CbCoaOid.Text = Trim(xreader("cashbankacctgoid").ToString)
                Else
                    dparpayacctgoid.SelectedValue = Trim(xreader("cashbankacctgoid").ToString)
                    CbCoaOid.Text = Trim(xreader("cashbankacctgoid").ToString)
                End If

                curroid.SelectedValue = Trim(xreader("currencyoid").ToString)
                dparamt.Text = ToMaskEdit(ToDouble(Trim(xreader("trndparamt").ToString)), 4)
                ReAmountDP()
                dparnote.Text = Trim(xreader("trndparnote").ToString)
                dparstatus.Text = Trim(xreader("trndparstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False
            btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        dparpaytype.CssClass = "inpTextDisabled" : dparpaytype.Enabled = False
        dparpayacctgoid.CssClass = "inpTextDisabled" : dparpayacctgoid.Enabled = False
        If dparstatus.Text = "Post" Then
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False
            btnShowCOA.Visible = True : lblTrnNo.Text = "DP No." : dparoid.Visible = False
            dparno.Visible = True
        End If
    End Sub

    'Private Sub ShowReport()
    '    Try
    '        Dim sOid As String = ""
    '        If Session("TblMst") IsNot Nothing Then
    '            Dim dv As DataView = Session("TblMst").DefaultView
    '            dv.RowFilter = "checkvalue='True'"
    '            For C1 As Integer = 0 To dv.Count - 1
    '                sOid &= dv(C1)("trndparoid").ToString & ","
    '            Next
    '            dv.RowFilter = ""
    '        End If
    '        report.Load(Server.MapPath(folderReport & "rptDPAR.rpt"))
    '        Dim sWhere As String = ""
    '        If Session("CompnyCode") <> CompnyCode Then
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.cmpcode='" & Session("CompnyCode") & "'"
    '        End If
    '        If sOid = "" Then
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
    '            If cbPeriode.Checked Then
    '                If IsValidPeriod() Then
    '                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dpardate>='" & FilterPeriod1.Text & " 00:00:00' AND dpardate<='" & FilterPeriod2.Text & " 23:59:59'"
    '                Else
    '                    Exit Sub
    '                End If
    '            End If
    '            If cbStatus.Checked Then
    '                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dparstatus='" & FilterDDLStatus.SelectedValue & "'"
    '            End If
    '            If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
    '                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.createuser='" & Session("UserID") & "'"
    '            End If
    '        Else
    '            sOid = Left(sOid, sOid.Length - 1)
    '            sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.dparoid IN (" & sOid & ")"
    '        End If
    '        report.SetParameterValue("sWhere", sWhere)
    '        cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
    '        Response.Buffer = False
    '        Response.ClearContent()
    '        Response.ClearHeaders()
    '        report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DownPaymentARPrintOut")
    '        report.Close()
    '        report.Dispose()
    '    Catch ex As Exception
    '        showMessage(ex.Message, 1)
    '        report.Close()
    '        report.Dispose()
    '    End Try
    '    Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    'End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnDPAR.aspx")
        End If
        'If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("Role")) = False Then
        '    Response.Redirect("~\Other\NotAuthorize.aspx")
        'End If
        Page.Title = CompnyName & " - Down Payment A/R"
        Session("oid") = Request.QueryString("oid")
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' /*AND BRANCH_CODE='" & Session("branch_id") & "'*/")
        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", CompnyName)
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If

        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            If checkPagePermission("Accounting/trnDPAR.aspx", Session("SpecialAccess")) = False Then
                sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
            End If
            sSql = "Select genother1 from QL_mstgen jp INNER JOIN QL_MSTPERSON pr ON genoid=pr.PERSONSTATUS AND gengroup='JOBPOSITION'INNER JOIN QL_MSTPROF pf ON pf.USERNAME=pr.PERSONNAME Where USERID='" & Session("UserID") & "'"
            Dim aks As String = GetStrData(sSql)

            If aks = "ANALISA ACC PUSAT" Or aks = "STAFF ACC PUSAT" Or aks = "OWNER" Then
                sSqlPlus = ""
            ElseIf aks = "KACAB" Then
                sSqlPlus &= " AND dp.branch_code='" & DDLBusUnit.SelectedValue & "'"
            ElseIf aks = "KASIRCABANG" Then
                sSqlPlus &= " AND dp.branch_code='" & DDLBusUnit.SelectedValue & "' AND dp.createuser='" & Session("UserID") & "'"
            End If

            BindTrnData(sSqlPlus) : CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            InitALLDDL() : fDDLBranch()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dparoid.Text = GenerateID("QL_TRNDPAR", CompnyCode)
                'cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                dpardate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                dparstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False : btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                dparpaytype_SelectedIndexChanged(Nothing, Nothing)
                cashbankno.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7 : FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, dp.updtime, GETDATE()) > " & nDays & " AND dparstatus='In Process' "
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND trndpardate>='" & FilterPeriod1.Text & " 00:00:00' AND trndpardate<='" & CDate(toDate(FilterPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND trndparstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        'If checkPagePermission("Accounting/trnDPAR.aspx", Session("SpecialAccess")) = False Then
        '    sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        'End If
        If fddlCabang.SelectedValue <> "ALL" Then
            sSqlPlus &= " AND dp.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = "" : FilterDDL.SelectedIndex = -1
        FilterText.Text = "" : cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = False : FilterDDLStatus.SelectedIndex = -1
        'If checkPagePermission("Accounting/trnDPAR.aspx", Session("SpecialAccess")) = False Then
        '    sSqlplus &= " AND dp.createuser='" & Session("UserID") & "'"
        'End If
        If fddlCabang.SelectedValue <> "ALL" Then
            sSqlplus &= " AND dp.branch_code = '" & fddlCabang.SelectedValue & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        'UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDPAccount() : InitDDLAdd()
        dparpaytype_SelectedIndexChanged(Nothing, Nothing)
        'If i_u.Text = "New Data" Then
        '    GenerateCashBankNo()
        'End If
    End Sub

    Protected Sub dpardate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpardate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = ""
        custname.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        If DDLTypeDP.SelectedValue = "PENGAKUAN" Then
            dparpayrefno.Text = gvListCust.SelectedDataKey.Item("payrefno").ToString
            dparpayrefno.Enabled = False
        End If
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub dparpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpaytype.SelectedIndexChanged
        dparpayrefno.Text = "" : dparduedate.Text = "" : dpartakegiro.Text = ""
        If dparpaytype.SelectedValue = "BKM" Then
            EnableAddInfo(False) : EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_CASH", DDLBusUnit.SelectedValue)
        ElseIf dparpaytype.SelectedValue = "BBM" Then
            'EnableAddInfo(True)
            EnableAddInfoGiro(False) : EnableAddInfo(False)
            'Format(GetServerTime(), "dd/MM/yyyy")
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        ElseIf dparpaytype.SelectedValue = "BGM" Then
            EnableAddInfo(True) : EnableAddInfoGiro(True)
            FillDDLAcctg(dparpayacctgoid, "VAR_GIRO_PIUTANG", DDLBusUnit.SelectedValue)
        Else
            'EnableAddInfo(True) : EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        End If
    End Sub

    Protected Sub dparpayacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpayacctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparamt.TextChanged
        dparamt.Text = ToMaskEdit(ToDouble(dparamt.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trndpar WHERE trndparoid=" & dparoid.Text
                If CheckDataExists(sSql) Then
                    dparoid.Text = GenerateID("QL_TRNDPAR", CompnyCode)
                    isRegenOid = True
                End If

                If DDLTypeDP.SelectedValue = "PENGAKUAN" Then
                    cashbankoid.Text = cashbankoid.Text
                Else
                    If Session("oid") = "" Or Session("oid") = Nothing Then
                        cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                    End If
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpar", "trndparoid", dparoid.Text, "trndparstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    dparstatus.Text = "In Process"
                    Exit Sub
                End If
            End If

            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iSeq As Integer = 1 : Dim dDate As String = ""
            Dim cRate As New ClassRate() : Dim iGiroAcctgOid As Integer = 0

            periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
            dDate = GetServerTime()

            If dparstatus.Text.ToLower = "post" Then
                GeneratedNoDP() : GenerateCashBankNo()
                cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))

                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If

                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If

                Dim sVarErr As String = ""
                If dparpaytype.SelectedValue = "BGM" Then
                    If Not IsInterfaceExists("VAR_GIRO_PIUTANG", DDLBusUnit.SelectedValue) Then
                        sVarErr &= "VAR_GIRO_PIUTANG"
                    Else
                        iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO_PIUTANG", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If

                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
            End If

            '==============================
            'Cek Peroide Bulanan Dari Crdgl
            '==============================
            If ToDouble(dparamt.Text) > 0 Then
                'CEK PERIODE AKTIF BULANAN
                sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
                'If GetStrData(sSql) <> "" Then
                If GetPeriodAcctg(GetServerTime()) < GetStrData(sSql) Then
                    showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", 2)
                    Exit Sub
                End If
            End If

            Dim CoaOidCB As Integer = 0
            If DDLTypeDP.SelectedValue.ToUpper = "PENGAKUAN" Then
                CoaOidCB = CbCoaOid.Text
            Else
                CoaOidCB = dparpayacctgoid.SelectedValue
            End If

            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    Dim RateIDR As Double = cRate.GetRateDailyIDRValue
                    Dim RateUSD As Double = cRate.GetRateDailyUSDValue
                    Dim Rate2IDR As Double = cRate.GetRateMonthlyIDRValue
                    Dim Rate2USD As Double = cRate.GetRateMonthlyUSDValue
                    '========================
                    'Insert QL_TRNCASHBANKMST
                    '========================
                    If DDLTypeDP.SelectedValue.ToUpper <> "PENGAKUAN" Then
                        sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid,branch_code, cashbankno, cashbankdate, cashbanktype, cashbankgroup, cashbankacctgoid,cashbankcurroid, pic,pic_refname , cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime,cashbankamount,cashbankamountidr)" & _
                        " VALUES ('" & CompnyCode & "'," & cashbankoid.Text & ",'" & DDLBusUnit.SelectedValue & "','" & cashbankno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),'" & dparpaytype.SelectedValue & "','DPAR'," & dparpayacctgoid.SelectedValue & "," & curroid.SelectedValue & ", " & custoid.Text & ",'QL_MSTCUST','DPAR Payment " & cashbankno.Text & " ', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & ToDouble(dparamt.Text) & "," & ToDouble(dparamt.Text) & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    '=================
                    'Update QL_TRNDPAR
                    '=================

                    sSql = "INSERT INTO QL_trnDPAR (cmpcode,trnDPARoid,branch_code,trnDPARno,trnDPARdate,custoid,cashbankoid,trnDPARacctgoid,payreftype,cashbankacctgoid,payduedate,payrefno,currencyoid,currencyrate,rate2oid,trnDPARamt,trnDPARamtidr,trnDPARamtusd,taxtype,taxoid,taxpct,taxamt,taxamtidr,taxamtusd,trnDPARnote,trnDPARflag,trnDPARacumamt,trnDPARacumamtidr,trnDPARacumamtusd, trnDPARstatus, createuser,createtime,upduser,updtime,oiddp,flagdp) " & _
                    " VALUES ('" & CompnyCode & "', " & dparoid.Text & ",'" & DDLBusUnit.SelectedValue & "','" & dparno.Text & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), " & custoid.Text & "," & cashbankoid.Text & ", " & acctgoid.SelectedValue & ", '" & dparpaytype.SelectedValue & "', " & CoaOidCB & ",'" & IIf(dparpaytype.SelectedValue <> "BKM", dDate, "1/1/1900") & "','" & Tchar(dparpayrefno.Text) & "', " & curroid.SelectedValue & ",0," & cRate.GetRateMonthlyOid & "," & ToDouble(dparamt.Text) & "," & ToDouble(dparnett.Text) & "," & ToDouble(dparamt.Text) & ",'NONTAX','',0,0,0,0,'" & Tchar(dparnote.Text) & "','',0,0,0,'" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "',CURRENT_TIMESTAMP," & OidAbal2.Text & ",'" & DDLTypeDP.SelectedValue & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & dparoid.Text & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    '========================
                    'Update QL_TRNCASHBANKMST
                    '======================== 
                    If DDLTypeDP.SelectedValue.ToUpper <> "PENGAKUAN" Then
                        sSql = "UPDATE QL_trncashbankmst set cashbankno='" & cashbankno.Text & "',cashbankdate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)),cashbanktype='" & dparpaytype.SelectedValue & "',cashbankacctgoid=" & dparpayacctgoid.SelectedValue & ",cashbankcurroid=" & curroid.SelectedValue & ",pic=" & custoid.Text & ",cashbanknote='DPAR Payment " & cashbankno.Text & "',cashbankstatus='" & dparstatus.Text & "',upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP,cashbankamount=" & ToDouble(dparnett.Text) & ",cashbankamountidr=" & ToDouble(dparnett.Text) & " Where cmpcode='" & CompnyCode & "' and cashbankoid='" & cashbankoid.Text & "' and branch_code='" & DDLBusUnit.SelectedValue & "'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If

                    '=================
                    'Update QL_TRNDPAP
                    '=================
                    sSql = "UPDATE QL_trndpar SET trndparno='" & dparno.Text & "', trndpardate=(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), custoid=" & custoid.Text & ", payreftype='" & dparpaytype.SelectedValue & "', trndparacctgoid= " & acctgoid.SelectedValue & ", payrefno='" & Tchar(dparpayrefno.Text) & "', payduedate='" & IIf(dparpaytype.SelectedValue <> "BKM", dDate, "1/1/1900") & "', currencyoid=" & curroid.SelectedValue & ",rate2oid=" & cRate.GetRateMonthlyOid & ", trndparamt=" & ToDouble(dparamt.Text) & ", trndparnote='" & Tchar(dparnote.Text) & "', trndparstatus='" & dparstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, trndparamtidr=" & ToDouble(dparamt.Text) & ", trndparamtusd=" & ToDouble(dparamt.Text) & " WHERE cmpcode='" & CompnyCode & "' AND trndparoid=" & dparoid.Text & " and branch_code='" & DDLBusUnit.SelectedValue & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                '--- Kondisi jika data di postig ---
                '===================================
                If dparstatus.Text = "Post" Then
                    Dim sDate As Date = CDate(toDate(dpardate.Text))
                    If dparpaytype.SelectedValue = "BBM" Then
                        sDate = CDate(toDate(dpardate.Text))
                    End If
                    '==================
                    'Insert Auto Jurnal
                    '==================
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid,branch_code, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type) " & _
                    " VALUES ('" & CompnyCode & "', " & iGlMstOid & ",'" & DDLBusUnit.SelectedValue & "',(Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & periodacctg.Text & "', 'DP A/R|No=" & dparno.Text & "', '" & dparstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'DPAR')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' Cash/Bank/Giro
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1,glpostdate) " & _
                    " VALUES " & _
                    " ('" & CompnyCode & "', " & iGlDtlOid & ",'" & DDLBusUnit.SelectedValue & "', " & iSeq & ", " & iGlMstOid & ", " & IIf(dparpaytype.SelectedValue = "BGM", iGiroAcctgOid, dparpayacctgoid.SelectedValue) & ",'D', " & ToDouble(dparnett.Text) & ", '" & dparno.Text & "', 'DP A/R | Cust. " & custname.Text & " | No. Cashbank " & cashbankno.Text & " | Note. " & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparnett.Text) & ", " & ToDouble(dparnett.Text) & ",'QL_trndpar " & dparoid.Text & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    ' Uang Muka
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid,branch_code, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glpostdate) " & _
                    "VALUES " & _
                    " ('" & CompnyCode & "', " & iGlDtlOid & ",'" & DDLBusUnit.SelectedValue & "', " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'C', " & ToDouble(dparamt.Text) & ", '" & dparno.Text & "', 'DP A/R | Cust. " & custname.Text & " | No. Cashbank " & cashbankno.Text & " | Note. " & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparamt.Text) & ", " & ToDouble(dparamt.Text) & ", 'QL_trndpar " & dparoid.Text & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                dparstatus.Text = "In Process"
                showMessage(ex.ToString & "<br>" & sSql, 1)
                Exit Sub
            End Try

            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & dparoid.Text & ".<BR>"
            End If

            If dparstatus.Text = "Post" Then
                Session("SavedInfo") &= "Data Sudah posting dengan DP No. = " & dparno.Text & "."
            End If

            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dparoid.Text = "" Then
            showMessage("Please select Down Payment A/R data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpar", "trndparoid", dparoid.Text, "trndparstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                dparstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If DDLTypeDP.SelectedValue.ToUpper <> "PENGAKUAN" Then
                sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankoid IN (SELECT cashbankoid FROM QL_trndpar WHERE cmpcode='" & CompnyCode & "' AND trndparoid=" & dparoid.Text & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            
            sSql = "DELETE FROM QL_trndpar WHERE cmpcode='" & CompnyCode & "' AND trndparoid=" & dparoid.Text & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit() : xCmd.Connection.Close()

        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        dparstatus.Text = "Post" : btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(dparno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpar " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(dparno.Text, CompnyCode, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpar " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        'UpdateCheckedValue()
        'ShowReport()
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        Try
            Dim swhere As String = ""
            Dim cabang As String = ""
            If fddlCabang.SelectedValue <> "ALL" Then
                cabang = fddlCabang.SelectedValue
            End If
            swhere = "where dp.branch_code = '" & cabang & "' and dp.trndparoid = '" & gvTRN.SelectedDataKey.Item(0) & "'"
            report.Load(Server.MapPath("~/report/rptNotaSO.rpt"))

            report.Load(Server.MapPath("~/report/printoutdpar.rpt"))

            report.SetParameterValue("sWhere", swhere)
            report.SetParameterValue("userid", Session("userid"))
            cProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.PrintOptions.PaperSize = PaperSize.PaperA4

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAR" & "_" & Format(GetServerTime(), "dd_MM_yy"))
            report.Close() : report.Dispose()
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Protected Sub DDLTypeDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        InitDDLDPAccount()
        If DDLTypeDP.SelectedValue = "TITIPAN" Then
            LblTransBank.Visible = True : dparpaytype.Visible = True
            TrnDpNo.Visible = False : btnDp.Visible = False
            ebtnDp.Visible = False : dparpaytype.SelectedValue = "BBM"
        ElseIf DDLTypeDP.SelectedValue = "PENGAKUAN" Then
            LblTransBank.Visible = False : dparpaytype.Visible = False
            TrnDpNo.Visible = True : btnDp.Visible = True
            ebtnDp.Visible = True : LblTransBank.Visible = True
            dparpaytype.SelectedValue = "BBM" : Label10.Text = "Dp Titipan"
        Else
            TrnDpNo.Visible = False : dparpaytype.Visible = True
            btnDp.Visible = False : ebtnDp.Visible = False
            LblTransBank.Visible = False
        End If
        dparpaytype_SelectedIndexChanged(e, e)
    End Sub

    Protected Sub btnDp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDp.Click
        Abal2BindDP()
    End Sub

    Protected Sub gvTitipanDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTitipanDP.PageIndexChanging
        gvTitipanDP.PageIndex = e.NewPageIndex
        Abal2BindDP()
    End Sub

    Protected Sub gvTitipanDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTitipanDP.SelectedIndexChanged
        FillDDLAcctg(dparpayacctgoid, "VAR_DPAR_TITIPAN", DDLBusUnit.SelectedValue)
        OidAbal2.Text = gvTitipanDP.SelectedDataKey.Item("trndparoid").ToString
        cashbankoid.Text = gvTitipanDP.SelectedDataKey.Item("cashbankoid").ToString
        TrnDpNo.Text = gvTitipanDP.SelectedDataKey.Item("trndparno").ToString
        cashbankno.Text = gvTitipanDP.SelectedDataKey.Item("cashbankno").ToString
        dparpayacctgoid.SelectedValue = gvTitipanDP.SelectedDataKey.Item("trndparacctgoid").ToString
        CbCoaOid.Text = gvTitipanDP.SelectedDataKey.Item("cashbankacctgoid").ToString
        dparamt.Text = ToMaskEdit(ToDouble(gvTitipanDP.SelectedDataKey.Item("trndparamtidr").ToString), 4)
        dparnett.Text = ToMaskEdit(ToDouble(gvTitipanDP.SelectedDataKey.Item("trndparamtidr").ToString), 4)
        dparpayacctgoid.Enabled = False : dparamt.Enabled = False
        gvTitipanDP.Visible = False
    End Sub

    Protected Sub ebtnDp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        OidAbal2.Text = "0"
        cashbankoid.Text = "0"
        dparpayacctgoid.Enabled = False : dparamt.Enabled = False
        gvTitipanDP.Visible = False
    End Sub
End Class