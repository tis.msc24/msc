<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="mstItemsts.aspx.vb" Inherits="mstItemsts" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent"> 
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" style="height: 28px">
                <asp:Label ID="HDRBARANG" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Large"
                    ForeColor="Navy" Text=".: Data Katalog"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">

                <contenttemplate></contenttemplate>
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<ajaxToolkit:TabContainer id="TabContainer1" runat="server" Width="100%" ActiveTabIndex="1"><cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
                            <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
                            <strong><span style="font-size: 9pt">Daftar Barang :.</span></strong>

                        
</HeaderTemplate>
<ContentTemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><asp:UpdatePanel id="UPanelListMaterial" runat="server" __designer:wfdid="w91"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Width="56px" CssClass="normalFont" Text="Filter" __designer:wfdid="w92"></asp:Label></TD><TD align=left colSpan=5><asp:DropDownList id="FilterDDL" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w93" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged"><asp:ListItem Selected="True" Value="itemdesc">Nama Barang</asp:ListItem>
<asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="personname">PIC</asp:ListItem>
<asp:ListItem>Merk</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w94" MaxLength="30"></asp:TextBox></TD></TR><TR><TD class="Label" align=left>Jenis Barang</TD><TD align=left colSpan=5><asp:DropDownList id="JenisB" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w105"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem Value="ASSET">ASSET (Barang Depresiasi)</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Status</TD><TD align=left colSpan=5><asp:DropDownList id="ddlStatusView" runat="server" Width="99px" CssClass="inpText" __designer:wfdid="w95" AutoPostBack="True"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList>&nbsp;View Top <asp:TextBox id="SelectTop" runat="server" Width="30px" CssClass="inpText" __designer:wfdid="w96" MaxLength="30">100</asp:TextBox>&nbsp;<asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w97"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w98"></asp:ImageButton> <asp:ImageButton id="imbPrint" onclick="imbPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w99"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="fteTop" runat="server" __designer:wfdid="w100" Enabled="True" TargetControlID="SelectTop" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender> <asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w101"><ProgressTemplate>
<asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w102"></asp:Image><BR /><SPAN class="normalFont">Please wait...</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="Panel1" runat="server" __designer:wfdid="w103" DefaultButton="btnSearch"><asp:GridView id="GVmstgen" runat="server" Width="100%" CssClass="normalFont" ForeColor="#333333" __designer:wfdid="w104" EnableModelValidation="True" DataKeyNames="itemoid" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" GridLines="None" PageSize="8">
<PagerSettings FirstPageText="&#171;" LastPageText="&#187;" Mode="NumericFirstLast" PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" BorderColor="Cyan" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="itemoid" DataNavigateUrlFormatString="mstItem.aspx?oid={0}" DataTextField="itemcode" HeaderText="Kode" SortExpression="itemcode">
<ControlStyle Font-Size="Small"></ControlStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="15%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="itemdesc" HeaderText="Nama Katalog" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="Small" ForeColor="Black" Width="65%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="merk" HeaderText="Merk Katalog" SortExpression="merk">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="personoid" HeaderText="PIC" SortExpression="personoid">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="JenisBarang" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemflag" HeaderText="Status" SortExpression="itemflag">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:ImageButton id="btnprintlist" onclick="btnprintlist_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" __designer:wfdid="w425" CommandArgument='<%#eval("itemoid") %>' ToolTip='<%#eval("itemcode") %>'></asp:ImageButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Tidak Ditemukan !!!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR></TBODY></TABLE><DIV style="DISPLAY: block; TEXT-ALIGN: center">&nbsp;</DIV>
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="imbPrint"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel> &nbsp; &nbsp;&nbsp; </DIV>
</ContentTemplate>
</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2"><HeaderTemplate>
 <img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
<strong><span style="font-size: 9pt">Form Barang :.</span></strong>
                       
</HeaderTemplate>
<ContentTemplate>
<TABLE><TBODY><TR><TD class="Label" align=left><asp:Label id="Label1" runat="server" Width="90px" CssClass="normalFont" Font-Size="Small" Text="Grup Barang" __designer:wfdid="w42"></asp:Label></TD><TD align=left><asp:DropDownList id="itemgroupoid" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w47" OnSelectedIndexChanged="itemgroupoid_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="itemoid" runat="server" Width="4px" CssClass="normalFont" Font-Size="Small" __designer:wfdid="w39" Visible="False"></asp:Label> </TD><TD align=left><asp:Label id="UserName" runat="server" __designer:wfdid="w41" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red" Text="New" __designer:wfdid="w40" Visible="False"></asp:Label>&nbsp;<asp:Label id="ItemRef" runat="server" __designer:wfdid="w41" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label58" runat="server" Width="90px" CssClass="normalFont" Font-Size="Small" Text="Kode Barang" __designer:wfdid="w42"></asp:Label> <SPAN style="COLOR: #ff0000">*</SPAN></TD><TD align=left><asp:TextBox id="itemcode" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w43" MaxLength="20" Enabled="False" size="20"></asp:TextBox> </TD><TD align=left></TD><TD align=left></TD></TR><!-- Grouping Barang--><TR><TD class="Label" align=left><asp:Label id="Label57" runat="server" Width="94px" CssClass="normalFont" Font-Size="Small" Text="Nama Barang" __designer:wfdid="w44"></asp:Label> </TD><TD align=left><asp:TextBox style="TEXT-TRANSFORM: uppercase" id="itemdesc" runat="server" Width="279px" Height="16px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w45" MaxLength="250" AutoPostBack="True" size="20"></asp:TextBox> </TD><TD align=left></TD><TD align=left></TD></TR><TR><TD style="COLOR: #000099" align=left colSpan=2><asp:Label style="FONT-WEIGHT: 700" id="Label44" runat="server" Width="236px" CssClass="normalFont" Font-Size="Small" Text=":. Grouping Barang" __designer:wfdid="w46"></asp:Label>&nbsp; </TD><TD style="COLOR: #000099" align=left colSpan=1></TD><TD style="COLOR: #000099" align=left colSpan=1></TD></TR><TR><TD align=left><asp:Label id="Label55" runat="server" Width="40px" CssClass="normalFont" Font-Size="Small" Text="Merk" __designer:wfdid="w48"></asp:Label> </TD><TD align=left><asp:DropDownList id="dd_merk" runat="server" Width="112px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w49"></asp:DropDownList> </TD><TD align=left><asp:Label id="spg" runat="server" Width="27px" CssClass="normalFont" Font-Size="Small" Text="PIC" __designer:wfdid="w50"></asp:Label> </TD><TD align=left><asp:DropDownList id="spgOid" runat="server" Width="186px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w51"></asp:DropDownList> </TD></TR><TR><TD align=left><asp:Label id="Label18" runat="server" Width="57px" CssClass="normalFont" Font-Size="Small" Text="Satuan" __designer:wfdid="w52"></asp:Label> </TD><TD vAlign=top align=left><asp:DropDownList id="satuan2" runat="server" Width="112px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w53"></asp:DropDownList> </TD><TD vAlign=top align=left><asp:Label id="Label51" runat="server" Width="79px" Text="Jenis Barang" __designer:wfdid="w54"></asp:Label> </TD><TD vAlign=top align=left><asp:DropDownList id="dd_stock" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w55"><asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem Value="ASSET">ASSET (Barang Depresiasi)</asp:ListItem>
</asp:DropDownList> </TD></TR><!-- end grouping barang --><TR><TD align=left><asp:Label id="Label50" runat="server" Width="85px" CssClass="normalFont" Font-Size="Small" Text="Jenis Produk" __designer:wfdid="w56"></asp:Label> </TD><TD align=left><asp:DropDownList id="DDLStatusItem" runat="server" Width="112px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w57"><asp:ListItem>UNGGULAN</asp:ListItem>
<asp:ListItem>NON UNGGULAN</asp:ListItem>
</asp:DropDownList> </TD><TD align=left><asp:Label id="Label12" runat="server" CssClass="normalFont" Font-Size="Small" Text="Status" __designer:wfdid="w58"></asp:Label> </TD><TD align=left><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w59"><asp:ListItem Value="Aktif">Aktif</asp:ListItem>
<asp:ListItem Value="Tidak Aktif">Tidak Aktif</asp:ListItem>
</asp:DropDownList> </TD></TR><TR><TD align=left><asp:Label id="Label21" runat="server" Width="54px" CssClass="normalFont" Font-Size="Small" Text="Has SN" __designer:wfdid="w60"></asp:Label> </TD><TD vAlign=top align=left><asp:CheckBox id="cb_sn" runat="server" __designer:wfdid="w61"></asp:CheckBox> <asp:Label id="flagitem" runat="server" __designer:wfdid="w62" Visible="False"></asp:Label> </TD><TD vAlign=top align=left></TD><TD vAlign=top align=left>&nbsp;</TD></TR><TR><TD vAlign=top align=left><asp:Label id="Label4" runat="server" CssClass="normalFont" Font-Size="Small" Text="Keterangan" __designer:wfdid="w63"></asp:Label> </TD><TD align=left colSpan=3><asp:TextBox id="keterangan" runat="server" Width="500px" CssClass="inpText" Font-Size="Small" __designer:wfdid="w64" MaxLength="100"></asp:TextBox> </TD></TR><!-- Dimensi --><!-- end dimensi --><TR><TD align=left colSpan=2><asp:Label id="lblupd" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w65"></asp:Label> by &nbsp;<asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w66"></asp:Label> on &nbsp;<asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w67"></asp:Label> &nbsp;&nbsp; </TD><TD align=left colSpan=1><asp:Label id="Label6" runat="server" Width="69px" Height="11px" CssClass="normalFont" Font-Size="Small" Text="Kode Akun" __designer:wfdid="w68" Visible="False"></asp:Label> </TD><TD align=left colSpan=1><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w69" Visible="False"></asp:DropDownList> </TD></TR><TR><TD align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w70"></asp:ImageButton> <asp:ImageButton id="BtnCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w71"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w72"></asp:ImageButton> </TD><TD align=left colSpan=1><asp:Label id="Label53" runat="server" Width="80px" CssClass="normalFont" Font-Size="Small" Text="Tipe Barang" __designer:wfdid="w73" Visible="False"></asp:Label> </TD><TD align=left colSpan=1><asp:DropDownList id="dd_type" runat="server" CssClass="inpText" Font-Size="Small" __designer:wfdid="w74" Visible="False"></asp:DropDownList> </TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w77" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div5" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="imgReportForm" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w78"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR></TBODY></TABLE>
</ContentTemplate>
</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3"><HeaderTemplate>
<img align="absMiddle" alt="" height="16" src="../Images/corner.gif" />
<strong><span style="font-size: 9pt">List Harga Cabang :.</span></strong>                 
</HeaderTemplate>
<ContentTemplate>
<asp:GridView ID="GV_Branch" runat="server" Width="100%" CssClass="normalFont" ForeColor="#333333" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid" EnableModelValidation="True" GridLines="None">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" BorderColor="Cyan" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="nomor" HeaderText="No" SortExpression="nomor" Visible="False"></asp:BoundField>
<asp:BoundField DataField="branch_name" HeaderText="Nama Cabang" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="65%" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="Small" Width="65%"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="pricelist" HeaderText="Price List">
    <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
<asp:BoundField DataField="biayaExpedisi" HeaderText="Biaya Expedisi">
    <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
<asp:BoundField DataField="pricelist_cbg" HeaderText="Price List Cabang">
    <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
<asp:BoundField DataField="expedisi_type" HeaderText="Jalur Expedisi" SortExpression="Expedisi">
    <HeaderStyle CssClass="gvhdr" ForeColor="Black" />
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" CssClass="Important" Text="Data Tidak Ditemukan !!!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#990000"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> <table><tr><td align="left"><asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Cancel.png" /> </td></tr></table>
</ContentTemplate>
</cc1:TabPanel>
</ajaxToolkit:TabContainer> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanelValidasi" runat="server">
        <ContentTemplate>
<asp:Panel id="PanelValidasi" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD><asp:Image id="Image1" runat="server" Width="24px" ImageUrl="~/Images/warn.png" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left"><asp:Label id="Validasi" runat="server" Font-Size="X-Small" ForeColor="Red"></asp:Label><BR /></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="ImageButton4" onclick="ImageButton4_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderValidasi" runat="server" TargetControlID="ButtonExtendervalidasi" Drag="True" PopupDragHandleControlID="label15" BackgroundCssClass="modalBackground" PopupControlID="Panelvalidasi" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="ButtonExtendervalidasi" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdPanelPrint" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPrint" runat="server" Width="300px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPrint" runat="server" Font-Size="Medium" Font-Bold="True" Text="Print Data Barang"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="printType" runat="server" CssClass="Important" Visible="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="orderIDForReport" runat="server" CssClass="Important" Visible="False"></asp:Label><asp:Label id="orderNoForReport" runat="server" CssClass="Important"></asp:Label></TD></TR><TR><TD style="HEIGHT: 23px; TEXT-ALIGN: center" align=left colSpan=2><asp:ImageButton id="imbPrintPDF" onclick="imbPrintPDF_Click" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintExcel" onclick="imbPrintExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbCancelPrint" onclick="imbCancelPrint_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblError" runat="server" CssClass="Important"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePrint" runat="server" TargetControlID="btnHidePrint" PopupControlID="pnlPrint" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPrint" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePrint" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="imbPrintPDF"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="imbPrintExcel"></asp:PostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
