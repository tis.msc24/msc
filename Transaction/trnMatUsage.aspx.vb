Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnMatUsage
    Inherits System.Web.UI.Page

#Region "variabel"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim sTemp As String = ""
    Dim da As SqlDataAdapter
    Dim ds As New DataSet
    Dim tempID As Integer
    Public folderReport As String = "~/Report/"
    'Public PrinterPos As String = "PRINTER POS"
    Private RestoReport As New ReportDocument
    Public printerPOS As String = ConfigurationSettings.AppSettings("PrinterName")
    Dim report As New ReportDocument
    Dim ckon As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Procedure"
    Private Sub FillTextbox()
        Dim tolocationoid As Integer = 0
        sSql = "SELECT FromBranch,transferno, trfmtrdate, trfmtrnote, noref, status, FromMtrlocoid, ToMtrlocOid, updtime, upduser ,COAusage FROM QL_matUsageMst WHERE cmpcode = '" & cmpcode & "' AND trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                InitAllDDL()
                ddlFromcabang.SelectedValue = xreader("FromBranch")
                transferno.Text = xreader("transferno")
                transferdate.Text = Format(xreader("trfmtrdate"), "dd/MM/yyyy")
                fromlocation.SelectedValue = xreader("FromMtrlocoid")
                note.Text = xreader("trfmtrnote")
                noref.Text = xreader("noref")
                tolocationoid = xreader("ToMtrlocOid")
                trnstatus.Text = xreader("status").ToString.Trim
                upduser.Text = xreader("upduser")
                updtime.Text = Format(xreader("updtime"), " ddd, dd/MM/yyyy hh:mm:ss tt")
                FillDDLAcctg(ddlCOA, "VAR_MATERIAL_USAGE", ddlFromcabang.SelectedValue)
                ddlCOA.SelectedValue = xreader("COAusage")

                FillDDL(ddlTocabang, "select gencode,gendesc from ql_mstgen where gengroup='cabang' and gencode = '" & xreader("FromBranch") & "'")

                FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' and a.genother6='UMUM' AND a.genother2=(select genoid from ql_mstgen WHERE gencode='" & xreader("FromBranch") & "' and gengroup='cabang') AND a.cmpcode='" & cmpcode & "' AND b.gendesc NOT LIKE '%RETUR%' ORDER BY a.gendesc")
            End While
        Else
            showMessage("Missing transfer data >.<", 2)
        End If
        xreader.Close()

        sSql = "SELECT a.seq, a.refoid AS itemoid, b.itemdesc, b.merk, a.qty, a.unitoid AS satuan, b.satuan1, b. satuan1 satuan2, b.satuan1 satuan3, b.konversi1_2, b.konversi2_3, c.gendesc AS unit, d.gendesc AS unit1, d.gendesc AS unit2, d.gendesc AS unit3, a.trfdtlnote AS note FROM QL_matUsageDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_mstgen c ON a.cmpcode = c.cmpcode AND a.unitoid = c.genoid INNER JOIN QL_mstgen d ON b.cmpcode = d.cmpcode AND b.satuan1 = d.genoid AND d.gengroup='ITEMUNIT' WHERE a.cmpcode = '" & cmpcode & "' AND a.trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
        xCmd.CommandText = sSql
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "detailtw")
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        If trnstatus.Text = "POST" Or trnstatus.Text = "In Approval" Or trnstatus.Text = "Approved" Then
            btnSave.Visible = False
            btnDelete.Visible = False : BtnCancel.Visible = True
            btnPosting.Visible = False : btnApproval.Visible = False
        Else
            btnSave.Visible = True
            btnDelete.Visible = True : BtnCancel.Visible = True
            btnPosting.Visible = True : btnApproval.Visible = False
        End If
        conn.Close()
        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")
        tolocation.SelectedValue = tolocationoid
    End Sub

    Private Sub BindData(ByVal state As String)
        Dim date1, date2 As New Date : Dim sWhere As String = ""

        If CbTanggal.Checked = True Then
            date1 = Date.ParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing)
            date2 = Date.ParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing)
            sWhere &= "AND CONVERT(DATE, c.trfmtrdate, 101) BETWEEN '" & date1 & "' AND '" & date2 & "'"
        End If

        Dim searchitemoid As Integer = 0
        If itemoid.Text <> "" Then
            If Integer.TryParse(itemoid.Text, searchitemoid) = False Then
                showMessage("Item tidak valid !", 2)
                Exit Sub
            Else
                sWhere &= " AND a.refoid = " & Integer.Parse(itemoid.Text) & ""
            End If
        End If

        If fDDLCabang.SelectedValue <> "ALL" Then
            sWhere &= "AND FromBranch='" & fDDLCabang.SelectedValue & "'"
        End If

        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sWhere &= "AND c.upduser='" & Session("UserID") & "'"
        End If

        If FilterText.Text.Trim <> "" Then
            sWhere &= " AND c.transferno LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
        End If

        sSql = "SELECT DISTINCT c.trfmtrmstoid, c.transferno, c.trfmtrdate, c.upduser, c.status FROM QL_matUsageDtl a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' INNER JOIN QL_matUsageMst c ON a.cmpcode = c.cmpcode AND a.trfmtrmstoid = c.trfmtrmstoid WHERE a.cmpcode='MSC' " & sWhere & " ORDER BY c.trfmtrmstoid DESC"

        Dim dtab As DataTable = ckon.ambiltabel(sSql, "tw")
        gvMaster.DataSource = dtab
        gvMaster.DataBind()
        Session("tw") = dtab
    End Sub

    Private Sub BindItem()
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        Dim DescGen As String = ""
        If GrupCb.Checked = True Then
            DescGen = DDLGrupItem.SelectedItem.Text.Trim
        End If
        sSql = "Select * from (SELECT a.itemoid, a.itemcode, a.itemdesc, a.merk, Isnull((Select SUM(qtyIn)-SUM(qtyOut) From QL_conmtr con Where con.refoid=a.itemoid ANd periodacctg='" & GetDateToPeriodAcctg(transdate) & "' And mtrlocoid=" & fromlocation.SelectedValue & " And con.branch_code='" & ddlFromcabang.SelectedValue & "'),0.0000) as sisa, a.satuan1, b.gendesc AS unit1, a.satuan1 satuan2, b.gendesc AS unit2, a.satuan1 satuan3, b.gendesc AS unit3, a.konversi1_2, a.konversi1_2 konversi2_3 FROM QL_mstItem a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.satuan1 = b.genoid INNER JOIN QL_mstgen f ON a.cmpcode = f.cmpcode AND a.itemgroupoid = f.genoid AND f.gengroup='ITEMGROUP' WHERE a.itemflag = 'Aktif' AND (a.itemdesc LIKE '%" & TcharNoTrim(item.Text) & "%' OR a.itemcode LIKE '%" & TcharNoTrim(item.Text) & "%' OR a.merk LIKE '%" & TcharNoTrim(item.Text) & "%') AND a.cmpcode = '" & cmpcode & "' AND f.gendesc LIKE '%" & DescGen & "%' And a.stockflag ='I') it Where sisa>0.00"
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "itemlist")
        GVItemList.DataSource = objTable
        GVItemList.DataBind()
        Session("account") = objTable
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub initLoc()
        FillDDL(fromlocation, "SELECT a.genoid, b.gendesc + ' - ' + a.gendesc FROM QL_mstgen a INNER JOIN QL_mstgen b ON a.cmpcode = b.cmpcode AND a.genother1 = b.genoid AND b.genother1 = 'GROSIR' AND a.gengroup = 'LOCATION' and a.genother6='UMUM' AND a.genother2=(select genoid from ql_mstgen WHERE gencode='" & ddlFromcabang.SelectedValue & "' and gengroup='cabang') AND a.cmpcode='" & cmpcode & "' AND b.gendesc NOT LIKE '%RETUR%' ORDER BY a.gendesc")
        If fromlocation.Items.Count = 0 Then
            showMessage("Please create Location data!", 3)
            Exit Sub
        End If

        fromlocation.SelectedIndex = 0
        FillDDLAcctg(ddlCOA, "VAR_MATERIAL_USAGE", ddlFromcabang.SelectedValue)
        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")
        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If
        tolocation.SelectedIndex = 0
    End Sub

    Private Sub fCabangDdl()
        sSql = "select gencode,gendesc from ql_mstgen Where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(fDDLCabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(fDDLCabang, sSql)
            Else
                FillDDL(fDDLCabang, sSql)
                fDDLCabang.Items.Add(New ListItem("ALL", "ALL"))
                fDDLCabang.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(fDDLCabang, sSql)
            fDDLCabang.Items.Add(New ListItem("ALL", "ALL"))
            fDDLCabang.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub InitAllDDL()
        FillDDLGen("ITEMGROUP", Group)
        Group.Items.Add("ALL") : Group.SelectedValue = "ALL"
        FillDDLGen("ITEMSUBGROUP", subgroup)
        subgroup.Items.Add("ALL") : subgroup.SelectedValue = "ALL"
        'Init DDL Cabang
        FillDDL(ddlTocabang, "select gencode,gendesc from ql_mstgen where gengroup='cabang'")

        sSql = "select gencode,gendesc from ql_mstgen where gengroup='cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "'"
            FillDDL(ddlFromcabang, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "'"
                FillDDL(ddlFromcabang, sSql)
            Else
                FillDDL(ddlFromcabang, sSql)
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            FillDDL(ddlFromcabang, sSql)
        End If
    End Sub

    Private Sub ddlGroup(ByVal desc As String)
        sSql = "Select genoid,gendesc from QL_mstgen Where cmpcode='MSC' AND gengroup='ITEMGROUP' AND gendesc LIKE '%" & Tchar(txtFilter.Text.Trim) & "%'"
        FillDDL(DDLGrupItem, sSql)
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            'Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            'simpan session k variabel spy tidak hilang
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch

            'di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
            Response.Redirect("~\Transaction\trnMatUsage.aspx")
        End If

        Session("oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?')")
        btnApproval.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to send this data to approval?')")
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")
        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "edit"
        Else
            i_u.Text = "new"
        End If

        Page.Title = CompnyName & " - Material Usage"
        If Not Page.IsPostBack Then
            fCabangDdl() : BindData(0)
            transferno.Text = GenerateID("QL_matUsageMst", cmpcode)
            transferdate.Text = Format(Date.Now, "dd/MM/yyyy")
            InitAllDDL() : ddlGroup("") : initLoc()
            GrupCb_CheckedChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                TabContainer1.ActiveTabIndex = 1
                FillTextbox()
            Else
                Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
                Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
                tgl1.Text = Format(date1, "dd/MM/yyyy")
                tgl2.Text = Format(date2, "dd/MM/yyyy")

                upduser.Text = Session("UserID")
                updtime.Text = Format(GetServerTime(), "dd/MM/yyyy hh:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0 : qty.Text = "0.00"
                GVItemDetail.DataSource = Nothing
                GVItemDetail.DataBind()
                trnstatus.Text = "In Process" : labelseq.Text = "1"
                labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"
            End If
        End If
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        FilterText.Text = ""
        CbTanggal.Checked = False
        Dim date1 As Date = New Date(Date.Now.Year, Date.Now.Month, 1)
        Dim date2 As Date = New Date(Date.Now.Year, Date.Now.Month, Date.Now.Day)
        tgl1.Text = Format(date1, "dd/MM/yyyy")
        tgl2.Text = Format(date2, "dd/MM/yyyy")
        BindData(1)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If tgl1.Text.Trim = "" Or tgl2.Text.Trim = "" Then
            showMessage("Tanggal period harus diisi !", 2)
            Exit Sub
        End If

        Dim date1, date2 As New Date
        If Date.TryParseExact(tgl1.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date1) = False Then
            showMessage("Tanggal period 1 tidak valid !", 2)
            Exit Sub
        End If

        If Date.TryParseExact(tgl2.Text.Trim, "dd/MM/yyyy", Nothing, Nothing, date2) = False Then
            showMessage("Tanggal period 2 tidak valid !", 2)
            Exit Sub
        End If

        If date1 > date2 Then
            showMessage("Tanggal period 1 tidak boleh lebih dari period 2 !", 2)
            Exit Sub
        End If
        BindData(1)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ibitemsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemsearch.Click
        Try
            BindItem()
            GVItemList.Visible = True
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub ibitemdel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibitemdel.Click
        item.Text = "" : labelitemoid.Text = ""
        qty.Text = "0.00" : labelmaxqty.Text = "0.00"
        merk.Text = "" : notedtl.Text = ""
        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.Visible = False
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
        End If
    End Sub

    Protected Sub GVItemList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemList.PageIndexChanging
        Try
            Dim dtab As DataTable = Session("account")
            GVItemList.PageIndex = e.NewPageIndex
            GVItemList.DataSource = dtab
            GVItemList.DataBind()
            Session("account") = dtab
        Catch ex As Exception
            showMessage(ex.Message, 2)
        End Try
    End Sub

    Protected Sub GVItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemList.SelectedIndexChanged
        item.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(2).Text
        labelitemoid.Text = GVItemList.SelectedDataKey("itemoid")
        merk.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(3).Text
        qty.Text = ""
        'GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text
        labelmaxqty.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(4).Text

        Unit.Items.Clear()
        Unit.Items.Add(GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text)
        Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan3")
        If GVItemList.SelectedDataKey("satuan2") <> GVItemList.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemList.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan2")
        End If
        If GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan3") And GVItemList.SelectedDataKey("satuan1") <> GVItemList.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemList.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemList.SelectedDataKey("satuan1")
        End If
        Unit.SelectedValue = GVItemList.SelectedDataKey("satuan3")
        labelsatuan1.Text = GVItemList.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemList.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemList.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemList.SelectedDataKey("unit1").ToString
        labelunit2.Text = GVItemList.SelectedDataKey("unit2").ToString
        labelunit3.Text = GVItemList.Rows(GVItemList.SelectedIndex).Cells(5).Text

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelkonversi1_2.Text = GVItemList.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemList.SelectedDataKey("konversi2_3")

        GVItemList.Visible = False : GVItemList.DataSource = Nothing
        GVItemList.DataBind()
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Transaction\trnMatUsage.aspx?awal=true")
    End Sub

    Protected Sub qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles qty.TextChanged
        Dim qtyval As Double = 0.0
        If qty.Text.Trim <> "" Then
            If Double.TryParse(qty.Text.Trim, qtyval) Then
                qty.Text = Format(qtyval, "#,##0.00")
            Else
                qty.Text = Format(0.0, "#,##0.00")
            End If
        Else
            qty.Text = Format(0.0, "#,##0.00")
        End If
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If item.Text.Trim = "" Or labelitemoid.Text = "" Then
            showMessage("Item tidak boleh kosong!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) = 0.0 Then
            showMessage("Qty harus lebih dari 0!", 2)
            Exit Sub
        End If
        If Double.Parse(qty.Text.Trim) > Double.Parse(labelmaxqty.Text.Trim) Then
            showMessage("Qty tidak boleh lebih dari maksimum qty!", 2)
            Exit Sub
        End If

        Dim dtab As DataTable

        If I_u2.Text = "new" Then
            If Session("itemdetail") Is Nothing Then
                dtab = New DataTable
                dtab.Columns.Add("seq", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemoid", Type.GetType("System.Int32"))
                dtab.Columns.Add("itemdesc", Type.GetType("System.String"))
                dtab.Columns.Add("merk", Type.GetType("System.String"))
                dtab.Columns.Add("qty", Type.GetType("System.Double"))
                dtab.Columns.Add("satuan", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan1", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan2", Type.GetType("System.Int32"))
                dtab.Columns.Add("satuan3", Type.GetType("System.Int32"))
                dtab.Columns.Add("konversi1_2", Type.GetType("System.Double"))
                dtab.Columns.Add("konversi2_3", Type.GetType("System.Double"))
                dtab.Columns.Add("unit", Type.GetType("System.String"))
                dtab.Columns.Add("unit1", Type.GetType("System.String"))
                dtab.Columns.Add("unit2", Type.GetType("System.String"))
                dtab.Columns.Add("unit3", Type.GetType("System.String"))
                dtab.Columns.Add("note", Type.GetType("System.String"))
                Session("itemdetail") = dtab
                labelseq.Text = "1"
            Else
                dtab = Session("itemdetail")
                labelseq.Text = (dtab.Rows.Count + 1).ToString
            End If
        Else
            dtab = Session("itemdetail")
        End If

        If dtab.Rows.Count > 0 Then
            Dim drowc() As DataRow = dtab.Select("itemoid = " & Integer.Parse(labelitemoid.Text) & " AND merk = '" & merk.Text & "' AND seq <> " & Integer.Parse(labelseq.Text) & "")
            If drowc.Length > 0 Then
                showMessage("Item tidak bisa ditambahkan ke dalam list ! Item ini telah ada di dalam list !", 2)
                Exit Sub
            End If
        End If

        Dim drow As DataRow
        Dim drowedit() As DataRow
        If I_u2.Text = "new" Then
            drow = dtab.NewRow

            drow("seq") = Integer.Parse(labelseq.Text)
            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim

            dtab.Rows.Add(drow)
            dtab.AcceptChanges()
        Else
            drowedit = dtab.Select("seq = " & Integer.Parse(labelseq.Text) & "", "")
            drow = drowedit(0)
            drowedit(0).BeginEdit()

            drow("itemoid") = Integer.Parse(labelitemoid.Text)
            drow("itemdesc") = item.Text
            drow("merk") = merk.Text
            drow("qty") = Double.Parse(qty.Text)
            drow("satuan") = Unit.SelectedValue
            drow("unit") = Unit.SelectedItem.Text

            drow("satuan1") = Integer.Parse(labelsatuan1.Text)
            drow("satuan2") = Integer.Parse(labelsatuan2.Text)
            drow("satuan3") = Integer.Parse(labelsatuan3.Text)
            drow("unit1") = labelunit1.Text
            drow("unit2") = labelunit2.Text
            drow("unit3") = labelunit3.Text

            drow("konversi1_2") = Double.Parse(labelkonversi1_2.Text)
            drow("konversi2_3") = Double.Parse(labelkonversi2_3.Text)
            drow("note") = notedtl.Text.Trim

            drowedit(0).EndEdit()
            dtab.Select(Nothing, Nothing)
            dtab.AcceptChanges()
        End If

        GVItemDetail.DataSource = dtab : GVItemDetail.DataBind()
        Session("itemdetail") = dtab

        labelseq.Text = (GVItemDetail.Rows.Count + 1).ToString
        merk.Text = "" : qty.Text = "0.00"
        labelmaxqty.Text = "0.00" : Unit.Items.Clear()
        notedtl.Text = "" : labelitemoid.Text = ""
        item.Text = "" : I_u2.Text = "new"

        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        GVItemDetail.SelectedIndex = -1 : GVItemDetail.Columns(7).Visible = True

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        I_u2.Text = "new" : labelitemoid.Text = ""
        qty.Text = "0.00" : item.Text = ""
        labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = "" : merk.Text = ""

        If Session("itemdetail") Is Nothing = False Then
            Dim objTable As DataTable = Session("itemdetail")
            labelseq.Text = objTable.Rows.Count + 1
        Else
            labelseq.Text = 1
        End If

        labelsatuan1.Text = "" : labelsatuan2.Text = ""
        labelsatuan3.Text = "" : labelunit1.Text = ""
        labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1"

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If

        GVItemDetail.SelectedIndex = -1
        GVItemDetail.Columns(7).Visible = True
    End Sub

    Protected Sub lbdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbutton As System.Web.UI.WebControls.LinkButton
        Dim gvr As GridViewRow

        lbutton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
        gvr = TryCast(lbutton.NamingContainer, GridViewRow)

        Dim dtab As DataTable
        If Not Session("itemdetail") Is Nothing Then
            dtab = Session("itemdetail")
        Else
            showMessage("Missing detail list session !", 2)
            Exit Sub
        End If

        Dim drow() As DataRow = dtab.Select("seq = " & Integer.Parse(gvr.Cells(1).Text) & "", "")
        drow(0).Delete()
        dtab.Select(Nothing, Nothing)
        dtab.AcceptChanges()

        Dim dtrow As DataRow
        For i As Integer = 0 To dtab.Rows.Count - 1
            dtrow = dtab.Rows(i)
            dtrow.BeginEdit()
            dtrow("seq") = i + 1
            dtrow.EndEdit()
        Next
        dtab.AcceptChanges()
        GVItemDetail.DataSource = dtab
        GVItemDetail.DataBind()
        Session("itemdetail") = dtab
    End Sub

    Protected Sub GVItemDetail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemDetail.SelectedIndexChanged
        I_u2.Text = "edit"
        labelitemoid.Text = GVItemDetail.SelectedDataKey("itemoid")
        item.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(2).Text
        qty.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(4).Text

        notedtl.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(6).Text.Replace("&nbsp;", "")
        merk.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(3).Text

        Unit.Items.Clear()
        Unit.Items.Add(GVItemDetail.SelectedDataKey("unit3"))
        Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan3")
        If GVItemDetail.SelectedDataKey("satuan2") <> GVItemDetail.SelectedDataKey("satuan3") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit2"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan2")
        End If
        If GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan3") And GVItemDetail.SelectedDataKey("satuan1") <> GVItemDetail.SelectedDataKey("satuan2") Then
            Unit.Items.Add(GVItemDetail.SelectedDataKey("unit1"))
            Unit.Items(Unit.Items.Count - 1).Value = GVItemDetail.SelectedDataKey("satuan1")
        End If
        Unit.SelectedValue = GVItemDetail.SelectedDataKey("satuan")
        labelsatuan1.Text = GVItemDetail.SelectedDataKey("satuan1")
        labelsatuan2.Text = GVItemDetail.SelectedDataKey("satuan2")
        labelsatuan3.Text = GVItemDetail.SelectedDataKey("satuan3")
        labelunit1.Text = GVItemDetail.SelectedDataKey("unit1")
        labelunit2.Text = GVItemDetail.SelectedDataKey("unit2")
        labelunit3.Text = GVItemDetail.SelectedDataKey("unit3")
        labelkonversi1_2.Text = GVItemDetail.SelectedDataKey("konversi1_2")
        labelkonversi2_3.Text = GVItemDetail.SelectedDataKey("konversi2_3")
        labelseq.Text = GVItemDetail.Rows(GVItemDetail.SelectedIndex).Cells(1).Text

        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()
        sSql = "SELECT (saldoakhir-qtybooking) from QL_crdmtr WHERE refname = 'QL_MSTITEM' AND refoid = " & Integer.Parse(labelitemoid.Text) & " AND periodacctg = '" & GetDateToPeriodAcctg(Date.Now).Trim & "' AND cmpcode = '" & cmpcode & "'"
        xCmd.CommandText = sSql
        Dim maxqty As Double = xCmd.ExecuteScalar
        If Not maxqty = Nothing Then
            If GVItemDetail.SelectedDataKey("satuan") = GVItemDetail.SelectedDataKey("satuan1") Then
                maxqty = maxqty / GVItemDetail.SelectedDataKey("konversi1_2")
            End If
            labelmaxqty.Text = Format(maxqty, "#,##0.00")
        Else
            labelmaxqty.Text = "0.00"
        End If
        conn.Close()

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
        GVItemDetail.Columns(7).Visible = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errmsg As String = ""

        If fromlocation.Items.Count <= 0 Then
            errmsg &= "Lokasi asal transfer tidak boleh kosong !"
        End If
        If tolocation.Items.Count <= 0 Then
            errmsg &= "Lokasi tujuan transfer tidak boleh kosong !"
        End If
        If GVItemDetail.Rows.Count <= 0 Then
            errmsg &= "Detail transfer tidak boleh kosong !"
        End If

        If errmsg.Trim <> "" Then
            showMessage(errmsg, 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
            Dim period As String = GetDateToPeriodAcctg(GetServerTime()).Trim

            'Cek period is open stock
            sSql = "SELECT COUNT(-1) FROM ql_crdmtr WHERE cmpcode = '" & cmpcode & "' AND periodacctg = '" & period & "' AND closingdate = '1/1/1900'"
            xCmd.CommandText = sSql
            If xCmd.ExecuteScalar = 0 Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Tanggal ini tidak dalam periode Open Stock !", 2)
                trnstatus.Text = "In Process"
                Exit Sub
            End If

            'Cek saldoakhir
            If Not Session("itemdetail") Is Nothing Then
                Dim saldoakhire As Double = 0.0
                Dim dtab As DataTable = Session("itemdetail")
                For j As Integer = 0 To dtab.Rows.Count - 1
                    sSql = "Select Isnull(SUM(qtyIn)-SUM(qtyOut),0.00) From QL_conmtr Where refoid=" & dtab.Rows(j).Item("itemoid") & " AND branch_code='" & ddlFromcabang.SelectedValue & "' AND mtrlocoid=" & fromlocation.SelectedValue & " AND periodacctg='" & period & "'"
                    xCmd.CommandText = sSql : saldoakhire = xCmd.ExecuteScalar
                    If saldoakhire = Nothing Or saldoakhire = 0 Then
                        objTrans.Rollback()
                        conn.Close()
                        showMessage("Material Usage tidak bisa dilakukan untuk barang '" & dtab.Rows(j).Item("itemdesc") & "' karena saldo akhir satuan std hanya tersedia 0.00 !", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub 
                    End If
                Next
            End If

            'Generate crdmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_conmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim conmtroid As Int32 = xCmd.ExecuteScalar + 1

            'Generate conmtr ID
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_crdmtr' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim crdmtroid As Int32 = xCmd.ExecuteScalar + 1
            Dim tempoid As Int32 = crdmtroid

            Dim trfmtrmstoid As Integer = 0
            If i_u.Text = "new" Then
                'Generate transfer master ID
                sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_matUsageMst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                trfmtrmstoid = xCmd.ExecuteScalar + 1
            End If

            'Generate transfer number Posting
            If trnstatus.Text = "POST" Then
                Dim iCurID As Integer = 0
                Dim cabang As String = GetStrData("select genother1 from ql_mstgen Where gencode='" & ddlFromcabang.SelectedValue & "' And gengroup='CABANG'")
                Dim trnno As String = "MU/" & cabang & "/" & Format(GetServerTime(), "yy/MM/dd") & "/"

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transferno, 4) AS INT)), 0) FROM QL_matUsageMst WHERE cmpcode = '" & cmpcode & "' AND transferno LIKE '" & trnno & "%'"

                xCmd.CommandText = sSql : iCurID = xCmd.ExecuteScalar + 1
                trnno = GenNumberString(trnno, "", iCurID, 4)
                transferno.Text = trnno

            Else
                If i_u.Text = "new" Then
                    transferno.Text = trfmtrmstoid.ToString
                End If
            End If

            'Generate transfer detail ID
            Dim lasthpp As Double = 0
            sSql = "SELECT ISNULL(lastoid,0) FROM QL_mstoid WHERE tablename = 'QL_matUsageDtl' AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql
            Dim trfmtrdtloid As Int32 = xCmd.ExecuteScalar + 1
            Dim iglmst As Int64 = GenerateID("QL_trnglmst", cmpcode)
            Dim igldtl As Int64 = GenerateID("QL_trngldtl", cmpcode)
            Dim iGlSeq As Int16 = 1
            Dim COA_adjust_stock_minus As Integer = ddlCOA.SelectedValue

            Dim dt As DataTable = Session("itemdetail")
            Dim COA_gudang As Integer = 0, vargudang As String = "", iMatAccount As String = "", itemdesc As String = ""

            If trnstatus.Text = "POST" Then
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, upduser, updtime, createuser, createtime, branch_code,type) VALUES" & _
                " ('" & cmpcode & "', " & iglmst & ", CURRENT_TIMESTAMP, '" & period & "', 'MU', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP,'" & ddlFromcabang.SelectedValue & "','MU')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid = " & iglmst & " WHERE tablename = 'QL_trnglmst' AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            'Insert new record
            If i_u.Text = "new" Then
                transferno.Text = trfmtrmstoid.ToString
                'Insert to master
                sSql = "INSERT INTO QL_matUsageMst (cmpcode, frombranch, tobranch, trfmtrmstoid, transferno, trfmtrdate, trfmtrnote, noref, status, frommtrlocoid, tomtrlocoid, updtime, upduser, typeTransfer, COAusage) VALUES " & _
                "('" & cmpcode & "', '" & ddlFromcabang.SelectedValue & "', '" & ddlFromcabang.SelectedValue & "', " & trfmtrmstoid & ", '" & transferno.Text & "', (Select CAST(CONVERT(varchar(10),Current_timestamp,112) AS datetime)), '" & Tchar(note.Text.Trim) & "', '" & Tchar(noref.Text.Trim) & "', '" & trnstatus.Text & "', " & fromlocation.SelectedValue & ", " & tolocation.SelectedValue & ", CURRENT_TIMESTAMP, '" & upduser.Text & "', 'TW', " & ddlCOA.SelectedValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update lastoid QL_matUsageMst
                sSql = "UPDATE QL_mstoid SET lastoid = " & trfmtrmstoid & " WHERE tablename = 'QL_matUsageMst' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                ' Validasi cek status transaksi utk multiuser/multitab/multibrowser
                sSql = "SELECT status FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql
                Dim srest As String = xCmd.ExecuteScalar
                If srest Is Nothing Or srest = "" Then
                    objTrans.Rollback() : conn.Close()
                    showMessage("Data Material Usage tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                    trnstatus.Text = "In Process" : Exit Sub
                Else
                    If srest.ToLower = "in approval" Or srest.ToLower = "approved" Or srest.ToLower = "post" Then
                        objTrans.Rollback() : conn.Close()
                        showMessage("Status transaksi Material Usage sudah terupdate!<br />Tekan Cancel dan periksa ulang bila data telah dalam status '" & srest & "'!", 2)
                        trnstatus.Text = "In Process"
                        Exit Sub
                    End If 
                End If

                'Update master record
                sSql = "UPDATE QL_matUsageMst SET transferno = '" & transferno.Text & "', trfmtrnote = '" & Tchar(note.Text.Trim) & "', frommtrlocoid = " & fromlocation.SelectedValue & ", tomtrlocoid = " & tolocation.SelectedValue & ", noref = '" & Tchar(noref.Text.Trim) & "', status = '" & trnstatus.Text & "', updtime = CURRENT_TIMESTAMP, upduser = '" & Session("UserID") & "' , COAusage = " & ddlCOA.SelectedValue & " WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Delete all detail
                sSql = "DELETE FROM QL_matUsageDtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            End If

            'Insert to detail
            If Not Session("itemdetail") Is Nothing Then
                Dim objTable As DataTable = Session("itemdetail")
                Dim unitseq As Integer = 0 : Dim qty_to As Double = 0.0
                Dim xBRanch As String = ""
                For i As Integer = 0 To objTable.Rows.Count - 1
                    Dim sTockFlag As String = GetStrData("Select stockflag From ql_mstitem Where itemoid= " & objTable.Rows(i)("itemoid").ToString & "")
                    xBRanch = GetStrData("Select genother3 From QL_mstgen WHERE gengroup='COA' AND genother1=" & fromlocation.SelectedValue & " AND genother4='" & sTockFlag & "'")
                    If sTockFlag = "I" Then
                        iMatAccount = GetVarInterface("VAR_INVENTORY", xBRanch)
                        If iMatAccount = "?" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_INVENTORY' !!", CompnyName & " - WARNING")
                            Session("click_post") = "false"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    Else
                        iMatAccount = GetVarInterface("VAR_GUDANG", xBRanch)
                        If iMatAccount = "?" Then
                            showMessage("Please Setup Interface for Account Receivable on Variable 'VAR_GUDANG' !!", CompnyName & " - WARNING")
                            Session("click_post") = "false"
                            objTrans.Rollback() : conn.Close()
                            Exit Sub
                        End If
                    End If

                    If iMatAccount Is Nothing Or iMatAccount = "" Then
                        showMessage("Interface untuk Akun VAR_GUDANG tidak ditemukan!", 2)
                        objTrans.Rollback() : conn.Close()
                        Exit Sub
                    Else
                        sSql = "SELECT ACCTGOID FROM QL_MSTACCTG WHERE CMPCODE = '" & cmpcode & "' AND ACCTGCODE = '" & iMatAccount & "'"
                        xCmd.CommandText = sSql : COA_gudang = xCmd.ExecuteScalar
                        If COA_gudang = 0 Or COA_gudang = Nothing Then
                            showMessage("Akun COA untuk VAR_GUDANG tidak ditemukan!", 2)
                            objTrans.Rollback()
                            conn.Close()
                            Exit Sub
                        End If
                    End If

                    If i_u.Text = "edit" Then
                        trfmtrmstoid = Integer.Parse(Session("oid"))
                    End If 

                    sSql = "INSERT INTO QL_matUsageDtl (cmpcode, trfmtrdtloid, trfmtrmstoid, seq, refoid, refname, unitseq, qty, unitoid, trfdtlnote, unitseq_to, qty_to, unitoid_to, lasthpp) " & _
                    "VALUES ('" & cmpcode & "', " & trfmtrdtloid & ", " & trfmtrmstoid & ", " & i + 1 & ", " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & unitseq & ", " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan") & ", '" & Tchar(objTable.Rows(i).Item("note")) & "', 3, " & objTable.Rows(i).Item("qty") & ", " & objTable.Rows(i).Item("satuan3") & ", (Select hpp From ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & "))"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    If trnstatus.Text = "POST" Then
                        'Update crdmtr for item out                         
                        sSql = "INSERT INTO QL_conmtr (cmpcode,branch_code, conmtroid, type, trndate, periodacctg, FormAction, formoid, Formname, refoid, refname, unitoid, mtrlocoid, qtyIn, qtyOut, reason, upduser, updtime, typeMin, personoid, amount, note, HPP) VALUES" & _
                        " ('" & cmpcode & "','" & ddlFromcabang.SelectedValue & "', " & conmtroid & ", 'MU', '" & GetServerTime() & "', '" & period & "', '" & transferno.Text & "', " & trfmtrdtloid & ", 'QL_matUsageDtl', " & objTable.Rows(i).Item("itemoid") & ", 'QL_MSTITEM', " & objTable.Rows(i).Item("satuan") & ", " & fromlocation.SelectedValue & ", 0, " & objTable.Rows(i).Item("qty") & ", '', '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, 0, '" & ToDouble(lasthpp) * ToDouble(objTable.Rows(i).Item("qty")) & "', '" & Tchar(objTable.Rows(i).Item("note")) & "', (Select hpp From ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & "))"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        conmtroid += 1
                        'update stock inventory(-)( biaya(D)-persediaan(C))
                        '--------------------------
                        sSql = "Select hpp From ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & ""
                        xCmd.CommandText = sSql : lasthpp = xCmd.ExecuteScalar

                        sSql = "select itemdesc from ql_mstitem where itemoid=" & objTable.Rows(i).Item("itemoid") & " "
                        xCmd.CommandText = sSql : itemdesc = xCmd.ExecuteScalar
                        '--------------------------
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, glamtIDR, branch_code)" & _
                        " VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_adjust_stock_minus & ", 'D', " & ToDouble(objTable.Rows(i).Item("qty")) * ToDouble(lasthpp) & ", '" & transferno.Text & " ', 'MATERIAL USAGE - " & itemdesc & "', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(i).Item("qty")) * ToDouble(lasthpp) & ", '" & ddlFromcabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glother1, glother2, glflag, glpostdate, upduser, updtime, glamtIDR, branch_code)" & _
                        " VALUES ('" & cmpcode & "', " & igldtl & ", " & iGlSeq & ", " & iglmst & ", " & COA_gudang & ", 'C', " & ToDouble(objTable.Rows(i).Item("qty")) * ToDouble(lasthpp) & ", '" & transferno.Text & " ', 'MATERIAL USAGE - " & itemdesc & "', '', '', 'POST', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP," & ToDouble(objTable.Rows(i).Item("qty")) * ToDouble(lasthpp) & ", '" & ddlFromcabang.SelectedValue & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        igldtl += 1 : iGlSeq += 1
                    End If
                    trfmtrdtloid += 1
                Next

                'Update lastoid QL_matUsageDtl
                sSql = "UPDATE QL_mstoid SET lastoid =" & trfmtrdtloid - 1 & "  WHERE tablename = 'QL_matUsageDtl' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() 

                'Update lastoid QL_conmtr
                sSql = "UPDATE QL_mstoid SET lastoid =" & conmtroid - 1 & "  WHERE tablename = 'QL_conmtr' and cmpcode = '" & cmpcode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid = " & igldtl - 1 & " where tablename = 'QL_trngldtl' and cmpcode = '" & cmpcode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & "<br />" & sSql, 1)
            trnstatus.Text = "In Process"
            Exit Sub
        End Try
        Response.Redirect("trnMatUsage.aspx?awal=true")
    End Sub

    Protected Sub unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unit.SelectedIndexChanged
        Dim transdate As Date = Date.ParseExact(transferdate.Text, "dd/MM/yyyy", Nothing)
        conn.Open()

        sSql = "SELECT (a.saldoakhir-a.qtybooking) saldoakhir, b.satuan1, b.satuan2, b.satuan3, b.konversi1_2, b.konversi2_3 from QL_crdmtr a INNER JOIN QL_mstitem b ON a.cmpcode = b.cmpcode AND a.refoid = b.itemoid AND a.refname = 'QL_MSTITEM' WHERE a.refoid = " & Integer.Parse(labelitemoid.Text) & " AND a.periodacctg = '" & GetDateToPeriodAcctg(transdate).Trim & "' AND a.mtrlocoid = " & fromlocation.SelectedValue & " AND a.cmpcode = '" & cmpcode & "'"
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        Dim maxqty As Double = 0.0
        If xreader.HasRows Then
            While xreader.Read
                If Unit.SelectedValue = xreader("satuan2") Then
                    maxqty = xreader("saldoakhir") / xreader("konversi2_3")
                ElseIf Unit.SelectedValue = xreader("satuan1") Then
                    maxqty = xreader("saldoakhir") / xreader("konversi1_2") / xreader("konversi2_3")
                End If
            End While
        End If
        labelmaxqty.Text = Format(maxqty, "#,##0.00")
        conn.Close()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            sSql = "SELECT status FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & " AND cmpcode = '" & cmpcode & "'"
            xCmd.CommandText = sSql : Dim srest As String = xCmd.ExecuteScalar
            If srest Is Nothing Or srest = "" Then
                objTrans.Rollback()
                conn.Close()
                showMessage("Data transfer warehouse tidak ditemukan !<br />Periksa bila data telah dihapus oleh user lain", 2)
                Exit Sub
            Else
                If srest = "Approved" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Periksa bila data telah berstatus 'Approved'!", 2)
                    Exit Sub
                ElseIf srest = "In Approval" Then
                    objTrans.Rollback()
                    conn.Close()
                    showMessage("Data transfer warehouse tidak dapat dihapus !<br />Data sedang berada dalam status 'In Approval'!", 2)
                    Exit Sub
                End If
            End If

            sSql = "DELETE FROM QL_matUsageMst WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "DELETE FROM QL_matUsageDtl WHERE trfmtrmstoid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_approval where tablename = 'QL_matUsageMst' and oid = " & Integer.Parse(Session("oid")) & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnMatUsage.aspx?awal=true")
    End Sub

    Protected Sub BtnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        trnstatus.Text = "POST"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub fromlocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fromlocation.SelectedIndexChanged
        'locasi kedua dibuat Nol
        FillDDL(tolocation, "SELECT 0,'Material Usage'")
        If tolocation.Items.Count = 0 Then
            showMessage("Please create Location (minimal 2 location) !! ", 3)
            Exit Sub
        End If

        Session("itemdetail") = Nothing : GVItemDetail.DataSource = Nothing
        GVItemDetail.DataBind()

        I_u2.Text = "new" : labelitemoid.Text = ""
        qty.Text = "0.00" : item.Text = "" : labelmaxqty.Text = "0.00"
        Unit.Items.Clear()
        notedtl.Text = "" : merk.Text = ""
        labelsatuan1.Text = "" : labelsatuan2.Text = "" : labelsatuan3.Text = ""
        labelunit1.Text = "" : labelunit2.Text = "" : labelunit3.Text = ""
        labelkonversi1_2.Text = "1" : labelkonversi2_3.Text = "1" : labelseq.Text = 1

        If GVItemList.Visible = True Then
            GVItemList.DataSource = Nothing
            GVItemList.DataBind()
            GVItemList.Visible = False
        End If
    End Sub

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.DataBind()
        GVItemSearch.Visible = True
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        Filteritem.Text = "" : itemoid.Text = ""
        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()
        GVItemSearch.Visible = False
    End Sub

    Protected Sub GVItemSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVItemSearch.PageIndexChanging
        sSql = "SELECT itemoid, itemcode, itemdesc FROM QL_mstitem WHERE cmpcode = '" & cmpcode & "' AND (itemcode LIKE '%" & Tchar(Filteritem.Text.Trim) & "%' OR itemdesc LIKE '%" & Tchar(Filteritem.Text.Trim) & "%') AND itemflag = 'Aktif'"
        Dim dtab As DataTable = ckon.ambiltabel(sSql, "mstitem")
        GVItemSearch.DataSource = dtab
        GVItemSearch.PageIndex = e.NewPageIndex
        GVItemSearch.DataBind()
    End Sub

    Protected Sub GVItemSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVItemSearch.SelectedIndexChanged
        Filteritem.Text = GVItemSearch.Rows(GVItemSearch.SelectedIndex).Cells(2).Text
        itemoid.Text = GVItemSearch.SelectedDataKey("itemoid")
        GVItemSearch.DataSource = Nothing
        GVItemSearch.DataBind()
        GVItemSearch.Visible = False
    End Sub

    Protected Sub lbprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbutton As System.Web.UI.WebControls.LinkButton = TryCast(sender, System.Web.UI.WebControls.LinkButton)
            Dim gvrow As GridViewRow = TryCast(lbutton.NamingContainer, GridViewRow)
            Dim labeloid As System.Web.UI.WebControls.Label = gvrow.FindControl("labeloid")
            Dim sWhere As String = " " & Integer.Parse(labeloid.Text) & " "
            report = New ReportDocument
            report.Load(Server.MapPath(folderReport & "MUprintout.rpt"))
            CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            report.SetParameterValue("sWhere", sWhere)
            'report.PrintOptions.PaperSize = PaperSize.PaperA5
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "TW_" & labeloid.Text & "")
            report.Close() : report.Dispose()
            Response.Redirect("trnMatUsage.aspx?awal=true")
        Catch ex As Exception
            showMessage(ex.Message.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub gvMaster_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMaster.PageIndexChanging
        If Not Session("tw") Is Nothing Then
            Dim dtab As DataTable = Session("tw")
            gvMaster.DataSource = dtab
            gvMaster.PageIndex = e.NewPageIndex
            gvMaster.DataBind()
        Else
            showMessage("Missing something !", 2)
            Exit Sub
        End If
    End Sub

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApproval.Click
        If CheckDateIsClosedMtr(toDate(transferdate.Text), cmpcode) Then
            showMessage("This Periode was closed !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        If CheckDateIsPeriodMtr(toDate(transferdate.Text), cmpcode) = False Then
            showMessage("This is not active periode !", 2)
            trnstatus.Text = "In Process"
            Exit Sub
        End If

        trnstatus.Text = "In Approval"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub txtFilter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If txtFilter.Text.Trim <> "" Then
            DDLGrupItem.SelectedItem.Text = txtFilter.Text.Trim
        Else
            ddlGroup(txtFilter.Text.Trim)
        End If
    End Sub

    Protected Sub GrupCb_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If GrupCb.Checked = False Then
            txtFilter.Text = "" : ddlGroup("")
            txtFilter.Enabled = False : txtFilter.CssClass = "inpTextDisabled"
            DDLGrupItem.Enabled = False : DDLGrupItem.CssClass = "inpTextDisabled"
        Else
            txtFilter.Enabled = True : txtFilter.CssClass = "inpText"
            DDLGrupItem.Enabled = True : DDLGrupItem.CssClass = "inpText"
        End If
    End Sub

    Protected Sub ddlFromcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromcabang.SelectedIndexChanged
        initLoc()
    End Sub

    Protected Sub tgl1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tgl1.TextChanged
        CbTanggal.Checked = True
    End Sub
#End Region
End Class
