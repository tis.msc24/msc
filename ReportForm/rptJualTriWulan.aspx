<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptJualTriWulan.aspx.vb" Inherits="rptJualTriWulan" title="MSC - Penjualan Tri Wulan" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan Penjualan Tri Wulan"></asp:Label>
            </th>
        </tr>

        <tr>
            <th align="center" class="header" style="width: 100%; background-color: transparent"
                valign="center">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="3">
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                 <contenttemplate>
                                    <TABLE>
                                        <TBODY>
                                            <TR>
                                                <TD align=left runat="server" Visible="true">
                                                    <asp:Label ID="lblbln1" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblbln2" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblbln3" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblbln4" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label id="lblCabang" runat="server" Text="Cabang"></asp:Label>
                                                </TD>
                                                <TD align=left colSpan=3 runat="server" Visible="true">
                                                    <asp:DropDownList id="dCabangNya" runat="server" CssClass="inpText"></asp:DropDownList>
                                                </TD>
                                            </TR>
                                            
                                            <TR>
                                                <TD id="tdPeriod1" align=left runat="server" Visible="true">
                                                    <asp:Label id="Label6" runat="server" Text="Periode"></asp:Label>
                                                </TD>

                                                <TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"> 
                                                    <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText"/>&nbsp;
                                                    <asp:ImageButton id="ImgDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"/>&nbsp;
                                                    <asp:Label id="Label4" runat="server" ForeColor="Red">(dd/mm/yyyy)</asp:Label>
                                                </TD>
                                            </TR> 
                                              
                                            <TR>
                                                <TD id="td7" align=left runat="server" Visible="true">
                                                    <asp:Label id="Label7" runat="server" Text="Katalog"/>
                                                    <asp:Label id="TxtItemOid" runat="server" Text="0" Visible="false"/>
                                                </TD>
                                                <TD id="td8" align=left colSpan=3 runat="server" Visible="true">
                                                    <asp:TextBox id="TxtKatalog" runat="server" Width="130px" TextMode="MultiLine" Height="40px" Enabled="false" CssClass="inpTextDisabled"/>&nbsp;
                                                    <asp:ImageButton ID="BtnItem" runat="server" Width="30px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="30px" />
                                                    <asp:ImageButton id="BtnEraseItem" runat="server" Width="30px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="30px"/>
                                                </TD>
                                            </TR>                                                                                       

                                            <TR>
                                                <TD align=left colSpan=4>
                                                    <asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"/>
                                                    <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"/>
                                                </TD>
                                            </TR>

                                            <TR>
                                                <TD align=center colSpan=4>
                                                    <asp:UpdateProgress id="uProgReportForm" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                        <ProgressTemplate>
                                                        <DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV>
                                                        <DIV id="processMessage" class="processMessage">
                                                            <SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">
                                                                <asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom"></asp:Image>
                                                                <BR />Please Wait .....!
                                                            </SPAN>
                                                            <BR />
                                                        </DIV>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress> 
                                                  
                                                    <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" MaskType="Date" Mask="99/99/9999" TargetControlID="dateAkhir"></ajaxToolkit:MaskedEditExtender> 
                                                   
                                                    <ajaxToolkit:CalendarExtender id="CETdateAkhir" runat="server" TargetControlID="dateAkhir" Format="dd/MM/yyyy" PopupButtonID="ImgDate2"></ajaxToolkit:CalendarExtender>
                                                </TD>
                                            </TR>
                                        </TBODY>
                                    </TABLE>
                                     
                                    <CR:CrystalReportViewer id="CrvTriWulan" runat="server" Width="350px" Height="50px" ShowAllPageIds="True" AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" HasExportButton="False" HasPrintButton="False" HasRefreshButton="True" HasSearchButton="False" HasToggleGroupTreeButton="False">
                                    </CR:CrystalReportViewer> 
                                     
                                </contenttemplate>
                                <triggers>
                                    <asp:PostBackTrigger ControlID="CrvTriWulan"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
                                    <asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
                                </triggers>
                            </asp:UpdatePanel> 
                                                        
                            <!--Start Pop Up Data Katalog -->    
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Panel id="PanelItem" runat="server" Width="600px" CssClass="modalBox" Visible="False">
                                <TABLE style="WIDTH: 100%">
                                    <TBODY>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                <asp:Label id="LblItem" runat="server" Font-Size="Large" Font-Bold="True" Text="List of Katalog"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                                   <asp:Panel id="Panel4" runat="server" Width="100%" DefaultButton="BtnFindItem">
                                                    <asp:Label id="Label10" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> 
                                                    <asp:DropDownList id="DDLFilterItem" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt">
                                                        <asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
                                                        <asp:ListItem Value="itemcode">Kode</asp:ListItem>
                                                        </asp:DropDownList> 
                                                    <asp:TextBox id="TxtFilterItem" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"/>
                                                    <asp:ImageButton id="BtnFindItem" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"/> 
                                                    <asp:ImageButton id="BtnViewAllItem" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"/>                                                     
                                                </asp:Panel> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD style="HEIGHT: 5px" class="Label" align=center colSpan=3>
                                                <asp:ImageButton id="BtnCheckAllItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"/>&nbsp;
                                                <asp:ImageButton id="BtnCheckNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle" />&nbsp;
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="Label" align=center colSpan=3>
                                            <div style="overflow-y: scroll; width: 100%; height: 250px; background-color: beige">
                                            <asp:GridView id="gvItem" runat="server" Width="98%" ForeColor="#333333" GridLines="None" CellPadding="4" DataKeyNames="itemoid,itemcode" AutoGenerateColumns="False" AllowPaging="True" PageSize="5">
                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
                                                    <Columns>
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbItem" runat="server" Checked='<%#Eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>'/>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Center"/>
                                                        </asp:TemplateField> 

                                                        <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField DataField="Itemdesc" HeaderText="Katalog">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="8pt"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>
                            
                                                        <asp:BoundField DataField="Unit" HeaderText="Unit">
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        </asp:BoundField>                                               
                                                   </Columns>

                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                    <PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"/>
                                                    <PagerSettings Visible="false" />
                                                    <EmptyDataTemplate>&nbsp;</EmptyDataTemplate>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="White"/>
                                                </asp:GridView>
                                            </div> 
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD align="center" colSpan="3">
                                                <asp:LinkButton id="LkbAddToListItem" runat="server">[ Add To List ]</asp:LinkButton> - 
                                                <asp:LinkButton id="LkbCloseItem" runat="server">[ Cancel & Close ]</asp:LinkButton>
                                            </TD>
                                        </TR>
                                    </TBODY>
                                </TABLE>
                            </asp:Panel> 
                                <ajaxToolkit:ModalPopupExtender id="mpeItem" runat="server" TargetControlID="BtnHiddenListItem" PopupDragHandleControlID="LblItem" PopupControlID="PanelItem" BackgroundCssClass="modalBackground">
                                 </ajaxToolkit:ModalPopupExtender> 
                                <asp:Button id="BtnHiddenListItem" runat="server" Visible="False"></asp:Button> 
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <!--End Pop Up Data Katalog -->

                            <asp:UpdatePanel ID="upPopUpMsg" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False">
                                        <table>
                                            <tr>
                                                <td colspan="2" style="background-color: #cc0000; text-align: left">
                                                    <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="White"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="imIcon" runat="server" Height="24px" ImageUrl="~/Images/error.jpg"
                                                        Width="24px" /></td>
                                                <td class="Label" style="text-align: left">
                                                    <asp:Label ID="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px; text-align: center">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center">
                                                    &nbsp;<asp:ImageButton ID="imbOKPopUpMsg" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/ok.png" /></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="mpePopUpMsg" runat="server" BackgroundCssClass="modalBackground"
                                        Drag="True" DropShadow="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"
                                        TargetControlID="bePopUpMsg">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <span style="display: none">
                                        <asp:Button ID="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" />
                                    </span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </th>
        </tr> 
    </table>     
</asp:Content>