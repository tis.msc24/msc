<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptDNCN.aspx.vb" Inherits="ReportForm_RptDNCN" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text=".: Debit Note & Credit Note Report" CssClass="Title" ForeColor="Navy" Width="512px" Font-Size="X-Large"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" colspan="2" style="background-color: #ffffff" valign="center">
                <asp:UpdatePanel id="upReportForm" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlReportForm" runat="server" Width="100%" DefaultButton="btnSearchSupplier" __designer:wfdid="w4"><TABLE><TBODY><TR><TD align=left runat="server">Cabang</TD><TD class="Label" align=center runat="server">:</TD><TD align=left colSpan=3 runat="server"><asp:DropDownList id="CbgDDL" runat="server" CssClass="inpText" __designer:wfdid="w10"></asp:DropDownList></TD></TR><TR><TD align=left runat="server">DN / CN</TD><TD class="Label" align=center runat="server">:</TD><TD align=left colSpan=3 runat="server"><asp:DropDownList id="ddlDNCN" runat="server" CssClass="inpText" __designer:wfdid="w6"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>DN</asp:ListItem>
<asp:ListItem>CN</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server">Type</TD><TD class="Label" align=center runat="server">:</TD><TD align=left colSpan=3 runat="server"><asp:DropDownList id="ddlType" runat="server" CssClass="inpText" __designer:wfdid="w1"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>AR</asp:ListItem>
<asp:ListItem>AP</asp:ListItem>
<asp:ListItem>NT</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Period" __designer:wfdid="w11"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left colSpan=3><asp:TextBox id="txtStart" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w12"></asp:TextBox> <asp:ImageButton id="imbStart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w13"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w14"></asp:Label> <asp:TextBox id="txtFinish" runat="server" Width="60px" CssClass="inpText" __designer:wfdid="w15"></asp:TextBox> <asp:ImageButton id="imbFinish" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton> <asp:Label id="lblMMDD" runat="server" CssClass="Important" Text="(dd/mm/yyyy)" __designer:wfdid="w20"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Customer" __designer:wfdid="w18"></asp:Label></TD><TD class="Label" align=center>:</TD><TD align=left><asp:RadioButtonList id="rbSupplier" runat="server" Width="138px" __designer:wfdid="w19" RepeatDirection="Horizontal" AutoPostBack="True"><asp:ListItem Selected="True">ALL</asp:ListItem>
<asp:ListItem>SELECT</asp:ListItem>
</asp:RadioButtonList></TD><TD align=left></TD><TD align=left><asp:TextBox id="FilterTextSupplier" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w20" Visible="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupplier" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w21"></asp:ImageButton> <asp:ImageButton id="btnClearSupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w22" Visible="False"></asp:ImageButton></TD></TR><TR><TD id="TD8" align=left runat="server" Visible="false"></TD><TD id="TD1" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD9" align=left colSpan=3 runat="server" Visible="false"><asp:CheckBox id="chkEmpty" runat="server" Text="Hide Customer with 0 DN & CN amount" __designer:wfdid="w23" Checked="True"></asp:CheckBox></TD></TR><TR><TD align=left><asp:Label id="custoid" runat="server" __designer:wfdid="w24" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD align=left colSpan=3><asp:Panel id="pnlSupplier" runat="server" Width="100%" __designer:wfdid="w25"><asp:GridView id="gvCustomer" runat="server" Width="100%" ForeColor="Red" __designer:wfdid="w26" Visible="False" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="custoid,custname" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><EditItemTemplate>
<asp:CheckBox id="CheckBox1" runat="server"></asp:CheckBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("custoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR><TR><TD align=left>No Nota</TD><TD class="Label" align=center>:</TD><TD align=left colSpan=2><asp:RadioButtonList id="rbNota" runat="server" Width="138px" __designer:wfdid="w125" RepeatDirection="Horizontal" AutoPostBack="True"><asp:ListItem Selected="True">ALL</asp:ListItem>
<asp:ListItem>SELECT</asp:ListItem>
</asp:RadioButtonList>&nbsp;<asp:TextBox id="refnonota" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w8" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchRefnoNota" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="17px" __designer:wfdid="w9" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearRefnoNota" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w10" Visible="False"></asp:ImageButton> <asp:Label id="refoid" runat="server" __designer:wfdid="w11" Visible="False"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR><TR><TD align=left></TD><TD class="Label" align=center></TD><TD align=left colSpan=3><asp:GridView id="gvRefnoNota" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w5" Visible="False" PageSize="8" DataKeyNames="refoid,refnotano,refnodate" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><EditItemTemplate>
<asp:CheckBox id="CheckBox1" runat="server" __designer:wfdid="w2"></asp:CheckBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:CheckBox id="chkSelect" runat="server" __designer:wfdid="w1" Checked='<%# Eval("selected") %>' ToolTip='<%# Eval("refoid") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="refnotano" HeaderText="SI No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refnodate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left>Status</TD><TD class="Label" align=center>:</TD><TD align=left colSpan=3><asp:DropDownList id="ddlstatus" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w12"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="TD6" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Width="96px" Text="Currency" __designer:wfdid="w281"></asp:Label></TD><TD id="TD4" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD5" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="FilterCurrency" runat="server" CssClass="inpText" __designer:wfdid="w28"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=5><asp:ImageButton id="btnViewReport" onclick="btnViewReport_Click" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w29"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w30"></asp:ImageButton> <asp:ImageButton id="btnExportToExcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w31"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w32"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=5><ajaxToolkit:CalendarExtender id="ceStart" runat="server" __designer:wfdid="w33" TargetControlID="txtStart" PopupButtonID="imbStart" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceFin" runat="server" __designer:wfdid="w34" TargetControlID="txtFinish" PopupButtonID="imbFinish" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeStart" runat="server" __designer:wfdid="w35" TargetControlID="txtStart" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeFin" runat="server" __designer:wfdid="w36" TargetControlID="txtFinish" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 60px" vAlign=top align=center colSpan=5><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w37" AssociatedUpdatePanelID="upReportForm"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w38"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvReportForm" runat="server" Width="350px" __designer:wfdid="w39" HasDrillUpButton="False" HasToggleGroupTreeButton="False" HasExportButton="False" HasPrintButton="False" HasCrystalLogo="False" HasViewList="False" AutoDataBind="True"></CR:CrystalReportViewer></asp:Panel> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToExcel"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w40" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w42"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w43"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w44"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" __designer:wfdid="w45" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w46" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

