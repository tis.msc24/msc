Imports System.Data.SqlClient
Imports System.Data
Imports Koneksi
Imports ClassFunction
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Drawing
Imports System.Windows.Forms
Partial Class Transaction_trnoperteknisi
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim cFunction As New ClassFunction
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = xKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Procedure"
    Public Sub BindData(ByVal sWhere As String)
        sSql = "Select distinct o.operoid,r.barcode, r.reqcode,convert(char(20),o.operdate,106) operdate,c.custname,p1.PERSONNAME as dari ,isnull(p2.PERSONNAME,'Teknisi Out') as ke ,o.operstatus from QL_TRNREQUEST r inner join QL_TRNSERVICES s on r.REQOID = s.MSTREQOID inner join QL_mstcust c on r.REQCUSTOID = c.custoid inner join QL_MSTPERSON p on r.REQPERSON = p.PERSONOID inner join QL_trnoper o on r.REQOID = o.reqoid inner join QL_MSTPERSON p1 on p1.PERSONOID = o.opersonfrom left join QL_MSTPERSON p2 on p2.PERSONOID = o.opersonto where r.cmpcode like '%" & CompnyCode & "%'" & sWhere & " order by o.operoid desc "
        Dim xTableItem1 As DataTable = ckoneksi.ambiltabel(sSql, "Oper")
        gvOper.DataSource = xTableItem1
        gvOper.DataBind()
        gvOper.SelectedIndex = -1
    End Sub

    Public Sub FillTextBox(ByVal operid As String)
        Dim xdt As New DataTable

        Dim xquery = "select o.operoid,r.reqoid,r.barcode, r.reqcode,o.operdate ,c.custname,p1.PERSONNAME as dari ,isnull(p2.PERSONNAME,'') as ke ,o.operstatus,o.oplanoid, o.operreason,o.upduser,o.updtime,o.opersonfrom,o.opersonto,o.operflag from QL_TRNREQUEST r inner join QL_TRNSERVICES s on r.REQOID = s.MSTREQOID inner join QL_mstcust c on r.REQCUSTOID = c.custoid inner join QL_MSTPERSON p on r.REQPERSON = p.PERSONOID inner join QL_trnoper  o on r.REQOID = o.reqoid inner join QL_MSTPERSON p1 on p1.PERSONOID = o.opersonfrom left join QL_MSTPERSON p2 on p2.PERSONOID = o.opersonto inner join ql_trnservices s1 on o.oplanoid=s1.SOID  where o.cmpcode like '%" & CompnyCode & "%' and operoid = '" & operid & "'"
        xdt = ckoneksi.ambiltabel(xquery, "QL_TRNOPER")
        Dim xRow() As DataRow
        xRow = xdt.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If xRow.Length > 0 Then
            lbloid.Text = xRow(0)("OPEROID")
            barcode.Text = xRow(0)("barcode")
            notanda.Text = xRow(0)("reqcode")
            cust.Text = xRow(0)("custname")
            drteknisi.Text = xRow(0)("dari")
            alasan.Text = xRow(0)("operreason")
            status.Text = xRow(0)("operstatus")
            tgloper.Text = xRow(0)("operdate")
            keteknisi.Text = xRow(0)("ke")
            UpdTime.Text = xRow(0)("updtime")
            dari.Text = xRow(0)("opersonfrom")
            ke.Text = xRow(0)("opersonto")
            planoid.Text = xRow(0)("oplanoid")
            reqoid.Text = xRow(0)("reqoid")
            operflag.SelectedValue = xRow(0)("operflag")
        End If

        oper()
        If status.Text = "Switch" Then
            btnPost.Visible = False
            btnSave.Visible = False
            ibCarike.Enabled = False
            ibClearke.Enabled = False
            btnDel.Visible = False
        End If
        ibCarino.Enabled = true
        ibClearno.Enabled = true
    End Sub

    Sub BindDataNo(ByVal sWhere As String)
        sSql = "select r.reqcode,r.barcode,r.reqoid, c.custname , s.sstatus,p.personname,s.SOID planoid from QL_TRNREQUEST r inner join QL_TRNSERVICES s on s.MSTREQOID=r.REQOID inner join QL_mstcust c on r.REQCUSTOID = c.custoid inner join QL_MSTPERSON p on s.sperson = p.PERSONOID where s.SSTATUS IN ('Ready','Start') and s.SFLAG ='IN' and r.cmpcode='MSC' " & sWhere & " order by r.reqoid desc"
        Dim xTableItem2 As DataTable = ckoneksi.ambiltabel(sSql, "Penerimaan")
        gvNo.DataSource = xTableItem2
        gvNo.DataBind()
        gvNo.SelectedIndex = -1
        gvNo.Visible = True
    End Sub

    Sub BindDataKe(ByVal sWhere As String)
        sSql = "Select distinct p.personoid,p.PERSONNIP,p.personname,(select count(reqperson)from QL_TRNREQUEST where REQSTATUS in ('In','Out','Receive','Check','SPK') and REQPERSON =PERSONOID And branch_code='02')+(select COUNT(sperson) from QL_TRNSERVICES  where SSTATUS in('Ready','Start') and SPERSON=PERSONOID)as pengerjaan From QL_MSTPERSON p INNER JOIN QL_MSTPROF pr ON pr.USERNAME=p.PERSONNAME Where PERSONPOST = (SELECT GENOID FROM QL_MSTGEN WHERE GENDESC = 'TEKNISI' AND GENGROUP = 'JOBPOSITION') AND p.personoid not in('" & dari.Text & "') and p.cmpcode ='MSC'" & sWhere & " AND pr.BRANCH_CODE='" & DDLcabang.SelectedValue & "' Order By p.personoid desc" 
        Dim xTableItem2 As DataTable = ckoneksi.ambiltabel(sSql, "Keteknisi")
        gvKe.DataSource = xTableItem2
        gvKe.DataBind() : gvKe.SelectedIndex = -1
        gvKe.Visible = True
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal sCaption As String, ByVal iType As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
        End If
        lblCaption.Text = sCaption
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Private Sub showMessage(ByVal message As String)
        Validasi.Text = message
        panelMsg.Visible = True
        btnValidasi.Visible = True
        mpeValidasi.Show()
    End Sub

    Sub FillTextNo(ByVal reqid As Integer)
        sSql = "select r.reqoid,r.barcode,r.reqcode,c.custname,p.personname,p.PERSONOID,s.SOID planoid from QL_TRNREQUEST r inner join QL_mstcust c on c.custoid = r.REQCUSTOID inner join QL_TRNSERVICES s on r.REQOID = s.MSTREQOID inner join QL_MSTPERSON p on s.SPERSON=p.PERSONOID  where  r.cmpcode like '%" & CompnyCode & "%' and r.reqoid=" & reqid & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                reqoid.Text = xreader("reqoid").ToString.Trim
                barcode.Text = xreader("barcode").ToString.Trim
                notanda.Text = xreader("reqcode").ToString.Trim
                cust.Text = xreader("custname").ToString.Trim
                drteknisi.Text = xreader("personname").ToString.Trim
                dari.Text = xreader("personoid").ToString.Trim
                planoid.Text = xreader("planoid").ToString.Trim
            End While
        End If

    End Sub

    Sub FillTextKe(ByVal toteknisi As Integer)
        sSql = "select p.personnip,p.personname,p.personoid from QL_MSTPERSON p where p.personoid not in(" & Convert.ToInt32(dari.Text) & ") and p.cmpcode like '%" & CompnyCode & "%' and p.personoid=" & toteknisi & ""

        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                ke.Text = xreader("personoid").ToString.Trim
                keteknisi.Text = xreader("personname").ToString.Trim
            End While
        End If
    End Sub

    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/frmPenerimaanService.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & " AND gencode <> '" & Session("branch_id").ToString & "'"
        FillDDL(DDLcabang, sSql)
    End Sub
#End Region

#Region "Function"
    Private Function chkFilter()
        chkFilter &= " AND " & DDLFilter.SelectedValue & " like '%" & Tchar(TxtFilter.Text) & "%'"
        If cbPeriode.Checked Then
            Try

                Dim date1 As Date = toDate(periode1.Text)
                Dim date2 As Date = toDate(periode2.Text)
                If periode1.Text = "" And periode2.Text = "" Then
                    showMessage("Isi Tanggal !!", CompnyName & "- Warning", 2)
                End If

                If date1 < toDate("01/01/1900") Then
                    showMessage("Tanggal Awal Tidak Valid !!", CompnyName & " - Warning", 2) : Exit Function
                End If

                If date2 < toDate("01/01/1900") Then
                    showMessage("Tanggal Akhir Tidak Valid !!", CompnyName & " - Warning", 2) : Exit Function
                End If
                If date1 > date2 Then
                    showMessage("Tanggal Awal harus lebih kecil dari Tanggal Akhir !!", CompnyName & " - Warning", 2) : Exit Function
                    periode1.Text = ""
                    periode2.Text = ""
                    Exit Function
                End If

                chkFilter &= " AND CONVERT(CHAR(10),o.operdate,101)  between '" & toDate(periode1.Text) & "' AND '" & toDate(periode2.Text) & "'"

            Catch ex As Exception
                showMessage("Format Tanggal tidak sesuai !!", CompnyName & " - Warning", 2)
            End Try
        End If
        If cbStatus.Checked Then
            If StatusDDL.SelectedValue <> "All" Then
                chkFilter &= " AND o.operstatus='" & StatusDDL.SelectedValue & "'"
            End If
        End If
        Return chkFilter
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.btnPost.Attributes.Add("onclick", "return confirm('data sudah dipost tidak dapat diupdate');")
        Me.btnDel.Attributes.Add("onclick", "return confirm('apakah anda yakin akan menghapus data ini ??');")
        If Session("UserID") = "" Then Response.Redirect("~\other\login.aspx")
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("trnoperteknisi.aspx") ' recall lagi untuk clear
        End If
        Session("oid") = Request.QueryString("oid")
        Me.Title = CompnyName & " - Oper Teknisi"
        If Not IsPostBack Then
            BindData("") : Cabangddl()
            If Session("oid") Is Nothing Or Session("oid") = "" Then
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                i_u.Text = "new" : TabContainer1.ActiveTabIndex = 0
                btnDel.Visible = False : lblUpd.Text = "Create"
            Else
                FillTextBox(Session("oid"))
                i_u.Text = "update" : TabContainer1.ActiveTabIndex = 1
                UpdUser.Text = Session("UserID")
                lblUpd.Text = "Last Update"
            End If
            periode1.Text = Format(GetServerTime(), "dd/MM/yyyy")
            periode2.Text = Format(GetServerTime(), "dd/MM/yyyy")
            tgloper.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub ibCarino_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCarino.Click
        panelno.Visible = True : btnHideNo.Visible = True
        mpeNo.Show() : Dim sWhere As String = ""
        sWhere = " " : BindDataNo(sWhere)
        DDLno.SelectedIndex = 0 : txtFilterNo.Text = ""
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        If cbPeriode.Checked = True Then
            Dim sMsg As String = ""
            If periode1.Text = "" Then
                sMsg &= "- Silahkan isi Periode 1 !<br>"
            End If
            If periode2.Text = "" Then
                sMsg &= "- Silahkan isi Periode 2 !<br>"
            End If
            If sMsg <> "" Then
                showMessage(sMsg, CompnyName & " - WARNING", 2) : Exit Sub
            End If
        End If
        BindData(chkFilter())
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Session("oid") = Nothing
        Response.Redirect("~\Transaction\trnoperteknisi.aspx?awal=true")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        TxtFilter.Text = "" : DDLFilter.SelectedIndex = 0
        cbPeriode.Checked = False
        cbStatus.Checked = False
        periode1.Text = ""
        periode2.Text = ""
        BindData("")
        StatusDDL.SelectedIndex = 0

    End Sub

    Protected Sub lkbCloseno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseno.Click
        panelno.Visible = False : btnHideNo.Visible = False
        CProc.DisposeGridView(gvNo)
    End Sub

    Protected Sub ibClearno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClearno.Click
        barcode.Text = ""
        notanda.Text = ""
        cust.Text = ""
        drteknisi.Text = ""
        dari.Text = ""
    End Sub

    Protected Sub ibClearke_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibClearke.Click
        
        keteknisi.Text = ""
        DDLOper.SelectedIndex = 0
        txtOper.Text = ""
        ke.Text = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_trnoper WHERE reqoid = '" & Tchar(reqoid.Text) & "' and operstatus='Process' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
            If ckoneksi.ambilscalar(sSqlCheck1) > 0 Then
                showMessage(" No Tanda Terima telah diproses sebelumnya !!", CompnyName & "- Warning", 2) : Exit Sub
            End If
        Else : sSqlCheck1 &= " AND reqoid <> " & reqoid.Text
        End If

        
        If barcode.Text.Trim = "" Then
            showMessage("Isi barcode terlebih dulu !!", CompnyName & "- Warning", 2) : Exit Sub
        End If
        If notanda.Text.Trim = "" Then
            sMsg &= "- Isi No Tanda Terima !!<br>"
        End If
        If cust.Text.Trim = "" Then
            sMsg &= "- Isi Nama Customer !!<br>"
        End If
        If drteknisi.Text.Trim = "" Then
            sMsg &= "- Isi Dari Teknisi !!<br>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, CompnyName & "- Warning", 2) : Exit Sub
        End If
        If alasan.Text.Trim = "" Then
            showMessage("Isi Alasan oper teknisi", CompnyName & "- Warning", 2)
            Exit Sub
        End If
        If tgloper.Text.Trim = "" Then
            showMessage("Isi tanggal oper teknisi", CompnyName & "- Warning", 2)
            Exit Sub
        End If
        If operflag.SelectedValue = "In" Then
            If keteknisi.Text.Trim = "" Then
                showMessage("Pilih Ke Teknisi terlebih dulu !!", CompnyName & "- Warning", 2) : Exit Sub
            End If
        Else
            ke.Text = 0
        End If
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                lbloid.Text = GenerateID("QL_TRNOPER", CompnyCode)
                lblstatus.Text = "Process"
                sSql = "INSERT INTO QL_TRNOPER(cmpcode,operoid,reqoid,operdate,opersonfrom,opersonto,operstatus,operreason,upduser,updtime,createuser,createtime,oplanoid,operflag,branch_code) VALUES" & _
                " ('" & CompnyCode & "'," & Convert.ToInt32(lbloid.Text) & "," & Convert.ToInt32(reqoid.Text) & ",'" & toDate(tgloper.Text) & "'," & Convert.ToInt32(dari.Text) & "," & Convert.ToInt32(ke.Text) & ",'" & lblstatus.Text & "','" & Tchar(alasan.Text) & "','" & Session("UserID") & "',current_timestamp,'" & Session("UserID") & "',current_timestamp," & Convert.ToInt32(planoid.Text) & ",'" & operflag.SelectedValue & "','" & DDLcabang.SelectedValue & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & lbloid.Text & " where tablename='ql_trnoper' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                lblstatus.Text = "Process"
                sSql = "UPDATE QL_TRNOPER set operdate='" & toDate(tgloper.Text) & "',opersonfrom=" & Convert.ToInt32(dari.Text) & ",opersonto=" & Convert.ToInt32(ke.Text) & ",operstatus='" & lblstatus.Text & "',operreason='" & Tchar(alasan.Text) & "',upduser='" & Session("UserID") & "',updtime=current_timestamp,createuser='" & Session("UserID") & "',createtime=current_timestamp,oplanoid=" & Convert.ToInt32(planoid.Text) & ",operflag = '" & operflag.SelectedValue & "' Where operoid=" & Convert.ToInt32(lbloid.Text) & " and reqoid=" & Convert.ToInt32(reqoid.Text) & " And branch_code='" & DDLcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If post.Text = "Post" Then
                lblstatus.Text = "Switch"
                If operflag.SelectedValue = "In" Then
                    sSql = "Update ql_trnservices set sperson=" & Convert.ToInt32(ke.Text) & " where cmpcode='" & CompnyCode & "' and mstreqoid=" & Convert.ToInt32(reqoid.Text) & " And soid=" & Convert.ToInt32(planoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "update ql_trnservices set sstatus = 'CheckOut', SFLAG = 'Out' Where cmpcode='" & CompnyCode & "' and mstreqoid=" & Convert.ToInt32(reqoid.Text) & " and soid=" & Convert.ToInt32(planoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "update ql_trnrequest set reqstatus = 'CheckOut' where cmpcode='" & CompnyCode & "' and reqoid=" & Convert.ToInt32(reqoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                    'sperson=" & Convert.ToInt32(ke.Text) & "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "SELECT count(-1) FROM QL_trnsvcwork where cmpcode='" & CompnyCode & "' and reqoid = " & Convert.ToInt32(reqoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteScalar > 0 Then 'berarti ada dan udah di start
                        sSql = "update QL_trnsvcwork set status = 'Failed' where cmpcode='" & CompnyCode & "' and reqoid=" & Convert.ToInt32(reqoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                        'sperson=" & Convert.ToInt32(ke.Text) & "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "update ql_trnservices set sstatus = 'Failed' , SFLAG = 'Out' where cmpcode='" & CompnyCode & "' and mstreqoid=" & Convert.ToInt32(reqoid.Text) & " and soid=" & Convert.ToInt32(planoid.Text) & " And branch_code='" & Session("branch_id") & "'"
                        'sperson=" & Convert.ToInt32(ke.Text) & "
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If
                sSql = "update ql_trnoper set operstatus='" & lblstatus.Text & "' where cmpcode='" & CompnyCode & "' and operoid=" & Convert.ToInt32(lbloid.Text) & " and reqoid=" & Convert.ToInt32(reqoid.Text) & " And branch_code='" & DDLcabang.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnoperteknisi.aspx?awal=true")
    End Sub

    Protected Sub gvNo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvNo.PageIndexChanging
        gvNo.PageIndex = e.NewPageIndex
        panelno.Visible = True
        btnHideNo.Visible = True : mpeNo.Show()

        BindDataNo("")
    End Sub

    Protected Sub gvNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvNo.SelectedIndexChanged
        Dim sReqid As Integer
        btnHideNo.Visible = False
        panelno.Visible = False
        mpeNo.Show()
        sReqid = gvNo.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextNo(sReqid)
    End Sub

    Protected Sub ibFindno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFindno.Click
        panelno.Visible = True
        btnHideNo.Visible = True
        mpeNo.Show()
        Dim sWhere As String = "AND " & DDLno.SelectedValue & " LIKE '%" & Tchar(txtFilterNo.Text) & "%'"
        BindDataNo(sWhere)
    End Sub

    Protected Sub ibViewno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibViewno.Click
        panelno.Visible = True
        btnHideNo.Visible = True
        mpeNo.Show()
        DDLno.SelectedIndex = 0 : txtFilterNo.Text = ""
        BindDataNo("")
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        btnValidasi.Visible = False
        panelMsg.Visible = False
        btnHideNo.Visible = False
        panelno.Visible = False
    End Sub

    Protected Sub ibCarike_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibCarike.Click
        If notanda.Text = "" Then
            showMessage("Isi No Tanda Terima terlebih dulu !!", CompnyName & "- Warning", 2) : Exit Sub
        End If
        keteknisi.Text = ""
        panelke.Visible = True
        btnHideKe.Visible = True
        mpeke.Show()
        Dim sWhere As String = ""
        sWhere = " "
        BindDataKe(sWhere)
        DDLOper.SelectedIndex = 0
        txtOper.Text = ""
    End Sub

    Protected Sub gvKe_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvKe.PageIndexChanging
        gvKe.PageIndex = e.NewPageIndex
        panelke.Visible = True
        btnHideKe.Visible = True : mpeke.Show()

        BindDataKe("")
    End Sub

    Protected Sub gvKe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvKe.SelectedIndexChanged
        Dim sKeteknisi As Integer
        btnHideKe.Visible = False
        panelke.Visible = False
        mpeke.Show()
        sKeteknisi = gvKe.SelectedDataKey(0).ToString.Trim
        CProc.DisposeGridView(sender)
        FillTextKe(sKeteknisi)
    End Sub

    Protected Sub lkbCloseke_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseke.Click
        panelke.Visible = False : btnHideKe.Visible = False
        CProc.DisposeGridView(gvKe)
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        post.Text = "Post"
        btnSave_Click(sender, e)

        'Dim objTrans As SqlClient.SqlTransaction
        'If conn.State = ConnectionState.Closed Then
        '    conn.Open()
        'End If
        'objTrans = conn.BeginTransaction()
        'xCmd.Transaction = objTrans
        'Try


        '    objTrans.Commit()
        '    conn.Close()
        'Catch ex As Exception
        '    objTrans.Rollback() : conn.Close()
        '    showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        'End Try

    End Sub

    Protected Sub btnDel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDel.Click

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction
        xCmd.Transaction = objTrans

        Try
            sSql = "delete from QL_TRNOPER where operoid=" & Session("oid") & " and cmpcode like '%" & CompnyCode & "%' branch_code='" & Session("branch_id") & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & "- Warning", 2) : Exit Sub
        End Try
        Response.Redirect("~\Transaction\trnoperteknisi.aspx?awal=true")
    End Sub

    Protected Sub ibFindOper_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFindOper.Click
        panelke.Visible = True : btnHideKe.Visible = True
        mpeke.Show()
        Dim sWhere As String = " AND " & DDLOper.SelectedValue & " LIKE '%" & Tchar(txtOper.Text) & "%'"
        BindDataKe(sWhere)
    End Sub

    Protected Sub ibViewOper_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibViewOper.Click
        panelke.Visible = True
        btnHideKe.Visible = True
        mpeke.Show()
        DDLOper.SelectedIndex = 0 : txtOper.Text = ""
        BindDataKe("")
    End Sub

    Sub oper()
        If operflag.SelectedValue = "In" Then
            lblketeknisi.Visible = True
            lblpetik2.Visible = True : keteknisi.Visible = True
            ibCarike.Visible = True : ibClearke.Visible = True
        Else
            lblketeknisi.Visible = False
            lblpetik2.Visible = False : keteknisi.Visible = False
            ibCarike.Visible = False : ibClearke.Visible = False
        End If
    End Sub
    
    Protected Sub operflag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        oper()
    End Sub

    Protected Sub barcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not barcode.Text = "" Then
            sSql = "select r.reqoid,r.barcode,r.reqcode,c.custname,p.personname,p.PERSONOID,s.SOID planoid from QL_TRNREQUEST r inner join QL_mstcust c on c.custoid = r.REQCUSTOID inner join QL_TRNSERVICES s on r.REQOID = s.MSTREQOID inner join QL_MSTPERSON p on s.SPERSON=p.PERSONOID where r.cmpcode like '%" & CompnyCode & "%' and r.barcode='" & Tchar(barcode.Text) & "' and (s.SSTATUS = 'Ready' or s.SSTATUS = 'Start') and s.SFLAG = 'IN'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    reqoid.Text = xreader("reqoid").ToString.Trim
                    barcode.Text = xreader("barcode").ToString.Trim
                    notanda.Text = xreader("reqcode").ToString.Trim
                    cust.Text = xreader("custname").ToString.Trim
                    drteknisi.Text = xreader("personname").ToString.Trim
                    dari.Text = xreader("personoid").ToString.Trim
                    planoid.Text = xreader("planoid").ToString.Trim
                End While
            Else
                showMessage("Tidak ada No. Barcode yang ditemukan", CompnyName & " - Warning", 2) : Exit Sub
            End If
        End If
        conn.Close()
    End Sub

    Protected Sub gvOper_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOper.PageIndexChanging
        gvOper.PageIndex = e.NewPageIndex
        BindData(chkFilter())
    End Sub
#End Region
End Class
