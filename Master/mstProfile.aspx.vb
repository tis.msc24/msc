'Programmer: ^Valz_niK^ | 11.05.2011
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_mstprofile
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Dim folderReport As String = "~/Report/"
#End Region

#Region "Procedure"

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""

        FilterText.Text = Session("FilterText_mstprofile")
        FilterDDL.SelectedIndex = Session("FilterDDL_mstprofile")

        sWhere = " AND UPPER(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") LIKE '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "
        If chkGroup.Checked Then
            sWhere &= " AND statusprof='" & FilterGroup.SelectedValue & "'"
        Else
            sWhere &= " AND statusprof<>'DELETED' "
        End If
        'disimpan ke dalam session untuk keperluan print
        Session("sWhere_mstprofile") = sWhere

        If cbLevelUser.Checked = True Then
            sWhere &= " And userlevel='" & fLevelUser.SelectedValue & "'"
        End If
        If sCompCode <> "" Then
            sSql = "SELECT userid, username, approvallimit, statusprof,Case userlevel When 1 Then 'ADMIN' When 2 Then 'MANAGER' When 3 Then 'SUPERVISOR' When 4 Then 'OPERATOR' Else '' End Userlevel FROM QL_mstprof WHERE 1=1 " & sWhere & " and upper(cmpcode)='" & sCompCode.ToUpper & "' ORDER BY userid"
        Else
            sSql = "SELECT userid, username, approvallimit, statusprof,Case userlevel When 1 Then 'ADMIN' When 2 Then 'MANAGER' When 3 Then 'SUPERVISOR' When 4 Then 'OPERATOR' Else '' End Userlevel FROM QL_mstprof WHERE 1=1 " & sWhere & " ORDER BY userid"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstprofile.DataSource = objDs.Tables("data")
        GVmstprofile.DataBind()

        For x As Int32 = 0 To GVmstprofile.Rows.Count - 1
            If GVmstprofile.Rows(x).Cells(3).Text = "INACTIVE" Then
                GVmstprofile.Rows(x).Cells(3).ForeColor = Drawing.Color.Red
            End If
        Next
    End Sub

    Public Sub BindDataPersonPopup(ByVal sqltext As String)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        sSql = "select * from (SELECT p.personnip nip, p.personname, (SELECT GENDESC FROM QL_MSTGEN WHERE GENOID = P.PERSONSTATUS) divisi, personoid FROM QL_mstperson p where p.cmpcode='" & CompnyCode & "' and p.status = 'aktif' and p.PERSONNIP not in (select userid  from QL_mstprof) " & sqltext & " ) t  ORDER BY t.divisi, personname"
        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        GVPerson.Visible = True
        GVPerson.DataSource = xreader
        GVPerson.DataBind()
        GVPerson.SelectedIndex = -1
  End Sub

    Public Sub InitAllDDL()
        Dim sSql As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLCbg, sSql)
    End Sub

    Public Sub FillTextBox(ByVal vUserid As String)
        sSql = "SELECT cmpcode, userid, username, approvallimit, statusprof, upduser, updtime,profpass ,isnull(position,'')position,isnull(divisi,'') divisi ,isnull(DIVISICREDITLIMIT,'') DIVISICREDITLIMIT,branch_code,userlevel,FLAGSALES,potypeapp,personnoid FROM QL_mstprof Where cmpcode='" & CompnyCode & "' and userid = '" & Tchar(vUserid) & "'"
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        While xreader.Read
            userid.Text = Trim(xreader("userid").ToString)
            useridOld.Text = Trim(xreader("userid").ToString)
            username.Text = Trim(xreader("username").ToString)
            usertemp.Text = Trim(xreader("username").ToString)
            approvallimit.Text = Trim(xreader("approvallimit").ToString)
            ddlStatus.SelectedValue = xreader("statusprof")
            profpass.Text = Trim(xreader("profpass").ToString)
            Upduser.Text = Trim(xreader("upduser").ToString)
            Updtime.Text = CDate(Trim(xreader("updtime").ToString)).ToString("dd/MM/yyyy HH:mm:ss tt")
            LblPaswd.Text = Trim(xreader("profpass").ToString)
            devisitemp.Text = Trim(xreader("divisi").ToString)
            PoType.Text = Trim(xreader("potypeapp").ToString)
            divisiCR.Text = Trim(xreader("DIVISICREDITLIMIT").ToString)
            jobpos.Text = Trim(xreader("position"))
            personoid.Text = xreader("personnoid").ToString
            DDLCbg.SelectedValue = xreader("branch_code").ToString
            DDLCbg.Enabled = False
            DDLCbg.CssClass = "inpTextDisabled"
            LevelUserDDL.SelectedValue = xreader("userlevel")
            profpass.TextMode = TextBoxMode.Password

            If xreader("FLAGSALES").ToString = "YES" Then
                sUser.Checked = True
            Else
                sUser.Checked = False
            End If

        End While
        xreader.Close()
        conn.Close()

        If divisiCR.Text = "Approval Credit Limit" Then
            crApp.Checked = True
            divisiCR.Text = ""
        Else
            crApp.Checked = False
            If divisiCR.Text.Trim <> "" Then 'devisi lain tidak boleh acces approval
                'DevApproval.Visible = False
            End If
        End If

        If devisitemp.Text = "Approval" Then
            DevApproval.Checked = True
            devisitemp.Text = ""
        Else
            DevApproval.Checked = False
            If devisitemp.Text.Trim <> "" Then 'devisi lain tidak boleh acces approval
                'DevApproval.Visible = False
            End If
        End If

        If PoType.Text = "True" Then
            CbTypePO.Checked = True
        Else
            CbTypePO.Checked = False
        End If

        userid.ControlStyle.CssClass = "inpTextDisabled"
        userid.Enabled = False : btnSales.Visible = False
    End Sub

    Sub clearItem()
        Session("UserID") = Session("UserID")
        Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
    End Sub

#End Region

#Region "Function"

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Remove("oid_mstprofile")
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("mstprofile.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Session("oid_mstprofile") = Request.QueryString("oid")
        Me.BtnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
        Page.Title = CompnyName & " - Data Profile"

        If Not Page.IsPostBack Then
            BindData(CompnyCode)
            InitAllDDL()
            If Session("oid_mstprofile") IsNot Nothing And Session("oid_mstprofile") <> "" Then
                BtnDelete.Visible = True
                PanelUpdate.Visible = True
                FillTextBox(Session("oid_mstprofile"))
                TabContainer1.ActiveTabIndex = 1
            Else
                BtnDelete.Visible = False
                PanelUpdate.Visible = False
                Session("UserID") = Session("UserID")
                Updtime.Text = Date.Now.ToString("dd/MM/yyyy HH:mm:ss tt")
                TabContainer1.ActiveTabIndex = 0
      End If
      'btnClearPerson.Visible = True
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText_mstprofile") = FilterText.Text
        Session("FilterDDL_mstprofile") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session.Remove("FilterText_mstprofile")
        Session.Remove("FilterDDL_mstprofile")
        chkGroup.Checked = False : cbLevelUser.Checked = False
        FilterGroup.SelectedIndex = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\mstprofile.aspx?awal=true")
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnDelete.Click
        If userid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Tolong mengisi User ID!"))
            Exit Sub
        Else
            Dim usid As String = Session("UserID")
            sSql = "SELECT statusprof FROM QL_mstprof WHERE userid='" & Tchar(useridOld.Text) & "'"
            If cKon.ambilscalar(sSql).ToString = "Active" Then
                showMessage("User dengan status aktif tidak bisa dihapus!", CompnyName & " - WARNING")
                Exit Sub
            End If
            If usid.ToUpper() = useridOld.Text.ToUpper() Then
                showMessage("User tidak bisa dihapus karena sedang login!", CompnyName & " - WARNING")
                Exit Sub
            End If
            sSql = "SELECT * FROM QL_USERROLE WHERE USERPROF = '" & useridOld.Text.ToUpper() & "'"
            If cKon.ambiltabel(sSql, "usrRole").Rows.Count <> 0 Then
                showMessage("Profil telah digunakan di User role! ", CompnyName & " - WARNING ")
                Exit Sub
            End If
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
            sSql = "DELETE FROM ql_mstprof WHERE cmpcode like '%" & CompnyCode & "%' and userid='" & Tchar(useridOld.Text) & "'"
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
            clearItem()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, CompnyName & " - WARNING")
            Exit Sub
        End Try
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Data telah dihapus!"))
        Session.Remove("oid_mstprofile")
        Response.Redirect("~\Master\mstprofile.aspx?awal=true")
    End Sub

  Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
    Dim errMessage As String = ""

    If userid.Text = "" Then
            errMessage &= "Tolong masukkan/pilih User ID!<BR>"
        End If

        If Session("oid_mstprofile") Is Nothing Or Session("oid_mstprofile") = "" Then
            If profpass.Text = "" Then
                errMessage &= "Tolong isikan Password terlebih dahulu!<BR>"
            Else
                If Tchar(profpass.Text).Length < 4 Then
                    errMessage &= "Password minimal 4 karakter! <br />"
                End If
            End If
        End If

        If devisitemp.Text.Trim = "" Then
            If DevApproval.Checked = True Then
                devisitemp.Text = "Approval"
            Else
                devisitemp.Text = ""
            End If
        End If


        If CbTypePO.Checked = True Then
            PoType.Text = "True"
        Else
            PoType.Text = "False"
        End If 

        If divisiCR.Text.Trim = "" Then
            If crApp.Checked = True Then
                divisiCR.Text = "Approval Credit Limit"
            Else
                divisiCR.Text = ""
            End If
        End If
        'waktu mode insert, cek userid [QL_mstprof] yang kembar (userid tidak boleh daftar lebih dari 1x)
        If Session("oid_mstprofile") Is Nothing Or Session("oid_mstprofile") = "" Then
            Dim sSqlCheck As String = "SELECT COUNT(-1) FROM QL_mstprof WHERE userid='" & Tchar(userid.Text) & "'"
            If cKon.ambilscalar(sSqlCheck) > 0 Then
                errMessage &= "User ID telah digunakan sebelumnya!<BR>"
            End If
        End If

        'waktu mode insert, cek username [QL_mstprof] yang kembar (username tidak boleh daftar lebih dari 1x)
        If Session("oid_mstprofile") Is Nothing Or Session("oid_mstprofile") = "" Then
            Dim sSqlCheck As String = "SELECT COUNT(-1) FROM QL_mstprof WHERE username='" & Tchar(username.Text) & "'"
            If cKon.ambilscalar(sSqlCheck) > 0 Then
                errMessage &= "Username sudah memiliki akun sebelumnya !<BR>"
            End If
        End If

        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Session("UserID") = Session("UserID")
            Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)

            Dim statusprofTemp As String = "" : Dim tUser As String = "NO"
            If ddlStatus.SelectedValue = "Active" Then
                statusprofTemp = "Active"
            ElseIf ddlStatus.SelectedValue = "Inactive" Then
                statusprofTemp = "Inactive"
            Else
                statusprofTemp = "Suspended"
            End If

            If sUser.Checked = True Then
                tUser = "YES"
            End If
            'mode insert
            If Session("oid_mstprofile") Is Nothing Or Session("oid_mstprofile") = "" Then
                sSql = "INSERT into QL_mstprof (cmpcode,userid, username,profpass,approvallimit,statusprof,branch_code,upduser,updtime,position,divisi,divisicreditlimit,USERLEVEL,FLAGSALES,potypeapp,personnoid) VALUES (" & _
                "'" & CompnyCode & "'," & _
                "'" & Tchar(userid.Text) & "'," & _
                "'" & Tchar(username.Text) & "'," & _
                "'" & Tchar(profpass.Text) & "'," & _
                "1," & _
                "'" & Tchar(statusprofTemp) & "'," & _
                "'" & Tchar(DDLCbg.SelectedValue) & "'," & _
                "'" & Session("UserID") & "'," & _
                " " & "current_timestamp,'" & jobpos.Text & "', '" & devisitemp.Text & "','" & divisiCR.Text & "','" & LevelUserDDL.SelectedValue & "','" & tUser & "','" & PoType.Text & "'," & personoid.Text & ")"
            Else 'mode update
                If profpass.Text.Trim <> "" Then
                    LblPaswd.Text = profpass.Text
                Else
                    LblPaswd.Text = LblPaswd.Text
                End If
                sSql = "UPDATE QL_mstprof SET userid='" & Tchar(userid.Text) & "'," & _
                "cmpcode='" & CompnyCode & "', " & _
                "username='" & Tchar(username.Text) & "', " & _
                "profpass='" & Tchar(LblPaswd.Text) & "'," & _
                "statusprof='" & Tchar(statusprofTemp) & "'," & _
                "branch_code='" & (DDLCbg.SelectedValue) & "'," & _
                "upduser='" & Session("UserID") & "', " & _
                "updtime=" & "current_timestamp" & ", position='" & jobpos.Text & "', divisi = '" & devisitemp.Text & "', divisicreditlimit= '" & divisiCR.Text & "',USERLEVEL='" & LevelUserDDL.SelectedValue & "', FLAGSALES='" & tUser & "', potypeapp='" & PoType.Text & "' WHERE cmpcode like '%" & CompnyCode & "%' and userid='" & Tchar(useridOld.Text) & "'"
            End If
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, CompnyName & " - WARNING")
            Exit Sub
        End Try
        Session.Remove("oid_mstprofile")
        Response.Redirect("~\Master\mstprofile.aspx?awal=true")
  End Sub

  Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrOK.Click
    CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, False)
  End Sub
  
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport("", "Data_Profile", ExportFormatType.PortableDocFormat)
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        'untuk print
        rptReport.Load(Server.MapPath("~/Report/rptMstProfile.rpt"))
        'rptReport.SetParameterValue("cmpcode", CompnyCode)
        Dim swhere As String = " where " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%' "
        rptReport.SetParameterValue("sWhere", swhere)
       

        cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
        System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

      
        Response.Buffer = False
        'Clear the response content and headers
        Response.ClearContent()
        Response.ClearHeaders()
        ' Export the Report to Response stream in PDF format and file name Customers
        rptReport.ExportToHttpResponse(formatReport, Response, True, sFileName)
        'Response.Redirect(Page.AppRelativeVirtualPath.ToString)
    End Sub

    Protected Sub btnClearPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearPerson.Click
        DevApproval.Visible = True
        DevApproval.Checked = False
        devisitemp.Text = ""
        userid.Text = "" : userid.ReadOnly = False : userid.CssClass = "inpText"
        username.Text = "" : username.ReadOnly = False : username.CssClass = "inpText"
        btnClearPerson.Visible = False
    End Sub

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton4.Click
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        MPEPerson.Hide()
        CProc.DisposeGridView(GVPerson)
    End Sub

    Protected Sub btnFindPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindPerson.Click
        Dim sqltext As String = " AND " & DDLFilterPerson.SelectedValue & " LIKE '%" & Tchar(FilterPerson.Text) & "%'"
        BindDataPersonPopup(sqltext)
        MPEPerson.Show()
    End Sub

    Protected Sub btnAllPerson_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllPerson.Click
    FilterPerson.Text = ""
    DDLFilterPerson.SelectedIndex = 0
        BindDataPersonPopup("")
        MPEPerson.Show()
    End Sub

    Protected Sub GVPerson_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVPerson.SelectedIndexChanged
        PanelPerson.Visible = False
        ButtonPerson.Visible = False
        btnClearPerson.Visible = True
        DevApproval.Visible = True
        MPEPerson.Hide()
        'userid.Text = GVPerson.SelectedDataKey(0).ToString().Trim 'nip
        'userid.CssClass = "inpTextDisabled" : userid.ReadOnly = True
        username.Text = GVPerson.SelectedDataKey(1).ToString().Trim 'personname
        username.CssClass = "inpTextDisabled" : username.ReadOnly = True
        personoid.Text = GVPerson.SelectedDataKey("personoid")
        'personoid
        jobpos.Text = GVPerson.SelectedDataKey(3).ToString().Trim
        CProc.DisposeGridView(sender)
    End Sub

    Protected Sub btnSales_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PanelPerson.Visible = True
        ButtonPerson.Visible = True
        MPEPerson.Show()
        BindDataPersonPopup("")
    End Sub
#End Region

    Protected Sub fLevelUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fLevelUser.SelectedIndexChanged
        cbLevelUser.Checked = True
    End Sub
End Class
