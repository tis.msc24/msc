<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnInitVoucher.aspx.vb" Inherits="Accounting_InitVoucher" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" runat="Server">
   <table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Maroon" Text=".: Saldo Awal Voucher"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:Panel id="Panel1" runat="server" Width="800px" DefaultButton="btnFindConap" __designer:wfdid="w98"><TABLE width="100%"><TBODY><TR id="Tr1" runat="server" visible="false"><TD id="TD2" align=left runat="server" Visible="false"><asp:Label id="Label9" runat="server" Text="Cabang" __designer:wfdid="w99"></asp:Label></TD><TD id="TD1" align=left runat="server" Visible="false">:</TD><TD id="TD3" align=left colSpan=4 runat="server" Visible="false"><asp:DropDownList id="dd_branch" runat="server" Width="220px" CssClass="inpText" __designer:wfdid="w100">
                                                        </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 25px" align=left><asp:Label id="Label1" runat="server" Text="Filter" __designer:wfdid="w101"></asp:Label> </TD><TD style="HEIGHT: 25px" align=left>:</TD><TD style="HEIGHT: 25px" align=left colSpan=4><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" __designer:wfdid="w102" AutoPostBack="True"><asp:ListItem Value="voucherno">No. Voucher</asp:ListItem>
<asp:ListItem Value="voucherdesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterVcr" runat="server" Width="123px" CssClass="inpText" __designer:wfdid="w103"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindConap" runat="server" ImageUrl="~/Images/find.png" __designer:wfdid="w104"></asp:ImageButton> <asp:ImageButton id="btnViewAllConap" onclick="btnViewAllConap_Click1" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w105"></asp:ImageButton></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="DdlStatus" runat="server" CssClass="inpText" __designer:wfdid="w106" AutoPostBack="True"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="Post">Post</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="gvTrnConap" runat="server" Width="950px" ForeColor="#333333" __designer:wfdid="w96" PageSize="8" OnRowDataBound="gvTrnConap_RowDataBound" AutoGenerateColumns="False" AllowPaging="True" CellPadding="4" GridLines="None" DataKeyNames="voucheroid">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="voucheroid" DataNavigateUrlFormatString="~\Accounting\trnInitVoucher.aspx?Oid={0}" DataTextField="voucherno" HeaderText="No. Voucher">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="voucheroid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="voucherdesc" HeaderText="Nama Voucher">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="200px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtvoucheridr" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="voucherstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="Data tidak ditemukan"></asp:Label>
                                            
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong>.<span style="font-size: 9pt">: List Saldo Awal Voucher</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="WIDTH: 18px" align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w55" Visible="False"></asp:Label> <asp:Label id="i_u" runat="server" Font-Bold="True" ForeColor="Red" __designer:wfdid="w56"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px" align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w57" TargetControlID="DateVoucher" PopupButtonID="imbInvoiceDate" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" align=left><asp:Label id="Label21" runat="server" Width="70px" Font-Size="X-Small" Text="No. Voucher" __designer:wfdid="w58"></asp:Label> <asp:Label id="Label18" runat="server" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w59"></asp:Label></TD><TD align=left><asp:TextBox id="VoucherNo" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w60" AutoPostBack="True" Enabled="true" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnVcr" onclick="BtnVcr_Click" runat="server" ImageUrl="~/Images/search2.gif" __designer:wfdid="w61"></asp:ImageButton> <asp:ImageButton id="btnErase" onclick="btnErase_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" Height="16px" __designer:wfdid="w62"></asp:ImageButton>&nbsp;<asp:Label id="VoucherOid" runat="server" Font-Size="X-Small" __designer:wfdid="w63" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" align=left></TD><TD align=left><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvVoucher" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w64" Visible="False" AutoGenerateColumns="False" AllowPaging="True" CellPadding="4" GridLines="None" DataKeyNames="itemoid,itemcode,itemdesc" OnPageIndexChanging="gvVoucher_PageIndexChanging" OnSelectedIndexChanged="gvVoucher_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Voucher">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="False" Font-Size="X-Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                                        <asp:Label ID="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No Voucher data found!!" __designer:wfdid="w2"></asp:Label>

                                                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" class="Label" align=left>Desc. Voucher</TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:TextBox id="VcrDesc" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w65" AutoPostBack="True" Enabled="False" MaxLength="20"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" class="Label" align=left><asp:Label id="Label4" runat="server" Width="84px" Font-Size="X-Small" Text="Date Voucher" __designer:wfdid="w66"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:TextBox id="DateVoucher" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w67" AutoPostBack="True" Enabled="False" MaxLength="10"></asp:TextBox> <asp:ImageButton id="imbInvoiceDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w68" Visible="False"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" class="Label" align=left><asp:Label id="Label2" runat="server" Font-Size="X-Small" Text="Currency" __designer:wfdid="w69"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:DropDownList id="curroid" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w70" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="curroid_SelectedIndexChanged"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" class="Label" align=left>Rate</TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:TextBox id="currate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w71" Enabled="False" MaxLength="30">1</asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 18px" class="Label" align=left><asp:Label id="Label3" runat="server" Width="49px" Font-Size="X-Small" Text="Amount" __designer:wfdid="w72"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:TextBox id="AmtVoucher" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w73" AutoPostBack="True" Enabled="true" MaxLength="12" OnTextChanged="amtbeli_TextChanged">0</asp:TextBox> <asp:Label id="rate2oidvcr" runat="server" __designer:wfdid="w74" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 18px" align=left><asp:Label id="Label10" runat="server" Width="38px" Font-Size="X-Small" Text="Status" __designer:wfdid="w75"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left><asp:Label id="lblPosting" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" __designer:wfdid="w76">In Process</asp:Label></TD></TR><TR><TD style="WIDTH: 18px" align=left><asp:Label id="Label7" runat="server" Width="51px" Font-Size="X-Small" Text="Catatan" __designer:wfdid="w77"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 462px; COLOR: #000099" align=left colSpan=1><asp:TextBox id="vNote" runat="server" Width="404px" CssClass="inpText" __designer:wfdid="w78" Enabled="true" MaxLength="220" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=2>Last Update On <asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w79"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w80"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=2><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w81"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w82"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w83"></asp:ImageButton> <asp:ImageButton id="imbposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w84"></asp:ImageButton>&nbsp;</TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <strong><span style="font-size: 9pt">
                                .: Form Saldo Awal Voucher&nbsp;</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upMsgbox" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False">
                <table style="WIDTH: 495px" cellspacing="1" cellpadding="1" border="0">
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2">
                            <asp:Panel ID="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow">
                                <asp:Label ID="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px" valign="top" align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 46px" valign="top" align="center">
                            <asp:Image ID="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image>
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblMessage" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="HEIGHT: 10px; TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:Label ID="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="TEXT-ALIGN: center" valign="top" align="left" colspan="2">
                            <asp:ImageButton ID="btnMsgBoxOK" OnClick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpeMsgbox" runat="server" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="PanelMsgBox" TargetControlID="beMsgBox" DropShadow="True">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Button ID="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" AutoGenerateColumns="False"><Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblPosting2" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

