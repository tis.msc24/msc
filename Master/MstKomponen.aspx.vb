Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class MstKomponen
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsExistsMatData() As Boolean
        Dim sErr As String = ""
        Dim sError As String = ""
         
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstitem WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If ToDouble(GetStrData(sSql)) = 0 Then
            showMessage("There is no Finish Good data. Please input material data first or contact your administrator!", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If itemoid.Text = "" Then
            sError &= "- Please select MATERIAL field!<BR>"
        End If
        If pritemqty.Text = "" Then
            sError &= "- Please fill QUANTITY field!<BR>"
        Else
            If ToDouble(pritemqty.Text) <= 0 Then
                sError &= "- QUANTITY field must be more than 0!<BR>"
            Else
                'If ToDouble(itemlimitqty.Text) > 0 Then
                '    If Not IsQtyRounded(ToDouble(pritemqty.Text), ToDouble(itemlimitqty.Text)) Then
                '        sError &= "- QUANTITY field must be rounded by ROUNDING QTY!<BR>"
                '    End If
                'End If
            End If
        End If
        Dim sErr As String = ""
         
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = "", sErr = ""
        If pritemdate.Text = "" Then
            sError &= "- Maaf, Tanggal.nya belum di isi..!!<BR>"
        Else
            If Not IsValidDate(CDate(toDate(pritemdate.Text)), "MM/dd/yyyy", sErr) Then
                sError &= "- Maaf, Tanggal.nya invalid, bosque..!!<BR>"
            End If
        End If
         
        If pritemmstnote.Text.Length > 100 Then
            sError &= "- Maaf, kolom.nya keterangan melebihi 100 karakter, bosque..!!<BR>"
        End If

        'If katalogOid.Text <> "" Then
        '    sError &= "- Maaf, Pilih katalog.nya dulu, bosque..!!<br>"
        'End If

        If QtyKatalog.Text = "0" Then
            sError &= "- Maaf, isi qty katalog.nya dulu, bosque..!!<br>"
        End If

        If Not Session("TblDtl") Is Nothing Then
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Maaf, Data detail tidak ada, bosque.!!<BR>"
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedValue
            End If
        Next
        Return sReturn
    End Function

    Private Function GetDDLText(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.DropDownList Then
                sReturn = CType(myControl, System.Web.UI.WebControls.DropDownList).SelectedItem.Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("prqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("prdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                    dtView(0)("prqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                    dtView(0)("prdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    If dtView2.Count > 0 Then
                                        dtView2(0)("prqty") = ToDouble(GetTextBoxValue(row.Cells(3).Controls))
                                        dtView2(0)("prdtlnote") = GetTextBoxValue(row.Cells(5).Controls)
                                    End If
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsFolderExists(ByVal sFolder As String) As Boolean
        Dim fso = CreateObject("Scripting.FileSystemObject")
        If (fso.FolderExists(sFolder)) Then
            Return True
        Else
            Return False
        End If
    End Function 

    Private Overloads Function CheckMaterial(ByRef sOid As String, ByVal sCode As String) As Boolean
        sSql = "SELECT TOP 1 itemoid FROM QL_mstitem m WHERE cmpcode='" & CompnyCode & "' AND itemcode='" & sCode & "' AND m.itemflag='AKTIF' "
        sOid = GetStrData(sSql)
        If sOid = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Overloads Function CheckUnit(ByRef sOid As String, ByVal sCode As String) As Boolean
        sSql = "SELECT TOP 1 genoid FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='ITEM UNIT' AND gendesc='" & sCode & "' AND itemflag='AKTIF'"
        sOid = GetStrData(sSql)
        If sOid = "" Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub AddNewFolder(ByVal sFolder As String)
        Dim fso, f, fc, nf
        fso = CreateObject("Scripting.FileSystemObject")
        f = fso.GetFolder(Server.MapPath("~/"))
        fc = f.SubFolders
        nf = fc.Add(sFolder)
    End Sub

    Private Sub CheckPRStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_pritemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pritemmststatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "' "
        End If 

        sSql = "SELECT COUNT(*) FROM QL_pritemmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND pritemmststatus='In Approval'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "' "
        End If 
    End Sub  

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "Select k.komponenmstoid, CONVERT(CHAR(20),k.crttime,103) crttime, i.itemcode, i.itemdesc, k.komponennote, k.komponenstatus, Round(itemqty, 2) QtyKatalog From QL_mstkomponen k INNER JOIN QL_mstitem i ON i.itemoid=k.itemoid Order By komponenmstoid"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_mstkomponen")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind() : lblViewInfo.Visible = False
    End Sub

    Private Sub BindItemNya()
        sSql = "SELECT m.itemoid, m.itemcode, m.itemdesc itemlongdesc, 0 itemlimitqty, 945 itemunitoid, 'BUAH' AS unit, 0.0 AS prqty, '' AS prdtlnote, '' AS prarrdatereq, m.pricelist itempricelist, HPP itemhpp FROM QL_mstitem m WHERE m.cmpcode='" & CompnyCode & "' AND m.itemflag='Aktif' AND stockflag='T' AND (m.itemcode LIKE '%" & TcharNoTrim(TxtKatalog.Text) & "%' OR m.itemdesc LIKE '%" & TcharNoTrim(TxtKatalog.Text) & "%' )ORDER BY m.itemcode"
        Session("Tblitem") = cKon.ambiltabel(sSql, "QL_mstitem")
        gvListItem.DataSource = Session("Tblitem")
        gvListItem.DataBind() : gvListItem.Visible = True
    End Sub

    Private Sub BindMatData()
        sSql = "SELECT 'False' AS checkvalue, m.itemoid, m.itemcode, m.itemdesc itemlongdesc, 0 itemlimitqty, 945 itemunitoid, 'BUAH' AS unit, 0 AS prqty, '' AS prdtlnote, '' AS prarrdatereq, 945 AS matunitoid, m.pricelist itempricelist, HPP itemhpp FROM QL_mstitem m WHERE m.cmpcode='" & CompnyCode & "' AND m.itemflag='Aktif' AND stockflag='T' ORDER BY m.itemcode"
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("QL_mstkomponendtl")
        dtlTable.Columns.Add("komponendtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemlongdesc", Type.GetType("System.String")) 
        dtlTable.Columns.Add("unit", Type.GetType("System.String"))
        dtlTable.Columns.Add("itempricelist", Type.GetType("System.Double"))
        dtlTable.Columns.Add("itemhpp", Type.GetType("System.Double"))
        dtlTable.Columns.Add("komponendtlqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("komponendtlunitoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("komponendtlnote", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        pritemdtlseq.Text = "1"
        If Session("TblDtl") Is Nothing = False Then
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            pritemdtlseq.Text = objTable.Rows.Count + 1
        End If
        i_u2.Text = "New Detail" : itemlongdesc.Text = ""
        itemoid.Text = "" : itemcode.Text = ""
        pritemqty.Text = "" : pritemunitoid.SelectedIndex = -1
        pritemdtlnote.Text = "" : gvPRDtl.SelectedIndex = -1
        btnSearchMat.Visible = True
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        sSql = "Select k.komponenmstoid, i.itemoid, k.crttime, i.itemcode, i.itemdesc, k.komponennote, k.komponenstatus, k.crtuser, k.updtime, k.upduser, k.itemqty From QL_mstkomponen k INNER JOIN QL_mstitem i ON i.itemoid=k.itemoid WHERE k.komponenmstoid=" & sOid
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        xreader = xCmd.ExecuteReader
        While xreader.Read
            katalogOid.Text = xreader("itemoid")
            TxtKatalog.Text = Trim(xreader("itemdesc").ToString)
            pritemmstoid.Text = Trim(xreader("komponenmstoid").ToString)
            pritemdate.Text = Format(xreader("crttime"), "dd/MM/yyyy")
            pritemmstnote.Text = Trim(xreader("komponennote").ToString)
            QtyKatalog.Text = ToMaskEdit(xreader("itemqty"), 3)
            DDLStatus.SelectedValue = Trim(xreader("komponenstatus").ToString)
            createuser.Text = Trim(xreader("crtuser").ToString)
            createtime.Text = Trim(xreader("crttime").ToString)
            upduser.Text = Trim(xreader("upduser").ToString)
            updtime.Text = Trim(xreader("updtime").ToString)
        End While
        xreader.Close() : conn.Close()

        sSql = "Select kd.komponendtlseq, i.itemoid, i.itemcode, i.itemdesc itemlongdesc, kd.komponendtlqty, kd.komponendtlunitoid, i.pricelist itempricelist, i.HPP itemhpp, 'BUAH' Unit, kd.komponendtlnote From QL_mstkomponendtl kd INNER JOIN QL_mstitem i ON i.itemoid=kd.komponendtlitemoid WHERE kd.komponenmstoid=" & sOid & " ORDER BY kd.komponendtlseq"
        Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_pritemdtl")
        Session("TblDtl") = dtTbl
        gvPRDtl.DataSource = dtTbl
        gvPRDtl.DataBind()
        TotalDetail() : ClearDetail()
    End Sub

    Private Sub PrintReport(ByVal sOid As String)
        Try
            report.Load(Server.MapPath(folderReport & "rptMstKomponen.rpt"))
            sSql = "Select k.komponenmstoid, i.itemoid, k.crttime, i.itemcode itemheaderkode, i.itemdesc, k.komponennote, k.komponenstatus, k.crtuser, k.updtime, k.upduser, kd.itemcode, itemlongdesc, kd.komponendtlqty, kd.itempricelist, kd.itemhpp From QL_mstkomponen k INNER JOIN QL_mstitem i ON i.itemoid=k.itemoid INNER JOIN (Select kd.komponendtlseq, kd.komponenmstoid, i.itemoid, i.itemcode, i.itemdesc itemlongdesc, kd.komponendtlqty, kd.komponendtlunitoid, i.pricelist itempricelist, i.HPP itemhpp, 'BUAH' Unit, kd.komponendtlnote From QL_mstkomponendtl kd INNER JOIN QL_mstitem i ON i.itemoid=kd.komponendtlitemoid) kd ON kd.komponenmstoid=k.komponenmstoid Where k.cmpcode='" & CompnyCode & "'"
 
            If sOid = "" Then
                sSql &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sSql &= " AND k.crttime>='" & FilterPeriod1.Text & " 00:00:00' AND k.crttime<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    If FilterDDLStatus.SelectedValue <> "All" Then
                        sSql &= " AND k.komponenstatus='" & FilterDDLStatus.SelectedValue & "'"
                    End If
                End If
                'If checkPagePermission("~\Master\MstKomponen.aspx", Session("SpecialAccess")) = False Then
                '    sSql &= " AND k.crtuser='" & Session("UserID") & "'"
                'End If
            Else
                sSql &= " AND k.komponenmstoid=" & sOid
            End If
            sSql &= " Order by i.itemcode ASC"
            Dim dtTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstkomponen")
            Dim dvTbl As DataView = dtTbl.DefaultView
            report.SetDataSource(dvTbl.ToTable)
            report.SetParameterValue("sUserID", Session("UserID")) 
            'cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MasterKomponen")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
    End Sub

    Private Sub SaveRecords(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByVal status As Integer)
        If IsInputValid() Then 
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            pritemdtloid.Text = GenerateID("QL_mstkomponendtl", CompnyCode)
            Dim isRegenOid As Boolean = False

            If Session("oid") = Nothing Or Session("oid") = "" Then
                If CheckDataExists("SELECT COUNT(*) FROM QL_mstkomponen WHERE komponenmstoid=" & pritemmstoid.Text) Then
                    pritemmstoid.Text = GenerateID("QL_mstkomponen", CompnyCode)
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_mstkomponen", "komponenmstoid", pritemmstoid.Text, "komponenstatus", updtime.Text, "AKTIF")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    'pritemmststatus.Text = "In Process"
                    Exit Sub
                End If
            End If

            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO [QL_mstkomponen] ([cmpcode], [komponenmstoid], [itemoid], [itemqty], [itemunitoid], [komponennote], [komponenstatus], [crtuser], [crttime], [upduser], [updtime]) VALUES ('" & CompnyCode & "', " & pritemmstoid.Text & " , " & katalogOid.Text & ", " & ToDouble(QtyKatalog.Text) & ", 945, '" & Tchar(pritemmstnote.Text) & "', '" & DDLStatus.SelectedValue & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid=" & pritemmstoid.Text & " WHERE tablename='QL_mstkomponen' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE [QL_mstkomponen] SET [itemoid] = " & katalogOid.Text & ", [itemqty] =" & ToDouble(QtyKatalog.Text) & ", [komponennote] = '" & Tchar(pritemmstnote.Text) & "', [komponenstatus] = '" & DDLStatus.SelectedValue & "', [upduser] = '" & Session("UserID") & "', [updtime] = CURRENT_TIMESTAMP WHERE komponenmstoid=" & pritemmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "DELETE FROM [QL_mstkomponendtl] WHERE komponenmstoid=" & pritemmstoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                Dim objTable As DataTable = Session("TblDtl")
                If Not Session("TblDtl") Is Nothing Then
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO [QL_mstkomponendtl] ([cmpcode], [komponendtloid], [komponenmstoid], [komponendtlseq], [komponendtlitemoid], [komponendtlqty], [komponendtlunitoid], [komponendtlnote], [upduser], [updtime]) VALUES ('" & CompnyCode & "', " & (C1 + CInt(pritemdtloid.Text)) & ", " & pritemmstoid.Text & ", " & C1 + 1 & ", " & objTable.Rows(C1).Item("itemoid") & ", " & ToDouble(objTable.Rows(C1).Item("komponendtlqty")) & ", " & pritemunitoid.SelectedValue & ", '" & Tchar(objTable.Rows(C1).Item("komponendtlnote")) & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(pritemdtloid.Text)) & " WHERE tablename='QL_mstkomponendtl' AND cmpcode='" & CompnyCode & "' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                conn.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString & sSql, 1)
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString & sSql, 1)
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString & sSql, 1)
                conn.Close()
                Exit Sub
            End Try
            Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
            'If isRegenOid Then
            '    Session("SavedInfo") = "- Maaf, nomer sudah di generate dengan nomer " & pritemmstoid.Text & ".<BR>"
            'End If

            'If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            '    showMessage(Session("SavedInfo"), 3)
            'Else
            '    If status = 0 Then
            '        Response.Redirect("~\Master\MstKomponen.aspx?oid=" & pritemmstoid.Text)
            '    Else
            '        Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
            '    End If
            'End If
        End If
    End Sub

    Private Sub TotalDetail()
        Dim TblDtl As DataTable = Session("TblDtl")
        Dim totalPrice As Double, totalHpp As Double = 0
        For L As Integer = 0 To TblDtl.Rows.Count - 1
            totalPrice += ToDouble(TblDtl.Rows(L)("itempricelist"))
            totalHpp += ToDouble(TblDtl.Rows(L)("itemhpp"))
        Next
        totalpricedtl.Text = ToMaskEdit(totalPrice, 3)
        TotalHPPDtl.Text = ToMaskEdit(totalHpp, 3)
    End Sub

#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Not (checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("~\Master\MstKomponen.aspx")
        End If

        Page.Title = CompnyName & " - Master Komponen"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")

        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckPRStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                pritemmstoid.Text = GenerateID("QL_mstkomponen", CompnyCode)
                pritemdate.Text = Format(GetServerTime(), "dd/MM/yyyy")
                btnDelete.Visible = False
                btnPrint.Visible = False
                TabContainer1.ActiveTabIndex = 0
                If Request.QueryString("back") <> "" Then
                    TabContainer1.ActiveTabIndex = 1
                End If
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvPRDtl.DataSource = dt
            gvPRDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If

        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & TcharNoTrim(FilterText.Text) & "%'"
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND k.cmpcode='" & CompnyCode & "' "
        End If

        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND k.crttime>='" & FilterPeriod1.Text & " 00:00:00' AND k.crttime<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "All" Then
                sSqlPlus &= " AND k.komponenstatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = 0
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If Session("CompnyCode") <> CompnyCode Then
            sSqlPlus &= " AND k.cmpcode='" & CompnyCode & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnPrintHdr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintHdr.Click
        PrintReport("")
    End Sub

    Protected Sub gvTRN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTRN.SelectedIndexChanged
        PrintReport(gvTRN.SelectedDataKey.Item("komponenmstoid").ToString)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub btnSearchMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchMat.Click
        Dim sMsg = ""
        If katalogOid.Text <> "" Then
            sMsg = "- Maaf, Pilih katalog.nya dulu, bosque..!!<br>"
        End If
        If QtyKatalog.Text = "0" Then
            sMsg = "- Maaf, isi qty katalog.nya dulu, bosque..!!<br>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        Session("TblMat") = Nothing : Session("TblMatView") = Nothing
        gvListMat.DataSource = Nothing : gvListMat.DataBind()
        tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub gvListMat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListMat.PageIndexChanging
        If UpdateCheckedMat2() Then
            gvListMat.PageIndex = e.NewPageIndex
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub gvListMat_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListMat.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cc As System.Web.UI.ControlCollection = e.Row.Cells(5).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    If ToDouble(CType(myControl, System.Web.UI.WebControls.TextBox).Text) <= 0 Then
                        CType(myControl, System.Web.UI.WebControls.TextBox).Text = ""
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindMatData()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Material data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    objView(0)("prqty") = 0
                    objView(0)("prdtlnote") = ""
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    dtTbl.Rows(C1)("prqty") = 0
                    dtTbl.Rows(C1)("prdtlnote") = ""
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected material data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected material data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lbShowData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShowData.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Please find material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If tbData.Text = "" Then
            Session("WarningListMat") = "Please fill total data to be shown!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        Else
            If ToDouble(tbData.Text) <= 0 Then
                Session("WarningListMat") = "Total data to be shown must be more than 0!"
                showMessage(Session("WarningListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat2() Then
            gvListMat.PageSize = Int(tbData.Text)
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    dtView.RowFilter = "checkvalue='True' AND prqty=0"
                    If dtView.Count > 0 Then
                        Session("WarningListMat") = "- Maaf, Qty.nya tidak boleh 0..!!"
                        showMessage(Session("WarningListMat"), 2)
                        dtView.RowFilter = ""
                        Exit Sub
                    End If

                    dtView.RowFilter = ""
                    dtView.RowFilter = "checkvalue='True'"
                    If Session("TblDtl") Is Nothing Then
                        CreateTblDetail()
                    End If

                    Dim objTable As DataTable
                    objTable = Session("TblDtl")
                    Dim dv As DataView = objTable.DefaultView
                    Dim objRow As DataRow
                    Dim counter As Integer = objTable.Rows.Count
                    For C1 As Integer = 0 To dtView.Count - 1
                        dv.RowFilter = "itemoid=" & dtView(C1)("itemoid") & ""
                        If dv.Count > 0 Then
                            dv.AllowEdit = True
                            dv(0)("itemoid") = dtView(C1)("itemoid")
                            dv(0)("itemcode") = dtView(C1)("itemcode").ToString
                            dv(0)("itemlongdesc") = dtView(C1)("itemlongdesc").ToString
                            dv(0)("itempricelist") = ToDouble(dtView(C1)("itempricelist"))
                            dv(0)("itemhpp") = ToDouble(dtView(C1)("itemhpp"))
                            dv(0)("komponendtlqty") = dtView(C1)("prqty")
                            dv(0)("komponendtlnote") = dtView(C1)("prdtlnote").ToString
                        Else
                            counter += 1
                            objRow = objTable.NewRow()
                            objRow("komponendtlseq") = counter
                            objRow("itemoid") = dtView(C1)("itemoid")
                            objRow("itemcode") = dtView(C1)("itemcode").ToString
                            objRow("itemlongdesc") = dtView(C1)("itemlongdesc").ToString
                            objRow("komponendtlqty") = dtView(C1)("prqty")
                            objRow("komponendtlunitoid") = dtView(C1)("matunitoid")
                            objRow("itempricelist") = ToDouble(dtView(C1)("itempricelist"))
                            objRow("itemhpp") = ToDouble(dtView(C1)("itemhpp"))
                            objRow("unit") = dtView(C1)("unit").ToString
                            objRow("komponendtlnote") = dtView(C1)("prdtlnote").ToString
                            objTable.Rows.Add(objRow)
                        End If
                        dv.RowFilter = ""
                    Next
                    dtView.RowFilter = ""
                    Session("TblDtl") = objTable
                    gvPRDtl.DataSource = objTable
                    gvPRDtl.DataBind()
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                    ClearDetail() : TotalDetail()
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some material data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable
            objTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "itemoid=" & itemoid.Text & ""
                If dv.Count > 0 Then
                    showMessage("This Data has been added before!", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
            Else
                dv.RowFilter = "itemoid=" & itemoid.Text & " AND komponendtlseq <>" & pritemdtlseq.Text & ""
                If dv.Count > 0 Then
                    showMessage("This Data has been added before!", 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                objRow("komponendtlseq") = objTable.Rows.Count + 1
            Else
                objRow = objTable.Rows(pritemdtlseq.Text - 1)
                objRow.BeginEdit()
            End If 

            objRow("itemoid") = itemoid.Text
            objRow("itemcode") = itemcode.Text
            objRow("itemlongdesc") = itemlongdesc.Text
            objRow("komponendtlqty") = pritemqty.Text
            objRow("komponendtlunitoid") = pritemunitoid.SelectedValue
            objRow("itempricelist") = GetScalar("Select pricelist from QL_mstitem Where itemoid = " & itemoid.Text & "")
            objRow("itemhpp") = GetScalar("Select hpp from QL_mstitem Where itemoid = " & itemoid.Text & "")
            objRow("unit") = pritemunitoid.SelectedItem.Text
            objRow("komponendtlnote") = pritemdtlnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvPRDtl.DataSource = objTable
            gvPRDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvPRDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPRDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 3)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 3)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 3)
        End If
    End Sub

    Protected Sub gvPRDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvPRDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("komponendtlseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvPRDtl.DataSource = objTable
        gvPRDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub gvPRDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPRDtl.SelectedIndexChanged
        Try
            pritemdtlseq.Text = gvPRDtl.SelectedDataKey.Item("komponendtlseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable
                objTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "komponendtlseq=" & pritemdtlseq.Text
                itemlongdesc.Text = dv.Item(0).Item("itemlongdesc").ToString
                itemoid.Text = dv.Item(0).Item("itemoid").ToString
                itemcode.Text = dv.Item(0).Item("itemcode").ToString
                pritemqty.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("komponendtlqty").ToString), 3)
                pritemunitoid.SelectedValue = dv.Item(0).Item("komponendtlunitoid").ToString
                pritemdtlnote.Text = dv.Item(0).Item("komponendtlnote").ToString
                dv.RowFilter = ""
            End If
            btnSearchMat.Visible = False
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        SaveRecords(sender, e, 0)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If pritemmstoid.Text.Trim = "" Then
            showMessage("- Maaf, !", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_mstkomponen", "komponenmstoid", pritemmstoid.Text, "komponenstatus", updtime.Text, "AKTIF")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2) 
                Exit Sub
            End If
        End If

        '-- Cek data --
        sSql = "Select transformno from QL_TransformItemMst Where womstoid=" & pritemmstoid.Text
        Dim tbl As DataTable = cKon.ambiltabel(sSql, "QL_TransformItemMst")
        If tbl.Rows.Count > 0 Then 
            showMessage("- Maaf, data komponen draft " & pritemmstoid.Text & " sudah pernah di pakai", 2)
            Exit Sub
        End If

        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_mstkomponen WHERE komponenmstoid=" & pritemmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_mstkomponendtl WHERE komponenmstoid=" & pritemmstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Master\MstKomponen.aspx?awal=true")
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        PrintReport(pritemmstoid.Text)
    End Sub

    Protected Sub BtnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearchItem.Click
        BindItemNya()
    End Sub

    Protected Sub gvListItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListItem.PageIndexChanging
        gvListItem.PageIndex = e.NewPageIndex
        gvListItem.DataSource = Session("Tblitem")
        gvListItem.DataBind()
    End Sub

    Protected Sub gvListItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListItem.SelectedIndexChanged
        katalogOid.Text = gvListItem.SelectedDataKey.Item("itemoid").ToString
        TxtKatalog.Text = gvListItem.SelectedDataKey.Item("itemlongdesc").ToString
        gvListItem.Visible = False
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        gvListItem.Visible = False
        TxtKatalog.Text = ""
        katalogOid.Text = ""
    End Sub
#End Region
End Class