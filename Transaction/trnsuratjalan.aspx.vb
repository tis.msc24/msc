Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure
Imports Koneksi
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Transaction_trnsuratjalan
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim rpt As ReportDocument
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imicon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imicon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imicon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblcaption.Text = strCaption : lblpopupmsg.Text = sMessage
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, True)
    End Sub

    Sub initTypeJob()
        Dim kCbg As String = ""
        If Session("UserID").ToString.ToUpper <> "ADMIN" Then
            kCbg &= "And g2.gencode='" & Session("branch_id").ToString & "'"
        End If
        sSql = "Select gencode ,g2.gendesc from ql_mstgen g2 where cmpcode = 'MSC' and gengroup = 'CABANG' " & kCbg & " ORDER BY g2.gencode"
        FillDDL(DdlCabang, sSql)
    End Sub

    Private Sub clearform()
        lblcurrentuser.Text = Session("UserID")
        ddlfilter.SelectedIndex = 0
        tbperiodstart.Text = Date.Now.ToString("dd/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        cbstatus.Checked = False
        cbperiod.Checked = False
        ddlfilterstatus.SelectedIndex = 0

        lbloid.Text = ""
        tbdatesend.Text = ""
        tbpic.Text = ""
        tbnopol.Text = ""
        tbdriver.Text = ""
        tbsupplier.Text = ""
        lblstatus.Text = "In Process"

        lblbarcode.Text = ""
        lblbarcodeview.Text = ""
        lblreqoid.Text = ""
        tbitemname.Text = ""
        tbitemqty.Text = 1
        tbitemtype.Text = ""
        tbitembrand.Text = ""
        lblitemtype.Text = ""
        lblitembrand.Text = ""
        tbnote.Text = ""
        ibclear.Visible = True
        lblstat.Text = "new"
        lblrow.Text = ""
        gvdaftar.DataSource = Nothing
        gvdaftar.DataBind()

        ibsaveinfo.Visible = True
        'ibpostinginfo.Visible = True
        ibdelete.Visible = False
        lblcreate.Text = "Create On <span style='font-weight: bold;'>" & Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & Session("UserID") & "</span>"

        gvdaftar.Columns(0).Visible = True
        gvdaftar.Columns(7).Visible = True

        ibsearchbarcode.Visible = True
        ibdelbarcode.Visible = True

        ibapprove.Visible = False
    End Sub

    Private Sub bindalldata()
        sSql = "SELECT a.sjloid, a.sjlno, a.sjlsend, a.sjlperson, a.sjlnopol, a.sjldriver, b.suppname, a.sjlstatus FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid AND a.cmpcode = '" & CompnyCode & "' ORDER BY a.sjloid DESC"
        Session("suratjalan") = Nothing
        Session("suratjalan") = cKon.ambiltabel(sSql, "suratjalan")
        gvdata.DataSource = Session("suratjalan")
        gvdata.DataBind()
    End Sub

    Private Sub finddata(ByVal filter As String, ByVal val As String, ByVal period As Boolean, ByVal datestart As String, ByVal dateend As String, ByVal stat As Boolean, ByVal status As String)
        sSql = "SELECT a.sjloid, a.sjlno, a.sjlsend, a.sjlperson, a.sjlnopol, a.sjldriver, b.suppname, a.sjlstatus FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid AND a.cmpcode = '" & CompnyCode & "'"

        If period = True Then
            Dim sdate, edate As New Date
            If Date.TryParseExact(datestart, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, sdate) And Date.TryParseExact(dateend, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, edate) Then
                Dim syear As Integer = Integer.Parse(sdate.Date.Year)
                Dim eyear As Integer = Integer.Parse(edate.Date.Year)
                If (syear < 1900 Or syear > 9999) Or (eyear < 1900 Or eyear > 9999) Then
                    showMessage("Tahun periode harus lebih besar dari 1900 dan lebih kecil dari 9999!", 2)
                    Exit Sub
                Else
                    If edate >= sdate Then
                        sSql &= " AND CONVERT(date, a.sjldate, 103) BETWEEN '" & sdate & "' AND '" & edate & "'"
                    Else
                        showMessage("Periode akhir harus lebih besar dari periode awal!", 2)
                        Exit Sub
                    End If
                End If
            Else
                showMessage("Periode tidak valid!", 2)
                Exit Sub
            End If
        End If

        If filter = "no" Then
            sSql &= " AND a.sjlno LIKE '%" & val & "%'"
        ElseIf filter = "pic" Then
            sSql &= " AND a.sjlperson LIKE '%" & val & "%'"
        ElseIf filter = "driver" Then
            sSql &= " AND a.sjldriver LIKE '%" & val & "%'"
        ElseIf filter = "supplier" Then
            sSql &= " AND b.suppname LIKE '%" & val & "%'"
        End If

        If stat = True Then
            If status = "inprocess" Then
                sSql &= " AND LOWER(a.sjlstatus) = 'in process'"
            ElseIf status = "send" Then
                sSql &= " AND LOWER(a.sjlstatus) = 'send'"
            End If
        End If

        sSql &= " ORDER BY a.sjloid DESC"
        Session("suratjalan") = cKon.ambiltabel(sSql, "suratjalan")
        gvdata.DataSource = Session("suratjalan")
        gvdata.DataBind()
    End Sub

    Private Sub fillform()
        lblcurrentuser.Text = Session("UserID")
        Try
            Dim oid As Integer = Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT a.sjloid, a.branch_code, a.sjlno, a.sjldate, a.sjlsend, 0 personoid, c.userid personnip, c.USERNAME  personname, a.sjlnopol, a.sjldriver, a.sjlsupp, b.suppname, a.sjlstatus, a.createuser, a.createtime, a.upduser, a.updtime FROM ql_trnsjalan a INNER JOIN ql_mstsupp b ON a.sjlsupp = b.suppoid INNER JOIN QL_MSTPROF  c ON a.sjlperson = c.USERID  where a.cmpcode = '" & CompnyCode & "' AND a.sjloid = " & oid & ""
            xCmd.CommandText = sSql
            xreader = xCmd.ExecuteReader
            If xreader.HasRows Then
                While xreader.Read
                    lbloid.Text = xreader("sjloid")
                    tbno.Text = xreader("sjlno").ToString
                    tbdatesend.Text = Format(xreader("sjlsend"), "dd/MM/yyyy")
                    tbpic.Text = xreader("personname").ToString
                    'lblpic.Text = xreader("personoid").ToString
                    lblapprovaluser.Text = xreader("personnip").ToString
                    tbnopol.Text = xreader("sjlnopol").ToString
                    tbdriver.Text = xreader("sjldriver").ToString
                    tbsupplier.Text = xreader("suppname").ToString
                    lblsupplieroid.Text = xreader("sjlsupp")
                    lblstatus.Text = xreader("sjlstatus").ToString
                    DdlCabang.SelectedValue = xreader("branch_code").ToString.Trim
                    If lblstatus.Text <> "Send" Then
                        If lblstatus.Text = "In Approval" Then
                            ibsaveinfo.Visible = False
                            'ibpostinginfo.Visible = False
                            ibdelete.Visible = False
                            ibprint.Visible = False
                            ibaddtolist.Visible = False
                            ibclear.Visible = False
                            gvdaftar.Columns(0).Visible = False
                            gvdaftar.Columns(7).Visible = False
                            ibsearchbarcode.Visible = False
                            ibdelbarcode.Visible = False
                            ibapprove.Visible = False
                        Else
                            ibsaveinfo.Visible = True
                            'ibpostinginfo.Visible = True
                            ibdelete.Visible = True
                            ibprint.Visible = False
                            ibaddtolist.Visible = True
                            ibclear.Visible = True
                            gvdaftar.Columns(0).Visible = True
                            gvdaftar.Columns(7).Visible = True
                            ibsearchbarcode.Visible = True
                            ibdelbarcode.Visible = True
                            ibapprove.Visible = True
                        End If
                    Else
                        ibsaveinfo.Visible = False
                        'ibpostinginfo.Visible = False
                        ibdelete.Visible = False
                        ibprint.Visible = True
                        ibaddtolist.Visible = False
                        ibclear.Visible = False
                        gvdaftar.Columns(0).Visible = False
                        gvdaftar.Columns(7).Visible = False
                        ibsearchbarcode.Visible = False
                        ibdelbarcode.Visible = False
                        ibapprove.Visible = False
                    End If
                    If Not IsDBNull(xreader("upduser")) Or xreader("upduser").ToString <> "" Then
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("updtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("upduser").ToString & "</span>"
                    Else
                        lblcreate.Text = "Last Update On <span style='font-weight: bold;'>" & Format(xreader("createtime"), "dd/MM/yyyy hh:mm:ss tt") & "</span> By <span style='font-weight: bold;'>" & xreader("createuser").ToString & "</span>"
                    End If
                End While
                sSql = "SELECT a.sjldtloid, a.reqoid, a.barcode, a.reqitemname, a.reqitemtype, c.gendesc as type, a.reqitembrand, d.gendesc as brand, a.itemqty, a.note, e.sstarttime as date FROM ql_trnsjalandtl a INNER JOIN ql_trnsjalan b ON a.sjloid = b.sjloid INNER JOIN ql_mstgen c ON a.reqitemtype = c.genoid INNER JOIN ql_mstgen d ON a.reqitembrand = d.genoid INNER JOIN ql_trnservices e ON a.reqoid = e.mstreqoid AND a.cmpcode = '" & CompnyCode & "' AND a.sjloid = " & oid & " ORDER BY a.sjldtloid"
                Dim dtable As DataTable = cKon.ambiltabel(sSql, "suratjalandetail")
                Session("suratjalandetail") = dtable
                gvdaftar.DataSource = Session("suratjalandetail")
                gvdaftar.DataBind()
            End If
            tbitemqty.Text = 1

            If Not xreader.IsClosed Then
                xreader.Close()
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 2)
        Finally
            If Not xreader Is Nothing Then
                If Not xreader.IsClosed Then
                    xreader.Close()
                End If
            End If
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub bindsupplierdata()
        sSql = "SELECT suppoid, suppcode, suppname FROM ql_mstsupp WHERE cmpcode = '" & CompnyCode & "' /*and supptype = 'Service'*/ ORDER BY suppoid"
        Session("DataSupplierPop") = Nothing
        Session("DataSupplierPop") = cKon.ambiltabel(sSql, "supplierpop")
        gvpopsupplier.DataSource = Session("DataSupplierPop")
        gvpopsupplier.DataBind()
    End Sub

    Private Sub findpopdata(ByVal param As String, ByVal val As String)
        sSql = "SELECT suppoid, suppcode, suppname FROM ql_mstsupp WHERE cmpcode = '" & CompnyCode & "'"
        If param = "kode" Then
            sSql &= " AND suppcode LIKE '%" & val & "%'"
        ElseIf param = "nama" Then
            sSql &= " AND suppname LIKE '%" & val & "%'"
        End If
        sSql &= " ORDER BY suppoid"
        Session("datasupplierpop") = Nothing
        Session("datasupplierpop") = cKon.ambiltabel(sSql, "datasupplierpop")
        gvpopsupplier.DataSource = Session("datasupplierpop")
        gvpopsupplier.DataBind()
    End Sub

    Private Sub bindorderdata()
        sSql = "SELECT a.reqoid, a.barcode, b.custname, a.reqitemname, a.reqitemtype, c.gendesc as type, a.reqitembrand, d.gendesc as brand, e.updtime as sstarttime FROM ql_trnrequest a INNER JOIN ql_mstcust b ON a.reqcustoid = b.custoid INNER JOIN ql_mstgen c ON a.reqitemtype = c.genoid INNER JOIN ql_mstgen d ON a.reqitembrand = d.genoid INNER JOIN ql_trnservices e ON a.reqoid = e.mstreqoid where a.cmpcode = '" & CompnyCode & "' and a.branch_code = '" & DdlCabang.SelectedValue & "' AND LOWER(e.sflag) = 'out' AND LOWER(a.reqstatus) = 'checkout' AND a.reqoid NOT IN (SELECT reqoid FROM ql_trnsjalandtl) ORDER BY a.reqoid"
        Session("datarequestpop") = Nothing
        Session("datarequestpop") = cKon.ambiltabel(sSql, "datarequestpop")
        gvpoporder.DataSource = Session("datarequestpop")
        gvpoporder.DataBind()
    End Sub

    Private Sub findpopdataorder(ByVal param As String, ByVal val As String)
        sSql = "SELECT a.reqoid, a.barcode, b.custname, a.reqitemname, a.reqitemtype, c.gendesc as type, a.reqitembrand, d.gendesc as brand, e.updtime as sstarttime FROM ql_trnrequest a INNER JOIN ql_mstcust b ON a.reqcustoid = b.custoid INNER JOIN ql_mstgen c ON a.reqitemtype = c.genoid INNER JOIN ql_mstgen d ON a.reqitembrand = d.genoid INNER JOIN ql_trnservices e ON a.reqoid = e.mstreqoid AND a.cmpcode = '" & CompnyCode & "' AND LOWER(e.sflag) = 'out' AND LOWER(a.reqstatus) = 'checkout' AND a.reqoid NOT IN (SELECT reqoid FROM ql_trnsjalandtl)"
        If param = "barcode" Then
            sSql &= " AND a.barcode LIKE '%" & val & "%'"
        ElseIf param = "namacustomer" Then
            sSql &= " AND b.custname LIKE '%" & val & "%'"
        ElseIf param = "namabarang" Then
            sSql &= " AND a.reqitemname LIKE '%" & val & "%'"
        End If
        sSql &= " ORDER BY a.reqoid"
        Session("datarequestpop") = Nothing
        Session("datarequestpop") = cKon.ambiltabel(sSql, "datarequestpop")
        gvpoporder.DataSource = Session("datarequestpop")
        gvpoporder.DataBind()
    End Sub

    Private Sub addtolist()
        If Session("suratjalandetail") Is Nothing Then
            createlist()
        End If
        Dim otable As DataTable = Session("suratjalandetail")
        Dim dv As DataView = otable.DefaultView
        dv.RowFilter = ""
        dv.RowFilter = "barcode = '" & lblbarcode.Text.Trim & "'"
        If dv.Count <= 0 Then
            Dim orow As DataRow
            orow = otable.NewRow
            orow("sjldtloid") = 0
            orow("reqoid") = Integer.Parse(lblreqoid.Text.Trim)
            orow("barcode") = lblbarcode.Text.Trim
            orow("reqitemname") = tbitemname.Text.Trim
            orow("reqitemtype") = Integer.Parse(lblitemtype.Text.Trim)
            orow("type") = tbitemtype.Text.Trim
            orow("reqitembrand") = Integer.Parse(lblitembrand.Text.Trim)
            orow("brand") = tbitembrand.Text.Trim
            Dim qty As Integer = 1
            If Integer.TryParse(tbitemqty.Text.Replace(",", "").Trim, qty) Then
                orow("itemqty") = qty
            End If
            orow("note") = tbnote.Text.Trim

            Dim sstarttime As New Date
            If Date.TryParseExact(lblstarttime.Text, "dd/MM/yyyy", Nothing, DateTimeStyles.None, sstarttime) = False Then
                showMessage("Tanggal rencana servis tidak valid!", 2)
                Exit Sub
            End If
            
            orow("date") = sstarttime

            otable.Rows.Add(orow)
            dv.RowFilter = ""
            Session("suratjalandetail") = Nothing
            Session("suratjalandetail") = otable
            gvdaftar.DataSource = Nothing
            gvdaftar.DataSource = otable
            gvdaftar.DataBind()
            tbnote.Text = ""
            dv.RowFilter = ""
        Else
            dv.RowFilter = ""
            showMessage("Barcode telah tercatat di dalam daftar!", 2)
            tbnote.Text = ""
            Exit Sub
        End If
    End Sub

    Private Sub editlist()
        Dim index As Integer = -1
        If Integer.TryParse(lblrow.Text.Trim, index) Then
            Dim otable As DataTable = Session("suratjalandetail")
            Dim orow As DataRow
            orow = otable.Rows(index)
            orow.BeginEdit()
            Dim qty As Integer = 1
            If Integer.TryParse(tbitemqty.Text.Trim, qty) Then
                orow.Item("itemqty") = qty
            End If
            orow("note") = tbnote.Text.Trim
            orow.EndEdit()

            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                sSql = "UPDATE ql_trnsjalandtl SET note = '" & Tchar(orow.Item("note")) & "' WHERE sjldtloid = " & orow.Item("sjldtloid") & ""
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch ex As Exception
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try

            Session("suratjalandetail") = Nothing
            Session("suratjalandetail") = otable
            gvdaftar.DataSource = Nothing
            gvdaftar.DataSource = otable
            gvdaftar.DataBind()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub createlist()
        Dim dtable As New DataTable
        dtable.Columns.Add("sjldtloid", Type.GetType("System.Int32"))
        dtable.Columns.Add("reqoid", Type.GetType("System.Int32"))
        dtable.Columns.Add("barcode", Type.GetType("System.String"))
        dtable.Columns.Add("reqitemname", Type.GetType("System.String"))
        dtable.Columns.Add("reqitemtype", Type.GetType("System.Int32"))
        dtable.Columns.Add("type", Type.GetType("System.String"))
        dtable.Columns.Add("reqitembrand", Type.GetType("System.Int32"))
        dtable.Columns.Add("brand", Type.GetType("System.String"))
        dtable.Columns.Add("itemqty", Type.GetType("System.Int32"))
        dtable.Columns.Add("note", Type.GetType("System.String"))
        dtable.Columns.Add("date", Type.GetType("System.DateTime"))
        Session("suratjalandetail") = dtable
    End Sub

    Private Sub createdel()
        Dim objTable As New DataTable
        objTable.Columns.Add("sjldtloid", Type.GetType("System.Int32"))
        Session("delete") = objTable
    End Sub

    Private Sub removedetail()
        If Session("delete") Is Nothing Then
            createdel()
        End If
        Dim oTable As DataTable = Session("delete")
        Dim oRow As DataRow
        Dim objTable As DataTable
        Dim objRow() As DataRow

        objTable = Session("suratjalandetail")
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length() > 0 Then
            For i As Integer = objRow.Length() - 1 To 0 Step -1
                If cKon.getCheckBoxValue(i, 1, gvdaftar) = True Then
                    objTable.Rows.Remove(objRow(i))
                    oRow = oTable.NewRow
                    oRow("sjldtloid") = gvdaftar.DataKeys(i).Values("sjldtloid")
                    oTable.Rows.Add(oRow)
                End If
            Next
            Session("delete") = Nothing
            Session("delete") = oTable
            Session("suratjalandetail") = Nothing
            Session("suratjalandetail") = objTable
            gvdaftar.DataSource = objTable
            gvdaftar.DataBind()
        End If
    End Sub

    Protected Sub getdetail(ByVal sender As Object, ByVal e As EventArgs)
        lblstat.Text = "edit"
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        lblrow.Text = gvr.RowIndex
        lblbarcode.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("barcode")
        lblreqoid.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("reqoid")
        tbitemname.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("reqitemname")
        tbitemqty.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("itemqty")
        lblitemtype.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("reqitemtype")
        lblitembrand.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("reqitembrand")
        tbitemtype.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("type")
        tbitembrand.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("brand")
        tbnote.Text = gvdaftar.DataKeys(gvr.RowIndex).Values("note")
    End Sub

    Protected Sub deleterow(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtn As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lbtn.NamingContainer, GridViewRow)
        If Session("delete") Is Nothing Then
            createdel()
        End If
        Dim sjldtloid As Int32 = gvdaftar.DataKeys(gvr.RowIndex).Values("sjldtloid")
        Dim dtab As DataTable = Session("delete")
        Dim drow As DataRow = dtab.NewRow
        drow("sjldtloid") = sjldtloid
        dtab.Rows.Add(drow)
        Session("delete") = dtab
        Dim otab As DataTable = Session("suratjalandetail")
        otab.Rows.RemoveAt(gvr.RowIndex)
        Session("suratjalandetail") = otab
        gvdaftar.DataSource = Session("suratjalandetail")
        gvdaftar.DataBind()
    End Sub

    Private Sub printouttopdf(ByVal oid As Integer)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "SELECT sjlno FROM ql_trnsjalan WHERE sjloid = " & oid & ""
            xCmd.CommandText = sSql
            Dim res As String = xCmd.ExecuteScalar

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            rpt = New ReportDocument
            rpt.Load(Server.MapPath("~\Report\notasuratjalan.rpt"))
            cProc.SetDBLogonForReport(rpt, ".", "QL_MSC")
            rpt.SetParameterValue("sWhere", " AND a.cmpcode = '" & CompnyCode & "' AND a.sjloid = " & oid & "")
            Response.Buffer = False
            Response.ClearHeaders()
            Response.ClearContent()
            rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "SJ_" & res)
            rpt.Close() : rpt.Dispose()
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
        End Try
    End Sub
#End Region

#Region "Function"
    Private Function initsjlno() As String
        Dim sjlno As String = ""

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim code As String = ""
            sSql = "Select genother1 from ql_mstgen where gengroup='cabang' and gencode='" & DdlCabang.SelectedValue & "'"
            xCmd.CommandText = sSql : Dim sCabang As String = xCmd.ExecuteScalar()
            code = "SJ/" & sCabang & "/" & Format(GetServerTime, "yy/MM/dd") & "/"

            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sjlno,4) AS INT)),0) FROM QL_TRNSJALAN WHERE sjlno LIKE '" & code & "%'"
            xCmd.CommandText = sSql
            Dim sequence As String = Format((xCmd.ExecuteScalar + 1), "0000")
            sjlno = code & sequence
            tbno.Text = sjlno

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

        Return sjlno
    End Function

    Private Function saverecord() As String
        Dim result As String = ""

        If tbno.Text.Trim = "" Then
            result = "Nomor surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbdatesend.Text.Trim = "" Then
            result = "Tanggal pengiriman surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbpic.Text.Trim = "" Then
            result = "PIC surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
            'ElseIf tbnopol.Text.Trim = "" Then
            '    result = "Nomor Expedisi tidak boleh kosong!"
            '    If MultiView1.ActiveViewIndex = 1 Then
            '        MultiView1.ActiveViewIndex = 0
            '    End If
            '    Return result
        ElseIf tbdriver.Text.Trim = "" Then
            result = "Nama driver tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbsupplier.Text.Trim = "" Then
            result = "Nama supplier tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf gvdaftar.Rows.Count <= 0 Then
            result = "Detail surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 0 Then
            '    MultiView1.ActiveViewIndex = 1
            'End If
            Return result
        Else
            Dim sjno As String = initsjlno()
            Dim senddate As New Date
            If Date.TryParseExact(tbdatesend.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, senddate) Then
                'senddate = senddate
            Else
                result = "Tanggal Pengiriman tidak valid!"
                Return result
            End If
            'Dim pic As String = Tchar(tbpic.Text.Trim)
            Dim pic As String = Tchar(lblapprovaluser.Text.Trim)
            Dim nopol As String = Tchar(tbnopol.Text.Trim)
            Dim driver As String = Tchar(tbdriver.Text.Trim)
            Dim supplier As Integer = 0
            If Integer.TryParse(lblsupplieroid.Text.Trim, supplier) Then
                supplier = supplier
            End If
            Dim status As String = lblstatus.Text

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                If Not Session("suratjalandetail") Is Nothing Then
                    Dim countstrtitle = "Barcode<br />"
                    Dim countstr As String = ""
                    Dim countdtab As DataTable = Session("suratjalandetail")
                    If countdtab.Rows.Count > 0 Then
                        For i As Integer = 0 To countdtab.Rows.Count - 1
                            sSql = "SELECT COUNT(reqoid) FROM ql_trnsjalandtl WHERE reqoid = " & countdtab.Rows(i).Item("reqoid") & ""
                            xCmd.CommandText = sSql
                            Dim countres As Integer = xCmd.ExecuteScalar()
                            If countres > 0 Then
                                countstr &= "- " & countdtab.Rows(i).Item("barcode").ToString & "<br />"
                            End If
                        Next
                    End If
                    If countstr <> "" Then
                        countstr &= "Tidak bisa disimpan karena telah tercatat di surat jalan lainnya!"
                        result = countstrtitle & countstr
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        Return result
                    End If

                End If

                sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNSJALAN' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                Dim sjoid As Integer = xCmd.ExecuteScalar + 1

                sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNSJALANDTL' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql
                Dim sjdtloid As Integer = xCmd.ExecuteScalar + 1

                sSql = "INSERT INTO ql_trnsjalan (cmpcode, branch_code, sjloid, sjlno, sjldate, sjlsend, sjlsupp, sjlperson, sjldriver, sjlnopol, sjlstatus, createuser, upduser, updtime) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & sjoid & ", '" & sjno & "', '" & DateTime.Now & "', '" & senddate & "', " & supplier & ", '" & pic & "', '" & driver & "', '" & nopol & "', '" & status & "', '" & lblcurrentuser.Text & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid = " & sjoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNSJALAN'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If Not Session("suratjalandetail") Is Nothing Then
                    Dim dtab As DataTable = Session("suratjalandetail")
                    If dtab.Rows.Count > 0 Then
                        For i As Integer = 0 To dtab.Rows.Count - 1
                            sSql = "INSERT INTO ql_trnsjalandtl (cmpcode,branch_code, sjldtloid, sjloid, reqoid, barcode, reqitemname, reqitemtype, reqitembrand, itemqty, note, upduser, updtime) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & sjdtloid & ", " & sjoid & ", " & dtab.Rows(i).Item("reqoid") & ", '" & Tchar(dtab.Rows(i).Item("barcode").ToString.Trim) & "', '" & Tchar(dtab.Rows(i).Item("reqitemname").ToString.Trim) & "', " & dtab.Rows(i).Item("reqitemtype") & ", " & dtab.Rows(i).Item("reqitembrand") & ", " & dtab.Rows(i).Item("itemqty") & ", '" & Tchar(dtab.Rows(i).Item("note").ToString.Trim) & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sjdtloid = sjdtloid + 1
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid = " & sjdtloid - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNSJALANDTL'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                End If

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If
        Return result
    End Function

    Private Function updaterecord(ByVal sjlstatus As String) As String
        Dim result As String = ""

        If tbno.Text.Trim = "" Then
            result = "Nomor surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbdatesend.Text.Trim = "" Then
            result = "Tanggal pengiriman surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbpic.Text.Trim = "" Then
            result = "PIC surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
            'ElseIf tbnopol.Text.Trim = "" Then
            '    result = "Nomor Expedisi tidak boleh kosong!"
            '    If MultiView1.ActiveViewIndex = 1 Then
            '        MultiView1.ActiveViewIndex = 0
            '    End If
            '    Return result
        ElseIf tbdriver.Text.Trim = "" Then
            result = "Nama driver tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbsupplier.Text.Trim = "" Then
            result = "Nama supplier tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf gvdaftar.Rows.Count <= 0 Then
            result = "Detail surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 0 Then
            '    MultiView1.ActiveViewIndex = 1
            'End If
            Return result
        Else
            Dim oid As Integer = 0
            If Integer.TryParse(lbloid.Text, oid) Then
                oid = oid
            Else
                result = "ID surat jalan tidak valid!"
                Return result
            End If
            Dim senddate As New Date
            If Date.TryParseExact(tbdatesend.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, senddate) Then
                'senddate = senddate
            Else
                result = "Tanggal Pengiriman tidak valid!"
                Return result
            End If
            'Dim pic As String = Tchar(tbpic.Text.Trim)
            Dim pic As String = Tchar(lblapprovaluser.Text.Trim)
            Dim nopol As String = Tchar(tbnopol.Text.Trim)
            Dim driver As String = Tchar(tbdriver.Text.Trim)
            Dim supplier As Integer = 0
            If Integer.TryParse(lblsupplieroid.Text.Trim, supplier) Then
                supplier = supplier
            End If
            Dim status As String = lblstatus.Text

            If sjlstatus = "In Approval" Then
                status = sjlstatus
            End If

            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                If Not Session("suratjalandetail") Is Nothing Then
                    Dim countstrtitle = "Barcode<br />"
                    Dim countstr As String = ""
                    Dim countdtab As DataTable = Session("suratjalandetail")
                    If countdtab.Rows.Count > 0 Then
                        For i As Integer = 0 To countdtab.Rows.Count - 1
                            sSql = "SELECT COUNT(reqoid) FROM ql_trnsjalandtl WHERE reqoid = " & countdtab.Rows(i).Item("reqoid") & " AND sjloid <> " & oid & ""
                            xCmd.CommandText = sSql
                            Dim countres As Integer = xCmd.ExecuteScalar()
                            If countres > 0 Then
                                countstr &= "- " & countdtab.Rows(i).Item("barcode").ToString & "<br />"
                            End If
                        Next
                    End If
                    If countstr <> "" Then
                        countstr &= "Tidak bisa disimpan karena telah tercatat di surat jalan lainnya!"
                        result = countstrtitle & countstr
                        otrans.Rollback()
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        Return result
                    End If
                End If

                sSql = "UPDATE ql_trnsjalan SET sjlsend = '" & senddate & "', sjlsupp = " & supplier & ", sjlperson = '" & pic & "', sjldriver = '" & driver & "', sjlnopol = '" & nopol & "', sjlstatus = '" & status & "', upduser = '" & lblcurrentuser.Text & "', updtime = '" & DateTime.Now & "' WHERE sjloid = " & oid & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If Not Session("suratjalandetail") Is Nothing Then
                    Dim dtab As DataTable = Session("suratjalandetail")
                    sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNSJALANDTL' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : Dim sjdtloid As Integer = xCmd.ExecuteScalar + 1

                    If dtab.Rows.Count > 0 Then
                        For i As Integer = 0 To dtab.Rows.Count - 1
                            If dtab.Rows(i).Item("sjldtloid") = 0 Then
                                sSql = "INSERT INTO ql_trnsjalandtl (cmpcode,branch_code, sjldtloid, sjloid, reqoid, barcode, reqitemname, reqitemtype, reqitembrand, itemqty, note, upduser, updtime) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & sjdtloid & ", " & oid & ", " & dtab.Rows(i).Item("reqoid") & ", '" & Tchar(dtab.Rows(i).Item("barcode").ToString.Trim) & "', '" & Tchar(dtab.Rows(i).Item("reqitemname").ToString.Trim) & "', " & dtab.Rows(i).Item("reqitemtype") & ", " & dtab.Rows(i).Item("reqitembrand") & ", " & dtab.Rows(i).Item("itemqty") & ", '" & Tchar(dtab.Rows(i).Item("note").ToString.Trim) & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sjdtloid = sjdtloid + 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid = " & sjdtloid - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNSJALANDTL'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If Not Session("delete") Is Nothing Then
                    Dim objTable As DataTable = Session("delete")
                    For i As Integer = 0 To objTable.Rows.Count - 1
                        Dim sjdtloid As Integer = objTable.Rows(i).Item("sjldtloid")
                        If sjdtloid <> 0 Then
                            sSql = "DELETE FROM ql_trnsjalandtl WHERE sjldtloid=" & objTable.Rows(i).Item("sjldtloid") & ""
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If
                    Next
                End If

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If

        Return result
    End Function

    Private Function postingrecord() As String
        Dim result As String = ""

        If tbno.Text.Trim = "" Then
            result = "Nomor surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbdatesend.Text.Trim = "" Then
            result = "Tanggal pengiriman surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbpic.Text.Trim = "" Then
            result = "PIC surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
            'ElseIf tbnopol.Text.Trim = "" Then
            '    result = "Nomor Expedisi tidak boleh kosong!"
            '    If MultiView1.ActiveViewIndex = 1 Then
            '        MultiView1.ActiveViewIndex = 0
            '    End If
            '    Return result
        ElseIf tbdriver.Text.Trim = "" Then
            result = "Nama driver tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf tbsupplier.Text.Trim = "" Then
            result = "Nama supplier tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Return result
        ElseIf gvdaftar.Rows.Count <= 0 Then
            result = "Detail surat jalan tidak boleh kosong!"
            'If MultiView1.ActiveViewIndex = 0 Then
            '    MultiView1.ActiveViewIndex = 1
            'End If
            Return result
        Else
            Dim oid As Integer = 0
            Dim sjno As String = "" : Dim sjoid As Integer = 0
            If lbloid.Text <> "" Then
                If Integer.TryParse(lbloid.Text, oid) Then
                    oid = oid
                Else
                    result = "ID surat jalan tidak valid!"
                    Return result
                End If
            Else
                sjno = initsjlno()
            End If
            Dim senddate As New Date
            If Date.TryParseExact(tbdatesend.Text.Trim, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, senddate) Then
                'senddate = senddate
            Else
                result = "Tanggal Pengiriman tidak valid!"
                Return result
            End If
            'Dim pic As String = Tchar(tbpic.Text.Trim)
            Dim pic As String = Tchar(lblapprovaluser.Text.Trim)
            Dim nopol As String = Tchar(tbnopol.Text.Trim)
            Dim driver As String = Tchar(tbdriver.Text.Trim)
            Dim supplier As Integer = 0
            If Integer.TryParse(lblsupplieroid.Text.Trim, supplier) Then
                supplier = supplier
            End If
            Dim status As String = "In Approval"
            Dim otrans As SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            otrans = conn.BeginTransaction
            xCmd.Transaction = otrans
            Try
                If oid = 0 Then
                    If Not Session("suratjalandetail") Is Nothing Then
                        Dim countstrtitle = "Barcode<br />"
                        Dim countstr As String = ""
                        Dim countdtab As DataTable = Session("suratjalandetail")
                        If countdtab.Rows.Count > 0 Then
                            For i As Integer = 0 To countdtab.Rows.Count - 1
                                sSql = "SELECT COUNT(reqoid) FROM ql_trnsjalandtl WHERE reqoid = " & countdtab.Rows(i).Item("reqoid") & ""
                                xCmd.CommandText = sSql
                                Dim countres As Integer = xCmd.ExecuteScalar()
                                If countres > 0 Then
                                    countstr &= "- " & countdtab.Rows(i).Item("barcode").ToString & "<br />"
                                End If
                            Next
                        End If
                        If countstr <> "" Then
                            countstr &= "Tidak bisa disimpan karena telah tercatat di surat jalan lainnya!"
                            result = countstrtitle & countstr
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Return result
                        End If
                    End If

                    sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNSJALAN' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : sjoid = xCmd.ExecuteScalar + 1

                    sSql = "INSERT INTO ql_trnsjalan (cmpcode,branch_code, sjloid, sjlno, sjldate, sjlsend, sjlsupp, sjlperson, sjldriver, sjlnopol, sjlstatus, createuser, upduser, updtime) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & sjoid & ", '" & sjno & "', '" & DateTime.Now & "', '" & senddate & "', " & supplier & ", '" & pic & "', '" & driver & "', '" & nopol & "', '" & status & "', '" & lblcurrentuser.Text & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstoid SET lastoid = " & sjoid & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNSJALAN'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    If Not Session("suratjalandetail") Is Nothing Then
                        Dim countstrtitle = "Barcode<br />"
                        Dim countstr As String = ""
                        Dim countdtab As DataTable = Session("suratjalandetail")
                        If countdtab.Rows.Count > 0 Then
                            For i As Integer = 0 To countdtab.Rows.Count - 1
                                sSql = "SELECT COUNT(reqoid) FROM ql_trnsjalandtl WHERE reqoid = " & countdtab.Rows(i).Item("reqoid") & " AND sjloid <> " & oid & ""
                                xCmd.CommandText = sSql
                                Dim countres As Integer = xCmd.ExecuteScalar()
                                If countres > 0 Then
                                    countstr &= "- " & countdtab.Rows(i).Item("barcode").ToString & "<br />"
                                End If
                            Next
                        End If
                        If countstr <> "" Then
                            countstr &= "Tidak bisa disimpan karena telah tercatat di surat jalan lainnya!"
                            result = countstrtitle & countstr
                            otrans.Rollback()
                            If conn.State = ConnectionState.Open Then
                                conn.Close()
                            End If
                            Return result
                        End If
                    End If

                    sSql = "UPDATE ql_trnsjalan SET sjlsend = '" & senddate & "', sjlsupp = " & supplier & ", sjlperson = '" & pic & "', sjldriver = '" & driver & "', sjlnopol = '" & nopol & "', sjlstatus = '" & status & "', upduser = '" & lblcurrentuser.Text & "', updtime = '" & DateTime.Now & "' WHERE sjloid = " & oid & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                If Not Session("suratjalandetail") Is Nothing Then
                    Dim dtab As DataTable = Session("suratjalandetail")
                    sSql = "SELECT ISNULL(lastoid,0) FROM ql_mstoid WHERE tablename = 'QL_TRNSJALANDTL' AND cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql : Dim sjdtloid As Integer = xCmd.ExecuteScalar + 1

                    If oid <> 0 Then
                        sjoid = oid
                    End If

                    If dtab.Rows.Count > 0 Then
                        For i As Integer = 0 To dtab.Rows.Count - 1
                            If dtab.Rows(i).Item("sjldtloid") = 0 Then
                                sSql = "INSERT INTO ql_trnsjalandtl (cmpcode,branch_code, sjldtloid, sjloid, reqoid, barcode, reqitemname, reqitemtype, reqitembrand, itemqty, note, upduser, updtime) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & sjdtloid & ", " & sjoid & ", " & dtab.Rows(i).Item("reqoid") & ", '" & Tchar(dtab.Rows(i).Item("barcode").ToString.Trim) & "', '" & Tchar(dtab.Rows(i).Item("reqitemname").ToString.Trim) & "', " & dtab.Rows(i).Item("reqitemtype") & ", " & dtab.Rows(i).Item("reqitembrand") & ", " & dtab.Rows(i).Item("itemqty") & ", '" & Tchar(dtab.Rows(i).Item("note").ToString.Trim) & "', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "')"
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                sjdtloid = sjdtloid + 1
                            End If
                        Next
                        sSql = "UPDATE QL_mstoid SET lastoid = " & sjdtloid - 1 & " WHERE cmpcode = '" & CompnyCode & "' AND tablename = 'QL_TRNSJALANDTL'"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    End If
                End If

                If oid <> 0 Then
                    If Not Session("delete") Is Nothing Then
                        Dim objTable As DataTable = Session("delete")
                        For i As Integer = 0 To objTable.Rows.Count - 1
                            Dim sjdtloid As Integer = objTable.Rows(i).Item("sjldtloid")
                            If sjdtloid <> 0 Then
                                sSql = "DELETE FROM ql_trnsjalandtl WHERE sjldtloid=" & objTable.Rows(i).Item("sjldtloid") & ""
                                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            End If
                        Next
                    End If
                End If

                otrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch ex As Exception
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                result = ex.ToString
            End Try
        End If

        Return result
    End Function

    Private Function deleterecord() As String
        Dim result As String = ""

        Try
            Dim oid As Integer = Session("oid")
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "DELETE FROM ql_trnsjalan WHERE sjloid = " & oid & ""
            xCmd.CommandText = sSql
            Dim res As Integer = xCmd.ExecuteNonQuery
            If res <= 0 Then
                result = "Data surat jalan tidak dapat dihapus!"
            Else
                sSql = "DELETE FROM ql_trnsjalandtl WHERE sjloid = " & oid & ""
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            result = ex.ToString
        End Try

        Return result
    End Function

    Private Function getromawi(ByVal month As String) As String
        Dim res As String = ""

        If month = "01" Then
            res = "I"
        ElseIf month = "02" Then
            res = "II"
        ElseIf month = "03" Then
            res = "III"
        ElseIf month = "04" Then
            res = "IV"
        ElseIf month = "05" Then
            res = "V"
        ElseIf month = "06" Then
            res = "VI"
        ElseIf month = "07" Then
            res = "VII"
        ElseIf month = "08" Then
            res = "VIII"
        ElseIf month = "09" Then
            res = "IX"
        ElseIf month = "10" Then
            res = "X"
        ElseIf month = "11" Then
            res = "XI"
        ElseIf month = "12" Then
            res = "XII"
        End If

        Return res
    End Function
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("branch_id") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
            Server.Transfer("~\other\NotAuthorize.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Session.Timeout = 60
            Response.Redirect("~\Transaction\trnsuratjalan.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Surat Jalan"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            lbltitleinfo.Text = "New Surat Jalan"
        Else
            lbltitleinfo.Text = "Update Surat Jalan"
        End If 
        ibdelete.Attributes.Add("onclick", "javascript:return confirm('Apakah anda ingin menghapus data ini?');")
        ibapprove.Attributes.Add("onclick", "javascript:return confirm('Apakah anda ingin mengirim approval terhadap data ini?');")

        If Not Page.IsPostBack Then
            bindalldata() : initTypeJob()
            If Session("oid") = "" Or Session("oid") Is Nothing Then
                initsjlno()
                clearform()
                TabContainer1.ActiveTabIndex = 0
                Session("suratjalandetail") = Nothing
                Session("delete") = Nothing
                tbdatesend.Text = Format(GetServerTime(), "dd/MM/yyyy")
            Else
                fillform()
                TabContainer1.ActiveTabIndex = 1
            End If
        End If
    End Sub

    Protected Sub ibfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfind.Click
        finddata(ddlfilter.SelectedValue.ToString, Tchar(tbfilter.Text.Trim), cbperiod.Checked, tbperiodstart.Text, tbperiodend.Text, cbstatus.Checked, ddlfilterstatus.SelectedValue.ToString)
    End Sub

    Protected Sub ibviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibviewall.Click
        bindalldata()
        ddlfilter.SelectedIndex = 0
        tbfilter.Text = ""
        tbperiodstart.Text = Date.Now.ToString("dd/MM/yyyy")
        tbperiodend.Text = Date.Now.ToString("dd/MM/yyyy")
        cbstatus.Checked = False
        cbperiod.Checked = False
        ddlfilterstatus.SelectedIndex = 0
    End Sub

    Protected Sub imbokpopupmsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbokpopupmsg.Click
        cProc.SetModalPopUpExtender(bepopupmsg, pnlpopupmsg, mpepopupmsg, False)
    End Sub

    Protected Sub ibsearchsupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchsupplier.Click
        bindsupplierdata()
        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, True)
    End Sub

    Protected Sub ibdelsupplier_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelsupplier.Click
        tbsupplier.Text = ""
        lblsupplieroid.Text = ""
    End Sub

    Protected Sub ibpopcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancel.Click
        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        tbpopfilter.Text = ""
    End Sub

    Protected Sub ibpopfind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfind.Click
        findpopdata(ddlpopfilter.SelectedValue.ToString, Tchar(tbpopfilter.Text))
        mpepopgv.Show()
    End Sub

    Protected Sub ibclear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibclear.Click
        lblbarcode.Text = ""
        lblbarcodeview.Text = ""
        tbitemname.Text = ""
        tbitemqty.Text = 1
        tbitemtype.Text = ""
        tbitembrand.Text = ""
        lblitemtype.Text = ""
        lblitembrand.Text = ""
        tbnote.Text = ""
        lblreqoid.Text = ""
    End Sub

    Protected Sub ibaddtolist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibaddtolist.Click
        If lblbarcode.Text = "" Then
            showMessage("Barcode harus dipilih terlebih dahulu!", 2)
            Exit Sub
        ElseIf tbnote.Text = "" Then
            showMessage("Detail kerusakan harus diisi!", 2)
            Exit Sub
        Else
            If lblstat.Text = "new" Then
                addtolist()
            ElseIf lblstat.Text = "edit" Then
                editlist()
            End If
            lblreqoid.Text = ""
            lblbarcode.Text = ""
            lblbarcodeview.Text = ""
            tbitemname.Text = ""
            tbitemqty.Text = 1
            tbitemtype.Text = ""
            tbitembrand.Text = ""
            lblitemtype.Text = ""
            lblitembrand.Text = ""
            tbnote.Text = ""
            lblstat.Text = "new"
            lblrow.Text = ""
            lblstarttime.Text = ""
        End If
    End Sub

    Protected Sub ibcancelinfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancelinfo.Click
        Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
    End Sub

    Protected Sub ibsaveinfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsaveinfo.Click
        Dim result As String = ""
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            result = saverecord()
            If result = "" Then
                Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        Else
            result = updaterecord("")
            If result = "" Then
                Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
            Else
                showMessage(result, 2)
            End If
        End If
    End Sub

    Protected Sub ibpostinginfo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpostinginfo.Click
        Dim result As String = ""
        result = postingrecord()
        If result = "" Then
            Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
        Else
            showMessage(result, 2)
        End If
    End Sub

    Protected Sub gvpopsupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpopsupplier.SelectedIndexChanged
        tbsupplier.Text = gvpopsupplier.SelectedDataKey.Item("suppname")
        lblsupplieroid.Text = gvpopsupplier.SelectedDataKey.Item("suppoid")
        cProc.SetModalPopUpExtender(bepopgv, panpopgv, mpepopgv, False)
        tbpopfilter.Text = ""
    End Sub

    Protected Sub ibsearchbarcode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchbarcode.Click
        bindorderdata()
        cProc.SetModalPopUpExtender(bepopgvorder, panpopgvorder, mpepopgvorder, True)
    End Sub

    Protected Sub ibdelbarcode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelbarcode.Click
        lblbarcode.Text = ""
        lblbarcodeview.Text = "" : tbitemname.Text = "" : tbitemqty.Text = 1
        tbitemtype.Text = "" : tbitembrand.Text = "" : lblitemtype.Text = ""
        lblitembrand.Text = "" : tbnote.Text = "" : lblreqoid.Text = ""
    End Sub

    Protected Sub ibpopfindorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfindorder.Click
        findpopdataorder(ddlpopfilterorder.SelectedValue.ToString, Tchar(tbpopfilterorder.Text))
        mpepopgvorder.Show()
    End Sub

    Protected Sub ibpopcancelorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancelorder.Click
        cProc.SetModalPopUpExtender(bepopgvorder, panpopgvorder, mpepopgvorder, False)
        tbpopfilterorder.Text = ""
    End Sub

    Protected Sub gvpoporder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpoporder.SelectedIndexChanged
        lblreqoid.Text = gvpoporder.SelectedDataKey.Item("reqoid")
        lblbarcode.Text = gvpoporder.SelectedDataKey.Item("barcode")
        tbitemname.Text = gvpoporder.SelectedDataKey.Item("reqitemname")
        tbitemtype.Text = gvpoporder.SelectedDataKey.Item("type")
        tbitembrand.Text = gvpoporder.SelectedDataKey.Item("brand")
        lblitemtype.Text = gvpoporder.SelectedDataKey.Item("reqitemtype")
        lblitembrand.Text = gvpoporder.SelectedDataKey.Item("reqitembrand")
        lblstarttime.Text = Format(gvpoporder.SelectedDataKey.Item("sstarttime"), "dd/MM/yyyy")

        sSql = "select REQDTLJOB from QL_TRNREQUESTDTL where reqmstoid = " & lblreqoid.Text & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        If xreader.HasRows Then
            While xreader.Read
                tbnote.Text &= xreader("REQDTLJOB") & Chr(13)
            End While
        End If

        cProc.SetModalPopUpExtender(bepopgvorder, panpopgvorder, mpepopgvorder, False)
        tbpopfilterorder.Text = ""
        lblstat.Text = "new"
    End Sub

    Protected Sub ibpopviewall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewall.Click
        bindsupplierdata()
        ddlpopfilter.SelectedIndex = 0
        tbpopfilter.Text = ""
        mpepopgv.Show()
    End Sub

    Protected Sub ibpopviewallorder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewallorder.Click
        bindorderdata()
        ddlpopfilterorder.SelectedIndex = 0
        tbpopfilterorder.Text = ""
        mpepopgvorder.Show()
    End Sub

    Protected Sub gvdata_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdata.PageIndexChanging
        gvdata.PageIndex = e.NewPageIndex
        finddata(ddlfilter.SelectedValue.ToString, Tchar(tbfilter.Text.Trim), cbperiod.Checked, tbperiodstart.Text, tbperiodend.Text, cbstatus.Checked, ddlfilterstatus.SelectedValue.ToString)
        'gvdata.DataSource = Session("suratjalan")
        'gvdata.DataBind()
    End Sub

    Protected Sub ibdelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelete.Click
        Dim result As String = ""
        result = deleterecord()
        If result = "" Then
            Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
        Else
            showMessage(result, 2)
        End If
    End Sub

    Protected Sub ibprint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibprint.Click
        printouttopdf(Integer.Parse(lbloid.Text))
    End Sub

    Protected Sub printSjalan(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = TryCast(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.NamingContainer, GridViewRow)
        Dim status As String = gvr.Cells(6).Text.ToString
        If status <> "Send" Then
            showMessage("Surat jalan hanya bisa dicetak setelah statusnya 'Send' !", 2)
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        Else
            Dim oid As Integer = Integer.Parse(gvdata.DataKeys(gvr.RowIndex).Item("sjloid"))
            printouttopdf(oid)
            TabContainer1.ActiveTabIndex = 0
        End If
    End Sub
#End Region

    Private Sub bindpicdata(ByVal sWhere As String)
        sSql = "SELECT a.userid personnip, a.username personname FROM ql_mstprof a WHERE a.cmpcode = '" & CompnyCode & "' AND a.STATUSPROF  = 'Active' and USERID not in (select personnip from QL_MSTPERSON) " & sWhere & " " 
        Session("datapic") = Nothing
        Session("datapic") = cKon.ambiltabel(sSql, "datapic")
        gvpoppic.DataSource = Session("datapic")
        gvpoppic.DataBind()
    End Sub

    Protected Sub ibsearchpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibsearchpic.Click
        bindpicdata(" and divisi = 'Approval' and branch_code  = '" & DdlCabang.SelectedValue & "' ")
        cProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, True)
    End Sub

    Protected Sub ibdelpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelpic.Click
        tbpic.Text = ""
        'lblpic.Text = ""
        lblapprovaluser.Text = ""
    End Sub

    Protected Sub ibpopviewallpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopviewallpic.Click
        bindpicdata("")
        ddlpopfilterpic.SelectedIndex = 0
        tbpopfilterpic.Text = ""
        mpepic.Show()
    End Sub

    Protected Sub ibpopfindpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopfindpic.Click
        Dim sWhere As String = ""

        sWhere &= "AND " & ddlpopfilterpic.SelectedValue & " LIKE '%" & Tchar(tbpopfilterpic.Text) & "%'"

        bindpicdata(sWhere)
        mpepic.Show()
    End Sub

    Protected Sub gvpoppic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvpoppic.SelectedIndexChanged
        'lblpic.Text = gvpoppic.SelectedDataKey.Item("personoid")
        lblapprovaluser.Text = gvpoppic.SelectedDataKey.Item("personnip")
        tbpic.Text = gvpoppic.SelectedDataKey.Item("personname")
        cProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, False)
    End Sub

    Protected Sub ibpopcancelpic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibpopcancelpic.Click
        cProc.SetModalPopUpExtender(bepopgvpic, panpopgvpic, mpepic, False)
    End Sub

    Private Sub removeApproval(ByVal sjloid As Integer)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            sSql = "DELETE FROM ql_approval WHERE tablename = 'QL_TRNSJALAN' AND oid = " & sjloid & " AND statusrequest = 'New' AND cmpcode = '" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            showMessage(ex.ToString, 2)
            Exit Sub
        End Try
    End Sub

    Protected Sub ibapprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibapprove.Click

        Dim oid As Integer = 0
        If Integer.TryParse(lbloid.Text, oid) = False Then
            showMessage("Surat Jalan tidak valid!", 2)
            Exit Sub
        End If
        'Or lblpic.Text = ""
        If tbpic.Text.Trim = "" Or lblapprovaluser.Text = "" Then
            showMessage("PIC tidak boleh kosong!", 2)
            'If MultiView1.ActiveViewIndex = 1 Then
            '    MultiView1.ActiveViewIndex = 0
            'End If
            Exit Sub
        End If

        If oid > 0 Then
            Dim res As String = ""
            res = sendapproval()
            If res <> "" Then
                showMessage(res, 2)
                'Exit Sub
            Else
                res = postingrecord()
                If res <> "" Then
                    'removeApproval(oid)
                    showMessage(res, 2)
                    'Exit Sub
                Else
                    Response.Redirect("~\Transaction\trnsuratjalan.aspx?awal=true")
                End If
            End If
        End If

    End Sub

    Private Function sendapproval() As String
        Dim res As String = ""

        Dim otrans As SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        otrans = conn.BeginTransaction
        xCmd.Transaction = otrans
        Try
            Dim loid As Integer = 0
            If Integer.TryParse(lbloid.Text, loid) Then
                sSql = "SELECT approvaloid FROM ql_approval WHERE tablename = 'QL_TRNSJALAN' AND oid = " & loid & " AND cmpcode = '" & CompnyCode & "' AND statusrequest = 'New'"
                xCmd.CommandText = sSql : Dim toid As Integer = xCmd.ExecuteScalar

                sSql = "UPDATE ql_approval SET statusrequest = 'Canceled' WHERE approvaloid = " & toid & " AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "SELECT lastoid FROM ql_mstoid WHERE tablename = 'QL_APPROVAL' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : Dim oid As Integer = xCmd.ExecuteScalar + 1

                sSql = "INSERT INTO ql_approval (cmpcode,branch_code, approvaloid, requestcode, requestuser, requestdate, statusrequest, tablename, oid, event, approvalcode, approvaluser) VALUES ('" & CompnyCode & "','" & DdlCabang.SelectedValue & "', " & oid & ", '0', '" & lblcurrentuser.Text & "', '" & DateTime.Now & "', 'New', 'QL_TRNSJALAN', " & loid & ", 'In Approval','0','" & lblapprovaluser.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE ql_mstoid SET lastoid = " & oid & " WHERE tablename = 'QL_APPROVAL' AND cmpcode = '" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                res = "Surat jalan tidak valid!"
                otrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Return res
            End If

            otrans.Commit()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            res = ex.ToString
            otrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return res
        End Try

        Return res
    End Function

    Protected Sub DdlCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        initsjlno()
    End Sub
End Class
