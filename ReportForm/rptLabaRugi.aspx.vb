Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports ClassFunction

Partial Class ReportForm_rptLabaRugi
    Inherits System.Web.UI.Page

#Region "Variables"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public cmpcode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public folderReport As String = "~/report/"
    Public cKon As New Koneksi
    Dim conn As New SqlConnection(ConnStr)
    Dim oReport As New ReportDocument
    Dim sSql As String = ""
    Dim CProc As New ClassProcedure
#End Region

#Region "Functions"
    Private Function ValidateInput() As String ' Return empty string if Success
        Dim sReturn As String = ""
        If chkPeriode.Checked Then ' Check Period
            Dim st1, st2 As Boolean
            Try
                Dim dt As Date = CDate(txtPeriode1.Text) : st1 = True
            Catch ex As Exception
                sReturn &= "- Invalid start period date !!<BR>" : st1 = False
            End Try
            Try
                Dim dt As Date = CDate(txtPeriode2.Text) : st2 = True
            Catch ex As Exception
                sReturn &= "- Invalid end period date !!<BR>" : st2 = False
            End Try
            If st1 And st2 Then
                If CDate(txtPeriode1.Text) > CDate(txtPeriode2.Text) Then
                    sReturn &= "- End period date can't be less than start date !!<BR>" : st2 = False
                End If
            End If
        End If
        If chkCust.Checked Then
            If lbloid.Text = "" Then
                sReturn &= "- Select a Customer first !!<BR>"
            End If
        End If
        Return sReturn
    End Function

    Private Function ProcessReport() As String
        Dim sMsg As String = ""
        
        Try
          
            Dim swarherzero As String = ""
            If CheckBox1.Checked = True Then
                If ddlTipe.SelectedValue = "S" Then
                    swarherzero = " and a.amtbalanceidr <> 0 "
                Else
                    swarherzero = " where a.amtbalanceidr <> 0 "
                End If
            End If

            If ddlTipe.SelectedValue = "S" Then
                oReport.Load(Server.MapPath("~/report/crLabaRugi.rpt"))
                oReport.SetParameterValue("sWhereZero", swarherzero)
            Else
                oReport.Load(Server.MapPath("~/report/crLabaRugiDtl.rpt"))
                oReport.SetParameterValue("sWhereZero", swarherzero)
            End If

            CProc.SetDBLogonForReport2(oReport)

            'oReport.SetDataSource(dtRptLR)
            oReport.PrintOptions.PaperSize = PaperSize.PaperA4
            oReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            oReport.SetParameterValue("branch_code", dd_branch.SelectedValue)
            oReport.SetParameterValue("cmpcode", "MSC")
            oReport.SetParameterValue("periodawal", GetDateToPeriodAcctg(New Date(DDLYear2.SelectedValue, DDLMonth2.SelectedValue, 1)))
            oReport.SetParameterValue("speriode", DDLMonth2.SelectedItem.Text & " " & DDLYear2.SelectedItem.Text)
            oReport.SetParameterValue("currency", "IDR")
            oReport.SetParameterValue("PrintUserID", "tello")
            oReport.SetParameterValue("ReportPath", "tello")
        Catch ex As Exception
            sMsg = ex.ToString
        End Try
        Return sMsg
    End Function
#End Region

#Region "Procedures"
    Private Sub InitDDL()

        If Session("branch_id") = "01" Then
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang'"
            FillDDL(dd_branch, sSql)
            dd_branch.Enabled = True
            dd_branch.CssClass = "inpText"
        Else
            sSql = "select gencode,gendesc from QL_mstgen where gengroup = 'cabang' and gencode='" & Session("branch_id") & "'"
            FillDDL(dd_branch, sSql)
            dd_branch.Enabled = False
            dd_branch.CssClass = "inpTextDisabled"
        End If

        ' BULAN
        For C1 As Integer = 1 To 12
            Dim liMonth As New ListItem(MonthName(C1), C1)
            DDLMonth.Items.Add(liMonth)
            Dim liMonth2 As New ListItem(MonthName(C1), C1)
            DDLMonth2.Items.Add(liMonth2)
        Next
        DDLMonth2.SelectedValue = 12
        ' TAHUN
        For C1 As Integer = GetServerTime.Year - 5 To GetServerTime.Year + 5
            Dim liYear As New ListItem(C1, C1)
            DDLYear.Items.Add(liYear)
            Dim liYear2 As New ListItem(C1, C1)
            DDLYear2.Items.Add(liYear2)
        Next
        DDLYear2.SelectedValue = GetServerTime.Year


    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
        If Session("branch") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If cKon.checkSpecialPermission(Page.AppRelativeVirtualPath, Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()

            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptLabaRugi.aspx")
        End If

        Page.Title = CompnyName & " - Income Statement Report"
        If Not IsPostBack Then
            InitDDL()
            DDL_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        oReport.Close() : oReport.Dispose()
    End Sub

    Protected Sub DDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    DDLMonth2.SelectedIndexChanged, DDLYear2.SelectedIndexChanged
        DDLYear.Items.Clear()

        ' Calculate Start Periode for 12 month report periods
        Dim iStartM As Integer = 0 : Dim iStartY As Integer = 0
        If DDLMonth2.SelectedValue - 12 < 0 Then
            iStartM = 12 - (11 - DDLMonth2.SelectedValue)
            iStartY = DDLYear2.SelectedValue - 1
        Else
            iStartM = DDLMonth2.SelectedValue - 11
            iStartY = DDLYear2.SelectedValue
        End If

        DDLMonth.SelectedValue = iStartM
        DDLYear.Items.Add(New ListItem(iStartY, iStartY))
    End Sub

  
    Protected Sub btView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btView.Click
        Session("viewReport") = "False" : crvLR.ReportSource = Nothing
        lblWarning.Text = "" ' ValidateInput()

        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport() = "" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                'oReport.SetParameterValue("Branch", dd_branch.SelectedItem.Text)
                oReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "INCOME_STATEMENT_" & ddlTipe.SelectedItem.Text.ToUpper & _
                "_" & (DDLMonth.SelectedItem.Text & DDLYear.SelectedItem.Text).ToUpper & "_" & (DDLMonth2.SelectedItem.Text & DDLYear2.SelectedItem.Text).ToUpper)
            Catch ex As Exception
                oReport.Close() : oReport.Dispose()
            End Try
        Else
            lblWarning.Text = ProcessReport()
        End If
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClear.Click
        Response.Redirect("rptLabaRugi.aspx?awal=true")
    End Sub

    Protected Sub imgExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExcel.Click
        Session("viewReport") = "False" : crvLR.ReportSource = Nothing
        lblWarning.Text = "" ' ValidateInput()
        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport() = "" Then
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Try
                oReport.SetParameterValue("Branch", dd_branch.SelectedItem.Text)
                oReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "INCOME_STATEMENT_" & ddlTipe.SelectedItem.Text.ToUpper & _
                "_" & (DDLMonth.SelectedItem.Text & DDLYear.SelectedItem.Text).ToUpper & "_" & (DDLMonth2.SelectedItem.Text & DDLYear2.SelectedItem.Text).ToUpper)
            Catch ex As Exception
                oReport.Close() : oReport.Dispose()
            End Try
        Else
            lblWarning.Text = ProcessReport()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnViewReport.Click
        Session("viewReport") = "False" : crvLR.ReportSource = Nothing
        lblWarning.Text = "" ' ValidateInput()
        If lblWarning.Text <> "" Then Exit Sub
        If ProcessReport() = "" Then
            crvLR.DisplayGroupTree = False
            crvLR.DisplayToolbar = False
            crvLR.SeparatePages = False
            Session("viewReport") = "True"
            crvLR.ReportSource = oReport
        Else
            lblWarning.Text = ProcessReport()
        End If
    End Sub
   

    
#End Region

   
End Class

