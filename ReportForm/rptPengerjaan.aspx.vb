Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class ReportForm_rptPengerjaan
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As New ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode") 'CompnyName
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName") 'CompnyName
    Dim CProc As New ClassProcedure
    Dim cKon As New Koneksi
    Dim mysql As String
    Dim sSql As String
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = cKon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Procedure"
    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/frmPenerimaanService.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        FillDDL(DDLcabang, sSql)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            Image1.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            Image1.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 3 Then 'Warning
            Image1.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        WARNING.Text = strCaption : lblValidasi.Text = sMessage
        CProc.SetModalPopUpExtender(ButtonExtendervalidasi, PanelValidasi, ModalPopupExtenderValidasi, True)
    End Sub

    Private Sub ResetReport()
        Session("showReport") = False
        CrystalReportViewer1.ReportSource = Nothing
    End Sub

    Sub showPrint(ByVal tipe As String)
        Try
            Dim dat1 As Date = CDate(toDate(period1.Text))
            Dim dat2 As Date = CDate(toDate(period2.Text))
        Catch ex As Exception
            showMessage("Format Tanggal Salah - Gunakan format dd/MM/yyyy !!", 2) : Exit Sub
        End Try
        If CDate(toDate(period1.Text)) > CDate(toDate(period2.Text)) Then
            showMessage("Tanggal mulai tidak boleh lebih besar dari tanggal destinasi !!", 2) : Exit Sub
        End If
        CrystalReportViewer1.Visible = True
        Try
            Dim sPeriode As String = ""
            Dim dat1 As Date = CDate(toDate(period1.Text))
            Dim dat2 As Date = CDate(toDate(period2.Text))
            Dim sWhere As String = "WHERE QLB.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"

            If txtFilter.Text.Trim <> "" Then
                sWhere = " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(txtFilter.Text) & "%'"
            End If

            If cbStatus.SelectedValue = "Status" Then
                If ddlStatus.SelectedValue <> "All" Then
                    sWhere &= " AND QLA.REQSTATUS = '" & ddlStatus.SelectedValue & "' "
                End If
                sWhere &= " AND QLA.REQSTATUS <> 'Paid' "
            Else
                sWhere &= " AND QLA.REQSTATUS = 'Paid' "
            End If

            sPeriode = " AND (CONVERT(date,qla.CREATETIME,101)) between '" & CDate(toDate(period1.Text)) & "' AND '" & CDate(toDate(period2.Text)) & "' " & sWhere & " "
            'vReport = New ReportDocument
            vReport.Load(Server.MapPath("~\Report\rptPengerjaan.rpt"))
            'vReport.SetParameterValue("sCabang", DDLcabang.SelectedValue)
            vReport.SetParameterValue("sWhere", sPeriode)
            vReport.SetParameterValue("dPeriode", period1.Text & " - " & period2.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            CProc.SetDBLogonForReport(vReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                  System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))
            'CrystalReportViewer1.DisplayGroupTree = False

            If tipe = "EXCEL" Then
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
            Else
                If tipe = "PDF" Then
                    Response.Buffer = False
                    Response.ClearContent()
                    Response.ClearHeaders()
                    vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, Format(GetServerTime(), "dd_MM_yy"))
                    vReport.Close() : vReport.Dispose() 'Session("no") = Nothing
                Else
                    CrystalReportViewer1.DisplayGroupTree = False
                    CrystalReportViewer1.ReportSource = vReport
                End If
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/OTHER/login.aspx")
        End If
        'Session.Timeout = 15
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    'Server.Transfer("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptPengerjaan.aspx")
        End If

        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            period1.Text = Format(GetServerTime, "01" & "/MM/yyyy")
            period2.Text = Format(GetServerTime.Date, "dd/MM/yyyy ")
            Cabangddl()
        End If
        Page.Title = "Laporan Pengerjaan Servis"
        If Session("showReport") = True Then
            showPrint("")
        Else
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("showReport") = True
        showPrint("")
    End Sub

    Protected Sub imbExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("showReport") = True
        'Dim excel As String = "excel"
        showPrint("EXCEL")
    End Sub

    Protected Sub ibcancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibcancel.Click
        ResetReport()
        Session("oid") = Nothing
        Response.Redirect("rptPengerjaan.aspx?awal=true")
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub ToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        showPrint("PDF")
    End Sub

    Protected Sub btnErrOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ResetReport()
        ButtonExtendervalidasi.Visible = False
        PanelValidasi.Visible = False
        Response.Redirect("rptPengerjaan.aspx?awal=true")
    End Sub

    Protected Sub CrystalReportViewer1_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles CrystalReportViewer1.Navigate
        Session("showReport") = True
        showPrint("")
    End Sub
#End Region
End Class
