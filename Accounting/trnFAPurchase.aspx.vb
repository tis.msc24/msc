
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnFAPurchase
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Dim ckon As New Koneksi
    Dim cRate As New ClassRate
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
    Dim PPN As Integer = 10
#End Region

#Region "Function"

    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("tbldtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtloid", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifamstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnbelifadtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemfacode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemfadesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtlqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtldisctype", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtldiscvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtldiscamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlamtnetto", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtldep", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnbelifadtllastvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(CDate(toDate(fixDate.Text)), "dd/MM/yyyy", sErr) Then
            showMessage("Fixed Asset Date is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(toDate(fixDate.Text)) <= CDate(toDate(CutofDate.Text)) Then
            showMessage("Tanggal fix asset purchase Tidak boleh <= Tanggal CutofDate (" & CutofDate.Text & ") !!", 2)
            Return False
        End If
        'If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
        '    showMessage("Period 2 must be more than Period 1 !", 2)
        '    Return False
        'End If
        Return True
    End Function
#End Region

#Region "Prosedure"

    Public Sub binddata()
        Try
            Dim tgle As Date = (toDate(txtPeriode1.Text))
            tgle = CDate(toDate(txtPeriode1.Text))
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(toDate(txtPeriode1.Text)) > CDate(toDate(txtPeriode2.Text)) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If

        sSql = "SELECT m.cmpcode,m.trnbelifamstoid,m.periodacctg,m.trnbelifano,Convert(Varchar(10),m.trnbelifadate,103) AS trnbelifadate,m.trnbelifaref,m.suppoid,m.trnbelifatype,m.trnbelifapaytype,m.trnbelifaamt,m.trnbelifadisctype,m.trnbelifadiscvalue,m.trnbelifadiscamt,m.trnbelifamsttaxtype,m.trnbelifamsttaxpct,m.trnbelifamsttaxamt,m.trnbelifaamtnetto,m.trnbelifamstnote,m.trnbelifamststatus,s.suppoid,s.suppname,g.gencode,g.gendesc FROM QL_trnbelimst_fa m inner  JOIN QL_mstsupp s ON s.suppoid=m.suppoid AND s.cmpcode=m.cmpcode INNER JOIN QL_mstgen g ON g.genoid=m.trnbelifapaytype WHERE m.cmpcode='" & CompnyCode & "'" & IIf(DDLstatus.SelectedValue = "ALL", "", " AND m.trnbelifamststatus = '" & DDLstatus.SelectedValue & "'") & "AND " & DDLfilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'"

        'If cbPeriode.Checked Then
        sSql += "AND m.trnbelifadate BETWEEN '" & CDate(toDate(txtPeriode1.Text)) & "' AND '" & CDate(toDate(txtPeriode2.Text)) & "' "
        'End If
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnbelimst_fa")
        Session("tbldata") = objTable
        GVFixedAsset.DataSource = objTable
        GVFixedAsset.DataBind()
    End Sub

    Public Sub BindDataSupp()
        sSql = "select suppoid,suppcode,supptype,suppname,suppaddr,0 AS coa_hutang, s.suppacctgoid AS coa FROM QL_mstsupp s inner join ql_mstacctg a ON a.acctgoid=s.suppacctgoid WHERE s.cmpcode = '" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND supptype='GROSIR' ORDER BY suppname "

        Dim objTbl As DataTable = ckon.ambiltabel(sSql, "QL_mstsupp")
        Session("TblSupp") = objTbl
        gvSupplier.DataSource = objTbl
        gvSupplier.DataBind()
    End Sub

    Public Sub BindDataItem()
        sSql = "select i.itemoid,i.itemcode,i.itemdesc AS itemShortDescription FROM QL_mstitem i WHERE i.cmpcode='" & CompnyCode & "' AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' AND i.stockflag ='I'"
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "ql_mstitem")
        Session("TableItem") = objTable
        GVmstitem.DataSource = objTable
        GVmstitem.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub initAllDDL()
        sSql = "SELECT genoid,gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='ASSETS_TYPE' And genoid <>0 ORDER BY gendesc"
        FillDDL(fixgroup, sSql)

        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYTYPE' ORDER BY gendesc"
        FillDDL(DDLpayterm, sSql)

        sSql = "SELECT currencyoid,currencycode FROM QL_mstcurr order by currencyoid ASC"
        FillDDL(CurrDDL, sSql)

        Dim kFilter As String = ""
        If Session("branch_id") = "10" Or Session("UserID") = "admin" Then
            kFilter = ""
        Else
            kFilter = " And gencode='" & DDLoutlet.SelectedValue & "'"
        End If
        FillDDL(DDLBranch, "Select gencode,gendesc From QL_mstgen Where gengroup='CABANG'" & kFilter & "")
    End Sub

    Private Sub fillTextBox(ByVal sCmpcode As String, ByVal vjurnaloid As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT m.branch_code,m.cmpcode,m.trnbelifamstoid,m.periodacctg,m.trnbelifano,m.trnbelifadate,m.trnbelifaref,m.suppoid,m.trnbelifatype,m.trnbelifapaytype,m.trnbelifaamt,m.trnbelifadisctype,m.trnbelifadiscvalue,m.trnbelifadiscamt,m.trnbelifamsttaxtype,m.trnbelifamsttaxpct,m.trnbelifamsttaxamt,m.trnbelifaamtnetto,m.trnbelifamstnote,m.trnbelifamststatus,m.createuser,m.createtime,m.upduser,m.updtime, s.suppoid,s.suppname,g.gencode,g.gendesc,m.curroid ,m.rateoid ,m.beliratetoidr ,m.beliratetousd ,m.rate2oid,m.belirate2toidr,m.belirate2tousd, isnull((select SUM(d.trnbelifadtldiscamt) trnbelifadtldiscamt FROM QL_trnbelidtl_fa d where d.trnbelifamstoid = " & vjurnaloid & " AND cmpcode = '" & sCmpcode & "'),0) AS trnbelifadtldiscamt FROM QL_trnbelimst_fa m INNER JOIN QL_mstgen g ON g.genoid=m.trnbelifapaytype INNER JOIN QL_mstsupp s ON s.suppoid=m.suppoid WHERE m.trnbelifamstoid = '" & vjurnaloid & "' AND m.cmpcode = '" & sCmpcode & "'"
        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow
        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length > 0 Then
            famstoid.Text = Trim(objRow(0)("trnbelifamstoid").ToString)
            fixmstoid.Text = Trim(objRow(0)("trnbelifamstoid").ToString)
            fixDate.Text = Format(objRow(0)("trnbelifadate"), "dd/MM/yyyy")
            fixgroup.SelectedValue = Trim(objRow(0)("trnbelifatype").ToString)
            DDLBranch.SelectedValue = Trim(objRow(0)("branch_code").ToString)
            FixAssetNo.Text = Trim(objRow(0)("trnbelifano").ToString)
            RefNo.Text = Trim(objRow(0)("trnbelifaref").ToString)
            lblSuppOid.Text = Trim(objRow(0)("suppoid").ToString)
            Supp.Text = Trim(objRow(0)("suppname").ToString)
            TotalDetail.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamt").ToString), 4)
            TotalDiscDetail.Text = ToMaskEdit(Trim(objRow(0)("trnbelifadtldiscamt").ToString), 4)
            DDLtype1.SelectedValue = Trim(objRow(0)("trnbelifadisctype").ToString)
            FilterType1.Text = Trim(objRow(0)("trnbelifadiscvalue").ToString)
            Tax.Text = ToMaskEdit(Trim(objRow(0)("trnbelifamsttaxamt").ToString), 4)
            DDLtax.SelectedValue = Trim(objRow(0)("trnbelifamsttaxtype").ToString)
            DiscHdr.Text = ToMaskEdit(Trim(objRow(0)("trnbelifadiscamt").ToString), 4)
            Netto.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamtnetto").ToString), 4)
            Status.Text = Trim(objRow(0)("trnbelifamststatus").ToString)
            Note1.Text = Trim(objRow(0)("trnbelifamstnote").ToString)
            CurrDDL.SelectedValue = Trim(objRow(0)("curroid").ToString)
            Rateoid.Text = Trim(objRow(0)("rateoid").ToString)
            Rate2oid.Text = Trim(objRow(0)("rate2oid").ToString)
            RateToIDR.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("beliratetoidr").ToString)), GetRoundValue(Trim(objRow(0)("beliratetoidr").ToString)))
            RateToUSD.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("beliratetousd").ToString)), GetRoundValue(Trim(objRow(0)("beliratetousd").ToString)))
            Rate2ToIDR.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("belirate2toidr").ToString)), GetRoundValue(Trim(objRow(0)("belirate2toidr").ToString)))
            Rate2ToUsd.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("belirate2tousd").ToString)), GetRoundValue(Trim(objRow(0)("belirate2tousd").ToString)))

            If DDLtax.SelectedValue = "INCLUSIVE" Then
                DPP.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamtnetto").ToString) - Trim(objRow(0)("trnbelifamsttaxamt").ToString), 4)
            Else
                DPP.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamt").ToString) - Trim(objRow(0)("trnbelifadiscamt").ToString), 4)
            End If
            lblUser.Text = Trim(objRow(0)("createuser").ToString)
            lblTime.Text = Format(objRow(0)("createtime"), "dd/MM/yyyy")
            upduser.Text = Trim(objRow(0)("upduser").ToString)
            updtime.Text = Format(objRow(0)("updtime"), "dd/MM/yyyy")
            DDLpayterm.SelectedValue = Trim(objRow(0)("trnbelifapaytype").ToString)
            sSql = "select genother5 FROM QL_mstgen where gengroup = 'ASSETS_TYPE' and genoid = '" & fixgroup.SelectedValue & "' and cmpcode='" & CompnyCode & "'"
            fixdepmonth.Text = GetStrData(sSql)

            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(toDate(GetStrData(sSql)))
            End If
            CutofDate.Text = Format(CUTOFFDATE, "dd/MM/yyyy")

            If Status.Text.ToUpper = "POST" Then
                btnPosting.Visible = False : btnDelete.Visible = False : btnSave.Visible = False
            Else
                btnPosting.Visible = True : btnDelete.Visible = True : btnSave.Visible = True
            End If
            If Trim(objRow(0)("trnbelifamststatus").ToString) <> "POST" Then
                DDLoutlet.Enabled = False : DDLoutlet.CssClass = "inpTextDisabled"
                btnSave.Visible = True : btnDelete.Visible = True : btnPosting.Visible = True
            Else
                DDLoutlet.Enabled = False : DDLoutlet.CssClass = "inpTextDisabled"
                btnSave.Visible = False : btnDelete.Visible = False : btnPosting.Visible = False
            End If

            'data detail
            sqlSelect = "select cmpcode,trnbelifadtloid, trnbelifamstoid, trnbelifadtlseq, itemfacode, itemfadesc, trnbelifadtlqty, trnbelifadtlprice, trnbelifadtldisctype, trnbelifadtldiscvalue, trnbelifadtldiscamt, trnbelifadtlamtnetto, trnbelifadtldep, trnbelifadtllastvalue, trnbelifadtlnote, crtuser, crttime, upduser, updtime FROM QL_trnbelidtl_fa where trnbelifamstoid =" & vjurnaloid & " AND cmpcode='" & sCmpcode & "'"

            Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            Dim objDsDtl As New DataSet
            Dim objTableDtl As DataTable
            Dim objRowDtl() As DataRow

            mySqlDAdtl.Fill(objDsDtl)
            objTableDtl = objDsDtl.Tables(0)
            objRowDtl = objTableDtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Session("tbldtl") = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataSource = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataBind()
        End If
        mySqlConn.Close()
    End Sub

    Private Sub generatemstoid()
        famstoid.Text = GenerateID("QL_trnbelimst_fa", CompnyCode)
    End Sub

    Private Sub generetedtloid()
        fadtloid.Text = GenerateID("QL_trnbelidtl_fa", CompnyCode)
    End Sub

    Private Sub GenerateFixNo()
        ' Cashbank No
        If lblPOST.Text = "POST" Then
            Dim iCurID As Integer = 0
            Dim Cabang As String = GetStrData("select genother1 from ql_mstgen where gencode='" & Session("branch_id") & "' And gengroup='CABANG'")
            Session("vNofa") = "PIFA/" & Cabang & "/" & Format(CDate(toDate(fixdate.Text)), "yy/MM/dd") & "/"
            sSql = "SELECT MAX(replace(trnbelifano,'" & Session("vNofa") & "','')) trnbelifano FROM QL_trnbelimst_fa WHERE trnbelifano LIKE '" & Session("vNofa") & "%' AND cmpcode='" & CompnyCode & "'"
            If Not IsDBNull(ckon.ambilscalar(sSql)) Then
                iCurID = ckon.ambilscalar(sSql) + 1
            Else
                iCurID = 1
            End If
            Session("sNo") = GenNumberString(Session("vNofa"), "", iCurID, 4)
        Else
            Session("sNo") = famstoid.Text
        End If
    End Sub

    Protected Sub cbPeriode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtPeriode1.Enabled = cbPeriode.Checked : txtPeriode2.Enabled = cbPeriode.Checked
        btnPeriode1.Visible = cbPeriode.Checked : btnPeriode2.Visible = cbPeriode.Checked
        If cbPeriode.Checked Then
            txtPeriode1.CssClass = "inpText" : txtPeriode2.CssClass = "inpText"
        Else
            txtPeriode1.CssClass = "inpTextDisabled" : txtPeriode2.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub ReAmountDep()
        If ToMaskEdit(ToDouble(Price.Text), 4) <> "" And ToMaskEdit(ToDouble(Qty.Text), 0) <> "" Then
            Qty.Text = ToMaskEdit(ToDouble(Qty.Text), 1)
            Price.Text = ToMaskEdit(ToDouble(Price.Text), 4)
            subTotal.Text = ToMaskEdit(ToDouble(Price.Text), 4) * ToMaskEdit(ToDouble(Qty.Text), 1)

            If ToMaskEdit(ToDouble(FilterType2.Text), 4) <> "" Then
                If DDLtype2.SelectedValue = "AMT" Then
                    FilterType2.MaxLength = 10
                    subTotal.Text = ToMaskEdit((ToDouble(Price.Text) * ToDouble(Qty.Text)) - ToDouble(FilterType2.Text), 4)
                    discDtl.Text = ToMaskEdit(ToDouble(FilterType2.Text), 4)

                ElseIf DDLtype2.SelectedValue = "PCT" Then
                    FilterType2.MaxLength = 3
                    Dim disc As Decimal = ToMaskEdit(ToDouble(subTotal.Text) * (ToDouble(FilterType2.Text) / 100), 4)
                    subTotal.Text -= ToMaskEdit(ToDouble(disc), 4)
                    discDtl.Text = ToMaskEdit(ToDouble(disc), 4)
                End If
            End If
        End If
        subTotal.Text = ToMaskEdit(ToDouble(subTotal.Text), 4)
    End Sub

    Private Sub ReAmountHdr()
        If (ToMaskEdit(ToDouble(TotalDetail.Text), 4) <> "" And ToMaskEdit(ToDouble(TotalDiscDetail.Text), 4) <> "") Or (ToMaskEdit(ToDouble(TotalDetail.Text), 4) <> "0.00" And ToMaskEdit(ToDouble(TotalDiscDetail.Text), 4) <> "0.00") Then
            If ToMaskEdit(ToDouble(FilterType1.Text), 4) <> "" Then
                If DDLtype1.SelectedValue = "AMT" Then
                    FilterType1.MaxLength = 10
                    DiscHdr.Text = ToMaskEdit(ToDouble(FilterType1.Text), 4)

                ElseIf DDLtype1.SelectedValue = "PCT" Then
                    FilterType1.MaxLength = 3
                    DiscHdr.Text = ToMaskEdit(ToDouble(TotalDetail.Text) * (ToDouble(FilterType1.Text) / 100), 4)
                    DiscHdr.Text = ToMaskEdit(ToDouble(DiscHdr.Text), 4)
                End If
            End If

            If DDLtax.SelectedValue = "NON TAX" Then
                DPP.Text = ToMaskEdit(ToDouble(TotalDetail.Text) - ToDouble(DiscHdr.Text), 4)
                Tax.Text = 0
                Netto.Text = ToMaskEdit(ToDouble(DPP.Text), 4)
            ElseIf DDLtax.SelectedValue = "INCLUSIVE" Then
                Dim totHdr As Decimal = ToMaskEdit(ToDouble(TotalDetail.Text) - ToDouble(DiscHdr.Text), 4)
                DPP.Text = ToMaskEdit(ToDouble(totHdr) / ((100 + PPN) / 100), 4)
                Tax.Text = ToMaskEdit((PPN / (100 + PPN)) * ToDouble(totHdr), 4)
                Netto.Text = ToMaskEdit(ToDouble(totHdr), 4)
            ElseIf DDLtax.SelectedValue = "EXCLUSIVE" Then
                DPP.Text = ToMaskEdit(ToDouble(TotalDetail.Text) - ToDouble(DiscHdr.Text), 4)
                Tax.Text = ToMaskEdit(ToDouble(DPP.Text) * (PPN / 100), 4)
                Netto.Text = ToMaskEdit(ToDouble(DPP.Text) + ToDouble(Tax.Text), 4)
            End If
            TotalDetail.Text = ToMaskEdit(ToDouble(TotalDetail.Text), 4)
            TotalDiscDetail.Text = ToMaskEdit(ToDouble(TotalDiscDetail.Text), 4)
            DPP.Text = ToMaskEdit(ToDouble(DPP.Text), 4)
            Tax.Text = ToMaskEdit(ToDouble(Tax.Text), 4)
            Netto.Text = ToMaskEdit(ToDouble(Netto.Text), 4)
        End If
    End Sub

    Private Sub TotalDtl()
        Dim iCountrow As Integer = GVFixedAssetdtl.Rows.Count
        Dim iGtotal As Double = 0
        Dim iGdisc As Double = 0
        Dim dt_temp As New DataTable
        dt_temp = Session("dtlTable")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDouble(GVFixedAssetdtl.Rows(i).Cells(7).Text)
            iGdisc += ToDouble(GVFixedAssetdtl.Rows(i).Cells(6).Text)
            'dt_temp.Rows(i).BeginEdit()
            'dt_temp.Rows(i).Item(0) = i + 1
            'dt_temp.Rows(i).EndEdit()
        Next
        'Session("dtlTable") = dt_temp
        'GVFixedAssetdtl.DataSource = dt_temp
        'GVFixedAssetdtl.DataBind()
        TotalDetail.Text = ToMaskEdit(ToDouble(iGtotal), 4)
        TotalDiscDetail.Text = ToMaskEdit(ToDouble(iGdisc), 4)
        DPP.Text = ToMaskEdit(ToDouble(TotalDetail.Text), 4)
        Netto.Text = ToMaskEdit(ToDouble(TotalDetail.Text), 4)
        ReAmountHdr()
    End Sub

    Protected Sub ClearDtl()
        Code.Text = ""
        Desc.Text = ""
        Qty.Text = ""
        Price.Text = ""
        FilterType2.Text = ""
        subTotal.Text = ""
        FinalValue.Text = ""
        discDtl.Text = ""
        Note2.Text = ""
        AddToList.Text = "Add To List"
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            ' Clear all session
            Session.Clear()  ' -->>  clear all session 
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnFAPurchase.aspx")
        End If
        Page.Title = CompnyName & " - Fix Asset Purchase "
        Session("oid") = Request.QueryString("oid")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        'btnPostingDtl.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST all this data?');")

        If Not IsPostBack Then
            'binddata()
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"
            If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = toDate(GetStrData(sSql))
            End If
            txtPeriode1.Text = Format(GetServerTime.AddDays(-2), "dd/MM/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "dd/MM/yyyy")
            lblViewInfo.Visible = True
            initAllDDL()
            AddToList.Text = "Add To List"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(CompnyCode, Session("oid"))
                fixgroup_SelectedIndexChanged(Nothing, Nothing)
                generetedtloid()
                lblPOST.Text = "IN PROCESS"
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 1
            Else
                generatemstoid()
                generetedtloid()
                GenerateFixNo()
                CutofDate.Text = Format(CUTOFFDATE, "dd/MM/yyyy")
                btnDelete.Visible = False : btnPosting.Visible = True : fixdepmonth.Text = -1
                fixdepmonth.CssClass = "inpText" : fixdepmonth.Enabled = True
                fixgroup_SelectedIndexChanged(Nothing, Nothing)
                fixDate.Text = Format(GetServerTime, "dd/MM/yyyy")
                FixAssetNo.Text = Session("sNo")
                upduser.Text = "-" : updtime.Text = "-"
                CurrDDL_SelectedIndexChanged(Nothing, Nothing)
                lblUser.Text = Session("UserID")
                lblTime.Text = Format(GetServerTime, "dd/MM/yyyy")
                Status.Text = "IN PROCESS" : lblPOST.Text = "IN PROCESS"
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As DataTable
            dt = Session("tbldtl")
            GVFixedAssetdtl.DataSource = dt
            GVFixedAssetdtl.DataBind()
        End If

        If Status.Text.ToUpper = "POST" Or Status.Text.ToUpper = "CLOSED" Then
            btnshowCOA.Visible = False : btnPosting.Visible = False
            btnDelete.Visible = False : btnSave.Visible = False
        Else
            btnshowCOA.Visible = False : btnPosting.Visible = True
            btnDelete.Visible = True : btnSave.Visible = True
        End If
    End Sub

    Protected Sub GVFixedAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnFAPurchase.aspx?cmpcode=" & GVFixedAsset.SelectedDataKey("cmpcode").ToString & "&oid=" & GVFixedAsset.SelectedDataKey("trnbelifamstoid").ToString & "")
    End Sub

    Protected Sub GVFixedAsset_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAsset.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub GVFixedAsset_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(toDate(e.Row.Cells(3).Text)), "dd/MM/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblItemOid.Text = GVFixedAssetdtl.SelectedDataKey.Item(1).ToString
        Code.Text = GVFixedAssetdtl.SelectedDataKey.Item(2).ToString
        Desc.Text = GVFixedAssetdtl.SelectedDataKey.Item(3).ToString
        Qty.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(4).ToString)
        Price.Text = ToMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(5).ToString, 4)
        discDtl.Text = ToMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(6).ToString, 4)
        subTotal.Text = ToMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(7).ToString, 4)
        fixdepmonth.Text = GVFixedAssetdtl.SelectedDataKey.Item(8).ToString
        FinalValue.Text = ToMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(9).ToString, 4)
        Note2.Text = GVFixedAssetdtl.SelectedDataKey.Item(10).ToString
        FilterType2.Text = ToMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(11).ToString, 4)
        DDLtype2.SelectedValue = GVFixedAssetdtl.SelectedDataKey.Item(12).ToString
        AddToList.Text = "EDIT"
    End Sub

    Protected Sub GVFixedAssetdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAssetdtl.PageIndex = e.NewPageIndex
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
    End Sub

    Protected Sub GVDtlMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
        End If
    End Sub

    Protected Sub fixgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            sSql = "SELECT ISNULL(genother5,'') FROM QL_mstgen WHERE gengroup = 'ASSETS_TYPE' AND genoid = " & fixgroup.SelectedValue & ""
            If GetStrData(sSql) = "" Then
                showMessage("- Akun COA belum di setting, Setting Akun COA Di form Master General", 2)
                Exit Sub
            Else
                fixdepmonth.Text = GetStrData(sSql)
            End If
        Catch ex As Exception
            showMessage(ex.ToString & "<br>" & sSql, 1)
            fixdepmonth.Text = "0"
        End Try
        
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkPostMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 1
    End Sub

    Protected Sub lbkPostInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("tbldata") = Nothing
        GVFixedAsset.PageIndex = 0
        Session("SearchFixAsset") = sql_temp
        lblViewInfo.Visible = False
        binddata()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFilter.Text = ""
        DDLstatus.SelectedValue = "ALL"
        lblViewInfo.Visible = False
        binddata()
        txtPeriode1.Text = Format(Now.AddDays(-2), "dd/MM/yyyy")
        txtPeriode2.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        ' Validasi
        Dim sMsg As String = ""
        If Not IsValidDate(fixDate.Text, "dd/MM/yyyy", "") Then
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End If
        If CDate(toDate(fixDate.Text)) <= CDate(toDate(CutofDate.Text)) Then
            showMessage("- Tanggal fix asset purchase Tidak boleh Kurang dari CutoffDate (" & CutofDate.Text & ") !!<BR>", 2)
            Exit Sub
        End If
        If Supp.Text.Trim = "" Then
            sMsg &= "- Supplier Kosong !!<BR>"
        End If
        If RefNo.Text.Trim = "" Then
            sMsg &= "- Ref No Kosong !!<BR>"
        End If
        If DDLtype1.SelectedValue = "PCT" Then
            If FilterType1.Text > 100 Then
                sMsg &= "- Discount PCT Header tidak boleh > 100 !!<BR>"
            End If
        ElseIf DDLtype1.SelectedValue = "AMT" Then
            If ToDouble(FilterType1.Text) > ToDouble(TotalDetail.Text) Then
                sMsg &= "- Discount AMT Header tidak boleh > dari Total Detail !!<BR>"
            End If
        End If
        If TotalDetail.Text.Length > 16 Then
            sMsg &= "- maximum digit total detail tidak boleh > 16 digit !!<BR>"
        End If
        If DPP.Text.Length > 16 Then
            sMsg &= "- maximum digit DPP tidak boleh > 16 digit !!<BR>"
        End If
        If Netto.Text.Length > 16 Then
            sMsg &= "- maximum digit Netto tidak boleh > 16 digit !!<BR>"
        End If
        If ToDouble(Netto.Text) <= 0 Then
            sMsg &= "- Netto tidak boleh <= 0 !!<BR>"
        End If
        If Note1.Text.Length > 100 Then
            sMsg &= "- Karakter Note Tidak Boleh > 100 !!<BR>"
        End If
        If Note2.Text.Length > 100 Then
            sMsg &= "- Karakter Detail Note Tidak Boleh > 100 !!<BR>"
        End If
        sSql = "SELECT COUNT(-1) FROM ql_trnbelimst_fa WHERE cmpcode='" & CompnyCode & "' AND trnbelifaref='" & Tchar(RefNo.Text) & "'"
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            sSql &= " AND trnbelifamstoid<>" & Session("oid")
        End If
        If GetStrData(sSql) > 0 Then
            sMsg &= "- Kode ini sudah digunakan oleh fixed aset yang lain!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "IN PROCESS"
            Exit Sub
        End If
        '==============================
        'Cek Peroide Bulanan Dari Crdgl
        '==============================
        If ToDouble(Netto.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            'sSql = "select distinct isnull(periodacctg,'') FROM QL_crdgl where glflag <> 'CLOSE'"
            'If GetStrData(sSql) <> "" Then
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(fixDate.Text))) < GetStrData(sSql) Then
                showMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "", 2)
                Exit Sub
            End If
        End If
        'End If

        If lblPOST.Text = "POST" Then
            GenerateFixNo()
        End If

        cRate.SetRateValue(CInt(CurrDDL.SelectedValue), toDate(fixDate.Text))
        '-------------------------
        'Rate pajak (Daily Rate)
        '-------------------------
        Rateoid.Text = cRate.GetRateDailyOid
        RateToIDR.Text = cRate.GetRateDailyIDRValue
        RateToUSD.Text = cRate.GetRateDailyUSDValue
        '----------------------------
        'Rate Standart (Monthly Rate)
        '----------------------------
        Rate2oid.Text = cRate.GetRateMonthlyOid
        Rate2ToIDR.Text = cRate.GetRateMonthlyIDRValue
        Rate2ToUsd.Text = cRate.GetRateMonthlyUSDValue
        'If cRate.GetRateDailyLastError <> "" Then
        '    showMessage(cRate.GetRateDailyLastError, 2)
        '    Exit Sub
        'End If
        If cRate.GetRateMonthlyLastError <> "" Then
            showMessage(cRate.GetRateMonthlyLastError, 2)
            Exit Sub
        End If

        ' Generate oid
        Dim MstOid As String = GenerateID("QL_trnbelimst_fa", CompnyCode)
        Dim DtlOid As String = GenerateID("QL_trnbelidtl_fa", CompnyCode)
        Dim MstGLOid As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim DtlGLOid As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim MstConakOid As Integer = GenerateID("QL_conap", CompnyCode)
        Dim acctgPPN As Integer = GetAcctgOID(GetVarInterface("VAR_PPN_IN", CompnyCode), CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnbelimst_fa" & _
                "(cmpcode,branch_code, trnbelifamstoid, periodacctg, trnbelifano, trnbelifadate, trnbelifatype, trnbelifaref, suppoid, pomstoid, trnbelifapaytype, curroid, Rateoid, beliratetoidr, beliratetousd, Rate2oid, belirate2toidr, belirate2tousd, trnbelifaamt, trnbelifaamtidr, trnbelifaamtusd, trnbelifadisctype, trnbelifadiscvalue, trnbelifadiscamt, trnbelifadiscamtidr, trnbelifadiscamtusd, trnbelifamsttaxtype, trnbelifamsttaxpct, trnbelifamsttaxamt, trnbelifamsttaxamtidr, trnbelifamsttaxamtusd, trnbelifaamtnetto, trnbelifaamtnettoidr, trnbelifaamtnettousd, trnbelifamstnote, trnbelifamststatus, createuser, createtime, upduser, updtime)" & _
                " VALUES " & _
                " ('" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & MstOid & ",'" & GetDateToPeriodAcctg(CDate(toDate(fixDate.Text))) & "','" & Tchar(FixAssetNo.Text.Trim) & "','" & CDate(toDate(fixDate.Text)) & "'," & fixgroup.SelectedValue & ",'" & Tchar(RefNo.Text.Trim) & "'," & lblSuppOid.Text & ",0,'" & DDLpayterm.SelectedValue & "','" & CurrDDL.SelectedValue & "','" & Rateoid.Text & "','" & ToDouble(RateToIDR.Text) & "','" & ToDouble(RateToUSD.Text) & "','" & Rate2oid.Text & "','" & ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(Rate2ToUsd.Text) & "','" & ToDouble(TotalDetail.Text) & "','" & ToDouble(TotalDetail.Text) * ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(TotalDetail.Text) * ToDouble(Rate2ToUsd.Text) & "','" & DDLtype1.SelectedValue & "','" & ToDouble(FilterType1.Text) & "','" & ToDouble(DiscHdr.Text) & "','" & ToDouble(DiscHdr.Text) * ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(DiscHdr.Text) * ToDouble(Rate2ToUsd.Text) & "','" & DDLtax.SelectedValue & "','10','" & ToDouble(Tax.Text) & "','" & ToDouble(Tax.Text) * ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(Tax.Text) * ToDouble(Rate2ToUsd.Text) & "','" & ToDouble(Netto.Text) & "','" & ToDouble(Netto.Text) * ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(Netto.Text) * ToDouble(Rate2ToUsd.Text) & "','" & Tchar(Note1.Text) & "','" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"

                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & MstOid & " WHERE tablename='QL_trnbelimst_fa' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            Else
                Dim haha As String = FixAssetNo.Text
                ' Update
                'trnbelifaamt, trnbelifaamtidr, trnbelifaamtusd,
                sSql = "UPDATE QL_trnbelimst_fa SET branch_code = '" & DDLBranch.SelectedValue & "',periodacctg='" & GetDateToPeriodAcctg(CDate(toDate(fixDate.Text))) & "' , trnbelifadate='" & CDate(toDate(fixDate.Text)) & "', trnbelifaref='" & Tchar(RefNo.Text.Trim) & "', suppoid=" & lblSuppOid.Text & ", trnbelifatype='" & fixgroup.SelectedValue & "', trnbelifapaytype='" & DDLpayterm.SelectedValue & "',curroid ='" & CurrDDL.SelectedValue & "', Rateoid ='" & Rateoid.Text & "', beliratetoidr='" & ToDouble(RateToIDR.Text) & "', beliratetousd = '" & ToDouble(RateToUSD.Text) & "', Rate2oid ='" & Rate2oid.Text & "', belirate2toidr = '" & ToDouble(Rate2ToIDR.Text) & "', belirate2tousd = '" & ToDouble(Rate2ToUsd.Text) & "', trnbelifaamt=" & ToDouble(TotalDetail.Text) & ",trnbelifaamtidr ='" & ToDouble(TotalDetail.Text) * ToDouble(Rate2ToIDR.Text) & "', trnbelifaamtusd = '" & ToDouble(TotalDetail.Text) * ToDouble(Rate2ToUsd.Text) & "', trnbelifadisctype='" & DDLtype1.SelectedValue & "', trnbelifadiscvalue=" & ToDouble(FilterType1.Text) & ", trnbelifadiscamt=" & ToDouble(DiscHdr.Text) & ", trnbelifadiscamtidr = '" & ToDouble(DiscHdr.Text) * ToDouble(Rate2ToIDR.Text) & "', trnbelifadiscamtusd ='" & ToDouble(DiscHdr.Text) * ToDouble(Rate2ToUsd.Text) & "',trnbelifamsttaxtype='" & DDLtax.SelectedValue & "', trnbelifamsttaxamt=" & ToDouble(Tax.Text) & ",trnbelifamsttaxamtidr ='" & ToDouble(Tax.Text) * ToDouble(Rate2ToIDR.Text) & "', trnbelifamsttaxamtusd ='" & ToDouble(Tax.Text) * ToDouble(Rate2ToUsd.Text) & "', trnbelifaamtnetto=" & ToDouble(Netto.Text) & ",  trnbelifaamtnettoidr ='" & ToDouble(Netto.Text) * ToDouble(Rate2ToIDR.Text) & "', trnbelifaamtnettousd ='" & ToDouble(Netto.Text) * ToDouble(Rate2ToUsd.Text) & "', trnbelifamstnote ='" & Tchar(Note1.Text) & "', trnbelifamststatus='" & lblPOST.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE trnbelifamstoid = " & Session("oid") & " AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "Delete FROM QL_trnbelidtl_fa where cmpcode='" & CompnyCode & "' AND trnbelifamstoid='" & famstoid.Text & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If Not Session("tbldtl") Is Nothing Then
                Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For i = 0 To objRow.Length - 1
                    ' Insert new dtl
                    sSql = "INSERT INTO QL_trnbelidtl_fa" & _
                    "(cmpcode,branch_code,trnbelifadtloid,trnbelifamstoid,trnbelifadtlseq,itemfacode,itemfadesc,trnbelifadtlqty,trnbelifadtlprice,trnbelifadtldisctype,trnbelifadtldiscvalue,trnbelifadtldiscamt,trnbelifadtlamtnetto,trnbelifadtldep,trnbelifadtllastvalue,trnbelifadtlusage,trnbelifadtlnote,crtuser,crttime,upduser,updtime)" & _
                    " VALUES " & _
                    " ('" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & DtlOid & "," & famstoid.Text & "," & (i + 1) & ",'" & Tchar(objRow(i)("itemfacode").ToString.Trim) & "','" & Tchar(objRow(i)("itemfadesc").ToString.Trim) & "','" & objRow(i)("trnbelifadtlqty").ToString.Trim & "','" & objRow(i)("trnbelifadtlprice").ToString.Trim & "','" & objRow(i)("trnbelifadtldisctype").ToString.Trim & "','" & objRow(i)("trnbelifadtldiscvalue").ToString.Trim & "','" & objRow(i)("trnbelifadtldiscamt").ToString.Trim & "','" & objRow(i)("trnbelifadtlamtnetto").ToString.Trim & "','" & objRow(i)("trnbelifadtldep").ToString.Trim & "','" & objRow(i)("trnbelifadtllastvalue").ToString.Trim & "','0','" & Tchar(objRow(i)("trnbelifadtlnote").ToString.Trim) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() : DtlOid += 1

                Next
                ' Update dtl lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & (DtlOid + objRow.Length - 1) & " WHERE tablename='QL_trnbelidtl_fa' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If lblPOST.Text = "POST" Then
                'Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                sSql = "UPDATE QL_trnbelimst_fa SET trnbelifano='" & Session("sNo") & "' WHERE trnbelifamstoid = " & fixmstoid.Text & " AND cmpcode='" & CompnyCode & "' AND branch_code='" & DDLBranch.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "select suppname FROM QL_mstsupp where suppoid = '" & lblSuppOid.Text & "'"
                Dim SpOid As String = GetStrData(sSql)
                sSql = "select suppacctgoid FROM QL_mstsupp where suppoid = '" & lblSuppOid.Text & "'"
                Dim spAcctgOid As Integer = GetStrData(sSql)

                sSql = "select genother1 FROM QL_mstgen where gengroup = 'ASSETS_TYPE' and genoid = '" & fixgroup.SelectedValue & "'"
                Dim acctgOid As Integer = GetStrData(sSql)
                Dim payType As Integer = 0
                'Session("DueDate") = 0
                If DDLpayterm.SelectedItem.Text = "CASH" Then
                    payType = 0
                    Session("DueDate") = Format(GetServerTime(), "dd/MM/yyyy")
                Else
                    sSql = "select genoid,gendesc FROM QL_mstgen where gengroup = 'PAYTYPE' and genoid = '" & DDLpayterm.SelectedValue & "'"
                    payType = GetStrData(sSql)
                    For C1 As Int16 = 0 To CInt(payType) - 1
                        Session("DueDate") = CDate(toDate(fixDate.Text)).AddDays(Integer.Parse(C1))
                    Next
                End If

                '//////INSERT INTO TABLE CONAP
                sSql = "INSERT INTO QL_conap (cmpcode,branch_code,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnapnote,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd) " & _
                " VALUES " & _
"('" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & MstConakOid & ",'ql_trnbelimst_fa'," & Session("oid") & ",0,'" & lblSuppOid.Text & "','" & spAcctgOid & "','POST','APFA','" & CDate(toDate(fixDate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(fixDate.Text))) & "',0,'1900/1/1','" & Session("sNo") & "',0,'" & Session("DueDate") & "','" & ToDouble(Netto.Text) & "','0.00','FIXED ASSET-PURCHASE-" & Session("sNo") & "- Supplier=" & SpOid & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(Netto.Text) & "','" & 0 * ToDouble(Rate2ToIDR.Text) & "','" & ToDouble(Netto.Text) * ToDouble(Rate2ToUsd.Text) & "','" & 0 * ToDouble(Rate2ToUsd.Text) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                '//////INSERT INTO TRN GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode,branch_code,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type) " & _
                " VALUES " & _
                "( '" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & MstGLOid & ",'" & CDate(toDate(fixDate.Text)) & "','" & GetDateToPeriodAcctg(CDate(toDate(fixDate.Text))) & "','FIXED ASSET-PURCHASE-" & Session("sNo") & "-Supplier=" & SpOid & "','POST',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'ASSET')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'For i = 0 To objRow.Length - 1
                'insert data GL DTL D
                sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glother2,glflag,upduser,updtime,glamtidr,glamtusd) " & _
                " VALUES " & _
                "( '" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & DtlGLOid & ",1," & MstGLOid & "," & acctgOid & ",'D','" & ToDouble(DPP.Text) & "','" & Session("sNo") & "','FIXED ASSET-PURCHASE-" & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & Session("oid") & "','NULL','POST','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(DPP.Text) & "','" & ToDouble(DPP.Text) * ToDouble(Rate2ToUsd.Text) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery() : DtlGLOid += 1
                'Next

                'insert data GL DTL C
                sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glother2,glflag,upduser,updtime,glamtidr,glamtusd) " & _
                " VALUES " & _
                "( '" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & DtlGLOid & ",2," & MstGLOid & "," & spAcctgOid & ",'C','" & ToDouble(Netto.Text) & "','" & Session("sNo") & "','FIXED ASSET-PURCHASE-" & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & MstOid & "','NULL','POST','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(Netto.Text) & "','" & ToDouble(Netto.Text) * ToDouble(Rate2ToUsd.Text) & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'insert data GL DTL PPN
                If Tax.Text > 0 Then

                    DtlGLOid += 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode,branch_code,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glother2,glflag,upduser,updtime,glamtidr,glamtusd) " & _
                    " VALUES " & _
                    "( '" & CompnyCode & "','" & DDLBranch.SelectedValue & "'," & DtlGLOid & ",1," & MstGLOid & "," & acctgPPN & ",'D','" & ToDouble(Tax.Text) & "','" & Session("sNo") & "','FIXED ASSET-PURCHASE-" & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & Session("oid") & "','NULL','POST','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(Tax.Text) & "','" & ToDouble(Tax.Text) * ToDouble(Rate2ToUsd.Text) & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                sSql = "UPDATE QL_mstoid SET lastoid=" & MstConakOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conap' AND branch_code='" & DDLBranch.SelectedValue & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & MstGLOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnglmst'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & DtlGLOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trngldtl'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            lblPOST.Text = "IN PROCESS"
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("tbldtl") = Nothing
        Session("oid") = Nothing
        If lblPOST.Text = "POST" Then
            Session("SavedInfo") &= "Data telah diposting !!!<BR>Fix Asset Code = " & Session("sNo")
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFAPurchase.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim strSQL As String
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            'delete dtl
            strSQL = "Delete FROM QL_trnbelidtl_fa where trnbelifamstoid=" & famstoid.Text & " AND cmpcode='" & CompnyCode & "' AND branch_code='" & DDLBranch.SelectedValue & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            ' delete mst
            strSQL = "DELETE FROM QL_trnbelimst_fa where trnbelifamstoid=" & famstoid.Text & " AND cmpcode='" & CompnyCode & "' AND branch_code='" & DDLBranch.SelectedValue & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If lblPOST.Text <> "" Then
            Session("SavedInfo") &= "Data telah dihapus !!!<BR>Fix Asset Code = " & FixAssetNo.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFAPurchase.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'btnSave.Enabled = True
        'Session("oid") = Nothing
        'Session("tbldtl") = Nothing
        Response.Redirect("trnFAPurchase.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPOST.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub BtnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindDataSupp()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnHapusSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblSuppOid.Text = ""
        Supp.Text = ""
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Supp.Text = gvSupplier.SelectedDataKey(1).ToString().Trim & " - " & gvSupplier.SelectedDataKey(2).ToString().Trim
        lblSuppOid.Text = gvSupplier.SelectedDataKey(0).ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAddtoList_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If Code.Text.Trim = "" Then
            sMsg &= "Code Detail harus diisi !!<BR>"
        End If
        If Qty.Text = "" Then
            sMsg &= "QTY Harus Diisi!!"
        End If
        If ToDouble(Price.Text) <= 0 Then
            sMsg &= "Price Harus Diisi!!"
        End If
        If subTotal.Text.Length > 18 Then
            sMsg &= "maximum digit sub total tidak boleh > 18 digit !!<BR>"
        End If
        If ToDouble(FinalValue.Text) > ToDouble(Price.Text) Then
            sMsg &= "Final Value tidak boleh > dari Price !!<BR>"
        End If
        If DDLtype2.SelectedValue = "PCT" Then
            If FilterType2.Text > 100 Then
                sMsg &= "Discon PCT Detail tidak boleh > 100 !!<BR>"
            End If
        ElseIf DDLtype2.SelectedValue = "AMT" Then
            If ToDouble(FilterType2.Text) > ToDouble(subTotal.Text) Then
                sMsg &= "Discon AMT Detail tidak boleh > dari Sub Total !!<BR>"
            End If
        End If
        'If Note1.Text.Length > 100 Then
        '    sMsg &= "Karakter Note Tidak Boleh > 100 !!<BR>"
        'End If
        If Note2.Text.Length > 100 Then
            sMsg &= "Karakter Detail Note Tidak Boleh > 100 !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If

        If AddToList.Text = "Add To List" Then
            If IsNothing(Session("tbldtl")) Then
                Dim Seq As Integer = 0
                Dim dtlTableNew As DataTable = SetTableDetail()
                Dim xrow As DataRow
                xrow = dtlTableNew.NewRow
                xrow(3) = Seq + 1
                xrow(4) = Code.Text
                xrow(5) = Desc.Text
                xrow(6) = ToDouble(Qty.Text)
                xrow(7) = ToDouble(Price.Text)
                xrow(8) = DDLtype2.SelectedValue
                xrow(9) = ToDouble(FilterType2.Text)
                xrow(10) = ToDouble(discDtl.Text)
                xrow(11) = ToDouble(subTotal.Text)
                xrow(12) = ToDouble(fixdepmonth.Text)
                xrow(13) = ToDouble(FinalValue.Text)
                xrow(14) = Note2.Text
                dtlTableNew.Rows.Add(xrow)
                Session("tbldtl") = dtlTableNew
                GVFixedAssetdtl.DataSource = Session("tbldtl")
                GVFixedAssetdtl.DataBind()
                ClearDtl()
            Else
                Dim Seq As Integer = GVFixedAssetdtl.Rows.Count
                Dim dtlTable As DataTable
                dtlTable = Session("tbldtl")
                Dim dv As DataView = dtlTable.DefaultView
                dv.RowFilter = "trnbelifadtloid = " & fadtloid.Text & ""

                ' cek apakah data telah ada
                'If dv.Count > 0 Then
                '    dv.RowFilter = ""
                '    QLMsgBox1.ShowMessage("COA ini telah digunakan !!", 2, "")
                '    ClearDtlAP()
                '    Exit Sub

                'Else
                '    ' kalo data blom ada

                dv.RowFilter = ""
                'dv.Sort = "seq desc"
                'If dv.Count > 0 Then
                '    Seq = dv.Item(0).Item(0)
                'Else
                '    Seq = 0
                'End If

                Dim xrow As DataRow
                xrow = dtlTable.NewRow
                xrow(3) = Seq + 1
                xrow(4) = Code.Text
                xrow(5) = Desc.Text
                xrow(6) = ToDouble(Qty.Text)
                xrow(7) = ToDouble(Price.Text)
                xrow(8) = DDLtype2.SelectedValue
                xrow(9) = ToDouble(FilterType2.Text)
                xrow(10) = ToDouble(discDtl.Text)
                xrow(11) = ToDouble(subTotal.Text)
                xrow(12) = ToDouble(fixdepmonth.Text)
                xrow(13) = ToDouble(FinalValue.Text)
                xrow(14) = Note2.Text
                dtlTable.Rows.Add(xrow)
                Session("tbldtl") = dtlTable
                Dim dv2 As DataView = dtlTable.DefaultView
                'dv2.Sort = "seq desc"
                GVFixedAssetdtl.DataSource = dtlTable
                GVFixedAssetdtl.DataBind()
                ClearDtl()
                'End If
            End If
        Else
            ' untuk transaksi edit
            Dim xindex = GVFixedAssetdtl.SelectedIndex
            Dim dt_temp As New DataTable
            dt_temp = Session("tbldtl")
            dt_temp.Rows(xindex).BeginEdit()
            dt_temp.Rows(xindex).Item(4) = Code.Text
            dt_temp.Rows(xindex).Item(5) = Desc.Text
            dt_temp.Rows(xindex).Item(6) = ToDouble(Qty.Text)
            dt_temp.Rows(xindex).Item(7) = ToDouble(Price.Text)
            dt_temp.Rows(xindex).Item(8) = DDLtype2.SelectedValue
            dt_temp.Rows(xindex).Item(9) = ToDouble(FilterType2.Text)
            dt_temp.Rows(xindex).Item(10) = ToDouble(discDtl.Text)
            dt_temp.Rows(xindex).Item(11) = ToDouble(subTotal.Text)
            dt_temp.Rows(xindex).Item(12) = ToDouble(fixdepmonth.Text)
            dt_temp.Rows(xindex).Item(13) = ToDouble(FinalValue.Text)
            dt_temp.Rows(xindex).Item(14) = Note2.Text
            dt_temp.Rows(xindex).EndEdit()
            Session("tbldtl") = dt_temp
            GVFixedAssetdtl.DataSource = dt_temp
            GVFixedAssetdtl.DataBind()
            AddToList.Text = "Add To List"
            ClearDtl()
        End If
        TotalDtl()
        GVFixedAssetdtl.SelectedIndex = -1
    End Sub

    Protected Sub Price_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmountDep()
    End Sub

    Protected Sub Qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmountDep()
    End Sub

    Protected Sub DDLtype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType2.Text = "0"
        ReAmountDep()
    End Sub

    Protected Sub FilterType2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        discDtl.Text = ToMaskEdit(ToDouble(FilterType2.Text), 4)
        subTotal.Text -= ToMaskEdit(ToDouble(discDtl.Text), 4)
        ReAmountDep()
    End Sub

    Protected Sub btnSearchCode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing
        Session("TblMatView") = Nothing
        GVmstitem.DataSource = Nothing
        GVmstitem.DataBind()
        BindDataItem()
        'cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnHapusCode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'GVmstitem.Visible = False
        Code.Text = ""
        Desc.Text = ""
    End Sub

    Protected Sub GVmstitem_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Code.Text = GVmstitem.SelectedDataKey(1).ToString().Trim
        Desc.Text = GVmstitem.SelectedDataKey(2).ToString().Trim
        lblCode.Text = GVmstitem.SelectedDataKey(1).ToString().Trim
        lblItemOid.Text = GVmstitem.SelectedDataKey(0).ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub GVmstitem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstitem.PageIndexChanging
        GVmstitem.PageIndex = e.NewPageIndex
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtl()
        TotalDtl()
    End Sub

    Protected Sub FinalValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FinalValue.Text = ToMaskEdit(ToDouble(FinalValue.Text), 4)
    End Sub

    Protected Sub GVFixedAssetdtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        If Status.Text = "IN PROCESS" Then
            Dim test As String = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")
            'Dim iCountrow As Integer = objTable.Rows.Count
            'Dim idx As Integer = iCountrow - 1 - e.RowIndex
            objTable.Rows.RemoveAt(test)
            Session("tbldtl") = objTable
            GVFixedAssetdtl.DataSource = objTable
            GVFixedAssetdtl.DataBind()
        Else
            showMessage("Data terposting tidak dapat di hapus!!", 2)
        End If
        TotalDtl()
    End Sub

    Protected Sub TotalDetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TotalDetail.Text = ToMaskEdit(ToDouble(TotalDetail.Text), 4)
        ReAmountHdr()
    End Sub

    Protected Sub TotalDiscDetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TotalDiscDetail.Text = ToMaskEdit(ToDouble(TotalDiscDetail.Text), 4)
    End Sub

    Protected Sub DDLtax_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmountHdr()
    End Sub

    Protected Sub DDLtype1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType1.Text = "0"
        ReAmountHdr()
    End Sub

    Protected Sub FilterType1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType1.Text = ToMaskEdit(ToDouble(FilterType1.Text), 4)
        ReAmountHdr()
    End Sub

    Protected Sub Netto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Netto.Text = ToMaskEdit(ToDouble(Netto.Text), 4)
    End Sub

    Protected Sub DDLoutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generatemstoid()
        GenerateFixNo()
        FixAssetNo.Text = Session("sNo")
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowCOAPosting(FixAssetNo.Text, CompnyCode, gvakun) 'cashbankoid.Text)
        pnlPosting2.Visible = True
        btnHidePosting2.Visible = True
        mpePosting2.Show()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub CurrDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim sErr As String = ""
        'If fixDate.Text <> "" Then
        '    If IsValidDate(CDate(toDate(fixDate.Text)), "dd/MM/yyyy", sErr) Then
        '        If CurrDDL.SelectedValue <> "" Then
        '            cRate.SetRateValue(CInt(CurrDDL.SelectedValue), fixDate.Text)
        '            If cRate.GetRateDailyLastError <> "" Then
        '                showMessage(cRate.GetRateDailyLastError, 2)
        '                Rateoid.Text = "" : RateToIDR.Text = "" : RateToUSD.Text = ""
        '                Exit Sub
        '            End If
        '            If cRate.GetRateMonthlyLastError <> "" Then
        '                showMessage(cRate.GetRateMonthlyLastError, 2)
        '                Rate2oid.Text = "" : Rate2ToIDR.Text = "" : Rate2ToUsd.Text = ""
        '                Exit Sub
        '            End If
        '            Rateoid.Text = cRate.GetRateDailyOid
        '            RateToIDR.Text = cRate.GetRateDailyIDRValue
        '            RateToUSD.Text = cRate.GetRateDailyUSDValue
        '            Rate2oid.Text = cRate.GetRateMonthlyOid
        '            Rate2ToIDR.Text = cRate.GetRateMonthlyIDRValue
        '            Rate2ToUsd.Text = Rate.GetRateMonthlyUSDValue
        '        End If
        '    End If
        'End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnFAPurchase.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub fixDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsValidDate(fixDate.Text, "dd/MM/yyyy", "") Then
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End If
    End Sub
#End Region

    Protected Sub GVFixedAssetdtl_RowDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 4)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 4)
        End If
    End Sub
End Class
