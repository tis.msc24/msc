<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="frmFixedAsset.aspx.vb" Inherits="ReportForm_frmFixedAsset" title="" EnableEventValidation="false" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Lbl1" runat="server" Font-Bold="True" Text=".: Fixed Assets Report" CssClass="Title" ForeColor="Navy" Font-Size="X-Large"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<DIV align=center><TABLE style="WIDTH: 533px" width="100%" align=center><TBODY><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" id="TD1" vAlign=top colSpan=1 runat="server" Visible="false"><asp:Label id="Label2" runat="server" Text="Busines Unit"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" id="TD3" vAlign=top align=center colSpan=1 runat="server" Visible="false"><asp:Label id="lbl11" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 602px; TEXT-ALIGN: left" id="TD2" vAlign=top align=center width=843 colSpan=2 runat="server" Visible="false"><asp:DropDownList id="ddlOutlet" runat="server" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label20" runat="server" Width="80px" Text="Report Type"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top align=center colSpan=1><asp:Label id="Label5" runat="server" Text=":"></asp:Label></TD><TD style="WIDTH: 602px; TEXT-ALIGN: left" vAlign=top align=center width=843 colSpan=2><asp:DropDownList id="DDLType" runat="server" CssClass="inpText" OnSelectedIndexChanged="DDLType_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem Value="Summary">Summary</asp:ListItem>
<asp:ListItem Value="Detail">Detail</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" id="TD5" vAlign=top colSpan=1 runat="server" Visible="false"><asp:Label id="Label14" runat="server" Text="Currency"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" id="TD4" vAlign=top colSpan=1 runat="server" Visible="false"><asp:Label id="Label10" runat="server" Text=":"></asp:Label></TD><TD id="TD6" vAlign=top align=left colSpan=2 runat="server" Visible="false"><asp:DropDownList id="CurrDDL" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label8" runat="server" Width="80px" Text="Type Asset"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top colSpan=1><asp:Label id="Label7" runat="server" Text=":"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="FixAssets" runat="server" Width="175px" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchFix" onclick="btnSearch_Click" runat="server" Width="17px" ImageUrl="~/Images/search.gif" ImageAlign="AbsBottom" Height="17px"></asp:ImageButton> <asp:ImageButton id="btnClearFix" onclick="imbClearAcctg_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsBottom"></asp:ImageButton> <asp:Label id="FixAcctgoid" runat="server" Visible="False"></asp:Label> </TD></TR><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label6" runat="server" Text="Status" Visible="False"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top colSpan=1><asp:Label id="Label9" runat="server" Text=":" Visible="False"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:DropDownList id="ddlStatus" runat="server" CssClass="inpText" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="Post">POST</asp:ListItem>
<asp:ListItem Value="In Process">IN PROCESS</asp:ListItem>
<asp:ListItem>DISPOSED</asp:ListItem>
</asp:DropDownList><asp:DropDownList id="ddlStatusDtl" runat="server" CssClass="inpText" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="Post">POST</asp:ListItem>
<asp:ListItem Value="In Process">IN PROCESS</asp:ListItem>
<asp:ListItem>CLOSED</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 71px; TEXT-ALIGN: left" vAlign=top colSpan=1><asp:Label id="Label4" runat="server" Text="Periode" Visible="False"></asp:Label></TD><TD style="WIDTH: 6px; TEXT-ALIGN: center" vAlign=top colSpan=1><asp:Label id="Label21" runat="server" Text=":" Visible="False"></asp:Label></TD><TD vAlign=top align=left colSpan=2><asp:TextBox id="Fixdate1" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox>&nbsp;<asp:ImageButton id="ibcal1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label1" runat="server" Text="to" Visible="False"></asp:Label> <asp:TextBox id="Fixdate2" runat="server" Width="75px" CssClass="inpText" Visible="False"></asp:TextBox> <asp:ImageButton id="ibcal2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:Label id="Label3" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" vAlign=bottom colSpan=4><asp:ImageButton id="btnViewPrint" onclick="btnViewPrint_Click" runat="server" ImageUrl="~/Images/viewreport.png"></asp:ImageButton> <asp:ImageButton id="btnExportToPdf" onclick="btnExportToPdf_Click" runat="server" ImageUrl="~/Images/topdf.png"></asp:ImageButton> <asp:ImageButton id="ExportToXls" onclick="ExportToXls_Click" runat="server" ImageUrl="~/Images/toexcel.png"></asp:ImageButton> <asp:ImageButton id="ibClear" onclick="ibClear_Click" runat="server" ImageUrl="~/Images/clear.png"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 130px; TEXT-ALIGN: center" vAlign=top colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image2" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image><BR />Loading report ... 
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" PopupButtonID="ibcal1" Format="dd/MM/yyyy" TargetControlID="Fixdate1"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="ibcal2" Format="dd/MM/yyyy" TargetControlID="Fixdate2"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" TargetControlID="Fixdate1" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" TargetControlID="Fixdate2" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></DIV><cr:crystalreportviewer id="CrystalReportViewer1" runat="server" Width="350px" autodatabind="true" DisplayGroupTree="False"></cr:crystalreportviewer> 
</contenttemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="CrystalReportViewer1"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnExportToPdf"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnViewPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ExportToXls"></asp:PostBackTrigger>
</triggers>
                </asp:UpdatePanel>
<asp:UpdatePanel id="upListSupp" runat="server"><contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" CssClass="modalBox" Visible="False" __designer:wfdid="w3"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Fixed Asset" __designer:wfdid="w4"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" __designer:wfdid="w5" DefaultButton="btnFindListSupp"><asp:Label id="Label11" runat="server" Text="Filter : " __designer:wfdid="w6"></asp:Label>&nbsp;<asp:DropDownList id="FilterFAPDDL" runat="server" CssClass="inpText" __designer:wfdid="w7"><asp:ListItem Value="g.gencode">Asset Code</asp:ListItem>
<asp:ListItem Value="g.gendesc">Asset Type</asp:ListItem>
<asp:ListItem Value="g.gengroup">Group Asset</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterFAP" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="GvFAPmst" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="GvFAPmst_SelectedIndexChanged" __designer:wfdid="w11" DataKeyNames="genoid,gencode,gendesc" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5" AllowSorting="True" OnPageIndexChanging="GvFAPmst_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="gencode" HeaderText="Asset Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Asset Type">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gengroup" HeaderText="Asset Group">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server" __designer:wfdid="w12">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" __designer:wfdid="w13" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False" __designer:wfdid="w14"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Width="600px" CssClass="modalMsgBox" Visible="False" __designer:wfdid="w16"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w17"></asp:Label></TD></TR><TR><TD colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w18"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w19"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w20"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" __designer:wfdid="w21" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w22"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

