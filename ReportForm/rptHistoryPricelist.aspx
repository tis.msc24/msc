<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptHistoryPricelist.aspx.vb" Inherits="rptStock" title="Padjajaran - Laporan Mutasi Stock" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="21px" ForeColor="Navy" Text=".: Laporan History PriceList Item"></asp:Label></th>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center" colspan="3">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=left><asp:Label id="Label2" runat="server" Text="Tipe Laporan :" Visible="False" __designer:wfdid="w96"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" Visible="False" __designer:wfdid="w97" AutoPostBack="True"><asp:ListItem>Detail</asp:ListItem>
<asp:ListItem>Summary</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="false"><asp:Label id="Label6" runat="server" Text="Periode : " __designer:wfdid="w98"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w99"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w100"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w101"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w102"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w103">(dd/mm/yyyy)</asp:Label><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w104" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w105" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w106" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w107" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD id="tdperiod3" align=left runat="server" Visible="false"><asp:Label id="Label1" runat="server" Text="Lokasi : " Visible="False" __designer:wfdid="w108"></asp:Label></TD><TD id="tdperiod4" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="ddlLocation" runat="server" Width="230px" CssClass="inpText" Visible="False" __designer:wfdid="w135" AutoPostBack="True"></asp:DropDownList>Periode :<asp:DropDownList id="period" runat="server" Width="135px" CssClass="inpText" Visible="False" __designer:wfdid="w136"></asp:DropDownList></TD></TR><TR><TD align=left>Grup :</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLGrup" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w134"></asp:DropDownList>&nbsp; </TD></TR><TR><TD align=left>Sub Group :</TD><TD align=left colSpan=3><asp:DropDownList id="FilterDDLSubGrup" runat="server" Width="230px" CssClass="inpText" __designer:wfdid="w133"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Width="91px" Text="Item / Barang :" __designer:wfdid="w74"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="itemname" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w114"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w115"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w116"></asp:ImageButton>&nbsp;&nbsp;<asp:Label id="itemoid" runat="server" Visible="False" __designer:wfdid="w117"></asp:Label></TD></TR><TR><TD align=right></TD><TD align=left colSpan=3><asp:GridView id="gvItem" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w79" CellPadding="4" UseAccessibleHeader="False" PageSize="8" DataKeyNames="itemcode,itemdesc,itempriceunit1,itempriceunit2,itempriceunit3,itemoid,satuan1,satuan2,satuan3,konversi1_2,konversi2_3" AutoGenerateColumns="False" AllowPaging="True" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" ForeColor="Navy" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Kode Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itemdesc" HeaderText="Nama Barang">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="satuan1" HeaderText="Sat Besar" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit1" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan2" HeaderText="Sat Sdg" Visible="False"></asp:BoundField>
<asp:BoundField DataField="Itempriceunit2" HeaderText="Harga" Visible="False"></asp:BoundField>
<asp:BoundField DataField="satuan3" HeaderText="Sat Std">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Itempriceunit3" HeaderText="Harga" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left><asp:Label id="lblfilterqty" runat="server" Width="70px" Text="Stok Akhir :" __designer:wfdid="w80"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="ddlfilterqty" runat="server" Width="71px" CssClass="inpText" __designer:wfdid="w120"><asp:ListItem>IGNORE</asp:ListItem>
<asp:ListItem>&lt;</asp:ListItem>
<asp:ListItem>&lt;=</asp:ListItem>
<asp:ListItem>&gt;</asp:ListItem>
<asp:ListItem>&gt;=</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="filterqty" runat="server" Width="97px" CssClass="inpText" __designer:wfdid="w121" AutoPostBack="True"></asp:TextBox><ajaxToolkit:FilteredTextBoxExtender id="fte5" runat="server" __designer:wfdid="w122" TargetControlID="filterqty" Enabled="True" ValidChars=",.0123456789"></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD id="Td3" align=left runat="server" Visible="false">SPG</TD><TD id="Td4" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="person" runat="server" Width="280px" CssClass="inpText" __designer:wfdid="w123"></asp:DropDownList></TD></TR><TR><TD id="Td5" align=left colSpan=4 runat="server"><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w125"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibPDF" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w126"></asp:ImageButton>&nbsp;<asp:ImageButton id="ibexcel" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w127"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w128"></asp:ImageButton> <asp:ImageButton id="export_konversi" runat="server" ImageUrl="~/Images/preview.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w124"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="Td6" align=center colSpan=4 runat="server" Visible="false"><asp:Label id="lblkonfirmasi" runat="server" ForeColor="Red" __designer:wfdid="w129"></asp:Label></TD></TR><TR><TD style="VERTICAL-ALIGN: top" id="Td7" align=center colSpan=4 runat="server" Visible="false"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w130"><ProgressTemplate>
<STRONG><SPAN style="FONT-SIZE: 14pt; COLOR: #800080">Please Wait....</SPAN></STRONG><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w131"></asp:Image> 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crv" runat="server" __designer:wfdid="w132" HasZoomFactorList="False" HasToggleGroupTreeButton="False" HasDrillUpButton="False" HasViewList="False" HasSearchButton="False" HasExportButton="False" DisplayGroupTree="False" HasPrintButton="False" HasGotoPageButton="False" HasCrystalLogo="False" AutoDataBind="true"></CR:CrystalReportViewer> 
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="ibPDF"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="export_konversi"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>

