Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptNotaJual
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim ckon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Functions"
    Public Function GetSessionCheckItem() As Boolean
        If Session("CheckItem") = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckon.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "trnrequestitemoid=" & cbOid
                                dtView2.RowFilter = "trnrequestitemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCust") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblCust") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedCust2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtTbl2 As DataTable = Session("TblCustView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListCust.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListCust.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "custoid=" & cbOid
                                dtView2.RowFilter = "custoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblCust") = dtTbl
                Session("TblCustView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblReg") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblReg") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedReg2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtTbl2 As DataTable = Session("TblRegView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListReg.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListReg.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "registermstoid=" & cbOid
                                dtView2.RowFilter = "registermstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblReg") = dtTbl
                Session("TblRegView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function
#End Region

#Region "Procedures"
    Private Sub BindListSO()
        Try
            If CDate(toDate(dateAwal.Text)) > CDate(toDate(dateAkhir.Text)) Then
                showMessage("Period 2 must be more than Period 1 !", 2)
                Exit Sub
            End If
        Catch ex As Exception
            showMessage("Please check Period value", 2)
            Exit Sub
        End Try

        sSql = "SELECT 'False' AS checkvalue, mst.trnjualmstoid trnrequestitemoid, Convert(Varchar(20),mst.trnjualdate,103) trnrequestitemdate, mst.trnjualno trnrequestitemno, mst.branch_code branchcode, cb.gendesc, mst.trnjualstatus trnrequestitemstatus, ISNULL((Select om.trnordernote from QL_trnordermst om Where om.ordermstoid=mst.ordermstoid AND om.branch_code=mst.branch_code),'') trnrequestitemnote FROM QL_trnjualmst mst INNER JOIN QL_mstgen cb ON cb.gencode=mst.branch_code AND cb.gengroup='CABANG' Where convert(char(10),mst.trnjualdate,121) Between '" & Format(CDate(toDate(dateAwal.Text)), "yyyy-MM-dd") & "' And '" & Format(CDate(toDate(dateAkhir.Text)), "yyyy-MM-dd") & "' " & IIf(DDLcabang.SelectedValue = "ALL", " ", " And mst.branch_code='" & DDLcabang.SelectedValue & "'") & ""
        If DDLPenjType.SelectedValue <> "ALL" Then
            sSql &= " AND ordermstoid IN (Select ordermstoid From QL_trnordermst om Where om.ordermstoid=mst.ordermstoid AND om.branch_code=mst.branch_code "
            Dim typeSO As String = ""
            For C1 As Integer = 0 To cbType.Items.Count - 1
                If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                    typeSO &= "'" & cbType.Items(C1).Value & "',"
                End If
            Next

            If typeSO <> "" Then
                typeSO = Left(typeSO, typeSO.Length - 1)
                sSql &= "And om.typeSO IN (" & typeSO & ")"
            End If
            sSql &= ")"
        End If
        sSql &= "ORDER BY mst.trnjualmstoid"
        Session("TblSO") = ckon.ambiltabel(sSql, "QL_trnordermst")
    End Sub

    Private Sub BindListCust()
        sSql = "SELECT 'False' AS checkvalue, gendesc, custoid, custcode, custname, custaddr FROM QL_mstcust c INNER JOIN QL_mstgen b ON b.gencode=c.branch_code AND b.gengroup='CABANG' WHERE c.cmpcode='" & CompnyCode & "'"
        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND c.branch_code='" & DDLcabang.SelectedValue & "'"
        End If
        sSql &= " AND c.custoid IN (SELECT jm.trncustoid FROM QL_trnjualmst jm Where jm.branch_code=c.branch_code"
        If SoNo.Text <> "" Then
            Dim scode() As String = Split(SoNo.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " jm.trnjualno = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND jm.trnjualdate>='" & CDate(toDate(dateAwal.Text)) & " 00:00:00' AND jm.trnjualdate<='" & CDate(toDate(dateAkhir.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY custcode"
        Session("TblCust") = ckon.ambiltabel(sSql, "QL_mstCust")
    End Sub

    Public Sub BindDataListItem()
        sSql = "SELECT 'False' Checkvalue, i.itemcode, i.itemdesc, i.itemoid, i.Merk, 'BUAH' AS satuan3 FROM ql_mstitem i WHERE (itemdesc LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%' OR i.itemcode LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%') AND itemoid IN (SELECT jd.itemoid FROM QL_trnjualdtl jd INNER JOIN ql_trnjualmst jm ON jd.branch_code=jm.branch_code AND jd.trnjualmstoid=jm.trnjualmstoid INNER JOIN QL_mstcust c ON c.custoid=jm.trncustoid AND c.branch_code=jm.branch_code LEFT JOIN QL_MSTPERSON pr ON pr.personoid=jm.spgoid WHERE i.itemoid=jd.itemoid"

        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND jm.branch_code='" & DDLcabang.SelectedValue & "'"
        End If
 
        If SoNo.Text <> "" Then
            Dim scode() As String = Split(SoNo.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " jm.trnjualno = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " c.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If regno.Text <> "" Then
            Dim sregno() As String = Split(regno.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sregno.Length - 1
                sSql &= " pr.PERSONNAME LIKE '%" & Tchar(sregno(c1)) & "%'"
                If c1 < sregno.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND jm.trnjualdate>='" & CDate(toDate(dateAwal.Text)) & " 00:00:00' AND jm.trnjualdate<='" & CDate(toDate(dateAkhir.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= ") ORDER BY itemcode"
        Dim dt As DataTable = ckon.ambiltabel(sSql, "QL_mstitem")
        Session("TblMat") = dt
        gvItem.DataSource = Session("TblMat")
        gvItem.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Private Sub BindListReg()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, personoid registermstoid, personname registerno, gendesc Jabatan, pr.STATUS, pr.PERSONNIP FROM QL_mstperson pr Inner Join QL_MSTPROF pf ON pf.personnoid=pr.PERSONOID Inner Join QL_mstgen jp ON jp.genoid=pr.personstatus AND jp.gengroup='JOBPOSITION' Inner Join QL_trnjualmst jm ON jm.spgoid=pr.PERSONOID AND pf.BRANCH_CODE=jm.branch_code Inner Join QL_mstcust cuk ON cuk.custoid=jm.trncustoid AND jm.branch_code=cuk.branch_code WHERE pr.CMPCODE = '" & CompnyCode & "' AND (genother2='YES' OR FLAGSALES='YES')"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= "AND pf.USERID='" & Session("UserID") & "'"
        End If

        If DDLcabang.SelectedValue <> "ALL" Then
            sSql &= " AND pf.BRANCH_CODE='" & DDLcabang.SelectedValue & "'"
        End If

        If custcode.Text <> "" Then
            Dim scode() As String = Split(custcode.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To scode.Length - 1
                sSql &= " cuk.custcode = '" & Tchar(scode(c1)) & "'"
                If c1 < scode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dateAwal.Text <> "" And dateAkhir.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND jm.trnjualdate>='" & CDate(toDate(dateAwal.Text)) & " 00:00:00' AND jm.trnjualdate<='" & CDate(toDate(dateAkhir.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY personname DESC"
        Session("TblReg") = ckon.ambiltabel(sSql, "QL_Sales")
    End Sub

    Public Sub sCabang()
        Dim sSql As String = "select gencode,gendesc from ql_mstgen where gengroup = 'cabang'"
        FillDDL(DDLcabang, sSql)
        DDLcabang.Items.Add(New ListItem("ALL", "ALL"))
        If Session("branch_id") <> "10" Then
            DDLcabang.SelectedValue = Session("branch_id")
        Else
            DDLcabang.SelectedIndex = DDLcabang.Items.Count - 1
        End If
    End Sub

    Public Sub initddl(ByVal qCabang As String) 
        sSql = "Select Distinct typeSO, Case When typeSO='ecommers' then 'E-Commerce' else typeSO End TypeNya From QL_trnordermst"
        cbType.Items.Clear()
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xCmd = New SqlCommand(sSql, conn)
        xreader = xCmd.ExecuteReader
        While xreader.Read
            cbType.Items.Add(xreader.GetValue(1).ToString)
            cbType.Items(cbType.Items.Count - 1).Value = xreader.GetValue(0).ToString
        End While
        xreader.Close()
        conn.Close()
        Session("LastAllCheck") = False
    End Sub

    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Public Sub showPrint(ByVal tipe As String)
        lblkonfirmasi.Text = "" : Session("diprint") = "False"
        Try
            Dim sWhere As String = "", sWhereuser As String = "", namaPDF As String = "", sWhereloc As String = "", sWhereitem As String = "", sWheregrup As String = "", sWheresubgrup As String = "", typeSO As String = ""
            vReport = New ReportDocument
            '& IIf(DDLPenjType.SelectedValue = "ALL", " ", " And o.typeso = '" & DDLPenjType.SelectedValue & "'") 
            sWhere = " Where mst.trnjualdate BETWEEN '" & CDate(toDate(dateAwal.Text)) & " 00:00' And '" & CDate(toDate(dateAkhir.Text)) & " 23:59' " & IIf(status.SelectedValue = "All", " ", " And mst.trnjualstatus = '" & status.SelectedValue & "' ") & IIf(DDLcabang.SelectedValue = "ALL", " ", " And mst.branch_code= '" & DDLcabang.SelectedValue & "'") & ""

            For C1 As Integer = 0 To cbType.Items.Count - 1
                If cbType.Items(C1).Value <> "All" And cbType.Items(C1).Selected = True Then
                    typeSO &= "'" & cbType.Items(C1).Value & "',"
                End If
            Next

            If typeSO <> "" Then
                typeSO = Left(typeSO, typeSO.Length - 1)
                sWhere &= "And o.typeSO IN (" & typeSO & ")"
            End If

            If SoNo.Text <> "" Then
                Dim scode() As String = Split(SoNo.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " mst.trnjualno = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If custcode.Text <> "" Then
                Dim scode() As String = Split(custcode.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To scode.Length - 1
                    sWhere &= " cust.custcode = '" & Tchar(scode(c1)) & "'"
                    If c1 < scode.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If regno.Text <> "" Then
                Dim sregno() As String = Split(regno.Text, ";")
                sWhere &= " AND ("
                For c1 As Integer = 0 To sregno.Length - 1
                    sWhere &= " p.PERSONNAME LIKE '%" & Tchar(sregno(c1)) & "%'"
                    If c1 < sregno.Length - 1 Then
                        sWhere &= " OR "
                    End If
                Next
                sWhere &= ")"
            End If

            If type.SelectedValue = "SUMMARY" Then
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptnotajualSumExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptnotajualSum.rpt"))
                End If 
                namaPDF = "SI_(Sum)_" 
            Else
                If tipe = "excel" Then
                    vReport.Load(Server.MapPath("~\Report\rptNotajualDetailExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptnotajualDetail.rpt"))
                End If

                If PicTxt.Text.Trim <> "" Then                    ' 
                    sWhere &= " AND (pic.PERSONNAME LIKE '%" & Tchar(PicTxt.Text.Trim) & "%' OR pic.PERSONNIP LIKE '%" & Tchar(PicTxt.Text.Trim) & "%')"
                End If

                Dim arCode() As String = ItemName.Text.Split(";")
                Dim sCodeIn As String = "" : Dim sQel As String = ""
                Dim adr As String = ""
                For C1 As Integer = 0 To arCode.Length - 1
                    If arCode(C1) <> "" Then
                        sCodeIn &= "'" & arCode(C1) & "',"
                    End If
                Next
                If sCodeIn <> "" Then
                    sWhere &= " AND itemcode IN (" & Left(sCodeIn, sCodeIn.Length - 1) & ")"
                End If
                namaPDF = "SI_(Dtl)_" 
            End If

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With

            setDBLogonForReport(crConnInfo, vReport)
            vReport.SetParameterValue("sWhere", swhere)
            vReport.SetParameterValue("sWhereuser", sWhereuser)
            vReport.SetParameterValue("startperiod", dateAwal.Text)
            vReport.SetParameterValue("endperiod", dateAkhir.Text)
            vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            vReport.SetParameterValue("titledef", "")
            Session("diprint") = "True"

            If tipe = "view" Then
                CrystalReportViewer1.DisplayGroupTree = False
                CrystalReportViewer1.ReportSource = vReport
            ElseIf tipe = "excel" Then 
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptNotaJual.aspx?awal=true")
            ElseIf tipe = "pdf" Then 
                Response.Buffer = False
                Response.ClearContent()
                Response.ClearHeaders()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime(), "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
                Response.Redirect("~\ReportForm\rptNotaJual.aspx?awal=true")
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = ex.Message
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", ex.Message))
        End Try
    End Sub

    Public Sub PicDataBind()
        Dim sItem As String = ""
        If itemoid.Text <> "" Then
            sItem &= " And i.itemoid=" & itemoid.Text & ""
        End If
        sSql = "SELECT DISTINCT pic.PERSONOID,pic.PERSONNIP,pic.PERSONNAME FROM QL_MSTPERSON pic INNER JOIN QL_MSTITEM i ON pic.PERSONOID=i.personoid WHERE (pic.PERSONNAME LIKE '%" & Tchar(PicTxt.Text.Trim) & "%' OR Pic.PERSONNIP LIKE '%" & Tchar(PicTxt.Text.Trim) & "%') AND i.itemoid IN (SELECT jd.itemoid FROM QL_trnjualdtl jd INNER JOIN QL_trnjualmst jm ON jm.trnjualmstoid=jd.trnjualmstoid AND jm.branch_code=jd.branch_code AND jd.itemoid=i.itemoid WHERE jm.trnjualno LIKE '%%' " & sItem & ")"
        FillGV(gvPic, sSql, "ql_trnjualmst")
        gvPic.Visible = True
    End Sub
#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") : Dim userName As String = Session("UserName")  ' simpan session k variabel spy tidak hilang
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Session.Clear()  ' -->>  clear all session 
            Session("SpecialAccess") = xsetAcc
            Session.Clear() : Session("UserName") = userName   '  clear all session
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId 'insert lagi sesion yg disimpan dan create session lagi
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptNotaJual.aspx") ' di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Laporan Nota Penjualan "
        If kacabCek("ReportForm/rptNotaJual.aspx", Session("UserID")) = True Or Session("UserID") = "POPY" Then
            ibexcel.Visible = True : ibpdf.Visible = True
        Else
            If Session("branch_id") <> "10" Then
                ibexcel.Visible = True : ibpdf.Visible = True
            Else
                ibexcel.Visible = True : ibpdf.Visible = True
            End If
        End If

        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If type.SelectedValue = "SUMMARY" Then
            Label11.Visible = False : Label3.Visible = False
            ItemName.Visible = False : ItemName.CssClass = "inpTextDisabled"
            btnSearchItem.Visible = False : btnEraseItem.Visible = False
        Else
            Label11.Visible = True : Label3.Visible = True
            ItemName.Visible = True : ItemName.CssClass = "inpTextDisabled"
            btnSearchItem.Visible = True : btnEraseItem.Visible = True
        End If
        If IsPostBack Then
            If Session("diprint") = "True" Then
                showPrint("view")
            End If
        Else
            If Session("branch_id") = "10" Then
                DDLcabang.Visible = True : Label12.Visible = True : Label13.Visible = True

            Else
                DDLcabang.Enabled = False : DDLcabang.CssClass = "inpTextDisabled"
                Label12.Enabled = False : Label13.Enabled = False
            End If
            initddl("") : sCabang()
            DDLcabang_SelectedIndexChanged(Nothing, Nothing)
            dateAwal.Text = Format(GetServerTime, "01/MM/yyyy")
            dateAkhir.Text = Format(GetServerTime, "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewReport.Click
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("view")
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptNotaJual.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("excel")
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        UpdateCheckedMat()
        gvItem.PageIndex = e.NewPageIndex
        Dim dtItem As DataTable = Session("TblMat")
        gvItem.DataSource = dtItem
        gvItem.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub gvItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItem.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            Dim cb As System.Web.UI.WebControls.CheckBox
            cb = e.Row.FindControl("chkAllItem")
            cb.Checked = GetSessionCheckItem()
        End If
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        itemname.Text = gvItem.SelectedDataKey.Item(1)
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        gvItem.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
        Session("diprint") = "False"
    End Sub

    Protected Sub btnEraseItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEraseItem.Click
        ItemName.Text = "" : itemoid.Text = ""
        'gvItem.Visible = False
    End Sub

    Protected Sub btnSearchItem_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        lblkonfirmasi.Text = "" 
        Session("TblMat") = Nothing : Session("TblMat") = Nothing
        gvItem.DataSource = Nothing : gvItem.DataBind()
        BindDataListItem()
        LBLBOXMSG.Visible = False
    End Sub 

    Protected Sub BTNpRINT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If CDate(toDate(dateAwal.Text.Trim)) > CDate(toDate(dateAkhir.Text.Trim)) Then
                lblkonfirmasi.Text = "Period 2 must be more than Period 1 !"
                Exit Sub
            End If
        Catch ex As Exception
            lblkonfirmasi.Text = "Please check Period value"
            Exit Sub
        End Try
        showPrint("pdf")
    End Sub

    Protected Sub type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If type.SelectedValue = "DETAIL" Then
            lblprd1.Visible = True : lblprd2.Visible = True : ddlproduct.Visible = True
            LbLpIc.Visible = True : pIcLbL.Visible = True : PicTxt.Visible = True
            pIcBtn.Visible = True : PicDel.Visible = True : OidPic.Visible = True
        Else
            lblprd1.Visible = False : lblprd2.Visible = False : ddlproduct.Visible = False
            LbLpIc.Visible = False : pIcLbL.Visible = False : PicTxt.Visible = False
            pIcBtn.Visible = False : PicDel.Visible = False : OidPic.Visible = False
            PicTxt.Text = "" : OidPic.Text = "" : gvPic.Visible = False
        End If
    End Sub

    Protected Sub DDLcabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "SELECT j.gendesc FROM ql_mstperson p INNER JOIN QL_mstgen j ON j.genoid=p.PERSONSTATUS WHERE PERSONNAME=(SELECT USERNAME FROM QL_MSTPROF k WHERE k.USERID='" & Session("UserID") & "' AND k.BRANCH_CODE='" & Session("branch_id") & "')"
        Dim qCabang As String = ""
        If DDLcabang.SelectedItem.Text = "ALL" Then
            qCabang = ""
        Else
            qCabang = "AND so.branch_code='" & DDLcabang.SelectedValue & "'"
        End If
        initddl(qCabang)
    End Sub

    Protected Sub pIcBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles pIcBtn.Click
        PicDataBind()
    End Sub

    Protected Sub gvPic_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPic.PageIndexChanging
        gvPic.PageIndex = e.NewPageIndex
        PicDataBind()
    End Sub

    Protected Sub PicDel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PicDel.Click
        PicTxt.Text = "" : OidPic.Text = ""
        gvPic.Visible = False
    End Sub

    Protected Sub gvPic_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvPic.SelectedIndexChanged
        OidPic.Text = gvPic.SelectedDataKey.Item("PERSONOID")
        PicTxt.Text = gvPic.SelectedDataKey.Item("PERSONNAME")
        gvPic.Visible = False : OidPic.Visible = False
        CrystalReportViewer1.ReportSource = Nothing
    End Sub

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "" : dtView.AllowEdit = True
                For C1 As Integer = 0 To gvItem.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvItem.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                dtTbl.AcceptChanges() : dtTbl = dtView.ToTable
                Session("TblMat") = dtTbl : bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Protected Sub lbAddToListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateCheckedMat()
        Dim dt As DataTable = Session("TblMat")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "Checkvalue=True"
        If dv.Count > 0 Then
            If ItemName.Text <> "" Then
                For C1 As Integer = 0 To dv.Count - 1
                    ItemName.Text &= ";" & dv(C1)("itemcode").ToString & ""
                Next
            Else
                For C1 As Integer = 0 To dv.Count - 1
                    ItemName.Text &= dv(C1)("itemcode").ToString & ";"
                Next
            End If

            If ItemName.Text <> "" Then
                ItemName.Text = Left(ItemName.Text, ItemName.Text.Length)
            End If
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
        Else
            dv.RowFilter = ""
            LBLBOXMSG.Visible = True
            LBLBOXMSG.Text = "- Maaf, Silahkan pilih Barang, dengan beri centang pada kolom pilih..<BR>"
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        End If
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataListItem()
        gvItem.DataSource = Session("TblMat")
        gvItem.DataBind()
        mpeListMat.Show()

        'If UpdateCheckedMat() Then
        '    Dim dtTbl As DataTable = Session("TblMat")
        '    Dim dtView As DataView = dtTbl.DefaultView
        '    Dim sFilter As String = ""
        '    Dim sSplit() As String = FilterTextListMat.Text.Split(" ")
        '    For C1 As Integer = 0 To sSplit.Length - 1
        '        sFilter &= FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(sSplit(C1)) & "%'"
        '        'If ddlPIC.SelectedValue <> "NONE" Then
        '        '    sFilter &= " And personoid = " & ddlPIC.SelectedValue & ""
        '        'End If
        '        If C1 < sSplit.Length - 1 Then
        '            sFilter &= " AND "
        '        End If
        '    Next
        '    dtView.RowFilter = sFilter
        '    If dtView.Count > 0 Then
        '        Session("TblMatView") = dtView.ToTable
        '        gvItem.DataSource = Session("TblMatView")
        '        gvItem.DataBind() : dtView.RowFilter = ""
        '        mpeListMat.Show()
        '    Else
        '        dtView.RowFilter = ""
        '        Session("WarningListMat") = "Material data can't be found!"
        '        'showMessage(Session("WarningListMat"), 2)
        '    End If
        'Else
        '    mpeListMat.Show()
        'End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = 0
        FilterTextListMat.Text = ""
        BindDataListItem()
        gvItem.DataSource = Session("TblMat")
        gvItem.DataBind()
        mpeListMat.Show()
    End Sub

    Protected Sub lbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub chkAllItem_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Session("TblMat") Is Nothing Then
            Dim dtab As DataTable = Session("TblMat")

            If dtab.Rows.Count > 0 Then
                For i As Integer = 0 To dtab.Rows.Count - 1
                    dtab.Rows(i)("Checkvalue") = sender.Checked
                Next
                dtab.AcceptChanges()
            End If
            gvItem.DataSource = dtab
            gvItem.DataBind()
            Session("TblMat") = dtab
            If Session("CheckItem") <> True Then
                Session("CheckItem") = True
            Else
                Session("CheckItem") = False
            End If
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListReg") Is Nothing And Session("EmptyListReg") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListReg") Then
                Session("EmptyListReg") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
            End If
        End If
        If Not Session("WarningListReg") Is Nothing And Session("WarningListReg") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListReg") Then
                Session("WarningListReg") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("EmptyListCust") Is Nothing And Session("EmptyListCust") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListCust") Then
                Session("EmptyListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
        If Not Session("WarningListCust") Is Nothing And Session("WarningListCust") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListCust") Then
                Session("WarningListCust") = Nothing
                cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
            End If
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        SoNo.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & Tchar(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, data tidak ketemu..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "SI data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Maaf, Silahkan pilih nomer transaksi..!!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnrequestitemoid=" & dtTbl.Rows(C1)("trnrequestitemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Maaf, Data yang di pilih tidak ada..!!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If SoNo.Text <> "" Then
                            SoNo.Text &= ";" + vbCrLf + dtView(C1)("trnrequestitemno")
                        Else
                            SoNo.Text &= dtView(C1)("trnrequestitemno").ToString
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "- Maaf, Silahkan pilih data dulu..!!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If IsValidPeriod() Then
            DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
            Session("TblCust") = Nothing : Session("TblCustView") = Nothing
            gvListCust.DataSource = Nothing : gvListCust.DataBind()
            cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListCust.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListCust.Text) & "%'"
        If UpdateCheckedCust() Then
            Dim dv As DataView = Session("TblCust").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblCustView") = dv.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dv.RowFilter = ""
                mpeListCust.Show()
            Else
                dv.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListCust.Click
        DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
        If Session("TblCust") Is Nothing Then
            BindListCust()
            If Session("TblCust").Rows.Count <= 0 Then
                Session("EmptyListCust") = "Customer data can't be found!"
                showMessage(Session("EmptyListCust"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedCust() Then
            Dim dt As DataTable = Session("TblCust")
            Session("TblCustView") = dt
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        If UpdateCheckedCust2() Then
            gvListCust.PageIndex = e.NewPageIndex
            gvListCust.DataSource = Session("TblCustView")
            gvListCust.DataBind()
        End If
        mpeListCust.Show()
    End Sub

    Protected Sub lkbAddToListListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListCust.Click
        If Not Session("TblCust") Is Nothing Then
            If UpdateCheckedCust() Then
                Dim dtTbl As DataTable = Session("TblCust")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If custcode.Text <> "" Then
                            custcode.Text &= ";" + vbCrLf + dtView(C1)("custcode")
                        Else
                            custcode.Text = dtView(C1)("custcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
                Else
                    Session("WarningListCust") = "Please select Customer to add to list!"
                    showMessage(Session("WarningListCust"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnSelectAllCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneCust.Click
        If Not Session("TblCustView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblCustView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblCust")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "custoid=" & dtTbl.Rows(C1)("custoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblCust") = objTbl
                Session("TblCustView") = dtTbl
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
            End If
            mpeListCust.Show()
        Else
            Session("WarningListCust") = "Please show some Customer data first!"
            showMessage(Session("WarningListCust"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedCust.Click
        If Session("TblCust") Is Nothing Then
            Session("WarningListCust") = "Selected Customer data can't be found!"
            showMessage(Session("WarningListCust"), 2)
            Exit Sub
        End If
        If UpdateCheckedCust() Then
            Dim dtTbl As DataTable = Session("TblCust")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListCust.SelectedIndex = -1 : txtFilterListCust.Text = ""
                Session("TblCustView") = dtView.ToTable
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                dtView.RowFilter = ""
                mpeListCust.Show()
            Else
                dtView.RowFilter = ""
                Session("TblCustView") = Nothing
                gvListCust.DataSource = Session("TblCustView")
                gvListCust.DataBind()
                Session("WarningListCust") = "Selected Customer data can't be found!"
                showMessage(Session("WarningListCust"), 2)
            End If
        Else
            mpeListCust.Show()
        End If
    End Sub   

    Protected Sub imbFindReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindReg.Click
        If IsValidPeriod() Then
            DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
            Session("TblReg") = Nothing : Session("TblRegView") = Nothing
            gvListReg.DataSource = Nothing : gvListReg.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, True)
        Else
            Exit Sub
        End If
    End Sub 

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custcode.Text = ""
    End Sub

    Protected Sub imbEraseReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseReg.Click
        regno.Text = ""
    End Sub

    Protected Sub btnFindListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListReg.Click
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Sales data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListReg.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListReg.Text) & "%'"
        If UpdateCheckedReg() Then
            Dim dv As DataView = Session("TblReg").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblRegView") = dv.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dv.RowFilter = ""
                mpeListReg.Show()
            Else
                dv.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub btnViewAllListReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListReg.Click
        DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
        If Session("TblReg") Is Nothing Then
            BindListReg()
            If Session("TblReg").Rows.Count <= 0 Then
                Session("EmptyListReg") = "Register data can't be found!"
                showMessage(Session("EmptyListReg"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedReg() Then
            Dim dt As DataTable = Session("TblReg")
            Session("TblRegView") = dt
            gvListReg.DataSource = Session("TblRegView")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub btnSelectAllReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneReg.Click
        If Not Session("TblRegView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblRegView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblReg")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "registermstoid=" & dtTbl.Rows(C1)("registermstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblReg") = objTbl
                Session("TblRegView") = dtTbl
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
            End If
            mpeListReg.Show()
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedReg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedReg.Click
        If Session("TblReg") Is Nothing Then
            Session("WarningListReg") = "Selected sales data can't be found!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
        If UpdateCheckedReg() Then
            Dim dtTbl As DataTable = Session("TblReg")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListReg.SelectedIndex = -1 : txtFilterListReg.Text = ""
                Session("TblRegView") = dtView.ToTable
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                dtView.RowFilter = ""
                mpeListReg.Show()
            Else
                dtView.RowFilter = ""
                Session("TblRegView") = Nothing
                gvListReg.DataSource = Session("TblRegView")
                gvListReg.DataBind()
                Session("WarningListReg") = "Selected sales data can't be found!"
                showMessage(Session("WarningListReg"), 2)
            End If
        Else
            mpeListReg.Show()
        End If
    End Sub

    Protected Sub gvListReg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListReg.PageIndexChanging
        If UpdateCheckedReg2() Then
            gvListReg.PageIndex = e.NewPageIndex
            gvListReg.DataSource = Session("TblCustView")
            gvListReg.DataBind()
        End If
        mpeListReg.Show()
    End Sub

    Protected Sub lkbAddToListListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListReg.Click
        If Not Session("TblReg") Is Nothing Then
            If UpdateCheckedReg() Then
                Dim dtTbl As DataTable = Session("TblReg")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If regno.Text <> "" Then
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= ";" + vbCrLf + dtView(C1)("registerno")
                            End If
                        Else
                            If dtView(C1)("registerno") <> "" Then
                                regno.Text &= dtView(C1)("registerno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
                Else
                    Session("WarningListReg") = "Please select sales to add to list!"
                    showMessage(Session("WarningListReg"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListReg") = "Please show some sales data first!"
            showMessage(Session("WarningListReg"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListReg.Click
        cProc.SetModalPopUpExtender(btnHiddenListReg, PanelListReg, mpeListReg, False)
    End Sub
#End Region

    Protected Sub cbType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbType.SelectedIndexChanged
        Dim strcabang As Int32 = 0
        For C1 As Integer = 0 To cbType.Items.Count - 1
            If cbType.Items(C1).Selected = True Then
                strcabang += 1
            End If
        Next
    End Sub

    Protected Sub cbCheckAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCheckAll.CheckedChanged
        If cbCheckAll.Checked = True Then
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = True
            Next
        Else
            For Each listItem As ListItem In cbType.Items
                listItem.Selected = False
            Next
        End If
    End Sub
End Class
