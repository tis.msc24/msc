Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class ReportForm_rptOperTeknisi
    Inherits System.Web.UI.Page
#Region "Variable"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_SIP_Conn")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)

    Dim xreader As SqlDataReader
    Dim xKon As New Koneksi
    Dim sSql As String = ""
    Dim dsData As New DataSet
    Dim dv As DataView
    Dim ckoneksi As New Koneksi
    Dim CProc As New ClassProcedure

    Dim report As New ReportDocument
    Dim folderReport As String = "~/report/"
    Dim param As String = ""
#End Region

#Region "Procedure"
    Private Sub showMessage(ByVal sMessage As String, ByVal iitservtypeoid As Integer)
        Dim strCaption As String = CompnyName
        If iitservtypeoid = 1 Then 'Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - Error"
        ElseIf iitservtypeoid = 2 Then 'Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - Warning"
        ElseIf iitservtypeoid = 3 Then 'Warning
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - Information"
        End If
        lblCaption.Text = strCaption : Validasi.Text = sMessage
        CProc.SetModalPopUpExtender(btnMsg, panelMsg, mpeMsg, True)
    End Sub

    Sub showPrint(ByVal tipe As String)
        Dim sWhere As String = ""
        Try
            sWhere &= "and o.branch_code = '" & DDLcabang.SelectedValue & "'"
            If FilterText.Text <> "" Then
                sWhere &= " and " & FilterDDL.SelectedValue & " like '%" & Tchar(FilterText.Text) & "%'"
            End If
            If tgl1.Text <> "" And tgl2.Text <> "" Then
                Try


                    Try

                        Dim dat1 As Date = CDate(toDate(tgl1.Text))
                        Dim dat2 As Date = CDate(toDate(tgl2.Text))
                    Catch ex As Exception
                        showMessage("Format Tanggal Salah - Gunakan format dd/MM/yyyy !!", 2) : Exit Sub
                    End Try
                    If CDate(toDate(tgl1.Text)) > CDate(toDate(tgl2.Text)) Then
                        showMessage("Tanggal mulai tidak boleh lebih besar dari tanggal destinasi !!", 2) : Exit Sub
                    End If


                    sWhere &= " where convert(date,o.operdate, 101) between '" & CDate(toDate(tgl1.Text)) & "' " & _
                            "and '" & CDate(toDate(tgl2.Text)) & "'"

                Catch ex As Exception
                    showMessage("Format Tanggal tidak sesuai !!", 2) : Exit Sub
                End Try
            End If

            If cbStatus.Checked = True And DDLStatus.SelectedValue <> "All" Then
                sWhere &= " AND o.operstatus = '" & DDLStatus.SelectedValue & "'"
            End If
            Try
                report.Load(Server.MapPath(folderReport & "rptOper.rpt"))
                report.SetParameterValue("sWhere", sWhere)
                report.SetParameterValue("dPeriode", tgl1.Text & " - " & tgl2.Text)
                report.SetParameterValue("reportName", report.FileName.Substring(report.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
                CProc.SetDBLogonForReport(report, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

                If tipe = "PRINT" Then
                    crvOper.DisplayGroupTree = False
                    'CRVPenerimaanBarang.SeparatePages = False
                    crvOper.ReportSource = report
                ElseIf tipe = "EXCEL" Then
                    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                    Response.Buffer = False

                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "LaporanOperTeknisi" & Format(GetServerTime(), "dd_MM_yy"))
                    report.Close() : report.Dispose()
                    Response.Redirect("~\ReportForm\rptOperTeknisi.aspx?awal=true")
                Else
                    report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize

                    Response.Buffer = False

                    Response.ClearContent()
                    Response.ClearHeaders()
                    report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "LaporanOperTeknisi" & Format(GetServerTime(), "dd_MM_yy"))
                    report.Close() : report.Dispose()
                    Response.Redirect("~\ReportForm\rptOperTeknisi.aspx?awal=true")
                End If
            Catch ex As Exception
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                showMessage(ex.ToString, 1)
            End Try
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        End Try
    End Sub

    Private Sub resetReport()
        crvOper.ReportSource = Nothing
    End Sub

    Private Sub Cabangddl()
        Dim sCabang As String = ""
        If kacabCek("ReportForm/rptservicefinal.aspx", Session("UserID")) = False Then
            If Session("branch_id") <> "01" Then
                sCabang &= " AND gencode='" & Session("branch_id") & "'"
            Else
                sCabang = ""
            End If
        End If
        sSql = "Select gencode,gendesc From QL_mstgen g Where cmpcode='MSC' And g.gengroup='CABANG' " & sCabang & ""
        FillDDL(DDLcabang, sSql)
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~\other\login.aspx")
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Server.Transfer("~\other\NotAuthorize.aspx")
        'End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()
            Session("SpecialAccess") = xsetAcc
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Session("branch_id") = branchId
            Session("branch") = branch
            Response.Redirect("rptOperTeknisi.aspx") ' recall lagi untuk clear
        End If
        Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - Laporan Oper Teknisi"

        If Not Page.IsPostBack Then
            Session("showReport") = Nothing
            Cabangddl()
            tgl1.Text = Format(Now, "01/" & "MM/yyyy")
            tgl2.Text = Format(Now.Date, "dd/MM/yyyy ")

        End If
        If Session("showReport") = True Then
            showPrint("PRINT")
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        resetReport()
        cbStatus.Checked = False
        FilterDDL.SelectedIndex = 0
        DDLStatus.SelectedIndex = 0
        FilterText.Text = ""
        tgl1.Text = Format(Now, "01/" & "MM/yyyy")
        tgl2.Text = Format(Now.Date, "dd/MM/yyyy ")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnView.Click
        Session("showReport") = True
        showPrint("PRINT")
    End Sub

    Protected Sub btnErrorOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnErrorOK.Click
        resetReport()
        Response.Redirect("rptOperTeknisi.aspx?awal=true")
        btnMsg.Visible = False
        panelMsg.Visible = False
    End Sub
#End Region

#Region "Function"
    Private Function kacabCek(ByVal FormUrl As String, ByVal idUser As String)
        Dim sKcb As String = "SELECT COUNT(*) FROM [QL_USERROLE] r INNER JOIN ql_roledtl rd ON r.ROLEOID=rd.ROLEOID WHERE [USERPROF]= '" & idUser & "' AND KACAB='YES' AND FORMADDRESS LIKE '%" & FormUrl & "%'"
        Dim sCek As Integer = ckoneksi.ambilscalar(sKcb)
        If sCek > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

    Protected Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        Try
            If Not report Is Nothing Then
                If report.IsLoaded Then
                    report.Dispose()
                    report.Close()
                End If
            End If
        Catch ex As Exception
            report.Dispose()
            report.Close()
        End Try
    End Sub
End Class
