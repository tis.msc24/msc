
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction

Partial Class fm_mstgen
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_SIP_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim CProc As New ClassProcedure
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")

#End Region

#Region "Procedure"

    Private Sub InitCbAssetDdl()
        sSql = "Select gencode,gendesc from QL_mstgen Where cmpcode='MSC' AND gengroup='CABANG'"
        FillDDL(sCabang, sSql)
    End Sub

    Private Sub checkDDL()
        If gengroup.SelectedValue.ToUpper = "ASSETS_TYPE" Then
            Label5.Visible = True : Label6.Visible = True : Label7.Visible = True
            ddlItemtype.Visible = True : ddlProvince.Visible = True : ddlCountry.Visible = True
            ValueDep.Visible = True : genother7.Visible = True
            CbAssetType.Visible = True : sCabang.Visible = True

            gencode.Enabled = False : genother1.Visible = False : genother2.Visible = False
            genother3.Visible = False : genother1.ReadOnly = False : genother2.ReadOnly = False
            imbDate.Visible = False : TglCOA.Visible = False : gencode.CssClass = "inpTextDisabled"
            Label7.Text = "Other 1" : Label6.Text = "Other 2" : Label5.Text = "Other 3"
            TglCOA.Visible = False : imbDate.Visible = False

            FillDDLAcctg(ddlCountry, "VAR_ASSET", sCabang.SelectedValue, "")
            FillDDLAcctg(ddlProvince, "VAR_ASSET_DEP", sCabang.SelectedValue, "")
            FillDDLAcctg(ddlItemtype, "VAR_ASSET_DEP_EXPENSE_1", sCabang.SelectedValue, "")
            DdlType.Visible = False

        ElseIf gengroup.SelectedValue.ToUpper = "CITY" Then
            gencode.Text = "" : gencode.Enabled = True
            ddlCountry.Visible = True : ddlProvince.Visible = True
            ddlProvince.Visible = True : Label7.Visible = True
            Label6.Visible = True : Label5.Visible = False
            Label7.Text = "Negara" : Label6.Text = "Provinsi" : Label5.Text = "Other 3"
            '---Visible False----
            genother3.Visible = False : genother2.Visible = False : genother1.Visible = False
            imbDate.Visible = False : TglCOA.Visible = False : genother7.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            TglCOA.Visible = False : imbDate.Visible = False
            DdlType.Visible = False
            sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('COUNTRY') and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCountry, sSql)

            sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('PROVINCE') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & ddlCountry.SelectedValue & "'"
            FillDDL(ddlProvince, sSql)

        ElseIf gengroup.SelectedValue.ToUpper = "PROVINCE" Then
            ddlCountry.Visible = True
            Label7.Text = "Negara"
            sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('COUNTRY') and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCountry, sSql)

            TglCOA.Visible = False : imbDate.Visible = False
            genother1.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "PAYTYPE" Then
            Label7.Text = "Termin" : genother1.Visible = True
            Label7.Visible = True

            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "CREDITLIMITTYPE" Then
            Label7.Text = "Termin (Hours)" : genother1.Visible = True
            Label7.Visible = True
            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "SOCLOSETYPE" Then
            Label7.Text = "Termin (DAY)" : genother1.Visible = True
            Label7.Visible = True
            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "BANK NAME" Then
            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : Label7.Visible = False
            genother1.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "NOTA" Then
            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : Label7.Visible = False
            genother1.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedItem.Text.ToUpper = "CABANG" Then
            ddlCountry.Visible = True : gencode.Text = ""
            Label7.Text = "Kota" : Label7.Visible = True
            gencode.Enabled = True : gencode.CssClass = "inpText"
            sSql = "select genoid,gendesc, gengroup from QL_mstgen Where gengroup ='CITY' and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCountry, sSql)

            genother1.Visible = False : genother1.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedItem.Text.ToUpper = "FORMNAME" Then
            Label7.Visible = True : Label7.Text = "Alamat Url"
            Label6.Visible = True : genother2.Visible = True
            Label6.Text = "Nama Folder" : genother1.Visible = True
            gencode.Enabled = True
            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : ddlProvince.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            genother3.Visible = False : genother7.Visible = False
            DdlType.Visible = False : ValueDep.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "LOCATION" Then
            Label7.Visible = True : Label7.Text = "Warehouse"
            ddlCountry.Visible = True : ddlProvince.Visible = True
            Label6.Visible = True : Label6.Text = "Cabang"
            Label5.Visible = True : Label5.Text = "Type Gudang"
            DdlType.Visible = True

            sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('WAREHOUSE') and cmpcode like '%" & CompnyCode & "%'"
            FillDDL(ddlCountry, sSql)

            sSql = "SELECT genoid, UPPER(gendesc) gendesc FROM QL_mstgen Where gengroup = 'CABANG'"
            FillDDL(ddlProvince, sSql)

            CbAssetType.Visible = False : sCabang.Visible = False
            TglCOA.Visible = False : imbDate.Visible = False
            genother3.Visible = False : genother7.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "WAREHOUSE" Then
            Label7.Visible = True : Label7.Text = "Type"
            genother1.Visible = True

            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : genother3.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            genother7.Visible = False : Label6.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "TABLENAME" Then
            Label7.Visible = True : Label7.Text = "Nama table DB"
            genother1.Visible = True : gencode.Enabled = True
            gencode.CssClass = "inpText"

            TglCOA.Visible = False : imbDate.Visible = False
            ddlCountry.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : genother3.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            genother7.Visible = False : Label6.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "CUTOFFDATE" Then
            Label7.Visible = True : Label7.Text = "Tanggal"
            TglCOA.Visible = True : imbDate.Visible = True
            ddlCountry.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : genother3.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            genother7.Visible = False : Label6.Visible = False
            ddlItemtype.Visible = False : ValueDep.Visible = False
            CbAssetType.Visible = False : sCabang.Visible = False
            gencode.Enabled = False : gencode.CssClass = "inpTextDisabled"
            genother1.Visible = False : DdlType.Visible = False
        ElseIf gengroup.SelectedValue.ToUpper = "DEPARTMENT" Then
            ddlCountry.Visible = True : Label7.Visible = True
            gencode.CssClass = "inpTextDisabled" : Label7.Text = "Hak Akses"

            sSql = "Select Distinct * from (select 1 genoid, 'YA' gendesc from QL_mstgen Where gengroup='DEPARTMENT' UNION ALL select 2 genoid, 'TIDAK' gendesc from QL_mstgen Where gengroup='DEPARTMENT') gb"
            FillDDL(ddlCountry, sSql)

            ddlProvince.Visible = True : Label6.Visible = True
            Label6.Text = "Cabang"
            sSql = "SELECT gencode, UPPER(gendesc) gendesc FROM QL_mstgen where gengroup = 'cabang'"
            FillDDL(ddlProvince, sSql)

            genother1.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            TglCOA.Visible = False : imbDate.Visible = False
            DdlType.Visible = False
        Else
            ddlCountry.Visible = False
            Label7.Visible = False : gencode.CssClass = "inpTextDisabled"
            genother1.Visible = False : gencode.Enabled = False
            CbAssetType.Visible = False : sCabang.Visible = False
            Label6.Visible = False : ddlProvince.Visible = False
            Label5.Visible = False : ddlItemtype.Visible = False
            ValueDep.Visible = False : genother7.Visible = False
            TglCOA.Visible = False : imbDate.Visible = False
            DdlType.Visible = False
        End If
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")
        If Session("FilterDDL") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") LIKE '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "
            End If
        Else
            sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value.Trim & ") lIKE '%" & FilterText.Text.Trim.Replace("'", "''").ToUpper & "%' "
        End If
        If chkGroup.Checked Then
            sWhere &= (IIf(sWhere = "", " WHERE ", " AND ") & " gengroup='" & FilterGroup.SelectedValue.Trim & "'")
        End If

        If sCompCode <> "" Then
            sSql = "SELECT cmpcode, genoid, gencode, UPPER(gendesc) gendesc, UPPER(gengroup) gengroup, genother1, genother2, genother3, UPPER(upduser) upduser, updtime FROM QL_mstgen " & sWhere & IIf(sWhere <> "", " And ", " WHERE ") & "upper(cmpcode) = '" & sCompCode.ToUpper & "' and gengroup not in ('EKSPEDISI','JOBPOSITION','CABANG') ORDER BY gengroup,gencode"
        Else
            sSql = "SELECT cmpcode, genoid, gencode, UPPER(gendesc) gendesc, UPPER(gengroup) gengroup, genother1, genother2, genother3, UPPER(upduser) upduser, updtime FROM QL_mstgen " & sWhere & IIf(sWhere <> "", " And ", " WHERE ") & " Gengroup not in ('EKSPEDISI','JOBPOSITION','CABANG') ORDER BY gengroup,gencode"
        End If

        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
        GVmstgen.PageIndex = 0
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As Integer)
        'checkDDL()
        sSql = "SELECT top 1 cmpcode, genoid, gencode, UPPER(gendesc) gendesc, gengroup, genother1, genother2, genother3, upduser, updtime, genother4, genother5, isnull(genother6,1) genother6 FROM QL_mstgen where cmpcode='" & CompnyCode & "' and genoid = " & vGenoid & ""
        xCmd.CommandText = sSql
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        xreader = xCmd.ExecuteReader
        btnDelete.Enabled = False
        If xreader.HasRows Then
            While xreader.Read
                'sTrueFalse = True
                gengroup.Enabled = False : gengroup.CssClass = "inpTextDisabled"
                genoid.Text = Integer.Parse(xreader("genoid"))
                gencode.Text = Trim(xreader("gencode").ToString)
                gendesc.Text = Trim(xreader("gendesc").ToString)
                gengroup.SelectedValue = xreader("gengroup").ToString
                Dim jumRow As Integer = gengroup.Items.Count

                If gengroup.SelectedValue.ToUpper = "ASSETS_TYPE" Then
                    gencode.Enabled = False
                    Label5.Visible = True : Label6.Visible = True : Label7.Visible = True
                    ddlItemtype.Visible = True : ddlProvince.Visible = True
                    ddlCountry.Visible = True : ValueDep.Visible = True
                    genother7.Visible = True : CbAssetType.Visible = True
                    sCabang.Visible = True

                    gencode.Enabled = False : genother1.Visible = False
                    genother2.Visible = False : genother3.Visible = False
                    genother1.ReadOnly = False : genother2.ReadOnly = False
                    imbDate.Visible = False : TglCOA.Visible = False
                    gencode.CssClass = "inpTextDisabled" : DdlType.Visible = False
                    Label7.Text = "Other 1" : Label6.Text = "Other 2" : Label5.Text = "Other 3"
                    TglCOA.Visible = False : imbDate.Visible = False

                    sCabang.SelectedValue = xreader("genother5").ToString
                    genother7.Text = ToDouble(xreader("genother4"))
                    FillDDLAcctg(ddlCountry, "VAR_ASSET", sCabang.SelectedValue, "")
                    FillDDLAcctg(ddlProvince, "VAR_ASSET_DEP", sCabang.SelectedValue, "")
                    FillDDLAcctg(ddlItemtype, "VAR_ASSET_DEP_EXPENSE_1", sCabang.SelectedValue, "")

                ElseIf gengroup.Text.ToUpper = "CITY" Then
                    gencode.Enabled = False
                    ddlCountry.Visible = True : ddlProvince.Visible = True
                    ddlProvince.Visible = True : Label7.Visible = True
                    Label6.Visible = True : Label5.Visible = False
                    Label7.Text = "Negara" : Label6.Text = "Provinsi" : Label5.Text = "Other 3"
                    '---Visible False----
                    genother3.Visible = False : genother2.Visible = False
                    genother1.Visible = False
                    imbDate.Visible = False : TglCOA.Visible = False : genother7.Visible = False
                    ddlItemtype.Visible = False : CbAssetType.Visible = False
                    sCabang.Visible = False : DdlType.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('COUNTRY') and cmpcode like '%" & CompnyCode & "%'"
                    FillDDL(ddlCountry, sSql)
                    ddlCountry.SelectedValue = Trim(xreader("genother1").ToString)

                    sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('PROVINCE') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & ddlCountry.SelectedValue & "'"
                    FillDDL(ddlProvince, sSql)
                    ddlProvince.SelectedValue = Trim(xreader("genother2").ToString)

                ElseIf gengroup.Text.ToUpper = "PROVINCE" Then 
                    ddlCountry.Visible = True
                    Label7.Text = "Negara"
                    sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('COUNTRY') and cmpcode like '%" & CompnyCode & "%'"
                    FillDDL(ddlCountry, sSql)
                    ddlCountry.SelectedValue = xreader("genother1")

                    TglCOA.Visible = False : imbDate.Visible = False
                    genother1.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "PAYTYPE" Then
                    Label7.Text = "Termin" : genother1.Visible = True
                    genother1.Text = xreader("genother1")
                    ddlCountry.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "CREDITLIMITTYPE" Then
                    Label7.Text = "Termin (Hours)" : genother1.Visible = True
                    genother1.Text = xreader("genother1")
                    ddlCountry.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "SOCLOSETYPE" Then
                    Label7.Text = "Termin (DAY)" : genother1.Visible = True
                    genother1.Text = xreader("genother1")
                    ddlCountry.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "BANK NAME" Then
                    ddlCountry.Visible = False : Label7.Visible = False
                    genother1.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "NOTA" Then
                    ddlCountry.Visible = False : Label7.Visible = False
                    genother1.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedItem.Text.ToUpper = "CABANG" Then
                    ddlCountry.Visible = True : gencode.Enabled = False
                    gencode.CssClass = "inpTextDisabled" : Label7.Text = "Kota"
                    Label7.Visible = True
                    sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup ='CITY' and cmpcode like '%" & CompnyCode & "%'"
                    FillDDL(ddlCountry, sSql)
                    ddlCountry.SelectedValue = xreader("genother6")
                    genother1.Text = Trim(xreader("genother1").ToString)
                    DdlType.Visible = False
                    genother1.Visible = True : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "LOCATION" Then
                    Label7.Visible = True : Label7.Text = "Warehouse"
                    ddlCountry.Visible = True : ddlProvince.Visible = True
                    Label6.Visible = True : Label6.Text = "Cabang"
                    DdlType.Visible = True
                    Label5.Visible = True : Label5.Text = "Type Gudang"
                    DdlType.SelectedValue = xreader("genother6").ToString

                    sSql = "select genoid, UPPER(gendesc) gendesc, gengroup from QL_mstgen where gengroup IN ('WAREHOUSE') and cmpcode like '%" & CompnyCode & "%'"
                    FillDDL(ddlCountry, sSql)
                    ddlCountry.SelectedValue = xreader("genother1")

                    sSql = "SELECT genoid, UPPER(gendesc) gendesc FROM QL_mstgen where gengroup = 'cabang'"
                    FillDDL(ddlProvince, sSql)

                    ddlProvince.SelectedValue = xreader("genother2")
                    genother3.Visible = False : genother7.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "WAREHOUSE" Then
                    Label7.Visible = True : Label7.Text = "Type"
                    genother1.Visible = True
                    genother1.Text = xreader("genother1").ToString
                    ddlCountry.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : genother3.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    genother7.Visible = False : Label6.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "TABLENAME" Then
                    Label7.Visible = True : Label7.Text = "Nama table DB"
                    genother1.Visible = True
                    genother1.Text = xreader("genother1").ToString

                    ddlCountry.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : genother3.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    genother7.Visible = False : Label6.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "CUTOFFDATE" Then
                    Label7.Visible = True : Label7.Text = "Tanggal"
                    TglCOA.Visible = True : imbDate.Visible = True
                    TglCOA.Text = xreader("genother1").ToString

                    ddlCountry.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : genother3.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    genother7.Visible = False : Label6.Visible = False
                    ddlItemtype.Visible = False : ValueDep.Visible = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    gencode.Enabled = False : gencode.CssClass = "inpTextDisabled"
                    genother1.Visible = False : DdlType.Visible = False
                ElseIf gengroup.SelectedItem.Text.ToUpper = "FORMNAME" Then
                    Label7.Visible = True : Label7.Text = "Alamat Url"
                    Label6.Visible = True : genother2.Visible = True
                    Label6.Text = "Nama Folder" : genother1.Visible = True

                    genother1.Text = xreader("genother1").ToString
                    genother2.Text = xreader("genother2").ToString

                    TglCOA.Visible = False : imbDate.Visible = False
                    ddlCountry.Visible = False : ddlProvince.Visible = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    genother3.Visible = False : genother7.Visible = False
                    DdlType.Visible = False : ValueDep.Visible = False
                ElseIf gengroup.SelectedValue.ToUpper = "DEPARTMENT" Then
                    ddlCountry.Visible = True : Label7.Visible = True
                    gencode.CssClass = "inpText" : Label7.Text = "Hak Akses"

                    sSql = "Select Distinct * from (select 1 genoid,'YA' gendesc from QL_mstgen Where gengroup='DEPARTMENT' UNION ALL select 2 genoid,'TIDAK' gendesc from QL_mstgen Where gengroup='DEPARTMENT') gb"
                    FillDDL(ddlCountry, sSql)
                    ddlCountry.SelectedValue = xreader("genother1")

                    ddlProvince.Visible = True : Label6.Visible = True
                    Label6.Text = "Cabang" : ddlProvince.Enabled = False
                    ddlProvince.CssClass = "inpTextDisabled"

                    sSql = "SELECT gencode ,gendesc FROM QL_mstgen Where gengroup = 'Cabang'"
                    FillDDL(ddlProvince, sSql)
                    ddlProvince.SelectedValue = xreader("genother2").ToString

                    genother1.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                Else
                    ddlCountry.Visible = False : Label7.Visible = False
                    genother1.Visible = False : gencode.Enabled = False
                    CbAssetType.Visible = False : sCabang.Visible = False
                    Label6.Visible = False : ddlProvince.Visible = False
                    Label5.Visible = False : ddlItemtype.Visible = False
                    ValueDep.Visible = False : genother7.Visible = False
                    TglCOA.Visible = False : imbDate.Visible = False
                    DdlType.Visible = False
                End If
                Upduser.Text = Trim(xreader.GetValue(8).ToString)
                Updtime.Text = Trim(xreader.GetValue(9).ToString)
                btnDelete.Visible = True : btnDelete.Enabled = True
            End While
        Else
            gencode.Text = ""
        End If

        xreader.Close()
        conn.Close()
    End Sub

    Public Sub InitAllDDL()
        'sSql = "select lastoid,tablename from QL_mstoid where tablegroup='GEN'"
        'FillDDL(gengroup, sSql)
        sSql = "Select lastoid, tablename from QL_mstoid Where tablegroup='GEN' AND tablename NOT IN ('CABANG','JOBPOSITION') order by tablename"
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        gengroup.Items.Clear()

        xCmd.CommandText = sSql
        xreader = xCmd.ExecuteReader
        While xreader.Read
            gengroup.Items.Add(Trim(xreader.GetValue(1).ToString.Trim))
            gengroup.Items.Item(gengroup.Items.Count - 1).Value = Trim(xreader.GetValue(1).ToString.Trim)

            FilterGroup.Items.Add(Trim(xreader.GetValue(1).ToString.Trim))
            FilterGroup.Items.Item(FilterGroup.Items.Count - 1).Value = Trim(xreader.GetValue(1).ToString.Trim)
        End While
        xreader.Close() : conn.Close()

        'sSql = "select tablename AS tb1,tablename AS tb2 from QL_mstoid where tablegroup='GEN'"
        'FillDDL(FilterGroup, sSql)

        'DLLPROVINCE'
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('PROVINCE') and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddlProvince, sSql)
        'COUNTRY'
        sSql = "select genoid, gendesc, gengroup from QL_mstgen where gengroup IN ('COUNTRY') and cmpcode like '%" & CompnyCode & "%'"
        FillDDL(ddlCountry, sSql)

        If gengroup.Items.Count = 0 Then
            'RegisterStartupScript("Informasi", "<script>alert('Please create/fill master TABLE!')</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please create/fill master TABLE!"))
            Exit Sub
        End If
    End Sub

    Public Sub GenerateGenID()


        If gengroup.SelectedValue = "ITEMUNIT" Then
            'genoid.Text = GetStrData("select ISNULL(lastoid,0)+1 from " & DB_JPT_RITEL & ".dbo.QL_mstoid where tablename = 'QL_mstgen' and cmpcode='" & CompnyCode & "' ")
            'Dim genoid_grosir As Integer = GetStrData("select ISNULL(lastoid,0)+1 from " & DB_JPT_GROSIR & ".dbo.QL_mstoid where tablename = 'QL_mstgen' and cmpcode='" & CompnyCode & "' ")
            'If genoid_grosir > ToDouble(genoid.Text) Then
            '    genoid.Text = genoid_grosir
            'End If
            genoid.Text = GenerateID("ql_mstgen", CompnyCode)
        Else
            genoid.Text = GenerateID("ql_mstgen", CompnyCode)
        End If

    End Sub

    Private Sub clearItem()
        gencode.Text = ""
        gendesc.Text = ""
        gengroup.SelectedIndex = 0
        genother1.Text = ""
        genother2.Text = ""
        genother3.Text = ""
        Upduser.Text = Session("UserID")
        Updtime.Text = FormatDateTime(Now(), DateFormat.GeneralDate)
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            btnSearch.Visible = True
            btnList.Visible = True
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("branch_id") = branchId
            Session("branch") = branch
            If Session("branch_id") = "" Then
                Response.Redirect("~/Other/login.aspx")
            End If
            Response.Redirect("fm_mstgen.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
        Page.Title = CompnyName & " - Data General"

        If Not IsPostBack Then
            'Me.btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');")
            BindData(CompnyCode)
            InitCbAssetDdl()
            InitAllDDL() : checkDDL()
            btnSearch.Visible = True
            btnList.Visible = True
            gendesc_TextChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                i_u.Text = "Update"
                btnDelete.Visible = True
                TabContainer1.ActiveTabIndex = 1
                FillTextBox(Session("oid"))
            Else
                i_u.Text = "New"
                GenerateGenID()
                TglCOA.Text = Format(GetServerTime(), "dd/MM/yyyy")
                btnDelete.Enabled = False
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
    End Sub

    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session("FilterText") = "" : Session("FilterDDL") = 0
        chkGroup.Checked = False : FilterGroup.SelectedIndex = 0
        BindData(CompnyCode)
        GVmstgen.PageIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        'If sTrueFalse Then
        '    btnCancel_Click(sender, e)
        'End If
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sMsg As String = ""
        If gencode.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Code Dahulu !!<BR>"
        End If
        If gendesc.Text.Trim = "" Then
            sMsg &= "- Maaf, Tolong Isi Description Dahulu !!<BR>"
        End If
        If gendesc.Text.Trim.Length > 70 Then
            sMsg &= "- Maaf, Maximum 70 characters untuk Description !!<BR>"
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck As String = ""
        If gengroup.SelectedValue = "CITY" Then
            sSqlCheck = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "' and genother1 = '" & ddlCountry.SelectedValue & "' and genother2 = '" & ddlProvince.SelectedValue & "'"
        ElseIf gengroup.SelectedValue = "PROVINCE" Then
            sSqlCheck = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "' and genother1 = '" & ddlCountry.SelectedValue & "'"
        ElseIf gengroup.SelectedValue = "LOCATION" Then
            sSqlCheck = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "' and genother1 = '" & ddlCountry.SelectedValue & "'"
        Else
            sSqlCheck = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "'"
        End If

        If Not Session("oid") Is Nothing Then
            sSqlCheck &= " AND genoid <> " & genoid.Text
        End If

        If ToDouble(GetScalar(sSqlCheck)) > 0 Then
            sMsg &= "- Maaf, Description sudah dipakai di General Group yang lain..!!<br>"
        End If

        'cek code msgen yang kembar
        Dim sSqlCheckcode As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gencode = '" & Tchar(gencode.Text) & "'"

        If Not Session("oid") Is Nothing Then
            sSqlCheckcode &= " AND genoid <> " & genoid.Text
        End If

        If ToDouble(GetScalar(sSqlCheckcode)) > 0 Then
            sMsg &= "- Maaf, Code sudah dipakai di General Group yang sama...!!<br>"
        Else
            Dim sSqlCheckdesc As String = "SELECT COUNT(-1) FROM QL_mstgen WHERE gengroup = '" & gengroup.Text & "' AND gendesc = '" & Tchar(gendesc.Text) & "'"

            If Not Session("oid") Is Nothing Then
                sSqlCheckdesc &= " AND genoid <> " & genoid.Text
            End If

            If ToDouble(GetScalar(sSqlCheckdesc)) > 0 Then
                sMsg &= "- Maaf, Description sudah dipakai di General Group yang sama..!!<br>"
            End If
        End If

        If gengroup.SelectedValue = "WAREHOUSE" Then
            'Pengecekan tambahan tuk warehouse
            If genother1.Text.Trim = "" Then
                sMsg &= "- Maaf, Tolong isi Other 1 dengan 'GROSIR', 'RETAIL', 'RETUR', 'SUPPLIER','KONSINYASI' pada Group 'WAREHOUSE'..!!!<br>"
            Else
                If genother1.Text <> "GROSIR" Then
                    If genother1.Text <> "RETAIL" Then
                        If genother1.Text <> "RETUR" Then
                            If genother1.Text <> "SUPPLIER" Then
                                If genother1.Text <> "KONSINYASI" Then
                                    sMsg &= "- Maaf, Tolong isi Other 1 dengan 'GROSIR', 'RETAIL', 'RETUR', 'SUPPLIER','KONSINYASI' pada Group 'WAREHOUSE'..!!<br>"
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

        If Session("oid") = Nothing Or Session("oid") = "" Then
            If gengroup.SelectedValue.ToUpper = "DEPARTMENT" Then
                sSql = "select Count(genoid) from QL_mstgen Where gengroup='DEPARTMENT' AND genother2='" & ddlProvince.SelectedValue & "'"
                If ToDouble(GetStrData(sSql)) > 0.0 Then
                    sMsg &= "- Maaf, Cabang ini sudah pernah di input..!!<br />"
                End If
            End If
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2) : Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = FormatDateTime(GetServerTime(), DateFormat.GeneralDate)
            'UNTUK MENGISI GENOTHER TEXT PADA SAAT DDL AKTIF
            If gengroup.SelectedValue = "CUTOFFDATE" Then
                If TglCOA.Text = "" Then
                    showMessage("Other2 Tidak Boleh Kosong", 3)
                    objTrans.Rollback()
                    conn.Close()
                    Exit Sub
                End If

                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "SELECT count(*) FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND genother2 = '" & ddlProvince.SelectedValue & "'" : xCmd.CommandText = sSql

                    If xCmd.ExecuteScalar() = 0 Then
                        If ddlCountry.Visible = False And ddlProvince.Visible = True Then
                            If gengroup.SelectedValue.ToUpper = "CABANG" Then
                                genother1.Text = genother1.Text
                                genother2.Text = ddlProvince.SelectedValue
                            Else
                                genother1.Text = TglCOA.Text
                                genother2.Text = ddlProvince.SelectedValue
                            End If
                        End If
                        If ddlItemtype.Visible = True Then
                            genother3.Text = ddlItemtype.SelectedValue
                        End If
                    Else
                        showMessage("CUTOFFDATE sudah ada di Branch " + ddlProvince.SelectedItem.Text, 3)
                        objTrans.Rollback()
                        conn.Close()
                        Exit Sub
                    End If
                Else

                    If ddlCountry.Visible = False And ddlProvince.Visible = True Then
                        If gengroup.SelectedValue.ToUpper = "CABANG" Then
                            genother1.Text = genother1.Text
                        Else
                            genother1.Text = TglCOA.Text
                            genother2.Text = ddlProvince.SelectedValue
                        End If
                    End If

                    If ddlItemtype.Visible = True Then
                        genother3.Text = ddlItemtype.SelectedValue
                    End If
                End If
            Else
                If ddlCountry.Visible = True Or ddlProvince.Visible = True Then
                    If ddlCountry.Visible = True And ddlProvince.Visible = True Then
                        If gengroup.SelectedValue.ToUpper = "CABANG" Then
                            genother1.Text = genother1.Text
                            genother2.Text = ddlProvince.SelectedValue
                        Else
                            genother1.Text = ddlCountry.SelectedValue
                            genother2.Text = ddlProvince.SelectedValue
                        End If
                    End If
                    If ddlItemtype.Visible = True Then
                        genother3.Text = ddlItemtype.SelectedValue
                    End If

                    If ddlCountry.Visible = True Then
                        If gengroup.SelectedValue.ToUpper = "CABANG" Then
                            genother1.Text = genother1.Text
                        Else
                            genother1.Text = ddlCountry.SelectedValue
                        End If
                    End If
                End If
            End If

            Dim sCbgAssetType As String = "" : Dim sCode As String = ""
            If gengroup.SelectedValue.ToUpper = "ASSETS_TYPE" Then
                sCbgAssetType = sCabang.SelectedValue
            ElseIf gengroup.SelectedValue.ToUpper = "LOCATION" Then
                sCbgAssetType = DdlType.SelectedValue
                genother1.Text = ddlCountry.SelectedValue
            ElseIf gengroup.SelectedValue.ToUpper = "CABANG" Then
                sCbgAssetType = ddlCountry.SelectedValue : sCode = gencode.Text
            ElseIf gengroup.SelectedValue.ToUpper = "CUTOFFDATE" Then
                genother1.Text = TglCOA.Text
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
                GenerateGenID()
                If gengroup.SelectedValue = "ITEMUNIT" Then
                    sSql = "INSERT into QL_mstgen (cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,upduser,updtime) " & _
                    "VALUES ('" & CompnyCode & "'," & genoid.Text & ",'" & Tchar(gencode.Text) & "','" & Tchar(gendesc.Text) & "','" & Tchar(gengroup.Text.Trim) & "','" & Tchar(genother1.Text) & "','" & Tchar(genother2.Text) & "','" & Tchar(genother3.Text) & "','" & Upduser.Text & "',current_timestamp)"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "INSERT into QL_mstgen(cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,upduser,updtime) " & _
                    "VALUES ('" & CompnyCode & "'," & genoid.Text & ",'" & Tchar(gencode.Text) & "','" & Tchar(gendesc.Text) & "','" & Tchar(gengroup.Text.Trim) & "','" & Tchar(genother1.Text) & "','" & Tchar(genother2.Text) & "','" & Tchar(genother3.Text) & "','" & Upduser.Text & "',current_timestamp)"
                Else
                    sSql = "INSERT into QL_mstgen(cmpcode,genoid,gencode,gendesc,gengroup,genother1,genother2,genother3,upduser,updtime,genother4,genother5,genother6) " & _
                    "VALUES ('" & CompnyCode & "'," & genoid.Text & ",'" & Tchar(gencode.Text) & "','" & Tchar(gendesc.Text) & "','" & Tchar(gengroup.Text.Trim) & "','" & Tchar(genother1.Text) & "','" & Tchar(genother2.Text) & "','" & Tchar(genother3.Text) & "','" & Upduser.Text & "',current_timestamp,'" & genother7.Text & "','" & sCbgAssetType & "','" & sCbgAssetType & "')"
                End If

            Else 'update data

                If gengroup.SelectedValue = "ITEMUNIT" Then
                    sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', genDesc='" & Tchar(gendesc.Text) & "', gengroup='" & Tchar(gengroup.Text.Trim) & "', genother1='" & Tchar(genother1.Text) & "', genother2='" & Tchar(genother2.Text) & "', genother3='" & Tchar(genother3.Text) & "',upduser='" & Upduser.Text & "', updtime=current_timestamp WHERE cmpcode like '%" & CompnyCode & "%' and genoid=" & genoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', genDesc='" & Tchar(gendesc.Text) & "', gengroup='" & Tchar(gengroup.Text.Trim) & "', genother1='" & Tchar(genother1.Text) & "', genother2='" & Tchar(genother2.Text) & "', genother3='" & Tchar(genother3.Text) & "',upduser='" & Upduser.Text & "', updtime=current_timestamp WHERE cmpcode like '%" & CompnyCode & "%' and genoid=" & genoid.Text

                Else
                    sSql = "UPDATE QL_mstgen SET gencode='" & Tchar(gencode.Text) & "', genDesc='" & Tchar(gendesc.Text) & "', gengroup='" & Tchar(gengroup.Text.Trim) & "', genother1='" & Tchar(genother1.Text) & "', genother2='" & Tchar(genother2.Text) & "', genother3='" & Tchar(genother3.Text) & "',upduser='" & Upduser.Text & "', updtime=current_timestamp,genother4='" & genother7.Text & "',genother5='" & sCbgAssetType & "',genother6='" & sCbgAssetType & "' WHERE cmpcode like '%" & CompnyCode & "%' and genoid=" & genoid.Text
                End If

            End If

            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "update QL_mstoid set lastoid=" & genoid.Text & " where tablename like '%QL_mstgen%' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                If gengroup.SelectedValue = "ITEMUNIT" Then
                    sSql = "update QL_mstoid set lastoid=" & genoid.Text & " where tablename like '%QL_mstgen%' and cmpcode like '%" & CompnyCode & "%' "
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("oid") = Nothing
        Response.Redirect("~\Master\fm_mstgen.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If genoid.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "WARNING_MSG", String.Format("<script>alert('{0}')</script>", "Please fill General ID"))
            Exit Sub
        End If
 
        Dim sColomnName() As String = {"custcityoid", "custprovoid", "custcountryoid", "prefixcmp", "prefixcp1", "prefixcp2", "prefixcp3", "itservtypeoid", "reqitemtype", "areaoid", "reqitembrand", "reqperson"}
        Dim sTable() As String = {"QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstcust", "QL_mstitemserv", "QL_trnrequest", "Ql_mstregiondtl"}

        If CheckDataExists(genoid.Text, sColomnName, sTable) = True Then
            showMessage("Tidak dapat dihapus karena digunakan di tabel lain !!", 2)
            Exit Sub
        End If

        If gengroup.SelectedValue = "ITEMUNIT" Then
            DeleteData("" & DB_JPT_RITEL & ".dbo.QL_mstgen", "genoid", genoid.Text, CompnyCode)
            DeleteData("" & DB_JPT_GROSIR & ".dbo.QL_mstgen", "genoid", genoid.Text, CompnyCode)
        Else
            DeleteData("QL_mstgen", "genoid", genoid.Text, CompnyCode)

        End If


        Response.Redirect("~\Master\fm_mstgen.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        clearItem()
        Response.Redirect("~\Master\fm_mstgen.aspx?awal=true")
    End Sub

    Protected Sub gengroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gengroup.SelectedIndexChanged
        genother3.Enabled = False
        checkDDL() : generateItemId("")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        If gengroup.SelectedValue = "CITY" Then
            sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('PROVINCE') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & ddlCountry.SelectedValue & "'"
            FillDDL(ddlProvince, sSql)
        ElseIf gengroup.SelectedValue = "ITEMTYPE" Or gengroup.SelectedValue = "ITEMMERK" Then
            sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMSUBGROUP') and cmpcode like '%" & CompnyCode & "%' and genother1 = '" & ddlCountry.SelectedValue & "'"
            FillDDL(ddlProvince, sSql)
        End If
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        If gengroup.SelectedValue = "ITEMTYPE" Or gengroup.SelectedValue = "ITEMMERK" Then
            sSql = "select genoid,gendesc, gengroup from QL_mstgen where gengroup IN ('ITEMTYPE') and cmpcode like '%" & CompnyCode & "%' and genother2 = '" & ddlProvince.SelectedValue & "'"
      FillDDL(ddlItemtype, sSql)
        End If

    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim gDesc, jumlahNol, c, c2, kodegen As String
        jumlahNol = "" : Dim d As Integer = 0
        gDesc = Left(gendesc.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0)) FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' and gengroup='" & gengroup.SelectedItem.Text & "'"))

        c2 = ToDouble(GetStrData("SELECT Count(isnull(left(gencode,1) ,0))+1 FROM ql_mstgen WHERE left(gencode,1) = '" & Tchar(gDesc) & "' and gengroup='" & gengroup.SelectedItem.Text & "'"))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If

        kodegen = GenNumberString(gDesc, "", d, 5)
        gencode.Text = kodegen
    End Sub

    Protected Sub gendesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gendesc.TextChanged
        generateItemId("")
    End Sub

    Protected Sub FilterGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FilterGroup.SelectedIndexChanged
        chkGroup.Checked = True
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub sCabang_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sCabang.SelectedIndexChanged
        checkDDL()
    End Sub

#End Region
End Class