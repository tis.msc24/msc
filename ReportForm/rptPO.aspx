<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="rptPO.aspx.vb" Inherits="rptPO" title="Toko Ali - Laporan Order Pembelian" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias" width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="LabelTItle" runat="server" CssClass="Title" Font-Bold="True" Font-Names="Verdana"
                    Font-Size="X-Large" ForeColor="Navy" Text=".: Laporan PO"></asp:Label></th>
        </tr>
        <tr>
            <th align="center" style="background-color: #ffffff" valign="center">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE><TBODY><TR><TD align=center><TABLE><TBODY><TR><TD align=left>Status</TD><TD align=left colSpan=3><asp:RadioButton id="rbPO" runat="server" Text="Status PO" __designer:wfdid="w3" GroupName="rbStatus" Checked="True" AutoPostBack="True"></asp:RadioButton> <asp:RadioButton id="rbPOLPB" runat="server" Text="Status PO-Purchase DO" __designer:wfdid="w4" GroupName="rbStatus" AutoPostBack="True"></asp:RadioButton></TD></TR><TR><TD id="tdTipeLap1" align=left runat="server"><asp:Label id="Label2" runat="server" Width="100px" Text="Tipe Laporan" __designer:wfdid="w5"></asp:Label></TD><TD id="tdTipeLap2" align=left colSpan=3 runat="server"><asp:DropDownList id="type" runat="server" Width="78px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w6"><asp:ListItem>Summary</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:CheckBox id="cbSR" runat="server" Text="Special Request" __designer:wfdid="w1"></asp:CheckBox></TD></TR><TR><TD id="tdPeriod1" align=left runat="server" Visible="true"><asp:Label id="Label6" runat="server" Text="Periode" __designer:wfdid="w7"></asp:Label></TD><TD id="tdperiod2" align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dateAwal" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w8"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w9"></asp:ImageButton>&nbsp;-&nbsp;<asp:TextBox id="dateAkhir" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w10"></asp:TextBox>&nbsp;<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w11"></asp:ImageButton>&nbsp;<asp:Label id="Label4" runat="server" ForeColor="Red" __designer:wfdid="w12">(dd/mm/yyyy)</asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="LblPOcLosed" runat="server" Text="Laporan" __designer:wfdid="w7"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:CheckBox id="CBclose" runat="server" Text="PO CLOSE" __designer:wfdid="w2" AutoPostBack="True"></asp:CheckBox> </TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label1" runat="server" Text="PIC" __designer:wfdid="w18"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="dono" runat="server" Width="178px" CssClass="inpTextDisabled" __designer:wfdid="w7" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindDO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w8"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseDO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w9"></asp:ImageButton> <asp:Label id="PersonOid" runat="server" __designer:wfdid="w18" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label9" runat="server" Text="No. PO" __designer:wfdid="w7"></asp:Label> </TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="SoNo" runat="server" Width="158px" CssClass="inpTextDisabled" __designer:wfdid="w2" Enabled="False" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindSO" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbEraseSO" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w4"></asp:ImageButton> <asp:Label id="PomstOid" runat="server" __designer:wfdid="w7" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label3" runat="server" Text="Katalog" __designer:wfdid="w38"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="ItemName" runat="server" Width="158px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w35" Enabled="False" TextMode="MultiLine" Rows="2"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchItem" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w39"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnEraseItem" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w40"></asp:ImageButton> <asp:Label id="ItemOId" runat="server" __designer:wfdid="w38" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label8" runat="server" Text="Supplier" __designer:wfdid="w38"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:TextBox id="custcode" runat="server" Width="158px" CssClass="inpTextDisabled" Font-Size="8pt" __designer:wfdid="w63" TextMode="MultiLine"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCust" runat="server" Width="25px" ImageUrl="~/Images/search.gif" ImageAlign="Top" Height="25px" __designer:wfdid="w64"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCust" runat="server" Width="25px" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" Height="25px" __designer:wfdid="w65"></asp:ImageButton> <asp:Label id="trnsuppoid" runat="server" __designer:wfdid="w38" Visible="False"></asp:Label></TD></TR><TR><TD align=left runat="server" Visible="true">Status PO</TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlstatusPO" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w42"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem Value="CLOSED MANUAL">CLOSED</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label5" runat="server" Width="75px" Text="Status PDO" __designer:wfdid="w28" Visible="False"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="statusDelivery" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w41" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>Approved</asp:ListItem>
<asp:ListItem>Rejected</asp:ListItem>
<asp:ListItem>INVOICED</asp:ListItem>
<asp:ListItem>In Approval</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left runat="server" Visible="true"><asp:Label id="Label7" runat="server" Width="108px" Text="Status PO - PDO" __designer:wfdid="w28" Visible="False"></asp:Label></TD><TD align=left colSpan=3 runat="server" Visible="true"><asp:DropDownList id="ddlPODO" runat="server" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w1" Visible="False"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>IN COMPLETE</asp:ListItem>
<asp:ListItem>COMPLETE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD id="Td5" align=left runat="server" Visible="false">Group</TD><TD id="Td6" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="FilterDDLGrup" runat="server" Width="250px" CssClass="inpText" Font-Size="8pt" __designer:wfdid="w26"></asp:DropDownList></TD></TR><TR><TD id="TD8" align=left runat="server" Visible="false">Merk</TD><TD id="TD7" align=left colSpan=3 runat="server" Visible="false"><asp:TextBox id="merk" runat="server" Width="250px" CssClass="inpTextDisabled" __designer:wfdid="w34" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnViewReport" runat="server" ImageUrl="~/Images/viewreport.png" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton> <asp:ImageButton id="btnPdf" runat="server" ImageUrl="~/Images/topdf.png" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton> <asp:ImageButton id="ibexcel" onclick="ibexcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton> <asp:ImageButton id="imbClear" onclick="imbClear_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=4><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:dtid="844424930132041" __designer:wfdid="w52" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate __designer:dtid="844424930132042">
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter" __designer:dtid="844424930132043"></DIV><DIV id="processMessage" class="processMessage" __designer:dtid="844424930132044"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple" __designer:dtid="844424930132045"><asp:Image id="Image4" runat="server" Width="81px" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w53"></asp:Image><BR __designer:dtid="844424930132047" />Please Wait .....</SPAN><BR __designer:dtid="844424930132048" /></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="CE1" runat="server" __designer:wfdid="w1" PopupButtonID="imageButton1" TargetControlID="dateAwal" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="CE2" runat="server" __designer:wfdid="w2" PopupButtonID="imageButton2" TargetControlID="dateAkhir" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" __designer:wfdid="w3" TargetControlID="dateAwal" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" __designer:wfdid="w4" TargetControlID="dateAkhir" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE><CR:CrystalReportViewer id="crvMutasiStock" runat="server" Width="350px" Height="50px" __designer:wfdid="w55" HasToggleGroupTreeButton="False" HasSearchButton="False" HasRefreshButton="True" HasPrintButton="False" HasExportButton="False" HasCrystalLogo="False" DisplayGroupTree="False" AutoDataBind="true" ShowAllPageIds="True"></CR:CrystalReportViewer></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnViewReport"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="ibexcel"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnPdf"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel><asp:UpdatePanel id="upListDO" runat="server"><contenttemplate>
<asp:Panel id="PanelListDO" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=center colSpan=3><asp:Label id="lblListDO" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of PIC"></asp:Label></TD></TR><TR><TD align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:Panel id="pnlFindListDO" runat="server" Width="100%" DefaultButton="btnFindListDO"><asp:Label id="Label124" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListDO" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="doitemno">PIC</asp:ListItem>
<asp:ListItem Value="custname">User ID</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListDO" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"></asp:TextBox><BR /><asp:CheckBox id="PicCb" runat="server" Text="Status"></asp:CheckBox> &nbsp; <asp:DropDownList id="DDLStatus" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="AKTIF">AKTIF</asp:ListItem>
<asp:ListItem Value="IN AKTIF">IN AKTIF</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListDO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListDO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> </asp:Panel> </TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="btnSelectAllDO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneDO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedDO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:GridView id="gvListDO" runat="server" Width="98%" ForeColor="#333333" PageSize="8" GridLines="None" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:CheckBox id="cbLMDO" runat="server" ToolTip='<%# eval("doitemmstoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="doitemmstoid" HeaderText="Draft No" Visible="False">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemno" HeaderText="Nama PIC">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="User ID">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemcustpono" HeaderText="Username">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="doitemmststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
&nbsp;
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListDO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListDO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListDO" runat="server" TargetControlID="btnHiddenListDO" PopupControlID="PanelListDO" PopupDragHandleControlID="lblListDO" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListDO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListSO" runat="server"><contenttemplate>
<asp:Panel id="PanelListSO" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSO" runat="server" Font-Size="Large" Font-Bold="True" Text="List Purchasing Order"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListSO" runat="server" Width="100%" DefaultButton="btnFindListSO"><asp:Label id="Label24" runat="server" Font-Size="8pt" Text="Filter :"></asp:Label> <asp:DropDownList id="DDLFilterListSO" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="trnrequestitemno">No. PO</asp:ListItem>
<asp:ListItem Value="trnrequestitemnote">Keterangan</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListSO" runat="server" Width="150px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListSO" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewAllListSO" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnSelectAllSO" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneSO" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedSO" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=3><asp:GridView id="gvListSO" runat="server" Width="98%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="trnrequestitemoid,trnrequestitemno,trnrequestitemdate,trnrequestitemstatus,trnrequestitemnote" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                <asp:CheckBox ID="cbLMSO" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("trnrequestitemoid") %>' />
                                            
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="trnrequestitemoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemno" HeaderText="No. PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemdate" HeaderText="Tanggal">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="typeSO" HeaderText="Type PO">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemnote" HeaderText="Keterangan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnrequestitemstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                        &nbsp;
                                    
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListSO" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbListSO" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSO" runat="server" TargetControlID="btnHiddenListSO" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSO" PopupControlID="PanelListSO">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHiddenListSO" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="upListMat" runat="server"><contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Width="900px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Large" Font-Bold="True" Text="List Of Katalog" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat"><TABLE style="WIDTH: 100%"><TR><TD class="Label" align=center colSpan=3><SPAN style="FONT-SIZE: 8pt">Filter</SPAN> <SPAN style="FONT-SIZE: 8pt">:</SPAN> <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="itemcode">Kode</asp:ListItem>
<asp:ListItem Value="itemdesc">Katalog</asp:ListItem>
<asp:ListItem Value="JenisNya">Jenis</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText" Font-Size="8pt"></asp:TextBox>&nbsp; </TD></TR><TR><TD class="Label" align=center colSpan=3><SPAN style="FONT-SIZE: 8pt">Jenis</SPAN> <SPAN style="FONT-SIZE: 8pt">:</SPAN> <asp:DropDownList id="stockFlag" runat="server" CssClass="inpText" Font-Size="8pt"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="T">Transaction (Barang Dagangan)</asp:ListItem>
<asp:ListItem Value="I">Material Usage (Perlengkapan/Habis karena Dipakai)</asp:ListItem>
<asp:ListItem Value="V">Barang Hadiah/Voucher Fisik (Merchandise)</asp:ListItem>
<asp:ListItem>ASSET</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFindListMat" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel> </TD></TR><TR><TD class="Label" align=center colSpan=3><asp:ImageButton id="btnALLSelectItem" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnSelectNoneItem" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewCheckedItem" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:UpdateProgress id="uprogListMat" runat="server" AssociatedUpdatePanelID="upListMat"><ProgressTemplate>
<TABLE style="WIDTH: 200px"><TR><TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple">Load Data, Please Wait ...</TD></TR><TR><TD align=center><asp:Image id="imgLoadListMat" runat="server" ImageUrl="~/Images/loadingbar.gif"></asp:Image></TD></TR></TABLE>
</ProgressTemplate>
</asp:UpdateProgress> </TD></TR><TR style="FONT-SIZE: 8pt"><TD id="TD3" align=left colSpan=3 runat="server" Visible="false">Jenis <asp:TextBox id="tbData" runat="server" Width="25px" CssClass="inpText" Font-Size="8pt">8</asp:TextBox> <SPAN style="FONT-SIZE: 8pt">Data</SPAN> <SPAN style="FONT-SIZE: 8pt">Per</SPAN> <SPAN style="FONT-SIZE: 8pt">Page</SPAN> <asp:LinkButton id="lbShowData" runat="server" Font-Size="8pt">>> View</asp:LinkButton></TD></TR><TR><TD align=center colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 282px; BACKGROUND-COLOR: beige"><asp:GridView id="gvListMat" runat="server" Width="99%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="itemcode" CellPadding="4" GridLines="None" PageSize="8">
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbLM" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("itemoid") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="itemcode" HeaderText="Kode">
                                                <HeaderStyle CssClass="gvhdr" Font-Strikeout="False" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemdesc" HeaderText="Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="merk" HeaderText="Merk">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="satuan3" HeaderText="Satuan">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JenisNya" HeaderText="Jenis Katalog">
                                                <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="itemflag" HeaderText="Status">
                                                <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"
                                            HorizontalAlign="Right" />
                                        <EmptyDataTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="Klik tombol View All atau Find untuk tampilkan data"></asp:Label>
                                        </EmptyDataTemplate>
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    </asp:GridView> </DIV></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListMat" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListMat" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblTitleListMat" PopupControlID="pnlListMat" Drag="True">
                </ajaxToolkit:ModalPopupExtender> </asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
                <asp:UpdatePanel id="upListCust" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of supplier"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" Font-Size="8pt" DefaultButton="btnFindListCust"><SPAN style="FONT-SIZE: 8pt">Filter</SPAN> <SPAN style="FONT-SIZE: 8pt">:</SPAN> <asp:DropDownList id="DDLFilterListCust" runat="server" Width="100px" CssClass="inpText" Font-Size="8pt"><asp:ListItem Value="custcode">Code</asp:ListItem>
<asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilterListCust" runat="server" Width="150px" CssClass="inpText" Font-Size="8pt"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnViewAllListCust" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:ImageButton id="btnSelectAllCust" runat="server" ImageUrl="~/Images/selectall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnSelectNoneCust" runat="server" ImageUrl="~/Images/selectnone.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnViewCheckedCust" runat="server" ImageUrl="~/Images/viewselected.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" Width="98%" ForeColor="#333333" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
                                            <asp:CheckBox ID="cbLMCust" runat="server" Checked='<%# eval("checkvalue") %>' ToolTip='<%# eval("custoid") %>' />
                                        
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gvpopup" Font-Strikeout="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custaddr" HeaderText="Alamat">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbAddToListListCust" runat="server">[ Add To List ]</asp:LinkButton> - <asp:LinkButton id="lkbCloseListCust" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListCust" PopupControlID="pnlListCust" Drag="True">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel><asp:UpdatePanel id="upPopUpMsg" runat="server"><contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" PopupControlID="pnlPopUpMsg" DropShadow="True" Drag="True"></ajaxToolkit:ModalPopupExtender> <SPAN style="DISPLAY: none"><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button></SPAN> 
</contenttemplate>
    </asp:UpdatePanel></th>
        </tr>
    </table>
</asp:Content>

