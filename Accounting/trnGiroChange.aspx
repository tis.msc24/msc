<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnGiroChange.aspx.vb" Inherits="Accounting_trnGiroChange" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px"
                    ForeColor="Navy" Text="GANTI GIRO"></asp:Label></th>
        </tr>
    <tr>
        <th align="left" style="background-color: #ffffff" valign="center">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%" TabIndex="1">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            &nbsp;<strong><span style="font-size: 9pt">List of Giro Change</span></strong>&nbsp;<span
                                style="font-size: 9pt">:.</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel5" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel3" runat="server" DefaultButton="btnSearch" __designer:wfdid="w274"><TABLE width="100%"><TBODY><TR><TD style="WIDTH: 73px" align=left>Periode</TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w275"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w276"></asp:ImageButton> <asp:Label id="Label21" runat="server" Font-Size="X-Small" Text="to" __designer:wfdid="w277"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w278"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w279"></asp:ImageButton> <asp:Label id="Label10" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w280"></asp:Label> </TD></TR><TR><TD style="WIDTH: 73px" align=left>Tipe</TD><TD align=left><asp:DropDownList id="ddlTipe" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w281" Enabled="False"><asp:ListItem>BGM</asp:ListItem>
<asp:ListItem Text="BGK" Value="BGK"></asp:ListItem>
</asp:DropDownList></TD><TD align=left>Reason</TD><TD align=left><asp:DropDownList id="FilterStatus" runat="server" Width="113px" CssClass="inpText" __designer:wfdid="w282"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem Text="Giro Di Tolak" Value="Giro Di Tolak"></asp:ListItem>
<asp:ListItem>Ganti Giro</asp:ListItem>
<asp:ListItem Enabled="False">Ganti Cash/Bank</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 73px" align=left>Old Giro No.</TD><TD align=left><asp:TextBox id="searchNoGiroLama" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w283"></asp:TextBox></TD><TD align=left>New Giro No.</TD><TD align=left><asp:TextBox id="searchNoGiroBaru" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w284"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 73px; HEIGHT: 10px" align=left><asp:Label id="lblcust_supp" runat="server" Width="92px" Text="Cust/Supp" __designer:wfdid="w285"></asp:Label></TD><TD style="HEIGHT: 10px" align=left><asp:TextBox id="searchNamaCustomer" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w286"></asp:TextBox></TD><TD style="HEIGHT: 10px" align=left>Bank</TD><TD style="HEIGHT: 10px" align=left><asp:DropDownList id="bankfilter" runat="server" Width="202px" CssClass="inpText" __designer:wfdid="w287"></asp:DropDownList></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w288"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w289"></asp:ImageButton>&nbsp;</TD></TR><TR><TD align=left colSpan=4><FIELDSET style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset1"><DIV id="Div1"><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: #ffffff" class="gvhdr"><asp:GridView id="GVmst" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w267" EmptyDataText="No data in database." AutoGenerateColumns="False" CellPadding="4" DataKeyNames="TukarGiroOid" OnRowDataBound="GVmst_RowDataBound" GridLines="None" OnSelectedIndexChanged="GVmst_SelectedIndexChanged">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="TukarGiroOid" DataNavigateUrlFormatString="trnGiroChange.aspx?oid={0}" DataTextField="nocg" HeaderText="No CG">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cashbankno" HeaderText="No. Trans. Lama">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="170px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Cust/Supp">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NoGiroLama" HeaderText="Old Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NoGiroBaru" HeaderText="New Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sebab" HeaderText="Reason">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="70px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField Visible="False"><ItemTemplate>
<%--                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick='<%# ConfirmPrint(Container.DataItem("cashbankno")) %>'>Print</asp:LinkButton>
--%><asp:ImageButton id="imbPrintFormList" onclick="imbPrintFormList_Click" runat="server" ImageUrl="~/Images/print.gif" ImageAlign="AbsMiddle" ToolTip='<%# Eval("TukarGiroOid") %>' CommandArgument='<%# Eval("TukarGiroOid") %>'></asp:ImageButton><ajaxToolkit:ConfirmButtonExtender id="ConfirmButtonExtender2" runat="server" TargetControlID="imbPrintFormList" ConfirmText="Are you sure to Print?"></ajaxToolkit:ConfirmButtonExtender> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblmsg" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data tidak ditemukan !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle HorizontalAlign="Center" BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></DIV><ajaxToolkit:MaskedEditExtender id="meece4" runat="server" __designer:wfdid="w291" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear" TargetControlID="txtPeriode1"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meece5" runat="server" __designer:wfdid="w292" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear" TargetControlID="txtPeriode2"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ce4" runat="server" __designer:wfdid="w293" TargetControlID="txtperiode1" PopupButtonID="btnPeriode1" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ce5" runat="server" __designer:wfdid="w294" TargetControlID="txtperiode2" PopupButtonID="btnPeriode2" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></FIELDSET></TD></TR></TBODY></TABLE></asp:Panel>&nbsp;&nbsp;&nbsp;&nbsp; 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="GVmst"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD align=left><asp:Label id="Label6" runat="server" Font-Size="X-Small" Text="Cabang" __designer:wfdid="w166"></asp:Label></TD><TD align=left><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" Font-Bold="False" __designer:wfdid="w167" AutoPostBack="True"></asp:DropDownList> <asp:Label id="TukarGiroOid" runat="server" Width="50px" __designer:wfdid="w224" Visible="False"></asp:Label></TD><TD align=left>Tanggal <asp:Label id="Label2" runat="server" CssClass="Important" Text="*" __designer:wfdid="w168"></asp:Label></TD><TD align=left><asp:TextBox id="TanggalTukarGiro" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w169" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnTanggalTukarGiro" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w170"></asp:ImageButton>&nbsp;<asp:Label id="Label8" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w171"></asp:Label></TD></TR><TR><TD align=left>Giro Change No.</TD><TD align=left><asp:TextBox id="NOCG" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w172" Enabled="False" ReadOnly="True"></asp:TextBox> <asp:Label id="girodtloid_old" runat="server" Width="50px" __designer:wfdid="w188"></asp:Label></TD><TD align=left>Tipe</TD><TD align=left><asp:DropDownList id="jenis" runat="server" Width="51px" CssClass="inpText" __designer:wfdid="w173" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="jenis_SelectedIndexChanged"><asp:ListItem>BGM</asp:ListItem>
<asp:ListItem>BGK</asp:ListItem>
</asp:DropDownList> <asp:Label id="cashbankacctgoid_old" runat="server" Width="50px" __designer:wfdid="w174" Visible="False"></asp:Label></TD></TR><TR><TD align=left>Reason</TD><TD align=left><asp:DropDownList id="sebab" runat="server" Width="99px" CssClass="inpText" __designer:wfdid="w175" AutoPostBack="True" OnSelectedIndexChanged="sebab_SelectedIndexChanged"><asp:ListItem>Ganti Giro</asp:ListItem>
<asp:ListItem Enabled="False">Giro Di Tolak</asp:ListItem>
<asp:ListItem Enabled="False">Ganti Cash/Bank</asp:ListItem>
</asp:DropDownList> <asp:Label id="bankoid_old" runat="server" Width="50px" __designer:wfdid="w176"></asp:Label></TD><TD align=left><asp:Label id="Label11" runat="server" Width="82px" Text="Old Giro No." __designer:wfdid="w177"></asp:Label><asp:Label id="Label12" runat="server" CssClass="Important" Text="*" __designer:wfdid="w178"></asp:Label></TD><TD align=left><asp:TextBox id="noGiroLama" runat="server" Width="170px" CssClass="inpTextDisabled" __designer:wfdid="w179" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbGiro" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w180"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbClearGiro" onclick="imbClearGiro_Click" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w181"></asp:ImageButton> <asp:Label id="refoid" runat="server" Width="50px" __designer:wfdid="w182" Visible="False"></asp:Label> <asp:Label id="reftype" runat="server" Width="50px" __designer:wfdid="w183" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Width="117px" Text="Tempo Giro Lama" __designer:wfdid="w184"></asp:Label>&nbsp;<asp:Label id="Label5" runat="server" CssClass="Important" Text="*" __designer:wfdid="w185"></asp:Label></TD><TD align=left><asp:TextBox id="payduedatelama" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w186" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w187"></asp:Label> </TD><TD align=left>Bank Lama</TD><TD align=left><asp:TextBox id="banklama" runat="server" Width="170px" CssClass="inpTextDisabled" __designer:wfdid="w189" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Width="69px" Text="Customer" __designer:wfdid="w190"></asp:Label></TD><TD align=left><asp:TextBox id="custname" runat="server" Width="200px" CssClass="inpTextDisabled" __designer:wfdid="w191" Enabled="False" ReadOnly="True"></asp:TextBox></TD><TD align=left>No. Transaksi Lama</TD><TD align=left><asp:TextBox id="nobukti" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w192" Enabled="False"></asp:TextBox></TD></TR><TR><TD align=left>Total</TD><TD align=left><asp:TextBox id="amount" runat="server" Width="101px" CssClass="inpTextDisabled" __designer:wfdid="w194" Enabled="False"></asp:TextBox> <asp:TextBox id="currency" runat="server" Width="32px" CssClass="inpTextDisabled" __designer:wfdid="w195" Enabled="False"></asp:TextBox> </TD><TD align=left>Currency</TD><TD align=left><asp:TextBox id="currencyrate" runat="server" Width="77px" CssClass="inpTextDisabled" __designer:wfdid="w196" Enabled="False"></asp:TextBox> <asp:Label id="cashbankgroup" runat="server" Width="50px" __designer:wfdid="w197" Visible="False"></asp:Label></TD></TR><TR><TD id="TD1" align=left runat="server">Bank Baru</TD><TD id="TD2" align=left runat="server"><asp:DropDownList id="bankbaru" runat="server" Width="200px" CssClass="inpText" __designer:wfdid="w198"></asp:DropDownList><asp:Label id="girodtloid" runat="server" Width="50px" __designer:wfdid="w199" Visible="False"></asp:Label></TD><TD id="Td13" align=left runat="server">New&nbsp;Giro No.&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="*" __designer:wfdid="w200"></asp:Label></TD><TD id="Td14" align=left runat="server"><asp:TextBox id="noGiroBaru" runat="server" Width="170px" CssClass="inpTextDisabled" __designer:wfdid="w201" Enabled="False" MaxLength="25"></asp:TextBox> <asp:ImageButton id="creditsearch" onclick="creditsearch_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w202"></asp:ImageButton></TD></TR><TR><TD id="TD3" align=left runat="server">Cash / Bank</TD><TD id="TD4" align=left runat="server"><asp:DropDownList id="cash_bank_coa" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w203"></asp:DropDownList></TD><TD id="Td15" align=left runat="server">Tempo&nbsp;Giro Baru&nbsp;<asp:Label id="Label7" runat="server" CssClass="Important" Text="*" __designer:wfdid="w204"></asp:Label></TD><TD id="Td16" align=left runat="server"><asp:TextBox id="payduedatebaru" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w205" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDueDatebaru" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w206" Visible="False"></asp:ImageButton> <asp:Label id="lblNotice" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w207"></asp:Label></TD></TR><TR><TD align=left>Catatan</TD><TD align=left colSpan=2><asp:TextBox id="note" runat="server" Width="300px" Height="41px" CssClass="inpText" __designer:wfdid="w208" MaxLength="100" TextMode="MultiLine"></asp:TextBox></TD><TD align=left><asp:Label id="lblPost" runat="server" __designer:wfdid="w225"></asp:Label> <asp:Label id="cashbankoid" runat="server" Width="50px" __designer:wfdid="w193" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=4><SPAN style="FONT-SIZE: 10pt; COLOR: #585858">Last update on </SPAN><asp:Label id="UpdTime" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w209"></asp:Label> by <asp:Label id="UpdUser" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w210"></asp:Label> <asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" Text="New Detail" __designer:wfdid="w217" Visible="False"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:wfdid="w211" Visible="False"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsBottom" __designer:wfdid="w212"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" __designer:wfdid="w213"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:wfdid="w214"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbPrintNota" onclick="imbPrintNota_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w215" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w216" Visible="False"></asp:ImageButton> </TD></TR><TR><TD align=center colSpan=4><ajaxToolkit:CalendarExtender id="ceTanggal" runat="server" __designer:wfdid="w219" PopupButtonID="btnTanggalTukarGiro" Format="dd/MM/yyyy" TargetControlID="TanggalTukarGiro"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="ceDueDateBaru" runat="server" __designer:wfdid="w218" PopupButtonID="btnDueDatebaru" Format="dd/MM/yyyy" TargetControlID="payduedatebaru"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeceTanggal" runat="server" __designer:wfdid="w221" TargetControlID="TanggalTukarGiro" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeceDUeDateBaru" runat="server" __designer:wfdid="w220" TargetControlID="payduedatebaru" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w222" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="250"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div6" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w223"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress>&nbsp;&nbsp;&nbsp; </TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="imbPrintNota"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                                &nbsp;Form Ganti Giro :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></th>
    </tr>
    </table>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" DropShadow="True" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" Drag="True" BackgroundCssClass="modalBackground">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel3" runat="server">
        <contenttemplate>
<asp:Panel id="PanelGiro" runat="server" Width="750px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblGiroData" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Akun Giro"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small; TEXT-ALIGN: center" align=left><asp:DropDownList id="giroFilter" runat="server" Width="82px" CssClass="inpText"><asp:ListItem Value="cd.refno">Giro No.</asp:ListItem>
<asp:ListItem Value="cashbankno">Id Proof</asp:ListItem>
<asp:ListItem Value="g.gendesc">Bank</asp:ListItem>
</asp:DropDownList>&nbsp;: <asp:TextBox id="tbGiro" runat="server" CssClass="inpText"></asp:TextBox>&nbsp; <asp:ImageButton id="btnFindInvoice" onclick="btnFindInvoice_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="imbViewAllInv" onclick="imbViewAllInv_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton></TD></TR><TR><TD align=left><FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 200px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset5"><DIV id="Div5"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView style="Z-INDEX: 100; LEFT: -1px; TOP: 128px; BACKGROUND-COLOR: transparent" id="gvGiro" runat="server" Width="98%" ForeColor="#333333" OnSelectedIndexChanged="gvGiro_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cashbankoid,refno,custoid,custname,duedate,amount,cashbankno,bank,cashbankgroup,cashbankacctgoid,cashbankcurrate,currency,bankoid,girodtloid" OnRowDataBound="gvGiro_RowDataBound" AllowSorting="True" EmptyDataRowStyle-ForeColor="Red" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="bank" HeaderText="Bank">
<FooterStyle HorizontalAlign="Center"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="refno" HeaderText="Giro No" SortExpression="refno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankno" HeaderText="Id Proof">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer/Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Total Giro" SortExpression="amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="125px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label6" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Tidak ada data AR !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSales" onclick="CloseSales_Click" runat="server" CausesValidation="False" Font-Size="X-Small" Font-Bold="True">[ Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHiddenGiro" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="ModalPopupExtenderGiro" runat="server" TargetControlID="btnHiddenGiro" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="PanelGiro" PopupDragHandleControlID="lblGiroData"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel6" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Size="Small" Font-Bold="True">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPosting2" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanel4sX" runat="server">
        <contenttemplate>
<asp:Panel id="Panel1X" runat="server" Width="800px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left><asp:Label id="lblSuppdataX" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Giro Baru"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left>Filter : <asp:DropDownList id="DDLSuppIDX" runat="server" Width="96px" CssClass="inpText"><asp:ListItem Selected="True" Value="girono">Giro No</asp:ListItem>
<asp:ListItem Value="m.gendesc">Bank</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFindSuppIDX" runat="server" Width="121px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="ibtnSuppIDX" onclick="ibtnSuppIDX_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="imbViewAllsX" onclick="imbViewAllsX_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton> </TD></TR><TR><TD align=left><asp:GridView id="gvSupplierX" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="girodtloid,Trans_No,girono,Bank,bankoid,NoRekening,GiroDueDate,amount" AllowSorting="True" EmptyDataText="No data in database." GridLines="None">
<RowStyle BackColor="#FFFBD6" Font-Size="X-Small" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="girono" HeaderText="Giro No" SortExpression="girono">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank" SortExpression="bank">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="NoRekening" HeaderText="No.Akun" SortExpression="NoRekening">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroDueDate" HeaderText="Jatuh Tempo">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblstatusdatasupp" runat="server" ForeColor="Red" Text="No Suppplier Data !" Visible="False"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left><asp:LinkButton id="CloseSuppX" onclick="CloseSuppX_Click" runat="server" CausesValidation="False" Font-Bold="False">[ Close ]</asp:LinkButton> </TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender3sX" runat="server" TargetControlID="hiddenbtn2sX" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="Panel1x" PopupDragHandleControlID="lblSuppdata"></ajaxToolkit:ModalPopupExtender> <asp:Button id="hiddenbtn2sX" runat="server" Text="hiddenbtn2sX" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>