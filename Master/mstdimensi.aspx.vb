Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class mstdimensi
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim conn2 As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private oRegex As Regex : Dim oMatches As MatchCollection
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Private rptSupp As New ReportDocument
    Private cProc As New ClassProcedure
    Dim rptReport As New ReportDocument
    Dim salesitem As String = ""
#End Region

#Region "Function"
    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function 

    Function formatToIDR(ByVal money As String) As String
        'Dim ci As New System.Globalization.CultureInfo("id-ID")
        'Return money.ToString("c", ci)
        If money.IndexOf(".") > 0 Then
            Dim depan As String = money.Substring(0, money.IndexOf("."))
            depan = depan.Replace(",", ".")
            Dim belakang As String = money.Substring(money.IndexOf(".") + 1, money.Length - (depan.Length + 1))
            Return depan & "," & belakang
        Else
            Return money.Replace(",", ".")
        End If
    End Function
#End Region

#Region "Procedure"

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function
 
    Private Sub bindDataMaterial()
        sSql = "Select i.itemcode, itemdesc, itemoid, g.gendesc satuan1, konversi1_2, merk,dimensi_p,dimensi_l,dimensi_t,beratbarang,beratvolume,itemtype,stockflag,statusitem,itemflag,satuan1 unitoid,itemgroupoid,keterangan From ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and itemflag='AKTIF' inner join ql_mstgen g1 ON g1.genoid=itemgroupoid And g1.gengroup='ITEMGROUP' LEFT join ql_mstgen gt ON gt.genoid=itemtype And gt.gengroup='ITEMTYPE' Where (itemdesc like '%" & Tchar(itemdesc.Text) & "%' or itemcode like '%" & Tchar(itemdesc.Text) & "%' or merk like '%" & Tchar(itemdesc.Text) & "%')"
        FillGV(gvItem, sSql, "ql_mstmat")
        gvItem.Visible = True
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        ' this function will load ALL DATA at first time
        ' so call this function wisely.

        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")

        If Session("FilterDDL") Is Nothing = False Or Session("FilterDDL2") Is Nothing = False Or Session("FilterDDL3") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                    sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
                Else
                    sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
                End If
            End If
        Else

            If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = " where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If

            If Session("UserId") <> "admin" Then
                If salesitem = "" Then salesitem = 0
                sWhere &= " and QL_mstitem.personoid=" & salesitem & ""
            End If

        End If

        If SelectTop.Text.Trim = "" Or SelectTop.Text <= 0 Then
            SelectTop.Text = 100
        End If
        sSql = "Select i.itemcode, itemdesc, itemoid, g.gendesc satuan1, konversi1_2, merk,dimensi_p,dimensi_l,dimensi_t,beratbarang,beratvolume,itemtype,stockflag,statusitem,itemflag,satuan1 unitoid,itemgroupoid,keterangan,Case stockflag When 'T' Then 'Barang Dagangan' When 'I' Then 'Material Usage (Perlengkapan)' When 'V' Then 'Barang Hadiah (Voucher Fisik/Merchandise)' Else 'ASSET (Barang Depresiasi)' End JenisBarang from ql_mstitem i inner join ql_mstgen g on g.genoid=satuan1 and itemflag='AKTIF' inner join ql_mstgen g1 ON g1.genoid=itemgroupoid And g1.gengroup='ITEMGROUP' LEFT join ql_mstgen gt ON gt.genoid=itemtype And gt.gengroup='ITEMTYPE' Where (" & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' Or merk LIKE '%" & Tchar(FilterText.Text) & "%')"
        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
        Session("dtPublic") = GVmstgen.DataSource
    End Sub

    Public Sub findAData()
        ' search data
        Dim dtTmp As DataTable = Session("dtPublic")
        Dim dv As DataView = dtTmp.DefaultView
        Dim aa As Integer = 2
        GVmstgen.DataSource = dv.ToTable()
        GVmstgen.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As String)
        Try
            sSql = "SELECT mi.[cmpcode],mi.personoid,mi.[itemoid],mi.[itemcode],mi.[itemname],mi.[itemdesc],mi.maxstock, " & _
                   "mi.[itemgroupoid],mi.[itemsubgroupoid],mi.itemtype,mi.merk,mi.Ukuran,mi.Warna, mi.[itembarcode1], " & _
                   "mi.[itembarcode2], mi.[itembarcode3], " & _
                    "mi.[lastpricebuyunit1], mi.[lastpricebuyunit2], mi.[lastpricebuyunit3], " & _
                   "mi.[itempriceunit1], mi.[itempriceunit2], mi.[itempriceunit3], " & _
                    "mi.[itempriceunit1retail], mi.[itempriceunit2retail], mi.[itempriceunit3retail], " & _
                     "mi.[bottompricegrosir], mi.[bottompriceretail], " & _
                   "mi.[satuan1], mi.[satuan2], mi.[satuan3],mi.payoid, " & _
                   "mi.[itemsafetystockunit1],mi.[createuser],mi.[upduser],mi.[updtime], mi.[itemflag], mi.[acctgoid] , konversi1_2, konversi2_3,mi.personoid,mi.itempictureloc, keterangan , editharga ,itemsafetystockunitgrosir,maxstockgrosir ,pricelist,discunit1,discunit2,discunit3,qty3_to2,qty3_to1,stockflag  ,isnull(mi.has_SN,0) has_SN,isnull(dimensi_p,0)dimensi_p,isnull(dimensi_l,0)dimensi_l,isnull(dimensi_t,0)dimensi_t,isnull(beratbarang,0.00) beratbarang,isnull(beratvolume,0.00) beratvolume" & _
                    " ,statusitem,stockflag FROM [QL_mstitem] mi where mi.cmpcode='" & CompnyCode & "' and mi.itemoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False

            While xreader.Read
                itemoid.Text = xreader("itemoid")
                itemcode.Text = xreader("itemcode")
                itemdesc.Text = xreader("itemdesc")

                itemgroupoid.SelectedValue = xreader("itemgroupoid").ToString.Trim
                dd_merk.SelectedValue = xreader("merk").ToString.Trim
                DDLStatusItem.SelectedValue = xreader("statusitem").ToString.Trim
                dd_stock.SelectedValue = xreader("stockflag").trim
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss")
                ddlStatus.SelectedValue = xreader("itemflag").ToString.Trim
                keterangan.Text = xreader("keterangan").trim
                btnDelete.Enabled = True
                'SN
                'Dimensi
                txt_panjang.Text = xreader("dimensi_p")
                txt_lebar.Text = xreader("dimensi_l")
                txt_tinggi.Text = xreader("dimensi_t")
                beratBarang.Text = xreader("beratbarang")
                beratVolume.Text = xreader("beratvolume")
                If xreader("stockflag").ToString = "Stock" Then
                    dd_stock.SelectedValue = "Stock"
                Else
                    dd_stock.SelectedValue = "transaction"
                End If
            End While
            xreader.Close()
            conn.Close()
            If Session("UserId") <> "admin" Then
                'spgOid.Enabled = False
            End If

        Catch ex As Exception
            showMessage(ex.Message & ex.ToString & sSql, "ERROR") : Exit Sub
        End Try

    End Sub

    Public Sub GenerateGenID()
        itemoid.Text = GenerateID("QL_mstitem", CompnyCode)
    End Sub

    Sub clearItem()
        itemoid.Text = "" : itemcode.Text = ""
        itemdesc.Text = "" : itemgroupoid.SelectedIndex = 0
        satuan2.SelectedIndex = 0 : ddlStatus.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")

    End Sub

    Sub initDDL()
        Dim msg As String = ""

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemgroupoid, sSql)
        Else
            msg &= " - Isi Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If
        'itemtype
        sSql = "select genoid ,gendesc from ql_mstgen where cmpcode = '" & CompnyCode & "' and gengroup = 'ITEMTYPE'order by GENDESC"
        FillDDL(dd_type, sSql)

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        'itemmerk
        sSql = "select gendesc ,g2.gendesc from ql_mstgen g2 where cmpcode = '" & CompnyCode & "' and gengroup = 'ITEMMERK' ORDER BY g2.GENDESC"
        FillDDL(dd_merk, sSql)

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMUNIT' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(satuan2, sSql)

        If msg <> "" Then
            showMessage(msg, CompnyName & " - INFORMASI")
            btnSave.Visible = False
            Exit Sub
        End If

    End Sub

    Sub inituserID()
        salesitem = GetStrData("select personoid from QL_MSTPERSON where PERSONNAME =(select username from QL_MSTPROF where USERID='" & Session("UserID") & "') ")
    End Sub

    Private Sub PrintReport(ByVal oid As String, ByVal sFileName As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = "" : Dim sFilter As String = ""
        Try
            If printType.Text = "PDF" Then
                rptReport.Load(Server.MapPath("~/report/rptMstItem.rpt"))
                sWhere = " Where QL_mstitem.cmpcode LIKE '%" & CompnyCode & "%' and itemflag = '" & ddlStatusView.SelectedValue & "'"
                If FilterText.Text.Trim <> "" Then
                    If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                        sWhere &= " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
                    Else
                        sWhere &= " and upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
                    End If
                    sFilter &= " " & FilterDDL.SelectedItem.Text & " = " & FilterText.Text & ", "
                End If


                If sFilter = "" Then
                    sFilter = " ALL "
                End If
                rptReport.SetParameterValue("sFilter", " Filter By : " & sFilter)
                rptReport.SetParameterValue("sWhere", sWhere)
            Else
                rptReport.Load(Server.MapPath("~/report/rptMstDimensiExl.rpt"))

                sWhere = " Where i.cmpcode ='" & CompnyCode & "' AND i.itemflag = '" & ddlStatusView.SelectedValue & "' AND " & FilterDDL.SelectedValue & " LIKE '%" & TcharNoTrim(FilterText.Text) & "%'"
                rptReport.SetParameterValue("SelectTop", SelectTop.Text)
                rptReport.SetParameterValue("dWhere", sWhere)
            End If

            cProc.SetDBLogonForReport(rptReport, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            If printType.Text = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rptReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MASTER_ITEM")
                rptReport.Close() : rptReport.Dispose()
            ElseIf printType.Text = "EXCEL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                rptReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "MASTER_DIMENSI")
                rptReport.Close() : rptReport.Dispose()
            End If

        Catch ex As Exception
            lblError.Text = ex.ToString & "<br />"
            Exit Sub
        End Try
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)

        Dim sWhere As String = ""
        If FilterText.Text.Trim <> "" Then
            If FilterDDL.SelectedItem.Text.ToUpper = "HARGA QTY" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA PARTAI" Or FilterDDL.SelectedItem.Text.ToUpper = "HARGA ECER" Then
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '" & Tchar(FilterText.Text) & "' "
            Else
                sWhere = " Where upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
        End If
        Response.Clear()
        If printType = "SUMMARY" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"
            sSql = "SELECT itemoid [Id Item], itemcode KOde, itemdesc Deskripsi,merk, QL_mstgen.gendesc as [Group barang] , g1.gendesc as Unit,QL_mstitem.createuser [Create User], QL_mstitem.Upduser [Update User], QL_mstitem.Updtime, itemflag, personname as PIC ,dimensi_p panjang,dimensi_l lebar,dimensi_t tinggi,beratvolume [Berat Volume],beratbarang [Berat barang] FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid = QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1 " & sWhere & " ORDER BY itemdesc"

        ElseIf printType = "DETAIL" Then
            Response.AddHeader("content-disposition", "inline;filename=mstitemdtl.xls")
            Response.Charset = ""
            'set the response mime type for excel
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT itemoid [Id Item], itemcode KOde, itemdesc Deskripsi,merk, QL_mstgen.gendesc as Group , g1.gendesc as Unit,QL_mstitem.createuser [Create User], QL_mstitem.Upduser [Update User], QL_mstitem.Updtime, itemflag, personname as PIC ,dimensi_p panjang,dimensi_l lebar,dimensi_t tinggi,beratvolume [Berat Volume],beratbarang [Berat barang] FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid = QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1 " & sWhere & " And itemoid = " & oid

        End If

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
        conn.Close()
        Response.End()
        mpePrint.Show()

    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If

        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("branch_id") = branch_id
            Session("branch") = branch
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            If Session("branch_code") = "" Then
                Response.Redirect("~\Other\login.aspx")
            End If

            Response.Redirect("mstdimensi.aspx?page=" & GVmstgen.PageIndex)
        End If

        Page.Title = CompnyName & " - Data Item"
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "javascript:return confirm('Anda yakin akan HAPUS data ini ?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update"
        Else
            i_u.Text = "New"
        End If

        If Not IsPostBack Then
            GVmstgen.PageIndex = Session("page")
            inituserID()
            BindData(CompnyCode)
            initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid")) : getUsername()
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update"
            Else
                GenerateGenID()
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                GVmstgen.PageIndex = ToDouble(Request.QueryString("page"))
                GVmstgen.DataBind()
                lblupd.Text = "Create"
            End If

        End If
    End Sub
    'BUat paging
    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
        Session("page") = GVmstgen.PageIndex
        'findAData()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        ' FilterDDL.SelectedIndex = 0
        Session("FilterText") = "" : Session("FilterDDL") = 0
        Session("FilterText2") = "" : Session("FilterDDL2") = 0
        Session("FilterText3") = "" : Session("FilterDDL3") = 0
        BindData(CompnyCode)
        'findAData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        GVmstgen.PageIndex = 0
        BindData(CompnyCode)
        'findAData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If itemcode.Text.Trim = "" Then errMessage &= "- Isi Kode Barang Dahulu ! <BR>"
        If itemdesc.Text.Trim = "" Then errMessage &= "- Isi Nama Barang Dahulu ! <BR>"
        If txt_panjang.Text.Trim = "" Then errMessage &= "- Dimensi Panjang Harus Diisi<br/>"
        If txt_lebar.Text.Trim = "" Then errMessage &= "- Dimensi Lebar Harus Diisi<br/>"
        If txt_tinggi.Text.Trim = "" Then errMessage &= "- Dimensi Tinggi Harus Diisi<br/>"
        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        'cek deskipsi msgen yang kembar 

        Session("ItemOid") = GenerateID("QL_mstitem", CompnyCode)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(GetServerTime(), "dd/MM/yyyy HH:mm:ss")

            If Session("oid") = Nothing Or Session("oid") = "" Then
                'generateItemId(itemdesc.Text)
                'Dim sn As Integer = 0
                'Dim branch_id As String = String.Empty

                'sSql = "INSERT INTO QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc],[itemgroupoid],[itemsubgroupoid],itemtype,merk,[itempriceunit1], [itempriceunit2],[satuan1],[itemsafetystockunit1],[acctgoid],[createuser],[upduser],[updtime],[itemflag], konversi1_2,personoid, keterangan, bottompricegrosir,itemsafetystockunitgrosir,pricelist,discunit1,discunit2,discunit3,qty3_to2,qty3_to1,statusitem,stockflag) Values ('" & CompnyCode & "'," & Session("ItemOid") & ",'" & Tchar(itemcode.Text) & "','" & Tchar(itemdesc.Text.ToUpper) & "'," & itemgroupoid.SelectedValue & "," & 0 & ",'" & dd_type.SelectedValue & "','" & Tchar(dd_merk.SelectedValue).ToUpper & "'," & 0 & "," & ToDouble(PriceNota.Text) & "," & ToDouble(PriceKhusus.Text) & "," & satuan2.SelectedValue & "," & ToDouble(MinimStock.Text) & "," & Tchar(acctgoid.SelectedValue) & ",'" & Session("UserID") & "',current_timestamp,'" & Tchar(ddlStatus.SelectedValue) & "'," & 1 & "," & spgOid.SelectedValue & ",'" & Tchar(keterangan.Text.ToUpper) & "'," & ToDouble(bottompriceGrosir.Text) & "," & ToDouble(MinimStock.Text) & "," & ToDouble(PriceList.Text) & "," & ToDouble(Disc1.Text) & "," & ToDouble(Disc2.Text) & "," & ToDouble(Disc3.Text) & "," & 1 & "," & 1 & ",'" & Tchar(DDLStatusItem.SelectedValue.ToUpper) & "','" & dd_stock.SelectedValue.ToUpper & "')"

                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                'Dim vhargExpedisi_Darat, vhargExpedisi_Laut, vhargExpedisi_Udara As Double
                'vhargExpedisi_Darat = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Darat")
                'vhargExpedisi_Laut = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Laut")
                'vhargExpedisi_Udara = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Udara")
                'sSql = "Update QL_mstoid set lastoid=" & Session("ItemOid") + 1 & " Where tablename='QL_mstitem' and cmpcode ='" & CompnyCode & "'"
                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                'Insert Ke Branch
                'Dim sqlstr As String = String.Empty
                'Dim rdr As SqlDataReader : Dim cmd As New SqlCommand
                'conn2.Open() : cmd.Connection = conn2
                'sqlstr = "SELECT gencode FROM ql_mstgen where gengroup = 'cabang' and cmpcode='" & CompnyName & "'"
                'cmd.CommandType = CommandType.Text
                'cmd.CommandText = sqlstr : rdr = cmd.ExecuteReader
                'If rdr.HasRows Then
                '    While rdr.Read
                '        Try
                '            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type) " & _
                '            "values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Darat & ",'" & rdr("gencode").ToString & "','Darat')"
                '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '        Catch
                '            Exit Try
                '        End Try
                '        Try
                '            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type) " & _
                '           "values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Laut & ",'" & rdr("gencode").ToString & "','Laut')"
                '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '        Catch
                '            Exit Try
                '        End Try
                '        Try
                '            sSql = "insert into QL_mstItem_branch(cmpcode,itemoid,itemcode,createuser,crttime,biayaExpedisi,branch_code,expedisi_type) " & _
                '           "values ('" & CompnyCode & "'," & Session("ItemOid") & ", '" & itemcode.Text & "', '" & Upduser.Text & "', current_timestamp ," & vhargExpedisi_Udara & ",'" & rdr("gencode").ToString & "','Udara')"
                '            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                '        Catch
                '            Exit Try
                '        End Try
                '    End While
                'End If
            Else
                sSql = "UPDATE " & DB_JPT_GROSIR & ".dbo.QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                       "[itemgroupoid]=" & itemgroupoid.SelectedValue & "," & _
                       "[itemtype]=" & dd_type.SelectedValue & "," & _
                       "merk='" & Tchar(dd_merk.SelectedValue) & "'," & _
                       "[satuan2]=" & satuan2.SelectedValue & "," & _
                       "[itemsafetystockunit1]=" & ToDouble(0) & ", " & _
                       "[statusitem]='" & DDLStatusItem.SelectedValue & "', " & _
                       " stockflag = '" & dd_stock.SelectedValue & "', " & _
                       "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp,  [itemflag]='" & ddlStatus.SelectedValue & "' ,keterangan='" & Tchar(keterangan.Text) & "',itemsafetystockunitgrosir = " & ToDouble(0) & ",maxstockgrosir =" & ToDouble(0) & "" & _
                       ", dimensi_p=" & ToDouble(txt_panjang.Text) & ", dimensi_l=" & ToDouble(txt_lebar.Text) & ", dimensi_t = " & ToDouble(txt_tinggi.Text) & ", beratbarang=" & ToDouble(beratBarang.Text) & ",beratvolume=" & ToDouble(beratVolume.Text) & "" & _
                       " WHERE cmpcode like '%" & CompnyCode & "%' and itemoid=" & Session("oid")
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Dim vhargExpedisi_Darat, vhargExpedisi_Laut, vhargExpedisi_Udara As Double
                vhargExpedisi_Darat = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Darat")
                vhargExpedisi_Laut = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Laut")
                vhargExpedisi_Udara = HargaExpedisi(txt_panjang.Text, txt_lebar.Text, txt_tinggi.Text, "Udara")

                'Update Ke Branch
                sSql = "update QL_mstItem_branch set biayaExpedisi= " & vhargExpedisi_Darat & " WHERE itemoid = " & Session("oid") & " and branch_code = '" & Session("branch_code") & "' and expedisi_type='Darat'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstItem_branch set biayaExpedisi= " & vhargExpedisi_Laut & " WHERE itemoid = " & Session("oid") & " and branch_code = '" & Session("branch_code") & "' and expedisi_type='Laut'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "update QL_mstItem_branch set biayaExpedisi= " & vhargExpedisi_Udara & " WHERE itemoid = " & Session("oid") & " and branch_code = '" & Session("branch_code") & "' and expedisi_type='Udara'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try
        Session("oid") = Nothing
        Image1.ImageUrl = "~/images/information.png"
        Response.Redirect("~\Master\mstdimensi.aspx")
    End Sub

    Protected Sub getBranch()
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As Double
        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT gencode FROM ql_mstgen where gengroup = 'cabang' and cmpcode='" & CompnyName & "'"
        cmd.CommandType = CommandType.Text

        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read

            End While
        Else
            refcode = 0
        End If

        conn2.Close()
    End Sub

    Protected Sub getUsername()
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT ISNULL(PERSONOID,0) PERSONOID FROM QL_MSTPROF prof INNER JOIN QL_MSTPERSON per ON prof.USERNAME = per.PERSONNAME WHERE PERSONNAME = '" & Session("UserId") & "'"
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read
                UserName.Text = rdr("PERSONOID").ToString
            End While
        Else
            UserName.Text = 0
        End If

        conn2.Close()
    End Sub

    Function HargaExpedisi(ByVal p As Decimal, ByVal l As Decimal, ByVal t As Decimal, ByVal jalur As String) As Double
        Dim sqlstr As String = String.Empty
        Dim rdr As SqlDataReader
        Dim cmd As New SqlCommand
        Dim refcode As Double

        conn2.Open()
        cmd.Connection = conn2
        sqlstr = "SELECT price FROM mstexpedisibranch where panjang = " & p & " and lebar =" & l & " and tinggi=" & t & " and branchcode = '" & Session("branch_code") & "' and expedisi_type='" & jalur & "'"
        cmd.CommandType = CommandType.Text
        'cmd.Parameters.AddWithValue("@gencode", dd_branch.SelectedValue)
        cmd.CommandText = sqlstr
        rdr = cmd.ExecuteReader
        If rdr.HasRows Then
            While rdr.Read
                refcode = rdr("price") / (p * l * t)
            End While
        Else
            refcode = 0
        End If

        conn2.Close()
        Return refcode
    End Function

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If itemoid.Text = "" Then
            showMessage("Silahkan Pilih Data Barang !!", CompnyName & " - WARNING")
            Exit Sub
        End If

        Dim sColomnName() As String = {"refoid", "itemoid", "itemoid"}
        Dim sTable() As String = {"ql_crdmtr", "QL_podtl", "QL_trnorderdtl"}
        If CheckDataExists(Session("oid"), sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from dbo.ql_crdmtr where refoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from QL_podtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from QL_trnorderdtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain!", CompnyName & " - Warning")
            Exit Sub
        End If

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "delete from QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            sSql = "delete from QL_MSTITEM_branch WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR") : Exit Sub
        End Try

        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah terhapus !", "INFORMASI")
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "SUMMARY"
        imbPrintPDF.Visible = True
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoItem") = "" : lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)

    End Sub

    Protected Sub itemdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemdesc.TextChanged
        itemdesc.Text = itemdesc.Text.Replace("'", "")
        bindDataMaterial()
        'If (Session("oid") <> Nothing And Session("oid") <> "") = False Then
        '    generateItemId(itemdesc.Text)
        'End If
    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim namaItem, jumlahNol, c, c2, kodeItem As String
        jumlahNol = "" : Dim d As Integer = 0
        namaItem = Left(itemdesc.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT isnull(max((replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM " & DB_JPT_GROSIR & ".dbo.ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))
        c2 = ToDouble(GetStrData("SELECT isnull(max((replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM " & DB_JPT_RITEL & ".dbo.ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If

        kodeItem = GenNumberString(namaItem, "", d, 5)
        itemcode.Text = kodeItem
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = NewMaskEdit(ToDouble(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
            e.Row.Cells(6).Text = NewMaskEdit(ToDouble(e.Row.Cells(6).Text))
            e.Row.Cells(7).Text = NewMaskEdit(ToDouble(e.Row.Cells(7).Text))
        End If
    End Sub

    Protected Sub btnprintlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "DETAIL"
        imbPrintPDF.Visible = False
        orderNoForReport.Text = sender.ToolTip : orderIDForReport.Text = sender.CommandArgument()
        Session("NoItem") = sender.ToolTip
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = "" : printType.Text = "PDF"
        Try
            PrintReport("", "Barang_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstdimensi.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()

        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstdimensi.aspx?awal=true")
        End If
    End Sub

    Protected Sub itemgroupoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemgroupoid.SelectedIndexChanged
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and (genother1='' or genother1='" & itemgroupoid.SelectedValue & "') and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrintExcel.Click
        lblError.Text = "" : printType.Text = "EXCEL"
        Try
            PrintReport("", "Barang_PrintOut", ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptSupp.Close() : rptSupp.Dispose()
    End Sub

    Protected Sub txt_panjang_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_panjang.TextChanged, txt_tinggi.TextChanged, txt_lebar.TextChanged
        beratVolume.Text = ToMaskEdit((CDbl(txt_panjang.Text * txt_lebar.Text * txt_tinggi.Text)), 3)
    End Sub
#End Region

    Protected Sub btnSearchItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchItem.Click
        bindDataMaterial()
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        gvItem.Visible = True
        bindDataMaterial()
    End Sub

    Protected Sub gvItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItem.SelectedIndexChanged
        itemdesc.Text = gvItem.SelectedDataKey.Item("itemdesc")
        itemoid.Text = gvItem.SelectedDataKey.Item("itemoid")
        itemcode.Text = gvItem.SelectedDataKey.Item("itemcode")
        itemgroupoid.Text = gvItem.SelectedDataKey.Item("itemgroupoid")
        If gvItem.SelectedDataKey.Item("itemtype") = 0 Then
            showMessage("Maaf, " & itemdesc.Text & " belum punya identitas type barang, Silahkan hubungi PIC barang untuk menambahkan type barang", CompnyName & " - INFORMASI")
            Exit Sub
        Else
            dd_type.SelectedValue = gvItem.SelectedDataKey.Item("itemtype")
        End If
        '
        If gvItem.SelectedDataKey.Item("merk") = "" Then
            showMessage("Maaf, " & itemdesc.Text & " belum punya identitas Merk barang, Silahkan hubungi PIC barang untuk menambahkan Merk barang", CompnyName & " - INFORMASI")
            Exit Sub
        Else
            dd_merk.SelectedValue = gvItem.SelectedDataKey.Item("merk")
        End If
        satuan2.SelectedValue = gvItem.SelectedDataKey.Item("unitoid")
        dd_stock.SelectedValue = gvItem.SelectedDataKey.Item("stockflag")
        DDLStatusItem.SelectedValue = gvItem.SelectedDataKey.Item("statusitem")
        ddlStatus.SelectedItem.Text = gvItem.SelectedDataKey.Item("itemflag")
        keterangan.Text = gvItem.SelectedDataKey.Item("keterangan")
        txt_panjang.Text = gvItem.SelectedDataKey.Item("dimensi_p")
        txt_tinggi.Text = gvItem.SelectedDataKey.Item("dimensi_t")
        txt_lebar.Text = gvItem.SelectedDataKey.Item("dimensi_l")
        beratBarang.Text = gvItem.SelectedDataKey.Item("beratbarang")
        beratVolume.Text = gvItem.SelectedDataKey.Item("beratvolume ")
        gvItem.DataSource = Nothing
        gvItem.DataBind() : gvItem.Visible = False
    End Sub

    Protected Sub btnDelItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelItem.Click
        itemdesc.Text = "" : itemcode.Text = ""
    End Sub
End Class