<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false"
    CodeFile="trnNotaBeli.aspx.vb" Inherits="trnNotaBeli" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
 
    <table id="tableutama" align="center" border="1" cellpadding="5" cellspacing="0"
        width="100%" onclick="return tableutama_onclick()" class="tabelhias">
        <tr>
            <th class="header" colspan="3" valign="top" align="left">
                <asp:Label ID="Label111" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Purchase Invoice"></asp:Label></Th>                    
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
<asp:Panel id="Panel2" runat="server" DefaultButton="btnSearch" __designer:wfdid="w29"><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; WIDTH: 100px" align=left>Cabang</TD><TD align=left colSpan=2><asp:DropDownList id="dCabangNya" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w30" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 100px" align=left>User PO</TD><TD align=left colSpan=2><asp:DropDownList id="FilterDDLUserPO" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w60"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 100px" align=left>Filter :</TD><TD align=left colSpan=2><asp:DropDownList id="ddlFilter" runat="server" CssClass="inpText" __designer:wfdid="w31"><asp:ListItem Value="trnbelino">Invoice No</asp:ListItem>
<asp:ListItem Value="po.trnbelipono">PO No</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilter" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w32" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 100px" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Period" __designer:wfdid="w33" AutoPostBack="False"></asp:CheckBox></TD><TD align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w34" AutoPostBack="True"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w35"></asp:ImageButton>&nbsp;<asp:Label id="Label169" runat="server" Text="to" __designer:wfdid="w36"></asp:Label>&nbsp;<asp:TextBox id="FilterPeriod2" runat="server" Width="75px" CssClass="inpText" __designer:wfdid="w37"></asp:TextBox>&nbsp;<asp:ImageButton id="imbPeriod2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w38"></asp:ImageButton>&nbsp;<asp:Label id="Label179" runat="server" CssClass="Important" Text="(dd/MM/yyyy)" __designer:wfdid="w39"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" class="Label" align=left><asp:CheckBox id="chkStatus" runat="server" Text="Status" __designer:wfdid="w40"></asp:CheckBox></TD><TD align=left colSpan=2><asp:DropDownList id="DDLStatus" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w41" AutoPostBack="True"><asp:ListItem Value="All">All</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle" __designer:wfdid="w42"></asp:ImageButton> <asp:ImageButton id="btnList" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w43"></asp:ImageButton> <asp:ImageButton id="imbLastSearch" onclick="imbLastSearch_Click" runat="server" ImageUrl="~/Images/viewlast.png" ImageAlign="AbsMiddle" __designer:wfdid="w44" Visible="False"></asp:ImageButton>&nbsp; </TD></TR><TR><TD align=left colSpan=3><asp:GridView id="tbldata" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w51" OnPageIndexChanging="tbldata_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelino,trntaxtype" GridLines="None" OnSelectedIndexChanged="tbldata_SelectedIndexChanged" PageSize="7">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="trnbelimstoid" DataNavigateUrlFormatString="~\transaction\trnNotabeli.aspx?oid={0}" DataTextField="trnbelino" HeaderText="Invoice No">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidate" HeaderText="Invoice Date">
<HeaderStyle HorizontalAlign="Center" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="supplier" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="User PO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelistatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:CommandField SelectImageUrl="~/Images/print.gif" ShowSelectButton="True" ButtonType="Image">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="lblmsg" runat="server" __designer:wfdid="w57" Font-Size="X-Small" 
        ForeColor="Red" Text="No data found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=left colSpan=3><asp:Label id="Company" runat="server" CssClass="Important" __designer:wfdid="w53" Visible="False"></asp:Label><asp:Label id="InvoiceID" runat="server" CssClass="Important" __designer:wfdid="w54"></asp:Label><ajaxToolkit:CalendarExtender id="cePer2" runat="server" __designer:wfdid="w55" TargetControlID="FilterPeriod2" Format="dd/MM/yyyy" PopupButtonID="imbPeriod2"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="cePer1" runat="server" __designer:wfdid="w56" TargetControlID="FilterPeriod1" Format="dd/MM/yyyy" PopupButtonID="imbPeriod1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meePer1" runat="server" __designer:wfdid="w57" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="meePer2" runat="server" __designer:wfdid="w58" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel> 
</ContentTemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="tbldata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong> <span style="font-size: 9pt">
                                List Of Purchase
                            Invoice</span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel7" runat="server" RenderMode="Inline">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label1" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Information" __designer:wfdid="w107"></asp:Label></TD><TD align=left><asp:Label id="trnbelimstoid" runat="server" __designer:wfdid="w44" Visible="False"></asp:Label><asp:Label id="dbpino" runat="server" __designer:wfdid="w45" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="periodacctg" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w46" Visible="False"></asp:Label></TD><TD style="COLOR: #000099" align=left><asp:Label id="i_u" runat="server" Font-Size="X-Small" ForeColor="Red" Text="N E W" __designer:wfdid="w42" Visible="False"></asp:Label><asp:Label id="mCabang" runat="server" __designer:wfdid="w61" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:DropDownList id="typeTax" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w48" Visible="False" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="prefixoid" runat="server" __designer:wfdid="w60" Visible="False"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Type PO</TD><TD align=left><asp:DropDownList id="TypePOnya" runat="server" CssClass="inpText" __designer:wfdid="w2"><asp:ListItem>Normal</asp:ListItem>
<asp:ListItem Value="Selisih">Selisih Stok</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Cabang</TD><TD style="COLOR: #000099" align=left><asp:DropDownList id="CabangNya" runat="server" CssClass="inpText" __designer:wfdid="w1"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" id="TD5" align=left runat="server" Visible="false">Identifer No</TD><TD id="TD6" align=left runat="server" Visible="false"><asp:TextBox id="identifierno" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w63" MaxLength="30" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice No</TD><TD align=left><asp:TextBox id="trnbelino" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w50" MaxLength="30" ReadOnly="True"></asp:TextBox> <asp:Label id="oid" runat="server" Font-Size="X-Small" __designer:wfdid="w43" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>Invoice Date <asp:Label id="Label19" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w51"></asp:Label></TD><TD align=left><asp:TextBox style="TEXT-ALIGN: justify" id="trnbelidate" runat="server" Width="75px" CssClass="inpTextDisabled" __designer:wfdid="w52" AutoPostBack="True" MaxLength="10" OnTextChanged="trnbelidate_TextChanged" Enabled="False"></asp:TextBox> <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w53" Visible="False"></asp:ImageButton> <asp:Label id="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w54"></asp:Label></TD><TD style="FONT-SIZE: x-small; COLOR: #000099" align=left>PO Date</TD><TD align=left><asp:TextBox id="podate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w55" MaxLength="30" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>PO No. <asp:Label id="Label4" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w56"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelipono" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w57" MaxLength="35"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchPO" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w58"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearPO" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w59"></asp:ImageButton>&nbsp;&nbsp;</TD><TD style="FONT-SIZE: x-small" align=left>Supplier</TD><TD align=left><asp:TextBox id="suppliername" runat="server" Width="175px" CssClass="inpTextDisabled" __designer:wfdid="w62" MaxLength="100" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" id="Td1" align=left visible="false">Promo Date</TD><TD id="Td2" align=left visible="false"><asp:TextBox id="TglPromo" runat="server" Width="65px" CssClass="inpText" __designer:wfdid="w229"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnTglPromo" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w232"></asp:ImageButton>&nbsp;<asp:Label id="Label11" runat="server" Font-Size="X-Small" ForeColor="Red" Text="(dd/MM/yyyy)" __designer:wfdid="w4"></asp:Label></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="gvListPO" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w64" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelimstoid,trnbelipono,trnbelipodate,trnsuppoid,suppname,curroid,currate,trnbelidisctype,trnbelidisc,digit,trnpaytype,prefixoid,branch_code" GridLines="None" OnSelectedIndexChanged="gvListPO_SelectedIndexChanged" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelimstoid" HeaderText="trnbelimstoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipono" HeaderText="PO No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelipodate" HeaderText="PO Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="upduser" HeaderText="Create User">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsuppoid" HeaderText="trnsuppoid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton ID="lkbDetail" runat="server" __designer:wfdid="w6" 
            onclick="lkbDetail_Click" ToolTip='<%# eval("trnbelipono") %>'>Detail</asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD vAlign=top align=left colSpan=3><asp:GridView id="ItemGv" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w65" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="NoSJ" HeaderText="No. PDO">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemdesc" HeaderText="Deskripsi" SortExpression="itemdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity" SortExpression="qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label ID="Label8" runat="server" Font-Size="X-Small" ForeColor="Red" 
        Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Payment Term</TD><TD align=left><asp:DropDownList id="TRNPAYTYPE" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w66" Enabled="False" AppendDataBoundItems="True"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left>Currency</TD><TD align=left><asp:DropDownList id="CurrencyOid" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w67" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD style="FONT-SIZE: x-small" id="TD4" align=left runat="server" Visible="false">Currency Rate</TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox id="currencyRate" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w68" AutoPostBack="True" ReadOnly="True" OnTextChanged="currencyRate_TextChanged">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice Note</TD><TD align=left><asp:TextBox id="trnbelinote" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w69" MaxLength="30"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Status</TD><TD align=left><asp:TextBox id="posting" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w70" ReadOnly="True">In Process</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:Label id="Label25" runat="server" Text="Ref. No" __designer:wfdid="w71" Visible="False"></asp:Label>&nbsp;<asp:TextBox id="trnbeliref" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w72" Visible="False" MaxLength="30"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Purchase Amount</TD><TD align=left><asp:TextBox id="amtbeli" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w73" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Total Detail Disc.</TD><TD align=left><asp:TextBox id="amtdiscdtl" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w74" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Net. Detail Amount</TD><TD align=left><asp:TextBox id="netdetailamt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w75" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Disc. Header Type</TD><TD style="HEIGHT: 21px" align=left><asp:DropDownList id="trnbelidisctype" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w76" AutoPostBack="True"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left><asp:Label id="lbl1" runat="server" Font-Size="X-Small" Text="Disc. Header (Rp.)" __designer:wfdid="w77"></asp:Label></TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="trnbelidisc" runat="server" Width="125px" CssClass="inpText" __designer:wfdid="w78" AutoPostBack="True" MaxLength="12">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; HEIGHT: 21px" align=left>Disc. Header Amount</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="amtdischdr" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w79" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Tax Type</TD><TD align=left><asp:DropDownList id="Tax" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w80" Enabled="False" OnSelectedIndexChanged="Tax_SelectedIndexChanged"><asp:ListItem Value="1">NON TAX</asp:ListItem>
<asp:ListItem Value="0">TAX</asp:ListItem>
</asp:DropDownList></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label33" runat="server" Text="Voucher Amount" __designer:wfdid="w81"></asp:Label></TD><TD align=left><asp:TextBox id="voucherAmt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w82" AutoPostBack="True" MaxLength="4">0</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="TaxAmmount" runat="server" Text="Tax Amount" __designer:wfdid="w83"></asp:Label></TD><TD align=left><asp:TextBox id="amttax" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w84" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left>Invoice + (Tax)</TD><TD align=left><asp:TextBox id="amtbelinetto1" runat="server" Width="130px" CssClass="inpTextDisabled" __designer:wfdid="w85" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label34" runat="server" Text="Ekpedisi" __designer:wfdid="w86"></asp:Label></TD><TD align=left><asp:TextBox id="ekspedisiAmt" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w87" ReadOnly="True" Enabled="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left>Total Invoice</TD><TD align=left><asp:TextBox id="totalinvoice" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w88" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label32" runat="server" Text="Tax Number" __designer:wfdid="w89" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="nofaktur_pajak" runat="server" Width="180px" CssClass="inpText" __designer:wfdid="w90" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="PercentageTax" runat="server" Text="Tax (%)" __designer:wfdid="w91" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="trntaxpct" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w92" Visible="False" AutoPostBack="True" MaxLength="4">0.00</asp:TextBox> <asp:Label id="temptotalamount" runat="server" Text="0" __designer:wfdid="w93" Visible="False"></asp:Label> <asp:Label id="tempdisc" runat="server" Text="0" __designer:wfdid="w94" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblacctg" runat="server" Font-Size="X-Small" Text="For Purchasing :" __designer:wfdid="w95" Visible="False"></asp:Label></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label22" runat="server" Text="Total Cost" __designer:wfdid="w96" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="totalamtbiaya" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w97" Visible="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblnofakturpajak" runat="server" Font-Size="X-Small" Text="No Faktur Pajak" __designer:wfdid="w98" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="txtnofakturpajak" runat="server" Width="130px" CssClass="inpText" __designer:wfdid="w99" Visible="False" MaxLength="20"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:ImageButton id="postingfaktur" onclick="postingfaktur_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:dtid="562949953421349" __designer:wfdid="w100" Visible="False"></asp:ImageButton></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD align=left><asp:Label id="belibiayamstoid" runat="server" __designer:wfdid="w101" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="TypeTaxlabel" runat="server" __designer:wfdid="w103" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="trnbelitype" runat="server" Font-Size="X-Small" __designer:wfdid="w104" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="trnsuppoid" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w105" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left>&nbsp;&nbsp;&nbsp; </TD></TR></TBODY></TABLE><TABLE width="100%"><TBODY><TR><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left><asp:Label id="Label2" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="Black" Text="Detail :" __designer:wfdid="w107"></asp:Label> </TD><TD style="WIDTH: 233px" align=left><asp:Label id="iRow" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" __designer:wfdid="w109" Visible="False"></asp:Label> <asp:Label id="trnbelidtlunit" runat="server" Font-Size="X-Small" Font-Bold="False" __designer:wfdid="w108"></asp:Label> <asp:Label id="noSj" runat="server" Font-Size="X-Small" ForeColor="Red" __designer:wfdid="w36" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; FONT-FAMILY: Verdana" align=left><asp:Label id="to_branch_code" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" __designer:wfdid="w112" Visible="False"></asp:Label></TD><TD style="WIDTH: 190px; FONT-FAMILY: Verdana" align=left><asp:TextBox id="txtamount" runat="server" CssClass="inpText" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w110" Visible="False" MaxLength="30" ReadOnly="True"></asp:TextBox></TD><TD style="FONT-SIZE: x-small; FONT-FAMILY: Verdana" align=left><asp:TextBox id="Location" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w113" Visible="False" ReadOnly="True"></asp:TextBox>&nbsp; </TD><TD style="FONT-FAMILY: Verdana" align=left><asp:Label id="sjoid" runat="server" Font-Size="8pt" Font-Names="Arial,Helvetica,sans-serif" Font-Bold="True" __designer:wfdid="w111" Visible="False"></asp:Label></TD><TD style="FONT-SIZE: x-small; WIDTH: 112px; FONT-FAMILY: Verdana" align=left></TD><TD style="WIDTH: 192px; FONT-FAMILY: Verdana" align=left></TD></TR><TR style="FONT-FAMILY: Verdana"><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left><asp:Label id="Label10" runat="server" Width="53px" Text="No. PDO" __designer:wfdid="w118"></asp:Label><asp:Label id="Label5" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w114"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="noteSJ" runat="server" Width="150px" CssClass="inpText" __designer:wfdid="w153" MaxLength="20"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchSJ" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w154"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSj" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w155"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left>&nbsp;</TD><TD style="FONT-SIZE: x-small; WIDTH: 112px" align=left>&nbsp;</TD><TD style="WIDTH: 192px" align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=4><asp:GridView id="gvListSJ" runat="server" Width="100%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w22" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnsjbelioid,trnsjbelino,trnsjbelinote,to_branch_code" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnsjbelioid" HeaderText="trnsjbelioid" Visible="False">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelino" HeaderText="No. PDO" SortExpression="trnsjbelino">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="125px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="125px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjreceivedate" HeaderText="Recvd. Date" SortExpression="trnsjreceivedate">
<HeaderStyle Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelinote" HeaderText="Note" SortExpression="trnsjbelinote">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" Width="150px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lkbDetailSJ" onclick="lkbDetailSJ_Click" runat="server" __designer:wfdid="w7" ToolTip='<%# eval("trnsjbelioid") %>'>Detail</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD><TD vAlign=top align=left colSpan=4><asp:GridView id="gvListSJDetail" runat="server" Width="80%" Font-Size="X-Small" ForeColor="#333333" __designer:wfdid="w23" Visible="False" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="5">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="trnsjbelidtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="matlongdesc" HeaderText="Katalog" SortExpression="matlongdesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="250px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="qty" HeaderText="Quantity" SortExpression="qty">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="100px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Unit" SortExpression="gendesc">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label8" runat="server" ForeColor="Red" Font-Size="X-Small" Text="No data in database!!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left><asp:Label id="lblmtrref" runat="server" Text="Katalog" __designer:wfdid="w118"></asp:Label></TD><TD style="WIDTH: 233px" align=left><asp:TextBox id="Material" runat="server" Width="150px" CssClass="inpTextDisabled" __designer:wfdid="w119" MaxLength="100" ReadOnly="True"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w120" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnClearSupp" runat="server" Width="16px" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w17" Visible="False"></asp:ImageButton></TD><TD style="FONT-SIZE: x-small" align=left>Quantity</TD><TD style="WIDTH: 190px" align=left><asp:TextBox id="trnbelidtlqty" runat="server" Width="79px" CssClass="inpTextDisabled" __designer:wfdid="w18" AutoPostBack="True" MaxLength="10" ReadOnly="True">0.00</asp:TextBox> <asp:Label id="unit" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w19"></asp:Label></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label9" runat="server" Width="33px" Font-Size="X-Small" Text="Price" __designer:wfdid="w26"></asp:Label> <asp:Label id="Label23" runat="server" Width="1px" Font-Size="X-Small" ForeColor="Red" Text="*" __designer:wfdid="w149"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelidtlprice" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w150" AutoPostBack="True" MaxLength="30" Enabled="False" ValidationGroup="MKE">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; WIDTH: 112px" align=left></TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="amtdisc2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w33" Visible="False" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left>Total Amt</TD><TD style="WIDTH: 233px" align=left><asp:TextBox id="amtbrutto" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w24" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label6" runat="server" Width="77px" Font-Size="X-Small" Text="Disc. Type 1" __designer:wfdid="w26"></asp:Label></TD><TD style="WIDTH: 190px" align=left><asp:DropDownList id="trnbelidtldisctype" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w25" AutoPostBack="True" Enabled="False"><asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Label id="lbl2" runat="server" Width="94px" Font-Size="X-Small" Text="Discount (Rp) 1" __designer:wfdid="w26"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelidtldisc" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w27" AutoPostBack="True" MaxLength="12" Enabled="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; WIDTH: 112px" align=left><asp:Label id="Label3" runat="server" Width="94px" Font-Size="X-Small" Text="Disc. Amt (Rp) 1" __designer:wfdid="w26"></asp:Label></TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="amtdisc" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w28" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left>Total Disc</TD><TD style="WIDTH: 233px" align=left><asp:TextBox id="totdiscdtl" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w29" ReadOnly="True">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label7" runat="server" Width="75px" Font-Size="X-Small" Text="Disc. Type 2" __designer:wfdid="w26"></asp:Label></TD><TD style="WIDTH: 190px" align=left><asp:DropDownList id="trnbelidtldisctype2" runat="server" Width="105px" CssClass="inpTextDisabled" __designer:wfdid="w30" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="trnbelidtldisctype2_SelectedIndexChanged"><asp:ListItem Value="VCR">VOUCHER</asp:ListItem>
<asp:ListItem Value="AMT">AMOUNT</asp:ListItem>
<asp:ListItem Value="PCT">PERCENTAGE</asp:ListItem>
</asp:DropDownList></TD><TD align=left><asp:Label id="lbl3" runat="server" Width="94px" Font-Size="X-Small" Text="Discount (Rp) 2" __designer:wfdid="w26"></asp:Label></TD><TD align=left><asp:TextBox id="trnbelidtldisc2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w32" AutoPostBack="True" MaxLength="20" OnTextChanged="trnbelidtldisc2_TextChanged" Enabled="False">0.00</asp:TextBox></TD><TD style="FONT-SIZE: x-small; WIDTH: 112px" align=left>Nett. Amt</TD><TD style="WIDTH: 192px" align=left><asp:TextBox id="amtbelinetto2" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w157" MaxLength="30" ReadOnly="True">0.00</asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 101px" align=left>Note</TD><TD style="WIDTH: 233px" align=left><asp:TextBox id="trnbelidtlnote" runat="server" Width="190px" CssClass="inpText" __designer:wfdid="w35" MaxLength="30"></asp:TextBox></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD style="WIDTH: 190px" align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD><TD style="FONT-SIZE: x-small" align=left colSpan=2><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w37" Visible="False" AlternateText="Update"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w38" Visible="False" AlternateText="Clear"></asp:ImageButton></TD></TR><TR><TD style="FONT-SIZE: x-small" align=center colSpan=8><DIV style="OVERFLOW-Y: scroll; WIDTH: 1000px; HEIGHT: 200px; BACKGROUND-COLOR: beige"><asp:GridView id="tbldtl2" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w39" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="trnbelidtlseq" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle Font-Size="X-Small" ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelidtlseq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjno" HeaderText="No. PDO">
<FooterStyle Width="75px"></FooterStyle>

<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="material" HeaderText="Katalog">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="250px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidtlqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="unit" HeaderText="Unit">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelidtlprice" HeaderText="Price">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc2" HeaderText="Voucher">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtdisc" HeaderText="Discount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbelinetto" HeaderText="Total Amt.">
<HeaderStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnsjbelinote" HeaderText="No. SJ">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:CommandField ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="sjrefoid" Visible="False">
<HeaderStyle ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="sjoid" Visible="False">
<HeaderStyle ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="to_branch_code" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label24" runat="server" CssClass="Important" Text="No detail Invoice !!" __designer:wfdid="w10"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=8><asp:Label id="lblAmtjualnetto" runat="server" Font-Size="Small" Font-Bold="True" Text="Total Invoice Detail = " __designer:wfdid="w2"></asp:Label> <asp:Label id="txtAmountNetto" runat="server" Font-Size="Small" Font-Bold="True" Text="0.00" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=8>Last Update By <asp:Label id="Upduser" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w4"></asp:Label>&nbsp;On <asp:Label id="Updtime" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="#585858" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left colSpan=8><asp:ImageButton id="btnSaveMstr" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsMiddle" __designer:wfdid="w121"></asp:ImageButton> <asp:ImageButton id="btnCancelMstr" onclick="btnCancelMstr_Click" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsMiddle" __designer:wfdid="w122"></asp:ImageButton> <asp:ImageButton id="btnDeleteMstr" onclick="btnDeleteMstr_Click" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsMiddle" __designer:wfdid="w123"></asp:ImageButton> <asp:ImageButton id="BtnPosting2" onclick="BtnPosting2_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w124" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:wfdid="w125" Visible="False"></asp:ImageButton> <asp:ImageButton id="Export" onclick="Export_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsBottom" __designer:dtid="1407374883553333" __designer:wfdid="w1" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsBottom" Height="21px" __designer:wfdid="w102" Visible="False"></asp:ImageButton></TD></TR><TR><TD align=center colSpan=8><asp:UpdateProgress id="uProgReportForm" runat="server" __designer:wfdid="w6" AssociatedUpdatePanelID="UpdatePanel7"><ProgressTemplate>
<DIV id="Div2" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><SPAN style="FONT-SIZE: 14pt">LOADING....</SPAN><BR /><asp:Image id="Image4" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsBottom" __designer:wfdid="w7"></asp:Image></SPAN></DIV>
</ProgressTemplate>
</asp:UpdateProgress><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w225" TargetControlID="trnbelidate" Format="dd/MM/yyyy" PopupButtonID="ImageButton1"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MEE1" runat="server" __designer:wfdid="w226" TargetControlID="currencyRate" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" __designer:wfdid="w227" TargetControlID="trnbelidate" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="True" UserDateFormat="MonthDayYear" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee50" runat="server" __designer:wfdid="w228" TargetControlID="trnbelidisc" InputDirection="RightToLeft" Mask="999,999,999.99" MaskType="Number" ErrorTooltipEnabled="True" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CEETglPromo" runat="server" __designer:wfdid="w230" TargetControlID="TglPromo" Format="dd/MM/yyyy" PopupButtonID="BtnTglPromo"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEETglPromo" runat="server" __designer:wfdid="w231" TargetControlID="TglPromo" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <asp:UpdatePanel id="upPreview" runat="server" __designer:dtid="562949953421348" __designer:wfdid="w24"><ContentTemplate __designer:dtid="562949953421349">
<asp:Panel id="pnlPreview" runat="server" Width="750px" CssClass="modalBox" __designer:wfdid="w8" Visible="False"><TABLE width="100%" align=center><TBODY><TR><TD align=center><asp:Label id="lblCaptPreview" runat="server" Font-Size="Medium" Font-Bold="True" Text="Preview Jurnal" __designer:wfdid="w9"></asp:Label></TD></TR><TR><TD><asp:Panel id="pnlScrollPreview" runat="server" Width="100%" Height="200px" __designer:wfdid="w10" ScrollBars="Vertical"><asp:GridView id="gvPreview" runat="server" Width="97%" ForeColor="#333333" __designer:wfdid="w11" GridLines="None" CellPadding="4" AutoGenerateColumns="False" EmptyDataRowStyle-ForeColor="Red" AllowSorting="True">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Center" 
        Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="desc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="credit" HeaderText="Credit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>
<EmptyDataTemplate>
                                                    <asp:Label ID="Label1" 
        runat="server" ForeColor="Red" Text="No Data Found !!"></asp:Label>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR><TR><TD align=center><asp:LinkButton id="lkbPostPreview" onclick="lkbPostPreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w12">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbClosePreview" onclick="lkbClosePreview_Click" runat="server" Font-Bold="True" __designer:wfdid="w13">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="beHidePreview" runat="server" __designer:wfdid="w14" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpePreview" runat="server" __designer:wfdid="w15" TargetControlID="beHidePreview" PopupDragHandleControlID="lblCaptPreview" PopupControlID="pnlPreview" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel8" runat="server" __designer:dtid="844424930131998" __designer:wfdid="w103"><ContentTemplate __designer:dtid="844424930131999">
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" __designer:wfdid="w16" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TBODY><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting" __designer:wfdid="w17"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" ForeColor="#333333" __designer:wfdid="w18" GridLines="None" CellPadding="4" AutoGenerateColumns="False" OnRowDataBound="gvakun_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Akun">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle CssClass="gvhdr" ForeColor="Black" HorizontalAlign="Right"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 18px" align=center colSpan=3><asp:LinkButton id="lkbCancel2" onclick="lkbCancel2_Click" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w19">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" __designer:wfdid="w20" TargetControlID="btnHidePosting2" PopupDragHandleControlID="lblPosting2" PopupControlID="pnlPosting2" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" __designer:wfdid="w21" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel> <asp:UpdatePanel id="UpdatePanel5" runat="server" __designer:wfdid="w159"><ContentTemplate>
<asp:Panel id="pnlPosting" runat="server" Width="400px" CssClass="modalBox" __designer:wfdid="w22" Visible="False" BorderStyle="Solid" BorderWidth="2px"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2><asp:Label id="lblPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="Invoice Posting" __designer:wfdid="w23"></asp:Label></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpaymentype" runat="server" Text="Payment Type" __designer:wfdid="w24"></asp:Label></TD><TD align=left><asp:DropDownList id="paymentype" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w25" AutoPostBack="True" OnSelectedIndexChanged="paymentype_SelectedIndexChanged"><asp:ListItem>CASH</asp:ListItem>
<asp:ListItem>NONCASH</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="Label26" runat="server" Width="90px" Text="Reference No" __designer:wfdid="w26" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="referensino" runat="server" CssClass="inpText" __designer:wfdid="w27" Visible="False"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblcashbankacctg" runat="server" Text="Cash Account" __designer:wfdid="w28"></asp:Label></TD><TD align=left><asp:DropDownList id="cashbankacctg" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w29"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblapaccount" runat="server" Text="AP Account" __designer:wfdid="w30" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="apaccount" runat="server" Width="275px" CssClass="inpText" __designer:wfdid="w31" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small" align=left><asp:Label id="lblpurchasingaccount" runat="server" Text="Purchasing Account" __designer:wfdid="w32"></asp:Label></TD><TD align=left><asp:DropDownList id="purchasingaccount" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w33"></asp:DropDownList></TD></TR><TR><TD style="FONT-SIZE: x-small; HEIGHT: 15px" align=left><asp:Label id="Label21" runat="server" Text="Cash Account For Cost" __designer:wfdid="w34" Visible="False"></asp:Label></TD><TD style="HEIGHT: 15px" align=left><asp:DropDownList id="CashForCost" runat="server" Width="250px" CssClass="inpText" __designer:wfdid="w35" Visible="False"></asp:DropDownList></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=2>&nbsp;<asp:LinkButton id="lkbPosting" onclick="lkbPosting_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w36">[ POST ]</asp:LinkButton> <asp:LinkButton id="lkbCancel" onclick="lkbCancel_Click" runat="server" Font-Size="X-Small" __designer:wfdid="w37">[ CANCEL ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpePosting" runat="server" __designer:wfdid="w38" TargetControlID="btnHidePosting" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPosting" PopupDragHandleControlID="lblPosting"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting" runat="server" __designer:wfdid="w39" Visible="False"></asp:Button> 
</ContentTemplate>
</asp:UpdatePanel></TD></TR></TBODY></TABLE>
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="Export"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong><span style="font-size: 9pt">
                                Form Purchase
                            Invoice</span></strong> <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center">
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" CssClass="modalMsgBox" __designer:wfdid="w7" Visible="False" DefaultButton="btnMsgBoxOK"><TABLE cellSpacing=1 cellPadding=1 border=0><TBODY><TR><TD style="COLOR: white; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" vAlign=top align=left colSpan=2><asp:Label id="lblCaption" runat="server" Font-Size="Small" Font-Bold="True" __designer:wfdid="w8"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2></TD></TR><TR><TD vAlign=top align=left><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w9"></asp:Image> </TD><TD class="Label" vAlign=middle align=left><asp:Label id="lblMessage" runat="server" ForeColor="Red" __designer:wfdid="w10"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" __designer:wfdid="w11" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnErrOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom" __designer:wfdid="w12"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" __designer:wfdid="w13" TargetControlID="beMsgBox" Drag="True" PopupControlID="PanelMsgBox" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender> <asp:Button id="beMsgBox" runat="server" CausesValidation="False" __designer:wfdid="w14" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
        
</asp:Content>
