''Prgmr:Bhie Nduuutt | LastUpdt:16.05.2013
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports ClassFunctionAccounting

Partial Class Accounting_InitVoucher
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_Conn")
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public BranchCode As String = ConfigurationSettings.AppSettings("Branch")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public compnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cfunction As New ClassFunction
#End Region

#Region "Function"
    Private Function validatingData() As String
        Dim sMsg As String = ""
        If VoucherOid.Text = "" Then
            sMsg &= "- No. Voucher belum dipilih !!<BR>"
        End If
        If AmtVoucher.Text = 0 Then
            sMsg &= "- Amount Voucher harus diisi !!<BR>"
        End If
        'sSql = "Select COUNT(*) FROM ql_mstitem Where itemcode='" & Tchar(VoucherNo.Text) & "'"
        'If cKon.ambilscalar(sSql) > 0 Then
        '    sMsg = "No. Voucher " & Tchar(VoucherNo.Text) & " sudah pernah di input..!!<br>"
        'End If
        Return sMsg
    End Function
#End Region

#Region "Prosedure"
    Private Sub ReAmount()
        'dpp.Text = NewMaskEdit(ToDouble(dpp.Text))
        'amttax.Text = NewMaskEdit(ToDouble(dpp.Text) * (ToDouble(trntaxpct.Text) / 100))
        'amtbeli.Text = NewMaskEdit(ToDouble(dpp.Text) + ToDouble(amttax.Text))
        'amtbayar.Text = NewMaskEdit(ToDouble(amtbayar.Text))
        'totalAP.Text = NewMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtbayar.Text))
    End Sub

    Private Sub BindVoucher()
        sSql = "Select itemoid,itemcode,itemdesc From ql_mstitem Where (itemdesc Like '%" & Tchar(VoucherNo.Text.Trim) & "%' OR itemcode LIKE '" & (VoucherNo.Text.Trim) & "') And stockflag= 'V' AND itemcode NOT IN (Select voucherno FROM ql_voucherdtl Where voucherflag='INIT')"
        FillGV(gvVoucher, sSql, "QL_VOUCHER")
    End Sub

    Private Sub initDDL()
        ' Init Currency
        sSql = "select currencyoid, currencycode  + '-' + currencydesc from QL_mstcurr where cmpcode='" & CompnyCode & "'"
        FillDDL(curroid, sSql)
    End Sub

    Private Sub FillRate(ByVal iCurrOid As Integer)
        currate.Text = ToMaskEdit(ToDouble(cKon.ambilscalar("select top 1 rate2idrvalue from QL_mstrate2 where currencyoid=1 order by rate2date desc ")), 4)
    End Sub

    Public Sub binddata()
        Dim sFilter As String = ""
        If DdlStatus.SelectedValue <> "ALL" Then
            sFilter = "AND voucherstatus='" & DdlStatus.SelectedValue & "'"
        End If
        sSql = "Select cmpcode,voucheroid,voucherno,voucherdesc,voucherdate,amtvoucheridr,voucherstatus,vouchernote,currencyoid,currencyrate,rate2oidvcr FROM QL_voucherdtl Where cmpcode='" & CompnyCode & "' " & sFilter & " AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterVcr.Text.Trim) & "%' And voucherflag='INIT' Order By voucheroid Desc"
        FillGV(gvTrnConap, sSql, "QL_voucher")
    End Sub

    Public Sub fillTextBox(ByVal vID As String)
        Dim Conn As New SqlConnection(ConnStr)
        sSql = "Select cmpcode,voucheroid,voucherno,voucherdesc,voucherdate,amtvoucheridr,voucherstatus,vouchernote,currencyoid,currencyrate,rate2oidvcr FROM QL_voucherdtl Where voucheroid=" & vID & " AND cmpcode='" & CompnyCode & "'"
        Dim Sql As New SqlClient.SqlDataAdapter(sSql, ConnStr)
        Dim objDs As New DataSet : Dim objTable As DataTable
        Dim objRow() As DataRow

        Sql.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        If objRow.Length > 0 Then
            VoucherOid.Text = objRow(0)("voucheroid")
            VoucherNo.Text = objRow(0)("voucherno").ToString
            VcrDesc.Text = objRow(0)("voucherdesc").ToString
            DateVoucher.Text = Format(CDate(Trim(objRow(0)("voucherdate").ToString)), "dd/MM/yyyy")
            curroid.SelectedValue = objRow(0)("currencyoid")
            currate.Text = ToMaskEdit(Trim(objRow(0)("currencyrate").ToString), 4)
            AmtVoucher.Text = ToMaskEdit(Trim(objRow(0)("amtvoucheridr").ToString), 4)
            lblPosting.Text = objRow(0)("voucherstatus").ToString
            vNote.Text = objRow(0)("vouchernote").ToString
            rate2oidvcr.Text = objRow(0)("rate2oidvcr")
            'ReAmount()
        End If
        Conn.Close()
        If lblPosting.Text = "POST" Then
            imbposting.Visible = False : btnSave.Visible = False : btnDelete.Visible = False
        Else
            imbposting.Visible = True : btnSave.Visible = True : btnDelete.Visible = True
        End If
    End Sub

    Private Sub generateID()
        If Session("oid") = Nothing Or Session("oid") = "" Then
            sSql = "SELECT ISNULL(MIN(voucheroid),0) FROM QL_voucherdtl WHERE voucheroid < 0 And voucherflag='INIT'"
            Session("vIDTrnBeli") = cKon.ambilscalar(sSql)
            VoucherOid.Text = Session("vIDTrnBeli") - 1
        End If
    End Sub

    Private Sub ShowMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If

        If Session("branch_id") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("trnInitVoucher.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If
        Session("Oid") = Request.QueryString("oid")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        imbposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        Page.Title = "Voucher Initial Balance"
        '======================
        'Cek Peride CutOffDate
        '======================
        Dim CutOffDate As Date
        sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup = 'CUTOFFDATE' AND gencode='c1'"

        If Not IsDate(CDate(toDate(GetStrData(sSql)))) Then
            ShowMessage("Invalid Cut Off Date setup on General group 'CUTOFFDATE' !", compnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        Else
            CutOffDate = toDate(GetStrData(sSql))
        End If
        CutofDate.Text = Format(CutOffDate, "dd/MM/yyyy")
        '=============================================================
        If Not Page.IsPostBack Then
            initDDL()
            If Session("Oid") <> Nothing And Session("Oid") <> "" Then
                binddata()
                btnDelete.Visible = True : fillTextBox(Session("Oid"))
                TabContainer1.ActiveTabIndex = 1 : i_u.Text = "UPDATE"
            Else
                i_u.Text = "N E W" : generateID()
                btnDelete.Visible = False
                UpdUser.Text = Session("UserID")
                UpdTime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 0
                DateVoucher.Text = CutofDate.Text
                curroid_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
     
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim sValue As String = validatingData()
        If sValue <> "" Then
            lblPosting.Text = ""
            ShowMessage(sValue, compnyName & " - WARNING", 2, "modalMsgBoxWarn") : Exit Sub
        End If
        generateID()

        If CDate(toDate(DateVoucher.Text)) > CDate(toDate(CutofDate.Text)) Then
            ShowMessage("- Tanggal invoice Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !! <BR> ", compnyName & "- WARNING", 2, "modalMsgBox")
            Exit Sub
        End If

        '        '==============================
        '        'Cek Peroide Bulanan Dari Crdgl
        '        '==============================
        If ToDouble(AmtVoucher.Text) > 0 Then
            'CEK PERIODE AKTIF BULANAN
            sSql = "Select Distinct left(ISNULL(periodacctg,''),4)+'-'+right(isnull(periodacctg,''),2) FROM QL_crdgl where glflag='OPEN'"
            If GetPeriodAcctg(CDate(toDate(DateVoucher.Text))) > GetStrData(sSql) Then
                ShowMessage("Periode pembayaran bukan periode aktif, periode yang aktif adalah " & GetStrData(sSql) & "<br>", compnyName & "- WARNING", 2, "modalMsgBox")
                Exit Sub
            End If
        End If

        '=======================================================================================
        Dim rateidr As Decimal = GetStrData("select top 1 rate2idrvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")
        Dim rateusd As Decimal = GetStrData("select top 1 rate2usdvalue from ql_mstrate2 where currencyoid='" & curroid.Text & "' order by rate2date desc")

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans
        Try
            ' Insert QL_voucherdtl
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "Insert Into ql_voucherdtl(cmpcode,voucheroid,branch_code,voucherseq,trnbelimstoid,trnbelino,voucherno,voucherdate,voucherdesc,amtvoucher,amtvoucheridr,amtvoucherusd,vouchernote,currencyoid,currencyrate,rate2oidvcr,voucherflag,voucherstatus) Values " & _
                "('" & CompnyCode & "','" & VoucherOid.Text & "','" & Session("branch_id") & "',1,0,'" & Tchar(VoucherNo.Text.Trim) & "','" & Tchar(VoucherNo.Text.Trim) & "','" & CDate(toDate(DateVoucher.Text)) & "','" & Tchar(VcrDesc.Text.Trim) & "','" & ToDouble(AmtVoucher.Text) & "','" & ToDouble(AmtVoucher.Text) * rateidr & "','" & ToDouble(AmtVoucher.Text) * rateusd & "','" & Tchar(vNote.Text.Trim) & "','" & curroid.SelectedValue & "','" & ToDouble(currate.Text) & "','" & rate2oidvcr.Text & "','INIT','" & lblPosting.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "Update ql_voucherdtl Set voucherno='" & Tchar(VoucherNo.Text.Trim) & "',voucherdate='" & CDate(toDate(DateVoucher.Text)) & "',voucherdesc='" & Tchar(VcrDesc.Text.Trim) & "',amtvoucher=" & ToDouble(AmtVoucher.Text) & ",amtvoucheridr='" & ToDouble(AmtVoucher.Text) * rateidr & "',amtvoucherusd='" & ToDouble(AmtVoucher.Text) * rateusd & "',vouchernote='" & Tchar(vNote.Text.Trim) & "',currencyoid='" & curroid.SelectedValue & "',currencyrate=" & ToDouble(currate.Text) & ",rate2oidvcr='" & rate2oidvcr.Text & "',voucherstatus='" & lblPosting.Text & "'" & _
                " Where voucheroid=" & VoucherOid.Text
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            lblPosting.Text = ""
            objTrans.Rollback() : conn.Close()
            ShowMessage(ex.ToString & "<BR>" & sSql, compnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        If lblPosting.Text = "POST" Then
            ShowMessage("Data Sudah terposting !", compnyName & _
            " - INFORMASI", 3, "modalMsgBoxOK")
        Else
            ShowMessage("Data Sudah tersimpan !", compnyName & _
            " - INFORMASI", 3, "modalMsgBoxOK")
        End If
        Response.Redirect("trnInitVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim objConn As New SqlClient.SqlConnection(ConnStr)
        Dim objTrans As SqlClient.SqlTransaction
        Dim objCmd As New SqlClient.SqlCommand
        objConn.Open()
        objTrans = objConn.BeginTransaction()
        objCmd.Connection = objConn
        objCmd.Transaction = objTrans
        Try
            ' ql_trnbelimst
            sSql = "DELETE ql_voucherdtl Where voucheroid=" & VoucherOid.Text & ""
            objCmd.CommandText = sSql : objCmd.ExecuteNonQuery()
            objTrans.Commit() : objCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : objCmd.Connection.Close()
            ShowMessage(ex.Message, compnyName & " - ERROR", 1, "modalMsgBox")
            Exit Sub
        End Try
        'ShowMessage("Data Sudah dihapus !", compnyName & " - INFORMASI", 3, "modalMsgBoxOK")
        Response.Redirect("trnInitVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnInitVoucher.aspx?awal=true")
    End Sub

    Protected Sub btnFindConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindConap.Click
        binddata()
    End Sub

    Protected Sub btnViewAllConap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddata()
        FilterVcr.Text = ""
    End Sub

    Protected Sub gvTrnConap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnInitVoucher.aspx?oid=" & gvTrnConap.SelectedDataKey(0).ToString().Trim)
    End Sub

    Protected Sub gvTrnConap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 4)
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterVcr.Text = ""
    End Sub

    Protected Sub amtbeli_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
        AmtVoucher.Text = ToMaskEdit(AmtVoucher.Text, 4)
    End Sub

    Protected Sub imbposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbposting.Click
        ' POST to GL
        lblPosting.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        Dim sErr As String = ""

        If DateVoucher.Text = "" Then
            ShowMessage("Please fill Voucher Date first!", compnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        Else
            If Not IsValidDate(DateVoucher.Text, "dd/MM/yyyy", sErr) Then
                ShowMessage("Voucher Date is invalid.!", compnyName & " - WARNING", 2, "modalMsgBoxWarn")

                Exit Sub
            End If
        End If
        Dim cRate As New ClassRate()

        If curroid.SelectedValue <> "" Then
            'Dim vDate As Date = Format(CDate(GetServerTime()), "dd/MM/yyyy")
            cRate.SetRateValue(CInt(curroid.SelectedValue), toDate(DateVoucher.Text))
            If cRate.GetRateDailyLastError <> "" Then
                ShowMessage("Please Create Rate for This Week!", compnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                ShowMessage("Please Create Rate for This Month!", compnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If
            rate2oidvcr.Text = cRate.GetRateMonthlyOid
            currate.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, 4)
        End If
        'DateVoucher.Text = Format(CDate(GetServerTime()), "dd/MM/yyyy")
        FillRate(curroid.SelectedValue)
    End Sub

    Protected Sub gvTrnConap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTrnConap.PageIndexChanging
        gvTrnConap.PageIndex = e.NewPageIndex
        binddata()
        gvTrnConap.Visible = True
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()
        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Accounting\trnInitVoucher.aspx?awal=true")
        End If
    End Sub

    Protected Sub BtnVcr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindVoucher()
        gvVoucher.Visible = True
    End Sub

    Protected Sub gvVoucher_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvVoucher.PageIndex = e.NewPageIndex
        BindVoucher()
    End Sub

    Protected Sub gvVoucher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        VoucherNo.Text = gvVoucher.SelectedDataKey.Item("itemcode").ToString().Trim
        VcrDesc.Text = gvVoucher.SelectedDataKey.Item("itemdesc").ToString().Trim
        gvVoucher.Visible = False
    End Sub

    Protected Sub btnViewAllConap_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddata() : gvTrnConap.Visible = True
    End Sub
#End Region

    Protected Sub btnErase_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        VoucherNo.Text = "" : VoucherOid.Text = 0
        VcrDesc.Text = "" : gvVoucher.Visible = False
    End Sub
End Class
