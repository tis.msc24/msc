Imports ClassFunction
Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions

Partial Class rptPinjamBarang
    Inherits System.Web.UI.Page

#Region "Variable"
    Dim vReport As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
#End Region

#Region "Function"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 1 is invalid. " & sErr, 2)
            Return False
        End If
        If Not IsValidDate(CDate(toDate(txtPeriod2.Text)), "MM/dd/yyyy", sErr) Then
            showMessage("Your Period 2 is invalid. " & sErr, 2)
            Return False
        End If
        If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(txtPeriod2.Text)) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function UpdateCheckedSO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblSO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedSO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtTbl2 As DataTable = Session("TblSOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListSO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListSO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "soitemmstoid=" & cbOid
                                dtView2.RowFilter = "soitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblSO") = dtTbl
                Session("TblSOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDO") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblDO") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedDO2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtTbl2 As DataTable = Session("TblDOView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListDO.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListDO.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "doitemmstoid=" & cbOid
                                dtView2.RowFilter = "doitemmstoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblDO") = dtTbl
                Session("TblDOView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMat") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("checkvalue") = cbCheckValue
                                dtView.RowFilter = ""
                            End If
                        Next
                    End If
                Next
                dtTbl.AcceptChanges()
                Session("TblMat") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function UpdateCheckedMat2() As Boolean
        Dim bReturn As Boolean = False
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtTbl2 As DataTable = Session("TblMatView")
            If dtTbl2.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                Dim dtView2 As DataView = dtTbl2.DefaultView
                dtView.RowFilter = ""
                dtView2.RowFilter = ""
                dtView.AllowEdit = True
                dtView2.AllowEdit = True
                For C1 As Integer = 0 To gvListMat.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvListMat.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                                Dim cbCheckValue As String = "False"
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "itemoid=" & cbOid
                                dtView2.RowFilter = "itemoid=" & cbOid
                                If cbCheck Then
                                    cbCheckValue = "True"
                                End If
                                dtView(0)("CheckValue") = cbCheckValue
                                If dtView2.Count > 0 Then
                                    dtView2(0)("CheckValue") = cbCheckValue
                                End If
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                    dtView2.RowFilter = ""
                Next
                dtTbl.AcceptChanges()
                dtTbl2.AcceptChanges()
                Session("TblMat") = dtTbl
                Session("TblMatView") = dtTbl2
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

#End Region

#Region "Procedure"
    Public Sub setDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        For Each mytable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As TableLogOnInfo = mytable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            mytable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub InitDLLCabang()
        sSql = "Select gencode, gendesc From ql_mstgen Where gengroup='Cabang'"
        If Session("UserLevel") = 4 Or Session("UserLevel") = 0 Then
            sSql &= " AND gencode='" & Session("branch_id") & "' Order By gencode"
            FillDDL(dd_branch, sSql)
        ElseIf Session("UserLevel") = 2 Then
            If Session("branch_id") <> "10" Then
                sSql &= " AND gencode='" & Session("branch_id") & "' Order By gencode"
                FillDDL(dd_branch, sSql)
            Else
                sSql &= " Order By gencode"
                FillDDL(dd_branch, sSql)
                dd_branch.Items.Add(New ListItem("ALL", "ALL"))
                dd_branch.SelectedValue = "ALL"
            End If
        ElseIf Session("UserLevel") = 1 Or Session("UserLevel") = 3 Then
            sSql &= " Order By gencode"
            FillDDL(dd_branch, sSql)
            dd_branch.Items.Add(New ListItem("ALL", "ALL"))
            dd_branch.SelectedValue = "ALL"
        End If
    End Sub

    Private Sub BindListSO()
        Try
            sSql = "SELECT DISTINCT 'False' AS checkvalue, som.trnpinjammstoid soitemmstoid, som.trnpinjamno soitemno, som.trndatepinjam soitemdate, CONVERT(VARCHAR(10), som.trndatepinjam, 101) AS sodate, som.status soitemmststatus, som.note soitemmstnote, cb.gendesc cabang, som.namapeminjam From QL_trnpinjammst som Inner Join QL_mstgen cb ON cb.gencode=som.branch_code AND cb.gengroup='CABANG' Where som.status IN ('CLOSED','POST')"
            If dd_branch.SelectedValue <> "ALL" Then
                sSql &= " AND som.branch_code ='" & dd_branch.SelectedValue & "'"
            End If

            If FilterPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
                If IsValidPeriod() Then
                    If fTanggal.SelectedValue = "trndatepinjam" Then
                        sSql &= " AND trndatepinjam>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND trndatepinjam<='" & CDate(toDate(txtPeriod2.Text)) & " 23:59:59'"
                    Else
                        sSql &= " AND trnpinjammstoid IN (Select trnpinjammstoid From QL_trnkembalimst km Where AND trnkembalidate>='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND trnkembalidate<='" & CDate(toDate(txtPeriod2.Text)) & " 23:59:59' AND km.branch_code=som.branch_code)"
                    End If
                Else
                    Exit Sub
                End If
            End If

            sSql &= " ORDER BY som.trndatepinjam DESC, som.trnpinjammstoid DESC"
            Session("TblSO") = CKon.ambiltabel(sSql, "QL_soitemmst")
        Catch ex As Exception
            showMessage(ex.ToString & sSql, 2)
            Exit Sub
        End Try
    End Sub

    Private Sub BindListDO() 
        sSql = "SELECT DISTINCT 'False' AS checkvalue, dom.trnkembalimstoid doitemmstoid, dom.trnkembalino doitemno, dom.trnkembalidate doitemdate, CONVERT(VARCHAR(10), dom.trnkembalidate, 101) AS dodate, som.namapeminjam AS custname, dom.Status doitemmststatus, UPPER(dom.Note) doitemmstnote FROM QL_trnkembalimst dom INNER JOIN QL_trnpinjammst som ON som.trnpinjammstoid=dom.trnpinjammstoid AND som.branch_code=dom.branch_code Where dom.cmpcode='" & CompnyCode & "'"
        If dd_branch.SelectedValue <> "ALL" Then
            sSql &= " AND dom.branch_code='" & dd_branch.SelectedValue & "'"
        End If 

        If sono.Text <> "" Then
            Dim sSono() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " som.trnpinjamno LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & fTanggal.SelectedValue & ">='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND " & fTanggal.SelectedValue & "<='" & CDate(toDate(txtPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If

        sSql &= " ORDER BY dom.trnkembalidate DESC, dom.trnkembalimstoid DESC"
        Session("TblDO") = cKon.ambiltabel(sSql, "ql_trnjualmst")
    End Sub

    Private Sub BindListMat()
        sSql = "SELECT DISTINCT 'False' AS checkvalue, i.itemoid, i.itemdesc itemlongdesc, i.itemcode, 'BUAH' unit,Case i.stockflag When 'T' then 'Barang Dagangan' When 'I' then 'Material Usage' WHEN 'V' THEN 'Merchandise/Barang Hadiah' ELSE 'ASSET' End JenisNya, i.stockflag FROM QL_trnpinjamdtl dod INNER JOIN QL_trnpinjammst som ON som.cmpcode=dod.cmpcode AND som.trnpinjammstoid=dod.trnpinjammstoid AND dod.branch_code=som.branch_code INNER JOIN QL_mstitem i ON i.itemoid=dod.itemoid LEFT JOIN QL_trnkembalimst dom ON dom.trnpinjammstoid=som.trnpinjammstoid AND dom.branch_code=som.branch_code Where som.status IN ('POST','CLOSED')"
        If dd_branch.SelectedValue <> "ALL" Then
            sSql &= " AND dom.branch_code='" & dd_branch.SelectedValue & "'"
        End If

        If sono.Text <> "" Then
            Dim sSono() As String = Split(sono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sSono.Length - 1
                sSql &= " som.trnpinjamno LIKE '%" & Tchar(sSono(c1)) & "%'"
                If c1 < sSono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If dono.Text <> "" Then
            Dim sDono() As String = Split(dono.Text, ";")
            sSql &= " AND ("
            For c1 As Integer = 0 To sDono.Length - 1
                sSql &= " dom.trnkembalino LIKE '%" & Tchar(sDono(c1)) & "%'"
                If c1 < sDono.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ")"
        End If

        If FilterPeriod1.Text <> "" And txtPeriod2.Text <> "" Then
            If IsValidPeriod() Then
                sSql &= " AND " & fTanggal.SelectedValue & ">='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND " & fTanggal.SelectedValue & "<='" & CDate(toDate(txtPeriod2.Text)) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        Session("TblMat") = cKon.ambiltabel(sSql, "QL_mstitem")
    End Sub

    Private Sub showReport(ByVal tipe As String)
        Dim dDate1 As String = Format(CDate(toDate(FilterPeriod1.Text)), "MM/dd/yyyy")
        Dim ddate2 As String = Format(CDate(toDate(txtPeriod2.Text)), "MM/dd/yyyy")
        Dim sDmn As String = "Where cmpcode ='" & CompnyCode & "'"
        If FilterPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Try
                If dDate1 < CDate("01/01/1900") Then
                    showMessage("Please fill period 1 value!", 2)
                    Exit Sub
                End If
                If ddate2 < CDate("01/01/1900") Then
                    showMessage("Please fill period 2 value!", 2)
                    Exit Sub
                End If
                If CDate(toDate(FilterPeriod1.Text)) > CDate(toDate(txtPeriod2.Text)) Then
                    showMessage("Period 2 must be more than Period 1!", 2)
                    Exit Sub
                End If
            Catch ex As Exception
                showMessage("Period report is invalid!", 2)
                Exit Sub
            End Try
        Else
            showMessage("Please fill period report first!", 2)
            Exit Sub
        End If

        Try
            Dim namaPDF As String = ""
            If dd_branch.SelectedValue <> "ALL" Then
                sDmn &= "AND branch_code='" & dd_branch.SelectedValue & "'"
            End If

            If sono.Text <> "" Then
                Dim sSono() As String = Split(sono.Text, ";")
                sSql = " AND ("
                For c1 As Integer = 0 To sSono.Length - 1
                    sSql &= " trnpinjamno LIKE '%" & Tchar(sSono(c1)) & "%'"
                    If c1 < sSono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            sDmn &= sSql

            If dono.Text <> "" Then
                Dim sDono() As String = Split(dono.Text, ";")
                sSql = " AND ("
                For c1 As Integer = 0 To sDono.Length - 1
                    sSql &= " trnkembalino LIKE '%" & Tchar(sDono(c1)) & "%'"
                    If c1 < sDono.Length - 1 Then
                        sSql &= " OR "
                    End If
                Next
                sSql &= ")"
            End If
            sDmn &= sSql

            If itemcode.Text <> "" Then
                Dim sMatcode() As String = Split(itemcode.Text, ";")
                sSql = " AND ("
                For c1 As Integer = 0 To sMatcode.Length - 1
                    sSql &= " itemcode LIKE '%" & Tchar(sMatcode(c1)) & "%'"
                    If c1 < sMatcode.Length - 1 Then
                        sSql &= " OR"
                    End If
                Next
                sSql &= ")"
            End If
            sDmn &= sSql
            sDmn &= " AND " & fTanggal.SelectedValue & ">='" & CDate(toDate(FilterPeriod1.Text)) & " 00:00:00' AND " & fTanggal.SelectedValue & "<='" & CDate(toDate(txtPeriod2.Text)) & " 23:59:59'"

            Dim crConnInfo As New ConnectionInfo
            With crConnInfo
                .ServerName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server")
                .DatabaseName = System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name")
                .IntegratedSecurity = True
            End With

            If DDLlap.SelectedValue = "pinjam" Then
                vReport = New ReportDocument
                If tipe = "EXCEL" Then
                    vReport.Load(Server.MapPath("~\Report\rptPinjamBarangExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptPinjamBarang.rpt"))
                End If
                namaPDF = "Lap_Pinjam_Barang"
                setDBLogonForReport(crConnInfo, vReport)
                vReport.SetParameterValue("sDmn", sDmn)
                vReport.SetParameterValue("vPeriode1", FilterPeriod1.Text)
                vReport.SetParameterValue("vPeriode2", txtPeriod2.Text)
                vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))
            Else
                vReport = New ReportDocument
                If tipe = "EXCEL" Then
                    vReport.Load(Server.MapPath("~\Report\rptKembaliBarangExl.rpt"))
                Else
                    vReport.Load(Server.MapPath("~\Report\rptKembaliBarang.rpt"))
                End If
                namaPDF = "Lap_Kembali_Barang"
                setDBLogonForReport(crConnInfo, vReport)
                vReport.SetParameterValue("sDmn", sDmn)
                vReport.SetParameterValue("vPeriode1", FilterPeriod1.Text)
                vReport.SetParameterValue("vPeriode2", txtPeriod2.Text)
                vReport.SetParameterValue("reportName", vReport.FileName.Substring(vReport.FileName.LastIndexOf("\") + 1) & "/" & System.IO.Path.GetFileName(Me.Request.PhysicalPath).Replace(".vb", ""))

            End If

            Session("diprint") = "True"
            If tipe = "View" Then
                crvMutasiStock.DisplayGroupTree = False
                crvMutasiStock.ReportSource = vReport
            ElseIf tipe = "PDF" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            ElseIf tipe = "EXCEL" Then
                Response.Buffer = False
                Response.ClearHeaders()
                Response.ClearContent()
                vReport.ExportToHttpResponse(ExportFormatType.Excel, Response, True, namaPDF & Format(GetServerTime, "dd_MM_yy"))
                vReport.Close() : vReport.Dispose()
            End If
        Catch ex As Exception
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
    End Sub
    
#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\other\login.aspx")
        End If
 
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID")
            Dim userName As String = Session("UserName")
            Dim access As String = Session("Access")
            Dim xsetRole As DataTable = Session("Role")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_id")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Session.Clear()  ' -->>  clear all session 
            Session("SpecialAccess") = xsetAcc
            Session("branch") = branch
            Session("branch_id") = branch_id
            Session("UserName") = userName
            Session("Role") = xsetRole
            Session("UserID") = userId
            Session("Access") = access
            Response.Redirect("~\ReportForm\rptPinjamBarang.aspx")
        End If

        Page.Title = CompnyName & " - Laporan Status Pinjam Barang"
        Session("UserLevel") = GetStrData("Select USERLEVEL From ql_mstprof Where USERID='" & Session("UserID") & "' AND BRANCH_CODE='" & Session("branch_id") & "'")

        If Not IsPostBack Then
            InitDLLCabang()
            FilterPeriod1.Text = Format(GetServerTime(), "01/MM/yyyy")
            txtPeriod2.Text = Format(GetServerTime(), "dd/MM/yyyy")
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Not vReport Is Nothing Then
                If vReport.IsLoaded Then
                    vReport.Dispose()
                    vReport.Close()
                End If
            End If
        Catch ex As Exception
            vReport.Dispose()
            vReport.Close()
        End Try
    End Sub

    Protected Sub imbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("rptPinjamBarang.aspx?awal=true")
    End Sub

    Protected Sub ibexcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibexcel.Click
        Session("diprint") = "False"
        showReport("EXCEL")
    End Sub 

    Protected Sub ibpdf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("diprint") = "False"
        showReport("PDF")
    End Sub

    Protected Sub btnreport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreport.Click 
        showReport("View")
    End Sub    

    Protected Sub crvMutasiStock_Navigate(ByVal source As Object, ByVal e As CrystalDecisions.Web.NavigateEventArgs) Handles crvMutasiStock.Navigate
        showReport("View")
    End Sub
 
    Protected Sub imbFindSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindSO.Click
        If IsValidPeriod() Then
            DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
            Session("TblSO") = Nothing : Session("TblSOView") = Nothing
            gvListSO.DataSource = Nothing : gvListSO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseSO.Click
        sono.Text = ""
    End Sub

    Protected Sub btnFindListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSO.Click
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No. pinjam data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = DDLFilterListSO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListSO.Text) & "%'"
        If UpdateCheckedSO() Then
            Dim dv As DataView = Session("TblSO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblSOView") = dv.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dv.RowFilter = ""
                mpeListSO.Show()
            Else
                dv.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "No. pinjam data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListSO.Click
        DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
        If Session("TblSO") Is Nothing Then
            BindListSO()
            If Session("TblSO").Rows.Count <= 0 Then
                Session("EmptyListSO") = "No. pinjam data can't be found!"
                showMessage(Session("EmptyListSO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedSO() Then
            Dim dt As DataTable = Session("TblSO")
            Session("TblSOView") = dt
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub btnSelectAllSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No. pinjam data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneSO.Click
        If Not Session("TblSOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblSOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblSO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "soitemmstoid=" & dtTbl.Rows(C1)("soitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblSO") = objTbl
                Session("TblSOView") = dtTbl
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
            End If
            mpeListSO.Show()
        Else
            Session("WarningListSO") = "Please show some No. pinjam data first!"
            showMessage(Session("WarningListSO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedSO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedSO.Click
        If Session("TblSO") Is Nothing Then
            Session("WarningListSO") = "Selected No. pinjam data can't be found!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
        If UpdateCheckedSO() Then
            Dim dtTbl As DataTable = Session("TblSO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListSO.SelectedIndex = -1 : txtFilterListSO.Text = ""
                Session("TblSOView") = dtView.ToTable
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                dtView.RowFilter = ""
                mpeListSO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblSOView") = Nothing
                gvListSO.DataSource = Session("TblSOView")
                gvListSO.DataBind()
                Session("WarningListSO") = "Selected No. pinjam data can't be found!"
                showMessage(Session("WarningListSO"), 2)
            End If
        Else
            mpeListSO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListSO.Click
        If Not Session("TblSO") Is Nothing Then
            If UpdateCheckedSO() Then
                Dim dtTbl As DataTable = Session("TblSO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If sono.Text <> "" Then
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= ";" + vbCrLf + dtView(C1)("soitemno")
                            End If
                        Else
                            If dtView(C1)("soitemno") <> "" Then
                                sono.Text &= dtView(C1)("soitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = "" : dd_branch.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
                Else
                    Session("WarningListSO") = "Please select SO to add to list!"
                    showMessage(Session("WarningListSO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListSO") = "Please show some SO data first!"
            showMessage(Session("WarningListSO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListSO.Click
        cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, False)
    End Sub

    Protected Sub gvListSO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSO.PageIndexChanging
        If UpdateCheckedSO2() Then
            gvListSO.PageIndex = e.NewPageIndex
            gvListSO.DataSource = Session("TblSOView")
            gvListSO.DataBind()
        End If
        mpeListSO.Show()
    End Sub

    Protected Sub imbFindDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindDO.Click
        If IsValidPeriod() Then
            DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
            Session("TblDO") = Nothing : Session("TblDOView") = Nothing
            gvListDO.DataSource = Nothing : gvListDO.DataBind()
            cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseDO.Click
        dono.Text = ""
    End Sub

    Protected Sub btnFindListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDO.Click
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "SI data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = ""
        If DDLFilterListDO.SelectedValue = "doitemno" Then
            sPlus = DDLFilterListDO.SelectedValue & " LIKE '%" & TcharNoTrim(txtFilterListDO.Text) & "%'"
        Else
            sPlus = DDLFilterListDO.SelectedValue & " = '" & TcharNoTrim(txtFilterListDO.Text) & "'"
        End If
        If UpdateCheckedDO() Then
            Dim dv As DataView = Session("TblDO").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblDOView") = dv.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dv.RowFilter = ""
                mpeListDO.Show()
            Else
                dv.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub btnViewAllListDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListDO.Click
        DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
        If Session("TblDO") Is Nothing Then
            BindListDO()
            If Session("TblDO").Rows.Count <= 0 Then
                Session("EmptyListDO") = "SI data can't be found!"
                showMessage(Session("EmptyListDO"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedDO() Then
            Dim dt As DataTable = Session("TblDO")
            Session("TblDOView") = dt
            gvListDO.DataSource = Session("TblDOView")
            gvListDO.DataBind()
        End If
        mpeListDO.Show()
    End Sub

    Protected Sub btnSelectAllDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAllDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some DO data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnSelectNoneDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNoneDO.Click
        If Not Session("TblDOView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDOView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblDO")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "doitemmstoid=" & dtTbl.Rows(C1)("doitemmstoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblDO") = objTbl
                Session("TblDOView") = dtTbl
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
            End If
            mpeListDO.Show()
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
        End If
    End Sub

    Protected Sub btnViewCheckedDO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewCheckedDO.Click
        If Session("TblDO") Is Nothing Then
            Session("WarningListDO") = "Selected SI data can't be found!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
        If UpdateCheckedDO() Then
            Dim dtTbl As DataTable = Session("TblDO")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                DDLFilterListDO.SelectedIndex = -1 : txtFilterListDO.Text = ""
                Session("TblDOView") = dtView.ToTable
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                dtView.RowFilter = ""
                mpeListDO.Show()
            Else
                dtView.RowFilter = ""
                Session("TblDOView") = Nothing
                gvListDO.DataSource = Session("TblDOView")
                gvListDO.DataBind()
                Session("WarningListDO") = "Selected SI data can't be found!"
                showMessage(Session("WarningListDO"), 2)
            End If
        Else
            mpeListDO.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListDO.Click
        If Not Session("TblDO") Is Nothing Then
            If UpdateCheckedDO() Then
                Dim dtTbl As DataTable = Session("TblDO")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If dono.Text <> "" Then
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= ";" + vbCrLf + dtView(C1)("doitemno")
                            End If
                        Else
                            If dtView(C1)("doitemno") <> "" Then
                                dono.Text &= dtView(C1)("doitemno")
                            End If
                        End If
                    Next
                    dtView.RowFilter = "" : dd_branch.Enabled = False
                    cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
                Else
                    Session("WarningListDO") = "Please select SI to add to list!"
                    showMessage(Session("WarningListDO"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListDO") = "Please show some SI data first!"
            showMessage(Session("WarningListDO"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbListDO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListDO.Click
        cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, False)
    End Sub
#End Region

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("EmptyListMat") Is Nothing And Session("EmptyListMat") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListMat") Then
                Session("EmptyListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
            End If
        End If
        If Not Session("WarningListMat") Is Nothing And Session("WarningListMat") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListMat") Then
                Session("WarningListMat") = Nothing
                cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
            End If
        End If
        If Not Session("EmptyListSO") Is Nothing And Session("EmptyListSO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListSO") Then
                Session("EmptyListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("WarningListSO") Is Nothing And Session("WarningListSO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListSO") Then
                Session("WarningListSO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListSO, PanelListSO, mpeListSO, True)
            End If
        End If
        If Not Session("EmptyListDO") Is Nothing And Session("EmptyListDO") <> "" Then
            If lblPopUpMsg.Text = Session("EmptyListDO") Then
                Session("EmptyListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
        If Not Session("WarningListDO") Is Nothing And Session("WarningListDO") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListDO") Then
                Session("WarningListDO") = Nothing
                cProc.SetModalPopUpExtender(btnHiddenListDO, PanelListDO, mpeListDO, True)
            End If
        End If
         
    End Sub

    Protected Sub imbFindMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMat.Click
        If IsValidPeriod() Then
            FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
            Session("TblMat") = Nothing : Session("TblMatView") = Nothing
            gvListMat.DataSource = Nothing : gvListMat.DataBind()
            tbData.Text = "5" : gvListMat.PageSize = CInt(tbData.Text)
            cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub imbEraseMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMat.Click
        itemcode.Text = ""
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        Dim sPlus As String = FilterDDLListMat.SelectedValue & " LIKE '%" & TcharNoTrim(FilterTextListMat.Text) & "%'"
        If JenisBarangDDL.SelectedValue <> "ALL" Then
            sPlus &= " AND stockflag ='" & JenisBarangDDL.SelectedValue & "'"
        End If
        If UpdateCheckedMat() Then
            Dim dv As DataView = Session("TblMat").DefaultView
            dv.RowFilter = sPlus
            If dv.Count > 0 Then
                Session("TblMatView") = dv.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dv.RowFilter = ""
                mpeListMat.Show()
            Else
                dv.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
        If Session("TblMat") Is Nothing Then
            BindListMat()
            If Session("TblMat").Rows.Count <= 0 Then
                Session("EmptyListMat") = "Katalog data can't be found!"
                showMessage(Session("EmptyListMat"), 2)
                Exit Sub
            End If
        End If
        If UpdateCheckedMat() Then
            Dim dt As DataTable = Session("TblMat")
            Session("TblMatView") = dt
            gvListMat.DataSource = Session("TblMatView")
            gvListMat.DataBind()
        End If
        mpeListMat.Show()
    End Sub

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectAll.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "True"
                    dtTbl.Rows(C1)("checkvalue") = "True"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnSelectNone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectNone.Click
        If Not Session("TblMatView") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblMatView")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblMat")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "itemoid=" & dtTbl.Rows(C1)("itemoid")
                    objView(0)("checkvalue") = "False"
                    dtTbl.Rows(C1)("checkvalue") = "False"
                    objView.RowFilter = ""
                Next
                objTbl.AcceptChanges()
                Session("TblMat") = objTbl
                Session("TblMatView") = dtTbl
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
            End If
            mpeListMat.Show()
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnViewChecked_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewChecked.Click
        If Session("TblMat") Is Nothing Then
            Session("WarningListMat") = "Selected Katalog data can't be found!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
        If UpdateCheckedMat() Then
            Dim dtTbl As DataTable = Session("TblMat")
            Dim dtView As DataView = dtTbl.DefaultView
            dtView.RowFilter = "checkvalue='True'"
            If dtView.Count > 0 Then
                FilterDDLListMat.SelectedIndex = -1 : FilterTextListMat.Text = ""
                Session("TblMatView") = dtView.ToTable
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                dtView.RowFilter = ""
                mpeListMat.Show()
            Else
                dtView.RowFilter = ""
                Session("TblMatView") = Nothing
                gvListMat.DataSource = Session("TblMatView")
                gvListMat.DataBind()
                Session("WarningListMat") = "Selected Katalog data can't be found!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            mpeListMat.Show()
        End If
    End Sub

    Protected Sub lkbAddToListListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbAddToListListMat.Click
        If Not Session("TblMat") Is Nothing Then
            If UpdateCheckedMat() Then
                Dim dtTbl As DataTable = Session("TblMat")
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = "checkvalue='True'"
                Dim iCheck As Integer = dtView.Count
                If iCheck > 0 Then
                    For C1 As Integer = 0 To dtView.Count - 1
                        If itemcode.Text <> "" Then
                            itemcode.Text &= ";" + vbCrLf + dtView(C1)("itemcode")
                        Else
                            itemcode.Text = dtView(C1)("itemcode")
                        End If
                    Next
                    dtView.RowFilter = ""
                    cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
                Else
                    Session("WarningListMat") = "Please select material to add to list!"
                    showMessage(Session("WarningListMat"), 2)
                    Exit Sub
                End If
            End If
        Else
            Session("WarningListMat") = "Please show some Katalog data first!"
            showMessage(Session("WarningListMat"), 2)
            Exit Sub
        End If
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub
End Class
