<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnGiroIn.aspx.vb" Inherits="Accounting_trnGiroIn" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" valign="top">
                <table id="Table2" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="LabelM" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Giro Flag Status"></asp:Label></th>
                    </tr>
                </table>
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
           <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <ContentTemplate>
              <ContentTemplate></ContentTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="imbSearchListOf">
                    <table width="100%">
                        <tr>
                            <td align="left" class="Label">
                                Filter :</td>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="ddlFilter" runat="server" CssClass="inpText"
                                    Width="115px">
                                    <asp:ListItem Value="g.Trans_No">Trans No</asp:ListItem>
                                    <asp:ListItem Value="girono">No Giro</asp:ListItem>
                                    <asp:ListItem Value="c.custname">Customer</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<asp:TextBox ID="txtFilter" runat="server" CssClass="inpText" MaxLength="30"
                                    Width="136px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label" style="height: 23px">
                                Cabang</td>
                            <td align="left" colspan="3" style="height: 23px">
                                <asp:DropDownList ID="ddlBranch" runat="server" CssClass="inpText">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                                <asp:CheckBox ID="cbPeriod" runat="server" Checked="True" Text="Period" AutoPostBack="True" />
                            </td>
                            <td align="left" class="Label" colspan="3">
                                <asp:TextBox ID="FilterPeriod1" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="imbPeriod1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                &nbsp;<asp:Label ID="lblTo" runat="server" Text="to"></asp:Label>
                                &nbsp;<asp:TextBox ID="FilterPeriod2" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="imbPeriod2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/oCalendar.gif" />
                                &nbsp;<asp:Label ID="lblFormat" runat="server" CssClass="Important" Text="(dd/MM/yyyy)"></asp:Label>
                                <ajaxToolkit:CalendarExtender ID="cePer1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    PopupButtonID="imbPeriod1" TargetControlID="FilterPeriod1">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="cePer2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    PopupButtonID="imbPeriod2" TargetControlID="FilterPeriod2">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod1"
                                    UserDateFormat="MonthDayYear">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="FilterPeriod2"
                                    UserDateFormat="MonthDayYear">
                                </ajaxToolkit:MaskedEditExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                                <asp:Label ID="Label1" runat="server" Text="Status"></asp:Label></td>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="DDLStatus" runat="server" CssClass="inpText" Width="80px">
                                    <asp:ListItem>All</asp:ListItem>
                                    <asp:ListItem Value="In Process">In Process</asp:ListItem>
                                    <asp:ListItem>POST</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;&nbsp; &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                                Giro Status</td>
                            <td align="left" colspan="3">
                                <asp:DropDownList ID="ddlPayStatus" runat="server" CssClass="inpText" Width="80px">
                                    <asp:ListItem>ALL</asp:ListItem>
                                    <asp:ListItem>Cancel</asp:ListItem>
                                    <asp:ListItem>Bank</asp:ListItem>
                                    <asp:ListItem>Change</asp:ListItem>
                                    <asp:ListItem>Reject</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" class="Label">
                            </td>
                            <td align="left" colspan="3">
                                <asp:ImageButton ID="imbSearchListOf" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/find.png" />
                                <asp:ImageButton ID="imbViewAllListOf" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/viewall.png" /></td>
                        </tr>
                        <tr>
                            <td align="left" class="Label" colspan="4">
                <fieldset id="Fieldset3" style="width: 98%; border-top-style: none; border-right-style: none;
                    border-left-style: none; height: 169px; text-align: left; border-bottom-style: none">
                    <div id="Div2">
                    <asp:GridView ID="gvListOf" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            CellPadding="4" DataKeyNames="trngirooid" Font-Bold="False" Font-Size="X-Small"
                            Width="100%" ForeColor="#333333" GridLines="None" AllowPaging="True" Height="100%" PageSize="5">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="trngirooid" DataNavigateUrlFormatString="~\accounting\trnGiroIn.aspx?oid={0}"
                                    DataTextField="Trans_No" HeaderText="No Trans">
                                    <HeaderStyle Font-Size="X-Small" ForeColor="Black" HorizontalAlign="Left" Width="150px" CssClass="gvhdr" />
                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="150px" Wrap="True" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="tgl" HeaderText="Tanggal Transfer">
                                    <HeaderStyle Font-Size="X-Small" ForeColor="Black" HorizontalAlign="Left" Width="80px" CssClass="gvhdr" />
                                    <ItemStyle Font-Size="X-Small" Font-Underline="False" Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="duedate" HeaderText="Jatuh Tempo">
                                    <HeaderStyle CssClass="gvhdr" Font-Size="X-Small" />
                                    <ItemStyle Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="girono" HeaderText="Giro No">
                                    <HeaderStyle Font-Size="X-Small" ForeColor="Black" HorizontalAlign="Left" Width="155px" CssClass="gvhdr" />
                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="custname" HeaderText="Customer">
                                    <HeaderStyle Font-Size="X-Small" ForeColor="Black" HorizontalAlign="Left" Width="200px" CssClass="gvhdr" />
                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="status" HeaderText="Status">
                                    <HeaderStyle Font-Size="X-Small" ForeColor="Black" HorizontalAlign="Left" Width="80px" CssClass="gvhdr" />
                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="girostatus" HeaderText="Giro Status">
                                    <HeaderStyle CssClass="gvhdr" Font-Size="X-Small" />
                                    <ItemStyle Font-Size="X-Small" Wrap="True" />
                                </asp:BoundField>
                                <asp:BoundField DataField="amount" HeaderText="Total">
                                    <HeaderStyle Font-Size="X-Small" CssClass="gvhdr" ForeColor="Black" />
                                    <ItemStyle Font-Size="X-Small" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="Label13" runat="server" Font-Size="X-Small" ForeColor="Red" Text="Data tidak ditemukan !!"></asp:Label>
                            </EmptyDataTemplate>
                            <FooterStyle ForeColor="Black" Font-Bold="True" CssClass="gvhdr" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="Red" HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        </asp:GridView>
                        &nbsp;</div>
                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp;&nbsp;<asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:BoundField DataField="iomstoid">
                    <itemstyle forecolor="White" width="1px" />
                                <triggers></triggers>
                    <asp:POSTBACKTRIGGER ControlID="tbldata">
                           
            </ContentTemplate>
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />&nbsp; <span style="font-size: 9pt">
                                <strong> Daftar In Coming Giro Flag</strong></span> <strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/corner.gif" />
                <strong><span style="font-size: 9pt">
                                Form Giro Flag Status </span></strong><strong><span style="font-size: 9pt">:.</span></strong>
            </HeaderTemplate>
            <ContentTemplate>
                
                <asp:UpdatePanel id="UpdatePanel7" runat="server"><ContentTemplate>
<TABLE width="100%"><TBODY><TR><TD align=left colSpan=6><asp:Label id="Label2" runat="server" Font-Bold="True" Text=".: Information" Font-Underline="True" __designer:wfdid="w57"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Cabang</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlCabang" runat="server" Width="105px" CssClass="inpText" __designer:wfdid="w58" OnSelectedIndexChanged="ddlCabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lblTrans" runat="server" Width="62px" Text="Trans No" __designer:wfdid="w59"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="trnNo" runat="server" Width="125px" CssClass="inpTextDisabled" __designer:wfdid="w60" ReadOnly="True"></asp:TextBox> <asp:Label id="i_u" runat="server" ForeColor="Red" Text="N E W" __designer:wfdid="w61" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lbTransDate" runat="server" Width="103px" Text="Tanggal" __designer:wfdid="w62"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox style="TEXT-ALIGN: justify" id="transDate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w63" AutoPostBack="True" ValidationGroup="MKE" Enabled="False"></asp:TextBox>&nbsp; <asp:ImageButton id="imTransDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w64" Visible="False"></asp:ImageButton>&nbsp;<asp:Label id="Label6" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w65"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lblCust" runat="server" Width="62px" Text="Customer" __designer:wfdid="w66"></asp:Label> </TD><TD align=left>:</TD><TD align=left><asp:TextBox id="custname" runat="server" Width="232px" CssClass="inpTextDisabled" __designer:wfdid="w68" ReadOnly="True"></asp:TextBox>&nbsp; <asp:Label id="Label4" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w67"></asp:Label>&nbsp;<asp:ImageButton id="btnSearchCust" onclick="btnSearchCust_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w69"></asp:ImageButton> <asp:ImageButton id="btnClearcust" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w70"></asp:ImageButton> <asp:Label id="custoid" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w71" Visible="False"></asp:Label> <asp:Label id="oid" runat="server" Font-Size="X-Small" Font-Bold="True" __designer:wfdid="w72" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="lbStatus" runat="server" Width="62px" Text="Status" __designer:wfdid="w73"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="status" runat="server" Width="77px" CssClass="inpTextDisabled" __designer:wfdid="w74" AutoPostBack="True" ReadOnly="True" MaxLength="20">In Process</asp:TextBox> <asp:Label id="CutofDate" runat="server" __designer:wfdid="w75" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6><ajaxToolkit:CalendarExtender id="CEDP" runat="server" __designer:wfdid="w76" Format="dd/MM/yyyy" PopupButtonID="imTransDate" TargetControlID="transDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MPETanggal" runat="server" __designer:wfdid="w77" Enabled="True" TargetControlID="transDate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder=""></ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6><asp:Label id="Label5" runat="server" Font-Bold="True" Text=".: List" Font-Underline="True" __designer:wfdid="w78"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left>Status Giro</TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="ddlStatusGiro" runat="server" Width="100px" CssClass="inpText" __designer:wfdid="w118" AutoPostBack="True"><asp:ListItem>Cancel</asp:ListItem>
<asp:ListItem>Bank</asp:ListItem>
<asp:ListItem>Change</asp:ListItem>
<asp:ListItem Value="Reject">Reject</asp:ListItem>
</asp:DropDownList></TD><TD id="TD6" align=left runat="server" Visible="false">No. Giro Baru</TD><TD id="TD8" align=left runat="server" Visible="false">:</TD><TD id="TD4" align=left runat="server" Visible="false"><asp:TextBox id="girononew" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w8" Enabled="False" MaxLength="25"></asp:TextBox>&nbsp;<asp:ImageButton id="btnsearchgironew" onclick="btnsearchgironew_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w9"></asp:ImageButton>&nbsp;<asp:ImageButton id="btncleargironew" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w10"></asp:ImageButton>&nbsp;<asp:Label id="girooidnew" runat="server" Font-Size="XX-Small" __designer:wfdid="w6" Visible="False"></asp:Label> <asp:Label id="Label10" runat="server" Width="1px" CssClass="Important" Text="*" __designer:wfdid="w92"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lbGiroNo" runat="server" Width="55px" Text="Giro No" __designer:wfdid="w79"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="GiroNo" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w1" Enabled="False" MaxLength="25"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchGiro" onclick="btnSearchGiro_Click" runat="server" Width="16px" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearGiro" onclick="btnClearGiro_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp; <asp:Label id="seq" runat="server" Font-Size="XX-Small" Text="1" __designer:wfdid="w5" Visible="False"></asp:Label><asp:Label id="girooid" runat="server" Font-Size="XX-Small" __designer:wfdid="w6" Visible="False"></asp:Label> <asp:Label id="I_u2" runat="server" Width="74px" ForeColor="Red" Text="New Detail" __designer:wfdid="w7" Visible="False"></asp:Label></TD><TD id="TD7" align=left runat="server" Visible="false">Jatuh Tempo Giro Baru</TD><TD id="TD9" align=left runat="server" Visible="false">:</TD><TD id="TD3" align=left runat="server" Visible="false"><asp:TextBox style="TEXT-ALIGN: justify" id="duedatenew" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w12" AutoPostBack="True" ValidationGroup="MKE" Enabled="False"></asp:TextBox>&nbsp;<asp:Label id="Label3" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w13" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lbBank" runat="server" Width="55px" Text="BANK" __designer:wfdid="w88"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:DropDownList id="bank" runat="server" Width="188px" CssClass="inpTextDisabled" __designer:wfdid="w89" Enabled="False"></asp:DropDownList></TD><TD id="TD1" align=left runat="server" Visible="false">Bank Baru</TD><TD id="TD2" align=left runat="server" Visible="false">:</TD><TD id="TD5" align=left runat="server" Visible="false"><asp:DropDownList id="ddlbanknew" runat="server" Width="188px" CssClass="inpTextDisabled" __designer:wfdid="w14" Enabled="False"></asp:DropDownList></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lbNoRek" runat="server" Width="86px" Text="No.Akun" __designer:wfdid="w90"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox id="RekNo" runat="server" Width="179px" CssClass="inpTextDisabled" __designer:wfdid="w91" Enabled="False" MaxLength="25"></asp:TextBox></TD><TD id="TD11" align=left runat="server" Visible="false">No Akun Baru</TD><TD id="TD10" align=left runat="server" Visible="false">:</TD><TD id="TD12" align=left runat="server" Visible="false"><asp:TextBox id="norekeningnew" runat="server" Width="179px" CssClass="inpTextDisabled" __designer:wfdid="w2" Enabled="False" MaxLength="25"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lbDueDate" runat="server" Width="74px" Text="Jatuh Tempo" __designer:wfdid="w84"></asp:Label></TD><TD align=left>:</TD><TD align=left><asp:TextBox style="TEXT-ALIGN: justify" id="DueDate" runat="server" Width="100px" CssClass="inpTextDisabled" __designer:wfdid="w15" AutoPostBack="True" ValidationGroup="MKE" Enabled="False"></asp:TextBox> <asp:ImageButton id="ibDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w16" Visible="False"></asp:ImageButton> <asp:Label id="Label8" runat="server" CssClass="Important" Text="dd/MM/yyyy" __designer:wfdid="w17" Visible="False"></asp:Label></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="HEIGHT: 21px" align=left>Total&nbsp;</TD><TD style="HEIGHT: 21px" align=left>:</TD><TD style="HEIGHT: 21px" align=left><asp:TextBox id="amount" runat="server" Width="99px" CssClass="inpTextDisabled" __designer:wfdid="w93" AutoPostBack="True" Enabled="False" MaxLength="12" OnTextChanged="amount_TextChanged"></asp:TextBox></TD><TD style="HEIGHT: 21px" align=left></TD><TD style="HEIGHT: 21px" align=left></TD><TD style="HEIGHT: 21px" align=left>&nbsp;</TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left><asp:Label id="lbNote" runat="server" Width="55px" Text="Catatan" __designer:wfdid="w96"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=4><asp:TextBox id="note" runat="server" Width="520px" CssClass="inpText" __designer:wfdid="w97" MaxLength="50"></asp:TextBox><asp:Label id="Label7" runat="server" CssClass="Important" Text="*50" __designer:wfdid="w98"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=right colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w95" TargetControlID="amount" ValidChars="1234567890.,"></ajaxToolkit:FilteredTextBoxExtender>&nbsp;<asp:ImageButton id="imbAddToList" onclick="imbAddToList_Click" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w99"></asp:ImageButton> <asp:ImageButton id="imbClearDtl" onclick="imbClearDtl_Click" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w100"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6><FIELDSET style="WIDTH: 99%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 100px; BORDER-BOTTOM-STYLE: none" id="Fieldset2"><DIV id="Div1"></DIV><TABLE id="dtltb" class="gvhdr" width="100%" runat="server" Visible="false"><TBODY><TR><TD style="WIDTH: 32px; HEIGHT: 15px"></TD><TD style="WIDTH: 36px; HEIGHT: 15px">No</TD><TD style="WIDTH: 80px; HEIGHT: 15px">Giro No</TD><TD style="WIDTH: 84px; HEIGHT: 15px">Bank</TD><TD style="WIDTH: 84px; HEIGHT: 15px">Status</TD><TD style="WIDTH: 176px; HEIGHT: 15px">No. Akun</TD><TD style="WIDTH: 183px; HEIGHT: 15px">Jatuh Tempo</TD><TD style="WIDTH: 156px; HEIGHT: 15px" align=center>Total</TD><TD style="WIDTH: 151px; HEIGHT: 15px" align=center>Catatan</TD></TR></TBODY></TABLE><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 117%; BACKGROUND-COLOR: transparent"><asp:GridView id="tbldtl" runat="server" Width="100%" Font-Size="X-Small" Font-Names="Microsoft Sans Serif" ForeColor="#333333" __designer:wfdid="w101" OnSelectedIndexChanged="tbldtl_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Seq,GiroNo,bankoid,Bank,RekNo,DueDate,Note,amount,girores1,girostatus,girooidnew,girononew,giroduedatenew,norekeningnew,bankoidnew,banknew" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="White" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="Seq" HeaderText="No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="GiroNo" HeaderText="Giro No">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="girostatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="RekNo" HeaderText="No.Akun">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="DueDate" HeaderText="Jatuh Tempo">
<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="girores1" HeaderText="girores1" Visible="False"></asp:BoundField>
<asp:BoundField DataField="girononew" HeaderText="No. Giro Baru">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="giroduedatenew" HeaderText="Jatuh Tempo Baru">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="banknew" HeaderText="Bank Baru">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="norekeningnew" HeaderText="No. Akun Baru">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Total">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Size="X-Small" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Note" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowCancelButton="False" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Width="30px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="girooidnew" HeaderText="girooidnew" Visible="False"></asp:BoundField>
<asp:BoundField DataField="bankoidnew" HeaderText="bankoidnew" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label12" runat="server" ForeColor="Red" Text="No detail data."></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></FIELDSET></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6>Last Update <asp:Label id="UpdTime" runat="server" Font-Bold="True" ForeColor="#585858" __designer:dtid="5066549580791910" Text='<%# Eval("UPDTIME", "{0:G}") %>' __designer:wfdid="w102"></asp:Label> By <asp:Label id="UpdUser" runat="server" Font-Bold="True" ForeColor="#585858" __designer:dtid="5066549580791911" Text='<%# Eval("UPDUSER", "{0}") %>' __designer:wfdid="w103"></asp:Label> <ajaxToolkit:CalendarExtender id="CeDueDate" runat="server" __designer:wfdid="w104" Format="dd/MM/yyyy" PopupButtonID="ibDueDate" TargetControlID="DueDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MEEDue" runat="server" __designer:wfdid="w105" Enabled="True" TargetControlID="DueDate" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" CultureDatePlaceholder="" CultureTimePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureDateFormat="" CultureCurrencySymbolPlaceholder="" CultureAMPMPlaceholder=""></ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD align=left colSpan=6><asp:ImageButton id="imbSave" runat="server" ImageUrl="~/Images/Save.png" ImageAlign="AbsBottom" __designer:dtid="5066549580791916" __designer:wfdid="w106"></asp:ImageButton> <asp:ImageButton id="ibPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:dtid="5066549580791917" __designer:wfdid="w107"></asp:ImageButton> <asp:ImageButton id="imbDelete" runat="server" ImageUrl="~/Images/Delete.png" ImageAlign="AbsBottom" __designer:dtid="5066549580791918" __designer:wfdid="w108" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsBottom" __designer:dtid="5066549580791919" __designer:wfdid="w109" Visible="False"></asp:ImageButton> <asp:ImageButton id="imbCancel" runat="server" ImageUrl="~/Images/Cancel.png" ImageAlign="AbsBottom" CausesValidation="False" __designer:dtid="5066549580791920" __designer:wfdid="w110"></asp:ImageButton> </TD></TR></TBODY></TABLE><%--<ajaxToolkit:FilteredTextBoxExtender id="fteqty2" runat="server" __designer:wfdid="w170" ValidChars="1234567890" TargetControlID="qty2"></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="fteQty1" runat="server" __designer:wfdid="w171" ValidChars="1234567890" TargetControlID="qty1"></ajaxToolkit:FilteredTextBoxExtender>--%>
</ContentTemplate>
                    <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
</asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upMsgbox" runat="server">
        <contenttemplate>
<asp:Panel id="PanelMsgBox" runat="server" Width="496px" CssClass="modalMsgBoxWarn" DefaultButton="btnMsgBoxOK" Visible="False"><TABLE style="WIDTH: 495px" cellSpacing=1 cellPadding=1 border=0><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2><asp:Panel id="panelPesan" runat="server" Width="100%" Height="25px" BackColor="Yellow"><asp:Label id="lblCaption" runat="server" Font-Size="Medium" Font-Bold="True" ForeColor="Black"></asp:Label></asp:Panel></TD></TR><TR><TD style="HEIGHT: 10px" vAlign=top align=left colSpan=2></TD></TR><TR><TD style="WIDTH: 46px" vAlign=top align=center><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image> </TD><TD vAlign=top align=left><asp:Label id="lblMessage" runat="server" ForeColor="Black"></asp:Label> </TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:Label id="lblState" runat="server" Font-Size="X-Small" Visible="False"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" vAlign=top align=left colSpan=2><asp:ImageButton id="btnMsgBoxOK" onclick="btnMsgBoxOK_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsBottom"></asp:ImageButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeMsgbox" runat="server" TargetControlID="beMsgBox" PopupDragHandleControlID="lblCaption" PopupControlID="PanelMsgBox" BackgroundCssClass="modalBackground" Drag="True" DropShadow="True">
                        </ajaxToolkit:ModalPopupExtender><asp:Button id="beMsgBox" runat="server" Width="130px" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="UpdatePanelCust" runat="server">
        <contenttemplate>
<asp:Panel id="PanelCust" runat="server" Width="700px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="TEXT-ALIGN: center" align=left colSpan=3><asp:Label id="LaberCustomer" runat="server" Font-Size="Medium" Font-Bold="True" Text="Daftar Customer"></asp:Label></TD></TR><TR><TD style="HEIGHT: 26px; TEXT-ALIGN: center" align=center colSpan=3>Filter : <asp:DropDownList id="DDLFilterCust" runat="server" CssClass="inpText"><asp:ListItem Value="custname">Name</asp:ListItem>
<asp:ListItem Value="custcode">Code</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="txtFilterCust" runat="server" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="imbFindCust" onclick="imbFindSupp_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<asp:ImageButton id="imbAllCust" onclick="imbAllSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=3><asp:GridView style="Z-INDEX: 100; LEFT: 1px; POSITION: static; TOP: 4px; BACKGROUND-COLOR: transparent" id="gvCust" runat="server" Width="100%" Font-Bold="False" ForeColor="#333333" __designer:wfdid="w36" OnSelectedIndexChanged="gvCust_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="custoid,custname,custcode" GridLines="None" AllowPaging="True" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField SelectImageUrl="~/picture/select.gif" ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="False" Font-Size="X-Small" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="custcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Size="X-Small" ForeColor="Black" Width="275px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="275px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvhdr" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label20" runat="server" Font-Size="X-Small" ForeColor="Red" Text="No customer data found!!" __designer:wfdid="w1"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" BorderColor="White" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="TEXT-ALIGN: center" align=left colSpan=3>&nbsp;<asp:LinkButton id="Close" onclick="Close_Click" runat="server" Font-Size="Small" Font-Bold="True">[ Close ]</asp:LinkButton></TD></TR><TR><TD style="TEXT-ALIGN: center" align=right colSpan=3></TD></TR></TBODY></TABLE><ajaxToolkit:ModalPopupExtender id="MPECust" runat="server" TargetControlID="buttonCust" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="PanelCust" PopupDragHandleControlID="LaberCustomer"></ajaxToolkit:ModalPopupExtender></asp:Panel>&nbsp; <asp:Button id="ButtonCust" runat="server" Width="0px" BackColor="White" __designer:wfdid="w115" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upListBGK" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListBGK" runat="server" Width="650px" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListBGK" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Giro"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=3>Filter : <asp:DropDownList id="FilterDDLListBGK" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="girono">Giro No.</asp:ListItem>
<asp:ListItem Value="note">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListBGK" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListBGK" onclick="btnFindListBGK_Click" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListBGK" onclick="btnAllListBGK_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvListBGK" runat="server" Width="99%" ForeColor="#333333" OnSelectedIndexChanged="gvListBGK_SelectedIndexChanged" GridLines="None" DataKeyNames="girooid,girono,duedate,bankoid,rekno,note,amount" CellPadding="4" AutoGenerateColumns="False" PageSize="5" AllowPaging="True" OnPageIndexChanging="gvListBGK_PageIndexChanging" EnableModelValidation="True" OnRowDataBound="gvListBGK_RowDataBound">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="girono" HeaderText="Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="#333333"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="duedate" HeaderText="duedate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" ForeColor="#333333"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amount" HeaderText="Amount">
<HeaderStyle CssClass="gvpopup" ForeColor="#333333"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="bank" HeaderText="Bank">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" ForeColor="#333333"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="girooid" HeaderText="girooid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" CssClass="gvpopup" Font-Bold="True" ForeColor="Red"></PagerStyle>
<EmptyDataTemplate>
                                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lbCloseListBGK" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListBGK" runat="server" TargetControlID="btnHideListBGK" BackgroundCssClass="modalBackground" PopupControlID="pnlListBGK" PopupDragHandleControlID="lblListBGK"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListBGK" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    &nbsp;
    &nbsp;&nbsp;
</asp:Content>