<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnsuratjalan.aspx.vb" Inherits="Transaction_trnsuratjalan" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table style="width: 100%">
        <tr>
            <th align="left" class="header" style="width: 274px" valign="center">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" ForeColor="Navy" Text=".: Data Surat Jalan"></asp:Label>
            </th>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td colspan="3" style="width: 971px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Font-Bold="False" Style="font-weight: bold; font-size: 12px">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                        <span style="font-weight: bold; font-size: 12px">
                            <img align="absMiddle" alt="" src="../Images/corner.gif"/> List Of Surat Jalan :.</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<TABLE style="FONT-WEIGHT: normal; WIDTH: 100%"><TBODY><TR><TD style="VERTICAL-ALIGN: top" colSpan=3></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 98px">Filter</TD><TD style="VERTICAL-ALIGN: top">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilter" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w91"><asp:ListItem Value="no">Nomor Surat Jalan</asp:ListItem>
<asp:ListItem Value="pic">PIC</asp:ListItem>
<asp:ListItem Value="driver">Driver</asp:ListItem>
<asp:ListItem Value="supplier">Supplier</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="tbfilter" runat="server" Width="184px" CssClass="inpText" __designer:wfdid="w92"></asp:TextBox> <asp:ImageButton id="ibfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsBottom" __designer:wfdid="w93"></asp:ImageButton> <asp:ImageButton id="ibviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsBottom" __designer:wfdid="w94"></asp:ImageButton></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 98px"><asp:CheckBox id="cbperiod" runat="server" Text="Periode" __designer:wfdid="w95"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox style="TEXT-ALIGN: right" id="tbperiodstart" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w96"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodstart" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w97"></asp:ImageButton>&nbsp;&nbsp;to&nbsp; <asp:TextBox style="TEXT-ALIGN: right" id="tbperiodend" runat="server" Width="56px" CssClass="inpText" __designer:wfdid="w98"></asp:TextBox>&nbsp;<asp:ImageButton id="ibperiodend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w99"></asp:ImageButton> &nbsp; &nbsp;<SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN></TD></TR><TR style="COLOR: #000099"><TD style="VERTICAL-ALIGN: top; WIDTH: 98px"><asp:CheckBox id="cbstatus" runat="server" Text="Status" __designer:wfdid="w100"></asp:CheckBox></TD><TD style="VERTICAL-ALIGN: top">:</TD><TD style="VERTICAL-ALIGN: top"><asp:DropDownList id="ddlfilterstatus" runat="server" Width="128px" CssClass="inpText" __designer:wfdid="w101"><asp:ListItem Value="all">All</asp:ListItem>
<asp:ListItem Value="inprocess">In Process</asp:ListItem>
<asp:ListItem Value="send">Send</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD></TR><TR><TD colSpan=3><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w102" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodstart"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w103" MaskType="Date" Mask="99/99/9999" TargetControlID="tbperiodend"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w104" TargetControlID="tbperiodstart" PopupButtonID="ibperiodstart" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w105" TargetControlID="tbperiodend" PopupButtonID="ibperiodend" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD colSpan=3><asp:GridView style="WIDTH: 100%" id="gvdata" runat="server" Width="474px" ForeColor="#333333" __designer:wfdid="w89" AllowPaging="True" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="sjloid" PageSize="8">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="sjloid" DataNavigateUrlFormatString="~/Transaction/trnsuratjalan.aspx?oid={0}" DataTextField="sjlno" HeaderText="No. Surat Jalan">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" Width="20%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="sjlsend" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Tanggal Kirim">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjlperson" HeaderText="PIC">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjlnopol" HeaderText="No Expedisi">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjldriver" HeaderText="Driver">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="sjlstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbprint" runat="server" __designer:wfdid="w35" OnClick="printSjalan">Print</asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="#FFCC66" Font-Bold="True" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label4" runat="server" ForeColor="Red" Text="Data not found!" __designer:wfdid="w507"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="gvdata"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                            &nbsp;<br />
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                        <span style="font-weight: bold; font-size: 12px">
                        <img align="absMiddle" alt="" src="../Images/corner.gif" /> Form Surat Jalan :.</span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE width="100%"><TBODY><TR><TD colSpan=6><asp:Label id="lbltitleinfo" runat="server" Font-Size="10px" ForeColor="Red" __designer:wfdid="w53" Visible="False" Font-Underline="True"></asp:Label> <asp:Label id="lblcurrentuser" runat="server" __designer:wfdid="w54" Visible="False"></asp:Label> </TD></TR><TR><TD colSpan=6><TABLE width="100%"><TBODY><TR style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099"><TD>Cabang</TD><TD>:</TD><TD><asp:DropDownList id="DdlCabang" runat="server" Width="144px" CssClass="inpText" __designer:wfdid="w1" OnSelectedIndexChanged="DdlCabang_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; COLOR: #000099"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px"><SPAN>No. </SPAN></TD><TD>:</TD><TD><asp:TextBox id="tbno" runat="server" Width="168px" CssClass="inpTextDisabled" __designer:wfdid="w55" Enabled="False"></asp:TextBox> <asp:Label id="lbloid" runat="server" __designer:wfdid="w56" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px"><SPAN><SPAN>T</SPAN>angg<SPAN>al&nbsp;<SPAN style="COLOR: #ff0000">*</SPAN></SPAN></SPAN></TD><TD style="VERTICAL-ALIGN: top; COLOR: #000099">:</TD><TD><asp:TextBox style="TEXT-ALIGN: right" id="tbdatesend" runat="server" Width="64px" CssClass="inpText" __designer:wfdid="w57"></asp:TextBox><SPAN style="COLOR: #ff0000"> </SPAN><asp:ImageButton id="ibdatesend" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="Top" __designer:wfdid="w58"></asp:ImageButton>&nbsp;<SPAN style="COLOR: red">(dd/mm/yyyy)</SPAN><ajaxToolkit:CalendarExtender id="CalendarExtender3" runat="server" __designer:wfdid="w59" TargetControlID="tbdatesend" PopupButtonID="ibdatesend" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender><SPAN style="COLOR: #ff0000"> </SPAN><ajaxToolkit:MaskedEditExtender id="MaskedEditExtender1" runat="server" __designer:wfdid="w60" MaskType="Date" Mask="99/99/9999" TargetControlID="tbdatesend"></ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px">PIC <SPAN style="COLOR: #ff0000">*</SPAN></TD><TD style="COLOR: #000099">:</TD><TD><asp:TextBox id="tbpic" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w61" Enabled="False" MaxLength="50"></asp:TextBox>&nbsp; <asp:ImageButton id="ibsearchpic" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w62"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibdelpic" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w63"></asp:ImageButton> <SPAN style="VERTICAL-ALIGN: top; COLOR: red"></SPAN><asp:Label id="lblpic" runat="server" __designer:wfdid="w64" Visible="False"></asp:Label> <asp:Label id="lblapprovaluser" runat="server" __designer:wfdid="w65" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px"><SPAN>Nomor&nbsp;Expedi</SPAN>si</TD><TD>:</TD><TD><asp:TextBox id="tbnopol" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w66" MaxLength="10"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px">Driver <SPAN style="COLOR: #ff0000">*</SPAN></TD><TD>:</TD><TD><asp:TextBox id="tbdriver" runat="server" Width="120px" CssClass="inpText" __designer:wfdid="w67" MaxLength="50"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px">Supplier <SPAN style="COLOR: #ff0000">*</SPAN></TD><TD>:</TD><TD><asp:TextBox id="tbsupplier" runat="server" Width="120px" CssClass="inpTextDisabled" __designer:wfdid="w68" Enabled="False"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="ibsearchsupplier" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w69"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibdelsupplier" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w70"></asp:ImageButton><SPAN style="VERTICAL-ALIGN: top; COLOR: red"></SPAN><asp:Label id="lblsupplieroid" runat="server" __designer:wfdid="w71" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px">Status</TD><TD>:</TD><TD><asp:TextBox id="lblstatus" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w72" Enabled="False"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px"></TD><TD></TD><TD></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:Label id="Label3" runat="server" Font-Size="Medium" Font-Bold="True" Text="Detail Barang :" __designer:wfdid="w73" Font-Underline="True"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px">Barcode </TD><TD>:</TD><TD><asp:TextBox id="lblbarcode" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w74"></asp:TextBox> <asp:ImageButton id="ibsearchbarcode" runat="server" ImageUrl="~/Images/search2.gif" ImageAlign="Top" __designer:wfdid="w75"></asp:ImageButton> <asp:ImageButton id="ibdelbarcode" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="Top" __designer:wfdid="w76"></asp:ImageButton> <asp:Label id="lblreqoid" runat="server" __designer:wfdid="w77" Visible="False"></asp:Label> <asp:Label id="lblbarcodeview" runat="server" __designer:wfdid="w78" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px">Nama Barang</TD><TD>:</TD><TD><asp:TextBox id="tbitemname" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w79" Enabled="False"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px">Quantity</TD><TD>:</TD><TD><asp:TextBox style="TEXT-ALIGN: right" id="tbitemqty" runat="server" Width="72px" CssClass="inpTextDisabled" __designer:wfdid="w80" Enabled="False"></asp:TextBox> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender2" runat="server" __designer:wfdid="w81" MaskType="Number" Mask="999,999,999" TargetControlID="tbitemqty" InputDirection="RightToLeft"></ajaxToolkit:MaskedEditExtender></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px">Jenis Barang</TD><TD>:</TD><TD><asp:TextBox id="tbitemtype" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w82" Enabled="False"></asp:TextBox> <asp:Label id="lblitemtype" runat="server" __designer:wfdid="w83" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="WIDTH: 130px">Merk</TD><TD>:</TD><TD><asp:TextBox id="tbitembrand" runat="server" Width="152px" CssClass="inpTextDisabled" __designer:wfdid="w84" Enabled="False"></asp:TextBox> <asp:Label id="lblitembrand" runat="server" __designer:wfdid="w85" Visible="False"></asp:Label> <asp:Label id="lblstarttime" runat="server" __designer:wfdid="w86" Visible="False"></asp:Label> <asp:Label id="lblmaxdate" runat="server" __designer:wfdid="w87" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top; WIDTH: 130px">Detail Kerusakan <SPAN style="VERTICAL-ALIGN: top; COLOR: red">*<BR /></SPAN></TD><TD style="VERTICAL-ALIGN: top">:</TD><TD style="VERTICAL-ALIGN: top"><asp:TextBox id="tbnote" runat="server" Width="496px" Height="40px" CssClass="inpText" __designer:wfdid="w88" TextMode="MultiLine"></asp:TextBox><asp:Label id="Label5" runat="server" ForeColor="Red" Text="* Maks 250 Karakter" __designer:wfdid="w89"></asp:Label>&nbsp;<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Width="312px" __designer:wfdid="w90" ErrorMessage="* Warning : Detail kerusakan lebih dari 250 karakter!" ControlToValidate="tbnote" ValidationExpression="^[\s\S]{0,240}$"></asp:RegularExpressionValidator></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:Label id="lblstat" runat="server" __designer:wfdid="w91" Visible="False"></asp:Label> <asp:Label id="lblrow" runat="server" __designer:wfdid="w92" Visible="False"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:ImageButton id="ibaddtolist" runat="server" ImageUrl="~/Images/addtolist.png" __designer:wfdid="w93"></asp:ImageButton> <asp:ImageButton id="ibclear" runat="server" ImageUrl="~/Images/clear.png" CausesValidation="False" __designer:wfdid="w94"></asp:ImageButton></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=3>Detail Surat Jalan&nbsp;:</TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top" colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 175px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvdaftar" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w95" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="sjldtloid,barcode,reqoid,reqitemname,reqitemtype,type,reqitembrand,brand,itemqty,note" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbeditgvr" onclick="getdetail" runat="server" ForeColor="CornflowerBlue" __designer:wfdid="w13">Select</asp:LinkButton> 
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="barcode" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="reqitemname" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="type" HeaderText="Jenis Barang">
<HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="brand" HeaderText="Merk">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemqty" DataFormatString="{0:#,###}" HeaderText="Jumlah">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="note" HeaderText="Kerusakan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="Black" Width="30%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:LinkButton id="lbdelgvr" runat="server" __designer:wfdid="w251" onclientclick="return confirm('Hapus data?')" onclick="deleterow">
<asp:Image ID="imgdelgvr" runat="server" ImageUrl="~/Images/del.jpeg"></asp:Image>
</asp:LinkButton>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr" ForeColor="Black"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label2" runat="server" ForeColor="Red" __designer:wfdid="w141" Text="Data not found!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR style="FONT-SIZE: 8pt"><TD style="VERTICAL-ALIGN: top" colSpan=3><asp:Label id="lblcreate" runat="server" __designer:wfdid="w96"></asp:Label></TD></TR><TR style="FONT-SIZE: 8pt"><TD colSpan=3><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibsaveinfo" runat="server" ImageUrl="~/Images/Save.png" __designer:wfdid="w97"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibcancelinfo" runat="server" ImageUrl="~/Images/Cancel.png" CausesValidation="False" __designer:wfdid="w98"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibdelete" runat="server" ImageUrl="~/Images/Delete.png" CausesValidation="False" __designer:wfdid="w99"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibapprove" runat="server" ImageUrl="~/Images/sendapproval.png" CausesValidation="False" __designer:wfdid="w100"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibpostinginfo" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w101" Visible="False"></asp:ImageButton><asp:ImageButton style="MARGIN-RIGHT: 10px" id="ibprint" runat="server" ImageUrl="~/Images/print.png" __designer:wfdid="w102" Visible="False"></asp:ImageButton></TD></TR></TBODY></TABLE></TD></TR><TR style="FONT-SIZE: 8pt; COLOR: #000099"><TD colSpan=6></TD></TR></TBODY></TABLE>
</contenttemplate>
<triggers>
<asp:PostBackTrigger ControlID="ibprint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="uppopupmsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlpopupmsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w12" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblcaption" runat="server" Font-Size="Small" Font-Bold="True" ForeColor="White" __designer:wfdid="w13"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imicon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px" __designer:wfdid="w14"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblpopupmsg" runat="server" Font-Size="11px" Font-Bold="True" ForeColor="Red" __designer:wfdid="w15"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbokpopupmsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w16"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpepopupmsg" runat="server" __designer:wfdid="w17" TargetControlID="bepopupmsg" DropShadow="True" PopupControlID="pnlpopupmsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblcaption" Drag="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bepopupmsg" runat="server" CausesValidation="False" BackColor="Transparent" __designer:wfdid="w18" Visible="False" BorderColor="Transparent" BorderStyle="None"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="uppopgv" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgv" runat="server" CssClass="modalBox" __designer:wfdid="w594" Visible="False" DefaultButton="ibpopfind">&nbsp;<DIV style="OVERFLOW: auto"><TABLE style="WIDTH: 650px"><TBODY><TR><TD>Filter</TD><TD>:</TD><TD><asp:DropDownList id="ddlpopfilter" runat="server" Width="80px" CssClass="inpText" __designer:wfdid="w370"><asp:ListItem Value="kode">Code</asp:ListItem>
<asp:ListItem Value="nama">Nama</asp:ListItem>
</asp:DropDownList>&nbsp; <asp:TextBox id="tbpopfilter" runat="server" Width="224px" CssClass="inpText" __designer:wfdid="w595"></asp:TextBox>&nbsp;&nbsp;<asp:ImageButton id="ibpopfind" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top" __designer:wfdid="w596"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibpopviewall" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top" __designer:wfdid="w93"></asp:ImageButton></TD></TR><TR><TD colSpan=3></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 168px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvpopsupplier" runat="server" Width="100%" ForeColor="#333333" __designer:wfdid="w597" DataKeyNames="suppoid,suppname" AutoGenerateColumns="False" CellPadding="4" GridLines="None" PageSize="15">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Underline="False" ForeColor="Blue" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Nama">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Red" __designer:wfdid="w712" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="ibpopcancel" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w598"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopgv" runat="server" __designer:wfdid="w599" TargetControlID="bepopgv" Drag="True" PopupDragHandleControlID="ibpopcancel" BackgroundCssClass="modalBackground" PopupControlID="panpopgv" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgv" runat="server" CausesValidation="False" BackColor="Transparent" __designer:wfdid="w600" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel><asp:UpdatePanel id="uppoporder" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgvorder" runat="server" CssClass="modalBox" Visible="False" __designer:wfdid="w608" DefaultButton="ibpopfindorder">&nbsp;<DIV style="OVERFLOW: auto"><TABLE style="WIDTH: 650px"><TBODY><TR><TD style="WIDTH: 64px">Filter</TD><TD style="WIDTH: 7px">:</TD><TD><asp:DropDownList id="ddlpopfilterorder" runat="server" Width="136px" CssClass="inpText" __designer:wfdid="w371"><asp:ListItem Value="barcode">Barcode</asp:ListItem>
<asp:ListItem Value="namacustomer">Nama Customer</asp:ListItem>
<asp:ListItem Value="namabarang">Nama Barang</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:TextBox id="tbpopfilterorder" runat="server" Width="224px" CssClass="inpText" __designer:wfdid="w609"></asp:TextBox>&nbsp;&nbsp; <asp:ImageButton id="ibpopfindorder" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top" __designer:wfdid="w610"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibpopviewallorder" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top" __designer:wfdid="w94"></asp:ImageButton></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 98%; HEIGHT: 168px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvpoporder" runat="server" ForeColor="#333333" __designer:wfdid="w611" PageSize="15" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="reqoid,barcode,custname,reqitemname,reqitemtype,type,reqitembrand,brand,sstarttime">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Underline="True" ForeColor="Blue" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="barcode" HeaderText="Barcode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Nama Customer">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="reqitemname" HeaderText="Nama Barang">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="Data not found!" __designer:wfdid="w372"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD align=center colSpan=3><asp:ImageButton id="ibpopcancelorder" runat="server" ImageUrl="~/Images/Cancel.png" __designer:wfdid="w612"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepopgvorder" runat="server" __designer:wfdid="w613" TargetControlID="bepopgvorder" Drag="True" PopupDragHandleControlID="ibpopcancelorder" BackgroundCssClass="modalBackground" PopupControlID="panpopgvorder" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgvorder" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" __designer:wfdid="w614" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="uppoppic" runat="server">
        <contenttemplate>
<asp:Panel id="panpopgvpic" runat="server" CssClass="modalBox" Visible="False" DefaultButton="ibpopfindpic">&nbsp;<DIV style="OVERFLOW: auto"><TABLE style="WIDTH: 650px"><TBODY><TR><TD style="WIDTH: 64px">Filter</TD><TD style="WIDTH: 7px">:</TD><TD><asp:DropDownList id="ddlpopfilterpic" runat="server" Width="136px" CssClass="inpText"><asp:ListItem Value="username">Nama PIC</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:TextBox id="tbpopfilterpic" runat="server" Width="224px" CssClass="inpText"></asp:TextBox>&nbsp;&nbsp; <asp:ImageButton id="ibpopfindpic" runat="server" ImageUrl="~/Images/find.png" ImageAlign="Top"></asp:ImageButton>&nbsp; <asp:ImageButton id="ibpopviewallpic" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="Top"></asp:ImageButton></TD></TR><TR><TD colSpan=3></TD></TR><TR><TD colSpan=3><DIV style="OVERFLOW-Y: scroll; WIDTH: 98%; HEIGHT: 168px; BACKGROUND-COLOR: beige"><asp:GridView style="WIDTH: 99%" id="gvpoppic" runat="server" ForeColor="#333333" PageSize="15" GridLines="None" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="personnip,personname">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black" Width="60px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Underline="True" ForeColor="Blue" Width="60px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="personnip" HeaderText="userid" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="personname" HeaderText="Nama PIC">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="Black"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#FFCC66" ForeColor="#333333"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label1" runat="server" Font-Bold="True" ForeColor="Red" Text="Data not found!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></DIV></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: center" colSpan=3><asp:ImageButton id="ibpopcancelpic" runat="server" ImageUrl="~/Images/Cancel.png"></asp:ImageButton></TD></TR></TBODY></TABLE></DIV></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpepic" runat="server" TargetControlID="bepopgvpic" Drag="True" PopupDragHandleControlID="ibpopcancelpic" BackgroundCssClass="modalBackground" PopupControlID="panpopgvpic" DropShadow="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="bepopgvpic" runat="server" CausesValidation="False" BackColor="Transparent" Visible="False" BorderStyle="None" BorderColor="Transparent"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>    
</asp:Content>

