Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Master_uploadinit
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public BranchCode As String = ConfigurationSettings.AppSettings("BranchCode")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cFunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim oRegex As Regex
    Dim oMatches As MatchCollection
#End Region

#Region "Functions"

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function

#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal sCaption As String, ByVal iType As Integer, ByVal cssClass As String)
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Red
        ElseIf iType = 2 Then ' WARNING
            imIcon.ImageUrl = "~/images/warn.png"
            lblCaption.ForeColor = Drawing.Color.Black
            panelPesan.BackColor = Drawing.Color.Yellow
            btnMsgBoxOK.ImageUrl = "~/images/button-ok-yellow.png"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png"
            lblCaption.ForeColor = Drawing.Color.White
            panelPesan.BackColor = Drawing.Color.Green
            btnMsgBoxOK.ImageUrl = "~/images/button-ok_inf.png"
        Else
            imIcon.ImageUrl = "~/images/error.jpg"
            btnMsgBoxOK.ImageUrl = "~/Images/ok.png"
            lblCaption.ForeColor = Drawing.Color.White
        End If
        PanelMsgBox.CssClass = cssClass
        lblCaption.Text = sCaption : lblMessage.Text = sMessage
        PanelMsgBox.Visible = True : beMsgBox.Visible = True : mpeMsgbox.Show()
    End Sub

    Private Sub setTableDetailPromo()
        If Session("viewTarget") Is Nothing Then
            Dim tblDetail As DataTable
            tblDetail = New DataTable("QL_trnUpload")
            tblDetail.Columns.Add("nomor", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("itemoid", Type.GetType("System.Int32")) '0
            tblDetail.Columns.Add("itemcode", Type.GetType("System.String")) '4
            tblDetail.Columns.Add("gudang", Type.GetType("System.Int32")) '1
            tblDetail.Columns.Add("hpp", Type.GetType("System.Decimal")) '2
            tblDetail.Columns.Add("saldo", Type.GetType("System.Decimal")) '3
            Session("viewTarget") = tblDetail
        End If
    End Sub

    Private Sub GenerateDraftNo()
        sSql = "SELECT ISNULL(MAX(CAST(ISNULL(resfield1,0) AS INTEGER)) + 1, 1) AS IDNEW FROM ql_trnstockadj"
        resfield1.Text = ToDouble(cKoneksi.ambilscalar(sSql).ToString)
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then Response.Redirect("~/Other/login.aspx")
        ' THROW TO NOT AUTHORIZED PAGE IF NO ROLE EXIST
        'If Not (ClassFunction.checkPagePermission(Page.AppRelativeVirtualPath.ToString, Session("Role"))) Then
        '    Response.Redirect("~\other\NotAuthorize.aspx")
        'End If
        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim branchId As String = Session("branch_id")
            Dim branch As String = Session("branch")
            Session.Clear()  ' -->>  clear all session 
            Session("branch_id") = branchId
            Session("branch") = branch
            Session("UserID") = userId '--> insert lagi sesion yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Response.Redirect("uploadinit.aspx") '----> di call lagi krn untuk menghilangkan parameter "awal" shg refresh ulang
        End If

        Page.Title = CompnyName & " - Upload Init Stock" ' CHANGE FORM NAME HERE
        Session("oid") = Request.QueryString("oid")
        btnSave.Attributes.Add("OnClick", "javascript:return confirm('Are u sure want to SAVE this data?');")

        If Not Page.IsPostBack Then
            setTableDetailPromo()
            upduser.Text = Session("UserID")
            updtime.Text = GetServerTime()
        End If
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 3)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 3)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("uploadinit.aspx?awal=true")
    End Sub

    Protected Sub btnMsgBoxOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelMsgBox.Visible = False
        beMsgBox.Visible = False
        mpeMsgbox.Hide()

        If lblMessage.Text = "Data telah disimpan !" Or lblMessage.Text = "Data telah dihapus !" Or lblMessage.Text = "Data telah diposting !" Then
            Response.Redirect("~\Master\uploadinit.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnSave_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim Validasi As String = ""
        Dim objTran As DataTable = Session("viewTarget")
        If (Session("viewTarget")) Is Nothing Then
            Validasi &= "- Data belum di upload !!<br/>"
        Else
            If (objTran.Rows.Count) <= 0 Then
                showMessage("Data belum di upload", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                Exit Sub
            End If

        End If

        If objTran.Rows.Count >= 200 Then
            showMessage("Maaf data yang akan di upload tidak bisa lebih dari 200", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        If Validasi <> "" Then
            showMessage(Validasi, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim iInitStockOid As Integer = GenerateID("QL_INITSTOCK", CompnyCode)
        Dim conmtroid As Int64 = GenerateID("QL_conmtr", CompnyCode)
        Dim crdmtroid As Int64 = GenerateID("QL_crdmtr", CompnyCode)
        Dim sDate As String = Format(GetServerTime(), "yyyy-MM-dd")
        Dim bc As String = ""

        Dim objTransApproval As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTransApproval = conn.BeginTransaction()
        xCmd.Transaction = objTransApproval

        Try
            If Not Session("viewTarget") Is Nothing Then
                Dim obj As DataTable = Session("viewTarget")
                For c1 As Int16 = 0 To obj.Rows.Count - 1
                    sSql = "Select ISNULL(COUNT(*),0.0) from QL_conmtr Where refoid=" & obj.Rows(c1).Item("itemoid") & "" : xCmd.CommandText = sSql : Dim CekIdItem As Integer = xCmd.ExecuteScalar

                    If CekIdItem > 0 Then
                        showMessage("Maaf, Katalog dengan kode " & obj.Rows(c1).Item("itemcode") & " sudah pernah di input saldo awal qty", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
                        objTransApproval.Rollback()
                        xCmd.Connection.Close()
                        Exit Sub
                    End If
                    sSql = "select b.gencode as branchcode from QL_mstgen a inner join QL_mstgen b on b.genoid=CAST(a.genother2 as int) Where a.gengroup='LOCATION'and a.genoid=" & obj.Rows(c1).Item("gudang") & "" : xCmd.CommandText = sSql : bc = CStr(xCmd.ExecuteScalar)

                    'Insert Ql_trninitstock
                    sSql = "INSERT INTO QL_initstock (cmpcode, initoid, initdate, periodacctg, initrefoid, initbrach, initlocation, initqty, initflag, upduser, updtime, initvalueidr, initvalueusd, inituser, initdatetime) VALUES ('" & CompnyCode & "', " & iInitStockOid & ", '" & sDate & "', '" & GetDateToPeriodAcctg(sDate) & "', " & obj.Rows(c1).Item("itemoid") & ",'" & bc & "', " & obj.Rows(c1).Item("gudang") & ", " & ToDouble(obj.Rows(c1).Item("saldo").ToString) & ", 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(obj.Rows(c1).Item("hpp").ToString) & ", " & ToDouble(obj.Rows(c1).Item("hpp").ToString) & ", 'admin', CURRENT_TIMESTAMP )"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iInitStockOid += 1

                    'insert&update stock
                    sSql = "update QL_crdmtr set lasttrans='" & sDate & "',lasttranstype='SA', saldoakhir = saldoakhir + " & ToDouble(obj.Rows(c1).Item("saldo").ToString) & " Where periodacctg = '" & GetDateToPeriodAcctg(sDate) & "' and refoid = " & obj.Rows(c1).Item("itemoid") & " and refname = 'QL_MSTITEM' and mtrlocoid = " & obj.Rows(c1).Item("gudang") & " and branch_code = '" & bc & "' and cmpcode = '" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    If xCmd.ExecuteNonQuery() <= 0 Then
                        sSql = "insert into QL_crdmtr(cmpcode, crdmatoid, lasttrans, lasttranstype, qtyin, periodacctg, refoid,saldoakhir, upduser, updtime, refname, mtrlocoid, branch_code) VALUES ('" & CompnyCode & "', '" & crdmtroid & "','" & sDate & "', 'SA'," & ToDouble(obj.Rows(c1).Item("saldo").ToString) & ",'" & GetDateToPeriodAcctg(sDate) & "'," & obj.Rows(c1).Item("itemoid") & "," & ToDouble(obj.Rows(c1).Item("saldo").ToString) & ", 'admin', '" & sDate & "', 'QL_MSTITEM'," & obj.Rows(c1).Item("gudang") & ", '" & bc & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        crdmtroid += 1
                    End If
                    sSql = "INSERT INTO ql_conmtr (cmpcode,conmtroid, type, trndate, periodacctg, formaction, formoid, Formname, refoid, refname,unitoid, mtrlocoid,branch_code, qtyin, qtyout, reason, upduser, updtime, typemin, personoid,amount,note,HPP) VALUES ('" & CompnyCode & "'," & conmtroid & ",'Init Stock','" & sDate & "','" & GetPeriodAcctg(sDate) & "','SA'," & iInitStockOid & ",'QL_initstock'," & obj.Rows(c1)("itemoid").ToString & ",'QL_MSTITEM',945,'" & obj.Rows(c1)("gudang").ToString & "','" & bc & "'," & ToDouble(obj.Rows(c1).Item("saldo").ToString) & ",0,'','admin','" & sDate & "',0,0," & (ToDouble(obj.Rows(c1).Item("saldo").ToString) * ToDouble(obj.Rows(c1).Item("hpp").ToString)) & ",'INIT STOCK - Cut Off 28 Des 2016','" & ToDouble(obj.Rows(c1)("hpp").ToString) & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    conmtroid += 1
                    sSql = "update ql_mstitem set hpp=" & ToDouble(obj.Rows(c1)("hpp").ToString) & " where itemoid=" & obj.Rows(c1)("itemoid").ToString & ""
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Next

                sSql = "UPDATE QL_mstoid SET lastoid=" & iInitStockOid - 1 & " WHERE tablename='QL_INITSTOCK' AND cmpcode='" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "update  QL_mstoid set lastoid=" & conmtroid - 1 & " where tablename = 'QL_conmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "update  QL_mstoid set lastoid=" & crdmtroid - 1 & " where tablename = 'QL_crdmtr' and cmpcode = '" & CompnyCode & "' "
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTransApproval.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTransApproval.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - ERROR", 1, "modalMsgBoxWarn")
            Exit Sub
        End Try
        showMessage("Data telah disimpan !", CompnyName & " - INFORMASI", 3, "modalMsgBoxOK")
    End Sub

    Protected Sub btnXls_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnXls.Click
        Dim objTran As DataTable = Session("viewTarget")
        If (objTran.Rows.Count) > 0 Then
            showMessage("Data Excel TO Exist From List", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            Exit Sub
        End If

        Dim excelconn As New OleDbConnection
        Try
            If fuXls.HasFile Then
                Dim filePath As String = fuXls.PostedFile.FileName
                Dim fileName As String = Path.GetFileName(filePath)
                Dim ext As String = Path.GetExtension(fileName)
                Dim type As String = String.Empty
                Dim uploadName As String = "INIT" & Format(GetServerTime(), "MMddyyyyHHmm")

                Select Case ext
                    Case ".xls"
                        type = "application/vnd.ms-excel"
                    Case ".xlsx"
                        type = "application/vnd.ms-excel"
                End Select

                If type <> String.Empty Then
                    fuXls.SaveAs(Server.MapPath("~/Upload/" & uploadName & ext))

                    If File.Exists(Server.MapPath("~/Upload/" & uploadName & ext)) Then
                        Dim exceldtab As DataTable = Session("viewTarget")
                        Dim exceldrow As DataRow

                        Dim sHeaderMsg As String = "" : Dim errMsg As String = ""
                        Dim excelconnstr As String = "" : Dim iMatCode As String = ""
                        Dim iMatOid As Integer = 0 : Dim iMatDesc As String = ""

                        If ext = ".xlsx" Then
                            excelconnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 12.0;Persist Security Info=False"
                        ElseIf ext = ".xls" Then
                            excelconnstr = "Provider=Microsoft.JET.OLEDB.4.0;Data Source='" & Server.MapPath("~/Upload/" & uploadName & ext) & "';Extended Properties=Excel 8.0;Persist Security Info=False"
                        End If

                        excelconn.ConnectionString = excelconnstr
                        excelconn.Open()

                        Dim ds As New DataSet
                        Try
                            Dim sqlstring As String = "SELECT * FROM [mstitem$]"
                            Dim oda As New OleDb.OleDbDataAdapter(sqlstring, excelconnstr)
                            oda.Fill(ds, "mstitem")
                            excelconn.Close()
                        Catch ex As Exception
                            showMessage("Format Template Salah, Ganti Nama Sheet Menjadi mstitem ..!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                            Exit Sub
                        End Try
                        Dim dtab As DataTable = ds.Tables("mstitem")

                        If conn.State = ConnectionState.Closed Then
                            conn.Open()
                        End If

                        Dim dtrow() As DataRow = dtab.Select("", "")
                        Dim trnbelidate As New Date
                        Dim dvExcel As DataView
                        dvExcel = exceldtab.DefaultView

                        'Validasi Excel
                        For i As Integer = 0 To dtrow.Length - 1
                            If dtrow(i).Item("itemoid").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum itemoid " & dtrow(i).Item("itemoid").ToString.Trim & " Harus Angka<br />"
                            End If

                            If dtrow(i).Item("itemoid").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum itemoid " & dtrow(i).Item("itemcode").ToString.Trim & " Harus Angka<br />"
                            End If

                            If dtrow(i).Item("gudang").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum gudang " & dtrow(i).Item("gudang").ToString.Trim & " Harus Angka<br />"
                            End If

                            If dtrow(i).Item("hpp").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum hpp " & dtrow(i).Item("hpp").ToString.Trim & " Harus Angka<br />"
                            End If

                            If dtrow(i).Item("saldo").ToString.Trim = "" Then
                                sHeaderMsg &= "Excel Coloum saldo " & dtrow(i).Item("saldo").ToString.Trim & " Harus Angka<br />"
                            End If
                            Exit For
                        Next

                        If sHeaderMsg.Trim <> "" Then
                            showMessage(sHeaderMsg.Trim, CompnyName & " - INFO", 2, "modalMsgBoxWarn")
                            Exit Sub
                        End If

                        For i As Integer = 0 To dtrow.Length - 1
                            If dtrow(i).Item("gudang").ToString = 0 Then
                                showMessage("Maaf, Id gudang katalog dengan kode " & dtrow(i).Item("itemcode").ToString.Trim & " masih nol,<br> Silahkan klik tombol list gudang untuk info id gudang!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                                Exit For
                            End If
                            If dtrow(i).Item("saldo").ToString = 0 Then
                                showMessage("Maaf, Saldo Awal Qty untuk katalog dengan kode " & dtrow(i).Item("itemcode").ToString.Trim & " masih nol", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                                Exit For
                            End If

                            exceldrow = exceldtab.NewRow
                            exceldrow("nomor") = i + 1
                            exceldrow("itemoid") = dtrow(i).Item("itemoid").ToString
                            exceldrow("itemcode") = dtrow(i).Item("itemcode").ToString
                            exceldrow("gudang") = dtrow(i).Item("gudang").ToString
                            exceldrow("hpp") = ToDouble(dtrow(i).Item("hpp").ToString)
                            exceldrow("saldo") = ToDouble(dtrow(i).Item("saldo").ToString)
                            exceldtab.Rows.Add(exceldrow)
                            If exceldtab.Rows.Count >= 200 Then
                                showMessage("Maksimal data yang bisa di upload hanya 200...!!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                                Exit For
                            End If
                        Next

                        conn.Close()
                        gvDtl.DataSource = exceldtab
                        gvDtl.DataBind()
                    Else
                        showMessage("Upload failed!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                    End If
                Else
                    excelconn.Close() : conn.Close()
                    showMessage("You can only select .xls or .xlsx file!", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
                End If
            Else
                showMessage("Please choose file to upload first.", CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
            End If

        Catch ex As Exception
            excelconn.Close() : conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End Try
    End Sub

    Private Sub PrintContract(ByVal formatReport As ExportFormatType)
        Try
            Response.Clear()

            Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"

            sSql = "SELECT itemoid,itemcode,itemdesc,0 Gudang,0.00 hpp,0.00 saldo FROM QL_mstitem i Where itemoid NOT IN (Select refoid From QL_crdmtr crd Where crd.refoid=i.itemoid) ORDER BY itemoid"

            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If

            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
            da.SelectCommand = xcmd
            da.Fill(ds)
            Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
            dt = ds.Tables(0)
            Response.Write(ConvertDtToTDF(dt))
            conn.Close()
            Response.End()
        Catch ex As Exception
            conn.Close()
            showMessage(ex.ToString & "<BR>" & sSql, CompnyName & " - WARNING", 2, "modalMsgBoxWarn")
        End Try
    End Sub

    Protected Sub PrintExl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrintExl.Click
        PrintContract(ExportFormatType.Excel)
    End Sub

    Protected Sub BtnListGdg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnListGdg.Click
        LinkClose.Visible = True
        sSql = "Select genoid,gendesc from QL_mstgen Where gengroup='LOCATION' AND genoid<>-10"
        Dim objTable As DataTable = cKoneksi.ambiltabel(sSql, "QL_mstgen")
        Session("QL_mstgen") = objTable : GvGudang.DataSource = objTable
        GvGudang.DataBind() : GvGudang.Visible = True
    End Sub

    Protected Sub LinkClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GvGudang.Visible = False
    End Sub
#End Region
End Class
