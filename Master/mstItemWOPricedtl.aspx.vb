Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports ClassFunction
Imports System.IO
Imports System.Drawing
Imports Koneksi
Imports ClassProcedure
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class mstItemWOPrice
    Inherits System.Web.UI.Page


#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    Public DB_JPT_RITEL As String = ConfigurationSettings.AppSettings("DB_JPT_RITEL")
    Public DB_JPT_GROSIR As String = ConfigurationSettings.AppSettings("DB_JPT_GROSIR")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Private oRegex As Regex : Dim oMatches As MatchCollection
    Dim dv As DataView
    Dim cfunction As New ClassFunction
    Dim cKoneksi As New Koneksi
    Dim dtTemp As New DataTable
    Private rptSupp As New ReportDocument
    Private cProc As New ClassProcedure
#End Region


#Region "Function"
    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
    End Function



    Function checkTypeFile(ByVal filename As String) As Boolean
        Dim fileExt As String = Path.GetExtension(filename).ToLower()
        Dim allowedExt As String() = {".jpg", ".jpeg", ".bmp", ".gif", ".png"}
        Dim fileOK As Boolean = False
        For i As Integer = 0 To allowedExt.Length - 1
            If fileExt = allowedExt(i) Then
                fileOK = True
            End If
        Next
        Return fileOK
    End Function
#End Region


#Region "Procedure"

    Public Function getFileName(ByVal backID As String) As String
        Dim fname As String = ""
        Dim ID As String = Trim(backID)
        fname = "item_" & ID & ".jpg"
        Return fname
    End Function

    Public Sub uploadFileGambar(ByVal proses As String, ByVal fname As String)
        Dim savePath As String = Server.MapPath("~/Images/")
        Const sbmpW = 80
        Const sbmpH = 50

        imgPerson.Visible = False
        If FileUploadInsert.HasFile Then
            If checkTypeFile(FileUploadInsert.FileName) Then
                Dim newWidth As Integer
                Dim newHeight As Integer
                newWidth = sbmpW
                newHeight = sbmpH
                savePath += fname
                Dim uploadBmp As Bitmap = Bitmap.FromStream(FileUploadInsert.PostedFile.InputStream)
                Dim newBmp As Bitmap = New Bitmap(newWidth, newHeight)
                Dim newGraphic As Graphics = Graphics.FromImage(newBmp)

                Try
                    newGraphic.Clear(Color.White)
                    newGraphic.DrawImage(uploadBmp, 0, 0, newWidth, newHeight)
                    newBmp.Save(savePath, Imaging.ImageFormat.Jpeg)
                    lblMenuPic.Text = "Upload File Berhasil"
                    lblMenuPic.Visible = True
                    imgPerson.ImageUrl = "~/Images/" & fname
                    imgPerson.Visible = True
                Catch ex As Exception
                End Try
            Else
                lblMenuPic.Text = "Tidak dapat diupload. Format file harus .jpg/.jpeg/.gif/.png/.bmp!"
                lblMenuPic.Visible = True
            End If
        Else
            lblMenuPic.Text = "Data Gambar belum dipilih"
            lblMenuPic.Visible = True
        End If
    End Sub

    Private Sub showMessage(ByVal message As String, ByVal caption As String)
        Validasi.Text = message
        lblCaption.Text = caption
        PanelValidasi.Visible = True
        ButtonExtendervalidasi.Visible = True
        ModalPopupExtenderValidasi.Show()
    End Sub

    Public Sub BindData(ByVal sCompCode As String)
        Dim sWhere As String = ""
        FilterText.Text = Session("FilterText")
        FilterDDL.SelectedIndex = Session("FilterDDL")

        FilterText2.Text = Session("FilterText2")
        FilterDDL2.SelectedIndex = Session("FilterDDL2")

        FilterText3.Text = Session("FilterText3")
        FilterDDL3.SelectedIndex = Session("FilterDDL3")

        orderby.SelectedIndex = Session("orderby")

        If Session("FilterDDL") Is Nothing = False Or Session("FilterDDL2") Is Nothing = False Or Session("FilterDDL3") Is Nothing = False Then
            If Session("FilterText").ToString.Trim <> "" Then
                sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            End If
            If Session("FilterText2").ToString.Trim <> "" Then
                sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
            End If
            If Session("FilterText3").ToString.Trim <> "" Then
                sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "
            End If
        Else
            sWhere = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
            sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
            sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "
        End If

        'If sCompCode <> "" Then
        '    sSql = "SELECT TOP 1000 1 as nomor,	QL_mstitem.cmpcode, itemoid, itemcode, merk,itemdesc, gp.gendesc pay,	QL_mstgen.gendesc as itemgroupoid, itemsubgroupoid, itembarcode1, itembarcode2, itembarcode3, itempriceunit1, itempriceunit2, itempriceunit3, g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2, konversi2_3,itemsafetystockunit1, QL_mstgen.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as personoid , pricelist bottompricegrosir,keterangan  FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid  inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 inner join ql_mstgen gp on ql_mstitem.payoid = gp.genoid  " & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " QL_mstitem.cmpcode LIKE '%" & CompnyCode & "%' ORDER BY " & orderby.SelectedValue
        'Else
        '    sSql = "SELECT TOP 1000	1 as nomor, QL_mstitem.cmpcode, itemoid, itemcode, itemdesc,	QL_mstgen.gendesc as itemgroupoid, itemsubgroupoid, itembarcode1, itembarcode2, itembarcode3, itempriceunit1, itempriceunit2, itempriceunit3, g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2,konversi2_3, itemsafetystockunit1, QL_mstgen.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as personoid  ,pricelist bottompricegrosir , keterangan FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3    " & sWhere & " ORDER BY  " & orderby.SelectedValue
        'End If

        sSql = "SELECT top 100 1 as nomor,	QL_mstitem.cmpcode, itemoid, itemcode, itemdesc,merk,	QL_mstgen.gendesc as itemgroupoid, itemsubgroupoid, itembarcode1, itembarcode2, itembarcode3, convert(numeric(18,2),round((itempriceunit1/konversi1_2)/konversi2_3,0)) itempriceunit1, convert(numeric(18,2),round(itempriceunit2/konversi2_3,0))  itempriceunit2, itempriceunit3, replace(g1.gendesc + '( '+convert(char(4),konversi1_2*konversi2_3)+')',' ','') as satuan1, replace(g2.gendesc + '( '+convert(char(4),konversi2_3)+')',' ','') as satuan2, g3.gendesc as satuan3,konversi1_2, konversi2_3,itemsafetystockunit1, ql_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as personoid,'?' 'infoharga' ,pricelist bottompricegrosir,keterangan, (select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'GROSIR' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stock " & _
" ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from ql_jpt.dbo.QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (691,706,718,719,721,722) and mtrlocoid in (select genoid from ql_jpt.dbo.QL_mstgen where genother1 = (select genoid from ql_jpt.dbo.QL_mstgen where genother1  = 'RETAIL' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockGUDANG35	" & _
" ,(select isnull(SUM(saldoAkhir - qtyBooking ),0) from QL_crdmtr where closingdate = '1/1/1900' and refoid = QL_mstItem.itemoid and mtrlocoid in (select genoid from QL_mstgen where genother1 = (select genoid from QL_mstgen where genother1  = 'SUPPLIER' and gengroup = 'WAREHOUSE') and gengroup = 'LOCATION')  ) stockSupp" & _
" FROM QL_mstitem 	inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid 	inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid  	inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  	inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  	inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3 	" & sWhere & IIf(sWhere <> "", " AND ", " WHERE ") & " QL_mstitem.cmpcode LIKE '%" & CompnyCode & "%' ORDER BY " & orderby.SelectedValue


        Dim mySqlDA As New SqlDataAdapter(sSql, conn)
        Dim objDs As New DataSet
        mySqlDA.Fill(objDs, "data")

        Dim dr As DataRow
        For c1 As Integer = 0 To objDs.Tables("data").Rows.Count - 1
            dr = objDs.Tables("data").Rows(c1)
            dr.BeginEdit()
            dr(0) = c1 + 1
            dr.EndEdit()
        Next

        GVmstgen.DataSource = objDs.Tables("data")
        GVmstgen.DataBind()
    End Sub

    Public Sub FillTextBox(ByVal vGenoid As String)

        Try
            sSql = "SELECT mi.[cmpcode],mi.[itemoid],mi.[itemcode],mi.[itemname],mi.[itemdesc],mi.merk, mi.payoid," & _
                   "mi.[itemgroupoid],mi.[itemsubgroupoid],mi.[itembarcode1]," & _
                   "mi.[itembarcode2], mi.[itembarcode3], " & _
                   "mi.[itempriceunit1], mi.[itempriceunit2], mi.[itempriceunit3], " & _
                   "mi.[satuan1], mi.[satuan2], mi.[satuan3], " & _
                   "mi.[itemsafetystockunit1],mi.[createuser],mi.[upduser],mi.[updtime], mi.[itemflag], mi.[acctgoid] , konversi1_2, konversi2_3,mi.personoid,mi.itempictureloc, keterangan , editharga ,itemsafetystockunitgrosir,maxstockgrosir,maxstock  " & _
                   "FROM [QL_mstitem] mi where mi.cmpcode='" & CompnyCode & "' and mi.itemoid = '" & vGenoid & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            btnDelete.Enabled = False
            While xreader.Read
                itemoid.Text = xreader("itemoid")
                itemcode.Text = xreader("itemcode")
                itemdesc.Text = xreader("itemdesc")
                merk.Text = xreader("merk")
                itemgroupoid.SelectedValue = xreader("itemgroupoid").ToString.Trim
                itemsubgroupoid.SelectedValue = xreader("itemsubgroupoid").ToString.Trim
                itembarcode1.Text = xreader("itembarcode1")
                itembarcode2.Text = xreader("itembarcode2")
                itembarcode3.Text = xreader("itembarcode3")
                payment.SelectedValue = xreader("payoid")
               
                satuan1.SelectedValue = xreader("satuan1").ToString.Trim
                satuan2.SelectedValue = xreader("satuan2").ToString.Trim
                satuan3.SelectedValue = xreader("satuan3").ToString.Trim
                itemsafetystock.Text = NewMaskEdit(xreader("itemsafetystockunit1"))
                Maxstock.Text = NewMaskEdit(xreader("maxstock"))
                itemsafetystockgrosir.Text = NewMaskEdit(xreader("itemsafetystockunitgrosir"))
                Maxstockgrosir.Text = NewMaskEdit(xreader("maxstockgrosir"))

                konversi2_3.Text = NewMaskEdit(xreader("konversi2_3"))
                konversi1_2.Text = NewMaskEdit(xreader("konversi1_2"))
                spgOid.SelectedValue = xreader("personoid").ToString.Trim
                imgPerson.Visible = True
                imgPerson.ImageUrl = "~/Images/" & xreader("itempictureloc").ToString

                acctgoid.SelectedValue = xreader("acctgoid").ToString.Trim
                Upduser.Text = xreader("upduser")
                Updtime.Text = Format(xreader("updtime"), "dd/MM/yyyy HH:mm:ss")
                ddlStatus.SelectedValue = xreader("itemflag").ToString.Trim
                keterangan.Text = xreader("keterangan").trim
                satuan1.Enabled = False
                satuan2.Enabled = False
                satuan3.Enabled = False

                btnDelete.Enabled = True
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message & ex.ToString & sSql, "ERROR") : Exit Sub

        End Try

    End Sub

    Public Sub GenerateGenID()
        itemoid.Text = GenerateID("QL_mstitem", CompnyCode)
    End Sub

    Sub clearItem()
        itemoid.Text = ""
        itemcode.Text = ""
        itemdesc.Text = ""
        itemgroupoid.SelectedIndex = 0
        itemsubgroupoid.SelectedIndex = 0
        itembarcode1.Text = ""
        itembarcode2.Text = ""
        itembarcode3.Text = ""
        konversi2_3.Text = ""
        konversi1_2.Text = ""

        satuan1.SelectedIndex = 0
        satuan2.SelectedIndex = 0
        satuan3.SelectedIndex = 0
        acctgoid.SelectedIndex = 0
        itemsafetystock.Text = 0
        ddlStatus.SelectedIndex = 0
        Upduser.Text = Session("UserID")
        Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")

    End Sub

    Sub initDDL()
        Dim msg As String = ""

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemgroupoid, sSql)
        Else
            msg &= " - Isi Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"

        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemsubgroupoid, sSql)
        Else
            msg &= " - Isi Sub Grup Barang di Data General terlebih dahulu !! " & "<BR>"
        End If


        sSql = "select acctgoid, acctgcode + '-' + acctgdesc from QL_mstacctg where cmpcode ='" & CompnyCode & "' "
        FillDDL(acctgoid, sSql)

        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMUNIT' and cmpcode='" & CompnyCode & "' ORDER BY gendesc"
        FillDDL(satuan1, sSql)
        FillDDL(satuan2, sSql)
        FillDDL(satuan3, sSql)

        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE cmpcode = '" & CompnyCode & "'   and  personpost in (select genoid from ql_mstgen where gengroup = 'JOBPOSITION' AND GENDESC = 'SALES PERSON')  and STATUS='AKTIF' "
        FillDDL(spgOid, sSql)


        sSql = "select genoid ,gendesc from ql_mstgen where cmpcode = '" & CompnyCode & "' and gengroup = 'PAYTYPE' "
        FillDDL(payment, sSql)


        If msg <> "" Then
            showMessage(msg, CompnyName & " - INFORMASI")
            btnSave.Visible = False
            Exit Sub
        End If

    End Sub

#End Region


#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~\Other\login.aspx")
        End If


        If Request.QueryString("awal") = "true" Then
            Dim userId As String = Session("UserID") '--> simpan session k variabel spy tidak hilang
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim xnomeja As String = Request.QueryString("nomeja")
            Dim xoutletoid As String = Session("outletoid")
            Dim branch As String = Session("branch")
            Dim branch_id As String = Session("branch_code")
            Session.Clear()  ' -->>  clear all session 
            Session("branch") = branch
            Session("branch_code") = branch_id
            Session("outletoid") = xoutletoid
            Session("UserID") = userId '--> insert lagi session yg disimpan dan create session 
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole

            Response.Redirect("mstItemWOPricedtl.aspx?page=" & GVmstgen.PageIndex)
        End If

        Page.Title = CompnyName & " - Data Item"
        Session("oid") = Request.QueryString("oid")
        Me.btnDelete.Attributes.Add("onclick", "javascript:return confirm('Anda yakin akan HAPUS data ini ?');")

        If Session("oid") <> Nothing And Session("oid") <> "" Then
            i_u.Text = "Update"
            'TabContainer1.ActiveTabIndex = 1
        Else
            i_u.Text = "New"
        End If

        If Not IsPostBack Then
            Session("page") = GVmstgen.PageIndex
            BindData(CompnyCode)
            initDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                lblupd.Text = "Last Update"
                btnDelete.Visible = False
            Else
                GenerateGenID()
                btnDelete.Visible = False
                Upduser.Text = Session("UserID")
                Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")
                TabContainer1.ActiveTabIndex = 0
                GVmstgen.PageIndex = ToDouble(Request.QueryString("page"))
                GVmstgen.DataBind()
                lblupd.Text = "Create"
            End If
        End If
    End Sub

    'BUat paging
    Protected Sub GVmstgen_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstgen.PageIndexChanging
        GVmstgen.PageIndex = e.NewPageIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Session("FilterText") = ""
        Session("FilterDDL") = 0

        Session("FilterText2") = ""
        Session("FilterDDL2") = 0

        Session("FilterText3") = ""
        Session("FilterDDL3") = 0

        Session("orderby") = orderby.SelectedIndex
        BindData(CompnyCode)
    End Sub

    Protected Sub orderby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles orderby.SelectedIndexChanged
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex

        Session("FilterText2") = FilterText2.Text
        Session("FilterDDL2") = FilterDDL2.SelectedIndex

        Session("FilterText3") = FilterText3.Text
        Session("FilterDDL3") = FilterDDL3.SelectedIndex

        Session("orderby") = orderby.SelectedIndex
        BindData(CompnyCode)
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Session("FilterText") = FilterText.Text
        Session("FilterDDL") = FilterDDL.SelectedIndex
        Session("orderby") = orderby.SelectedIndex

        Session("FilterText2") = FilterText2.Text
        Session("FilterDDL2") = FilterDDL2.SelectedIndex

        Session("FilterText3") = FilterText3.Text
        Session("FilterDDL3") = FilterDDL3.SelectedIndex

        GVmstgen.PageIndex = 0
        BindData(CompnyCode)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim errMessage As String = ""
        If itemcode.Text.Trim = "" Then errMessage &= "- Isi Kode Barang Dahulu ! <BR>"
        If itemdesc.Text.Trim = "" Then errMessage &= "- Isi Nama Barang Dahulu ! <BR>"
        If merk.Text.Trim = "" Then errMessage &= "- Isi Merk Dahulu ! <BR>"
        If itemsafetystock.Text.Trim = 0 Then errMessage &= "- Isi Safety Stok Retail Dahulu ! <BR>"
        If Maxstock.Text.Trim = 0 Then errMessage &= "- Isi Max Stok Retail Dahulu ! <BR>"
        If itemgroupoid.Items.Count <= 0 Then errMessage &= "- Pilih Grup Barang Dahulu ! <BR>"
        If itemsubgroupoid.Items.Count <= 0 Then errMessage &= "- Pilih Sub Grup Barang Dahulu ! <BR>"
        If ToDouble(konversi1_2.Text.Trim) = 0 Then errMessage &= "- Konversi 1-2 tidak boleh kosong / 0 ! <BR>"
        If ToDouble(konversi2_3.Text.Trim) = 0 Then errMessage &= "- Konversi 2-3 tidak boleh kosong / 0 ! <BR>"
        If ToDouble(itemsafetystock.Text.Trim) > ToDouble(Maxstock.Text.Trim) Then errMessage &= "- Safety Stock Retail harus lebih kecil dari Max Stock Retail! <BR>"
        If ToDouble(itemsafetystockgrosir.Text.Trim) > ToDouble(Maxstockgrosir.Text.Trim) Then errMessage &= "- Safety Stock Grosir harus lebih kecil dari Max Stock Grosir! <BR>"
        'If Left(itemdesc.Text, 1).ToUpper <> Left(itemcode.Text, 1).ToUpper Then errMessage &= "- Kode awal tidak sama dengan karakter awal dari nama barang ! <BR>"

        If errMessage <> "" Then
            showMessage(errMessage, CompnyName & " - WARNING")
            Exit Sub
        End If

        'cek deskipsi msgen yang kembar
        Dim sSqlCheck1 As String = "SELECT COUNT(-1) FROM ql_mstitem WHERE cmpcode='" & CompnyCode & "' and " & _
                                   " itemdesc = '" & Tchar(itemdesc.Text) & "' and merk = '" & Tchar(merk.Text) & "' "
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else : sSqlCheck1 &= " AND itemoid <> " & itemoid.Text
        End If
        If cKon.ambilscalar(sSqlCheck1) > 0 Then
            showMessage("Nama Barang dan Merk sudah dipakai ", CompnyName & " - WARNING")
            Exit Sub
        End If

        Session("ItemOid") = GenerateID("QL_mstitem", CompnyCode)

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            Upduser.Text = Session("UserID")
            Updtime.Text = Format(Now, "dd/MM/yyyy HH:mm:ss")

            If Session("oid") = Nothing Or Session("oid") = "" Then
                generateItemId(itemdesc.Text)

                sSql = "INSERT INTO QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc], " & _
                       "[itemgroupoid],[itemsubgroupoid],[itembarcode1]," & _
                       "[itembarcode2], [itembarcode3], " & _
                       "[itempriceunit1], [itempriceunit2], [itempriceunit3], " & _
                       "[satuan1], [satuan2], [satuan3], " & _
                       "[itemsafetystockunit1],[itempictureloc]," & _
                       "[acctgoid],[createuser],[upduser],[updtime], [itemflag], konversi1_2, konversi2_3,personoid, keterangan, editharga,merk,payoid,itemsafetystockunitgrosir,maxstockgrosir,maxstock) " & _
                       "values ('" & CompnyCode & "'," & _
                       "" & Session("ItemOid") & "," & _
                       "'" & Tchar(itemcode.Text) & "', " & _
                       "'" & Tchar(itemdesc.Text) & "'," & _
                       "" & itemgroupoid.SelectedValue & ", " & _
                       "" & itemsubgroupoid.SelectedValue & "," & _
                       "'" & Tchar(itembarcode1.Text) & "'," & _
                       "'" & Tchar(itembarcode2.Text) & "'," & _
                       "'" & Tchar(itembarcode3.Text) & "'," & _
                       "" & 0 & "," & _
                       "" & 0 & "," & _
                       "" & 0 & "," & _
                       "'" & satuan1.Items(satuan1.SelectedIndex).Value & "'," & _
                       "'" & satuan2.Items(satuan2.SelectedIndex).Value & "'," & _
                       "'" & satuan3.Items(satuan3.SelectedIndex).Value & "'," & _
                       "" & ToDouble(itemsafetystock.Text) & ", " & _
                       "'item_" & Trim(itemoid.Text) & ".jpg'," & _
                       "'" & acctgoid.SelectedValue & "'," & _
                       "'" & Session("UserID") & "', " & _
                       "'" & Session("UserID") & "'," & _
                       "current_timestamp, " & _
                       "'" & ddlStatus.SelectedValue & "', " & ToDouble(konversi1_2.Text) & "," & _
                       "'" & ToDouble(konversi2_3.Text) & "','" & spgOid.SelectedValue & "', '" & Tchar(keterangan.Text) & "', 'F','" & Tchar(merk.Text) & "'," & payment.SelectedValue & "," & ToDouble(itemsafetystockgrosir.Text) & "," & ToDouble(Maxstockgrosir.Text) & "," & ToDouble(Maxstock.Text) & ") "

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update QL_mstoid set lastoid=" & Session("ItemOid") & " where " & _
                       "tablename='QL_mstitem' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Insert ke Retail juga
                sSql = "INSERT INTO " & DB_JPT_RITEL & ".dbo.QL_MSTITEM ([cmpcode],[itemoid],[itemcode],[itemdesc], " & _
                     "[itemgroupoid],[itemsubgroupoid],[itembarcode1]," & _
                     "[itembarcode2], [itembarcode3], " & _
                     "[itempriceunit1], [itempriceunit2], [itempriceunit3], " & _
                     "[satuan1], [satuan2], [satuan3], " & _
                     "[itemsafetystockunit1],[itempictureloc]," & _
                     "[acctgoid],[createuser],[upduser],[updtime], [itemflag], konversi1_2, konversi2_3,personoid, keterangan, editharga,merk,payoid,itemsafetystockunitgrosir,maxstockgrosir,maxstock) " & _
                     "values ('" & CompnyCode & "'," & _
                     "" & Session("ItemOid") & "," & _
                     "'" & Tchar(itemcode.Text) & "', " & _
                     "'" & Tchar(itemdesc.Text) & "'," & _
                     "" & itemgroupoid.SelectedValue & ", " & _
                     "" & itemsubgroupoid.SelectedValue & "," & _
                     "'" & Tchar(itembarcode1.Text) & "'," & _
                     "'" & Tchar(itembarcode2.Text) & "'," & _
                     "'" & Tchar(itembarcode3.Text) & "'," & _
                     "" & 0 & "," & _
                     "" & 0 & "," & _
                     "" & 0 & "," & _
                     "'" & satuan1.Items(satuan1.SelectedIndex).Value & "'," & _
                     "'" & satuan2.Items(satuan2.SelectedIndex).Value & "'," & _
                     "'" & satuan3.Items(satuan3.SelectedIndex).Value & "'," & _
                     "" & ToDouble(itemsafetystock.Text) & ", " & _
                     "'item_" & Trim(itemoid.Text) & ".jpg'," & _
                     "'" & acctgoid.SelectedValue & "'," & _
                     "'" & Session("UserID") & "', " & _
                     "'" & Session("UserID") & "'," & _
                     "current_timestamp, " & _
                     "'" & ddlStatus.SelectedValue & "', " & ToDouble(konversi1_2.Text) & "," & _
                     "'" & ToDouble(konversi2_3.Text) & "','" & spgOid.SelectedValue & "', '" & Tchar(keterangan.Text) & "', 'F','" & Tchar(merk.Text) & "'," & payment.SelectedValue & "," & ToDouble(itemsafetystockgrosir.Text) & "," & ToDouble(Maxstockgrosir.Text) & "," & ToDouble(Maxstock.Text) & ") "

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "update " & DB_JPT_RITEL & ".dbo.QL_mstoid set lastoid=" & Session("ItemOid") & " where " & _
                       "tablename='QL_mstitem' and cmpcode like '%" & CompnyCode & "%' "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()


            Else
                sSql = "UPDATE QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                       "[itemname]='', " & _
                       "[itemdesc]='" & Tchar(itemdesc.Text) & "'," & _
                       " merk= '" & Tchar(merk.Text) & "', " & _
                       " payoid= '" & payment.SelectedValue & "', " & _
                       "[itemgroupoid]=" & itemgroupoid.SelectedValue & ", " & _
                       "[itemsubgroupoid]=" & itemsubgroupoid.SelectedValue & "," & _
                       "[itembarcode1]='" & Tchar(itembarcode1.Text) & "', " & _
                       "[itembarcode2]='" & Tchar(itembarcode2.Text) & "', " & _
                       "[itembarcode3]='" & Tchar(itembarcode3.Text) & "', " & _
                       "[itemsafetystockunit1]=" & ToDouble(itemsafetystock.Text) & ", " & _
                       "[itempictureloc]='item_" & Trim(itemoid.Text) & ".jpg'," & _
                       "[acctgoid]='" & acctgoid.SelectedValue & "', " & _
                       "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp,  [itemflag]='" & ddlStatus.SelectedValue & "' , konversi1_2=" & ToDouble(konversi1_2.Text) & ", konversi2_3=" & ToDouble(konversi2_3.Text) & ", personoid = '" & spgOid.SelectedValue & "', keterangan='" & Tchar(keterangan.Text) & "' ,itemsafetystockunitgrosir = " & ToDouble(itemsafetystockgrosir.Text) & ",maxstockgrosir =" & ToDouble(Maxstockgrosir.Text) & ",maxstock =" & ToDouble(Maxstock.Text) & "  WHERE cmpcode " & _
                       "like '%" & CompnyCode & "%' and itemoid=" & Session("oid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'update juga ke Retail
                sSql = "UPDATE " & DB_JPT_RITEL & ".dbo.QL_MSTITEM SET [itemcode]='" & Tchar(itemcode.Text) & "'," & _
                    "[itemname]='', " & _
                    "[itemdesc]='" & Tchar(itemdesc.Text) & "'," & _
                    " merk= '" & Tchar(merk.Text) & "', " & _
                    " payoid= '" & payment.SelectedValue & "', " & _
                    "[itemgroupoid]=" & itemgroupoid.SelectedValue & ", " & _
                    "[itemsubgroupoid]=" & itemsubgroupoid.SelectedValue & "," & _
                    "[itembarcode1]='" & Tchar(itembarcode1.Text) & "', " & _
                    "[itembarcode2]='" & Tchar(itembarcode2.Text) & "', " & _
                    "[itembarcode3]='" & Tchar(itembarcode3.Text) & "', " & _
                    "[itemsafetystockunit1]=" & ToDouble(itemsafetystock.Text) & ", " & _
                    "[itempictureloc]='item_" & Trim(itemoid.Text) & ".jpg'," & _
                    "[acctgoid]='" & acctgoid.SelectedValue & "', " & _
                    "[upduser]='" & Session("UserID") & "',[updtime]=current_timestamp,  konversi1_2=" & ToDouble(konversi1_2.Text) & ", konversi2_3=" & ToDouble(konversi2_3.Text) & " ,itemsafetystockunitgrosir = " & ToDouble(itemsafetystockgrosir.Text) & ",maxstockgrosir =" & ToDouble(Maxstockgrosir.Text) & ",maxstock =" & ToDouble(Maxstock.Text) & "  WHERE cmpcode " & _
                    "like '%" & CompnyCode & "%' and itemoid=" & Session("oid")

                '[itemflag]='" & ddlStatus.SelectedValue & "' , , personoid = '" & spgOid.SelectedValue & "', keterangan='" & Tchar(keterangan.Text) & "'

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.ToString & sSql, CompnyName & " - ERROR") : Exit Sub
        End Try
        Session("oid") = Nothing
        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah tersimpan !", "INFORMASI")
        Response.Redirect("~\Master\mstItemWOPricedtl.aspx")
    End Sub


    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        Dim sColomnName() As String = {"refoid", "itemoid", "itemoid"}
        Dim sTable() As String = {"ql_crdmtr", "QL_podtl", "QL_trnorderdtl"}
        If CheckDataExists(Session("oid"), sColomnName, sTable) = True Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.ql_crdmtr where refoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.QL_podtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If
        sSql = "select count(-1) from " & DB_JPT_RITEL & ".dbo.QL_trnorderdtl where itemoid=" & Session("oid")
        If GetStrData(sSql) > 0 Then
            showMessage("Anda tidak dapat menghapus data ini, karena sudah digunakan di transaksi lain(Retail) !", CompnyName & " - Warning")
            Exit Sub
        End If


        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "delete from " & DB_JPT_RITEL & ".dbo.QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                   "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            sSql = "delete from " & DB_JPT_GROSIR & ".dbo.QL_MSTITEM WHERE [cmpcode] = '" & CompnyCode & "' AND " & _
                  "[itemoid] = " & Session("oid") & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            showMessage(ex.Message, CompnyName & " - ERROR") : Exit Sub
        End Try

        Image1.ImageUrl = "~/images/information.png"
        'showMessage("Data telah terhapus !", "INFORMASI")
        Response.Redirect("~\Master\mstitem.aspx?awal=true")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Response.Redirect("~\Master\mstItemWOPricedtl.aspx?awal=true")
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PanelValidasi.Visible = False
        ButtonExtendervalidasi.Visible = False
        ModalPopupExtenderValidasi.Hide()

        If Validasi.Text = "Data telah tersimpan !" Or Validasi.Text = "Data telah terhapus !" Then
            Me.Response.Redirect("~\Master\mstItemWOPricedtl.aspx?awal=true")
        End If

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim fname As String = getFileName(Trim(itemoid.Text))
        uploadFileGambar("insert", fname)
    End Sub

#End Region


    Protected Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "SUMMARY"
        orderNoForReport.Text = "" : orderIDForReport.Text = ""
        Session("NoItem") = ""
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)

    End Sub

   

    Protected Sub itemdesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemdesc.TextChanged
        itemdesc.Text = itemdesc.Text.Replace("'", "")
        If (Session("oid") <> Nothing And Session("oid") <> "") = False Then
            generateItemId(itemdesc.Text)
        End If
    End Sub

    Protected Sub generateItemId(ByVal namaBarang As String)
        Dim namaItem, jumlahNol, c, c2, kodeItem As String
        jumlahNol = ""
        Dim a, b, d As Integer
        namaItem = Left(itemdesc.Text, 1).ToUpper
        c = ToDouble(GetStrData("SELECT isnull(max(abs(replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))
        c2 = ToDouble(GetStrData("SELECT isnull(max(abs(replace(itemcode, '" & Tchar(namaItem) & "', ''))),0) FROM " & DB_JPT_RITEL & ".dbo.ql_mstitem WHERE left(itemcode,1) = '" & Tchar(namaItem) & "' "))

        If CInt(c) > CInt(c2) Then
            d = CInt(c) + 1
        Else
            d = CInt(c2) + 1
        End If


        kodeItem = GenNumberString(namaItem, "", d, 5)
        itemcode.Text = kodeItem
    End Sub
 

    Protected Sub konversi1_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        konversi1_2.Text = NewMaskEdit(ToDouble(konversi1_2.Text))
    End Sub

    Protected Sub itemsafetystock_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemsafetystock.Text = NewMaskEdit(ToDouble(itemsafetystock.Text))
    End Sub

    Protected Sub konversi2_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        konversi2_3.Text = NewMaskEdit(ToDouble(konversi2_3.Text))
    End Sub

    Protected Sub GVmstgen_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmstgen.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = NewMaskEdit(ToDouble(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = NewMaskEdit(ToDouble(e.Row.Cells(5).Text))
            e.Row.Cells(7).Text = NewMaskEdit(ToDouble(e.Row.Cells(7).Text))
            e.Row.Cells(9).Text = NewMaskEdit(ToDouble(e.Row.Cells(9).Text))

            e.Row.Cells(12).Text = NewMaskEdit(ToDouble(e.Row.Cells(12).Text))
            e.Row.Cells(13).Text = NewMaskEdit(ToDouble(e.Row.Cells(13).Text))
            e.Row.Cells(14).Text = NewMaskEdit(ToDouble(e.Row.Cells(14).Text))

        End If
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Cells(8).Text = NewMaskEdit(ToDouble(e.Row.Cells(8).Text))
        'End If

    End Sub

    Protected Sub btnprintlist_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        printType.Text = "DETAIL"
        orderNoForReport.Text = sender.ToolTip : orderIDForReport.Text = sender.CommandArgument()
        Session("NoItem") = sender.ToolTip
        lblError.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, True)
    End Sub

    Protected Sub imbPrintPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Try
            PrintContract(printType.Text, orderIDForReport.Text, orderNoForReport.Text, ExportFormatType.PortableDocFormat)
        Catch ex As Exception
            lblError.Text = ex.ToString
        End Try
        mpePrint.Show()
    End Sub

    Protected Sub imbPrintExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblError.Text = ""
        Response.Clear()
        Response.AddHeader("content-disposition", "inline;filename=mstitem.xls")
        Response.Charset = ""
        'set the response mime type for excel
        Response.ContentType = "application/vnd.ms-excel"
        Dim sWhere As String = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        sWhere &= "  and  upper(" & FilterDDL2.Items(FilterDDL2.SelectedIndex).Value & ") like '%" & Tchar(FilterText2.Text) & "%' "
        sWhere &= "  and  upper(" & FilterDDL3.Items(FilterDDL3.SelectedIndex).Value & ") like '%" & Tchar(FilterText3.Text) & "%' "

        sSql = "SELECT 	  itemoid, itemcode, itemdesc,merk,keterangan lokasi , QL_mstgen.gendesc as itemgroup, itembarcode1, itembarcode2, itembarcode3,   g1.gendesc as satuan1, g2.gendesc as satuan2, g3.gendesc as satuan3,konversi1_2,konversi2_3, itemsafetystockunit1, QL_mstitem.createuser, QL_mstitem.Upduser, QL_mstitem.Updtime, itemflag, personname as PIC FROM QL_mstitem inner join QL_mstgen on QL_mstitem.itemgroupoid =  QL_mstgen.genoid inner join QL_mstperson on QL_mstitem.personoid = QL_mstperson.personoid inner join ql_mstgen g1 on g1.genoid=QL_mstitem.satuan1  inner join ql_mstgen g2 on g2.genoid=QL_mstitem.satuan2  inner join ql_mstgen g3 on g3.genoid=QL_mstitem.satuan3   " & sWhere & " ORDER BY itemdesc    "

        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
         Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim xcmd As SqlCommand = New SqlCommand(sSql, conn)
        da.SelectCommand = xcmd
        da.Fill(ds)
        Dim dt As DataTable = CType(Application.Item("MyDataTable"), DataTable)
        dt = ds.Tables(0)
        Response.Write(ConvertDtToTDF(dt))
             conn.Close()
      

        Response.End()
        mpePrint.Show()
    End Sub

    Protected Sub imbCancelPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        orderIDForReport.Text = "" : orderNoForReport.Text = ""
        cProc.SetModalPopUpExtender(btnHidePrint, pnlPrint, mpePrint, False)
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        rptSupp.Close() : rptSupp.Dispose()
    End Sub

    Private Sub PrintContract(ByVal printType As String, ByVal oid As String, ByVal no As String, ByVal formatReport As ExportFormatType)
        Dim sWhere As String = "  where  upper(" & FilterDDL.Items(FilterDDL.SelectedIndex).Value & ") like '%" & Tchar(FilterText.Text) & "%' "
        If printType = "SUMMARY" Then
            rptSupp.Load(Server.MapPath("~/report/ItemSummary.rpt"))

            rptSupp.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(rptSupp, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()

            rptSupp.ExportToHttpResponse(formatReport, Response, True, "Data_All_Barang")

        ElseIf printType = "DETAIL" Then
            rptSupp.Load(Server.MapPath("~/report/ItemDetail.rpt"))
            rptSupp.SetParameterValue("sWhere", sWhere)
            rptSupp.SetParameterValue("itemid", oid)

            cProc.SetDBLogonForReport(rptSupp, System.Configuration.ConfigurationManager.AppSettings("Report-DB-Server"), _
                System.Configuration.ConfigurationManager.AppSettings("Report-DB-Name"))

            Response.Buffer = False

            'Clear the response content and headers
            Response.ClearContent()
            Response.ClearHeaders()
            rptSupp.ExportToHttpResponse(formatReport, Response, True, "Data_Barang_" & no.Replace("/", "_"))

        End If



    End Sub
 
    Protected Sub GVmstgen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub satuan2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles satuan2.SelectedIndexChanged
        'sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMUNIT' and cmpcode='" & CompnyCode & "' and genoid = '" & satuan2.SelectedValue & "' ORDER BY gendesc"
        'FillDDL(satuan3, sSql)
    End Sub

    Protected Sub Maxstock_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Maxstock.Text = NewMaskEdit(ToDouble(Maxstock.Text))
    End Sub

    Protected Sub itemsafetystockgrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        itemsafetystockgrosir.Text = NewMaskEdit(ToDouble(itemsafetystockgrosir.Text))
    End Sub

    Protected Sub Maxstockgrosir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Maxstockgrosir.Text = NewMaskEdit(ToDouble(Maxstockgrosir.Text))
    End Sub

    Protected Sub itemgroupoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "select genoid,gendesc from QL_mstgen where gengroup='ITEMSUBGROUP' and (genother1='' or genother1='" & itemgroupoid.SelectedValue & "') and cmpcode='" & CompnyCode & "' ORDER BY gendesc"

        If cKon.ambilscalar(sSql) > 0 Then
            FillDDL(itemsubgroupoid, sSql)
        Else
            showMessage("Isi Sub Grup Barang di Data General terlebih dahulu !!", CompnyName & " - INFORMASI")
            btnSave.Visible = False
        End If
    End Sub
End Class